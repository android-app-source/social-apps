.class public final LX/BrC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic b:LX/0hy;

.field public final synthetic c:LX/0Zb;

.field public final synthetic d:Lcom/facebook/content/SecureContextHelper;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/0hy;LX/0Zb;Lcom/facebook/content/SecureContextHelper;)V
    .locals 0

    .prologue
    .line 1826012
    iput-object p1, p0, LX/BrC;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iput-object p2, p0, LX/BrC;->b:LX/0hy;

    iput-object p3, p0, LX/BrC;->c:LX/0Zb;

    iput-object p4, p0, LX/BrC;->d:Lcom/facebook/content/SecureContextHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x0

    const/4 v0, 0x2

    const/4 v2, 0x1

    const v3, -0x1b957f72

    invoke-static {v0, v2, v3}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 1826013
    iget-object v0, p0, LX/BrC;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/BrC;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1826014
    iget-object v3, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v3

    .line 1826015
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/BrC;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1826016
    iget-object v3, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v3

    .line 1826017
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->v()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/BrC;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1826018
    iget-object v3, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v3

    .line 1826019
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->v()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/BrC;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1826020
    iget-object v3, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v3

    .line 1826021
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->v()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->K()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1826022
    iget-object v0, p0, LX/BrC;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1826023
    iget-object v3, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v3

    .line 1826024
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->v()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->K()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v3

    .line 1826025
    iget-object v0, p0, LX/BrC;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1826026
    iget-object v4, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v4

    .line 1826027
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-static {v0}, LX/6X3;->c(Lcom/facebook/graphql/model/GraphQLNode;)Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v4

    .line 1826028
    if-nez v4, :cond_1

    .line 1826029
    const v0, 0x61858769

    invoke-static {v0, v2}, LX/02F;->a(II)V

    .line 1826030
    :goto_0
    return-void

    .line 1826031
    :cond_0
    const v0, 0x267f1bf8

    invoke-static {v0, v2}, LX/02F;->a(II)V

    goto :goto_0

    .line 1826032
    :cond_1
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLComment;->v()Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v5

    .line 1826033
    new-instance v0, LX/89k;

    invoke-direct {v0}, LX/89k;-><init>()V

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v6

    .line 1826034
    iput-object v6, v0, LX/89k;->b:Ljava/lang/String;

    .line 1826035
    move-object v0, v0

    .line 1826036
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v6

    .line 1826037
    iput-object v6, v0, LX/89k;->c:Ljava/lang/String;

    .line 1826038
    move-object v6, v0

    .line 1826039
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v0

    .line 1826040
    :goto_1
    iput-object v0, v6, LX/89k;->e:Ljava/lang/String;

    .line 1826041
    move-object v0, v6

    .line 1826042
    if-eqz v5, :cond_2

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLComment;->z()Ljava/lang/String;

    move-result-object v1

    .line 1826043
    :cond_2
    iput-object v1, v0, LX/89k;->d:Ljava/lang/String;

    .line 1826044
    move-object v0, v0

    .line 1826045
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLComment;->z()Ljava/lang/String;

    move-result-object v1

    .line 1826046
    iput-object v1, v0, LX/89k;->f:Ljava/lang/String;

    .line 1826047
    move-object v0, v0

    .line 1826048
    sget-object v1, LX/21y;->DEFAULT_ORDER:LX/21y;

    .line 1826049
    iput-object v1, v0, LX/89k;->i:LX/21y;

    .line 1826050
    move-object v0, v0

    .line 1826051
    invoke-virtual {v0}, LX/89k;->a()Lcom/facebook/ipc/feed/PermalinkStoryIdParams;

    move-result-object v0

    .line 1826052
    iget-object v1, p0, LX/BrC;->b:LX/0hy;

    invoke-interface {v1, v0}, LX/0hy;->a(Lcom/facebook/ipc/feed/PermalinkStoryIdParams;)Landroid/content/Intent;

    move-result-object v1

    .line 1826053
    if-eqz v1, :cond_3

    .line 1826054
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v0, "comment_share_story_attachment_click"

    invoke-direct {v3, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1826055
    if-eqz v5, :cond_5

    const-string v0, "reply"

    .line 1826056
    :goto_2
    const-string v4, "click_type"

    invoke-virtual {v3, v4, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1826057
    iget-object v0, p0, LX/BrC;->c:LX/0Zb;

    invoke-interface {v0, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1826058
    iget-object v0, p0, LX/BrC;->d:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v0, v1, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1826059
    :cond_3
    const v0, 0x59f1ab7b

    invoke-static {v0, v2}, LX/02F;->a(II)V

    goto :goto_0

    :cond_4
    move-object v0, v1

    .line 1826060
    goto :goto_1

    .line 1826061
    :cond_5
    const-string v0, "top_level_comment"

    goto :goto_2
.end method
