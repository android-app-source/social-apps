.class public final LX/B2i;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Z

.field public c:Z

.field public d:Z

.field public e:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1738877
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$LocalGroupModerationFieldsModel;
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v2, 0x0

    .line 1738878
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1738879
    iget-object v1, p0, LX/B2i;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1738880
    const/4 v3, 0x5

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 1738881
    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 1738882
    iget-boolean v1, p0, LX/B2i;->b:Z

    invoke-virtual {v0, v4, v1}, LX/186;->a(IZ)V

    .line 1738883
    const/4 v1, 0x2

    iget-boolean v3, p0, LX/B2i;->c:Z

    invoke-virtual {v0, v1, v3}, LX/186;->a(IZ)V

    .line 1738884
    const/4 v1, 0x3

    iget-boolean v3, p0, LX/B2i;->d:Z

    invoke-virtual {v0, v1, v3}, LX/186;->a(IZ)V

    .line 1738885
    const/4 v1, 0x4

    iget-boolean v3, p0, LX/B2i;->e:Z

    invoke-virtual {v0, v1, v3}, LX/186;->a(IZ)V

    .line 1738886
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 1738887
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1738888
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1738889
    invoke-virtual {v1, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1738890
    new-instance v0, LX/15i;

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1738891
    new-instance v1, Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$LocalGroupModerationFieldsModel;

    invoke-direct {v1, v0}, Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$LocalGroupModerationFieldsModel;-><init>(LX/15i;)V

    .line 1738892
    return-object v1
.end method
