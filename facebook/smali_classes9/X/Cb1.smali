.class public LX/Cb1;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/75B;

.field private final b:LX/Caq;

.field public c:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLInterfaces$TagInfoQuery$Edges;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Ri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ri",
            "<",
            "Landroid/graphics/RectF;",
            "Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLInterfaces$FaceBoxInfo;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Ri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ri",
            "<",
            "Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLInterfaces$TagInfoQuery$Edges;",
            "Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLInterfaces$FaceBoxInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/75B;LX/Caq;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1918887
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1918888
    invoke-static {}, LX/0R9;->b()Ljava/util/LinkedList;

    move-result-object v0

    iput-object v0, p0, LX/Cb1;->c:Ljava/util/LinkedList;

    .line 1918889
    invoke-static {}, LX/1Ei;->a()LX/1Ei;

    move-result-object v0

    iput-object v0, p0, LX/Cb1;->d:LX/0Ri;

    .line 1918890
    invoke-static {}, LX/1Ei;->a()LX/1Ei;

    move-result-object v0

    iput-object v0, p0, LX/Cb1;->e:LX/0Ri;

    .line 1918891
    iput-object p1, p0, LX/Cb1;->a:LX/75B;

    .line 1918892
    iput-object p2, p0, LX/Cb1;->b:LX/Caq;

    .line 1918893
    return-void
.end method

.method private static a(Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel;Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxInfoModel;)D
    .locals 6

    .prologue
    .line 1918933
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1918934
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1918935
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel;->c()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoModel;->c()LX/1f8;

    move-result-object v0

    invoke-interface {v0}, LX/1f8;->a()D

    move-result-wide v0

    double-to-float v0, v0

    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel;->c()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoModel;->c()LX/1f8;

    move-result-object v1

    invoke-interface {v1}, LX/1f8;->b()D

    move-result-wide v2

    double-to-float v1, v2

    invoke-virtual {p1}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxInfoModel;->b()LX/1f8;

    move-result-object v2

    invoke-interface {v2}, LX/1f8;->a()D

    move-result-wide v2

    double-to-float v2, v2

    invoke-virtual {p1}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxInfoModel;->b()LX/1f8;

    move-result-object v3

    invoke-interface {v3}, LX/1f8;->b()D

    move-result-wide v4

    double-to-float v3, v4

    invoke-static {p1}, LX/Caq;->a(Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxInfoModel;)Landroid/graphics/RectF;

    move-result-object v4

    invoke-static {v0, v1, v2, v3, v4}, LX/75B;->a(FFFFLandroid/graphics/RectF;)D

    move-result-wide v0

    return-wide v0
.end method

.method public static a(LX/0QB;)LX/Cb1;
    .locals 3

    .prologue
    .line 1918922
    new-instance v2, LX/Cb1;

    invoke-static {p0}, LX/75B;->a(LX/0QB;)LX/75B;

    move-result-object v0

    check-cast v0, LX/75B;

    invoke-static {p0}, LX/Caq;->a(LX/0QB;)LX/Caq;

    move-result-object v1

    check-cast v1, LX/Caq;

    invoke-direct {v2, v0, v1}, LX/Cb1;-><init>(LX/75B;LX/Caq;)V

    .line 1918923
    move-object v0, v2

    .line 1918924
    return-object v0
.end method

.method private static a(LX/Cb1;Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxInfoModel;Ljava/util/LinkedList;)Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLInterfaces$FaceBoxInfo;",
            "Ljava/util/LinkedList",
            "<",
            "Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLInterfaces$TagInfoQuery$Edges;",
            ">;)",
            "Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLInterfaces$TagInfoQuery$Edges;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1918925
    const/4 v1, 0x0

    .line 1918926
    const-wide v4, 0x7fefffffffffffffL    # Double.MAX_VALUE

    .line 1918927
    invoke-virtual {p2}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel;

    .line 1918928
    iget-object v2, p0, LX/Cb1;->e:LX/0Ri;

    invoke-interface {v2, v0}, LX/0Ri;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1918929
    invoke-static {v0, p1}, LX/Cb1;->a(Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel;Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxInfoModel;)D

    move-result-wide v2

    .line 1918930
    cmpg-double v7, v2, v4

    if-gez v7, :cond_2

    move-wide v8, v2

    move-object v2, v0

    move-wide v0, v8

    :goto_1
    move-wide v4, v0

    move-object v1, v2

    .line 1918931
    goto :goto_0

    .line 1918932
    :cond_1
    return-object v1

    :cond_2
    move-object v2, v1

    move-wide v0, v4

    goto :goto_1
.end method


# virtual methods
.method public final a()LX/0Rh;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rh",
            "<",
            "Landroid/graphics/RectF;",
            "Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLInterfaces$FaceBoxInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1918921
    iget-object v0, p0, LX/Cb1;->d:LX/0Ri;

    invoke-static {v0}, LX/0Rh;->a(Ljava/util/Map;)LX/0Rh;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/5kD;Z)V
    .locals 12
    .param p1    # LX/5kD;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1918894
    iget-object v0, p0, LX/Cb1;->c:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 1918895
    iget-object v0, p0, LX/Cb1;->d:LX/0Ri;

    invoke-interface {v0}, LX/0Ri;->clear()V

    .line 1918896
    iget-object v0, p0, LX/Cb1;->e:LX/0Ri;

    invoke-interface {v0}, LX/0Ri;->clear()V

    .line 1918897
    if-nez p1, :cond_1

    .line 1918898
    :cond_0
    return-void

    .line 1918899
    :cond_1
    invoke-interface {p1}, LX/5kD;->an()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, LX/5kD;->an()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1918900
    iget-object v0, p0, LX/Cb1;->c:Ljava/util/LinkedList;

    invoke-interface {p1}, LX/5kD;->an()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->addAll(Ljava/util/Collection;)Z

    .line 1918901
    :cond_2
    if-eqz p2, :cond_4

    invoke-interface {p1}, LX/5kD;->F()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PhotosFaceBoxesQueryModel;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-interface {p1}, LX/5kD;->F()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PhotosFaceBoxesQueryModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PhotosFaceBoxesQueryModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1918902
    invoke-interface {p1}, LX/5kD;->F()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PhotosFaceBoxesQueryModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$PhotosFaceBoxesQueryModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_4

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxInfoModel;

    .line 1918903
    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxInfoModel;->b()LX/1f8;

    move-result-object v4

    if-eqz v4, :cond_3

    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxInfoModel;->c()LX/1f8;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 1918904
    iget-object v4, p0, LX/Cb1;->d:LX/0Ri;

    invoke-static {v0}, LX/Caq;->a(Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxInfoModel;)Landroid/graphics/RectF;

    move-result-object v5

    invoke-interface {v4, v5, v0}, LX/0Ri;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1918905
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1918906
    :cond_4
    iget-object v0, p0, LX/Cb1;->c:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->listIterator()Ljava/util/ListIterator;

    move-result-object v7

    .line 1918907
    :cond_5
    :goto_1
    invoke-interface {v7}, Ljava/util/ListIterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1918908
    invoke-interface {v7}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel;

    .line 1918909
    const/4 v6, 0x0

    .line 1918910
    const-wide v2, 0x7fefffffffffffffL    # Double.MAX_VALUE

    .line 1918911
    iget-object v1, p0, LX/Cb1;->d:LX/0Ri;

    invoke-interface {v1}, LX/0Ri;->b_()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxInfoModel;

    .line 1918912
    invoke-static {v0, v1}, LX/Cb1;->a(Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel;Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxInfoModel;)D

    move-result-wide v4

    .line 1918913
    const-wide/high16 v10, 0x3ff8000000000000L    # 1.5

    cmpg-double v9, v4, v10

    if-gez v9, :cond_8

    if-eqz v6, :cond_6

    cmpg-double v9, v4, v2

    if-gez v9, :cond_8

    :cond_6
    move-wide v2, v4

    :goto_3
    move-object v6, v1

    .line 1918914
    goto :goto_2

    .line 1918915
    :cond_7
    if-eqz v6, :cond_5

    .line 1918916
    iget-object v1, p0, LX/Cb1;->c:Ljava/util/LinkedList;

    invoke-static {p0, v6, v1}, LX/Cb1;->a(LX/Cb1;Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxInfoModel;Ljava/util/LinkedList;)Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel;

    move-result-object v1

    .line 1918917
    if-ne v1, v0, :cond_5

    .line 1918918
    iget-object v1, p0, LX/Cb1;->e:LX/0Ri;

    invoke-interface {v1, v0, v6}, LX/0Ri;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1918919
    iget-object v0, p0, LX/Cb1;->d:LX/0Ri;

    invoke-interface {v0}, LX/0Ri;->a_()LX/0Ri;

    move-result-object v0

    invoke-interface {v0, v6}, LX/0Ri;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1918920
    invoke-interface {v7}, Ljava/util/ListIterator;->remove()V

    goto :goto_1

    :cond_8
    move-object v1, v6

    goto :goto_3
.end method
