.class public final LX/BJX;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;)V
    .locals 0

    .prologue
    .line 1772123
    iput-object p1, p0, LX/BJX;->a:Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 2

    .prologue
    .line 1772124
    sparse-switch p1, :sswitch_data_0

    .line 1772125
    :goto_0
    return-void

    .line 1772126
    :sswitch_0
    goto :goto_0

    .line 1772127
    :sswitch_1
    iget-object v0, p0, LX/BJX;->a:Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;

    .line 1772128
    invoke-static {v0}, Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;->n(Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;)V

    .line 1772129
    iget-object v1, v0, Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;->d:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;

    if-nez v1, :cond_0

    .line 1772130
    new-instance v1, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;

    invoke-direct {v1}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;-><init>()V

    iput-object v1, v0, Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;->d:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;

    .line 1772131
    iget-object v1, v0, Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;->d:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;

    iget-object p0, v0, Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;->e:LX/BJU;

    .line 1772132
    iput-object p0, v1, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->t:LX/BJU;

    .line 1772133
    iget-object v1, v0, Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;->d:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;

    iget-object p0, v0, Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;->f:LX/BJV;

    .line 1772134
    iput-object p0, v1, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->n:LX/BJV;

    .line 1772135
    iget-object v1, v0, Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;->d:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;

    iget-object p0, v0, Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;->g:LX/BJW;

    .line 1772136
    iput-object p0, v1, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->o:LX/BJW;

    .line 1772137
    iget-object v1, v0, Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;->d:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;

    iget-object p0, v0, Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;->b:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    .line 1772138
    iget-object p1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->as:LX/BKO;

    move-object p0, p1

    .line 1772139
    iput-object p0, v1, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->q:LX/BKO;

    .line 1772140
    iget-object v1, v0, Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;->d:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;

    iget-object p0, v0, Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;->b:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    .line 1772141
    iget-object p1, p0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->av:LX/BJo;

    move-object p0, p1

    .line 1772142
    iput-object p0, v1, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->p:LX/BJo;

    .line 1772143
    iget-object v1, v0, Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;->d:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;

    iget-object p0, v0, Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;->c:LX/BJX;

    .line 1772144
    iput-object p0, v1, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->e:LX/BJX;

    .line 1772145
    :cond_0
    iget-object v1, v0, Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;->d:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;

    move-object v1, v1

    .line 1772146
    const/4 p0, 0x0

    .line 1772147
    iput-boolean p0, v1, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->u:Z

    .line 1772148
    invoke-static {v0, v1}, Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;->a(Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;Lcom/facebook/base/fragment/FbFragment;)V

    .line 1772149
    goto :goto_0

    .line 1772150
    :sswitch_2
    iget-object v0, p0, LX/BJX;->a:Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;

    .line 1772151
    iget-object v1, v0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 1772152
    new-instance p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendSearchFragment;

    invoke-direct {p0}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendSearchFragment;-><init>()V

    .line 1772153
    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1772154
    move-object v1, p0

    .line 1772155
    iget-object p0, v0, Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;->e:LX/BJU;

    .line 1772156
    iput-object p0, v1, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerProfileSearchFragment;->a:LX/BJU;

    .line 1772157
    move-object v1, v1

    .line 1772158
    invoke-static {v0, v1}, Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;->a(Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;Lcom/facebook/base/fragment/FbFragment;)V

    .line 1772159
    goto :goto_0

    .line 1772160
    :sswitch_3
    iget-object v0, p0, LX/BJX;->a:Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;

    .line 1772161
    iget-object v1, v0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 1772162
    new-instance p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupSearchFragment;

    invoke-direct {p0}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupSearchFragment;-><init>()V

    .line 1772163
    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1772164
    move-object v1, p0

    .line 1772165
    iget-object p0, v0, Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;->e:LX/BJU;

    .line 1772166
    iput-object p0, v1, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerProfileSearchFragment;->a:LX/BJU;

    .line 1772167
    move-object v1, v1

    .line 1772168
    invoke-static {v0, v1}, Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;->a(Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;Lcom/facebook/base/fragment/FbFragment;)V

    .line 1772169
    goto :goto_0

    .line 1772170
    :sswitch_4
    iget-object v0, p0, LX/BJX;->a:Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;

    .line 1772171
    invoke-static {v0}, Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;->l(Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;)Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacySearchFragment;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;->a(Lcom/facebook/platform/composer/composer/PlatformComposerControllerFragment;Lcom/facebook/base/fragment/FbFragment;)V

    .line 1772172
    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x14 -> :sswitch_0
        0x46 -> :sswitch_1
        0x50 -> :sswitch_2
        0x5a -> :sswitch_3
        0x64 -> :sswitch_4
    .end sparse-switch
.end method
