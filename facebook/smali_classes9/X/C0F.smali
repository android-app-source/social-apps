.class public final LX/C0F;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/35h;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/1Pm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public final synthetic c:LX/35h;


# direct methods
.method public constructor <init>(LX/35h;)V
    .locals 1

    .prologue
    .line 1840185
    iput-object p1, p0, LX/C0F;->c:LX/35h;

    .line 1840186
    move-object v0, p1

    .line 1840187
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1840188
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1840170
    const-string v0, "ShareAttachmentComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1840171
    if-ne p0, p1, :cond_1

    .line 1840172
    :cond_0
    :goto_0
    return v0

    .line 1840173
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1840174
    goto :goto_0

    .line 1840175
    :cond_3
    check-cast p1, LX/C0F;

    .line 1840176
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1840177
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1840178
    if-eq v2, v3, :cond_0

    .line 1840179
    iget-object v2, p0, LX/C0F;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/C0F;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/C0F;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1840180
    goto :goto_0

    .line 1840181
    :cond_5
    iget-object v2, p1, LX/C0F;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 1840182
    :cond_6
    iget-object v2, p0, LX/C0F;->b:LX/1Pm;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/C0F;->b:LX/1Pm;

    iget-object v3, p1, LX/C0F;->b:LX/1Pm;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1840183
    goto :goto_0

    .line 1840184
    :cond_7
    iget-object v2, p1, LX/C0F;->b:LX/1Pm;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
