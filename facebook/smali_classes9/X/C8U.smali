.class public final LX/C8U;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/C33;

.field public final synthetic b:Landroid/content/Intent;

.field public final synthetic c:Lcom/facebook/feedplugins/hotconversations/HotConversationPermalinkOnClickPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/hotconversations/HotConversationPermalinkOnClickPartDefinition;LX/C33;Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 1852801
    iput-object p1, p0, LX/C8U;->c:Lcom/facebook/feedplugins/hotconversations/HotConversationPermalinkOnClickPartDefinition;

    iput-object p2, p0, LX/C8U;->a:LX/C33;

    iput-object p3, p0, LX/C8U;->b:Landroid/content/Intent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x6b4c20a4

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1852802
    iget-object v1, p0, LX/C8U;->c:Lcom/facebook/feedplugins/hotconversations/HotConversationPermalinkOnClickPartDefinition;

    iget-object v1, v1, Lcom/facebook/feedplugins/hotconversations/HotConversationPermalinkOnClickPartDefinition;->c:LX/C3a;

    iget-object v2, p0, LX/C8U;->a:LX/C33;

    .line 1852803
    iget-object v3, v2, LX/C33;->b:LX/C34;

    sget-object v5, LX/C34;->GROUP_RELATED_STORIES:LX/C34;

    invoke-virtual {v3, v5}, LX/C34;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1852804
    const-string v3, "grs_click"

    invoke-static {v2}, LX/C3a;->c(LX/C33;)LX/162;

    move-result-object v5

    invoke-static {v3, v5}, LX/17Q;->d(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    .line 1852805
    iget-object v5, v1, LX/C3a;->a:LX/0Zb;

    invoke-interface {v5, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1852806
    :cond_0
    iget-object v1, p0, LX/C8U;->c:Lcom/facebook/feedplugins/hotconversations/HotConversationPermalinkOnClickPartDefinition;

    iget-object v1, v1, Lcom/facebook/feedplugins/hotconversations/HotConversationPermalinkOnClickPartDefinition;->b:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/C8U;->b:Landroid/content/Intent;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1852807
    const v1, 0x3923dfa8

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
