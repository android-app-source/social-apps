.class public LX/Azk;
.super Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;
.source ""

# interfaces
.implements LX/Azj;


# instance fields
.field public a:Landroid/content/Context;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/1Ns;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/1Nq;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/1aw;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1732921
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/Azk;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1732922
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 1732910
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1732911
    const-class v0, LX/Azk;

    invoke-static {v0, p0}, LX/Azk;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1732912
    const v0, 0x7f0307b8

    invoke-static {p1, v0, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1732913
    invoke-virtual {p0, v1}, LX/Azk;->setOrientation(I)V

    .line 1732914
    invoke-virtual {p0, v1}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setSegmentedDividerThickness(I)V

    .line 1732915
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setShowSegmentedDividers(I)V

    .line 1732916
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00fc

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setSegmentedDivider(Landroid/graphics/drawable/Drawable;)V

    .line 1732917
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setSegmentedDividerPadding(I)V

    .line 1732918
    iget-object v0, p0, LX/Azk;->b:LX/1Ns;

    new-instance v1, LX/1Qg;

    invoke-direct {v1}, LX/1Qg;-><init>()V

    new-instance v2, LX/AnN;

    invoke-direct {v2}, LX/AnN;-><init>()V

    invoke-virtual {v0, v1, v2}, LX/1Ns;->a(LX/1Qh;LX/1DQ;)LX/1aw;

    move-result-object v0

    iput-object v0, p0, LX/Azk;->d:LX/1aw;

    .line 1732919
    new-instance v0, LX/Azi;

    invoke-direct {v0, p0}, LX/Azi;-><init>(LX/Azk;)V

    invoke-virtual {p0, v0}, LX/Azk;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1732920
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/Azk;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    const-class v2, LX/1Ns;

    invoke-interface {p0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/1Ns;

    invoke-static {p0}, LX/1Nq;->b(LX/0QB;)LX/1Nq;

    move-result-object p0

    check-cast p0, LX/1Nq;

    iput-object v1, p1, LX/Azk;->a:Landroid/content/Context;

    iput-object v2, p1, LX/Azk;->b:LX/1Ns;

    iput-object p0, p1, LX/Azk;->c:LX/1Nq;

    return-void
.end method


# virtual methods
.method public final a()Landroid/view/View;
    .locals 0

    .prologue
    .line 1732909
    return-object p0
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 1732905
    return-void
.end method

.method public getAspectRatio()F
    .locals 1

    .prologue
    .line 1732908
    const/high16 v0, 0x40000000    # 2.0f

    return v0
.end method

.method public getGlyphLabel()I
    .locals 1

    .prologue
    .line 1732907
    const v0, 0x7f020315

    return v0
.end method

.method public getLoggingType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1732906
    const-string v0, "composer_tab"

    return-object v0
.end method
