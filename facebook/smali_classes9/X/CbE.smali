.class public final LX/CbE;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Ljava/util/Map",
        "<",
        "Ljava/lang/String;",
        "Lcom/facebook/tagging/model/TaggingProfile;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/util/List;

.field public final synthetic b:LX/7Gn;

.field public final synthetic c:LX/CbF;


# direct methods
.method public constructor <init>(LX/CbF;Ljava/util/List;LX/7Gn;)V
    .locals 0

    .prologue
    .line 1919292
    iput-object p1, p0, LX/CbE;->c:LX/CbF;

    iput-object p2, p0, LX/CbE;->a:Ljava/util/List;

    iput-object p3, p0, LX/CbE;->b:LX/7Gn;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1919293
    iget-object v0, p0, LX/CbE;->c:LX/CbF;

    iget-object v0, v0, LX/CbF;->c:LX/03V;

    const-string v1, "TaggingProfileFactorytag_suggestion_lookup_failed"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1919294
    iget-object v0, p0, LX/CbE;->c:LX/CbF;

    iget-object v0, v0, LX/CbF;->b:LX/Cao;

    new-instance v1, LX/CbD;

    invoke-direct {v1, p0}, LX/CbD;-><init>(LX/CbE;)V

    invoke-virtual {v0, v1}, LX/Cao;->a(LX/Can;)V

    .line 1919295
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1919296
    check-cast p1, Ljava/util/Map;

    .line 1919297
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 1919298
    iget-object v0, p0, LX/CbE;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1919299
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1919300
    :cond_0
    iget-object v0, p0, LX/CbE;->b:LX/7Gn;

    invoke-interface {v0, v1}, LX/7Gn;->setTagSuggestions(Ljava/util/List;)V

    .line 1919301
    return-void
.end method
