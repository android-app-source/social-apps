.class public LX/ATz;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1677752
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/photos/base/media/PhotoItem;Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Lcom/facebook/graphql/model/GraphQLMedia;
    .locals 6
    .param p1    # Lcom/facebook/graphql/model/GraphQLTextWithEntities;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1677728
    if-eqz p0, :cond_0

    .line 1677729
    new-instance v0, LX/2dc;

    invoke-direct {v0}, LX/2dc;-><init>()V

    invoke-virtual {p0}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1677730
    iput-object v1, v0, LX/2dc;->h:Ljava/lang/String;

    .line 1677731
    move-object v0, v0

    .line 1677732
    invoke-virtual {v0}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    .line 1677733
    new-instance v1, LX/4XB;

    invoke-direct {v1}, LX/4XB;-><init>()V

    .line 1677734
    iput-object v0, v1, LX/4XB;->U:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1677735
    move-object v1, v1

    .line 1677736
    iput-object v0, v1, LX/4XB;->ac:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1677737
    move-object v1, v1

    .line 1677738
    iput-object v0, v1, LX/4XB;->V:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1677739
    move-object v0, v1

    .line 1677740
    iput-object p1, v0, LX/4XB;->aK:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1677741
    move-object v0, v0

    .line 1677742
    new-instance v1, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v2, 0x4984e12

    invoke-direct {v1, v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    .line 1677743
    iput-object v1, v0, LX/4XB;->bQ:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1677744
    move-object v0, v0

    .line 1677745
    iget-object v1, p0, Lcom/facebook/ipc/media/MediaItem;->c:Lcom/facebook/ipc/media/data/LocalMediaData;

    move-object v1, v1

    .line 1677746
    iget-wide v4, v1, Lcom/facebook/ipc/media/data/LocalMediaData;->mMediaStoreId:J

    move-wide v2, v4

    .line 1677747
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    .line 1677748
    iput-object v1, v0, LX/4XB;->T:Ljava/lang/String;

    .line 1677749
    move-object v0, v0

    .line 1677750
    invoke-virtual {v0}, LX/4XB;->a()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    .line 1677751
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/ipc/composer/model/ComposerLocationInfo;)Lcom/facebook/graphql/model/GraphQLPlace;
    .locals 8
    .param p0    # Lcom/facebook/ipc/composer/model/ComposerLocationInfo;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const-wide/16 v2, 0x0

    const/4 v0, 0x0

    .line 1677704
    if-eqz p0, :cond_0

    .line 1677705
    invoke-virtual {p0}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v1

    .line 1677706
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bv_()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 1677707
    invoke-virtual {v1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->j()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$LocationModel;

    move-result-object v4

    .line 1677708
    if-nez v4, :cond_1

    .line 1677709
    :cond_0
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, LX/4Y6;

    invoke-direct {v0}, LX/4Y6;-><init>()V

    invoke-virtual {v1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bv_()Ljava/lang/String;

    move-result-object v5

    .line 1677710
    iput-object v5, v0, LX/4Y6;->n:Ljava/lang/String;

    .line 1677711
    move-object v0, v0

    .line 1677712
    invoke-virtual {v1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->k()Ljava/lang/String;

    move-result-object v1

    .line 1677713
    iput-object v1, v0, LX/4Y6;->r:Ljava/lang/String;

    .line 1677714
    move-object v5, v0

    .line 1677715
    new-instance v6, LX/4X8;

    invoke-direct {v6}, LX/4X8;-><init>()V

    if-eqz v4, :cond_3

    invoke-virtual {v4}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$LocationModel;->a()D

    move-result-wide v0

    .line 1677716
    :goto_1
    iput-wide v0, v6, LX/4X8;->b:D

    .line 1677717
    move-object v0, v6

    .line 1677718
    if-eqz v4, :cond_2

    invoke-virtual {v4}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$LocationModel;->b()D

    move-result-wide v2

    .line 1677719
    :cond_2
    iput-wide v2, v0, LX/4X8;->c:D

    .line 1677720
    move-object v0, v0

    .line 1677721
    invoke-virtual {v0}, LX/4X8;->a()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v0

    .line 1677722
    iput-object v0, v5, LX/4Y6;->p:Lcom/facebook/graphql/model/GraphQLLocation;

    .line 1677723
    move-object v0, v5

    .line 1677724
    new-instance v1, LX/4Xc;

    invoke-direct {v1}, LX/4Xc;-><init>()V

    invoke-virtual {v1}, LX/4Xc;->a()Lcom/facebook/graphql/model/GraphQLPageVisitsConnection;

    move-result-object v1

    .line 1677725
    iput-object v1, v0, LX/4Y6;->u:Lcom/facebook/graphql/model/GraphQLPageVisitsConnection;

    .line 1677726
    move-object v0, v0

    .line 1677727
    invoke-virtual {v0}, LX/4Y6;->a()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v0

    goto :goto_0

    :cond_3
    move-wide v0, v2

    goto :goto_1
.end method

.method public static a(Lcom/facebook/composer/privacy/model/ComposerPrivacyData;)Lcom/facebook/graphql/model/GraphQLPrivacyScope;
    .locals 5

    .prologue
    .line 1677685
    invoke-virtual {p0}, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "[createPrivacyScopeFromComposerPrivacyData] selectedLegacyGraphApiPrivacyJson empty"

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1677686
    new-instance v1, LX/4YL;

    invoke-direct {v1}, LX/4YL;-><init>()V

    new-instance v2, LX/4YI;

    invoke-direct {v2}, LX/4YI;-><init>()V

    new-instance v3, LX/4YJ;

    invoke-direct {v3}, LX/4YJ;-><init>()V

    iget-object v0, p0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1677687
    iget-object v4, v0, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v0, v4

    .line 1677688
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1677689
    iget-object v4, v0, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v0, v4

    .line 1677690
    :goto_1
    iput-object v0, v3, LX/4YJ;->c:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 1677691
    move-object v0, v3

    .line 1677692
    invoke-virtual {v0}, LX/4YJ;->a()Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentEdge;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 1677693
    iput-object v0, v2, LX/4YI;->b:LX/0Px;

    .line 1677694
    move-object v0, v2

    .line 1677695
    invoke-virtual {v0}, LX/4YI;->a()Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;

    move-result-object v0

    .line 1677696
    iput-object v0, v1, LX/4YL;->j:Lcom/facebook/graphql/model/GraphQLPrivacyOptionsContentConnection;

    .line 1677697
    move-object v0, v1

    .line 1677698
    invoke-virtual {v0}, LX/4YL;->a()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    return-object v0

    .line 1677699
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1677700
    :cond_1
    new-instance v0, LX/4YH;

    invoke-direct {v0}, LX/4YH;-><init>()V

    invoke-virtual {p0}, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->a()Ljava/lang/String;

    move-result-object v4

    .line 1677701
    iput-object v4, v0, LX/4YH;->h:Ljava/lang/String;

    .line 1677702
    move-object v0, v0

    .line 1677703
    invoke-virtual {v0}, LX/4YH;->a()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v0

    goto :goto_1
.end method

.method public static a(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Lcom/facebook/graphql/model/GraphQLProfile;
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1677661
    iget-wide v4, p0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-lez v0, :cond_0

    move v0, v1

    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[createTargetProfileFromComposerTargetData] Invalid targetId: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v4, p0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1677662
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetName:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    const-string v3, "[createTargetProfileFromComposerTargetData] Empty targetName"

    invoke-static {v0, v3}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1677663
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetProfilePicUrl:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    const-string v3, "[createTargetProfileFromComposerTargetData] Empty targetProfilePicUrl"

    invoke-static {v0, v3}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1677664
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    sget-object v3, LX/2rw;->USER:LX/2rw;

    if-ne v0, v3, :cond_3

    :goto_3
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 1677665
    new-instance v0, LX/25F;

    invoke-direct {v0}, LX/25F;-><init>()V

    iget-wide v2, p0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    .line 1677666
    iput-object v1, v0, LX/25F;->C:Ljava/lang/String;

    .line 1677667
    move-object v0, v0

    .line 1677668
    iget-object v1, p0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetName:Ljava/lang/String;

    .line 1677669
    iput-object v1, v0, LX/25F;->T:Ljava/lang/String;

    .line 1677670
    move-object v0, v0

    .line 1677671
    new-instance v1, LX/2dc;

    invoke-direct {v1}, LX/2dc;-><init>()V

    iget-object v2, p0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetProfilePicUrl:Ljava/lang/String;

    .line 1677672
    iput-object v2, v1, LX/2dc;->h:Ljava/lang/String;

    .line 1677673
    move-object v1, v1

    .line 1677674
    invoke-virtual {v1}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 1677675
    iput-object v1, v0, LX/25F;->ag:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1677676
    move-object v0, v0

    .line 1677677
    new-instance v1, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v2, 0x285feb

    invoke-direct {v1, v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    .line 1677678
    iput-object v1, v0, LX/25F;->aH:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1677679
    move-object v0, v0

    .line 1677680
    invoke-virtual {v0}, LX/25F;->a()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v2

    .line 1677681
    goto :goto_0

    :cond_1
    move v0, v2

    .line 1677682
    goto :goto_1

    :cond_2
    move v0, v2

    .line 1677683
    goto :goto_2

    :cond_3
    move v1, v2

    .line 1677684
    goto :goto_3
.end method

.method public static a(Lcom/facebook/ipc/composer/model/ComposerStickerData;)Lcom/facebook/graphql/model/GraphQLSticker;
    .locals 2
    .param p0    # Lcom/facebook/ipc/composer/model/ComposerStickerData;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1677651
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/model/ComposerStickerData;->getStickerId()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/model/ComposerStickerData;->getStaticWebUri()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1677652
    :cond_0
    const/4 v0, 0x0

    .line 1677653
    :goto_0
    return-object v0

    .line 1677654
    :cond_1
    new-instance v0, LX/4Yq;

    invoke-direct {v0}, LX/4Yq;-><init>()V

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/model/ComposerStickerData;->getStickerId()Ljava/lang/String;

    move-result-object v1

    .line 1677655
    iput-object v1, v0, LX/4Yq;->i:Ljava/lang/String;

    .line 1677656
    move-object v0, v0

    .line 1677657
    invoke-virtual {p0}, Lcom/facebook/ipc/composer/model/ComposerStickerData;->getStaticWebUri()Ljava/lang/String;

    move-result-object v1

    .line 1677658
    iput-object v1, v0, LX/4Yq;->R:Ljava/lang/String;

    .line 1677659
    move-object v0, v0

    .line 1677660
    invoke-virtual {v0}, LX/4Yq;->a()Lcom/facebook/graphql/model/GraphQLSticker;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Landroid/net/Uri;LX/74n;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;
    .locals 3

    .prologue
    .line 1677640
    sget-object v0, LX/74j;->DEFAULT:LX/74j;

    invoke-virtual {p1, p0, v0}, LX/74n;->a(Landroid/net/Uri;LX/74j;)Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    .line 1677641
    new-instance v1, LX/39x;

    invoke-direct {v1}, LX/39x;-><init>()V

    const/4 v2, 0x1

    .line 1677642
    iput-boolean v2, v1, LX/39x;->i:Z

    .line 1677643
    move-object v1, v1

    .line 1677644
    check-cast v0, Lcom/facebook/photos/base/media/PhotoItem;

    const/4 v2, 0x0

    invoke-static {v0, v2}, LX/ATz;->a(Lcom/facebook/photos/base/media/PhotoItem;Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    .line 1677645
    iput-object v0, v1, LX/39x;->j:Lcom/facebook/graphql/model/GraphQLMedia;

    .line 1677646
    move-object v0, v1

    .line 1677647
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->PHOTO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 1677648
    iput-object v1, v0, LX/39x;->p:LX/0Px;

    .line 1677649
    move-object v0, v0

    .line 1677650
    invoke-virtual {v0}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/facebook/composer/attachments/ComposerAttachment;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;
    .locals 12

    .prologue
    .line 1677568
    invoke-virtual {p0}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v0

    .line 1677569
    iget-object v1, v0, Lcom/facebook/ipc/media/data/MediaData;->mType:LX/4gQ;

    move-object v0, v1

    .line 1677570
    sget-object v1, LX/4gQ;->Video:LX/4gQ;

    if-ne v0, v1, :cond_1

    .line 1677571
    invoke-virtual {p0}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v2

    instance-of v2, v2, Lcom/facebook/photos/base/media/VideoItem;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[createVideoStoryAttachmentFromComposerAttachment] attachment type: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v4

    .line 1677572
    iget-object v5, v4, Lcom/facebook/ipc/media/data/MediaData;->mType:LX/4gQ;

    move-object v4, v5

    .line 1677573
    invoke-virtual {v4}, LX/4gQ;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1677574
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1677575
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->VIDEO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-virtual {v3, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1677576
    invoke-static {p0}, LX/7kq;->c(Lcom/facebook/composer/attachments/ComposerAttachment;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1677577
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->ANIMATED_IMAGE_VIDEO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-virtual {v3, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1677578
    :cond_0
    new-instance v2, LX/39x;

    invoke-direct {v2}, LX/39x;-><init>()V

    const/4 v4, 0x1

    .line 1677579
    iput-boolean v4, v2, LX/39x;->i:Z

    .line 1677580
    move-object v4, v2

    .line 1677581
    invoke-virtual {p0}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v2

    check-cast v2, Lcom/facebook/photos/base/media/VideoItem;

    invoke-virtual {p0}, Lcom/facebook/composer/attachments/ComposerAttachment;->d()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    .line 1677582
    if-eqz v2, :cond_2

    .line 1677583
    new-instance v6, LX/2oI;

    invoke-direct {v6}, LX/2oI;-><init>()V

    invoke-virtual {v2}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v7}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    .line 1677584
    iput-object v7, v6, LX/2oI;->bN:Ljava/lang/String;

    .line 1677585
    move-object v6, v6

    .line 1677586
    invoke-virtual {v6}, LX/2oI;->a()Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v6

    .line 1677587
    new-instance v7, LX/4XB;

    invoke-direct {v7}, LX/4XB;-><init>()V

    .line 1677588
    iget-object v8, v2, Lcom/facebook/ipc/media/MediaItem;->c:Lcom/facebook/ipc/media/data/LocalMediaData;

    move-object v8, v8

    .line 1677589
    iget-wide v10, v8, Lcom/facebook/ipc/media/data/LocalMediaData;->mMediaStoreId:J

    move-wide v8, v10

    .line 1677590
    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    .line 1677591
    iput-object v8, v7, LX/4XB;->T:Ljava/lang/String;

    .line 1677592
    move-object v7, v7

    .line 1677593
    invoke-virtual {v2}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v8

    .line 1677594
    iget-object v9, v8, Lcom/facebook/ipc/media/data/MediaData;->mUri:Landroid/net/Uri;

    move-object v8, v9

    .line 1677595
    invoke-virtual {v8}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v8

    .line 1677596
    iput-object v8, v7, LX/4XB;->bc:Ljava/lang/String;

    .line 1677597
    move-object v7, v7

    .line 1677598
    iget-wide v10, v2, Lcom/facebook/photos/base/media/VideoItem;->d:J

    move-wide v8, v10

    .line 1677599
    long-to-int v8, v8

    .line 1677600
    iput v8, v7, LX/4XB;->bb:I

    .line 1677601
    move-object v7, v7

    .line 1677602
    iput-object v6, v7, LX/4XB;->aU:Lcom/facebook/graphql/model/GraphQLVideo;

    .line 1677603
    move-object v6, v7

    .line 1677604
    iput-object v5, v6, LX/4XB;->aK:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1677605
    move-object v6, v6

    .line 1677606
    new-instance v7, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v8, 0x4ed245b

    invoke-direct {v7, v8}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    .line 1677607
    iput-object v7, v6, LX/4XB;->bQ:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1677608
    move-object v6, v6

    .line 1677609
    invoke-virtual {v2}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v7

    .line 1677610
    iget v8, v7, Lcom/facebook/ipc/media/data/MediaData;->mHeight:I

    move v7, v8

    .line 1677611
    iput v7, v6, LX/4XB;->S:I

    .line 1677612
    move-object v6, v6

    .line 1677613
    invoke-virtual {v2}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v7

    .line 1677614
    iget v8, v7, Lcom/facebook/ipc/media/data/MediaData;->mWidth:I

    move v7, v8

    .line 1677615
    iput v7, v6, LX/4XB;->bO:I

    .line 1677616
    move-object v6, v6

    .line 1677617
    invoke-virtual {v6}, LX/4XB;->a()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v6

    .line 1677618
    :goto_0
    move-object v2, v6

    .line 1677619
    iput-object v2, v4, LX/39x;->j:Lcom/facebook/graphql/model/GraphQLMedia;

    .line 1677620
    move-object v2, v4

    .line 1677621
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    .line 1677622
    iput-object v3, v2, LX/39x;->p:LX/0Px;

    .line 1677623
    move-object v2, v2

    .line 1677624
    invoke-virtual {v2}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v2

    move-object v0, v2

    .line 1677625
    :goto_1
    return-object v0

    .line 1677626
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/photos/base/media/PhotoItem;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[createPhotoStoryAttachmentFromComposerAttachment] attachment type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v2

    .line 1677627
    iget-object v3, v2, Lcom/facebook/ipc/media/data/MediaData;->mType:LX/4gQ;

    move-object v2, v3

    .line 1677628
    invoke-virtual {v2}, LX/4gQ;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1677629
    new-instance v0, LX/39x;

    invoke-direct {v0}, LX/39x;-><init>()V

    const/4 v1, 0x1

    .line 1677630
    iput-boolean v1, v0, LX/39x;->i:Z

    .line 1677631
    move-object v1, v0

    .line 1677632
    invoke-virtual {p0}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/base/media/PhotoItem;

    invoke-virtual {p0}, Lcom/facebook/composer/attachments/ComposerAttachment;->d()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-static {v0, v2}, LX/ATz;->a(Lcom/facebook/photos/base/media/PhotoItem;Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    .line 1677633
    iput-object v0, v1, LX/39x;->j:Lcom/facebook/graphql/model/GraphQLMedia;

    .line 1677634
    move-object v0, v1

    .line 1677635
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->PHOTO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 1677636
    iput-object v1, v0, LX/39x;->p:LX/0Px;

    .line 1677637
    move-object v0, v0

    .line 1677638
    invoke-virtual {v0}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    move-object v0, v0

    .line 1677639
    goto :goto_1

    :cond_2
    const/4 v6, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/ipc/composer/intent/ComposerShareParams;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1677514
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->linkForShare:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "[createLinkStoryAttachmentFromComposerShareParams] linkForShare empty"

    invoke-static {v0, v3}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1677515
    new-instance v0, LX/39x;

    invoke-direct {v0}, LX/39x;-><init>()V

    iget-object v3, p0, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->linkForShare:Ljava/lang/String;

    .line 1677516
    iput-object v3, v0, LX/39x;->w:Ljava/lang/String;

    .line 1677517
    move-object v0, v0

    .line 1677518
    new-instance v3, LX/4XR;

    invoke-direct {v3}, LX/4XR;-><init>()V

    new-instance v4, Lcom/facebook/graphql/enums/GraphQLObjectType$Builder;

    invoke-direct {v4}, Lcom/facebook/graphql/enums/GraphQLObjectType$Builder;-><init>()V

    const v5, 0x1eaef984

    .line 1677519
    iput v5, v4, Lcom/facebook/graphql/enums/GraphQLObjectType$Builder;->a:I

    .line 1677520
    move-object v4, v4

    .line 1677521
    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLObjectType$Builder;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v4

    .line 1677522
    iput-object v4, v3, LX/4XR;->qc:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1677523
    move-object v3, v3

    .line 1677524
    invoke-virtual {v3}, LX/4XR;->a()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v3

    .line 1677525
    iput-object v3, v0, LX/39x;->s:Lcom/facebook/graphql/model/GraphQLNode;

    .line 1677526
    move-object v3, v0

    .line 1677527
    invoke-static {p0}, LX/9J0;->a(Lcom/facebook/ipc/composer/intent/ComposerShareParams;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1677528
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->linkForShare:Ljava/lang/String;

    .line 1677529
    iput-object v0, v3, LX/39x;->t:Ljava/lang/String;

    .line 1677530
    move-object v0, v3

    .line 1677531
    new-instance v1, LX/173;

    invoke-direct {v1}, LX/173;-><init>()V

    iget-object v2, p0, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->linkForShare:Ljava/lang/String;

    .line 1677532
    iput-object v2, v1, LX/173;->f:Ljava/lang/String;

    .line 1677533
    move-object v1, v1

    .line 1677534
    invoke-virtual {v1}, LX/173;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    .line 1677535
    iput-object v1, v0, LX/39x;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1677536
    move-object v0, v0

    .line 1677537
    sget-object v1, LX/16z;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1677538
    iput-object v1, v0, LX/39x;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1677539
    move-object v0, v0

    .line 1677540
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->ANIMATED_IMAGE_AUTOPLAY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->ANIMATED_IMAGE_SHARE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->IMAGE_SHARE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->SHARE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->FALLBACK:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-static {v1, v2, v4, v5, v6}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 1677541
    iput-object v1, v0, LX/39x;->p:LX/0Px;

    .line 1677542
    :goto_1
    invoke-virtual {v3}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v2

    .line 1677543
    goto :goto_0

    .line 1677544
    :cond_1
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->attachmentPreview:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    if-eqz v0, :cond_2

    :goto_2
    const-string v0, "[createLinkStoryAttachmentFromComposerShareParams] attachmentPreview empty"

    invoke-static {v1, v0}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1677545
    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->attachmentPreview:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->A()Ljava/lang/String;

    move-result-object v0

    .line 1677546
    iput-object v0, v3, LX/39x;->t:Ljava/lang/String;

    .line 1677547
    move-object v0, v3

    .line 1677548
    new-instance v1, LX/173;

    invoke-direct {v1}, LX/173;-><init>()V

    iget-object v2, p0, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->attachmentPreview:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->y()Ljava/lang/String;

    move-result-object v2

    .line 1677549
    iput-object v2, v1, LX/173;->f:Ljava/lang/String;

    .line 1677550
    move-object v1, v1

    .line 1677551
    invoke-virtual {v1}, LX/173;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    .line 1677552
    iput-object v1, v0, LX/39x;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1677553
    move-object v1, v0

    .line 1677554
    new-instance v2, LX/173;

    invoke-direct {v2}, LX/173;-><init>()V

    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->attachmentPreview:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->attachmentPreview:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    .line 1677555
    :goto_3
    iput-object v0, v2, LX/173;->f:Ljava/lang/String;

    .line 1677556
    move-object v0, v2

    .line 1677557
    invoke-virtual {v0}, LX/173;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    .line 1677558
    iput-object v0, v1, LX/39x;->n:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1677559
    move-object v0, v1

    .line 1677560
    iget-object v1, p0, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->attachmentPreview:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    .line 1677561
    iput-object v1, v0, LX/39x;->j:Lcom/facebook/graphql/model/GraphQLMedia;

    .line 1677562
    move-object v0, v0

    .line 1677563
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->SHARE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->FALLBACK:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-static {v1, v2}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 1677564
    iput-object v1, v0, LX/39x;->p:LX/0Px;

    .line 1677565
    goto :goto_1

    :cond_2
    move v1, v2

    .line 1677566
    goto :goto_2

    .line 1677567
    :cond_3
    const/4 v0, 0x0

    goto :goto_3
.end method

.method public static a(Ljava/util/List;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;)",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 1677474
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v3, :cond_1

    .line 1677475
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1677476
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-static {v0}, LX/ATz;->a(Lcom/facebook/composer/attachments/ComposerAttachment;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 1677477
    :goto_0
    return-object v0

    .line 1677478
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1677479
    :cond_1
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    .line 1677480
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 1677481
    invoke-static {v0}, LX/ATz;->a(Lcom/facebook/composer/attachments/ComposerAttachment;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 1677482
    :cond_2
    new-instance v0, LX/39x;

    invoke-direct {v0}, LX/39x;-><init>()V

    .line 1677483
    iput-boolean v3, v0, LX/39x;->i:Z

    .line 1677484
    move-object v0, v0

    .line 1677485
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 1677486
    iput-object v1, v0, LX/39x;->q:LX/0Px;

    .line 1677487
    move-object v0, v0

    .line 1677488
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->ALBUM:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 1677489
    iput-object v1, v0, LX/39x;->p:LX/0Px;

    .line 1677490
    move-object v0, v0

    .line 1677491
    invoke-virtual {v0}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(Ljava/util/List;)Lcom/facebook/graphql/model/GraphQLWithTagsConnection;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/ipc/composer/model/ComposerTaggedUser;",
            ">;)",
            "Lcom/facebook/graphql/model/GraphQLWithTagsConnection;"
        }
    .end annotation

    .prologue
    .line 1677492
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    .line 1677493
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;

    .line 1677494
    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;->a()J

    move-result-wide v4

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;->c()Ljava/lang/String;

    move-result-object v0

    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 1677495
    const-wide/16 v10, 0x0

    cmp-long v6, v4, v10

    if-eqz v6, :cond_2

    move v6, v7

    :goto_1
    const-string v9, "[createActor] Invalid actorId"

    invoke-static {v6, v9}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1677496
    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_3

    :goto_2
    const-string v6, "[createActor] Empty name"

    invoke-static {v7, v6}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1677497
    new-instance v6, LX/3dL;

    invoke-direct {v6}, LX/3dL;-><init>()V

    .line 1677498
    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    .line 1677499
    iput-object v7, v6, LX/3dL;->E:Ljava/lang/String;

    .line 1677500
    iput-object v3, v6, LX/3dL;->ag:Ljava/lang/String;

    .line 1677501
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 1677502
    invoke-static {v0, v8, v8}, LX/16z;->a(Ljava/lang/String;II)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    .line 1677503
    iput-object v7, v6, LX/3dL;->as:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1677504
    :cond_0
    new-instance v7, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v8, 0x285feb

    invoke-direct {v7, v8}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    .line 1677505
    iput-object v7, v6, LX/3dL;->aW:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1677506
    invoke-virtual {v6}, LX/3dL;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v6

    move-object v0, v6

    .line 1677507
    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 1677508
    :cond_1
    new-instance v0, LX/4ZV;

    invoke-direct {v0}, LX/4ZV;-><init>()V

    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 1677509
    iput-object v1, v0, LX/4ZV;->b:LX/0Px;

    .line 1677510
    move-object v0, v0

    .line 1677511
    invoke-virtual {v0}, LX/4ZV;->a()Lcom/facebook/graphql/model/GraphQLWithTagsConnection;

    move-result-object v0

    return-object v0

    :cond_2
    move v6, v8

    .line 1677512
    goto :goto_1

    :cond_3
    move v7, v8

    .line 1677513
    goto :goto_2
.end method
