.class public LX/CE9;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/CE8;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/fig/components/newsfeed/FigStoryProfilePictureComponentSpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1860665
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/CE9;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/fig/components/newsfeed/FigStoryProfilePictureComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1860708
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1860709
    iput-object p1, p0, LX/CE9;->b:LX/0Ot;

    .line 1860710
    return-void
.end method

.method public static a(LX/0QB;)LX/CE9;
    .locals 4

    .prologue
    .line 1860697
    const-class v1, LX/CE9;

    monitor-enter v1

    .line 1860698
    :try_start_0
    sget-object v0, LX/CE9;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1860699
    sput-object v2, LX/CE9;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1860700
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1860701
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1860702
    new-instance v3, LX/CE9;

    const/16 p0, 0x220f

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/CE9;-><init>(LX/0Ot;)V

    .line 1860703
    move-object v0, v3

    .line 1860704
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1860705
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/CE9;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1860706
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1860707
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 7

    .prologue
    .line 1860676
    check-cast p2, Lcom/facebook/fig/components/newsfeed/FigStoryProfilePictureComponent$FigStoryProfilePictureComponentImpl;

    .line 1860677
    iget-object v0, p0, LX/CE9;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/components/newsfeed/FigStoryProfilePictureComponentSpec;

    iget-object v2, p2, Lcom/facebook/fig/components/newsfeed/FigStoryProfilePictureComponent$FigStoryProfilePictureComponentImpl;->a:Ljava/lang/String;

    iget v3, p2, Lcom/facebook/fig/components/newsfeed/FigStoryProfilePictureComponent$FigStoryProfilePictureComponentImpl;->b:I

    iget-object v4, p2, Lcom/facebook/fig/components/newsfeed/FigStoryProfilePictureComponent$FigStoryProfilePictureComponentImpl;->c:Ljava/lang/String;

    iget-object v5, p2, Lcom/facebook/fig/components/newsfeed/FigStoryProfilePictureComponent$FigStoryProfilePictureComponentImpl;->d:Ljava/lang/String;

    iget-object v6, p2, Lcom/facebook/fig/components/newsfeed/FigStoryProfilePictureComponent$FigStoryProfilePictureComponentImpl;->e:Lcom/facebook/common/callercontext/CallerContext;

    move-object v1, p1

    .line 1860678
    invoke-static {v4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result p0

    if-eqz p0, :cond_0

    iget-object p0, v0, Lcom/facebook/fig/components/newsfeed/FigStoryProfilePictureComponentSpec;->c:LX/CDt;

    invoke-virtual {p0, v1}, LX/CDt;->c(LX/1De;)LX/CDs;

    move-result-object p0

    invoke-virtual {p0, v6}, LX/CDs;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/CDs;

    move-result-object p0

    invoke-virtual {p0, v2}, LX/CDs;->b(Ljava/lang/String;)LX/CDs;

    move-result-object p0

    .line 1860679
    :goto_0
    invoke-virtual {p0}, LX/1X5;->c()LX/1Di;

    move-result-object p0

    invoke-interface {p0, v3}, LX/1Di;->i(I)LX/1Di;

    move-result-object p0

    invoke-interface {p0, v3}, LX/1Di;->q(I)LX/1Di;

    move-result-object p0

    invoke-interface {p0}, LX/1Di;->k()LX/1Dg;

    move-result-object p0

    move-object v0, p0

    .line 1860680
    return-object v0

    .line 1860681
    :cond_0
    const/4 p0, 0x0

    .line 1860682
    new-instance v0, LX/CDw;

    invoke-direct {v0}, LX/CDw;-><init>()V

    .line 1860683
    sget-object v2, LX/CDx;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/CDv;

    .line 1860684
    if-nez v2, :cond_1

    .line 1860685
    new-instance v2, LX/CDv;

    invoke-direct {v2}, LX/CDv;-><init>()V

    .line 1860686
    :cond_1
    invoke-static {v2, v1, p0, p0, v0}, LX/CDv;->a$redex0(LX/CDv;LX/1De;IILX/CDw;)V

    .line 1860687
    move-object v0, v2

    .line 1860688
    move-object p0, v0

    .line 1860689
    move-object p0, p0

    .line 1860690
    iget-object v0, p0, LX/CDv;->a:LX/CDw;

    iput-object v4, v0, LX/CDw;->a:Ljava/lang/String;

    .line 1860691
    iget-object v0, p0, LX/CDv;->d:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1860692
    move-object p0, p0

    .line 1860693
    iget-object v0, p0, LX/CDv;->a:LX/CDw;

    iput-object v5, v0, LX/CDw;->b:Ljava/lang/String;

    .line 1860694
    iget-object v0, p0, LX/CDv;->d:Ljava/util/BitSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1860695
    move-object p0, p0

    .line 1860696
    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1860674
    invoke-static {}, LX/1dS;->b()V

    .line 1860675
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c(LX/1De;)LX/CE8;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1860666
    new-instance v1, Lcom/facebook/fig/components/newsfeed/FigStoryProfilePictureComponent$FigStoryProfilePictureComponentImpl;

    invoke-direct {v1, p0}, Lcom/facebook/fig/components/newsfeed/FigStoryProfilePictureComponent$FigStoryProfilePictureComponentImpl;-><init>(LX/CE9;)V

    .line 1860667
    sget-object v2, LX/CE9;->a:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/CE8;

    .line 1860668
    if-nez v2, :cond_0

    .line 1860669
    new-instance v2, LX/CE8;

    invoke-direct {v2}, LX/CE8;-><init>()V

    .line 1860670
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/CE8;->a$redex0(LX/CE8;LX/1De;IILcom/facebook/fig/components/newsfeed/FigStoryProfilePictureComponent$FigStoryProfilePictureComponentImpl;)V

    .line 1860671
    move-object v1, v2

    .line 1860672
    move-object v0, v1

    .line 1860673
    return-object v0
.end method
