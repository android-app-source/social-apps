.class public LX/Cb0;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/Caz;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1918883
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 1918884
    return-void
.end method


# virtual methods
.method public final a(Landroid/widget/FrameLayout;Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;ILjava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)LX/Caz;
    .locals 21

    .prologue
    .line 1918885
    new-instance v1, LX/Caz;

    invoke-static/range {p0 .. p0}, LX/7iV;->a(LX/0QB;)LX/7iV;

    move-result-object v8

    check-cast v8, LX/7iV;

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v9

    check-cast v9, LX/0ad;

    invoke-static/range {p0 .. p0}, LX/Cb1;->a(LX/0QB;)LX/Cb1;

    move-result-object v10

    check-cast v10, LX/Cb1;

    invoke-static/range {p0 .. p0}, LX/CbH;->a(LX/0QB;)LX/CbH;

    move-result-object v11

    check-cast v11, LX/CbH;

    invoke-static/range {p0 .. p0}, LX/9hh;->a(LX/0QB;)LX/9hh;

    move-result-object v12

    check-cast v12, LX/9hh;

    const-class v2, LX/CbJ;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v13

    check-cast v13, LX/CbJ;

    const-class v2, LX/CbC;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v14

    check-cast v14, LX/CbC;

    const/16 v2, 0x2e06

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v15

    const-class v2, LX/CbM;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v16

    check-cast v16, LX/CbM;

    invoke-static/range {p0 .. p0}, LX/1Ck;->a(LX/0QB;)LX/1Ck;

    move-result-object v17

    check-cast v17, LX/1Ck;

    invoke-static/range {p0 .. p0}, LX/1L1;->a(LX/0QB;)LX/1L1;

    move-result-object v18

    check-cast v18, LX/1L1;

    invoke-static/range {p0 .. p0}, LX/8Jo;->a(LX/0QB;)LX/8Jo;

    move-result-object v19

    check-cast v19, LX/8Jo;

    invoke-static/range {p0 .. p0}, LX/7j7;->a(LX/0QB;)LX/7j7;

    move-result-object v20

    check-cast v20, LX/7j7;

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    invoke-direct/range {v1 .. v20}, LX/Caz;-><init>(Landroid/widget/FrameLayout;Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;ILjava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;LX/7iV;LX/0ad;LX/Cb1;LX/CbH;LX/9hh;LX/CbJ;LX/CbC;LX/0Ot;LX/CbM;LX/1Ck;LX/1L1;LX/8Jo;LX/7j7;)V

    .line 1918886
    return-object v1
.end method
