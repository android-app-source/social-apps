.class public LX/AnS;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public a:LX/0W3;


# direct methods
.method public constructor <init>(LX/0W3;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1712470
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1712471
    iput-object p1, p0, LX/AnS;->a:LX/0W3;

    .line 1712472
    return-void
.end method

.method public static a(LX/0QB;)LX/AnS;
    .locals 4

    .prologue
    .line 1712473
    const-class v1, LX/AnS;

    monitor-enter v1

    .line 1712474
    :try_start_0
    sget-object v0, LX/AnS;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1712475
    sput-object v2, LX/AnS;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1712476
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1712477
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1712478
    new-instance p0, LX/AnS;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v3

    check-cast v3, LX/0W3;

    invoke-direct {p0, v3}, LX/AnS;-><init>(LX/0W3;)V

    .line 1712479
    move-object v0, p0

    .line 1712480
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1712481
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/AnS;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1712482
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1712483
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 1712484
    iget-object v0, p0, LX/AnS;->a:LX/0W3;

    sget-wide v2, LX/0X5;->kz:J

    invoke-interface {v0, v2, v3}, LX/0W4;->f(J)Ljava/lang/String;

    move-result-object v0

    .line 1712485
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return-object p1

    :cond_0
    move-object p1, v0

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 1712486
    iget-object v0, p0, LX/AnS;->a:LX/0W3;

    sget-wide v2, LX/0X5;->ky:J

    invoke-interface {v0, v2, v3}, LX/0W4;->f(J)Ljava/lang/String;

    move-result-object v0

    .line 1712487
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return-object p1

    :cond_0
    move-object p1, v0

    goto :goto_0
.end method

.method public final c(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 1712488
    iget-object v0, p0, LX/AnS;->a:LX/0W3;

    sget-wide v2, LX/0X5;->kx:J

    invoke-interface {v0, v2, v3}, LX/0W4;->f(J)Ljava/lang/String;

    move-result-object v0

    .line 1712489
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return-object p1

    :cond_0
    move-object p1, v0

    goto :goto_0
.end method
