.class public LX/CDt;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/CDs;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CDu;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1860247
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/CDt;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/CDu;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1860244
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1860245
    iput-object p1, p0, LX/CDt;->b:LX/0Ot;

    .line 1860246
    return-void
.end method

.method public static a(LX/0QB;)LX/CDt;
    .locals 4

    .prologue
    .line 1860233
    const-class v1, LX/CDt;

    monitor-enter v1

    .line 1860234
    :try_start_0
    sget-object v0, LX/CDt;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1860235
    sput-object v2, LX/CDt;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1860236
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1860237
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1860238
    new-instance v3, LX/CDt;

    const/16 p0, 0x220b

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/CDt;-><init>(LX/0Ot;)V

    .line 1860239
    move-object v0, v3

    .line 1860240
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1860241
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/CDt;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1860242
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1860243
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 9

    .prologue
    .line 1860218
    check-cast p2, Lcom/facebook/fig/components/newsfeed/FigProfilePhotoComponent$FigProfilePhotoComponentImpl;

    .line 1860219
    iget-object v0, p0, LX/CDt;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CDu;

    iget-object v2, p2, Lcom/facebook/fig/components/newsfeed/FigProfilePhotoComponent$FigProfilePhotoComponentImpl;->a:Ljava/lang/String;

    iget-object v3, p2, Lcom/facebook/fig/components/newsfeed/FigProfilePhotoComponent$FigProfilePhotoComponentImpl;->b:Lcom/facebook/common/callercontext/CallerContext;

    iget-object v4, p2, Lcom/facebook/fig/components/newsfeed/FigProfilePhotoComponent$FigProfilePhotoComponentImpl;->c:Landroid/graphics/PointF;

    iget-object v5, p2, Lcom/facebook/fig/components/newsfeed/FigProfilePhotoComponent$FigProfilePhotoComponentImpl;->d:LX/1Up;

    iget-object v6, p2, Lcom/facebook/fig/components/newsfeed/FigProfilePhotoComponent$FigProfilePhotoComponentImpl;->e:LX/1Up;

    iget-object v7, p2, Lcom/facebook/fig/components/newsfeed/FigProfilePhotoComponent$FigProfilePhotoComponentImpl;->f:LX/4Ab;

    iget v8, p2, Lcom/facebook/fig/components/newsfeed/FigProfilePhotoComponent$FigProfilePhotoComponentImpl;->g:F

    move-object v1, p1

    .line 1860220
    iget-object p0, v0, LX/CDu;->a:LX/0Or;

    invoke-interface {p0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/1Ad;

    invoke-virtual {p0, v2}, LX/1Ad;->b(Ljava/lang/String;)LX/1Ad;

    move-result-object p0

    invoke-virtual {p0, v3}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object p0

    invoke-virtual {p0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object p0

    .line 1860221
    invoke-static {v1}, LX/1um;->c(LX/1De;)LX/1up;

    move-result-object p1

    invoke-virtual {p1, p0}, LX/1up;->a(LX/1aZ;)LX/1up;

    move-result-object p0

    const p1, 0x7f02111f

    invoke-virtual {p0, p1}, LX/1up;->h(I)LX/1up;

    move-result-object p0

    invoke-virtual {p0, v8}, LX/1up;->c(F)LX/1up;

    move-result-object p0

    invoke-virtual {p0, v7}, LX/1up;->a(LX/4Ab;)LX/1up;

    move-result-object p0

    invoke-virtual {p0, v6}, LX/1up;->b(LX/1Up;)LX/1up;

    move-result-object p0

    invoke-virtual {p0, v4}, LX/1up;->b(Landroid/graphics/PointF;)LX/1up;

    move-result-object p0

    invoke-virtual {p0, v5}, LX/1up;->a(LX/1Up;)LX/1up;

    move-result-object p0

    invoke-virtual {p0}, LX/1X5;->b()LX/1Dg;

    move-result-object p0

    move-object v0, p0

    .line 1860222
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1860231
    invoke-static {}, LX/1dS;->b()V

    .line 1860232
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c(LX/1De;)LX/CDs;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1860223
    new-instance v1, Lcom/facebook/fig/components/newsfeed/FigProfilePhotoComponent$FigProfilePhotoComponentImpl;

    invoke-direct {v1, p0}, Lcom/facebook/fig/components/newsfeed/FigProfilePhotoComponent$FigProfilePhotoComponentImpl;-><init>(LX/CDt;)V

    .line 1860224
    sget-object v2, LX/CDt;->a:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/CDs;

    .line 1860225
    if-nez v2, :cond_0

    .line 1860226
    new-instance v2, LX/CDs;

    invoke-direct {v2}, LX/CDs;-><init>()V

    .line 1860227
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/CDs;->a$redex0(LX/CDs;LX/1De;IILcom/facebook/fig/components/newsfeed/FigProfilePhotoComponent$FigProfilePhotoComponentImpl;)V

    .line 1860228
    move-object v1, v2

    .line 1860229
    move-object v0, v1

    .line 1860230
    return-object v0
.end method
