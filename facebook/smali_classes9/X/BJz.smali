.class public final LX/BJz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V
    .locals 0

    .prologue
    .line 1772520
    iput-object p1, p0, LX/BJz;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x2f749304

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1772521
    iget-object v1, p0, LX/BJz;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    .line 1772522
    invoke-static {}, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->newBuilder()LX/9jF;

    move-result-object v2

    .line 1772523
    iget-object v3, v1, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v3, v3, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v3}, Lcom/facebook/platform/composer/model/PlatformComposition;->e()LX/0Px;

    move-result-object v3

    invoke-static {v3}, LX/7kq;->l(LX/0Px;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1772524
    sget-object v3, LX/9jG;->PHOTO:LX/9jG;

    .line 1772525
    :goto_0
    move-object v3, v3

    .line 1772526
    iput-object v3, v2, LX/9jF;->q:LX/9jG;

    .line 1772527
    move-object v2, v2

    .line 1772528
    iget-object v3, v1, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v3, v3, LX/BKm;->a:Ljava/lang/String;

    .line 1772529
    iput-object v3, v2, LX/9jF;->g:Ljava/lang/String;

    .line 1772530
    move-object v2, v2

    .line 1772531
    iget-object v3, v1, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v3, v3, LX/BKm;->f:Lcom/facebook/ipc/composer/model/ComposerLocation;

    .line 1772532
    iput-object v3, v2, LX/9jF;->f:Lcom/facebook/ipc/composer/model/ComposerLocation;

    .line 1772533
    move-object v2, v2

    .line 1772534
    iget-object v3, v1, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v3, v3, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v3}, Lcom/facebook/platform/composer/model/PlatformComposition;->h()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v3

    .line 1772535
    iput-object v3, v2, LX/9jF;->c:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 1772536
    move-object v2, v2

    .line 1772537
    iget-object v3, v1, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v3, v3, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v3}, Lcom/facebook/platform/composer/model/PlatformComposition;->h()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->c()Ljava/lang/String;

    move-result-object v3

    .line 1772538
    iput-object v3, v2, LX/9jF;->B:Ljava/lang/String;

    .line 1772539
    move-object v3, v2

    .line 1772540
    iget-object v2, v1, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->g:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {v3}, LX/9jF;->a()Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    move-result-object v3

    invoke-static {p1, v3}, LX/9jD;->a(Landroid/content/Context;Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;)Landroid/content/Intent;

    move-result-object v3

    const/16 p1, 0x28

    invoke-interface {v2, v3, p1, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 1772541
    iget-object v1, p0, LX/BJz;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v1, v1, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->w:LX/0if;

    sget-object v2, LX/0ig;->k:LX/0ih;

    const-string v3, "place_tagging_button"

    invoke-virtual {v1, v2, v3}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1772542
    const v1, -0x4cfffb5b

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1772543
    :cond_0
    iget-object v3, v1, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v3, v3, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v3}, Lcom/facebook/platform/composer/model/PlatformComposition;->e()LX/0Px;

    move-result-object v3

    invoke-static {v3}, LX/7kq;->j(LX/0Px;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1772544
    sget-object v3, LX/9jG;->VIDEO:LX/9jG;

    goto :goto_0

    .line 1772545
    :cond_1
    iget-object v3, v1, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v3, v3, LX/BKm;->b:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    invoke-virtual {v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->k()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1772546
    sget-object v3, LX/9jG;->CHECKIN:LX/9jG;

    goto :goto_0

    .line 1772547
    :cond_2
    sget-object v3, LX/9jG;->STATUS:LX/9jG;

    goto :goto_0
.end method
