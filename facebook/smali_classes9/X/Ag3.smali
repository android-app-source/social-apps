.class public final LX/Ag3;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveVideoViewersQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Ag4;


# direct methods
.method public constructor <init>(LX/Ag4;)V
    .locals 0

    .prologue
    .line 1700103
    iput-object p1, p0, LX/Ag3;->a:LX/Ag4;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 1700151
    iget-object v2, p0, LX/Ag3;->a:LX/Ag4;

    monitor-enter v2

    .line 1700152
    :try_start_0
    iget-object v1, p0, LX/Ag3;->a:LX/Ag4;

    iget-object v1, v1, LX/Ag4;->k:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/Ag3;->a:LX/Ag4;

    iget-object v1, v1, LX/Ag4;->k:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v1}, Lcom/google/common/util/concurrent/ListenableFuture;->isCancelled()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1700153
    :cond_0
    monitor-exit v2

    .line 1700154
    :goto_0
    return-void

    .line 1700155
    :cond_1
    iget-object v1, p0, LX/Ag3;->a:LX/Ag4;

    iget-object v3, v1, LX/Ag4;->i:LX/03V;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, LX/Ag4;->f:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "_graphFailure"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v5, "Failed to get watch events for "

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, LX/Ag3;->a:LX/Ag4;

    iget-object v5, v5, LX/AeQ;->c:Ljava/lang/String;

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, LX/Ag3;->a:LX/Ag4;

    iget-object v1, v1, LX/AeQ;->c:Ljava/lang/String;

    :goto_1
    invoke-virtual {v3, v4, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1700156
    iget-object v1, p0, LX/Ag3;->a:LX/Ag4;

    iget-boolean v1, v1, LX/AeQ;->e:Z

    if-nez v1, :cond_4

    .line 1700157
    :goto_2
    iget-object v1, p0, LX/Ag3;->a:LX/Ag4;

    const/4 v3, 0x1

    .line 1700158
    iput-boolean v3, v1, LX/Ag4;->e:Z

    .line 1700159
    iget-object v1, p0, LX/Ag3;->a:LX/Ag4;

    iget-object v1, v1, LX/AeQ;->b:LX/AeV;

    if-eqz v1, :cond_2

    .line 1700160
    iget-object v1, p0, LX/Ag3;->a:LX/Ag4;

    iget-object v1, v1, LX/AeQ;->b:LX/AeV;

    iget-object v3, p0, LX/Ag3;->a:LX/Ag4;

    invoke-virtual {v3}, LX/Ag4;->g()LX/AeN;

    move-result-object v3

    invoke-virtual {v1, v3, v0}, LX/AeV;->a(LX/AeN;Z)V

    .line 1700161
    :cond_2
    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1700162
    :cond_3
    :try_start_1
    const-string v1, "no story id"
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 1700163
    :cond_4
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 8
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1700104
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1700105
    iget-object v3, p0, LX/Ag3;->a:LX/Ag4;

    monitor-enter v3

    .line 1700106
    :try_start_0
    iget-object v0, p0, LX/Ag3;->a:LX/Ag4;

    iget-object v0, v0, LX/Ag4;->k:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Ag3;->a:LX/Ag4;

    iget-object v0, v0, LX/Ag4;->k:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1700107
    :cond_0
    monitor-exit v3

    .line 1700108
    :goto_0
    return-void

    .line 1700109
    :cond_1
    if-nez p1, :cond_2

    .line 1700110
    monitor-exit v3

    goto :goto_0

    .line 1700111
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1700112
    :cond_2
    :try_start_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1700113
    check-cast v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveVideoViewersQueryModel;

    .line 1700114
    if-nez v0, :cond_3

    .line 1700115
    monitor-exit v3

    goto :goto_0

    .line 1700116
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveVideoViewersQueryModel;->k()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveVideoViewersCollectionFragmentModel;

    move-result-object v0

    .line 1700117
    if-nez v0, :cond_4

    .line 1700118
    monitor-exit v3

    goto :goto_0

    .line 1700119
    :cond_4
    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveVideoViewersCollectionFragmentModel;->j()LX/0Px;

    move-result-object v0

    .line 1700120
    if-nez v0, :cond_5

    .line 1700121
    monitor-exit v3

    goto :goto_0

    .line 1700122
    :cond_5
    new-instance v4, Ljava/util/LinkedList;

    invoke-direct {v4}, Ljava/util/LinkedList;-><init>()V

    .line 1700123
    iget-object v5, p0, LX/Ag3;->a:LX/Ag4;

    iget-boolean v5, v5, LX/AeQ;->e:Z

    if-nez v5, :cond_a

    .line 1700124
    :goto_1
    iget-object v2, p0, LX/Ag3;->a:LX/Ag4;

    const/4 v5, 0x1

    .line 1700125
    iput-boolean v5, v2, LX/Ag4;->e:Z

    .line 1700126
    if-eqz v1, :cond_b

    .line 1700127
    iget-object v2, p0, LX/Ag3;->a:LX/Ag4;

    iget-object v2, v2, LX/AeQ;->c:Ljava/lang/String;

    .line 1700128
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v6

    .line 1700129
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_6
    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_7

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveVideoViewersCollectionFragmentModel$EdgesModel;

    .line 1700130
    invoke-virtual {v5}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveVideoViewersCollectionFragmentModel$EdgesModel;->a()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveEventAuthorModel;

    move-result-object v5

    invoke-static {v5}, LX/AcC;->a(Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveEventAuthorModel;)LX/AcC;

    move-result-object v5

    .line 1700131
    if-eqz v5, :cond_6

    .line 1700132
    invoke-virtual {v6, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_2

    .line 1700133
    :cond_7
    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v6

    .line 1700134
    invoke-virtual {v6}, LX/0Px;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_d

    .line 1700135
    const/4 v5, 0x0

    .line 1700136
    :goto_3
    move-object v0, v5

    .line 1700137
    if-eqz v0, :cond_8

    .line 1700138
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1700139
    :cond_8
    iget-object v0, p0, LX/Ag3;->a:LX/Ag4;

    iget-object v0, v0, LX/AeQ;->b:LX/AeV;

    if-eqz v0, :cond_9

    .line 1700140
    iget-object v0, p0, LX/Ag3;->a:LX/Ag4;

    iget-object v0, v0, LX/AeQ;->b:LX/AeV;

    iget-object v2, p0, LX/Ag3;->a:LX/Ag4;

    invoke-virtual {v2}, LX/Ag4;->g()LX/AeN;

    move-result-object v2

    invoke-virtual {v0, v2, v4, v1}, LX/AeV;->a(LX/AeN;Ljava/util/List;Z)V

    .line 1700141
    :cond_9
    monitor-exit v3

    goto :goto_0

    :cond_a
    move v1, v2

    .line 1700142
    goto :goto_1

    .line 1700143
    :cond_b
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_c
    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveVideoViewersCollectionFragmentModel$EdgesModel;

    .line 1700144
    iget-object v5, p0, LX/Ag3;->a:LX/Ag4;

    iget-object v5, v5, LX/AeQ;->c:Ljava/lang/String;

    const/4 p1, 0x0

    .line 1700145
    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveVideoViewersCollectionFragmentModel$EdgesModel;->a()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveEventAuthorModel;

    move-result-object v6

    invoke-static {v6}, LX/AcC;->a(Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveEventAuthorModel;)LX/AcC;

    move-result-object v7

    .line 1700146
    if-nez v7, :cond_e

    .line 1700147
    const/4 v6, 0x0

    .line 1700148
    :goto_5
    move-object v0, v6

    .line 1700149
    if-eqz v0, :cond_c

    .line 1700150
    const/4 v5, 0x0

    invoke-interface {v4, v5, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_4

    :cond_d
    :try_start_2
    new-instance v5, LX/Ag2;

    const/4 v7, 0x1

    const/4 p1, 0x0

    invoke-direct {v5, v6, v7, p1, v2}, LX/Ag2;-><init>(LX/0Px;ZZLjava/lang/String;)V

    goto :goto_3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_e
    new-instance v6, LX/Ag2;

    invoke-static {v7}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v7

    invoke-direct {v6, v7, p1, p1, v5}, LX/Ag2;-><init>(LX/0Px;ZZLjava/lang/String;)V

    goto :goto_5
.end method
