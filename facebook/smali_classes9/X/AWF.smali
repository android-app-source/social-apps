.class public final LX/AWF;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15i;ILX/0nX;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1682433
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1682434
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1682435
    if-eqz v0, :cond_0

    .line 1682436
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1682437
    invoke-static {p0, p1, v2, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1682438
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1682439
    if-eqz v0, :cond_1

    .line 1682440
    const-string v1, "angry_weight"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1682441
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1682442
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1682443
    if-eqz v0, :cond_2

    .line 1682444
    const-string v1, "haha_weight"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1682445
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1682446
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1682447
    if-eqz v0, :cond_3

    .line 1682448
    const-string v1, "like_weight"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1682449
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1682450
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1682451
    if-eqz v0, :cond_4

    .line 1682452
    const-string v1, "love_weight"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1682453
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1682454
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1682455
    if-eqz v0, :cond_5

    .line 1682456
    const-string v1, "reaction_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1682457
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1682458
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1682459
    if-eqz v0, :cond_6

    .line 1682460
    const-string v1, "sad_weight"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1682461
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1682462
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1682463
    if-eqz v0, :cond_7

    .line 1682464
    const-string v1, "time_duration_ms"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1682465
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1682466
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1682467
    if-eqz v0, :cond_8

    .line 1682468
    const-string v1, "time_offset_ms"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1682469
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1682470
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1682471
    if-eqz v0, :cond_9

    .line 1682472
    const-string v1, "wow_weight"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1682473
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1682474
    :cond_9
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1682475
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 25

    .prologue
    .line 1682476
    const/16 v21, 0x0

    .line 1682477
    const/16 v20, 0x0

    .line 1682478
    const/16 v19, 0x0

    .line 1682479
    const/16 v18, 0x0

    .line 1682480
    const/16 v17, 0x0

    .line 1682481
    const/16 v16, 0x0

    .line 1682482
    const/4 v15, 0x0

    .line 1682483
    const/4 v14, 0x0

    .line 1682484
    const/4 v13, 0x0

    .line 1682485
    const/4 v12, 0x0

    .line 1682486
    const/4 v11, 0x0

    .line 1682487
    const/4 v10, 0x0

    .line 1682488
    const/4 v9, 0x0

    .line 1682489
    const/4 v8, 0x0

    .line 1682490
    const/4 v7, 0x0

    .line 1682491
    const/4 v6, 0x0

    .line 1682492
    const/4 v5, 0x0

    .line 1682493
    const/4 v4, 0x0

    .line 1682494
    const/4 v3, 0x0

    .line 1682495
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v22

    sget-object v23, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    if-eq v0, v1, :cond_1

    .line 1682496
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1682497
    const/4 v3, 0x0

    .line 1682498
    :goto_0
    return v3

    .line 1682499
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1682500
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v22

    sget-object v23, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    if-eq v0, v1, :cond_c

    .line 1682501
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v22

    .line 1682502
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1682503
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v23

    sget-object v24, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v23

    move-object/from16 v1, v24

    if-eq v0, v1, :cond_1

    if-eqz v22, :cond_1

    .line 1682504
    const-string v23, "__type__"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-nez v23, :cond_2

    const-string v23, "__typename"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_3

    .line 1682505
    :cond_2
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v21

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v21

    goto :goto_1

    .line 1682506
    :cond_3
    const-string v23, "angry_weight"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_4

    .line 1682507
    const/4 v11, 0x1

    .line 1682508
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v20

    goto :goto_1

    .line 1682509
    :cond_4
    const-string v23, "haha_weight"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_5

    .line 1682510
    const/4 v10, 0x1

    .line 1682511
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v19

    goto :goto_1

    .line 1682512
    :cond_5
    const-string v23, "like_weight"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_6

    .line 1682513
    const/4 v9, 0x1

    .line 1682514
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v18

    goto :goto_1

    .line 1682515
    :cond_6
    const-string v23, "love_weight"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_7

    .line 1682516
    const/4 v8, 0x1

    .line 1682517
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v17

    goto :goto_1

    .line 1682518
    :cond_7
    const-string v23, "reaction_count"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_8

    .line 1682519
    const/4 v7, 0x1

    .line 1682520
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v16

    goto/16 :goto_1

    .line 1682521
    :cond_8
    const-string v23, "sad_weight"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_9

    .line 1682522
    const/4 v6, 0x1

    .line 1682523
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v15

    goto/16 :goto_1

    .line 1682524
    :cond_9
    const-string v23, "time_duration_ms"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_a

    .line 1682525
    const/4 v5, 0x1

    .line 1682526
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v14

    goto/16 :goto_1

    .line 1682527
    :cond_a
    const-string v23, "time_offset_ms"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_b

    .line 1682528
    const/4 v4, 0x1

    .line 1682529
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v13

    goto/16 :goto_1

    .line 1682530
    :cond_b
    const-string v23, "wow_weight"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_0

    .line 1682531
    const/4 v3, 0x1

    .line 1682532
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v12

    goto/16 :goto_1

    .line 1682533
    :cond_c
    const/16 v22, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 1682534
    const/16 v22, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v22

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1682535
    if-eqz v11, :cond_d

    .line 1682536
    const/4 v11, 0x1

    const/16 v21, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v11, v1, v2}, LX/186;->a(III)V

    .line 1682537
    :cond_d
    if-eqz v10, :cond_e

    .line 1682538
    const/4 v10, 0x2

    const/4 v11, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v10, v1, v11}, LX/186;->a(III)V

    .line 1682539
    :cond_e
    if-eqz v9, :cond_f

    .line 1682540
    const/4 v9, 0x3

    const/4 v10, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v9, v1, v10}, LX/186;->a(III)V

    .line 1682541
    :cond_f
    if-eqz v8, :cond_10

    .line 1682542
    const/4 v8, 0x4

    const/4 v9, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v8, v1, v9}, LX/186;->a(III)V

    .line 1682543
    :cond_10
    if-eqz v7, :cond_11

    .line 1682544
    const/4 v7, 0x5

    const/4 v8, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v7, v1, v8}, LX/186;->a(III)V

    .line 1682545
    :cond_11
    if-eqz v6, :cond_12

    .line 1682546
    const/4 v6, 0x6

    const/4 v7, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v15, v7}, LX/186;->a(III)V

    .line 1682547
    :cond_12
    if-eqz v5, :cond_13

    .line 1682548
    const/4 v5, 0x7

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v14, v6}, LX/186;->a(III)V

    .line 1682549
    :cond_13
    if-eqz v4, :cond_14

    .line 1682550
    const/16 v4, 0x8

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v13, v5}, LX/186;->a(III)V

    .line 1682551
    :cond_14
    if-eqz v3, :cond_15

    .line 1682552
    const/16 v3, 0x9

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v12, v4}, LX/186;->a(III)V

    .line 1682553
    :cond_15
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method
