.class public final LX/BhV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Landroid/view/View;

.field public final synthetic b:Landroid/view/ViewGroup;

.field public final synthetic c:Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;Landroid/view/View;Landroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 1809295
    iput-object p1, p0, LX/BhV;->c:Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;

    iput-object p2, p0, LX/BhV;->a:Landroid/view/View;

    iput-object p3, p0, LX/BhV;->b:Landroid/view/ViewGroup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x2

    const v1, 0x2097bda9

    invoke-static {v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v3

    .line 1809296
    iget-object v0, p0, LX/BhV;->c:Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;

    .line 1809297
    iput-boolean v2, v0, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->h:Z

    .line 1809298
    iget-object v0, p0, LX/BhV;->c:Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;

    iget-object v0, v0, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tK;

    invoke-virtual {v0}, LX/0tK;->d()V

    .line 1809299
    iget-object v0, p0, LX/BhV;->c:Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;

    iget-object v0, v0, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tK;

    invoke-virtual {v0}, LX/0tK;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1809300
    new-instance v0, LX/6WI;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/6WI;-><init>(Landroid/content/Context;)V

    const v1, 0x7f080a1d

    invoke-virtual {v0, v1}, LX/6WI;->a(I)LX/6WI;

    move-result-object v0

    const v1, 0x7f080a1e

    invoke-virtual {v0, v1}, LX/6WI;->b(I)LX/6WI;

    move-result-object v0

    const v1, 0x7f080a1f

    new-instance v4, LX/BhU;

    invoke-direct {v4, p0}, LX/BhU;-><init>(LX/BhV;)V

    invoke-virtual {v0, v1, v4}, LX/6WI;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/6WI;

    move-result-object v0

    const v1, 0x7f080a20

    new-instance v4, LX/BhT;

    invoke-direct {v4, p0}, LX/BhT;-><init>(LX/BhV;)V

    invoke-virtual {v0, v1, v4}, LX/6WI;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/6WI;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/6WI;->a(Z)LX/6WI;

    move-result-object v0

    invoke-virtual {v0}, LX/6WI;->b()LX/6WJ;

    .line 1809301
    :goto_0
    const v0, -0x4d1ed515

    invoke-static {v0, v3}, LX/02F;->a(II)V

    return-void

    .line 1809302
    :cond_0
    iget-object v0, p0, LX/BhV;->c:Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;

    iget-object v0, v0, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tK;

    iget-object v1, p0, LX/BhV;->c:Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;

    iget-object v1, v1, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0tK;

    invoke-virtual {v1}, LX/0tK;->l()Z

    move-result v1

    if-nez v1, :cond_1

    move v1, v2

    :goto_1
    invoke-virtual {v0, v1}, LX/0tK;->g(Z)V

    .line 1809303
    iget-object v0, p0, LX/BhV;->c:Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;

    iget-object v1, p0, LX/BhV;->a:Landroid/view/View;

    iget-object v2, p0, LX/BhV;->b:Landroid/view/ViewGroup;

    .line 1809304
    invoke-static {v0, v1, v2}, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->a$redex0(Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;Landroid/view/View;Landroid/view/ViewGroup;)V

    .line 1809305
    goto :goto_0

    .line 1809306
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method
