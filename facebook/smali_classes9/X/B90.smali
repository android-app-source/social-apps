.class public LX/B90;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements LX/B6N;


# instance fields
.field private a:Lcom/facebook/widget/FbScrollView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1750860
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 1750861
    const v0, 0x7f0309bf

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1750862
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 1750901
    return-void
.end method

.method public final a(LX/B7B;ILX/B7F;ILX/B7w;)V
    .locals 8

    .prologue
    .line 1750868
    check-cast p1, LX/B7C;

    .line 1750869
    iget-object v0, p1, LX/B7C;->d:LX/B7K;

    move-object v2, v0

    .line 1750870
    const v0, 0x7f0d18d1    # 1.8755E38f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1750871
    iget-object v1, p1, LX/B7C;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1750872
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1750873
    const v0, 0x7f0d18c7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/FbScrollView;

    iput-object v0, p0, LX/B90;->a:Lcom/facebook/widget/FbScrollView;

    .line 1750874
    const v0, 0x7f0d04f9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/leadgen/view/LeadGenPageProfileHeaderView;

    .line 1750875
    const v1, 0x7f0d18c5

    invoke-virtual {p0, v1}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/leadgen/view/LeadGenHeaderBackgroundView;

    .line 1750876
    invoke-virtual {p3}, LX/B7F;->u()Z

    move-result v3

    invoke-virtual {v0, v2, v3}, Lcom/facebook/leadgen/view/LeadGenPageProfileHeaderView;->a(LX/B7K;Z)V

    .line 1750877
    invoke-virtual {p3}, LX/B7F;->u()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1750878
    invoke-virtual {v1, v2}, Lcom/facebook/leadgen/view/LeadGenHeaderBackgroundView;->setUpView(LX/B7K;)V

    .line 1750879
    :cond_0
    const/4 v3, 0x0

    .line 1750880
    const v0, 0x7f0d18d2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 1750881
    iget-object v1, p1, LX/B7C;->c:LX/0Px;

    move-object v4, v1

    .line 1750882
    if-eqz v4, :cond_1

    invoke-virtual {v4}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1750883
    :cond_1
    return-void

    .line 1750884
    :cond_2
    iget-object v1, p1, LX/B7C;->b:Lcom/facebook/graphql/enums/GraphQLLeadGenContextPageContentStyle;

    move-object v5, v1

    .line 1750885
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v6

    move v2, v3

    :goto_0
    if-ge v2, v6, :cond_1

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1750886
    new-instance v7, Landroid/widget/TextView;

    invoke-virtual {p0}, LX/B90;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-direct {v7, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 1750887
    invoke-virtual {p0}, LX/B90;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    const p3, 0x7f0b0a04

    invoke-virtual {p2, p3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result p2

    invoke-virtual {v7, v3, p2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1750888
    invoke-virtual {p0}, LX/B90;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    const p3, 0x7f0a00a7

    invoke-virtual {p2, p3}, Landroid/content/res/Resources;->getColor(I)I

    move-result p2

    invoke-virtual {v7, p2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1750889
    sget-object p2, Lcom/facebook/graphql/enums/GraphQLLeadGenContextPageContentStyle;->LIST_STYLE:Lcom/facebook/graphql/enums/GraphQLLeadGenContextPageContentStyle;

    if-ne v5, p2, :cond_3

    .line 1750890
    const/4 p1, 0x0

    .line 1750891
    new-instance p2, Landroid/text/SpannableString;

    invoke-direct {p2, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 1750892
    new-instance p3, LX/B8o;

    invoke-virtual {p0}, LX/B90;->getResources()Landroid/content/res/Resources;

    move-result-object p4

    const p5, 0x7f0b19a3

    invoke-virtual {p4, p5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result p4

    float-to-int p4, p4

    invoke-direct {p3, p4}, LX/B8o;-><init>(I)V

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result p4

    invoke-virtual {p2, p3, p1, p4, p1}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 1750893
    invoke-virtual {v7, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1750894
    invoke-virtual {p0}, LX/B90;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    const p3, 0x7f0b19a2

    invoke-virtual {p2, p3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result p2

    float-to-int p2, p2

    .line 1750895
    new-instance p3, Landroid/widget/LinearLayout$LayoutParams;

    const/4 p4, -0x1

    const/4 p5, -0x2

    invoke-direct {p3, p4, p5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1750896
    invoke-virtual {p3, p1, p1, p1, p2}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 1750897
    invoke-virtual {v7, p3}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1750898
    :goto_1
    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1750899
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 1750900
    :cond_3
    invoke-virtual {v7, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public final a(Lcom/facebook/leadgen/cache/LeadGenFormPendingInputEntry;)V
    .locals 0

    .prologue
    .line 1750867
    return-void
.end method

.method public final b()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/B8s;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1750866
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c()LX/0P1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1750865
    const/4 v0, 0x0

    return-object v0
.end method

.method public getContentScrollView()Lcom/facebook/widget/FbScrollView;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1750864
    iget-object v0, p0, LX/B90;->a:Lcom/facebook/widget/FbScrollView;

    return-object v0
.end method

.method public final l_(I)LX/B77;
    .locals 1

    .prologue
    .line 1750863
    const/4 v0, 0x0

    return-object v0
.end method
