.class public final LX/AnR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/098;


# instance fields
.field private a:LX/395;

.field private b:Lcom/facebook/video/engine/VideoPlayerParams;


# direct methods
.method public constructor <init>(LX/395;Lcom/facebook/video/engine/VideoPlayerParams;)V
    .locals 0

    .prologue
    .line 1712466
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1712467
    iput-object p1, p0, LX/AnR;->a:LX/395;

    .line 1712468
    iput-object p2, p0, LX/AnR;->b:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 1712469
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 1712465
    iget-object v0, p0, LX/AnR;->b:Lcom/facebook/video/engine/VideoPlayerParams;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/AnR;->b:Lcom/facebook/video/engine/VideoPlayerParams;

    invoke-virtual {v0}, Lcom/facebook/video/engine/VideoPlayerParams;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1712464
    iget-object v0, p0, LX/AnR;->a:LX/395;

    invoke-virtual {v0}, LX/395;->b()Z

    move-result v0

    return v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1712463
    iget-object v0, p0, LX/AnR;->a:LX/395;

    invoke-virtual {v0}, LX/395;->c()Z

    move-result v0

    return v0
.end method

.method public final d()LX/19o;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1712462
    iget-object v0, p0, LX/AnR;->b:Lcom/facebook/video/engine/VideoPlayerParams;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/AnR;->b:Lcom/facebook/video/engine/VideoPlayerParams;

    invoke-virtual {v0}, Lcom/facebook/video/engine/VideoPlayerParams;->d()LX/19o;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 1712458
    iget-object v0, p0, LX/AnR;->b:Lcom/facebook/video/engine/VideoPlayerParams;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/AnR;->b:Lcom/facebook/video/engine/VideoPlayerParams;

    invoke-virtual {v0}, Lcom/facebook/video/engine/VideoPlayerParams;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()LX/03z;
    .locals 1

    .prologue
    .line 1712461
    iget-object v0, p0, LX/AnR;->b:Lcom/facebook/video/engine/VideoPlayerParams;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/AnR;->b:Lcom/facebook/video/engine/VideoPlayerParams;

    invoke-virtual {v0}, Lcom/facebook/video/engine/VideoPlayerParams;->f()LX/03z;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;
    .locals 1

    .prologue
    .line 1712460
    iget-object v0, p0, LX/AnR;->a:LX/395;

    invoke-virtual {v0}, LX/395;->g()Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-result-object v0

    return-object v0
.end method

.method public final h()I
    .locals 1

    .prologue
    .line 1712459
    iget-object v0, p0, LX/AnR;->a:LX/395;

    invoke-virtual {v0}, LX/395;->h()I

    move-result v0

    return v0
.end method
