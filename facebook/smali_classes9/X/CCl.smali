.class public LX/CCl;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/MountSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:LX/D1j;


# direct methods
.method public constructor <init>(LX/D1j;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1858285
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1858286
    iput-object p1, p0, LX/CCl;->a:LX/D1j;

    .line 1858287
    return-void
.end method

.method public static a(LX/0QB;)LX/CCl;
    .locals 4

    .prologue
    .line 1858288
    const-class v1, LX/CCl;

    monitor-enter v1

    .line 1858289
    :try_start_0
    sget-object v0, LX/CCl;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1858290
    sput-object v2, LX/CCl;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1858291
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1858292
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1858293
    new-instance p0, LX/CCl;

    invoke-static {v0}, LX/D1j;->a(LX/0QB;)LX/D1j;

    move-result-object v3

    check-cast v3, LX/D1j;

    invoke-direct {p0, v3}, LX/CCl;-><init>(LX/D1j;)V

    .line 1858294
    move-object v0, p0

    .line 1858295
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1858296
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/CCl;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1858297
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1858298
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
