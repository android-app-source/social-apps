.class public LX/BzI;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public final a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1838496
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/BzI;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1838497
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1838498
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1838499
    const v0, 0x7f0309e0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1838500
    const v0, 0x7f0d0aa1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, LX/BzI;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1838501
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/BzI;->setOrientation(I)V

    .line 1838502
    return-void
.end method
