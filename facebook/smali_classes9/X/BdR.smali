.class public final LX/BdR;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/BdR;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/BdP;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/BdS;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1803761
    const/4 v0, 0x0

    sput-object v0, LX/BdR;->a:LX/BdR;

    .line 1803762
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/BdR;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1803763
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1803764
    new-instance v0, LX/BdS;

    invoke-direct {v0}, LX/BdS;-><init>()V

    iput-object v0, p0, LX/BdR;->c:LX/BdS;

    .line 1803765
    return-void
.end method

.method public static c(LX/1De;)LX/BdP;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1803766
    new-instance v1, LX/BdQ;

    invoke-direct {v1}, LX/BdQ;-><init>()V

    .line 1803767
    sget-object v2, LX/BdR;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/BdP;

    .line 1803768
    if-nez v2, :cond_0

    .line 1803769
    new-instance v2, LX/BdP;

    invoke-direct {v2}, LX/BdP;-><init>()V

    .line 1803770
    :cond_0
    invoke-static {v2, p0, v0, v0, v1}, LX/BdP;->a$redex0(LX/BdP;LX/1De;IILX/BdQ;)V

    .line 1803771
    move-object v1, v2

    .line 1803772
    move-object v0, v1

    .line 1803773
    return-object v0
.end method

.method public static declared-synchronized q()LX/BdR;
    .locals 2

    .prologue
    .line 1803774
    const-class v1, LX/BdR;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/BdR;->a:LX/BdR;

    if-nez v0, :cond_0

    .line 1803775
    new-instance v0, LX/BdR;

    invoke-direct {v0}, LX/BdR;-><init>()V

    sput-object v0, LX/BdR;->a:LX/BdR;

    .line 1803776
    :cond_0
    sget-object v0, LX/BdR;->a:LX/BdR;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 1803777
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 1

    .prologue
    .line 1803778
    const/16 p2, 0x14

    .line 1803779
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    const/4 p0, 0x1

    invoke-interface {v0, p0}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v0

    invoke-static {p1}, LX/5Jt;->c(LX/1De;)LX/5Jr;

    move-result-object p0

    invoke-virtual {p0}, LX/1X5;->c()LX/1Di;

    move-result-object p0

    invoke-interface {p0, p2}, LX/1Di;->j(I)LX/1Di;

    move-result-object p0

    invoke-interface {p0, p2}, LX/1Di;->r(I)LX/1Di;

    move-result-object p0

    const/4 p2, 0x2

    invoke-interface {p0, p2}, LX/1Di;->b(I)LX/1Di;

    move-result-object p0

    invoke-interface {v0, p0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    move-object v0, v0

    .line 1803780
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1803781
    invoke-static {}, LX/1dS;->b()V

    .line 1803782
    const/4 v0, 0x0

    return-object v0
.end method
