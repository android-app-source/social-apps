.class public LX/BJN;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/BJN;


# instance fields
.field public final a:LX/0aG;

.field public final b:Landroid/content/Context;

.field public final c:Ljava/util/concurrent/Executor;

.field public final d:LX/0tX;


# direct methods
.method public constructor <init>(LX/0aG;Landroid/content/Context;Ljava/util/concurrent/Executor;LX/0tX;)V
    .locals 0
    .param p3    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1771832
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1771833
    iput-object p1, p0, LX/BJN;->a:LX/0aG;

    .line 1771834
    iput-object p2, p0, LX/BJN;->b:Landroid/content/Context;

    .line 1771835
    iput-object p3, p0, LX/BJN;->c:Ljava/util/concurrent/Executor;

    .line 1771836
    iput-object p4, p0, LX/BJN;->d:LX/0tX;

    .line 1771837
    return-void
.end method

.method public static a(LX/0QB;)LX/BJN;
    .locals 7

    .prologue
    .line 1771838
    sget-object v0, LX/BJN;->e:LX/BJN;

    if-nez v0, :cond_1

    .line 1771839
    const-class v1, LX/BJN;

    monitor-enter v1

    .line 1771840
    :try_start_0
    sget-object v0, LX/BJN;->e:LX/BJN;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1771841
    if-eqz v2, :cond_0

    .line 1771842
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1771843
    new-instance p0, LX/BJN;

    invoke-static {v0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v3

    check-cast v3, LX/0aG;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/0Zo;->a(LX/0QB;)LX/0Tf;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/Executor;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v6

    check-cast v6, LX/0tX;

    invoke-direct {p0, v3, v4, v5, v6}, LX/BJN;-><init>(LX/0aG;Landroid/content/Context;Ljava/util/concurrent/Executor;LX/0tX;)V

    .line 1771844
    move-object v0, p0

    .line 1771845
    sput-object v0, LX/BJN;->e:LX/BJN;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1771846
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1771847
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1771848
    :cond_1
    sget-object v0, LX/BJN;->e:LX/BJN;

    return-object v0

    .line 1771849
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1771850
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/share/model/ComposerAppAttribution;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/facebook/share/model/ComposerAppAttribution;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Landroid/text/SpannableStringBuilder;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1771851
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    .line 1771852
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Failed to generate preview for user due to invalid input"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1771853
    :cond_1
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1771854
    new-instance v1, Lcom/facebook/platform/opengraph/server/GetRobotextPreviewMethod$Params;

    invoke-virtual {p3}, Lcom/facebook/share/model/ComposerAppAttribution;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p3}, Lcom/facebook/share/model/ComposerAppAttribution;->c()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, p1, p2, v2, v3}, Lcom/facebook/platform/opengraph/server/GetRobotextPreviewMethod$Params;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1771855
    const-string v2, "og_action"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1771856
    iget-object v1, p0, LX/BJN;->a:LX/0aG;

    const-string v2, "platform_get_robotext_preview"

    const v3, 0x980621f

    invoke-static {v1, v2, v0, v3}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    .line 1771857
    new-instance v1, LX/BJK;

    invoke-direct {v1, p0}, LX/BJK;-><init>(LX/BJN;)V

    iget-object v2, p0, LX/BJN;->c:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
