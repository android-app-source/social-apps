.class public LX/Bl4;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<",
            "LX/Bl4;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:LX/2Cb;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1816159
    const-class v0, LX/Bl4;

    sput-object v0, LX/Bl4;->a:Ljava/lang/Class;

    .line 1816160
    const-string v0, "\t"

    invoke-static {v0}, LX/2Cb;->on(Ljava/lang/String;)LX/2Cb;

    move-result-object v0

    sput-object v0, LX/Bl4;->b:LX/2Cb;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1816158
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/database/Cursor;ID)D
    .locals 2

    .prologue
    .line 1816157
    if-ltz p1, :cond_0

    invoke-interface {p0, p1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-wide p2

    :cond_1
    invoke-interface {p0, p1}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide p2

    goto :goto_0
.end method

.method public static a(Landroid/database/Cursor;IJ)J
    .locals 2

    .prologue
    .line 1816156
    if-ltz p1, :cond_0

    invoke-interface {p0, p1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-wide p2

    :cond_1
    invoke-interface {p0, p1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide p2

    goto :goto_0
.end method

.method public static a(Lcom/facebook/events/model/Event;)Landroid/content/ContentValues;
    .locals 9

    .prologue
    .line 1815784
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 1815785
    sget-object v0, LX/Bkw;->b:LX/0U1;

    .line 1815786
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 1815787
    iget-object v2, p0, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v2, v2

    .line 1815788
    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1815789
    sget-object v0, LX/Bkw;->c:LX/0U1;

    .line 1815790
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 1815791
    iget-object v2, p0, Lcom/facebook/events/model/Event;->b:Ljava/lang/String;

    move-object v2, v2

    .line 1815792
    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1815793
    iget-object v0, p0, Lcom/facebook/events/model/Event;->c:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;

    move-object v0, v0

    .line 1815794
    if-nez v0, :cond_5

    .line 1815795
    sget-object v0, LX/Bkw;->d:LX/0U1;

    .line 1815796
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 1815797
    invoke-virtual {v1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1815798
    :cond_0
    sget-object v0, LX/Bkw;->e:LX/0U1;

    .line 1815799
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 1815800
    invoke-virtual {v1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1815801
    :goto_0
    iget-object v0, p0, Lcom/facebook/events/model/Event;->d:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    move-object v0, v0

    .line 1815802
    if-eqz v0, :cond_8

    .line 1815803
    sget-object v0, LX/Bkw;->f:LX/0U1;

    .line 1815804
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 1815805
    iget-object v2, p0, Lcom/facebook/events/model/Event;->d:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    move-object v2, v2

    .line 1815806
    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1815807
    :goto_1
    iget-object v0, p0, Lcom/facebook/events/model/Event;->e:Lcom/facebook/events/model/EventType;

    move-object v0, v0

    .line 1815808
    if-eqz v0, :cond_9

    .line 1815809
    sget-object v0, LX/Bkw;->g:LX/0U1;

    .line 1815810
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 1815811
    iget-object v2, p0, Lcom/facebook/events/model/Event;->e:Lcom/facebook/events/model/EventType;

    move-object v2, v2

    .line 1815812
    invoke-virtual {v2}, Lcom/facebook/events/model/EventType;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1815813
    :goto_2
    iget-object v0, p0, Lcom/facebook/events/model/Event;->g:Lcom/facebook/events/model/PrivacyKind;

    move-object v0, v0

    .line 1815814
    if-eqz v0, :cond_a

    .line 1815815
    sget-object v0, LX/Bkw;->h:LX/0U1;

    .line 1815816
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 1815817
    iget-object v2, p0, Lcom/facebook/events/model/Event;->g:Lcom/facebook/events/model/PrivacyKind;

    move-object v2, v2

    .line 1815818
    invoke-virtual {v2}, Lcom/facebook/events/model/PrivacyKind;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1815819
    :goto_3
    iget-object v0, p0, Lcom/facebook/events/model/Event;->f:Lcom/facebook/events/model/PrivacyType;

    move-object v0, v0

    .line 1815820
    if-eqz v0, :cond_b

    .line 1815821
    sget-object v0, LX/Bkw;->i:LX/0U1;

    .line 1815822
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 1815823
    iget-object v2, p0, Lcom/facebook/events/model/Event;->f:Lcom/facebook/events/model/PrivacyType;

    move-object v2, v2

    .line 1815824
    invoke-virtual {v2}, Lcom/facebook/events/model/PrivacyType;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1815825
    :goto_4
    sget-object v2, LX/Bkw;->j:LX/0U1;

    .line 1815826
    iget-object v0, p0, Lcom/facebook/events/model/Event;->m:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-object v0, v0

    .line 1815827
    if-nez v0, :cond_c

    const/4 v0, 0x0

    :goto_5
    invoke-static {v1, v2, v0}, LX/Bl4;->a(Landroid/content/ContentValues;LX/0U1;Ljava/lang/String;)V

    .line 1815828
    iget-object v0, p0, Lcom/facebook/events/model/Event;->n:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    move-object v0, v0

    .line 1815829
    if-eqz v0, :cond_d

    .line 1815830
    sget-object v0, LX/Bkw;->k:LX/0U1;

    .line 1815831
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 1815832
    iget-object v2, p0, Lcom/facebook/events/model/Event;->n:Lcom/facebook/graphql/enums/GraphQLEventActionStyle;

    move-object v2, v2

    .line 1815833
    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLEventActionStyle;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1815834
    :goto_6
    sget-object v0, LX/Bkw;->l:LX/0U1;

    .line 1815835
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 1815836
    iget-boolean v2, p0, Lcom/facebook/events/model/Event;->h:Z

    move v2, v2

    .line 1815837
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1815838
    sget-object v0, LX/Bkw;->m:LX/0U1;

    .line 1815839
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 1815840
    iget-boolean v2, p0, Lcom/facebook/events/model/Event;->i:Z

    move v2, v2

    .line 1815841
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1815842
    sget-object v0, LX/Bkw;->n:LX/0U1;

    .line 1815843
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 1815844
    iget-boolean v2, p0, Lcom/facebook/events/model/Event;->j:Z

    move v2, v2

    .line 1815845
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1815846
    sget-object v0, LX/Bkw;->o:LX/0U1;

    .line 1815847
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 1815848
    iget-object v2, p0, Lcom/facebook/events/model/Event;->k:LX/03R;

    move-object v2, v2

    .line 1815849
    invoke-virtual {v2}, LX/03R;->getDbValue()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1815850
    sget-object v0, LX/Bkw;->p:LX/0U1;

    .line 1815851
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 1815852
    iget-boolean v2, p0, Lcom/facebook/events/model/Event;->l:Z

    move v2, v2

    .line 1815853
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1815854
    sget-object v0, LX/Bkw;->q:LX/0U1;

    .line 1815855
    iget-object v2, p0, Lcom/facebook/events/model/Event;->r:Ljava/lang/String;

    move-object v2, v2

    .line 1815856
    invoke-static {v1, v0, v2}, LX/Bl4;->a(Landroid/content/ContentValues;LX/0U1;Ljava/lang/String;)V

    .line 1815857
    sget-object v0, LX/Bkw;->r:LX/0U1;

    .line 1815858
    iget-object v2, p0, Lcom/facebook/events/model/Event;->s:Ljava/lang/String;

    move-object v2, v2

    .line 1815859
    invoke-static {v1, v0, v2}, LX/Bl4;->a(Landroid/content/ContentValues;LX/0U1;Ljava/lang/String;)V

    .line 1815860
    sget-object v0, LX/Bkw;->s:LX/0U1;

    .line 1815861
    iget-object v2, p0, Lcom/facebook/events/model/Event;->t:Ljava/lang/String;

    move-object v2, v2

    .line 1815862
    invoke-static {v1, v0, v2}, LX/Bl4;->a(Landroid/content/ContentValues;LX/0U1;Ljava/lang/String;)V

    .line 1815863
    sget-object v0, LX/Bkw;->t:LX/0U1;

    .line 1815864
    iget-object v2, p0, Lcom/facebook/events/model/Event;->u:Ljava/lang/String;

    move-object v2, v2

    .line 1815865
    invoke-static {v1, v0, v2}, LX/Bl4;->a(Landroid/content/ContentValues;LX/0U1;Ljava/lang/String;)V

    .line 1815866
    sget-object v0, LX/Bkw;->u:LX/0U1;

    .line 1815867
    iget-object v2, p0, Lcom/facebook/events/model/Event;->v:Ljava/lang/String;

    move-object v2, v2

    .line 1815868
    invoke-static {v1, v0, v2}, LX/Bl4;->a(Landroid/content/ContentValues;LX/0U1;Ljava/lang/String;)V

    .line 1815869
    sget-object v0, LX/Bkw;->v:LX/0U1;

    .line 1815870
    iget-object v2, p0, Lcom/facebook/events/model/Event;->w:Ljava/lang/String;

    move-object v2, v2

    .line 1815871
    invoke-static {v1, v0, v2}, LX/Bl4;->a(Landroid/content/ContentValues;LX/0U1;Ljava/lang/String;)V

    .line 1815872
    sget-object v0, LX/Bkw;->w:LX/0U1;

    .line 1815873
    iget-object v2, p0, Lcom/facebook/events/model/Event;->x:Ljava/lang/String;

    move-object v2, v2

    .line 1815874
    invoke-static {v1, v0, v2}, LX/Bl4;->a(Landroid/content/ContentValues;LX/0U1;Ljava/lang/String;)V

    .line 1815875
    sget-object v0, LX/Bkw;->x:LX/0U1;

    .line 1815876
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 1815877
    iget-boolean v2, p0, Lcom/facebook/events/model/Event;->y:Z

    move v2, v2

    .line 1815878
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1815879
    sget-object v0, LX/Bkw;->y:LX/0U1;

    .line 1815880
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 1815881
    iget-boolean v2, p0, Lcom/facebook/events/model/Event;->z:Z

    move v2, v2

    .line 1815882
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1815883
    sget-object v0, LX/Bkw;->z:LX/0U1;

    .line 1815884
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 1815885
    iget-wide v6, p0, Lcom/facebook/events/model/Event;->A:J

    move-wide v2, v6

    .line 1815886
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1815887
    sget-object v0, LX/Bkw;->A:LX/0U1;

    .line 1815888
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 1815889
    iget-boolean v2, p0, Lcom/facebook/events/model/Event;->B:Z

    move v2, v2

    .line 1815890
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1815891
    invoke-virtual {p0}, Lcom/facebook/events/model/Event;->F()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v0

    if-eqz v0, :cond_e

    .line 1815892
    sget-object v0, LX/Bkw;->B:LX/0U1;

    .line 1815893
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 1815894
    invoke-virtual {p0}, Lcom/facebook/events/model/Event;->F()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1815895
    :goto_7
    iget-object v0, p0, Lcom/facebook/events/model/Event;->D:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-object v0, v0

    .line 1815896
    if-eqz v0, :cond_f

    .line 1815897
    sget-object v0, LX/Bkw;->C:LX/0U1;

    .line 1815898
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 1815899
    iget-object v2, p0, Lcom/facebook/events/model/Event;->D:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-object v2, v2

    .line 1815900
    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1815901
    :goto_8
    sget-object v0, LX/Bkw;->D:LX/0U1;

    .line 1815902
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 1815903
    iget-boolean v2, p0, Lcom/facebook/events/model/Event;->E:Z

    move v2, v2

    .line 1815904
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1815905
    sget-object v0, LX/Bkw;->E:LX/0U1;

    .line 1815906
    iget-object v2, p0, Lcom/facebook/events/model/Event;->F:Ljava/lang/String;

    move-object v2, v2

    .line 1815907
    invoke-static {v1, v0, v2}, LX/Bl4;->a(Landroid/content/ContentValues;LX/0U1;Ljava/lang/String;)V

    .line 1815908
    sget-object v0, LX/Bkw;->F:LX/0U1;

    .line 1815909
    iget-object v2, p0, Lcom/facebook/events/model/Event;->G:Ljava/lang/String;

    move-object v2, v2

    .line 1815910
    invoke-static {v1, v0, v2}, LX/Bl4;->a(Landroid/content/ContentValues;LX/0U1;Ljava/lang/String;)V

    .line 1815911
    sget-object v0, LX/Bkw;->G:LX/0U1;

    .line 1815912
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 1815913
    iget-boolean v2, p0, Lcom/facebook/events/model/Event;->H:Z

    move v2, v2

    .line 1815914
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1815915
    sget-object v0, LX/Bkw;->H:LX/0U1;

    .line 1815916
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 1815917
    sget-object v2, LX/7vK;->ADMIN:LX/7vK;

    invoke-virtual {p0, v2}, Lcom/facebook/events/model/Event;->a(LX/7vK;)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1815918
    invoke-virtual {p0}, Lcom/facebook/events/model/Event;->L()Ljava/util/Date;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1815919
    sget-object v0, LX/Bkw;->I:LX/0U1;

    .line 1815920
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 1815921
    invoke-virtual {p0}, Lcom/facebook/events/model/Event;->M()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1815922
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/events/model/Event;->N()Ljava/util/Date;

    move-result-object v0

    if-eqz v0, :cond_10

    .line 1815923
    sget-object v0, LX/Bkw;->J:LX/0U1;

    .line 1815924
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 1815925
    invoke-virtual {p0}, Lcom/facebook/events/model/Event;->O()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1815926
    :goto_9
    iget-object v0, p0, Lcom/facebook/events/model/Event;->M:Ljava/util/TimeZone;

    move-object v0, v0

    .line 1815927
    if-eqz v0, :cond_2

    .line 1815928
    sget-object v0, LX/Bkw;->K:LX/0U1;

    .line 1815929
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 1815930
    iget-object v2, p0, Lcom/facebook/events/model/Event;->M:Ljava/util/TimeZone;

    move-object v2, v2

    .line 1815931
    invoke-virtual {v2}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1815932
    :cond_2
    sget-object v0, LX/Bkw;->L:LX/0U1;

    .line 1815933
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 1815934
    iget-boolean v2, p0, Lcom/facebook/events/model/Event;->N:Z

    move v2, v2

    .line 1815935
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1815936
    iget-object v0, p0, Lcom/facebook/events/model/Event;->R:Ljava/lang/String;

    move-object v0, v0

    .line 1815937
    if-eqz v0, :cond_11

    .line 1815938
    sget-object v0, LX/Bkw;->M:LX/0U1;

    .line 1815939
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 1815940
    iget-object v2, p0, Lcom/facebook/events/model/Event;->R:Ljava/lang/String;

    move-object v2, v2

    .line 1815941
    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1815942
    :goto_a
    invoke-virtual {p0}, Lcom/facebook/events/model/Event;->R()Z

    move-result v0

    if-eqz v0, :cond_12

    .line 1815943
    sget-object v0, LX/Bkw;->N:LX/0U1;

    .line 1815944
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 1815945
    invoke-virtual {p0}, Lcom/facebook/events/model/Event;->S()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 1815946
    sget-object v0, LX/Bkw;->O:LX/0U1;

    .line 1815947
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 1815948
    invoke-virtual {p0}, Lcom/facebook/events/model/Event;->T()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 1815949
    :goto_b
    iget-object v0, p0, Lcom/facebook/events/model/Event;->S:Ljava/util/TimeZone;

    move-object v0, v0

    .line 1815950
    if-eqz v0, :cond_13

    .line 1815951
    sget-object v0, LX/Bkw;->R:LX/0U1;

    .line 1815952
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 1815953
    iget-object v2, p0, Lcom/facebook/events/model/Event;->S:Ljava/util/TimeZone;

    move-object v2, v2

    .line 1815954
    invoke-virtual {v2}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1815955
    :goto_c
    sget-object v0, LX/Bkw;->P:LX/0U1;

    .line 1815956
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 1815957
    iget-wide v6, p0, Lcom/facebook/events/model/Event;->P:J

    move-wide v2, v6

    .line 1815958
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1815959
    iget v0, p0, Lcom/facebook/events/model/Event;->T:I

    move v0, v0

    .line 1815960
    if-nez v0, :cond_14

    .line 1815961
    sget-object v0, LX/Bkw;->S:LX/0U1;

    .line 1815962
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 1815963
    invoke-virtual {v1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1815964
    :goto_d
    iget-object v0, p0, Lcom/facebook/events/model/Event;->Q:Ljava/lang/String;

    move-object v0, v0

    .line 1815965
    if-eqz v0, :cond_15

    .line 1815966
    sget-object v0, LX/Bkw;->Q:LX/0U1;

    .line 1815967
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 1815968
    iget-object v2, p0, Lcom/facebook/events/model/Event;->Q:Ljava/lang/String;

    move-object v2, v2

    .line 1815969
    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1815970
    :goto_e
    sget-object v0, LX/Bkw;->T:LX/0U1;

    .line 1815971
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 1815972
    iget-object v2, p0, Lcom/facebook/events/model/Event;->V:Landroid/net/Uri;

    move-object v2, v2

    .line 1815973
    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1815974
    sget-object v0, LX/Bkw;->U:LX/0U1;

    .line 1815975
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 1815976
    iget-object v2, p0, Lcom/facebook/events/model/Event;->W:Ljava/lang/String;

    move-object v2, v2

    .line 1815977
    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1815978
    iget-object v0, p0, Lcom/facebook/events/model/Event;->X:Landroid/net/Uri;

    move-object v0, v0

    .line 1815979
    if-eqz v0, :cond_16

    .line 1815980
    sget-object v0, LX/Bkw;->V:LX/0U1;

    .line 1815981
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 1815982
    iget-object v2, p0, Lcom/facebook/events/model/Event;->X:Landroid/net/Uri;

    move-object v2, v2

    .line 1815983
    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1815984
    :goto_f
    iget-object v0, p0, Lcom/facebook/events/model/Event;->Y:Landroid/net/Uri;

    move-object v0, v0

    .line 1815985
    if-eqz v0, :cond_17

    .line 1815986
    sget-object v0, LX/Bkw;->W:LX/0U1;

    .line 1815987
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 1815988
    iget-object v2, p0, Lcom/facebook/events/model/Event;->Y:Landroid/net/Uri;

    move-object v2, v2

    .line 1815989
    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1815990
    :goto_10
    iget-object v0, p0, Lcom/facebook/events/model/Event;->Z:Landroid/net/Uri;

    move-object v0, v0

    .line 1815991
    if-eqz v0, :cond_3

    .line 1815992
    sget-object v0, LX/Bkw;->X:LX/0U1;

    .line 1815993
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 1815994
    iget-object v2, p0, Lcom/facebook/events/model/Event;->Z:Landroid/net/Uri;

    move-object v2, v2

    .line 1815995
    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1815996
    :cond_3
    sget-object v0, LX/Bkw;->Y:LX/0U1;

    .line 1815997
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 1815998
    invoke-virtual {p0}, Lcom/facebook/events/model/Event;->ag()Ljava/util/EnumSet;

    move-result-object v2

    invoke-static {v2}, LX/7vK;->serializeCapabilities(Ljava/util/EnumSet;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1815999
    iget-object v0, p0, Lcom/facebook/events/model/Event;->o:Ljava/lang/String;

    move-object v0, v0

    .line 1816000
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 1816001
    sget-object v0, LX/Bkw;->Z:LX/0U1;

    .line 1816002
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 1816003
    invoke-virtual {v1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1816004
    :goto_11
    sget-object v0, LX/Bkw;->aa:LX/0U1;

    .line 1816005
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 1816006
    iget v2, p0, Lcom/facebook/events/model/Event;->p:I

    move v2, v2

    .line 1816007
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1816008
    iget-object v0, p0, Lcom/facebook/events/model/Event;->q:LX/0Px;

    move-object v0, v0

    .line 1816009
    invoke-static {v0}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1816010
    sget-object v0, LX/Bkw;->ab:LX/0U1;

    .line 1816011
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 1816012
    const-string v2, ","

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 1816013
    iget-object v5, p0, Lcom/facebook/events/model/Event;->q:LX/0Px;

    move-object v5, v5

    .line 1816014
    aput-object v5, v3, v4

    invoke-static {v2, v3}, LX/0YN;->b(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1816015
    :cond_4
    iget-object v0, p0, Lcom/facebook/events/model/Event;->ad:Ljava/lang/String;

    move-object v0, v0

    .line 1816016
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 1816017
    sget-object v0, LX/Bkw;->ac:LX/0U1;

    .line 1816018
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 1816019
    invoke-virtual {v1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1816020
    :goto_12
    sget-object v0, LX/Bkw;->ad:LX/0U1;

    .line 1816021
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 1816022
    iget v2, p0, Lcom/facebook/events/model/Event;->ae:I

    move v2, v2

    .line 1816023
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1816024
    iget-object v0, p0, Lcom/facebook/events/model/Event;->af:Ljava/lang/String;

    move-object v0, v0

    .line 1816025
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 1816026
    sget-object v0, LX/Bkw;->ae:LX/0U1;

    .line 1816027
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 1816028
    invoke-virtual {v1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1816029
    :goto_13
    sget-object v0, LX/Bkw;->af:LX/0U1;

    .line 1816030
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 1816031
    iget v2, p0, Lcom/facebook/events/model/Event;->ag:I

    move v2, v2

    .line 1816032
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1816033
    iget-object v0, p0, Lcom/facebook/events/model/Event;->ah:Ljava/lang/String;

    move-object v0, v0

    .line 1816034
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 1816035
    sget-object v0, LX/Bkw;->ag:LX/0U1;

    .line 1816036
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 1816037
    invoke-virtual {v1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1816038
    :goto_14
    sget-object v0, LX/Bkw;->ah:LX/0U1;

    .line 1816039
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 1816040
    iget v2, p0, Lcom/facebook/events/model/Event;->ai:I

    move v2, v2

    .line 1816041
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1816042
    iget-object v0, p0, Lcom/facebook/events/model/Event;->aj:Lcom/facebook/events/model/EventUser;

    move-object v0, v0

    .line 1816043
    if-nez v0, :cond_1c

    .line 1816044
    sget-object v0, LX/Bkw;->ai:LX/0U1;

    .line 1816045
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 1816046
    invoke-virtual {v1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1816047
    sget-object v0, LX/Bkw;->aj:LX/0U1;

    .line 1816048
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 1816049
    invoke-virtual {v1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1816050
    sget-object v0, LX/Bkw;->ak:LX/0U1;

    .line 1816051
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 1816052
    invoke-virtual {v1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1816053
    :goto_15
    sget-object v0, LX/Bkw;->al:LX/0U1;

    .line 1816054
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 1816055
    iget v2, p0, Lcom/facebook/events/model/Event;->aq:I

    move v2, v2

    .line 1816056
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1816057
    return-object v1

    .line 1816058
    :cond_5
    sget-object v2, LX/Bkw;->d:LX/0U1;

    .line 1816059
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 1816060
    invoke-interface {v0}, LX/175;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1816061
    invoke-interface {v0}, LX/175;->b()LX/0Px;

    move-result-object v0

    .line 1816062
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1816063
    sget-object v2, LX/Bkw;->e:LX/0U1;

    .line 1816064
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 1816065
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 1816066
    const/4 v3, 0x0

    move v4, v3

    :goto_16
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    if-ge v4, v3, :cond_7

    .line 1816067
    if-eqz v4, :cond_6

    .line 1816068
    const-string v3, "\t"

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1816069
    :cond_6
    invoke-virtual {v0, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/1W5;

    .line 1816070
    invoke-interface {v3}, LX/1W5;->a()LX/171;

    move-result-object v7

    .line 1816071
    invoke-interface {v7}, LX/171;->e()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, "\t"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface {v7}, LX/171;->v_()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, "\t"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface {v7}, LX/171;->w_()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, "\t"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-interface {v7}, LX/171;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v6

    if-nez v6, :cond_1d

    const/4 v6, 0x0

    :goto_17
    invoke-static {v6}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, "\t"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface {v7}, LX/171;->j()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\t"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface {v3}, LX/1W5;->b()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\t"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface {v3}, LX/1W5;->c()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1816072
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto/16 :goto_16

    .line 1816073
    :cond_7
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v0, v3

    .line 1816074
    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1816075
    :cond_8
    sget-object v0, LX/Bkw;->f:LX/0U1;

    .line 1816076
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 1816077
    invoke-virtual {v1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1816078
    :cond_9
    sget-object v0, LX/Bkw;->g:LX/0U1;

    .line 1816079
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 1816080
    invoke-virtual {v1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 1816081
    :cond_a
    sget-object v0, LX/Bkw;->h:LX/0U1;

    .line 1816082
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 1816083
    invoke-virtual {v1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 1816084
    :cond_b
    sget-object v0, LX/Bkw;->i:LX/0U1;

    .line 1816085
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 1816086
    invoke-virtual {v1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto/16 :goto_4

    .line 1816087
    :cond_c
    iget-object v0, p0, Lcom/facebook/events/model/Event;->m:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-object v0, v0

    .line 1816088
    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLConnectionStyle;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_5

    .line 1816089
    :cond_d
    sget-object v0, LX/Bkw;->k:LX/0U1;

    .line 1816090
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 1816091
    invoke-virtual {v1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto/16 :goto_6

    .line 1816092
    :cond_e
    sget-object v0, LX/Bkw;->B:LX/0U1;

    .line 1816093
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 1816094
    invoke-virtual {v1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto/16 :goto_7

    .line 1816095
    :cond_f
    sget-object v0, LX/Bkw;->C:LX/0U1;

    .line 1816096
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 1816097
    invoke-virtual {v1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto/16 :goto_8

    .line 1816098
    :cond_10
    sget-object v0, LX/Bkw;->J:LX/0U1;

    .line 1816099
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 1816100
    invoke-virtual {v1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto/16 :goto_9

    .line 1816101
    :cond_11
    sget-object v0, LX/Bkw;->M:LX/0U1;

    .line 1816102
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 1816103
    invoke-virtual {v1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto/16 :goto_a

    .line 1816104
    :cond_12
    sget-object v0, LX/Bkw;->N:LX/0U1;

    .line 1816105
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 1816106
    invoke-virtual {v1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1816107
    sget-object v0, LX/Bkw;->O:LX/0U1;

    .line 1816108
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 1816109
    invoke-virtual {v1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto/16 :goto_b

    .line 1816110
    :cond_13
    sget-object v0, LX/Bkw;->R:LX/0U1;

    .line 1816111
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 1816112
    invoke-virtual {v1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto/16 :goto_c

    .line 1816113
    :cond_14
    sget-object v0, LX/Bkw;->S:LX/0U1;

    .line 1816114
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 1816115
    new-instance v2, Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1816116
    iget v3, p0, Lcom/facebook/events/model/Event;->T:I

    move v3, v3

    .line 1816117
    invoke-direct {v2, v3}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_d

    .line 1816118
    :cond_15
    sget-object v0, LX/Bkw;->Q:LX/0U1;

    .line 1816119
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 1816120
    invoke-virtual {v1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto/16 :goto_e

    .line 1816121
    :cond_16
    sget-object v0, LX/Bkw;->V:LX/0U1;

    .line 1816122
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 1816123
    invoke-virtual {v1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto/16 :goto_f

    .line 1816124
    :cond_17
    sget-object v0, LX/Bkw;->W:LX/0U1;

    .line 1816125
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 1816126
    invoke-virtual {v1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto/16 :goto_10

    .line 1816127
    :cond_18
    sget-object v0, LX/Bkw;->Z:LX/0U1;

    .line 1816128
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 1816129
    iget-object v2, p0, Lcom/facebook/events/model/Event;->o:Ljava/lang/String;

    move-object v2, v2

    .line 1816130
    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_11

    .line 1816131
    :cond_19
    sget-object v0, LX/Bkw;->ac:LX/0U1;

    .line 1816132
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 1816133
    iget-object v2, p0, Lcom/facebook/events/model/Event;->ad:Ljava/lang/String;

    move-object v2, v2

    .line 1816134
    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_12

    .line 1816135
    :cond_1a
    sget-object v0, LX/Bkw;->ae:LX/0U1;

    .line 1816136
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 1816137
    iget-object v2, p0, Lcom/facebook/events/model/Event;->af:Ljava/lang/String;

    move-object v2, v2

    .line 1816138
    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_13

    .line 1816139
    :cond_1b
    sget-object v0, LX/Bkw;->ag:LX/0U1;

    .line 1816140
    iget-object v2, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v2

    .line 1816141
    iget-object v2, p0, Lcom/facebook/events/model/Event;->ah:Ljava/lang/String;

    move-object v2, v2

    .line 1816142
    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_14

    .line 1816143
    :cond_1c
    iget-object v0, p0, Lcom/facebook/events/model/Event;->aj:Lcom/facebook/events/model/EventUser;

    move-object v0, v0

    .line 1816144
    sget-object v2, LX/Bkw;->ai:LX/0U1;

    .line 1816145
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 1816146
    iget-object v3, v0, Lcom/facebook/events/model/EventUser;->c:Ljava/lang/String;

    move-object v3, v3

    .line 1816147
    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1816148
    sget-object v2, LX/Bkw;->aj:LX/0U1;

    .line 1816149
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 1816150
    iget-object v3, v0, Lcom/facebook/events/model/EventUser;->h:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-object v3, v3

    .line 1816151
    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1816152
    sget-object v2, LX/Bkw;->ak:LX/0U1;

    .line 1816153
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 1816154
    iget-object v3, v0, Lcom/facebook/events/model/EventUser;->b:Ljava/lang/String;

    move-object v0, v3

    .line 1816155
    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_15

    :cond_1d
    invoke-interface {v7}, LX/171;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_17
.end method

.method public static a(Landroid/database/Cursor;II)Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;
    .locals 12

    .prologue
    .line 1815708
    new-instance v0, LX/7pC;

    invoke-direct {v0}, LX/7pC;-><init>()V

    .line 1815709
    invoke-static {p0, p1}, LX/Bl4;->i(Landroid/database/Cursor;I)Ljava/lang/String;

    move-result-object v1

    .line 1815710
    iput-object v1, v0, LX/7pC;->b:Ljava/lang/String;

    .line 1815711
    invoke-static {p0, p2}, LX/Bl4;->i(Landroid/database/Cursor;I)Ljava/lang/String;

    move-result-object v1

    .line 1815712
    if-eqz v1, :cond_0

    .line 1815713
    const/4 v4, 0x0

    .line 1815714
    sget-object v2, LX/Bl4;->b:LX/2Cb;

    invoke-virtual {v2, v1}, LX/2Cb;->split(Ljava/lang/CharSequence;)Ljava/lang/Iterable;

    move-result-object v2

    const-class v3, Ljava/lang/String;

    invoke-static {v2, v3}, LX/0Ph;->a(Ljava/lang/Iterable;Ljava/lang/Class;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    .line 1815715
    array-length v3, v2

    rem-int/lit8 v3, v3, 0x7

    if-eqz v3, :cond_1

    .line 1815716
    sget-object v2, LX/Bl4;->a:Ljava/lang/Class;

    const-string v3, "Serialized entities have in appropriate number of values."

    invoke-static {v2, v3}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 1815717
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 1815718
    :goto_0
    move-object v1, v2

    .line 1815719
    iput-object v1, v0, LX/7pC;->a:LX/0Px;

    .line 1815720
    :cond_0
    invoke-virtual {v0}, LX/7pC;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;

    move-result-object v0

    return-object v0

    .line 1815721
    :cond_1
    new-instance v6, LX/0Pz;

    invoke-direct {v6}, LX/0Pz;-><init>()V

    move v3, v4

    .line 1815722
    :goto_1
    array-length v5, v2

    if-ge v3, v5, :cond_2

    .line 1815723
    add-int/lit8 v5, v3, 0x1

    aget-object v7, v2, v3

    .line 1815724
    add-int/lit8 v3, v5, 0x1

    aget-object v8, v2, v5

    .line 1815725
    add-int/lit8 v5, v3, 0x1

    aget-object v3, v2, v3

    invoke-static {v3}, LX/0XM;->emptyToNull(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 1815726
    new-instance v10, Lcom/facebook/graphql/enums/GraphQLObjectType;

    add-int/lit8 v3, v5, 0x1

    aget-object v5, v2, v5

    invoke-direct {v10, v5}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(Ljava/lang/String;)V

    .line 1815727
    add-int/lit8 v11, v3, 0x1

    aget-object v3, v2, v3

    invoke-static {v3}, LX/0XM;->emptyToNull(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 1815728
    add-int/lit8 v5, v11, 0x1

    :try_start_0
    aget-object v3, v2, v11

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v11

    .line 1815729
    add-int/lit8 v3, v5, 0x1

    :try_start_1
    aget-object v5, v2, v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 1815730
    new-instance p1, LX/7pE;

    invoke-direct {p1}, LX/7pE;-><init>()V

    .line 1815731
    iput-object v7, p1, LX/7pE;->f:Ljava/lang/String;

    .line 1815732
    move-object p1, p1

    .line 1815733
    iput-object v8, p1, LX/7pE;->j:Ljava/lang/String;

    .line 1815734
    move-object v8, p1

    .line 1815735
    iput-object v9, v8, LX/7pE;->n:Ljava/lang/String;

    .line 1815736
    move-object v8, v8

    .line 1815737
    iput-object v10, v8, LX/7pE;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1815738
    move-object v8, v8

    .line 1815739
    iput-object p0, v8, LX/7pE;->o:Ljava/lang/String;

    .line 1815740
    move-object v8, v8

    .line 1815741
    invoke-virtual {v8}, LX/7pE;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;

    move-result-object v8

    .line 1815742
    new-instance v9, LX/7pD;

    invoke-direct {v9}, LX/7pD;-><init>()V

    .line 1815743
    iput-object v8, v9, LX/7pD;->a:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel$EntityModel;

    .line 1815744
    move-object v8, v9

    .line 1815745
    iput v5, v8, LX/7pD;->c:I

    .line 1815746
    move-object v5, v8

    .line 1815747
    iput v11, v5, LX/7pD;->b:I

    .line 1815748
    move-object v5, v5

    .line 1815749
    invoke-virtual {v5}, LX/7pD;->a()Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel$RangesModel;

    move-result-object v5

    .line 1815750
    invoke-virtual {v6, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 1815751
    :catch_0
    move-exception v5

    .line 1815752
    :goto_2
    sget-object v8, LX/Bl4;->a:Ljava/lang/Class;

    const-string v9, "Was not able to parse entity in text (%s)."

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    aput-object v7, v10, v4

    invoke-static {v8, v5, v9, v10}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 1815753
    :cond_2
    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    goto/16 :goto_0

    .line 1815754
    :catch_1
    move-exception v3

    move-object p2, v3

    move v3, v5

    move-object v5, p2

    goto :goto_2
.end method

.method private static a(Landroid/content/ContentValues;LX/0U1;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1815778
    invoke-static {p2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1815779
    iget-object v0, p1, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v0

    .line 1815780
    invoke-virtual {p0, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1815781
    :goto_0
    return-void

    .line 1815782
    :cond_0
    iget-object v0, p1, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v0

    .line 1815783
    invoke-virtual {p0, v0, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a(Landroid/database/Cursor;IZ)Z
    .locals 1

    .prologue
    .line 1815777
    if-gez p1, :cond_0

    :goto_0
    return p2

    :cond_0
    invoke-interface {p0, p1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_1

    const/4 p2, 0x0

    goto :goto_0

    :cond_1
    const/4 p2, 0x1

    goto :goto_0
.end method

.method public static b(Landroid/database/Cursor;II)I
    .locals 0

    .prologue
    .line 1815776
    if-gez p1, :cond_0

    :goto_0
    return p2

    :cond_0
    invoke-interface {p0, p1}, Landroid/database/Cursor;->getInt(I)I

    move-result p2

    goto :goto_0
.end method

.method public static i(Landroid/database/Cursor;I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1815775
    if-gez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p0, p1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static k(Landroid/database/Cursor;I)Ljava/util/Date;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1815770
    if-gez p1, :cond_1

    .line 1815771
    :cond_0
    :goto_0
    return-object v0

    .line 1815772
    :cond_1
    :try_start_0
    invoke-interface {p0, p1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1815773
    new-instance v1, Ljava/util/Date;

    invoke-interface {p0, p1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    goto :goto_0

    .line 1815774
    :catch_0
    goto :goto_0
.end method

.method public static l(Landroid/database/Cursor;I)Ljava/util/TimeZone;
    .locals 1

    .prologue
    .line 1815768
    invoke-static {p0, p1}, LX/Bl4;->i(Landroid/database/Cursor;I)Ljava/lang/String;

    move-result-object v0

    .line 1815769
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    goto :goto_0
.end method

.method public static m(Landroid/database/Cursor;I)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 1815764
    invoke-static {p0, p1}, LX/Bl4;->i(Landroid/database/Cursor;I)Ljava/lang/String;

    move-result-object v0

    .line 1815765
    if-nez v0, :cond_0

    .line 1815766
    const/4 v0, 0x0

    .line 1815767
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public static o(Landroid/database/Cursor;I)Ljava/util/EnumSet;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "I)",
            "Ljava/util/EnumSet",
            "<",
            "LX/7vK;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1815755
    const/4 v2, 0x0

    .line 1815756
    if-gez p1, :cond_2

    .line 1815757
    :cond_0
    :goto_0
    move-object v0, v2

    .line 1815758
    if-nez v0, :cond_1

    .line 1815759
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 1815760
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, LX/7vK;->deserializeCapabilities(J)Ljava/util/EnumSet;

    move-result-object v0

    return-object v0

    .line 1815761
    :cond_2
    :try_start_0
    invoke-interface {p0, p1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1815762
    invoke-interface {p0, p1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    goto :goto_0

    .line 1815763
    :catch_0
    goto :goto_0
.end method
