.class public final enum LX/C0a;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/C0a;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/C0a;

.field public static final enum CANNOT_SUBSCRIBE:LX/C0a;

.field public static final enum FOLLOWING_USER:LX/C0a;

.field public static final enum FOLLOW_USER:LX/C0a;

.field public static final enum LIKED_PAGE:LX/C0a;

.field public static final enum LIKE_PAGE:LX/C0a;


# instance fields
.field private mText:I

.field private mVisibility:I


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x4

    const/4 v3, 0x0

    .line 1841026
    new-instance v0, LX/C0a;

    const-string v1, "FOLLOW_USER"

    const v2, 0x7f081a44

    invoke-direct {v0, v1, v3, v2, v3}, LX/C0a;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/C0a;->FOLLOW_USER:LX/C0a;

    .line 1841027
    new-instance v0, LX/C0a;

    const-string v1, "FOLLOWING_USER"

    const v2, 0x7f081a45

    invoke-direct {v0, v1, v5, v2, v3}, LX/C0a;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/C0a;->FOLLOWING_USER:LX/C0a;

    .line 1841028
    new-instance v0, LX/C0a;

    const-string v1, "LIKE_PAGE"

    const v2, 0x7f081a46

    invoke-direct {v0, v1, v6, v2, v3}, LX/C0a;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/C0a;->LIKE_PAGE:LX/C0a;

    .line 1841029
    new-instance v0, LX/C0a;

    const-string v1, "LIKED_PAGE"

    const v2, 0x7f081a47

    invoke-direct {v0, v1, v7, v2, v3}, LX/C0a;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/C0a;->LIKED_PAGE:LX/C0a;

    .line 1841030
    new-instance v0, LX/C0a;

    const-string v1, "CANNOT_SUBSCRIBE"

    const v2, 0x7f081a44

    invoke-direct {v0, v1, v4, v2, v4}, LX/C0a;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/C0a;->CANNOT_SUBSCRIBE:LX/C0a;

    .line 1841031
    const/4 v0, 0x5

    new-array v0, v0, [LX/C0a;

    sget-object v1, LX/C0a;->FOLLOW_USER:LX/C0a;

    aput-object v1, v0, v3

    sget-object v1, LX/C0a;->FOLLOWING_USER:LX/C0a;

    aput-object v1, v0, v5

    sget-object v1, LX/C0a;->LIKE_PAGE:LX/C0a;

    aput-object v1, v0, v6

    sget-object v1, LX/C0a;->LIKED_PAGE:LX/C0a;

    aput-object v1, v0, v7

    sget-object v1, LX/C0a;->CANNOT_SUBSCRIBE:LX/C0a;

    aput-object v1, v0, v4

    sput-object v0, LX/C0a;->$VALUES:[LX/C0a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 1841022
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1841023
    iput p3, p0, LX/C0a;->mText:I

    .line 1841024
    iput p4, p0, LX/C0a;->mVisibility:I

    .line 1841025
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/C0a;
    .locals 1

    .prologue
    .line 1841032
    const-class v0, LX/C0a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/C0a;

    return-object v0
.end method

.method public static values()[LX/C0a;
    .locals 1

    .prologue
    .line 1841021
    sget-object v0, LX/C0a;->$VALUES:[LX/C0a;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/C0a;

    return-object v0
.end method


# virtual methods
.method public final setState(Lcom/facebook/resources/ui/FbButton;)Lcom/facebook/resources/ui/FbButton;
    .locals 1

    .prologue
    .line 1841018
    iget v0, p0, LX/C0a;->mText:I

    invoke-virtual {p1, v0}, Lcom/facebook/resources/ui/FbButton;->setText(I)V

    .line 1841019
    iget v0, p0, LX/C0a;->mVisibility:I

    invoke-virtual {p1, v0}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 1841020
    return-object p1
.end method
