.class public final LX/Bfe;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;

.field public final synthetic b:Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;


# direct methods
.method public constructor <init>(Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;)V
    .locals 0

    .prologue
    .line 1806403
    iput-object p1, p0, LX/Bfe;->b:Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;

    iput-object p2, p0, LX/Bfe;->a:Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x71c9b7cc

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1806404
    iget-object v1, p0, LX/Bfe;->b:Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;

    iget-object v1, v1, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6Zi;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "after_party"

    iget-object v4, p0, LX/Bfe;->a:Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;

    invoke-virtual {v4}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;->e()LX/1k1;

    move-result-object v4

    invoke-interface {v4}, LX/1k1;->a()D

    move-result-wide v4

    iget-object v6, p0, LX/Bfe;->a:Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;

    invoke-virtual {v6}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;->e()LX/1k1;

    move-result-object v6

    invoke-interface {v6}, LX/1k1;->b()D

    move-result-wide v6

    iget-object v8, p0, LX/Bfe;->a:Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;

    invoke-virtual {v8}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;->d()Ljava/lang/String;

    move-result-object v8

    iget-object v9, p0, LX/Bfe;->a:Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;

    invoke-virtual {v9}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;->a()Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel$AddressModel;

    move-result-object v9

    if-eqz v9, :cond_0

    iget-object v9, p0, LX/Bfe;->a:Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;

    invoke-virtual {v9}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;->a()Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel$AddressModel;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel$AddressModel;->a()Ljava/lang/String;

    move-result-object v9

    :goto_0
    invoke-virtual/range {v1 .. v9}, LX/6Zi;->a(Landroid/content/Context;Ljava/lang/String;DDLjava/lang/String;Ljava/lang/String;)V

    .line 1806405
    const v1, 0x26f5b08b

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 1806406
    :cond_0
    const/4 v9, 0x0

    goto :goto_0
.end method
