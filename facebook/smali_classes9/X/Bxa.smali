.class public LX/Bxa;
.super Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;
.source ""

# interfaces
.implements LX/2eZ;


# instance fields
.field public final a:LX/BxZ;

.field public b:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1835720
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/Bxa;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1835721
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1835722
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1835723
    const v0, 0x7f030119

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1835724
    new-instance v0, LX/BxZ;

    invoke-direct {v0, p0}, LX/BxZ;-><init>(LX/Bxa;)V

    iput-object v0, p0, LX/Bxa;->a:LX/BxZ;

    .line 1835725
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a045d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 1835726
    new-instance v1, Lcom/facebook/drawee/drawable/AutoRotateDrawable;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f021af6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    const/16 v3, 0x3e8

    invoke-direct {v1, v2, v3}, Lcom/facebook/drawee/drawable/AutoRotateDrawable;-><init>(Landroid/graphics/drawable/Drawable;I)V

    .line 1835727
    new-instance v2, LX/1Uo;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-direct {v2, v3}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    .line 1835728
    iput-object v0, v2, LX/1Uo;->f:Landroid/graphics/drawable/Drawable;

    .line 1835729
    move-object v0, v2

    .line 1835730
    iput-object v1, v0, LX/1Uo;->l:Landroid/graphics/drawable/Drawable;

    .line 1835731
    move-object v0, v0

    .line 1835732
    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    .line 1835733
    iget-object v1, p0, LX/Bxa;->a:LX/BxZ;

    iget-object v1, v1, LX/BxZ;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 1835734
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 1835719
    iget-boolean v0, p0, LX/Bxa;->b:Z

    return v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x62801c3c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1835715
    invoke-super {p0}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;->onAttachedToWindow()V

    .line 1835716
    const/4 v1, 0x1

    .line 1835717
    iput-boolean v1, p0, LX/Bxa;->b:Z

    .line 1835718
    const/16 v1, 0x2d

    const v2, 0x77b61a3d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x6ee60b8c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1835711
    invoke-super {p0}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;->onDetachedFromWindow()V

    .line 1835712
    const/4 v1, 0x0

    .line 1835713
    iput-boolean v1, p0, LX/Bxa;->b:Z

    .line 1835714
    const/16 v1, 0x2d

    const v2, 0x51342df7

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
