.class public LX/B9S;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements LX/B9N;


# static fields
.field public static final d:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public a:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/BVS;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/B9T;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:Z

.field public f:Lcom/facebook/graphql/model/GraphQLMegaphone;

.field public g:Lcom/facebook/widget/images/UrlImage;

.field public h:Landroid/widget/Button;

.field public i:Landroid/widget/Button;

.field public j:Landroid/widget/Button;

.field public k:Landroid/widget/ImageButton;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1751591
    const-class v0, LX/B9S;

    sput-object v0, LX/B9S;->d:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 1751640
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 1751641
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, LX/B9S;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v2

    check-cast v2, LX/03V;

    invoke-static {v0}, LX/BVS;->b(LX/0QB;)LX/BVS;

    move-result-object p1

    check-cast p1, LX/BVS;

    invoke-static {v0}, LX/B9T;->b(LX/0QB;)LX/B9T;

    move-result-object v0

    check-cast v0, LX/B9T;

    iput-object v2, p0, LX/B9S;->a:LX/03V;

    iput-object p1, p0, LX/B9S;->b:LX/BVS;

    iput-object v0, p0, LX/B9S;->c:LX/B9T;

    .line 1751642
    return-void
.end method

.method public static a(LX/B9S;Ljava/lang/Integer;)Landroid/view/View;
    .locals 1
    .param p0    # LX/B9S;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1751639
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, LX/B9S;->findViewById(I)Landroid/view/View;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/B9S;)V
    .locals 2

    .prologue
    .line 1751634
    invoke-virtual {p0}, LX/B9S;->removeAllViews()V

    .line 1751635
    new-instance v0, LX/B9O;

    invoke-virtual {p0}, LX/B9S;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/B9O;-><init>(Landroid/content/Context;)V

    .line 1751636
    iget-object v1, p0, LX/B9S;->f:Lcom/facebook/graphql/model/GraphQLMegaphone;

    invoke-virtual {v0, v1}, LX/B9O;->setMegaphoneStory(Lcom/facebook/graphql/model/GraphQLMegaphone;)V

    .line 1751637
    invoke-virtual {p0, v0}, LX/B9S;->addView(Landroid/view/View;)V

    .line 1751638
    return-void
.end method


# virtual methods
.method public final onMeasure(II)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1751630
    invoke-virtual {p0}, LX/B9S;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    .line 1751631
    invoke-virtual {p0, v2, v2}, LX/B9S;->setMeasuredDimension(II)V

    .line 1751632
    :goto_0
    return-void

    .line 1751633
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/CustomRelativeLayout;->onMeasure(II)V

    goto :goto_0
.end method

.method public setMegaphoneStory(LX/3TD;)V
    .locals 5

    .prologue
    .line 1751592
    iget-boolean v0, p0, LX/B9S;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/B9S;->f:Lcom/facebook/graphql/model/GraphQLMegaphone;

    .line 1751593
    iget-object v1, p1, LX/3TD;->b:Lcom/facebook/graphql/model/GraphQLMegaphone;

    move-object v1, v1

    .line 1751594
    if-ne v0, v1, :cond_0

    .line 1751595
    :goto_0
    return-void

    .line 1751596
    :cond_0
    iget-object v0, p1, LX/3TD;->b:Lcom/facebook/graphql/model/GraphQLMegaphone;

    move-object v0, v0

    .line 1751597
    iput-object v0, p0, LX/B9S;->f:Lcom/facebook/graphql/model/GraphQLMegaphone;

    .line 1751598
    if-eqz v0, :cond_1

    .line 1751599
    iget-object v0, p1, LX/3TD;->a:Ljava/lang/String;

    move-object v0, v0

    .line 1751600
    if-nez v0, :cond_2

    .line 1751601
    invoke-static {p0}, LX/B9S;->b(LX/B9S;)V

    .line 1751602
    :goto_1
    goto :goto_0

    .line 1751603
    :cond_1
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, LX/B9S;->setVisibility(I)V

    goto :goto_0

    .line 1751604
    :cond_2
    :try_start_0
    invoke-virtual {p0}, LX/B9S;->removeAllViews()V

    .line 1751605
    iget-object v1, p0, LX/B9S;->b:LX/BVS;

    invoke-virtual {p0}, LX/B9S;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v0, p0, v2}, LX/BVS;->a(Ljava/lang/String;Landroid/view/ViewGroup;Landroid/content/Context;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/B9S;->addView(Landroid/view/View;)V

    .line 1751606
    iget-object v1, p0, LX/B9S;->b:LX/BVS;

    const-string v2, "megaphone_icon"

    invoke-virtual {v1, v2}, LX/BVS;->a(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p0, v1}, LX/B9S;->a(LX/B9S;Ljava/lang/Integer;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/images/UrlImage;

    iput-object v1, p0, LX/B9S;->g:Lcom/facebook/widget/images/UrlImage;

    .line 1751607
    iget-object v1, p0, LX/B9S;->b:LX/BVS;

    const-string v2, "megaphone_button_accept"

    invoke-virtual {v1, v2}, LX/BVS;->a(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p0, v1}, LX/B9S;->a(LX/B9S;Ljava/lang/Integer;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, LX/B9S;->i:Landroid/widget/Button;

    .line 1751608
    iget-object v1, p0, LX/B9S;->b:LX/BVS;

    const-string v2, "megaphone_button_solo_accept"

    invoke-virtual {v1, v2}, LX/BVS;->a(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p0, v1}, LX/B9S;->a(LX/B9S;Ljava/lang/Integer;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, LX/B9S;->h:Landroid/widget/Button;

    .line 1751609
    iget-object v1, p0, LX/B9S;->b:LX/BVS;

    const-string v2, "megaphone_button_cancel"

    invoke-virtual {v1, v2}, LX/BVS;->a(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p0, v1}, LX/B9S;->a(LX/B9S;Ljava/lang/Integer;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, LX/B9S;->j:Landroid/widget/Button;

    .line 1751610
    iget-object v1, p0, LX/B9S;->b:LX/BVS;

    const-string v2, "megaphone_x_button"

    invoke-virtual {v1, v2}, LX/BVS;->a(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p0, v1}, LX/B9S;->a(LX/B9S;Ljava/lang/Integer;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, p0, LX/B9S;->k:Landroid/widget/ImageButton;
    :try_end_0
    .catch LX/BVR; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1751611
    new-instance v1, LX/B9P;

    invoke-direct {v1, p0}, LX/B9P;-><init>(LX/B9S;)V

    .line 1751612
    new-instance v2, LX/B9Q;

    invoke-direct {v2, p0}, LX/B9Q;-><init>(LX/B9S;)V

    .line 1751613
    iget-object v3, p0, LX/B9S;->k:Landroid/widget/ImageButton;

    if-eqz v3, :cond_3

    .line 1751614
    iget-object v3, p0, LX/B9S;->k:Landroid/widget/ImageButton;

    invoke-virtual {v3, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1751615
    :cond_3
    iget-object v3, p0, LX/B9S;->j:Landroid/widget/Button;

    if-eqz v3, :cond_4

    .line 1751616
    iget-object v3, p0, LX/B9S;->j:Landroid/widget/Button;

    invoke-virtual {v3, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1751617
    :cond_4
    iget-object v1, p0, LX/B9S;->i:Landroid/widget/Button;

    if-eqz v1, :cond_5

    .line 1751618
    iget-object v1, p0, LX/B9S;->i:Landroid/widget/Button;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1751619
    :cond_5
    iget-object v1, p0, LX/B9S;->h:Landroid/widget/Button;

    if-eqz v1, :cond_6

    .line 1751620
    iget-object v1, p0, LX/B9S;->h:Landroid/widget/Button;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1751621
    :cond_6
    iget-object v1, p0, LX/B9S;->g:Lcom/facebook/widget/images/UrlImage;

    if-eqz v1, :cond_7

    .line 1751622
    iget-object v1, p0, LX/B9S;->g:Lcom/facebook/widget/images/UrlImage;

    new-instance v2, LX/B9R;

    invoke-direct {v2, p0}, LX/B9R;-><init>(LX/B9S;)V

    invoke-virtual {v1, v2}, Lcom/facebook/widget/images/UrlImage;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1751623
    :cond_7
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/B9S;->e:Z

    goto/16 :goto_1

    .line 1751624
    :catch_0
    move-exception v1

    .line 1751625
    iget-object v2, p0, LX/B9S;->a:LX/03V;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, LX/B9S;->d:Ljava/lang/Class;

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "_inflation"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string p1, "error thrown while inflating, megaphone ID: "

    invoke-direct {v4, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object p1, p0, LX/B9S;->f:Lcom/facebook/graphql/model/GraphQLMegaphone;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLMegaphone;->l()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1751626
    invoke-static {p0}, LX/B9S;->b(LX/B9S;)V

    goto/16 :goto_1

    .line 1751627
    :catch_1
    move-exception v1

    .line 1751628
    iget-object v2, p0, LX/B9S;->a:LX/03V;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, LX/B9S;->d:Ljava/lang/Class;

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "_buttons"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string p1, "error thrown while finding buttons: "

    invoke-direct {v4, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object p1, p0, LX/B9S;->f:Lcom/facebook/graphql/model/GraphQLMegaphone;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLMegaphone;->l()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1751629
    invoke-static {p0}, LX/B9S;->b(LX/B9S;)V

    goto/16 :goto_1
.end method
