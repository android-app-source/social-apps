.class public LX/ByV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/26N;


# instance fields
.field private final a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLMedia;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLMedia;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1837414
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1837415
    iput-object p1, p0, LX/ByV;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1837416
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 2

    .prologue
    .line 1837417
    iget-object v0, p0, LX/ByV;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1837418
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 1837419
    check-cast v0, Lcom/facebook/graphql/model/GraphQLMedia;

    .line 1837420
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v1, 0x4ed245b

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1837413
    invoke-virtual {p0}, LX/ByV;->c()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, LX/ByV;->c()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->at()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, LX/ByV;->c()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->aN()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Lcom/facebook/graphql/model/GraphQLMedia;
    .locals 1

    .prologue
    .line 1837410
    iget-object v0, p0, LX/ByV;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1837411
    iget-object p0, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, p0

    .line 1837412
    check-cast v0, Lcom/facebook/graphql/model/GraphQLMedia;

    return-object v0
.end method
