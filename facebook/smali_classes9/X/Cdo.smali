.class public LX/Cdo;
.super LX/0bY;
.source ""


# instance fields
.field private final a:LX/Cdl;

.field private final b:I


# direct methods
.method public constructor <init>(LX/Cdl;I)V
    .locals 0

    .prologue
    .line 1923462
    invoke-direct {p0}, LX/0bY;-><init>()V

    .line 1923463
    iput-object p1, p0, LX/Cdo;->a:LX/Cdl;

    .line 1923464
    iput p2, p0, LX/Cdo;->b:I

    .line 1923465
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1923466
    const-string v0, "BLE Broadcast Detected:\n  MAC: %s\n  RSSI: %d\n%s\n"

    iget-object v1, p0, LX/Cdo;->a:LX/Cdl;

    .line 1923467
    iget-object v2, v1, LX/Cdl;->a:Landroid/bluetooth/BluetoothDevice;

    move-object v1, v2

    .line 1923468
    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v1

    iget v2, p0, LX/Cdo;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget-object v3, p0, LX/Cdo;->a:LX/Cdl;

    .line 1923469
    iget-object p0, v3, LX/Cdl;->c:Ljava/lang/String;

    move-object v3, p0

    .line 1923470
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
