.class public final LX/Bsy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Bsv;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/Bsv",
        "<",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/feed/rows/sections/offline/OfflineStoryPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/sections/offline/OfflineStoryPartDefinition;)V
    .locals 0

    .prologue
    .line 1828513
    iput-object p1, p0, LX/Bsy;->a:Lcom/facebook/feed/rows/sections/offline/OfflineStoryPartDefinition;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/1RF;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pr;)V
    .locals 1

    .prologue
    .line 1828514
    iget-object v0, p0, LX/Bsy;->a:Lcom/facebook/feed/rows/sections/offline/OfflineStoryPartDefinition;

    iget-object v0, v0, Lcom/facebook/feed/rows/sections/offline/OfflineStoryPartDefinition;->p:Lcom/facebook/feed/rows/sections/header/components/FundraiserUpsellHeaderComponentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1828515
    iget-object v0, p0, LX/Bsy;->a:Lcom/facebook/feed/rows/sections/offline/OfflineStoryPartDefinition;

    iget-object v0, v0, Lcom/facebook/feed/rows/sections/offline/OfflineStoryPartDefinition;->a:Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1828516
    iget-object v0, p0, LX/Bsy;->a:Lcom/facebook/feed/rows/sections/offline/OfflineStoryPartDefinition;

    iget-object v0, v0, Lcom/facebook/feed/rows/sections/offline/OfflineStoryPartDefinition;->q:Lcom/facebook/feedplugins/taptoadd/TapToAddPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1828517
    iget-object v0, p0, LX/Bsy;->a:Lcom/facebook/feed/rows/sections/offline/OfflineStoryPartDefinition;

    iget-object v0, v0, Lcom/facebook/feed/rows/sections/offline/OfflineStoryPartDefinition;->b:Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1828518
    iget-object v0, p0, LX/Bsy;->a:Lcom/facebook/feed/rows/sections/offline/OfflineStoryPartDefinition;

    iget-object v0, v0, Lcom/facebook/feed/rows/sections/offline/OfflineStoryPartDefinition;->c:Lcom/facebook/feedplugins/attachments/MinutiaeAttachmentGroupPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1828519
    iget-object v0, p0, LX/Bsy;->a:Lcom/facebook/feed/rows/sections/offline/OfflineStoryPartDefinition;

    iget-object v0, v0, Lcom/facebook/feed/rows/sections/offline/OfflineStoryPartDefinition;->d:Lcom/facebook/feed/rows/sections/text/TextSelectorPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1828520
    iget-object v0, p0, LX/Bsy;->a:Lcom/facebook/feed/rows/sections/offline/OfflineStoryPartDefinition;

    iget-object v0, v0, Lcom/facebook/feed/rows/sections/offline/OfflineStoryPartDefinition;->e:Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1828521
    iget-object v0, p0, LX/Bsy;->a:Lcom/facebook/feed/rows/sections/offline/OfflineStoryPartDefinition;

    iget-object v0, v0, Lcom/facebook/feed/rows/sections/offline/OfflineStoryPartDefinition;->i:Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1828522
    iget-object v0, p0, LX/Bsy;->a:Lcom/facebook/feed/rows/sections/offline/OfflineStoryPartDefinition;

    iget-object v0, v0, Lcom/facebook/feed/rows/sections/offline/OfflineStoryPartDefinition;->n:Lcom/facebook/feedplugins/offline/rows/MediaUploadProcessingComponentPartDefinition;

    invoke-virtual {v0, p2}, Lcom/facebook/feedplugins/offline/rows/MediaUploadProcessingComponentPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1828523
    iget-object v0, p0, LX/Bsy;->a:Lcom/facebook/feed/rows/sections/offline/OfflineStoryPartDefinition;

    iget-object v0, v0, Lcom/facebook/feed/rows/sections/offline/OfflineStoryPartDefinition;->f:Lcom/facebook/feed/rows/sections/StoryPostFooterSelectorPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1828524
    iget-object v0, p0, LX/Bsy;->a:Lcom/facebook/feed/rows/sections/offline/OfflineStoryPartDefinition;

    iget-object v0, v0, Lcom/facebook/feed/rows/sections/offline/OfflineStoryPartDefinition;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1828525
    :cond_0
    return-void
.end method
