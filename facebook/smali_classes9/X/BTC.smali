.class public final enum LX/BTC;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/BTC;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/BTC;

.field public static final enum CLICK_EDIT_BUTTON:LX/BTC;

.field public static final enum TAP_PLAYER:LX/BTC;

.field public static final enum TAP_VIDEO:LX/BTC;


# instance fields
.field private final name:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1786989
    new-instance v0, LX/BTC;

    const-string v1, "TAP_VIDEO"

    const-string v2, "tap_video"

    invoke-direct {v0, v1, v3, v2}, LX/BTC;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BTC;->TAP_VIDEO:LX/BTC;

    .line 1786990
    new-instance v0, LX/BTC;

    const-string v1, "CLICK_EDIT_BUTTON"

    const-string v2, "edit_button"

    invoke-direct {v0, v1, v4, v2}, LX/BTC;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BTC;->CLICK_EDIT_BUTTON:LX/BTC;

    .line 1786991
    new-instance v0, LX/BTC;

    const-string v1, "TAP_PLAYER"

    const-string v2, "tap_player"

    invoke-direct {v0, v1, v5, v2}, LX/BTC;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BTC;->TAP_PLAYER:LX/BTC;

    .line 1786992
    const/4 v0, 0x3

    new-array v0, v0, [LX/BTC;

    sget-object v1, LX/BTC;->TAP_VIDEO:LX/BTC;

    aput-object v1, v0, v3

    sget-object v1, LX/BTC;->CLICK_EDIT_BUTTON:LX/BTC;

    aput-object v1, v0, v4

    sget-object v1, LX/BTC;->TAP_PLAYER:LX/BTC;

    aput-object v1, v0, v5

    sput-object v0, LX/BTC;->$VALUES:[LX/BTC;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1786986
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1786987
    iput-object p3, p0, LX/BTC;->name:Ljava/lang/String;

    .line 1786988
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/BTC;
    .locals 1

    .prologue
    .line 1786985
    const-class v0, LX/BTC;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/BTC;

    return-object v0
.end method

.method public static values()[LX/BTC;
    .locals 1

    .prologue
    .line 1786983
    sget-object v0, LX/BTC;->$VALUES:[LX/BTC;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/BTC;

    return-object v0
.end method


# virtual methods
.method public final getParamKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1786984
    iget-object v0, p0, LX/BTC;->name:Ljava/lang/String;

    return-object v0
.end method
