.class public final enum LX/CU9;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/CU9;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/CU9;

.field public static final enum SHIMMER:LX/CU9;

.field public static final enum UNSHIMMER:LX/CU9;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1896266
    new-instance v0, LX/CU9;

    const-string v1, "SHIMMER"

    invoke-direct {v0, v1, v2}, LX/CU9;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CU9;->SHIMMER:LX/CU9;

    .line 1896267
    new-instance v0, LX/CU9;

    const-string v1, "UNSHIMMER"

    invoke-direct {v0, v1, v3}, LX/CU9;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CU9;->UNSHIMMER:LX/CU9;

    .line 1896268
    const/4 v0, 0x2

    new-array v0, v0, [LX/CU9;

    sget-object v1, LX/CU9;->SHIMMER:LX/CU9;

    aput-object v1, v0, v2

    sget-object v1, LX/CU9;->UNSHIMMER:LX/CU9;

    aput-object v1, v0, v3

    sput-object v0, LX/CU9;->$VALUES:[LX/CU9;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1896269
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/CU9;
    .locals 1

    .prologue
    .line 1896270
    const-class v0, LX/CU9;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/CU9;

    return-object v0
.end method

.method public static values()[LX/CU9;
    .locals 1

    .prologue
    .line 1896271
    sget-object v0, LX/CU9;->$VALUES:[LX/CU9;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/CU9;

    return-object v0
.end method
