.class public LX/BHG;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/4gI;",
            "Ljava/util/List",
            "<",
            "LX/BHH;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    .line 1768642
    new-instance v0, LX/0P2;

    invoke-direct {v0}, LX/0P2;-><init>()V

    sget-object v1, LX/4gI;->PHOTO_ONLY:LX/4gI;

    sget-object v2, LX/BHH;->PHOTO:LX/BHH;

    sget-object v3, LX/BHH;->GIF:LX/BHH;

    sget-object v4, LX/BHH;->LIVE_CAMERA:LX/BHH;

    invoke-static {v2, v3, v4}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, LX/4gI;->VIDEO_ONLY:LX/4gI;

    sget-object v2, LX/BHH;->VIDEO:LX/BHH;

    sget-object v3, LX/BHH;->LIVE_CAMERA:LX/BHH;

    invoke-static {v2, v3}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, LX/4gI;->ALL:LX/4gI;

    invoke-static {}, LX/BHH;->values()[LX/BHH;

    move-result-object v2

    invoke-static {v2}, LX/0Px;->copyOf([Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, LX/4gI;->PHOTO_ONLY_EXCLUDING_GIFS:LX/4gI;

    sget-object v2, LX/BHH;->PHOTO:LX/BHH;

    sget-object v3, LX/BHH;->LIVE_CAMERA:LX/BHH;

    invoke-static {v2, v3}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    sput-object v0, LX/BHG;->a:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1768643
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
