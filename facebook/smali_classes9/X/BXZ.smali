.class public LX/BXZ;
.super LX/8tB;
.source ""

# interfaces
.implements LX/2ht;


# instance fields
.field private final e:I

.field private final f:LX/333;

.field private g:I


# direct methods
.method public constructor <init>(Landroid/view/inputmethod/InputMethodManager;Landroid/content/Context;LX/8vM;LX/8tF;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1793290
    invoke-direct {p0, p1, p3}, LX/8tB;-><init>(Landroid/view/inputmethod/InputMethodManager;LX/8vM;)V

    .line 1793291
    const/4 v0, 0x0

    iput v0, p0, LX/BXZ;->g:I

    .line 1793292
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a022e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, LX/BXZ;->e:I

    .line 1793293
    invoke-virtual {p0}, LX/8tB;->e()LX/8vE;

    move-result-object v0

    invoke-virtual {p4, v0}, LX/8tF;->a(LX/8vE;)LX/8tE;

    move-result-object v0

    iput-object v0, p0, LX/BXZ;->f:LX/333;

    .line 1793294
    return-void
.end method

.method public static b(LX/0QB;)LX/BXZ;
    .locals 5

    .prologue
    .line 1793295
    new-instance v4, LX/BXZ;

    invoke-static {p0}, LX/10d;->b(LX/0QB;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    const-class v2, LX/8vM;

    invoke-interface {p0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/8vM;

    const-class v3, LX/8tF;

    invoke-interface {p0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/8tF;

    invoke-direct {v4, v0, v1, v2, v3}, LX/BXZ;-><init>(Landroid/view/inputmethod/InputMethodManager;Landroid/content/Context;LX/8vM;LX/8tF;)V

    .line 1793296
    move-object v0, v4

    .line 1793297
    return-object v0
.end method


# virtual methods
.method public a()LX/333;
    .locals 1

    .prologue
    .line 1793298
    iget-object v0, p0, LX/BXZ;->f:LX/333;

    return-object v0
.end method

.method public final b(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 1793299
    invoke-virtual {p0, p1}, LX/3Tf;->d(I)[I

    move-result-object v0

    const/4 v1, 0x0

    aget v0, v0, v1

    .line 1793300
    invoke-virtual {p0, v0, p2, p3}, LX/8tB;->a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 1793301
    iget v1, p0, LX/BXZ;->g:I

    if-nez v1, :cond_0

    .line 1793302
    const/4 p3, 0x0

    .line 1793303
    new-instance v1, Landroid/widget/AbsListView$LayoutParams;

    const/4 p1, -0x1

    const/4 p2, -0x2

    invoke-direct {v1, p1, p2}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1793304
    invoke-static {p3, p3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-static {p3, p3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    invoke-virtual {v0, v1, p1}, Landroid/view/View;->measure(II)V

    .line 1793305
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    move v1, v1

    .line 1793306
    iput v1, p0, LX/BXZ;->g:I

    .line 1793307
    :cond_0
    return-object v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 1793308
    iget v0, p0, LX/BXZ;->e:I

    return v0
.end method

.method public final e(I)I
    .locals 1

    .prologue
    .line 1793309
    iget v0, p0, LX/BXZ;->g:I

    return v0
.end method

.method public final f(I)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 1793310
    invoke-virtual {p0, p1}, LX/3Tf;->d(I)[I

    move-result-object v1

    .line 1793311
    aget v1, v1, v0

    .line 1793312
    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final o_(I)I
    .locals 1

    .prologue
    .line 1793313
    sget-object v0, LX/8vI;->HEADER:LX/8vI;

    invoke-virtual {v0}, LX/8vI;->ordinal()I

    move-result v0

    return v0
.end method
