.class public LX/B0O;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2kR;


# instance fields
.field private final a:Ljava/lang/String;

.field public final b:LX/B0U;

.field public final c:LX/B0L;

.field public d:LX/B0N;

.field private final e:LX/0SG;

.field private final f:LX/0tX;

.field public final g:Ljava/util/concurrent/Executor;

.field private final h:Ljava/util/concurrent/Executor;

.field public final i:LX/2k0;

.field public final j:LX/0Sh;

.field private final k:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field public l:LX/2kb;

.field public m:Z

.field public n:LX/B0d;

.field public final o:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private p:J

.field public q:J

.field private r:LX/B0M;

.field private s:LX/2kT;

.field private final t:Ljava/lang/Runnable;

.field private final u:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Ljava/lang/String;LX/B0U;LX/B0L;LX/B0N;LX/0SG;LX/0tX;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;LX/2k0;LX/0Sh;Lcom/facebook/quicklog/QuickPerformanceLogger;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/B0U;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/B0L;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/B0N;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p8    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1733856
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1733857
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/B0O;->m:Z

    .line 1733858
    const/4 v0, 0x0

    iput-object v0, p0, LX/B0O;->n:LX/B0d;

    .line 1733859
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, LX/B0O;->o:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 1733860
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/B0O;->p:J

    .line 1733861
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/B0O;->q:J

    .line 1733862
    const/4 v0, 0x0

    iput-object v0, p0, LX/B0O;->r:LX/B0M;

    .line 1733863
    new-instance v0, Lcom/facebook/graphql/connection/ConnectionTailLoaderManager$3;

    invoke-direct {v0, p0}, Lcom/facebook/graphql/connection/ConnectionTailLoaderManager$3;-><init>(LX/B0O;)V

    iput-object v0, p0, LX/B0O;->t:Ljava/lang/Runnable;

    .line 1733864
    new-instance v0, Lcom/facebook/graphql/connection/ConnectionTailLoaderManager$4;

    invoke-direct {v0, p0}, Lcom/facebook/graphql/connection/ConnectionTailLoaderManager$4;-><init>(LX/B0O;)V

    iput-object v0, p0, LX/B0O;->u:Ljava/lang/Runnable;

    .line 1733865
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/B0O;->a:Ljava/lang/String;

    .line 1733866
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/B0U;

    iput-object v0, p0, LX/B0O;->b:LX/B0U;

    .line 1733867
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/B0L;

    iput-object v0, p0, LX/B0O;->c:LX/B0L;

    .line 1733868
    iput-object p4, p0, LX/B0O;->d:LX/B0N;

    .line 1733869
    iput-object p5, p0, LX/B0O;->e:LX/0SG;

    .line 1733870
    iput-object p6, p0, LX/B0O;->f:LX/0tX;

    .line 1733871
    iput-object p7, p0, LX/B0O;->g:Ljava/util/concurrent/Executor;

    .line 1733872
    iput-object p8, p0, LX/B0O;->h:Ljava/util/concurrent/Executor;

    .line 1733873
    iput-object p9, p0, LX/B0O;->i:LX/2k0;

    .line 1733874
    iput-object p10, p0, LX/B0O;->j:LX/0Sh;

    .line 1733875
    iput-object p11, p0, LX/B0O;->k:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 1733876
    iget-object v0, p0, LX/B0O;->i:LX/2k0;

    iget-object v1, p0, LX/B0O;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/2k0;->a(Ljava/lang/String;)LX/2kb;

    move-result-object v0

    iput-object v0, p0, LX/B0O;->l:LX/2kb;

    .line 1733877
    iget-object v0, p0, LX/B0O;->i:LX/2k0;

    invoke-virtual {v0, p1, p0}, LX/2k0;->a(Ljava/lang/String;LX/2kR;)LX/2kT;

    move-result-object v0

    iput-object v0, p0, LX/B0O;->s:LX/2kT;

    .line 1733878
    return-void
.end method

.method private static a(LX/B0O;Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/cursor/edgestore/PageInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1733892
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1733893
    invoke-interface {p1}, Ljava/util/List;->size()I

    .line 1733894
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1733895
    iget-object v0, p0, LX/B0O;->e:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    invoke-static {v0, v1}, LX/B0d;->a(J)LX/B0d;

    move-result-object v0

    iput-object v0, p0, LX/B0O;->n:LX/B0d;

    .line 1733896
    :goto_1
    return-void

    :cond_0
    move v0, v1

    .line 1733897
    goto :goto_0

    .line 1733898
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/cursor/edgestore/PageInfo;

    .line 1733899
    iget-object v3, v0, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->b:Ljava/lang/String;

    .line 1733900
    iget-object v2, v0, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->d:Ljava/lang/String;

    .line 1733901
    iget-boolean v1, v0, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->f:Z

    move-object v0, v2

    move-object v2, v3

    .line 1733902
    :goto_2
    new-instance v3, LX/B0d;

    invoke-direct {v3, v2, v0, v1}, LX/B0d;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    iput-object v3, p0, LX/B0O;->n:LX/B0d;

    goto :goto_1

    :cond_2
    move-object v0, v2

    goto :goto_2
.end method

.method private a(LX/B0d;Z)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1733879
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1733880
    iget-object v2, p0, LX/B0O;->o:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v2, v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1733881
    :goto_0
    return v0

    .line 1733882
    :cond_0
    invoke-static {p0}, LX/B0O;->h(LX/B0O;)V

    .line 1733883
    iget-object v0, p0, LX/B0O;->r:LX/B0M;

    if-eqz v0, :cond_1

    .line 1733884
    iget-object v0, p0, LX/B0O;->r:LX/B0M;

    invoke-virtual {v0}, LX/B0M;->a()V

    .line 1733885
    const/4 v0, 0x0

    iput-object v0, p0, LX/B0O;->r:LX/B0M;

    .line 1733886
    :cond_1
    new-instance v0, LX/B0M;

    invoke-direct {v0, p0, p2}, LX/B0M;-><init>(LX/B0O;Z)V

    iput-object v0, p0, LX/B0O;->r:LX/B0M;

    .line 1733887
    iget-object v0, p0, LX/B0O;->b:LX/B0U;

    iget-object v2, p0, LX/B0O;->k:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget-object v3, p0, LX/B0O;->r:LX/B0M;

    .line 1733888
    sget-object v4, LX/131;->INSTANCE:LX/131;

    move-object v4, v4

    .line 1733889
    invoke-interface {v0, v2, v3, v4, p1}, LX/B0U;->a(Lcom/facebook/quicklog/QuickPerformanceLogger;LX/B0M;Ljava/util/concurrent/Executor;LX/B0d;)LX/0v6;

    move-result-object v0

    .line 1733890
    iget-object v2, p0, LX/B0O;->f:LX/0tX;

    invoke-virtual {v2, v0}, LX/0tX;->a(LX/0v6;)V

    move v0, v1

    .line 1733891
    goto :goto_0
.end method

.method public static c(LX/B0O;LX/2nf;)I
    .locals 14
    .annotation build Lcom/facebook/graphql/connection/ConnectionTailLoaderManager$DataFreshness;
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v2, -0x1

    const/4 v3, 0x2

    const/4 v5, 0x0

    .line 1733799
    if-nez p1, :cond_0

    .line 1733800
    const/4 v0, -0x2

    .line 1733801
    :goto_0
    return v0

    .line 1733802
    :cond_0
    invoke-interface {p1}, LX/2nf;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 1733803
    const-string v1, "CHUNKS"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v6

    .line 1733804
    iget-boolean v1, p0, LX/B0O;->m:Z

    if-nez v1, :cond_4

    .line 1733805
    invoke-interface {p1}, LX/2nf;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_5

    if-eqz v6, :cond_5

    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    .line 1733806
    invoke-interface {v6, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/cursor/edgestore/PageInfo;

    iget-wide v8, v1, Lcom/facebook/graphql/cursor/edgestore/PageInfo;->h:J

    .line 1733807
    iget-object v1, p0, LX/B0O;->e:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v10

    sub-long v8, v10, v8

    .line 1733808
    iget-object v1, p0, LX/B0O;->b:LX/B0U;

    invoke-interface {v1}, LX/B0U;->a()J

    move-result-wide v10

    const-wide/16 v12, 0x3e8

    mul-long/2addr v10, v12

    cmp-long v1, v8, v10

    if-gez v1, :cond_1

    move v1, v3

    .line 1733809
    :goto_1
    if-eq v1, v3, :cond_4

    .line 1733810
    iget-object v0, p0, LX/B0O;->d:LX/B0N;

    if-eqz v0, :cond_3

    .line 1733811
    iget-object v0, p0, LX/B0O;->o:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v5, v4}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v2

    .line 1733812
    goto :goto_0

    :cond_1
    move v1, v4

    .line 1733813
    goto :goto_1

    .line 1733814
    :cond_2
    invoke-static {p0}, LX/B0O;->h(LX/B0O;)V

    .line 1733815
    iget-object v0, p0, LX/B0O;->h:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/facebook/graphql/connection/ConnectionTailLoaderManager$2;

    invoke-direct {v1, p0}, Lcom/facebook/graphql/connection/ConnectionTailLoaderManager$2;-><init>(LX/B0O;)V

    const v3, -0x23d0eac9

    invoke-static {v0, v1, v3}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    move v0, v2

    .line 1733816
    goto :goto_0

    .line 1733817
    :cond_3
    invoke-virtual {p0}, LX/B0O;->c()V

    move v0, v1

    .line 1733818
    goto :goto_0

    .line 1733819
    :cond_4
    const-string v1, "CHANGE_NUMBER"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, LX/B0O;->p:J

    .line 1733820
    invoke-static {p0, v6}, LX/B0O;->a(LX/B0O;Ljava/util/List;)V

    move v0, v3

    .line 1733821
    goto :goto_0

    :cond_5
    move v1, v5

    goto :goto_1
.end method

.method public static g(LX/B0O;)V
    .locals 2

    .prologue
    .line 1733853
    iget-object v0, p0, LX/B0O;->i:LX/2k0;

    iget-object v1, p0, LX/B0O;->l:LX/2kb;

    invoke-virtual {v0, v1}, LX/2k0;->b(LX/2kb;)LX/2nd;

    move-result-object v0

    .line 1733854
    invoke-virtual {p0, v0}, LX/B0O;->a(LX/2nf;)V

    .line 1733855
    return-void
.end method

.method public static h(LX/B0O;)V
    .locals 3

    .prologue
    .line 1733849
    invoke-virtual {p0}, LX/B0O;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1733850
    iget-object v0, p0, LX/B0O;->g:Ljava/util/concurrent/Executor;

    iget-object v1, p0, LX/B0O;->t:Ljava/lang/Runnable;

    const v2, 0x51064ec7

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1733851
    :goto_0
    return-void

    .line 1733852
    :cond_0
    iget-object v0, p0, LX/B0O;->g:Ljava/util/concurrent/Executor;

    iget-object v1, p0, LX/B0O;->u:Ljava/lang/Runnable;

    const v2, 0x3114cc06

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_0
.end method

.method public static i$redex0(LX/B0O;)V
    .locals 2

    .prologue
    .line 1733903
    iget-object v0, p0, LX/B0O;->j:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 1733904
    invoke-virtual {p0}, LX/B0O;->e()Z

    move-result v0

    .line 1733905
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 1733906
    iget-object v1, p0, LX/B0O;->c:LX/B0L;

    invoke-interface {v1, v0}, LX/B0L;->a(Z)V

    .line 1733907
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 1733847
    iget-object v0, p0, LX/B0O;->h:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/facebook/graphql/connection/ConnectionTailLoaderManager$LoadRunnable;

    invoke-direct {v1, p0}, Lcom/facebook/graphql/connection/ConnectionTailLoaderManager$LoadRunnable;-><init>(LX/B0O;)V

    const v2, -0x656304a3

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1733848
    return-void
.end method

.method public final a(LX/2nf;)V
    .locals 3
    .param p1    # LX/2nf;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1733845
    iget-object v0, p0, LX/B0O;->g:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/facebook/graphql/connection/ConnectionTailLoaderManager$1;

    invoke-direct {v1, p0, p1}, Lcom/facebook/graphql/connection/ConnectionTailLoaderManager$1;-><init>(LX/B0O;LX/2nf;)V

    const v2, 0x13b1bd6

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1733846
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1733838
    iget-object v0, p0, LX/B0O;->s:LX/2kT;

    if-eqz v0, :cond_0

    .line 1733839
    iget-object v0, p0, LX/B0O;->s:LX/2kT;

    invoke-interface {v0}, LX/2kT;->close()V

    .line 1733840
    iput-object v1, p0, LX/B0O;->s:LX/2kT;

    .line 1733841
    :cond_0
    iget-object v0, p0, LX/B0O;->l:LX/2kb;

    if-eqz v0, :cond_1

    .line 1733842
    iget-object v0, p0, LX/B0O;->l:LX/2kb;

    invoke-interface {v0}, LX/2kb;->close()V

    .line 1733843
    iput-object v1, p0, LX/B0O;->l:LX/2kb;

    .line 1733844
    :cond_1
    return-void
.end method

.method public final declared-synchronized c()V
    .locals 2

    .prologue
    .line 1733829
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/B0O;->o:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/B0O;->m:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 1733830
    :goto_0
    monitor-exit p0

    return-void

    .line 1733831
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/B0O;->o:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 1733832
    iget-object v0, p0, LX/B0O;->r:LX/B0M;

    if-eqz v0, :cond_1

    .line 1733833
    iget-object v0, p0, LX/B0O;->r:LX/B0M;

    invoke-virtual {v0}, LX/B0M;->a()V

    .line 1733834
    const/4 v0, 0x0

    iput-object v0, p0, LX/B0O;->r:LX/B0M;

    .line 1733835
    :cond_1
    iget-object v0, p0, LX/B0O;->e:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    invoke-static {v0, v1}, LX/B0d;->a(J)LX/B0d;

    move-result-object v0

    .line 1733836
    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, LX/B0O;->a(LX/B0d;Z)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1733837
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1733823
    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, LX/B0O;->m:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/B0O;->n:LX/B0d;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_1

    .line 1733824
    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    .line 1733825
    :cond_1
    :try_start_1
    iget-wide v2, p0, LX/B0O;->q:J

    iget-wide v4, p0, LX/B0O;->p:J

    cmp-long v1, v2, v4

    if-gtz v1, :cond_0

    .line 1733826
    iget-object v1, p0, LX/B0O;->n:LX/B0d;

    iget-boolean v1, v1, LX/B0d;->c:Z

    if-eqz v1, :cond_0

    .line 1733827
    iget-object v0, p0, LX/B0O;->n:LX/B0d;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, LX/B0O;->a(LX/B0d;Z)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0

    .line 1733828
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 1733822
    iget-object v0, p0, LX/B0O;->o:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    return v0
.end method
