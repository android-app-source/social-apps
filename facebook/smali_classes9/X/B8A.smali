.class public LX/B8A;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements LX/B7m;


# static fields
.field public static final a:LX/B7j;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/B7j",
            "<",
            "LX/B8A;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public b:LX/7Tk;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/B8r;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/B7W;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:Landroid/widget/AutoCompleteTextView;

.field public f:Lcom/facebook/resources/ui/FbButton;

.field public g:Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;

.field public h:LX/B7w;

.field private i:Lcom/facebook/resources/ui/FbTextView;

.field public j:Landroid/widget/TextView;

.field private k:Landroid/text/TextWatcher;

.field public l:LX/7Tj;

.field public m:LX/7Tl;

.field public n:LX/B7F;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1748516
    new-instance v0, LX/B84;

    invoke-direct {v0}, LX/B84;-><init>()V

    sput-object v0, LX/B8A;->a:LX/B7j;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 1748526
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 1748527
    const v0, 0x7f0309c9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1748528
    const v0, 0x7f0d18e8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/AutoCompleteTextView;

    iput-object v0, p0, LX/B8A;->e:Landroid/widget/AutoCompleteTextView;

    .line 1748529
    const v0, 0x7f0d18e7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, LX/B8A;->f:Lcom/facebook/resources/ui/FbButton;

    .line 1748530
    const v0, 0x7f0d18e6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/B8A;->i:Lcom/facebook/resources/ui/FbTextView;

    .line 1748531
    const v0, 0x7f0d18d4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/B8A;->j:Landroid/widget/TextView;

    .line 1748532
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, LX/B8A;

    const-class v2, LX/7Tk;

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/7Tk;

    invoke-static {v0}, LX/B8r;->a(LX/0QB;)LX/B8r;

    move-result-object p1

    check-cast p1, LX/B8r;

    invoke-static {v0}, LX/B7W;->a(LX/0QB;)LX/B7W;

    move-result-object v0

    check-cast v0, LX/B7W;

    iput-object v2, p0, LX/B8A;->b:LX/7Tk;

    iput-object p1, p0, LX/B8A;->c:LX/B8r;

    iput-object v0, p0, LX/B8A;->d:LX/B7W;

    .line 1748533
    return-void
.end method

.method private static a(Landroid/widget/AutoCompleteTextView;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1748519
    invoke-virtual {p0}, Landroid/widget/AutoCompleteTextView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    .line 1748520
    instance-of v1, v0, Landroid/widget/ArrayAdapter;

    if-eqz v1, :cond_0

    .line 1748521
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Landroid/widget/AutoCompleteTextView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1748522
    invoke-virtual {p0, p1}, Landroid/widget/AutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1748523
    check-cast v0, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0, v0}, Landroid/widget/AutoCompleteTextView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1748524
    :goto_0
    return-void

    .line 1748525
    :cond_0
    invoke-virtual {p0, p1}, Landroid/widget/AutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public static f(LX/B8A;)V
    .locals 1

    .prologue
    .line 1748517
    const v0, 0x7f020deb

    invoke-static {p0, v0}, LX/B8A;->setIconDrawable(LX/B8A;I)V

    .line 1748518
    return-void
.end method

.method public static setAndFormatPhoneNumber(LX/B8A;LX/7Tl;)V
    .locals 3

    .prologue
    .line 1748504
    iput-object p1, p0, LX/B8A;->m:LX/7Tl;

    .line 1748505
    iget-object v0, p0, LX/B8A;->f:Lcom/facebook/resources/ui/FbButton;

    .line 1748506
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p1, LX/7Tl;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, LX/7Tl;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object v1, v1

    .line 1748507
    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 1748508
    iget-object v0, p0, LX/B8A;->e:Landroid/widget/AutoCompleteTextView;

    iget-object v1, p0, LX/B8A;->k:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1748509
    new-instance v0, LX/A8g;

    iget-object v1, p1, LX/7Tl;->a:Ljava/lang/String;

    invoke-virtual {p0}, LX/B8A;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/A8g;-><init>(Ljava/lang/String;Landroid/content/Context;)V

    iput-object v0, p0, LX/B8A;->k:Landroid/text/TextWatcher;

    .line 1748510
    iget-object v0, p0, LX/B8A;->e:Landroid/widget/AutoCompleteTextView;

    iget-object v1, p0, LX/B8A;->k:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1748511
    iget-object v0, p0, LX/B8A;->e:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1748512
    sget-object v1, LX/1IA;->WHITESPACE:LX/1IA;

    const-string v2, "()-."

    invoke-static {v2}, LX/1IA;->anyOf(Ljava/lang/CharSequence;)LX/1IA;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/1IA;->or(LX/1IA;)LX/1IA;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/1IA;->removeFrom(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 1748513
    iget-object v1, p0, LX/B8A;->e:Landroid/widget/AutoCompleteTextView;

    const-string v2, ""

    invoke-static {v1, v2}, LX/B8A;->a(Landroid/widget/AutoCompleteTextView;Ljava/lang/String;)V

    .line 1748514
    iget-object v1, p0, LX/B8A;->e:Landroid/widget/AutoCompleteTextView;

    invoke-static {v1, v0}, LX/B8A;->a(Landroid/widget/AutoCompleteTextView;Ljava/lang/String;)V

    .line 1748515
    return-void
.end method

.method public static setIconDrawable(LX/B8A;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1748501
    invoke-virtual {p0}, LX/B8A;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1748502
    iget-object v1, p0, LX/B8A;->e:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v1, v2, v2, v0, v2}, Landroid/widget/AutoCompleteTextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 1748503
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1748494
    iget-object v0, p0, LX/B8A;->e:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0, v2}, Landroid/widget/AutoCompleteTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1748495
    iget-object v0, p0, LX/B8A;->e:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0, v2}, Landroid/widget/AutoCompleteTextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 1748496
    iget-object v0, p0, LX/B8A;->e:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0, v2}, Landroid/widget/AutoCompleteTextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 1748497
    iget-object v0, p0, LX/B8A;->e:Landroid/widget/AutoCompleteTextView;

    iget-object v1, p0, LX/B8A;->k:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1748498
    iget-object v0, p0, LX/B8A;->e:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0, v2}, Landroid/widget/AutoCompleteTextView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1748499
    iget-object v0, p0, LX/B8A;->f:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1748500
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;LX/B7F;I)V
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 1748534
    iput-object p1, p0, LX/B8A;->g:Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;

    .line 1748535
    iput-object p2, p0, LX/B8A;->n:LX/B7F;

    .line 1748536
    iget-object v0, p0, LX/B8A;->i:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, LX/B8A;->g:Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->p()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1748537
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1748538
    const/4 v0, 0x0

    .line 1748539
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->r()LX/0Px;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->r()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1748540
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->r()LX/0Px;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/B7F;->a(LX/0Px;)LX/0Px;

    move-result-object v1

    .line 1748541
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->r()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move-object v6, v0

    move-object v0, v1

    move-object v1, v6

    .line 1748542
    :goto_0
    new-instance v2, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, LX/B8A;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x1090011

    invoke-direct {v2, v3, v4, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 1748543
    iget-object v3, p0, LX/B8A;->e:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v3, v2}, Landroid/widget/AutoCompleteTextView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1748544
    iget-object v2, p0, LX/B8A;->e:Landroid/widget/AutoCompleteTextView;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Landroid/widget/AutoCompleteTextView;->setInputType(I)V

    .line 1748545
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1748546
    iget-object v2, p0, LX/B8A;->e:Landroid/widget/AutoCompleteTextView;

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v2, v0}, Landroid/widget/AutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1748547
    :cond_0
    invoke-virtual {p2, v1}, LX/B7F;->a(Ljava/lang/String;)LX/7Tl;

    move-result-object v0

    invoke-static {p0, v0}, LX/B8A;->setAndFormatPhoneNumber(LX/B8A;LX/7Tl;)V

    .line 1748548
    invoke-static {p0}, LX/B8A;->f(LX/B8A;)V

    .line 1748549
    iget-object v0, p0, LX/B8A;->e:Landroid/widget/AutoCompleteTextView;

    new-instance v1, LX/B85;

    invoke-direct {v1, p0}, LX/B85;-><init>(LX/B8A;)V

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 1748550
    iget-object v0, p0, LX/B8A;->e:Landroid/widget/AutoCompleteTextView;

    new-instance v1, LX/B86;

    invoke-direct {v1, p0}, LX/B86;-><init>(LX/B8A;)V

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 1748551
    iget-object v0, p0, LX/B8A;->e:Landroid/widget/AutoCompleteTextView;

    new-instance v1, LX/B87;

    invoke-direct {v1, p0}, LX/B87;-><init>(LX/B8A;)V

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1748552
    iget-object v0, p0, LX/B8A;->f:Lcom/facebook/resources/ui/FbButton;

    new-instance v1, LX/B89;

    invoke-direct {v1, p0}, LX/B89;-><init>(LX/B8A;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1748553
    return-void

    :cond_1
    move-object v6, v0

    move-object v0, v1

    move-object v1, v6

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1748491
    const v0, 0x7f020dec

    invoke-static {p0, v0}, LX/B8A;->setIconDrawable(LX/B8A;I)V

    .line 1748492
    iget-object v0, p0, LX/B8A;->j:Landroid/widget/TextView;

    invoke-static {v0, p1}, LX/B8v;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 1748493
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1748488
    iget-object v0, p0, LX/B8A;->e:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->requestFocus()Z

    .line 1748489
    iget-object v0, p0, LX/B8A;->e:Landroid/widget/AutoCompleteTextView;

    iget-object v1, p0, LX/B8A;->j:Landroid/widget/TextView;

    invoke-static {v0, v1}, LX/B8v;->a(Landroid/view/View;Landroid/widget/TextView;)V

    .line 1748490
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1748485
    invoke-static {p0}, LX/B8A;->f(LX/B8A;)V

    .line 1748486
    iget-object v0, p0, LX/B8A;->j:Landroid/widget/TextView;

    invoke-static {v0}, LX/B8v;->a(Landroid/widget/TextView;)V

    .line 1748487
    return-void
.end method

.method public getBoundedInfoFieldData()Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;
    .locals 1

    .prologue
    .line 1748484
    iget-object v0, p0, LX/B8A;->g:Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;

    return-object v0
.end method

.method public getInputCustomToken()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1748470
    invoke-virtual {p0}, LX/B8A;->getInputValue()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getInputValue()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1748483
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, LX/B8A;->m:LX/7Tl;

    iget-object v1, v1, LX/7Tl;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/B8A;->e:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v1}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setInputValue(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1748473
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1748474
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1748475
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1748476
    iget-object v1, p0, LX/B8A;->n:LX/B7F;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/B7F;->a(LX/0Px;)LX/0Px;

    move-result-object v0

    .line 1748477
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1748478
    iget-object v1, p0, LX/B8A;->e:Landroid/widget/AutoCompleteTextView;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Landroid/widget/AutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1748479
    iget-object v0, p0, LX/B8A;->n:LX/B7F;

    invoke-virtual {v0, p1}, LX/B7F;->a(Ljava/lang/String;)LX/7Tl;

    move-result-object v0

    invoke-static {p0, v0}, LX/B8A;->setAndFormatPhoneNumber(LX/B8A;LX/7Tl;)V

    .line 1748480
    :cond_0
    iget-object v0, p0, LX/B8A;->e:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->clearFocus()V

    .line 1748481
    iget-object v0, p0, LX/B8A;->f:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbButton;->clearFocus()V

    .line 1748482
    return-void
.end method

.method public setOnDataChangeListener(LX/B7w;)V
    .locals 0

    .prologue
    .line 1748471
    iput-object p1, p0, LX/B8A;->h:LX/B7w;

    .line 1748472
    return-void
.end method
