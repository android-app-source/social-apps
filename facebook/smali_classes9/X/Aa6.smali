.class public LX/Aa6;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1687889
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/AVT;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1687890
    if-eqz p3, :cond_0

    .line 1687891
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 1687892
    const-string v0, "object_id"

    invoke-interface {v5, v0, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1687893
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "creative_tool_applied_main_"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v0, p0

    move-object v2, p2

    move-object v4, v1

    invoke-virtual/range {v0 .. v5}, LX/AVT;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/video/videostreaming/LiveStreamingError;Ljava/util/Map;)V

    .line 1687894
    return-void

    :cond_0
    move-object v5, v1

    goto :goto_0
.end method

.method public static a(LX/AVT;ZLjava/lang/String;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1687895
    const-string v2, "main"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v0, "creative_tool_did_"

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-eqz p1, :cond_0

    const-string v0, "show"

    :goto_0
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "_"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v0, p0

    move-object v4, v1

    move-object v5, v1

    invoke-virtual/range {v0 .. v5}, LX/AVT;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/video/videostreaming/LiveStreamingError;Ljava/util/Map;)V

    .line 1687896
    return-void

    .line 1687897
    :cond_0
    const-string v0, "hide"

    goto :goto_0
.end method

.method public static b(LX/AVT;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1687898
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "creative_tool_toolbar_pressed_"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v0, p0

    move-object v2, p2

    move-object v4, v1

    move-object v5, v1

    invoke-virtual/range {v0 .. v5}, LX/AVT;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/video/videostreaming/LiveStreamingError;Ljava/util/Map;)V

    .line 1687899
    return-void
.end method

.method public static c(LX/AVT;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1687900
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, LX/Aa6;->a(LX/AVT;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1687901
    return-void
.end method
