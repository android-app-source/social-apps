.class public LX/AbS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:LX/AVT;

.field public final c:LX/03V;

.field public d:Landroid/widget/TextView;

.field public e:Lcom/facebook/facecast/restriction/FacecastAudienceDialogFragment;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/facecast/restriction/AudienceRestrictionData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:LX/AYP;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1690780
    const-class v0, LX/AbS;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/AbS;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/AVT;LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1690795
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1690796
    iput-object p1, p0, LX/AbS;->b:LX/AVT;

    .line 1690797
    iput-object p2, p0, LX/AbS;->c:LX/03V;

    .line 1690798
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x25b3cabd

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1690781
    iget-object v0, p0, LX/AbS;->d:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v2, Landroid/support/v4/app/FragmentActivity;

    invoke-static {v0, v2}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    .line 1690782
    if-nez v0, :cond_0

    .line 1690783
    const v0, 0x2a650e07

    invoke-static {v3, v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1690784
    :goto_0
    return-void

    .line 1690785
    :cond_0
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v2

    .line 1690786
    const-string v3, "AUDIENCE_RESTRICTION_FRAGMENT_TAG"

    invoke-virtual {v2, v3}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 1690787
    const v0, 0x5e4814f7

    invoke-static {v0, v1}, LX/02F;->a(II)V

    goto :goto_0

    .line 1690788
    :cond_1
    iget-object v2, p0, LX/AbS;->e:Lcom/facebook/facecast/restriction/FacecastAudienceDialogFragment;

    if-nez v2, :cond_2

    .line 1690789
    new-instance v2, Lcom/facebook/facecast/restriction/FacecastAudienceDialogFragment;

    invoke-direct {v2}, Lcom/facebook/facecast/restriction/FacecastAudienceDialogFragment;-><init>()V

    iput-object v2, p0, LX/AbS;->e:Lcom/facebook/facecast/restriction/FacecastAudienceDialogFragment;

    .line 1690790
    iget-object v2, p0, LX/AbS;->e:Lcom/facebook/facecast/restriction/FacecastAudienceDialogFragment;

    .line 1690791
    iput-object p0, v2, Lcom/facebook/facecast/restriction/FacecastAudienceDialogFragment;->u:LX/AbS;

    .line 1690792
    :cond_2
    iget-object v2, p0, LX/AbS;->e:Lcom/facebook/facecast/restriction/FacecastAudienceDialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const-string v3, "AUDIENCE_RESTRICTION_FRAGMENT_TAG"

    invoke-virtual {v2, v0, v3}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 1690793
    iget-object v0, p0, LX/AbS;->b:LX/AVT;

    const-string v2, "geotargeting_settings_opened"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, LX/AVT;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1690794
    const v0, 0x96fb6a0

    invoke-static {v0, v1}, LX/02F;->a(II)V

    goto :goto_0
.end method
