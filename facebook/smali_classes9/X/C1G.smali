.class public LX/C1G;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1842221
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(ZLcom/facebook/feed/rows/core/props/FeedProps;LX/C17;)LX/1X6;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;",
            "Lcom/facebook/feedplugins/attachments/poll/PollDisplayAndClickListenerBuilder;",
            ")",
            "LX/1X6;"
        }
    .end annotation

    .prologue
    .line 1842222
    if-eqz p0, :cond_0

    const/high16 v0, 0x41f80000    # 31.0f

    :goto_0
    move v0, v0

    .line 1842223
    if-eqz p0, :cond_1

    const/high16 v1, 0x42820000    # 65.0f

    :goto_1
    move v1, v1

    .line 1842224
    const/high16 v2, -0x40400000    # -1.5f

    move v2, v2

    .line 1842225
    invoke-static {}, LX/1UY;->a()LX/1UY;

    move-result-object v3

    .line 1842226
    iput v2, v3, LX/1UY;->b:F

    .line 1842227
    move-object v2, v3

    .line 1842228
    iput v1, v2, LX/1UY;->e:F

    .line 1842229
    move-object v1, v2

    .line 1842230
    iput v0, v1, LX/1UY;->d:F

    .line 1842231
    move-object v0, v1

    .line 1842232
    invoke-virtual {v0}, LX/1UY;->i()LX/1Ua;

    move-result-object v0

    .line 1842233
    new-instance v1, LX/1X6;

    const v2, 0x7f0214c8

    const/4 v3, -0x1

    invoke-direct {v1, p1, v0, v2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;II)V

    return-object v1

    :cond_0
    const/high16 v0, 0x40400000    # 3.0f

    goto :goto_0

    :cond_1
    const/high16 v1, 0x42b90000    # 92.5f

    goto :goto_1
.end method
