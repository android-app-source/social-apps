.class public final LX/An3;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1711753
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLActor;)Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel;
    .locals 12

    .prologue
    const/4 v2, 0x0

    .line 1711754
    if-nez p0, :cond_1

    .line 1711755
    :cond_0
    :goto_0
    return-object v2

    .line 1711756
    :cond_1
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1711757
    const/4 v1, 0x0

    .line 1711758
    if-nez p0, :cond_3

    .line 1711759
    :goto_1
    move v1, v1

    .line 1711760
    if-eqz v1, :cond_0

    .line 1711761
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1711762
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1711763
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1711764
    new-instance v0, LX/15i;

    const/4 v4, 0x1

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1711765
    instance-of v1, p0, Lcom/facebook/flatbuffers/Flattenable;

    if-eqz v1, :cond_2

    .line 1711766
    const-string v1, "PermalinkProfileListConverter.getPermalinkProfileListFragmentGraphQL"

    invoke-virtual {v0, v1, p0}, LX/15i;->a(Ljava/lang/String;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1711767
    :cond_2
    new-instance v2, Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel;

    invoke-direct {v2, v0}, Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel;-><init>(LX/15i;)V

    goto :goto_0

    .line 1711768
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    .line 1711769
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->E()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v4

    .line 1711770
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1711771
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aa()Lcom/facebook/graphql/model/GraphQLMutualFriendsConnection;

    move-result-object v6

    const/4 v7, 0x0

    .line 1711772
    if-nez v6, :cond_4

    .line 1711773
    :goto_2
    move v6, v7

    .line 1711774
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1711775
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v8

    const/4 v9, 0x0

    .line 1711776
    if-nez v8, :cond_5

    .line 1711777
    :goto_3
    move v8, v9

    .line 1711778
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->ba()Lcom/facebook/graphql/model/GraphQLUnseenStoriesConnection;

    move-result-object v9

    const/4 v10, 0x0

    .line 1711779
    if-nez v9, :cond_6

    .line 1711780
    :goto_4
    move v9, v10

    .line 1711781
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->az()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 1711782
    const/16 v11, 0x9

    invoke-virtual {v0, v11}, LX/186;->c(I)V

    .line 1711783
    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 1711784
    const/4 v3, 0x1

    invoke-virtual {v0, v3, v4}, LX/186;->b(II)V

    .line 1711785
    const/4 v3, 0x2

    invoke-virtual {v0, v3, v5}, LX/186;->b(II)V

    .line 1711786
    const/4 v3, 0x3

    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 1711787
    const/4 v3, 0x4

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 1711788
    const/4 v3, 0x5

    invoke-virtual {v0, v3, v8}, LX/186;->b(II)V

    .line 1711789
    const/4 v3, 0x6

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->ay()I

    move-result v4

    invoke-virtual {v0, v3, v4, v1}, LX/186;->a(III)V

    .line 1711790
    const/4 v1, 0x7

    invoke-virtual {v0, v1, v9}, LX/186;->b(II)V

    .line 1711791
    const/16 v1, 0x8

    invoke-virtual {v0, v1, v10}, LX/186;->b(II)V

    .line 1711792
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 1711793
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    goto/16 :goto_1

    .line 1711794
    :cond_4
    const/4 v8, 0x1

    invoke-virtual {v0, v8}, LX/186;->c(I)V

    .line 1711795
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLMutualFriendsConnection;->a()I

    move-result v8

    invoke-virtual {v0, v7, v8, v7}, LX/186;->a(III)V

    .line 1711796
    invoke-virtual {v0}, LX/186;->d()I

    move-result v7

    .line 1711797
    invoke-virtual {v0, v7}, LX/186;->d(I)V

    goto :goto_2

    .line 1711798
    :cond_5
    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 1711799
    const/4 v11, 0x3

    invoke-virtual {v0, v11}, LX/186;->c(I)V

    .line 1711800
    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v11

    invoke-virtual {v0, v9, v11, v9}, LX/186;->a(III)V

    .line 1711801
    const/4 v11, 0x1

    invoke-virtual {v0, v11, v10}, LX/186;->b(II)V

    .line 1711802
    const/4 v10, 0x2

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v11

    invoke-virtual {v0, v10, v11, v9}, LX/186;->a(III)V

    .line 1711803
    invoke-virtual {v0}, LX/186;->d()I

    move-result v9

    .line 1711804
    invoke-virtual {v0, v9}, LX/186;->d(I)V

    goto :goto_3

    .line 1711805
    :cond_6
    const/4 v11, 0x1

    invoke-virtual {v0, v11}, LX/186;->c(I)V

    .line 1711806
    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLUnseenStoriesConnection;->j()I

    move-result v11

    invoke-virtual {v0, v10, v11, v10}, LX/186;->a(III)V

    .line 1711807
    invoke-virtual {v0}, LX/186;->d()I

    move-result v10

    .line 1711808
    invoke-virtual {v0, v10}, LX/186;->d(I)V

    goto/16 :goto_4
.end method
