.class public final LX/AyT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/AlW;


# annotations
.annotation build Lcom/google/common/annotations/VisibleForTesting;
.end annotation


# instance fields
.field public final a:Lcom/facebook/friendsharing/souvenirs/prompt/SouvenirPromptAttachment;

.field public final b:Landroid/net/Uri;

.field public final c:Ljava/lang/String;

.field public final d:Landroid/text/SpannableStringBuilder;

.field public final synthetic e:LX/1RO;


# direct methods
.method public constructor <init>(LX/1RO;LX/Ayb;)V
    .locals 4

    .prologue
    .line 1730090
    iput-object p1, p0, LX/AyT;->e:LX/1RO;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1730091
    new-instance v0, Lcom/facebook/friendsharing/souvenirs/prompt/SouvenirPromptAttachment;

    iget-object v1, p1, LX/1RO;->d:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/facebook/friendsharing/souvenirs/prompt/SouvenirPromptAttachment;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/AyT;->a:Lcom/facebook/friendsharing/souvenirs/prompt/SouvenirPromptAttachment;

    .line 1730092
    invoke-virtual {p2}, LX/Ayb;->e()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, LX/AyT;->b:Landroid/net/Uri;

    .line 1730093
    iget-object v0, p1, LX/1RO;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AyS;

    invoke-virtual {v0, p2}, LX/AyS;->a(LX/Ayb;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/AyT;->c:Ljava/lang/String;

    .line 1730094
    iget-object v0, p1, LX/1RO;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AyS;

    iget-object v1, p1, LX/1RO;->d:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p1, LX/1RO;->a:Landroid/content/res/Resources;

    const v3, 0x7f082766

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/AyS;->a(Landroid/content/res/Resources;Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    iput-object v0, p0, LX/AyT;->d:Landroid/text/SpannableStringBuilder;

    .line 1730095
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1730102
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1730101
    iget-object v0, p0, LX/AyT;->e:LX/1RO;

    iget-object v0, v0, LX/1RO;->a:Landroid/content/res/Resources;

    const v1, 0x7f082765

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/Integer;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1730100
    iget-object v0, p0, LX/AyT;->e:LX/1RO;

    iget-object v0, v0, LX/1RO;->a:Landroid/content/res/Resources;

    const v1, 0x7f0a00fa

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final d()Landroid/graphics/drawable/Drawable;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1730103
    iget-object v0, p0, LX/AyT;->e:LX/1RO;

    iget-object v0, v0, LX/1RO;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0214dd

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public final e()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 1730099
    const/4 v0, 0x0

    return-object v0
.end method

.method public final f()Landroid/net/Uri;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1730098
    iget-object v0, p0, LX/AyT;->e:LX/1RO;

    iget-object v0, v0, LX/1RO;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    const v0, 0x7f021877

    invoke-static {v0}, LX/1bQ;->a(I)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public final g()LX/AkM;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1730097
    iget-object v0, p0, LX/AyT;->a:Lcom/facebook/friendsharing/souvenirs/prompt/SouvenirPromptAttachment;

    return-object v0
.end method

.method public final h()Lcom/facebook/productionprompts/model/PromptDisplayReason;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1730096
    const/4 v0, 0x0

    return-object v0
.end method
