.class public LX/At9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/AsV;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "Lcom/facebook/friendsharing/inspiration/model/CameraStateSpec$ProvidesCameraState;",
        "DerivedData:",
        "Ljava/lang/Object;",
        "Mutation::",
        "Lcom/facebook/ipc/composer/dataaccessor/ComposerCanSave;",
        ":",
        "Lcom/facebook/friendsharing/inspiration/model/CameraStateSpec$SetsCameraState",
        "<TMutation;>;Services::",
        "LX/0il",
        "<TModelData;>;:",
        "LX/0ik",
        "<TDerivedData;>;:",
        "LX/0im",
        "<TMutation;>;>",
        "Ljava/lang/Object;",
        "LX/AsV;"
    }
.end annotation


# static fields
.field public static final a:LX/0jK;


# instance fields
.field private final b:LX/AsT;

.field public final c:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TServices;>;"
        }
    .end annotation
.end field

.field public final d:LX/ArL;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1721193
    const-class v0, LX/At9;

    invoke-static {v0}, LX/0jK;->a(Ljava/lang/Class;)LX/0jK;

    move-result-object v0

    sput-object v0, LX/At9;->a:LX/0jK;

    return-void
.end method

.method public constructor <init>(LX/0il;LX/ArL;)V
    .locals 1
    .param p1    # LX/0il;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/ArL;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TServices;",
            "LX/ArL;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1721197
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1721198
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/At9;->c:Ljava/lang/ref/WeakReference;

    .line 1721199
    iput-object p2, p0, LX/At9;->d:LX/ArL;

    .line 1721200
    new-instance v0, LX/At8;

    invoke-direct {v0, p0}, LX/At8;-><init>(LX/At9;)V

    iput-object v0, p0, LX/At9;->b:LX/AsT;

    .line 1721201
    return-void
.end method


# virtual methods
.method public final a()LX/AsT;
    .locals 1

    .prologue
    .line 1721196
    iget-object v0, p0, LX/At9;->b:LX/AsT;

    return-object v0
.end method

.method public final a(Lcom/facebook/drawee/fbpipeline/FbDraweeView;)V
    .locals 1

    .prologue
    .line 1721194
    const v0, 0x7f02154d

    invoke-virtual {p1, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setImageResource(I)V

    .line 1721195
    return-void
.end method
