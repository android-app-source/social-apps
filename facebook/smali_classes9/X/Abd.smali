.class public LX/Abd;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/Abd;


# instance fields
.field private final a:LX/0SG;

.field private b:LX/BJ2;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0SG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1691096
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1691097
    iput-object p1, p0, LX/Abd;->a:LX/0SG;

    .line 1691098
    return-void
.end method

.method public static a(LX/0QB;)LX/Abd;
    .locals 4

    .prologue
    .line 1691068
    sget-object v0, LX/Abd;->c:LX/Abd;

    if-nez v0, :cond_1

    .line 1691069
    const-class v1, LX/Abd;

    monitor-enter v1

    .line 1691070
    :try_start_0
    sget-object v0, LX/Abd;->c:LX/Abd;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1691071
    if-eqz v2, :cond_0

    .line 1691072
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1691073
    new-instance p0, LX/Abd;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v3

    check-cast v3, LX/0SG;

    invoke-direct {p0, v3}, LX/Abd;-><init>(LX/0SG;)V

    .line 1691074
    move-object v0, p0

    .line 1691075
    sput-object v0, LX/Abd;->c:LX/Abd;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1691076
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1691077
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1691078
    :cond_1
    sget-object v0, LX/Abd;->c:LX/Abd;

    return-object v0

    .line 1691079
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1691080
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Ljava/lang/Object;)Landroid/animation/Animator;
    .locals 5

    .prologue
    const/4 v4, 0x2

    .line 1691081
    const-string v0, "alpha"

    new-array v1, v4, [F

    fill-array-data v1, :array_0

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 1691082
    const-wide/16 v2, 0x384

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 1691083
    new-instance v1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1691084
    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setRepeatCount(I)V

    .line 1691085
    invoke-virtual {v0, v4}, Landroid/animation/ObjectAnimator;->setRepeatMode(I)V

    .line 1691086
    return-object v0

    nop

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x3e4ccccd    # 0.2f
    .end array-data
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;
    .locals 1
    .param p0    # Lcom/facebook/graphql/model/GraphQLStoryAttachment;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 1691036
    if-nez p0, :cond_0

    .line 1691037
    const/4 v0, 0x0

    .line 1691038
    :goto_0
    return-object v0

    :cond_0
    const v0, 0x602953b6

    invoke-static {p0, v0}, LX/1VO;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;
    .locals 2
    .param p0    # Lcom/facebook/graphql/model/GraphQLStory;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1691087
    if-nez p0, :cond_1

    .line 1691088
    :cond_0
    :goto_0
    return-object v0

    .line 1691089
    :cond_1
    invoke-static {p0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    .line 1691090
    if-eqz v1, :cond_0

    .line 1691091
    invoke-static {v1}, LX/Abd;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;

    move-result-object v1

    .line 1691092
    if-eqz v1, :cond_0

    .line 1691093
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->R()Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;)Z
    .locals 2
    .param p0    # Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 1691094
    if-nez p0, :cond_1

    .line 1691095
    :cond_0
    :goto_0
    return v0

    :cond_1
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->SCHEDULED_CANCELED:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-eq p0, v1, :cond_2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->SCHEDULED_EXPIRED:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-eq p0, v1, :cond_2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->SCHEDULED_LIVE:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-eq p0, v1, :cond_2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->SCHEDULED_PREVIEW:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-ne p0, v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLVideo;)Z
    .locals 1
    .param p0    # Lcom/facebook/graphql/model/GraphQLVideo;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1691064
    if-nez p0, :cond_0

    .line 1691065
    const/4 v0, 0x0

    .line 1691066
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideo;->r()Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-result-object v0

    invoke-static {v0}, LX/Abd;->a(Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;)Z

    move-result v0

    goto :goto_0
.end method

.method public static d(Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 1691067
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->A()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->A()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->A()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static e(Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 1691063
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->z()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->z()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->z()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static g(Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;)J
    .locals 4

    .prologue
    .line 1691062
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->n()J

    move-result-wide v0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->D()J

    move-result-wide v2

    add-long/2addr v0, v2

    return-wide v0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;)Z
    .locals 6

    .prologue
    .line 1691060
    iget-object v0, p0, LX/Abd;->a:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    .line 1691061
    invoke-static {p1}, LX/Abd;->g(Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;)J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->m()J

    move-result-wide v4

    sub-long/2addr v2, v4

    cmp-long v2, v2, v0

    if-gtz v2, :cond_0

    invoke-static {p1}, LX/Abd;->g(Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;)J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->j()J

    move-result-wide v4

    add-long/2addr v2, v4

    cmp-long v0, v2, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 5
    .param p1    # Lcom/facebook/graphql/model/GraphQLStory;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 1691054
    if-nez p1, :cond_1

    .line 1691055
    :cond_0
    :goto_0
    return v0

    .line 1691056
    :cond_1
    invoke-static {p1}, LX/17E;->v(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    .line 1691057
    if-eqz v1, :cond_0

    .line 1691058
    invoke-static {p1}, LX/Abd;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    move-result-object v2

    .line 1691059
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->s()Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-result-object v3

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->SCHEDULED_PREVIEW:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-eq v3, v4, :cond_2

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->s()Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-result-object v1

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->SCHEDULED_LIVE:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-ne v1, v3, :cond_0

    :cond_2
    invoke-virtual {p0, v2}, LX/Abd;->a(Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b(Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;)Z
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 1691050
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->s()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1691051
    :cond_0
    :goto_0
    return v0

    .line 1691052
    :cond_1
    iget-object v1, p0, LX/Abd;->a:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    .line 1691053
    invoke-static {p1}, LX/Abd;->g(Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;)J

    move-result-wide v4

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->m()J

    move-result-wide v6

    sub-long/2addr v4, v6

    cmp-long v1, v4, v2

    if-gtz v1, :cond_0

    invoke-static {p1}, LX/Abd;->g(Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;)J

    move-result-wide v4

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->w()J

    move-result-wide v6

    add-long/2addr v4, v6

    cmp-long v1, v4, v2

    if-lez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final c(Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;)J
    .locals 6

    .prologue
    .line 1691049
    const-wide/16 v0, 0x0

    invoke-static {p1}, LX/Abd;->g(Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;)J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    iget-object v4, p0, LX/Abd;->a:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v4

    sub-long/2addr v2, v4

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public final f(Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;)LX/1bf;
    .locals 2
    .param p1    # Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 1691039
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->y()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->y()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1691040
    :cond_0
    const/4 v0, 0x0

    .line 1691041
    :goto_0
    return-object v0

    .line 1691042
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->y()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    .line 1691043
    iget-object v1, p0, LX/Abd;->b:LX/BJ2;

    if-nez v1, :cond_2

    .line 1691044
    new-instance v1, LX/BJ2;

    invoke-direct {v1}, LX/BJ2;-><init>()V

    iput-object v1, p0, LX/Abd;->b:LX/BJ2;

    .line 1691045
    :cond_2
    if-nez v0, :cond_3

    sget-object v0, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    :goto_1
    invoke-static {v0}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v0

    iget-object v1, p0, LX/Abd;->b:LX/BJ2;

    .line 1691046
    iput-object v1, v0, LX/1bX;->j:LX/33B;

    .line 1691047
    move-object v0, v0

    .line 1691048
    invoke-virtual {v0}, LX/1bX;->n()LX/1bf;

    move-result-object v0

    goto :goto_0

    :cond_3
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_1
.end method
