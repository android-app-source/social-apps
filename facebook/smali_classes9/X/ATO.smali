.class public LX/ATO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0iK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "LX/0io;",
        ":",
        "LX/0iv;",
        ":",
        "LX/0j0;",
        ":",
        "LX/0j3;",
        ":",
        "LX/0j4;",
        ":",
        "LX/0jA;",
        ":",
        "LX/0j6;",
        "DerivedData::",
        "LX/5Qw;",
        ":",
        "LX/5Qz;",
        ":",
        "LX/5R0;",
        "Services::",
        "LX/0il",
        "<TModelData;>;:",
        "LX/0ik",
        "<TDerivedData;>;>",
        "Ljava/lang/Object;",
        "LX/0iK",
        "<TModelData;TDerivedData;>;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# static fields
.field public static final d:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final e:LX/0wT;


# instance fields
.field private A:LX/9c9;

.field public B:LX/ASe;

.field public C:LX/7yZ;

.field private D:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public E:Z

.field private F:I

.field public G:LX/0wd;

.field public final H:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public I:I

.field private J:Z

.field private K:Z

.field private L:Z

.field public M:LX/Hqr;

.field public N:Ljava/lang/String;

.field public O:I

.field public P:I

.field public Q:Z

.field public R:Z

.field public S:Z

.field public T:D

.field public U:I

.field public V:Z

.field public W:I

.field private X:Z

.field private Y:Z

.field public Z:Z

.field public a:Ljava/util/List;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public final aa:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TServices;>;"
        }
    .end annotation
.end field

.field private ab:LX/ATk;

.field private ac:LX/AT3;

.field private ad:LX/ATt;

.field private ae:LX/ASs;

.field private af:LX/ASo;

.field private ag:LX/ATA;

.field public final ah:Lcom/facebook/facerec/manager/FaceBoxPrioritizer;

.field public final ai:LX/75F;

.field private final aj:LX/75Q;

.field public final ak:LX/1EZ;

.field public final al:LX/9iZ;

.field private final am:LX/ASb;

.field private final an:LX/3iT;

.field private final ao:LX/BTg;

.field public b:Ljava/util/List;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/ASn;",
            ">;"
        }
    .end annotation
.end field

.field public c:Ljava/util/List;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/ATE;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Z

.field public final g:Landroid/content/Context;

.field public final h:Landroid/view/LayoutInflater;

.field private final i:LX/ATL;

.field private final j:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final k:LX/ASg;

.field public final l:LX/ATQ;

.field public final m:LX/7kn;

.field private final n:LX/03V;

.field public final o:LX/74n;

.field private final p:LX/9bz;

.field private final q:LX/9cA;

.field public r:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;",
            ">;"
        }
    .end annotation
.end field

.field private final s:LX/8GT;

.field private final t:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

.field private final u:LX/ATG;

.field private v:LX/0gc;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:Lcom/facebook/widget/ScrollingAwareScrollView;

.field public x:Landroid/widget/FrameLayout;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public y:Landroid/widget/LinearLayout;

.field public z:LX/ATl;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 1675643
    const-class v0, LX/ATO;

    sput-object v0, LX/ATO;->d:Ljava/lang/Class;

    .line 1675644
    const-wide/high16 v0, 0x405e000000000000L    # 120.0

    const-wide/high16 v2, 0x4028000000000000L    # 12.0

    invoke-static {v0, v1, v2, v3}, LX/0wT;->a(DD)LX/0wT;

    move-result-object v0

    sput-object v0, LX/ATO;->e:LX/0wT;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/LayoutInflater;LX/1Ck;LX/ASg;LX/ATQ;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0wW;LX/7kn;LX/0Or;LX/03V;LX/9cA;LX/74n;LX/8GT;LX/ATk;LX/AT3;LX/ATt;LX/ASs;LX/ASo;LX/ATA;Lcom/facebook/facerec/manager/FaceBoxPrioritizer;LX/75F;LX/75Q;LX/1EZ;LX/9iZ;LX/ASb;LX/3iT;LX/BTg;LX/0gc;Lcom/facebook/widget/ScrollingAwareScrollView;Landroid/widget/LinearLayout;ZZLX/0il;Z)V
    .locals 7
    .param p9    # LX/0Or;
        .annotation runtime Lcom/facebook/photos/annotation/MaxNumberPhotosPerUpload;
        .end annotation
    .end param
    .param p28    # LX/0gc;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p29    # Lcom/facebook/widget/ScrollingAwareScrollView;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p30    # Landroid/widget/LinearLayout;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p31    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p32    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p33    # LX/0il;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p34    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/view/LayoutInflater;",
            "LX/1Ck;",
            "LX/ASg;",
            "LX/ATQ;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0wW;",
            "LX/7kn;",
            "LX/0Or",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/9cA;",
            "LX/74n;",
            "LX/8GT;",
            "LX/ATk;",
            "LX/AT3;",
            "LX/ATt;",
            "LX/ASs;",
            "LX/ASo;",
            "LX/ATA;",
            "Lcom/facebook/facerec/manager/FaceBoxPrioritizer;",
            "LX/75F;",
            "LX/75Q;",
            "LX/1EZ;",
            "LX/9iZ;",
            "LX/ASb;",
            "LX/3iT;",
            "LX/BTg;",
            "LX/0gc;",
            "Lcom/facebook/widget/ScrollingAwareScrollView;",
            "Landroid/widget/LinearLayout;",
            "ZZTServices;Z)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1675567
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1675568
    new-instance v2, LX/ATM;

    invoke-direct {v2, p0}, LX/ATM;-><init>(LX/ATO;)V

    iput-object v2, p0, LX/ATO;->p:LX/9bz;

    .line 1675569
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iput-object v2, p0, LX/ATO;->r:Ljava/util/Map;

    .line 1675570
    new-instance v2, LX/ATF;

    invoke-direct {v2, p0}, LX/ATF;-><init>(LX/ATO;)V

    iput-object v2, p0, LX/ATO;->t:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 1675571
    new-instance v2, LX/ATG;

    invoke-direct {v2, p0}, LX/ATG;-><init>(LX/ATO;)V

    iput-object v2, p0, LX/ATO;->u:LX/ATG;

    .line 1675572
    const/4 v2, 0x1

    iput-boolean v2, p0, LX/ATO;->K:Z

    .line 1675573
    const/4 v2, 0x0

    iput-boolean v2, p0, LX/ATO;->L:Z

    .line 1675574
    const/4 v2, 0x0

    iput v2, p0, LX/ATO;->O:I

    .line 1675575
    const/4 v2, 0x0

    iput-boolean v2, p0, LX/ATO;->Q:Z

    .line 1675576
    const/4 v2, 0x1

    iput-boolean v2, p0, LX/ATO;->R:Z

    .line 1675577
    const/4 v2, 0x0

    iput-boolean v2, p0, LX/ATO;->S:Z

    .line 1675578
    const-wide/16 v2, 0x0

    iput-wide v2, p0, LX/ATO;->T:D

    .line 1675579
    const/4 v2, 0x0

    iput v2, p0, LX/ATO;->U:I

    .line 1675580
    const/4 v2, 0x0

    iput-boolean v2, p0, LX/ATO;->V:Z

    .line 1675581
    const/4 v2, 0x0

    iput v2, p0, LX/ATO;->W:I

    .line 1675582
    iput-object p1, p0, LX/ATO;->g:Landroid/content/Context;

    .line 1675583
    move-object/from16 v0, p14

    iput-object v0, p0, LX/ATO;->ab:LX/ATk;

    .line 1675584
    move-object/from16 v0, p15

    iput-object v0, p0, LX/ATO;->ac:LX/AT3;

    .line 1675585
    move-object/from16 v0, p16

    iput-object v0, p0, LX/ATO;->ad:LX/ATt;

    .line 1675586
    move-object/from16 v0, p17

    iput-object v0, p0, LX/ATO;->ae:LX/ASs;

    .line 1675587
    move-object/from16 v0, p18

    iput-object v0, p0, LX/ATO;->af:LX/ASo;

    .line 1675588
    move-object/from16 v0, p20

    iput-object v0, p0, LX/ATO;->ah:Lcom/facebook/facerec/manager/FaceBoxPrioritizer;

    .line 1675589
    move-object/from16 v0, p21

    iput-object v0, p0, LX/ATO;->ai:LX/75F;

    .line 1675590
    move-object/from16 v0, p22

    iput-object v0, p0, LX/ATO;->aj:LX/75Q;

    .line 1675591
    move-object/from16 v0, p23

    iput-object v0, p0, LX/ATO;->ak:LX/1EZ;

    .line 1675592
    move-object/from16 v0, p24

    iput-object v0, p0, LX/ATO;->al:LX/9iZ;

    .line 1675593
    move-object/from16 v0, p25

    iput-object v0, p0, LX/ATO;->am:LX/ASb;

    .line 1675594
    move-object/from16 v0, p26

    iput-object v0, p0, LX/ATO;->an:LX/3iT;

    .line 1675595
    move-object/from16 v0, p19

    iput-object v0, p0, LX/ATO;->ag:LX/ATA;

    .line 1675596
    iput-object p2, p0, LX/ATO;->h:Landroid/view/LayoutInflater;

    .line 1675597
    iput-object p3, p0, LX/ATO;->j:LX/1Ck;

    .line 1675598
    iput-object p4, p0, LX/ATO;->k:LX/ASg;

    .line 1675599
    iput-object p5, p0, LX/ATO;->l:LX/ATQ;

    .line 1675600
    iput-object p6, p0, LX/ATO;->D:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1675601
    move-object/from16 v0, p13

    iput-object v0, p0, LX/ATO;->s:LX/8GT;

    .line 1675602
    invoke-virtual {p7}, LX/0wW;->a()LX/0wd;

    move-result-object v2

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v2, v4, v5}, LX/0wd;->a(D)LX/0wd;

    move-result-object v2

    invoke-virtual {v2}, LX/0wd;->j()LX/0wd;

    move-result-object v2

    sget-object v3, LX/ATO;->e:LX/0wT;

    invoke-virtual {v2, v3}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v2

    new-instance v3, LX/ATN;

    invoke-direct {v3, p0}, LX/ATN;-><init>(LX/ATO;)V

    invoke-virtual {v2, v3}, LX/0wd;->a(LX/0xi;)LX/0wd;

    move-result-object v2

    iput-object v2, p0, LX/ATO;->G:LX/0wd;

    .line 1675603
    move-object/from16 v0, p9

    iput-object v0, p0, LX/ATO;->H:LX/0Or;

    .line 1675604
    iput-object p8, p0, LX/ATO;->m:LX/7kn;

    .line 1675605
    move-object/from16 v0, p10

    iput-object v0, p0, LX/ATO;->n:LX/03V;

    .line 1675606
    move-object/from16 v0, p11

    iput-object v0, p0, LX/ATO;->q:LX/9cA;

    .line 1675607
    move-object/from16 v0, p12

    iput-object v0, p0, LX/ATO;->o:LX/74n;

    .line 1675608
    const/4 v2, -0x1

    iput v2, p0, LX/ATO;->I:I

    .line 1675609
    const/4 v2, 0x0

    iput v2, p0, LX/ATO;->F:I

    .line 1675610
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    iput-object v2, p0, LX/ATO;->a:Ljava/util/List;

    .line 1675611
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    iput-object v2, p0, LX/ATO;->b:Ljava/util/List;

    .line 1675612
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    iput-object v2, p0, LX/ATO;->c:Ljava/util/List;

    .line 1675613
    new-instance v2, LX/ATL;

    invoke-direct {v2, p0}, LX/ATL;-><init>(LX/ATO;)V

    iput-object v2, p0, LX/ATO;->i:LX/ATL;

    .line 1675614
    iget-object v2, p0, LX/ATO;->D:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/0dp;->i:LX/0Tn;

    const/4 v4, 0x1

    invoke-interface {v2, v3, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v2

    iput-boolean v2, p0, LX/ATO;->f:Z

    .line 1675615
    move/from16 v0, p31

    iput-boolean v0, p0, LX/ATO;->K:Z

    .line 1675616
    move/from16 v0, p32

    iput-boolean v0, p0, LX/ATO;->L:Z

    .line 1675617
    invoke-interface/range {p33 .. p33}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0io;

    check-cast v2, LX/0j0;

    invoke-interface {v2}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, LX/ATO;->N:Ljava/lang/String;

    .line 1675618
    iget-object v2, p0, LX/ATO;->q:LX/9cA;

    iget-object v3, p0, LX/ATO;->N:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/9cA;->a(Ljava/lang/String;)LX/9c9;

    move-result-object v2

    iput-object v2, p0, LX/ATO;->A:LX/9c9;

    .line 1675619
    invoke-virtual/range {p28 .. p28}, LX/0gc;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1675620
    move-object/from16 v0, p28

    iput-object v0, p0, LX/ATO;->v:LX/0gc;

    .line 1675621
    :goto_0
    invoke-interface/range {p33 .. p33}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0io;

    check-cast v2, LX/0j3;

    invoke-interface {v2}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isEdit()Z

    move-result v2

    iput-boolean v2, p0, LX/ATO;->X:Z

    .line 1675622
    invoke-interface/range {p33 .. p33}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0io;

    check-cast v2, LX/0j3;

    invoke-interface {v2}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->canViewerEditPostMedia()Z

    move-result v2

    iput-boolean v2, p0, LX/ATO;->Y:Z

    move-object/from16 v2, p33

    .line 1675623
    check-cast v2, LX/0ik;

    invoke-interface {v2}, LX/0ik;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/5Qw;

    invoke-interface {v2}, LX/5Qw;->s()Z

    move-result v2

    iput-boolean v2, p0, LX/ATO;->V:Z

    .line 1675624
    move/from16 v0, p34

    iput-boolean v0, p0, LX/ATO;->Z:Z

    .line 1675625
    move-object/from16 v0, p29

    iput-object v0, p0, LX/ATO;->w:Lcom/facebook/widget/ScrollingAwareScrollView;

    .line 1675626
    iget-object v2, p0, LX/ATO;->w:Lcom/facebook/widget/ScrollingAwareScrollView;

    const-string v3, "tag_scroll_View"

    invoke-virtual {v2, v3}, Lcom/facebook/widget/ScrollingAwareScrollView;->setTag(Ljava/lang/Object;)V

    .line 1675627
    iget-object v2, p0, LX/ATO;->w:Lcom/facebook/widget/ScrollingAwareScrollView;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/facebook/widget/ScrollingAwareScrollView;->setFocusable(Z)V

    .line 1675628
    iget-object v2, p0, LX/ATO;->w:Lcom/facebook/widget/ScrollingAwareScrollView;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/facebook/widget/ScrollingAwareScrollView;->setFocusableInTouchMode(Z)V

    .line 1675629
    iget-object v2, p0, LX/ATO;->w:Lcom/facebook/widget/ScrollingAwareScrollView;

    const/high16 v3, 0x20000

    invoke-virtual {v2, v3}, Lcom/facebook/widget/ScrollingAwareScrollView;->setDescendantFocusability(I)V

    .line 1675630
    iget-object v2, p0, LX/ATO;->w:Lcom/facebook/widget/ScrollingAwareScrollView;

    invoke-direct {p0}, LX/ATO;->r()LX/4oV;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/widget/ScrollingAwareScrollView;->a(LX/4oV;)V

    .line 1675631
    iget-object v2, p0, LX/ATO;->w:Lcom/facebook/widget/ScrollingAwareScrollView;

    invoke-virtual {v2}, Lcom/facebook/widget/ScrollingAwareScrollView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v2

    iget-object v3, p0, LX/ATO;->t:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v2, v3}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 1675632
    new-instance v2, LX/ATl;

    iget-object v3, p0, LX/ATO;->g:Landroid/content/Context;

    invoke-direct {v2, v3}, LX/ATl;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, LX/ATO;->z:LX/ATl;

    .line 1675633
    iget-object v2, p0, LX/ATO;->z:LX/ATl;

    iget-object v3, p0, LX/ATO;->u:LX/ATG;

    invoke-virtual {v2, v3}, LX/ATl;->setAttachmentViewAdapter(LX/ATG;)V

    .line 1675634
    move-object/from16 v0, p30

    iput-object v0, p0, LX/ATO;->y:Landroid/widget/LinearLayout;

    .line 1675635
    iget-object v2, p0, LX/ATO;->y:Landroid/widget/LinearLayout;

    iget-object v3, p0, LX/ATO;->z:LX/ATl;

    new-instance v4, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v5, -0x1

    const/4 v6, -0x2

    invoke-direct {v4, v5, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v3, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1675636
    const/4 v2, 0x1

    iput-boolean v2, p0, LX/ATO;->J:Z

    .line 1675637
    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-static/range {p33 .. p33}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v2, p0, LX/ATO;->aa:Ljava/lang/ref/WeakReference;

    .line 1675638
    const/4 v2, 0x1

    iput-boolean v2, p0, LX/ATO;->Q:Z

    .line 1675639
    move-object/from16 v0, p27

    iput-object v0, p0, LX/ATO;->ao:LX/BTg;

    .line 1675640
    return-void

    .line 1675641
    :cond_0
    const/4 v2, 0x0

    iput-object v2, p0, LX/ATO;->v:LX/0gc;

    .line 1675642
    iget-object v2, p0, LX/ATO;->n:LX/03V;

    const-string v3, "underwood_setup_fragment_manager"

    const-string v4, "Not safe to commit stateful transactions to the fragment manager"

    invoke-virtual {v2, v3, v4}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private static a(Lcom/facebook/composer/attachments/ComposerAttachment;Ljava/util/List;)I
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 1675881
    invoke-virtual {p0}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v2

    .line 1675882
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1675883
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    .line 1675884
    invoke-virtual {v2}, Lcom/facebook/ipc/media/MediaItem;->c()J

    move-result-wide v4

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->c()J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-nez v3, :cond_0

    invoke-virtual {v2}, Lcom/facebook/ipc/media/MediaItem;->g()I

    move-result v3

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->g()I

    move-result v4

    if-ne v3, v4, :cond_0

    invoke-virtual {v2}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1675885
    :goto_1
    return v1

    .line 1675886
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1675887
    :cond_1
    const/4 v1, -0x1

    goto :goto_1
.end method

.method private a(I)LX/ATE;
    .locals 5

    .prologue
    .line 1675888
    new-instance v2, LX/ATE;

    iget-object v0, p0, LX/ATO;->g:Landroid/content/Context;

    invoke-direct {v2, v0}, LX/ATE;-><init>(Landroid/content/Context;)V

    .line 1675889
    iget-object v0, p0, LX/ATO;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/attachments/ComposerAttachment;

    iget-object v3, p0, LX/ATO;->i:LX/ATL;

    iget-object v4, p0, LX/ATO;->an:LX/3iT;

    iget-object v1, p0, LX/ATO;->b:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/ASn;

    invoke-interface {v1}, LX/ASn;->b()Landroid/view/View;

    move-result-object v1

    .line 1675890
    iput-object v0, v2, LX/ATE;->e:Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 1675891
    iput-object v3, v2, LX/ATE;->d:LX/ATL;

    .line 1675892
    iput-object v4, v2, LX/ATE;->f:LX/3iT;

    .line 1675893
    new-instance p0, LX/ATD;

    invoke-direct {p0, v2}, LX/ATD;-><init>(LX/ATE;)V

    iput-object p0, v2, LX/ATE;->c:LX/ATD;

    .line 1675894
    iput-object v1, v2, LX/ATE;->b:Landroid/view/View;

    .line 1675895
    iget-object p0, v2, LX/ATE;->a:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    iget-object p1, v2, LX/ATE;->c:LX/ATD;

    invoke-virtual {p0, p1}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1675896
    iget-object p0, v2, LX/ATE;->a:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    const/4 p1, 0x0

    .line 1675897
    iput-object p1, p0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->b:LX/8oi;

    .line 1675898
    iget-object p0, v2, LX/ATE;->e:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {p0}, Lcom/facebook/composer/attachments/ComposerAttachment;->d()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object p0

    iget-object p1, v2, LX/ATE;->f:LX/3iT;

    invoke-static {p0, p1}, LX/8of;->a(LX/175;LX/3iT;)LX/8of;

    move-result-object p0

    .line 1675899
    iget-object p1, v2, LX/ATE;->a:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    invoke-static {p0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string p0, ""

    :cond_0
    invoke-virtual {p1, p0}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1675900
    iget-object p0, v2, LX/ATE;->a:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    iget-object p1, v2, LX/ATE;->c:LX/ATD;

    invoke-virtual {p0, p1}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1675901
    iget-object p0, v2, LX/ATE;->a:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    iget-object p1, v2, LX/ATE;->c:LX/ATD;

    .line 1675902
    iput-object p1, p0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->b:LX/8oi;

    .line 1675903
    iget-object p0, v2, LX/ATE;->b:Landroid/view/View;

    invoke-virtual {v2, p0}, LX/ATE;->addView(Landroid/view/View;)V

    .line 1675904
    iget-object p0, v2, LX/ATE;->a:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    invoke-virtual {v2, p0}, LX/ATE;->addView(Landroid/view/View;)V

    .line 1675905
    return-object v2
.end method

.method private b(I)LX/ASn;
    .locals 16

    .prologue
    .line 1675906
    move-object/from16 v0, p0

    iget-object v1, v0, LX/ATO;->aa:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, LX/0il;

    .line 1675907
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v15

    .line 1675908
    move-object/from16 v0, p0

    iget-object v1, v0, LX/ATO;->ac:LX/AT3;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/ATO;->i:LX/ATL;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/ATO;->N:Ljava/lang/String;

    invoke-virtual {v1, v10, v2, v3}, LX/AT3;->a(LX/0il;LX/ATL;Ljava/lang/String;)Lcom/facebook/composer/ui/underwood/SphericalPhotoAttachmentViewController;

    move-result-object v1

    invoke-virtual {v15, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1675909
    move-object/from16 v0, p0

    iget-boolean v1, v0, LX/ATO;->Z:Z

    if-nez v1, :cond_0

    .line 1675910
    const/4 v1, 0x4

    new-array v1, v1, [LX/ASn;

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, LX/ATO;->ag:LX/ATA;

    invoke-virtual {v3, v10}, LX/ATA;->a(LX/0il;)LX/AT9;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    move-object/from16 v0, p0

    iget-object v3, v0, LX/ATO;->af:LX/ASo;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/ATO;->i:LX/ATL;

    invoke-virtual {v3, v10, v4}, LX/ASo;->a(LX/0il;LX/ATL;)Lcom/facebook/composer/ui/underwood/GifPreviewAttachmentViewController;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    move-object/from16 v0, p0

    iget-object v3, v0, LX/ATO;->ad:LX/ATt;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/ATO;->i:LX/ATL;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/ATO;->v:LX/0gc;

    move-object/from16 v0, p0

    iget-object v6, v0, LX/ATO;->N:Ljava/lang/String;

    invoke-virtual {v3, v10, v4, v5, v6}, LX/ATt;->a(LX/0il;LX/ATL;LX/0gc;Ljava/lang/String;)LX/ATs;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    move-object/from16 v0, p0

    iget-object v3, v0, LX/ATO;->ae:LX/ASs;

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, LX/ATO;->i:LX/ATL;

    invoke-virtual {v3, v10, v4, v5}, LX/ASs;->a(LX/0il;Ljava/lang/Integer;LX/ATL;)Lcom/facebook/composer/ui/underwood/SlideshowAttachmentViewController;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v15, v1}, LX/0Pz;->b([Ljava/lang/Object;)LX/0Pz;

    .line 1675911
    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, LX/ATO;->ab:LX/ATk;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/ATO;->v:LX/0gc;

    move-object/from16 v0, p0

    iget v3, v0, LX/ATO;->F:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static/range {p1 .. p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, LX/ATO;->N:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-boolean v6, v0, LX/ATO;->X:Z

    move-object/from16 v0, p0

    iget-boolean v7, v0, LX/ATO;->Y:Z

    move-object/from16 v0, p0

    iget-object v8, v0, LX/ATO;->i:LX/ATL;

    move-object/from16 v0, p0

    iget-object v9, v0, LX/ATO;->p:LX/9bz;

    move-object/from16 v0, p0

    iget-object v11, v0, LX/ATO;->M:LX/Hqr;

    move-object/from16 v0, p0

    iget-boolean v12, v0, LX/ATO;->J:Z

    move-object/from16 v0, p0

    iget-boolean v13, v0, LX/ATO;->S:Z

    move-object/from16 v0, p0

    iget-object v14, v0, LX/ATO;->w:Lcom/facebook/widget/ScrollingAwareScrollView;

    invoke-virtual/range {v1 .. v14}, LX/ATk;->a(LX/0gc;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;ZZLX/ATL;LX/9bz;Ljava/lang/Object;LX/Hqr;ZZLcom/facebook/widget/ScrollingAwareScrollView;)LX/ATj;

    move-result-object v1

    invoke-virtual {v15, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1675912
    invoke-virtual {v15}, LX/0Pz;->b()LX/0Px;

    move-result-object v5

    .line 1675913
    const/4 v3, 0x0

    .line 1675914
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    const/4 v1, 0x0

    move v4, v1

    :goto_0
    if-ge v4, v6, :cond_4

    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/ASn;

    .line 1675915
    move-object/from16 v0, p0

    iget-object v2, v0, LX/ATO;->a:Ljava/util/List;

    move/from16 v0, p1

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-interface {v1, v2}, LX/ASn;->b(Lcom/facebook/composer/attachments/ComposerAttachment;)Z

    move-result v2

    if-eqz v2, :cond_3

    move-object v2, v1

    .line 1675916
    :goto_1
    const-string v1, "Attachment type is not supported!"

    invoke-static {v2, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1675917
    invoke-interface {v2}, LX/ASn;->b()Landroid/view/View;

    move-result-object v3

    .line 1675918
    move-object/from16 v0, p0

    iget-object v1, v0, LX/ATO;->a:Ljava/util/List;

    move/from16 v0, p1

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v1}, Lcom/facebook/composer/attachments/ComposerAttachment;->i()I

    move-result v1

    .line 1675919
    const/4 v4, -0x1

    if-ne v1, v4, :cond_1

    .line 1675920
    invoke-static {}, LX/473;->a()I

    move-result v1

    .line 1675921
    :cond_1
    invoke-virtual {v3, v1}, Landroid/view/View;->setId(I)V

    .line 1675922
    if-nez p1, :cond_2

    .line 1675923
    const-string v1, "first_attachment_view"

    invoke-virtual {v3, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 1675924
    :cond_2
    return-object v2

    .line 1675925
    :cond_3
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_0

    :cond_4
    move-object v2, v3

    goto :goto_1
.end method

.method public static b(LX/ATO;Lcom/facebook/photos/base/media/PhotoItem;)LX/ASn;
    .locals 7
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1675926
    iget-object v0, p0, LX/ATO;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ASn;

    .line 1675927
    invoke-interface {v0}, LX/ASn;->c()Lcom/facebook/composer/attachments/ComposerAttachment;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/composer/attachments/ComposerAttachment;->g()Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1675928
    invoke-interface {v0}, LX/ASn;->c()Lcom/facebook/composer/attachments/ComposerAttachment;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/composer/attachments/ComposerAttachment;->g()Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;->getFrames()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    .line 1675929
    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v5, :cond_0

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame;

    .line 1675930
    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame;->getMediaData()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v6

    if-eqz v6, :cond_1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame;->getMediaData()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v6

    .line 1675931
    iget-object p0, v6, Lcom/facebook/ipc/media/data/MediaData;->mUri:Landroid/net/Uri;

    move-object v6, p0

    .line 1675932
    if-eqz v6, :cond_1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame;->getMediaData()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v1

    .line 1675933
    iget-object v6, v1, Lcom/facebook/ipc/media/data/MediaData;->mUri:Landroid/net/Uri;

    move-object v1, v6

    .line 1675934
    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v6

    .line 1675935
    iget-object p0, v6, Lcom/facebook/ipc/media/data/MediaData;->mUri:Landroid/net/Uri;

    move-object v6, p0

    .line 1675936
    invoke-virtual {v1, v6}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1675937
    :goto_1
    return-object v0

    .line 1675938
    :cond_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 1675939
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static b(LX/ATO;Lcom/facebook/composer/attachments/ComposerAttachment;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1675940
    iget-object v0, p0, LX/ATO;->a:Ljava/util/List;

    invoke-interface {v0, p2, p1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 1675941
    invoke-direct {p0, p2}, LX/ATO;->b(I)LX/ASn;

    move-result-object v1

    .line 1675942
    iget-object v0, p0, LX/ATO;->b:Ljava/util/List;

    invoke-interface {v0, p2, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 1675943
    iget-object v0, p0, LX/ATO;->c:Ljava/util/List;

    invoke-direct {p0, p2}, LX/ATO;->a(I)LX/ATE;

    move-result-object v2

    invoke-interface {v0, p2, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 1675944
    invoke-interface {v1, p1}, LX/ASn;->a(Lcom/facebook/composer/attachments/ComposerAttachment;)V

    .line 1675945
    iget-object v0, p0, LX/ATO;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le p2, v0, :cond_4

    .line 1675946
    iget-object v2, p0, LX/ATO;->z:LX/ATl;

    iget-object v0, p0, LX/ATO;->c:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v2, v0}, LX/ATl;->addView(Landroid/view/View;)V

    .line 1675947
    :goto_0
    iget-object v0, p0, LX/ATO;->a:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->i()I

    move-result v0

    const/4 v2, -0x1

    if-ne v0, v2, :cond_1

    .line 1675948
    iget-object v0, p0, LX/ATO;->a:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-static {v0}, LX/7kv;->a(Lcom/facebook/composer/attachments/ComposerAttachment;)LX/7kv;

    move-result-object v0

    invoke-interface {v1}, LX/ASn;->b()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getId()I

    move-result v2

    .line 1675949
    iput v2, v0, LX/7kv;->h:I

    .line 1675950
    move-object v2, v0

    .line 1675951
    iget-object v0, p0, LX/ATO;->a:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->f()Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1675952
    iget-object v0, p0, LX/ATO;->ao:LX/BTg;

    invoke-virtual {v0}, LX/BTg;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "high"

    .line 1675953
    :goto_1
    iput-object v0, v2, LX/7kv;->i:Ljava/lang/String;

    .line 1675954
    :cond_0
    invoke-virtual {v2}, LX/7kv;->a()Lcom/facebook/composer/attachments/ComposerAttachment;

    move-result-object v2

    .line 1675955
    iget-object v0, p0, LX/ATO;->a:Ljava/util/List;

    invoke-interface {v0, p2, v2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1675956
    iget-object v0, p0, LX/ATO;->c:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ATE;

    .line 1675957
    iput-object v2, v0, LX/ATE;->e:Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 1675958
    invoke-interface {v1, v2}, LX/ASn;->c(Lcom/facebook/composer/attachments/ComposerAttachment;)V

    .line 1675959
    iget-object v0, p0, LX/ATO;->B:LX/ASe;

    invoke-interface {v0, p2, v2, v3, v3}, LX/ASe;->a(ILcom/facebook/composer/attachments/ComposerAttachment;ZZ)V

    .line 1675960
    :cond_1
    const/4 v0, 0x0

    .line 1675961
    invoke-virtual {p1}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 1675962
    invoke-virtual {p1}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->d()Lcom/facebook/ipc/media/MediaIdKey;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaIdKey;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1675963
    :cond_2
    if-eqz v0, :cond_3

    iget-object v1, p0, LX/ATO;->r:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 1675964
    iget-object v1, p0, LX/ATO;->r:Ljava/util/Map;

    invoke-static {}, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->newBuilder()LX/9cC;

    move-result-object v2

    invoke-virtual {v2}, LX/9cC;->a()Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1675965
    :cond_3
    return-void

    .line 1675966
    :cond_4
    iget-object v2, p0, LX/ATO;->z:LX/ATl;

    iget-object v0, p0, LX/ATO;->c:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v2, v0, p2}, LX/ATl;->addView(Landroid/view/View;I)V

    goto/16 :goto_0

    .line 1675967
    :cond_5
    const-string v0, "standard"

    goto :goto_1
.end method

.method private r()LX/4oV;
    .locals 1

    .prologue
    .line 1675972
    new-instance v0, LX/ATH;

    invoke-direct {v0, p0}, LX/ATH;-><init>(LX/ATO;)V

    return-object v0
.end method

.method public static t(LX/ATO;)V
    .locals 2

    .prologue
    .line 1675968
    iget-object v0, p0, LX/ATO;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ASn;

    .line 1675969
    invoke-interface {v0}, LX/ASn;->d()V

    .line 1675970
    invoke-interface {v0}, LX/ASn;->e()V

    goto :goto_0

    .line 1675971
    :cond_0
    return-void
.end method

.method private u()V
    .locals 5

    .prologue
    .line 1675977
    iget-boolean v0, p0, LX/ATO;->Q:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/ATO;->L:Z

    if-eqz v0, :cond_1

    .line 1675978
    :cond_0
    :goto_0
    return-void

    .line 1675979
    :cond_1
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1675980
    iget-boolean v0, p0, LX/ATO;->E:Z

    if-nez v0, :cond_5

    move v0, v2

    .line 1675981
    :goto_1
    move v0, v0

    .line 1675982
    if-nez v0, :cond_2

    iget-object v1, p0, LX/ATO;->x:Landroid/widget/FrameLayout;

    if-eqz v1, :cond_0

    .line 1675983
    :cond_2
    iget-object v1, p0, LX/ATO;->x:Landroid/widget/FrameLayout;

    if-nez v1, :cond_3

    .line 1675984
    iget-object v1, p0, LX/ATO;->h:Landroid/view/LayoutInflater;

    const v2, 0x7f031546

    iget-object v3, p0, LX/ATO;->y:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1675985
    iget-object v1, p0, LX/ATO;->y:Landroid/widget/LinearLayout;

    const v2, 0x7f0d191e

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    iput-object v1, p0, LX/ATO;->x:Landroid/widget/FrameLayout;

    .line 1675986
    iget-object v1, p0, LX/ATO;->x:Landroid/widget/FrameLayout;

    const v2, 0x7f0d2fe5

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 1675987
    iget-boolean v2, p0, LX/ATO;->V:Z

    if-eqz v2, :cond_a

    const v2, 0x7f08147b

    :goto_2
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 1675988
    iget-object v1, p0, LX/ATO;->x:Landroid/widget/FrameLayout;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1675989
    iget-object v1, p0, LX/ATO;->x:Landroid/widget/FrameLayout;

    new-instance v2, LX/ATI;

    invoke-direct {v2, p0}, LX/ATI;-><init>(LX/ATO;)V

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1675990
    :cond_3
    iget-object v1, p0, LX/ATO;->x:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_4

    const/4 v0, 0x0

    :goto_3
    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto :goto_0

    :cond_4
    const/16 v0, 0x8

    goto :goto_3

    .line 1675991
    :cond_5
    iget-boolean v0, p0, LX/ATO;->Z:Z

    if-eqz v0, :cond_6

    move v0, v3

    .line 1675992
    goto :goto_1

    .line 1675993
    :cond_6
    const/4 v1, 0x0

    .line 1675994
    iget-object v0, p0, LX/ATO;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p0, LX/ATO;->a:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_7

    iget-object v0, p0, LX/ATO;->a:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 1675995
    iget-object v0, p0, LX/ATO;->a:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v0

    .line 1675996
    iget-object v1, v0, Lcom/facebook/ipc/media/data/MediaData;->mType:LX/4gQ;

    move-object v0, v1

    .line 1675997
    move-object v1, v0

    .line 1675998
    :cond_7
    iget-object v0, p0, LX/ATO;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_9

    iget-object v0, p0, LX/ATO;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    iget-object v0, p0, LX/ATO;->H:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ge v4, v0, :cond_9

    sget-object v0, LX/4gQ;->Photo:LX/4gQ;

    if-eq v1, v0, :cond_8

    iget-boolean v0, p0, LX/ATO;->V:Z

    if-eqz v0, :cond_9

    :cond_8
    move v0, v3

    .line 1675999
    goto/16 :goto_1

    :cond_9
    move v0, v2

    .line 1676000
    goto/16 :goto_1

    .line 1676001
    :cond_a
    const v2, 0x7f08147a

    goto/16 :goto_2
.end method


# virtual methods
.method public final a()LX/0P1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1676002
    iget-object v0, p0, LX/ATO;->r:Ljava/util/Map;

    invoke-static {v0}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0P1;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1675973
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/ATO;->r:Ljava/util/Map;

    .line 1675974
    invoke-virtual {p1}, LX/0P1;->entrySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1675975
    iget-object v2, p0, LX/ATO;->r:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1675976
    :cond_0
    return-void
.end method

.method public final a(LX/0Px;LX/03R;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/base/media/PhotoItem;",
            ">;",
            "LX/03R;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1675783
    sget-object v1, LX/ATK;->a:[I

    invoke-virtual {p2}, LX/03R;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1675784
    :goto_0
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v2

    move v1, v0

    :goto_1
    if-ge v1, v2, :cond_2

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/base/media/PhotoItem;

    .line 1675785
    iget-object v5, p0, LX/ATO;->b:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/ASn;

    .line 1675786
    invoke-interface {v5}, LX/ASn;->c()Lcom/facebook/composer/attachments/ComposerAttachment;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/ipc/media/MediaItem;->c()J

    move-result-wide v7

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->c()J

    move-result-wide v9

    cmp-long v7, v7, v9

    if-nez v7, :cond_0

    .line 1675787
    :goto_2
    move-object v3, v5

    .line 1675788
    if-eqz v3, :cond_1

    .line 1675789
    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v0

    iget-boolean v4, p0, LX/ATO;->J:Z

    invoke-interface {v3, v0, v4}, LX/ASn;->a(Lcom/facebook/ipc/media/data/MediaData;Z)V

    .line 1675790
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1675791
    :pswitch_0
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/ATO;->J:Z

    goto :goto_0

    .line 1675792
    :pswitch_1
    iput-boolean v0, p0, LX/ATO;->J:Z

    goto :goto_0

    .line 1675793
    :cond_2
    return-void

    :cond_3
    const/4 v5, 0x0

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(LX/0Px;Z)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    const/4 v7, -0x1

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1675794
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    iput v0, p0, LX/ATO;->P:I

    .line 1675795
    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/ATO;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1675796
    iget-object v0, p0, LX/ATO;->w:Lcom/facebook/widget/ScrollingAwareScrollView;

    invoke-virtual {v0}, Lcom/facebook/widget/ScrollingAwareScrollView;->requestFocus()Z

    .line 1675797
    :cond_0
    iget-object v0, p0, LX/ATO;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 1675798
    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1675799
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 1675800
    invoke-static {v0, p1}, LX/ATO;->a(Lcom/facebook/composer/attachments/ComposerAttachment;Ljava/util/List;)I

    move-result v2

    if-eq v2, v7, :cond_2

    if-eqz p2, :cond_1

    .line 1675801
    :cond_2
    iget-object v2, p0, LX/ATO;->a:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v2

    .line 1675802
    if-gez v2, :cond_12

    .line 1675803
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_3
    move v2, v3

    .line 1675804
    :goto_2
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    if-ge v2, v0, :cond_7

    .line 1675805
    invoke-virtual {p1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 1675806
    iget-object v1, p0, LX/ATO;->a:Ljava/util/List;

    invoke-static {v0, v1}, LX/ATO;->a(Lcom/facebook/composer/attachments/ComposerAttachment;Ljava/util/List;)I

    move-result v5

    .line 1675807
    if-ne v5, v7, :cond_4

    .line 1675808
    invoke-static {p0, v0, v2}, LX/ATO;->b(LX/ATO;Lcom/facebook/composer/attachments/ComposerAttachment;I)V

    .line 1675809
    :goto_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 1675810
    :cond_4
    iget-object v1, p0, LX/ATO;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v6, v2, 0x1

    if-lt v1, v6, :cond_5

    invoke-direct {p0, v2}, LX/ATO;->b(I)LX/ASn;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    iget-object v1, p0, LX/ATO;->b:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/ASn;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 1675811
    iget-object v5, p0, LX/ATO;->z:LX/ATl;

    iget-object v1, p0, LX/ATO;->c:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-virtual {v5, v1}, LX/ATl;->removeView(Landroid/view/View;)V

    .line 1675812
    iget-object v1, p0, LX/ATO;->b:Ljava/util/List;

    iget-object v5, p0, LX/ATO;->b:Ljava/util/List;

    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v1, v5}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1675813
    iget-object v1, p0, LX/ATO;->a:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 1675814
    invoke-static {p0, v0, v2}, LX/ATO;->b(LX/ATO;Lcom/facebook/composer/attachments/ComposerAttachment;I)V

    .line 1675815
    goto :goto_3

    .line 1675816
    :cond_5
    const/4 v6, 0x1

    .line 1675817
    if-ne v5, v2, :cond_16

    move v1, v6

    .line 1675818
    :goto_4
    move v1, v1

    .line 1675819
    if-eqz v1, :cond_6

    .line 1675820
    iget-object v1, p0, LX/ATO;->a:Ljava/util/List;

    invoke-interface {v1, v2, v0}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 1675821
    iget-object v1, p0, LX/ATO;->b:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/ASn;

    invoke-interface {v1, v0}, LX/ASn;->c(Lcom/facebook/composer/attachments/ComposerAttachment;)V

    .line 1675822
    iget-object v1, p0, LX/ATO;->c:Ljava/util/List;

    invoke-interface {v1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/ATE;

    .line 1675823
    iput-object v0, v1, LX/ATE;->e:Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 1675824
    goto :goto_3

    .line 1675825
    :cond_6
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v6, "Unexpected failure: could not move attachment. \nfrom position: "

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, "\nto position: "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, "\nmAttachments size: "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v5, p0, LX/ATO;->a:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, "\nmAttachmentControllers size: "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v5, p0, LX/ATO;->b:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v0

    .line 1675826
    iput-boolean v4, v0, LX/0VK;->d:Z

    .line 1675827
    move-object v0, v0

    .line 1675828
    invoke-virtual {v0}, LX/0VK;->g()LX/0VG;

    move-result-object v0

    .line 1675829
    iget-object v1, p0, LX/ATO;->n:LX/03V;

    invoke-virtual {v1, v0}, LX/03V;->a(LX/0VG;)V

    goto/16 :goto_3

    .line 1675830
    :cond_7
    iget-object v0, p0, LX/ATO;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v4, :cond_b

    .line 1675831
    iget-object v0, p0, LX/ATO;->aa:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    check-cast v0, LX/0jA;

    invoke-interface {v0}, LX/0jA;->getSlideshowData()Lcom/facebook/ipc/composer/model/ComposerSlideshowData;

    move-result-object v0

    if-nez v0, :cond_8

    move v1, v4

    .line 1675832
    :goto_5
    iget-object v0, p0, LX/ATO;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_6
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ATE;

    .line 1675833
    iget-boolean v5, p0, LX/ATO;->R:Z

    if-eqz v5, :cond_9

    if-eqz v1, :cond_9

    .line 1675834
    iget-object v5, v0, LX/ATE;->a:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->setVisibility(I)V

    .line 1675835
    goto :goto_6

    :cond_8
    move v1, v3

    .line 1675836
    goto :goto_5

    .line 1675837
    :cond_9
    iget-object v5, v0, LX/ATE;->a:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    invoke-virtual {v5}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_a

    .line 1675838
    iget-object v5, v0, LX/ATE;->a:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    const-string v6, ""

    invoke-virtual {v5, v6}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1675839
    :cond_a
    iget-object v5, v0, LX/ATE;->a:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->setVisibility(I)V

    .line 1675840
    goto :goto_6

    .line 1675841
    :cond_b
    iget-object v0, p0, LX/ATO;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v4, :cond_c

    .line 1675842
    iget-object v0, p0, LX/ATO;->c:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ATE;

    .line 1675843
    iget-object v1, v0, LX/ATE;->a:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    invoke-virtual {v1}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_c

    .line 1675844
    iget-object v1, v0, LX/ATE;->a:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->setVisibility(I)V

    .line 1675845
    :cond_c
    iget-object v0, p0, LX/ATO;->y:Landroid/widget/LinearLayout;

    if-nez v0, :cond_10

    .line 1675846
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected failure: attempting to set attachments before initializing. \nmIsInitialized: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, LX/ATO;->Q:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\nmIsNewSession: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v2, p0, LX/ATO;->K:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\nmAttachments is null: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, LX/ATO;->a:Ljava/util/List;

    if-nez v0, :cond_d

    move v0, v4

    :goto_7
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\nmAttachmentViews is null: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, LX/ATO;->b:Ljava/util/List;

    if-nez v0, :cond_e

    move v0, v4

    :goto_8
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\nmAttachmentViewsContainer is null: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, LX/ATO;->z:LX/ATl;

    if-nez v2, :cond_f

    :goto_9
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\nmTotalAttachments: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, LX/ATO;->P:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v0

    invoke-virtual {v0}, LX/0VK;->g()LX/0VG;

    move-result-object v0

    .line 1675847
    iget-object v1, p0, LX/ATO;->n:LX/03V;

    invoke-virtual {v1, v0}, LX/03V;->a(LX/0VG;)V

    .line 1675848
    :goto_a
    return-void

    :cond_d
    move v0, v3

    .line 1675849
    goto :goto_7

    :cond_e
    move v0, v3

    goto :goto_8

    :cond_f
    move v4, v3

    goto :goto_9

    .line 1675850
    :cond_10
    iget-object v0, p0, LX/ATO;->y:Landroid/widget/LinearLayout;

    iget-object v1, p0, LX/ATO;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_11

    iget-boolean v1, p0, LX/ATO;->Z:Z

    if-nez v1, :cond_11

    const/16 v3, 0x8

    :cond_11
    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1675851
    invoke-direct {p0}, LX/ATO;->u()V

    goto :goto_a

    .line 1675852
    :cond_12
    if-eqz v3, :cond_13

    .line 1675853
    iget-object v5, p0, LX/ATO;->a:Ljava/util/List;

    invoke-interface {v5, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 1675854
    :cond_13
    iget-object v5, p0, LX/ATO;->b:Ljava/util/List;

    invoke-interface {v5, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 1675855
    iget-object v5, p0, LX/ATO;->c:Ljava/util/List;

    invoke-interface {v5, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .line 1675856
    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/ipc/media/MediaItem;->d()Lcom/facebook/ipc/media/MediaIdKey;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/ipc/media/MediaIdKey;->toString()Ljava/lang/String;

    move-result-object v6

    .line 1675857
    iget-object v5, p0, LX/ATO;->r:Ljava/util/Map;

    invoke-interface {v5, v6}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_14

    .line 1675858
    iget-object v5, p0, LX/ATO;->r:Ljava/util/Map;

    invoke-interface {v5, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;

    .line 1675859
    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->e()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v8

    .line 1675860
    if-eqz v8, :cond_14

    invoke-virtual {v5}, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->a()Z

    move-result v9

    if-eqz v9, :cond_14

    .line 1675861
    iget-object v9, p0, LX/ATO;->r:Ljava/util/Map;

    invoke-static {v5}, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->a(Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;)LX/9cC;

    move-result-object v5

    invoke-virtual {v8}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getStickerParams()LX/0Px;

    move-result-object v10

    invoke-virtual {v10}, LX/0Px;->size()I

    move-result v10

    .line 1675862
    iput v10, v5, LX/9cC;->j:I

    .line 1675863
    move-object v5, v5

    .line 1675864
    invoke-virtual {v8}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getTextParams()LX/0Px;

    move-result-object v10

    invoke-virtual {v10}, LX/0Px;->size()I

    move-result v10

    .line 1675865
    iput v10, v5, LX/9cC;->k:I

    .line 1675866
    move-object v10, v5

    .line 1675867
    invoke-virtual {v8}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getCropBox()Lcom/facebook/photos/creativeediting/model/PersistableRect;

    move-result-object v5

    if-eqz v5, :cond_15

    const/4 v5, 0x1

    .line 1675868
    :goto_b
    iput-boolean v5, v10, LX/9cC;->a:Z

    .line 1675869
    move-object v5, v10

    .line 1675870
    invoke-virtual {v5}, LX/9cC;->a()Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;

    move-result-object v5

    invoke-interface {v9, v6, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1675871
    :cond_14
    iget-object v5, p0, LX/ATO;->z:LX/ATl;

    invoke-virtual {v5, v2}, LX/ATl;->removeView(Landroid/view/View;)V

    goto/16 :goto_1

    .line 1675872
    :cond_15
    const/4 v5, 0x0

    goto :goto_b

    .line 1675873
    :cond_16
    iget-object v1, p0, LX/ATO;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v5, v1, :cond_17

    iget-object v1, p0, LX/ATO;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v2, v1, :cond_18

    .line 1675874
    :cond_17
    const/4 v1, 0x0

    goto/16 :goto_4

    .line 1675875
    :cond_18
    iget-object v1, p0, LX/ATO;->z:LX/ATl;

    invoke-virtual {v1, v5}, LX/ATl;->removeViewAt(I)V

    .line 1675876
    iget-object v8, p0, LX/ATO;->z:LX/ATl;

    iget-object v1, p0, LX/ATO;->c:Ljava/util/List;

    invoke-interface {v1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-virtual {v8, v1, v2}, LX/ATl;->addView(Landroid/view/View;I)V

    .line 1675877
    iget-object v1, p0, LX/ATO;->a:Ljava/util/List;

    iget-object v8, p0, LX/ATO;->a:Ljava/util/List;

    invoke-interface {v8, v5}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v8

    invoke-interface {v1, v2, v8}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 1675878
    iget-object v1, p0, LX/ATO;->c:Ljava/util/List;

    iget-object v8, p0, LX/ATO;->c:Ljava/util/List;

    invoke-interface {v8, v5}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v8

    invoke-interface {v1, v2, v8}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 1675879
    iget-object v1, p0, LX/ATO;->b:Ljava/util/List;

    iget-object v8, p0, LX/ATO;->b:Ljava/util/List;

    invoke-interface {v8, v5}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v8

    invoke-interface {v1, v2, v8}, Ljava/util/List;->add(ILjava/lang/Object;)V

    move v1, v6

    .line 1675880
    goto/16 :goto_4
.end method

.method public final a(LX/5L2;)V
    .locals 2

    .prologue
    .line 1675780
    iget-object v0, p0, LX/ATO;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ASn;

    .line 1675781
    invoke-interface {v0, p1}, LX/ASn;->a(LX/5L2;)V

    goto :goto_0

    .line 1675782
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 1675777
    iget-object v0, p0, LX/ATO;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ASn;

    .line 1675778
    invoke-interface {v0}, LX/ASn;->h()V

    goto :goto_0

    .line 1675779
    :cond_0
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1675771
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1675772
    iget-object v0, p0, LX/ATO;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 1675773
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    if-lez v3, :cond_0

    .line 1675774
    const/16 v3, 0x2c

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1675775
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/1la;->a(Ljava/lang/String;)LX/1lW;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 1675776
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(Z)V
    .locals 9

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1675735
    iget-object v0, p0, LX/ATO;->r:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1675736
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;

    invoke-static {v1}, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->a(Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;)LX/9cC;

    move-result-object v6

    .line 1675737
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1675738
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_7

    const/4 v2, 0x1

    :goto_1
    invoke-static {v2}, LX/0PB;->checkArgument(Z)V

    .line 1675739
    iget-object v2, p0, LX/ATO;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 1675740
    invoke-virtual {v2}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/ipc/media/MediaItem;->d()Lcom/facebook/ipc/media/MediaIdKey;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/ipc/media/MediaIdKey;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 1675741
    :goto_2
    move-object v2, v2

    .line 1675742
    iput-boolean p1, v6, LX/9cC;->c:Z

    .line 1675743
    if-eqz v2, :cond_5

    .line 1675744
    iput-boolean v4, v6, LX/9cC;->b:Z

    .line 1675745
    invoke-virtual {v2}, Lcom/facebook/composer/attachments/ComposerAttachment;->e()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v7

    .line 1675746
    if-eqz v7, :cond_1

    .line 1675747
    invoke-virtual {v7}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getStickerParams()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    .line 1675748
    iput v2, v6, LX/9cC;->j:I

    .line 1675749
    move-object v2, v6

    .line 1675750
    invoke-virtual {v7}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getTextParams()LX/0Px;

    move-result-object v8

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v8

    .line 1675751
    iput v8, v2, LX/9cC;->k:I

    .line 1675752
    move-object v8, v2

    .line 1675753
    invoke-virtual {v7}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getCropBox()Lcom/facebook/photos/creativeediting/model/PersistableRect;

    move-result-object v2

    if-eqz v2, :cond_4

    move v2, v3

    .line 1675754
    :goto_3
    iput-boolean v2, v8, LX/9cC;->a:Z

    .line 1675755
    move-object v2, v8

    .line 1675756
    invoke-virtual {v7}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getFilterName()Ljava/lang/String;

    move-result-object v7

    .line 1675757
    iput-object v7, v2, LX/9cC;->n:Ljava/lang/String;

    .line 1675758
    :cond_1
    :goto_4
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;

    invoke-virtual {v2}, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1675759
    sget-object v2, LX/5Rz;->COMPOSER:LX/5Rz;

    .line 1675760
    iput-object v2, v6, LX/9cC;->o:LX/5Rz;

    .line 1675761
    :cond_2
    invoke-virtual {v6}, LX/9cC;->a()Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;

    move-result-object v6

    .line 1675762
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;

    invoke-virtual {v2}, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;->a()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1675763
    iget-object v2, p0, LX/ATO;->A:LX/9c9;

    invoke-virtual {v2, v1, v6}, LX/9c9;->a(Ljava/lang/String;Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;)V

    .line 1675764
    :cond_3
    invoke-interface {v0, v6}, Ljava/util/Map$Entry;->setValue(Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    :cond_4
    move v2, v4

    .line 1675765
    goto :goto_3

    .line 1675766
    :cond_5
    iput-boolean v3, v6, LX/9cC;->b:Z

    .line 1675767
    goto :goto_4

    .line 1675768
    :cond_6
    return-void

    .line 1675769
    :cond_7
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 1675770
    :cond_8
    const/4 v2, 0x0

    goto :goto_2
.end method

.method public final c()Ljava/lang/String;
    .locals 6
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1675729
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1675730
    iget-object v0, p0, LX/ATO;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 1675731
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    if-lez v3, :cond_0

    .line 1675732
    const/16 v3, 0x2c

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1675733
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->c()J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 1675734
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c(Z)V
    .locals 0

    .prologue
    .line 1675726
    iput-boolean p1, p0, LX/ATO;->E:Z

    .line 1675727
    invoke-direct {p0}, LX/ATO;->u()V

    .line 1675728
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 1675714
    iget-boolean v0, p0, LX/ATO;->Q:Z

    if-eqz v0, :cond_1

    .line 1675715
    iget-object v0, p0, LX/ATO;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ASn;

    .line 1675716
    invoke-interface {v0}, LX/ASn;->a()V

    goto :goto_0

    .line 1675717
    :cond_0
    iget-object v0, p0, LX/ATO;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1675718
    iget-object v0, p0, LX/ATO;->z:LX/ATl;

    invoke-virtual {v0}, LX/ATl;->removeAllViews()V

    .line 1675719
    iget-object v0, p0, LX/ATO;->G:LX/0wd;

    invoke-virtual {v0}, LX/0wd;->a()V

    .line 1675720
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/ATO;->Q:Z

    .line 1675721
    iget-object v0, p0, LX/ATO;->w:Lcom/facebook/widget/ScrollingAwareScrollView;

    invoke-virtual {v0}, Lcom/facebook/widget/ScrollingAwareScrollView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iget-object v1, p0, LX/ATO;->t:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 1675722
    iget-object v0, p0, LX/ATO;->ah:Lcom/facebook/facerec/manager/FaceBoxPrioritizer;

    const/4 v1, 0x0

    .line 1675723
    iput-object v1, v0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->n:LX/7yZ;

    .line 1675724
    iget-object v0, p0, LX/ATO;->ah:Lcom/facebook/facerec/manager/FaceBoxPrioritizer;

    invoke-virtual {v0}, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->b()V

    .line 1675725
    :cond_1
    return-void
.end method

.method public final h()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1675710
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 1675711
    iget-object v0, p0, LX/ATO;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ASn;

    .line 1675712
    invoke-interface {v0}, LX/ASn;->f()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1675713
    :cond_0
    return-object v1
.end method

.method public final l()V
    .locals 3

    .prologue
    .line 1675702
    iget-object v0, p0, LX/ATO;->N:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1675703
    :goto_0
    return-void

    .line 1675704
    :cond_0
    iget-object v0, p0, LX/ATO;->s:LX/8GT;

    iget-object v1, p0, LX/ATO;->N:Ljava/lang/String;

    const/4 v2, 0x0

    .line 1675705
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result p0

    if-nez p0, :cond_1

    const/4 v2, 0x1

    :cond_1
    invoke-static {v2}, LX/0PB;->checkArgument(Z)V

    .line 1675706
    const/4 v2, 0x0

    :try_start_0
    invoke-static {v0, v1, v2}, LX/8GT;->a(LX/8GT;Ljava/lang/String;Z)Ljava/io/File;

    move-result-object v2

    .line 1675707
    invoke-static {v2}, LX/2W9;->a(Ljava/io/File;)Z

    .line 1675708
    invoke-virtual {v2}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1675709
    :goto_1
    goto :goto_0

    :catch_0
    goto :goto_1
.end method

.method public final m()Z
    .locals 10

    .prologue
    .line 1675673
    iget-object v0, p0, LX/ATO;->aa:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 1675674
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, LX/0jA;

    invoke-interface {v1}, LX/0jA;->getSlideshowData()Lcom/facebook/ipc/composer/model/ComposerSlideshowData;

    move-result-object v1

    if-nez v1, :cond_0

    iget-object v2, p0, LX/ATO;->am:LX/ASb;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    invoke-interface {v1}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v1

    invoke-static {v1}, LX/7kq;->e(LX/0Px;)LX/0Px;

    move-result-object v1

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 1675675
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1675676
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v8

    move v7, v6

    :goto_0
    if-ge v7, v8, :cond_5

    invoke-virtual {v1, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/ipc/media/MediaItem;

    .line 1675677
    instance-of v4, v3, Lcom/facebook/photos/base/media/PhotoItem;

    if-eqz v4, :cond_4

    move-object v4, v3

    check-cast v4, Lcom/facebook/photos/base/media/PhotoItem;

    .line 1675678
    iget-object v9, v4, Lcom/facebook/photos/base/media/PhotoItem;->i:Lcom/facebook/photos/base/photos/LocalPhoto;

    move-object v4, v9

    .line 1675679
    instance-of v4, v4, Lcom/facebook/photos/base/photos/LocalPhoto;

    if-eqz v4, :cond_4

    .line 1675680
    check-cast v3, Lcom/facebook/photos/base/media/PhotoItem;

    .line 1675681
    iget-object v4, v3, Lcom/facebook/photos/base/media/PhotoItem;->i:Lcom/facebook/photos/base/photos/LocalPhoto;

    move-object v3, v4

    .line 1675682
    check-cast v3, Lcom/facebook/photos/base/photos/LocalPhoto;

    .line 1675683
    iget-object v4, v2, LX/ASb;->a:LX/75F;

    invoke-virtual {v4, v3}, LX/75F;->a(LX/74x;)Ljava/util/List;

    move-result-object v4

    .line 1675684
    if-nez v4, :cond_2

    move v3, v5

    .line 1675685
    :goto_1
    move v1, v3

    .line 1675686
    if-nez v1, :cond_1

    .line 1675687
    :cond_0
    const/4 v0, 0x0

    .line 1675688
    :goto_2
    return v0

    .line 1675689
    :cond_1
    iget-object v1, p0, LX/ATO;->ah:Lcom/facebook/facerec/manager/FaceBoxPrioritizer;

    invoke-virtual {v1}, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->d()V

    .line 1675690
    invoke-virtual {p0}, LX/ATO;->p()V

    .line 1675691
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    invoke-interface {v0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/7kq;->b(LX/0Px;)LX/0Px;

    move-result-object v0

    sget-object v1, LX/03R;->YES:LX/03R;

    invoke-virtual {p0, v0, v1}, LX/ATO;->a(LX/0Px;LX/03R;)V

    .line 1675692
    const/4 v0, 0x1

    goto :goto_2

    .line 1675693
    :cond_2
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_4

    .line 1675694
    iget-boolean v9, v3, Lcom/facebook/photos/base/photos/LocalPhoto;->f:Z

    move v3, v9

    .line 1675695
    if-nez v3, :cond_4

    .line 1675696
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/photos/base/tagging/FaceBox;

    .line 1675697
    iget-boolean v9, v3, Lcom/facebook/photos/base/tagging/FaceBox;->f:Z

    move v3, v9

    .line 1675698
    if-nez v3, :cond_3

    move v3, v5

    .line 1675699
    goto :goto_1

    .line 1675700
    :cond_4
    add-int/lit8 v3, v7, 0x1

    move v7, v3

    goto :goto_0

    :cond_5
    move v3, v6

    .line 1675701
    goto :goto_1
.end method

.method public final n()V
    .locals 1

    .prologue
    .line 1675671
    iget-object v0, p0, LX/ATO;->ah:Lcom/facebook/facerec/manager/FaceBoxPrioritizer;

    invoke-virtual {v0}, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->e()V

    .line 1675672
    return-void
.end method

.method public final p()V
    .locals 8

    .prologue
    .line 1675645
    iget-object v0, p0, LX/ATO;->aa:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, LX/0il;

    .line 1675646
    invoke-interface {v4}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    invoke-interface {v0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/7kq;->e(LX/0Px;)LX/0Px;

    move-result-object v2

    .line 1675647
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1675648
    :goto_0
    return-void

    .line 1675649
    :cond_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1675650
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    .line 1675651
    instance-of v1, v0, Lcom/facebook/photos/base/media/PhotoItem;

    if-eqz v1, :cond_1

    .line 1675652
    check-cast v0, Lcom/facebook/photos/base/media/PhotoItem;

    .line 1675653
    iget-object v1, v0, Lcom/facebook/photos/base/media/PhotoItem;->i:Lcom/facebook/photos/base/photos/LocalPhoto;

    move-object v1, v1

    .line 1675654
    check-cast v1, Lcom/facebook/photos/base/photos/LocalPhoto;

    .line 1675655
    iget-boolean v6, v1, Lcom/facebook/photos/base/photos/LocalPhoto;->f:Z

    move v6, v6

    .line 1675656
    if-eqz v6, :cond_2

    iget-object v6, p0, LX/ATO;->ai:LX/75F;

    invoke-virtual {v6, v1}, LX/75F;->c(LX/74x;)Z

    move-result v6

    if-eqz v6, :cond_2

    iget-object v6, p0, LX/ATO;->ai:LX/75F;

    invoke-virtual {v6, v1}, LX/75F;->d(LX/74x;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1675657
    :cond_2
    new-instance v1, LX/7ye;

    iget-object v6, p0, LX/ATO;->aj:LX/75Q;

    iget-object v7, p0, LX/ATO;->ai:LX/75F;

    invoke-direct {v1, v0, v6, v7}, LX/7ye;-><init>(Lcom/facebook/photos/base/media/PhotoItem;LX/75Q;LX/75F;)V

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1675658
    :cond_3
    iget-object v0, p0, LX/ATO;->ah:Lcom/facebook/facerec/manager/FaceBoxPrioritizer;

    new-instance v1, LX/ATJ;

    invoke-direct {v1, p0, v4, v2}, LX/ATJ;-><init>(LX/ATO;LX/0il;Ljava/util/List;)V

    .line 1675659
    iput-object v1, v0, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->n:LX/7yZ;

    .line 1675660
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 1675661
    iget-object v0, p0, LX/ATO;->ah:Lcom/facebook/facerec/manager/FaceBoxPrioritizer;

    invoke-virtual {v0, v3}, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->a(Ljava/util/List;)V

    .line 1675662
    iget-object v0, p0, LX/ATO;->am:LX/ASb;

    .line 1675663
    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1675664
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, v0, LX/ASb;->b:Ljava/util/List;

    .line 1675665
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/7ye;

    .line 1675666
    iget-object v4, v0, LX/ASb;->b:Ljava/util/List;

    .line 1675667
    iget-object v3, v1, LX/7ye;->a:Ljava/lang/String;

    move-object v1, v3

    .line 1675668
    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1675669
    :cond_4
    goto :goto_0

    .line 1675670
    :cond_5
    iget-object v0, p0, LX/ATO;->ak:LX/1EZ;

    invoke-interface {v4}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    invoke-interface {v1}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v1

    invoke-static {v1}, LX/7kq;->e(LX/0Px;)LX/0Px;

    move-result-object v1

    invoke-interface {v4}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0io;

    check-cast v2, LX/0j0;

    invoke-interface {v2}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v4}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0io;

    check-cast v3, LX/0j4;

    invoke-interface {v3}, LX/0j4;->getPageData()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v3

    invoke-static {v3}, LX/2rm;->a(Lcom/facebook/ipc/composer/intent/ComposerPageData;)Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface {v4}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0io;

    check-cast v3, LX/0j4;

    invoke-interface {v3}, LX/0j4;->getPageData()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v3

    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/ipc/composer/intent/ComposerPageData;

    invoke-virtual {v3}, Lcom/facebook/ipc/composer/intent/ComposerPageData;->getPostAsPageViewerContext()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v3

    :goto_3
    invoke-interface {v4}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0io;

    check-cast v4, LX/0j6;

    invoke-interface {v4}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v4

    iget-wide v4, v4, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    invoke-virtual/range {v0 .. v5}, LX/1EZ;->a(LX/0Px;Ljava/lang/String;Lcom/facebook/auth/viewercontext/ViewerContext;J)V

    goto/16 :goto_0

    :cond_6
    const/4 v3, 0x0

    goto :goto_3
.end method
