.class public LX/Bp0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Boz;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:LX/CaG;

.field private final c:Landroid/content/Context;

.field private final d:LX/BpF;

.field private final e:LX/9hM;

.field private final f:LX/9hN;

.field private final g:Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;

.field private final h:Ljava/util/concurrent/Callable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Callable",
            "<",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;>;"
        }
    .end annotation
.end field

.field private final i:I

.field private final j:Ljava/lang/String;

.field private final k:Z

.field private final l:LX/03V;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1822548
    const-class v0, LX/Bp0;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Bp0;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/CaG;LX/03V;Landroid/content/Context;LX/BpF;LX/9hM;LX/9hN;Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;ILjava/lang/String;ZLjava/util/concurrent/Callable;)V
    .locals 0
    .param p3    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/BpF;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/9hM;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/9hN;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p8    # I
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p9    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p10    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p11    # Ljava/util/concurrent/Callable;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CaG;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "Landroid/content/Context;",
            "LX/BpF;",
            "LX/9hM;",
            "LX/9hN;",
            "Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;",
            "I",
            "Ljava/lang/String;",
            "Z",
            "Ljava/util/concurrent/Callable",
            "<",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;>;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1822549
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1822550
    iput-object p1, p0, LX/Bp0;->b:LX/CaG;

    .line 1822551
    iput-object p3, p0, LX/Bp0;->c:Landroid/content/Context;

    .line 1822552
    iput-object p4, p0, LX/Bp0;->d:LX/BpF;

    .line 1822553
    iput-object p5, p0, LX/Bp0;->e:LX/9hM;

    .line 1822554
    iput-object p6, p0, LX/Bp0;->f:LX/9hN;

    .line 1822555
    iput-object p7, p0, LX/Bp0;->g:Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;

    .line 1822556
    iput p8, p0, LX/Bp0;->i:I

    .line 1822557
    iput-object p9, p0, LX/Bp0;->j:Ljava/lang/String;

    .line 1822558
    iput-object p11, p0, LX/Bp0;->h:Ljava/util/concurrent/Callable;

    .line 1822559
    iput-boolean p10, p0, LX/Bp0;->k:Z

    .line 1822560
    iput-object p2, p0, LX/Bp0;->l:LX/03V;

    .line 1822561
    return-void
.end method

.method private a(LX/0Px;)LX/0Px;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/5kD;",
            ">;)",
            "LX/0Px",
            "<",
            "LX/5kD;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1822562
    iget-boolean v0, p0, LX/Bp0;->k:Z

    if-eqz v0, :cond_0

    .line 1822563
    :goto_0
    return-object p1

    .line 1822564
    :cond_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1822565
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_2

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5kD;

    .line 1822566
    invoke-interface {v0}, LX/5kD;->S()Z

    move-result v4

    if-nez v4, :cond_1

    .line 1822567
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1822568
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1822569
    :cond_2
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object p1

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/5kD;LX/1bf;ZI)V
    .locals 14

    .prologue
    .line 1822570
    const/4 v13, 0x0

    .line 1822571
    :try_start_0
    iget-object v0, p0, LX/Bp0;->h:Ljava/util/concurrent/Callable;

    invoke-interface {v0}, Ljava/util/concurrent/Callable;->call()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v13, v0

    .line 1822572
    :goto_0
    iget-object v0, p0, LX/Bp0;->b:LX/CaG;

    iget-object v1, p0, LX/Bp0;->c:Landroid/content/Context;

    iget-object v2, p0, LX/Bp0;->g:Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;

    iget-object v3, p0, LX/Bp0;->d:LX/BpF;

    invoke-interface {v3}, LX/0g1;->size()I

    move-result v3

    if-nez v3, :cond_0

    invoke-static {p1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    :goto_1
    invoke-interface {p1}, LX/5kD;->d()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, LX/Bp0;->f:LX/9hN;

    iget-object v9, p0, LX/Bp0;->e:LX/9hM;

    sget-object v10, LX/74S;->PHOTOS_FEED:LX/74S;

    iget v11, p0, LX/Bp0;->i:I

    iget-object v12, p0, LX/Bp0;->j:Ljava/lang/String;

    move-object/from16 v6, p2

    move/from16 v7, p3

    move/from16 v8, p4

    invoke-virtual/range {v0 .. v13}, LX/CaG;->a(Landroid/content/Context;Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;LX/0Px;Ljava/lang/String;LX/9hN;LX/1bf;ZILX/9hM;LX/74S;ILjava/lang/String;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1822573
    return-void

    .line 1822574
    :catch_0
    move-exception v0

    .line 1822575
    iget-object v1, p0, LX/Bp0;->l:LX/03V;

    sget-object v2, LX/Bp0;->a:Ljava/lang/String;

    const-string v3, "mStoryCallable threw an exception"

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1822576
    :cond_0
    iget-object v3, p0, LX/Bp0;->d:LX/BpF;

    invoke-interface {v3}, LX/BpF;->b()LX/0Px;

    move-result-object v3

    invoke-direct {p0, v3}, LX/Bp0;->a(LX/0Px;)LX/0Px;

    move-result-object v3

    goto :goto_1
.end method
