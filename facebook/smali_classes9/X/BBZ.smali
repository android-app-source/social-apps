.class public final LX/BBZ;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 25

    .prologue
    .line 1757458
    const/16 v19, 0x0

    .line 1757459
    const/16 v18, 0x0

    .line 1757460
    const/16 v17, 0x0

    .line 1757461
    const/16 v16, 0x0

    .line 1757462
    const/4 v15, 0x0

    .line 1757463
    const/4 v14, 0x0

    .line 1757464
    const/4 v13, 0x0

    .line 1757465
    const/4 v12, 0x0

    .line 1757466
    const/4 v9, 0x0

    .line 1757467
    const-wide/16 v10, 0x0

    .line 1757468
    const/4 v8, 0x0

    .line 1757469
    const/4 v7, 0x0

    .line 1757470
    const/4 v6, 0x0

    .line 1757471
    const/4 v5, 0x0

    .line 1757472
    const/4 v4, 0x0

    .line 1757473
    const/4 v3, 0x0

    .line 1757474
    const/4 v2, 0x0

    .line 1757475
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v20

    sget-object v21, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    if-eq v0, v1, :cond_13

    .line 1757476
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1757477
    const/4 v2, 0x0

    .line 1757478
    :goto_0
    return v2

    .line 1757479
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v21, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v21

    if-eq v7, v0, :cond_f

    .line 1757480
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 1757481
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1757482
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v21

    sget-object v22, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    if-eq v0, v1, :cond_0

    if-eqz v7, :cond_0

    .line 1757483
    const-string v21, "app_icon"

    move-object/from16 v0, v21

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_1

    .line 1757484
    invoke-static/range {p0 .. p1}, LX/BBR;->a(LX/15w;LX/186;)I

    move-result v7

    move/from16 v20, v7

    goto :goto_1

    .line 1757485
    :cond_1
    const-string v21, "cache_id"

    move-object/from16 v0, v21

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_2

    .line 1757486
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    move/from16 v19, v7

    goto :goto_1

    .line 1757487
    :cond_2
    const-string v21, "eligible_buckets"

    move-object/from16 v0, v21

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_3

    .line 1757488
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v7

    move/from16 v18, v7

    goto :goto_1

    .line 1757489
    :cond_3
    const-string v21, "id"

    move-object/from16 v0, v21

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_4

    .line 1757490
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    move/from16 v17, v7

    goto :goto_1

    .line 1757491
    :cond_4
    const-string v21, "local_is_rich_notif_collapsed"

    move-object/from16 v0, v21

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_5

    .line 1757492
    const/4 v6, 0x1

    .line 1757493
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v7

    move/from16 v16, v7

    goto :goto_1

    .line 1757494
    :cond_5
    const-string v21, "local_num_impressions"

    move-object/from16 v0, v21

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_6

    .line 1757495
    const/4 v3, 0x1

    .line 1757496
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v7

    move v15, v7

    goto/16 :goto_1

    .line 1757497
    :cond_6
    const-string v21, "navigation_endpoint"

    move-object/from16 v0, v21

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_7

    .line 1757498
    invoke-static/range {p0 .. p1}, LX/BBT;->a(LX/15w;LX/186;)I

    move-result v7

    move v14, v7

    goto/16 :goto_1

    .line 1757499
    :cond_7
    const-string v21, "notif_image"

    move-object/from16 v0, v21

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_8

    .line 1757500
    invoke-static/range {p0 .. p1}, LX/BBU;->a(LX/15w;LX/186;)I

    move-result v7

    move v13, v7

    goto/16 :goto_1

    .line 1757501
    :cond_8
    const-string v21, "notif_option_sets"

    move-object/from16 v0, v21

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_9

    .line 1757502
    invoke-static/range {p0 .. p1}, LX/BBW;->a(LX/15w;LX/186;)I

    move-result v7

    move v12, v7

    goto/16 :goto_1

    .line 1757503
    :cond_9
    const-string v21, "notif_sent_time"

    move-object/from16 v0, v21

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_a

    .line 1757504
    const/4 v2, 0x1

    .line 1757505
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    goto/16 :goto_1

    .line 1757506
    :cond_a
    const-string v21, "rich_notification"

    move-object/from16 v0, v21

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_b

    .line 1757507
    invoke-static/range {p0 .. p1}, LX/BDV;->a(LX/15w;LX/186;)I

    move-result v7

    move v11, v7

    goto/16 :goto_1

    .line 1757508
    :cond_b
    const-string v21, "seen_state"

    move-object/from16 v0, v21

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_c

    .line 1757509
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v7

    move v10, v7

    goto/16 :goto_1

    .line 1757510
    :cond_c
    const-string v21, "title"

    move-object/from16 v0, v21

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_d

    .line 1757511
    invoke-static/range {p0 .. p1}, LX/BBY;->a(LX/15w;LX/186;)I

    move-result v7

    move v9, v7

    goto/16 :goto_1

    .line 1757512
    :cond_d
    const-string v21, "tracking"

    move-object/from16 v0, v21

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_e

    .line 1757513
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    move v8, v7

    goto/16 :goto_1

    .line 1757514
    :cond_e
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 1757515
    :cond_f
    const/16 v7, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->c(I)V

    .line 1757516
    const/4 v7, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v7, v1}, LX/186;->b(II)V

    .line 1757517
    const/4 v7, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v7, v1}, LX/186;->b(II)V

    .line 1757518
    const/4 v7, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v7, v1}, LX/186;->b(II)V

    .line 1757519
    const/4 v7, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v7, v1}, LX/186;->b(II)V

    .line 1757520
    if-eqz v6, :cond_10

    .line 1757521
    const/4 v6, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v6, v1}, LX/186;->a(IZ)V

    .line 1757522
    :cond_10
    if-eqz v3, :cond_11

    .line 1757523
    const/4 v3, 0x5

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v15, v6}, LX/186;->a(III)V

    .line 1757524
    :cond_11
    const/4 v3, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v14}, LX/186;->b(II)V

    .line 1757525
    const/4 v3, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v13}, LX/186;->b(II)V

    .line 1757526
    const/16 v3, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v12}, LX/186;->b(II)V

    .line 1757527
    if-eqz v2, :cond_12

    .line 1757528
    const/16 v3, 0x9

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1757529
    :cond_12
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 1757530
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 1757531
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 1757532
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 1757533
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_13
    move/from16 v20, v19

    move/from16 v19, v18

    move/from16 v18, v17

    move/from16 v17, v16

    move/from16 v16, v15

    move v15, v14

    move v14, v13

    move v13, v12

    move v12, v9

    move v9, v6

    move v6, v4

    move-wide/from16 v23, v10

    move v10, v7

    move v11, v8

    move v8, v5

    move-wide/from16 v4, v23

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/16 v4, 0xb

    const/4 v3, 0x2

    const/4 v2, 0x0

    .line 1757534
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1757535
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1757536
    if-eqz v0, :cond_0

    .line 1757537
    const-string v1, "app_icon"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1757538
    invoke-static {p0, v0, p2}, LX/BBR;->a(LX/15i;ILX/0nX;)V

    .line 1757539
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1757540
    if-eqz v0, :cond_1

    .line 1757541
    const-string v1, "cache_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1757542
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1757543
    :cond_1
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 1757544
    if-eqz v0, :cond_2

    .line 1757545
    const-string v0, "eligible_buckets"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1757546
    invoke-virtual {p0, p1, v3}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 1757547
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1757548
    if-eqz v0, :cond_3

    .line 1757549
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1757550
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1757551
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1757552
    if-eqz v0, :cond_4

    .line 1757553
    const-string v1, "local_is_rich_notif_collapsed"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1757554
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1757555
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1757556
    if-eqz v0, :cond_5

    .line 1757557
    const-string v1, "local_num_impressions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1757558
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1757559
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1757560
    if-eqz v0, :cond_6

    .line 1757561
    const-string v1, "navigation_endpoint"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1757562
    invoke-static {p0, v0, p2, p3}, LX/BBT;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1757563
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1757564
    if-eqz v0, :cond_7

    .line 1757565
    const-string v1, "notif_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1757566
    invoke-static {p0, v0, p2}, LX/BBU;->a(LX/15i;ILX/0nX;)V

    .line 1757567
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1757568
    if-eqz v0, :cond_8

    .line 1757569
    const-string v1, "notif_option_sets"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1757570
    invoke-static {p0, v0, p2, p3}, LX/BBW;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1757571
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0, v6, v7}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 1757572
    cmp-long v2, v0, v6

    if-eqz v2, :cond_9

    .line 1757573
    const-string v2, "notif_sent_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1757574
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 1757575
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1757576
    if-eqz v0, :cond_a

    .line 1757577
    const-string v1, "rich_notification"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1757578
    invoke-static {p0, v0, p2, p3}, LX/BDV;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1757579
    :cond_a
    invoke-virtual {p0, p1, v4}, LX/15i;->g(II)I

    move-result v0

    .line 1757580
    if-eqz v0, :cond_b

    .line 1757581
    const-string v0, "seen_state"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1757582
    invoke-virtual {p0, p1, v4}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1757583
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1757584
    if-eqz v0, :cond_c

    .line 1757585
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1757586
    invoke-static {p0, v0, p2, p3}, LX/BBY;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1757587
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1757588
    if-eqz v0, :cond_d

    .line 1757589
    const-string v1, "tracking"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1757590
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1757591
    :cond_d
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1757592
    return-void
.end method
