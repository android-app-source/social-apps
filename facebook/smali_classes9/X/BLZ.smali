.class public final LX/BLZ;
.super LX/8RF;
.source ""


# instance fields
.field public a:LX/8Rf;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1775557
    const/4 v0, 0x1

    invoke-direct {p0, v0}, LX/8RF;-><init>(Z)V

    .line 1775558
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;LX/8QL;Z)V
    .locals 1

    .prologue
    .line 1775548
    invoke-super {p0, p1, p2, p3}, LX/8RF;->a(Landroid/view/View;LX/8QL;Z)V

    .line 1775549
    instance-of v0, p2, LX/8QO;

    if-nez v0, :cond_0

    instance-of v0, p2, LX/8QX;

    if-nez v0, :cond_0

    instance-of v0, p2, LX/8QN;

    if-eqz v0, :cond_1

    .line 1775550
    :cond_0
    iget-object v0, p0, LX/BLZ;->a:LX/8Rf;

    .line 1775551
    iput-object v0, p2, LX/8QK;->b:LX/8Re;

    .line 1775552
    check-cast p1, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadItemRow;

    invoke-virtual {p1}, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadItemRow;->b()V

    .line 1775553
    :cond_1
    return-void
.end method

.method public final b(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 1775554
    new-instance v0, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadSubtitledItemRow;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f03152f

    invoke-direct {v0, v1, v2}, Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadSubtitledItemRow;-><init>(Landroid/content/Context;I)V

    return-object v0
.end method

.method public final c(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 1775555
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1775556
    const v1, 0x7f030fd1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
