.class public LX/Cbs;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/23P;

.field private final c:LX/0ad;

.field private final d:LX/5SF;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/23P;LX/0ad;LX/5SF;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1920450
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1920451
    iput-object p1, p0, LX/Cbs;->a:Landroid/content/Context;

    .line 1920452
    iput-object p2, p0, LX/Cbs;->b:LX/23P;

    .line 1920453
    iput-object p3, p0, LX/Cbs;->c:LX/0ad;

    .line 1920454
    iput-object p4, p0, LX/Cbs;->d:LX/5SF;

    .line 1920455
    return-void
.end method

.method public static a(LX/0QB;)LX/Cbs;
    .locals 1

    .prologue
    .line 1920456
    invoke-static {p0}, LX/Cbs;->b(LX/0QB;)LX/Cbs;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/Cbs;
    .locals 5

    .prologue
    .line 1920457
    new-instance v4, LX/Cbs;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/23P;->b(LX/0QB;)LX/23P;

    move-result-object v1

    check-cast v1, LX/23P;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v2

    check-cast v2, LX/0ad;

    invoke-static {p0}, LX/BQz;->a(LX/0QB;)LX/BQz;

    move-result-object v3

    check-cast v3, LX/5SF;

    invoke-direct {v4, v0, v1, v2, v3}, LX/Cbs;-><init>(Landroid/content/Context;LX/23P;LX/0ad;LX/5SF;)V

    .line 1920458
    return-object v4
.end method


# virtual methods
.method public final a(LX/5kD;)Landroid/content/Intent;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1920459
    iget-object v0, p0, LX/Cbs;->c:LX/0ad;

    sget-short v2, LX/0wf;->c:S

    invoke-interface {v0, v2, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    .line 1920460
    :goto_0
    new-instance v2, LX/5SJ;

    invoke-direct {v2}, LX/5SJ;-><init>()V

    invoke-interface {p1}, LX/5kD;->aj_()LX/1Fb;

    move-result-object v3

    invoke-interface {v3}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-interface {p1}, LX/5kD;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/5SJ;->a(Landroid/net/Uri;Ljava/lang/String;)LX/5SJ;

    move-result-object v2

    sget-object v3, LX/5Rz;->PROFILE:LX/5Rz;

    invoke-virtual {v3}, LX/5Rz;->name()Ljava/lang/String;

    move-result-object v3

    .line 1920461
    iput-object v3, v2, LX/5SJ;->c:Ljava/lang/String;

    .line 1920462
    move-object v2, v2

    .line 1920463
    iput-boolean v1, v2, LX/5SJ;->o:Z

    .line 1920464
    move-object v1, v2

    .line 1920465
    iput v0, v1, LX/5SJ;->q:I

    .line 1920466
    move-object v0, v1

    .line 1920467
    invoke-virtual {v0}, LX/5SJ;->b()Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;

    move-result-object v0

    .line 1920468
    new-instance v1, LX/5Rw;

    invoke-direct {v1}, LX/5Rw;-><init>()V

    sget-object v2, LX/5Rr;->CROP:LX/5Rr;

    invoke-virtual {v1, v2}, LX/5Rw;->a(LX/5Rr;)LX/5Rw;

    move-result-object v1

    sget-object v2, LX/5Rq;->ZOOM_CROP:LX/5Rq;

    invoke-virtual {v1, v2}, LX/5Rw;->a(LX/5Rq;)LX/5Rw;

    move-result-object v1

    invoke-virtual {v1}, LX/5Rw;->a()Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;

    move-result-object v1

    .line 1920469
    iget-object v2, p0, LX/Cbs;->d:LX/5SF;

    iget-object v3, p0, LX/Cbs;->a:Landroid/content/Context;

    invoke-interface {v2, v3, v0, v1}, LX/5SF;->a(Landroid/content/Context;Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;)Landroid/content/Intent;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v1

    .line 1920470
    goto :goto_0
.end method
