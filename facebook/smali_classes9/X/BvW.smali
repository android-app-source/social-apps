.class public LX/BvW;
.super LX/7N3;
.source ""


# instance fields
.field public final o:Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;

.field private final p:Landroid/view/View;

.field public final q:Landroid/view/animation/Animation;

.field public final r:Landroid/view/animation/Animation;

.field public s:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1832364
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/BvW;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1832365
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1832362
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/BvW;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1832363
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 1832352
    invoke-direct {p0, p1, p2, p3}, LX/7N3;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1832353
    const v0, 0x7f0d19b2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;

    iput-object v0, p0, LX/BvW;->o:Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;

    .line 1832354
    const v0, 0x7f0d19b6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/BvW;->p:Landroid/view/View;

    .line 1832355
    iget-object v0, p0, LX/BvW;->p:Landroid/view/View;

    new-instance v1, LX/BvR;

    invoke-direct {v1, p0}, LX/BvR;-><init>(LX/BvW;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1832356
    const v0, 0x7f040004

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, LX/BvW;->q:Landroid/view/animation/Animation;

    .line 1832357
    const v0, 0x7f040006

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, LX/BvW;->r:Landroid/view/animation/Animation;

    .line 1832358
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/BvU;

    invoke-direct {v1, p0}, LX/BvU;-><init>(LX/BvW;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1832359
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/BvV;

    invoke-direct {v1, p0}, LX/BvV;-><init>(LX/BvW;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1832360
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/BvT;

    invoke-direct {v1, p0}, LX/BvT;-><init>(LX/BvW;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1832361
    return-void
.end method

.method public static e(LX/BvW;I)V
    .locals 2

    .prologue
    .line 1832347
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 1832348
    iget-object v0, p0, LX/7MM;->b:LX/7Me;

    sget-object v1, LX/7OE;->CENTER:LX/7OE;

    invoke-virtual {v0, v1}, LX/7Me;->setVideoPluginAlignment(LX/7OE;)V

    .line 1832349
    :goto_0
    iget-object v0, p0, LX/BvW;->o:Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;

    invoke-virtual {p0}, LX/BvW;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    invoke-virtual {v0, v1}, Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;->c(I)V

    .line 1832350
    return-void

    .line 1832351
    :cond_0
    iget-object v0, p0, LX/7MM;->b:LX/7Me;

    sget-object v1, LX/7OE;->TOP:LX/7OE;

    invoke-virtual {v0, v1}, LX/7Me;->setVideoPluginAlignment(LX/7OE;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/2pa;Z)V
    .locals 2

    .prologue
    .line 1832342
    invoke-super {p0, p1, p2}, LX/7N3;->a(LX/2pa;Z)V

    .line 1832343
    if-eqz p2, :cond_0

    .line 1832344
    invoke-virtual {p0}, LX/BvW;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    invoke-static {p0, v0}, LX/BvW;->e(LX/BvW;I)V

    .line 1832345
    :cond_0
    iget-object v0, p0, LX/BvW;->o:Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;

    invoke-static {p1}, LX/393;->a(LX/2pa;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;->setMetadata(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1832346
    return-void
.end method

.method public final a(Landroid/view/ViewGroup;)V
    .locals 1

    .prologue
    .line 1832320
    invoke-super {p0, p1}, LX/7N3;->a(Landroid/view/ViewGroup;)V

    .line 1832321
    const v0, 0x7f0d268a

    invoke-virtual {p0, v0}, LX/2oX;->setInnerResource(I)V

    .line 1832322
    return-void
.end method

.method public final c(I)V
    .locals 3

    .prologue
    .line 1832336
    iget-boolean v0, p0, LX/BvW;->s:Z

    if-eqz v0, :cond_1

    .line 1832337
    :cond_0
    :goto_0
    return-void

    .line 1832338
    :cond_1
    iget-object v0, p0, LX/BvW;->o:Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;

    iget-object v1, p0, LX/BvW;->r:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1832339
    invoke-super {p0, p1}, LX/7N3;->c(I)V

    .line 1832340
    iget-object v0, p0, LX/2oy;->i:LX/2oj;

    if-eqz v0, :cond_0

    .line 1832341
    iget-object v0, p0, LX/2oy;->i:LX/2oj;

    new-instance v1, LX/7M9;

    sget-object v2, LX/7M8;->FADE_OUT:LX/7M8;

    invoke-direct {v1, v2}, LX/7M9;-><init>(LX/7M8;)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    goto :goto_0
.end method

.method public final d(I)V
    .locals 3

    .prologue
    .line 1832328
    iget-boolean v0, p0, LX/BvW;->s:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/7N3;->c:Z

    if-eqz v0, :cond_1

    .line 1832329
    :cond_0
    :goto_0
    return-void

    .line 1832330
    :cond_1
    iget-object v0, p0, LX/BvW;->o:Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;->a()V

    .line 1832331
    iget-boolean v0, p0, LX/7N3;->c:Z

    if-eqz v0, :cond_2

    .line 1832332
    :goto_1
    invoke-super {p0, p1}, LX/7N3;->d(I)V

    .line 1832333
    iget-object v0, p0, LX/2oy;->i:LX/2oj;

    if-eqz v0, :cond_0

    .line 1832334
    iget-object v0, p0, LX/2oy;->i:LX/2oj;

    new-instance v1, LX/7M9;

    sget-object v2, LX/7M8;->FADE_IN:LX/7M8;

    invoke-direct {v1, v2}, LX/7M9;-><init>(LX/7M8;)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    goto :goto_0

    .line 1832335
    :cond_2
    iget-object v0, p0, LX/BvW;->o:Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;

    iget-object v1, p0, LX/BvW;->q:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_1
.end method

.method public getContentView()I
    .locals 1

    .prologue
    .line 1832327
    const v0, 0x7f03100b

    return v0
.end method

.method public final h()V
    .locals 0

    .prologue
    .line 1832325
    invoke-virtual {p0}, LX/7N3;->i()V

    .line 1832326
    return-void
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, 0x2173f46a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1832324
    const/4 v1, 0x0

    const v2, 0x6a1c186d

    invoke-static {v3, v3, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return v1
.end method

.method public final r()Z
    .locals 1

    .prologue
    .line 1832323
    const/4 v0, 0x1

    return v0
.end method
