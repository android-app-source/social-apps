.class public final LX/B49;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/heisman/protocol/ProfileOverlayCategoryGraphQLModels$ProfileOverlayCategoryFieldsModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/1DI;

.field public final synthetic b:LX/B4A;


# direct methods
.method public constructor <init>(LX/B4A;LX/1DI;)V
    .locals 0

    .prologue
    .line 1741294
    iput-object p1, p0, LX/B49;->b:LX/B4A;

    iput-object p2, p0, LX/B49;->a:LX/1DI;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1741295
    instance-of v0, p1, Ljava/io/IOException;

    if-nez v0, :cond_0

    .line 1741296
    iget-object v0, p0, LX/B49;->b:LX/B4A;

    iget-object v0, v0, LX/B4A;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "pivot_load_failed"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1741297
    :cond_0
    invoke-static {}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;->newBuilder()LX/62Q;

    move-result-object v0

    sget-object v1, LX/1lD;->ERROR:LX/1lD;

    .line 1741298
    iput-object v1, v0, LX/62Q;->a:LX/1lD;

    .line 1741299
    move-object v0, v0

    .line 1741300
    iget-object v1, p0, LX/B49;->b:LX/B4A;

    iget-object v1, v1, LX/B4A;->c:Landroid/app/Activity;

    const v2, 0x7f080039

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1741301
    iput-object v1, v0, LX/62Q;->b:Ljava/lang/String;

    .line 1741302
    move-object v0, v0

    .line 1741303
    invoke-virtual {v0}, LX/62Q;->a()Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;

    move-result-object v0

    .line 1741304
    iget-object v1, p0, LX/B49;->b:LX/B4A;

    iget-object v1, v1, LX/B4A;->g:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iget-object v2, p0, LX/B49;->a:LX/1DI;

    invoke-virtual {v1, v0, v2}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a(Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;LX/1DI;)V

    .line 1741305
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 7

    .prologue
    .line 1741306
    check-cast p1, Lcom/facebook/heisman/protocol/ProfileOverlayCategoryGraphQLModels$ProfileOverlayCategoryFieldsModel;

    .line 1741307
    invoke-virtual {p1}, Lcom/facebook/heisman/protocol/ProfileOverlayCategoryGraphQLModels$ProfileOverlayCategoryFieldsModel;->k()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1741308
    iget-object v0, p0, LX/B49;->b:LX/B4A;

    iget-object v0, v0, LX/B4A;->d:LX/B3T;

    invoke-virtual {p1}, Lcom/facebook/heisman/protocol/ProfileOverlayCategoryGraphQLModels$ProfileOverlayCategoryFieldsModel;->k()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LX/B3T;->b(Ljava/lang/String;)V

    .line 1741309
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/heisman/protocol/ProfileOverlayCategoryGraphQLModels$ProfileOverlayCategoryFieldsModel;->j()Lcom/facebook/heisman/protocol/ProfileOverlayCategoryGraphQLModels$ProfileOverlayCategoryPagesConnectionFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/heisman/protocol/ProfileOverlayCategoryGraphQLModels$ProfileOverlayCategoryPagesConnectionFieldsModel;->a()LX/0Px;

    move-result-object v2

    .line 1741310
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1741311
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_3

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/heisman/protocol/ProfileOverlayCategoryGraphQLModels$ProfileOverlayCategoryPageFieldsModel;

    .line 1741312
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/heisman/protocol/ProfileOverlayCategoryGraphQLModels$ProfileOverlayCategoryPageFieldsModel;->d()Lcom/facebook/heisman/protocol/ProfileOverlayCategoryGraphQLModels$PageProfilePictureOverlaysConnectionFieldsModel;

    move-result-object v5

    if-eqz v5, :cond_1

    invoke-virtual {v0}, Lcom/facebook/heisman/protocol/ProfileOverlayCategoryGraphQLModels$ProfileOverlayCategoryPageFieldsModel;->d()Lcom/facebook/heisman/protocol/ProfileOverlayCategoryGraphQLModels$PageProfilePictureOverlaysConnectionFieldsModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/heisman/protocol/ProfileOverlayCategoryGraphQLModels$PageProfilePictureOverlaysConnectionFieldsModel;->a()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1741313
    :cond_1
    iget-object v0, p0, LX/B49;->b:LX/B4A;

    iget-object v0, v0, LX/B4A;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "Fail to fetch overlay fileds"

    invoke-virtual {v0, v5, v6}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1741314
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1741315
    :cond_2
    invoke-static {v0}, LX/B3w;->a(Lcom/facebook/heisman/protocol/ProfileOverlayCategoryGraphQLModels$ProfileOverlayCategoryPageFieldsModel;)Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1741316
    :cond_3
    iget-object v0, p0, LX/B49;->b:LX/B4A;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v1

    .line 1741317
    iput v1, v0, LX/B4A;->q:I

    .line 1741318
    iget-object v0, p0, LX/B49;->b:LX/B4A;

    iget-object v1, p0, LX/B49;->b:LX/B4A;

    iget-object v1, v1, LX/B4A;->k:LX/B41;

    invoke-static {v3}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    iget-object v3, p0, LX/B49;->b:LX/B4A;

    .line 1741319
    new-instance v5, LX/B40;

    invoke-static {v1}, LX/1PK;->b(LX/0QB;)Landroid/view/LayoutInflater;

    move-result-object v4

    check-cast v4, Landroid/view/LayoutInflater;

    const/16 v6, 0x24e7

    invoke-static {v1, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-direct {v5, v2, v3, v4, v6}, LX/B40;-><init>(LX/0Px;LX/B4A;Landroid/view/LayoutInflater;LX/0Or;)V

    .line 1741320
    move-object v1, v5

    .line 1741321
    iput-object v1, v0, LX/B4A;->p:LX/B40;

    .line 1741322
    iget-object v0, p0, LX/B49;->b:LX/B4A;

    iget-object v0, v0, LX/B4A;->h:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v1, p0, LX/B49;->b:LX/B4A;

    iget-object v1, v1, LX/B4A;->p:LX/B40;

    iget-object v2, p0, LX/B49;->b:LX/B4A;

    iget-object v2, v2, LX/B4A;->c:Landroid/app/Activity;

    invoke-static {v0, v1, v2}, LX/B3v;->a(Lcom/facebook/widget/recyclerview/BetterRecyclerView;LX/1OM;Landroid/content/Context;)V

    .line 1741323
    iget-object v0, p0, LX/B49;->b:LX/B4A;

    iget-object v0, v0, LX/B4A;->g:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->b()V

    .line 1741324
    return-void
.end method
