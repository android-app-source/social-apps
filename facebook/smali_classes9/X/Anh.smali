.class public final LX/Anh;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Z

.field public final b:Z

.field public final c:I

.field public final d:I

.field public final e:I


# direct methods
.method public constructor <init>(Lcom/facebook/graphql/model/GraphQLFeedback;Z)V
    .locals 6
    .param p1    # Lcom/facebook/graphql/model/GraphQLFeedback;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1712768
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->s_()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    move v1, v0

    .line 1712769
    invoke-static {p1}, LX/Ani;->b(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v3

    invoke-static {p1}, LX/Ani;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v4

    invoke-static {p1}, LX/16z;->l(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v5

    move-object v0, p0

    move v2, p2

    invoke-direct/range {v0 .. v5}, LX/Anh;-><init>(ZZIII)V

    .line 1712770
    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(ZZIII)V
    .locals 0

    .prologue
    .line 1712771
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1712772
    iput-boolean p1, p0, LX/Anh;->a:Z

    .line 1712773
    iput p3, p0, LX/Anh;->d:I

    .line 1712774
    iput p4, p0, LX/Anh;->c:I

    .line 1712775
    iput p5, p0, LX/Anh;->e:I

    .line 1712776
    iput-boolean p2, p0, LX/Anh;->b:Z

    .line 1712777
    return-void
.end method
