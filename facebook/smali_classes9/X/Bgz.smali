.class public LX/Bgz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Bgq;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/Bgq",
        "<",
        "Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;",
        "LX/97f;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/Bg8;

.field private final b:LX/03V;

.field public final c:LX/BfJ;


# direct methods
.method public constructor <init>(LX/03V;LX/Bg8;LX/BfJ;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1808636
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1808637
    iput-object p2, p0, LX/Bgz;->a:LX/Bg8;

    .line 1808638
    iput-object p1, p0, LX/Bgz;->b:LX/03V;

    .line 1808639
    iput-object p3, p0, LX/Bgz;->c:LX/BfJ;

    .line 1808640
    return-void
.end method

.method public static a(Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;LX/97f;LX/BgS;)LX/97f;
    .locals 3

    .prologue
    .line 1808641
    invoke-static {p1}, LX/Bgb;->f(LX/97f;)Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;

    move-result-object v0

    .line 1808642
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;->getStreetAddress()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;->getZip()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;->q()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1808643
    :goto_0
    return-object p1

    .line 1808644
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;->getStreetAddress()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;->getZip()Ljava/lang/String;

    move-result-object v1

    .line 1808645
    invoke-static {p1}, LX/Bgb;->f(LX/97f;)Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;->a(Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;)Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;

    move-result-object v2

    .line 1808646
    if-nez v2, :cond_1

    .line 1808647
    :goto_1
    move-object p1, p1

    .line 1808648
    invoke-interface {p2, p1}, LX/BgS;->a(Ljava/lang/Object;)V

    goto :goto_0

    .line 1808649
    :cond_1
    new-instance p0, LX/97m;

    invoke-direct {p0}, LX/97m;-><init>()V

    invoke-static {v2}, LX/97m;->a(Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;)LX/97m;

    move-result-object v2

    .line 1808650
    iput-object v0, v2, LX/97m;->k:Ljava/lang/String;

    .line 1808651
    move-object v2, v2

    .line 1808652
    iput-object v1, v2, LX/97m;->q:Ljava/lang/String;

    .line 1808653
    move-object v2, v2

    .line 1808654
    invoke-virtual {v2}, LX/97m;->a()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;

    move-result-object v2

    .line 1808655
    invoke-static {p1, v2}, LX/BgV;->a(LX/97f;Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;)LX/97f;

    move-result-object p1

    goto :goto_1
.end method

.method private static a(LX/Bgz;Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;LX/97f;LX/BgS;LX/BeD;Landroid/support/v4/app/Fragment;)V
    .locals 6

    .prologue
    .line 1808656
    if-eqz p4, :cond_0

    invoke-virtual {p4}, LX/BeD;->getInputStyle()LX/BeC;

    move-result-object v0

    sget-object v1, LX/BeC;->LOCATION:LX/BeC;

    if-eq v0, v1, :cond_1

    .line 1808657
    :cond_0
    iget-object v0, p0, LX/Bgz;->b:LX/03V;

    const-string v1, "SuggestEdits"

    const-string v2, "Input style not supported by field"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1808658
    :goto_0
    return-void

    .line 1808659
    :cond_1
    new-instance v0, LX/Bgx;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, LX/Bgx;-><init>(LX/Bgz;Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;LX/97f;LX/BgS;Landroid/support/v4/app/Fragment;)V

    invoke-virtual {p1, v0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;->setCityOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1808660
    new-instance v0, LX/Bgy;

    invoke-direct {v0, p0, p1, p2, p3}, LX/Bgy;-><init>(LX/Bgz;Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;LX/97f;LX/BgS;)V

    invoke-virtual {p1, v0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    goto :goto_0
.end method

.method private a(Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;LX/97f;LX/BgS;LX/BgK;LX/BeD;Landroid/support/v4/app/Fragment;)V
    .locals 6
    .param p5    # LX/BeD;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 1808661
    invoke-virtual {p1}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;->a()V

    .line 1808662
    invoke-interface {p2}, LX/97e;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;->setCityHintText(Ljava/lang/String;)V

    .line 1808663
    invoke-interface {p2}, LX/97e;->gv_()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;->setStreetAddressHintText(Ljava/lang/String;)V

    .line 1808664
    invoke-interface {p2}, LX/97e;->gw_()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;->setZipHintText(Ljava/lang/String;)V

    .line 1808665
    invoke-static {p2}, LX/Bgb;->f(LX/97f;)Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;

    move-result-object v2

    .line 1808666
    if-eqz v2, :cond_0

    .line 1808667
    invoke-virtual {v2}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;->b()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedAddressModel$CityModel;

    move-result-object v0

    if-nez v0, :cond_2

    move-object v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;->setCity(Ljava/lang/String;)V

    .line 1808668
    invoke-virtual {v2}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;->q()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;->setZip(Ljava/lang/String;)V

    .line 1808669
    invoke-static {p2}, LX/Bgb;->h(LX/97f;)Landroid/location/Location;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;->setMapLocation(Landroid/location/Location;)V

    .line 1808670
    :cond_0
    invoke-static {p2}, LX/Bgb;->a(LX/97f;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;->setIcon(Ljava/lang/String;)V

    .line 1808671
    iget-object v0, p0, LX/Bgz;->a:LX/Bg8;

    invoke-virtual {p1}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;->getActionButtonView()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v0, v3, p2, p3, v1}, LX/Bg8;->a(Landroid/view/View;LX/97f;LX/BgS;LX/Bh0;)Landroid/view/View$OnClickListener;

    move-result-object v0

    .line 1808672
    invoke-virtual {p1, v0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;->setActionButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1808673
    invoke-interface {p2}, LX/97e;->e()Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    move-result-object v1

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;->HAS_VALUE:Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    if-ne v1, v3, :cond_3

    .line 1808674
    const-string v1, "<<not-applicable>>"

    invoke-virtual {v2}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;->k()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x0

    :goto_1
    move-object v1, v1

    .line 1808675
    invoke-virtual {p1, v1}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;->setStreetAddress(Ljava/lang/String;)V

    .line 1808676
    :cond_1
    :goto_2
    new-instance v0, LX/Bgw;

    invoke-direct {v0, p0, p4, p1}, LX/Bgw;-><init>(LX/Bgz;LX/BgK;Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;)V

    invoke-virtual {p1, v0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;->setTextChangedListener(Landroid/text/TextWatcher;)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p5

    move-object v5, p6

    .line 1808677
    invoke-static/range {v0 .. v5}, LX/Bgz;->a(LX/Bgz;Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;LX/97f;LX/BgS;LX/BeD;Landroid/support/v4/app/Fragment;)V

    .line 1808678
    return-void

    .line 1808679
    :cond_2
    invoke-virtual {v2}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;->b()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedAddressModel$CityModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedAddressModel$CityModel;->d()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1808680
    :cond_3
    invoke-interface {p2}, LX/97e;->e()Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    move-result-object v1

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;->DOESNT_HAVE_VALUE:Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    if-ne v1, v3, :cond_1

    .line 1808681
    invoke-static {p2}, LX/Bgb;->i(LX/97f;)Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsOptionModel;

    move-result-object v1

    .line 1808682
    if-nez v1, :cond_4

    const-string v1, ""

    :goto_3
    invoke-virtual {p1, v1}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;->setStreetAddressHintText(Ljava/lang/String;)V

    .line 1808683
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;->setStreetAddress(Ljava/lang/String;)V

    .line 1808684
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;->setStreetAddressFieldFocusable(Z)V

    .line 1808685
    invoke-virtual {p1, v0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;->setStreetAddressOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_2

    .line 1808686
    :cond_4
    invoke-virtual {v1}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsOptionModel;->a()Ljava/lang/String;

    move-result-object v1

    goto :goto_3

    :cond_5
    invoke-virtual {v2}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;->k()Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method


# virtual methods
.method public final a()LX/BeE;
    .locals 1

    .prologue
    .line 1808687
    sget-object v0, LX/BeE;->LOCATION:LX/BeE;

    return-object v0
.end method

.method public final a(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 1808688
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1808689
    const v1, 0x7f031420

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;

    .line 1808690
    return-object v0
.end method

.method public final bridge synthetic a(Landroid/view/View;Ljava/lang/Object;LX/BgS;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1808691
    check-cast p1, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;

    check-cast p2, LX/97f;

    invoke-static {p1, p2, p3}, LX/Bgz;->a(Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;LX/97f;LX/BgS;)LX/97f;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Landroid/view/View;Ljava/lang/Object;Ljava/lang/Object;LX/BgS;LX/BgK;LX/BeD;Landroid/support/v4/app/Fragment;Ljava/lang/String;)V
    .locals 7
    .param p3    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # LX/BeD;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Landroid/support/v4/app/Fragment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1808692
    move-object v1, p1

    check-cast v1, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;

    move-object v2, p2

    check-cast v2, LX/97f;

    move-object v0, p0

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    move-object v6, p7

    invoke-direct/range {v0 .. v6}, LX/Bgz;->a(Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;LX/97f;LX/BgS;LX/BgK;LX/BeD;Landroid/support/v4/app/Fragment;)V

    return-void
.end method
