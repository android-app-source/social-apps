.class public final LX/BJj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:Lcom/facebook/platform/composer/composer/PlatformComposerEditText;


# direct methods
.method public constructor <init>(Lcom/facebook/platform/composer/composer/PlatformComposerEditText;)V
    .locals 0

    .prologue
    .line 1772328
    iput-object p1, p0, LX/BJj;->a:Lcom/facebook/platform/composer/composer/PlatformComposerEditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 4

    .prologue
    .line 1772329
    iget-object v0, p0, LX/BJj;->a:Lcom/facebook/platform/composer/composer/PlatformComposerEditText;

    invoke-static {v0}, Lcom/facebook/platform/composer/composer/PlatformComposerEditText;->getTextWithEntities(Lcom/facebook/platform/composer/composer/PlatformComposerEditText;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    .line 1772330
    iget-object v0, p0, LX/BJj;->a:Lcom/facebook/platform/composer/composer/PlatformComposerEditText;

    iget-object v0, v0, Lcom/facebook/platform/composer/composer/PlatformComposerEditText;->e:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BJm;

    .line 1772331
    invoke-interface {v0, v1}, LX/BJm;->a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)V

    goto :goto_0

    .line 1772332
    :cond_0
    iget-object v0, p0, LX/BJj;->a:Lcom/facebook/platform/composer/composer/PlatformComposerEditText;

    const/4 v1, 0x0

    .line 1772333
    iput-boolean v1, v0, Lcom/facebook/platform/composer/composer/PlatformComposerEditText;->d:Z

    .line 1772334
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1772335
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1772336
    return-void
.end method
