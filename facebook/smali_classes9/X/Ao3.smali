.class public LX/Ao3;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public final a:Lcom/facebook/resources/ui/FbTextView;

.field public final b:Landroid/widget/ImageView;

.field public final c:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1713201
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1713202
    const v0, 0x7f030c72

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1713203
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/Ao3;->setOrientation(I)V

    .line 1713204
    const v0, 0x7f0d175f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/Ao3;->a:Lcom/facebook/resources/ui/FbTextView;

    .line 1713205
    const v0, 0x7f0d1e8f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/Ao3;->c:Landroid/widget/ImageView;

    .line 1713206
    const v0, 0x7f0d1e91

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/Ao3;->b:Landroid/widget/ImageView;

    .line 1713207
    return-void
.end method


# virtual methods
.method public setFooterText(I)V
    .locals 1

    .prologue
    .line 1713208
    iget-object v0, p0, LX/Ao3;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 1713209
    return-void
.end method

.method public setFooterText(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1713210
    iget-object v0, p0, LX/Ao3;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1713211
    return-void
.end method
