.class public LX/BGb;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:I

.field private final b:J

.field public final c:Ljava/util/concurrent/ExecutorService;

.field public final d:Ljava/util/concurrent/Executor;

.field public final e:LX/75S;

.field public final f:LX/BI6;

.field public final g:LX/BHQ;

.field public final h:LX/0Uh;

.field public i:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final j:LX/8I4;

.field public final k:LX/BHr;

.field public final l:LX/0SG;

.field public final m:LX/BGa;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/Executor;LX/BI6;LX/75S;LX/BHQ;LX/0Uh;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/8I4;LX/BHr;LX/0SG;LX/BGa;)V
    .locals 2
    .param p1    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .param p2    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p3    # LX/BI6;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1767314
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1767315
    const/4 v0, 0x3

    iput v0, p0, LX/BGb;->a:I

    .line 1767316
    const-wide/32 v0, 0x3f480

    iput-wide v0, p0, LX/BGb;->b:J

    .line 1767317
    iput-object p1, p0, LX/BGb;->c:Ljava/util/concurrent/ExecutorService;

    .line 1767318
    iput-object p2, p0, LX/BGb;->d:Ljava/util/concurrent/Executor;

    .line 1767319
    iput-object p4, p0, LX/BGb;->e:LX/75S;

    .line 1767320
    iput-object p3, p0, LX/BGb;->f:LX/BI6;

    .line 1767321
    iput-object p5, p0, LX/BGb;->g:LX/BHQ;

    .line 1767322
    iput-object p6, p0, LX/BGb;->h:LX/0Uh;

    .line 1767323
    iput-object p7, p0, LX/BGb;->i:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1767324
    iput-object p8, p0, LX/BGb;->j:LX/8I4;

    .line 1767325
    iput-object p9, p0, LX/BGb;->k:LX/BHr;

    .line 1767326
    iput-object p10, p0, LX/BGb;->l:LX/0SG;

    .line 1767327
    iput-object p11, p0, LX/BGb;->m:LX/BGa;

    .line 1767328
    return-void
.end method
