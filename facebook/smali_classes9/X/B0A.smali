.class public LX/B0A;
.super Lcom/facebook/resources/ui/FbFrameLayout;
.source ""


# instance fields
.field public a:LX/1Ml;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0oJ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/B0B;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Lcom/facebook/goodfriends/camera/GoodFriendsGalleryRequestPermissionsView;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Lcom/facebook/goodfriends/camera/GoodFriendsGalleryImportView;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Lcom/facebook/goodfriends/camera/GoodFriendsNoGalleryMediaView;",
            ">;"
        }
    .end annotation
.end field

.field public g:I

.field public final h:LX/B05;

.field private final i:LX/B09;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1733385
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, LX/B0A;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1733386
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1733357
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/resources/ui/FbFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1733358
    new-instance v0, LX/B08;

    invoke-direct {v0, p0}, LX/B08;-><init>(LX/B0A;)V

    iput-object v0, p0, LX/B0A;->h:LX/B05;

    .line 1733359
    new-instance v0, LX/B09;

    invoke-direct {v0, p0}, LX/B09;-><init>(LX/B0A;)V

    iput-object v0, p0, LX/B0A;->i:LX/B09;

    .line 1733360
    const-class v0, LX/B0A;

    invoke-static {v0, p0}, LX/B0A;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1733361
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0307c2

    const/4 p2, 0x1

    invoke-virtual {v0, v1, p0, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 1733362
    new-instance v1, LX/0zw;

    const v0, 0x7f0d1494

    invoke-virtual {p0, v0}, LX/B0A;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-direct {v1, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    iput-object v1, p0, LX/B0A;->e:LX/0zw;

    .line 1733363
    new-instance v1, LX/0zw;

    const v0, 0x7f0d1495

    invoke-virtual {p0, v0}, LX/B0A;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-direct {v1, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    iput-object v1, p0, LX/B0A;->d:LX/0zw;

    .line 1733364
    new-instance v1, LX/0zw;

    const v0, 0x7f0d1496

    invoke-virtual {p0, v0}, LX/B0A;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-direct {v1, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    iput-object v1, p0, LX/B0A;->f:LX/0zw;

    .line 1733365
    iget-object v0, p0, LX/B0A;->b:LX/0oJ;

    const/4 p1, 0x0

    .line 1733366
    iget-object v1, v0, LX/0oJ;->a:LX/0ad;

    sget-char p2, LX/0ob;->q:C

    invoke-interface {v1, p2, p1}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1733367
    iget-object p2, v0, LX/0oJ;->a:LX/0ad;

    sget-char p3, LX/0ob;->C:C

    invoke-interface {p2, p3, p1}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object p2

    .line 1733368
    if-eqz v1, :cond_1

    .line 1733369
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 1733370
    :goto_0
    move v0, v1

    .line 1733371
    iput v0, p0, LX/B0A;->g:I

    .line 1733372
    iget-object v0, p0, LX/B0A;->a:LX/1Ml;

    sget-object v1, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryRequestPermissionsView;->a:[Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/1Ml;->a([Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1733373
    iget-object v0, p0, LX/B0A;->d:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1733374
    iget-object v0, p0, LX/B0A;->d:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryRequestPermissionsView;

    .line 1733375
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryRequestPermissionsView;->setVisibility(I)V

    .line 1733376
    iget-object v1, p0, LX/B0A;->h:LX/B05;

    .line 1733377
    iput-object v1, v0, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryRequestPermissionsView;->d:LX/B05;

    .line 1733378
    :cond_0
    invoke-static {p0}, LX/B0A;->c$redex0(LX/B0A;)V

    .line 1733379
    invoke-static {p0}, LX/B0A;->e(LX/B0A;)V

    .line 1733380
    :goto_1
    return-void

    .line 1733381
    :cond_1
    if-eqz p2, :cond_2

    .line 1733382
    invoke-static {p2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    goto :goto_0

    .line 1733383
    :cond_2
    const/16 v1, 0xa

    goto :goto_0

    .line 1733384
    :cond_3
    invoke-virtual {p0}, LX/B0A;->a()V

    goto :goto_1
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p1, LX/B0A;

    invoke-static {v3}, LX/1Ml;->b(LX/0QB;)LX/1Ml;

    move-result-object v1

    check-cast v1, LX/1Ml;

    invoke-static {v3}, LX/0oJ;->b(LX/0QB;)LX/0oJ;

    move-result-object v2

    check-cast v2, LX/0oJ;

    new-instance v0, LX/B0B;

    invoke-static {v3}, LX/1kH;->a(LX/0QB;)LX/1kH;

    move-result-object v4

    check-cast v4, LX/1kH;

    invoke-static {v3}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/Executor;

    invoke-static {v3}, LX/0Zo;->a(LX/0QB;)LX/0Tf;

    move-result-object p0

    check-cast p0, Ljava/util/concurrent/Executor;

    invoke-direct {v0, v4, v5, p0}, LX/B0B;-><init>(LX/1kH;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;)V

    move-object v3, v0

    check-cast v3, LX/B0B;

    iput-object v1, p1, LX/B0A;->a:LX/1Ml;

    iput-object v2, p1, LX/B0A;->b:LX/0oJ;

    iput-object v3, p1, LX/B0A;->c:LX/B0B;

    return-void
.end method

.method public static c$redex0(LX/B0A;)V
    .locals 1

    .prologue
    .line 1733354
    iget-object v0, p0, LX/B0A;->e:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1733355
    iget-object v0, p0, LX/B0A;->e:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->c()V

    .line 1733356
    :cond_0
    return-void
.end method

.method public static e(LX/B0A;)V
    .locals 1

    .prologue
    .line 1733341
    iget-object v0, p0, LX/B0A;->f:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1733342
    iget-object v0, p0, LX/B0A;->f:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->c()V

    .line 1733343
    :cond_0
    return-void
.end method

.method public static g(LX/B0A;)V
    .locals 2

    .prologue
    .line 1733348
    iget-object v0, p0, LX/B0A;->d:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1733349
    iget-object v0, p0, LX/B0A;->d:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryRequestPermissionsView;

    .line 1733350
    const/4 v1, 0x0

    .line 1733351
    iput-object v1, v0, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryRequestPermissionsView;->d:LX/B05;

    .line 1733352
    iget-object v0, p0, LX/B0A;->d:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->c()V

    .line 1733353
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    .line 1733344
    iget-object v0, p0, LX/B0A;->a:LX/1Ml;

    sget-object v1, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryRequestPermissionsView;->a:[Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/1Ml;->a([Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1733345
    iget-object v0, p0, LX/B0A;->c:LX/B0B;

    iget-object v1, p0, LX/B0A;->i:LX/B09;

    iget v2, p0, LX/B0A;->g:I

    .line 1733346
    iget-object v3, v0, LX/B0B;->c:Ljava/util/concurrent/Executor;

    new-instance v4, Lcom/facebook/goodfriends/camera/GoodFriendsMediaFetcher$1;

    invoke-direct {v4, v0, v2, v1}, Lcom/facebook/goodfriends/camera/GoodFriendsMediaFetcher$1;-><init>(LX/B0B;ILX/B09;)V

    const p0, 0x115a973c

    invoke-static {v3, v4, p0}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1733347
    :cond_0
    return-void
.end method
