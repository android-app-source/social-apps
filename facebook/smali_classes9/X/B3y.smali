.class public final LX/B3y;
.super Landroid/widget/Filter;
.source ""


# instance fields
.field public final synthetic a:LX/B40;

.field private final b:Landroid/widget/Filter$FilterResults;

.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/B40;)V
    .locals 1

    .prologue
    .line 1741104
    iput-object p1, p0, LX/B3y;->a:LX/B40;

    invoke-direct {p0}, Landroid/widget/Filter;-><init>()V

    .line 1741105
    new-instance v0, Landroid/widget/Filter$FilterResults;

    invoke-direct {v0}, Landroid/widget/Filter$FilterResults;-><init>()V

    iput-object v0, p0, LX/B3y;->b:Landroid/widget/Filter$FilterResults;

    .line 1741106
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/B3y;->c:Ljava/util/List;

    return-void
.end method

.method private static a(Ljava/lang/CharSequence;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1741103
    invoke-interface {p0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/1fg;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/String;Ljava/util/Iterator;Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Iterator",
            "<",
            "Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1741093
    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1741094
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;

    .line 1741095
    iget-object v1, v0, Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;->b:Ljava/lang/String;

    move-object v1, v1

    .line 1741096
    invoke-static {v1}, LX/1fg;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1741097
    if-nez v2, :cond_2

    .line 1741098
    :cond_1
    return-void

    .line 1741099
    :cond_2
    const-string v1, "\\s"

    invoke-virtual {v2, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    array-length v4, v3

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v4, :cond_0

    aget-object v5, v3, v1

    .line 1741100
    invoke-virtual {v5, p0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_3

    invoke-virtual {v2, p0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1741101
    :cond_3
    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1741102
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method


# virtual methods
.method public final performFiltering(Ljava/lang/CharSequence;)Landroid/widget/Filter$FilterResults;
    .locals 4

    .prologue
    .line 1741081
    iget-object v0, p0, LX/B3y;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1741082
    invoke-static {p1}, LX/B3y;->a(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 1741083
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/B3y;->a:LX/B40;

    iget-object v1, v1, LX/B40;->g:Ljava/lang/String;

    invoke-static {v0, v1}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1741084
    :cond_0
    iget-object v0, p0, LX/B3y;->a:LX/B40;

    iget-object v0, v0, LX/B40;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_2

    iget-object v0, p0, LX/B3y;->a:LX/B40;

    iget-object v0, v0, LX/B40;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;

    .line 1741085
    iget-object v3, p0, LX/B3y;->c:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1741086
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1741087
    :cond_1
    iget-object v1, p0, LX/B3y;->a:LX/B40;

    iget-object v1, v1, LX/B40;->g:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, LX/B3y;->a:LX/B40;

    iget-object v1, v1, LX/B40;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1741088
    iget-object v1, p0, LX/B3y;->a:LX/B40;

    iget-object v1, v1, LX/B40;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    iget-object v2, p0, LX/B3y;->c:Ljava/util/List;

    invoke-static {v0, v1, v2}, LX/B3y;->a(Ljava/lang/String;Ljava/util/Iterator;Ljava/util/List;)V

    .line 1741089
    :cond_2
    :goto_1
    iget-object v0, p0, LX/B3y;->b:Landroid/widget/Filter$FilterResults;

    iget-object v1, p0, LX/B3y;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    iput v1, v0, Landroid/widget/Filter$FilterResults;->count:I

    .line 1741090
    iget-object v0, p0, LX/B3y;->b:Landroid/widget/Filter$FilterResults;

    iget-object v1, p0, LX/B3y;->c:Ljava/util/List;

    iput-object v1, v0, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    .line 1741091
    iget-object v0, p0, LX/B3y;->b:Landroid/widget/Filter$FilterResults;

    return-object v0

    .line 1741092
    :cond_3
    iget-object v1, p0, LX/B3y;->a:LX/B40;

    iget-object v1, v1, LX/B40;->a:LX/0Px;

    invoke-virtual {v1}, LX/0Py;->iterator()LX/0Rc;

    move-result-object v1

    iget-object v2, p0, LX/B3y;->c:Ljava/util/List;

    invoke-static {v0, v1, v2}, LX/B3y;->a(Ljava/lang/String;Ljava/util/Iterator;Ljava/util/List;)V

    goto :goto_1
.end method

.method public final publishResults(Ljava/lang/CharSequence;Landroid/widget/Filter$FilterResults;)V
    .locals 2

    .prologue
    .line 1741075
    iget-object v0, p0, LX/B3y;->a:LX/B40;

    iget-object v0, v0, LX/B40;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1741076
    iget-object v0, p2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    if-eqz v0, :cond_0

    .line 1741077
    iget-object v0, p0, LX/B3y;->a:LX/B40;

    iget-object v1, v0, LX/B40;->f:Ljava/util/List;

    iget-object v0, p2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1741078
    :cond_0
    iget-object v0, p0, LX/B3y;->a:LX/B40;

    invoke-static {p1}, LX/B3y;->a(Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    .line 1741079
    iput-object v1, v0, LX/B40;->g:Ljava/lang/String;

    .line 1741080
    return-void
.end method
