.class public final LX/CHU;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$RichMediaDocumentQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 1867673
    const-class v1, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$RichMediaDocumentQueryModel;

    const v0, -0x1b6bbdb

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "RichMediaDocumentQuery"

    const-string v6, "36896f9c1d22c4f9cb1ecedc7e4043ad"

    const-string v7, "node"

    const-string v8, "10155217777856729"

    const-string v9, "10155259090121729"

    .line 1867674
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 1867675
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 1867676
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1867677
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1867678
    sparse-switch v0, :sswitch_data_0

    .line 1867679
    :goto_0
    return-object p1

    .line 1867680
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 1867681
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 1867682
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 1867683
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 1867684
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 1867685
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    .line 1867686
    :sswitch_6
    const-string p1, "6"

    goto :goto_0

    .line 1867687
    :sswitch_7
    const-string p1, "7"

    goto :goto_0

    .line 1867688
    :sswitch_8
    const-string p1, "8"

    goto :goto_0

    .line 1867689
    :sswitch_9
    const-string p1, "9"

    goto :goto_0

    .line 1867690
    :sswitch_a
    const-string p1, "10"

    goto :goto_0

    .line 1867691
    :sswitch_b
    const-string p1, "11"

    goto :goto_0

    .line 1867692
    :sswitch_c
    const-string p1, "12"

    goto :goto_0

    .line 1867693
    :sswitch_d
    const-string p1, "13"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x65578195 -> :sswitch_d
        -0x33f8633e -> :sswitch_4
        -0x3093008a -> :sswitch_0
        -0x15afd767 -> :sswitch_6
        -0x38aa96a -> :sswitch_a
        0x2ef6341 -> :sswitch_5
        0x683094a -> :sswitch_c
        0x2e61b03a -> :sswitch_9
        0x45e5f0b4 -> :sswitch_7
        0x66c8fb9c -> :sswitch_8
        0x7191d8b1 -> :sswitch_3
        0x73a026b5 -> :sswitch_2
        0x78668257 -> :sswitch_b
        0x78a3267b -> :sswitch_1
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1867694
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_1

    .line 1867695
    :goto_1
    return v0

    .line 1867696
    :pswitch_0
    const-string v2, "13"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    .line 1867697
    :pswitch_1
    const/16 v0, 0x14

    const-string v1, "%s"

    invoke-static {p2, v0, v1}, LX/0wE;->a(Ljava/lang/Object;ILjava/lang/String;)Z

    move-result v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x622
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method
