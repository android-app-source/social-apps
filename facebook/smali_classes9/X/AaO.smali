.class public final enum LX/AaO;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/AaO;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/AaO;

.field public static final enum GET_DEFAULT_PAYMENT_METHOD:LX/AaO;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1688442
    new-instance v0, LX/AaO;

    const-string v1, "GET_DEFAULT_PAYMENT_METHOD"

    invoke-direct {v0, v1, v2}, LX/AaO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AaO;->GET_DEFAULT_PAYMENT_METHOD:LX/AaO;

    .line 1688443
    const/4 v0, 0x1

    new-array v0, v0, [LX/AaO;

    sget-object v1, LX/AaO;->GET_DEFAULT_PAYMENT_METHOD:LX/AaO;

    aput-object v1, v0, v2

    sput-object v0, LX/AaO;->$VALUES:[LX/AaO;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1688441
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/AaO;
    .locals 1

    .prologue
    .line 1688444
    const-class v0, LX/AaO;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/AaO;

    return-object v0
.end method

.method public static values()[LX/AaO;
    .locals 1

    .prologue
    .line 1688440
    sget-object v0, LX/AaO;->$VALUES:[LX/AaO;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/AaO;

    return-object v0
.end method
