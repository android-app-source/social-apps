.class public final LX/BoY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:Landroid/view/View;

.field public final synthetic b:Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;

.field public final synthetic c:LX/BoZ;


# direct methods
.method public constructor <init>(LX/BoZ;Landroid/view/View;Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;)V
    .locals 0

    .prologue
    .line 1821800
    iput-object p1, p0, LX/BoY;->c:LX/BoZ;

    iput-object p2, p0, LX/BoY;->a:Landroid/view/View;

    iput-object p3, p0, LX/BoY;->b:Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 4

    .prologue
    .line 1821801
    iget-object v0, p0, LX/BoY;->c:LX/BoZ;

    iget-object v1, p0, LX/BoY;->a:Landroid/view/View;

    iget-object v2, p0, LX/BoY;->b:Lcom/facebook/graphql/model/GraphQLPeopleYouMayInviteFeedUnit;

    .line 1821802
    new-instance v3, Landroid/net/Uri$Builder;

    invoke-direct {v3}, Landroid/net/Uri$Builder;-><init>()V

    sget-object p0, LX/0ax;->a:Ljava/lang/String;

    invoke-virtual {v3, p0}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    const-string p0, "faceweb"

    invoke-virtual {v3, p0}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    const-string p0, "f"

    invoke-virtual {v3, p0}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    const-string p0, "href"

    const-string p1, "/pymi/legal/learn_more"

    invoke-virtual {v3, p0, p1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    .line 1821803
    new-instance p0, Landroid/content/Intent;

    const-string p1, "android.intent.action.VIEW"

    invoke-direct {p0, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1821804
    invoke-virtual {p0, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1821805
    iget-object v3, v0, LX/BoZ;->b:LX/1dt;

    iget-object v3, v3, LX/1dt;->H:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-interface {v3, p0, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1821806
    invoke-static {v2}, LX/1fz;->a(LX/16g;)LX/162;

    move-result-object v3

    .line 1821807
    invoke-static {v3}, LX/17Q;->F(LX/0lF;)Z

    move-result p0

    if-eqz p0, :cond_0

    .line 1821808
    const/4 p0, 0x0

    .line 1821809
    :goto_0
    move-object v3, p0

    .line 1821810
    iget-object p0, v0, LX/BoZ;->b:LX/1dt;

    iget-object p0, p0, LX/1dt;->E:LX/0Zb;

    invoke-interface {p0, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1821811
    const/4 v0, 0x1

    return v0

    .line 1821812
    :cond_0
    new-instance p0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p1, "pymi_about_invites"

    invoke-direct {p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p1, "tracking"

    invoke-virtual {p0, p1, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string p1, "native_newsfeed"

    .line 1821813
    iput-object p1, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1821814
    move-object p0, p0

    .line 1821815
    goto :goto_0
.end method
