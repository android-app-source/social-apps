.class public final LX/Bqy;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/Bqz;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Z

.field public b:LX/Br2;

.field public c:LX/1Pq;

.field public d:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/8wT;

.field public f:LX/Bqu;

.field public g:Ljava/lang/String;

.field public final synthetic h:LX/Bqz;


# direct methods
.method public constructor <init>(LX/Bqz;)V
    .locals 1

    .prologue
    .line 1825625
    iput-object p1, p0, LX/Bqy;->h:LX/Bqz;

    .line 1825626
    move-object v0, p1

    .line 1825627
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1825628
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1825629
    const-string v0, "StoryPromotionMountComponent"

    return-object v0
.end method

.method public final a(LX/1X1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1",
            "<",
            "LX/Bqz;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1825630
    check-cast p1, LX/Bqy;

    .line 1825631
    iget-object v0, p1, LX/Bqy;->e:LX/8wT;

    iput-object v0, p0, LX/Bqy;->e:LX/8wT;

    .line 1825632
    iget-object v0, p1, LX/Bqy;->f:LX/Bqu;

    iput-object v0, p0, LX/Bqy;->f:LX/Bqu;

    .line 1825633
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1825634
    if-ne p0, p1, :cond_1

    .line 1825635
    :cond_0
    :goto_0
    return v0

    .line 1825636
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1825637
    goto :goto_0

    .line 1825638
    :cond_3
    check-cast p1, LX/Bqy;

    .line 1825639
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1825640
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1825641
    if-eq v2, v3, :cond_0

    .line 1825642
    iget-boolean v2, p0, LX/Bqy;->a:Z

    iget-boolean v3, p1, LX/Bqy;->a:Z

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 1825643
    goto :goto_0

    .line 1825644
    :cond_4
    iget-object v2, p0, LX/Bqy;->b:LX/Br2;

    if-eqz v2, :cond_6

    iget-object v2, p0, LX/Bqy;->b:LX/Br2;

    iget-object v3, p1, LX/Bqy;->b:LX/Br2;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    :cond_5
    move v0, v1

    .line 1825645
    goto :goto_0

    .line 1825646
    :cond_6
    iget-object v2, p1, LX/Bqy;->b:LX/Br2;

    if-nez v2, :cond_5

    .line 1825647
    :cond_7
    iget-object v2, p0, LX/Bqy;->c:LX/1Pq;

    if-eqz v2, :cond_9

    iget-object v2, p0, LX/Bqy;->c:LX/1Pq;

    iget-object v3, p1, LX/Bqy;->c:LX/1Pq;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    :cond_8
    move v0, v1

    .line 1825648
    goto :goto_0

    .line 1825649
    :cond_9
    iget-object v2, p1, LX/Bqy;->c:LX/1Pq;

    if-nez v2, :cond_8

    .line 1825650
    :cond_a
    iget-object v2, p0, LX/Bqy;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_c

    iget-object v2, p0, LX/Bqy;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/Bqy;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    :cond_b
    move v0, v1

    .line 1825651
    goto :goto_0

    .line 1825652
    :cond_c
    iget-object v2, p1, LX/Bqy;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_b

    .line 1825653
    :cond_d
    iget-object v2, p0, LX/Bqy;->g:Ljava/lang/String;

    if-eqz v2, :cond_e

    iget-object v2, p0, LX/Bqy;->g:Ljava/lang/String;

    iget-object v3, p1, LX/Bqy;->g:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1825654
    goto :goto_0

    .line 1825655
    :cond_e
    iget-object v2, p1, LX/Bqy;->g:Ljava/lang/String;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public final g()LX/1X1;
    .locals 2

    .prologue
    .line 1825656
    const/4 v1, 0x0

    .line 1825657
    invoke-super {p0}, LX/1X1;->g()LX/1X1;

    move-result-object v0

    check-cast v0, LX/Bqy;

    .line 1825658
    iput-object v1, v0, LX/Bqy;->e:LX/8wT;

    .line 1825659
    iput-object v1, v0, LX/Bqy;->f:LX/Bqu;

    .line 1825660
    return-object v0
.end method
