.class public LX/BzB;
.super LX/37T;
.source ""


# instance fields
.field private final a:Lcom/facebook/graphql/model/GraphQLStory;

.field private final b:LX/1KL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1KL",
            "<",
            "Ljava/lang/String;",
            "LX/1yT;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Landroid/content/Context;

.field public final d:LX/1Uf;

.field public final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/17W;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1Uf;LX/0Or;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLTextWithEntities;LX/1eK;Ljava/lang/String;)V
    .locals 8
    .param p4    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # Lcom/facebook/graphql/model/GraphQLTextWithEntities;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # LX/1eK;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p8    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/1Uf;",
            "LX/0Or",
            "<",
            "LX/17W;",
            ">;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLTextWithEntities;",
            "LX/1eK;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1838296
    invoke-direct {p0}, LX/37T;-><init>()V

    .line 1838297
    invoke-virtual {p5}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    iput-object v0, p0, LX/BzB;->a:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1838298
    new-instance v0, LX/BzA;

    const/4 v7, 0x0

    move-object v1, p0

    move-object v2, p4

    move-object v3, p5

    move-object v4, p6

    move-object v5, p7

    move-object/from16 v6, p8

    invoke-direct/range {v0 .. v7}, LX/BzA;-><init>(LX/BzB;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLTextWithEntities;LX/1eK;Ljava/lang/String;B)V

    iput-object v0, p0, LX/BzB;->b:LX/1KL;

    .line 1838299
    iput-object p1, p0, LX/BzB;->c:Landroid/content/Context;

    .line 1838300
    iput-object p2, p0, LX/BzB;->d:LX/1Uf;

    .line 1838301
    iput-object p3, p0, LX/BzB;->e:LX/0Or;

    .line 1838302
    return-void
.end method


# virtual methods
.method public final a(Landroid/text/Spannable;)I
    .locals 1

    .prologue
    .line 1838303
    const/4 v0, 0x0

    return v0
.end method

.method public final a()LX/1KL;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1KL",
            "<",
            "Ljava/lang/String;",
            "LX/1yT;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1838304
    iget-object v0, p0, LX/BzB;->b:LX/1KL;

    return-object v0
.end method

.method public final b()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1838305
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c()LX/0jW;
    .locals 1

    .prologue
    .line 1838306
    iget-object v0, p0, LX/BzB;->a:Lcom/facebook/graphql/model/GraphQLStory;

    return-object v0
.end method
