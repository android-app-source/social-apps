.class public final LX/AvP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/AvL;


# instance fields
.field public final synthetic a:LX/AvQ;


# direct methods
.method public constructor <init>(LX/AvQ;)V
    .locals 0

    .prologue
    .line 1724052
    iput-object p1, p0, LX/AvP;->a:LX/AvQ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/io/File;)V
    .locals 6

    .prologue
    .line 1724053
    iget-object v0, p0, LX/AvP;->a:LX/AvQ;

    iget-object v0, v0, LX/AvQ;->b:LX/AvG;

    .line 1724054
    iget-boolean v1, v0, LX/AvG;->d:Z

    move v0, v1

    .line 1724055
    if-nez v0, :cond_0

    .line 1724056
    iget-object v0, p0, LX/AvP;->a:LX/AvQ;

    iget-object v0, v0, LX/AvQ;->c:LX/AvS;

    invoke-interface {v0, p1}, LX/AvS;->a(Ljava/io/File;)V

    .line 1724057
    :goto_0
    return-void

    .line 1724058
    :cond_0
    iget-object v0, p0, LX/AvP;->a:LX/AvQ;

    iget-object v0, v0, LX/AvQ;->a:Lcom/facebook/friendsharing/inspiration/fetch/asset/InspirationAssetDownloader;

    iget-object v0, v0, Lcom/facebook/friendsharing/inspiration/fetch/asset/InspirationAssetDownloader;->d:LX/AvM;

    iget-object v1, p0, LX/AvP;->a:LX/AvQ;

    iget-object v1, v1, LX/AvQ;->b:LX/AvG;

    .line 1724059
    iget-object v2, v1, LX/AvG;->e:Ljava/lang/String;

    move-object v1, v2

    .line 1724060
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    new-instance v3, LX/AvO;

    invoke-direct {v3, p0}, LX/AvO;-><init>(LX/AvP;)V

    .line 1724061
    iget-object v4, v0, LX/AvM;->a:Lcom/facebook/compactdisk/DiskCache;

    new-instance v5, LX/AvI;

    invoke-direct {v5, v0, v2}, LX/AvI;-><init>(LX/AvM;Ljava/lang/String;)V

    invoke-virtual {v4, v1, v5}, Lcom/facebook/compactdisk/PersistentKeyValueStore;->storeManual(Ljava/lang/String;Lcom/facebook/compactdisk/ManualWrite;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    .line 1724062
    iget-object v5, v0, LX/AvM;->b:LX/1Ck;

    sget-object p0, LX/AvK;->UNZIP_TO_CACHE:LX/AvK;

    new-instance p1, LX/AvJ;

    invoke-direct {p1, v0, v1, v3}, LX/AvJ;-><init>(LX/AvM;Ljava/lang/String;LX/AvL;)V

    invoke-virtual {v5, p0, v4, p1}, LX/1Ck;->c(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1724063
    goto :goto_0
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1724064
    iget-object v0, p0, LX/AvP;->a:LX/AvQ;

    iget-object v0, v0, LX/AvQ;->c:LX/AvS;

    invoke-interface {v0}, LX/AvS;->a()V

    .line 1724065
    return-void
.end method
