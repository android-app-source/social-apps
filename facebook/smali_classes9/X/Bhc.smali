.class public LX/Bhc;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/Bhc;


# instance fields
.field private final a:LX/0W3;


# direct methods
.method public constructor <init>(LX/0W3;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1809512
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1809513
    iput-object p1, p0, LX/Bhc;->a:LX/0W3;

    .line 1809514
    return-void
.end method

.method public static a(LX/0QB;)LX/Bhc;
    .locals 4

    .prologue
    .line 1809515
    sget-object v0, LX/Bhc;->b:LX/Bhc;

    if-nez v0, :cond_1

    .line 1809516
    const-class v1, LX/Bhc;

    monitor-enter v1

    .line 1809517
    :try_start_0
    sget-object v0, LX/Bhc;->b:LX/Bhc;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1809518
    if-eqz v2, :cond_0

    .line 1809519
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1809520
    new-instance p0, LX/Bhc;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v3

    check-cast v3, LX/0W3;

    invoke-direct {p0, v3}, LX/Bhc;-><init>(LX/0W3;)V

    .line 1809521
    move-object v0, p0

    .line 1809522
    sput-object v0, LX/Bhc;->b:LX/Bhc;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1809523
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1809524
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1809525
    :cond_1
    sget-object v0, LX/Bhc;->b:LX/Bhc;

    return-object v0

    .line 1809526
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1809527
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()J
    .locals 6

    .prologue
    .line 1809510
    iget-object v0, p0, LX/Bhc;->a:LX/0W3;

    sget-wide v2, LX/0X5;->cK:J

    const-wide/16 v4, 0x0

    invoke-interface {v0, v2, v3, v4, v5}, LX/0W4;->a(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public final b()J
    .locals 6

    .prologue
    .line 1809511
    iget-object v0, p0, LX/Bhc;->a:LX/0W3;

    sget-wide v2, LX/0X5;->cL:J

    const-wide/16 v4, 0x7d0

    invoke-interface {v0, v2, v3, v4, v5}, LX/0W4;->a(JJ)J

    move-result-wide v0

    return-wide v0
.end method
