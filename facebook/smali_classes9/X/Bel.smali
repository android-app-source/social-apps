.class public final LX/Bel;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorLeaderboardQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/crowdsourcing/grapheditor/fragment/GraphEditorScoreboardFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/crowdsourcing/grapheditor/fragment/GraphEditorScoreboardFragment;)V
    .locals 0

    .prologue
    .line 1805300
    iput-object p1, p0, LX/Bel;->a:Lcom/facebook/crowdsourcing/grapheditor/fragment/GraphEditorScoreboardFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1805316
    iget-object v0, p0, LX/Bel;->a:Lcom/facebook/crowdsourcing/grapheditor/fragment/GraphEditorScoreboardFragment;

    iget-object v0, v0, Lcom/facebook/crowdsourcing/grapheditor/fragment/GraphEditorScoreboardFragment;->c:LX/03V;

    const-string v1, "crowdsourcing_graph_editor_scoreboard_fragment"

    const-string v2, "Failed on scoreboard load"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1805317
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1805301
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1805302
    if-eqz p1, :cond_0

    .line 1805303
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1805304
    if-eqz v0, :cond_0

    .line 1805305
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1805306
    check-cast v0, Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorLeaderboardQueryModel;

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorLeaderboardQueryModel;->a()Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorLeaderboardQueryModel$AllFriendsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1805307
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1805308
    check-cast v0, Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorLeaderboardQueryModel;

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorLeaderboardQueryModel;->a()Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorLeaderboardQueryModel$AllFriendsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorLeaderboardQueryModel$AllFriendsModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1805309
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1805310
    check-cast v0, Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorLeaderboardQueryModel;

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorLeaderboardQueryModel;->a()Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorLeaderboardQueryModel$AllFriendsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorLeaderboardQueryModel$AllFriendsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1805311
    :cond_0
    iget-object v0, p0, LX/Bel;->a:Lcom/facebook/crowdsourcing/grapheditor/fragment/GraphEditorScoreboardFragment;

    iget-object v0, v0, Lcom/facebook/crowdsourcing/grapheditor/fragment/GraphEditorScoreboardFragment;->c:LX/03V;

    const-string v1, "crowdsourcing_graph_editor_scoreboard_fragment"

    const-string v2, "Scoreboard missing results"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1805312
    :goto_0
    return-void

    .line 1805313
    :cond_1
    iget-object v0, p0, LX/Bel;->a:Lcom/facebook/crowdsourcing/grapheditor/fragment/GraphEditorScoreboardFragment;

    iget-object v1, v0, Lcom/facebook/crowdsourcing/grapheditor/fragment/GraphEditorScoreboardFragment;->e:Lcom/facebook/crowdsourcing/grapheditor/adapter/GraphEditorScoreboardAdapter;

    .line 1805314
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1805315
    check-cast v0, Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorLeaderboardQueryModel;

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorLeaderboardQueryModel;->a()Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorLeaderboardQueryModel$AllFriendsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorLeaderboardQueryModel$AllFriendsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/crowdsourcing/grapheditor/adapter/GraphEditorScoreboardAdapter;->addAll(Ljava/util/Collection;)V

    goto :goto_0
.end method
