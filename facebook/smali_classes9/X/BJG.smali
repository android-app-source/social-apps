.class public LX/BJG;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:J

.field public final b:LX/9bY;

.field public final c:LX/9at;

.field public final d:Lcom/facebook/content/SecureContextHelper;

.field public final e:Landroid/content/Context;

.field public final f:Lcom/facebook/auth/viewercontext/ViewerContext;

.field public final g:LX/BK9;

.field public h:LX/BJD;

.field public i:LX/BJF;

.field public final j:LX/BK5;

.field public final k:Landroid/support/v4/app/Fragment;

.field public l:Z

.field public m:LX/0ht;

.field public n:Z

.field public final o:LX/0Sh;

.field public final p:LX/9bq;

.field public final q:LX/9b2;


# direct methods
.method public constructor <init>(LX/BK9;LX/BK5;Landroid/support/v4/app/Fragment;Lcom/facebook/auth/viewercontext/ViewerContext;Ljava/lang/String;LX/9bY;LX/9at;Lcom/facebook/content/SecureContextHelper;Landroid/content/Context;LX/0Sh;LX/9bq;LX/9b2;)V
    .locals 2
    .param p1    # LX/BK9;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/BK5;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Landroid/support/v4/app/Fragment;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/auth/viewercontext/ViewerContext;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1771724
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1771725
    iput-object p1, p0, LX/BJG;->g:LX/BK9;

    .line 1771726
    iput-object p2, p0, LX/BJG;->j:LX/BK5;

    .line 1771727
    iput-object p3, p0, LX/BJG;->k:Landroid/support/v4/app/Fragment;

    .line 1771728
    iput-object p4, p0, LX/BJG;->f:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1771729
    invoke-static {p5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, LX/BJG;->a:J

    .line 1771730
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/BJG;->l:Z

    .line 1771731
    iput-object p6, p0, LX/BJG;->b:LX/9bY;

    .line 1771732
    iput-object p7, p0, LX/BJG;->c:LX/9at;

    .line 1771733
    iput-object p8, p0, LX/BJG;->d:Lcom/facebook/content/SecureContextHelper;

    .line 1771734
    iput-object p9, p0, LX/BJG;->e:Landroid/content/Context;

    .line 1771735
    iput-object p10, p0, LX/BJG;->o:LX/0Sh;

    .line 1771736
    iput-object p11, p0, LX/BJG;->p:LX/9bq;

    .line 1771737
    iput-object p12, p0, LX/BJG;->q:LX/9b2;

    .line 1771738
    return-void
.end method

.method public static d(LX/BJG;)Landroid/view/View;
    .locals 14
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "InflateParams",
            "BadArgument-LayoutInflater#inflate-0"
        }
    .end annotation

    .prologue
    .line 1771739
    iget-object v0, p0, LX/BJG;->e:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030fb6

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 1771740
    const v3, 0x7f0d051c

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ListView;

    .line 1771741
    const v4, 0x7f0d051d

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ProgressBar;

    .line 1771742
    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1771743
    new-instance v5, LX/BJF;

    iget-object v6, p0, LX/BJG;->j:LX/BK5;

    invoke-virtual {v6}, LX/BK5;->b()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v6

    iget-object v7, p0, LX/BJG;->f:Lcom/facebook/auth/viewercontext/ViewerContext;

    if-eqz v7, :cond_0

    iget-object v7, p0, LX/BJG;->f:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1771744
    iget-object v8, v7, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v7, v8

    .line 1771745
    invoke-static {v7}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v7

    :goto_0
    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    iget-object v8, p0, LX/BJG;->e:Landroid/content/Context;

    iget-object v10, p0, LX/BJG;->o:LX/0Sh;

    iget-object v11, p0, LX/BJG;->p:LX/9bq;

    iget-object v12, p0, LX/BJG;->q:LX/9b2;

    new-instance v13, LX/BJB;

    invoke-direct {v13, p0, v4}, LX/BJB;-><init>(LX/BJG;Landroid/widget/ProgressBar;)V

    move-object v9, v3

    invoke-direct/range {v5 .. v13}, LX/BJF;-><init>(Lcom/facebook/ipc/composer/intent/ComposerTargetData;Ljava/lang/Long;Landroid/content/Context;Landroid/widget/ListView;LX/0Sh;LX/9bq;LX/9b2;LX/BJB;)V

    iput-object v5, p0, LX/BJG;->i:LX/BJF;

    .line 1771746
    const v1, 0x7f0d25f0

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 1771747
    new-instance v2, LX/BJA;

    invoke-direct {v2, p0}, LX/BJA;-><init>(LX/BJG;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1771748
    return-object v0

    .line 1771749
    :cond_0
    iget-wide v7, p0, LX/BJG;->a:J

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1771750
    iget-boolean v0, p0, LX/BJG;->l:Z

    if-nez v0, :cond_0

    .line 1771751
    :goto_0
    return-void

    .line 1771752
    :cond_0
    iget-object v0, p0, LX/BJG;->b:LX/9bY;

    iget-object v1, p0, LX/BJG;->h:LX/BJD;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    goto :goto_0
.end method
