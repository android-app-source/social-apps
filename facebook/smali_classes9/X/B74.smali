.class public final LX/B74;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field public final synthetic a:Lcom/facebook/leadgen/LeadGenUserInputFormFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/leadgen/LeadGenUserInputFormFragment;)V
    .locals 0

    .prologue
    .line 1747542
    iput-object p1, p0, LX/B74;->a:Lcom/facebook/leadgen/LeadGenUserInputFormFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onGlobalLayout()V
    .locals 4

    .prologue
    .line 1747543
    iget-object v0, p0, LX/B74;->a:Lcom/facebook/leadgen/LeadGenUserInputFormFragment;

    iget-object v1, p0, LX/B74;->a:Lcom/facebook/leadgen/LeadGenUserInputFormFragment;

    iget-object v1, v1, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->m:LX/B6p;

    new-instance v2, LX/B73;

    invoke-direct {v2, p0}, LX/B73;-><init>(LX/B74;)V

    iget-object v3, p0, LX/B74;->a:Lcom/facebook/leadgen/LeadGenUserInputFormFragment;

    iget-object v3, v3, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->r:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v3}, Landroid/support/v4/view/ViewPager;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/B6p;->a(LX/B6D;Landroid/content/Context;)LX/B6o;

    move-result-object v1

    .line 1747544
    iput-object v1, v0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->B:LX/B6o;

    .line 1747545
    iget-object v0, p0, LX/B74;->a:Lcom/facebook/leadgen/LeadGenUserInputFormFragment;

    iget-object v0, v0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->B:LX/B6o;

    iget-object v1, p0, LX/B74;->a:Lcom/facebook/leadgen/LeadGenUserInputFormFragment;

    iget-object v1, v1, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->w:LX/B7F;

    invoke-virtual {v1}, LX/B7F;->u()Z

    move-result v1

    invoke-virtual {v0, v1}, LX/B6o;->a(Z)V

    .line 1747546
    iget-object v0, p0, LX/B74;->a:Lcom/facebook/leadgen/LeadGenUserInputFormFragment;

    iget-object v0, v0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->B:LX/B6o;

    invoke-virtual {v0}, LX/B6o;->a()V

    .line 1747547
    iget-object v0, p0, LX/B74;->a:Lcom/facebook/leadgen/LeadGenUserInputFormFragment;

    iget-object v1, p0, LX/B74;->a:Lcom/facebook/leadgen/LeadGenUserInputFormFragment;

    iget-object v1, v1, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->n:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    .line 1747548
    iput-object v1, v0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->C:Landroid/view/ViewTreeObserver;

    .line 1747549
    iget-object v0, p0, LX/B74;->a:Lcom/facebook/leadgen/LeadGenUserInputFormFragment;

    iget-object v0, v0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->C:Landroid/view/ViewTreeObserver;

    iget-object v1, p0, LX/B74;->a:Lcom/facebook/leadgen/LeadGenUserInputFormFragment;

    iget-object v1, v1, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->B:LX/B6o;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnScrollChangedListener(Landroid/view/ViewTreeObserver$OnScrollChangedListener;)V

    .line 1747550
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_0

    .line 1747551
    iget-object v0, p0, LX/B74;->a:Lcom/facebook/leadgen/LeadGenUserInputFormFragment;

    iget-object v0, v0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->C:Landroid/view/ViewTreeObserver;

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 1747552
    :goto_0
    return-void

    .line 1747553
    :cond_0
    iget-object v0, p0, LX/B74;->a:Lcom/facebook/leadgen/LeadGenUserInputFormFragment;

    iget-object v0, v0, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->C:Landroid/view/ViewTreeObserver;

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    goto :goto_0
.end method
