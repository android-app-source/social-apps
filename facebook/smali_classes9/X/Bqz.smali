.class public LX/Bqz;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Bqx;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Br0;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1825717
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/Bqz;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/Br0;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1825718
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1825719
    iput-object p1, p0, LX/Bqz;->b:LX/0Ot;

    .line 1825720
    return-void
.end method

.method public static a(LX/0QB;)LX/Bqz;
    .locals 4

    .prologue
    .line 1825723
    const-class v1, LX/Bqz;

    monitor-enter v1

    .line 1825724
    :try_start_0
    sget-object v0, LX/Bqz;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1825725
    sput-object v2, LX/Bqz;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1825726
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1825727
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1825728
    new-instance v3, LX/Bqz;

    const/16 p0, 0x1d0c

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/Bqz;-><init>(LX/0Ot;)V

    .line 1825729
    move-object v0, v3

    .line 1825730
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1825731
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Bqz;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1825732
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1825733
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1825721
    invoke-static {}, LX/1dS;->b()V

    .line 1825722
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b(LX/1De;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1825703
    iget-object v0, p0, LX/Bqz;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    .line 1825704
    new-instance v0, LX/1oJ;

    invoke-direct {v0}, LX/1oJ;-><init>()V

    move-object v0, v0

    .line 1825705
    return-object v0
.end method

.method public final b(LX/1De;LX/1X1;)V
    .locals 7

    .prologue
    .line 1825706
    check-cast p2, LX/Bqy;

    .line 1825707
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v5

    .line 1825708
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v6

    .line 1825709
    iget-object v0, p0, LX/Bqz;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Br0;

    iget-boolean v1, p2, LX/Bqy;->a:Z

    iget-object v2, p2, LX/Bqy;->b:LX/Br2;

    iget-object v3, p2, LX/Bqy;->c:LX/1Pq;

    iget-object v4, p2, LX/Bqy;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual/range {v0 .. v6}, LX/Br0;->a(ZLX/Br2;LX/1Pq;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1np;LX/1np;)V

    .line 1825710
    iget-object v0, v5, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 1825711
    check-cast v0, LX/8wT;

    iput-object v0, p2, LX/Bqy;->e:LX/8wT;

    .line 1825712
    invoke-static {v5}, LX/1cy;->a(LX/1np;)V

    .line 1825713
    iget-object v0, v6, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 1825714
    check-cast v0, LX/Bqu;

    iput-object v0, p2, LX/Bqy;->f:LX/Bqu;

    .line 1825715
    invoke-static {v6}, LX/1cy;->a(LX/1np;)V

    .line 1825716
    return-void
.end method

.method public final e(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 3

    .prologue
    .line 1825662
    check-cast p3, LX/Bqy;

    .line 1825663
    iget-object v0, p0, LX/Bqz;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Br0;

    iget-object v1, p3, LX/Bqy;->e:LX/8wT;

    iget-object v2, p3, LX/Bqy;->f:LX/Bqu;

    .line 1825664
    iget-object p0, v0, LX/Br0;->b:LX/8wR;

    invoke-virtual {p0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 1825665
    iget-object p0, v0, LX/Br0;->c:LX/2PZ;

    invoke-virtual {p0, v2}, LX/2PZ;->a(LX/Bqu;)V

    .line 1825666
    return-void
.end method

.method public final f()LX/1mv;
    .locals 1

    .prologue
    .line 1825661
    sget-object v0, LX/1mv;->DRAWABLE:LX/1mv;

    return-object v0
.end method

.method public final f(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 3

    .prologue
    .line 1825667
    check-cast p3, LX/Bqy;

    .line 1825668
    iget-object v0, p0, LX/Bqz;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Br0;

    iget-object v1, p3, LX/Bqy;->e:LX/8wT;

    iget-object v2, p3, LX/Bqy;->f:LX/Bqu;

    .line 1825669
    iget-object p0, v0, LX/Br0;->b:LX/8wR;

    invoke-virtual {p0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 1825670
    iget-object p0, v0, LX/Br0;->c:LX/2PZ;

    invoke-virtual {p0, v2}, LX/2PZ;->b(LX/Bqu;)V

    .line 1825671
    return-void
.end method

.method public final g(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 7

    .prologue
    .line 1825673
    check-cast p3, LX/Bqy;

    .line 1825674
    iget-object v0, p0, LX/Bqz;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Br0;

    iget-object v1, p3, LX/Bqy;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p3, LX/Bqy;->g:Ljava/lang/String;

    .line 1825675
    iget-object v4, v0, LX/Br0;->a:LX/Bqw;

    .line 1825676
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 1825677
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStory;

    const/4 p1, 0x0

    .line 1825678
    invoke-static {v3}, LX/Bqw;->g(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v5

    if-eqz v5, :cond_4

    sget-object v6, LX/8wV;->BOOSTED_EVENT_MOBILE_MODULE:LX/8wV;

    .line 1825679
    :goto_0
    invoke-static {v3}, LX/1VF;->d(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    if-eqz v5, :cond_5

    invoke-static {v3}, LX/1VF;->d(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    .line 1825680
    :goto_1
    iget-object v5, v4, LX/Bqw;->e:LX/2Pb;

    .line 1825681
    sget-object p0, LX/Bqt;->a:[I

    invoke-static {v3}, LX/1Wq;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    move-result-object p2

    invoke-virtual {p2}, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->ordinal()I

    move-result p2

    aget p0, p0, p2

    packed-switch p0, :pswitch_data_0

    .line 1825682
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->ay()Lcom/facebook/graphql/model/GraphQLBoostedComponent;

    move-result-object p0

    if-eqz p0, :cond_7

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->ay()Lcom/facebook/graphql/model/GraphQLBoostedComponent;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLBoostedComponent;->j()Lcom/facebook/graphql/model/GraphQLBoostedComponentMessage;

    move-result-object p0

    if-eqz p0, :cond_7

    sget-object p0, LX/8wW;->EVENT_RENDER_FAIL_ENTRY_POINT:LX/8wW;

    :goto_2
    move-object p0, p0

    .line 1825683
    invoke-static {v3}, LX/185;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object p2

    if-eqz p2, :cond_0

    invoke-static {v3}, LX/185;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object p1

    :cond_0
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->al()Ljava/lang/String;

    move-result-object p3

    if-eqz v0, :cond_6

    const/4 v1, 0x1

    :goto_3
    move-object p2, v2

    .line 1825684
    :try_start_0
    iget-object v4, v5, LX/2Pb;->a:LX/0Zb;

    invoke-virtual {p0}, LX/8wW;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v2, 0x1

    invoke-interface {v4, v3, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v4

    .line 1825685
    invoke-virtual {v4}, LX/0oG;->a()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1825686
    invoke-virtual {v6}, LX/8wV;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 1825687
    const-string v3, "post_id"

    invoke-virtual {v4, v3, p3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1825688
    const-string v3, "placement"

    invoke-virtual {v4, v3, p2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1825689
    const-string v3, "page_id"

    invoke-virtual {v4, v3, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1825690
    const-string v3, "connection_quality_class"

    iget-object v2, v5, LX/2Pb;->b:LX/0oz;

    invoke-virtual {v2}, LX/0oz;->c()LX/0p3;

    move-result-object v2

    invoke-virtual {v2}, LX/0p3;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v3, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1825691
    sget-object v3, LX/8wW;->EVENT_RENDER_FAIL_ENTRY_POINT:LX/8wW;

    if-ne p0, v3, :cond_1

    .line 1825692
    const-string v3, "ineligible_reason"

    invoke-virtual {v4, v3, v0}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1825693
    :cond_1
    if-eqz v1, :cond_2

    .line 1825694
    const-string v3, "flow"

    const-string v2, "ineligible"

    invoke-virtual {v4, v3, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1825695
    :cond_2
    invoke-virtual {v4}, LX/0oG;->d()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1825696
    :cond_3
    :goto_4
    return-void

    .line 1825697
    :cond_4
    sget-object v6, LX/8wV;->BOOSTED_POST_MOBILE_MODULE:LX/8wV;

    goto/16 :goto_0

    :cond_5
    move-object v0, p1

    .line 1825698
    goto/16 :goto_1

    .line 1825699
    :cond_6
    const/4 v1, 0x0

    goto :goto_3

    .line 1825700
    :pswitch_0
    sget-object p0, LX/8wW;->EVENT_RENDER_CREATE_ENTRY_POINT:LX/8wW;

    goto :goto_2

    .line 1825701
    :pswitch_1
    sget-object p0, LX/8wW;->EVENT_RENDER_EDIT_ENTRY_POINT:LX/8wW;

    goto :goto_2

    .line 1825702
    :cond_7
    sget-object p0, LX/8wW;->EVENT_RENDER_INSIGHTS_ENTRY_POINT:LX/8wW;

    goto :goto_2

    :catch_0
    goto :goto_4

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 1825672
    const/16 v0, 0xf

    return v0
.end method
