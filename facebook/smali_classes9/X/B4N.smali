.class public final LX/B4N;
.super LX/B4J;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/B4J",
        "<",
        "LX/B4N;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1741562
    invoke-direct {p0, p1, p2}, LX/B4J;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1741563
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/heisman/intent/ProfilePictureOverlayPivotIntentData;
    .locals 4

    .prologue
    .line 1741564
    invoke-virtual {p0}, LX/B4J;->c()Lcom/facebook/heisman/intent/ProfilePictureOverlayCommonParams;

    move-result-object v1

    .line 1741565
    iget-object v0, p0, LX/B4N;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, LX/B4N;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v2, "Must have either an image overlay ID or a category ID"

    invoke-static {v0, v2}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1741566
    new-instance v0, Lcom/facebook/heisman/intent/ProfilePictureOverlayPivotIntentData;

    iget-object v2, p0, LX/B4N;->a:Ljava/lang/String;

    iget-object v3, p0, LX/B4N;->b:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/heisman/intent/ProfilePictureOverlayPivotIntentData;-><init>(Lcom/facebook/heisman/intent/ProfilePictureOverlayCommonParams;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    .line 1741567
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic b()LX/B4J;
    .locals 1

    .prologue
    .line 1741568
    move-object v0, p0

    .line 1741569
    return-object v0
.end method

.method public final b(Ljava/lang/String;)LX/B4N;
    .locals 1

    .prologue
    .line 1741570
    iput-object p1, p0, LX/B4N;->a:Ljava/lang/String;

    .line 1741571
    move-object v0, p0

    .line 1741572
    return-object v0
.end method

.method public final c(Ljava/lang/String;)LX/B4N;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1741573
    iput-object p1, p0, LX/B4N;->b:Ljava/lang/String;

    .line 1741574
    move-object v0, p0

    .line 1741575
    return-object v0
.end method
