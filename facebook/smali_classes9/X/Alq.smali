.class public final LX/Alq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/AQ4;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/AQ4",
        "<",
        "Landroid/content/Intent;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Alt;


# direct methods
.method public constructor <init>(LX/Alt;)V
    .locals 0

    .prologue
    .line 1710305
    iput-object p1, p0, LX/Alq;->a:LX/Alt;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 6

    .prologue
    .line 1710298
    iget-object v0, p0, LX/Alq;->a:LX/Alt;

    iget-object v0, v0, LX/Alt;->b:LX/BMQ;

    iget-object v1, p0, LX/Alq;->a:LX/Alt;

    .line 1710299
    invoke-virtual {v1}, LX/AQ9;->R()LX/B5j;

    move-result-object v2

    move-object v1, v2

    .line 1710300
    iget-object v2, p0, LX/Alq;->a:LX/Alt;

    .line 1710301
    iget-object v3, v2, LX/AQ9;->q:LX/ARN;

    move-object v2, v3

    .line 1710302
    iget-object v3, p0, LX/Alq;->a:LX/Alt;

    iget-object v3, v3, LX/Alt;->a:Lcom/facebook/feed/photoreminder/composer/PhotoReminderPluginConfig;

    invoke-virtual {v3}, Lcom/facebook/feed/photoreminder/composer/PhotoReminderPluginConfig;->c()Lcom/facebook/productionprompts/logging/PromptAnalytics;

    move-result-object v3

    const-class v4, LX/1kJ;

    iget-object v5, p0, LX/Alq;->a:LX/Alt;

    .line 1710303
    invoke-virtual {v5}, LX/AQ9;->R()LX/B5j;

    move-result-object p0

    invoke-virtual {p0}, LX/B5j;->d()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object p0

    invoke-interface {p0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object p0

    invoke-virtual {p0}, LX/0Px;->toArray()[Ljava/lang/Object;

    move-result-object p0

    array-length p0, p0

    if-eqz p0, :cond_0

    const/4 p0, 0x1

    :goto_0
    move v5, p0

    .line 1710304
    invoke-virtual/range {v0 .. v5}, LX/BMQ;->a(LX/B5j;LX/ARN;Lcom/facebook/productionprompts/logging/PromptAnalytics;Ljava/lang/Class;Z)Landroid/content/Intent;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 p0, 0x0

    goto :goto_0
.end method
