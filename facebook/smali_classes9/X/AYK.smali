.class public LX/AYK;
.super LX/AWT;
.source ""


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/sounds/FBSoundUtil;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3RZ;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/fbui/glyph/GlyphView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1685162
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/AYK;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1685163
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1685164
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/AYK;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1685165
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1685166
    invoke-direct {p0, p1, p2, p3}, LX/AWT;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1685167
    const-class v0, LX/AYK;

    invoke-static {v0, p0}, LX/AYK;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1685168
    const v0, 0x7f0305e8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1685169
    const v0, 0x7f0d1045

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, LX/AYK;->c:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1685170
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p1, LX/AYK;

    const/16 v2, 0x3567

    invoke-static {v1, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 p0, 0x11ee

    invoke-static {v1, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    iput-object v2, p1, LX/AYK;->a:LX/0Ot;

    iput-object v1, p1, LX/AYK;->b:LX/0Ot;

    return-void
.end method


# virtual methods
.method public final g()V
    .locals 2

    .prologue
    .line 1685171
    iget-object v0, p0, LX/AYK;->c:Lcom/facebook/fbui/glyph/GlyphView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1685172
    return-void
.end method

.method public final h()V
    .locals 2

    .prologue
    .line 1685173
    iget-object v0, p0, LX/AYK;->c:Lcom/facebook/fbui/glyph/GlyphView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1685174
    return-void
.end method
