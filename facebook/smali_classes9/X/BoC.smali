.class public final LX/BoC;
.super LX/0Vd;
.source ""


# instance fields
.field public final synthetic a:LX/BoD;


# direct methods
.method public constructor <init>(LX/BoD;)V
    .locals 0

    .prologue
    .line 1821601
    iput-object p1, p0, LX/BoC;->a:LX/BoD;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1821599
    iget-object v0, p0, LX/BoC;->a:LX/BoD;

    iget-object v0, v0, LX/BoD;->f:LX/1dt;

    iget-object v0, v0, LX/1dt;->k:LX/03V;

    sget-object v1, LX/1dt;->a:Ljava/lang/String;

    const-string v2, "hide topic from user failed"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1821600
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 1821587
    iget-object v0, p0, LX/BoC;->a:LX/BoD;

    iget-object v0, v0, LX/BoD;->f:LX/1dt;

    invoke-virtual {v0}, LX/1SX;->b()LX/1Pf;

    move-result-object v0

    new-instance v4, LX/C7W;

    iget-object v5, p0, LX/BoC;->a:LX/BoD;

    iget-object v5, v5, LX/BoD;->e:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-direct {v4, v5}, LX/C7W;-><init>(Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;)V

    iget-object v5, p0, LX/BoC;->a:LX/BoD;

    iget-object v5, v5, LX/BoD;->e:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-interface {v0, v4, v5}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/C7V;

    iget-object v4, p0, LX/BoC;->a:LX/BoD;

    iget-object v4, v4, LX/BoD;->d:Lcom/facebook/graphql/model/GraphQLActor;

    iget-object v5, p0, LX/BoC;->a:LX/BoD;

    iget-object v5, v5, LX/BoD;->c:Lcom/facebook/graphql/model/GraphQLExploreFeed;

    .line 1821588
    if-eqz v4, :cond_0

    if-nez v5, :cond_2

    .line 1821589
    :cond_0
    :goto_0
    iget-object v0, p0, LX/BoC;->a:LX/BoD;

    iget-object v0, v0, LX/BoD;->f:LX/1dt;

    iget-object v4, v0, LX/1dt;->w:LX/1e8;

    iget-object v0, p0, LX/BoC;->a:LX/BoD;

    iget-object v5, v0, LX/BoD;->e:Lcom/facebook/graphql/model/GraphQLStory;

    iget-object v0, p0, LX/BoC;->a:LX/BoD;

    iget-object v0, v0, LX/BoD;->d:Lcom/facebook/graphql/model/GraphQLActor;

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {v4, v5, v0}, LX/1e8;->a(Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;Z)V

    .line 1821590
    iget-object v0, p0, LX/BoC;->a:LX/BoD;

    iget-object v0, v0, LX/BoD;->f:LX/1dt;

    invoke-virtual {v0}, LX/1SX;->b()LX/1Pf;

    move-result-object v0

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v4, p0, LX/BoC;->a:LX/BoD;

    iget-object v4, v4, LX/BoD;->e:Lcom/facebook/graphql/model/GraphQLStory;

    aput-object v4, v1, v3

    invoke-interface {v0, v1}, LX/1Pq;->a([Ljava/lang/Object;)V

    .line 1821591
    iget-object v0, p0, LX/BoC;->a:LX/BoD;

    iget-object v0, v0, LX/BoD;->f:LX/1dt;

    iget-object v6, v0, LX/1dt;->z:LX/0bH;

    new-instance v0, LX/1Nd;

    iget-object v1, p0, LX/BoC;->a:LX/BoD;

    iget-object v1, v1, LX/BoD;->e:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v1

    sget-object v4, Lcom/facebook/graphql/enums/StoryVisibility;->HIDDEN:Lcom/facebook/graphql/enums/StoryVisibility;

    iget-object v3, p0, LX/BoC;->a:LX/BoD;

    iget-object v3, v3, LX/BoD;->e:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->H_()I

    move-result v5

    move-object v3, v2

    invoke-direct/range {v0 .. v5}, LX/1Nd;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/StoryVisibility;I)V

    invoke-virtual {v6, v0}, LX/0b4;->a(LX/0b7;)V

    .line 1821592
    iget-object v0, p0, LX/BoC;->a:LX/BoD;

    iget-object v0, v0, LX/BoD;->f:LX/1dt;

    iget-object v0, v0, LX/1dt;->z:LX/0bH;

    new-instance v1, LX/1YM;

    invoke-direct {v1}, LX/1YM;-><init>()V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1821593
    return-void

    :cond_1
    move v0, v3

    .line 1821594
    goto :goto_1

    .line 1821595
    :cond_2
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v0, LX/C7V;->a:Ljava/lang/String;

    .line 1821596
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v0, LX/C7V;->b:Ljava/lang/String;

    .line 1821597
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLExploreFeed;->j()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v0, LX/C7V;->c:Ljava/lang/String;

    .line 1821598
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLExploreFeed;->k()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v0, LX/C7V;->d:Ljava/lang/String;

    goto :goto_0
.end method
