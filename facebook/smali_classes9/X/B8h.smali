.class public final LX/B8h;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    .line 1750361
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1750362
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 1750363
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 1750364
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1750365
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_9

    .line 1750366
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1750367
    :goto_1
    move v1, v2

    .line 1750368
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1750369
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0

    .line 1750370
    :cond_1
    const-string v10, "is_checked_by_default"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 1750371
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v4

    move v7, v4

    move v4, v3

    .line 1750372
    :cond_2
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_6

    .line 1750373
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 1750374
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1750375
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_2

    if-eqz v9, :cond_2

    .line 1750376
    const-string v10, "checkbox_body"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 1750377
    const/4 v9, 0x0

    .line 1750378
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v10, LX/15z;->START_OBJECT:LX/15z;

    if-eq v8, v10, :cond_d

    .line 1750379
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1750380
    :goto_3
    move v8, v9

    .line 1750381
    goto :goto_2

    .line 1750382
    :cond_3
    const-string v10, "is_required"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 1750383
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v1

    move v6, v1

    move v1, v3

    goto :goto_2

    .line 1750384
    :cond_4
    const-string v10, "token_key"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 1750385
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_2

    .line 1750386
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_2

    .line 1750387
    :cond_6
    const/4 v9, 0x4

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 1750388
    invoke-virtual {p1, v2, v8}, LX/186;->b(II)V

    .line 1750389
    if-eqz v4, :cond_7

    .line 1750390
    invoke-virtual {p1, v3, v7}, LX/186;->a(IZ)V

    .line 1750391
    :cond_7
    if-eqz v1, :cond_8

    .line 1750392
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v6}, LX/186;->a(IZ)V

    .line 1750393
    :cond_8
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 1750394
    invoke-virtual {p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_1

    :cond_9
    move v1, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    move v8, v2

    goto :goto_2

    .line 1750395
    :cond_a
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1750396
    :cond_b
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_c

    .line 1750397
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 1750398
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1750399
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_b

    if-eqz v10, :cond_b

    .line 1750400
    const-string v11, "text"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_a

    .line 1750401
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_4

    .line 1750402
    :cond_c
    const/4 v10, 0x1

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 1750403
    invoke-virtual {p1, v9, v8}, LX/186;->b(II)V

    .line 1750404
    invoke-virtual {p1}, LX/186;->d()I

    move-result v9

    goto/16 :goto_3

    :cond_d
    move v8, v9

    goto :goto_4
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1750405
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1750406
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1750407
    if-eqz v0, :cond_1

    .line 1750408
    const-string v1, "checkbox_body"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1750409
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1750410
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1750411
    if-eqz v1, :cond_0

    .line 1750412
    const-string p3, "text"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1750413
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1750414
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1750415
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1750416
    if-eqz v0, :cond_2

    .line 1750417
    const-string v1, "is_checked_by_default"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1750418
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1750419
    :cond_2
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1750420
    if-eqz v0, :cond_3

    .line 1750421
    const-string v1, "is_required"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1750422
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1750423
    :cond_3
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1750424
    if-eqz v0, :cond_4

    .line 1750425
    const-string v1, "token_key"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1750426
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1750427
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1750428
    return-void
.end method
