.class public final LX/AZc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/AZd;

.field public final synthetic b:Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;


# direct methods
.method public constructor <init>(Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;LX/AZd;)V
    .locals 0

    .prologue
    .line 1687163
    iput-object p1, p0, LX/AZc;->b:Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;

    iput-object p2, p0, LX/AZc;->a:LX/AZd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v3, 0x1

    const/4 v8, 0x2

    const v0, -0x68029fc

    invoke-static {v8, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1687164
    iget-object v1, p0, LX/AZc;->b:Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;

    iget-object v1, v1, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;->h:LX/AZd;

    if-eqz v1, :cond_0

    .line 1687165
    iget-object v1, p0, LX/AZc;->b:Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;

    iget-object v1, v1, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;->h:LX/AZd;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/AZd;->setSelected(Z)V

    .line 1687166
    :cond_0
    iget-object v1, p0, LX/AZc;->b:Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;

    iget-object v2, p0, LX/AZc;->a:LX/AZd;

    .line 1687167
    iput-object v2, v1, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;->h:LX/AZd;

    .line 1687168
    iget-object v1, p0, LX/AZc;->b:Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;

    iget-object v1, v1, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;->h:LX/AZd;

    invoke-virtual {v1, v3}, LX/AZd;->setSelected(Z)V

    .line 1687169
    iget-object v1, p0, LX/AZc;->b:Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;

    iget-object v1, v1, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;->g:LX/AZe;

    if-eqz v1, :cond_1

    .line 1687170
    iget-object v1, p0, LX/AZc;->b:Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;

    iget-object v1, v1, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;->h:LX/AZd;

    .line 1687171
    iget v2, v1, LX/AZd;->e:I

    move v1, v2

    .line 1687172
    const/high16 v2, 0xff0000

    and-int/2addr v2, v1

    shr-int/lit8 v2, v2, 0x10

    .line 1687173
    const v3, 0xff00

    and-int/2addr v3, v1

    shr-int/lit8 v3, v3, 0x8

    .line 1687174
    and-int/lit16 v1, v1, 0xff

    .line 1687175
    iget-object v4, p0, LX/AZc;->b:Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;

    iget-object v4, v4, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;->a:LX/AVT;

    iget-object v5, p0, LX/AZc;->b:Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;

    iget-object v5, v5, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;->g:LX/AZe;

    invoke-virtual {v5}, LX/AZe;->d()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "color "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, ","

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v5, v1}, LX/Aa6;->c(LX/AVT;Ljava/lang/String;Ljava/lang/String;)V

    .line 1687176
    :cond_1
    const v1, -0x681c40f7

    invoke-static {v8, v8, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
