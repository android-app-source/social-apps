.class public LX/B9g;
.super Lcom/facebook/fbui/widget/megaphone/Megaphone;
.source ""

# interfaces
.implements LX/76V;


# instance fields
.field public a:LX/13A;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/3Rb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

.field public f:LX/78A;

.field private g:Z

.field private h:Ljava/lang/Runnable;

.field public i:LX/B9V;

.field private j:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1752136
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/B9g;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1752137
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1752035
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/B9g;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1752036
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1752131
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/fbui/widget/megaphone/Megaphone;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1752132
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/B9g;->j:Z

    .line 1752133
    const-class v0, LX/B9g;

    invoke-static {v0, p0}, LX/B9g;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1752134
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/B9g;->g:Z

    .line 1752135
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/B9g;

    const-class v1, LX/13A;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/13A;

    invoke-static {p0}, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;->a(LX/0QB;)Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;

    move-result-object v2

    check-cast v2, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;

    invoke-static {p0}, LX/3Rb;->a(LX/0QB;)LX/3Rb;

    move-result-object v3

    check-cast v3, LX/3Rb;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object p0

    check-cast p0, LX/0Uh;

    iput-object v1, p1, LX/B9g;->a:LX/13A;

    iput-object v2, p1, LX/B9g;->b:Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;

    iput-object v3, p1, LX/B9g;->c:LX/3Rb;

    iput-object p0, p1, LX/B9g;->d:LX/0Uh;

    return-void
.end method

.method public static j(LX/B9g;)V
    .locals 1

    .prologue
    .line 1752138
    iget-object v0, p0, LX/B9g;->h:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 1752139
    iget-object v0, p0, LX/B9g;->h:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 1752140
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/B9g;->g:Z

    .line 1752141
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, LX/B9g;->setVisibility(I)V

    .line 1752142
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Ljava/lang/String;Lcom/facebook/interstitial/manager/InterstitialTrigger;)V
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v6, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1752065
    iget-object v0, p0, LX/B9g;->e:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    if-ne v0, p1, :cond_1

    .line 1752066
    iget-boolean v0, p0, LX/B9g;->g:Z

    if-eqz v0, :cond_0

    .line 1752067
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, LX/B9g;->setVisibility(I)V

    .line 1752068
    :cond_0
    :goto_0
    return-void

    .line 1752069
    :cond_1
    iget-object v0, p0, LX/B9g;->d:LX/0Uh;

    const/16 v3, 0x3ff

    invoke-virtual {v0, v3, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1752070
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->h()V

    .line 1752071
    :cond_2
    iput-object p1, p0, LX/B9g;->e:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    .line 1752072
    iget-object v0, p0, LX/B9g;->e:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    invoke-virtual {v0}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->c()Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    move-result-object v4

    .line 1752073
    iget-object v0, p0, LX/B9g;->a:LX/13A;

    iget-object v3, p0, LX/B9g;->e:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    invoke-virtual {v0, v3, p2, v4, p3}, LX/13A;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Ljava/lang/String;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/78A;

    move-result-object v0

    iput-object v0, p0, LX/B9g;->f:LX/78A;

    .line 1752074
    new-instance v0, LX/B9d;

    invoke-direct {v0, p0}, LX/B9d;-><init>(LX/B9g;)V

    .line 1752075
    new-instance v3, LX/B9e;

    invoke-direct {v3, p0}, LX/B9e;-><init>(LX/B9g;)V

    .line 1752076
    new-instance v5, LX/B9f;

    invoke-direct {v5, p0}, LX/B9f;-><init>(LX/B9g;)V

    .line 1752077
    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setOnPrimaryButtonClickListener(Landroid/view/View$OnClickListener;)V

    .line 1752078
    invoke-virtual {p0, v3}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setOnSecondaryButtonClickListener(Landroid/view/View$OnClickListener;)V

    .line 1752079
    iput-object v5, p0, Lcom/facebook/fbui/widget/megaphone/Megaphone;->m:LX/AhV;

    .line 1752080
    if-nez v4, :cond_3

    .line 1752081
    invoke-static {p0}, LX/B9g;->j(LX/B9g;)V

    goto :goto_0

    .line 1752082
    :cond_3
    iget-object v0, v4, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->primaryAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    if-eqz v0, :cond_4

    iget-object v0, v4, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->primaryAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    iget-object v0, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;->title:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1752083
    :cond_4
    invoke-virtual {p0, v2}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setShowPrimaryButton(Z)V

    move v0, v2

    .line 1752084
    :goto_1
    iget-object v3, v4, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->secondaryAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    if-eqz v3, :cond_5

    iget-object v3, v4, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->secondaryAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    iget-object v3, v3, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;->title:Ljava/lang/String;

    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 1752085
    :cond_5
    invoke-virtual {p0, v2}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setShowSecondaryButton(Z)V

    .line 1752086
    :goto_2
    iget-object v3, v4, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->dismissAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    if-eqz v3, :cond_9

    move v3, v1

    :goto_3
    invoke-virtual {p0, v3}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setShowCloseButton(Z)V

    .line 1752087
    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setShowButtonsContainer(Z)V

    .line 1752088
    iget-object v0, p0, LX/B9g;->b:Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;

    iget-object v3, p0, LX/B9g;->e:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    invoke-virtual {v3}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->c()Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    move-result-object v3

    sget-object v5, LX/76S;->ANY:LX/76S;

    invoke-virtual {v0, v3, v5}, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;LX/76S;)LX/1bf;

    move-result-object v0

    .line 1752089
    if-eqz v0, :cond_b

    .line 1752090
    invoke-virtual {p1}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->c()Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    move-result-object v0

    sget-object v3, LX/76S;->ANY:LX/76S;

    invoke-static {v0, v3}, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;->b(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;LX/76S;)Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;

    move-result-object v0

    .line 1752091
    iget-object v3, p0, LX/B9g;->i:LX/B9V;

    if-eqz v3, :cond_a

    .line 1752092
    iget-object v3, p0, LX/B9g;->i:LX/B9V;

    iget-object v0, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;->uri:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/B9V;->setImageUri(Landroid/net/Uri;)V

    .line 1752093
    iget-object v0, p0, LX/B9g;->i:LX/B9V;

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setImageView(Landroid/view/View;)V

    .line 1752094
    :goto_4
    iget-object v0, p0, LX/B9g;->e:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    iget-object v0, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->title:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 1752095
    invoke-virtual {p0, v2}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setShowTitle(Z)V

    .line 1752096
    :goto_5
    iget-object v0, p0, LX/B9g;->e:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    iget-object v0, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->content:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 1752097
    invoke-virtual {p0, v2}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setShowSubtitle(Z)V

    .line 1752098
    :goto_6
    invoke-virtual {p0, v6}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setFacepileUrls(Ljava/util/List;)V

    .line 1752099
    iget-object v0, v4, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->socialContext:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$SocialContext;

    if-nez v0, :cond_e

    .line 1752100
    invoke-virtual {p0, v6}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setSocialContext(Ljava/lang/CharSequence;)V

    .line 1752101
    :cond_6
    :goto_7
    iput-boolean v1, p0, LX/B9g;->j:Z

    .line 1752102
    iput-boolean v2, p0, LX/B9g;->g:Z

    .line 1752103
    invoke-virtual {p0, v2}, LX/B9g;->setVisibility(I)V

    goto/16 :goto_0

    .line 1752104
    :cond_7
    iget-object v0, v4, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->primaryAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    iget-object v0, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;->title:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setPrimaryButtonText(Ljava/lang/CharSequence;)V

    .line 1752105
    invoke-virtual {p0, v1}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setShowPrimaryButton(Z)V

    move v0, v1

    .line 1752106
    goto/16 :goto_1

    .line 1752107
    :cond_8
    iget-object v0, v4, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->secondaryAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    iget-object v0, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;->title:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setSecondaryButtonText(Ljava/lang/CharSequence;)V

    .line 1752108
    invoke-virtual {p0, v1}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setShowSecondaryButton(Z)V

    move v0, v1

    .line 1752109
    goto :goto_2

    :cond_9
    move v3, v2

    .line 1752110
    goto :goto_3

    .line 1752111
    :cond_a
    iget-object v3, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;->uri:Ljava/lang/String;

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setImageUri(Landroid/net/Uri;)V

    .line 1752112
    iget-object v3, p0, LX/B9g;->b:Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;

    invoke-virtual {p1}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->c()Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    move-result-object v5

    invoke-virtual {v3, v0, v5}, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;)I

    move-result v3

    .line 1752113
    iget-object v5, p0, LX/B9g;->b:Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;

    invoke-virtual {p1}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->c()Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    move-result-object v7

    invoke-virtual {v5, v0, v7}, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;->b(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;)I

    move-result v5

    .line 1752114
    invoke-virtual {p0, v3, v5}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->a(II)V

    .line 1752115
    goto :goto_4

    .line 1752116
    :cond_b
    invoke-virtual {p0, v6}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setImageView(Landroid/view/View;)V

    .line 1752117
    invoke-virtual {p0, v6}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_4

    .line 1752118
    :cond_c
    iget-object v0, p0, LX/B9g;->e:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    iget-object v0, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->title:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setTitle(Ljava/lang/CharSequence;)V

    .line 1752119
    invoke-virtual {p0, v1}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setShowTitle(Z)V

    goto :goto_5

    .line 1752120
    :cond_d
    iget-object v0, p0, LX/B9g;->e:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    iget-object v0, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->content:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setSubtitle(Ljava/lang/CharSequence;)V

    .line 1752121
    invoke-virtual {p0, v1}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setShowSubtitle(Z)V

    goto :goto_6

    .line 1752122
    :cond_e
    iget-object v0, v4, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->socialContext:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$SocialContext;

    iget-object v0, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$SocialContext;->text:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setSocialContext(Ljava/lang/CharSequence;)V

    .line 1752123
    iget-object v0, v4, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->socialContext:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$SocialContext;

    iget-object v4, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$SocialContext;->friendIds:LX/0Px;

    .line 1752124
    if-eqz v4, :cond_6

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_6

    .line 1752125
    invoke-virtual {p0}, LX/B9g;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0b00b6

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 1752126
    invoke-static {v8}, LX/0R9;->b(I)Ljava/util/ArrayList;

    move-result-object v6

    move v3, v2

    .line 1752127
    :goto_8
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_f

    if-ge v3, v8, :cond_f

    .line 1752128
    iget-object v7, p0, LX/B9g;->c:LX/3Rb;

    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v7, v0, v5, v5}, LX/3Rb;->a(Ljava/lang/String;II)Landroid/net/Uri;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1752129
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_8

    .line 1752130
    :cond_f
    invoke-virtual {p0, v6}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setFacepileUrls(Ljava/util/List;)V

    goto/16 :goto_7
.end method

.method public final onLayout(ZIIII)V
    .locals 1

    .prologue
    .line 1752043
    invoke-super/range {p0 .. p5}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->onLayout(ZIIII)V

    .line 1752044
    iget-boolean v0, p0, LX/B9g;->j:Z

    if-eqz v0, :cond_5

    .line 1752045
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/B9g;->j:Z

    .line 1752046
    new-instance v0, LX/77n;

    invoke-direct {v0}, LX/77n;-><init>()V

    .line 1752047
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->a()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 1752048
    iget-object p1, p0, LX/B9g;->e:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    iget-object p1, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->title:Ljava/lang/String;

    .line 1752049
    iput-object p1, v0, LX/77n;->a:Ljava/lang/String;

    .line 1752050
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->b()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 1752051
    iget-object p1, p0, LX/B9g;->e:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    iget-object p1, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->content:Ljava/lang/String;

    .line 1752052
    iput-object p1, v0, LX/77n;->b:Ljava/lang/String;

    .line 1752053
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->e()Z

    move-result p1

    if-eqz p1, :cond_2

    .line 1752054
    iget-object p1, p0, LX/B9g;->e:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    iget-object p1, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->primaryAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    iget-object p1, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;->title:Ljava/lang/String;

    .line 1752055
    iput-object p1, v0, LX/77n;->c:Ljava/lang/String;

    .line 1752056
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->f()Z

    move-result p1

    if-eqz p1, :cond_3

    .line 1752057
    iget-object p1, p0, LX/B9g;->e:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    iget-object p1, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->secondaryAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    iget-object p1, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;->title:Ljava/lang/String;

    .line 1752058
    iput-object p1, v0, LX/77n;->d:Ljava/lang/String;

    .line 1752059
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->g()Z

    move-result p1

    if-eqz p1, :cond_4

    .line 1752060
    iget-object p1, p0, LX/B9g;->e:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    iget-object p1, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->socialContext:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$SocialContext;

    iget-object p1, p1, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$SocialContext;->text:Ljava/lang/String;

    .line 1752061
    iput-object p1, v0, LX/77n;->e:Ljava/lang/String;

    .line 1752062
    :cond_4
    iget-object p1, p0, LX/B9g;->f:LX/78A;

    invoke-virtual {p1}, LX/78A;->a()V

    .line 1752063
    iget-object p1, p0, LX/B9g;->f:LX/78A;

    invoke-virtual {p1, v0}, LX/78A;->a(LX/77n;)V

    .line 1752064
    :cond_5
    return-void
.end method

.method public final onMeasure(II)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1752039
    iget-boolean v0, p0, LX/B9g;->g:Z

    if-eqz v0, :cond_0

    .line 1752040
    invoke-virtual {p0, v1, v1}, LX/B9g;->setMeasuredDimension(II)V

    .line 1752041
    :goto_0
    return-void

    .line 1752042
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->onMeasure(II)V

    goto :goto_0
.end method

.method public setOnDismiss(Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 1752037
    iput-object p1, p0, LX/B9g;->h:Ljava/lang/Runnable;

    .line 1752038
    return-void
.end method
