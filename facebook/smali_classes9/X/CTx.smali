.class public final LX/CTx;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:I

.field public final b:I

.field public final c:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;


# direct methods
.method public constructor <init>(LX/CTx;I)V
    .locals 1

    .prologue
    .line 1896119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1896120
    iget v0, p1, LX/CTx;->a:I

    add-int/2addr v0, p2

    iput v0, p0, LX/CTx;->a:I

    .line 1896121
    iget v0, p1, LX/CTx;->b:I

    iput v0, p0, LX/CTx;->b:I

    .line 1896122
    iget-object v0, p1, LX/CTx;->c:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    iput-object v0, p0, LX/CTx;->c:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    .line 1896123
    return-void
.end method

.method public constructor <init>(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel$InlineStylesModel;)V
    .locals 1

    .prologue
    .line 1896124
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1896125
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1896126
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel$InlineStylesModel;->c()Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1896127
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel$InlineStylesModel;->b()I

    move-result v0

    iput v0, p0, LX/CTx;->a:I

    .line 1896128
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel$InlineStylesModel;->a()I

    move-result v0

    iput v0, p0, LX/CTx;->b:I

    .line 1896129
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel$InlineStylesModel;->c()Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    move-result-object v0

    iput-object v0, p0, LX/CTx;->c:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    .line 1896130
    return-void
.end method
