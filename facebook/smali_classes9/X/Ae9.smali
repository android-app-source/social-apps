.class public final LX/Ae9;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoInvitedFriendsModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/AeB;


# direct methods
.method public constructor <init>(LX/AeB;)V
    .locals 0

    .prologue
    .line 1696748
    iput-object p1, p0, LX/Ae9;->a:LX/AeB;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 1696749
    iget-object v0, p0, LX/Ae9;->a:LX/AeB;

    iget-object v0, v0, LX/AeB;->e:LX/03V;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, LX/AeB;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_graphFailure"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to fetch invited friends for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/Ae9;->a:LX/AeB;

    iget-object v3, v3, LX/AeB;->g:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1696750
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1696751
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1696752
    if-eqz p1, :cond_0

    .line 1696753
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1696754
    if-nez v0, :cond_1

    .line 1696755
    :cond_0
    return-void

    .line 1696756
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1696757
    check-cast v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoInvitedFriendsModel;

    .line 1696758
    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoInvitedFriendsModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1696759
    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoInvitedFriendsModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoInvitedFriendsModel$AlreadyInvitedLiveViewersModel;

    .line 1696760
    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoInvitedFriendsModel$AlreadyInvitedLiveViewersModel;->j()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 1696761
    iget-object v4, p0, LX/Ae9;->a:LX/AeB;

    iget-object v4, v4, LX/AeB;->b:Ljava/util/List;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoInvitedFriendsModel$AlreadyInvitedLiveViewersModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1696762
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method
