.class public LX/AfX;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:LX/0gX;

.field public final c:LX/0Sh;

.field public final d:LX/03V;

.field public final e:Landroid/os/Handler;

.field public final f:LX/3HT;

.field public g:LX/AeV;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:LX/0gM;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1699389
    const-class v0, LX/AfX;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/AfX;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/03V;LX/0gX;LX/0Sh;Landroid/os/Handler;LX/3HT;)V
    .locals 0
    .param p4    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1699390
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1699391
    iput-object p2, p0, LX/AfX;->b:LX/0gX;

    .line 1699392
    iput-object p3, p0, LX/AfX;->c:LX/0Sh;

    .line 1699393
    iput-object p1, p0, LX/AfX;->d:LX/03V;

    .line 1699394
    iput-object p4, p0, LX/AfX;->e:Landroid/os/Handler;

    .line 1699395
    iput-object p5, p0, LX/AfX;->f:LX/3HT;

    .line 1699396
    return-void
.end method


# virtual methods
.method public final a(LX/AeV;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1699397
    iget-boolean v0, p0, LX/AfX;->j:Z

    if-eqz v0, :cond_1

    .line 1699398
    :cond_0
    :goto_0
    return-void

    .line 1699399
    :cond_1
    iget-object v0, p0, LX/AfX;->c:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 1699400
    iput-object p2, p0, LX/AfX;->i:Ljava/lang/String;

    .line 1699401
    iput-object p1, p0, LX/AfX;->g:LX/AeV;

    .line 1699402
    iget-object v0, p0, LX/AfX;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/AfX;->g:LX/AeV;

    if-eqz v0, :cond_0

    .line 1699403
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/AfX;->j:Z

    .line 1699404
    new-instance v0, LX/AdF;

    invoke-direct {v0}, LX/AdF;-><init>()V

    move-object v0, v0

    .line 1699405
    new-instance v1, LX/4Gj;

    invoke-direct {v1}, LX/4Gj;-><init>()V

    iget-object p1, p0, LX/AfX;->i:Ljava/lang/String;

    .line 1699406
    const-string p2, "video_id"

    invoke-virtual {v1, p2, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1699407
    move-object v1, v1

    .line 1699408
    const-string p1, "input"

    invoke-virtual {v0, p1, v1}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1699409
    const-string v1, "scale"

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    .line 1699410
    :try_start_0
    iget-object v1, p0, LX/AfX;->b:LX/0gX;

    new-instance p1, LX/AfW;

    invoke-direct {p1, p0}, LX/AfW;-><init>(LX/AfX;)V

    invoke-virtual {v1, v0, p1}, LX/0gX;->a(LX/0gV;LX/0TF;)LX/0gM;

    move-result-object v0

    iput-object v0, p0, LX/AfX;->h:LX/0gM;
    :try_end_0
    .catch LX/31B; {:try_start_0 .. :try_end_0} :catch_0

    .line 1699411
    :goto_1
    goto :goto_0

    .line 1699412
    :catch_0
    goto :goto_1
.end method
