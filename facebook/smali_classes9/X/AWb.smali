.class public LX/AWb;
.super LX/AWT;
.source ""


# instance fields
.field public a:LX/3RX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/3RZ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final c:Lcom/facebook/widget/text/BetterTextView;

.field public f:LX/AWn;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1683157
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/AWb;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1683158
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1683149
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/AWb;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1683150
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1683151
    invoke-direct {p0, p1, p2, p3}, LX/AWT;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1683152
    const-class v0, LX/AWb;

    invoke-static {v0, p0}, LX/AWb;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1683153
    invoke-virtual {p0}, LX/AWb;->getContentLayout()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1683154
    const v0, 0x7f0d0f7b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/AWb;->c:Lcom/facebook/widget/text/BetterTextView;

    .line 1683155
    iget-object v0, p0, LX/AWb;->c:Lcom/facebook/widget/text/BetterTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 1683156
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/AWb;

    invoke-static {p0}, LX/8iw;->b(LX/0QB;)LX/8iw;

    move-result-object v1

    check-cast v1, LX/3RX;

    invoke-static {p0}, LX/3RZ;->b(LX/0QB;)LX/3RZ;

    move-result-object p0

    check-cast p0, LX/3RZ;

    iput-object v1, p1, LX/AWb;->a:LX/3RX;

    iput-object p0, p1, LX/AWb;->b:LX/3RZ;

    return-void
.end method


# virtual methods
.method public final g()V
    .locals 2

    .prologue
    .line 1683143
    iget-object v0, p0, LX/AWb;->f:LX/AWn;

    if-eqz v0, :cond_0

    .line 1683144
    iget-object v0, p0, LX/AWb;->f:LX/AWn;

    invoke-virtual {v0}, LX/AWn;->cancel()V

    .line 1683145
    :cond_0
    iget-object v0, p0, LX/AWb;->c:Lcom/facebook/widget/text/BetterTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 1683146
    iget-object v0, p0, LX/AWb;->c:Lcom/facebook/widget/text/BetterTextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1683147
    return-void
.end method

.method public getContentLayout()I
    .locals 1

    .prologue
    .line 1683148
    const v0, 0x7f0305b8

    return v0
.end method
