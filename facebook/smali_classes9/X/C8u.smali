.class public LX/C8u;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

.field public b:Landroid/view/View;

.field public c:LX/C90;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1853419
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/C8u;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1853420
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1853421
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/C8u;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1853422
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1853423
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1853424
    new-instance p2, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-direct {p2, p1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, LX/C8u;->a:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 1853425
    iget-object p2, p0, LX/C8u;->a:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {p0, p2}, LX/C8u;->addView(Landroid/view/View;)V

    .line 1853426
    return-void
.end method

.method public static e(LX/C8u;)V
    .locals 2

    .prologue
    .line 1853427
    iget-object v0, p0, LX/C8u;->b:Landroid/view/View;

    if-nez v0, :cond_0

    .line 1853428
    :goto_0
    return-void

    .line 1853429
    :cond_0
    iget-object v0, p0, LX/C8u;->a:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->setVisibility(I)V

    .line 1853430
    iget-object v0, p0, LX/C8u;->b:Landroid/view/View;

    invoke-virtual {p0, v0}, LX/C8u;->removeView(Landroid/view/View;)V

    .line 1853431
    const/4 v0, 0x0

    iput-object v0, p0, LX/C8u;->b:Landroid/view/View;

    goto :goto_0
.end method
