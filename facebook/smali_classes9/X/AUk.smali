.class public final enum LX/AUk;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/AUk;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/AUk;

.field public static final enum BLOB:LX/AUk;

.field public static final enum INTEGER:LX/AUk;

.field public static final enum REAL:LX/AUk;

.field public static final enum TEXT:LX/AUk;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1678335
    new-instance v0, LX/AUk;

    const-string v1, "TEXT"

    invoke-direct {v0, v1, v2}, LX/AUk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AUk;->TEXT:LX/AUk;

    .line 1678336
    new-instance v0, LX/AUk;

    const-string v1, "INTEGER"

    invoke-direct {v0, v1, v3}, LX/AUk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AUk;->INTEGER:LX/AUk;

    .line 1678337
    new-instance v0, LX/AUk;

    const-string v1, "REAL"

    invoke-direct {v0, v1, v4}, LX/AUk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AUk;->REAL:LX/AUk;

    .line 1678338
    new-instance v0, LX/AUk;

    const-string v1, "BLOB"

    invoke-direct {v0, v1, v5}, LX/AUk;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AUk;->BLOB:LX/AUk;

    .line 1678339
    const/4 v0, 0x4

    new-array v0, v0, [LX/AUk;

    sget-object v1, LX/AUk;->TEXT:LX/AUk;

    aput-object v1, v0, v2

    sget-object v1, LX/AUk;->INTEGER:LX/AUk;

    aput-object v1, v0, v3

    sget-object v1, LX/AUk;->REAL:LX/AUk;

    aput-object v1, v0, v4

    sget-object v1, LX/AUk;->BLOB:LX/AUk;

    aput-object v1, v0, v5

    sput-object v0, LX/AUk;->$VALUES:[LX/AUk;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1678334
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/AUk;
    .locals 1

    .prologue
    .line 1678341
    const-class v0, LX/AUk;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/AUk;

    return-object v0
.end method

.method public static values()[LX/AUk;
    .locals 1

    .prologue
    .line 1678340
    sget-object v0, LX/AUk;->$VALUES:[LX/AUk;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/AUk;

    return-object v0
.end method
