.class public final LX/B3D;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Landroid/net/Uri;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;


# direct methods
.method public constructor <init>(Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;)V
    .locals 0

    .prologue
    .line 1739823
    iput-object p1, p0, LX/B3D;->a:Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()Landroid/net/Uri;
    .locals 2

    .prologue
    .line 1739820
    iget-object v0, p0, LX/B3D;->a:Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;

    iget-object v0, v0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->d:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->b(Landroid/content/ContentResolver;)Ljava/lang/String;

    move-result-object v0

    .line 1739821
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final synthetic call()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1739822
    invoke-direct {p0}, LX/B3D;->a()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method
