.class public abstract LX/BVT;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/BW4;

.field public final b:LX/BW5;


# direct methods
.method public constructor <init>(LX/BW4;LX/BW5;)V
    .locals 0

    .prologue
    .line 1791037
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1791038
    iput-object p1, p0, LX/BVT;->a:LX/BW4;

    .line 1791039
    iput-object p2, p0, LX/BVT;->b:LX/BW5;

    .line 1791040
    return-void
.end method

.method private a(Ljava/util/Map;Landroid/view/ViewGroup;)Landroid/view/ViewGroup$LayoutParams;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Landroid/view/ViewGroup;",
            ")",
            "Landroid/view/ViewGroup$LayoutParams;"
        }
    .end annotation

    .prologue
    .line 1791173
    instance-of v0, p2, Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 1791174
    invoke-static {p0, p1}, LX/BVT;->c(LX/BVT;Ljava/util/Map;)Landroid/view/ViewGroup$MarginLayoutParams;

    move-result-object v0

    .line 1791175
    :goto_0
    return-object v0

    .line 1791176
    :cond_0
    instance-of v0, p2, Landroid/widget/FrameLayout;

    if-eqz v0, :cond_1

    .line 1791177
    invoke-static {p0, p1}, LX/BVT;->b(LX/BVT;Ljava/util/Map;)Landroid/view/ViewGroup$MarginLayoutParams;

    move-result-object v0

    goto :goto_0

    .line 1791178
    :cond_1
    instance-of v0, p2, Landroid/widget/AbsListView;

    if-eqz v0, :cond_2

    .line 1791179
    invoke-static {p0, p1}, LX/BVT;->a(LX/BVT;Ljava/util/Map;)Landroid/widget/AbsListView$LayoutParams;

    move-result-object v0

    goto :goto_0

    .line 1791180
    :cond_2
    instance-of v0, p2, Landroid/widget/LinearLayout;

    if-eqz v0, :cond_3

    .line 1791181
    invoke-static {p0, p1}, LX/BVT;->d(LX/BVT;Ljava/util/Map;)Landroid/widget/LinearLayout$LayoutParams;

    move-result-object v0

    goto :goto_0

    .line 1791182
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unsupported parent type for creating layout params, type = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static a(LX/BVT;Ljava/util/Map;)Landroid/widget/AbsListView$LayoutParams;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Landroid/widget/AbsListView$LayoutParams;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1791166
    new-instance v2, Landroid/widget/AbsListView$LayoutParams;

    invoke-direct {v2, v0, v0}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    .line 1791167
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1791168
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1791169
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1791170
    invoke-static {p0, v2, v1, v0}, LX/BVT;->a(LX/BVT;Landroid/view/ViewGroup$LayoutParams;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1791171
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "unhandled relative layout param = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1791172
    :cond_1
    return-object v2
.end method

.method private static a(LX/BVT;Landroid/view/ViewGroup$LayoutParams;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1791158
    invoke-static {p2}, LX/BVv;->from(Ljava/lang/String;)LX/BVv;

    move-result-object v2

    .line 1791159
    if-nez v2, :cond_0

    .line 1791160
    :goto_0
    return v0

    .line 1791161
    :cond_0
    sget-object v3, LX/BVu;->b:[I

    invoke-virtual {v2}, LX/BVv;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 1791162
    :pswitch_0
    iget-object v0, p0, LX/BVT;->a:LX/BW4;

    invoke-virtual {v0, p3}, LX/BW4;->j(Ljava/lang/String;)I

    move-result v0

    iput v0, p1, Landroid/view/ViewGroup$LayoutParams;->width:I

    move v0, v1

    .line 1791163
    goto :goto_0

    .line 1791164
    :pswitch_1
    iget-object v0, p0, LX/BVT;->a:LX/BW4;

    invoke-virtual {v0, p3}, LX/BW4;->j(Ljava/lang/String;)I

    move-result v0

    iput v0, p1, Landroid/view/ViewGroup$LayoutParams;->height:I

    move v0, v1

    .line 1791165
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static a(LX/BVT;Landroid/view/ViewGroup$MarginLayoutParams;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 1791146
    invoke-static {p2}, LX/BVx;->from(Ljava/lang/String;)LX/BVx;

    move-result-object v2

    .line 1791147
    if-nez v2, :cond_0

    .line 1791148
    :goto_0
    return v0

    .line 1791149
    :cond_0
    sget-object v3, LX/BVu;->a:[I

    invoke-virtual {v2}, LX/BVx;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 1791150
    :pswitch_0
    iget-object v0, p0, LX/BVT;->a:LX/BW4;

    invoke-virtual {v0, p3}, LX/BW4;->b(Ljava/lang/String;)I

    move-result v0

    iput v0, p1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move v0, v1

    .line 1791151
    goto :goto_0

    .line 1791152
    :pswitch_1
    iget-object v0, p0, LX/BVT;->a:LX/BW4;

    invoke-virtual {v0, p3}, LX/BW4;->b(Ljava/lang/String;)I

    move-result v0

    iput v0, p1, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    move v0, v1

    .line 1791153
    goto :goto_0

    .line 1791154
    :pswitch_2
    iget-object v0, p0, LX/BVT;->a:LX/BW4;

    invoke-virtual {v0, p3}, LX/BW4;->b(Ljava/lang/String;)I

    move-result v0

    iput v0, p1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    move v0, v1

    .line 1791155
    goto :goto_0

    .line 1791156
    :pswitch_3
    iget-object v0, p0, LX/BVT;->a:LX/BW4;

    invoke-virtual {v0, p3}, LX/BW4;->b(Ljava/lang/String;)I

    move-result v0

    iput v0, p1, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    move v0, v1

    .line 1791157
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private static b(LX/BVT;Ljava/util/Map;)Landroid/view/ViewGroup$MarginLayoutParams;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Landroid/view/ViewGroup$MarginLayoutParams;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1791139
    new-instance v2, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-direct {v2, v0, v0}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    .line 1791140
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1791141
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1791142
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1791143
    invoke-static {p0, v2, v1, v0}, LX/BVT;->a(LX/BVT;Landroid/view/ViewGroup$LayoutParams;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-static {p0, v2, v1, v0}, LX/BVT;->a(LX/BVT;Landroid/view/ViewGroup$MarginLayoutParams;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1791144
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "unhandled relative layout param = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1791145
    :cond_1
    return-object v2
.end method

.method private b(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Z
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 1791096
    invoke-static {p2}, LX/BVz;->from(Ljava/lang/String;)LX/BVz;

    move-result-object v2

    .line 1791097
    if-nez v2, :cond_0

    .line 1791098
    :goto_0
    return v0

    .line 1791099
    :cond_0
    sget-object v3, LX/BVu;->e:[I

    invoke-virtual {v2}, LX/BVz;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 1791100
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/View;->getPaddingLeft()I

    move-result v0

    iget-object v2, p0, LX/BVT;->a:LX/BW4;

    invoke-virtual {v2, p3}, LX/BW4;->b(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {p1}, Landroid/view/View;->getPaddingRight()I

    move-result v3

    invoke-virtual {p1}, Landroid/view/View;->getPaddingBottom()I

    move-result v4

    invoke-virtual {p1, v0, v2, v3, v4}, Landroid/view/View;->setPadding(IIII)V

    move v0, v1

    .line 1791101
    goto :goto_0

    .line 1791102
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/View;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getPaddingTop()I

    move-result v2

    iget-object v3, p0, LX/BVT;->a:LX/BW4;

    invoke-virtual {v3, p3}, LX/BW4;->b(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {p1}, Landroid/view/View;->getPaddingBottom()I

    move-result v4

    invoke-virtual {p1, v0, v2, v3, v4}, Landroid/view/View;->setPadding(IIII)V

    move v0, v1

    .line 1791103
    goto :goto_0

    .line 1791104
    :pswitch_2
    iget-object v0, p0, LX/BVT;->a:LX/BW4;

    invoke-virtual {v0, p3}, LX/BW4;->b(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getPaddingTop()I

    move-result v2

    invoke-virtual {p1}, Landroid/view/View;->getPaddingRight()I

    move-result v3

    invoke-virtual {p1}, Landroid/view/View;->getPaddingBottom()I

    move-result v4

    invoke-virtual {p1, v0, v2, v3, v4}, Landroid/view/View;->setPadding(IIII)V

    move v0, v1

    .line 1791105
    goto :goto_0

    .line 1791106
    :pswitch_3
    invoke-virtual {p1}, Landroid/view/View;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getPaddingTop()I

    move-result v2

    invoke-virtual {p1}, Landroid/view/View;->getPaddingRight()I

    move-result v3

    iget-object v4, p0, LX/BVT;->a:LX/BW4;

    invoke-virtual {v4, p3}, LX/BW4;->b(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {p1, v0, v2, v3, v4}, Landroid/view/View;->setPadding(IIII)V

    move v0, v1

    .line 1791107
    goto :goto_0

    .line 1791108
    :pswitch_4
    iget-object v0, p0, LX/BVT;->a:LX/BW4;

    invoke-virtual {v0, p3}, LX/BW4;->b(Ljava/lang/String;)I

    move-result v0

    .line 1791109
    invoke-virtual {p1, v0, v0, v0, v0}, Landroid/view/View;->setPadding(IIII)V

    move v0, v1

    .line 1791110
    goto :goto_0

    .line 1791111
    :pswitch_5
    iget-object v0, p0, LX/BVT;->b:LX/BW5;

    invoke-static {p3}, LX/BW4;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1791112
    iget-object v3, v0, LX/BW5;->a:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    .line 1791113
    if-eqz v3, :cond_5

    .line 1791114
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 1791115
    :goto_1
    move v0, v3

    .line 1791116
    invoke-virtual {p1, v0}, Landroid/view/View;->setId(I)V

    move v0, v1

    .line 1791117
    goto/16 :goto_0

    .line 1791118
    :pswitch_6
    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setFocusable(Z)V

    move v0, v1

    .line 1791119
    goto/16 :goto_0

    .line 1791120
    :pswitch_7
    iget-object v0, p0, LX/BVT;->a:LX/BW4;

    invoke-virtual {v0, p3}, LX/BW4;->o(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    move v0, v1

    .line 1791121
    goto/16 :goto_0

    .line 1791122
    :pswitch_8
    invoke-static {p3}, LX/BW4;->r(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1791123
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-static {p3}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    invoke-direct {v0, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 1791124
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-lt v2, v3, :cond_1

    .line 1791125
    invoke-virtual {p1, v0}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    :goto_2
    move v0, v1

    .line 1791126
    goto/16 :goto_0

    .line 1791127
    :cond_1
    invoke-virtual {p1, v0}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_2

    .line 1791128
    :cond_2
    invoke-static {p3}, LX/BW4;->t(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1791129
    invoke-static {p3}, LX/BW4;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1791130
    invoke-virtual {p4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const-string v3, "color"

    invoke-virtual {p4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v0, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_2

    .line 1791131
    :cond_3
    invoke-static {p3}, LX/BW4;->s(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1791132
    invoke-virtual {p4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {p3}, LX/BW4;->i(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2, v4, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_2

    .line 1791133
    :cond_4
    invoke-static {p3}, LX/BW4;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1791134
    invoke-virtual {p4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const-string v3, "drawable"

    invoke-virtual {p4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v0, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_2

    .line 1791135
    :pswitch_9
    iget-object v0, p0, LX/BVT;->a:LX/BW4;

    invoke-virtual {v0, p3}, LX/BW4;->b(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setMinimumWidth(I)V

    move v0, v1

    .line 1791136
    goto/16 :goto_0

    .line 1791137
    :cond_5
    iget v3, v0, LX/BW5;->b:I

    add-int/lit8 v4, v3, 0x1

    iput v4, v0, LX/BW5;->b:I

    .line 1791138
    iget-object v4, v0, LX/BW5;->a:Ljava/util/Map;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-interface {v4, v2, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method private static c(LX/BVT;Ljava/util/Map;)Landroid/view/ViewGroup$MarginLayoutParams;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Landroid/view/ViewGroup$MarginLayoutParams;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 1791072
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v6, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 1791073
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1791074
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1791075
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1791076
    invoke-static {p0, v2, v1, v0}, LX/BVT;->a(LX/BVT;Landroid/view/ViewGroup$LayoutParams;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-static {p0, v2, v1, v0}, LX/BVT;->a(LX/BVT;Landroid/view/ViewGroup$MarginLayoutParams;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1791077
    sget-object v4, LX/BVu;->c:[I

    invoke-static {v1}, LX/BVy;->from(Ljava/lang/String;)LX/BVy;

    move-result-object v5

    invoke-virtual {v5}, LX/BVy;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 1791078
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "unhandled relative layout param = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1791079
    :pswitch_0
    const/4 v1, 0x2

    iget-object v4, p0, LX/BVT;->b:LX/BW5;

    invoke-static {v0}, LX/BW4;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/BW5;->b(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v2, v1, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    goto :goto_0

    .line 1791080
    :pswitch_1
    const/4 v1, 0x3

    iget-object v4, p0, LX/BVT;->b:LX/BW5;

    invoke-static {v0}, LX/BW4;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/BW5;->b(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v2, v1, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    goto :goto_0

    .line 1791081
    :pswitch_2
    iget-object v1, p0, LX/BVT;->b:LX/BW5;

    invoke-static {v0}, LX/BW4;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/BW5;->b(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v2, v6, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    goto :goto_0

    .line 1791082
    :pswitch_3
    const/4 v1, 0x1

    iget-object v4, p0, LX/BVT;->b:LX/BW5;

    invoke-static {v0}, LX/BW4;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/BW5;->b(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v2, v1, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    goto/16 :goto_0

    .line 1791083
    :pswitch_4
    const/16 v0, 0xf

    invoke-virtual {v2, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    goto/16 :goto_0

    .line 1791084
    :pswitch_5
    const/16 v0, 0xe

    invoke-virtual {v2, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    goto/16 :goto_0

    .line 1791085
    :pswitch_6
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1791086
    const/16 v0, 0xb

    invoke-virtual {v2, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    goto/16 :goto_0

    .line 1791087
    :pswitch_7
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1791088
    const/16 v0, 0xa

    invoke-virtual {v2, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    goto/16 :goto_0

    .line 1791089
    :pswitch_8
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1791090
    const/16 v0, 0x9

    invoke-virtual {v2, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    goto/16 :goto_0

    .line 1791091
    :pswitch_9
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1791092
    const/16 v0, 0xc

    invoke-virtual {v2, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    goto/16 :goto_0

    .line 1791093
    :pswitch_a
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1791094
    const/16 v0, 0xd

    invoke-virtual {v2, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    goto/16 :goto_0

    .line 1791095
    :cond_1
    return-object v2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method private static d(LX/BVT;Ljava/util/Map;)Landroid/widget/LinearLayout$LayoutParams;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Landroid/widget/LinearLayout$LayoutParams;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1791062
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v2, v0, v0}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1791063
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1791064
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1791065
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1791066
    invoke-static {p0, v2, v1, v0}, LX/BVT;->a(LX/BVT;Landroid/view/ViewGroup$LayoutParams;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-static {p0, v2, v1, v0}, LX/BVT;->a(LX/BVT;Landroid/view/ViewGroup$MarginLayoutParams;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1791067
    sget-object v4, LX/BVu;->d:[I

    invoke-static {v1}, LX/BVw;->from(Ljava/lang/String;)LX/BVw;

    move-result-object v5

    invoke-virtual {v5}, LX/BVw;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 1791068
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "unhandled linear layout param = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1791069
    :pswitch_0
    iget-object v1, p0, LX/BVT;->a:LX/BW4;

    invoke-virtual {v1, v0}, LX/BW4;->p(Ljava/lang/String;)I

    move-result v0

    iput v0, v2, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    goto :goto_0

    .line 1791070
    :pswitch_1
    invoke-static {v0}, LX/BW4;->c(Ljava/lang/String;)F

    move-result v0

    iput v0, v2, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    goto :goto_0

    .line 1791071
    :cond_1
    return-object v2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public abstract a(Landroid/content/Context;)Landroid/view/View;
.end method

.method public a(Lorg/w3c/dom/Node;Landroid/view/ViewGroup;LX/BVh;Landroid/content/Context;)Landroid/view/View;
    .locals 7

    .prologue
    .line 1791041
    invoke-virtual {p0, p4}, LX/BVT;->a(Landroid/content/Context;)Landroid/view/View;

    move-result-object v1

    .line 1791042
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 1791043
    invoke-interface {p1}, Lorg/w3c/dom/Node;->getAttributes()Lorg/w3c/dom/NamedNodeMap;

    move-result-object v3

    .line 1791044
    const/4 v0, 0x0

    :goto_0
    invoke-interface {v3}, Lorg/w3c/dom/NamedNodeMap;->getLength()I

    move-result v4

    if-ge v0, v4, :cond_2

    .line 1791045
    invoke-interface {v3, v0}, Lorg/w3c/dom/NamedNodeMap;->item(I)Lorg/w3c/dom/Node;

    move-result-object v4

    .line 1791046
    invoke-interface {v4}, Lorg/w3c/dom/Node;->getNodeType()S

    move-result v5

    const/4 v6, 0x2

    if-ne v5, v6, :cond_0

    .line 1791047
    iget-object v5, p0, LX/BVT;->a:LX/BW4;

    invoke-interface {v4}, Lorg/w3c/dom/Node;->getNamespaceURI()Ljava/lang/String;

    move-result-object v6

    .line 1791048
    iget-object p1, v5, LX/BW4;->h:Ljava/util/Set;

    invoke-interface {p1, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p1

    move v5, p1

    .line 1791049
    if-eqz v5, :cond_0

    .line 1791050
    invoke-interface {v4}, Lorg/w3c/dom/Node;->getLocalName()Ljava/lang/String;

    move-result-object v5

    .line 1791051
    const-string v6, "layout_"

    invoke-virtual {v5, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1791052
    const/4 v6, 0x7

    invoke-virtual {v5, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    .line 1791053
    :goto_1
    move-object v6, v6

    .line 1791054
    invoke-interface {v4}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v4

    .line 1791055
    if-eqz v6, :cond_1

    .line 1791056
    invoke-interface {v2, v6, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1791057
    :cond_0
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1791058
    :cond_1
    invoke-direct {p0, v1, v5, v4, p4}, LX/BVT;->b(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 1791059
    invoke-virtual {p0, v1, v5, v4, p4}, LX/BVT;->a(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    goto :goto_2

    .line 1791060
    :cond_2
    invoke-direct {p0, v2, p2}, LX/BVT;->a(Ljava/util/Map;Landroid/view/ViewGroup;)Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1791061
    return-object v1

    :cond_3
    const/4 v6, 0x0

    goto :goto_1
.end method

.method public abstract a(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V
.end method
