.class public final LX/BmC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;)V
    .locals 0

    .prologue
    .line 1817989
    iput-object p1, p0, LX/BmC;->a:Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0x32f59668    # -1.45136E8f

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1817983
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 1817984
    const-string v2, "extra_start_time"

    iget-object v3, p0, LX/BmC;->a:Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;

    iget-object v3, v3, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->x:Ljava/util/Calendar;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 1817985
    const-string v2, "extra_end_time"

    iget-object v3, p0, LX/BmC;->a:Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;

    iget-object v3, v3, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->y:Ljava/util/Calendar;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 1817986
    iget-object v2, p0, LX/BmC;->a:Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;

    const/4 v3, -0x1

    invoke-virtual {v2, v3, v1}, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->setResult(ILandroid/content/Intent;)V

    .line 1817987
    iget-object v1, p0, LX/BmC;->a:Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;

    invoke-virtual {v1}, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->finish()V

    .line 1817988
    const v1, 0x64ecc9b6

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
