.class public final LX/Atr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8GH;


# instance fields
.field public final synthetic a:Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;


# direct methods
.method public constructor <init>(Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;)V
    .locals 0

    .prologue
    .line 1721889
    iput-object p1, p0, LX/Atr;->a:Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/photos/creativeediting/model/SwipeableParams;)V
    .locals 2
    .param p1    # Lcom/facebook/photos/creativeediting/model/SwipeableParams;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 1721890
    iget-object v0, p0, LX/Atr;->a:Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;

    invoke-static {v0, v1}, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->b$redex0(Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;Z)V

    .line 1721891
    iget-object v0, p0, LX/Atr;->a:Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;

    invoke-static {v0, v1}, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->a(Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;Z)V

    .line 1721892
    return-void
.end method

.method public final a(Lcom/facebook/photos/creativeediting/model/SwipeableParams;ZI)V
    .locals 7
    .param p1    # Lcom/facebook/photos/creativeediting/model/SwipeableParams;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 1721893
    iget-object v0, p0, LX/Atr;->a:Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;

    iget-object v0, v0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->v:LX/0Px;

    const-string v1, "Call refreshCirclePageIndicator before registering the SwipeEventListener."

    invoke-static {v0, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1721894
    if-ne p3, v4, :cond_0

    move v3, v4

    .line 1721895
    :goto_0
    iget-object v0, p0, LX/Atr;->a:Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;

    iget-object v1, v0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->q:LX/ArT;

    if-eqz v3, :cond_1

    sget-object v0, LX/ArJ;->SWIPE_UP:LX/ArJ;

    :goto_1
    invoke-virtual {v1, v0}, LX/ArT;->q(LX/ArJ;)V

    .line 1721896
    iget-object v0, p0, LX/Atr;->a:Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;

    iget-object v0, v0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 1721897
    iget-object v1, p0, LX/Atr;->a:Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;

    iget-object v1, v1, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0il;

    check-cast v1, LX/0im;

    invoke-interface {v1}, LX/0im;->c()LX/0jJ;

    move-result-object v1

    const-class v2, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;

    invoke-static {v2}, LX/0jK;->a(Ljava/lang/Class;)LX/0jK;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v1

    check-cast v1, LX/0jL;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/composer/system/model/ComposerModelImpl;

    check-cast v2, LX/0is;

    invoke-interface {v2}, LX/0is;->o()Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;)Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel$Builder;

    move-result-object v2

    .line 1721898
    iget-object v6, p1, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->b:Ljava/lang/String;

    move-object v6, v6

    .line 1721899
    invoke-virtual {v2, v6}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel$Builder;->setSelectedId(Ljava/lang/String;)Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel$Builder;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel$Builder;->setIsFromTray(Z)Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0jL;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0jL;

    check-cast v1, LX/0jL;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/composer/system/model/ComposerModelImpl;

    check-cast v2, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v2}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationState;)Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;

    move-result-object v2

    invoke-virtual {v2, v4}, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->setHasChangedInspiration(Z)Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0jL;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationState;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0jL;

    invoke-virtual {v1}, LX/0jL;->a()V

    .line 1721900
    iget-object v1, p0, LX/Atr;->a:Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;

    iget-object v2, v1, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->q:LX/ArT;

    if-eqz v3, :cond_2

    sget-object v1, LX/ArJ;->SWIPE_UP:LX/ArJ;

    :goto_2
    invoke-virtual {v2, v1}, LX/ArT;->p(LX/ArJ;)V

    .line 1721901
    iget-object v1, p0, LX/Atr;->a:Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;

    iget-object v1, v1, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->r:LX/ArS;

    invoke-virtual {v1}, LX/ArS;->b()V

    .line 1721902
    iget-object v1, p0, LX/Atr;->a:Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;

    iget-object v2, v1, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->r:LX/ArS;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    check-cast v1, LX/0j0;

    invoke-interface {v1}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    check-cast v1, LX/0is;

    invoke-interface {v1}, LX/0is;->o()Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;

    move-result-object v1

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v0

    invoke-virtual {v2, v3, v1, v0}, LX/ArS;->a(Ljava/lang/String;Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;Lcom/facebook/friendsharing/inspiration/model/InspirationState;)V

    .line 1721903
    iget-object v0, p0, LX/Atr;->a:Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;

    .line 1721904
    iput-boolean v5, v0, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;->M:Z

    .line 1721905
    return-void

    :cond_0
    move v3, v5

    .line 1721906
    goto/16 :goto_0

    .line 1721907
    :cond_1
    sget-object v0, LX/ArJ;->SWIPE_DOWN:LX/ArJ;

    goto/16 :goto_1

    .line 1721908
    :cond_2
    sget-object v1, LX/ArJ;->SWIPE_DOWN:LX/ArJ;

    goto :goto_2
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 1721909
    return-void
.end method

.method public final b(Lcom/facebook/photos/creativeediting/model/SwipeableParams;)V
    .locals 0
    .param p1    # Lcom/facebook/photos/creativeediting/model/SwipeableParams;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1721910
    return-void
.end method

.method public final c(Lcom/facebook/photos/creativeediting/model/SwipeableParams;)V
    .locals 0
    .param p1    # Lcom/facebook/photos/creativeediting/model/SwipeableParams;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1721911
    return-void
.end method
