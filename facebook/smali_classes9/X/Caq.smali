.class public LX/Caq;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/Caq;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1918633
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1918634
    return-void
.end method

.method public static a(LX/0QB;)LX/Caq;
    .locals 3

    .prologue
    .line 1918635
    sget-object v0, LX/Caq;->a:LX/Caq;

    if-nez v0, :cond_1

    .line 1918636
    const-class v1, LX/Caq;

    monitor-enter v1

    .line 1918637
    :try_start_0
    sget-object v0, LX/Caq;->a:LX/Caq;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1918638
    if-eqz v2, :cond_0

    .line 1918639
    :try_start_1
    new-instance v0, LX/Caq;

    invoke-direct {v0}, LX/Caq;-><init>()V

    .line 1918640
    move-object v0, v0

    .line 1918641
    sput-object v0, LX/Caq;->a:LX/Caq;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1918642
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1918643
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1918644
    :cond_1
    sget-object v0, LX/Caq;->a:LX/Caq;

    return-object v0

    .line 1918645
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1918646
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxInfoModel;)Landroid/graphics/RectF;
    .locals 10

    .prologue
    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    .line 1918647
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1918648
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxInfoModel;->b()LX/1f8;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1918649
    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxInfoModel;->c()LX/1f8;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1918650
    new-instance v0, Landroid/graphics/RectF;

    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxInfoModel;->b()LX/1f8;

    move-result-object v1

    invoke-interface {v1}, LX/1f8;->a()D

    move-result-wide v2

    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxInfoModel;->c()LX/1f8;

    move-result-object v1

    invoke-interface {v1}, LX/1f8;->a()D

    move-result-wide v4

    div-double/2addr v4, v8

    sub-double/2addr v2, v4

    double-to-float v1, v2

    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxInfoModel;->b()LX/1f8;

    move-result-object v2

    invoke-interface {v2}, LX/1f8;->b()D

    move-result-wide v2

    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxInfoModel;->c()LX/1f8;

    move-result-object v4

    invoke-interface {v4}, LX/1f8;->b()D

    move-result-wide v4

    div-double/2addr v4, v8

    sub-double/2addr v2, v4

    double-to-float v2, v2

    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxInfoModel;->b()LX/1f8;

    move-result-object v3

    invoke-interface {v3}, LX/1f8;->a()D

    move-result-wide v4

    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxInfoModel;->c()LX/1f8;

    move-result-object v3

    invoke-interface {v3}, LX/1f8;->a()D

    move-result-wide v6

    div-double/2addr v6, v8

    add-double/2addr v4, v6

    double-to-float v3, v4

    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxInfoModel;->b()LX/1f8;

    move-result-object v4

    invoke-interface {v4}, LX/1f8;->b()D

    move-result-wide v4

    invoke-virtual {p0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxInfoModel;->c()LX/1f8;

    move-result-object v6

    invoke-interface {v6}, LX/1f8;->b()D

    move-result-wide v6

    div-double/2addr v6, v8

    add-double/2addr v4, v6

    double-to-float v4, v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    return-object v0
.end method
