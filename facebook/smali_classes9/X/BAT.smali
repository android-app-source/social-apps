.class public final enum LX/BAT;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/BAT;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/BAT;

.field public static final enum PHOTO_NOTIFICATION:LX/BAT;

.field public static final enum UNKNOWN:LX/BAT;


# instance fields
.field private final isUniqueNotification:Z

.field private final type:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 1753081
    new-instance v0, LX/BAT;

    const-string v1, "PHOTO_NOTIFICATION"

    const-string v2, "photonotification"

    invoke-direct {v0, v1, v4, v2, v3}, LX/BAT;-><init>(Ljava/lang/String;ILjava/lang/String;Z)V

    sput-object v0, LX/BAT;->PHOTO_NOTIFICATION:LX/BAT;

    .line 1753082
    new-instance v0, LX/BAT;

    const-string v1, "UNKNOWN"

    const-string v2, "unknown"

    invoke-direct {v0, v1, v3, v2, v3}, LX/BAT;-><init>(Ljava/lang/String;ILjava/lang/String;Z)V

    sput-object v0, LX/BAT;->UNKNOWN:LX/BAT;

    .line 1753083
    const/4 v0, 0x2

    new-array v0, v0, [LX/BAT;

    sget-object v1, LX/BAT;->PHOTO_NOTIFICATION:LX/BAT;

    aput-object v1, v0, v4

    sget-object v1, LX/BAT;->UNKNOWN:LX/BAT;

    aput-object v1, v0, v3

    sput-object v0, LX/BAT;->$VALUES:[LX/BAT;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Z)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 1753071
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1753072
    sget-boolean v0, LX/007;->k:Z

    move v0, v0

    .line 1753073
    if-eqz v0, :cond_2

    .line 1753074
    invoke-virtual {p3}, Ljava/lang/String;->toCharArray()[C

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-char v3, v1, v0

    .line 1753075
    const/16 v4, 0x61

    if-lt v3, v4, :cond_0

    const/16 v4, 0x7a

    if-le v3, v4, :cond_1

    .line 1753076
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Local Notification Type must be lowecase letters from \'a\' to \'z\' only!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1753077
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1753078
    :cond_2
    iput-object p3, p0, LX/BAT;->type:Ljava/lang/String;

    .line 1753079
    iput-boolean p4, p0, LX/BAT;->isUniqueNotification:Z

    .line 1753080
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/BAT;
    .locals 1

    .prologue
    .line 1753070
    const-class v0, LX/BAT;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/BAT;

    return-object v0
.end method

.method public static values()[LX/BAT;
    .locals 1

    .prologue
    .line 1753069
    sget-object v0, LX/BAT;->$VALUES:[LX/BAT;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/BAT;

    return-object v0
.end method


# virtual methods
.method public final isUniqueNotification()Z
    .locals 1

    .prologue
    .line 1753067
    iget-boolean v0, p0, LX/BAT;->isUniqueNotification:Z

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1753068
    iget-object v0, p0, LX/BAT;->type:Ljava/lang/String;

    return-object v0
.end method
