.class public LX/Bzd;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements LX/35q;


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# instance fields
.field private final a:Lcom/facebook/attachments/angora/CoverPhotoWithPlayIconView;

.field public final b:Landroid/widget/TextView;

.field public final c:Landroid/widget/TextView;

.field public final d:Landroid/widget/TextView;

.field public final e:Landroid/widget/ImageView;

.field public final f:Landroid/view/View;

.field public final g:Landroid/widget/LinearLayout;

.field public final h:Landroid/animation/ValueAnimator;

.field public final i:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1838992
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/Bzd;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1838993
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1838990
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/Bzd;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1838991
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 1838994
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1838995
    new-instance v0, Lcom/facebook/feedplugins/attachments/linkshare/InstantArticleEdgeToEdgeShareAttachmentView$1;

    invoke-direct {v0, p0}, Lcom/facebook/feedplugins/attachments/linkshare/InstantArticleEdgeToEdgeShareAttachmentView$1;-><init>(LX/Bzd;)V

    iput-object v0, p0, LX/Bzd;->i:Ljava/lang/Runnable;

    .line 1838996
    const v0, 0x7f030955

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1838997
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p0, v0}, LX/Bzd;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1838998
    const v0, 0x7f0d17f6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/attachments/angora/CoverPhotoWithPlayIconView;

    iput-object v0, p0, LX/Bzd;->a:Lcom/facebook/attachments/angora/CoverPhotoWithPlayIconView;

    .line 1838999
    const v0, 0x7f0d17fa

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/Bzd;->b:Landroid/widget/TextView;

    .line 1839000
    const v0, 0x7f0d17fc

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/Bzd;->c:Landroid/widget/TextView;

    .line 1839001
    const v0, 0x7f0d17f7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/Bzd;->d:Landroid/widget/TextView;

    .line 1839002
    const v0, 0x7f0d17f8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/Bzd;->e:Landroid/widget/ImageView;

    .line 1839003
    const v0, 0x7f0d17fd

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/Bzd;->f:Landroid/view/View;

    .line 1839004
    const v0, 0x7f0d17f9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/Bzd;->g:Landroid/widget/LinearLayout;

    .line 1839005
    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, LX/Bzd;->h:Landroid/animation/ValueAnimator;

    .line 1839006
    return-void

    nop

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method


# virtual methods
.method public final d()V
    .locals 2

    .prologue
    .line 1838988
    iget-object v0, p0, LX/Bzd;->d:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1838989
    return-void
.end method

.method public setLargeImageAspectRatio(F)V
    .locals 1

    .prologue
    .line 1838986
    iget-object v0, p0, LX/Bzd;->a:Lcom/facebook/attachments/angora/CoverPhotoWithPlayIconView;

    invoke-virtual {v0, p1}, Lcom/facebook/drawee/view/DraweeView;->setAspectRatio(F)V

    .line 1838987
    return-void
.end method

.method public setLargeImageController(LX/1aZ;)V
    .locals 1
    .param p1    # LX/1aZ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1838984
    iget-object v0, p0, LX/Bzd;->a:Lcom/facebook/attachments/angora/CoverPhotoWithPlayIconView;

    invoke-virtual {v0, p1}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 1838985
    return-void
.end method
