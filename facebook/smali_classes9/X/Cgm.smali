.class public final LX/Cgm;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Cgh;


# instance fields
.field public final synthetic a:LX/Cgz;


# direct methods
.method public constructor <init>(LX/Cgz;)V
    .locals 0

    .prologue
    .line 1927447
    iput-object p1, p0, LX/Cgm;->a:LX/Cgz;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 4

    .prologue
    .line 1927448
    iget-object v0, p0, LX/Cgm;->a:LX/Cgz;

    iget-object v0, v0, LX/Cgz;->h:LX/79D;

    iget-object v1, p0, LX/Cgm;->a:LX/Cgz;

    .line 1927449
    invoke-virtual {v1}, LX/AQ9;->R()LX/B5j;

    move-result-object v2

    move-object v1, v2

    .line 1927450
    invoke-virtual {v1}, LX/B5j;->d()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v1

    invoke-interface {v1}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v1

    iget-wide v2, v1, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/Cgm;->a:LX/Cgz;

    .line 1927451
    invoke-virtual {v2}, LX/AQ9;->R()LX/B5j;

    move-result-object v3

    move-object v2, v3

    .line 1927452
    invoke-virtual {v2}, LX/B5j;->d()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v2

    invoke-interface {v2}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v2

    .line 1927453
    const-string v3, "composer_rating_selector_tapped"

    invoke-static {v3, v1, v2}, LX/79D;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    .line 1927454
    const-string p0, "rating"

    invoke-virtual {v3, p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1927455
    iget-object p0, v0, LX/79D;->a:LX/0Zb;

    invoke-interface {p0, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1927456
    return-void
.end method

.method public final b(I)V
    .locals 5

    .prologue
    .line 1927457
    iget-object v0, p0, LX/Cgm;->a:LX/Cgz;

    iget-object v0, v0, LX/Cgz;->h:LX/79D;

    iget-object v1, p0, LX/Cgm;->a:LX/Cgz;

    .line 1927458
    invoke-virtual {v1}, LX/AQ9;->R()LX/B5j;

    move-result-object v2

    move-object v1, v2

    .line 1927459
    invoke-virtual {v1}, LX/B5j;->d()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v1

    invoke-interface {v1}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v1

    iget-wide v2, v1, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/Cgm;->a:LX/Cgz;

    .line 1927460
    invoke-virtual {v2}, LX/AQ9;->R()LX/B5j;

    move-result-object v3

    move-object v2, v3

    .line 1927461
    invoke-virtual {v2}, LX/B5j;->d()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v2

    invoke-interface {v2}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v2

    .line 1927462
    const-string v3, "composer_rating_selected"

    invoke-static {v3, v1, v2}, LX/79D;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    .line 1927463
    const-string v4, "rating"

    invoke-virtual {v3, v4, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1927464
    iget-object v4, v0, LX/79D;->a:LX/0Zb;

    invoke-interface {v4, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1927465
    iget-object v0, p0, LX/Cgm;->a:LX/Cgz;

    invoke-static {v0, p1}, LX/Cgz;->a$redex0(LX/Cgz;I)V

    .line 1927466
    return-void
.end method

.method public final c(I)V
    .locals 5

    .prologue
    .line 1927467
    iget-object v0, p0, LX/Cgm;->a:LX/Cgz;

    iget-object v0, v0, LX/Cgz;->h:LX/79D;

    iget-object v1, p0, LX/Cgm;->a:LX/Cgz;

    .line 1927468
    invoke-virtual {v1}, LX/AQ9;->R()LX/B5j;

    move-result-object v2

    move-object v1, v2

    .line 1927469
    invoke-virtual {v1}, LX/B5j;->d()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v1

    invoke-interface {v1}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v1

    iget-wide v2, v1, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/Cgm;->a:LX/Cgz;

    .line 1927470
    invoke-virtual {v2}, LX/AQ9;->R()LX/B5j;

    move-result-object v3

    move-object v2, v3

    .line 1927471
    invoke-virtual {v2}, LX/B5j;->d()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v2

    invoke-interface {v2}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v2

    .line 1927472
    const-string v3, "composer_popover_rating_selected"

    invoke-static {v3, v1, v2}, LX/79D;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    .line 1927473
    const-string v4, "rating"

    invoke-virtual {v3, v4, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1927474
    iget-object v4, v0, LX/79D;->a:LX/0Zb;

    invoke-interface {v4, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1927475
    iget-object v0, p0, LX/Cgm;->a:LX/Cgz;

    invoke-static {v0, p1}, LX/Cgz;->a$redex0(LX/Cgz;I)V

    .line 1927476
    return-void
.end method

.method public final d(I)V
    .locals 5

    .prologue
    .line 1927477
    iget-object v0, p0, LX/Cgm;->a:LX/Cgz;

    iget-object v0, v0, LX/Cgz;->h:LX/79D;

    iget-object v1, p0, LX/Cgm;->a:LX/Cgz;

    .line 1927478
    invoke-virtual {v1}, LX/AQ9;->R()LX/B5j;

    move-result-object v2

    move-object v1, v2

    .line 1927479
    invoke-virtual {v1}, LX/B5j;->d()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v1

    invoke-interface {v1}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v1

    iget-wide v2, v1, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/Cgm;->a:LX/Cgz;

    .line 1927480
    invoke-virtual {v2}, LX/AQ9;->R()LX/B5j;

    move-result-object v3

    move-object v2, v3

    .line 1927481
    invoke-virtual {v2}, LX/B5j;->d()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v2

    invoke-interface {v2}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v2

    .line 1927482
    const-string v3, "composer_rating_label_selected"

    invoke-static {v3, v1, v2}, LX/79D;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    .line 1927483
    const-string v4, "rating"

    invoke-virtual {v3, v4, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1927484
    iget-object v4, v0, LX/79D;->a:LX/0Zb;

    invoke-interface {v4, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1927485
    iget-object v0, p0, LX/Cgm;->a:LX/Cgz;

    invoke-static {v0, p1}, LX/Cgz;->a$redex0(LX/Cgz;I)V

    .line 1927486
    return-void
.end method
