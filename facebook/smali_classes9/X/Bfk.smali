.class public final LX/Bfk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionPlaceInfoFieldsModel;

.field public final synthetic b:LX/Bfl;


# direct methods
.method public constructor <init>(LX/Bfl;Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionPlaceInfoFieldsModel;)V
    .locals 0

    .prologue
    .line 1806584
    iput-object p1, p0, LX/Bfk;->b:LX/Bfl;

    iput-object p2, p0, LX/Bfk;->a:Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionPlaceInfoFieldsModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v0, 0x1

    const v1, -0x4f01c5

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1806585
    iget-object v1, p0, LX/Bfk;->b:LX/Bfl;

    iget-object v1, v1, LX/Bfl;->j:LX/17Y;

    iget-object v2, p0, LX/Bfk;->b:LX/Bfl;

    invoke-virtual {v2}, LX/Bfl;->getContext()Landroid/content/Context;

    move-result-object v2

    sget-object v3, LX/0ax;->aL:Ljava/lang/String;

    iget-object v4, p0, LX/Bfk;->a:Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionPlaceInfoFieldsModel;

    invoke-virtual {v4}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionPlaceInfoFieldsModel;->a()Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionPlaceInfoFieldsModel$PageModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionPlaceInfoFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object v4

    const-string v5, "mge_suggest_edits_button"

    invoke-static {v3, v4, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 1806586
    iget-object v2, p0, LX/Bfk;->a:Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionPlaceInfoFieldsModel;

    invoke-virtual {v2}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionPlaceInfoFieldsModel;->a()Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionPlaceInfoFieldsModel$PageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionPlaceInfoFieldsModel$PageModel;->c()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1806587
    const-string v2, "profile_name"

    iget-object v3, p0, LX/Bfk;->a:Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionPlaceInfoFieldsModel;

    invoke-virtual {v3}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionPlaceInfoFieldsModel;->a()Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionPlaceInfoFieldsModel$PageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionPlaceInfoFieldsModel$PageModel;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1806588
    :cond_0
    iget-object v2, p0, LX/Bfk;->b:LX/Bfl;

    iget-object v2, v2, LX/Bfl;->k:Lcom/facebook/content/SecureContextHelper;

    iget-object v3, p0, LX/Bfk;->b:LX/Bfl;

    invoke-virtual {v3}, LX/Bfl;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v2, v1, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1806589
    const v1, -0x7bcd6db7

    invoke-static {v6, v6, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
