.class public final LX/BrD;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/graphql/model/GraphQLComment;

.field public b:Lcom/facebook/graphql/model/GraphQLProfile;

.field public c:Landroid/net/Uri;

.field public d:Ljava/lang/CharSequence;

.field public e:Ljava/lang/CharSequence;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Landroid/view/View$OnClickListener;

.field public g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/text/Spannable;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/graphql/model/GraphQLComment;Lcom/facebook/graphql/model/GraphQLProfile;Landroid/net/Uri;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;Ljava/util/List;)V
    .locals 0
    .param p5    # Ljava/lang/CharSequence;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLComment;",
            "Lcom/facebook/graphql/model/GraphQLProfile;",
            "Landroid/net/Uri;",
            "Ljava/lang/CharSequence;",
            "Ljava/lang/CharSequence;",
            "Landroid/view/View$OnClickListener;",
            "Ljava/util/List",
            "<",
            "Landroid/text/Spannable;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1826083
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1826084
    iput-object p1, p0, LX/BrD;->a:Lcom/facebook/graphql/model/GraphQLComment;

    .line 1826085
    iput-object p2, p0, LX/BrD;->b:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 1826086
    iput-object p3, p0, LX/BrD;->c:Landroid/net/Uri;

    .line 1826087
    iput-object p4, p0, LX/BrD;->d:Ljava/lang/CharSequence;

    .line 1826088
    iput-object p5, p0, LX/BrD;->e:Ljava/lang/CharSequence;

    .line 1826089
    iput-object p6, p0, LX/BrD;->f:Landroid/view/View$OnClickListener;

    .line 1826090
    iput-object p7, p0, LX/BrD;->g:Ljava/util/List;

    .line 1826091
    return-void
.end method
