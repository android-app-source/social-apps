.class public LX/BTN;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/util/concurrent/Executor;

.field public final b:LX/BTJ;

.field public final c:LX/BTX;

.field public final d:LX/BTZ;

.field public final e:LX/BT6;

.field public f:Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;

.field private g:Ljava/util/Timer;

.field public h:Z


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;LX/BTX;LX/BTJ;LX/BTZ;LX/BT6;)V
    .locals 0
    .param p1    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p2    # LX/BTX;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/BTJ;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/BTZ;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/BT6;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1787297
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1787298
    iput-object p1, p0, LX/BTN;->a:Ljava/util/concurrent/Executor;

    .line 1787299
    iput-object p3, p0, LX/BTN;->b:LX/BTJ;

    .line 1787300
    iput-object p2, p0, LX/BTN;->c:LX/BTX;

    .line 1787301
    iput-object p4, p0, LX/BTN;->d:LX/BTZ;

    .line 1787302
    iput-object p5, p0, LX/BTN;->e:LX/BT6;

    .line 1787303
    return-void
.end method


# virtual methods
.method public final a(LX/BTM;LX/BTI;)V
    .locals 6

    .prologue
    .line 1787288
    invoke-virtual {p0}, LX/BTN;->b()V

    .line 1787289
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, LX/BTN;->g:Ljava/util/Timer;

    .line 1787290
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/BTN;->h:Z

    .line 1787291
    new-instance v1, Lcom/facebook/video/creativeediting/trimmer/StripSlidingController$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/facebook/video/creativeediting/trimmer/StripSlidingController$1;-><init>(LX/BTN;LX/BTM;LX/BTI;)V

    .line 1787292
    iget-object v0, p0, LX/BTN;->g:Ljava/util/Timer;

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x14

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->scheduleAtFixedRate(Ljava/util/TimerTask;JJ)V

    .line 1787293
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1787294
    iget-object v0, p0, LX/BTN;->g:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 1787295
    iget-object v0, p0, LX/BTN;->g:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 1787296
    :cond_0
    return-void
.end method
