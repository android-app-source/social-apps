.class public final LX/CFx;
.super LX/7hR;
.source ""


# instance fields
.field public final synthetic a:LX/CG2;


# direct methods
.method public constructor <init>(LX/CG2;)V
    .locals 0

    .prologue
    .line 1864718
    iput-object p1, p0, LX/CFx;->a:LX/CG2;

    invoke-direct {p0}, LX/7hR;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    .line 1864719
    iget-object v0, p0, LX/CFx;->a:LX/CG2;

    iget-object v0, v0, LX/CG2;->h:LX/CG1;

    sget-object v1, LX/CG1;->SPRINGING:LX/CG1;

    if-ne v0, v1, :cond_0

    .line 1864720
    iget-object v1, p0, LX/CFx;->a:LX/CG2;

    iget-object v0, p0, LX/CFx;->a:LX/CG2;

    invoke-static {v0}, LX/CG2;->d(LX/CG2;)D

    move-result-wide v2

    const-wide/high16 v4, 0x3fe0000000000000L    # 0.5

    cmpl-double v0, v2, v4

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, v0}, LX/CG2;->a$redex0(LX/CG2;Z)V

    .line 1864721
    :cond_0
    return-void

    .line 1864722
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/8YK;)V
    .locals 4

    .prologue
    .line 1864723
    iget-object v0, p0, LX/CFx;->a:LX/CG2;

    iget-object v0, v0, LX/CG2;->h:LX/CG1;

    sget-object v1, LX/CG1;->IDLE:LX/CG1;

    if-eq v0, v1, :cond_0

    .line 1864724
    iget-object v0, p0, LX/CFx;->a:LX/CG2;

    invoke-static {v0}, LX/CG2;->d(LX/CG2;)D

    .line 1864725
    iget-object v0, p0, LX/CFx;->a:LX/CG2;

    iget-object v0, v0, LX/CG2;->f:LX/CGF;

    iget-object v1, p0, LX/CFx;->a:LX/CG2;

    invoke-static {v1}, LX/CG2;->d(LX/CG2;)D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, LX/CGF;->a(D)V

    .line 1864726
    :cond_0
    return-void
.end method
