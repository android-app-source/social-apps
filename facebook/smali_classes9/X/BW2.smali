.class public final enum LX/BW2;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/BW2;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/BW2;

.field public static final enum INFLATED_ID:LX/BW2;

.field public static final enum LAYOUT:LX/BW2;


# instance fields
.field private final mValue:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1791616
    new-instance v0, LX/BW2;

    const-string v1, "INFLATED_ID"

    const-string v2, "inflatedId"

    invoke-direct {v0, v1, v3, v2}, LX/BW2;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BW2;->INFLATED_ID:LX/BW2;

    .line 1791617
    new-instance v0, LX/BW2;

    const-string v1, "LAYOUT"

    const-string v2, "layout"

    invoke-direct {v0, v1, v4, v2}, LX/BW2;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BW2;->LAYOUT:LX/BW2;

    .line 1791618
    const/4 v0, 0x2

    new-array v0, v0, [LX/BW2;

    sget-object v1, LX/BW2;->INFLATED_ID:LX/BW2;

    aput-object v1, v0, v3

    sget-object v1, LX/BW2;->LAYOUT:LX/BW2;

    aput-object v1, v0, v4

    sput-object v0, LX/BW2;->$VALUES:[LX/BW2;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1791613
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1791614
    iput-object p3, p0, LX/BW2;->mValue:Ljava/lang/String;

    .line 1791615
    return-void
.end method

.method public static from(Ljava/lang/String;)LX/BW2;
    .locals 4

    .prologue
    .line 1791606
    invoke-static {}, LX/BW2;->values()[LX/BW2;

    move-result-object v1

    .line 1791607
    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_1

    .line 1791608
    aget-object v2, v1, v0

    .line 1791609
    iget-object v3, v2, LX/BW2;->mValue:Ljava/lang/String;

    invoke-virtual {v3, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1791610
    return-object v2

    .line 1791611
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1791612
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unknown view stub attribute = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/BW2;
    .locals 1

    .prologue
    .line 1791605
    const-class v0, LX/BW2;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/BW2;

    return-object v0
.end method

.method public static values()[LX/BW2;
    .locals 1

    .prologue
    .line 1791604
    sget-object v0, LX/BW2;->$VALUES:[LX/BW2;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/BW2;

    return-object v0
.end method
