.class public LX/B9U;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements LX/B9N;


# instance fields
.field public a:LX/B9T;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/widget/images/UrlImage;

.field public c:Landroid/widget/TextView;

.field public d:Landroid/widget/TextView;

.field public e:Landroid/widget/LinearLayout;

.field public f:Landroid/widget/Button;

.field public g:Landroid/widget/Button;

.field public h:Landroid/widget/Button;

.field public i:Landroid/widget/Button;

.field public j:Landroid/widget/ImageButton;

.field private final k:Landroid/view/LayoutInflater;

.field public l:Lcom/facebook/graphql/model/GraphQLMegaphone;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1751797
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/B9U;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1751798
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1751795
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/B9U;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1751796
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 1751777
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1751778
    const-class v0, LX/B9U;

    invoke-static {v0, p0}, LX/B9U;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1751779
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, LX/B9U;->k:Landroid/view/LayoutInflater;

    .line 1751780
    invoke-virtual {p0}, LX/B9U;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f030ab6

    invoke-static {v0, v1, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1751781
    const v0, 0x7f0d1b5e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/images/UrlImage;

    iput-object v0, p0, LX/B9U;->b:Lcom/facebook/widget/images/UrlImage;

    .line 1751782
    const v0, 0x7f0d1b5f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/B9U;->c:Landroid/widget/TextView;

    .line 1751783
    const v0, 0x7f0d1b60

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/B9U;->d:Landroid/widget/TextView;

    .line 1751784
    const v0, 0x7f0d1b65

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, LX/B9U;->g:Landroid/widget/Button;

    .line 1751785
    const v0, 0x7f0d1b62

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, LX/B9U;->f:Landroid/widget/Button;

    .line 1751786
    const v0, 0x7f0d1b64

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, LX/B9U;->i:Landroid/widget/Button;

    .line 1751787
    const v0, 0x7f0d1b66

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, LX/B9U;->j:Landroid/widget/ImageButton;

    .line 1751788
    const v0, 0x7f0d1b61

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/B9U;->e:Landroid/widget/LinearLayout;

    .line 1751789
    iget-object v0, p0, LX/B9U;->j:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1751790
    iget-object v0, p0, LX/B9U;->i:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1751791
    iget-object v0, p0, LX/B9U;->g:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1751792
    iget-object v0, p0, LX/B9U;->f:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1751793
    iget-object v0, p0, LX/B9U;->b:Lcom/facebook/widget/images/UrlImage;

    invoke-virtual {v0, p0}, Lcom/facebook/widget/images/UrlImage;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1751794
    return-void
.end method

.method public static a(LX/B9U;ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 1751774
    iget-object v0, p0, LX/B9U;->k:Landroid/view/LayoutInflater;

    if-eqz v0, :cond_0

    .line 1751775
    iget-object v0, p0, LX/B9U;->k:Landroid/view/LayoutInflater;

    invoke-virtual {v0, p1, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 1751776
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(LX/B9U;LX/0Px;)Landroid/view/View;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLEntity;",
            ">;)",
            "Landroid/view/View;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1751753
    if-nez p1, :cond_1

    .line 1751754
    :cond_0
    :pswitch_0
    return-object v2

    .line 1751755
    :cond_1
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    .line 1751756
    packed-switch v0, :pswitch_data_0

    .line 1751757
    const v0, 0x7f030ab4

    invoke-static {p0, v0, v2}, LX/B9U;->a(LX/B9U;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 1751758
    const/4 v1, 0x6

    move-object v2, v0

    .line 1751759
    :goto_0
    const/4 v0, 0x0

    move v3, v0

    :goto_1
    if-ge v3, v1, :cond_0

    .line 1751760
    invoke-virtual {p1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEntity;

    const/4 v5, 0x0

    .line 1751761
    if-eqz v0, :cond_2

    if-nez v2, :cond_3

    .line 1751762
    :cond_2
    :goto_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 1751763
    :pswitch_1
    const v0, 0x7f030ab2

    invoke-static {p0, v0, v2}, LX/B9U;->a(LX/B9U;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 1751764
    const/4 v1, 0x2

    move-object v2, v0

    .line 1751765
    goto :goto_0

    .line 1751766
    :pswitch_2
    const v0, 0x7f030ab3

    invoke-static {p0, v0, v2}, LX/B9U;->a(LX/B9U;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 1751767
    const/4 v1, 0x3

    move-object v2, v0

    .line 1751768
    goto :goto_0

    .line 1751769
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEntity;->s()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    if-eqz v4, :cond_5

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEntity;->s()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v4

    move-object p0, v4

    .line 1751770
    :goto_3
    if-eqz p0, :cond_2

    .line 1751771
    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/facebook/widget/images/UrlImage;

    invoke-static {v4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/widget/images/UrlImage;

    .line 1751772
    if-eqz p0, :cond_4

    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    :cond_4
    invoke-virtual {v4, v5}, Lcom/facebook/widget/images/UrlImage;->setImageParams(Landroid/net/Uri;)V

    goto :goto_2

    :cond_5
    move-object p0, v5

    .line 1751773
    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/B9U;

    invoke-static {p0}, LX/B9T;->b(LX/0QB;)LX/B9T;

    move-result-object p0

    check-cast p0, LX/B9T;

    iput-object p0, p1, LX/B9U;->a:LX/B9T;

    return-void
.end method


# virtual methods
.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x4457057

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1751750
    invoke-super {p0}, Lcom/facebook/widget/CustomRelativeLayout;->onAttachedToWindow()V

    .line 1751751
    iget-object v1, p0, LX/B9U;->a:LX/B9T;

    iget-object v2, p0, LX/B9U;->l:Lcom/facebook/graphql/model/GraphQLMegaphone;

    invoke-virtual {v1, v2}, LX/B9T;->d(Lcom/facebook/graphql/model/GraphQLMegaphone;)V

    .line 1751752
    const/16 v1, 0x2d

    const v2, 0x65974234

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x1d685c12

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1751743
    iget-object v1, p0, LX/B9U;->j:Landroid/widget/ImageButton;

    if-eq p1, v1, :cond_0

    iget-object v1, p0, LX/B9U;->i:Landroid/widget/Button;

    if-ne p1, v1, :cond_2

    .line 1751744
    :cond_0
    iget-object v1, p0, LX/B9U;->a:LX/B9T;

    iget-object v2, p0, LX/B9U;->l:Lcom/facebook/graphql/model/GraphQLMegaphone;

    invoke-virtual {v1, v2}, LX/B9T;->c(Lcom/facebook/graphql/model/GraphQLMegaphone;)V

    .line 1751745
    :cond_1
    :goto_0
    const v1, -0x724c1ce9

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 1751746
    :cond_2
    iget-object v1, p0, LX/B9U;->f:Landroid/widget/Button;

    if-eq p1, v1, :cond_3

    iget-object v1, p0, LX/B9U;->g:Landroid/widget/Button;

    if-ne p1, v1, :cond_4

    .line 1751747
    :cond_3
    iget-object v1, p0, LX/B9U;->a:LX/B9T;

    iget-object v2, p0, LX/B9U;->l:Lcom/facebook/graphql/model/GraphQLMegaphone;

    invoke-virtual {v1, v2}, LX/B9T;->a(Lcom/facebook/graphql/model/GraphQLMegaphone;)V

    goto :goto_0

    .line 1751748
    :cond_4
    iget-object v1, p0, LX/B9U;->b:Lcom/facebook/widget/images/UrlImage;

    if-ne p1, v1, :cond_1

    .line 1751749
    iget-object v1, p0, LX/B9U;->a:LX/B9T;

    iget-object v2, p0, LX/B9U;->l:Lcom/facebook/graphql/model/GraphQLMegaphone;

    invoke-virtual {v1, v2}, LX/B9T;->b(Lcom/facebook/graphql/model/GraphQLMegaphone;)V

    goto :goto_0
.end method

.method public final onMeasure(II)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1751673
    invoke-virtual {p0}, LX/B9U;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    .line 1751674
    invoke-virtual {p0, v2, v2}, LX/B9U;->setMeasuredDimension(II)V

    .line 1751675
    :goto_0
    return-void

    .line 1751676
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/CustomRelativeLayout;->onMeasure(II)V

    goto :goto_0
.end method

.method public setMegaphoneStory(LX/3TD;)V
    .locals 4

    .prologue
    .line 1751677
    if-nez p1, :cond_4

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, LX/B9U;->l:Lcom/facebook/graphql/model/GraphQLMegaphone;

    .line 1751678
    iget-object v0, p0, LX/B9U;->l:Lcom/facebook/graphql/model/GraphQLMegaphone;

    if-eqz v0, :cond_5

    .line 1751679
    const/16 p1, 0x8

    const/4 v3, 0x0

    .line 1751680
    iget-object v0, p0, LX/B9U;->l:Lcom/facebook/graphql/model/GraphQLMegaphone;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMegaphone;->m()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_6

    .line 1751681
    iget-object v0, p0, LX/B9U;->f:Landroid/widget/Button;

    iput-object v0, p0, LX/B9U;->h:Landroid/widget/Button;

    .line 1751682
    iget-object v0, p0, LX/B9U;->g:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setVisibility(I)V

    .line 1751683
    iget-object v0, p0, LX/B9U;->i:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setVisibility(I)V

    .line 1751684
    iget-object v0, p0, LX/B9U;->j:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1751685
    :goto_1
    iget-object v0, p0, LX/B9U;->l:Lcom/facebook/graphql/model/GraphQLMegaphone;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMegaphone;->o()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_7

    iget-object v0, p0, LX/B9U;->l:Lcom/facebook/graphql/model/GraphQLMegaphone;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMegaphone;->o()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 1751686
    iget-object v0, p0, LX/B9U;->b:Lcom/facebook/widget/images/UrlImage;

    iget-object v1, p0, LX/B9U;->l:Lcom/facebook/graphql/model/GraphQLMegaphone;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMegaphone;->o()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/images/UrlImage;->setImageParams(Landroid/net/Uri;)V

    .line 1751687
    iget-object v0, p0, LX/B9U;->b:Lcom/facebook/widget/images/UrlImage;

    invoke-virtual {v0, v3}, Lcom/facebook/widget/images/UrlImage;->setVisibility(I)V

    .line 1751688
    :goto_2
    iget-object v0, p0, LX/B9U;->c:Landroid/widget/TextView;

    iget-object v1, p0, LX/B9U;->l:Lcom/facebook/graphql/model/GraphQLMegaphone;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMegaphone;->t()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1751689
    iget-object v1, p0, LX/B9U;->d:Landroid/widget/TextView;

    iget-object v0, p0, LX/B9U;->l:Lcom/facebook/graphql/model/GraphQLMegaphone;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMegaphone;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_8

    iget-object v0, p0, LX/B9U;->l:Lcom/facebook/graphql/model/GraphQLMegaphone;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMegaphone;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    :goto_3
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1751690
    iget-object v0, p0, LX/B9U;->c:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getLineCount()I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_9

    .line 1751691
    iget-object v0, p0, LX/B9U;->d:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 1751692
    :goto_4
    iget-object v0, p0, LX/B9U;->l:Lcom/facebook/graphql/model/GraphQLMegaphone;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMegaphone;->k()Lcom/facebook/graphql/model/GraphQLMegaphoneAction;

    move-result-object v0

    if-eqz v0, :cond_c

    .line 1751693
    iget-object v0, p0, LX/B9U;->h:Landroid/widget/Button;

    iget-object v1, p0, LX/B9U;->l:Lcom/facebook/graphql/model/GraphQLMegaphone;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMegaphone;->k()Lcom/facebook/graphql/model/GraphQLMegaphoneAction;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMegaphoneAction;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 1751694
    iget-object v0, p0, LX/B9U;->l:Lcom/facebook/graphql/model/GraphQLMegaphone;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMegaphone;->k()Lcom/facebook/graphql/model/GraphQLMegaphoneAction;

    move-result-object v0

    .line 1751695
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMegaphoneAction;->a()Ljava/lang/String;

    move-result-object v1

    .line 1751696
    if-eqz v1, :cond_0

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 1751697
    :cond_0
    const-string v1, "DEFAULT"

    .line 1751698
    :cond_1
    :goto_5
    move-object v0, v1

    .line 1751699
    const-string v1, "PROMINENT"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 1751700
    iget-object v0, p0, LX/B9U;->h:Landroid/widget/Button;

    const v1, 0x7f02019b

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 1751701
    iget-object v0, p0, LX/B9U;->h:Landroid/widget/Button;

    invoke-virtual {p0}, LX/B9U;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a004b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    .line 1751702
    :goto_6
    invoke-virtual {p0}, LX/B9U;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b19cb

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    .line 1751703
    iget-object v1, p0, LX/B9U;->h:Landroid/widget/Button;

    invoke-virtual {v1, v0, v3, v0, v3}, Landroid/widget/Button;->setPadding(IIII)V

    .line 1751704
    iget-object v0, p0, LX/B9U;->h:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 1751705
    :goto_7
    invoke-virtual {p0, v3}, LX/B9U;->setVisibility(I)V

    .line 1751706
    iget-object v0, p0, LX/B9U;->l:Lcom/facebook/graphql/model/GraphQLMegaphone;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMegaphone;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_d

    .line 1751707
    iget-object v0, p0, LX/B9U;->l:Lcom/facebook/graphql/model/GraphQLMegaphone;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMegaphone;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->c()LX/0Px;

    move-result-object v0

    iget-object v1, p0, LX/B9U;->l:Lcom/facebook/graphql/model/GraphQLMegaphone;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMegaphone;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    const/4 p1, 0x0

    .line 1751708
    iget-object v2, p0, LX/B9U;->e:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 1751709
    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_2

    .line 1751710
    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLAggregatedEntitiesAtRange;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLAggregatedEntitiesAtRange;->j()LX/0Px;

    move-result-object v2

    .line 1751711
    invoke-static {p0, v2}, LX/B9U;->a(LX/B9U;LX/0Px;)Landroid/view/View;

    move-result-object v2

    .line 1751712
    if-eqz v2, :cond_2

    .line 1751713
    iget-object v3, p0, LX/B9U;->e:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v2, p1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 1751714
    :cond_2
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 1751715
    const v2, 0x7f030ab5

    const/4 v3, 0x0

    invoke-static {p0, v2, v3}, LX/B9U;->a(LX/B9U;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 1751716
    iget-object v3, p0, LX/B9U;->e:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1751717
    const v2, 0x7f0d1b5b

    invoke-virtual {p0, v2}, LX/B9U;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 1751718
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1751719
    :cond_3
    iget-object v2, p0, LX/B9U;->e:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v2

    if-nez v2, :cond_f

    .line 1751720
    iget-object v2, p0, LX/B9U;->e:Landroid/widget/LinearLayout;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1751721
    :goto_8
    return-void

    .line 1751722
    :cond_4
    iget-object v0, p1, LX/3TD;->b:Lcom/facebook/graphql/model/GraphQLMegaphone;

    move-object v0, v0

    .line 1751723
    goto/16 :goto_0

    .line 1751724
    :cond_5
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, LX/B9U;->setVisibility(I)V

    goto :goto_8

    .line 1751725
    :cond_6
    iget-object v0, p0, LX/B9U;->g:Landroid/widget/Button;

    iput-object v0, p0, LX/B9U;->h:Landroid/widget/Button;

    .line 1751726
    iget-object v0, p0, LX/B9U;->f:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setVisibility(I)V

    .line 1751727
    iget-object v0, p0, LX/B9U;->j:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1751728
    iget-object v0, p0, LX/B9U;->i:Landroid/widget/Button;

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setVisibility(I)V

    .line 1751729
    iget-object v0, p0, LX/B9U;->i:Landroid/widget/Button;

    iget-object v1, p0, LX/B9U;->l:Lcom/facebook/graphql/model/GraphQLMegaphone;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMegaphone;->m()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 1751730
    :cond_7
    iget-object v0, p0, LX/B9U;->b:Lcom/facebook/widget/images/UrlImage;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/images/UrlImage;->setVisibility(I)V

    goto/16 :goto_2

    .line 1751731
    :cond_8
    const/4 v0, 0x0

    goto/16 :goto_3

    .line 1751732
    :cond_9
    iget-object v0, p0, LX/B9U;->d:Landroid/widget/TextView;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxLines(I)V

    goto/16 :goto_4

    .line 1751733
    :cond_a
    const-string v1, "SUBDUED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 1751734
    iget-object v0, p0, LX/B9U;->h:Landroid/widget/Button;

    const v1, 0x7f02019f

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 1751735
    iget-object v0, p0, LX/B9U;->h:Landroid/widget/Button;

    invoke-virtual {p0}, LX/B9U;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0758

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    goto/16 :goto_6

    .line 1751736
    :cond_b
    iget-object v0, p0, LX/B9U;->h:Landroid/widget/Button;

    const v1, 0x7f020190

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 1751737
    iget-object v0, p0, LX/B9U;->h:Landroid/widget/Button;

    invoke-virtual {p0}, LX/B9U;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a004b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    goto/16 :goto_6

    .line 1751738
    :cond_c
    iget-object v0, p0, LX/B9U;->h:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setVisibility(I)V

    goto/16 :goto_7

    .line 1751739
    :cond_d
    iget-object v0, p0, LX/B9U;->e:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_8

    .line 1751740
    :cond_e
    const-string v2, "DEFAULT"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "PROMINENT"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "SUBDUED"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1751741
    const-string v1, "DEFAULT"

    goto/16 :goto_5

    .line 1751742
    :cond_f
    iget-object v2, p0, LX/B9U;->e:Landroid/widget/LinearLayout;

    invoke-virtual {v2, p1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_8
.end method
