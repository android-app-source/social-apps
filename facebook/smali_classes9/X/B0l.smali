.class public final LX/B0l;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/groups/feed/protocol/FeedStoryMutationsModels$FeedbackEnableCommentingMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic b:LX/B0n;


# direct methods
.method public constructor <init>(LX/B0n;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 0

    .prologue
    .line 1734193
    iput-object p1, p0, LX/B0l;->b:LX/B0n;

    iput-object p2, p0, LX/B0l;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1734194
    iget-object v0, p0, LX/B0l;->b:LX/B0n;

    iget-object v0, v0, LX/B0n;->e:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f081055

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 1734195
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1734196
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1734197
    iget-object v0, p0, LX/B0l;->b:LX/B0n;

    iget-object v0, v0, LX/B0n;->h:LX/1Sl;

    invoke-virtual {v0}, LX/1Sl;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1734198
    if-eqz p1, :cond_0

    .line 1734199
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1734200
    if-eqz v0, :cond_0

    .line 1734201
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1734202
    check-cast v0, Lcom/facebook/groups/feed/protocol/FeedStoryMutationsModels$FeedbackEnableCommentingMutationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FeedStoryMutationsModels$FeedbackEnableCommentingMutationModel;->a()Lcom/facebook/groups/feed/protocol/FeedStoryMutationsModels$CommentsDisabledNoticeFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1734203
    iget-object v1, p0, LX/B0l;->b:LX/B0n;

    iget-object v2, p0, LX/B0l;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1734204
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1734205
    check-cast v0, Lcom/facebook/groups/feed/protocol/FeedStoryMutationsModels$FeedbackEnableCommentingMutationModel;

    invoke-virtual {v0}, Lcom/facebook/groups/feed/protocol/FeedStoryMutationsModels$FeedbackEnableCommentingMutationModel;->a()Lcom/facebook/groups/feed/protocol/FeedStoryMutationsModels$CommentsDisabledNoticeFieldsModel;

    move-result-object v0

    invoke-static {v1, v2, v0}, LX/B0n;->a$redex0(LX/B0n;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/groups/feed/protocol/FeedStoryMutationsModels$CommentsDisabledNoticeFieldsModel;)V

    .line 1734206
    :cond_0
    return-void
.end method
