.class public LX/C6A;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1KL;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1KL",
        "<",
        "Ljava/lang/String;",
        "LX/C69;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1849694
    const-class v0, LX/C6A;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/C6A;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/graphql/model/GraphQLComment;)V
    .locals 2

    .prologue
    .line 1849695
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1849696
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, LX/C6A;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/C6A;->b:Ljava/lang/String;

    .line 1849697
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1849698
    new-instance v0, LX/C69;

    invoke-direct {v0}, LX/C69;-><init>()V

    return-object v0
.end method

.method public final b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1849699
    iget-object v0, p0, LX/C6A;->b:Ljava/lang/String;

    return-object v0
.end method
