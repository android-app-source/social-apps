.class public LX/BxF;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pm;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public final a:LX/1vg;

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/5up;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/BxX;


# direct methods
.method public constructor <init>(LX/1vg;LX/0Ot;LX/BxX;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1vg;",
            "LX/0Ot",
            "<",
            "LX/5up;",
            ">;",
            "LX/BxX;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1835166
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1835167
    iput-object p1, p0, LX/BxF;->a:LX/1vg;

    .line 1835168
    iput-object p2, p0, LX/BxF;->b:LX/0Ot;

    .line 1835169
    iput-object p3, p0, LX/BxF;->c:LX/BxX;

    .line 1835170
    return-void
.end method

.method public static a(LX/BxV;I)I
    .locals 1

    .prologue
    .line 1835171
    iget-object v0, p0, LX/BxV;->a:Ljava/lang/Boolean;

    move-object v0, v0

    .line 1835172
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const p1, -0xa76f01

    :cond_0
    return p1
.end method

.method public static a(LX/0QB;)LX/BxF;
    .locals 6

    .prologue
    .line 1835173
    const-class v1, LX/BxF;

    monitor-enter v1

    .line 1835174
    :try_start_0
    sget-object v0, LX/BxF;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1835175
    sput-object v2, LX/BxF;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1835176
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1835177
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1835178
    new-instance v5, LX/BxF;

    invoke-static {v0}, LX/1vg;->a(LX/0QB;)LX/1vg;

    move-result-object v3

    check-cast v3, LX/1vg;

    const/16 v4, 0x329d

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/BxX;->a(LX/0QB;)LX/BxX;

    move-result-object v4

    check-cast v4, LX/BxX;

    invoke-direct {v5, v3, p0, v4}, LX/BxF;-><init>(LX/1vg;LX/0Ot;LX/BxX;)V

    .line 1835179
    move-object v0, v5

    .line 1835180
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1835181
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/BxF;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1835182
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1835183
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
