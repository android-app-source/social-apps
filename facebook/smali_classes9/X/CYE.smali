.class public LX/CYE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field public final mActionChannelType:Ljava/lang/String;
    .annotation build Lcom/facebook/graphql/calls/PageActionChannelType;
    .end annotation
.end field

.field public final mActionId:Ljava/lang/String;

.field public final mActionPosition:I

.field public final mActionType:Lcom/facebook/graphql/enums/GraphQLPageActionType;

.field public final mIncompleteAction:Z

.field public final mUseActionFlow:Z


# direct methods
.method public constructor <init>(ZLjava/lang/String;Lcom/facebook/graphql/enums/GraphQLPageActionType;Ljava/lang/String;IZ)V
    .locals 0
    .param p2    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/PageActionChannelType;
        .end annotation
    .end param

    .prologue
    .line 1910852
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1910853
    iput-boolean p1, p0, LX/CYE;->mUseActionFlow:Z

    .line 1910854
    iput-object p2, p0, LX/CYE;->mActionChannelType:Ljava/lang/String;

    .line 1910855
    iput-object p3, p0, LX/CYE;->mActionType:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 1910856
    iput-object p4, p0, LX/CYE;->mActionId:Ljava/lang/String;

    .line 1910857
    iput p5, p0, LX/CYE;->mActionPosition:I

    .line 1910858
    iput-boolean p6, p0, LX/CYE;->mIncompleteAction:Z

    .line 1910859
    return-void
.end method
