.class public final LX/BfW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;

.field public final synthetic b:I

.field public final synthetic c:LX/BfX;


# direct methods
.method public constructor <init>(LX/BfX;Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;I)V
    .locals 0

    .prologue
    .line 1806105
    iput-object p1, p0, LX/BfW;->c:LX/BfX;

    iput-object p2, p0, LX/BfW;->a:Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;

    iput p3, p0, LX/BfW;->b:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x2

    const v0, -0x43d10cad

    invoke-static {v3, v4, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1806106
    iget-object v1, p0, LX/BfW;->a:Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;

    invoke-virtual {v1, v2, v2}, Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1806107
    iget-object v1, p0, LX/BfW;->c:LX/BfX;

    iget-object v1, v1, LX/BfX;->g:Lcom/facebook/crowdsourcing/helper/HoursData;

    iget v2, p0, LX/BfW;->b:I

    .line 1806108
    invoke-virtual {v1, v2}, Lcom/facebook/crowdsourcing/helper/HoursData;->a(I)LX/Bex;

    move-result-object v5

    iget-object v5, v5, LX/Bex;->a:LX/0Px;

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v5

    if-lt v4, v5, :cond_1

    .line 1806109
    :goto_0
    iget-object v1, p0, LX/BfW;->c:LX/BfX;

    iget-object v1, v1, LX/BfX;->a:Lcom/facebook/crowdsourcing/picker/hours/HoursPickerFragment;

    .line 1806110
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 1806111
    const-string v4, "extra_hours_selected_option"

    iget-object v5, v1, Lcom/facebook/crowdsourcing/picker/hours/HoursPickerFragment;->a:LX/BfX;

    .line 1806112
    iget v6, v5, LX/BfX;->f:I

    move v5, v6

    .line 1806113
    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1806114
    iget-object v4, v1, Lcom/facebook/crowdsourcing/picker/hours/HoursPickerFragment;->a:LX/BfX;

    .line 1806115
    iget v5, v4, LX/BfX;->f:I

    move v4, v5

    .line 1806116
    if-nez v4, :cond_0

    .line 1806117
    const-string v4, "extra_hours_data"

    iget-object v5, v1, Lcom/facebook/crowdsourcing/picker/hours/HoursPickerFragment;->a:LX/BfX;

    .line 1806118
    iget-object v6, v5, LX/BfX;->g:Lcom/facebook/crowdsourcing/helper/HoursData;

    move-object v5, v6

    .line 1806119
    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1806120
    :cond_0
    invoke-virtual {v1}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v4

    const/4 v5, -0x1

    invoke-virtual {v4, v5, v2}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 1806121
    const v1, -0x540cae94

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1806122
    :cond_1
    invoke-virtual {v1, v2}, Lcom/facebook/crowdsourcing/helper/HoursData;->a(I)LX/Bex;

    move-result-object v6

    .line 1806123
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v7

    .line 1806124
    const/4 v5, 0x0

    :goto_1
    iget-object p1, v6, LX/Bex;->a:LX/0Px;

    invoke-virtual {p1}, LX/0Px;->size()I

    move-result p1

    if-ge v5, p1, :cond_3

    .line 1806125
    if-eq v5, v4, :cond_2

    .line 1806126
    iget-object p1, v6, LX/Bex;->a:LX/0Px;

    invoke-virtual {p1, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {v7, p1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1806127
    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 1806128
    :cond_3
    new-instance v5, LX/Bex;

    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v6

    invoke-direct {v5, v6}, LX/Bex;-><init>(LX/0Px;)V

    invoke-virtual {v1, v2, v5}, Lcom/facebook/crowdsourcing/helper/HoursData;->a(ILX/Bex;)V

    goto :goto_0
.end method
