.class public final LX/BzT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

.field public final synthetic b:Lcom/facebook/graphql/model/GraphQLStory;

.field public final synthetic c:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic d:Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoWithDirectInstallOverlayPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoWithDirectInstallOverlayPartDefinition;Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 0

    .prologue
    .line 1838735
    iput-object p1, p0, LX/BzT;->d:Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoWithDirectInstallOverlayPartDefinition;

    iput-object p2, p0, LX/BzT;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    iput-object p3, p0, LX/BzT;->b:Lcom/facebook/graphql/model/GraphQLStory;

    iput-object p4, p0, LX/BzT;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v0, 0x2

    const v1, -0x62d277ed

    invoke-static {v0, v6, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1838736
    check-cast p1, Lcom/facebook/attachments/angora/CoverPhotoWithCTAOverlayView;

    .line 1838737
    iget-boolean v1, p1, Lcom/facebook/attachments/angora/CoverPhotoWithCTAOverlayView;->d:Z

    move v1, v1

    .line 1838738
    if-eqz v1, :cond_0

    .line 1838739
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lcom/facebook/attachments/angora/CoverPhotoWithCTAOverlayView;->setIsOverlayVisible(Z)V

    .line 1838740
    :goto_0
    invoke-virtual {p1}, Lcom/facebook/attachments/angora/CoverPhotoWithCTAOverlayView;->invalidate()V

    .line 1838741
    const v1, 0x709514a3

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 1838742
    :cond_0
    invoke-virtual {p1, v6}, Lcom/facebook/attachments/angora/CoverPhotoWithCTAOverlayView;->setIsOverlayVisible(Z)V

    .line 1838743
    iget-object v1, p0, LX/BzT;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    iget-object v2, p0, LX/BzT;->b:Lcom/facebook/graphql/model/GraphQLStory;

    iget-object v3, p0, LX/BzT;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->C()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, LX/2yB;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;)LX/47G;

    move-result-object v1

    .line 1838744
    iget-object v2, p0, LX/BzT;->d:Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoWithDirectInstallOverlayPartDefinition;

    const-string v3, "neko_di_view_cta_overlay"

    iget-object v4, p0, LX/BzT;->d:Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoWithDirectInstallOverlayPartDefinition;

    iget-object v5, p0, LX/BzT;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v4, v5, v6}, Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoWithDirectInstallOverlayPartDefinition;->a(Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoWithDirectInstallOverlayPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;Z)Ljava/util/Map;

    move-result-object v4

    invoke-static {v2, v3, v1, v4, v6}, Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoWithDirectInstallOverlayPartDefinition;->a$redex0(Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoWithDirectInstallOverlayPartDefinition;Ljava/lang/String;LX/47G;Ljava/util/Map;Z)V

    goto :goto_0
.end method
