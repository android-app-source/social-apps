.class public final LX/BFf;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Landroid/net/Uri;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/photos/creativecam/ui/CreativeCamPickerPreviewControllerImpl;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/creativecam/ui/CreativeCamPickerPreviewControllerImpl;)V
    .locals 0

    .prologue
    .line 1766133
    iput-object p1, p0, LX/BFf;->a:Lcom/facebook/photos/creativecam/ui/CreativeCamPickerPreviewControllerImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1766128
    const/4 v0, 0x0

    .line 1766129
    iget-object v1, p0, LX/BFf;->a:Lcom/facebook/photos/creativecam/ui/CreativeCamPickerPreviewControllerImpl;

    iget-object v1, v1, Lcom/facebook/photos/creativecam/ui/CreativeCamPickerPreviewControllerImpl;->d:LX/8I2;

    sget-object v2, LX/4gI;->PHOTO_ONLY:LX/4gI;

    invoke-virtual {v1, v2, v0}, LX/8I2;->a(LX/4gI;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 1766130
    iget-object v2, p0, LX/BFf;->a:Lcom/facebook/photos/creativecam/ui/CreativeCamPickerPreviewControllerImpl;

    iget-object v2, v2, Lcom/facebook/photos/creativecam/ui/CreativeCamPickerPreviewControllerImpl;->d:LX/8I2;

    const/4 v3, 0x1

    invoke-virtual {v2, v1, v3}, LX/8I2;->a(Landroid/database/Cursor;I)Ljava/util/List;

    move-result-object v1

    .line 1766131
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1766132
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method
