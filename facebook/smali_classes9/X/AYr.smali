.class public LX/AYr;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/AYr;


# instance fields
.field public final a:LX/0Zb;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:J


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1686168
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1686169
    iput-object p1, p0, LX/AYr;->a:LX/0Zb;

    .line 1686170
    return-void
.end method

.method public static a(LX/0QB;)LX/AYr;
    .locals 4

    .prologue
    .line 1686171
    sget-object v0, LX/AYr;->e:LX/AYr;

    if-nez v0, :cond_1

    .line 1686172
    const-class v1, LX/AYr;

    monitor-enter v1

    .line 1686173
    :try_start_0
    sget-object v0, LX/AYr;->e:LX/AYr;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1686174
    if-eqz v2, :cond_0

    .line 1686175
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1686176
    new-instance p0, LX/AYr;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-direct {p0, v3}, LX/AYr;-><init>(LX/0Zb;)V

    .line 1686177
    move-object v0, p0

    .line 1686178
    sput-object v0, LX/AYr;->e:LX/AYr;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1686179
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1686180
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1686181
    :cond_1
    sget-object v0, LX/AYr;->e:LX/AYr;

    return-object v0

    .line 1686182
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1686183
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1686184
    iput-object p1, p0, LX/AYr;->b:Ljava/lang/String;

    .line 1686185
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1686186
    iput-object p1, p0, LX/AYr;->c:Ljava/lang/String;

    .line 1686187
    return-void
.end method
