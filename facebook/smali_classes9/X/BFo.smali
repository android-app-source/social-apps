.class public LX/BFo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/89f;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Landroid/view/View$OnTouchListener;

.field private static volatile e:LX/BFo;


# instance fields
.field public final b:LX/9c7;

.field public c:LX/89Z;

.field private d:Landroid/view/View;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1766315
    new-instance v0, LX/BFl;

    invoke-direct {v0}, LX/BFl;-><init>()V

    sput-object v0, LX/BFo;->a:Landroid/view/View$OnTouchListener;

    return-void
.end method

.method public constructor <init>(LX/9c7;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1766281
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1766282
    iput-object p1, p0, LX/BFo;->b:LX/9c7;

    .line 1766283
    return-void
.end method

.method public static a(LX/0QB;)LX/BFo;
    .locals 4

    .prologue
    .line 1766302
    sget-object v0, LX/BFo;->e:LX/BFo;

    if-nez v0, :cond_1

    .line 1766303
    const-class v1, LX/BFo;

    monitor-enter v1

    .line 1766304
    :try_start_0
    sget-object v0, LX/BFo;->e:LX/BFo;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1766305
    if-eqz v2, :cond_0

    .line 1766306
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1766307
    new-instance p0, LX/BFo;

    invoke-static {v0}, LX/9c7;->b(LX/0QB;)LX/9c7;

    move-result-object v3

    check-cast v3, LX/9c7;

    invoke-direct {p0, v3}, LX/BFo;-><init>(LX/9c7;)V

    .line 1766308
    move-object v0, p0

    .line 1766309
    sput-object v0, LX/BFo;->e:LX/BFo;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1766310
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1766311
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1766312
    :cond_1
    sget-object v0, LX/BFo;->e:LX/BFo;

    return-object v0

    .line 1766313
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1766314
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Landroid/view/View;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 1766301
    iget-object v0, p0, LX/BFo;->d:Landroid/view/View;

    return-object v0
.end method

.method public final a(Landroid/view/ViewStub;LX/Jvj;)Landroid/view/ViewGroup;
    .locals 3

    .prologue
    .line 1766289
    const v0, 0x7f030231

    invoke-virtual {p1, v0}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 1766290
    invoke-virtual {p1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 1766291
    const v1, 0x7f0d05f0

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/fbui/glyph/GlyphView;

    .line 1766292
    sget-object p1, LX/BFo;->a:Landroid/view/View$OnTouchListener;

    invoke-virtual {v1, p1}, Lcom/facebook/fbui/glyph/GlyphView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1766293
    new-instance p1, LX/BFm;

    invoke-direct {p1, p0, p2}, LX/BFm;-><init>(LX/BFo;LX/Jvj;)V

    invoke-virtual {v1, p1}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1766294
    const v1, 0x7f0d0888

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/fbui/glyph/GlyphView;

    .line 1766295
    invoke-static {}, Landroid/hardware/Camera;->getNumberOfCameras()I

    move-result v2

    const/4 p1, 0x2

    if-ge v2, p1, :cond_0

    .line 1766296
    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1766297
    :cond_0
    sget-object v2, LX/BFo;->a:Landroid/view/View$OnTouchListener;

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1766298
    new-instance v2, LX/BFn;

    invoke-direct {v2, p0, p2}, LX/BFn;-><init>(LX/BFo;LX/Jvj;)V

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1766299
    const v1, 0x7f0d0685

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, LX/BFo;->d:Landroid/view/View;

    .line 1766300
    return-object v0
.end method

.method public final a(Landroid/view/ViewGroup;LX/0Px;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "LX/0Px",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 1766284
    const v0, 0x7f0d0887

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    .line 1766285
    if-eqz p2, :cond_0

    invoke-virtual {p2}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1766286
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1766287
    :goto_0
    return-void

    .line 1766288
    :cond_0
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method
