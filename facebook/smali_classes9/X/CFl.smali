.class public final enum LX/CFl;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/CFl;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/CFl;

.field public static final enum IMAGE:LX/CFl;

.field public static final enum UNKNOWN:LX/CFl;

.field public static final enum VIDEO:LX/CFl;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1864040
    new-instance v0, LX/CFl;

    const-string v1, "IMAGE"

    invoke-direct {v0, v1, v2}, LX/CFl;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CFl;->IMAGE:LX/CFl;

    .line 1864041
    new-instance v0, LX/CFl;

    const-string v1, "VIDEO"

    invoke-direct {v0, v1, v3}, LX/CFl;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CFl;->VIDEO:LX/CFl;

    .line 1864042
    new-instance v0, LX/CFl;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v4}, LX/CFl;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CFl;->UNKNOWN:LX/CFl;

    .line 1864043
    const/4 v0, 0x3

    new-array v0, v0, [LX/CFl;

    sget-object v1, LX/CFl;->IMAGE:LX/CFl;

    aput-object v1, v0, v2

    sget-object v1, LX/CFl;->VIDEO:LX/CFl;

    aput-object v1, v0, v3

    sget-object v1, LX/CFl;->UNKNOWN:LX/CFl;

    aput-object v1, v0, v4

    sput-object v0, LX/CFl;->$VALUES:[LX/CFl;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1864044
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/CFl;
    .locals 1

    .prologue
    .line 1864045
    const-class v0, LX/CFl;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/CFl;

    return-object v0
.end method

.method public static values()[LX/CFl;
    .locals 1

    .prologue
    .line 1864046
    sget-object v0, LX/CFl;->$VALUES:[LX/CFl;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/CFl;

    return-object v0
.end method
