.class public LX/BAN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/BAH;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1753018
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLStoryActionLink;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1753019
    if-nez p1, :cond_0

    .line 1753020
    const/4 v0, 0x0

    .line 1753021
    :goto_0
    return-object v0

    .line 1753022
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->br()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    .line 1753023
    if-nez v0, :cond_1

    .line 1753024
    sget-object v0, LX/0ax;->iK:Ljava/lang/String;

    invoke-static {v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1753025
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v0

    .line 1753026
    sget-object v1, LX/0ax;->iL:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
