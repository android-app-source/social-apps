.class public LX/Ak8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ":",
        "LX/1aQ;",
        ">",
        "Ljava/lang/Object;",
        "Landroid/animation/Animator$AnimatorListener;"
    }
.end annotation


# instance fields
.field private final a:Landroid/view/View;

.field private final b:Z

.field private final c:F

.field public final d:LX/AkM;


# direct methods
.method public constructor <init>(Landroid/view/View;ZFLX/AkM;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;ZF",
            "LX/AkM;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1708784
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1708785
    check-cast p1, LX/1aQ;

    invoke-interface {p1}, LX/1aQ;->getFlyoutView()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/Ak8;->a:Landroid/view/View;

    .line 1708786
    iput-boolean p2, p0, LX/Ak8;->b:Z

    .line 1708787
    iput p3, p0, LX/Ak8;->c:F

    .line 1708788
    iput-object p4, p0, LX/Ak8;->d:LX/AkM;

    .line 1708789
    return-void
.end method


# virtual methods
.method public final onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 1708790
    return-void
.end method

.method public final onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 1708791
    iget-boolean v0, p0, LX/Ak8;->b:Z

    if-nez v0, :cond_0

    .line 1708792
    iget-object v0, p0, LX/Ak8;->a:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1708793
    iget-object v0, p0, LX/Ak8;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1708794
    :goto_0
    return-void

    .line 1708795
    :cond_0
    iget-object v0, p0, LX/Ak8;->d:LX/AkM;

    if-nez v0, :cond_2

    .line 1708796
    :cond_1
    :goto_1
    goto :goto_0

    .line 1708797
    :cond_2
    iget-object v0, p0, LX/Ak8;->d:LX/AkM;

    invoke-interface {v0}, LX/AkM;->getExpandAnimator()Landroid/animation/Animator;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1708798
    iget-object v0, p0, LX/Ak8;->d:LX/AkM;

    invoke-interface {v0}, LX/AkM;->getExpandAnimator()Landroid/animation/Animator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    goto :goto_1
.end method

.method public final onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 1708773
    return-void
.end method

.method public final onAnimationStart(Landroid/animation/Animator;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1708774
    iget-boolean v0, p0, LX/Ak8;->b:Z

    if-eqz v0, :cond_0

    .line 1708775
    iget-object v0, p0, LX/Ak8;->a:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1708776
    iget-object v0, p0, LX/Ak8;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1708777
    :goto_0
    iget-object v0, p0, LX/Ak8;->a:Landroid/view/View;

    iget-object v1, p0, LX/Ak8;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    int-to-float v1, v1

    iget v2, p0, LX/Ak8;->c:F

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/view/View;->setPivotX(F)V

    .line 1708778
    iget-object v0, p0, LX/Ak8;->a:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setPivotY(F)V

    .line 1708779
    return-void

    .line 1708780
    :cond_0
    iget-object v0, p0, LX/Ak8;->d:LX/AkM;

    if-nez v0, :cond_2

    .line 1708781
    :cond_1
    :goto_1
    goto :goto_0

    .line 1708782
    :cond_2
    iget-object v0, p0, LX/Ak8;->d:LX/AkM;

    invoke-interface {v0}, LX/AkM;->getCollapseAnimator()Landroid/animation/Animator;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1708783
    iget-object v0, p0, LX/Ak8;->d:LX/AkM;

    invoke-interface {v0}, LX/AkM;->getCollapseAnimator()Landroid/animation/Animator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    goto :goto_1
.end method
