.class public LX/BSe;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:LX/09G;

.field private final c:LX/0lC;

.field private final d:LX/03V;

.field public final e:LX/0Sh;

.field public final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0kL;

.field public final h:Ljava/lang/String;

.field public final i:Ljava/lang/String;

.field public final j:LX/3H4;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1785638
    const-class v0, LX/BSe;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/BSe;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LX/09G;LX/0lC;LX/03V;LX/0Sh;LX/0Or;LX/0kL;LX/3H4;LX/2ml;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/0Or;
        .annotation runtime Lcom/facebook/video/commercialbreak/annotations/IsCommercialBreakDebugToastsEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/09G;",
            "LX/0lC;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0kL;",
            "LX/3H4;",
            "LX/2ml;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1785639
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1785640
    iput-object p1, p0, LX/BSe;->h:Ljava/lang/String;

    .line 1785641
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p9}, LX/2ml;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/BSe;->i:Ljava/lang/String;

    .line 1785642
    iput-object p2, p0, LX/BSe;->b:LX/09G;

    .line 1785643
    iput-object p3, p0, LX/BSe;->c:LX/0lC;

    .line 1785644
    iput-object p4, p0, LX/BSe;->d:LX/03V;

    .line 1785645
    iput-object p5, p0, LX/BSe;->e:LX/0Sh;

    .line 1785646
    iput-object p6, p0, LX/BSe;->f:LX/0Or;

    .line 1785647
    iput-object p7, p0, LX/BSe;->g:LX/0kL;

    .line 1785648
    iput-object p8, p0, LX/BSe;->j:LX/3H4;

    .line 1785649
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/BSa;
    .locals 9
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v8, 0x0

    const/4 v1, 0x0

    .line 1785650
    :try_start_0
    iget-object v0, p0, LX/BSe;->c:LX/0lC;

    invoke-virtual {v0, p1}, LX/0lC;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 1785651
    const-string v2, "type"

    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    .line 1785652
    const-string v3, "commercial_break_length_ms"

    invoke-virtual {v0, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    .line 1785653
    const-string v3, "commercial_break_start_time_ms"

    invoke-virtual {v0, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v6

    .line 1785654
    const-string v3, "index"

    invoke-virtual {v0, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1785655
    if-nez v2, :cond_0

    .line 1785656
    :goto_0
    return-object v1

    .line 1785657
    :catch_0
    move-exception v0

    .line 1785658
    sget-object v2, LX/BSe;->a:Ljava/lang/String;

    const-string v3, "Failed to parse payload: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p1, v4, v8

    invoke-static {v2, v0, v3, v4}, LX/01m;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 1785659
    :cond_0
    :try_start_1
    invoke-virtual {v2}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/BSZ;->valueOf(Ljava/lang/String;)LX/BSZ;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v3

    .line 1785660
    new-instance v1, LX/BSa;

    iget-object v2, p0, LX/BSe;->h:Ljava/lang/String;

    if-eqz v4, :cond_2

    invoke-virtual {v4}, LX/0lF;->D()J

    move-result-wide v4

    :goto_1
    if-eqz v6, :cond_3

    invoke-virtual {v6}, LX/0lF;->D()J

    move-result-wide v6

    :goto_2
    if-eqz v0, :cond_1

    invoke-virtual {v0}, LX/0lF;->C()I

    move-result v8

    :cond_1
    invoke-direct/range {v1 .. v8}, LX/BSa;-><init>(Ljava/lang/String;LX/BSZ;JJI)V

    goto :goto_0

    .line 1785661
    :catch_1
    move-exception v0

    .line 1785662
    iget-object v2, p0, LX/BSe;->d:LX/03V;

    sget-object v3, LX/BSe;->a:Ljava/lang/String;

    const-string v4, "Received message with an invalid type"

    invoke-virtual {v2, v3, v4, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1785663
    :cond_2
    const-wide/16 v4, -0x1

    goto :goto_1

    :cond_3
    const-wide/16 v6, 0x0

    goto :goto_2
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 1785664
    iget-object v0, p0, LX/BSe;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1785665
    iget-object v0, p0, LX/BSe;->e:LX/0Sh;

    new-instance v1, Lcom/facebook/video/commercialbreak/pubsub/CommercialBreakMessageSubscriber$3;

    invoke-direct {v1, p0}, Lcom/facebook/video/commercialbreak/pubsub/CommercialBreakMessageSubscriber$3;-><init>(LX/BSe;)V

    invoke-virtual {v0, v1}, LX/0Sh;->a(Ljava/lang/Runnable;)V

    .line 1785666
    :cond_0
    iget-object v0, p0, LX/BSe;->b:LX/09G;

    iget-object v1, p0, LX/BSe;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/09G;->a(Ljava/lang/String;)V

    .line 1785667
    return-void
.end method
