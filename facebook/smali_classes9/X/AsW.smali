.class public LX/AsW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/AsV;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "LX/0io;",
        ":",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationStateSpec$ProvidesInspirationState;",
        "DerivedData:",
        "Ljava/lang/Object;",
        "Mutation::",
        "Lcom/facebook/ipc/composer/dataaccessor/ComposerCanSave;",
        ":",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationStateSpec$SetsInspirationState",
        "<TMutation;>;Services::",
        "LX/0il",
        "<TModelData;>;:",
        "LX/0ik",
        "<TDerivedData;>;:",
        "LX/0im",
        "<TMutation;>;>",
        "Ljava/lang/Object;",
        "LX/AsV;"
    }
.end annotation


# instance fields
.field public final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TServices;>;"
        }
    .end annotation
.end field

.field public final b:LX/0hs;

.field public c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public d:LX/AsT;


# direct methods
.method public constructor <init>(LX/0il;Landroid/content/Context;)V
    .locals 2
    .param p1    # LX/0il;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TServices;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1720639
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1720640
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/AsW;->a:Ljava/lang/ref/WeakReference;

    .line 1720641
    new-instance v0, LX/0hs;

    invoke-direct {v0, p2}, LX/0hs;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/AsW;->b:LX/0hs;

    .line 1720642
    iget-object v0, p0, LX/AsW;->b:LX/0hs;

    sget-object v1, LX/3AV;->BELOW:LX/3AV;

    invoke-virtual {v0, v1}, LX/0ht;->a(LX/3AV;)V

    .line 1720643
    new-instance v0, LX/AsU;

    invoke-direct {v0, p0}, LX/AsU;-><init>(LX/AsW;)V

    iput-object v0, p0, LX/AsW;->d:LX/AsT;

    .line 1720644
    return-void
.end method

.method public static a$redex0(LX/AsW;Z)V
    .locals 2

    .prologue
    .line 1720645
    iget-object v1, p0, LX/AsW;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    if-eqz p1, :cond_0

    const v0, 0x7f02154b

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setImageResource(I)V

    .line 1720646
    iget-object v0, p0, LX/AsW;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->invalidate()V

    .line 1720647
    return-void

    .line 1720648
    :cond_0
    const v0, 0x7f02154a

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/AsT;
    .locals 1

    .prologue
    .line 1720649
    iget-object v0, p0, LX/AsW;->d:LX/AsT;

    return-object v0
.end method

.method public final a(Lcom/facebook/drawee/fbpipeline/FbDraweeView;)V
    .locals 1

    .prologue
    .line 1720650
    iput-object p1, p0, LX/AsW;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1720651
    iget-object v0, p0, LX/AsW;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->isMuted()Z

    move-result v0

    invoke-static {p0, v0}, LX/AsW;->a$redex0(LX/AsW;Z)V

    .line 1720652
    return-void
.end method
