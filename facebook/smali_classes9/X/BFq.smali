.class public LX/BFq;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/photos/creativecam/ui/MultiStateToggleImageButton;

.field public final b:LX/Jvg;

.field public final c:LX/Jvh;

.field public d:Z


# direct methods
.method public constructor <init>(LX/Jvg;LX/Jvh;Lcom/facebook/photos/creativecam/ui/MultiStateToggleImageButton;Ljava/lang/Boolean;)V
    .locals 2
    .param p1    # LX/Jvg;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/Jvh;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/photos/creativecam/ui/MultiStateToggleImageButton;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1766323
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1766324
    iput-object p1, p0, LX/BFq;->b:LX/Jvg;

    .line 1766325
    iput-object p2, p0, LX/BFq;->c:LX/Jvh;

    .line 1766326
    iput-object p3, p0, LX/BFq;->a:Lcom/facebook/photos/creativecam/ui/MultiStateToggleImageButton;

    .line 1766327
    invoke-static {p4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/BFq;->d:Z

    .line 1766328
    iget-object v0, p0, LX/BFq;->a:Lcom/facebook/photos/creativecam/ui/MultiStateToggleImageButton;

    new-instance v1, LX/BFp;

    invoke-direct {v1, p0}, LX/BFp;-><init>(LX/BFq;)V

    invoke-virtual {v0, v1}, Lcom/facebook/photos/creativecam/ui/MultiStateToggleImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1766329
    return-void
.end method
