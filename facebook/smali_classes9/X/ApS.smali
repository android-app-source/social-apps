.class public final LX/ApS;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/ApS;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/ApQ;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/ApT;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1716078
    const/4 v0, 0x0

    sput-object v0, LX/ApS;->a:LX/ApS;

    .line 1716079
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/ApS;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1716058
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1716059
    new-instance v0, LX/ApT;

    invoke-direct {v0}, LX/ApT;-><init>()V

    iput-object v0, p0, LX/ApS;->c:LX/ApT;

    .line 1716060
    return-void
.end method

.method public static c(LX/1De;)LX/ApQ;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1716070
    new-instance v1, LX/ApR;

    invoke-direct {v1}, LX/ApR;-><init>()V

    .line 1716071
    sget-object v2, LX/ApS;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/ApQ;

    .line 1716072
    if-nez v2, :cond_0

    .line 1716073
    new-instance v2, LX/ApQ;

    invoke-direct {v2}, LX/ApQ;-><init>()V

    .line 1716074
    :cond_0
    invoke-static {v2, p0, v0, v0, v1}, LX/ApQ;->a$redex0(LX/ApQ;LX/1De;IILX/ApR;)V

    .line 1716075
    move-object v1, v2

    .line 1716076
    move-object v0, v1

    .line 1716077
    return-object v0
.end method

.method public static declared-synchronized q()LX/ApS;
    .locals 2

    .prologue
    .line 1716066
    const-class v1, LX/ApS;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/ApS;->a:LX/ApS;

    if-nez v0, :cond_0

    .line 1716067
    new-instance v0, LX/ApS;

    invoke-direct {v0}, LX/ApS;-><init>()V

    sput-object v0, LX/ApS;->a:LX/ApS;

    .line 1716068
    :cond_0
    sget-object v0, LX/ApS;->a:LX/ApS;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 1716069
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 9

    .prologue
    .line 1716063
    check-cast p2, LX/ApR;

    .line 1716064
    iget v1, p2, LX/ApR;->a:I

    iget-object v2, p2, LX/ApR;->b:Ljava/lang/CharSequence;

    iget-object v3, p2, LX/ApR;->c:Ljava/lang/CharSequence;

    iget-object v4, p2, LX/ApR;->d:Ljava/lang/CharSequence;

    iget-object v5, p2, LX/ApR;->e:LX/1X1;

    iget-object v6, p2, LX/ApR;->f:LX/1X1;

    iget-object v7, p2, LX/ApR;->g:LX/1dQ;

    iget-boolean v8, p2, LX/ApR;->h:Z

    move-object v0, p1

    invoke-static/range {v0 .. v8}, LX/ApT;->a(LX/1De;ILjava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;LX/1X1;LX/1X1;LX/1dQ;Z)LX/1Dg;

    move-result-object v0

    .line 1716065
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1716061
    invoke-static {}, LX/1dS;->b()V

    .line 1716062
    const/4 v0, 0x0

    return-object v0
.end method
