.class public final LX/BQa;
.super LX/8KR;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/8KR",
        "<",
        "Lcom/facebook/photos/upload/event/MediaUploadSuccessEvent;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/BQb;


# direct methods
.method public constructor <init>(LX/BQb;)V
    .locals 0

    .prologue
    .line 1782425
    iput-object p1, p0, LX/BQa;->a:LX/BQb;

    invoke-direct {p0}, LX/8KR;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "Lcom/facebook/photos/upload/event/MediaUploadSuccessEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1782426
    const-class v0, Lcom/facebook/photos/upload/event/MediaUploadSuccessEvent;

    return-object v0
.end method

.method public final b(LX/0b7;)V
    .locals 2

    .prologue
    .line 1782427
    check-cast p1, Lcom/facebook/photos/upload/event/MediaUploadSuccessEvent;

    .line 1782428
    sget-object v0, LX/BQY;->a:[I

    .line 1782429
    iget-object v1, p1, LX/0b5;->a:Lcom/facebook/photos/upload/operation/UploadOperation;

    move-object v1, v1

    .line 1782430
    iget-object p1, v1, Lcom/facebook/photos/upload/operation/UploadOperation;->r:LX/8LS;

    move-object v1, p1

    .line 1782431
    invoke-virtual {v1}, LX/8LS;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1782432
    :cond_0
    :goto_0
    return-void

    .line 1782433
    :pswitch_0
    iget-object v0, p0, LX/BQa;->a:LX/BQb;

    iget-object v0, v0, LX/BQb;->d:LX/HWy;

    if-eqz v0, :cond_0

    .line 1782434
    iget-object v0, p0, LX/BQa;->a:LX/BQb;

    iget-object v0, v0, LX/BQb;->d:LX/HWy;

    .line 1782435
    iget-object v1, v0, LX/HWy;->a:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v1, v1, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->ak:LX/HPn;

    sget-object p0, LX/HPm;->FORCED_BY_USER:LX/HPm;

    invoke-virtual {v1, p0}, LX/HPn;->a(LX/HPm;)V

    .line 1782436
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
