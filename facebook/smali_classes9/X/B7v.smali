.class public LX/B7v;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements LX/B7m;


# static fields
.field public static final a:LX/B7j;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/B7j",
            "<",
            "LX/B7v;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/11S;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/B7W;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;

.field public e:LX/B7w;

.field public f:Lcom/facebook/resources/ui/FbButton;

.field private g:Lcom/facebook/widget/text/BetterTextView;

.field public h:Ljava/util/Calendar;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1748270
    new-instance v0, LX/B7s;

    invoke-direct {v0}, LX/B7s;-><init>()V

    sput-object v0, LX/B7v;->a:LX/B7j;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1748290
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 1748291
    const/4 v0, 0x0

    iput-object v0, p0, LX/B7v;->h:Ljava/util/Calendar;

    .line 1748292
    const v0, 0x7f0309c4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1748293
    const v0, 0x7f0d18e0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, LX/B7v;->f:Lcom/facebook/resources/ui/FbButton;

    .line 1748294
    const v0, 0x7f0d18df

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/B7v;->g:Lcom/facebook/widget/text/BetterTextView;

    .line 1748295
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    check-cast p0, LX/B7v;

    const/16 v0, 0x2e4

    invoke-static {p1, v0}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v0

    invoke-static {p1}, LX/B7W;->a(LX/0QB;)LX/B7W;

    move-result-object p1

    check-cast p1, LX/B7W;

    iput-object v0, p0, LX/B7v;->b:LX/0Or;

    iput-object p1, p0, LX/B7v;->c:LX/B7W;

    .line 1748296
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1748287
    iget-object v0, p0, LX/B7v;->f:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1748288
    iput-object v1, p0, LX/B7v;->e:LX/B7w;

    .line 1748289
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;LX/B7F;I)V
    .locals 2

    .prologue
    .line 1748283
    iput-object p1, p0, LX/B7v;->d:Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;

    .line 1748284
    iget-object v0, p0, LX/B7v;->g:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, p0, LX/B7v;->d:Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->p()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1748285
    iget-object v0, p0, LX/B7v;->f:Lcom/facebook/resources/ui/FbButton;

    new-instance v1, LX/B7t;

    invoke-direct {v1, p0}, LX/B7t;-><init>(LX/B7v;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1748286
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1748282
    return-void
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 1748281
    return-void
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 1748280
    return-void
.end method

.method public getBoundedInfoFieldData()Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;
    .locals 1

    .prologue
    .line 1748279
    iget-object v0, p0, LX/B7v;->d:Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;

    return-object v0
.end method

.method public getInputCustomToken()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1748278
    invoke-virtual {p0}, LX/B7v;->getInputValue()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getInputValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1748277
    iget-object v0, p0, LX/B7v;->f:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbButton;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setInputValue(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1748273
    invoke-static {p1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1748274
    iget-object v0, p0, LX/B7v;->f:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 1748275
    :cond_0
    iget-object v0, p0, LX/B7v;->f:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbButton;->clearFocus()V

    .line 1748276
    return-void
.end method

.method public setOnDataChangeListener(LX/B7w;)V
    .locals 0

    .prologue
    .line 1748271
    iput-object p1, p0, LX/B7v;->e:LX/B7w;

    .line 1748272
    return-void
.end method
