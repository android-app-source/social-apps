.class public LX/BNF;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/BNF;


# instance fields
.field private final a:Ljava/util/Locale;

.field private final b:Landroid/content/res/Resources;

.field private final c:LX/1eu;


# direct methods
.method public constructor <init>(Ljava/util/Locale;Landroid/content/res/Resources;LX/1eu;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1778428
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1778429
    iput-object p1, p0, LX/BNF;->a:Ljava/util/Locale;

    .line 1778430
    iput-object p2, p0, LX/BNF;->b:Landroid/content/res/Resources;

    .line 1778431
    iput-object p3, p0, LX/BNF;->c:LX/1eu;

    .line 1778432
    return-void
.end method

.method public static a(LX/0QB;)LX/BNF;
    .locals 6

    .prologue
    .line 1778433
    sget-object v0, LX/BNF;->d:LX/BNF;

    if-nez v0, :cond_1

    .line 1778434
    const-class v1, LX/BNF;

    monitor-enter v1

    .line 1778435
    :try_start_0
    sget-object v0, LX/BNF;->d:LX/BNF;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1778436
    if-eqz v2, :cond_0

    .line 1778437
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1778438
    new-instance p0, LX/BNF;

    invoke-static {v0}, LX/0eD;->b(LX/0QB;)Ljava/util/Locale;

    move-result-object v3

    check-cast v3, Ljava/util/Locale;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v4

    check-cast v4, Landroid/content/res/Resources;

    invoke-static {v0}, LX/1eu;->a(LX/0QB;)LX/1eu;

    move-result-object v5

    check-cast v5, LX/1eu;

    invoke-direct {p0, v3, v4, v5}, LX/BNF;-><init>(Ljava/util/Locale;Landroid/content/res/Resources;LX/1eu;)V

    .line 1778439
    move-object v0, p0

    .line 1778440
    sput-object v0, LX/BNF;->d:LX/BNF;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1778441
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1778442
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1778443
    :cond_1
    sget-object v0, LX/BNF;->d:LX/BNF;

    return-object v0

    .line 1778444
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1778445
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(D)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 1778446
    iget-object v0, p0, LX/BNF;->a:Ljava/util/Locale;

    invoke-static {v0}, Ljava/text/NumberFormat;->getInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v0

    .line 1778447
    invoke-virtual {v0, v1}, Ljava/text/NumberFormat;->setMinimumFractionDigits(I)V

    .line 1778448
    invoke-virtual {v0, v1}, Ljava/text/NumberFormat;->setMaximumFractionDigits(I)V

    .line 1778449
    invoke-virtual {v0, p1, p2}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
