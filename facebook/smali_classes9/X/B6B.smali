.class public LX/B6B;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ">",
        "Ljava/lang/Object;",
        "Landroid/content/DialogInterface$OnDismissListener;",
        "Landroid/view/View$OnClickListener;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<",
            "LX/B6B;",
            ">;"
        }
    .end annotation
.end field

.field private static b:Z


# instance fields
.field private final c:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

.field private final e:Landroid/content/Context;

.field private final f:LX/B6l;

.field private final g:LX/03V;

.field private final h:LX/2sb;

.field private final i:LX/1Pq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field private final j:LX/1CK;

.field private final k:LX/B8t;

.field private final l:LX/17W;

.field private final m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1746312
    const-class v0, LX/B6B;

    sput-object v0, LX/B6B;->a:Ljava/lang/Class;

    .line 1746313
    const/4 v0, 0x0

    sput-boolean v0, LX/B6B;->b:Z

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/content/Context;LX/1Pq;LX/B6l;LX/03V;LX/2sb;LX/1CK;LX/B8t;LX/17W;LX/0SI;)V
    .locals 1
    .param p1    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/1Pq;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "Landroid/content/Context;",
            "TE;",
            "LX/B6l;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/2sb;",
            "LX/1CK;",
            "LX/B8t;",
            "LX/17W;",
            "LX/0SI;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1746314
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1746315
    iput-object p1, p0, LX/B6B;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1746316
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1746317
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    iput-object v0, p0, LX/B6B;->d:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1746318
    iput-object p2, p0, LX/B6B;->e:Landroid/content/Context;

    .line 1746319
    iput-object p3, p0, LX/B6B;->i:LX/1Pq;

    .line 1746320
    iput-object p4, p0, LX/B6B;->f:LX/B6l;

    .line 1746321
    iput-object p5, p0, LX/B6B;->g:LX/03V;

    .line 1746322
    iput-object p6, p0, LX/B6B;->h:LX/2sb;

    .line 1746323
    iput-object p7, p0, LX/B6B;->j:LX/1CK;

    .line 1746324
    iput-object p8, p0, LX/B6B;->k:LX/B8t;

    .line 1746325
    iput-object p9, p0, LX/B6B;->l:LX/17W;

    .line 1746326
    invoke-interface {p10}, LX/0SI;->a()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v0

    .line 1746327
    if-eqz v0, :cond_0

    .line 1746328
    iget-object p1, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v0, p1

    .line 1746329
    :goto_0
    iput-object v0, p0, LX/B6B;->m:Ljava/lang/String;

    .line 1746330
    return-void

    .line 1746331
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a()Z
    .locals 3

    .prologue
    .line 1746332
    iget-object v0, p0, LX/B6B;->h:LX/2sb;

    invoke-virtual {v0}, LX/2sb;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/B6B;->h:LX/2sb;

    invoke-virtual {v0}, LX/2sb;->c()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/B6B;->h:LX/2sb;

    .line 1746333
    iget-object v1, v0, LX/2sb;->a:LX/0ad;

    sget-short v2, LX/B78;->a:S

    const/4 p0, 0x0

    invoke-interface {v1, v2, p0}, LX/0ad;->a(SZ)Z

    move-result v1

    move v0, v1

    .line 1746334
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 13

    .prologue
    const/4 v12, 0x2

    const/4 v11, 0x1

    const v0, -0x6078a0fb

    invoke-static {v12, v11, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v4

    .line 1746335
    sget-boolean v0, LX/B6B;->b:Z

    if-eqz v0, :cond_0

    .line 1746336
    const v0, 0x7780b199

    invoke-static {v12, v12, v0, v4}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1746337
    :goto_0
    return-void

    .line 1746338
    :cond_0
    iget-object v0, p0, LX/B6B;->d:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    const v1, 0x46a1c4a4

    invoke-static {v0, v1}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v5

    .line 1746339
    if-nez v5, :cond_1

    .line 1746340
    const v0, 0x399d5e20

    invoke-static {v0, v4}, LX/02F;->a(II)V

    goto :goto_0

    .line 1746341
    :cond_1
    iget-object v0, p0, LX/B6B;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 1746342
    if-nez v1, :cond_2

    .line 1746343
    iget-object v0, p0, LX/B6B;->g:LX/03V;

    sget-object v1, LX/B6B;->a:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Parent Story is null"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1746344
    const v0, -0x23ae2038

    invoke-static {v0, v4}, LX/02F;->a(II)V

    goto :goto_0

    .line 1746345
    :cond_2
    iget-object v0, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1746346
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1746347
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->Z()Lcom/facebook/graphql/model/GraphQLLeadGenDeepLinkUserStatus;

    move-result-object v6

    .line 1746348
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->l()Ljava/lang/String;

    move-result-object v7

    .line 1746349
    iget-object v2, p0, LX/B6B;->k:LX/B8t;

    invoke-virtual {v2, v7}, LX/B8t;->a(Ljava/lang/String;)Z

    move-result v8

    .line 1746350
    invoke-static {v1}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v9

    .line 1746351
    iget-object v1, p0, LX/B6B;->e:Landroid/content/Context;

    if-eqz v1, :cond_5

    iget-object v1, p0, LX/B6B;->e:Landroid/content/Context;

    move-object v3, v1

    .line 1746352
    :goto_1
    iget-object v1, p0, LX/B6B;->j:LX/1CK;

    iget-object v2, p0, LX/B6B;->i:LX/1Pq;

    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v1, v0, v2, v10}, LX/1CK;->a(Lcom/facebook/graphql/model/FeedUnit;LX/1Pq;Ljava/lang/Boolean;)V

    .line 1746353
    const-class v1, LX/0ew;

    invoke-static {v3, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0ew;

    .line 1746354
    const-class v2, Landroid/app/Activity;

    invoke-static {v3, v2}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/Activity;

    .line 1746355
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1746356
    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1746357
    if-eqz v6, :cond_3

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLLeadGenDeepLinkUserStatus;->j()Z

    move-result v6

    if-nez v6, :cond_4

    :cond_3
    if-eqz v8, :cond_6

    .line 1746358
    :cond_4
    iget-object v0, p0, LX/B6B;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    sget-object v5, LX/B76;->SUCCESS:LX/B76;

    invoke-static {v0, v5}, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/B76;)Lcom/facebook/leadgen/LeadGenConfirmationFragment;

    move-result-object v0

    .line 1746359
    invoke-interface {v1}, LX/0ew;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v2}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-static {v3}, LX/18w;->a(Landroid/content/Context;)Landroid/view/View;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/leadgen/popover/MultiPagePopoverFragment;->a(LX/B6H;LX/0gc;Landroid/view/Window;Landroid/view/View;)Lcom/facebook/leadgen/popover/MultiPagePopoverFragment;

    move-result-object v0

    .line 1746360
    invoke-virtual {v0, p0}, Lcom/facebook/leadgen/popover/MultiPagePopoverFragment;->a(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 1746361
    const v0, 0x16ea57da

    invoke-static {v0, v4}, LX/02F;->a(II)V

    goto/16 :goto_0

    .line 1746362
    :cond_5
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    move-object v3, v1

    goto :goto_1

    .line 1746363
    :cond_6
    invoke-direct {p0}, LX/B6B;->a()Z

    move-result v6

    if-eqz v6, :cond_7

    .line 1746364
    iget-object v1, p0, LX/B6B;->l:LX/17W;

    sget-object v3, LX/0ax;->ht:Ljava/lang/String;

    const/4 v6, 0x6

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->Y()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v6, v8

    aput-object v7, v6, v11

    const-string v5, "facebook"

    aput-object v5, v6, v12

    const/4 v5, 0x3

    aput-object v7, v6, v5

    const/4 v5, 0x4

    iget-object v7, p0, LX/B6B;->m:Ljava/lang/String;

    aput-object v7, v6, v5

    const/4 v5, 0x5

    aput-object v9, v6, v5

    invoke-static {v3, v6}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 1746365
    :goto_2
    iget-object v1, p0, LX/B6B;->f:LX/B6l;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->v()Z

    move-result v0

    iget-object v2, p0, LX/B6B;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v2}, LX/2sb;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)I

    move-result v2

    invoke-virtual {v1, v9, v0, v2}, LX/B6l;->a(LX/0lF;ZI)V

    .line 1746366
    iget-object v0, p0, LX/B6B;->f:LX/B6l;

    const-string v1, "cta_lead_gen_open_popover"

    invoke-virtual {v0, v1}, LX/B6l;->a(Ljava/lang/String;)V

    .line 1746367
    const v0, -0x3b32f9f

    invoke-static {v0, v4}, LX/02F;->a(II)V

    goto/16 :goto_0

    .line 1746368
    :cond_7
    iget-object v5, p0, LX/B6B;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v5}, Lcom/facebook/leadgen/LeadGenUserInputFormFragment;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/leadgen/LeadGenUserInputFormFragment;

    move-result-object v5

    .line 1746369
    invoke-interface {v1}, LX/0ew;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v2}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-static {v3}, LX/18w;->a(Landroid/content/Context;)Landroid/view/View;

    move-result-object v3

    invoke-static {v5, v1, v2, v3}, Lcom/facebook/leadgen/popover/MultiPagePopoverFragment;->a(LX/B6H;LX/0gc;Landroid/view/Window;Landroid/view/View;)Lcom/facebook/leadgen/popover/MultiPagePopoverFragment;

    move-result-object v1

    .line 1746370
    invoke-virtual {v1, p0}, Lcom/facebook/leadgen/popover/MultiPagePopoverFragment;->a(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 1746371
    sput-boolean v11, LX/B6B;->b:Z

    goto :goto_2
.end method

.method public final onDismiss(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 1746372
    const/4 v0, 0x0

    sput-boolean v0, LX/B6B;->b:Z

    .line 1746373
    return-void
.end method
