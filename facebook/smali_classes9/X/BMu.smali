.class public LX/BMu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/BMq;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/facebook/ipc/model/FacebookProfile;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile f:LX/BMu;


# instance fields
.field private final b:LX/0TD;

.field public final c:LX/18V;

.field public final d:LX/8os;

.field public e:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1778070
    new-instance v0, LX/BMs;

    invoke-direct {v0}, LX/BMs;-><init>()V

    sput-object v0, LX/BMu;->a:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>(LX/0TD;LX/18V;LX/8os;)V
    .locals 2
    .param p1    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1778092
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1778093
    iput-object p1, p0, LX/BMu;->b:LX/0TD;

    .line 1778094
    iput-object p2, p0, LX/BMu;->c:LX/18V;

    .line 1778095
    iput-object p3, p0, LX/BMu;->d:LX/8os;

    .line 1778096
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/BMu;->e:J

    .line 1778097
    return-void
.end method

.method public static a(LX/0QB;)LX/BMu;
    .locals 6

    .prologue
    .line 1778075
    sget-object v0, LX/BMu;->f:LX/BMu;

    if-nez v0, :cond_1

    .line 1778076
    const-class v1, LX/BMu;

    monitor-enter v1

    .line 1778077
    :try_start_0
    sget-object v0, LX/BMu;->f:LX/BMu;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1778078
    if-eqz v2, :cond_0

    .line 1778079
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1778080
    new-instance p0, LX/BMu;

    invoke-static {v0}, LX/0Vo;->a(LX/0QB;)LX/0TD;

    move-result-object v3

    check-cast v3, LX/0TD;

    invoke-static {v0}, LX/18V;->a(LX/0QB;)LX/18V;

    move-result-object v4

    check-cast v4, LX/18V;

    .line 1778081
    new-instance v5, LX/8os;

    invoke-direct {v5}, LX/8os;-><init>()V

    .line 1778082
    move-object v5, v5

    .line 1778083
    move-object v5, v5

    .line 1778084
    check-cast v5, LX/8os;

    invoke-direct {p0, v3, v4, v5}, LX/BMu;-><init>(LX/0TD;LX/18V;LX/8os;)V

    .line 1778085
    move-object v0, p0

    .line 1778086
    sput-object v0, LX/BMu;->f:LX/BMu;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1778087
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1778088
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1778089
    :cond_1
    sget-object v0, LX/BMu;->f:LX/BMu;

    return-object v0

    .line 1778090
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1778091
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/0am;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/ipc/model/FacebookProfile;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1778072
    invoke-virtual {p1}, LX/0am;->isPresent()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1778073
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1778074
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/BMu;->b:LX/0TD;

    new-instance v1, LX/BMt;

    invoke-direct {v1, p0, p1}, LX/BMt;-><init>(LX/BMu;LX/0am;)V

    invoke-interface {v0, v1}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1778071
    const/4 v0, 0x0

    return v0
.end method
