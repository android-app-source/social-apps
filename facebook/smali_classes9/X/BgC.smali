.class public final LX/BgC;
.super LX/63W;
.source ""


# instance fields
.field public final synthetic a:LX/BgK;


# direct methods
.method public constructor <init>(LX/BgK;)V
    .locals 0

    .prologue
    .line 1807150
    iput-object p1, p0, LX/BgC;->a:LX/BgK;

    invoke-direct {p0}, LX/63W;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V
    .locals 11

    .prologue
    .line 1807151
    iget-object v0, p0, LX/BgC;->a:LX/BgK;

    .line 1807152
    iget-object v1, v0, LX/BgK;->x:LX/BgW;

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/BgK;->y:LX/0Px;

    if-nez v1, :cond_1

    .line 1807153
    :cond_0
    :goto_0
    return-void

    .line 1807154
    :cond_1
    iget-object v1, v0, LX/BgK;->d:Landroid/content/Context;

    iget-object v2, v0, LX/BgK;->d:Landroid/content/Context;

    const v3, 0x7f082913

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, v0, LX/BgK;->d:Landroid/content/Context;

    const v4, 0x7f082914

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-static {v1, v2, v3, v4, v5}, LX/4BY;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;ZZ)LX/4BY;

    move-result-object v1

    .line 1807155
    iget-object v2, v0, LX/BgK;->x:LX/BgW;

    .line 1807156
    iget-object v3, v2, LX/BgW;->f:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->hasFocus()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1807157
    iget-object v3, v2, LX/BgW;->d:LX/BhM;

    iget-object v4, v2, LX/BgW;->f:Landroid/view/View;

    sget-object v5, LX/BeE;->PAGE_HEADER:LX/BeE;

    invoke-virtual {v3, v4, v2, v5, v2}, LX/BhM;->a(Landroid/view/View;Ljava/lang/Object;LX/BeE;LX/BgS;)V

    .line 1807158
    :cond_2
    iget-object v2, v0, LX/BgK;->y:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v4

    const/4 v2, 0x0

    move v3, v2

    :goto_1
    if-ge v3, v4, :cond_4

    iget-object v2, v0, LX/BgK;->y:LX/0Px;

    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/BgT;

    .line 1807159
    iget-object v5, v2, LX/BgT;->f:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->hasFocus()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1807160
    iget-object v5, v2, LX/BgT;->a:LX/BhM;

    iget-object v6, v2, LX/BgT;->f:Landroid/view/View;

    iget-object v7, v2, LX/BgT;->i:LX/97f;

    iget-object v8, v2, LX/BgT;->g:LX/BeE;

    invoke-virtual {v5, v6, v7, v8, v2}, LX/BhM;->a(Landroid/view/View;Ljava/lang/Object;LX/BeE;LX/BgS;)V

    .line 1807161
    :cond_3
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    .line 1807162
    :cond_4
    iget-object v2, v0, LX/BgK;->x:LX/BgW;

    invoke-virtual {v2}, LX/BgW;->e()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1807163
    iget-object v2, v0, LX/BgK;->p:Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;

    iget-object v3, v0, LX/BgK;->t:Lcom/facebook/crowdsourcing/suggestedits/fragment/SuggestEditsFragment;

    invoke-virtual {v3}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, v0, LX/BgK;->x:LX/BgW;

    invoke-virtual {v4}, LX/BgW;->c()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;

    move-result-object v4

    iget-object v5, v0, LX/BgK;->r:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    .line 1807164
    new-instance v7, Landroid/content/Intent;

    const-class v8, Lcom/facebook/places/suggestions/common/SuggestProfilePicUploadService;

    invoke-direct {v7, v3, v8}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v8, "page_id"

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v7

    const-string v8, "photo_item"

    .line 1807165
    invoke-static {v4}, LX/Bgb;->d(Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;)Ljava/lang/String;

    move-result-object v9

    .line 1807166
    if-nez v9, :cond_1b

    .line 1807167
    const/4 v9, 0x0

    .line 1807168
    :goto_2
    move-object v9, v9

    .line 1807169
    invoke-virtual {v7, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v7

    const-string v8, "source"

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v7

    const-string v8, "entry_point"

    iget-object v9, v2, Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;->a:Ljava/lang/String;

    invoke-virtual {v7, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v7

    const-string v8, "endpoint"

    iget-object v9, v2, Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;->b:Ljava/lang/String;

    invoke-virtual {v7, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v7

    .line 1807170
    invoke-virtual {v3, v7}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 1807171
    :cond_5
    iget-object v2, v0, LX/BgK;->p:Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;

    iget-object v3, v0, LX/BgK;->r:Ljava/lang/String;

    iget-object v4, v0, LX/BgK;->x:LX/BgW;

    iget-object v5, v0, LX/BgK;->y:LX/0Px;

    .line 1807172
    invoke-static {v3}, LX/CdW;->a(Ljava/lang/String;)LX/CdV;

    move-result-object v6

    .line 1807173
    iget-object v7, v2, Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;->b:Ljava/lang/String;

    .line 1807174
    iput-object v7, v6, LX/CdV;->E:Ljava/lang/String;

    .line 1807175
    iget-object v7, v2, Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;->a:Ljava/lang/String;

    .line 1807176
    iput-object v7, v6, LX/CdV;->D:Ljava/lang/String;

    .line 1807177
    invoke-virtual {v4}, LX/BgW;->c()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;

    move-result-object v7

    invoke-static {v7}, LX/Bgb;->c(Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;)Ljava/lang/String;

    move-result-object v7

    .line 1807178
    invoke-static {v7}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 1807179
    invoke-static {v7}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iput-object v2, v6, LX/CdV;->b:Ljava/lang/String;

    .line 1807180
    :cond_6
    iget-object v7, v4, LX/BgW;->e:Lcom/facebook/crowdsourcing/suggestedits/data/SuggestEditsHeaderState;

    .line 1807181
    iget-object v4, v7, Lcom/facebook/crowdsourcing/suggestedits/data/SuggestEditsHeaderState;->b:Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;

    move-object v7, v4

    .line 1807182
    move-object v7, v7

    .line 1807183
    invoke-static {v7}, LX/Bgb;->c(Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;)Ljava/lang/String;

    move-result-object v7

    .line 1807184
    invoke-static {v7}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 1807185
    invoke-static {v7}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iput-object v2, v6, LX/CdV;->p:Ljava/lang/String;

    .line 1807186
    :cond_7
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v9

    const/4 v7, 0x0

    move v8, v7

    :goto_3
    if-ge v8, v9, :cond_1a

    invoke-virtual {v5, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/BgT;

    .line 1807187
    iget-object v10, v7, LX/BgT;->e:LX/97f;

    move-object v10, v10

    .line 1807188
    iget-object v2, v7, LX/BgT;->i:LX/97f;

    move-object v7, v2

    .line 1807189
    if-eq v7, v10, :cond_e

    .line 1807190
    invoke-interface {v7}, LX/97e;->c()Ljava/lang/String;

    move-result-object v2

    .line 1807191
    invoke-static {v10}, LX/Bgb;->k(LX/97f;)Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    .line 1807192
    invoke-static {v7}, LX/Bgb;->k(LX/97f;)Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    move-result-object v3

    .line 1807193
    const-string v4, "337046403064701"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_10

    .line 1807194
    invoke-static {v10}, LX/Bgb;->f(LX/97f;)Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;

    move-result-object v10

    .line 1807195
    invoke-static {v7}, LX/Bgb;->f(LX/97f;)Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;

    move-result-object v2

    .line 1807196
    const/4 v7, 0x0

    .line 1807197
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;->DOESNT_HAVE_VALUE:Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    if-ne v3, v4, :cond_f

    .line 1807198
    const-string v7, "<<not-applicable>>"

    .line 1807199
    :cond_8
    :goto_4
    invoke-static {v7}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_9

    .line 1807200
    invoke-static {v7}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1807201
    iput-object v7, v6, LX/CdV;->c:Ljava/lang/String;

    .line 1807202
    :cond_9
    if-eqz v2, :cond_b

    .line 1807203
    invoke-virtual {v2}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;->q()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_a

    .line 1807204
    invoke-virtual {v2}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;->q()Ljava/lang/String;

    move-result-object v7

    .line 1807205
    invoke-static {v7}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    iput-object v3, v6, LX/CdV;->l:Ljava/lang/String;

    .line 1807206
    :cond_a
    invoke-virtual {v2}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;->b()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedAddressModel$CityModel;

    move-result-object v7

    if-eqz v7, :cond_b

    invoke-virtual {v2}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;->b()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedAddressModel$CityModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedAddressModel$CityModel;->c()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_b

    .line 1807207
    invoke-virtual {v2}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;->b()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedAddressModel$CityModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedAddressModel$CityModel;->c()Ljava/lang/String;

    move-result-object v7

    .line 1807208
    invoke-static {v7}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iput-object v2, v6, LX/CdV;->e:Ljava/lang/String;

    .line 1807209
    :cond_b
    if-eqz v10, :cond_e

    .line 1807210
    invoke-virtual {v10}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;->k()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_c

    .line 1807211
    invoke-virtual {v10}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;->k()Ljava/lang/String;

    move-result-object v7

    .line 1807212
    invoke-static {v7}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1807213
    iput-object v7, v6, LX/CdV;->q:Ljava/lang/String;

    .line 1807214
    :cond_c
    invoke-virtual {v10}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;->q()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_d

    .line 1807215
    invoke-virtual {v10}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;->q()Ljava/lang/String;

    move-result-object v7

    .line 1807216
    invoke-static {v7}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iput-object v2, v6, LX/CdV;->z:Ljava/lang/String;

    .line 1807217
    :cond_d
    invoke-virtual {v10}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;->b()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedAddressModel$CityModel;

    move-result-object v7

    if-eqz v7, :cond_e

    invoke-virtual {v10}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;->b()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedAddressModel$CityModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedAddressModel$CityModel;->d()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_e

    .line 1807218
    invoke-virtual {v10}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;->b()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedAddressModel$CityModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedAddressModel$CityModel;->c()Ljava/lang/String;

    move-result-object v7

    .line 1807219
    invoke-static {v7}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    iput-object v10, v6, LX/CdV;->s:Ljava/lang/String;

    .line 1807220
    :cond_e
    :goto_5
    add-int/lit8 v7, v8, 0x1

    move v8, v7

    goto/16 :goto_3

    .line 1807221
    :cond_f
    if-eqz v2, :cond_8

    .line 1807222
    invoke-virtual {v2}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;->k()Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_4

    .line 1807223
    :cond_10
    const-string v3, "137075966484179"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_12

    .line 1807224
    invoke-static {v7}, LX/Bgb;->b(LX/97f;)Ljava/lang/String;

    move-result-object v7

    .line 1807225
    invoke-static {v7}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_11

    .line 1807226
    invoke-static {v7}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iput-object v2, v6, LX/CdV;->i:Ljava/lang/String;

    .line 1807227
    :cond_11
    invoke-static {v10}, LX/Bgb;->b(LX/97f;)Ljava/lang/String;

    move-result-object v7

    .line 1807228
    invoke-static {v7}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_e

    .line 1807229
    invoke-static {v7}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    iput-object v10, v6, LX/CdV;->w:Ljava/lang/String;

    .line 1807230
    goto :goto_5

    .line 1807231
    :cond_12
    const-string v3, "463427363734722"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_14

    .line 1807232
    invoke-static {v7}, LX/Bgb;->b(LX/97f;)Ljava/lang/String;

    move-result-object v7

    .line 1807233
    invoke-static {v7}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_13

    .line 1807234
    invoke-static {v7}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iput-object v2, v6, LX/CdV;->f:Ljava/lang/String;

    .line 1807235
    :cond_13
    invoke-static {v10}, LX/Bgb;->b(LX/97f;)Ljava/lang/String;

    move-result-object v7

    .line 1807236
    invoke-static {v7}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_e

    .line 1807237
    invoke-static {v7}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    iput-object v10, v6, LX/CdV;->t:Ljava/lang/String;

    .line 1807238
    goto :goto_5

    .line 1807239
    :cond_14
    const-string v3, "376081639179091"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_15

    .line 1807240
    invoke-static {v7}, LX/Bgb;->c(LX/97f;)LX/0Px;

    move-result-object v7

    .line 1807241
    invoke-static {v7}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Px;

    iput-object v2, v6, LX/CdV;->k:LX/0Px;

    .line 1807242
    invoke-static {v10}, LX/Bgb;->c(LX/97f;)LX/0Px;

    move-result-object v7

    .line 1807243
    invoke-static {v7}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, LX/0Px;

    iput-object v10, v6, LX/CdV;->y:LX/0Px;

    .line 1807244
    goto/16 :goto_5

    .line 1807245
    :cond_15
    const-string v3, "333522400104087"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_16

    .line 1807246
    invoke-static {v7}, LX/Bge;->c(LX/97f;)LX/0Px;

    move-result-object v7

    .line 1807247
    invoke-static {v7}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Px;

    iput-object v2, v6, LX/CdV;->n:LX/0Px;

    .line 1807248
    invoke-static {v10}, LX/Bge;->c(LX/97f;)LX/0Px;

    move-result-object v7

    .line 1807249
    invoke-static {v7}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, LX/0Px;

    iput-object v10, v6, LX/CdV;->C:LX/0Px;

    .line 1807250
    goto/16 :goto_5

    .line 1807251
    :cond_16
    const-string v3, "114481832091120"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 1807252
    invoke-static {v7}, LX/Bgb;->f(LX/97f;)Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;

    move-result-object v2

    .line 1807253
    if-eqz v2, :cond_17

    .line 1807254
    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/97c;

    iput-object v3, v6, LX/CdV;->g:LX/97c;

    .line 1807255
    :cond_17
    invoke-static {v7}, LX/Bge;->b(LX/97f;)Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;

    move-result-object v7

    .line 1807256
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;

    invoke-virtual {v7, v2}, Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_18

    .line 1807257
    invoke-static {v7}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;

    iput-object v2, v6, LX/CdV;->h:Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;

    .line 1807258
    :cond_18
    invoke-static {v10}, LX/Bgb;->f(LX/97f;)Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;

    move-result-object v7

    .line 1807259
    if-eqz v7, :cond_19

    .line 1807260
    invoke-static {v7}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/97c;

    iput-object v2, v6, LX/CdV;->u:LX/97c;

    .line 1807261
    :cond_19
    invoke-static {v10}, LX/Bge;->b(LX/97f;)Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;

    move-result-object v7

    .line 1807262
    sget-object v10, Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;

    invoke-virtual {v7, v10}, Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_e

    .line 1807263
    invoke-static {v7}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;

    iput-object v10, v6, LX/CdV;->v:Lcom/facebook/graphql/enums/GraphQLPlaceHoursType;

    .line 1807264
    goto/16 :goto_5

    .line 1807265
    :cond_1a
    invoke-virtual {v6}, LX/CdV;->a()LX/CdW;

    move-result-object v6

    move-object v2, v6

    .line 1807266
    iget-object v3, v0, LX/BgK;->k:LX/CdY;

    new-instance v4, LX/BgD;

    invoke-direct {v4, v0, v1}, LX/BgD;-><init>(LX/BgK;LX/4BY;)V

    invoke-virtual {v3, v2, v4}, LX/CdY;->a(LX/CdW;LX/0TF;)V

    goto/16 :goto_0

    .line 1807267
    :cond_1b
    const-string v10, "file://"

    invoke-virtual {v9, v10}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v10

    if-nez v10, :cond_1c

    .line 1807268
    const/4 v10, 0x7

    invoke-virtual {v9, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v9

    .line 1807269
    :cond_1c
    new-instance v10, LX/74k;

    invoke-direct {v10}, LX/74k;-><init>()V

    invoke-virtual {v10, v9}, LX/74k;->a(Ljava/lang/String;)LX/74k;

    move-result-object v9

    const-string v10, "image/jpeg"

    invoke-virtual {v9, v10}, LX/74k;->c(Ljava/lang/String;)LX/74k;

    move-result-object v9

    invoke-virtual {v9}, LX/74k;->a()Lcom/facebook/photos/base/media/PhotoItem;

    move-result-object v9

    goto/16 :goto_2
.end method
