.class public LX/Bx8;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pm;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Bx9;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Bx8",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/Bx9;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1834989
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1834990
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/Bx8;->b:LX/0Zi;

    .line 1834991
    iput-object p1, p0, LX/Bx8;->a:LX/0Ot;

    .line 1834992
    return-void
.end method

.method public static a(LX/0QB;)LX/Bx8;
    .locals 4

    .prologue
    .line 1834993
    const-class v1, LX/Bx8;

    monitor-enter v1

    .line 1834994
    :try_start_0
    sget-object v0, LX/Bx8;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1834995
    sput-object v2, LX/Bx8;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1834996
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1834997
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1834998
    new-instance v3, LX/Bx8;

    const/16 p0, 0x1e03

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/Bx8;-><init>(LX/0Ot;)V

    .line 1834999
    move-object v0, v3

    .line 1835000
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1835001
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Bx8;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1835002
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1835003
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 13

    .prologue
    .line 1835004
    check-cast p2, LX/Bx6;

    .line 1835005
    iget-object v0, p0, LX/Bx8;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Bx9;

    iget-object v1, p2, LX/Bx6;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/Bx6;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    iget-object v3, p2, LX/Bx6;->c:LX/1Pm;

    const/4 v6, 0x0

    const/4 v12, 0x6

    const/4 v11, 0x1

    const/high16 v10, 0x3f800000    # 1.0f

    const/16 v9, 0x8

    .line 1835006
    new-instance v5, LX/BxW;

    invoke-direct {v5, v2}, LX/BxW;-><init>(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    .line 1835007
    iget-object v4, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, v4

    .line 1835008
    check-cast v4, LX/0jW;

    invoke-interface {v3, v5, v4}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/BxV;

    .line 1835009
    invoke-static {v2, v4}, LX/BxV;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;LX/BxV;)V

    .line 1835010
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    const/4 v7, 0x2

    invoke-interface {v5, v7}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v5

    const/16 v7, 0x30

    invoke-interface {v5, v7}, LX/1Dh;->K(I)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, v11}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, v12, v9}, LX/1Dh;->r(II)LX/1Dh;

    move-result-object v5

    iget-object v7, v0, LX/Bx9;->a:LX/BxI;

    const/4 v8, 0x0

    .line 1835011
    new-instance p0, LX/BxG;

    invoke-direct {p0, v7}, LX/BxG;-><init>(LX/BxI;)V

    .line 1835012
    iget-object p2, v7, LX/BxI;->b:LX/0Zi;

    invoke-virtual {p2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/BxH;

    .line 1835013
    if-nez p2, :cond_0

    .line 1835014
    new-instance p2, LX/BxH;

    invoke-direct {p2, v7}, LX/BxH;-><init>(LX/BxI;)V

    .line 1835015
    :cond_0
    invoke-static {p2, p1, v8, v8, p0}, LX/BxH;->a$redex0(LX/BxH;LX/1De;IILX/BxG;)V

    .line 1835016
    move-object p0, p2

    .line 1835017
    move-object v8, p0

    .line 1835018
    move-object v7, v8

    .line 1835019
    iget-object v8, v7, LX/BxH;->a:LX/BxG;

    iput-object v2, v8, LX/BxG;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1835020
    iget-object v8, v7, LX/BxH;->e:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v8, p0}, Ljava/util/BitSet;->set(I)V

    .line 1835021
    move-object v7, v7

    .line 1835022
    iget-object v8, v7, LX/BxH;->a:LX/BxG;

    iput-object v3, v8, LX/BxG;->b:LX/1Pm;

    .line 1835023
    iget-object v8, v7, LX/BxH;->e:Ljava/util/BitSet;

    const/4 p0, 0x1

    invoke-virtual {v8, p0}, Ljava/util/BitSet;->set(I)V

    .line 1835024
    move-object v7, v7

    .line 1835025
    invoke-virtual {v7}, LX/1X5;->c()LX/1Di;

    move-result-object v7

    invoke-interface {v7, v9, v9}, LX/1Di;->h(II)LX/1Di;

    move-result-object v7

    invoke-interface {v7, v10}, LX/1Di;->a(F)LX/1Di;

    move-result-object v7

    invoke-interface {v5, v7}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v7

    .line 1835026
    iget-boolean v5, v4, LX/BxV;->b:Z

    move v5, v5

    .line 1835027
    if-nez v5, :cond_1

    move-object v5, v6

    :goto_0
    invoke-interface {v7, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v5

    .line 1835028
    iget-boolean v7, v4, LX/BxV;->b:Z

    move v4, v7

    .line 1835029
    if-nez v4, :cond_2

    :goto_1
    invoke-interface {v5, v6}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    move-object v0, v4

    .line 1835030
    return-object v0

    :cond_1
    invoke-static {p1}, LX/25N;->c(LX/1De;)LX/25Q;

    move-result-object v5

    const v8, 0x7f0a0160

    invoke-virtual {v5, v8}, LX/25Q;->i(I)LX/25Q;

    move-result-object v5

    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    invoke-interface {v5, v11}, LX/1Di;->j(I)LX/1Di;

    move-result-object v5

    const/4 v8, 0x7

    invoke-interface {v5, v8, v12}, LX/1Di;->d(II)LX/1Di;

    move-result-object v5

    goto :goto_0

    :cond_2
    iget-object v4, v0, LX/Bx9;->b:LX/BxC;

    const/4 v6, 0x0

    .line 1835031
    new-instance v7, LX/BxA;

    invoke-direct {v7, v4}, LX/BxA;-><init>(LX/BxC;)V

    .line 1835032
    iget-object v8, v4, LX/BxC;->b:LX/0Zi;

    invoke-virtual {v8}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/BxB;

    .line 1835033
    if-nez v8, :cond_3

    .line 1835034
    new-instance v8, LX/BxB;

    invoke-direct {v8, v4}, LX/BxB;-><init>(LX/BxC;)V

    .line 1835035
    :cond_3
    invoke-static {v8, p1, v6, v6, v7}, LX/BxB;->a$redex0(LX/BxB;LX/1De;IILX/BxA;)V

    .line 1835036
    move-object v7, v8

    .line 1835037
    move-object v6, v7

    .line 1835038
    move-object v4, v6

    .line 1835039
    iget-object v6, v4, LX/BxB;->a:LX/BxA;

    iput-object v1, v6, LX/BxA;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1835040
    iget-object v6, v4, LX/BxB;->e:Ljava/util/BitSet;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Ljava/util/BitSet;->set(I)V

    .line 1835041
    move-object v4, v4

    .line 1835042
    iget-object v6, v4, LX/BxB;->a:LX/BxA;

    iput-object v2, v6, LX/BxA;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1835043
    iget-object v6, v4, LX/BxB;->e:Ljava/util/BitSet;

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Ljava/util/BitSet;->set(I)V

    .line 1835044
    move-object v4, v4

    .line 1835045
    iget-object v6, v4, LX/BxB;->a:LX/BxA;

    iput-object v3, v6, LX/BxA;->c:LX/1Pm;

    .line 1835046
    iget-object v6, v4, LX/BxB;->e:Ljava/util/BitSet;

    const/4 v7, 0x2

    invoke-virtual {v6, v7}, Ljava/util/BitSet;->set(I)V

    .line 1835047
    move-object v4, v4

    .line 1835048
    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    invoke-interface {v4, v9, v9}, LX/1Di;->h(II)LX/1Di;

    move-result-object v4

    invoke-interface {v4, v10}, LX/1Di;->a(F)LX/1Di;

    move-result-object v6

    goto :goto_1
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1835049
    invoke-static {}, LX/1dS;->b()V

    .line 1835050
    const/4 v0, 0x0

    return-object v0
.end method
