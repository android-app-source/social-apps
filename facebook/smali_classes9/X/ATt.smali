.class public LX/ATt;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/ATs;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1677329
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 1677330
    return-void
.end method


# virtual methods
.method public final a(LX/0il;LX/ATL;LX/0gc;Ljava/lang/String;)LX/ATs;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ModelData:",
            "Ljava/lang/Object;",
            ":",
            "LX/0io;",
            ":",
            "LX/0iv;",
            ":",
            "LX/0j3;",
            "Services::",
            "LX/0il",
            "<TModelData;>;>(TServices;",
            "Lcom/facebook/composer/ui/underwood/AttachmentsEventListener;",
            "LX/0gc;",
            "Ljava/lang/String;",
            ")",
            "LX/ATs",
            "<TModelData;TServices;>;"
        }
    .end annotation

    .prologue
    .line 1677327
    new-instance v1, LX/ATs;

    invoke-static/range {p0 .. p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v2

    check-cast v2, LX/0Zb;

    invoke-static/range {p0 .. p0}, LX/ASg;->a(LX/0QB;)LX/ASg;

    move-result-object v3

    check-cast v3, LX/ASg;

    invoke-static/range {p0 .. p0}, LX/10d;->a(LX/0QB;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v4

    check-cast v4, Landroid/view/inputmethod/InputMethodManager;

    invoke-static/range {p0 .. p0}, LX/BTG;->a(LX/0QB;)LX/BTG;

    move-result-object v5

    check-cast v5, LX/BTG;

    invoke-static/range {p0 .. p0}, LX/ATR;->a(LX/0QB;)LX/ATR;

    move-result-object v6

    check-cast v6, LX/ATR;

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v7

    check-cast v7, LX/0ad;

    const-class v8, Landroid/content/Context;

    move-object/from16 v0, p0

    invoke-interface {v0, v8}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/content/Context;

    const-class v9, LX/Azd;

    move-object/from16 v0, p0

    invoke-interface {v0, v9}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v9

    check-cast v9, LX/Azd;

    invoke-static/range {p0 .. p0}, LX/7l0;->a(LX/0QB;)LX/7l0;

    move-result-object v10

    check-cast v10, LX/7l0;

    invoke-static/range {p0 .. p0}, LX/BSu;->a(LX/0QB;)LX/BSu;

    move-result-object v11

    check-cast v11, LX/BSu;

    invoke-static/range {p0 .. p0}, LX/0Vo;->a(LX/0QB;)LX/0TD;

    move-result-object v12

    check-cast v12, Ljava/util/concurrent/ExecutorService;

    invoke-static/range {p0 .. p0}, LX/0Ss;->a(LX/0QB;)Landroid/os/Handler;

    move-result-object v13

    check-cast v13, Landroid/os/Handler;

    move-object/from16 v14, p1

    move-object/from16 v15, p2

    move-object/from16 v16, p3

    move-object/from16 v17, p4

    invoke-direct/range {v1 .. v17}, LX/ATs;-><init>(LX/0Zb;LX/ASg;Landroid/view/inputmethod/InputMethodManager;LX/BTG;LX/ATR;LX/0ad;Landroid/content/Context;LX/Azd;LX/7l0;LX/BSu;Ljava/util/concurrent/ExecutorService;Landroid/os/Handler;LX/0il;LX/ATL;LX/0gc;Ljava/lang/String;)V

    .line 1677328
    return-object v1
.end method
