.class public final LX/COs;
.super LX/1OX;
.source ""


# instance fields
.field private final a:LX/COu;


# direct methods
.method public constructor <init>(LX/COu;)V
    .locals 0

    .prologue
    .line 1883694
    invoke-direct {p0}, LX/1OX;-><init>()V

    .line 1883695
    iput-object p1, p0, LX/COs;->a:LX/COu;

    .line 1883696
    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v7/widget/RecyclerView;I)V
    .locals 0

    .prologue
    .line 1883697
    invoke-super {p0, p1, p2}, LX/1OX;->a(Landroid/support/v7/widget/RecyclerView;I)V

    .line 1883698
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()LX/1OR;

    move-result-object p0

    check-cast p0, LX/25T;

    .line 1883699
    invoke-virtual {p0}, LX/1P1;->l()I

    .line 1883700
    return-void
.end method

.method public final a(Landroid/support/v7/widget/RecyclerView;II)V
    .locals 3

    .prologue
    .line 1883701
    iget-object v0, p0, LX/COs;->a:LX/COu;

    .line 1883702
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()LX/1OR;

    move-result-object v1

    check-cast v1, LX/25T;

    .line 1883703
    invoke-virtual {v1}, LX/1P1;->l()I

    move-result v2

    .line 1883704
    invoke-virtual {v1}, LX/1P1;->n()I

    move-result p0

    .line 1883705
    invoke-virtual {v1, v2}, LX/1OR;->c(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/components/ComponentView;

    .line 1883706
    if-eqz v2, :cond_1

    .line 1883707
    invoke-virtual {v2}, Lcom/facebook/components/ComponentView;->k()Z

    move-result p2

    if-eqz p2, :cond_0

    .line 1883708
    invoke-virtual {v2}, Lcom/facebook/components/ComponentView;->j()V

    .line 1883709
    :cond_0
    iget-boolean p2, v0, LX/COu;->e:Z

    if-nez p2, :cond_1

    .line 1883710
    invoke-virtual {v2}, Lcom/facebook/components/ComponentView;->getWidth()I

    move-result p2

    move-object v2, p1

    .line 1883711
    check-cast v2, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result p3

    invoke-virtual {v2, p2, p3}, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->h(II)V

    .line 1883712
    const/4 v2, 0x1

    iput-boolean v2, v0, LX/COu;->e:Z

    .line 1883713
    :cond_1
    invoke-virtual {v1, p0}, LX/1OR;->c(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/components/ComponentView;

    .line 1883714
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/facebook/components/ComponentView;->k()Z

    move-result p0

    if-eqz p0, :cond_2

    .line 1883715
    invoke-virtual {v2}, Lcom/facebook/components/ComponentView;->j()V

    .line 1883716
    :cond_2
    invoke-virtual {v1}, LX/1P1;->m()I

    move-result v1

    .line 1883717
    const/4 v2, -0x1

    if-eq v1, v2, :cond_3

    iget v2, v0, LX/COu;->f:I

    if-eq v1, v2, :cond_3

    .line 1883718
    iput v1, v0, LX/COu;->f:I

    .line 1883719
    :cond_3
    return-void
.end method
