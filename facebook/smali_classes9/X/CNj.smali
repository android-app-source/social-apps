.class public LX/CNj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/CNe;


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/CNe;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/CNb;LX/CNc;)V
    .locals 4

    .prologue
    .line 1882033
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1882034
    const-string v0, "children"

    invoke-virtual {p1, v0}, LX/CNb;->a(Ljava/lang/String;)LX/0Px;

    move-result-object v2

    .line 1882035
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/CNj;->a:Ljava/util/List;

    .line 1882036
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 1882037
    iget-object v3, p0, LX/CNj;->a:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CNb;

    invoke-static {v0, p2}, LX/CNd;->a(LX/CNb;LX/CNc;)LX/CNe;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1882038
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1882039
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1882040
    iget-object v0, p0, LX/CNj;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CNe;

    .line 1882041
    invoke-interface {v0}, LX/CNe;->a()V

    goto :goto_0

    .line 1882042
    :cond_0
    return-void
.end method
