.class public final enum LX/AeN;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/AeN;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/AeN;

.field public static final enum LIVE_ANNOUNCEMENT_EVENT:LX/AeN;

.field public static final enum LIVE_COMMENT_EVENT:LX/AeN;

.field public static final enum LIVE_COMMERCIAL_BREAK_EVENT:LX/AeN;

.field public static final enum LIVE_DONATION_EVENT:LX/AeN;

.field public static final enum LIVE_INFO_EVENT:LX/AeN;

.field public static final enum LIVE_LIKE_EVENT:LX/AeN;

.field public static final enum LIVE_PIN_EVENT:LX/AeN;

.field public static final enum LIVE_TIP_JAR_EVENT:LX/AeN;

.field public static final enum LIVE_WATCH_EVENT:LX/AeN;

.field public static final enum LIVE_WATCH_LIKE_EVENT:LX/AeN;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1697340
    new-instance v0, LX/AeN;

    const-string v1, "LIVE_COMMENT_EVENT"

    invoke-direct {v0, v1, v3}, LX/AeN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AeN;->LIVE_COMMENT_EVENT:LX/AeN;

    .line 1697341
    new-instance v0, LX/AeN;

    const-string v1, "LIVE_WATCH_EVENT"

    invoke-direct {v0, v1, v4}, LX/AeN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AeN;->LIVE_WATCH_EVENT:LX/AeN;

    .line 1697342
    new-instance v0, LX/AeN;

    const-string v1, "LIVE_LIKE_EVENT"

    invoke-direct {v0, v1, v5}, LX/AeN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AeN;->LIVE_LIKE_EVENT:LX/AeN;

    .line 1697343
    new-instance v0, LX/AeN;

    const-string v1, "LIVE_INFO_EVENT"

    invoke-direct {v0, v1, v6}, LX/AeN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AeN;->LIVE_INFO_EVENT:LX/AeN;

    .line 1697344
    new-instance v0, LX/AeN;

    const-string v1, "LIVE_ANNOUNCEMENT_EVENT"

    invoke-direct {v0, v1, v7}, LX/AeN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AeN;->LIVE_ANNOUNCEMENT_EVENT:LX/AeN;

    .line 1697345
    new-instance v0, LX/AeN;

    const-string v1, "LIVE_WATCH_LIKE_EVENT"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/AeN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AeN;->LIVE_WATCH_LIKE_EVENT:LX/AeN;

    .line 1697346
    new-instance v0, LX/AeN;

    const-string v1, "LIVE_PIN_EVENT"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/AeN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AeN;->LIVE_PIN_EVENT:LX/AeN;

    .line 1697347
    new-instance v0, LX/AeN;

    const-string v1, "LIVE_DONATION_EVENT"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/AeN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AeN;->LIVE_DONATION_EVENT:LX/AeN;

    .line 1697348
    new-instance v0, LX/AeN;

    const-string v1, "LIVE_TIP_JAR_EVENT"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/AeN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AeN;->LIVE_TIP_JAR_EVENT:LX/AeN;

    .line 1697349
    new-instance v0, LX/AeN;

    const-string v1, "LIVE_COMMERCIAL_BREAK_EVENT"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/AeN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AeN;->LIVE_COMMERCIAL_BREAK_EVENT:LX/AeN;

    .line 1697350
    const/16 v0, 0xa

    new-array v0, v0, [LX/AeN;

    sget-object v1, LX/AeN;->LIVE_COMMENT_EVENT:LX/AeN;

    aput-object v1, v0, v3

    sget-object v1, LX/AeN;->LIVE_WATCH_EVENT:LX/AeN;

    aput-object v1, v0, v4

    sget-object v1, LX/AeN;->LIVE_LIKE_EVENT:LX/AeN;

    aput-object v1, v0, v5

    sget-object v1, LX/AeN;->LIVE_INFO_EVENT:LX/AeN;

    aput-object v1, v0, v6

    sget-object v1, LX/AeN;->LIVE_ANNOUNCEMENT_EVENT:LX/AeN;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/AeN;->LIVE_WATCH_LIKE_EVENT:LX/AeN;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/AeN;->LIVE_PIN_EVENT:LX/AeN;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/AeN;->LIVE_DONATION_EVENT:LX/AeN;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/AeN;->LIVE_TIP_JAR_EVENT:LX/AeN;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/AeN;->LIVE_COMMERCIAL_BREAK_EVENT:LX/AeN;

    aput-object v2, v0, v1

    sput-object v0, LX/AeN;->$VALUES:[LX/AeN;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1697351
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/AeN;
    .locals 1

    .prologue
    .line 1697352
    const-class v0, LX/AeN;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/AeN;

    return-object v0
.end method

.method public static values()[LX/AeN;
    .locals 1

    .prologue
    .line 1697353
    sget-object v0, LX/AeN;->$VALUES:[LX/AeN;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/AeN;

    return-object v0
.end method
