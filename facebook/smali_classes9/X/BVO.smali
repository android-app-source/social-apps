.class public LX/BVO;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1790864
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static e()Landroid/util/Pair;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "LX/BVJ;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1790865
    new-instance v9, Ljava/util/LinkedList;

    invoke-direct {v9}, Ljava/util/LinkedList;-><init>()V

    .line 1790866
    const-string v8, "{}"

    .line 1790867
    new-instance v10, LX/BV9;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "res:///"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const v1, 0x7f021141

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v11

    new-instance v0, LX/BVG;

    invoke-direct {v0}, LX/BVG;-><init>()V

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1, v2, v3, v4}, LX/BVG;->a(FFFF)LX/BVG;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1, v2, v3, v4}, LX/BVG;->b(FFFF)LX/BVG;

    move-result-object v0

    const/high16 v1, -0x40800000    # -1.0f

    const/high16 v2, -0x40800000    # -1.0f

    invoke-virtual {v0, v1, v2}, LX/BVG;->c(FF)LX/BVG;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1, v2}, LX/BVG;->d(FF)LX/BVG;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/BVG;->g(FF)LX/BVG;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/BVG;->h(FF)LX/BVG;

    move-result-object v0

    const/16 v1, 0x1388

    invoke-virtual {v0, v1}, LX/BVG;->c(I)LX/BVG;

    move-result-object v0

    const/16 v1, 0x1388

    invoke-virtual {v0, v1}, LX/BVG;->d(I)LX/BVG;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1, v2}, LX/BVG;->f(FF)LX/BVG;

    move-result-object v0

    const/high16 v1, 0x40000000    # 2.0f

    const/high16 v2, 0x40400000    # 3.0f

    invoke-virtual {v0, v1, v2}, LX/BVG;->i(FF)LX/BVG;

    move-result-object v0

    const v1, 0x3e4ccccd    # 0.2f

    invoke-virtual {v0, v1}, LX/BVG;->a(F)LX/BVG;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/BVG;->a(I)LX/BVG;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/BVG;->e(FF)LX/BVG;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, LX/BVG;->b(I)LX/BVG;

    move-result-object v0

    const/4 v1, 0x0

    .line 1790868
    iput v1, v0, LX/BVG;->L:F

    .line 1790869
    move-object v12, v0

    .line 1790870
    new-instance v0, LX/BVB;

    invoke-direct {v0}, LX/BVB;-><init>()V

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/BVB;->a(FF)LX/BVB;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/BVB;->b(FF)LX/BVB;

    move-result-object v13

    new-instance v0, LX/7SR;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "res:///"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const v2, 0x7f02113f

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x4

    const/4 v3, 0x2

    const/4 v4, 0x4

    const/4 v5, 0x1

    const/4 v6, 0x7

    invoke-direct/range {v0 .. v6}, LX/7SR;-><init>(Landroid/net/Uri;IIIII)V

    new-instance v1, LX/7SR;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "res:///"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const v3, 0x7f021140

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x4

    const/4 v4, 0x2

    const/4 v5, 0x4

    const/4 v6, 0x1

    const/16 v7, 0x8

    invoke-direct/range {v1 .. v7}, LX/7SR;-><init>(Landroid/net/Uri;IIIII)V

    move-object v2, v10

    move-object v3, v11

    move-object v4, v12

    move-object v5, v13

    move-object v6, v8

    move-object v7, v0

    move-object v8, v1

    invoke-direct/range {v2 .. v8}, LX/BV9;-><init>(Landroid/net/Uri;LX/BVG;LX/BVB;Ljava/lang/String;LX/7SR;LX/7SR;)V

    invoke-interface {v9, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1790871
    new-instance v0, Landroid/util/Pair;

    const-string v1, "Animation"

    new-instance v2, LX/BVJ;

    new-instance v3, LX/BV8;

    invoke-direct {v3}, LX/BV8;-><init>()V

    const/4 v4, 0x0

    .line 1790872
    iput-object v4, v3, LX/BV8;->a:Ljava/lang/String;

    .line 1790873
    move-object v3, v3

    .line 1790874
    iput-object v9, v3, LX/BV8;->b:Ljava/util/List;

    .line 1790875
    move-object v3, v3

    .line 1790876
    const/4 v4, 0x0

    .line 1790877
    iput-object v4, v3, LX/BV8;->c:LX/BVN;

    .line 1790878
    move-object v3, v3

    .line 1790879
    invoke-virtual {v3}, LX/BV8;->a()LX/BVA;

    move-result-object v3

    invoke-direct {v2, v3}, LX/BVJ;-><init>(LX/BVA;)V

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0
.end method
