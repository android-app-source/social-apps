.class public final synthetic LX/AZ5;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final synthetic a:[I

.field public static final synthetic b:[I


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1686494
    invoke-static {}, LX/AZ8;->values()[LX/AZ8;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/AZ5;->b:[I

    :try_start_0
    sget-object v0, LX/AZ5;->b:[I

    sget-object v1, LX/AZ8;->NONE:LX/AZ8;

    invoke-virtual {v1}, LX/AZ8;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_6

    :goto_0
    :try_start_1
    sget-object v0, LX/AZ5;->b:[I

    sget-object v1, LX/AZ8;->BEGIN_TRANSITION:LX/AZ8;

    invoke-virtual {v1}, LX/AZ8;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_5

    :goto_1
    :try_start_2
    sget-object v0, LX/AZ5;->b:[I

    sget-object v1, LX/AZ8;->COUNTDOWN:LX/AZ8;

    invoke-virtual {v1}, LX/AZ8;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_4

    :goto_2
    :try_start_3
    sget-object v0, LX/AZ5;->b:[I

    sget-object v1, LX/AZ8;->END_TRANSITION:LX/AZ8;

    invoke-virtual {v1}, LX/AZ8;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3

    .line 1686495
    :goto_3
    invoke-static {}, LX/AYp;->values()[LX/AYp;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/AZ5;->a:[I

    :try_start_4
    sget-object v0, LX/AZ5;->a:[I

    sget-object v1, LX/AYp;->COMMERCIAL_BREAK_STARTED:LX/AYp;

    invoke-virtual {v1}, LX/AYp;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_2

    :goto_4
    :try_start_5
    sget-object v0, LX/AZ5;->a:[I

    sget-object v1, LX/AYp;->COMMERCIAL_BREAK_FINISHED:LX/AYp;

    invoke-virtual {v1}, LX/AYp;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_1

    :goto_5
    :try_start_6
    sget-object v0, LX/AZ5;->a:[I

    sget-object v1, LX/AYp;->COMMERCIAL_BREAK_INTERRUPTED:LX/AYp;

    invoke-virtual {v1}, LX/AYp;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_0

    :goto_6
    return-void

    :catch_0
    goto :goto_6

    :catch_1
    goto :goto_5

    :catch_2
    goto :goto_4

    :catch_3
    goto :goto_3

    :catch_4
    goto :goto_2

    :catch_5
    goto :goto_1

    :catch_6
    goto :goto_0
.end method
