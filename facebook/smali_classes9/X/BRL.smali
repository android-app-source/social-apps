.class public LX/BRL;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0tX;",
            ">;"
        }
    .end annotation
.end field

.field public volatile b:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/heisman/swipeable/SwipeableOverlaysRequestFactory;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/BRP;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/9d6;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/8GP;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public g:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public h:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public final i:Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;

.field public final j:LX/8GL;

.field public final k:LX/BRK;

.field public l:LX/9d5;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/FrameGraphQLInterfaces$Frame;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:LX/1Mv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Mv",
            "<",
            "Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Z


# direct methods
.method public constructor <init>(Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;)V
    .locals 2
    .param p1    # Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1783877
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1783878
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1783879
    iput-object v0, p0, LX/BRL;->h:LX/0Ot;

    .line 1783880
    new-instance v0, LX/BRK;

    invoke-direct {v0, p0}, LX/BRK;-><init>(LX/BRL;)V

    iput-object v0, p0, LX/BRL;->k:LX/BRK;

    .line 1783881
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;

    iput-object v0, p0, LX/BRL;->i:Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;

    .line 1783882
    new-instance v0, LX/8GL;

    invoke-direct {v0, p2}, LX/8GL;-><init>(Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;)V

    iput-object v0, p0, LX/BRL;->j:LX/8GL;

    .line 1783883
    return-void
.end method

.method public static b(LX/5QV;Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;Ljava/lang/String;)LX/0Px;
    .locals 6
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/5QV;",
            "Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLInterfaces$ImageOverlayWithSwipeableOverlays;",
            "Ljava/lang/String;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/FrameGraphQLInterfaces$Frame;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1783884
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1783885
    invoke-static {p0, p1}, LX/B5R;->a(LX/5QV;Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;)LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    .line 1783886
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5QV;

    .line 1783887
    invoke-interface {v0}, LX/5QV;->c()Ljava/lang/String;

    move-result-object v5

    invoke-static {p2, v5}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1783888
    invoke-interface {v0}, LX/5QV;->c()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v0}, LX/5QV;->d()Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel$ImageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel$ImageModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0}, LX/B5P;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1783889
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1783890
    :cond_1
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static c(LX/BRL;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1783891
    iget-object v0, p0, LX/BRL;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/heisman/swipeable/SwipeableOverlaysRequestFactory;

    .line 1783892
    iget-object v1, p0, LX/BRL;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0tX;

    .line 1783893
    iget-object v2, p0, LX/BRL;->c:LX/BRP;

    .line 1783894
    iget-object p0, v2, LX/BRP;->a:Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;

    move-object v2, p0

    .line 1783895
    invoke-virtual {v2}, Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;->c()LX/5QV;

    move-result-object v2

    invoke-interface {v2}, LX/5QV;->c()Ljava/lang/String;

    move-result-object v2

    .line 1783896
    invoke-virtual {v0, v2}, Lcom/facebook/heisman/swipeable/SwipeableOverlaysRequestFactory;->a(Ljava/lang/String;)LX/0zO;

    move-result-object v0

    .line 1783897
    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    invoke-static {v0}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1783898
    iget-object v0, p0, LX/BRL;->n:LX/1Mv;

    if-eqz v0, :cond_0

    .line 1783899
    iget-object v0, p0, LX/BRL;->n:LX/1Mv;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/1Mv;->a(Z)V

    .line 1783900
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/BRL;->o:Z

    .line 1783901
    return-void
.end method
