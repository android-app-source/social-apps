.class public LX/BUi;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile l:LX/BUi;


# instance fields
.field public final a:Landroid/content/Context;

.field private final b:LX/15V;

.field public final c:LX/0tQ;

.field private final d:Landroid/app/NotificationManager;

.field private final e:LX/16U;

.field private final f:LX/BUh;

.field private final g:LX/1Sc;

.field private final h:LX/0xX;

.field private final i:LX/0hy;

.field private j:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "LX/2HB;",
            ">;"
        }
    .end annotation
.end field

.field private k:I


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/15V;LX/0tQ;LX/1Sc;LX/16U;LX/0xX;LX/0hy;)V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 1789434
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1789435
    new-instance v0, LX/BUh;

    invoke-direct {v0, p0}, LX/BUh;-><init>(LX/BUi;)V

    iput-object v0, p0, LX/BUi;->f:LX/BUh;

    .line 1789436
    iput-object p4, p0, LX/BUi;->g:LX/1Sc;

    .line 1789437
    iput-object p1, p0, LX/BUi;->a:Landroid/content/Context;

    .line 1789438
    iput-object p2, p0, LX/BUi;->b:LX/15V;

    .line 1789439
    iput-object p3, p0, LX/BUi;->c:LX/0tQ;

    .line 1789440
    iget-object v0, p0, LX/BUi;->a:Landroid/content/Context;

    const-string v1, "notification"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, LX/BUi;->d:Landroid/app/NotificationManager;

    .line 1789441
    iput-object p5, p0, LX/BUi;->e:LX/16U;

    .line 1789442
    iget-object v0, p0, LX/BUi;->e:LX/16U;

    const-class v1, LX/1ub;

    iget-object v2, p0, LX/BUi;->f:LX/BUh;

    invoke-virtual {v0, v1, v2}, LX/16V;->a(Ljava/lang/Class;LX/16Y;)V

    .line 1789443
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/BUi;->j:Ljava/util/HashMap;

    .line 1789444
    iput-object p6, p0, LX/BUi;->h:LX/0xX;

    .line 1789445
    iput-object p7, p0, LX/BUi;->i:LX/0hy;

    .line 1789446
    iput v3, p0, LX/BUi;->k:I

    .line 1789447
    return-void
.end method

.method public static a(LX/0QB;)LX/BUi;
    .locals 11

    .prologue
    .line 1789448
    sget-object v0, LX/BUi;->l:LX/BUi;

    if-nez v0, :cond_1

    .line 1789449
    const-class v1, LX/BUi;

    monitor-enter v1

    .line 1789450
    :try_start_0
    sget-object v0, LX/BUi;->l:LX/BUi;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1789451
    if-eqz v2, :cond_0

    .line 1789452
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1789453
    new-instance v3, LX/BUi;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/15V;->a(LX/0QB;)LX/15V;

    move-result-object v5

    check-cast v5, LX/15V;

    invoke-static {v0}, LX/0tQ;->a(LX/0QB;)LX/0tQ;

    move-result-object v6

    check-cast v6, LX/0tQ;

    invoke-static {v0}, LX/1Sc;->b(LX/0QB;)LX/1Sc;

    move-result-object v7

    check-cast v7, LX/1Sc;

    invoke-static {v0}, LX/16U;->a(LX/0QB;)LX/16U;

    move-result-object v8

    check-cast v8, LX/16U;

    invoke-static {v0}, LX/0xX;->a(LX/0QB;)LX/0xX;

    move-result-object v9

    check-cast v9, LX/0xX;

    invoke-static {v0}, LX/10P;->a(LX/0QB;)LX/10P;

    move-result-object v10

    check-cast v10, LX/0hy;

    invoke-direct/range {v3 .. v10}, LX/BUi;-><init>(Landroid/content/Context;LX/15V;LX/0tQ;LX/1Sc;LX/16U;LX/0xX;LX/0hy;)V

    .line 1789454
    move-object v0, v3

    .line 1789455
    sput-object v0, LX/BUi;->l:LX/BUi;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1789456
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1789457
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1789458
    :cond_1
    sget-object v0, LX/BUi;->l:LX/BUi;

    return-object v0

    .line 1789459
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1789460
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static declared-synchronized a$redex0(LX/BUi;Ljava/lang/String;LX/2fs;)V
    .locals 6

    .prologue
    .line 1789461
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/BUi;->j:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 1789462
    :goto_0
    monitor-exit p0

    return-void

    .line 1789463
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/BUi;->j:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2HB;

    .line 1789464
    iget-object v1, p2, LX/2fs;->c:LX/1A0;

    sget-object v2, LX/1A0;->DOWNLOAD_NOT_REQUESTED:LX/1A0;

    if-ne v1, v2, :cond_1

    .line 1789465
    iget-object v0, p0, LX/BUi;->d:Landroid/app/NotificationManager;

    invoke-static {p1}, LX/BUi;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    .line 1789466
    iget-object v0, p0, LX/BUi;->j:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1789467
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1789468
    :cond_1
    :try_start_2
    sget-object v1, LX/BUg;->a:[I

    iget-object v2, p2, LX/2fs;->c:LX/1A0;

    invoke-virtual {v2}, LX/1A0;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1789469
    iget-boolean v1, p2, LX/2fs;->e:Z

    .line 1789470
    if-eqz v1, :cond_4

    .line 1789471
    iget-object v2, p0, LX/BUi;->a:Landroid/content/Context;

    const v3, 0x7f080dd3

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1789472
    :goto_1
    move-object v1, v2

    .line 1789473
    invoke-virtual {v0, v1}, LX/2HB;->b(Ljava/lang/CharSequence;)LX/2HB;

    .line 1789474
    iget-wide v2, p2, LX/2fs;->b:J

    const-wide/16 v4, 0x64

    mul-long/2addr v2, v4

    iget-wide v4, p2, LX/2fs;->a:J

    div-long/2addr v2, v4

    long-to-int v1, v2

    .line 1789475
    const/16 v2, 0x64

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v1, v3}, LX/2HB;->a(IIZ)LX/2HB;

    .line 1789476
    :cond_2
    :goto_2
    iget-object v1, p0, LX/BUi;->d:Landroid/app/NotificationManager;

    invoke-static {p1}, LX/BUi;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0}, LX/2HB;->c()Landroid/app/Notification;

    move-result-object v0

    invoke-virtual {v1, v2, v3, v0}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    goto :goto_0

    .line 1789477
    :pswitch_0
    iget-boolean v1, p2, LX/2fs;->e:Z

    .line 1789478
    if-eqz v1, :cond_5

    .line 1789479
    iget-object v2, p0, LX/BUi;->a:Landroid/content/Context;

    const v3, 0x7f080dca

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1789480
    :goto_3
    move-object v1, v2

    .line 1789481
    invoke-virtual {v0, v1}, LX/2HB;->b(Ljava/lang/CharSequence;)LX/2HB;

    .line 1789482
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, LX/2HB;->a(IIZ)LX/2HB;

    .line 1789483
    const/4 v1, 0x1

    .line 1789484
    iput v1, v0, LX/2HB;->j:I

    .line 1789485
    iget-object v1, p0, LX/BUi;->c:LX/0tQ;

    .line 1789486
    invoke-virtual {v1}, LX/0tQ;->c()Z

    move-result v2

    if-eqz v2, :cond_6

    iget-object v2, v1, LX/0tQ;->a:LX/0ad;

    sget-short v3, LX/0wh;->p:S

    invoke-static {v1}, LX/0tQ;->Q(LX/0tQ;)Z

    move-result v4

    invoke-interface {v2, v3, v4}, LX/0ad;->a(SZ)Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v2, 0x1

    :goto_4
    move v1, v2

    .line 1789487
    if-eqz v1, :cond_3

    .line 1789488
    const/4 v1, -0x1

    invoke-virtual {v0, v1}, LX/2HB;->c(I)LX/2HB;

    .line 1789489
    :cond_3
    iget-boolean v1, p2, LX/2fs;->e:Z

    if-nez v1, :cond_2

    .line 1789490
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/2HB;->c(Z)LX/2HB;

    goto :goto_2

    .line 1789491
    :pswitch_1
    iget-boolean v1, p2, LX/2fs;->e:Z

    .line 1789492
    if-eqz v1, :cond_7

    .line 1789493
    iget-object v2, p0, LX/BUi;->a:Landroid/content/Context;

    const v3, 0x7f080dce

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1789494
    :goto_5
    move-object v1, v2

    .line 1789495
    invoke-virtual {v0, v1}, LX/2HB;->b(Ljava/lang/CharSequence;)LX/2HB;

    .line 1789496
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, LX/2HB;->a(IIZ)LX/2HB;

    .line 1789497
    const/4 v1, 0x1

    .line 1789498
    iput v1, v0, LX/2HB;->j:I

    .line 1789499
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/2HB;->c(Z)LX/2HB;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 1789500
    :cond_4
    :try_start_3
    sget-object v2, LX/BUg;->b:[I

    iget-object v3, p0, LX/BUi;->c:LX/0tQ;

    invoke-virtual {v3}, LX/0tQ;->m()LX/2qY;

    move-result-object v3

    invoke-virtual {v3}, LX/2qY;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_1

    .line 1789501
    iget-object v2, p0, LX/BUi;->a:Landroid/content/Context;

    const v3, 0x7f080dcf

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_1

    .line 1789502
    :pswitch_2
    iget-object v2, p0, LX/BUi;->a:Landroid/content/Context;

    const v3, 0x7f080dd2

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_1

    .line 1789503
    :pswitch_3
    iget-object v2, p0, LX/BUi;->a:Landroid/content/Context;

    const v3, 0x7f080dd0

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1789504
    :cond_5
    :try_start_4
    sget-object v2, LX/BUg;->b:[I

    iget-object v3, p0, LX/BUi;->c:LX/0tQ;

    invoke-virtual {v3}, LX/0tQ;->m()LX/2qY;

    move-result-object v3

    invoke-virtual {v3}, LX/2qY;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_2

    .line 1789505
    iget-object v2, p0, LX/BUi;->a:Landroid/content/Context;

    const v3, 0x7f080dc7

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_3

    .line 1789506
    :pswitch_4
    iget-object v2, p0, LX/BUi;->a:Landroid/content/Context;

    const v3, 0x7f080dc9

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_3

    .line 1789507
    :pswitch_5
    iget-object v2, p0, LX/BUi;->a:Landroid/content/Context;

    const v3, 0x7f080dc8

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :cond_6
    :try_start_5
    const/4 v2, 0x0

    goto/16 :goto_4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 1789508
    :cond_7
    sget-object v2, LX/BUg;->b:[I

    iget-object v3, p0, LX/BUi;->c:LX/0tQ;

    invoke-virtual {v3}, LX/0tQ;->m()LX/2qY;

    move-result-object v3

    invoke-virtual {v3}, LX/2qY;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_3

    .line 1789509
    iget-object v2, p0, LX/BUi;->a:Landroid/content/Context;

    const v3, 0x7f080dcb

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_5

    .line 1789510
    :pswitch_6
    iget-object v2, p0, LX/BUi;->a:Landroid/content/Context;

    const v3, 0x7f080dcd

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_5

    .line 1789511
    :pswitch_7
    iget-object v2, p0, LX/BUi;->a:Landroid/content/Context;

    const v3, 0x7f080dcc

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_5

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_4
        :pswitch_5
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x1
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method private static c(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1789512
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "VideoDownloadNotification_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 1789513
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/BUi;->g:LX/1Sc;

    invoke-virtual {v0, p1}, LX/1Sc;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1789514
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/BUi;->j:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    .line 1789515
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 1789516
    :cond_1
    :try_start_1
    new-instance v2, LX/2HB;

    iget-object v0, p0, LX/BUi;->a:Landroid/content/Context;

    invoke-direct {v2, v0}, LX/2HB;-><init>(Landroid/content/Context;)V

    .line 1789517
    iget-object v3, p0, LX/BUi;->a:Landroid/content/Context;

    const/4 v4, 0x0

    iget-object v0, p0, LX/BUi;->h:LX/0xX;

    sget-object v5, LX/1vy;->DOWNLOAD_SECTION:LX/1vy;

    invoke-virtual {v0, v5}, LX/0xX;->a(LX/1vy;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/BUi;->b:LX/15V;

    invoke-virtual {v0}, LX/15V;->a()Landroid/content/Intent;

    move-result-object v0

    :goto_1
    const/high16 v5, 0x8000000

    invoke-static {v3, v4, v0, v5}, LX/0nt;->a(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 1789518
    iput-object v0, v2, LX/2HB;->d:Landroid/app/PendingIntent;

    .line 1789519
    const v0, 0x7f0218e4

    invoke-virtual {v2, v0}, LX/2HB;->a(I)LX/2HB;

    .line 1789520
    iget-object v0, p0, LX/BUi;->a:Landroid/content/Context;

    const v3, 0x7f080dc6

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    invoke-virtual {v0, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/2HB;->a(Ljava/lang/CharSequence;)LX/2HB;

    .line 1789521
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_2

    .line 1789522
    iget-object v0, p0, LX/BUi;->a:Landroid/content/Context;

    const v1, 0x7f0a00d1

    invoke-static {v0, v1}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v0

    .line 1789523
    iput v0, v2, LX/2HB;->y:I

    .line 1789524
    :cond_2
    iget-object v0, p0, LX/BUi;->j:Ljava/util/HashMap;

    invoke-virtual {v0, p1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1789525
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1789526
    :cond_3
    :try_start_2
    iget-object v0, p0, LX/BUi;->b:LX/15V;

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->VIDEO_DOWNLOAD_LOCAL_PUSH_NOTIF:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    invoke-virtual {v0, v5}, LX/15V;->b(Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;)Landroid/content/Intent;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    goto :goto_1
.end method

.method public final declared-synchronized a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 1789527
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/BUi;->j:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 1789528
    :goto_0
    monitor-exit p0

    return-void

    .line 1789529
    :cond_0
    :try_start_1
    new-instance v0, LX/2HB;

    iget-object v1, p0, LX/BUi;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/2HB;-><init>(Landroid/content/Context;)V

    .line 1789530
    new-instance v1, LX/89k;

    invoke-direct {v1}, LX/89k;-><init>()V

    .line 1789531
    iput-object p3, v1, LX/89k;->b:Ljava/lang/String;

    .line 1789532
    move-object v1, v1

    .line 1789533
    sget-object v2, LX/89g;->OFFLINE_VIDEO_CACHE:LX/89g;

    .line 1789534
    iput-object v2, v1, LX/89k;->a:LX/89g;

    .line 1789535
    move-object v1, v1

    .line 1789536
    invoke-virtual {v1}, LX/89k;->a()Lcom/facebook/ipc/feed/PermalinkStoryIdParams;

    move-result-object v1

    .line 1789537
    iget-object v2, p0, LX/BUi;->i:LX/0hy;

    invoke-interface {v2, v1}, LX/0hy;->a(Lcom/facebook/ipc/feed/PermalinkStoryIdParams;)Landroid/content/Intent;

    move-result-object v1

    .line 1789538
    iget-object v2, p0, LX/BUi;->a:Landroid/content/Context;

    iget v3, p0, LX/BUi;->k:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, LX/BUi;->k:I

    const/high16 v4, 0x8000000

    invoke-static {v2, v3, v1, v4}, LX/0nt;->a(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 1789539
    iput-object v1, v0, LX/2HB;->d:Landroid/app/PendingIntent;

    .line 1789540
    const v1, 0x7f0218e4

    invoke-virtual {v0, v1}, LX/2HB;->a(I)LX/2HB;

    .line 1789541
    iget-object v1, p0, LX/BUi;->a:Landroid/content/Context;

    const v2, 0x7f080dc6

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p2, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2HB;->a(Ljava/lang/CharSequence;)LX/2HB;

    .line 1789542
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v1, v2, :cond_1

    .line 1789543
    iget-object v1, p0, LX/BUi;->a:Landroid/content/Context;

    const v2, 0x7f0a00d1

    invoke-static {v1, v2}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v1

    .line 1789544
    iput v1, v0, LX/2HB;->y:I

    .line 1789545
    :cond_1
    iget-object v1, p0, LX/BUi;->j:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1789546
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1789547
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/BUi;->j:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 1789548
    :goto_0
    monitor-exit p0

    return-void

    .line 1789549
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/BUi;->d:Landroid/app/NotificationManager;

    invoke-static {p1}, LX/BUi;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1789550
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
