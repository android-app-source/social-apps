.class public LX/CfF;
.super Lcom/facebook/reaction/ReactionUtil;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/CfF;


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0SG;Landroid/content/Context;LX/0Ot;LX/0Or;LX/0tX;LX/0rq;LX/01T;LX/0sa;LX/0Or;LX/1vj;LX/1vi;LX/1vC;LX/1V6;LX/1vm;LX/1s6;LX/1vn;LX/1Ck;LX/0se;LX/1vo;LX/0yD;LX/0ad;LX/14x;LX/0Or;LX/0sU;Ljava/util/concurrent/ExecutorService;LX/0Ot;LX/0hB;LX/0dC;LX/0tQ;)V
    .locals 0
    .param p11    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .param p27    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/appchoreographer/AppChoreographer;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0sX;",
            ">;",
            "LX/0SG;",
            "Landroid/content/Context;",
            "LX/0Ot",
            "<",
            "LX/0y3;",
            ">;",
            "LX/0Or",
            "<",
            "LX/CK5;",
            ">;",
            "LX/0tX;",
            "LX/0rq;",
            "LX/01T;",
            "LX/0sa;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/1vj;",
            "LX/1vi;",
            "LX/1vC;",
            "LX/1V6;",
            "LX/1vm;",
            "LX/1s6;",
            "LX/1vn;",
            "LX/1Ck;",
            "LX/0se;",
            "LX/1vo;",
            "LX/0yD;",
            "LX/0ad;",
            "LX/14x;",
            "LX/0Or",
            "<",
            "LX/0wp;",
            ">;",
            "LX/0sU;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0hB;",
            "Lcom/facebook/device_id/UniqueIdForDeviceHolder;",
            "LX/0tQ;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1925719
    invoke-direct/range {p0 .. p31}, Lcom/facebook/reaction/ReactionUtil;-><init>(LX/0Ot;LX/0Ot;LX/0SG;Landroid/content/Context;LX/0Ot;LX/0Or;LX/0tX;LX/0rq;LX/01T;LX/0sa;LX/0Or;LX/1vj;LX/1vi;LX/1vC;LX/1V6;LX/1vm;LX/1s6;LX/1vn;LX/1Ck;LX/0se;LX/1vo;LX/0yD;LX/0ad;LX/14x;LX/0Or;LX/0sU;Ljava/util/concurrent/ExecutorService;LX/0Ot;LX/0hB;LX/0dC;LX/0tQ;)V

    .line 1925720
    return-void
.end method

.method public static a(LX/0QB;)LX/CfF;
    .locals 3

    .prologue
    .line 1925707
    sget-object v0, LX/CfF;->a:LX/CfF;

    if-nez v0, :cond_1

    .line 1925708
    const-class v1, LX/CfF;

    monitor-enter v1

    .line 1925709
    :try_start_0
    sget-object v0, LX/CfF;->a:LX/CfF;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1925710
    if-eqz v2, :cond_0

    .line 1925711
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/CfF;->c(LX/0QB;)LX/CfF;

    move-result-object v0

    sput-object v0, LX/CfF;->a:LX/CfF;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1925712
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1925713
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1925714
    :cond_1
    sget-object v0, LX/CfF;->a:LX/CfF;

    return-object v0

    .line 1925715
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1925716
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static c(LX/0QB;)LX/CfF;
    .locals 34

    .prologue
    .line 1925717
    new-instance v2, LX/CfF;

    const/16 v3, 0x23d

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x60

    move-object/from16 v0, p0

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-static/range {p0 .. p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v5

    check-cast v5, LX/0SG;

    const-class v6, Landroid/content/Context;

    move-object/from16 v0, p0

    invoke-interface {v0, v6}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/Context;

    const/16 v7, 0xc83

    move-object/from16 v0, p0

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x26ca

    move-object/from16 v0, p0

    invoke-static {v0, v8}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    invoke-static/range {p0 .. p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v9

    check-cast v9, LX/0tX;

    invoke-static/range {p0 .. p0}, LX/0rq;->a(LX/0QB;)LX/0rq;

    move-result-object v10

    check-cast v10, LX/0rq;

    invoke-static/range {p0 .. p0}, LX/15N;->a(LX/0QB;)LX/01T;

    move-result-object v11

    check-cast v11, LX/01T;

    invoke-static/range {p0 .. p0}, LX/0sa;->a(LX/0QB;)LX/0sa;

    move-result-object v12

    check-cast v12, LX/0sa;

    const/16 v13, 0x15e7

    move-object/from16 v0, p0

    invoke-static {v0, v13}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v13

    invoke-static/range {p0 .. p0}, LX/1vj;->a(LX/0QB;)LX/1vj;

    move-result-object v14

    check-cast v14, LX/1vj;

    invoke-static/range {p0 .. p0}, LX/1vi;->a(LX/0QB;)LX/1vi;

    move-result-object v15

    check-cast v15, LX/1vi;

    invoke-static/range {p0 .. p0}, LX/1vC;->a(LX/0QB;)LX/1vC;

    move-result-object v16

    check-cast v16, LX/1vC;

    const-class v17, LX/1V6;

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v17

    check-cast v17, LX/1V6;

    const-class v18, LX/1vm;

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v18

    check-cast v18, LX/1vm;

    invoke-static/range {p0 .. p0}, LX/1s6;->a(LX/0QB;)LX/1s6;

    move-result-object v19

    check-cast v19, LX/1s6;

    invoke-static/range {p0 .. p0}, LX/1vn;->a(LX/0QB;)LX/1vn;

    move-result-object v20

    check-cast v20, LX/1vn;

    invoke-static/range {p0 .. p0}, LX/1Ck;->a(LX/0QB;)LX/1Ck;

    move-result-object v21

    check-cast v21, LX/1Ck;

    invoke-static/range {p0 .. p0}, LX/0se;->a(LX/0QB;)LX/0se;

    move-result-object v22

    check-cast v22, LX/0se;

    invoke-static/range {p0 .. p0}, LX/1vo;->a(LX/0QB;)LX/1vo;

    move-result-object v23

    check-cast v23, LX/1vo;

    invoke-static/range {p0 .. p0}, LX/0yD;->a(LX/0QB;)LX/0yD;

    move-result-object v24

    check-cast v24, LX/0yD;

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v25

    check-cast v25, LX/0ad;

    invoke-static/range {p0 .. p0}, LX/14x;->a(LX/0QB;)LX/14x;

    move-result-object v26

    check-cast v26, LX/14x;

    const/16 v27, 0x12e4

    move-object/from16 v0, p0

    move/from16 v1, v27

    invoke-static {v0, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v27

    invoke-static/range {p0 .. p0}, LX/0sU;->a(LX/0QB;)LX/0sU;

    move-result-object v28

    check-cast v28, LX/0sU;

    invoke-static/range {p0 .. p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v29

    check-cast v29, Ljava/util/concurrent/ExecutorService;

    const/16 v30, 0x259

    move-object/from16 v0, p0

    move/from16 v1, v30

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v30

    invoke-static/range {p0 .. p0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v31

    check-cast v31, LX/0hB;

    invoke-static/range {p0 .. p0}, LX/0dB;->a(LX/0QB;)LX/0dC;

    move-result-object v32

    check-cast v32, LX/0dC;

    invoke-static/range {p0 .. p0}, LX/0tQ;->a(LX/0QB;)LX/0tQ;

    move-result-object v33

    check-cast v33, LX/0tQ;

    invoke-direct/range {v2 .. v33}, LX/CfF;-><init>(LX/0Ot;LX/0Ot;LX/0SG;Landroid/content/Context;LX/0Ot;LX/0Or;LX/0tX;LX/0rq;LX/01T;LX/0sa;LX/0Or;LX/1vj;LX/1vi;LX/1vC;LX/1V6;LX/1vm;LX/1s6;LX/1vn;LX/1Ck;LX/0se;LX/1vo;LX/0yD;LX/0ad;LX/14x;LX/0Or;LX/0sU;Ljava/util/concurrent/ExecutorService;LX/0Ot;LX/0hB;LX/0dC;LX/0tQ;)V

    .line 1925718
    return-object v2
.end method


# virtual methods
.method public final a(LX/2jY;)Z
    .locals 1

    .prologue
    .line 1925706
    sget-object v0, LX/0zS;->c:LX/0zS;

    invoke-virtual {p0, p1, v0}, LX/CfF;->a(LX/2jY;LX/0zS;)Z

    move-result v0

    return v0
.end method

.method public final a(LX/2jY;LX/0zS;)Z
    .locals 10

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1925683
    if-eqz p1, :cond_0

    .line 1925684
    iget-boolean v2, p1, LX/2jY;->o:Z

    move v2, v2

    .line 1925685
    if-eqz v2, :cond_0

    .line 1925686
    iget-boolean v2, p1, LX/2jY;->p:Z

    move v2, v2

    .line 1925687
    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    .line 1925688
    :goto_0
    return v0

    .line 1925689
    :cond_1
    iget-object v2, p1, LX/2jY;->y:Lcom/facebook/reaction/ReactionQueryParams;

    move-object v5, v2

    .line 1925690
    invoke-virtual {p1}, LX/2jY;->g()Ljava/lang/String;

    move-result-object v9

    .line 1925691
    if-eqz v5, :cond_2

    if-eqz v9, :cond_2

    .line 1925692
    iget-object v2, v5, Lcom/facebook/reaction/ReactionQueryParams;->t:Ljava/lang/Long;

    move-object v2, v2

    .line 1925693
    if-nez v2, :cond_3

    .line 1925694
    :cond_2
    iput-boolean v1, p1, LX/2jY;->o:Z

    .line 1925695
    move v0, v1

    .line 1925696
    goto :goto_0

    .line 1925697
    :cond_3
    iput-boolean v0, p1, LX/2jY;->p:Z

    .line 1925698
    iput-object v9, v5, Lcom/facebook/reaction/ReactionQueryParams;->a:Ljava/lang/String;

    .line 1925699
    move-object v2, v5

    .line 1925700
    iget-object v1, p1, LX/2jY;->a:Ljava/lang/String;

    move-object v3, v1

    .line 1925701
    iget-object v1, p1, LX/2jY;->b:Ljava/lang/String;

    move-object v4, v1

    .line 1925702
    const-wide/32 v6, 0x15180

    new-instance v8, LX/CfT;

    .line 1925703
    iget-object v1, p1, LX/2jY;->b:Ljava/lang/String;

    move-object v1, v1

    .line 1925704
    iget-object p1, v5, Lcom/facebook/reaction/ReactionQueryParams;->t:Ljava/lang/Long;

    move-object v5, p1

    .line 1925705
    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v8, v1, v5, v9}, LX/CfT;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, p0

    move-object v5, p2

    invoke-virtual/range {v1 .. v8}, Lcom/facebook/reaction/ReactionUtil;->b(Lcom/facebook/reaction/ReactionQueryParams;Ljava/lang/String;Ljava/lang/String;LX/0zS;JLX/0zT;)V

    goto :goto_0
.end method
