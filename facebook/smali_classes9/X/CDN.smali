.class public LX/CDN;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/MountSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pe;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pt;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static o:LX/0Xm;


# instance fields
.field public final a:Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic",
            "<TE;",
            "Lcom/facebook/feedplugins/video/RichVideoAttachmentView;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/094;

.field public final c:LX/198;

.field public final d:LX/1YQ;

.field private final e:LX/03V;

.field private final f:LX/2mj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2mj",
            "<TE;",
            "Lcom/facebook/feedplugins/video/RichVideoAttachmentView;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Lcom/facebook/feedplugins/attachments/video/VideoPrefetchComponentLogic;

.field private final h:LX/3Fb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/3Fb",
            "<",
            "Lcom/facebook/feedplugins/video/RichVideoAttachmentView;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/2mi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2mi",
            "<",
            "Lcom/facebook/feedplugins/video/RichVideoAttachmentView;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/23q;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/23q",
            "<TE;",
            "Lcom/facebook/feedplugins/video/RichVideoAttachmentView;",
            ">;"
        }
    .end annotation
.end field

.field private final k:LX/2mr;

.field public final l:LX/0iY;

.field private final m:LX/2ms;

.field public final n:LX/0iX;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;LX/094;LX/198;LX/1YQ;LX/03V;LX/2mj;Lcom/facebook/feedplugins/attachments/video/VideoPrefetchComponentLogic;LX/3Fb;LX/2mi;LX/23q;LX/2mr;LX/0iY;LX/2ms;LX/0iX;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1859068
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1859069
    iput-object p1, p0, LX/CDN;->a:Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;

    .line 1859070
    iput-object p2, p0, LX/CDN;->b:LX/094;

    .line 1859071
    iput-object p3, p0, LX/CDN;->c:LX/198;

    .line 1859072
    iput-object p4, p0, LX/CDN;->d:LX/1YQ;

    .line 1859073
    iput-object p5, p0, LX/CDN;->e:LX/03V;

    .line 1859074
    iput-object p6, p0, LX/CDN;->f:LX/2mj;

    .line 1859075
    iput-object p7, p0, LX/CDN;->g:Lcom/facebook/feedplugins/attachments/video/VideoPrefetchComponentLogic;

    .line 1859076
    iput-object p8, p0, LX/CDN;->h:LX/3Fb;

    .line 1859077
    iput-object p9, p0, LX/CDN;->i:LX/2mi;

    .line 1859078
    iput-object p10, p0, LX/CDN;->j:LX/23q;

    .line 1859079
    iput-object p11, p0, LX/CDN;->k:LX/2mr;

    .line 1859080
    iput-object p12, p0, LX/CDN;->l:LX/0iY;

    .line 1859081
    iput-object p13, p0, LX/CDN;->m:LX/2ms;

    .line 1859082
    iput-object p14, p0, LX/CDN;->n:LX/0iX;

    .line 1859083
    return-void
.end method

.method public static a(LX/0QB;)LX/CDN;
    .locals 3

    .prologue
    .line 1859084
    const-class v1, LX/CDN;

    monitor-enter v1

    .line 1859085
    :try_start_0
    sget-object v0, LX/CDN;->o:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1859086
    sput-object v2, LX/CDN;->o:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1859087
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1859088
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/CDN;->b(LX/0QB;)LX/CDN;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1859089
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/CDN;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1859090
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1859091
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)LX/CDN;
    .locals 15

    .prologue
    .line 1859092
    new-instance v0, LX/CDN;

    invoke-static {p0}, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->a(LX/0QB;)Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;

    move-result-object v1

    check-cast v1, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;

    invoke-static {p0}, LX/094;->a(LX/0QB;)LX/094;

    move-result-object v2

    check-cast v2, LX/094;

    invoke-static {p0}, LX/198;->a(LX/0QB;)LX/198;

    move-result-object v3

    check-cast v3, LX/198;

    invoke-static {p0}, LX/1YQ;->a(LX/0QB;)LX/1YQ;

    move-result-object v4

    check-cast v4, LX/1YQ;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-static {p0}, LX/2mj;->a(LX/0QB;)LX/2mj;

    move-result-object v6

    check-cast v6, LX/2mj;

    invoke-static {p0}, Lcom/facebook/feedplugins/attachments/video/VideoPrefetchComponentLogic;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/video/VideoPrefetchComponentLogic;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedplugins/attachments/video/VideoPrefetchComponentLogic;

    invoke-static {p0}, LX/3Fb;->a(LX/0QB;)LX/3Fb;

    move-result-object v8

    check-cast v8, LX/3Fb;

    invoke-static {p0}, LX/2mi;->a(LX/0QB;)LX/2mi;

    move-result-object v9

    check-cast v9, LX/2mi;

    invoke-static {p0}, LX/23q;->a(LX/0QB;)LX/23q;

    move-result-object v10

    check-cast v10, LX/23q;

    invoke-static {p0}, LX/2mr;->b(LX/0QB;)LX/2mr;

    move-result-object v11

    check-cast v11, LX/2mr;

    invoke-static {p0}, LX/0iY;->a(LX/0QB;)LX/0iY;

    move-result-object v12

    check-cast v12, LX/0iY;

    invoke-static {p0}, LX/2ms;->b(LX/0QB;)LX/2ms;

    move-result-object v13

    check-cast v13, LX/2ms;

    invoke-static {p0}, LX/0iX;->a(LX/0QB;)LX/0iX;

    move-result-object v14

    check-cast v14, LX/0iX;

    invoke-direct/range {v0 .. v14}, LX/CDN;-><init>(Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;LX/094;LX/198;LX/1YQ;LX/03V;LX/2mj;Lcom/facebook/feedplugins/attachments/video/VideoPrefetchComponentLogic;LX/3Fb;LX/2mi;LX/23q;LX/2mr;LX/0iY;LX/2ms;LX/0iX;)V

    .line 1859093
    return-object v0
.end method


# virtual methods
.method public final a(LX/3EE;LX/1Pe;LX/3FV;Landroid/view/View$OnClickListener;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;LX/1np;)V
    .locals 9
    .param p1    # LX/3EE;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p2    # LX/1Pe;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # LX/3FV;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p4    # Landroid/view/View$OnClickListener;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3EE;",
            "TE;",
            "LX/3FV;",
            "Landroid/view/View$OnClickListener;",
            "LX/1np",
            "<",
            "LX/3FN;",
            ">;",
            "LX/1np",
            "<",
            "LX/3FW;",
            ">;",
            "LX/1np",
            "<",
            "LX/3FX;",
            ">;",
            "LX/1np",
            "<",
            "LX/3FY;",
            ">;",
            "LX/1np",
            "<",
            "LX/3FZ;",
            ">;",
            "LX/1np",
            "<",
            "LX/3Fa;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1859094
    iget-object v1, p0, LX/CDN;->a:Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;

    invoke-virtual {v1, p1, p2}, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->a(LX/3EE;LX/1Pe;)LX/3FN;

    move-result-object v2

    .line 1859095
    invoke-virtual {p5, v2}, LX/1np;->a(Ljava/lang/Object;)V

    .line 1859096
    new-instance v1, LX/3FW;

    iget-object v3, p0, LX/CDN;->c:LX/198;

    iget-object v4, p0, LX/CDN;->b:LX/094;

    iget-object v5, p0, LX/CDN;->e:LX/03V;

    iget-object v6, p0, LX/CDN;->m:LX/2ms;

    iget-object v7, p0, LX/CDN;->k:LX/2mr;

    invoke-direct/range {v1 .. v7}, LX/3FW;-><init>(LX/3FN;LX/198;LX/094;LX/03V;LX/2ms;LX/2mr;)V

    invoke-virtual {p6, v1}, LX/1np;->a(Ljava/lang/Object;)V

    .line 1859097
    new-instance v1, LX/CDM;

    invoke-direct {v1, p0}, LX/CDM;-><init>(LX/CDN;)V

    move-object/from16 v0, p7

    invoke-virtual {v0, v1}, LX/1np;->a(Ljava/lang/Object;)V

    .line 1859098
    iget-object v4, p0, LX/CDN;->g:Lcom/facebook/feedplugins/attachments/video/VideoPrefetchComponentLogic;

    iget-object v5, p1, LX/3EE;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v1, p1, LX/3EE;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    iget v6, p1, LX/3EE;->b:I

    move-object v3, p2

    check-cast v3, LX/1Po;

    invoke-virtual {v4, v5, v1, v6, v3}, Lcom/facebook/feedplugins/attachments/video/VideoPrefetchComponentLogic;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLStoryAttachment;ILX/1Po;)V

    .line 1859099
    iget-object v1, p0, LX/CDN;->h:LX/3Fb;

    iget-object v3, v2, LX/3FN;->e:Lcom/facebook/graphql/model/GraphQLVideo;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLVideo;->ba()LX/0Px;

    move-result-object v3

    iget-object v4, v2, LX/3FN;->h:LX/2pa;

    iget-object v4, v4, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v4, v4, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, LX/3Fb;->a(LX/0Px;Ljava/lang/String;)LX/3Fa;

    move-result-object v1

    move-object/from16 v0, p10

    invoke-virtual {v0, v1}, LX/1np;->a(Ljava/lang/Object;)V

    .line 1859100
    if-eqz p4, :cond_0

    if-eqz p3, :cond_0

    .line 1859101
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Both channelFeedLauncherProps and clickListenerOverride cannot be provided."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1859102
    :cond_0
    if-eqz p4, :cond_1

    .line 1859103
    new-instance v1, LX/3FZ;

    invoke-direct {v1, p4}, LX/3FZ;-><init>(Landroid/view/View$OnClickListener;)V

    move-object/from16 v0, p9

    invoke-virtual {v0, v1}, LX/1np;->a(Ljava/lang/Object;)V

    .line 1859104
    iget-object v2, p0, LX/CDN;->i:LX/2mi;

    invoke-virtual/range {p9 .. p9}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3FZ;

    invoke-virtual {v2, v1}, LX/2mi;->a(LX/3FZ;)LX/3FY;

    move-result-object v1

    move-object/from16 v0, p8

    invoke-virtual {v0, v1}, LX/1np;->a(Ljava/lang/Object;)V

    .line 1859105
    :goto_0
    return-void

    .line 1859106
    :cond_1
    iget-object v1, p0, LX/CDN;->j:LX/23q;

    if-eqz p3, :cond_2

    :goto_1
    check-cast p2, LX/1Po;

    invoke-virtual {v1, p3, p2}, LX/23q;->a(LX/3FV;LX/1Po;)LX/3Qx;

    move-result-object v1

    .line 1859107
    new-instance v8, LX/3FZ;

    if-eqz v1, :cond_3

    :goto_2
    invoke-direct {v8, v1}, LX/3FZ;-><init>(Landroid/view/View$OnClickListener;)V

    move-object/from16 v0, p9

    invoke-virtual {v0, v8}, LX/1np;->a(Ljava/lang/Object;)V

    .line 1859108
    iget-object v2, p0, LX/CDN;->i:LX/2mi;

    invoke-virtual/range {p9 .. p9}, LX/1np;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3FZ;

    invoke-virtual {v2, v1}, LX/2mi;->a(LX/3FZ;)LX/3FY;

    move-result-object v1

    move-object/from16 v0, p8

    invoke-virtual {v0, v1}, LX/1np;->a(Ljava/lang/Object;)V

    goto :goto_0

    .line 1859109
    :cond_2
    new-instance p3, LX/3FV;

    iget-object v3, p1, LX/3EE;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v4, p1, LX/3EE;->d:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {p3, v3, v4}, LX/3FV;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/util/concurrent/atomic/AtomicReference;)V

    goto :goto_1

    .line 1859110
    :cond_3
    new-instance v1, LX/3FW;

    iget-object v3, p0, LX/CDN;->c:LX/198;

    iget-object v4, p0, LX/CDN;->b:LX/094;

    iget-object v5, p0, LX/CDN;->e:LX/03V;

    iget-object v6, p0, LX/CDN;->m:LX/2ms;

    iget-object v7, p0, LX/CDN;->k:LX/2mr;

    invoke-direct/range {v1 .. v7}, LX/3FW;-><init>(LX/3FN;LX/198;LX/094;LX/03V;LX/2ms;LX/2mr;)V

    goto :goto_2
.end method

.method public final a(Lcom/facebook/feedplugins/video/RichVideoAttachmentView;LX/3FN;LX/3FW;LX/3EE;)V
    .locals 2

    .prologue
    .line 1859111
    const-string v0, "OlderRichVideoAttachmentComponentSpec.onMount"

    const v1, 0x5c712388

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 1859112
    :try_start_0
    iget-object v0, p0, LX/CDN;->a:Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;

    invoke-virtual {v0, p4, p2, p1}, Lcom/facebook/feedplugins/video/richvideoplayer/RichVideoPlayerComponentLogic;->a(LX/3EE;LX/3FN;Landroid/view/View;)V

    .line 1859113
    invoke-virtual {p1, p3}, Lcom/facebook/feedplugins/video/RichVideoAttachmentView;->setOnClickListener(Landroid/view/View$OnClickListener;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1859114
    const v0, -0x36cc162d

    invoke-static {v0}, LX/02m;->a(I)V

    .line 1859115
    return-void

    .line 1859116
    :catchall_0
    move-exception v0

    const v1, -0x2820af51

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final a(Lcom/facebook/feedplugins/video/RichVideoAttachmentView;LX/3FN;LX/3FX;LX/3Fa;LX/3FY;LX/3FZ;LX/1Pe;)V
    .locals 8
    .annotation build Lcom/facebook/components/annotations/OnBind;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feedplugins/video/RichVideoAttachmentView;",
            "LX/3FN;",
            "LX/3FX;",
            "LX/3Fa;",
            "LX/3FY;",
            "LX/3FZ;",
            "TE;)V"
        }
    .end annotation

    .prologue
    .line 1859117
    const-string v0, "OlderRichVideoAttachmentComponentSpec.onBind"

    const v1, 0x7b650769

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 1859118
    :try_start_0
    iget-object v0, p0, LX/CDN;->f:LX/2mj;

    iget-object v2, p2, LX/3FN;->j:LX/2oL;

    iget-object v3, p2, LX/3FN;->k:LX/093;

    iget-object v1, p2, LX/3FN;->h:LX/2pa;

    iget-object v4, v1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v5, p2, LX/3FN;->g:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    iget-object v6, p2, LX/3FN;->f:LX/04D;

    move-object v1, p1

    move-object v7, p3

    invoke-virtual/range {v0 .. v7}, LX/2mj;->a(Landroid/view/View;LX/2oL;LX/093;Lcom/facebook/video/engine/VideoPlayerParams;Lcom/facebook/video/analytics/VideoFeedStoryInfo;LX/04D;LX/3FX;)V

    .line 1859119
    iget-object v0, p2, LX/3FN;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1859120
    invoke-interface {p7, v0, p1}, LX/1Pe;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Landroid/view/View;)V

    .line 1859121
    invoke-static {p4, p1}, LX/3Fb;->a(LX/3Fa;Landroid/view/View;)V

    .line 1859122
    invoke-static {p6, p5, p1}, LX/2mi;->a(LX/3FZ;LX/3FY;Landroid/view/View;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1859123
    const v0, 0x131d5e59

    invoke-static {v0}, LX/02m;->a(I)V

    .line 1859124
    return-void

    .line 1859125
    :catchall_0
    move-exception v0

    const v1, -0x3bd3e8f7

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method
