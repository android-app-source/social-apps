.class public final LX/B0q;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:Landroid/content/Context;

.field public final synthetic b:Lcom/facebook/graphql/model/GraphQLStory;

.field public final synthetic c:LX/1dz;


# direct methods
.method public constructor <init>(LX/1dz;Landroid/content/Context;Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 0

    .prologue
    .line 1734310
    iput-object p1, p0, LX/B0q;->c:LX/1dz;

    iput-object p2, p0, LX/B0q;->a:Landroid/content/Context;

    iput-object p3, p0, LX/B0q;->b:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    .prologue
    .line 1734311
    iget-object v0, p0, LX/B0q;->c:LX/1dz;

    iget-object v1, p0, LX/B0q;->a:Landroid/content/Context;

    iget-object v2, p0, LX/B0q;->b:Lcom/facebook/graphql/model/GraphQLStory;

    iget-object v3, p0, LX/B0q;->b:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1734312
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object p0

    if-eqz p0, :cond_0

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object p0

    if-eqz p0, :cond_0

    .line 1734313
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object p0

    .line 1734314
    :goto_0
    move-object v3, p0

    .line 1734315
    new-instance p0, LX/4G3;

    invoke-direct {p0}, LX/4G3;-><init>()V

    iget-object p1, v0, LX/1dz;->a:Ljava/lang/String;

    .line 1734316
    const-string p2, "actor_id"

    invoke-virtual {p0, p2, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1734317
    move-object p0, p0

    .line 1734318
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object p1

    .line 1734319
    const-string p2, "story_id"

    invoke-virtual {p0, p2, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1734320
    move-object p0, p0

    .line 1734321
    const-string p1, "treehouse_group_mall"

    .line 1734322
    const-string p2, "source"

    invoke-virtual {p0, p2, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1734323
    move-object p0, p0

    .line 1734324
    const-string p1, "group_id"

    invoke-virtual {p0, p1, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1734325
    move-object p0, p0

    .line 1734326
    new-instance p1, LX/9Lm;

    invoke-direct {p1}, LX/9Lm;-><init>()V

    move-object p1, p1

    .line 1734327
    const-string p2, "input"

    invoke-virtual {p1, p2, p0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1734328
    iget-object p0, v0, LX/1dz;->b:LX/0tX;

    invoke-static {p1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object p1

    invoke-virtual {p0, p1}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object p0

    .line 1734329
    new-instance p1, LX/B0r;

    invoke-direct {p1, v0, v1}, LX/B0r;-><init>(LX/1dz;Landroid/content/Context;)V

    iget-object p2, v0, LX/1dz;->c:Ljava/util/concurrent/ExecutorService;

    invoke-static {p0, p1, p2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1734330
    return-void

    :cond_0
    const/4 p0, 0x0

    goto :goto_0
.end method
