.class public LX/ByE;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pt;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pq;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/38w;

.field public final b:LX/ByS;


# direct methods
.method public constructor <init>(LX/38w;LX/ByS;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1836980
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1836981
    iput-object p1, p0, LX/ByE;->a:LX/38w;

    .line 1836982
    iput-object p2, p0, LX/ByE;->b:LX/ByS;

    .line 1836983
    return-void
.end method

.method public static a(LX/0QB;)LX/ByE;
    .locals 5

    .prologue
    .line 1836984
    const-class v1, LX/ByE;

    monitor-enter v1

    .line 1836985
    :try_start_0
    sget-object v0, LX/ByE;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1836986
    sput-object v2, LX/ByE;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1836987
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1836988
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1836989
    new-instance p0, LX/ByE;

    invoke-static {v0}, LX/38w;->b(LX/0QB;)LX/38w;

    move-result-object v3

    check-cast v3, LX/38w;

    invoke-static {v0}, LX/ByS;->a(LX/0QB;)LX/ByS;

    move-result-object v4

    check-cast v4, LX/ByS;

    invoke-direct {p0, v3, v4}, LX/ByE;-><init>(LX/38w;LX/ByS;)V

    .line 1836990
    move-object v0, p0

    .line 1836991
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1836992
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/ByE;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1836993
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1836994
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
