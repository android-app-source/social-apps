.class public final LX/Bwn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation build Landroid/support/annotation/RequiresApi;
.end annotation


# instance fields
.field public final synthetic a:LX/Bwq;

.field private b:Z


# direct methods
.method public constructor <init>(LX/Bwq;)V
    .locals 1

    .prologue
    .line 1834353
    iput-object p1, p0, LX/Bwn;->a:LX/Bwq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1834354
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Bwn;->b:Z

    return-void
.end method


# virtual methods
.method public final onAnimationCancel(Landroid/animation/Animator;)V
    .locals 1

    .prologue
    .line 1834355
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Bwn;->b:Z

    .line 1834356
    return-void
.end method

.method public final onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2

    .prologue
    .line 1834357
    iget-boolean v0, p0, LX/Bwn;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Bwn;->a:LX/Bwq;

    iget v0, v0, LX/Bwq;->u:I

    const/4 v1, 0x4

    if-ge v0, v1, :cond_0

    .line 1834358
    iget-object v0, p0, LX/Bwn;->a:LX/Bwq;

    .line 1834359
    iget v1, v0, LX/Bwq;->u:I

    add-int/lit8 p0, v1, 0x1

    iput p0, v0, LX/Bwq;->u:I

    .line 1834360
    const-wide/16 v0, 0x5dc

    invoke-virtual {p1, v0, v1}, Landroid/animation/Animator;->setStartDelay(J)V

    .line 1834361
    invoke-virtual {p1}, Landroid/animation/Animator;->start()V

    .line 1834362
    :cond_0
    return-void
.end method

.method public final onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 1834363
    return-void
.end method

.method public final onAnimationStart(Landroid/animation/Animator;)V
    .locals 2

    .prologue
    .line 1834364
    iget-object v0, p0, LX/Bwn;->a:LX/Bwq;

    iget-object v0, v0, LX/Bwq;->p:Lcom/facebook/widget/FbImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/FbImageView;->setImageAlpha(I)V

    .line 1834365
    iget-object v0, p0, LX/Bwn;->a:LX/Bwq;

    iget-object v0, v0, LX/Bwq;->p:Lcom/facebook/widget/FbImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/FbImageView;->setRotation(F)V

    .line 1834366
    return-void
.end method
