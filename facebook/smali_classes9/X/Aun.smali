.class public final LX/Aun;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source ""


# instance fields
.field public final synthetic a:LX/Aur;


# direct methods
.method public constructor <init>(LX/Aur;)V
    .locals 0

    .prologue
    .line 1723230
    iput-object p1, p0, LX/Aun;->a:LX/Aur;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method public final onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 1723216
    const/4 v0, 0x0

    return v0
.end method

.method public final onDown(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 1723231
    const/4 v0, 0x0

    return v0
.end method

.method public final onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 6

    .prologue
    .line 1723217
    iget-object v0, p0, LX/Aun;->a:LX/Aur;

    iget-object v0, v0, LX/Aur;->b:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getTranslationX()F

    move-result v0

    .line 1723218
    iget-object v1, p0, LX/Aun;->a:LX/Aur;

    iget-object v1, v1, LX/Aur;->b:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getTranslationY()F

    move-result v1

    .line 1723219
    iget-object v2, p0, LX/Aun;->a:LX/Aur;

    iget-object v2, v2, LX/Aur;->h:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    .line 1723220
    iget-object v2, p0, LX/Aun;->a:LX/Aur;

    iget-object v2, v2, LX/Aur;->h:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    .line 1723221
    iget-object v2, p0, LX/Aun;->a:LX/Aur;

    iget-object v2, v2, LX/Aur;->c:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getRotation()F

    move-result v2

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v2

    double-to-float v2, v2

    .line 1723222
    iget-object v3, p0, LX/Aun;->a:LX/Aur;

    iget-object v3, v3, LX/Aur;->c:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getRotation()F

    move-result v3

    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    double-to-float v3, v4

    .line 1723223
    mul-float v4, p3, v3

    mul-float v5, p4, v2

    sub-float/2addr v4, v5

    sub-float/2addr v0, v4

    .line 1723224
    mul-float/2addr v3, p4

    mul-float/2addr v2, p3

    add-float/2addr v2, v3

    sub-float/2addr v1, v2

    .line 1723225
    iget-object v2, p0, LX/Aun;->a:LX/Aur;

    iget-object v2, v2, LX/Aur;->j:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    iget-object v3, p0, LX/Aun;->a:LX/Aur;

    iget-object v3, v3, LX/Aur;->b:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getWidth()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    int-to-float v2, v2

    iget-object v3, p0, LX/Aun;->a:LX/Aur;

    iget-object v3, v3, LX/Aur;->j:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    iget-object v4, p0, LX/Aun;->a:LX/Aur;

    iget-object v4, v4, LX/Aur;->b:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    sub-int/2addr v3, v4

    int-to-float v3, v3

    invoke-static {v0, v3}, Ljava/lang/Math;->min(FF)F

    move-result v0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 1723226
    iget-object v2, p0, LX/Aun;->a:LX/Aur;

    iget-object v2, v2, LX/Aur;->j:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    iget-object v3, p0, LX/Aun;->a:LX/Aur;

    iget-object v3, v3, LX/Aur;->b:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    int-to-float v2, v2

    iget-object v3, p0, LX/Aun;->a:LX/Aur;

    iget-object v3, v3, LX/Aur;->j:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    iget-object v4, p0, LX/Aun;->a:LX/Aur;

    iget-object v4, v4, LX/Aur;->b:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getHeight()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    sub-int/2addr v3, v4

    int-to-float v3, v3

    invoke-static {v1, v3}, Ljava/lang/Math;->min(FF)F

    move-result v1

    invoke-static {v2, v1}, Ljava/lang/Math;->max(FF)F

    move-result v1

    .line 1723227
    iget-object v2, p0, LX/Aun;->a:LX/Aur;

    iget-object v2, v2, LX/Aur;->b:Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setTranslationX(F)V

    .line 1723228
    iget-object v0, p0, LX/Aun;->a:LX/Aur;

    iget-object v0, v0, LX/Aur;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setTranslationY(F)V

    .line 1723229
    const/4 v0, 0x1

    return v0
.end method

.method public final onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 1723212
    iget-object v0, p0, LX/Aun;->a:LX/Aur;

    iget-object v0, v0, LX/Aur;->v:LX/Aut;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Aun;->a:LX/Aur;

    iget-object v0, v0, LX/Aur;->b:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 1723213
    iget-object v0, p0, LX/Aun;->a:LX/Aur;

    iget-object v0, v0, LX/Aur;->v:LX/Aut;

    .line 1723214
    iget-object p0, v0, LX/Aut;->a:LX/Auw;

    invoke-virtual {p0}, LX/Auw;->a()V

    .line 1723215
    :cond_0
    const/4 v0, 0x1

    return v0
.end method
