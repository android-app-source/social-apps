.class public LX/Bxv;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field public final a:LX/1Kf;

.field public final b:LX/CEF;

.field public final c:Landroid/app/Activity;

.field public final d:LX/6VI;

.field public final e:Landroid/content/Context;

.field public final f:LX/0ad;


# direct methods
.method public constructor <init>(Landroid/app/Activity;LX/1Kf;Landroid/content/Context;LX/6VI;LX/CEF;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1836351
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1836352
    iput-object p1, p0, LX/Bxv;->c:Landroid/app/Activity;

    .line 1836353
    iput-object p2, p0, LX/Bxv;->a:LX/1Kf;

    .line 1836354
    iput-object p3, p0, LX/Bxv;->e:Landroid/content/Context;

    .line 1836355
    iput-object p4, p0, LX/Bxv;->d:LX/6VI;

    .line 1836356
    iput-object p5, p0, LX/Bxv;->b:LX/CEF;

    .line 1836357
    iput-object p6, p0, LX/Bxv;->f:LX/0ad;

    .line 1836358
    return-void
.end method

.method public static a(LX/0QB;)LX/Bxv;
    .locals 10

    .prologue
    .line 1836359
    const-class v1, LX/Bxv;

    monitor-enter v1

    .line 1836360
    :try_start_0
    sget-object v0, LX/Bxv;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1836361
    sput-object v2, LX/Bxv;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1836362
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1836363
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1836364
    new-instance v3, LX/Bxv;

    invoke-static {v0}, LX/0kU;->b(LX/0QB;)Landroid/app/Activity;

    move-result-object v4

    check-cast v4, Landroid/app/Activity;

    invoke-static {v0}, LX/1Ke;->a(LX/0QB;)LX/1Ke;

    move-result-object v5

    check-cast v5, LX/1Kf;

    const-class v6, Landroid/content/Context;

    invoke-interface {v0, v6}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/Context;

    invoke-static {v0}, LX/6VI;->b(LX/0QB;)LX/6VI;

    move-result-object v7

    check-cast v7, LX/6VI;

    invoke-static {v0}, LX/CEF;->b(LX/0QB;)LX/CEF;

    move-result-object v8

    check-cast v8, LX/CEF;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v9

    check-cast v9, LX/0ad;

    invoke-direct/range {v3 .. v9}, LX/Bxv;-><init>(Landroid/app/Activity;LX/1Kf;Landroid/content/Context;LX/6VI;LX/CEF;LX/0ad;)V

    .line 1836365
    move-object v0, v3

    .line 1836366
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1836367
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Bxv;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1836368
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1836369
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
