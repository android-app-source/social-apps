.class public final LX/BSv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;


# direct methods
.method public constructor <init>(Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;)V
    .locals 0

    .prologue
    .line 1786295
    iput-object p1, p0, LX/BSv;->a:Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x2

    const v2, -0x26b65fcf

    invoke-static {v1, v0, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1786296
    iget-object v2, p0, LX/BSv;->a:Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;

    iget-object v3, p0, LX/BSv;->a:Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;

    iget-boolean v3, v3, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->v:Z

    if-nez v3, :cond_0

    .line 1786297
    :goto_0
    iput-boolean v0, v2, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->v:Z

    .line 1786298
    iget-object v0, p0, LX/BSv;->a:Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;

    invoke-static {v0}, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->h(Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;)V

    .line 1786299
    iget-object v0, p0, LX/BSv;->a:Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;

    iget-object v0, v0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->i:LX/BSm;

    iget-object v2, p0, LX/BSv;->a:Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;

    iget-boolean v2, v2, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->v:Z

    invoke-interface {v0, v2}, LX/BSm;->a(Z)V

    .line 1786300
    iget-object v0, p0, LX/BSv;->a:Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;

    iget-boolean v0, v0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->v:Z

    if-eqz v0, :cond_1

    .line 1786301
    iget-object v0, p0, LX/BSv;->a:Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;

    iget-object v0, v0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->A:LX/0wL;

    iget-object v2, p0, LX/BSv;->a:Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;

    invoke-virtual {v2}, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0813e5

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1786302
    invoke-static {v0, p1, v2}, LX/0wL;->b(LX/0wL;Landroid/view/View;Ljava/lang/CharSequence;)V

    .line 1786303
    :goto_1
    const v0, 0xd6cd48c

    invoke-static {v0, v1}, LX/02F;->a(II)V

    return-void

    .line 1786304
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1786305
    :cond_1
    iget-object v0, p0, LX/BSv;->a:Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;

    iget-object v0, v0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->A:LX/0wL;

    iget-object v2, p0, LX/BSv;->a:Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;

    invoke-virtual {v2}, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0813e4

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1786306
    invoke-static {v0, p1, v2}, LX/0wL;->b(LX/0wL;Landroid/view/View;Ljava/lang/CharSequence;)V

    .line 1786307
    goto :goto_1
.end method
