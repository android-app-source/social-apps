.class public final LX/BhR;
.super LX/0xh;
.source ""


# instance fields
.field public a:I

.field public b:I

.field public c:Landroid/widget/Button;

.field public final synthetic d:Lcom/facebook/datasensitivity/DataSaverBar;


# direct methods
.method public constructor <init>(Lcom/facebook/datasensitivity/DataSaverBar;)V
    .locals 0

    .prologue
    .line 1809164
    iput-object p1, p0, LX/BhR;->d:Lcom/facebook/datasensitivity/DataSaverBar;

    invoke-direct {p0}, LX/0xh;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/datasensitivity/DataSaverBar;B)V
    .locals 0

    .prologue
    .line 1809163
    invoke-direct {p0, p1}, LX/BhR;-><init>(Lcom/facebook/datasensitivity/DataSaverBar;)V

    return-void
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 1809156
    iget-object v0, p0, LX/BhR;->d:Lcom/facebook/datasensitivity/DataSaverBar;

    iget-object v0, v0, Lcom/facebook/datasensitivity/DataSaverBar;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbTextView;->getCurrentTextColor()I

    move-result v0

    .line 1809157
    invoke-static {v0}, Landroid/graphics/Color;->red(I)I

    move-result v1

    invoke-static {v0}, Landroid/graphics/Color;->green(I)I

    move-result v2

    invoke-static {v0}, Landroid/graphics/Color;->blue(I)I

    move-result v0

    invoke-static {p1, v1, v2, v0}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    .line 1809158
    iget-object v1, p0, LX/BhR;->d:Lcom/facebook/datasensitivity/DataSaverBar;

    iget-object v1, v1, Lcom/facebook/datasensitivity/DataSaverBar;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 1809159
    iget-object v0, p0, LX/BhR;->d:Lcom/facebook/datasensitivity/DataSaverBar;

    iget-object v0, v0, Lcom/facebook/datasensitivity/DataSaverBar;->c:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/glyph/GlyphView;->setAlpha(I)V

    .line 1809160
    iget-object v0, p0, LX/BhR;->c:Landroid/widget/Button;

    if-eqz v0, :cond_0

    .line 1809161
    iget-object v0, p0, LX/BhR;->c:Landroid/widget/Button;

    int-to-float v1, p1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setAlpha(F)V

    .line 1809162
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1809150
    const/16 v0, 0xff

    invoke-direct {p0, v0}, LX/BhR;->a(I)V

    .line 1809151
    iget v0, p0, LX/BhR;->a:I

    if-eqz v0, :cond_0

    .line 1809152
    iget-object v0, p0, LX/BhR;->d:Lcom/facebook/datasensitivity/DataSaverBar;

    iget v1, p0, LX/BhR;->a:I

    invoke-static {v0, v1}, Lcom/facebook/datasensitivity/DataSaverBar;->setColorAndNotify(Lcom/facebook/datasensitivity/DataSaverBar;I)V

    .line 1809153
    :cond_0
    const/4 v0, 0x0

    iput v0, p0, LX/BhR;->b:I

    iput v0, p0, LX/BhR;->a:I

    .line 1809154
    const/4 v0, 0x0

    iput-object v0, p0, LX/BhR;->c:Landroid/widget/Button;

    .line 1809155
    return-void
.end method

.method public final a(LX/0wd;)V
    .locals 10

    .prologue
    .line 1809141
    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v0

    double-to-int v0, v0

    invoke-direct {p0, v0}, LX/BhR;->a(I)V

    .line 1809142
    iget v0, p0, LX/BhR;->a:I

    if-eqz v0, :cond_0

    .line 1809143
    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v0

    const-wide v2, 0x406fe00000000000L    # 255.0

    div-double/2addr v0, v2

    .line 1809144
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    sub-double/2addr v2, v0

    .line 1809145
    iget v4, p0, LX/BhR;->b:I

    invoke-static {v4}, Landroid/graphics/Color;->alpha(I)I

    move-result v4

    int-to-double v4, v4

    mul-double/2addr v4, v0

    iget v6, p0, LX/BhR;->a:I

    invoke-static {v6}, Landroid/graphics/Color;->alpha(I)I

    move-result v6

    int-to-double v6, v6

    mul-double/2addr v6, v2

    add-double/2addr v4, v6

    double-to-int v4, v4

    iget v5, p0, LX/BhR;->b:I

    invoke-static {v5}, Landroid/graphics/Color;->red(I)I

    move-result v5

    int-to-double v6, v5

    mul-double/2addr v6, v0

    iget v5, p0, LX/BhR;->a:I

    invoke-static {v5}, Landroid/graphics/Color;->red(I)I

    move-result v5

    int-to-double v8, v5

    mul-double/2addr v8, v2

    add-double/2addr v6, v8

    double-to-int v5, v6

    iget v6, p0, LX/BhR;->b:I

    invoke-static {v6}, Landroid/graphics/Color;->green(I)I

    move-result v6

    int-to-double v6, v6

    mul-double/2addr v6, v0

    iget v8, p0, LX/BhR;->a:I

    invoke-static {v8}, Landroid/graphics/Color;->green(I)I

    move-result v8

    int-to-double v8, v8

    mul-double/2addr v8, v2

    add-double/2addr v6, v8

    double-to-int v6, v6

    iget v7, p0, LX/BhR;->b:I

    invoke-static {v7}, Landroid/graphics/Color;->blue(I)I

    move-result v7

    int-to-double v8, v7

    mul-double/2addr v0, v8

    iget v7, p0, LX/BhR;->a:I

    invoke-static {v7}, Landroid/graphics/Color;->blue(I)I

    move-result v7

    int-to-double v8, v7

    mul-double/2addr v2, v8

    add-double/2addr v0, v2

    double-to-int v0, v0

    invoke-static {v4, v5, v6, v0}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    .line 1809146
    iget-object v1, p0, LX/BhR;->d:Lcom/facebook/datasensitivity/DataSaverBar;

    invoke-static {v1, v0}, Lcom/facebook/datasensitivity/DataSaverBar;->setColorAndNotify(Lcom/facebook/datasensitivity/DataSaverBar;I)V

    .line 1809147
    :cond_0
    return-void
.end method

.method public final b(LX/0wd;)V
    .locals 0

    .prologue
    .line 1809148
    invoke-virtual {p0}, LX/BhR;->a()V

    .line 1809149
    return-void
.end method
