.class public final LX/BbX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLStory;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Lcom/facebook/checkin/socialsearch/feed/SocialSearchImplicitPlaceListAttachmentPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/checkin/socialsearch/feed/SocialSearchImplicitPlaceListAttachmentPartDefinition;Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1800876
    iput-object p1, p0, LX/BbX;->c:Lcom/facebook/checkin/socialsearch/feed/SocialSearchImplicitPlaceListAttachmentPartDefinition;

    iput-object p2, p0, LX/BbX;->a:Lcom/facebook/graphql/model/GraphQLStory;

    iput-object p3, p0, LX/BbX;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x584cc19b

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1800860
    iget-object v1, p0, LX/BbX;->a:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/BbX;->b:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 1800861
    :cond_0
    iget-object v1, p0, LX/BbX;->c:Lcom/facebook/checkin/socialsearch/feed/SocialSearchImplicitPlaceListAttachmentPartDefinition;

    iget-object v1, v1, Lcom/facebook/checkin/socialsearch/feed/SocialSearchImplicitPlaceListAttachmentPartDefinition;->k:LX/0kL;

    new-instance v2, LX/27k;

    const v3, 0x7f0829d5

    invoke-direct {v2, v3}, LX/27k;-><init>(I)V

    invoke-virtual {v1, v2}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 1800862
    const v1, 0x376b6d85

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1800863
    :goto_0
    return-void

    .line 1800864
    :cond_1
    iget-object v1, p0, LX/BbX;->c:Lcom/facebook/checkin/socialsearch/feed/SocialSearchImplicitPlaceListAttachmentPartDefinition;

    iget-object v1, v1, Lcom/facebook/checkin/socialsearch/feed/SocialSearchImplicitPlaceListAttachmentPartDefinition;->g:LX/8y8;

    iget-object v2, p0, LX/BbX;->b:Ljava/lang/String;

    new-instance v3, LX/BbW;

    invoke-direct {v3, p0}, LX/BbW;-><init>(LX/BbX;)V

    .line 1800865
    new-instance v4, LX/4IQ;

    invoke-direct {v4}, LX/4IQ;-><init>()V

    .line 1800866
    const-string v5, "story_id"

    invoke-virtual {v4, v5, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1800867
    move-object v4, v4

    .line 1800868
    new-instance v5, LX/5Hh;

    invoke-direct {v5}, LX/5Hh;-><init>()V

    move-object v5, v5

    .line 1800869
    const-string v6, "input"

    invoke-virtual {v5, v6, v4}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1800870
    iget-object v4, v1, LX/8y8;->b:LX/1Ck;

    const-string v6, "placelist_remove_implicit_attachment"

    iget-object p1, v1, LX/8y8;->a:LX/0tX;

    invoke-static {v5}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    new-instance p1, LX/8y7;

    invoke-direct {p1, v1, v3}, LX/8y7;-><init>(LX/8y8;LX/0TF;)V

    invoke-virtual {v4, v6, v5, p1}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1800871
    iget-object v1, p0, LX/BbX;->c:Lcom/facebook/checkin/socialsearch/feed/SocialSearchImplicitPlaceListAttachmentPartDefinition;

    iget-object v1, v1, Lcom/facebook/checkin/socialsearch/feed/SocialSearchImplicitPlaceListAttachmentPartDefinition;->h:LX/189;

    iget-object v2, p0, LX/BbX;->a:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1800872
    sget-object v3, LX/0Q7;->a:LX/0Px;

    move-object v3, v3

    .line 1800873
    invoke-virtual {v1, v2, v3}, LX/189;->a(Lcom/facebook/graphql/model/GraphQLStory;LX/0Px;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 1800874
    iget-object v2, p0, LX/BbX;->c:Lcom/facebook/checkin/socialsearch/feed/SocialSearchImplicitPlaceListAttachmentPartDefinition;

    iget-object v2, v2, Lcom/facebook/checkin/socialsearch/feed/SocialSearchImplicitPlaceListAttachmentPartDefinition;->i:LX/0bH;

    new-instance v3, LX/1Ne;

    invoke-direct {v3, v1}, LX/1Ne;-><init>(Lcom/facebook/graphql/model/FeedUnit;)V

    invoke-virtual {v2, v3}, LX/0b4;->a(LX/0b7;)V

    .line 1800875
    const v1, -0x204b01ab

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto :goto_0
.end method
