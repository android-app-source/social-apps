.class public final LX/Acv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastDonationBroadcasterCampaignSummaryQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/AXX;

.field public final synthetic b:LX/Acy;


# direct methods
.method public constructor <init>(LX/Acy;LX/AXX;)V
    .locals 0

    .prologue
    .line 1693295
    iput-object p1, p0, LX/Acv;->b:LX/Acy;

    iput-object p2, p0, LX/Acv;->a:LX/AXX;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1693283
    iget-object v0, p0, LX/Acv;->a:LX/AXX;

    const/4 v1, -0x1

    invoke-interface {v0, v1}, LX/AXX;->r_(I)V

    .line 1693284
    iget-object v0, p0, LX/Acv;->b:LX/Acy;

    iget-object v0, v0, LX/Acy;->d:LX/03V;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, LX/Acy;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_graphFailure"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Failed to fetch donation summary for broadcaster"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1693285
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1693286
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1693287
    if-eqz p1, :cond_0

    .line 1693288
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1693289
    if-nez v0, :cond_1

    .line 1693290
    :cond_0
    iget-object v0, p0, LX/Acv;->a:LX/AXX;

    const/4 v1, -0x1

    invoke-interface {v0, v1}, LX/AXX;->r_(I)V

    .line 1693291
    :goto_0
    return-void

    .line 1693292
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1693293
    check-cast v0, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastDonationBroadcasterCampaignSummaryQueryModel;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastDonationBroadcasterCampaignSummaryQueryModel;->j()I

    move-result v0

    .line 1693294
    iget-object v1, p0, LX/Acv;->b:LX/Acy;

    iget-object v1, v1, LX/Acy;->e:Landroid/os/Handler;

    new-instance v2, Lcom/facebook/facecastdisplay/donation/LiveDonationCampaignQueryHelper$4$1;

    invoke-direct {v2, p0, v0}, Lcom/facebook/facecastdisplay/donation/LiveDonationCampaignQueryHelper$4$1;-><init>(LX/Acv;I)V

    const v0, 0x2eba2247

    invoke-static {v1, v2, v0}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto :goto_0
.end method
