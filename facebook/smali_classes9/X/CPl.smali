.class public final LX/CPl;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/CPl;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/CPj;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/CPm;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1884850
    const/4 v0, 0x0

    sput-object v0, LX/CPl;->a:LX/CPl;

    .line 1884851
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/CPl;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1884852
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1884853
    new-instance v0, LX/CPm;

    invoke-direct {v0}, LX/CPm;-><init>()V

    iput-object v0, p0, LX/CPl;->c:LX/CPm;

    .line 1884854
    return-void
.end method

.method public static declared-synchronized q()LX/CPl;
    .locals 2

    .prologue
    .line 1884855
    const-class v1, LX/CPl;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/CPl;->a:LX/CPl;

    if-nez v0, :cond_0

    .line 1884856
    new-instance v0, LX/CPl;

    invoke-direct {v0}, LX/CPl;-><init>()V

    sput-object v0, LX/CPl;->a:LX/CPl;

    .line 1884857
    :cond_0
    sget-object v0, LX/CPl;->a:LX/CPl;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 1884858
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 1884859
    check-cast p2, LX/CPk;

    .line 1884860
    iget-object v0, p2, LX/CPk;->a:LX/CNb;

    iget-object v1, p2, LX/CPk;->b:LX/CNc;

    iget-object v2, p2, LX/CPk;->c:Ljava/util/List;

    const/4 v3, 0x0

    .line 1884861
    const-string v4, "off-action"

    invoke-virtual {v0, v4}, LX/CNb;->b(Ljava/lang/String;)LX/CNb;

    move-result-object v4

    .line 1884862
    const-string v5, "on-action"

    invoke-virtual {v0, v5}, LX/CNb;->b(Ljava/lang/String;)LX/CNb;

    move-result-object v5

    .line 1884863
    if-eqz v4, :cond_1

    .line 1884864
    invoke-static {v4, v1}, LX/CNd;->a(LX/CNb;LX/CNc;)LX/CNe;

    move-result-object v4

    .line 1884865
    :goto_0
    if-eqz v5, :cond_0

    .line 1884866
    invoke-static {v5, v1}, LX/CNd;->a(LX/CNb;LX/CNc;)LX/CNe;

    move-result-object v3

    .line 1884867
    :cond_0
    invoke-static {p1}, LX/AqH;->c(LX/1De;)LX/AqF;

    move-result-object v5

    const-string p0, "on"

    const/4 p2, 0x0

    invoke-virtual {v0, p0, p2}, LX/CNb;->a(Ljava/lang/String;Z)Z

    move-result p0

    invoke-virtual {v5, p0}, LX/AqF;->a(Z)LX/AqF;

    move-result-object v5

    .line 1884868
    const p0, 0x45fbd669

    const/4 p2, 0x2

    new-array p2, p2, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object v3, p2, v1

    const/4 v1, 0x1

    aput-object v4, p2, v1

    invoke-static {p1, p0, p2}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p0

    move-object v3, p0

    .line 1884869
    invoke-virtual {v5, v3}, LX/AqF;->a(LX/1dQ;)LX/AqF;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    .line 1884870
    invoke-static {v3, p1, v0, v2}, LX/CPx;->a(LX/1Di;LX/1De;LX/CNb;Ljava/util/List;)LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 1884871
    return-object v0

    :cond_1
    move-object v4, v3

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1884872
    invoke-static {}, LX/1dS;->b()V

    .line 1884873
    iget v0, p1, LX/1dQ;->b:I

    .line 1884874
    packed-switch v0, :pswitch_data_0

    .line 1884875
    :goto_0
    return-object v4

    .line 1884876
    :pswitch_0
    check-cast p2, LX/Apr;

    .line 1884877
    iget-boolean v2, p2, LX/Apr;->a:Z

    iget-object v0, p1, LX/1dQ;->c:[Ljava/lang/Object;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    check-cast v0, LX/CNe;

    iget-object v1, p1, LX/1dQ;->c:[Ljava/lang/Object;

    const/4 v3, 0x1

    aget-object v1, v1, v3

    check-cast v1, LX/CNe;

    .line 1884878
    if-eqz v2, :cond_1

    if-eqz v0, :cond_1

    .line 1884879
    invoke-interface {v0}, LX/CNe;->a()V

    .line 1884880
    :cond_0
    :goto_1
    goto :goto_0

    .line 1884881
    :cond_1
    if-nez v2, :cond_0

    if-eqz v1, :cond_0

    .line 1884882
    invoke-interface {v1}, LX/CNe;->a()V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x45fbd669
        :pswitch_0
    .end packed-switch
.end method
