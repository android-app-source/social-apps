.class public final LX/Bjp;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageEventCreationPageHostsQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Bis;

.field public final synthetic b:LX/Bjs;


# direct methods
.method public constructor <init>(LX/Bjs;LX/Bis;)V
    .locals 0

    .prologue
    .line 1812846
    iput-object p1, p0, LX/Bjp;->b:LX/Bjs;

    iput-object p2, p0, LX/Bjp;->a:LX/Bis;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1812847
    iget-object v0, p0, LX/Bjp;->a:LX/Bis;

    sget-object v1, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    invoke-interface {v0, v1}, LX/Bis;->a(Ljava/util/List;)V

    .line 1812848
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 1812836
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1812837
    if-eqz p1, :cond_0

    .line 1812838
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1812839
    if-eqz v0, :cond_0

    .line 1812840
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1812841
    check-cast v0, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageEventCreationPageHostsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageEventCreationPageHostsQueryModel;->a()Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageEventCreationPageHostsQueryModel$AdminedPagesModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1812842
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1812843
    check-cast v0, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageEventCreationPageHostsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageEventCreationPageHostsQueryModel;->a()Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageEventCreationPageHostsQueryModel$AdminedPagesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageEventCreationPageHostsQueryModel$AdminedPagesModel;->a()LX/0Px;

    move-result-object v0

    .line 1812844
    iget-object v1, p0, LX/Bjp;->a:LX/Bis;

    invoke-interface {v1, v0}, LX/Bis;->a(Ljava/util/List;)V

    .line 1812845
    :cond_0
    return-void
.end method
