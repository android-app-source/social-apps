.class public final LX/AWS;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public b:Lcom/facebook/privacy/model/SelectablePrivacyData;

.field private c:Lcom/facebook/facecast/model/FacecastFixedPrivacyData;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1682941
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1682942
    return-void
.end method

.method public constructor <init>(Lcom/facebook/facecast/model/FacecastPrivacyData;)V
    .locals 1

    .prologue
    .line 1682943
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1682944
    iget-object v0, p1, Lcom/facebook/facecast/model/FacecastPrivacyData;->a:Ljava/lang/String;

    iput-object v0, p0, LX/AWS;->a:Ljava/lang/String;

    .line 1682945
    iget-object v0, p1, Lcom/facebook/facecast/model/FacecastPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    iput-object v0, p0, LX/AWS;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1682946
    iget-object v0, p1, Lcom/facebook/facecast/model/FacecastPrivacyData;->c:Lcom/facebook/facecast/model/FacecastFixedPrivacyData;

    iput-object v0, p0, LX/AWS;->c:Lcom/facebook/facecast/model/FacecastFixedPrivacyData;

    .line 1682947
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;)LX/AWS;
    .locals 1

    .prologue
    .line 1682938
    new-instance v0, Lcom/facebook/facecast/model/FacecastFixedPrivacyData;

    invoke-direct {v0, p1}, Lcom/facebook/facecast/model/FacecastFixedPrivacyData;-><init>(Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;)V

    move-object v0, v0

    .line 1682939
    iput-object v0, p0, LX/AWS;->c:Lcom/facebook/facecast/model/FacecastFixedPrivacyData;

    .line 1682940
    return-object p0
.end method

.method public final a()Lcom/facebook/facecast/model/FacecastPrivacyData;
    .locals 5

    .prologue
    .line 1682937
    new-instance v0, Lcom/facebook/facecast/model/FacecastPrivacyData;

    iget-object v1, p0, LX/AWS;->a:Ljava/lang/String;

    iget-object v2, p0, LX/AWS;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    iget-object v3, p0, LX/AWS;->c:Lcom/facebook/facecast/model/FacecastFixedPrivacyData;

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/facecast/model/FacecastPrivacyData;-><init>(Ljava/lang/String;Lcom/facebook/privacy/model/SelectablePrivacyData;Lcom/facebook/facecast/model/FacecastFixedPrivacyData;)V

    return-object v0
.end method
