.class public LX/CO7;
.super Landroid/graphics/drawable/Drawable;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field private final b:Landroid/graphics/Paint;

.field public c:LX/CNb;

.field public d:I

.field public e:I

.field public f:I

.field public g:I

.field public h:I

.field public i:I

.field public j:I

.field public k:I

.field public l:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1882597
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 1882598
    iput-object p1, p0, LX/CO7;->a:Landroid/content/Context;

    .line 1882599
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, LX/CO7;->b:Landroid/graphics/Paint;

    .line 1882600
    return-void
.end method


# virtual methods
.method public final draw(Landroid/graphics/Canvas;)V
    .locals 5

    .prologue
    .line 1882601
    invoke-virtual {p0}, LX/CO7;->copyBounds()Landroid/graphics/Rect;

    move-result-object v1

    .line 1882602
    iget-object v0, p0, LX/CO7;->b:Landroid/graphics/Paint;

    iget v2, p0, LX/CO7;->l:I

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 1882603
    iget-object v0, p0, LX/CO7;->b:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1882604
    iget-object v0, p0, LX/CO7;->b:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v0}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 1882605
    iget v0, v1, Landroid/graphics/Rect;->left:I

    iget v2, p0, LX/CO7;->h:I

    add-int/2addr v0, v2

    iput v0, v1, Landroid/graphics/Rect;->left:I

    .line 1882606
    iget v0, v1, Landroid/graphics/Rect;->right:I

    iget v2, p0, LX/CO7;->i:I

    sub-int/2addr v0, v2

    iput v0, v1, Landroid/graphics/Rect;->right:I

    .line 1882607
    iget v0, p0, LX/CO7;->i:I

    iget v2, p0, LX/CO7;->h:I

    if-ge v0, v2, :cond_0

    .line 1882608
    iget v0, p0, LX/CO7;->h:I

    iput v0, p0, LX/CO7;->i:I

    .line 1882609
    :cond_0
    iget v0, v1, Landroid/graphics/Rect;->top:I

    iget v2, p0, LX/CO7;->j:I

    add-int/2addr v0, v2

    iput v0, v1, Landroid/graphics/Rect;->top:I

    .line 1882610
    iget v0, v1, Landroid/graphics/Rect;->bottom:I

    iget v2, p0, LX/CO7;->k:I

    sub-int/2addr v0, v2

    iput v0, v1, Landroid/graphics/Rect;->bottom:I

    .line 1882611
    iget v0, p0, LX/CO7;->k:I

    iget v2, p0, LX/CO7;->j:I

    if-ge v0, v2, :cond_1

    .line 1882612
    iget v0, p0, LX/CO7;->j:I

    iput v0, p0, LX/CO7;->k:I

    .line 1882613
    :cond_1
    const/4 v0, 0x0

    .line 1882614
    iget v2, p0, LX/CO7;->g:I

    if-eqz v2, :cond_2

    .line 1882615
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, v1}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    .line 1882616
    :cond_2
    iget-object v2, p0, LX/CO7;->b:Landroid/graphics/Paint;

    iget v3, p0, LX/CO7;->d:I

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 1882617
    iget-object v2, p0, LX/CO7;->b:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1882618
    iget v2, p0, LX/CO7;->g:I

    if-nez v2, :cond_3

    .line 1882619
    iget-object v2, p0, LX/CO7;->b:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 1882620
    :goto_0
    iget-object v2, p0, LX/CO7;->b:Landroid/graphics/Paint;

    iget v3, p0, LX/CO7;->e:I

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 1882621
    iget-object v2, p0, LX/CO7;->b:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1882622
    iget-object v2, p0, LX/CO7;->b:Landroid/graphics/Paint;

    iget v3, p0, LX/CO7;->f:I

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1882623
    iget v2, p0, LX/CO7;->g:I

    if-nez v2, :cond_4

    .line 1882624
    iget-object v0, p0, LX/CO7;->b:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v0}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 1882625
    :goto_1
    return-void

    .line 1882626
    :cond_3
    iget v2, p0, LX/CO7;->g:I

    int-to-float v2, v2

    iget v3, p0, LX/CO7;->g:I

    int-to-float v3, v3

    iget-object v4, p0, LX/CO7;->b:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v2, v3, v4}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 1882627
    :cond_4
    iget v1, p0, LX/CO7;->g:I

    int-to-float v1, v1

    iget v2, p0, LX/CO7;->g:I

    int-to-float v2, v2

    iget-object v3, p0, LX/CO7;->b:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    goto :goto_1
.end method

.method public final getOpacity()I
    .locals 1

    .prologue
    .line 1882628
    const/4 v0, -0x1

    return v0
.end method

.method public final setAlpha(I)V
    .locals 1

    .prologue
    .line 1882629
    iget-object v0, p0, LX/CO7;->b:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 1882630
    return-void
.end method

.method public final setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 1
    .param p1    # Landroid/graphics/ColorFilter;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1882631
    iget-object v0, p0, LX/CO7;->b:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 1882632
    return-void
.end method
