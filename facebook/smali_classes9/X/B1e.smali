.class public LX/B1e;
.super LX/B1d;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/B1d",
        "<",
        "Lcom/facebook/groups/members/protocol/FetchGroupMembersModels$FetchGroupFriendMemberModel;",
        ">;"
    }
.end annotation


# instance fields
.field private e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljava/lang/Integer;

.field private g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field private i:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/1Ck;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;LX/0tX;LX/B1b;Ljava/lang/String;Ljava/lang/Boolean;)V
    .locals 1
    .param p2    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Ljava/lang/Integer;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/B1b;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .param p8    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1736171
    invoke-direct {p0, p1, p5, p6, p8}, LX/B1d;-><init>(LX/1Ck;LX/0tX;LX/B1b;Ljava/lang/Boolean;)V

    .line 1736172
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1736173
    iput-object v0, p0, LX/B1e;->e:LX/0Px;

    .line 1736174
    iput-object p2, p0, LX/B1e;->g:Ljava/lang/String;

    .line 1736175
    iput-object p7, p0, LX/B1e;->i:Ljava/lang/String;

    .line 1736176
    iput-object p4, p0, LX/B1e;->f:Ljava/lang/Integer;

    .line 1736177
    iput-object p3, p0, LX/B1e;->h:Ljava/lang/String;

    .line 1736178
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/0gW;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0gW",
            "<",
            "Lcom/facebook/groups/members/protocol/FetchGroupMembersModels$FetchGroupFriendMemberModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1736219
    new-instance v0, LX/B1i;

    invoke-direct {v0}, LX/B1i;-><init>()V

    move-object v0, v0

    .line 1736220
    const-string v1, "after_cursor"

    invoke-virtual {v0, v1, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "group_id"

    iget-object v3, p0, LX/B1e;->g:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "search_term"

    iget-object v3, p0, LX/B1e;->h:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "profile_image_size"

    iget-object v3, p0, LX/B1e;->f:Ljava/lang/Integer;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "member_count"

    const-string v3, "12"

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1736221
    return-object v0
.end method

.method public final a(Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/groups/members/protocol/FetchGroupMembersModels$FetchGroupFriendMemberModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 1736184
    new-instance v6, LX/0Pz;

    invoke-direct {v6}, LX/0Pz;-><init>()V

    .line 1736185
    iget-object v0, p0, LX/B1e;->e:LX/0Px;

    invoke-virtual {v6, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 1736186
    if-eqz p1, :cond_2

    .line 1736187
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1736188
    if-eqz v0, :cond_2

    .line 1736189
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1736190
    check-cast v0, Lcom/facebook/groups/members/protocol/FetchGroupMembersModels$FetchGroupFriendMemberModel;

    invoke-virtual {v0}, Lcom/facebook/groups/members/protocol/FetchGroupMembersModels$FetchGroupFriendMemberModel;->a()Lcom/facebook/groups/members/protocol/FetchGroupMembersModels$FetchGroupFriendMemberModel$GroupMemberProfilesModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1736191
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1736192
    check-cast v0, Lcom/facebook/groups/members/protocol/FetchGroupMembersModels$FetchGroupFriendMemberModel;

    invoke-virtual {v0}, Lcom/facebook/groups/members/protocol/FetchGroupMembersModels$FetchGroupFriendMemberModel;->a()Lcom/facebook/groups/members/protocol/FetchGroupMembersModels$FetchGroupFriendMemberModel$GroupMemberProfilesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/members/protocol/FetchGroupMembersModels$FetchGroupFriendMemberModel$GroupMemberProfilesModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1736193
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1736194
    check-cast v0, Lcom/facebook/groups/members/protocol/FetchGroupMembersModels$FetchGroupFriendMemberModel;

    invoke-virtual {v0}, Lcom/facebook/groups/members/protocol/FetchGroupMembersModels$FetchGroupFriendMemberModel;->a()Lcom/facebook/groups/members/protocol/FetchGroupMembersModels$FetchGroupFriendMemberModel$GroupMemberProfilesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/members/protocol/FetchGroupMembersModels$FetchGroupFriendMemberModel$GroupMemberProfilesModel;->a()LX/0Px;

    move-result-object v7

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    move v5, v3

    :goto_0
    if-ge v5, v8, :cond_2

    invoke-virtual {v7, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/members/protocol/FetchGroupMembersModels$FetchGroupFriendMemberModel$GroupMemberProfilesModel$NodesModel;

    .line 1736195
    invoke-virtual {v0}, Lcom/facebook/groups/members/protocol/FetchGroupMembersModels$FetchGroupFriendMemberModel$GroupMemberProfilesModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/groups/members/protocol/FetchGroupMembersModels$FetchGroupFriendMemberModel$GroupMemberProfilesModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v1

    iget-object v9, p0, LX/B1e;->i:Ljava/lang/String;

    invoke-virtual {v1, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1736196
    invoke-virtual {v0}, Lcom/facebook/groups/members/protocol/FetchGroupMembersModels$FetchGroupFriendMemberModel$GroupMemberProfilesModel$NodesModel;->l()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    if-eqz v1, :cond_1

    .line 1736197
    invoke-virtual {v0}, Lcom/facebook/groups/members/protocol/FetchGroupMembersModels$FetchGroupFriendMemberModel$GroupMemberProfilesModel$NodesModel;->l()LX/1vs;

    move-result-object v1

    iget-object v9, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 1736198
    invoke-virtual {v9, v1, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    .line 1736199
    :goto_1
    new-instance v9, LX/0XI;

    invoke-direct {v9}, LX/0XI;-><init>()V

    sget-object v10, LX/0XG;->FACEBOOK:LX/0XG;

    invoke-virtual {v0}, Lcom/facebook/groups/members/protocol/FetchGroupMembersModels$FetchGroupFriendMemberModel$GroupMemberProfilesModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, LX/0XI;->a(LX/0XG;Ljava/lang/String;)LX/0XI;

    move-result-object v9

    invoke-virtual {v0}, Lcom/facebook/groups/members/protocol/FetchGroupMembersModels$FetchGroupFriendMemberModel$GroupMemberProfilesModel$NodesModel;->k()Ljava/lang/String;

    move-result-object v0

    .line 1736200
    iput-object v0, v9, LX/0XI;->h:Ljava/lang/String;

    .line 1736201
    move-object v0, v9

    .line 1736202
    iput-object v1, v0, LX/0XI;->n:Ljava/lang/String;

    .line 1736203
    move-object v0, v0

    .line 1736204
    invoke-virtual {v0}, LX/0XI;->aj()Lcom/facebook/user/model/User;

    move-result-object v0

    invoke-virtual {v6, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1736205
    :cond_0
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_0

    :cond_1
    move-object v1, v2

    .line 1736206
    goto :goto_1

    .line 1736207
    :cond_2
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1736208
    if-eqz v0, :cond_5

    .line 1736209
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1736210
    check-cast v0, Lcom/facebook/groups/members/protocol/FetchGroupMembersModels$FetchGroupFriendMemberModel;

    invoke-virtual {v0}, Lcom/facebook/groups/members/protocol/FetchGroupMembersModels$FetchGroupFriendMemberModel;->a()Lcom/facebook/groups/members/protocol/FetchGroupMembersModels$FetchGroupFriendMemberModel$GroupMemberProfilesModel;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 1736211
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1736212
    check-cast v0, Lcom/facebook/groups/members/protocol/FetchGroupMembersModels$FetchGroupFriendMemberModel;

    invoke-virtual {v0}, Lcom/facebook/groups/members/protocol/FetchGroupMembersModels$FetchGroupFriendMemberModel;->a()Lcom/facebook/groups/members/protocol/FetchGroupMembersModels$FetchGroupFriendMemberModel$GroupMemberProfilesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/members/protocol/FetchGroupMembersModels$FetchGroupFriendMemberModel$GroupMemberProfilesModel;->j()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 1736213
    :goto_2
    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v5

    iput-object v5, p0, LX/B1e;->e:LX/0Px;

    .line 1736214
    if-eqz v0, :cond_3

    invoke-virtual {v1, v0, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    :cond_3
    iput-object v2, p0, LX/B1e;->a:Ljava/lang/String;

    .line 1736215
    if-eqz v0, :cond_4

    invoke-virtual {v1, v0, v4}, LX/15i;->h(II)Z

    move-result v0

    if-eqz v0, :cond_4

    :goto_3
    iput-boolean v3, p0, LX/B1e;->b:Z

    .line 1736216
    invoke-virtual {p0}, LX/B1d;->g()V

    .line 1736217
    return-void

    :cond_4
    move v3, v4

    .line 1736218
    goto :goto_3

    :cond_5
    move v0, v3

    move-object v1, v2

    goto :goto_2
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1736183
    const-string v0, "Group members for group chat fetch failed"

    return-object v0
.end method

.method public final i()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1736182
    iget-object v0, p0, LX/B1e;->e:LX/0Px;

    return-object v0
.end method

.method public final j()V
    .locals 1

    .prologue
    .line 1736179
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1736180
    iput-object v0, p0, LX/B1e;->e:LX/0Px;

    .line 1736181
    return-void
.end method
