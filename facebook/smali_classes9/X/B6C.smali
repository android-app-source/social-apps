.class public final LX/B6C;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/leadgen/LeadGenConfirmationFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/leadgen/LeadGenConfirmationFragment;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1746374
    iput-object p1, p0, LX/B6C;->b:Lcom/facebook/leadgen/LeadGenConfirmationFragment;

    iput-object p2, p0, LX/B6C;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x19a14766

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1746375
    iget-object v1, p0, LX/B6C;->b:Lcom/facebook/leadgen/LeadGenConfirmationFragment;

    iget-object v1, v1, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->b:LX/B6l;

    const-string v2, "cta_lead_gen_visit_offsite_click"

    iget-object v3, p0, LX/B6C;->b:Lcom/facebook/leadgen/LeadGenConfirmationFragment;

    iget v3, v3, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->o:I

    invoke-virtual {v1, v2, v3}, LX/B6l;->a(Ljava/lang/String;I)V

    .line 1746376
    sget-object v1, LX/B6G;->a:[I

    iget-object v2, p0, LX/B6C;->b:Lcom/facebook/leadgen/LeadGenConfirmationFragment;

    iget-object v2, v2, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->t:LX/B7Q;

    .line 1746377
    iget-object v3, v2, LX/B7Q;->i:LX/B7P;

    move-object v2, v3

    .line 1746378
    invoke-virtual {v2}, LX/B7P;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1746379
    :goto_0
    iget-object v1, p0, LX/B6C;->b:Lcom/facebook/leadgen/LeadGenConfirmationFragment;

    iget-object v1, v1, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->e:LX/0if;

    sget-object v2, LX/0ig;->v:LX/0ih;

    invoke-virtual {v1, v2}, LX/0if;->c(LX/0ih;)V

    .line 1746380
    iget-object v1, p0, LX/B6C;->b:Lcom/facebook/leadgen/LeadGenConfirmationFragment;

    iget-object v1, v1, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->a:LX/B6k;

    iget-object v2, p0, LX/B6C;->b:Lcom/facebook/leadgen/LeadGenConfirmationFragment;

    iget-object v2, v2, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->q:LX/B7F;

    .line 1746381
    iget-object v3, v2, LX/B7F;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v2, v3

    .line 1746382
    invoke-virtual {v1, v2}, LX/B6k;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/B6j;

    move-result-object v1

    iget-object v2, p0, LX/B6C;->a:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v1, p1, v2, v3}, LX/B6j;->b(Landroid/view/View;Ljava/lang/String;Z)V

    .line 1746383
    const v1, -0x13b88c26

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 1746384
    :pswitch_0
    iget-object v1, p0, LX/B6C;->b:Lcom/facebook/leadgen/LeadGenConfirmationFragment;

    iget-object v1, v1, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->e:LX/0if;

    sget-object v2, LX/0ig;->v:LX/0ih;

    const-string v3, "cta_lead_gen_visit_offsite_click"

    invoke-virtual {v1, v2, v3}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    goto :goto_0

    .line 1746385
    :pswitch_1
    iget-object v1, p0, LX/B6C;->b:Lcom/facebook/leadgen/LeadGenConfirmationFragment;

    iget-object v1, v1, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->e:LX/0if;

    sget-object v2, LX/0ig;->v:LX/0ih;

    const-string v3, "cta_open_messenger_thread"

    invoke-virtual {v1, v2, v3}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    goto :goto_0

    .line 1746386
    :pswitch_2
    iget-object v1, p0, LX/B6C;->b:Lcom/facebook/leadgen/LeadGenConfirmationFragment;

    iget-object v1, v1, Lcom/facebook/leadgen/LeadGenConfirmationFragment;->e:LX/0if;

    sget-object v2, LX/0ig;->v:LX/0ih;

    const-string v3, "cta_open_call_dialog"

    invoke-virtual {v1, v2, v3}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
