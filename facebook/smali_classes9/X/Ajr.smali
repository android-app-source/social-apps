.class public LX/Ajr;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/1qa;

.field public final b:LX/1VL;

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;>;"
        }
    .end annotation
.end field

.field private final d:LX/Ajk;

.field private final e:Lcom/facebook/common/callercontext/CallerContext;

.field private final f:Lcom/facebook/common/perftest/PerfTestConfig;

.field public final g:LX/03V;

.field public final h:LX/1BM;


# direct methods
.method public constructor <init>(LX/1qa;LX/1VL;Lcom/facebook/common/callercontext/CallerContext;LX/Ajk;Lcom/facebook/common/perftest/PerfTestConfig;LX/03V;LX/1BM;LX/17R;)V
    .locals 0
    .param p3    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/Ajk;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1708451
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1708452
    iput-object p1, p0, LX/Ajr;->a:LX/1qa;

    .line 1708453
    iput-object p2, p0, LX/Ajr;->b:LX/1VL;

    .line 1708454
    iput-object p3, p0, LX/Ajr;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 1708455
    iput-object p4, p0, LX/Ajr;->d:LX/Ajk;

    .line 1708456
    iput-object p5, p0, LX/Ajr;->f:Lcom/facebook/common/perftest/PerfTestConfig;

    .line 1708457
    iput-object p6, p0, LX/Ajr;->g:LX/03V;

    .line 1708458
    iput-object p7, p0, LX/Ajr;->h:LX/1BM;

    .line 1708459
    return-void
.end method

.method public static a(LX/1bf;Lcom/facebook/graphql/model/GraphQLMedia;LX/5g1;)LX/1bf;
    .locals 3

    .prologue
    .line 1708443
    const/4 v0, 0x0

    .line 1708444
    sget-object v1, LX/Ajq;->b:[I

    invoke-virtual {p2}, LX/5g1;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1708445
    :goto_0
    if-eqz v0, :cond_0

    .line 1708446
    invoke-static {p0}, LX/1bX;->a(LX/1bf;)LX/1bX;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/1bX;->b(Landroid/net/Uri;)LX/1bX;

    move-result-object v0

    invoke-virtual {v0}, LX/1bX;->n()LX/1bf;

    move-result-object p0

    .line 1708447
    :cond_0
    return-object p0

    .line 1708448
    :pswitch_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLMedia;->Z()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    goto :goto_0

    .line 1708449
    :pswitch_1
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLMedia;->aa()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    goto :goto_0

    .line 1708450
    :pswitch_2
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLMedia;->V()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private a(LX/Ajk;Lcom/facebook/graphql/model/GraphQLStory;LX/1bf;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3

    .prologue
    .line 1708460
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1708461
    iget-object v0, p0, LX/Ajr;->e:Lcom/facebook/common/callercontext/CallerContext;

    invoke-interface {p1, p3, v0}, LX/Ajk;->a(LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1708462
    :goto_0
    return-object v0

    .line 1708463
    :cond_0
    iget-object v0, p0, LX/Ajr;->h:LX/1BM;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v1

    .line 1708464
    iget-object v2, p3, LX/1bf;->b:Landroid/net/Uri;

    move-object v2, v2

    .line 1708465
    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/1BM;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1708466
    iget-object v0, p0, LX/Ajr;->e:Lcom/facebook/common/callercontext/CallerContext;

    invoke-interface {p1, p3, v0}, LX/Ajk;->a(LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1708467
    if-nez v0, :cond_1

    .line 1708468
    const/4 v0, 0x0

    goto :goto_0

    .line 1708469
    :cond_1
    new-instance v1, LX/Ajp;

    invoke-direct {v1, p0, p3}, LX/Ajp;-><init>(LX/Ajr;LX/1bf;)V

    .line 1708470
    sget-object v2, LX/1fo;->a:LX/1fo;

    move-object v2, v2

    .line 1708471
    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method

.method public static a(LX/Ajr;LX/26P;LX/Ajk;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1bf;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/26P;",
            "LX/Ajk;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "LX/1bf;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture;"
        }
    .end annotation

    .prologue
    .line 1708440
    sget-object v0, LX/26P;->Photo:LX/26P;

    if-eq p1, v0, :cond_0

    sget-object v0, LX/26P;->Album:LX/26P;

    if-eq p1, v0, :cond_0

    sget-object v0, LX/26P;->Share:LX/26P;

    if-eq p1, v0, :cond_0

    sget-object v0, LX/26P;->ShareLargeImage:LX/26P;

    if-ne p1, v0, :cond_1

    .line 1708441
    :cond_0
    invoke-static {p3}, LX/1WF;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-direct {p0, p2, v0, p4}, LX/Ajr;->a(LX/Ajk;Lcom/facebook/graphql/model/GraphQLStory;LX/1bf;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1708442
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, LX/Ajr;->e:Lcom/facebook/common/callercontext/CallerContext;

    invoke-interface {p2, p4, v0}, LX/Ajk;->a(LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/5g1;LX/Ajk;Ljava/util/List;)V
    .locals 10
    .param p4    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;",
            "LX/5g1;",
            "LX/Ajk;",
            "Ljava/util/List",
            "<",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;>;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 1708370
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1708371
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    .line 1708372
    instance-of v1, v0, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;

    if-eqz v1, :cond_0

    move-object v1, v0

    .line 1708373
    check-cast v1, Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;

    .line 1708374
    invoke-static {v1}, LX/25C;->a(Lcom/facebook/graphql/model/GraphQLPaginatedPeopleYouMayKnowFeedUnit;)LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v4, v3

    move v2, v3

    .line 1708375
    :goto_0
    if-ge v4, v6, :cond_0

    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Fa;

    .line 1708376
    invoke-static {v1}, LX/25D;->a(LX/16q;)Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v1

    .line 1708377
    if-eqz v1, :cond_6

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLProfile;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    if-eqz v7, :cond_6

    .line 1708378
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLProfile;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/1bf;->a(Ljava/lang/String;)LX/1bf;

    move-result-object v1

    .line 1708379
    iget-object v7, p0, LX/Ajr;->e:Lcom/facebook/common/callercontext/CallerContext;

    invoke-interface {p3, v1, v7}, LX/Ajk;->a(LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    invoke-static {p4, v1}, LX/Ajr;->a(Ljava/util/List;Lcom/google/common/util/concurrent/ListenableFuture;)V

    .line 1708380
    add-int/lit8 v1, v2, 0x1

    const/4 v2, 0x3

    if-ge v1, v2, :cond_0

    .line 1708381
    :goto_1
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move v2, v1

    goto :goto_0

    .line 1708382
    :cond_0
    instance-of v1, v0, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v1, :cond_5

    .line 1708383
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1708384
    invoke-static {v0}, LX/185;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    .line 1708385
    if-eqz v1, :cond_1

    invoke-static {v1}, LX/1xl;->b(Lcom/facebook/graphql/model/GraphQLActor;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1708386
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/1bf;->a(Ljava/lang/String;)LX/1bf;

    move-result-object v1

    .line 1708387
    invoke-static {p1}, LX/182;->d(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    invoke-direct {p0, p3, v2, v1}, LX/Ajr;->a(LX/Ajk;Lcom/facebook/graphql/model/GraphQLStory;LX/1bf;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    invoke-static {p4, v1}, LX/Ajr;->a(Ljava/util/List;Lcom/google/common/util/concurrent/ListenableFuture;)V

    .line 1708388
    :cond_1
    invoke-static {v0}, LX/17E;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    if-eqz v1, :cond_3

    move v2, v3

    .line 1708389
    :goto_2
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->u()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v2, v1, :cond_3

    .line 1708390
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->u()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->q()Z

    move-result v1

    if-nez v1, :cond_2

    .line 1708391
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->u()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/flatbuffers/Flattenable;

    invoke-virtual {p1, v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 1708392
    const/4 v6, 0x0

    .line 1708393
    iget-object v4, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, v4

    .line 1708394
    check-cast v4, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1708395
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_7

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 1708396
    :goto_3
    sget-object v7, LX/Ajq;->a:[I

    invoke-virtual {v5}, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->ordinal()I

    move-result v5

    aget v5, v7, v5

    packed-switch v5, :pswitch_data_0

    .line 1708397
    :cond_2
    :goto_4
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_2

    .line 1708398
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 1708399
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    invoke-direct {p0, v1, p2, p3, p4}, LX/Ajr;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/5g1;LX/Ajk;Ljava/util/List;)V

    .line 1708400
    :cond_4
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aP()I

    move-result v1

    if-lez v1, :cond_5

    .line 1708401
    :goto_5
    invoke-static {v0}, LX/16y;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;->j()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-ge v3, v1, :cond_5

    .line 1708402
    invoke-static {v0}, LX/16y;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;->j()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/flatbuffers/Flattenable;

    invoke-virtual {p1, v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    invoke-direct {p0, v1, p2, p3, p4}, LX/Ajr;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/5g1;LX/Ajk;Ljava/util/List;)V

    .line 1708403
    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    .line 1708404
    :cond_5
    return-void

    :cond_6
    move v1, v2

    goto/16 :goto_1

    .line 1708405
    :cond_7
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object v5

    invoke-virtual {v5, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    goto :goto_3

    .line 1708406
    :pswitch_0
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v5

    if-eqz v5, :cond_10

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLMedia;->aS()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_10

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLMedia;->aq()Z

    move-result v5

    if-eqz v5, :cond_10

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    if-eqz v5, :cond_10

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLMedia;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v5

    if-eqz v5, :cond_10

    const v5, 0x4ed245b

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLMedia;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v6

    if-ne v5, v6, :cond_10

    const/4 v5, 0x1

    :goto_6
    move v5, v5

    .line 1708407
    if-eqz v5, :cond_8

    .line 1708408
    sget-object v5, LX/26P;->Video:LX/26P;

    .line 1708409
    :goto_7
    iget-object v6, p0, LX/Ajr;->a:LX/1qa;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v7

    invoke-virtual {v6, v7, v5}, LX/1qa;->b(Lcom/facebook/graphql/model/GraphQLMedia;LX/26P;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v6

    .line 1708410
    if-eqz v6, :cond_2

    .line 1708411
    invoke-static {v6}, LX/1qa;->a(Lcom/facebook/graphql/model/GraphQLImage;)LX/1bf;

    move-result-object v6

    .line 1708412
    sget-object v7, LX/5g1;->OFF:LX/5g1;

    if-eq p2, v7, :cond_e

    .line 1708413
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v4

    invoke-static {v6, v4, p2}, LX/Ajr;->a(LX/1bf;Lcom/facebook/graphql/model/GraphQLMedia;LX/5g1;)LX/1bf;

    move-result-object v4

    .line 1708414
    :goto_8
    invoke-static {p0, v5, p3, v1, v4}, LX/Ajr;->a(LX/Ajr;LX/26P;LX/Ajk;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1bf;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    invoke-static {p4, v4}, LX/Ajr;->a(Ljava/util/List;Lcom/google/common/util/concurrent/ListenableFuture;)V

    goto/16 :goto_4

    .line 1708415
    :cond_8
    const v5, 0x4fb5732f

    invoke-static {v4, v5}, LX/1VX;->b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 1708416
    sget-object v5, LX/26P;->AddFriend:LX/26P;

    goto :goto_7

    .line 1708417
    :cond_9
    sget-object v5, LX/26P;->Share:LX/26P;

    goto :goto_7

    .line 1708418
    :pswitch_1
    sget-object v5, LX/26P;->ShareLargeImage:LX/26P;

    goto :goto_7

    .line 1708419
    :pswitch_2
    sget-object v5, LX/26P;->Photo:LX/26P;

    goto :goto_7

    .line 1708420
    :pswitch_3
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v5

    if-eqz v5, :cond_2

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_2

    .line 1708421
    iget-object v5, p0, LX/Ajr;->b:LX/1VL;

    invoke-virtual {v5, v4}, LX/1VL;->f(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v7

    .line 1708422
    if-eqz v7, :cond_b

    .line 1708423
    iget-object v4, p0, LX/Ajr;->b:LX/1VL;

    invoke-virtual {v4, v1}, LX/1VL;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0Px;

    move-result-object v4

    iput-object v4, p0, LX/Ajr;->c:Ljava/util/List;

    .line 1708424
    :cond_a
    iget-object v4, p0, LX/Ajr;->c:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_9
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1708425
    iget-object v5, v4, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, v5

    .line 1708426
    check-cast v4, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1708427
    if-eqz v7, :cond_c

    iget-object v5, p0, LX/Ajr;->a:LX/1qa;

    invoke-virtual {v5, v1, v4}, LX/1qa;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/1bf;

    move-result-object v5

    .line 1708428
    :goto_a
    if-nez v5, :cond_d

    .line 1708429
    iget-object v5, p0, LX/Ajr;->g:LX/03V;

    const-string v6, "FeedImagePreloader"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Null imageParams: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v6, v4}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 1708430
    :cond_b
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, p0, LX/Ajr;->c:Ljava/util/List;

    .line 1708431
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v5

    const/4 v8, 0x2

    invoke-static {v5, v8}, Ljava/lang/Math;->min(II)I

    move-result v8

    .line 1708432
    :goto_b
    if-ge v6, v8, :cond_a

    .line 1708433
    iget-object v9, p0, LX/Ajr;->c:Ljava/util/List;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v5

    invoke-virtual {v5, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/flatbuffers/Flattenable;

    invoke-virtual {v1, v5}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v5

    invoke-interface {v9, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1708434
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    goto :goto_b

    .line 1708435
    :cond_c
    iget-object v5, p0, LX/Ajr;->a:LX/1qa;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v8

    sget-object v9, LX/26P;->Album:LX/26P;

    invoke-virtual {v5, v8, v9}, LX/1qa;->a(Lcom/facebook/graphql/model/GraphQLMedia;LX/26P;)LX/1bf;

    move-result-object v5

    goto :goto_a

    .line 1708436
    :cond_d
    sget-object v8, LX/5g1;->OFF:LX/5g1;

    if-eq p2, v8, :cond_f

    .line 1708437
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v4

    invoke-static {v5, v4, p2}, LX/Ajr;->a(LX/1bf;Lcom/facebook/graphql/model/GraphQLMedia;LX/5g1;)LX/1bf;

    move-result-object v4

    .line 1708438
    :goto_c
    sget-object v5, LX/26P;->Album:LX/26P;

    invoke-static {p0, v5, p3, v1, v4}, LX/Ajr;->a(LX/Ajr;LX/26P;LX/Ajk;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1bf;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    invoke-static {p4, v4}, LX/Ajr;->a(Ljava/util/List;Lcom/google/common/util/concurrent/ListenableFuture;)V

    goto/16 :goto_9

    .line 1708439
    :pswitch_4
    sget-object v5, LX/26P;->AvatarList:LX/26P;

    goto/16 :goto_7

    :cond_e
    move-object v4, v6

    goto/16 :goto_8

    :cond_f
    move-object v4, v5

    goto :goto_c

    :cond_10
    const/4 v5, 0x0

    goto/16 :goto_6

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static a(Ljava/util/List;Lcom/google/common/util/concurrent/ListenableFuture;)V
    .locals 0
    .param p0    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Lcom/google/common/util/concurrent/ListenableFuture;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;>;",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 1708367
    if-eqz p1, :cond_0

    if-eqz p0, :cond_0

    .line 1708368
    invoke-interface {p0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1708369
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/5g1;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;",
            "LX/5g1;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;>;"
        }
    .end annotation

    .prologue
    .line 1708362
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1708363
    sget-boolean v1, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->h:Z

    move v1, v1

    .line 1708364
    if-eqz v1, :cond_0

    .line 1708365
    :goto_0
    return-object v0

    .line 1708366
    :cond_0
    iget-object v1, p0, LX/Ajr;->d:LX/Ajk;

    invoke-direct {p0, p1, p2, v1, v0}, LX/Ajr;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/5g1;LX/Ajk;Ljava/util/List;)V

    goto :goto_0
.end method
