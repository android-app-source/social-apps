.class public final LX/AaN;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/AaP;


# direct methods
.method public constructor <init>(LX/AaP;)V
    .locals 0

    .prologue
    .line 1688428
    iput-object p1, p0, LX/AaN;->a:LX/AaP;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1688429
    iget-object v0, p0, LX/AaN;->a:LX/AaP;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/AaP;->setPaymentInfo(LX/AaP;Lcom/facebook/payments/paymentmethods/model/PaymentMethod;)V

    .line 1688430
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 1688431
    check-cast p1, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;

    .line 1688432
    sget-object v0, LX/6zQ;->NEW_PAYPAL:LX/6zQ;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 1688433
    iget-object v1, p1, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;->e:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v3

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_1

    iget-object v1, p1, Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;->e:LX/0Px;

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/payments/paymentmethods/model/PaymentMethod;

    .line 1688434
    invoke-interface {v1}, Lcom/facebook/payments/paymentmethods/model/PaymentMethod;->b()LX/6zU;

    move-result-object v4

    invoke-virtual {v4}, LX/6zU;->toNewPaymentOptionType()LX/6zQ;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1688435
    :goto_1
    move-object v0, v1

    .line 1688436
    iget-object v1, p0, LX/AaN;->a:LX/AaP;

    invoke-static {v1, v0}, LX/AaP;->setPaymentInfo(LX/AaP;Lcom/facebook/payments/paymentmethods/model/PaymentMethod;)V

    .line 1688437
    return-void

    .line 1688438
    :cond_0
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 1688439
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method
