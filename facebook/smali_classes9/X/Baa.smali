.class public LX/Baa;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1qM;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/11H;

.field public final c:LX/Bab;

.field public final d:LX/0Xl;

.field public final e:LX/0aU;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/11H;LX/Bab;LX/0Xl;LX/0aU;)V
    .locals 0
    .param p4    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/CrossFbProcessBroadcast;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1799272
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1799273
    iput-object p1, p0, LX/Baa;->a:Landroid/content/Context;

    .line 1799274
    iput-object p2, p0, LX/Baa;->b:LX/11H;

    .line 1799275
    iput-object p3, p0, LX/Baa;->c:LX/Bab;

    .line 1799276
    iput-object p4, p0, LX/Baa;->d:LX/0Xl;

    .line 1799277
    iput-object p5, p0, LX/Baa;->e:LX/0aU;

    .line 1799278
    return-void
.end method

.method public static a(LX/0QB;)LX/Baa;
    .locals 9

    .prologue
    .line 1799279
    const-class v1, LX/Baa;

    monitor-enter v1

    .line 1799280
    :try_start_0
    sget-object v0, LX/Baa;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1799281
    sput-object v2, LX/Baa;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1799282
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1799283
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1799284
    new-instance v3, LX/Baa;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->a(LX/0QB;)Lcom/facebook/http/protocol/SingleMethodRunnerImpl;

    move-result-object v5

    check-cast v5, LX/11H;

    .line 1799285
    new-instance v6, LX/Bab;

    invoke-direct {v6}, LX/Bab;-><init>()V

    .line 1799286
    move-object v6, v6

    .line 1799287
    move-object v6, v6

    .line 1799288
    check-cast v6, LX/Bab;

    invoke-static {v0}, LX/0a5;->a(LX/0QB;)LX/0a5;

    move-result-object v7

    check-cast v7, LX/0Xl;

    invoke-static {v0}, LX/0aU;->a(LX/0QB;)LX/0aU;

    move-result-object v8

    check-cast v8, LX/0aU;

    invoke-direct/range {v3 .. v8}, LX/Baa;-><init>(Landroid/content/Context;LX/11H;LX/Bab;LX/0Xl;LX/0aU;)V

    .line 1799289
    move-object v0, v3

    .line 1799290
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1799291
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Baa;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1799292
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1799293
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 4

    .prologue
    .line 1799294
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v0, v0

    .line 1799295
    const-string v1, "background_location_update_settings"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1799296
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 1799297
    const-string v1, "BackgroundLocationUpdateSettingsParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/backgroundlocation/settings/write/BackgroundLocationUpdateSettingsParams;

    .line 1799298
    iget-object v1, p0, LX/Baa;->b:LX/11H;

    iget-object v2, p0, LX/Baa;->c:LX/Bab;

    .line 1799299
    iget-object v3, p1, LX/1qK;->mCallerContext:Lcom/facebook/common/callercontext/CallerContext;

    move-object v3, v3

    .line 1799300
    invoke-virtual {v1, v2, v0, v3}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 1799301
    if-eqz v1, :cond_1

    .line 1799302
    iget-object v1, v0, Lcom/facebook/backgroundlocation/settings/write/BackgroundLocationUpdateSettingsParams;->b:LX/0am;

    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, v0, Lcom/facebook/backgroundlocation/settings/write/BackgroundLocationUpdateSettingsParams;->a:LX/0am;

    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v1

    if-nez v1, :cond_2

    .line 1799303
    :goto_0
    iget-object v1, v0, Lcom/facebook/backgroundlocation/settings/write/BackgroundLocationUpdateSettingsParams;->a:LX/0am;

    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v1

    if-nez v1, :cond_3

    .line 1799304
    :goto_1
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 1799305
    :goto_2
    move-object v0, v0

    .line 1799306
    return-object v0

    .line 1799307
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected operation type "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    sget-object v0, LX/1nY;->API_ERROR:LX/1nY;

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_2

    .line 1799308
    :cond_2
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, LX/Baa;->e:LX/0aU;

    const-string v3, "NEARBY_FRIENDS_SETTINGS_CHANGED_ACTION"

    invoke-virtual {v2, v3}, LX/0aU;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1799309
    iget-object v2, p0, LX/Baa;->a:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0

    .line 1799310
    :cond_3
    iget-object v1, v0, Lcom/facebook/backgroundlocation/settings/write/BackgroundLocationUpdateSettingsParams;->a:LX/0am;

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 1799311
    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.facebook.backgroundlocation.reporting.OLD_SETTINGS_CHANGED_ACTION"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1799312
    const-string v3, "expected_location_history_setting"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1799313
    iget-object v3, p0, LX/Baa;->d:LX/0Xl;

    invoke-interface {v3, v2}, LX/0Xl;->a(Landroid/content/Intent;)V

    .line 1799314
    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, LX/Baa;->e:LX/0aU;

    const-string p1, "BACKGROUND_LOCATION_REPORTING_SETTINGS_REQUEST_REFRESH_ACTION"

    invoke-virtual {v3, p1}, LX/0aU;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1799315
    const-string v3, "expected_location_history_setting"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1799316
    iget-object v1, p0, LX/Baa;->a:Landroid/content/Context;

    invoke-virtual {v1, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_1
.end method
