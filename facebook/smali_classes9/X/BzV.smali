.class public final LX/BzV;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/BzX;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/BzW;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/BzX",
            "<TE;>.CoverPhotoShareComponentImpl;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/BzX;

.field private c:[Ljava/lang/String;

.field private d:I

.field private e:Ljava/util/BitSet;


# direct methods
.method public constructor <init>(LX/BzX;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 1838790
    iput-object p1, p0, LX/BzV;->b:LX/BzX;

    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 1838791
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "attachmentProps"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "environment"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/BzV;->c:[Ljava/lang/String;

    .line 1838792
    iput v3, p0, LX/BzV;->d:I

    .line 1838793
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/BzV;->d:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/BzV;->e:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/BzV;LX/1De;IILX/BzW;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "II",
            "LX/BzX",
            "<TE;>.CoverPhotoShareComponentImpl;)V"
        }
    .end annotation

    .prologue
    .line 1838794
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 1838795
    iput-object p4, p0, LX/BzV;->a:LX/BzW;

    .line 1838796
    iget-object v0, p0, LX/BzV;->e:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 1838797
    return-void
.end method


# virtual methods
.method public final a(LX/1Po;)LX/BzV;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)",
            "LX/BzX",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 1838798
    iget-object v0, p0, LX/BzV;->a:LX/BzW;

    iput-object p1, v0, LX/BzW;->b:LX/1Po;

    .line 1838799
    iget-object v0, p0, LX/BzV;->e:Ljava/util/BitSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1838800
    return-object p0
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/BzV;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)",
            "LX/BzX",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 1838801
    iget-object v0, p0, LX/BzV;->a:LX/BzW;

    iput-object p1, v0, LX/BzW;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1838802
    iget-object v0, p0, LX/BzV;->e:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1838803
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 1838804
    invoke-super {p0}, LX/1X5;->a()V

    .line 1838805
    const/4 v0, 0x0

    iput-object v0, p0, LX/BzV;->a:LX/BzW;

    .line 1838806
    iget-object v0, p0, LX/BzV;->b:LX/BzX;

    iget-object v0, v0, LX/BzX;->b:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 1838807
    return-void
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/BzX;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1838808
    iget-object v1, p0, LX/BzV;->e:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/BzV;->e:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/BzV;->d:I

    if-ge v1, v2, :cond_2

    .line 1838809
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1838810
    :goto_0
    iget v2, p0, LX/BzV;->d:I

    if-ge v0, v2, :cond_1

    .line 1838811
    iget-object v2, p0, LX/BzV;->e:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1838812
    iget-object v2, p0, LX/BzV;->c:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1838813
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1838814
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1838815
    :cond_2
    iget-object v0, p0, LX/BzV;->a:LX/BzW;

    .line 1838816
    invoke-virtual {p0}, LX/BzV;->a()V

    .line 1838817
    return-object v0
.end method
