.class public LX/Bba;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:Landroid/content/Context;

.field public final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0ad;

.field public final e:LX/03V;

.field public final f:Ljava/lang/Boolean;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1800910
    const-class v0, LX/Bba;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Bba;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0Or;LX/0ad;LX/03V;Ljava/lang/Boolean;)V
    .locals 0
    .param p5    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Or",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/0ad;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "Ljava/lang/Boolean;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1800911
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1800912
    iput-object p1, p0, LX/Bba;->b:Landroid/content/Context;

    .line 1800913
    iput-object p2, p0, LX/Bba;->c:LX/0Or;

    .line 1800914
    iput-object p3, p0, LX/Bba;->d:LX/0ad;

    .line 1800915
    iput-object p4, p0, LX/Bba;->e:LX/03V;

    .line 1800916
    iput-object p5, p0, LX/Bba;->f:Ljava/lang/Boolean;

    .line 1800917
    return-void
.end method

.method public static a(LX/0QB;)LX/Bba;
    .locals 7

    .prologue
    .line 1800918
    new-instance v1, LX/Bba;

    const-class v2, Landroid/content/Context;

    invoke-interface {p0, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    const/16 v3, 0x455

    invoke-static {p0, v3}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-static {p0}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v6

    check-cast v6, Ljava/lang/Boolean;

    invoke-direct/range {v1 .. v6}, LX/Bba;-><init>(Landroid/content/Context;LX/0Or;LX/0ad;LX/03V;Ljava/lang/Boolean;)V

    .line 1800919
    move-object v0, v1

    .line 1800920
    return-object v0
.end method
