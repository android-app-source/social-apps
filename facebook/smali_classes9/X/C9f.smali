.class public final LX/C9f;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/1Pq;

.field public final synthetic b:Lcom/facebook/feedplugins/offline/rows/OfflineProgressPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/offline/rows/OfflineProgressPartDefinition;LX/1Pq;)V
    .locals 0

    .prologue
    .line 1854339
    iput-object p1, p0, LX/C9f;->b:Lcom/facebook/feedplugins/offline/rows/OfflineProgressPartDefinition;

    iput-object p2, p0, LX/C9f;->a:LX/1Pq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1854340
    iget-object v0, p0, LX/C9f;->b:Lcom/facebook/feedplugins/offline/rows/OfflineProgressPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/offline/rows/OfflineProgressPartDefinition;->g:LX/03V;

    sget-object v1, Lcom/facebook/feedplugins/offline/rows/OfflineProgressPartDefinition;->c:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Unexpected Error in Offline Progress"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1854341
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 1854342
    iget-object v0, p0, LX/C9f;->a:LX/1Pq;

    invoke-interface {v0}, LX/1Pq;->iN_()V

    .line 1854343
    return-void
.end method
