.class public final LX/C4h;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

.field public final synthetic b:Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentAttachmentPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentAttachmentPartDefinition;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V
    .locals 0

    .prologue
    .line 1847738
    iput-object p1, p0, LX/C4h;->b:Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentAttachmentPartDefinition;

    iput-object p2, p0, LX/C4h;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x7d48d46a

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1847739
    iget-object v1, p0, LX/C4h;->b:Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentAttachmentPartDefinition;

    iget-object v1, v1, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentAttachmentPartDefinition;->i:LX/2mJ;

    iget-object v2, p0, LX/C4h;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/2mJ;->a(Landroid/net/Uri;)V

    .line 1847740
    iget-object v1, p0, LX/C4h;->b:Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentAttachmentPartDefinition;

    iget-object v1, v1, Lcom/facebook/feedplugins/goodwill/dailydialogue/CulturalMomentAttachmentPartDefinition;->c:LX/1Cn;

    iget-object v2, p0, LX/C4h;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v2

    const-string v3, "shared_story"

    .line 1847741
    new-instance p0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object p1, LX/3lw;->GOODWILL_DAILY_DIALOGUE_CTA_CLICKED:LX/3lw;

    iget-object p1, p1, LX/3lw;->name:Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p1, "goodwill"

    .line 1847742
    iput-object p1, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1847743
    move-object p0, p0

    .line 1847744
    const-string p1, "cultural_moment_id"

    invoke-virtual {p0, p1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string p1, "source"

    invoke-virtual {p0, p1, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    .line 1847745
    iget-object p1, v1, LX/1Cn;->a:LX/0Zb;

    invoke-interface {p1, p0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1847746
    const v1, -0x2663fd22

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
