.class public LX/BV5;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/Runnable;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public final b:Landroid/os/Handler;

.field public final c:Ljava/lang/Object;

.field public final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/BV4;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:J


# direct methods
.method public constructor <init>(ZLX/0Ot;LX/0Ot;)V
    .locals 2
    .param p1    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/0Ot;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .param p3    # LX/0Ot;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "LX/0Ot",
            "<",
            "Landroid/os/Handler;",
            ">;",
            "LX/0Ot",
            "<",
            "Landroid/os/Handler;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1790166
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1790167
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, LX/BV5;->c:Ljava/lang/Object;

    .line 1790168
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/BV5;->d:Ljava/util/Map;

    .line 1790169
    new-instance v0, Lcom/facebook/video/util/BatchStoryCollectionUpdater$BatchUpdateRunnable;

    invoke-direct {v0, p0}, Lcom/facebook/video/util/BatchStoryCollectionUpdater$BatchUpdateRunnable;-><init>(LX/BV5;)V

    iput-object v0, p0, LX/BV5;->a:Ljava/lang/Runnable;

    .line 1790170
    if-eqz p1, :cond_0

    invoke-interface {p3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    :goto_0
    iput-object v0, p0, LX/BV5;->b:Landroid/os/Handler;

    .line 1790171
    if-eqz p1, :cond_1

    const-wide/16 v0, 0x64

    :goto_1
    iput-wide v0, p0, LX/BV5;->f:J

    .line 1790172
    return-void

    .line 1790173
    :cond_0
    invoke-interface {p2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    goto :goto_0

    .line 1790174
    :cond_1
    const-wide/16 v0, 0x32

    goto :goto_1
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 8

    .prologue
    .line 1790175
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v0

    .line 1790176
    if-nez v0, :cond_0

    .line 1790177
    const-string v0, "BatchStoryCollectionUpdater"

    const-string v1, "Cannot add to batch update story with null id"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1790178
    :goto_0
    return-void

    .line 1790179
    :cond_0
    iget-object v1, p0, LX/BV5;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 1790180
    :try_start_0
    iget-object v2, p0, LX/BV5;->d:Ljava/util/Map;

    invoke-interface {v2, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1790181
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1790182
    iget-object v3, p0, LX/BV5;->b:Landroid/os/Handler;

    iget-object v4, p0, LX/BV5;->a:Ljava/lang/Runnable;

    invoke-static {v3, v4}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1790183
    iget-object v3, p0, LX/BV5;->b:Landroid/os/Handler;

    iget-object v4, p0, LX/BV5;->a:Ljava/lang/Runnable;

    iget-wide v5, p0, LX/BV5;->f:J

    const v7, 0x1ddec272

    invoke-static {v3, v4, v5, v6, v7}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 1790184
    goto :goto_0

    .line 1790185
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
