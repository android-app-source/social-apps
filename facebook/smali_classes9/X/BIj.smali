.class public LX/BIj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/BIi;


# instance fields
.field private a:Landroid/database/Cursor;

.field private b:LX/8I4;

.field private c:LX/8I2;


# direct methods
.method public constructor <init>(Landroid/database/Cursor;LX/8I4;LX/8I2;)V
    .locals 0
    .param p1    # Landroid/database/Cursor;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1770872
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1770873
    iput-object p1, p0, LX/BIj;->a:Landroid/database/Cursor;

    .line 1770874
    iput-object p2, p0, LX/BIj;->b:LX/8I4;

    .line 1770875
    iput-object p3, p0, LX/BIj;->c:LX/8I2;

    .line 1770876
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1770877
    iget-object v0, p0, LX/BIj;->a:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1770878
    const/4 v0, 0x0

    .line 1770879
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/BIj;->a:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    goto :goto_0
.end method

.method public final a(I)Lcom/facebook/photos/taggablegallery/PhotoGalleryContent;
    .locals 7

    .prologue
    .line 1770880
    iget-object v0, p0, LX/BIj;->a:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 1770881
    iget-object v0, p0, LX/BIj;->a:Landroid/database/Cursor;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 1770882
    iget-object v0, p0, LX/BIj;->c:LX/8I2;

    invoke-virtual {v0, v2, v3}, LX/8I2;->a(J)Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    .line 1770883
    if-eqz v0, :cond_0

    .line 1770884
    check-cast v0, Lcom/facebook/photos/base/media/PhotoItem;

    .line 1770885
    :goto_0
    invoke-static {v0}, LX/BIa;->a(Lcom/facebook/photos/base/media/PhotoItem;)LX/BIa;

    move-result-object v0

    invoke-virtual {v0}, LX/BIa;->a()Lcom/facebook/photos/taggablegallery/PhotoGalleryContent;

    move-result-object v0

    return-object v0

    .line 1770886
    :cond_0
    iget-object v1, p0, LX/BIj;->c:LX/8I2;

    iget-object v4, p0, LX/BIj;->a:Landroid/database/Cursor;

    const/4 v6, 0x5

    move v5, p1

    invoke-virtual/range {v1 .. v6}, LX/8I2;->a(JLandroid/database/Cursor;II)Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/base/media/PhotoItem;

    goto :goto_0
.end method

.method public final a(Lcom/facebook/ipc/media/MediaIdKey;)Ljava/lang/Integer;
    .locals 6
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1770887
    iget-object v0, p0, LX/BIj;->a:Landroid/database/Cursor;

    const/4 v1, -0x1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 1770888
    :cond_0
    iget-object v0, p0, LX/BIj;->a:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1770889
    iget-object v0, p0, LX/BIj;->a:Landroid/database/Cursor;

    .line 1770890
    const-string v4, "_id"

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    move-wide v0, v4

    .line 1770891
    iget-wide v4, p1, Lcom/facebook/ipc/media/MediaIdKey;->b:J

    move-wide v2, v4

    .line 1770892
    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 1770893
    iget-object v0, p0, LX/BIj;->a:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getPosition()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 1770894
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
