.class public final LX/Ccx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/places/graphql/PlacesGraphQLInterfaces$FBCitySearchQuery;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0TF;

.field public final synthetic b:Lcom/facebook/places/create/citypicker/FetchCityRunner;


# direct methods
.method public constructor <init>(Lcom/facebook/places/create/citypicker/FetchCityRunner;LX/0TF;)V
    .locals 0

    .prologue
    .line 1921841
    iput-object p1, p0, LX/Ccx;->b:Lcom/facebook/places/create/citypicker/FetchCityRunner;

    iput-object p2, p0, LX/Ccx;->a:LX/0TF;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1921830
    iget-object v0, p0, LX/Ccx;->a:LX/0TF;

    invoke-interface {v0, p1}, LX/0TF;->onFailure(Ljava/lang/Throwable;)V

    .line 1921831
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 1921832
    check-cast p1, Lcom/facebook/places/graphql/PlacesGraphQLModels$FBCitySearchQueryModel;

    .line 1921833
    invoke-virtual {p1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$FBCitySearchQueryModel;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$FBCitySearchQueryModel$PlaceResultsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$FBCitySearchQueryModel$PlaceResultsModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v2

    .line 1921834
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v3

    .line 1921835
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$FBCitySearchQueryModel$PlaceResultsModel$EdgesModel;

    .line 1921836
    if-eqz v0, :cond_0

    .line 1921837
    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$FBCitySearchQueryModel$PlaceResultsModel$EdgesModel;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1921838
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1921839
    :cond_1
    iget-object v0, p0, LX/Ccx;->a:LX/0TF;

    invoke-interface {v0, v3}, LX/0TF;->onSuccess(Ljava/lang/Object;)V

    .line 1921840
    return-void
.end method
