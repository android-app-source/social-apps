.class public LX/C2c;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pd;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Ps;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public final a:LX/AEd;

.field public final b:LX/AmS;

.field private final c:LX/17Q;


# direct methods
.method public constructor <init>(LX/AEd;LX/AmS;LX/17Q;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1844445
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1844446
    iput-object p1, p0, LX/C2c;->a:LX/AEd;

    .line 1844447
    iput-object p2, p0, LX/C2c;->b:LX/AmS;

    .line 1844448
    iput-object p3, p0, LX/C2c;->c:LX/17Q;

    .line 1844449
    return-void
.end method

.method public static a(LX/0QB;)LX/C2c;
    .locals 6

    .prologue
    .line 1844450
    const-class v1, LX/C2c;

    monitor-enter v1

    .line 1844451
    :try_start_0
    sget-object v0, LX/C2c;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1844452
    sput-object v2, LX/C2c;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1844453
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1844454
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1844455
    new-instance p0, LX/C2c;

    invoke-static {v0}, LX/AEd;->a(LX/0QB;)LX/AEd;

    move-result-object v3

    check-cast v3, LX/AEd;

    invoke-static {v0}, LX/AmS;->b(LX/0QB;)LX/AmS;

    move-result-object v4

    check-cast v4, LX/AmS;

    invoke-static {v0}, LX/17Q;->a(LX/0QB;)LX/17Q;

    move-result-object v5

    check-cast v5, LX/17Q;

    invoke-direct {p0, v3, v4, v5}, LX/C2c;-><init>(LX/AEd;LX/AmS;LX/17Q;)V

    .line 1844456
    move-object v0, p0

    .line 1844457
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1844458
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C2c;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1844459
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1844460
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLPage;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)",
            "Lcom/facebook/graphql/model/GraphQLPage;"
        }
    .end annotation

    .prologue
    .line 1844461
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1844462
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    const v1, -0x22a42d2a    # -9.8999738E17f

    invoke-static {v0, v1}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->as()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    return-object v0
.end method
