.class public LX/AvW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/89u;


# instance fields
.field public final a:LX/Arh;

.field private final b:LX/AsK;

.field public final c:LX/AsL;

.field private final d:LX/8GN;

.field private final e:LX/86g;

.field private f:LX/AvU;

.field public g:Ljava/lang/String;

.field public h:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;

.field public i:Z

.field private j:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/photos/creativeediting/model/SwipeableParams;",
            ">;"
        }
    .end annotation
.end field

.field public k:Z

.field private l:Lcom/facebook/photos/creativeediting/model/SwipeableParams;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Z


# direct methods
.method public constructor <init>(LX/Arh;LX/AsK;LX/AsL;LX/AvU;LX/8GN;LX/86g;LX/0fO;)V
    .locals 1
    .param p1    # LX/Arh;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/AsK;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/AsL;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1724195
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1724196
    iput-object p1, p0, LX/AvW;->a:LX/Arh;

    .line 1724197
    iput-object p2, p0, LX/AvW;->b:LX/AsK;

    .line 1724198
    iput-object p3, p0, LX/AvW;->c:LX/AsL;

    .line 1724199
    iput-object p4, p0, LX/AvW;->f:LX/AvU;

    .line 1724200
    iput-object p5, p0, LX/AvW;->d:LX/8GN;

    .line 1724201
    iput-object p6, p0, LX/AvW;->e:LX/86g;

    .line 1724202
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/AvW;->j:Ljava/util/HashMap;

    .line 1724203
    invoke-static {p7}, LX/0fO;->y(LX/0fO;)LX/0i8;

    move-result-object v0

    invoke-virtual {v0}, LX/0i8;->i()Z

    move-result v0

    move v0, v0

    .line 1724204
    iput-boolean v0, p0, LX/AvW;->m:Z

    .line 1724205
    return-void
.end method

.method private i()Lcom/facebook/photos/creativeediting/model/SwipeableParams;
    .locals 1

    .prologue
    .line 1724192
    iget-object v0, p0, LX/AvW;->l:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    if-nez v0, :cond_0

    .line 1724193
    iget-object v0, p0, LX/AvW;->g:Ljava/lang/String;

    invoke-static {v0}, LX/8GN;->a(Ljava/lang/String;)Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    move-result-object v0

    iput-object v0, p0, LX/AvW;->l:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    .line 1724194
    :cond_0
    iget-object v0, p0, LX/AvW;->l:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    return-object v0
.end method


# virtual methods
.method public final a()LX/86d;
    .locals 1

    .prologue
    .line 1724191
    iget-object v0, p0, LX/AvW;->e:LX/86g;

    return-object v0
.end method

.method public final a(Lcom/facebook/friendsharing/inspiration/model/InspirationModel;)V
    .locals 3

    .prologue
    .line 1724186
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->getId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/AvW;->g:Ljava/lang/String;

    .line 1724187
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->getFrame()Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig;->getFrame()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;

    move-result-object v0

    iput-object v0, p0, LX/AvW;->h:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;

    .line 1724188
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/AvW;->i:Z

    .line 1724189
    iget-object v0, p0, LX/AvW;->f:LX/AvU;

    iget-object v1, p0, LX/AvW;->h:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;

    new-instance v2, LX/AvV;

    invoke-direct {v2, p0}, LX/AvV;-><init>(LX/AvW;)V

    invoke-virtual {v0, v1, v2}, LX/AvU;->a(Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;LX/89p;)V

    .line 1724190
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 1724185
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1724181
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/AvW;->k:Z

    .line 1724182
    iget-boolean v0, p0, LX/AvW;->m:Z

    if-eqz v0, :cond_0

    .line 1724183
    iget-object v0, p0, LX/AvW;->a:LX/Arh;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/Arh;->a(Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;)V

    .line 1724184
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 1724173
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/AvW;->k:Z

    .line 1724174
    iget-boolean v0, p0, LX/AvW;->i:Z

    if-nez v0, :cond_1

    .line 1724175
    iget-object v0, p0, LX/AvW;->c:LX/AsL;

    iget-object v1, p0, LX/AvW;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/AsL;->a(Ljava/lang/String;)V

    .line 1724176
    iget-object v0, p0, LX/AvW;->c:LX/AsL;

    invoke-virtual {v0}, LX/AsL;->c()V

    .line 1724177
    iget-boolean v0, p0, LX/AvW;->m:Z

    if-eqz v0, :cond_0

    .line 1724178
    iget-object v0, p0, LX/AvW;->a:LX/Arh;

    iget-object v1, p0, LX/AvW;->h:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;

    invoke-virtual {v0, v1}, LX/Arh;->a(Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;)V

    .line 1724179
    :cond_0
    :goto_0
    return-void

    .line 1724180
    :cond_1
    iget-object v0, p0, LX/AvW;->c:LX/AsL;

    invoke-virtual {v0}, LX/AsL;->b()V

    goto :goto_0
.end method

.method public final d()Lcom/facebook/photos/creativeediting/model/SwipeableParams;
    .locals 8

    .prologue
    .line 1724150
    iget-boolean v0, p0, LX/AvW;->m:Z

    if-nez v0, :cond_2

    .line 1724151
    iget-boolean v0, p0, LX/AvW;->i:Z

    if-eqz v0, :cond_0

    .line 1724152
    invoke-direct {p0}, LX/AvW;->i()Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    move-result-object v0

    .line 1724153
    :goto_0
    return-object v0

    .line 1724154
    :cond_0
    iget-object v0, p0, LX/AvW;->b:LX/AsK;

    .line 1724155
    iget-object v1, v0, LX/AsK;->a:LX/AsM;

    iget v1, v1, LX/AsM;->o:I

    move v2, v1

    .line 1724156
    iget-object v0, p0, LX/AvW;->b:LX/AsK;

    .line 1724157
    iget-object v1, v0, LX/AsK;->a:LX/AsM;

    iget v1, v1, LX/AsM;->p:I

    move v3, v1

    .line 1724158
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 1724159
    iget-object v0, p0, LX/AvW;->j:Ljava/util/HashMap;

    invoke-virtual {v0, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1724160
    iget-object v7, p0, LX/AvW;->j:Ljava/util/HashMap;

    iget-object v0, p0, LX/AvW;->d:LX/8GN;

    iget-object v1, p0, LX/AvW;->h:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;

    const-string v4, ""

    iget-object v5, p0, LX/AvW;->g:Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, LX/8GN;->a(Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;IILjava/lang/String;Ljava/lang/String;)Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    move-result-object v0

    invoke-virtual {v7, v6, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1724161
    :cond_1
    iget-object v0, p0, LX/AvW;->j:Ljava/util/HashMap;

    invoke-virtual {v0, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    goto :goto_0

    .line 1724162
    :cond_2
    invoke-direct {p0}, LX/AvW;->i()Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    move-result-object v0

    goto :goto_0
.end method

.method public final e()Landroid/net/Uri;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1724172
    const/4 v0, 0x0

    return-object v0
.end method

.method public final f()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1724169
    iget-boolean v1, p0, LX/AvW;->i:Z

    if-nez v1, :cond_0

    .line 1724170
    iget-object v1, p0, LX/AvW;->h:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;->l()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$InstructionsModel;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v0, p0, LX/AvW;->h:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;->l()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$InstructionsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$InstructionsModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 1724171
    :cond_0
    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1724166
    iget-boolean v1, p0, LX/AvW;->i:Z

    if-nez v1, :cond_0

    .line 1724167
    iget-object v1, p0, LX/AvW;->h:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;->b()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$AttributionTextModel;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v0, p0, LX/AvW;->h:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;->b()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$AttributionTextModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$AttributionTextModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 1724168
    :cond_0
    return-object v0
.end method

.method public final h()Landroid/net/Uri;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1724163
    iget-boolean v1, p0, LX/AvW;->i:Z

    if-nez v1, :cond_0

    .line 1724164
    iget-object v1, p0, LX/AvW;->h:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;->c()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$AttributionThumbnailModel;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v0, p0, LX/AvW;->h:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;->c()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$AttributionThumbnailModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel$AttributionThumbnailModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1724165
    :cond_0
    return-object v0
.end method
