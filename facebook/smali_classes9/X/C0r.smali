.class public LX/C0r;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pb;",
        ":",
        "LX/1Pc;",
        ":",
        "LX/1Pd;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public final a:LX/1xN;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1xN",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final b:LX/1WM;

.field public final c:LX/Cch;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/Cch",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1xN;LX/1WM;LX/Cch;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1841556
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1841557
    iput-object p1, p0, LX/C0r;->a:LX/1xN;

    .line 1841558
    iput-object p2, p0, LX/C0r;->b:LX/1WM;

    .line 1841559
    iput-object p3, p0, LX/C0r;->c:LX/Cch;

    .line 1841560
    return-void
.end method

.method public static a(LX/C0r;LX/1bf;)LX/1bf;
    .locals 2

    .prologue
    .line 1841561
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p1}, LX/1bX;->a(LX/1bf;)LX/1bX;

    move-result-object v0

    iget-object v1, p0, LX/C0r;->b:LX/1WM;

    invoke-virtual {v1}, LX/1WM;->a()LX/33B;

    move-result-object v1

    .line 1841562
    iput-object v1, v0, LX/1bX;->j:LX/33B;

    .line 1841563
    move-object v0, v0

    .line 1841564
    invoke-virtual {v0}, LX/1bX;->n()LX/1bf;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/C0r;
    .locals 6

    .prologue
    .line 1841565
    const-class v1, LX/C0r;

    monitor-enter v1

    .line 1841566
    :try_start_0
    sget-object v0, LX/C0r;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1841567
    sput-object v2, LX/C0r;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1841568
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1841569
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1841570
    new-instance p0, LX/C0r;

    invoke-static {v0}, LX/1xN;->a(LX/0QB;)LX/1xN;

    move-result-object v3

    check-cast v3, LX/1xN;

    invoke-static {v0}, LX/1WM;->a(LX/0QB;)LX/1WM;

    move-result-object v4

    check-cast v4, LX/1WM;

    invoke-static {v0}, LX/Cch;->a(LX/0QB;)LX/Cch;

    move-result-object v5

    check-cast v5, LX/Cch;

    invoke-direct {p0, v3, v4, v5}, LX/C0r;-><init>(LX/1xN;LX/1WM;LX/Cch;)V

    .line 1841571
    move-object v0, p0

    .line 1841572
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1841573
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C0r;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1841574
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1841575
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
