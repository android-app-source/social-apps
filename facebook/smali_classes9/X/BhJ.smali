.class public LX/BhJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Bgq;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/Bgq",
        "<",
        "Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;",
        "LX/97f;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:LX/Bg8;

.field private final c:LX/03V;

.field public final d:LX/BfJ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1808991
    const-class v0, LX/BhJ;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/BhJ;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/BfJ;LX/03V;LX/Bg8;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1808986
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1808987
    iput-object p1, p0, LX/BhJ;->d:LX/BfJ;

    .line 1808988
    iput-object p2, p0, LX/BhJ;->c:LX/03V;

    .line 1808989
    iput-object p3, p0, LX/BhJ;->b:LX/Bg8;

    .line 1808990
    return-void
.end method

.method public static a(Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;LX/97f;LX/BgS;)LX/97f;
    .locals 2

    .prologue
    .line 1808977
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1}, LX/Bgb;->d(LX/97f;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1808978
    :goto_0
    return-object p1

    .line 1808979
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->getText()Ljava/lang/String;

    move-result-object v0

    .line 1808980
    invoke-static {p1}, LX/Bgb;->f(LX/97f;)Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;->a(Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;)Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;

    move-result-object v1

    .line 1808981
    if-nez v1, :cond_1

    .line 1808982
    new-instance v1, LX/97m;

    invoke-direct {v1}, LX/97m;-><init>()V

    invoke-virtual {v1}, LX/97m;->a()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;

    move-result-object v1

    .line 1808983
    :cond_1
    invoke-static {v1, v0}, LX/BgV;->a(Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;Ljava/lang/String;)Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;

    move-result-object v1

    .line 1808984
    invoke-static {p1, v1}, LX/BgV;->a(LX/97f;Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;)LX/97f;

    move-result-object v1

    move-object p1, v1

    .line 1808985
    invoke-interface {p2, p1}, LX/BgS;->a(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private static a(LX/BhJ;Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;LX/97f;LX/BgS;LX/BeD;Landroid/support/v4/app/Fragment;)V
    .locals 6

    .prologue
    .line 1808972
    sget-object v0, LX/BhI;->b:[I

    invoke-virtual {p4}, LX/BeD;->getInputStyle()LX/BeC;

    move-result-object v1

    invoke-virtual {v1}, LX/BeC;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1808973
    iget-object v0, p0, LX/BhJ;->c:LX/03V;

    sget-object v1, LX/BhJ;->a:Ljava/lang/String;

    const-string v2, "Input style not supported by field"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1808974
    :goto_0
    return-void

    .line 1808975
    :pswitch_0
    new-instance v0, LX/BhG;

    invoke-direct {v0, p0, p1, p2, p3}, LX/BhG;-><init>(LX/BhJ;Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;LX/97f;LX/BgS;)V

    invoke-virtual {p1, v0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    goto :goto_0

    .line 1808976
    :pswitch_1
    new-instance v0, LX/BhH;

    move-object v1, p0

    move-object v2, p2

    move-object v3, p4

    move-object v4, p3

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, LX/BhH;-><init>(LX/BhJ;LX/97f;LX/BeD;LX/BgS;Landroid/support/v4/app/Fragment;)V

    invoke-virtual {p1, v0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->setFieldOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a(Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;LX/97f;LX/BgS;LX/BgK;LX/BeD;Landroid/support/v4/app/Fragment;)V
    .locals 4
    .param p5    # LX/BeD;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1808992
    invoke-virtual {p1}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->a()V

    .line 1808993
    iget-object v0, p0, LX/BhJ;->b:LX/Bg8;

    .line 1808994
    iget-object v1, p1, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->c:Landroid/widget/ImageView;

    move-object v1, v1

    .line 1808995
    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, p3, v2}, LX/Bg8;->a(Landroid/view/View;LX/97f;LX/BgS;LX/Bh0;)Landroid/view/View$OnClickListener;

    move-result-object v0

    .line 1808996
    invoke-virtual {p1, v0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->setActionButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1808997
    sget-object v1, LX/BhI;->a:[I

    invoke-interface {p2}, LX/97e;->e()Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1808998
    iget-object v0, p0, LX/BhJ;->c:LX/03V;

    sget-object v1, LX/BhJ;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Trying to bind view with unsupported option selected: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p2}, LX/97e;->e()Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1808999
    :goto_0
    return-void

    .line 1809000
    :pswitch_0
    invoke-static/range {p0 .. p6}, LX/BhJ;->b(LX/BhJ;Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;LX/97f;LX/BgS;LX/BgK;LX/BeD;Landroid/support/v4/app/Fragment;)V

    goto :goto_0

    .line 1809001
    :pswitch_1
    invoke-static {p2}, LX/Bgb;->i(LX/97f;)Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsOptionModel;

    move-result-object v1

    .line 1809002
    invoke-virtual {v1}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsOptionModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->setFieldText(Ljava/lang/String;)V

    .line 1809003
    invoke-static {p2}, LX/Bgb;->a(LX/97f;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->setIcon(Ljava/lang/String;)V

    .line 1809004
    invoke-virtual {p1, v0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->setFieldOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1809005
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static b(LX/BhJ;Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;LX/97f;LX/BgS;LX/BgK;LX/BeD;Landroid/support/v4/app/Fragment;)V
    .locals 6
    .param p4    # LX/BgK;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # LX/BeD;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1808965
    invoke-interface {p2}, LX/97e;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->setFieldHintText(Ljava/lang/CharSequence;)V

    .line 1808966
    invoke-static {p2}, LX/Bgb;->d(LX/97f;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->setFieldText(Ljava/lang/String;)V

    .line 1808967
    invoke-static {p2}, LX/Bgb;->a(LX/97f;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->setIcon(Ljava/lang/String;)V

    .line 1808968
    new-instance v0, LX/BhF;

    invoke-direct {v0, p0, p4, p1}, LX/BhF;-><init>(LX/BhJ;LX/BgK;Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;)V

    invoke-virtual {p1, v0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->setTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1808969
    if-eqz p5, :cond_0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p5

    move-object v5, p6

    .line 1808970
    invoke-static/range {v0 .. v5}, LX/BhJ;->a(LX/BhJ;Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;LX/97f;LX/BgS;LX/BeD;Landroid/support/v4/app/Fragment;)V

    .line 1808971
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()LX/BeE;
    .locals 1

    .prologue
    .line 1808959
    sget-object v0, LX/BeE;->TEXT_FIELD:LX/BeE;

    return-object v0
.end method

.method public final a(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 1808962
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1808963
    const v1, 0x7f031428

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;

    .line 1808964
    return-object v0
.end method

.method public final bridge synthetic a(Landroid/view/View;Ljava/lang/Object;LX/BgS;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1808961
    check-cast p1, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;

    check-cast p2, LX/97f;

    invoke-static {p1, p2, p3}, LX/BhJ;->a(Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;LX/97f;LX/BgS;)LX/97f;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Landroid/view/View;Ljava/lang/Object;Ljava/lang/Object;LX/BgS;LX/BgK;LX/BeD;Landroid/support/v4/app/Fragment;Ljava/lang/String;)V
    .locals 7
    .param p3    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # LX/BeD;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1808960
    move-object v1, p1

    check-cast v1, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;

    move-object v2, p2

    check-cast v2, LX/97f;

    move-object v0, p0

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    move-object v6, p7

    invoke-direct/range {v0 .. v6}, LX/BhJ;->a(Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;LX/97f;LX/BgS;LX/BgK;LX/BeD;Landroid/support/v4/app/Fragment;)V

    return-void
.end method
