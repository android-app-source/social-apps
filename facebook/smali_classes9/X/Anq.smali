.class public LX/Anq;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Anr;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1712950
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/Anq;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/Anr;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1712951
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1712952
    iput-object p1, p0, LX/Anq;->b:LX/0Ot;

    .line 1712953
    return-void
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 3

    .prologue
    .line 1712954
    check-cast p2, LX/Anp;

    .line 1712955
    iget-object v0, p0, LX/Anq;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Anr;

    iget-object v1, p2, LX/Anp;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    const/4 p2, 0x2

    .line 1712956
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, p2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    const p0, 0x7f020140

    invoke-interface {v2, p0}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, p2}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v2

    invoke-static {v1}, LX/16z;->o(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result p0

    const p2, 0x7f0f0060

    invoke-static {v0, p1, p0, p2}, LX/Anr;->a(LX/Anr;LX/1De;II)LX/1Dg;

    move-result-object p0

    invoke-interface {v2, p0}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v2

    invoke-static {v1}, LX/16z;->e(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result p0

    const p2, 0x7f0f0064

    invoke-static {v0, p1, p0, p2}, LX/Anr;->a(LX/Anr;LX/1De;II)LX/1Dg;

    move-result-object p0

    invoke-interface {v2, p0}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v2

    invoke-static {v1}, LX/16z;->l(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result p0

    const p2, 0x7f0f0063

    invoke-static {v0, p1, p0, p2}, LX/Anr;->a(LX/Anr;LX/1De;II)LX/1Dg;

    move-result-object p0

    invoke-interface {v2, p0}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v2

    const/4 p0, 0x6

    const p2, 0x7f0b0971

    invoke-interface {v2, p0, p2}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v2

    const/4 p0, 0x1

    const p2, 0x7f0b0972

    invoke-interface {v2, p0, p2}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v2

    const/4 p0, 0x3

    const p2, 0x7f0b0974

    invoke-interface {v2, p0, p2}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 1712957
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1712958
    invoke-static {}, LX/1dS;->b()V

    .line 1712959
    const/4 v0, 0x0

    return-object v0
.end method
