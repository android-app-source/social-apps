.class public final LX/CBY;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/CBZ;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/1Pn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public b:I

.field public c:I

.field public d:Landroid/view/View$OnClickListener;

.field public e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field public final synthetic f:LX/CBZ;


# direct methods
.method public constructor <init>(LX/CBZ;)V
    .locals 1

    .prologue
    .line 1856512
    iput-object p1, p0, LX/CBY;->f:LX/CBZ;

    .line 1856513
    move-object v0, p1

    .line 1856514
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1856515
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1856537
    const-string v0, "QuickPromotionSocialContextHScrollComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1856516
    if-ne p0, p1, :cond_1

    .line 1856517
    :cond_0
    :goto_0
    return v0

    .line 1856518
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1856519
    goto :goto_0

    .line 1856520
    :cond_3
    check-cast p1, LX/CBY;

    .line 1856521
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1856522
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1856523
    if-eq v2, v3, :cond_0

    .line 1856524
    iget-object v2, p0, LX/CBY;->a:LX/1Pn;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/CBY;->a:LX/1Pn;

    iget-object v3, p1, LX/CBY;->a:LX/1Pn;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1856525
    goto :goto_0

    .line 1856526
    :cond_5
    iget-object v2, p1, LX/CBY;->a:LX/1Pn;

    if-nez v2, :cond_4

    .line 1856527
    :cond_6
    iget v2, p0, LX/CBY;->b:I

    iget v3, p1, LX/CBY;->b:I

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 1856528
    goto :goto_0

    .line 1856529
    :cond_7
    iget v2, p0, LX/CBY;->c:I

    iget v3, p1, LX/CBY;->c:I

    if-eq v2, v3, :cond_8

    move v0, v1

    .line 1856530
    goto :goto_0

    .line 1856531
    :cond_8
    iget-object v2, p0, LX/CBY;->d:Landroid/view/View$OnClickListener;

    if-eqz v2, :cond_a

    iget-object v2, p0, LX/CBY;->d:Landroid/view/View$OnClickListener;

    iget-object v3, p1, LX/CBY;->d:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    :cond_9
    move v0, v1

    .line 1856532
    goto :goto_0

    .line 1856533
    :cond_a
    iget-object v2, p1, LX/CBY;->d:Landroid/view/View$OnClickListener;

    if-nez v2, :cond_9

    .line 1856534
    :cond_b
    iget-object v2, p0, LX/CBY;->e:LX/0Px;

    if-eqz v2, :cond_c

    iget-object v2, p0, LX/CBY;->e:LX/0Px;

    iget-object v3, p1, LX/CBY;->e:LX/0Px;

    invoke-virtual {v2, v3}, LX/0Px;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1856535
    goto :goto_0

    .line 1856536
    :cond_c
    iget-object v2, p1, LX/CBY;->e:LX/0Px;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
