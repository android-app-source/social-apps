.class public final LX/CHK;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 49

    .prologue
    .line 1867204
    const/16 v45, 0x0

    .line 1867205
    const/16 v44, 0x0

    .line 1867206
    const/16 v43, 0x0

    .line 1867207
    const/16 v42, 0x0

    .line 1867208
    const/16 v41, 0x0

    .line 1867209
    const/16 v40, 0x0

    .line 1867210
    const/16 v39, 0x0

    .line 1867211
    const/16 v38, 0x0

    .line 1867212
    const/16 v37, 0x0

    .line 1867213
    const/16 v36, 0x0

    .line 1867214
    const/16 v35, 0x0

    .line 1867215
    const/16 v34, 0x0

    .line 1867216
    const/16 v33, 0x0

    .line 1867217
    const/16 v32, 0x0

    .line 1867218
    const/16 v31, 0x0

    .line 1867219
    const/16 v30, 0x0

    .line 1867220
    const/16 v29, 0x0

    .line 1867221
    const/16 v28, 0x0

    .line 1867222
    const/16 v27, 0x0

    .line 1867223
    const/16 v26, 0x0

    .line 1867224
    const/16 v25, 0x0

    .line 1867225
    const/16 v24, 0x0

    .line 1867226
    const/16 v23, 0x0

    .line 1867227
    const/16 v22, 0x0

    .line 1867228
    const/16 v21, 0x0

    .line 1867229
    const/16 v20, 0x0

    .line 1867230
    const/16 v19, 0x0

    .line 1867231
    const/16 v18, 0x0

    .line 1867232
    const/16 v17, 0x0

    .line 1867233
    const/16 v16, 0x0

    .line 1867234
    const/4 v15, 0x0

    .line 1867235
    const/4 v14, 0x0

    .line 1867236
    const/4 v13, 0x0

    .line 1867237
    const/4 v12, 0x0

    .line 1867238
    const/4 v11, 0x0

    .line 1867239
    const/4 v10, 0x0

    .line 1867240
    const/4 v9, 0x0

    .line 1867241
    const/4 v8, 0x0

    .line 1867242
    const/4 v7, 0x0

    .line 1867243
    const/4 v6, 0x0

    .line 1867244
    const/4 v5, 0x0

    .line 1867245
    const/4 v4, 0x0

    .line 1867246
    const/4 v3, 0x0

    .line 1867247
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v46

    sget-object v47, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v46

    move-object/from16 v1, v47

    if-eq v0, v1, :cond_1

    .line 1867248
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1867249
    const/4 v3, 0x0

    .line 1867250
    :goto_0
    return v3

    .line 1867251
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1867252
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v46

    sget-object v47, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v46

    move-object/from16 v1, v47

    if-eq v0, v1, :cond_2a

    .line 1867253
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v46

    .line 1867254
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1867255
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v47

    sget-object v48, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v47

    move-object/from16 v1, v48

    if-eq v0, v1, :cond_1

    if-eqz v46, :cond_1

    .line 1867256
    const-string v47, "__type__"

    invoke-virtual/range {v46 .. v47}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-nez v47, :cond_2

    const-string v47, "__typename"

    invoke-virtual/range {v46 .. v47}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_3

    .line 1867257
    :cond_2
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v45

    move-object/from16 v0, p1

    move-object/from16 v1, v45

    invoke-virtual {v0, v1}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v45

    goto :goto_1

    .line 1867258
    :cond_3
    const-string v47, "attribution_annotation"

    invoke-virtual/range {v46 .. v47}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_4

    .line 1867259
    invoke-static/range {p0 .. p1}, LX/8aj;->a(LX/15w;LX/186;)I

    move-result v44

    goto :goto_1

    .line 1867260
    :cond_4
    const-string v47, "audio_play_mode"

    invoke-virtual/range {v46 .. v47}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_5

    .line 1867261
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v43

    invoke-static/range {v43 .. v43}, Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLAudioAnnotationPlayMode;

    move-result-object v43

    move-object/from16 v0, p1

    move-object/from16 v1, v43

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v43

    goto :goto_1

    .line 1867262
    :cond_5
    const-string v47, "audio_title"

    invoke-virtual/range {v46 .. v47}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_6

    .line 1867263
    invoke-static/range {p0 .. p1}, LX/8aj;->a(LX/15w;LX/186;)I

    move-result v42

    goto :goto_1

    .line 1867264
    :cond_6
    const-string v47, "audio_url"

    invoke-virtual/range {v46 .. v47}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_7

    .line 1867265
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v41

    move-object/from16 v0, p1

    move-object/from16 v1, v41

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v41

    goto/16 :goto_1

    .line 1867266
    :cond_7
    const-string v47, "base_url"

    invoke-virtual/range {v46 .. v47}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_8

    .line 1867267
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v40

    move-object/from16 v0, p1

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v40

    goto/16 :goto_1

    .line 1867268
    :cond_8
    const-string v47, "block_title"

    invoke-virtual/range {v46 .. v47}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_9

    .line 1867269
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v39

    move-object/from16 v0, p1

    move-object/from16 v1, v39

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v39

    goto/16 :goto_1

    .line 1867270
    :cond_9
    const-string v47, "blocks"

    invoke-virtual/range {v46 .. v47}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_a

    .line 1867271
    invoke-static/range {p0 .. p1}, LX/8al;->b(LX/15w;LX/186;)I

    move-result v38

    goto/16 :goto_1

    .line 1867272
    :cond_a
    const-string v47, "copyright_annotation"

    invoke-virtual/range {v46 .. v47}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_b

    .line 1867273
    invoke-static/range {p0 .. p1}, LX/8aj;->a(LX/15w;LX/186;)I

    move-result v37

    goto/16 :goto_1

    .line 1867274
    :cond_b
    const-string v47, "display_height"

    invoke-virtual/range {v46 .. v47}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_c

    .line 1867275
    const/4 v5, 0x1

    .line 1867276
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v36

    goto/16 :goto_1

    .line 1867277
    :cond_c
    const-string v47, "display_width"

    invoke-virtual/range {v46 .. v47}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_d

    .line 1867278
    const/4 v4, 0x1

    .line 1867279
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v35

    goto/16 :goto_1

    .line 1867280
    :cond_d
    const-string v47, "document_element_type"

    invoke-virtual/range {v46 .. v47}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_e

    .line 1867281
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v34

    invoke-static/range {v34 .. v34}, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    move-result-object v34

    move-object/from16 v0, p1

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v34

    goto/16 :goto_1

    .line 1867282
    :cond_e
    const-string v47, "element_text"

    invoke-virtual/range {v46 .. v47}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_f

    .line 1867283
    invoke-static/range {p0 .. p1}, LX/8al;->a(LX/15w;LX/186;)I

    move-result v33

    goto/16 :goto_1

    .line 1867284
    :cond_f
    const-string v47, "element_video"

    invoke-virtual/range {v46 .. v47}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_10

    .line 1867285
    invoke-static/range {p0 .. p1}, LX/8ZV;->a(LX/15w;LX/186;)I

    move-result v32

    goto/16 :goto_1

    .line 1867286
    :cond_10
    const-string v47, "enable_ad_network_bridging"

    invoke-virtual/range {v46 .. v47}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_11

    .line 1867287
    const/4 v3, 0x1

    .line 1867288
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v31

    goto/16 :goto_1

    .line 1867289
    :cond_11
    const-string v47, "feedback"

    invoke-virtual/range {v46 .. v47}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_12

    .line 1867290
    invoke-static/range {p0 .. p1}, LX/2bG;->a(LX/15w;LX/186;)I

    move-result v30

    goto/16 :goto_1

    .line 1867291
    :cond_12
    const-string v47, "feedback_options"

    invoke-virtual/range {v46 .. v47}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_13

    .line 1867292
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v29

    invoke-static/range {v29 .. v29}, Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLDocumentFeedbackOptions;

    move-result-object v29

    move-object/from16 v0, p1

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v29

    goto/16 :goto_1

    .line 1867293
    :cond_13
    const-string v47, "html_source"

    invoke-virtual/range {v46 .. v47}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_14

    .line 1867294
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v28

    goto/16 :goto_1

    .line 1867295
    :cond_14
    const-string v47, "id"

    invoke-virtual/range {v46 .. v47}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_15

    .line 1867296
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v27

    goto/16 :goto_1

    .line 1867297
    :cond_15
    const-string v47, "list_elements"

    invoke-virtual/range {v46 .. v47}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_16

    .line 1867298
    invoke-static/range {p0 .. p1}, LX/8aA;->a(LX/15w;LX/186;)I

    move-result v26

    goto/16 :goto_1

    .line 1867299
    :cond_16
    const-string v47, "list_style"

    invoke-virtual/range {v46 .. v47}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_17

    .line 1867300
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v25

    invoke-static/range {v25 .. v25}, Lcom/facebook/graphql/enums/GraphQLDocumentListStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLDocumentListStyle;

    move-result-object v25

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v25

    goto/16 :goto_1

    .line 1867301
    :cond_17
    const-string v47, "location_annotation"

    invoke-virtual/range {v46 .. v47}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_18

    .line 1867302
    invoke-static/range {p0 .. p1}, LX/8aB;->a(LX/15w;LX/186;)I

    move-result v24

    goto/16 :goto_1

    .line 1867303
    :cond_18
    const-string v47, "map_locations"

    invoke-virtual/range {v46 .. v47}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_19

    .line 1867304
    invoke-static/range {p0 .. p1}, LX/8aB;->b(LX/15w;LX/186;)I

    move-result v23

    goto/16 :goto_1

    .line 1867305
    :cond_19
    const-string v47, "map_style"

    invoke-virtual/range {v46 .. v47}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_1a

    .line 1867306
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v22

    invoke-static/range {v22 .. v22}, Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLDocumentMapStyle;

    move-result-object v22

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v22

    goto/16 :goto_1

    .line 1867307
    :cond_1a
    const-string v47, "margin_style"

    invoke-virtual/range {v46 .. v47}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_1b

    .line 1867308
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLDocumentElementMarginStyle;

    move-result-object v21

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v21

    goto/16 :goto_1

    .line 1867309
    :cond_1b
    const-string v47, "option_call_to_action"

    invoke-virtual/range {v46 .. v47}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_1c

    .line 1867310
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Lcom/facebook/graphql/enums/GraphQLInstantArticleCallToAction;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLInstantArticleCallToAction;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v20

    goto/16 :goto_1

    .line 1867311
    :cond_1c
    const-string v47, "option_lead_gen_data"

    invoke-virtual/range {v46 .. v47}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_1d

    .line 1867312
    invoke-static/range {p0 .. p1}, LX/8a0;->a(LX/15w;LX/186;)I

    move-result v19

    goto/16 :goto_1

    .line 1867313
    :cond_1d
    const-string v47, "photo"

    invoke-virtual/range {v46 .. v47}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_1e

    .line 1867314
    invoke-static/range {p0 .. p1}, LX/8ZQ;->a(LX/15w;LX/186;)I

    move-result v18

    goto/16 :goto_1

    .line 1867315
    :cond_1e
    const-string v47, "poster_image"

    invoke-virtual/range {v46 .. v47}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_1f

    .line 1867316
    invoke-static/range {p0 .. p1}, LX/8ZQ;->a(LX/15w;LX/186;)I

    move-result v17

    goto/16 :goto_1

    .line 1867317
    :cond_1f
    const-string v47, "presentation_state"

    invoke-virtual/range {v46 .. v47}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_20

    .line 1867318
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v16

    goto/16 :goto_1

    .line 1867319
    :cond_20
    const-string v47, "related_article_objs"

    invoke-virtual/range {v46 .. v47}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_21

    .line 1867320
    invoke-static/range {p0 .. p1}, LX/8aT;->a(LX/15w;LX/186;)I

    move-result v15

    goto/16 :goto_1

    .line 1867321
    :cond_21
    const-string v47, "slideEdges"

    invoke-virtual/range {v46 .. v47}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_22

    .line 1867322
    invoke-static/range {p0 .. p1}, LX/8aV;->a(LX/15w;LX/186;)I

    move-result v14

    goto/16 :goto_1

    .line 1867323
    :cond_22
    const-string v47, "subtitle_annotation"

    invoke-virtual/range {v46 .. v47}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_23

    .line 1867324
    invoke-static/range {p0 .. p1}, LX/8aj;->a(LX/15w;LX/186;)I

    move-result v13

    goto/16 :goto_1

    .line 1867325
    :cond_23
    const-string v47, "text_background_color"

    invoke-virtual/range {v46 .. v47}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_24

    .line 1867326
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    goto/16 :goto_1

    .line 1867327
    :cond_24
    const-string v47, "title_annotation"

    invoke-virtual/range {v46 .. v47}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_25

    .line 1867328
    invoke-static/range {p0 .. p1}, LX/8aj;->a(LX/15w;LX/186;)I

    move-result v11

    goto/16 :goto_1

    .line 1867329
    :cond_25
    const-string v47, "video_autoplay_style"

    invoke-virtual/range {v46 .. v47}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_26

    .line 1867330
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v10

    goto/16 :goto_1

    .line 1867331
    :cond_26
    const-string v47, "video_control_style"

    invoke-virtual/range {v46 .. v47}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_27

    .line 1867332
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v9

    goto/16 :goto_1

    .line 1867333
    :cond_27
    const-string v47, "video_looping_style"

    invoke-virtual/range {v46 .. v47}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_28

    .line 1867334
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v8

    goto/16 :goto_1

    .line 1867335
    :cond_28
    const-string v47, "visual_style"

    invoke-virtual/range {v46 .. v47}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_29

    .line 1867336
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLInstantArticleSectionStyle;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v7

    goto/16 :goto_1

    .line 1867337
    :cond_29
    const-string v47, "webview_presentation_style"

    invoke-virtual/range {v46 .. v47}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v46

    if-eqz v46, :cond_0

    .line 1867338
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLDocumentWebviewPresentationStyle;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v6

    goto/16 :goto_1

    .line 1867339
    :cond_2a
    const/16 v46, 0x28

    move-object/from16 v0, p1

    move/from16 v1, v46

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 1867340
    const/16 v46, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v46

    move/from16 v2, v45

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1867341
    const/16 v45, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v45

    move/from16 v2, v44

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1867342
    const/16 v44, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v44

    move/from16 v2, v43

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1867343
    const/16 v43, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v43

    move/from16 v2, v42

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1867344
    const/16 v42, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v42

    move/from16 v2, v41

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1867345
    const/16 v41, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v41

    move/from16 v2, v40

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1867346
    const/16 v40, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v40

    move/from16 v2, v39

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1867347
    const/16 v39, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v39

    move/from16 v2, v38

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1867348
    const/16 v38, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v38

    move/from16 v2, v37

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1867349
    if-eqz v5, :cond_2b

    .line 1867350
    const/16 v5, 0x9

    const/16 v37, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v36

    move/from16 v2, v37

    invoke-virtual {v0, v5, v1, v2}, LX/186;->a(III)V

    .line 1867351
    :cond_2b
    if-eqz v4, :cond_2c

    .line 1867352
    const/16 v4, 0xa

    const/4 v5, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v4, v1, v5}, LX/186;->a(III)V

    .line 1867353
    :cond_2c
    const/16 v4, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1867354
    const/16 v4, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1867355
    const/16 v4, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1867356
    if-eqz v3, :cond_2d

    .line 1867357
    const/16 v3, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v3, v1}, LX/186;->a(IZ)V

    .line 1867358
    :cond_2d
    const/16 v3, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1867359
    const/16 v3, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1867360
    const/16 v3, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1867361
    const/16 v3, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1867362
    const/16 v3, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1867363
    const/16 v3, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1867364
    const/16 v3, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1867365
    const/16 v3, 0x16

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1867366
    const/16 v3, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1867367
    const/16 v3, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1867368
    const/16 v3, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1867369
    const/16 v3, 0x1a

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1867370
    const/16 v3, 0x1b

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1867371
    const/16 v3, 0x1c

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1867372
    const/16 v3, 0x1d

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1867373
    const/16 v3, 0x1e

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v15}, LX/186;->b(II)V

    .line 1867374
    const/16 v3, 0x1f

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v14}, LX/186;->b(II)V

    .line 1867375
    const/16 v3, 0x20

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v13}, LX/186;->b(II)V

    .line 1867376
    const/16 v3, 0x21

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v12}, LX/186;->b(II)V

    .line 1867377
    const/16 v3, 0x22

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v11}, LX/186;->b(II)V

    .line 1867378
    const/16 v3, 0x23

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v10}, LX/186;->b(II)V

    .line 1867379
    const/16 v3, 0x24

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9}, LX/186;->b(II)V

    .line 1867380
    const/16 v3, 0x25

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v8}, LX/186;->b(II)V

    .line 1867381
    const/16 v3, 0x26

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 1867382
    const/16 v3, 0x27

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 1867383
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 7

    .prologue
    const/16 v6, 0x14

    const/16 v5, 0x10

    const/16 v4, 0xb

    const/4 v3, 0x2

    const/4 v2, 0x0

    .line 1867037
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1867038
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1867039
    if-eqz v0, :cond_0

    .line 1867040
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1867041
    invoke-static {p0, p1, v2, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1867042
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1867043
    if-eqz v0, :cond_1

    .line 1867044
    const-string v1, "attribution_annotation"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1867045
    invoke-static {p0, v0, p2, p3}, LX/8aj;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1867046
    :cond_1
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 1867047
    if-eqz v0, :cond_2

    .line 1867048
    const-string v0, "audio_play_mode"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1867049
    invoke-virtual {p0, p1, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1867050
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1867051
    if-eqz v0, :cond_3

    .line 1867052
    const-string v1, "audio_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1867053
    invoke-static {p0, v0, p2, p3}, LX/8aj;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1867054
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1867055
    if-eqz v0, :cond_4

    .line 1867056
    const-string v1, "audio_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1867057
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1867058
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1867059
    if-eqz v0, :cond_5

    .line 1867060
    const-string v1, "base_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1867061
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1867062
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1867063
    if-eqz v0, :cond_6

    .line 1867064
    const-string v1, "block_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1867065
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1867066
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1867067
    if-eqz v0, :cond_8

    .line 1867068
    const-string v1, "blocks"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1867069
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1867070
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v3

    if-ge v1, v3, :cond_7

    .line 1867071
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v3

    invoke-static {p0, v3, p2, p3}, LX/8al;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1867072
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1867073
    :cond_7
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1867074
    :cond_8
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1867075
    if-eqz v0, :cond_9

    .line 1867076
    const-string v1, "copyright_annotation"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1867077
    invoke-static {p0, v0, p2, p3}, LX/8aj;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1867078
    :cond_9
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1867079
    if-eqz v0, :cond_a

    .line 1867080
    const-string v1, "display_height"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1867081
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1867082
    :cond_a
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1867083
    if-eqz v0, :cond_b

    .line 1867084
    const-string v1, "display_width"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1867085
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1867086
    :cond_b
    invoke-virtual {p0, p1, v4}, LX/15i;->g(II)I

    move-result v0

    .line 1867087
    if-eqz v0, :cond_c

    .line 1867088
    const-string v0, "document_element_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1867089
    invoke-virtual {p0, p1, v4}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1867090
    :cond_c
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1867091
    if-eqz v0, :cond_d

    .line 1867092
    const-string v1, "element_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1867093
    invoke-static {p0, v0, p2, p3}, LX/8al;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1867094
    :cond_d
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1867095
    if-eqz v0, :cond_e

    .line 1867096
    const-string v1, "element_video"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1867097
    invoke-static {p0, v0, p2, p3}, LX/8ZV;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1867098
    :cond_e
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1867099
    if-eqz v0, :cond_f

    .line 1867100
    const-string v1, "enable_ad_network_bridging"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1867101
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1867102
    :cond_f
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1867103
    if-eqz v0, :cond_10

    .line 1867104
    const-string v1, "feedback"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1867105
    invoke-static {p0, v0, p2, p3}, LX/2bG;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1867106
    :cond_10
    invoke-virtual {p0, p1, v5}, LX/15i;->g(II)I

    move-result v0

    .line 1867107
    if-eqz v0, :cond_11

    .line 1867108
    const-string v0, "feedback_options"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1867109
    invoke-virtual {p0, p1, v5}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1867110
    :cond_11
    const/16 v0, 0x11

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1867111
    if-eqz v0, :cond_12

    .line 1867112
    const-string v1, "html_source"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1867113
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1867114
    :cond_12
    const/16 v0, 0x12

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1867115
    if-eqz v0, :cond_13

    .line 1867116
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1867117
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1867118
    :cond_13
    const/16 v0, 0x13

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1867119
    if-eqz v0, :cond_14

    .line 1867120
    const-string v1, "list_elements"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1867121
    invoke-static {p0, v0, p2, p3}, LX/8aA;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1867122
    :cond_14
    invoke-virtual {p0, p1, v6}, LX/15i;->g(II)I

    move-result v0

    .line 1867123
    if-eqz v0, :cond_15

    .line 1867124
    const-string v0, "list_style"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1867125
    invoke-virtual {p0, p1, v6}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1867126
    :cond_15
    const/16 v0, 0x15

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1867127
    if-eqz v0, :cond_16

    .line 1867128
    const-string v1, "location_annotation"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1867129
    invoke-static {p0, v0, p2, p3}, LX/8aB;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1867130
    :cond_16
    const/16 v0, 0x16

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1867131
    if-eqz v0, :cond_17

    .line 1867132
    const-string v1, "map_locations"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1867133
    invoke-static {p0, v0, p2, p3}, LX/8aB;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1867134
    :cond_17
    const/16 v0, 0x17

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1867135
    if-eqz v0, :cond_18

    .line 1867136
    const-string v0, "map_style"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1867137
    const/16 v0, 0x17

    invoke-virtual {p0, p1, v0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1867138
    :cond_18
    const/16 v0, 0x18

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1867139
    if-eqz v0, :cond_19

    .line 1867140
    const-string v0, "margin_style"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1867141
    const/16 v0, 0x18

    invoke-virtual {p0, p1, v0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1867142
    :cond_19
    const/16 v0, 0x19

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1867143
    if-eqz v0, :cond_1a

    .line 1867144
    const-string v0, "option_call_to_action"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1867145
    const/16 v0, 0x19

    invoke-virtual {p0, p1, v0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1867146
    :cond_1a
    const/16 v0, 0x1a

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1867147
    if-eqz v0, :cond_1b

    .line 1867148
    const-string v1, "option_lead_gen_data"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1867149
    invoke-static {p0, v0, p2, p3}, LX/8a0;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1867150
    :cond_1b
    const/16 v0, 0x1b

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1867151
    if-eqz v0, :cond_1c

    .line 1867152
    const-string v1, "photo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1867153
    invoke-static {p0, v0, p2, p3}, LX/8ZQ;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1867154
    :cond_1c
    const/16 v0, 0x1c

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1867155
    if-eqz v0, :cond_1d

    .line 1867156
    const-string v1, "poster_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1867157
    invoke-static {p0, v0, p2, p3}, LX/8ZQ;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1867158
    :cond_1d
    const/16 v0, 0x1d

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1867159
    if-eqz v0, :cond_1e

    .line 1867160
    const-string v0, "presentation_state"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1867161
    const/16 v0, 0x1d

    invoke-virtual {p0, p1, v0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1867162
    :cond_1e
    const/16 v0, 0x1e

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1867163
    if-eqz v0, :cond_1f

    .line 1867164
    const-string v1, "related_article_objs"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1867165
    invoke-static {p0, v0, p2, p3}, LX/8aT;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1867166
    :cond_1f
    const/16 v0, 0x1f

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1867167
    if-eqz v0, :cond_20

    .line 1867168
    const-string v1, "slideEdges"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1867169
    invoke-static {p0, v0, p2, p3}, LX/8aV;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1867170
    :cond_20
    const/16 v0, 0x20

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1867171
    if-eqz v0, :cond_21

    .line 1867172
    const-string v1, "subtitle_annotation"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1867173
    invoke-static {p0, v0, p2, p3}, LX/8aj;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1867174
    :cond_21
    const/16 v0, 0x21

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1867175
    if-eqz v0, :cond_22

    .line 1867176
    const-string v1, "text_background_color"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1867177
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1867178
    :cond_22
    const/16 v0, 0x22

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1867179
    if-eqz v0, :cond_23

    .line 1867180
    const-string v1, "title_annotation"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1867181
    invoke-static {p0, v0, p2, p3}, LX/8aj;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1867182
    :cond_23
    const/16 v0, 0x23

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1867183
    if-eqz v0, :cond_24

    .line 1867184
    const-string v0, "video_autoplay_style"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1867185
    const/16 v0, 0x23

    invoke-virtual {p0, p1, v0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1867186
    :cond_24
    const/16 v0, 0x24

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1867187
    if-eqz v0, :cond_25

    .line 1867188
    const-string v0, "video_control_style"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1867189
    const/16 v0, 0x24

    invoke-virtual {p0, p1, v0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1867190
    :cond_25
    const/16 v0, 0x25

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1867191
    if-eqz v0, :cond_26

    .line 1867192
    const-string v0, "video_looping_style"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1867193
    const/16 v0, 0x25

    invoke-virtual {p0, p1, v0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1867194
    :cond_26
    const/16 v0, 0x26

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1867195
    if-eqz v0, :cond_27

    .line 1867196
    const-string v0, "visual_style"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1867197
    const/16 v0, 0x26

    invoke-virtual {p0, p1, v0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1867198
    :cond_27
    const/16 v0, 0x27

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1867199
    if-eqz v0, :cond_28

    .line 1867200
    const-string v0, "webview_presentation_style"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1867201
    const/16 v0, 0x27

    invoke-virtual {p0, p1, v0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1867202
    :cond_28
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1867203
    return-void
.end method
