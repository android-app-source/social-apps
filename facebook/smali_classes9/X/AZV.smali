.class public LX/AZV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/AZT;
.implements LX/AZU;
.implements LX/4np;


# instance fields
.field public final a:LX/AVT;

.field public final b:LX/AVP;

.field private final c:Lcom/facebook/widget/CustomHorizontalScrollView;

.field public final d:Landroid/widget/LinearLayout;

.field public final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/facecast/plugin/creativetools/CreativeToolsBasicAdjustmentFilterView;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Landroid/view/View$OnTouchListener;

.field public g:LX/AZR;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:LX/AZa;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:I

.field public j:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/AVP;LX/AVT;)V
    .locals 3
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/AVP;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1687057
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1687058
    iput-object p2, p0, LX/AZV;->b:LX/AVP;

    .line 1687059
    iput-object p3, p0, LX/AZV;->a:LX/AVT;

    .line 1687060
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1687061
    const v1, 0x7f0305bb

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomHorizontalScrollView;

    iput-object v0, p0, LX/AZV;->c:Lcom/facebook/widget/CustomHorizontalScrollView;

    .line 1687062
    iget-object v0, p0, LX/AZV;->c:Lcom/facebook/widget/CustomHorizontalScrollView;

    const v1, 0x7f0d0fcc

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomHorizontalScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/AZV;->d:Landroid/widget/LinearLayout;

    .line 1687063
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/AZV;->e:Ljava/util/List;

    .line 1687064
    new-instance v0, LX/3rW;

    new-instance v1, LX/AZS;

    invoke-direct {v1, p0}, LX/AZS;-><init>(LX/AZV;)V

    invoke-direct {v0, p1, v1}, LX/3rW;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    .line 1687065
    new-instance v1, LX/AZP;

    invoke-direct {v1, p0, v0}, LX/AZP;-><init>(LX/AZV;LX/3rW;)V

    iput-object v1, p0, LX/AZV;->f:Landroid/view/View$OnTouchListener;

    .line 1687066
    iget-object v0, p0, LX/AZV;->c:Lcom/facebook/widget/CustomHorizontalScrollView;

    .line 1687067
    iput-object p0, v0, Lcom/facebook/widget/CustomHorizontalScrollView;->a:LX/4np;

    .line 1687068
    iget-object v0, p0, LX/AZV;->d:Landroid/widget/LinearLayout;

    new-instance v1, LX/AZQ;

    invoke-direct {v1, p0}, LX/AZQ;-><init>(LX/AZV;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 1687069
    return-void
.end method

.method public static a$redex0(LX/AZV;I)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1687031
    iget-object v0, p0, LX/AZV;->h:LX/AZa;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1687032
    iget v0, p0, LX/AZV;->i:I

    .line 1687033
    iput p1, p0, LX/AZV;->i:I

    .line 1687034
    iget v1, p0, LX/AZV;->i:I

    if-ne v0, v1, :cond_1

    .line 1687035
    :cond_0
    :goto_0
    return-void

    .line 1687036
    :cond_1
    iget-object v1, p0, LX/AZV;->e:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsBasicAdjustmentFilterView;

    .line 1687037
    iget-object v1, p0, LX/AZV;->e:Ljava/util/List;

    iget v2, p0, LX/AZV;->i:I

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsBasicAdjustmentFilterView;

    .line 1687038
    iget-object v3, p0, LX/AZV;->b:LX/AVP;

    iget-object v2, p0, LX/AZV;->h:LX/AZa;

    iget v4, p0, LX/AZV;->i:I

    invoke-virtual {v2, v4}, LX/AZZ;->a(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/AZO;

    iget-object v2, v2, LX/AZO;->c:Lcom/facebook/videocodec/effects/model/ColorFilter;

    .line 1687039
    iget-object v4, v3, LX/AVP;->w:LX/AWK;

    if-eqz v4, :cond_2

    iget-object v4, v3, LX/AVP;->w:LX/AWK;

    instance-of v4, v4, LX/AWK;

    if-eqz v4, :cond_2

    .line 1687040
    iget-object v4, v3, LX/AVP;->w:LX/AWK;

    check-cast v4, LX/AWK;

    .line 1687041
    iget-object v3, v4, LX/AWK;->f:LX/7S6;

    invoke-virtual {v3, v2}, LX/7S6;->a(Lcom/facebook/videocodec/effects/model/ColorFilter;)V

    .line 1687042
    :cond_2
    invoke-virtual {v0, v5}, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsBasicAdjustmentFilterView;->setSelected(Z)V

    .line 1687043
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsBasicAdjustmentFilterView;->setSelected(Z)V

    .line 1687044
    invoke-virtual {v1}, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsBasicAdjustmentFilterView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 1687045
    invoke-virtual {v1}, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsBasicAdjustmentFilterView;->getLeft()I

    move-result v2

    iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    sub-int/2addr v2, v3

    .line 1687046
    invoke-virtual {v1}, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsBasicAdjustmentFilterView;->getRight()I

    move-result v1

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v0, v1

    .line 1687047
    invoke-direct {p0}, LX/AZV;->e()I

    move-result v1

    .line 1687048
    invoke-direct {p0}, LX/AZV;->f()I

    move-result v3

    .line 1687049
    if-ge v2, v1, :cond_4

    .line 1687050
    sub-int v0, v2, v1

    .line 1687051
    iget-object v1, p0, LX/AZV;->c:Lcom/facebook/widget/CustomHorizontalScrollView;

    invoke-virtual {v1, v0, v5}, Lcom/facebook/widget/CustomHorizontalScrollView;->smoothScrollBy(II)V

    .line 1687052
    :cond_3
    :goto_1
    iget-object v0, p0, LX/AZV;->g:LX/AZR;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 1687053
    iget-object v0, p0, LX/AZV;->g:LX/AZR;

    invoke-interface {v0}, LX/AZR;->iI_()V

    goto :goto_0

    .line 1687054
    :cond_4
    if-le v0, v3, :cond_3

    .line 1687055
    sub-int/2addr v0, v3

    .line 1687056
    iget-object v1, p0, LX/AZV;->c:Lcom/facebook/widget/CustomHorizontalScrollView;

    invoke-virtual {v1, v0, v5}, Lcom/facebook/widget/CustomHorizontalScrollView;->smoothScrollBy(II)V

    goto :goto_1
.end method

.method private e()I
    .locals 2

    .prologue
    .line 1687030
    iget-object v0, p0, LX/AZV;->c:Lcom/facebook/widget/CustomHorizontalScrollView;

    invoke-virtual {v0}, Lcom/facebook/widget/CustomHorizontalScrollView;->getScrollX()I

    move-result v1

    iget-object v0, p0, LX/AZV;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    sub-int v0, v1, v0

    return v0
.end method

.method private f()I
    .locals 2

    .prologue
    .line 1687029
    invoke-direct {p0}, LX/AZV;->e()I

    move-result v0

    iget-object v1, p0, LX/AZV;->c:Lcom/facebook/widget/CustomHorizontalScrollView;

    invoke-virtual {v1}, Lcom/facebook/widget/CustomHorizontalScrollView;->getWidth()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public static g(LX/AZV;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1686997
    invoke-direct {p0}, LX/AZV;->e()I

    move-result v4

    .line 1686998
    invoke-direct {p0}, LX/AZV;->f()I

    move-result v5

    .line 1686999
    iput-boolean v2, p0, LX/AZV;->j:Z

    move v1, v2

    .line 1687000
    :goto_0
    iget-object v0, p0, LX/AZV;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1687001
    iget-object v0, p0, LX/AZV;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsBasicAdjustmentFilterView;

    .line 1687002
    invoke-virtual {v0}, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsBasicAdjustmentFilterView;->getRight()I

    move-result v3

    if-le v3, v4, :cond_0

    invoke-virtual {v0}, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsBasicAdjustmentFilterView;->getLeft()I

    move-result v3

    if-ge v3, v5, :cond_0

    const/4 v3, 0x1

    .line 1687003
    :goto_1
    invoke-virtual {v0, v3}, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsBasicAdjustmentFilterView;->setVisibility(Z)V

    .line 1687004
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    move v3, v2

    .line 1687005
    goto :goto_1

    .line 1687006
    :cond_1
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 1687027
    invoke-static {p0}, LX/AZV;->g(LX/AZV;)V

    .line 1687028
    return-void
.end method

.method public final a(LX/AZX;)V
    .locals 5

    .prologue
    .line 1687023
    iget-object v0, p0, LX/AZV;->h:LX/AZa;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1687024
    iget-object v0, p0, LX/AZV;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    invoke-static {p0, v0}, LX/AZV;->a$redex0(LX/AZV;I)V

    .line 1687025
    iget-object v1, p0, LX/AZV;->a:LX/AVT;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, LX/AZV;->h:LX/AZa;

    invoke-virtual {v2}, LX/AZa;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "tap"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v0, "filter_"

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, LX/AZV;->h:LX/AZa;

    iget v4, p0, LX/AZV;->i:I

    invoke-virtual {v0, v4}, LX/AZZ;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AZO;

    iget-object v0, v0, LX/AZO;->b:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, LX/Aa6;->c(LX/AVT;Ljava/lang/String;Ljava/lang/String;)V

    .line 1687026
    return-void
.end method

.method public final a(Lcom/facebook/facecast/FacecastPreviewView;)V
    .locals 2

    .prologue
    .line 1687012
    iget-object v0, p0, LX/AZV;->f:Landroid/view/View$OnTouchListener;

    invoke-virtual {p1, v0}, Lcom/facebook/facecast/FacecastPreviewView;->a(Landroid/view/View$OnTouchListener;)V

    .line 1687013
    iget-object v0, p0, LX/AZV;->c:Lcom/facebook/widget/CustomHorizontalScrollView;

    invoke-virtual {v0}, Lcom/facebook/widget/CustomHorizontalScrollView;->getChildCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 1687014
    iget-object v0, p0, LX/AZV;->c:Lcom/facebook/widget/CustomHorizontalScrollView;

    iget-object v1, p0, LX/AZV;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomHorizontalScrollView;->addView(Landroid/view/View;)V

    .line 1687015
    iget-object v0, p0, LX/AZV;->c:Lcom/facebook/widget/CustomHorizontalScrollView;

    .line 1687016
    iget v1, p0, LX/AZV;->i:I

    if-gez v1, :cond_1

    .line 1687017
    const/4 v1, 0x0

    .line 1687018
    :goto_0
    move v1, v1

    .line 1687019
    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomHorizontalScrollView;->setScrollX(I)V

    .line 1687020
    :cond_0
    return-void

    .line 1687021
    :cond_1
    iget-object v1, p0, LX/AZV;->e:Ljava/util/List;

    iget p1, p0, LX/AZV;->i:I

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsBasicAdjustmentFilterView;

    .line 1687022
    invoke-virtual {v1}, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsBasicAdjustmentFilterView;->getLeft()I

    move-result v1

    goto :goto_0
.end method

.method public final b()Landroid/view/View;
    .locals 1

    .prologue
    .line 1687011
    iget-object v0, p0, LX/AZV;->c:Lcom/facebook/widget/CustomHorizontalScrollView;

    return-object v0
.end method

.method public final b(Lcom/facebook/facecast/FacecastPreviewView;)V
    .locals 2

    .prologue
    .line 1687008
    iget-object v0, p0, LX/AZV;->f:Landroid/view/View$OnTouchListener;

    invoke-virtual {p1, v0}, Lcom/facebook/facecast/FacecastPreviewView;->b(Landroid/view/View$OnTouchListener;)V

    .line 1687009
    iget-object v0, p0, LX/AZV;->c:Lcom/facebook/widget/CustomHorizontalScrollView;

    iget-object v1, p0, LX/AZV;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomHorizontalScrollView;->removeView(Landroid/view/View;)V

    .line 1687010
    return-void
.end method

.method public final c()Landroid/view/View;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1687007
    const/4 v0, 0x0

    return-object v0
.end method
