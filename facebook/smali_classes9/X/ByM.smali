.class public LX/ByM;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/ByN;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/ByM",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/ByN;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1837153
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1837154
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/ByM;->b:LX/0Zi;

    .line 1837155
    iput-object p1, p0, LX/ByM;->a:LX/0Ot;

    .line 1837156
    return-void
.end method

.method public static a(LX/0QB;)LX/ByM;
    .locals 4

    .prologue
    .line 1837142
    const-class v1, LX/ByM;

    monitor-enter v1

    .line 1837143
    :try_start_0
    sget-object v0, LX/ByM;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1837144
    sput-object v2, LX/ByM;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1837145
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1837146
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1837147
    new-instance v3, LX/ByM;

    const/16 p0, 0x1e25

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/ByM;-><init>(LX/0Ot;)V

    .line 1837148
    move-object v0, v3

    .line 1837149
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1837150
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/ByM;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1837151
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1837152
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1837141
    const v0, 0x2826e2f2

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 9

    .prologue
    .line 1837112
    check-cast p2, LX/ByL;

    .line 1837113
    iget-object v0, p0, LX/ByM;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ByN;

    iget-object v1, p2, LX/ByL;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v3, 0x0

    const/4 p2, 0x2

    const/4 p0, 0x0

    const/4 v8, 0x1

    .line 1837114
    iget-object v2, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 1837115
    check-cast v2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1837116
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->a()LX/0Px;

    move-result-object v6

    .line 1837117
    const/4 v4, 0x0

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    move v5, v4

    :goto_0
    if-ge v5, v7, :cond_6

    .line 1837118
    invoke-virtual {v6, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 1837119
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    if-eqz v1, :cond_5

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->E()Lcom/facebook/graphql/model/GraphQLEvent;

    move-result-object v1

    if-eqz v1, :cond_5

    invoke-static {v4}, LX/2v7;->a(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v4}, LX/2v7;->b(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1837120
    :cond_0
    :goto_1
    move-object v4, v4

    .line 1837121
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, p2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v5

    const v6, 0x7f0b0b58

    invoke-interface {v5, v6}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, p2}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, v8}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v5

    const/4 v6, 0x6

    const v7, 0x7f0b1097

    invoke-interface {v5, v6, v7}, LX/1Dh;->q(II)LX/1Dh;

    move-result-object v5

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v6

    invoke-interface {v6, p0}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v6

    const/high16 v7, 0x3f800000    # 1.0f

    invoke-interface {v6, v7}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v6

    invoke-interface {v6, v8}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v6

    const v7, 0x7f0e0738

    invoke-static {p1, p0, v7}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v7

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->A()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v2}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, v8}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v2

    sget-object v7, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v2, v7}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v2

    invoke-interface {v6, v2}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v6

    const v2, 0x7f0e0739

    invoke-static {p1, p0, v2}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v7

    if-eqz v4, :cond_3

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ab()Ljava/lang/String;

    move-result-object v2

    :goto_2
    invoke-virtual {v7, v2}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, v8}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v2

    sget-object v7, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v2, v7}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v2

    invoke-interface {v6, v2}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v2

    const v6, 0x7f0e073a

    invoke-static {p1, p0, v6}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v6

    if-eqz v4, :cond_1

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ac()Ljava/lang/String;

    move-result-object v3

    :cond_1
    invoke-virtual {v6, v3}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, v8}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v3

    sget-object v6, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v3, v6}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v2

    invoke-interface {v5, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    iget-object v2, v0, LX/ByN;->c:LX/2yS;

    invoke-virtual {v2, p1}, LX/2yS;->c(LX/1De;)LX/AE2;

    move-result-object v5

    if-eqz v4, :cond_4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aZ()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aZ()Ljava/lang/String;

    move-result-object v2

    :goto_3
    invoke-virtual {v5, v2}, LX/AE2;->a(Ljava/lang/CharSequence;)LX/AE2;

    move-result-object v2

    iget-object v5, v0, LX/ByN;->a:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a00a4

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v2, v5}, LX/AE2;->h(I)LX/AE2;

    move-result-object v2

    .line 1837122
    if-eqz v4, :cond_2

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->E()Lcom/facebook/graphql/model/GraphQLEvent;

    move-result-object v5

    if-nez v5, :cond_7

    .line 1837123
    :cond_2
    const/4 v5, 0x0

    .line 1837124
    :goto_4
    move-object v4, v5

    .line 1837125
    invoke-virtual {v2, v4}, LX/AE2;->a(Landroid/view/View$OnClickListener;)LX/AE2;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    invoke-interface {v2, p2}, LX/1Di;->b(I)LX/1Di;

    move-result-object v2

    invoke-interface {v3, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    .line 1837126
    const v3, 0x2826e2f2

    const/4 v4, 0x0

    invoke-static {p1, v3, v4}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v3

    move-object v3, v3

    .line 1837127
    invoke-interface {v2, v3}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 1837128
    return-object v0

    :cond_3
    move-object v2, v3

    goto/16 :goto_2

    :cond_4
    iget-object v2, v0, LX/ByN;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v6, 0x7f081a5b

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_3

    .line 1837129
    :cond_5
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto/16 :goto_0

    .line 1837130
    :cond_6
    const/4 v4, 0x0

    goto/16 :goto_1

    :cond_7
    iget-object v5, v0, LX/ByN;->d:LX/1nP;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->E()Lcom/facebook/graphql/model/GraphQLEvent;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLEvent;->ax()Ljava/lang/String;

    move-result-object v6

    sget-object v7, Lcom/facebook/events/common/ActionMechanism;->FEED_ATTACHMENT:Lcom/facebook/events/common/ActionMechanism;

    invoke-static {}, LX/1nP;->a()Lcom/facebook/events/common/EventAnalyticsParams;

    move-result-object v8

    invoke-virtual {v5, v6, v7, v8}, LX/1nP;->a(Ljava/lang/String;Lcom/facebook/events/common/ActionMechanism;Lcom/facebook/events/common/EventAnalyticsParams;)Landroid/view/View$OnClickListener;

    move-result-object v5

    goto :goto_4
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 1837131
    invoke-static {}, LX/1dS;->b()V

    .line 1837132
    iget v0, p1, LX/1dQ;->b:I

    .line 1837133
    packed-switch v0, :pswitch_data_0

    .line 1837134
    :goto_0
    return-object v2

    .line 1837135
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 1837136
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1837137
    check-cast v1, LX/ByL;

    .line 1837138
    iget-object v3, p0, LX/ByM;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/ByN;

    iget-object v4, v1, LX/ByL;->b:LX/1Pq;

    iget-object p1, v1, LX/ByL;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object p2, v1, LX/ByL;->c:Ljava/lang/String;

    .line 1837139
    iget-object p0, v3, LX/ByN;->b:LX/38w;

    invoke-virtual {p0, v0, v4, p1, p2}, LX/38w;->a(Landroid/view/View;LX/1Pq;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;)V

    .line 1837140
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x2826e2f2
        :pswitch_0
    .end packed-switch
.end method
