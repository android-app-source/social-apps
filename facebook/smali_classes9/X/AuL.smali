.class public LX/AuL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/0iK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "LX/0io;",
        ":",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationStateSpec$ProvidesInspirationState;",
        "Mutation::",
        "Lcom/facebook/ipc/composer/dataaccessor/ComposerCanSave;",
        ":",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBoundsSpec$SetsInspirationPreviewBounds",
        "<TMutation;>;DerivedData:",
        "Ljava/lang/Object;",
        "Services::",
        "LX/0il",
        "<TModelData;>;:",
        "LX/0ik",
        "<TDerivedData;>;:",
        "LX/0im",
        "<TMutation;>;>",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;",
        "LX/0iK",
        "<TModelData;TDerivedData;>;"
    }
.end annotation


# static fields
.field public static final a:LX/0jK;


# instance fields
.field public b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TServices;>;"
        }
    .end annotation
.end field

.field private final c:Landroid/view/ViewStub;

.field private final d:Landroid/content/Context;

.field public e:Lcom/facebook/video/player/RichVideoPlayer;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1722534
    const-class v0, LX/AuL;

    invoke-static {v0}, LX/0jK;->a(Ljava/lang/Class;)LX/0jK;

    move-result-object v0

    sput-object v0, LX/AuL;->a:LX/0jK;

    return-void
.end method

.method public constructor <init>(LX/0il;Landroid/view/ViewStub;Landroid/content/Context;)V
    .locals 1
    .param p1    # LX/0il;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Landroid/view/ViewStub;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TServices;",
            "Landroid/view/ViewStub;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1722454
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1722455
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/AuL;->b:Ljava/lang/ref/WeakReference;

    .line 1722456
    iput-object p2, p0, LX/AuL;->c:Landroid/view/ViewStub;

    .line 1722457
    iput-object p3, p0, LX/AuL;->d:Landroid/content/Context;

    .line 1722458
    return-void
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 1722526
    iget-object v0, p0, LX/AuL;->e:Lcom/facebook/video/player/RichVideoPlayer;

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    .line 1722527
    :goto_0
    return-void

    .line 1722528
    :cond_0
    iget-object v0, p0, LX/AuL;->e:Lcom/facebook/video/player/RichVideoPlayer;

    if-nez v0, :cond_1

    .line 1722529
    iget-object v0, p0, LX/AuL;->c:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/player/RichVideoPlayer;

    iput-object v0, p0, LX/AuL;->e:Lcom/facebook/video/player/RichVideoPlayer;

    .line 1722530
    iget-object v0, p0, LX/AuL;->e:Lcom/facebook/video/player/RichVideoPlayer;

    new-instance v1, Lcom/facebook/video/player/plugins/VideoPlugin;

    iget-object v2, p0, LX/AuL;->d:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/facebook/video/player/plugins/VideoPlugin;-><init>(Landroid/content/Context;)V

    .line 1722531
    invoke-static {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 1722532
    iget-object v0, p0, LX/AuL;->e:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v1, LX/04D;->INSPIRATION_CAMERA:LX/04D;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->setPlayerOrigin(LX/04D;)V

    .line 1722533
    :cond_1
    iget-object v0, p0, LX/AuL;->e:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0, p1}, Lcom/facebook/video/player/RichVideoPlayer;->setVisibility(I)V

    goto :goto_0
.end method

.method private a(Lcom/facebook/ipc/media/MediaItem;Z)V
    .locals 4

    .prologue
    .line 1722492
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/AuL;->a(I)V

    .line 1722493
    invoke-direct {p0}, LX/AuL;->c()V

    .line 1722494
    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "session"

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 1722495
    const/4 v3, 0x1

    .line 1722496
    invoke-static {}, Lcom/facebook/video/engine/VideoDataSource;->newBuilder()LX/2oE;

    move-result-object v1

    .line 1722497
    iput-object v0, v1, LX/2oE;->a:Landroid/net/Uri;

    .line 1722498
    move-object v1, v1

    .line 1722499
    sget-object v2, LX/097;->FROM_LOCAL_STORAGE:LX/097;

    .line 1722500
    iput-object v2, v1, LX/2oE;->e:LX/097;

    .line 1722501
    move-object v2, v1

    .line 1722502
    if-eqz p2, :cond_0

    sget-object v1, LX/2oF;->MIRROR_HORIZONTALLY:LX/2oF;

    .line 1722503
    :goto_0
    iput-object v1, v2, LX/2oE;->g:LX/2oF;

    .line 1722504
    move-object v1, v2

    .line 1722505
    invoke-virtual {v1}, LX/2oE;->h()Lcom/facebook/video/engine/VideoDataSource;

    move-result-object v1

    .line 1722506
    invoke-static {}, Lcom/facebook/video/engine/VideoPlayerParams;->newBuilder()LX/2oH;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/2oH;->a(Lcom/facebook/video/engine/VideoDataSource;)LX/2oH;

    move-result-object v1

    .line 1722507
    iput-boolean v3, v1, LX/2oH;->t:Z

    .line 1722508
    move-object v1, v1

    .line 1722509
    iput-boolean v3, v1, LX/2oH;->g:Z

    .line 1722510
    move-object v1, v1

    .line 1722511
    iput-boolean v3, v1, LX/2oH;->o:Z

    .line 1722512
    move-object v1, v1

    .line 1722513
    invoke-virtual {v1}, LX/2oH;->n()Lcom/facebook/video/engine/VideoPlayerParams;

    move-result-object v1

    .line 1722514
    new-instance v2, LX/2pZ;

    invoke-direct {v2}, LX/2pZ;-><init>()V

    .line 1722515
    iput-object v1, v2, LX/2pZ;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 1722516
    move-object v1, v2

    .line 1722517
    move-object v0, v1

    .line 1722518
    invoke-virtual {v0}, LX/2pZ;->b()LX/2pa;

    move-result-object v0

    .line 1722519
    iget-object v1, p0, LX/AuL;->e:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->c(LX/2pa;)V

    .line 1722520
    iget-object v0, p0, LX/AuL;->e:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v1, LX/04g;->BY_PLAYER:LX/04g;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/04g;)V

    .line 1722521
    iget-object v0, p0, LX/AuL;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 1722522
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->isMuted()Z

    move-result v0

    invoke-direct {p0, v0}, LX/AuL;->a(Z)V

    .line 1722523
    iget-object v0, p0, LX/AuL;->e:Lcom/facebook/video/player/RichVideoPlayer;

    new-instance v1, Lcom/facebook/friendsharing/inspiration/controller/InspirationVideoPreviewController$1;

    invoke-direct {v1, p0, p1}, Lcom/facebook/friendsharing/inspiration/controller/InspirationVideoPreviewController$1;-><init>(LX/AuL;Lcom/facebook/ipc/media/MediaItem;)V

    invoke-static {v0, v1}, LX/Awl;->a(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 1722524
    return-void

    .line 1722525
    :cond_0
    sget-object v1, LX/2oF;->NONE:LX/2oF;

    goto :goto_0
.end method

.method private a(Z)V
    .locals 2

    .prologue
    .line 1722490
    iget-object v0, p0, LX/AuL;->e:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v1, LX/04g;->BY_PLAYER:LX/04g;

    invoke-virtual {v0, p1, v1}, Lcom/facebook/video/player/RichVideoPlayer;->a(ZLX/04g;)V

    .line 1722491
    return-void
.end method

.method private b()V
    .locals 1

    .prologue
    .line 1722487
    const/16 v0, 0x8

    invoke-direct {p0, v0}, LX/AuL;->a(I)V

    .line 1722488
    invoke-direct {p0}, LX/AuL;->c()V

    .line 1722489
    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 1722482
    iget-object v0, p0, LX/AuL;->e:Lcom/facebook/video/player/RichVideoPlayer;

    if-nez v0, :cond_0

    .line 1722483
    :goto_0
    return-void

    .line 1722484
    :cond_0
    iget-object v0, p0, LX/AuL;->e:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->q()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1722485
    iget-object v0, p0, LX/AuL;->e:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v1, LX/04g;->BY_PLAYER:LX/04g;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/04g;)V

    .line 1722486
    :cond_1
    iget-object v0, p0, LX/AuL;->e:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->i()V

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/5L2;)V
    .locals 3

    .prologue
    .line 1722476
    iget-object v0, p0, LX/AuL;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    .line 1722477
    invoke-interface {v0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v1

    invoke-static {v1}, LX/7kq;->j(LX/0Px;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1722478
    :goto_0
    return-void

    .line 1722479
    :cond_0
    sget-object v1, LX/AuK;->a:[I

    invoke-virtual {p1}, LX/5L2;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 1722480
    :pswitch_0
    invoke-direct {p0}, LX/AuL;->b()V

    goto :goto_0

    .line 1722481
    :pswitch_1
    invoke-static {v0}, LX/2rf;->b(LX/0io;)Lcom/facebook/composer/attachments/ComposerAttachment;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v1}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v1

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->isMirrorOn()Z

    move-result v0

    invoke-direct {p0, v1, v0}, LX/AuL;->a(Lcom/facebook/ipc/media/MediaItem;Z)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 1722459
    check-cast p1, LX/0io;

    .line 1722460
    iget-object v0, p0, LX/AuL;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    move-object v1, p1

    .line 1722461
    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v2

    move-object v1, v0

    .line 1722462
    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v3

    .line 1722463
    invoke-virtual {v2}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->isMuted()Z

    move-result v1

    invoke-virtual {v3}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->isMuted()Z

    move-result v4

    if-eq v1, v4, :cond_0

    .line 1722464
    invoke-virtual {v3}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->isMuted()Z

    move-result v1

    invoke-direct {p0, v1}, LX/AuL;->a(Z)V

    .line 1722465
    :cond_0
    invoke-static {p1, v0}, LX/87R;->b(LX/0io;LX/0io;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1722466
    invoke-direct {p0}, LX/AuL;->b()V

    .line 1722467
    :cond_1
    invoke-interface {v0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v1

    invoke-static {v1}, LX/7kq;->j(LX/0Px;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 1722468
    :cond_2
    :goto_0
    return-void

    :cond_3
    move-object v1, p1

    .line 1722469
    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->isReleaseVideoPlayerRequested()Z

    move-result v1

    if-nez v1, :cond_4

    move-object v1, v0

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->isReleaseVideoPlayerRequested()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1722470
    iget-object v1, p0, LX/AuL;->e:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v1}, Lcom/facebook/video/player/RichVideoPlayer;->i()V

    :cond_4
    move-object v1, v0

    .line 1722471
    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->isReleaseVideoPlayerRequested()Z

    move-result v1

    if-nez v1, :cond_2

    .line 1722472
    invoke-static {p1}, LX/2rf;->b(LX/0io;)Lcom/facebook/composer/attachments/ComposerAttachment;

    move-result-object v1

    .line 1722473
    invoke-static {v0}, LX/2rf;->b(LX/0io;)Lcom/facebook/composer/attachments/ComposerAttachment;

    move-result-object v4

    .line 1722474
    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/facebook/composer/attachments/ComposerAttachment;->c()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v4}, Lcom/facebook/composer/attachments/ComposerAttachment;->c()Landroid/net/Uri;

    move-result-object v5

    invoke-static {v1, v5}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-virtual {v2}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->isMirrorOn()Z

    move-result v1

    invoke-virtual {v3}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->isMirrorOn()Z

    move-result v2

    if-ne v1, v2, :cond_5

    check-cast p1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {p1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->isReleaseVideoPlayerRequested()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1722475
    :cond_5
    invoke-virtual {v4}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v1

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->isMirrorOn()Z

    move-result v0

    invoke-direct {p0, v1, v0}, LX/AuL;->a(Lcom/facebook/ipc/media/MediaItem;Z)V

    goto :goto_0
.end method
