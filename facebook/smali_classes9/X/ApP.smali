.class public LX/ApP;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final b:Landroid/graphics/Typeface;

.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/23P;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1715961
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-ge v0, v1, :cond_0

    const-string v0, "sans-serif-regular"

    .line 1715962
    :goto_0
    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    sput-object v0, LX/ApP;->b:Landroid/graphics/Typeface;

    .line 1715963
    return-void

    .line 1715964
    :cond_0
    const-string v0, "sans-serif-medium"

    goto :goto_0
.end method

.method public constructor <init>(LX/23P;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1715965
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1715966
    iput-object p1, p0, LX/ApP;->a:LX/23P;

    .line 1715967
    return-void
.end method

.method public static a(LX/0QB;)LX/ApP;
    .locals 4

    .prologue
    .line 1715968
    const-class v1, LX/ApP;

    monitor-enter v1

    .line 1715969
    :try_start_0
    sget-object v0, LX/ApP;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1715970
    sput-object v2, LX/ApP;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1715971
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1715972
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1715973
    new-instance p0, LX/ApP;

    invoke-static {v0}, LX/23P;->b(LX/0QB;)LX/23P;

    move-result-object v3

    check-cast v3, LX/23P;

    invoke-direct {p0, v3}, LX/ApP;-><init>(LX/23P;)V

    .line 1715974
    move-object v0, p0

    .line 1715975
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1715976
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/ApP;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1715977
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1715978
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
