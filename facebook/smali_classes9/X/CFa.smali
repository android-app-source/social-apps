.class public LX/CFa;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/goodwill/publish/GoodwillPublishLifeEventParam;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1863667
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 8

    .prologue
    .line 1863644
    check-cast p1, Lcom/facebook/goodwill/publish/GoodwillPublishLifeEventParam;

    .line 1863645
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1863646
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "privacy"

    iget-object v2, p1, Lcom/facebook/goodwill/publish/GoodwillPublishLifeEventParam;->b:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1863647
    iget-object v0, p1, Lcom/facebook/goodwill/publish/GoodwillPublishLifeEventParam;->c:Ljava/util/List;

    if-eqz v0, :cond_2

    .line 1863648
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "photo_ids"

    iget-object v2, p1, Lcom/facebook/goodwill/publish/GoodwillPublishLifeEventParam;->c:Ljava/util/List;

    .line 1863649
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v3, "["

    invoke-direct {v6, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1863650
    const/4 v3, 0x0

    .line 1863651
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v4, v3

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/goodwill/publish/GoodwillPublishPhoto;

    .line 1863652
    iget-object p0, v3, Lcom/facebook/goodwill/publish/GoodwillPublishPhoto;->a:Ljava/lang/String;

    move-object p0, p0

    .line 1863653
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p0

    if-nez p0, :cond_3

    .line 1863654
    if-lez v4, :cond_0

    .line 1863655
    const-string p0, ","

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1863656
    :cond_0
    iget-object p0, v3, Lcom/facebook/goodwill/publish/GoodwillPublishPhoto;->a:Ljava/lang/String;

    move-object v3, p0

    .line 1863657
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1863658
    add-int/lit8 v3, v4, 0x1

    :goto_1
    move v4, v3

    .line 1863659
    goto :goto_0

    .line 1863660
    :cond_1
    const-string v3, "]"

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1863661
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v2, v3

    .line 1863662
    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1863663
    :cond_2
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "description"

    iget-object v2, p1, Lcom/facebook/goodwill/publish/GoodwillPublishLifeEventParam;->d:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1863664
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "source"

    iget-object v2, p1, Lcom/facebook/goodwill/publish/GoodwillPublishLifeEventParam;->e:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1863665
    const-string v0, "%s/life_events"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p1, Lcom/facebook/goodwill/publish/GoodwillPublishLifeEventParam;->a:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 1863666
    new-instance v0, LX/14N;

    const-string v1, "goodwillLifeEventsPost"

    const-string v2, "POST"

    sget-object v4, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    sget-object v6, LX/14S;->JSON:LX/14S;

    invoke-direct/range {v0 .. v6}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/http/interfaces/RequestPriority;Ljava/util/List;LX/14S;)V

    return-object v0

    :cond_3
    move v3, v4

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1863640
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 1863641
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    const-string v1, "id"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1863642
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    const-string v1, "id"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v0

    .line 1863643
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
