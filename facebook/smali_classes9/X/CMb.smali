.class public final LX/CMb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/model/threads/ThreadSummary;

.field public final synthetic b:Lcom/facebook/messaging/ephemeral/ModifyThreadEphemeralityDialog;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/ephemeral/ModifyThreadEphemeralityDialog;Lcom/facebook/messaging/model/threads/ThreadSummary;)V
    .locals 0

    .prologue
    .line 1880773
    iput-object p1, p0, LX/CMb;->b:Lcom/facebook/messaging/ephemeral/ModifyThreadEphemeralityDialog;

    iput-object p2, p0, LX/CMb;->a:Lcom/facebook/messaging/model/threads/ThreadSummary;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    .prologue
    .line 1880774
    iget-object v0, p0, LX/CMb;->b:Lcom/facebook/messaging/ephemeral/ModifyThreadEphemeralityDialog;

    iget v0, v0, Lcom/facebook/messaging/ephemeral/ModifyThreadEphemeralityDialog;->s:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 1880775
    iget-object v0, p0, LX/CMb;->b:Lcom/facebook/messaging/ephemeral/ModifyThreadEphemeralityDialog;

    iget-object v1, p0, LX/CMb;->a:Lcom/facebook/messaging/model/threads/ThreadSummary;

    iget-object v2, p0, LX/CMb;->b:Lcom/facebook/messaging/ephemeral/ModifyThreadEphemeralityDialog;

    iget-object v2, v2, Lcom/facebook/messaging/ephemeral/ModifyThreadEphemeralityDialog;->t:[I

    iget-object v3, p0, LX/CMb;->b:Lcom/facebook/messaging/ephemeral/ModifyThreadEphemeralityDialog;

    iget v3, v3, Lcom/facebook/messaging/ephemeral/ModifyThreadEphemeralityDialog;->s:I

    aget v2, v2, v3

    invoke-static {v0, v1, v2}, Lcom/facebook/messaging/ephemeral/ModifyThreadEphemeralityDialog;->a$redex0(Lcom/facebook/messaging/ephemeral/ModifyThreadEphemeralityDialog;Lcom/facebook/messaging/model/threads/ThreadSummary;I)V

    .line 1880776
    :cond_0
    iget-object v0, p0, LX/CMb;->b:Lcom/facebook/messaging/ephemeral/ModifyThreadEphemeralityDialog;

    iget-object v0, v0, Lcom/facebook/messaging/ephemeral/ModifyThreadEphemeralityDialog;->m:LX/CMf;

    iget-object v1, p0, LX/CMb;->b:Lcom/facebook/messaging/ephemeral/ModifyThreadEphemeralityDialog;

    iget-object v1, v1, Lcom/facebook/messaging/ephemeral/ModifyThreadEphemeralityDialog;->u:Ljava/lang/String;

    .line 1880777
    iget-object v2, v0, LX/CMf;->a:LX/0Zb;

    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p0, "ephemeral_confirm_change"

    invoke-direct {v3, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p0, "source"

    invoke-virtual {v3, p0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    invoke-interface {v2, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1880778
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 1880779
    return-void
.end method
