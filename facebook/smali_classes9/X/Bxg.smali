.class public LX/Bxg;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public final a:LX/3mL;

.field public final b:LX/Bxj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/Bxj",
            "<TE;>;"
        }
    .end annotation
.end field

.field public final c:Landroid/content/Context;


# direct methods
.method public constructor <init>(LX/3mL;LX/Bxj;Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1835846
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1835847
    iput-object p1, p0, LX/Bxg;->a:LX/3mL;

    .line 1835848
    iput-object p2, p0, LX/Bxg;->b:LX/Bxj;

    .line 1835849
    iput-object p3, p0, LX/Bxg;->c:Landroid/content/Context;

    .line 1835850
    return-void
.end method

.method public static a(LX/0QB;)LX/Bxg;
    .locals 6

    .prologue
    .line 1835851
    const-class v1, LX/Bxg;

    monitor-enter v1

    .line 1835852
    :try_start_0
    sget-object v0, LX/Bxg;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1835853
    sput-object v2, LX/Bxg;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1835854
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1835855
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1835856
    new-instance p0, LX/Bxg;

    invoke-static {v0}, LX/3mL;->a(LX/0QB;)LX/3mL;

    move-result-object v3

    check-cast v3, LX/3mL;

    invoke-static {v0}, LX/Bxj;->a(LX/0QB;)LX/Bxj;

    move-result-object v4

    check-cast v4, LX/Bxj;

    const-class v5, Landroid/content/Context;

    invoke-interface {v0, v5}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/Context;

    invoke-direct {p0, v3, v4, v5}, LX/Bxg;-><init>(LX/3mL;LX/Bxj;Landroid/content/Context;)V

    .line 1835857
    move-object v0, p0

    .line 1835858
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1835859
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Bxg;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1835860
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1835861
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
