.class public final LX/BH6;
.super LX/3x6;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1767621
    invoke-direct {p0}, LX/3x6;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Rect;Landroid/view/View;Landroid/support/v7/widget/RecyclerView;LX/1Ok;)V
    .locals 4

    .prologue
    .line 1767622
    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0bba

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 1767623
    invoke-static {p2}, Landroid/support/v7/widget/RecyclerView;->d(Landroid/view/View;)I

    move-result v2

    .line 1767624
    if-gez v2, :cond_0

    .line 1767625
    :goto_0
    return-void

    .line 1767626
    :cond_0
    invoke-virtual {p3}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()LX/1OR;

    move-result-object v0

    check-cast v0, LX/3wu;

    .line 1767627
    iget v3, v0, LX/3wu;->c:I

    move v0, v3

    .line 1767628
    rem-int v3, v2, v0

    .line 1767629
    div-int/2addr v2, v0

    .line 1767630
    if-lez v2, :cond_1

    .line 1767631
    iput v1, p1, Landroid/graphics/Rect;->top:I

    .line 1767632
    :cond_1
    mul-int v2, v3, v1

    div-int/2addr v2, v0

    iput v2, p1, Landroid/graphics/Rect;->left:I

    .line 1767633
    add-int/lit8 v2, v3, 0x1

    mul-int/2addr v2, v1

    div-int v0, v2, v0

    sub-int v0, v1, v0

    iput v0, p1, Landroid/graphics/Rect;->right:I

    goto :goto_0
.end method
