.class public final LX/Aqi;
.super LX/6oO;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;)V
    .locals 0

    .prologue
    .line 1718202
    iput-object p1, p0, LX/Aqi;->a:Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;

    invoke-direct {p0}, LX/6oO;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 4

    .prologue
    .line 1718203
    iget-object v0, p0, LX/Aqi;->a:Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;

    invoke-static {v0}, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->d$redex0(Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1718204
    iget-object v0, p0, LX/Aqi;->a:Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;

    iget-object v1, p0, LX/Aqi;->a:Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;

    invoke-static {v1}, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->d$redex0(Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;)Ljava/lang/String;

    move-result-object v1

    .line 1718205
    sget-object v2, LX/Aqq;->FETCH:LX/Aqq;

    invoke-static {v0, v2}, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->a$redex0(Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;LX/Aqq;)V

    .line 1718206
    const-string v2, " "

    const-string v3, "+"

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1718207
    new-instance v3, Ljava/lang/StringBuilder;

    const-string p0, "http://api.giphy.com/v1/gifs/search?q="

    invoke-direct {v3, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "&api_key=dc6zaTOxFJmzC"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1718208
    invoke-static {v0, v2}, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->c(Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    .line 1718209
    iget-object v2, v0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->a:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1Ck;

    const-string p0, "SearchGifsTask"

    new-instance p1, LX/Aqn;

    invoke-direct {p1, v0, v3, v1}, LX/Aqn;-><init>(Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;Lcom/google/common/util/concurrent/ListenableFuture;Ljava/lang/String;)V

    invoke-virtual {v2, p0, v3, p1}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1718210
    :goto_0
    return-void

    .line 1718211
    :cond_0
    iget-object v0, p0, LX/Aqi;->a:Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;

    invoke-static {v0}, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->e$redex0(Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;)V

    goto :goto_0
.end method
