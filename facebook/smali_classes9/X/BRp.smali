.class public final LX/BRp;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field public final synthetic a:LX/BRr;


# direct methods
.method public constructor <init>(LX/BRr;)V
    .locals 0

    .prologue
    .line 1784512
    iput-object p1, p0, LX/BRp;->a:LX/BRr;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 12

    .prologue
    .line 1784513
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1784514
    const/16 v1, 0x4c

    if-ge v0, v1, :cond_0

    .line 1784515
    iget-object v0, p0, LX/BRp;->a:LX/BRr;

    const/4 v1, 0x2

    .line 1784516
    iput v1, v0, LX/BRr;->g:I

    .line 1784517
    iget-object v0, p0, LX/BRp;->a:LX/BRr;

    iget-object v0, v0, LX/BRr;->i:LX/Fw2;

    if-eqz v0, :cond_0

    .line 1784518
    iget-object v0, p0, LX/BRp;->a:LX/BRr;

    iget-object v0, v0, LX/BRr;->i:LX/Fw2;

    .line 1784519
    iget-object v2, v0, LX/Fw2;->i:Lcom/facebook/timeline/header/TimelineProfileImageFrameController;

    iget-boolean v2, v2, Lcom/facebook/timeline/header/TimelineProfileImageFrameController;->h:Z

    if-eqz v2, :cond_1

    .line 1784520
    :goto_0
    iget-object v0, p0, LX/BRp;->a:LX/BRr;

    const/4 v1, 0x0

    .line 1784521
    iput-object v1, v0, LX/BRr;->i:LX/Fw2;

    .line 1784522
    :cond_0
    return-void

    .line 1784523
    :cond_1
    iget-object v2, v0, LX/Fw2;->i:Lcom/facebook/timeline/header/TimelineProfileImageFrameController;

    iget-object v3, v0, LX/Fw2;->i:Lcom/facebook/timeline/header/TimelineProfileImageFrameController;

    iget-object v3, v3, Lcom/facebook/timeline/header/TimelineProfileImageFrameController;->i:LX/BQ1;

    iget-object v4, v0, LX/Fw2;->a:LX/5SB;

    iget-boolean v5, v0, LX/Fw2;->b:Z

    iget-boolean v6, v0, LX/Fw2;->c:Z

    iget-boolean v7, v0, LX/Fw2;->d:Z

    iget-boolean v8, v0, LX/Fw2;->e:Z

    if-eqz v8, :cond_2

    const/4 v8, 0x0

    :goto_1
    iget-object v9, v0, LX/Fw2;->g:Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;

    iget-object v10, v0, LX/Fw2;->h:LX/0zw;

    const/4 v11, 0x1

    invoke-virtual/range {v2 .. v11}, Lcom/facebook/timeline/header/TimelineProfileImageFrameController;->a(LX/BQ1;LX/5SB;ZZZLandroid/view/View$OnClickListener;Lcom/facebook/caspian/ui/standardheader/StandardProfileImageView;LX/0zw;Z)V

    goto :goto_0

    :cond_2
    iget-object v8, v0, LX/Fw2;->f:Landroid/view/View$OnClickListener;

    goto :goto_1
.end method
