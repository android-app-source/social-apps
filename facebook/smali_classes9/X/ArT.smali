.class public LX/ArT;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingDataSpec$ProvidesInspirationLoggingData;",
        ":",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationStateSpec$ProvidesInspirationState;",
        "Mutation::",
        "Lcom/facebook/ipc/composer/dataaccessor/ComposerBasicSetters$SetsUserHasInteracted",
        "<TMutation;>;:",
        "Lcom/facebook/ipc/composer/dataaccessor/ComposerCanSave;",
        ":",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingDataSpec$SetsInspirationLoggingData",
        "<TMutation;>;:",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationStateSpec$SetsInspirationState",
        "<TMutation;>;Services::",
        "LX/0il",
        "<TModelData;>;:",
        "LX/0im",
        "<TMutation;>;>",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public a:LX/0SG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/ArL;

.field public c:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TServices;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/ArL;LX/0il;)V
    .locals 1
    .param p1    # LX/ArL;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/0il;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/ArL;",
            "TServices;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1719252
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1719253
    iput-object p1, p0, LX/ArT;->b:LX/ArL;

    .line 1719254
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/ArT;->c:Ljava/lang/ref/WeakReference;

    .line 1719255
    return-void
.end method

.method public static a(LX/ArT;Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;)V
    .locals 2

    .prologue
    .line 1719250
    iget-object v0, p0, LX/ArT;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    check-cast v0, LX/0im;

    invoke-interface {v0}, LX/0im;->c()LX/0jJ;

    move-result-object v0

    const-class v1, LX/ArT;

    invoke-static {v1}, LX/0jK;->a(Ljava/lang/Class;)LX/0jK;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v0

    check-cast v0, LX/0jL;

    check-cast v0, LX/0jL;

    invoke-virtual {v0, p1}, LX/0jL;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jL;

    check-cast v0, LX/0jL;

    invoke-virtual {v0}, LX/0jL;->a()V

    .line 1719251
    return-void
.end method


# virtual methods
.method public final a(LX/ArJ;)V
    .locals 10

    .prologue
    .line 1719247
    iget-object v1, p0, LX/ArT;->b:LX/ArL;

    iget-object v0, p0, LX/ArT;->a:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v2

    iget-object v0, p0, LX/ArT;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->l()Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->getInspirationSessionStartTime()J

    move-result-wide v4

    sub-long/2addr v2, v4

    long-to-float v0, v2

    const/high16 v2, 0x447a0000    # 1000.0f

    div-float/2addr v0, v2

    .line 1719248
    sget-object v6, LX/ArH;->END_INSPIRATION_SESSION:LX/ArH;

    invoke-static {v1, v6, p1}, LX/ArL;->a(LX/ArL;LX/5oU;LX/ArJ;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    sget-object v7, LX/ArI;->DURATION:LX/ArI;

    invoke-virtual {v7}, LX/ArI;->getName()Ljava/lang/String;

    move-result-object v7

    float-to-double v8, v0

    invoke-virtual {v6, v7, v8, v9}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    invoke-static {v1, v6}, LX/ArL;->a(LX/ArL;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1719249
    return-void
.end method

.method public final b(LX/ArJ;)V
    .locals 1

    .prologue
    .line 1719214
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/ArT;->b(LX/ArJ;Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;)V

    .line 1719215
    return-void
.end method

.method public final b(LX/ArJ;Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;)V
    .locals 4
    .param p2    # Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1719241
    iget-object v0, p0, LX/ArT;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->l()Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;)Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;

    move-result-object v0

    iget-object v1, p0, LX/ArT;->a:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->setCameraSessionStartTime(J)Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;

    move-result-object v0

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->setCameraSessionId(Ljava/lang/String;)Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;

    move-result-object v0

    sget-object v1, LX/874;->FULLSCREEN_CAMERA:LX/874;

    invoke-virtual {v0, v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->setLoggingSurface(LX/874;)Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    move-result-object v0

    invoke-static {p0, v0}, LX/ArT;->a(LX/ArT;Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;)V

    .line 1719242
    iget-object v0, p0, LX/ArT;->b:LX/ArL;

    .line 1719243
    sget-object v1, LX/ArH;->START_CAMERA_SESSION:LX/ArH;

    invoke-static {v0, v1, p1}, LX/ArL;->a(LX/ArL;LX/5oU;LX/ArJ;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v2}, LX/ArL;->a(LX/ArL;Z)Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 1719244
    invoke-static {v1, p2}, LX/ArL;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1719245
    invoke-static {v0, v1}, LX/ArL;->a(LX/ArL;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1719246
    return-void
.end method

.method public final c(LX/ArJ;)V
    .locals 11

    .prologue
    const/4 v6, 0x0

    .line 1719237
    iget-object v1, p0, LX/ArT;->b:LX/ArL;

    iget-object v0, p0, LX/ArT;->a:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v2

    iget-object v0, p0, LX/ArT;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->l()Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->getCameraSessionStartTime()J

    move-result-wide v4

    sub-long/2addr v2, v4

    long-to-float v0, v2

    const/high16 v2, 0x447a0000    # 1000.0f

    div-float/2addr v0, v2

    .line 1719238
    sget-object v7, LX/ArH;->END_CAMERA_SESSION:LX/ArH;

    invoke-static {v1, v7, p1}, LX/ArL;->a(LX/ArL;LX/5oU;LX/ArJ;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    sget-object v8, LX/ArI;->DURATION:LX/ArI;

    invoke-virtual {v8}, LX/ArI;->getName()Ljava/lang/String;

    move-result-object v8

    float-to-double v9, v0

    invoke-virtual {v7, v8, v9, v10}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    invoke-static {v1, v7}, LX/ArL;->a(LX/ArL;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1719239
    iget-object v0, p0, LX/ArT;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->l()Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;)Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->setCameraSessionId(Ljava/lang/String;)Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->setLoggingSurface(LX/874;)Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    move-result-object v0

    invoke-static {p0, v0}, LX/ArT;->a(LX/ArT;Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;)V

    .line 1719240
    return-void
.end method

.method public final c(LX/ArJ;Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;)V
    .locals 4
    .param p2    # Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1719256
    iget-object v0, p0, LX/ArT;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->l()Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;)Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;

    move-result-object v0

    iget-object v1, p0, LX/ArT;->a:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->setImpressionStartTime(J)Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    move-result-object v0

    invoke-static {p0, v0}, LX/ArT;->a(LX/ArT;Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;)V

    .line 1719257
    iget-object v0, p0, LX/ArT;->b:LX/ArL;

    .line 1719258
    sget-object v1, LX/ArH;->IMPRESSION:LX/ArH;

    invoke-static {v0, v1, p1}, LX/ArL;->a(LX/ArL;LX/5oU;LX/ArJ;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v2}, LX/ArL;->a(LX/ArL;Z)Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    sget-object v2, LX/ArI;->EXTRA_ANNOTATIONS_DATA:LX/ArI;

    invoke-virtual {v2}, LX/ArI;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, LX/ArL;->i(LX/ArL;)LX/0lF;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 1719259
    invoke-static {v1, p2}, LX/ArL;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1719260
    invoke-static {v0, v1}, LX/ArL;->a(LX/ArL;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1719261
    return-void
.end method

.method public final d(LX/ArJ;)V
    .locals 4

    .prologue
    .line 1719233
    iget-object v0, p0, LX/ArT;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->l()Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;)Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;

    move-result-object v0

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->setEditingSessionId(Ljava/lang/String;)Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;

    move-result-object v0

    iget-object v1, p0, LX/ArT;->a:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->setEditingSessionStartTime(J)Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;

    move-result-object v0

    sget-object v1, LX/874;->EDITING_VIEW:LX/874;

    invoke-virtual {v0, v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->setLoggingSurface(LX/874;)Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    move-result-object v0

    invoke-static {p0, v0}, LX/ArT;->a(LX/ArT;Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;)V

    .line 1719234
    iget-object v0, p0, LX/ArT;->b:LX/ArL;

    .line 1719235
    sget-object v1, LX/ArH;->START_EDITING_SESSION:LX/ArH;

    invoke-static {v0, v1, p1}, LX/ArL;->a(LX/ArL;LX/5oU;LX/ArJ;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v2}, LX/ArL;->a(LX/ArL;Z)Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-static {v0, v1}, LX/ArL;->a(LX/ArL;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1719236
    return-void
.end method

.method public final e(LX/ArJ;)V
    .locals 11

    .prologue
    const/4 v6, 0x0

    .line 1719229
    iget-object v1, p0, LX/ArT;->b:LX/ArL;

    iget-object v0, p0, LX/ArT;->a:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v2

    iget-object v0, p0, LX/ArT;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->l()Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->getEditingSessionStartTime()J

    move-result-wide v4

    sub-long/2addr v2, v4

    long-to-float v0, v2

    const/high16 v2, 0x447a0000    # 1000.0f

    div-float/2addr v0, v2

    .line 1719230
    sget-object v7, LX/ArH;->END_EDITING_SESSION:LX/ArH;

    invoke-static {v1, v7, p1}, LX/ArL;->a(LX/ArL;LX/5oU;LX/ArJ;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const/4 v8, 0x0

    invoke-static {v1, v8}, LX/ArL;->a(LX/ArL;Z)Ljava/util/Map;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    sget-object v8, LX/ArI;->DURATION:LX/ArI;

    invoke-virtual {v8}, LX/ArI;->getName()Ljava/lang/String;

    move-result-object v8

    float-to-double v9, v0

    invoke-virtual {v7, v8, v9, v10}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    invoke-static {v1, v7}, LX/ArL;->a(LX/ArL;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1719231
    iget-object v0, p0, LX/ArT;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->l()Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;)Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->setEditingSessionId(Ljava/lang/String;)Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->setLoggingSurface(LX/874;)Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    move-result-object v0

    invoke-static {p0, v0}, LX/ArT;->a(LX/ArT;Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;)V

    .line 1719232
    return-void
.end method

.method public final g(LX/ArJ;)V
    .locals 10

    .prologue
    .line 1719225
    iget-object v1, p0, LX/ArT;->b:LX/ArL;

    iget-object v0, p0, LX/ArT;->a:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v2

    iget-object v0, p0, LX/ArT;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->l()Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->getShareSheetSessionStartTime()J

    move-result-wide v4

    sub-long/2addr v2, v4

    long-to-float v0, v2

    const/high16 v2, 0x447a0000    # 1000.0f

    div-float/2addr v0, v2

    .line 1719226
    sget-object v6, LX/ArH;->END_SHARE_SHEET_SESSION:LX/ArH;

    invoke-static {v1, v6, p1}, LX/ArL;->a(LX/ArL;LX/5oU;LX/ArJ;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const/4 v7, 0x0

    invoke-static {v1, v7}, LX/ArL;->a(LX/ArL;Z)Ljava/util/Map;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    sget-object v7, LX/ArI;->EXTRA_ANNOTATIONS_DATA:LX/ArI;

    invoke-virtual {v7}, LX/ArI;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-static {v1}, LX/ArL;->i(LX/ArL;)LX/0lF;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    sget-object v7, LX/ArI;->DURATION:LX/ArI;

    invoke-virtual {v7}, LX/ArI;->getName()Ljava/lang/String;

    move-result-object v7

    float-to-double v8, v0

    invoke-virtual {v6, v7, v8, v9}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    invoke-static {v1, v6}, LX/ArL;->a(LX/ArL;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1719227
    iget-object v0, p0, LX/ArT;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->l()Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;)Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->setShareSheetSessionId(Ljava/lang/String;)Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    move-result-object v0

    invoke-static {p0, v0}, LX/ArT;->a(LX/ArT;Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;)V

    .line 1719228
    return-void
.end method

.method public final k(LX/ArJ;)V
    .locals 10

    .prologue
    .line 1719221
    iget-object v1, p0, LX/ArT;->b:LX/ArL;

    iget-object v0, p0, LX/ArT;->a:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v2

    iget-object v0, p0, LX/ArT;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->l()Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->getGallerySessionStartTime()J

    move-result-wide v4

    sub-long/2addr v2, v4

    long-to-float v0, v2

    const/high16 v2, 0x447a0000    # 1000.0f

    div-float/2addr v0, v2

    .line 1719222
    sget-object v6, LX/ArH;->END_GALLERY_SESSION:LX/ArH;

    invoke-static {v1, v6, p1}, LX/ArL;->a(LX/ArL;LX/5oU;LX/ArJ;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const/4 v7, 0x0

    invoke-static {v1, v7}, LX/ArL;->a(LX/ArL;Z)Ljava/util/Map;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    sget-object v7, LX/ArI;->DURATION:LX/ArI;

    invoke-virtual {v7}, LX/ArI;->getName()Ljava/lang/String;

    move-result-object v7

    float-to-double v8, v0

    invoke-virtual {v6, v7, v8, v9}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    invoke-static {v1, v6}, LX/ArL;->a(LX/ArL;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1719223
    iget-object v0, p0, LX/ArT;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->l()Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;)Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->setGallerySessionId(Ljava/lang/String;)Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    move-result-object v0

    invoke-static {p0, v0}, LX/ArT;->a(LX/ArT;Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;)V

    .line 1719224
    return-void
.end method

.method public final p(LX/ArJ;)V
    .locals 1

    .prologue
    .line 1719219
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/ArT;->c(LX/ArJ;Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;)V

    .line 1719220
    return-void
.end method

.method public final q(LX/ArJ;)V
    .locals 10

    .prologue
    .line 1719216
    iget-object v1, p0, LX/ArT;->b:LX/ArL;

    iget-object v0, p0, LX/ArT;->a:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v2

    iget-object v0, p0, LX/ArT;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->l()Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->getImpressionStartTime()J

    move-result-wide v4

    sub-long/2addr v2, v4

    long-to-float v0, v2

    const/high16 v2, 0x447a0000    # 1000.0f

    div-float/2addr v0, v2

    .line 1719217
    sget-object v6, LX/ArH;->IMPRESSION_END:LX/ArH;

    invoke-static {v1, v6, p1}, LX/ArL;->a(LX/ArL;LX/5oU;LX/ArJ;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    sget-object v7, LX/ArI;->DURATION:LX/ArI;

    invoke-virtual {v7}, LX/ArI;->getName()Ljava/lang/String;

    move-result-object v7

    float-to-double v8, v0

    invoke-virtual {v6, v7, v8, v9}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    const/4 v7, 0x1

    invoke-static {v1, v7}, LX/ArL;->a(LX/ArL;Z)Ljava/util/Map;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    sget-object v7, LX/ArI;->EXTRA_ANNOTATIONS_DATA:LX/ArI;

    invoke-virtual {v7}, LX/ArI;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-static {v1}, LX/ArL;->i(LX/ArL;)LX/0lF;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    invoke-static {v1, v6}, LX/ArL;->a(LX/ArL;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1719218
    return-void
.end method
