.class public LX/AgN;
.super Landroid/graphics/drawable/Drawable;
.source ""


# static fields
.field private static final a:Landroid/graphics/Rect;

.field private static final b:Landroid/graphics/Path;


# instance fields
.field private final c:Landroid/view/animation/Interpolator;

.field private final d:Landroid/content/res/Resources;

.field private final e:Landroid/graphics/Paint;

.field public final f:Landroid/animation/ValueAnimator;

.field private g:I

.field private h:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1700672
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, LX/AgN;->a:Landroid/graphics/Rect;

    .line 1700673
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    sput-object v0, LX/AgN;->b:Landroid/graphics/Path;

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1700674
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 1700675
    iput-object p1, p0, LX/AgN;->d:Landroid/content/res/Resources;

    .line 1700676
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, LX/AgN;->e:Landroid/graphics/Paint;

    .line 1700677
    iget-object v0, p0, LX/AgN;->e:Landroid/graphics/Paint;

    iget-object v1, p0, LX/AgN;->d:Landroid/content/res/Resources;

    const v2, 0x7f0b0477

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1700678
    iget-object v0, p0, LX/AgN;->e:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Cap;->BUTT:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 1700679
    iget-object v0, p0, LX/AgN;->e:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1700680
    iget-object v0, p0, LX/AgN;->d:Landroid/content/res/Resources;

    const v1, 0x7f0a0370

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, LX/AgN;->g:I

    .line 1700681
    iget v0, p0, LX/AgN;->g:I

    iput v0, p0, LX/AgN;->h:I

    .line 1700682
    const v0, 0x3d99999a    # 0.075f

    const v1, 0x3f51eb85    # 0.82f

    const v2, 0x3e28f5c3    # 0.165f

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-static {v0, v1, v2, v3}, LX/2pJ;->a(FFFF)Landroid/view/animation/Interpolator;

    move-result-object v0

    iput-object v0, p0, LX/AgN;->c:Landroid/view/animation/Interpolator;

    .line 1700683
    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, LX/AgN;->f:Landroid/animation/ValueAnimator;

    .line 1700684
    iget-object v0, p0, LX/AgN;->f:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0x258

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 1700685
    iget-object v0, p0, LX/AgN;->f:Landroid/animation/ValueAnimator;

    iget-object v1, p0, LX/AgN;->c:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1700686
    iget-object v0, p0, LX/AgN;->f:Landroid/animation/ValueAnimator;

    new-instance v1, LX/AgM;

    invoke-direct {v1, p0}, LX/AgM;-><init>(LX/AgN;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 1700687
    return-void

    .line 1700688
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method


# virtual methods
.method public final a(I)V
    .locals 0

    .prologue
    .line 1700689
    iput p1, p0, LX/AgN;->h:I

    .line 1700690
    return-void
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    const/high16 v5, 0x40000000    # 2.0f

    .line 1700691
    iget-object v0, p0, LX/AgN;->f:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1700692
    :cond_0
    :goto_0
    return-void

    .line 1700693
    :cond_1
    sget-object v0, LX/AgN;->a:Landroid/graphics/Rect;

    invoke-virtual {p0, v0}, LX/AgN;->copyBounds(Landroid/graphics/Rect;)V

    .line 1700694
    sget-object v0, LX/AgN;->a:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    sget-object v1, LX/AgN;->a:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v5

    .line 1700695
    const/high16 v1, 0x3f000000    # 0.5f

    mul-float/2addr v1, v0

    .line 1700696
    neg-float v2, v1

    .line 1700697
    sub-float v3, v0, v2

    iget-object v4, p0, LX/AgN;->f:Landroid/animation/ValueAnimator;

    invoke-virtual {v4}, Landroid/animation/ValueAnimator;->getAnimatedFraction()F

    move-result v4

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    .line 1700698
    add-float/2addr v1, v2

    .line 1700699
    const/4 v3, 0x0

    invoke-static {v3, v2}, Ljava/lang/Math;->max(FF)F

    move-result v2

    .line 1700700
    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 1700701
    iget-object v1, p0, LX/AgN;->e:Landroid/graphics/Paint;

    invoke-virtual {v1}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v1

    div-float/2addr v1, v5

    .line 1700702
    add-float/2addr v2, v1

    .line 1700703
    sub-float/2addr v0, v1

    .line 1700704
    cmpl-float v1, v2, v0

    if-gtz v1, :cond_0

    .line 1700705
    sget-object v1, LX/AgN;->b:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->reset()V

    .line 1700706
    sget-object v1, LX/AgN;->b:Landroid/graphics/Path;

    sget-object v3, LX/AgN;->a:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->centerX()I

    move-result v3

    int-to-float v3, v3

    sget-object v4, LX/AgN;->a:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->centerY()I

    move-result v4

    int-to-float v4, v4

    add-float/2addr v2, v4

    invoke-virtual {v1, v3, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 1700707
    sget-object v1, LX/AgN;->b:Landroid/graphics/Path;

    sget-object v2, LX/AgN;->a:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->centerX()I

    move-result v2

    int-to-float v2, v2

    sget-object v3, LX/AgN;->a:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->centerY()I

    move-result v3

    int-to-float v3, v3

    add-float/2addr v0, v3

    invoke-virtual {v1, v2, v0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1700708
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 1700709
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    const/16 v0, 0xa

    if-ge v1, v0, :cond_3

    .line 1700710
    iget-object v2, p0, LX/AgN;->e:Landroid/graphics/Paint;

    rem-int/lit8 v0, v1, 0x2

    if-nez v0, :cond_2

    iget v0, p0, LX/AgN;->h:I

    :goto_2
    invoke-virtual {v2, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 1700711
    sget-object v0, LX/AgN;->b:Landroid/graphics/Path;

    iget-object v2, p0, LX/AgN;->e:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 1700712
    const/high16 v0, 0x42100000    # 36.0f

    sget-object v2, LX/AgN;->a:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->centerX()I

    move-result v2

    int-to-float v2, v2

    sget-object v3, LX/AgN;->a:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->centerY()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p1, v0, v2, v3}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 1700713
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1700714
    :cond_2
    iget v0, p0, LX/AgN;->g:I

    goto :goto_2

    .line 1700715
    :cond_3
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    goto/16 :goto_0
.end method

.method public final getOpacity()I
    .locals 1

    .prologue
    .line 1700716
    iget-object v0, p0, LX/AgN;->e:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getAlpha()I

    move-result v0

    return v0
.end method

.method public final setAlpha(I)V
    .locals 1

    .prologue
    .line 1700717
    iget-object v0, p0, LX/AgN;->e:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 1700718
    return-void
.end method

.method public final setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 1

    .prologue
    .line 1700719
    iget-object v0, p0, LX/AgN;->e:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 1700720
    return-void
.end method
