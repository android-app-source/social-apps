.class public final LX/CYU;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 1911228
    const-class v1, Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel;

    const v0, 0x61ad591b

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "TabDataQuery"

    const-string v6, "a216b529ced71c57e2cdc6bc5f451c86"

    const-string v7, "page"

    const-string v8, "10155261262306729"

    const-string v9, "10155263176681729"

    .line 1911229
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 1911230
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 1911231
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1911232
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1911233
    sparse-switch v0, :sswitch_data_0

    .line 1911234
    :goto_0
    return-object p1

    .line 1911235
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 1911236
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 1911237
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 1911238
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 1911239
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 1911240
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x71e86c6d -> :sswitch_5
        -0x360f025c -> :sswitch_1
        -0x2fe52f35 -> :sswitch_0
        -0x69978cf -> :sswitch_3
        0x1c910e9a -> :sswitch_2
        0x78326898 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1911241
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_1

    .line 1911242
    :goto_1
    return v0

    .line 1911243
    :pswitch_0
    const-string v2, "2"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    .line 1911244
    :pswitch_1
    const-string v0, "undefined"

    invoke-static {p2, v0}, LX/0wE;->a(Ljava/lang/Object;Ljava/lang/String;)Z

    move-result v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x32
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method
