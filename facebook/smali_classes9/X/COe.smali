.class public final LX/COe;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/COg;",
        ">;"
    }
.end annotation


# static fields
.field private static b:[Ljava/lang/String;

.field private static c:I


# instance fields
.field public a:LX/COf;

.field public d:Ljava/util/BitSet;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x3

    .line 1883346
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "template"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "templateContext"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "wrappers"

    aput-object v2, v0, v1

    sput-object v0, LX/COe;->b:[Ljava/lang/String;

    .line 1883347
    sput v3, LX/COe;->c:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1883348
    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 1883349
    new-instance v0, Ljava/util/BitSet;

    sget v1, LX/COe;->c:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/COe;->d:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/COe;LX/1De;IILX/COf;)V
    .locals 1

    .prologue
    .line 1883350
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 1883351
    iput-object p4, p0, LX/COe;->a:LX/COf;

    .line 1883352
    iget-object v0, p0, LX/COe;->d:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 1883353
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1883354
    invoke-super {p0}, LX/1X5;->a()V

    .line 1883355
    const/4 v0, 0x0

    iput-object v0, p0, LX/COe;->a:LX/COf;

    .line 1883356
    sget-object v0, LX/COg;->b:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 1883357
    return-void
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/COg;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1883358
    iget-object v1, p0, LX/COe;->d:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/COe;->d:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    sget v2, LX/COe;->c:I

    if-ge v1, v2, :cond_2

    .line 1883359
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1883360
    :goto_0
    sget v2, LX/COe;->c:I

    if-ge v0, v2, :cond_1

    .line 1883361
    iget-object v2, p0, LX/COe;->d:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1883362
    sget-object v2, LX/COe;->b:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1883363
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1883364
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1883365
    :cond_2
    iget-object v0, p0, LX/COe;->a:LX/COf;

    .line 1883366
    invoke-virtual {p0}, LX/COe;->a()V

    .line 1883367
    return-object v0
.end method
