.class public final LX/Bou;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/feed/rows/permalink/PermalinkProfilePhotoPromptPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/permalink/PermalinkProfilePhotoPromptPartDefinition;)V
    .locals 0

    .prologue
    .line 1822260
    iput-object p1, p0, LX/Bou;->a:Lcom/facebook/feed/rows/permalink/PermalinkProfilePhotoPromptPartDefinition;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, 0x235dac94

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1822261
    iget-object v1, p0, LX/Bou;->a:Lcom/facebook/feed/rows/permalink/PermalinkProfilePhotoPromptPartDefinition;

    iget-object v1, v1, Lcom/facebook/feed/rows/permalink/PermalinkProfilePhotoPromptPartDefinition;->e:LX/749;

    iget-object v2, p0, LX/Bou;->a:Lcom/facebook/feed/rows/permalink/PermalinkProfilePhotoPromptPartDefinition;

    iget-object v2, v2, Lcom/facebook/feed/rows/permalink/PermalinkProfilePhotoPromptPartDefinition;->f:Ljava/lang/String;

    .line 1822262
    new-instance p0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object p1, LX/748;->PROFILE_PHOTO_PROMPT_DISMISSED:LX/748;

    invoke-virtual {p1}, LX/748;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1822263
    invoke-static {v1, p0, v2}, LX/749;->a(LX/749;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;)V

    .line 1822264
    const v1, -0x4d3b0f5c

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
