.class public final LX/ATc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;)V
    .locals 0

    .prologue
    .line 1676238
    iput-object p1, p0, LX/ATc;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, -0xb76695d

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1676239
    iget-object v0, p0, LX/ATc;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v0, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aq:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/ATc;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v0, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aq:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AT6;

    new-instance v2, Landroid/graphics/PointF;

    iget-object v3, p0, LX/ATc;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v3, v3, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aj:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->x:I

    int-to-float v3, v3

    iget-object v4, p0, LX/ATc;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v4, v4, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aj:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->y:I

    int-to-float v4, v4

    invoke-direct {v2, v3, v4}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1676240
    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1676241
    iget-object v3, v0, LX/AT6;->d:Lcom/facebook/photos/tagging/shared/FaceBoxesView;

    invoke-virtual {v3, v2}, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->a(Landroid/graphics/PointF;)Z

    move-result v3

    move v0, v3

    .line 1676242
    if-eqz v0, :cond_0

    .line 1676243
    const v0, 0x26107d6c

    invoke-static {v5, v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1676244
    :goto_0
    return-void

    .line 1676245
    :cond_0
    iget-object v0, p0, LX/ATc;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v0, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ad:LX/ATL;

    iget-object v2, p0, LX/ATc;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v2, v2, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v0, v2}, LX/ATL;->b(Lcom/facebook/composer/attachments/ComposerAttachment;)V

    .line 1676246
    const v0, -0x51b28d73

    invoke-static {v0, v1}, LX/02F;->a(II)V

    goto :goto_0
.end method
