.class public final LX/BdU;
.super LX/BcO;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/BcO",
        "<",
        "LX/BdW;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public b:Lcom/facebook/components/list/fb/fragment/ListComponentFragment;

.field public c:I

.field public d:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field public e:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1803802
    invoke-static {}, LX/BdW;->d()LX/BdW;

    move-result-object v0

    invoke-direct {p0, v0}, LX/BcO;-><init>(LX/BcS;)V

    .line 1803803
    return-void
.end method


# virtual methods
.method public final a(Z)LX/BcO;
    .locals 2

    .prologue
    .line 1803804
    invoke-super {p0, p1}, LX/BcO;->a(Z)LX/BcO;

    move-result-object v0

    check-cast v0, LX/BdU;

    .line 1803805
    if-nez p1, :cond_0

    .line 1803806
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/BdU;->e:Z

    .line 1803807
    :cond_0
    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1803808
    if-ne p0, p1, :cond_1

    .line 1803809
    :cond_0
    :goto_0
    return v0

    .line 1803810
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1803811
    goto :goto_0

    .line 1803812
    :cond_3
    check-cast p1, LX/BdU;

    .line 1803813
    iget-object v2, p0, LX/BdU;->b:Lcom/facebook/components/list/fb/fragment/ListComponentFragment;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/BdU;->b:Lcom/facebook/components/list/fb/fragment/ListComponentFragment;

    iget-object v3, p1, LX/BdU;->b:Lcom/facebook/components/list/fb/fragment/ListComponentFragment;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1803814
    goto :goto_0

    .line 1803815
    :cond_5
    iget-object v2, p1, LX/BdU;->b:Lcom/facebook/components/list/fb/fragment/ListComponentFragment;

    if-nez v2, :cond_4

    .line 1803816
    :cond_6
    iget v2, p0, LX/BdU;->c:I

    iget v3, p1, LX/BdU;->c:I

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 1803817
    goto :goto_0

    .line 1803818
    :cond_7
    iget-object v2, p0, LX/BdU;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    if-eqz v2, :cond_9

    iget-object v2, p0, LX/BdU;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget-object v3, p1, LX/BdU;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    :cond_8
    move v0, v1

    .line 1803819
    goto :goto_0

    .line 1803820
    :cond_9
    iget-object v2, p1, LX/BdU;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    if-nez v2, :cond_8

    .line 1803821
    :cond_a
    iget-boolean v2, p0, LX/BdU;->e:Z

    iget-boolean v3, p1, LX/BdU;->e:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 1803822
    goto :goto_0
.end method
