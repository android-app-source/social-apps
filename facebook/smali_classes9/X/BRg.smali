.class public LX/BRg;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/view/MenuItem$OnMenuItemClickListener;

.field private final b:Landroid/widget/NumberPicker$OnValueChangeListener;

.field private final c:Landroid/widget/NumberPicker$OnValueChangeListener;

.field private final d:Landroid/widget/NumberPicker$OnValueChangeListener;

.field private final e:Landroid/widget/NumberPicker$OnValueChangeListener;

.field private final f:Landroid/content/DialogInterface$OnClickListener;

.field private final g:Landroid/content/DialogInterface$OnClickListener;

.field public h:LX/2EJ;

.field public i:Landroid/widget/NumberPicker;

.field public j:Landroid/widget/NumberPicker;

.field public k:Landroid/widget/NumberPicker;

.field public l:Landroid/widget/NumberPicker;

.field public m:LX/5OM;

.field public n:Lcom/facebook/fig/button/FigButton;

.field public o:LX/BQt;

.field public final p:Landroid/content/Context;

.field public final q:LX/BRi;

.field private final r:LX/BRl;

.field private final s:LX/BRk;

.field private final t:LX/23P;

.field public final u:LX/0wM;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/BRj;LX/BRl;LX/BRk;LX/23P;LX/0wM;J)V
    .locals 1
    .param p7    # J
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1784289
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1784290
    new-instance v0, LX/BRY;

    invoke-direct {v0, p0}, LX/BRY;-><init>(LX/BRg;)V

    iput-object v0, p0, LX/BRg;->a:Landroid/view/MenuItem$OnMenuItemClickListener;

    .line 1784291
    new-instance v0, LX/BRZ;

    invoke-direct {v0, p0}, LX/BRZ;-><init>(LX/BRg;)V

    iput-object v0, p0, LX/BRg;->b:Landroid/widget/NumberPicker$OnValueChangeListener;

    .line 1784292
    new-instance v0, LX/BRa;

    invoke-direct {v0, p0}, LX/BRa;-><init>(LX/BRg;)V

    iput-object v0, p0, LX/BRg;->c:Landroid/widget/NumberPicker$OnValueChangeListener;

    .line 1784293
    new-instance v0, LX/BRb;

    invoke-direct {v0, p0}, LX/BRb;-><init>(LX/BRg;)V

    iput-object v0, p0, LX/BRg;->d:Landroid/widget/NumberPicker$OnValueChangeListener;

    .line 1784294
    new-instance v0, LX/BRc;

    invoke-direct {v0, p0}, LX/BRc;-><init>(LX/BRg;)V

    iput-object v0, p0, LX/BRg;->e:Landroid/widget/NumberPicker$OnValueChangeListener;

    .line 1784295
    new-instance v0, LX/BRd;

    invoke-direct {v0, p0}, LX/BRd;-><init>(LX/BRg;)V

    iput-object v0, p0, LX/BRg;->f:Landroid/content/DialogInterface$OnClickListener;

    .line 1784296
    new-instance v0, LX/BRe;

    invoke-direct {v0, p0}, LX/BRe;-><init>(LX/BRg;)V

    iput-object v0, p0, LX/BRg;->g:Landroid/content/DialogInterface$OnClickListener;

    .line 1784297
    iput-object p1, p0, LX/BRg;->p:Landroid/content/Context;

    .line 1784298
    invoke-static {p7, p8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 1784299
    new-instance p7, LX/BRi;

    invoke-static {p2}, LX/11R;->a(LX/0QB;)LX/11R;

    move-result-object p1

    check-cast p1, LX/11R;

    invoke-direct {p7, p1, v0}, LX/BRi;-><init>(LX/11R;Ljava/lang/Long;)V

    .line 1784300
    move-object v0, p7

    .line 1784301
    iput-object v0, p0, LX/BRg;->q:LX/BRi;

    .line 1784302
    iput-object p3, p0, LX/BRg;->r:LX/BRl;

    .line 1784303
    iput-object p4, p0, LX/BRg;->s:LX/BRk;

    .line 1784304
    iput-object p5, p0, LX/BRg;->t:LX/23P;

    .line 1784305
    iput-object p6, p0, LX/BRg;->u:LX/0wM;

    .line 1784306
    return-void
.end method

.method public static a$redex0(LX/BRg;Lcom/facebook/fig/button/FigButton;LX/BRi;)V
    .locals 3

    .prologue
    .line 1784285
    invoke-virtual {p2}, LX/BRi;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1784286
    iget-object v0, p0, LX/BRg;->t:LX/23P;

    iget-object v1, p0, LX/BRg;->p:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08281f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, LX/23P;->getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/fig/button/FigButton;->setText(Ljava/lang/CharSequence;)V

    .line 1784287
    :goto_0
    return-void

    .line 1784288
    :cond_0
    iget-object v0, p0, LX/BRg;->t:LX/23P;

    invoke-virtual {p2}, LX/BRi;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, LX/23P;->getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/fig/button/FigButton;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public static b(LX/BRg;)V
    .locals 6

    .prologue
    .line 1784245
    iget-object v0, p0, LX/BRg;->p:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p0, LX/BRg;->m:LX/5OM;

    iget-object v2, p0, LX/BRg;->a:Landroid/view/MenuItem$OnMenuItemClickListener;

    const/4 p0, 0x0

    .line 1784246
    invoke-virtual {v1}, LX/5OM;->c()LX/5OG;

    move-result-object v3

    .line 1784247
    const/4 v4, 0x1

    const v5, 0x7f082817

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, p0, v5}, LX/5OG;->a(IILjava/lang/CharSequence;)LX/3Ai;

    move-result-object v4

    invoke-virtual {v4, v2}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1784248
    const/4 v4, 0x2

    const v5, 0x7f082818

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, p0, v5}, LX/5OG;->a(IILjava/lang/CharSequence;)LX/3Ai;

    move-result-object v4

    invoke-virtual {v4, v2}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1784249
    const/4 v4, 0x3

    const v5, 0x7f082819

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, p0, v5}, LX/5OG;->a(IILjava/lang/CharSequence;)LX/3Ai;

    move-result-object v4

    invoke-virtual {v4, v2}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1784250
    const/4 v4, 0x4

    const v5, 0x7f08281a

    invoke-virtual {v3, v4, p0, v5}, LX/5OG;->a(III)LX/3Ai;

    move-result-object v4

    invoke-virtual {v4, v2}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1784251
    const/4 v4, 0x5

    const v5, 0x7f08281b

    invoke-virtual {v3, v4, p0, v5}, LX/5OG;->a(III)LX/3Ai;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1784252
    return-void
.end method

.method public static c(LX/BRg;)V
    .locals 13

    .prologue
    .line 1784272
    iget-object v0, p0, LX/BRg;->r:LX/BRl;

    iget-object v1, p0, LX/BRg;->q:LX/BRi;

    iget-object v2, p0, LX/BRg;->h:LX/2EJ;

    iget-object v3, p0, LX/BRg;->f:Landroid/content/DialogInterface$OnClickListener;

    iget-object v4, p0, LX/BRg;->g:Landroid/content/DialogInterface$OnClickListener;

    const/4 v12, 0x4

    const/4 v11, 0x0

    const/4 v8, -0x1

    .line 1784273
    invoke-virtual {v1}, LX/BRi;->e()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, LX/2EJ;->setTitle(Ljava/lang/CharSequence;)V

    .line 1784274
    iget-object v5, v0, LX/BRl;->a:Landroid/content/res/Resources;

    const v6, 0x7f082815

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v8, v5, v3}, LX/2EJ;->a(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 1784275
    const/4 v5, -0x2

    iget-object v6, v0, LX/BRl;->a:Landroid/content/res/Resources;

    const v7, 0x7f082816

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v5, v6, v4}, LX/2EJ;->a(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 1784276
    invoke-virtual {v2, v8}, LX/2EJ;->a(I)Landroid/widget/Button;

    move-result-object v5

    .line 1784277
    iget-object v6, v0, LX/BRl;->b:LX/0SG;

    invoke-interface {v6}, LX/0SG;->a()J

    move-result-wide v7

    const-wide/16 v9, 0x3e8

    div-long/2addr v7, v9

    .line 1784278
    invoke-virtual {v1}, LX/BRi;->d()J

    move-result-wide v9

    .line 1784279
    cmp-long v6, v7, v9

    if-ltz v6, :cond_1

    if-eqz v5, :cond_1

    invoke-virtual {v5}, Landroid/widget/Button;->getVisibility()I

    move-result v6

    if-nez v6, :cond_1

    .line 1784280
    invoke-virtual {v2}, LX/2EJ;->getContext()Landroid/content/Context;

    move-result-object v6

    iget-object v7, v0, LX/BRl;->a:Landroid/content/res/Resources;

    const v8, 0x7f08281d

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7, v11}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/Toast;->show()V

    .line 1784281
    invoke-virtual {v5, v12}, Landroid/widget/Button;->setVisibility(I)V

    .line 1784282
    :cond_0
    :goto_0
    return-void

    .line 1784283
    :cond_1
    cmp-long v6, v7, v9

    if-gez v6, :cond_0

    if-eqz v5, :cond_0

    invoke-virtual {v5}, Landroid/widget/Button;->getVisibility()I

    move-result v6

    if-ne v6, v12, :cond_0

    .line 1784284
    invoke-virtual {v5, v11}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0
.end method

.method public static d$redex0(LX/BRg;)V
    .locals 13

    .prologue
    .line 1784254
    iget-object v0, p0, LX/BRg;->s:LX/BRk;

    iget-object v1, p0, LX/BRg;->q:LX/BRi;

    iget-object v2, p0, LX/BRg;->i:Landroid/widget/NumberPicker;

    iget-object v3, p0, LX/BRg;->b:Landroid/widget/NumberPicker$OnValueChangeListener;

    iget-object v4, p0, LX/BRg;->j:Landroid/widget/NumberPicker;

    iget-object v5, p0, LX/BRg;->c:Landroid/widget/NumberPicker$OnValueChangeListener;

    iget-object v6, p0, LX/BRg;->k:Landroid/widget/NumberPicker;

    iget-object v7, p0, LX/BRg;->d:Landroid/widget/NumberPicker$OnValueChangeListener;

    iget-object v8, p0, LX/BRg;->l:Landroid/widget/NumberPicker;

    iget-object v9, p0, LX/BRg;->e:Landroid/widget/NumberPicker$OnValueChangeListener;

    const/4 p0, 0x1

    const/4 v12, 0x0

    .line 1784255
    invoke-virtual {v1}, LX/BRi;->f()Z

    move-result v10

    if-nez v10, :cond_0

    .line 1784256
    invoke-virtual {v1}, LX/BRi;->i()Z

    move-result v10

    if-eqz v10, :cond_1

    .line 1784257
    invoke-virtual {v1}, LX/BRi;->h()V

    .line 1784258
    :cond_0
    :goto_0
    invoke-static {}, LX/BRk;->a()[Ljava/lang/String;

    move-result-object v10

    .line 1784259
    iget v11, v1, LX/BRi;->e:I

    move v11, v11

    .line 1784260
    invoke-static {v2, v12, v10, v3, v11}, LX/BRk;->a(Landroid/widget/NumberPicker;I[Ljava/lang/String;Landroid/widget/NumberPicker$OnValueChangeListener;I)V

    .line 1784261
    iget-object v10, v0, LX/BRk;->b:Landroid/content/res/Resources;

    const v11, 0x7f100040

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v10

    .line 1784262
    iget v11, v1, LX/BRi;->f:I

    move v11, v11

    .line 1784263
    rem-int/lit8 v11, v11, 0xc

    invoke-static {v4, p0, v10, v5, v11}, LX/BRk;->a(Landroid/widget/NumberPicker;I[Ljava/lang/String;Landroid/widget/NumberPicker$OnValueChangeListener;I)V

    .line 1784264
    iget-object v10, v0, LX/BRk;->b:Landroid/content/res/Resources;

    const v11, 0x7f100041

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v10

    .line 1784265
    iget v11, v1, LX/BRi;->g:I

    move v11, v11

    .line 1784266
    div-int/lit8 v11, v11, 0xf

    invoke-static {v6, v12, v10, v7, v11}, LX/BRk;->a(Landroid/widget/NumberPicker;I[Ljava/lang/String;Landroid/widget/NumberPicker$OnValueChangeListener;I)V

    .line 1784267
    iget-object v10, v0, LX/BRk;->b:Landroid/content/res/Resources;

    const v11, 0x7f100042

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v10

    .line 1784268
    iget v11, v1, LX/BRi;->f:I

    move v11, v11

    .line 1784269
    div-int/lit8 v11, v11, 0xc

    invoke-static {v8, v12, v10, v9, v11}, LX/BRk;->a(Landroid/widget/NumberPicker;I[Ljava/lang/String;Landroid/widget/NumberPicker$OnValueChangeListener;I)V

    .line 1784270
    return-void

    .line 1784271
    :cond_1
    const/4 v10, 0x3

    invoke-virtual {v1, v10, p0}, LX/BRi;->a(II)V

    goto :goto_0
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 1784253
    iget-object v0, p0, LX/BRg;->q:LX/BRi;

    invoke-virtual {v0}, LX/BRi;->d()J

    move-result-wide v0

    return-wide v0
.end method
