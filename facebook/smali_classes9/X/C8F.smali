.class public final LX/C8F;
.super Landroid/text/style/ClickableSpan;
.source ""


# instance fields
.field public final synthetic a:LX/C8O;

.field public final synthetic b:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic c:LX/C8M;

.field public final synthetic d:Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;LX/C8O;Lcom/facebook/feed/rows/core/props/FeedProps;LX/C8M;)V
    .locals 0

    .prologue
    .line 1852567
    iput-object p1, p0, LX/C8F;->d:Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;

    iput-object p2, p0, LX/C8F;->a:LX/C8O;

    iput-object p3, p0, LX/C8F;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iput-object p4, p0, LX/C8F;->c:LX/C8M;

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 1852571
    iget-object v0, p0, LX/C8F;->d:Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;

    iget-object v1, p0, LX/C8F;->a:LX/C8O;

    iget-object v2, p0, LX/C8F;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p0, LX/C8F;->c:LX/C8M;

    .line 1852572
    new-instance p1, LX/C8K;

    invoke-direct {p1, v0, v1, v2, v3}, LX/C8K;-><init>(Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;LX/C8O;Lcom/facebook/feed/rows/core/props/FeedProps;LX/C8M;)V

    invoke-virtual {v1, p1}, LX/C8O;->a(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1852573
    iget-object v0, p0, LX/C8F;->a:LX/C8O;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/C8O;->setEnabled(Z)V

    .line 1852574
    return-void
.end method

.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 1

    .prologue
    .line 1852568
    iget v0, p1, Landroid/text/TextPaint;->linkColor:I

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 1852569
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    .line 1852570
    return-void
.end method
