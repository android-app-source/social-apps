.class public final LX/Bmd;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/Bme;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel$EventThemesModel$NodesModel;

.field public b:I

.field public c:I

.field public final synthetic d:LX/Bme;


# direct methods
.method public constructor <init>(LX/Bme;)V
    .locals 1

    .prologue
    .line 1818693
    iput-object p1, p0, LX/Bmd;->d:LX/Bme;

    .line 1818694
    move-object v0, p1

    .line 1818695
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1818696
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1818697
    const-string v0, "EventsThemeItem"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1818698
    if-ne p0, p1, :cond_1

    .line 1818699
    :cond_0
    :goto_0
    return v0

    .line 1818700
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1818701
    goto :goto_0

    .line 1818702
    :cond_3
    check-cast p1, LX/Bmd;

    .line 1818703
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1818704
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1818705
    if-eq v2, v3, :cond_0

    .line 1818706
    iget-object v2, p0, LX/Bmd;->a:Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel$EventThemesModel$NodesModel;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/Bmd;->a:Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel$EventThemesModel$NodesModel;

    iget-object v3, p1, LX/Bmd;->a:Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel$EventThemesModel$NodesModel;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1818707
    goto :goto_0

    .line 1818708
    :cond_5
    iget-object v2, p1, LX/Bmd;->a:Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel$EventThemesModel$NodesModel;

    if-nez v2, :cond_4

    .line 1818709
    :cond_6
    iget v2, p0, LX/Bmd;->b:I

    iget v3, p1, LX/Bmd;->b:I

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 1818710
    goto :goto_0

    .line 1818711
    :cond_7
    iget v2, p0, LX/Bmd;->c:I

    iget v3, p1, LX/Bmd;->c:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 1818712
    goto :goto_0
.end method
