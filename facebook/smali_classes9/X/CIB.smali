.class public final LX/CIB;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    .line 1872522
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1872523
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 1872524
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 1872525
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1872526
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_a

    .line 1872527
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1872528
    :goto_1
    move v1, v2

    .line 1872529
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1872530
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0

    .line 1872531
    :cond_1
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_7

    .line 1872532
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 1872533
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1872534
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_1

    if-eqz v10, :cond_1

    .line 1872535
    const-string v11, "is_compressed"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 1872536
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v4

    move v9, v4

    move v4, v3

    goto :goto_2

    .line 1872537
    :cond_2
    const-string v11, "is_font_family"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 1872538
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v1

    move v8, v1

    move v1, v3

    goto :goto_2

    .line 1872539
    :cond_3
    const-string v11, "resource_name"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 1872540
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_2

    .line 1872541
    :cond_4
    const-string v11, "resource_version"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 1872542
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_2

    .line 1872543
    :cond_5
    const-string v11, "url"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 1872544
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_2

    .line 1872545
    :cond_6
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_2

    .line 1872546
    :cond_7
    const/4 v10, 0x5

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 1872547
    if-eqz v4, :cond_8

    .line 1872548
    invoke-virtual {p1, v2, v9}, LX/186;->a(IZ)V

    .line 1872549
    :cond_8
    if-eqz v1, :cond_9

    .line 1872550
    invoke-virtual {p1, v3, v8}, LX/186;->a(IZ)V

    .line 1872551
    :cond_9
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v7}, LX/186;->b(II)V

    .line 1872552
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 1872553
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 1872554
    invoke-virtual {p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_1

    :cond_a
    move v1, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    move v8, v2

    move v9, v2

    goto/16 :goto_2
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1872555
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1872556
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_5

    .line 1872557
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    .line 1872558
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1872559
    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1872560
    if-eqz v2, :cond_0

    .line 1872561
    const-string p3, "is_compressed"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1872562
    invoke-virtual {p2, v2}, LX/0nX;->a(Z)V

    .line 1872563
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {p0, v1, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1872564
    if-eqz v2, :cond_1

    .line 1872565
    const-string p3, "is_font_family"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1872566
    invoke-virtual {p2, v2}, LX/0nX;->a(Z)V

    .line 1872567
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {p0, v1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1872568
    if-eqz v2, :cond_2

    .line 1872569
    const-string p3, "resource_name"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1872570
    invoke-virtual {p2, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1872571
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {p0, v1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1872572
    if-eqz v2, :cond_3

    .line 1872573
    const-string p3, "resource_version"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1872574
    invoke-virtual {p2, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1872575
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {p0, v1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1872576
    if-eqz v2, :cond_4

    .line 1872577
    const-string p3, "url"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1872578
    invoke-virtual {p2, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1872579
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1872580
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1872581
    :cond_5
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1872582
    return-void
.end method
