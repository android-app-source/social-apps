.class public LX/Bqe;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Bqd;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Bqg;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1824916
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/Bqe;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/Bqg;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1824917
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1824918
    iput-object p1, p0, LX/Bqe;->b:LX/0Ot;

    .line 1824919
    return-void
.end method

.method public static a(LX/0QB;)LX/Bqe;
    .locals 4

    .prologue
    .line 1824905
    const-class v1, LX/Bqe;

    monitor-enter v1

    .line 1824906
    :try_start_0
    sget-object v0, LX/Bqe;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1824907
    sput-object v2, LX/Bqe;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1824908
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1824909
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1824910
    new-instance v3, LX/Bqe;

    const/16 p0, 0x1d01

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/Bqe;-><init>(LX/0Ot;)V

    .line 1824911
    move-object v0, v3

    .line 1824912
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1824913
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Bqe;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1824914
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1824915
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 11

    .prologue
    .line 1824889
    check-cast p2, LX/Bqc;

    .line 1824890
    iget-object v0, p0, LX/Bqe;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Bqg;

    iget-object v1, p2, LX/Bqc;->a:Landroid/text/style/ClickableSpan;

    iget-object v2, p2, LX/Bqc;->b:Ljava/lang/Boolean;

    const/16 v10, 0x14

    const/4 v9, 0x5

    const/4 v4, 0x2

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 1824891
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v4}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v7, v9}, LX/1Dh;->r(II)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v4}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v4

    iget-object v5, v0, LX/Bqg;->a:LX/1vg;

    invoke-virtual {v5, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v5

    const v6, 0x7f02077d

    invoke-virtual {v5, v6}, LX/2xv;->h(I)LX/2xv;

    move-result-object v5

    const v6, -0x86dc5

    invoke-virtual {v5, v6}, LX/2xv;->i(I)LX/2xv;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/1o5;->a(LX/1n6;)LX/1o5;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    invoke-interface {v4, v10}, LX/1Di;->j(I)LX/1Di;

    move-result-object v4

    invoke-interface {v4, v10}, LX/1Di;->r(I)LX/1Di;

    move-result-object v4

    const/4 v5, 0x7

    invoke-interface {v4, v5, v7}, LX/1Di;->d(II)LX/1Di;

    move-result-object v4

    invoke-interface {v4, v8, v7}, LX/1Di;->d(II)LX/1Di;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v4

    .line 1824892
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-nez v5, :cond_0

    .line 1824893
    iget-object v5, v0, LX/Bqg;->b:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f08014c

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 1824894
    :goto_0
    move-object v5, v5

    .line 1824895
    invoke-virtual {v4, v5}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v4

    const/high16 v5, 0x41600000    # 14.0f

    invoke-virtual {v4, v5}, LX/1ne;->g(F)LX/1ne;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    .line 1824896
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v8}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v7, v9}, LX/1Dh;->r(II)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, LX/25N;->c(LX/1De;)LX/25Q;

    move-result-object v5

    const v6, 0x7f0a011d

    invoke-virtual {v5, v6}, LX/25Q;->i(I)LX/25Q;

    move-result-object v5

    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    invoke-interface {v5, v7}, LX/1Di;->r(I)LX/1Di;

    move-result-object v5

    const/4 v6, 0x4

    invoke-interface {v5, v6}, LX/1Di;->b(I)LX/1Di;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v3}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 1824897
    return-object v0

    .line 1824898
    :cond_0
    iget-object v5, v0, LX/Bqg;->b:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f08014b

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 1824899
    iget-object v5, v0, LX/Bqg;->b:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v10, 0x7f08014a

    const/4 p0, 0x1

    new-array p0, p0, [Ljava/lang/Object;

    const/4 p2, 0x0

    aput-object v6, p0, p2

    invoke-virtual {v5, v10, p0}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    .line 1824900
    new-instance v5, Landroid/text/SpannableStringBuilder;

    invoke-direct {v5}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 1824901
    invoke-virtual {v10, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result p0

    .line 1824902
    invoke-static {v6}, LX/1z7;->a(Ljava/lang/String;)I

    move-result v6

    .line 1824903
    invoke-virtual {v5, v10}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1824904
    new-instance v10, LX/Bqf;

    invoke-direct {v10, v0, v1}, LX/Bqf;-><init>(LX/Bqg;Landroid/text/style/ClickableSpan;)V

    add-int/2addr v6, p0

    const/16 p2, 0x21

    invoke-virtual {v5, v10, p0, v6, p2}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1824887
    invoke-static {}, LX/1dS;->b()V

    .line 1824888
    const/4 v0, 0x0

    return-object v0
.end method
