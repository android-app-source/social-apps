.class public LX/B6j;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Zb;

.field private final c:LX/1Ay;

.field private final d:LX/0bH;

.field private final e:LX/2mt;

.field private final f:LX/1xP;

.field private final g:LX/17Q;

.field public final h:LX/17V;

.field private final i:LX/2sb;

.field public j:Ljava/lang/String;

.field public k:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/2mt;LX/1xP;LX/0Zb;LX/1Ay;LX/0bH;LX/17V;LX/17Q;LX/2sb;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 0
    .param p9    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2mt;",
            "LX/1xP;",
            "LX/0Zb;",
            "LX/1Ay;",
            "LX/0bH;",
            "LX/17V;",
            "LX/17Q;",
            "LX/2sb;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1747251
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1747252
    iput-object p1, p0, LX/B6j;->e:LX/2mt;

    .line 1747253
    iput-object p2, p0, LX/B6j;->f:LX/1xP;

    .line 1747254
    iput-object p3, p0, LX/B6j;->b:LX/0Zb;

    .line 1747255
    iput-object p4, p0, LX/B6j;->c:LX/1Ay;

    .line 1747256
    iput-object p5, p0, LX/B6j;->d:LX/0bH;

    .line 1747257
    iput-object p7, p0, LX/B6j;->g:LX/17Q;

    .line 1747258
    iput-object p9, p0, LX/B6j;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1747259
    iput-object p6, p0, LX/B6j;->h:LX/17V;

    .line 1747260
    iput-object p8, p0, LX/B6j;->i:LX/2sb;

    .line 1747261
    return-void
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)",
            "LX/162;"
        }
    .end annotation

    .prologue
    .line 1747234
    invoke-static {p0}, LX/1WF;->g(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 1747235
    if-eqz v0, :cond_0

    invoke-static {v0}, LX/181;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(LX/162;Ljava/lang/String;Z)Landroid/os/Bundle;
    .locals 7

    .prologue
    const/4 v5, 0x0

    const/4 v6, 0x1

    .line 1747236
    iget-object v0, p0, LX/B6j;->f:LX/1xP;

    iget-object v2, p0, LX/B6j;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    const-string v4, "lead_gen_ads"

    move-object v1, p1

    move-object v3, p2

    invoke-virtual/range {v0 .. v5}, LX/1xP;->a(LX/162;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    .line 1747237
    iget-object v0, p0, LX/B6j;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1747238
    iget-object v2, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v2

    .line 1747239
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    const v2, 0x46a1c4a4

    invoke-static {v0, v2}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    .line 1747240
    if-nez v0, :cond_0

    .line 1747241
    :goto_0
    return-object v5

    .line 1747242
    :cond_0
    if-eqz p3, :cond_1

    invoke-static {v0}, LX/2sb;->b(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1747243
    const-string v2, "lead_gen_continued_flow_text"

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aR()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1747244
    const-string v2, "lead_gen_continued_flow_title"

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aS()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1747245
    const-string v2, "lead_gen_require_continued_flow"

    invoke-virtual {v1, v2, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1747246
    const-string v2, "lead_gen_continued_flow_user_info_value"

    iget-object v3, p0, LX/B6j;->j:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1747247
    sget-object v2, LX/B8u;->AUTOFILL:LX/B8u;

    invoke-static {v0}, LX/2sb;->a(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/B8u;->fromValue(Ljava/lang/String;)LX/B8u;

    move-result-object v0

    if-ne v2, v0, :cond_1

    .line 1747248
    const-string v0, "lead_gen_seamless_continued_flow"

    invoke-virtual {v1, v0, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1747249
    const-string v0, "extra_js_to_execute"

    iget-object v2, p0, LX/B6j;->k:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    move-object v5, v1

    .line 1747250
    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/view/View;Ljava/lang/String;Z)V
    .locals 9

    .prologue
    .line 1747222
    iget-object v0, p0, LX/B6j;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1747223
    const v2, 0x7f0d0083

    invoke-virtual {p1, v2}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 1747224
    invoke-static {p2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1747225
    const/4 v2, 0x0

    .line 1747226
    :goto_0
    move-object v0, v2

    .line 1747227
    invoke-static {v0}, LX/1vZ;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1747228
    invoke-static {v0, p1}, LX/1vZ;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Landroid/view/View;)V

    .line 1747229
    :cond_0
    iget-object v1, p0, LX/B6j;->b:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1747230
    invoke-virtual {p0, p1, p2, p3}, LX/B6j;->b(Landroid/view/View;Ljava/lang/String;Z)V

    .line 1747231
    return-void

    :cond_1
    invoke-static {v0}, LX/2mt;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v4

    invoke-static {v0}, LX/B6j;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v5

    const-string v6, "native_newsfeed"

    .line 1747232
    iget-object v2, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v2

    .line 1747233
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v3}, LX/1VO;->z(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;

    move-result-object v8

    move-object v3, p2

    invoke-static/range {v3 .. v8}, LX/17V;->a(Ljava/lang/String;ZLX/0lF;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    goto :goto_0
.end method

.method public final b(Landroid/view/View;Ljava/lang/String;Z)V
    .locals 7

    .prologue
    .line 1747197
    iget-object v0, p0, LX/B6j;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1747198
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 1747199
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v0}, LX/2mt;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "open_graph"

    .line 1747200
    :goto_0
    iget-object v1, p0, LX/B6j;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v1}, LX/B6j;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v1

    .line 1747201
    iget-object v2, p0, LX/B6j;->f:LX/1xP;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {p0, v1, p2, p3}, LX/B6j;->a(LX/162;Ljava/lang/String;Z)Landroid/os/Bundle;

    move-result-object v4

    iget-object v5, p0, LX/B6j;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1747202
    invoke-static {v5}, LX/2mt;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 1747203
    const/4 v6, 0x0

    .line 1747204
    :goto_1
    move-object v0, v6

    .line 1747205
    invoke-virtual {v2, v3, p2, v4, v0}, LX/1xP;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/util/Map;)V

    .line 1747206
    iget-object v0, p0, LX/B6j;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 1747207
    if-eqz v0, :cond_0

    .line 1747208
    invoke-static {v0}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v0

    .line 1747209
    if-eqz v0, :cond_0

    .line 1747210
    iget-object v2, p0, LX/B6j;->c:LX/1Ay;

    invoke-virtual {v2, v0, p2}, LX/1Ay;->a(LX/0lF;Ljava/lang/String;)V

    .line 1747211
    :cond_0
    if-eqz v1, :cond_1

    invoke-virtual {v1}, LX/0lF;->e()I

    move-result v0

    if-nez v0, :cond_3

    .line 1747212
    :cond_1
    :goto_2
    return-void

    .line 1747213
    :cond_2
    const-string v0, "other"

    goto :goto_0

    .line 1747214
    :cond_3
    const/4 v0, 0x0

    .line 1747215
    iget-object v1, p0, LX/B6j;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v1}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    .line 1747216
    if-nez v2, :cond_5

    .line 1747217
    :goto_3
    move-object v0, v0

    .line 1747218
    if-eqz v0, :cond_1

    .line 1747219
    iget-object v1, p0, LX/B6j;->d:LX/0bH;

    invoke-virtual {v1, v0}, LX/0b4;->a(LX/0b7;)V

    goto :goto_2

    :cond_4
    const-string v6, "native"

    invoke-static {v5}, LX/2mt;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result p1

    invoke-static {v5}, LX/B6j;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object p3

    invoke-static {v0, v6, p1, p3}, LX/17Q;->b(Ljava/lang/String;Ljava/lang/String;ZLX/0lF;)Ljava/util/Map;

    move-result-object v6

    goto :goto_1

    .line 1747220
    :cond_5
    iget-object v1, p0, LX/B6j;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v1}, LX/1WF;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    if-eq v1, v2, :cond_6

    iget-object v0, p0, LX/B6j;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, LX/1WF;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v0

    .line 1747221
    :cond_6
    new-instance v1, LX/1ZS;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, LX/1ZS;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_3
.end method
