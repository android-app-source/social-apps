.class public LX/AVT;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile j:LX/AVT;


# instance fields
.field public final a:LX/0Zb;

.field public final b:LX/0kb;

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Uo;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/lang/String;

.field public final e:LX/0oz;

.field public volatile f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public volatile g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public volatile h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public volatile i:LX/21D;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Zb;LX/0kb;LX/0Ot;Ljava/lang/String;LX/0oz;)V
    .locals 0
    .param p4    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Zb;",
            "LX/0kb;",
            "LX/0Ot",
            "<",
            "LX/0Uo;",
            ">;",
            "Ljava/lang/String;",
            "LX/0oz;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1679773
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1679774
    iput-object p1, p0, LX/AVT;->a:LX/0Zb;

    .line 1679775
    iput-object p2, p0, LX/AVT;->b:LX/0kb;

    .line 1679776
    iput-object p3, p0, LX/AVT;->c:LX/0Ot;

    .line 1679777
    iput-object p4, p0, LX/AVT;->d:Ljava/lang/String;

    .line 1679778
    iput-object p5, p0, LX/AVT;->e:LX/0oz;

    .line 1679779
    return-void
.end method

.method public static a(LX/0QB;)LX/AVT;
    .locals 9

    .prologue
    .line 1679791
    sget-object v0, LX/AVT;->j:LX/AVT;

    if-nez v0, :cond_1

    .line 1679792
    const-class v1, LX/AVT;

    monitor-enter v1

    .line 1679793
    :try_start_0
    sget-object v0, LX/AVT;->j:LX/AVT;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1679794
    if-eqz v2, :cond_0

    .line 1679795
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1679796
    new-instance v3, LX/AVT;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    invoke-static {v0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v5

    check-cast v5, LX/0kb;

    const/16 v6, 0x245

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {v0}, LX/0dG;->b(LX/0QB;)Ljava/lang/String;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-static {v0}, LX/0oz;->a(LX/0QB;)LX/0oz;

    move-result-object v8

    check-cast v8, LX/0oz;

    invoke-direct/range {v3 .. v8}, LX/AVT;-><init>(LX/0Zb;LX/0kb;LX/0Ot;Ljava/lang/String;LX/0oz;)V

    .line 1679797
    move-object v0, v3

    .line 1679798
    sput-object v0, LX/AVT;->j:LX/AVT;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1679799
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1679800
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1679801
    :cond_1
    sget-object v0, LX/AVT;->j:LX/AVT;

    return-object v0

    .line 1679802
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1679803
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/video/videostreaming/LiveStreamingError;Ljava/util/Map;)V
    .locals 2
    .param p0    # Lcom/facebook/video/videostreaming/LiveStreamingError;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/video/videostreaming/LiveStreamingError;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1679804
    if-eqz p0, :cond_0

    .line 1679805
    const-string v0, "error_class"

    iget-object v1, p0, Lcom/facebook/video/videostreaming/LiveStreamingError;->domain:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1679806
    const-string v0, "error_code"

    iget v1, p0, Lcom/facebook/video/videostreaming/LiveStreamingError;->errorCode:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1679807
    const-string v0, "error_message"

    iget-object v1, p0, Lcom/facebook/video/videostreaming/LiveStreamingError;->reason:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1679808
    const-string v0, "error_trace"

    iget-object v1, p0, Lcom/facebook/video/videostreaming/LiveStreamingError;->fullDescription:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1679809
    const-string v0, "inner_error_class"

    iget-object v1, p0, Lcom/facebook/video/videostreaming/LiveStreamingError;->domain:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1679810
    const-string v0, "inner_error_code"

    iget v1, p0, Lcom/facebook/video/videostreaming/LiveStreamingError;->errorCode:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1679811
    const-string v0, "inner_error_message"

    iget-object v1, p0, Lcom/facebook/video/videostreaming/LiveStreamingError;->reason:Ljava/lang/String;

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1679812
    :cond_0
    return-void
.end method

.method public static b(LX/AVT;Ljava/lang/String;Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1679813
    iget-object v0, p0, LX/AVT;->a:LX/0Zb;

    invoke-static {p0, p1, p2}, LX/AVT;->c(LX/AVT;Ljava/lang/String;Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1679814
    return-void
.end method

.method public static c(LX/AVT;Ljava/lang/String;Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/facebook/analytics/logger/HoneyClientEvent;"
        }
    .end annotation

    .prologue
    .line 1679815
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v2, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1679816
    const-string v0, "Facecast"

    .line 1679817
    iput-object v0, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1679818
    if-nez p2, :cond_0

    .line 1679819
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object p2

    .line 1679820
    :cond_0
    const-string v1, "broadcast_id"

    iget-object v0, p0, LX/AVT;->f:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "none"

    :goto_0
    invoke-interface {p2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1679821
    const-string v0, "broadcast_target_type"

    iget-object v1, p0, LX/AVT;->h:Ljava/lang/String;

    invoke-interface {p2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1679822
    const-string v0, "base_system_version"

    sget-object v1, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-interface {p2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1679823
    iget-object v4, p0, LX/AVT;->b:LX/0kb;

    invoke-virtual {v4}, LX/0kb;->p()Ljava/lang/String;

    move-result-object v4

    .line 1679824
    const-string v5, "connection_type"

    invoke-interface {p2, v5, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1679825
    const-string v4, "connection_quality"

    iget-object v5, p0, LX/AVT;->e:LX/0oz;

    invoke-virtual {v5}, LX/0oz;->b()LX/0p3;

    move-result-object v5

    invoke-virtual {v5}, LX/0p3;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {p2, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1679826
    const-string v4, "network_rtt"

    iget-object v5, p0, LX/AVT;->e:LX/0oz;

    invoke-virtual {v5}, LX/0oz;->k()D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v5

    invoke-interface {p2, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1679827
    const-string v4, "connection_bandwidth"

    iget-object v5, p0, LX/AVT;->e:LX/0oz;

    invoke-virtual {v5}, LX/0oz;->f()D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v5

    invoke-interface {p2, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1679828
    const-string v1, "app_state"

    iget-object v0, p0, LX/AVT;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uo;

    invoke-virtual {v0}, LX/0Uo;->j()Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "BACKGROUND"

    :goto_1
    invoke-interface {p2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1679829
    iget-object v0, p0, LX/AVT;->i:LX/21D;

    .line 1679830
    const-string v1, "composer_source_surface"

    if-eqz v0, :cond_5

    invoke-virtual {v0}, LX/21D;->getAnalyticsName()Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-interface {p2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1679831
    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1679832
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_3

    .line 1679833
    :cond_1
    iget-object v0, p0, LX/AVT;->f:Ljava/lang/String;

    goto/16 :goto_0

    .line 1679834
    :cond_2
    iget-object v0, p0, LX/AVT;->g:Ljava/lang/String;

    .line 1679835
    iput-object v0, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->f:Ljava/lang/String;

    .line 1679836
    iget-object v0, p0, LX/AVT;->d:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1679837
    iget-object v0, p0, LX/AVT;->d:Ljava/lang/String;

    invoke-virtual {v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->i(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1679838
    :cond_3
    return-object v2

    .line 1679839
    :cond_4
    const-string v0, "FOREGROUND"

    goto :goto_1

    .line 1679840
    :cond_5
    const-string v0, "n/a"

    goto :goto_2
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1679841
    iput-object p1, p0, LX/AVT;->f:Ljava/lang/String;

    .line 1679842
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 1679780
    move-object v0, p0

    move-object v1, p1

    move-object v2, p1

    move-object v3, p2

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, LX/AVT;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/video/videostreaming/LiveStreamingError;Ljava/util/Map;)V

    .line 1679781
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/video/videostreaming/LiveStreamingError;Ljava/util/Map;)V
    .locals 2
    .param p4    # Lcom/facebook/video/videostreaming/LiveStreamingError;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/facebook/video/videostreaming/LiveStreamingError;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1679782
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1679783
    invoke-static {p4, v0}, LX/AVT;->a(Lcom/facebook/video/videostreaming/LiveStreamingError;Ljava/util/Map;)V

    .line 1679784
    const-string v1, "broadcast_transition_from"

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1679785
    const-string v1, "broadcast_transition_to"

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1679786
    const-string v1, "broadcast_transition_reason"

    invoke-interface {v0, v1, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1679787
    if-eqz p5, :cond_0

    .line 1679788
    invoke-interface {v0, p5}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 1679789
    :cond_0
    invoke-virtual {p0, v0}, LX/AVT;->a(Ljava/util/Map;)V

    .line 1679790
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/util/Map;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1679739
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1679740
    const-string v1, "facecast_event_name"

    const-string v2, "audio_live_broadcast_fallback"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1679741
    if-eqz p1, :cond_0

    .line 1679742
    const-string v1, "audio_live_broadcast_id"

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1679743
    :cond_0
    if-eqz p2, :cond_1

    .line 1679744
    invoke-interface {v0, p2}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 1679745
    :cond_1
    invoke-virtual {p0, v0}, LX/AVT;->a(Ljava/util/Map;)V

    .line 1679746
    return-void
.end method

.method public final a(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1679747
    const-string v0, "facecast_broadcaster_update"

    invoke-static {p0, v0, p1}, LX/AVT;->b(LX/AVT;Ljava/lang/String;Ljava/util/Map;)V

    .line 1679748
    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1679749
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1679750
    const-string v1, "copyright_monitor_state"

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1679751
    invoke-virtual {p0, v0}, LX/AVT;->a(Ljava/util/Map;)V

    .line 1679752
    return-void
.end method

.method public final c(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1679753
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1679754
    const-string v1, "facecast_event_name"

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1679755
    const-string v1, "facecast_event_extra"

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1679756
    invoke-virtual {p0, v0}, LX/AVT;->a(Ljava/util/Map;)V

    .line 1679757
    return-void
.end method

.method public final d(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1679758
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1679759
    const-string v1, "broadcast_transition_reason"

    const-string v2, "recording_started"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1679760
    const-string v1, "orientation"

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1679761
    invoke-virtual {p0, v0}, LX/AVT;->a(Ljava/util/Map;)V

    .line 1679762
    return-void
.end method

.method public final f(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1679763
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1679764
    const-string v1, "facecast_event_name"

    const-string v2, "audio_live_broadcast"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1679765
    const-string v1, "audio_live_broadcast_id"

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1679766
    invoke-virtual {p0, v0}, LX/AVT;->a(Ljava/util/Map;)V

    .line 1679767
    return-void
.end method

.method public final g(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1679768
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1679769
    const-string v1, "facecast_event_name"

    const-string v2, "facecast_audio_load_cover_photo_fail"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1679770
    const-string v1, "facecast_event_extra"

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1679771
    invoke-virtual {p0, v0}, LX/AVT;->a(Ljava/util/Map;)V

    .line 1679772
    return-void
.end method
