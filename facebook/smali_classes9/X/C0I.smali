.class public final LX/C0I;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/C0J;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public b:I

.field public c:I


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1840251
    const-string v0, "SidePhotoShareComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1840252
    if-ne p0, p1, :cond_1

    .line 1840253
    :cond_0
    :goto_0
    return v0

    .line 1840254
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1840255
    goto :goto_0

    .line 1840256
    :cond_3
    check-cast p1, LX/C0I;

    .line 1840257
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1840258
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1840259
    if-eq v2, v3, :cond_0

    .line 1840260
    iget-object v2, p0, LX/C0I;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/C0I;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/C0I;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1840261
    goto :goto_0

    .line 1840262
    :cond_5
    iget-object v2, p1, LX/C0I;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 1840263
    :cond_6
    iget v2, p0, LX/C0I;->b:I

    iget v3, p1, LX/C0I;->b:I

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 1840264
    goto :goto_0

    .line 1840265
    :cond_7
    iget v2, p0, LX/C0I;->c:I

    iget v3, p1, LX/C0I;->c:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 1840266
    goto :goto_0
.end method
