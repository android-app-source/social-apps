.class public LX/Aca;
.super LX/2oX;
.source ""


# instance fields
.field public final a:Landroid/view/View;

.field public final b:Landroid/view/View;

.field public final c:Lcom/facebook/facecastdisplay/FacecastInteractionView;

.field public final d:Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputView;

.field public final e:Lcom/facebook/fbui/widget/text/GlyphWithTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1692865
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/Aca;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1692866
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1692867
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/Aca;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1692868
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1692869
    invoke-direct {p0, p1, p2, p3}, LX/2oX;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1692870
    const v0, 0x7f030a11

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1692871
    const v0, 0x7f0d0fc9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/Aca;->a:Landroid/view/View;

    .line 1692872
    const v0, 0x7f0d1973

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/Aca;->b:Landroid/view/View;

    .line 1692873
    const v0, 0x7f0d0f83

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/FacecastInteractionView;

    iput-object v0, p0, LX/Aca;->c:Lcom/facebook/facecastdisplay/FacecastInteractionView;

    .line 1692874
    const v0, 0x7f0d1974

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputView;

    iput-object v0, p0, LX/Aca;->d:Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputView;

    .line 1692875
    const v0, 0x7f0d1975

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    iput-object v0, p0, LX/Aca;->e:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    .line 1692876
    return-void
.end method
