.class public final LX/BhP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field public final synthetic a:I

.field public final synthetic b:Lcom/facebook/datasensitivity/DataSaverBar;


# direct methods
.method public constructor <init>(Lcom/facebook/datasensitivity/DataSaverBar;I)V
    .locals 0

    .prologue
    .line 1809135
    iput-object p1, p0, LX/BhP;->b:Lcom/facebook/datasensitivity/DataSaverBar;

    iput p2, p0, LX/BhP;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onGlobalLayout()V
    .locals 2

    .prologue
    .line 1809136
    iget-object v0, p0, LX/BhP;->b:Lcom/facebook/datasensitivity/DataSaverBar;

    invoke-virtual {v0}, Lcom/facebook/datasensitivity/DataSaverBar;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 1809137
    iget-object v0, p0, LX/BhP;->b:Lcom/facebook/datasensitivity/DataSaverBar;

    iget-object v0, v0, Lcom/facebook/datasensitivity/DataSaverBar;->f:LX/3GW;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/BhP;->b:Lcom/facebook/datasensitivity/DataSaverBar;

    invoke-virtual {v0}, Lcom/facebook/datasensitivity/DataSaverBar;->getHeight()I

    move-result v0

    iget v1, p0, LX/BhP;->a:I

    if-eq v0, v1, :cond_0

    .line 1809138
    iget-object v0, p0, LX/BhP;->b:Lcom/facebook/datasensitivity/DataSaverBar;

    iget-object v0, v0, Lcom/facebook/datasensitivity/DataSaverBar;->f:LX/3GW;

    invoke-interface {v0}, LX/3GW;->b()V

    .line 1809139
    :cond_0
    return-void
.end method
