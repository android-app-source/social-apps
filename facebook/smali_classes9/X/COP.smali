.class public final LX/COP;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/COP;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/CON;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/COQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1883061
    const/4 v0, 0x0

    sput-object v0, LX/COP;->a:LX/COP;

    .line 1883062
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/COP;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1883063
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1883064
    new-instance v0, LX/COQ;

    invoke-direct {v0}, LX/COQ;-><init>()V

    iput-object v0, p0, LX/COP;->c:LX/COQ;

    .line 1883065
    return-void
.end method

.method public static declared-synchronized q()LX/COP;
    .locals 2

    .prologue
    .line 1883057
    const-class v1, LX/COP;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/COP;->a:LX/COP;

    if-nez v0, :cond_0

    .line 1883058
    new-instance v0, LX/COP;

    invoke-direct {v0}, LX/COP;-><init>()V

    sput-object v0, LX/COP;->a:LX/COP;

    .line 1883059
    :cond_0
    sget-object v0, LX/COP;->a:LX/COP;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 1883060
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 5

    .prologue
    .line 1883011
    check-cast p2, LX/COO;

    .line 1883012
    iget-object v0, p2, LX/COO;->a:LX/CNb;

    iget-object v1, p2, LX/COO;->b:Ljava/util/List;

    const p2, 0x7fffffff

    const/high16 p0, -0x1000000

    .line 1883013
    const/4 v2, 0x0

    .line 1883014
    new-instance v3, LX/5JY;

    invoke-direct {v3}, LX/5JY;-><init>()V

    .line 1883015
    sget-object v4, LX/5JZ;->b:LX/0Zi;

    invoke-virtual {v4}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/5JX;

    .line 1883016
    if-nez v4, :cond_0

    .line 1883017
    new-instance v4, LX/5JX;

    invoke-direct {v4}, LX/5JX;-><init>()V

    .line 1883018
    :cond_0
    invoke-static {v4, p1, v2, v2, v3}, LX/5JX;->a$redex0(LX/5JX;LX/1De;IILX/5JY;)V

    .line 1883019
    move-object v3, v4

    .line 1883020
    move-object v2, v3

    .line 1883021
    move-object v2, v2

    .line 1883022
    const-string v3, "placeholder"

    const-string v4, ""

    invoke-virtual {v0, v3, v4}, LX/CNb;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1883023
    iget-object v4, v2, LX/5JX;->a:LX/5JY;

    iput-object v3, v4, LX/5JY;->b:Ljava/lang/CharSequence;

    .line 1883024
    move-object v2, v2

    .line 1883025
    const-string v3, "font-size"

    const/16 v4, 0xd

    invoke-virtual {v0, v3, v4}, LX/CNb;->a(Ljava/lang/String;I)I

    move-result v3

    .line 1883026
    iget-object v4, v2, LX/5JX;->a:LX/5JY;

    iput v3, v4, LX/5JY;->r:I

    .line 1883027
    move-object v2, v2

    .line 1883028
    const-string v3, "horizontal-alignment"

    const-string v4, ""

    invoke-virtual {v0, v3, v4}, LX/CNb;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/CPv;->b(Ljava/lang/String;)Landroid/text/Layout$Alignment;

    move-result-object v3

    .line 1883029
    iget-object v4, v2, LX/5JX;->a:LX/5JY;

    iput-object v3, v4, LX/5JY;->w:Landroid/text/Layout$Alignment;

    .line 1883030
    move-object v2, v2

    .line 1883031
    const-string v3, "font-weight"

    invoke-static {v0, v3}, LX/CPv;->a(LX/CNb;Ljava/lang/String;)I

    move-result v3

    .line 1883032
    iget-object v4, v2, LX/5JX;->a:LX/5JY;

    iput v3, v4, LX/5JY;->u:I

    .line 1883033
    move-object v2, v2

    .line 1883034
    const-string v3, "max-lines"

    invoke-virtual {v0, v3, p2}, LX/CNb;->a(Ljava/lang/String;I)I

    move-result v3

    .line 1883035
    iget-object v4, v2, LX/5JX;->a:LX/5JY;

    iput v3, v4, LX/5JY;->e:I

    .line 1883036
    move-object v2, v2

    .line 1883037
    const-string v3, "max-length"

    invoke-virtual {v0, v3, p2}, LX/CNb;->a(Ljava/lang/String;I)I

    move-result v3

    .line 1883038
    iget-object v4, v2, LX/5JX;->a:LX/5JY;

    iput v3, v4, LX/5JY;->f:I

    .line 1883039
    move-object v2, v2

    .line 1883040
    const-string v3, "editable"

    const/4 v4, 0x1

    invoke-virtual {v0, v3, v4}, LX/CNb;->a(Ljava/lang/String;Z)Z

    move-result v3

    .line 1883041
    iget-object v4, v2, LX/5JX;->a:LX/5JY;

    iput-boolean v3, v4, LX/5JY;->y:Z

    .line 1883042
    move-object v2, v2

    .line 1883043
    const/4 v3, 0x0

    .line 1883044
    iget-object v4, v2, LX/5JX;->a:LX/5JY;

    iput-object v3, v4, LX/5JY;->z:LX/1dQ;

    .line 1883045
    move-object v2, v2

    .line 1883046
    const-string v3, "placeholder-color"

    invoke-virtual {v0, v3}, LX/CNb;->c(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1883047
    const-string v3, "placeholder-color"

    invoke-virtual {v0, v3, p0}, LX/CNb;->b(Ljava/lang/String;I)I

    move-result v3

    .line 1883048
    iget-object v4, v2, LX/5JX;->a:LX/5JY;

    iput v3, v4, LX/5JY;->n:I

    .line 1883049
    :cond_1
    const-string v3, "color"

    invoke-virtual {v0, v3}, LX/CNb;->c(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1883050
    const-string v3, "color"

    invoke-virtual {v0, v3, p0}, LX/CNb;->b(Ljava/lang/String;I)I

    move-result v3

    .line 1883051
    iget-object v4, v2, LX/5JX;->a:LX/5JY;

    iput v3, v4, LX/5JY;->l:I

    .line 1883052
    :cond_2
    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    .line 1883053
    invoke-static {v2, p1, v0, v1}, LX/CPx;->a(LX/1Di;LX/1De;LX/CNb;Ljava/util/List;)LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 1883054
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1883055
    invoke-static {}, LX/1dS;->b()V

    .line 1883056
    const/4 v0, 0x0

    return-object v0
.end method
