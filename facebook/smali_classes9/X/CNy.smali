.class public final LX/CNy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/graphql/model/GraphQLNativeTemplatesRoot;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/CNq;

.field public final synthetic b:I

.field public final synthetic c:Ljava/util/List;

.field public final synthetic d:LX/CNz;


# direct methods
.method public constructor <init>(LX/CNz;LX/CNq;ILjava/util/List;)V
    .locals 0

    .prologue
    .line 1882345
    iput-object p1, p0, LX/CNy;->d:LX/CNz;

    iput-object p2, p0, LX/CNy;->a:LX/CNq;

    iput p3, p0, LX/CNy;->b:I

    iput-object p4, p0, LX/CNy;->c:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 1882346
    iget-object v0, p0, LX/CNy;->d:LX/CNz;

    iget-object v1, p0, LX/CNy;->a:LX/CNq;

    iget-object v2, p0, LX/CNy;->c:Ljava/util/List;

    iget v3, p0, LX/CNy;->b:I

    .line 1882347
    iget p0, v0, LX/CNz;->e:I

    add-int/lit8 p0, p0, -0x1

    if-ne v3, p0, :cond_0

    .line 1882348
    const/4 p0, 0x0

    iput-object p0, v0, LX/CNz;->b:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1882349
    :cond_0
    iget-object p0, v0, LX/CNz;->d:Ljava/util/ArrayList;

    invoke-virtual {p0, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 1882350
    iget p0, v0, LX/CNz;->e:I

    add-int/lit8 p0, p0, -0x1

    if-ge v3, p0, :cond_2

    const/4 p0, 0x1

    .line 1882351
    :goto_0
    iget-boolean p1, v0, LX/CNz;->a:Z

    if-nez p1, :cond_1

    iget-boolean p1, v0, LX/CNz;->g:Z

    if-nez p1, :cond_3

    if-eqz p0, :cond_3

    .line 1882352
    :cond_1
    :goto_1
    return-void

    .line 1882353
    :cond_2
    const/4 p0, 0x0

    goto :goto_0

    .line 1882354
    :cond_3
    if-eqz v2, :cond_1

    .line 1882355
    iget-object p0, v1, LX/CNc;->b:LX/CNS;

    invoke-virtual {p0}, LX/CNS;->a()V

    .line 1882356
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p0

    if-eqz p0, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/CNe;

    .line 1882357
    invoke-interface {p0}, LX/CNe;->a()V

    goto :goto_2

    .line 1882358
    :cond_4
    iget-object p0, v1, LX/CNc;->b:LX/CNS;

    invoke-virtual {p0}, LX/CNS;->b()V

    goto :goto_1
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 11

    .prologue
    .line 1882359
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1882360
    iget-object v1, p0, LX/CNy;->d:LX/CNz;

    .line 1882361
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1882362
    check-cast v0, Lcom/facebook/graphql/model/GraphQLNativeTemplatesRoot;

    iget-object v2, p0, LX/CNy;->a:LX/CNq;

    iget v3, p0, LX/CNy;->b:I

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 1882363
    iget v4, v1, LX/CNz;->e:I

    add-int/lit8 v4, v4, -0x1

    if-ne v3, v4, :cond_0

    .line 1882364
    const/4 v4, 0x0

    iput-object v4, v1, LX/CNz;->b:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1882365
    :cond_0
    iget v4, v1, LX/CNz;->e:I

    add-int/lit8 v4, v4, -0x1

    if-ge v3, v4, :cond_2

    move v4, v5

    .line 1882366
    :goto_0
    iget-boolean v7, v1, LX/CNz;->a:Z

    if-nez v7, :cond_1

    iget-boolean v7, v1, LX/CNz;->g:Z

    if-nez v7, :cond_3

    if-eqz v4, :cond_3

    .line 1882367
    :cond_1
    iget-object v4, v1, LX/CNz;->d:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 1882368
    :goto_1
    return-void

    :cond_2
    move v4, v6

    .line 1882369
    goto :goto_0

    .line 1882370
    :cond_3
    iget-boolean v4, v1, LX/CNz;->f:Z

    if-eqz v4, :cond_a

    iget-object v4, v1, LX/CNz;->d:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_a

    .line 1882371
    iget-object v4, v1, LX/CNz;->d:Ljava/util/ArrayList;

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 1882372
    if-eq v4, v3, :cond_a

    move v4, v5

    .line 1882373
    :goto_2
    iget-object v7, v1, LX/CNz;->d:Ljava/util/ArrayList;

    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 1882374
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNativeTemplatesRoot;->a()LX/0Px;

    move-result-object v7

    .line 1882375
    if-eqz v4, :cond_4

    if-eqz v7, :cond_4

    .line 1882376
    iget-object v4, v1, LX/CNz;->c:Ljava/util/HashMap;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 1882377
    :cond_4
    iget-object v4, v2, LX/CNc;->b:LX/CNS;

    invoke-virtual {v4}, LX/CNS;->a()V

    .line 1882378
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_3
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLNativeTemplateBundle;

    .line 1882379
    invoke-static {v4, v2}, LX/CNu;->a(Lcom/facebook/graphql/model/GraphQLNativeTemplateBundle;LX/CNc;)LX/CNb;

    move-result-object v4

    .line 1882380
    invoke-static {v4, v2}, LX/CNd;->a(LX/CNb;LX/CNc;)LX/CNe;

    move-result-object v8

    .line 1882381
    if-eqz v8, :cond_5

    .line 1882382
    invoke-interface {v8}, LX/CNe;->a()V

    goto :goto_3

    .line 1882383
    :cond_5
    const-class v8, LX/CNz;

    const-string v9, "Invalid action: %s"

    new-array v10, v5, [Ljava/lang/Object;

    invoke-virtual {v4}, LX/CNb;->a()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v10, v6

    invoke-static {v8, v9, v10}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_3

    .line 1882384
    :cond_6
    iget-object v4, v1, LX/CNz;->d:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_9

    .line 1882385
    iget-object v4, v1, LX/CNz;->d:Ljava/util/ArrayList;

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v8

    .line 1882386
    add-int/lit8 v4, v3, 0x1

    move v7, v4

    .line 1882387
    :goto_4
    if-ge v7, v8, :cond_9

    .line 1882388
    iget-object v4, v1, LX/CNz;->c:Ljava/util/HashMap;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v4, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLNativeTemplatesRoot;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLNativeTemplatesRoot;->a()LX/0Px;

    move-result-object v4

    .line 1882389
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_5
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_8

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLNativeTemplateBundle;

    .line 1882390
    invoke-static {v4, v2}, LX/CNu;->a(Lcom/facebook/graphql/model/GraphQLNativeTemplateBundle;LX/CNc;)LX/CNb;

    move-result-object v4

    .line 1882391
    invoke-static {v4, v2}, LX/CNd;->a(LX/CNb;LX/CNc;)LX/CNe;

    move-result-object v10

    .line 1882392
    if-eqz v10, :cond_7

    .line 1882393
    invoke-interface {v10}, LX/CNe;->a()V

    goto :goto_5

    .line 1882394
    :cond_7
    const-class v10, LX/CNz;

    const-string p0, "Invalid action: %s"

    new-array p1, v5, [Ljava/lang/Object;

    invoke-virtual {v4}, LX/CNb;->a()Ljava/lang/String;

    move-result-object v4

    aput-object v4, p1, v6

    invoke-static {v10, p0, p1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_5

    .line 1882395
    :cond_8
    add-int/lit8 v4, v7, 0x1

    move v7, v4

    .line 1882396
    goto :goto_4

    .line 1882397
    :cond_9
    iget-object v4, v2, LX/CNc;->b:LX/CNS;

    invoke-virtual {v4}, LX/CNS;->b()V

    goto/16 :goto_1

    :cond_a
    move v4, v6

    goto/16 :goto_2
.end method
