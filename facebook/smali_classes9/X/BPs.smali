.class public LX/BPs;
.super LX/0b4;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0b4",
        "<",
        "LX/BPt;",
        "LX/BPr;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/BPs;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1780956
    invoke-direct {p0}, LX/0b4;-><init>()V

    .line 1780957
    return-void
.end method

.method public static a(LX/0QB;)LX/BPs;
    .locals 3

    .prologue
    .line 1780958
    sget-object v0, LX/BPs;->a:LX/BPs;

    if-nez v0, :cond_1

    .line 1780959
    const-class v1, LX/BPs;

    monitor-enter v1

    .line 1780960
    :try_start_0
    sget-object v0, LX/BPs;->a:LX/BPs;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1780961
    if-eqz v2, :cond_0

    .line 1780962
    :try_start_1
    new-instance v0, LX/BPs;

    invoke-direct {v0}, LX/BPs;-><init>()V

    .line 1780963
    move-object v0, v0

    .line 1780964
    sput-object v0, LX/BPs;->a:LX/BPs;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1780965
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1780966
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1780967
    :cond_1
    sget-object v0, LX/BPs;->a:LX/BPs;

    return-object v0

    .line 1780968
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1780969
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
