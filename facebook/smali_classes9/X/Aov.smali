.class public LX/Aov;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile x:LX/Aov;


# instance fields
.field public final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/B9y;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/1EQ;

.field public final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/14w;

.field public final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/composer/publish/ComposerPublishServiceHelper;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1CW;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/2sP;

.field public final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/4nT;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/BYi;

.field private final j:LX/9Ip;

.field private final k:LX/9A4;

.field private final l:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field public final m:LX/0SG;

.field private final n:LX/0ad;

.field public final o:Z

.field private final p:LX/BYg;

.field private final q:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/01T;",
            ">;"
        }
    .end annotation
.end field

.field public final r:LX/0pf;

.field public final s:LX/0wL;

.field private final t:LX/BEH;

.field public final u:Lcom/facebook/content/SecureContextHelper;

.field public final v:Lcom/facebook/intent/feed/IFeedIntentBuilder;

.field private final w:LX/0W3;


# direct methods
.method public constructor <init>(LX/14w;LX/0Or;LX/0Ot;LX/2sP;LX/0Ot;LX/1EQ;LX/0Or;LX/0Ot;LX/BYi;LX/BYg;LX/9Ip;LX/9A4;LX/0Or;LX/0SG;LX/0ad;LX/0Uh;LX/0Ot;LX/0pf;LX/0wL;LX/BEH;Lcom/facebook/content/SecureContextHelper;Lcom/facebook/intent/feed/IFeedIntentBuilder;LX/0W3;)V
    .locals 3
    .param p13    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/14w;",
            "LX/0Or",
            "<",
            "Lcom/facebook/composer/publish/ComposerPublishServiceHelper;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1CW;",
            ">;",
            "LX/2sP;",
            "LX/0Ot",
            "<",
            "LX/B9y;",
            ">;",
            "LX/1EQ;",
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/4nT;",
            ">;",
            "LX/BYi;",
            "LX/BYg;",
            "LX/9Ip;",
            "LX/9A4;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "LX/0SG;",
            "LX/0ad;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0Ot",
            "<",
            "LX/01T;",
            ">;",
            "LX/0pf;",
            "LX/0wL;",
            "LX/BEH;",
            "Lcom/facebook/content/SecureContextHelper;",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1715116
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1715117
    iput-object p1, p0, LX/Aov;->d:LX/14w;

    .line 1715118
    iput-object p2, p0, LX/Aov;->e:LX/0Or;

    .line 1715119
    iput-object p3, p0, LX/Aov;->f:LX/0Ot;

    .line 1715120
    iput-object p4, p0, LX/Aov;->g:LX/2sP;

    .line 1715121
    iput-object p5, p0, LX/Aov;->a:LX/0Ot;

    .line 1715122
    iput-object p6, p0, LX/Aov;->b:LX/1EQ;

    .line 1715123
    iput-object p7, p0, LX/Aov;->c:LX/0Or;

    .line 1715124
    iput-object p8, p0, LX/Aov;->h:LX/0Ot;

    .line 1715125
    iput-object p11, p0, LX/Aov;->j:LX/9Ip;

    .line 1715126
    iput-object p12, p0, LX/Aov;->k:LX/9A4;

    .line 1715127
    move-object/from16 v0, p13

    iput-object v0, p0, LX/Aov;->l:LX/0Or;

    .line 1715128
    move-object/from16 v0, p14

    iput-object v0, p0, LX/Aov;->m:LX/0SG;

    .line 1715129
    move-object/from16 v0, p15

    iput-object v0, p0, LX/Aov;->n:LX/0ad;

    .line 1715130
    iput-object p9, p0, LX/Aov;->i:LX/BYi;

    .line 1715131
    sget v1, LX/4Bm;->a:I

    const/4 v2, 0x0

    move-object/from16 v0, p16

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v1

    iput-boolean v1, p0, LX/Aov;->o:Z

    .line 1715132
    iput-object p10, p0, LX/Aov;->p:LX/BYg;

    .line 1715133
    move-object/from16 v0, p17

    iput-object v0, p0, LX/Aov;->q:LX/0Ot;

    .line 1715134
    move-object/from16 v0, p18

    iput-object v0, p0, LX/Aov;->r:LX/0pf;

    .line 1715135
    move-object/from16 v0, p19

    iput-object v0, p0, LX/Aov;->s:LX/0wL;

    .line 1715136
    move-object/from16 v0, p20

    iput-object v0, p0, LX/Aov;->t:LX/BEH;

    .line 1715137
    move-object/from16 v0, p21

    iput-object v0, p0, LX/Aov;->u:Lcom/facebook/content/SecureContextHelper;

    .line 1715138
    move-object/from16 v0, p22

    iput-object v0, p0, LX/Aov;->v:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    .line 1715139
    move-object/from16 v0, p23

    iput-object v0, p0, LX/Aov;->w:LX/0W3;

    .line 1715140
    return-void
.end method

.method private a()LX/0Px;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/Aot;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, -0x1

    .line 1715105
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 1715106
    iget-object v1, p0, LX/Aov;->j:LX/9Ip;

    .line 1715107
    iget-object v2, v1, LX/9Ip;->a:LX/0ad;

    sget v3, LX/9Io;->h:I

    const/4 v4, -0x1

    invoke-interface {v2, v3, v4}, LX/0ad;->a(II)I

    move-result v2

    move v1, v2

    .line 1715108
    if-eq v1, v6, :cond_0

    .line 1715109
    new-instance v2, LX/Aot;

    sget-object v3, LX/Aou;->WRITE_POST:LX/Aou;

    const v4, 0x7f081095

    const v5, 0x7f02080f

    invoke-direct {v2, v3, v4, v5, v1}, LX/Aot;-><init>(LX/Aou;III)V

    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1715110
    :cond_0
    iget-object v1, p0, LX/Aov;->j:LX/9Ip;

    .line 1715111
    iget-object v2, v1, LX/9Ip;->a:LX/0ad;

    sget v3, LX/9Io;->l:I

    const/4 v4, -0x1

    invoke-interface {v2, v3, v4}, LX/0ad;->a(II)I

    move-result v2

    move v1, v2

    .line 1715112
    if-eq v1, v6, :cond_1

    .line 1715113
    new-instance v2, LX/Aot;

    sget-object v3, LX/Aou;->SHARE_NOW:LX/Aou;

    const v4, 0x7f081091

    const v5, 0x7f0209af

    invoke-direct {v2, v3, v4, v5, v1}, LX/Aot;-><init>(LX/Aou;III)V

    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1715114
    :cond_1
    new-instance v1, LX/Aot;

    sget-object v2, LX/Aou;->SEND_AS_MESSAGE:LX/Aou;

    const v3, 0x7f081096

    invoke-static {}, LX/10A;->a()I

    move-result v4

    const/4 v5, 0x2

    invoke-direct {v1, v2, v3, v4, v5}, LX/Aot;-><init>(LX/Aou;III)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1715115
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method private static a(Z)LX/0Px;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "LX/0Px",
            "<",
            "LX/Aot;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1715098
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 1715099
    new-instance v2, LX/Aot;

    sget-object v3, LX/Aou;->SHARE_NOW:LX/Aou;

    if-eqz p0, :cond_0

    const v0, 0x7f081092

    :goto_0
    const v4, 0x7f0209af

    const/4 v5, 0x0

    invoke-direct {v2, v3, v0, v4, v5}, LX/Aot;-><init>(LX/Aou;III)V

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1715100
    new-instance v0, LX/Aot;

    sget-object v2, LX/Aou;->WRITE_POST:LX/Aou;

    const v3, 0x7f081095

    const v4, 0x7f02080f

    const/4 v5, 0x1

    invoke-direct {v0, v2, v3, v4, v5}, LX/Aot;-><init>(LX/Aou;III)V

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1715101
    new-instance v0, LX/Aot;

    sget-object v2, LX/Aou;->SEND_AS_MESSAGE:LX/Aou;

    const v3, 0x7f081096

    invoke-static {}, LX/10A;->a()I

    move-result v4

    const/4 v5, 0x2

    invoke-direct {v0, v2, v3, v4, v5}, LX/Aot;-><init>(LX/Aou;III)V

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1715102
    new-instance v0, LX/Aot;

    sget-object v2, LX/Aou;->SHARE_TO_PAGE:LX/Aou;

    const v3, 0x7f08109a

    const v4, 0x7f02190c

    const/4 v5, 0x3

    invoke-direct {v0, v2, v3, v4, v5}, LX/Aot;-><init>(LX/Aou;III)V

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1715103
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0

    .line 1715104
    :cond_0
    const v0, 0x7f081091

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/Aov;
    .locals 3

    .prologue
    .line 1715088
    sget-object v0, LX/Aov;->x:LX/Aov;

    if-nez v0, :cond_1

    .line 1715089
    const-class v1, LX/Aov;

    monitor-enter v1

    .line 1715090
    :try_start_0
    sget-object v0, LX/Aov;->x:LX/Aov;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1715091
    if-eqz v2, :cond_0

    .line 1715092
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/Aov;->b(LX/0QB;)LX/Aov;

    move-result-object v0

    sput-object v0, LX/Aov;->x:LX/Aov;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1715093
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1715094
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1715095
    :cond_1
    sget-object v0, LX/Aov;->x:LX/Aov;

    return-object v0

    .line 1715096
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1715097
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(LX/5OG;LX/Aot;Lcom/facebook/feed/rows/core/props/FeedProps;LX/Amm;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/5OG;",
            "LX/Aot;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "LX/Amm;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1715080
    invoke-virtual {p2, p1}, LX/Aot;->a(LX/5OG;)LX/3Ai;

    move-result-object v6

    .line 1715081
    new-instance v0, LX/Aom;

    move-object v1, p0

    move-object v2, p3

    move-object v3, p5

    move-object v4, p6

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, LX/Aom;-><init>(LX/Aov;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Ljava/lang/String;LX/Amm;)V

    invoke-virtual {v6, v0}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1715082
    iget-object v0, p0, LX/Aov;->j:LX/9Ip;

    invoke-virtual {v0}, LX/9Ip;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1715083
    iget-object v0, p0, LX/Aov;->j:LX/9Ip;

    .line 1715084
    iget-object v1, v0, LX/9Ip;->a:LX/0ad;

    sget-char v2, LX/9Io;->i:C

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1715085
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1715086
    invoke-virtual {v6, v0}, LX/3Ai;->a(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 1715087
    :cond_0
    return-void
.end method

.method private a(LX/5OG;LX/Aot;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/5OG;",
            "LX/Aot;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Landroid/view/View;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1715069
    iget-object v1, p0, LX/Aov;->d:LX/14w;

    .line 1715070
    iget-object v0, p3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1715071
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1, v0}, LX/14w;->v(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1715072
    :cond_0
    :goto_0
    return-void

    .line 1715073
    :cond_1
    invoke-virtual {p2, p1}, LX/Aot;->a(LX/5OG;)LX/3Ai;

    move-result-object v6

    .line 1715074
    new-instance v0, LX/Aon;

    move-object v1, p0

    move-object v2, p5

    move-object v3, p3

    move-object v4, p6

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, LX/Aon;-><init>(LX/Aov;Ljava/lang/String;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Landroid/view/View;)V

    invoke-virtual {v6, v0}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1715075
    iget-object v0, p0, LX/Aov;->j:LX/9Ip;

    invoke-virtual {v0}, LX/9Ip;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1715076
    iget-object v0, p0, LX/Aov;->j:LX/9Ip;

    .line 1715077
    iget-object v1, v0, LX/9Ip;->a:LX/0ad;

    sget-char v2, LX/9Io;->k:C

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1715078
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1715079
    invoke-virtual {v6, v0}, LX/3Ai;->a(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method private a(LX/5OG;LX/Aot;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Ljava/lang/String;Landroid/app/Activity;Z)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/5OG;",
            "LX/Aot;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Landroid/app/Activity;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 1715055
    invoke-virtual {p3}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v5

    check-cast v5, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1715056
    invoke-static {p0, v5}, LX/Aov;->a(LX/Aov;Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1715057
    :cond_0
    :goto_0
    return-void

    .line 1715058
    :cond_1
    iget-object v0, p0, LX/Aov;->g:LX/2sP;

    invoke-virtual {v0}, LX/2sP;->a()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v3

    .line 1715059
    invoke-virtual {p2, p1}, LX/Aot;->a(LX/5OG;)LX/3Ai;

    move-result-object v10

    .line 1715060
    new-instance v0, LX/Aol;

    move-object v1, p0

    move-object v2, p3

    move-object v4, p4

    move-object/from16 v6, p5

    move/from16 v7, p7

    move-object/from16 v8, p6

    move-object v9, p1

    invoke-direct/range {v0 .. v9}, LX/Aol;-><init>(LX/Aov;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLPrivacyOption;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;ZLandroid/app/Activity;LX/5OG;)V

    invoke-virtual {v10, v0}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1715061
    iget-object v0, p0, LX/Aov;->r:LX/0pf;

    invoke-virtual {v0, v5}, LX/0pf;->a(Lcom/facebook/graphql/model/FeedUnit;)LX/1g0;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/Aov;->r:LX/0pf;

    invoke-virtual {v0, v5}, LX/0pf;->a(Lcom/facebook/graphql/model/FeedUnit;)LX/1g0;

    move-result-object v0

    invoke-virtual {v0}, LX/1g0;->o()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1715062
    iget-object v0, p0, LX/Aov;->l:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    invoke-virtual {v0}, Lcom/facebook/user/model/User;->c()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/Aov;->r:LX/0pf;

    invoke-virtual {v1, v5}, LX/0pf;->a(Lcom/facebook/graphql/model/FeedUnit;)LX/1g0;

    move-result-object v1

    invoke-virtual {v1}, LX/1g0;->o()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1715063
    const v0, 0x7f081093

    invoke-virtual {v10, v0}, LX/3Ai;->a(I)Landroid/view/MenuItem;

    goto :goto_0

    .line 1715064
    :cond_2
    const v0, 0x7f081094

    invoke-virtual {v10, v0}, LX/3Ai;->a(I)Landroid/view/MenuItem;

    goto :goto_0

    .line 1715065
    :cond_3
    invoke-static {v3}, LX/2cA;->a(LX/1oV;)Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->EVERYONE:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    if-ne v0, v1, :cond_4

    .line 1715066
    const v0, 0x7f081094

    invoke-virtual {v10, v0}, LX/3Ai;->a(I)Landroid/view/MenuItem;

    goto :goto_0

    .line 1715067
    :cond_4
    invoke-static {v3}, LX/2cA;->a(LX/1oV;)Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->FRIENDS:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    if-ne v0, v1, :cond_0

    .line 1715068
    const v0, 0x7f081093

    invoke-virtual {v10, v0}, LX/3Ai;->a(I)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method private static a(LX/Aov;Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1715045
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    .line 1715046
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->R()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, LX/Aov;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1715047
    iget-boolean v2, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->d:Z

    move v0, v2

    .line 1715048
    if-nez v0, :cond_1

    iget-object v0, p0, LX/Aov;->q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    sget-object v2, LX/01T;->PAA:LX/01T;

    if-ne v0, v2, :cond_2

    :cond_1
    move v0, v1

    .line 1715049
    :goto_0
    return v0

    .line 1715050
    :cond_2
    iget-object v0, p0, LX/Aov;->g:LX/2sP;

    invoke-virtual {v0}, LX/2sP;->a()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v0

    .line 1715051
    if-eqz v0, :cond_3

    .line 1715052
    sget-object v2, LX/Aos;->a:[I

    invoke-static {v0}, LX/2cA;->a(LX/1oV;)Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    :cond_3
    move v0, v1

    .line 1715053
    goto :goto_0

    .line 1715054
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private b(Z)LX/0Px;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "LX/0Px",
            "<",
            "LX/Aot;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1715032
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 1715033
    new-instance v2, LX/Aot;

    sget-object v3, LX/Aou;->SHARE_NOW:LX/Aou;

    if-eqz p1, :cond_2

    const v0, 0x7f081092

    :goto_0
    const v4, 0x7f0209af

    const/4 v5, 0x0

    invoke-direct {v2, v3, v0, v4, v5}, LX/Aot;-><init>(LX/Aou;III)V

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1715034
    new-instance v0, LX/Aot;

    sget-object v2, LX/Aou;->WRITE_POST:LX/Aou;

    const v3, 0x7f081095

    const v4, 0x7f02080f

    const/4 v5, 0x1

    invoke-direct {v0, v2, v3, v4, v5}, LX/Aot;-><init>(LX/Aou;III)V

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1715035
    iget-object v0, p0, LX/Aov;->p:LX/BYg;

    .line 1715036
    iget-object v2, v0, LX/BYg;->a:LX/0ad;

    sget-short v3, LX/BYe;->c:S

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, LX/0ad;->a(SZ)Z

    move-result v2

    move v0, v2

    .line 1715037
    if-eqz v0, :cond_0

    .line 1715038
    new-instance v0, LX/Aot;

    sget-object v2, LX/Aou;->SEND_AS_MESSAGE:LX/Aou;

    const v3, 0x7f081096

    invoke-static {}, LX/10A;->a()I

    move-result v4

    const/4 v5, 0x2

    invoke-direct {v0, v2, v3, v4, v5}, LX/Aot;-><init>(LX/Aou;III)V

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1715039
    :cond_0
    iget-object v0, p0, LX/Aov;->p:LX/BYg;

    .line 1715040
    iget-object v2, v0, LX/BYg;->a:LX/0ad;

    sget-short v3, LX/BYe;->d:S

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, LX/0ad;->a(SZ)Z

    move-result v2

    move v0, v2

    .line 1715041
    if-eqz v0, :cond_1

    .line 1715042
    new-instance v0, LX/Aot;

    sget-object v2, LX/Aou;->SHARE_EXTERNAL:LX/Aou;

    const v3, 0x7f081098

    const v4, 0x7f0209c3

    const/4 v5, 0x3

    invoke-direct {v0, v2, v3, v4, v5}, LX/Aot;-><init>(LX/Aou;III)V

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1715043
    :cond_1
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0

    .line 1715044
    :cond_2
    const v0, 0x7f081091

    goto :goto_0
.end method

.method private static b(LX/0QB;)LX/Aov;
    .locals 26

    .prologue
    .line 1714976
    new-instance v2, LX/Aov;

    invoke-static/range {p0 .. p0}, LX/14w;->a(LX/0QB;)LX/14w;

    move-result-object v3

    check-cast v3, LX/14w;

    const/16 v4, 0x19c6

    move-object/from16 v0, p0

    invoke-static {v0, v4}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 v5, 0x12b0

    move-object/from16 v0, p0

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static/range {p0 .. p0}, LX/2sP;->a(LX/0QB;)LX/2sP;

    move-result-object v6

    check-cast v6, LX/2sP;

    const/16 v7, 0x2978

    move-object/from16 v0, p0

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static/range {p0 .. p0}, LX/1EQ;->a(LX/0QB;)LX/1EQ;

    move-result-object v8

    check-cast v8, LX/1EQ;

    const/16 v9, 0x19e

    move-object/from16 v0, p0

    invoke-static {v0, v9}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    const/16 v10, 0x3770

    move-object/from16 v0, p0

    invoke-static {v0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-static/range {p0 .. p0}, LX/BYi;->a(LX/0QB;)LX/BYi;

    move-result-object v11

    check-cast v11, LX/BYi;

    invoke-static/range {p0 .. p0}, LX/BYg;->a(LX/0QB;)LX/BYg;

    move-result-object v12

    check-cast v12, LX/BYg;

    invoke-static/range {p0 .. p0}, LX/9Ip;->a(LX/0QB;)LX/9Ip;

    move-result-object v13

    check-cast v13, LX/9Ip;

    const-class v14, LX/9A4;

    move-object/from16 v0, p0

    invoke-interface {v0, v14}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v14

    check-cast v14, LX/9A4;

    const/16 v15, 0x12cb

    move-object/from16 v0, p0

    invoke-static {v0, v15}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v15

    invoke-static/range {p0 .. p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v16

    check-cast v16, LX/0SG;

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v17

    check-cast v17, LX/0ad;

    invoke-static/range {p0 .. p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v18

    check-cast v18, LX/0Uh;

    const/16 v19, 0x3df

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v19

    invoke-static/range {p0 .. p0}, LX/0pf;->a(LX/0QB;)LX/0pf;

    move-result-object v20

    check-cast v20, LX/0pf;

    invoke-static/range {p0 .. p0}, LX/0wL;->a(LX/0QB;)LX/0wL;

    move-result-object v21

    check-cast v21, LX/0wL;

    invoke-static/range {p0 .. p0}, LX/BEH;->a(LX/0QB;)LX/BEH;

    move-result-object v22

    check-cast v22, LX/BEH;

    invoke-static/range {p0 .. p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v23

    check-cast v23, Lcom/facebook/content/SecureContextHelper;

    invoke-static/range {p0 .. p0}, LX/1nB;->a(LX/0QB;)LX/1nB;

    move-result-object v24

    check-cast v24, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-static/range {p0 .. p0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v25

    check-cast v25, LX/0W3;

    invoke-direct/range {v2 .. v25}, LX/Aov;-><init>(LX/14w;LX/0Or;LX/0Ot;LX/2sP;LX/0Ot;LX/1EQ;LX/0Or;LX/0Ot;LX/BYi;LX/BYg;LX/9Ip;LX/9A4;LX/0Or;LX/0SG;LX/0ad;LX/0Uh;LX/0Ot;LX/0pf;LX/0wL;LX/BEH;Lcom/facebook/content/SecureContextHelper;Lcom/facebook/intent/feed/IFeedIntentBuilder;LX/0W3;)V

    .line 1714977
    return-object v2
.end method

.method private b(LX/5OG;LX/Aot;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/5OG;",
            "LX/Aot;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Landroid/view/View;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1715028
    invoke-virtual {p2, p1}, LX/Aot;->a(LX/5OG;)LX/3Ai;

    move-result-object v6

    .line 1715029
    const v0, 0x7f081099

    invoke-virtual {v6, v0}, LX/3Ai;->a(I)Landroid/view/MenuItem;

    .line 1715030
    new-instance v0, LX/Aoo;

    move-object v1, p0

    move-object v2, p3

    move-object v3, p5

    move-object v4, p6

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, LX/Aoo;-><init>(LX/Aov;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Ljava/lang/String;Landroid/view/View;)V

    invoke-virtual {v6, v0}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1715031
    return-void
.end method

.method private c(LX/5OG;LX/Aot;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/5OG;",
            "LX/Aot;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Landroid/view/View;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1715018
    iget-object v0, p3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, v0

    .line 1715019
    check-cast v4, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1715020
    invoke-virtual {p2, p1}, LX/Aot;->a(LX/5OG;)LX/3Ai;

    move-result-object v7

    .line 1715021
    new-instance v0, LX/Aop;

    move-object v1, p0

    move-object v2, p3

    move-object v3, p5

    move-object v5, p6

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, LX/Aop;-><init>(LX/Aov;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;Landroid/view/View;)V

    invoke-virtual {v7, v0}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1715022
    iget-object v0, p0, LX/Aov;->j:LX/9Ip;

    invoke-virtual {v0}, LX/9Ip;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1715023
    iget-object v0, p0, LX/Aov;->j:LX/9Ip;

    .line 1715024
    iget-object v1, v0, LX/9Ip;->a:LX/0ad;

    sget-char v2, LX/9Io;->b:C

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1715025
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1715026
    invoke-virtual {v7, v0}, LX/3Ai;->a(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 1715027
    :cond_0
    return-void
.end method

.method private d(LX/5OG;LX/Aot;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/5OG;",
            "LX/Aot;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Landroid/view/View;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1715015
    invoke-virtual {p2, p1}, LX/Aot;->a(LX/5OG;)LX/3Ai;

    move-result-object v6

    .line 1715016
    new-instance v0, LX/Aoq;

    move-object v1, p0

    move-object v2, p4

    move-object v3, p3

    move-object v4, p5

    move-object v5, p6

    invoke-direct/range {v0 .. v5}, LX/Aoq;-><init>(LX/Aov;Landroid/view/View;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v6, v0}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1715017
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;Ljava/lang/String;LX/Amm;LX/Amo;Z)LX/5OM;
    .locals 17
    .param p5    # LX/Amo;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Landroid/view/View;",
            "Ljava/lang/String;",
            "LX/Amm;",
            "LX/Amo;",
            "Z)",
            "LX/5OM;"
        }
    .end annotation

    .prologue
    .line 1714978
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v2

    move-object v14, v2

    check-cast v14, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1714979
    invoke-static {v14}, LX/214;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1714980
    const/4 v2, 0x0

    .line 1714981
    :goto_0
    return-object v2

    .line 1714982
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, LX/Aov;->j:LX/9Ip;

    invoke-virtual {v2}, LX/9Ip;->b()Z

    move-result v2

    .line 1714983
    move-object/from16 v0, p0

    iget-object v3, v0, LX/Aov;->j:LX/9Ip;

    invoke-virtual {v3}, LX/9Ip;->a()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1714984
    const/4 v2, 0x0

    goto :goto_0

    .line 1714985
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, LX/Aov;->j:LX/9Ip;

    invoke-virtual {v3}, LX/9Ip;->c()Z

    move-result v3

    .line 1714986
    invoke-static {v14}, LX/0sa;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v4

    invoke-static {v4}, LX/2cA;->a(Lcom/facebook/graphql/model/GraphQLPrivacyScope;)Z

    move-result v4

    .line 1714987
    if-eqz v2, :cond_3

    .line 1714988
    invoke-direct/range {p0 .. p0}, LX/Aov;->a()LX/0Px;

    move-result-object v2

    move-object v15, v2

    .line 1714989
    :goto_1
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1714990
    new-instance v16, LX/6WS;

    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-direct {v0, v2}, LX/6WS;-><init>(Landroid/content/Context;)V

    .line 1714991
    move-object/from16 v0, v16

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, LX/0ht;->c(Landroid/view/View;)V

    .line 1714992
    const/4 v2, 0x1

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, LX/5OM;->a(Z)V

    .line 1714993
    sget-object v2, LX/3AV;->ABOVE:LX/3AV;

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, LX/0ht;->a(LX/3AV;)V

    .line 1714994
    new-instance v2, LX/Aor;

    move-object/from16 v3, p0

    move-object/from16 v4, p2

    move-object/from16 v6, p1

    move-object/from16 v7, p3

    move-object/from16 v8, p5

    invoke-direct/range {v2 .. v8}, LX/Aor;-><init>(LX/Aov;Landroid/view/View;Ljava/lang/String;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;LX/Amo;)V

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, LX/0ht;->a(LX/2yQ;)V

    .line 1714995
    invoke-virtual/range {v16 .. v16}, LX/5OM;->c()LX/5OG;

    move-result-object v7

    .line 1714996
    invoke-virtual {v15}, LX/0Px;->size()I

    move-result v3

    const/4 v2, 0x0

    :goto_2
    if-ge v2, v3, :cond_5

    invoke-virtual {v15, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/Aot;

    .line 1714997
    sget-object v4, LX/Aos;->b:[I

    iget-object v6, v8, LX/Aot;->a:LX/Aou;

    invoke-virtual {v6}, LX/Aou;->ordinal()I

    move-result v6

    aget v4, v4, v6

    packed-switch v4, :pswitch_data_0

    .line 1714998
    :cond_2
    :goto_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 1714999
    :cond_3
    if-nez v3, :cond_4

    move-object/from16 v0, p0

    iget-object v2, v0, LX/Aov;->p:LX/BYg;

    invoke-virtual {v2}, LX/BYg;->a()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1715000
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, LX/Aov;->b(Z)LX/0Px;

    move-result-object v2

    move-object v15, v2

    goto :goto_1

    .line 1715001
    :cond_4
    invoke-static {v4}, LX/Aov;->a(Z)LX/0Px;

    move-result-object v2

    move-object v15, v2

    goto :goto_1

    .line 1715002
    :pswitch_0
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    const-class v6, Landroid/app/Activity;

    invoke-static {v4, v6}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Landroid/app/Activity;

    .line 1715003
    invoke-static {v12}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v6, p0

    move-object/from16 v9, p1

    move-object/from16 v10, p3

    move-object v11, v5

    move/from16 v13, p6

    .line 1715004
    invoke-direct/range {v6 .. v13}, LX/Aov;->a(LX/5OG;LX/Aot;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Ljava/lang/String;Landroid/app/Activity;Z)V

    goto :goto_3

    :pswitch_1
    move-object/from16 v6, p0

    move-object/from16 v9, p1

    move-object/from16 v10, p4

    move-object/from16 v11, p3

    move-object v12, v5

    .line 1715005
    invoke-direct/range {v6 .. v12}, LX/Aov;->a(LX/5OG;LX/Aot;Lcom/facebook/feed/rows/core/props/FeedProps;LX/Amm;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    :pswitch_2
    move-object/from16 v6, p0

    move-object/from16 v9, p1

    move-object/from16 v10, p2

    move-object/from16 v11, p3

    move-object v12, v5

    .line 1715006
    invoke-direct/range {v6 .. v12}, LX/Aov;->a(LX/5OG;LX/Aot;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    :pswitch_3
    move-object/from16 v6, p0

    move-object/from16 v9, p1

    move-object/from16 v10, p2

    move-object/from16 v11, p3

    move-object v12, v5

    .line 1715007
    invoke-direct/range {v6 .. v12}, LX/Aov;->b(LX/5OG;LX/Aot;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    :pswitch_4
    move-object/from16 v6, p0

    move-object/from16 v9, p1

    move-object/from16 v10, p2

    move-object/from16 v11, p3

    move-object v12, v5

    .line 1715008
    invoke-direct/range {v6 .. v12}, LX/Aov;->c(LX/5OG;LX/Aot;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 1715009
    :pswitch_5
    move-object/from16 v0, p0

    iget-object v4, v0, LX/Aov;->t:LX/BEH;

    invoke-virtual {v4}, LX/BEH;->a()LX/03R;

    move-result-object v4

    sget-object v6, LX/03R;->YES:LX/03R;

    if-ne v4, v6, :cond_2

    move-object/from16 v0, p0

    iget-object v4, v0, LX/Aov;->w:LX/0W3;

    sget-wide v10, LX/0X5;->ew:J

    const/4 v6, 0x0

    invoke-interface {v4, v10, v11, v6}, LX/0W4;->a(JZ)Z

    move-result v4

    if-eqz v4, :cond_2

    move-object/from16 v6, p0

    move-object/from16 v9, p1

    move-object/from16 v10, p2

    move-object/from16 v11, p3

    move-object v12, v5

    .line 1715010
    invoke-direct/range {v6 .. v12}, LX/Aov;->d(LX/5OG;LX/Aot;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 1715011
    :cond_5
    invoke-virtual {v7}, LX/5OG;->getCount()I

    move-result v2

    const/4 v3, 0x2

    if-ge v2, v3, :cond_6

    .line 1715012
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 1715013
    :cond_6
    move-object/from16 v0, p0

    iget-object v6, v0, LX/Aov;->b:LX/1EQ;

    invoke-static/range {p1 .. p1}, LX/14w;->q(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v2, v0, LX/Aov;->c:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-virtual {v2}, Lcom/facebook/auth/viewercontext/ViewerContext;->a()Ljava/lang/String;

    move-result-object v9

    invoke-static {v14}, LX/214;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v10

    move-object/from16 v7, p3

    move-object v11, v5

    invoke-virtual/range {v6 .. v11}, LX/1EQ;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v2, v16

    .line 1715014
    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
