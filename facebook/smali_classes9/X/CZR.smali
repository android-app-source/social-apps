.class public final LX/CZR;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1915455
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 1915456
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1915457
    :goto_0
    return v1

    .line 1915458
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1915459
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 1915460
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1915461
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1915462
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 1915463
    const-string v4, "image_ranges"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1915464
    invoke-static {p0, p1}, LX/CZQ;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 1915465
    :cond_2
    const-string v4, "text"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1915466
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 1915467
    :cond_3
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1915468
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1915469
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1915470
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    move v2, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 7

    .prologue
    .line 1915471
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1915472
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1915473
    if-eqz v0, :cond_7

    .line 1915474
    const-string v1, "image_ranges"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1915475
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1915476
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_6

    .line 1915477
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    const/4 v5, 0x0

    .line 1915478
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1915479
    invoke-virtual {p0, v2, v5}, LX/15i;->g(II)I

    move-result v3

    .line 1915480
    if-eqz v3, :cond_3

    .line 1915481
    const-string v4, "entity_with_image"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1915482
    const/4 v6, 0x0

    .line 1915483
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1915484
    invoke-virtual {p0, v3, v6}, LX/15i;->g(II)I

    move-result v4

    .line 1915485
    if-eqz v4, :cond_0

    .line 1915486
    const-string v4, "__type__"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1915487
    invoke-static {p0, v3, v6, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1915488
    :cond_0
    const/4 v4, 0x1

    invoke-virtual {p0, v3, v4}, LX/15i;->g(II)I

    move-result v4

    .line 1915489
    if-eqz v4, :cond_2

    .line 1915490
    const-string v6, "image"

    invoke-virtual {p2, v6}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1915491
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1915492
    const/4 v6, 0x0

    invoke-virtual {p0, v4, v6}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v6

    .line 1915493
    if-eqz v6, :cond_1

    .line 1915494
    const-string v3, "uri"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1915495
    invoke-virtual {p2, v6}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1915496
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1915497
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1915498
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {p0, v2, v3, v5}, LX/15i;->a(III)I

    move-result v3

    .line 1915499
    if-eqz v3, :cond_4

    .line 1915500
    const-string v4, "length"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1915501
    invoke-virtual {p2, v3}, LX/0nX;->b(I)V

    .line 1915502
    :cond_4
    const/4 v3, 0x2

    invoke-virtual {p0, v2, v3, v5}, LX/15i;->a(III)I

    move-result v3

    .line 1915503
    if-eqz v3, :cond_5

    .line 1915504
    const-string v4, "offset"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1915505
    invoke-virtual {p2, v3}, LX/0nX;->b(I)V

    .line 1915506
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1915507
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1915508
    :cond_6
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1915509
    :cond_7
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1915510
    if-eqz v0, :cond_8

    .line 1915511
    const-string v1, "text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1915512
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1915513
    :cond_8
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1915514
    return-void
.end method
