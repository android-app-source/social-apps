.class public LX/Av3;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Landroid/widget/ImageView;

.field public b:Landroid/graphics/Rect;

.field public c:Landroid/graphics/Rect;

.field public d:F

.field public e:F

.field public f:LX/Auk;

.field private g:LX/0wd;


# direct methods
.method public constructor <init>(LX/0wW;)V
    .locals 6
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1723623
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1723624
    invoke-virtual {p1}, LX/0wW;->a()LX/0wd;

    move-result-object v0

    iput-object v0, p0, LX/Av3;->g:LX/0wd;

    .line 1723625
    iget-object v0, p0, LX/Av3;->g:LX/0wd;

    const-wide v2, 0x406b800000000000L    # 220.0

    const-wide/high16 v4, 0x402e000000000000L    # 15.0

    invoke-static {v2, v3, v4, v5}, LX/0wT;->a(DD)LX/0wT;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0wT;)LX/0wd;

    .line 1723626
    iget-object v0, p0, LX/Av3;->g:LX/0wd;

    const/4 v1, 0x1

    .line 1723627
    iput-boolean v1, v0, LX/0wd;->c:Z

    .line 1723628
    iget-object v0, p0, LX/Av3;->g:LX/0wd;

    new-instance v1, LX/Av2;

    invoke-direct {v1, p0}, LX/Av2;-><init>(LX/Av3;)V

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0xi;)LX/0wd;

    .line 1723629
    return-void
.end method


# virtual methods
.method public final a(Landroid/widget/ImageView;Landroid/graphics/Rect;FFLX/Auk;)V
    .locals 8

    .prologue
    const/high16 v7, 0x43340000    # 180.0f

    const/high16 v6, 0x43b40000    # 360.0f

    .line 1723630
    iput-object p1, p0, LX/Av3;->a:Landroid/widget/ImageView;

    .line 1723631
    new-instance v0, Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/widget/ImageView;->getTranslationX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p1}, Landroid/widget/ImageView;->getTranslationY()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p1}, Landroid/widget/ImageView;->getTranslationX()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {p1}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    iget v4, v4, Landroid/view/ViewGroup$LayoutParams;->width:I

    add-int/2addr v3, v4

    invoke-virtual {p1}, Landroid/widget/ImageView;->getTranslationY()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {p1}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    iget v5, v5, Landroid/view/ViewGroup$LayoutParams;->height:I

    add-int/2addr v4, v5

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, LX/Av3;->b:Landroid/graphics/Rect;

    .line 1723632
    iput-object p2, p0, LX/Av3;->c:Landroid/graphics/Rect;

    .line 1723633
    add-float v0, p3, v7

    rem-float/2addr v0, v6

    sub-float/2addr v0, v6

    rem-float/2addr v0, v6

    add-float/2addr v0, v7

    iput v0, p0, LX/Av3;->d:F

    .line 1723634
    add-float v0, p4, v7

    rem-float/2addr v0, v6

    sub-float/2addr v0, v6

    rem-float/2addr v0, v6

    add-float/2addr v0, v7

    iput v0, p0, LX/Av3;->e:F

    .line 1723635
    iput-object p5, p0, LX/Av3;->f:LX/Auk;

    .line 1723636
    iget-object v0, p0, LX/Av3;->g:LX/0wd;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, LX/0wd;->a(D)LX/0wd;

    .line 1723637
    iget-object v0, p0, LX/Av3;->g:LX/0wd;

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    .line 1723638
    return-void
.end method
