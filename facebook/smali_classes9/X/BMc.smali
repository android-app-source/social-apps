.class public LX/BMc;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/BMa;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/productionprompts/ui/components/LowConfidencePromptBannerComponentSpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1777613
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/BMc;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/productionprompts/ui/components/LowConfidencePromptBannerComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1777614
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1777615
    iput-object p1, p0, LX/BMc;->b:LX/0Ot;

    .line 1777616
    return-void
.end method

.method public static a(LX/0QB;)LX/BMc;
    .locals 4

    .prologue
    .line 1777617
    const-class v1, LX/BMc;

    monitor-enter v1

    .line 1777618
    :try_start_0
    sget-object v0, LX/BMc;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1777619
    sput-object v2, LX/BMc;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1777620
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1777621
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1777622
    new-instance v3, LX/BMc;

    const/16 p0, 0x2ffc

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/BMc;-><init>(LX/0Ot;)V

    .line 1777623
    move-object v0, v3

    .line 1777624
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1777625
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/BMc;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1777626
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1777627
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1777628
    invoke-static {}, LX/1dS;->b()V

    .line 1777629
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b(LX/1De;IILX/1X1;)LX/1Dg;
    .locals 9

    .prologue
    .line 1777630
    check-cast p4, LX/BMb;

    .line 1777631
    iget-object v0, p0, LX/BMc;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/productionprompts/ui/components/LowConfidencePromptBannerComponentSpec;

    iget-object v3, p4, LX/BMb;->a:Ljava/lang/String;

    iget-object v4, p4, LX/BMb;->b:Ljava/lang/String;

    iget-object v5, p4, LX/BMb;->c:Ljava/lang/String;

    iget-object v6, p4, LX/BMb;->d:Ljava/util/List;

    move-object v1, p1

    move v2, p2

    const/4 p4, 0x1

    const/4 v8, 0x0

    .line 1777632
    invoke-static {v2}, LX/1mh;->b(I)I

    move-result v7

    invoke-virtual {v1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    const p1, 0x7f0b1172

    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p0

    invoke-virtual {v1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const p2, 0x7f0b1173

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    add-int/2addr p0, p1

    div-int p0, v7, p0

    .line 1777633
    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v7

    invoke-interface {v7, p4}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v7

    invoke-static {v1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object p1

    invoke-virtual {p1, v3}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object p1

    const p2, 0x7f0a00a8

    invoke-virtual {p1, p2}, LX/1ne;->n(I)LX/1ne;

    move-result-object p1

    const p2, 0x7f0b0050

    invoke-virtual {p1, p2}, LX/1ne;->q(I)LX/1ne;

    move-result-object p1

    sget-object p2, LX/0xq;->ROBOTO:LX/0xq;

    sget-object p3, LX/0xr;->MEDIUM:LX/0xr;

    invoke-static {v1, p2, p3, v8}, LX/0xs;->a(Landroid/content/Context;LX/0xq;LX/0xr;Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    move-result-object p2

    invoke-virtual {p1, p2}, LX/1ne;->a(Landroid/graphics/Typeface;)LX/1ne;

    move-result-object p1

    invoke-interface {v7, p1}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object p1

    if-eqz v6, :cond_0

    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_1

    :cond_0
    move-object v7, v8

    :goto_0
    invoke-interface {p1, v7}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object p0

    invoke-static {v4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_2

    move-object v7, v8

    :goto_1
    invoke-interface {p0, v7}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v7

    invoke-static {v5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result p0

    if-eqz p0, :cond_3

    :goto_2
    invoke-interface {v7, v8}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v7

    invoke-interface {v7}, LX/1Di;->k()LX/1Dg;

    move-result-object v7

    move-object v0, v7

    .line 1777634
    return-object v0

    :cond_1
    iget-object v7, v0, Lcom/facebook/productionprompts/ui/components/LowConfidencePromptBannerComponentSpec;->b:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/8yV;

    invoke-virtual {v7, v1}, LX/8yV;->c(LX/1De;)LX/8yU;

    move-result-object v7

    sget-object p2, Lcom/facebook/productionprompts/ui/components/LowConfidencePromptBannerComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v7, p2}, LX/8yU;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/8yU;

    move-result-object v7

    invoke-virtual {v7, v6}, LX/8yU;->a(Ljava/util/List;)LX/8yU;

    move-result-object v7

    const p2, 0x7f0b1172

    invoke-virtual {v7, p2}, LX/8yU;->h(I)LX/8yU;

    move-result-object v7

    const p2, 0x7f0b1173

    invoke-virtual {v7, p2}, LX/8yU;->l(I)LX/8yU;

    move-result-object v7

    const p2, 0x7f0b0066

    invoke-virtual {v7, p2}, LX/8yU;->r(I)LX/8yU;

    move-result-object v7

    const/4 p2, 0x0

    invoke-virtual {v7, p2}, LX/8yU;->a(Z)LX/8yU;

    move-result-object v7

    invoke-virtual {v7, p0}, LX/8yU;->m(I)LX/8yU;

    move-result-object v7

    invoke-virtual {v7}, LX/1X5;->c()LX/1Di;

    move-result-object v7

    const/high16 p0, 0x3f800000    # 1.0f

    invoke-interface {v7, p0}, LX/1Di;->a(F)LX/1Di;

    move-result-object v7

    goto :goto_0

    :cond_2
    invoke-static {v1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v7

    invoke-virtual {v7, v4}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v7

    const p1, 0x7f0a00a4

    invoke-virtual {v7, p1}, LX/1ne;->n(I)LX/1ne;

    move-result-object v7

    const p1, 0x7f0b004e

    invoke-virtual {v7, p1}, LX/1ne;->q(I)LX/1ne;

    move-result-object v7

    sget-object p1, LX/0xq;->ROBOTO:LX/0xq;

    sget-object p2, LX/0xr;->REGULAR:LX/0xr;

    invoke-static {v1, p1, p2, v8}, LX/0xs;->a(Landroid/content/Context;LX/0xq;LX/0xr;Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    move-result-object p1

    invoke-virtual {v7, p1}, LX/1ne;->a(Landroid/graphics/Typeface;)LX/1ne;

    move-result-object v7

    goto :goto_1

    :cond_3
    invoke-static {v1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object p0

    invoke-virtual {p0, v5}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object p0

    const p1, 0x7f0a008d

    invoke-virtual {p0, p1}, LX/1ne;->n(I)LX/1ne;

    move-result-object p0

    const p1, 0x7f0b004e

    invoke-virtual {p0, p1}, LX/1ne;->q(I)LX/1ne;

    move-result-object p0

    sget-object p1, LX/0xq;->ROBOTO:LX/0xq;

    sget-object p2, LX/0xr;->BOLD:LX/0xr;

    invoke-static {v1, p1, p2, v8}, LX/0xs;->a(Landroid/content/Context;LX/0xq;LX/0xr;Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    move-result-object v8

    invoke-virtual {p0, v8}, LX/1ne;->a(Landroid/graphics/Typeface;)LX/1ne;

    move-result-object v8

    invoke-virtual {v8}, LX/1X5;->c()LX/1Di;

    move-result-object v8

    const p0, 0x7f0b0063

    invoke-interface {v8, p4, p0}, LX/1Di;->c(II)LX/1Di;

    move-result-object v8

    goto/16 :goto_2
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1777635
    const/4 v0, 0x1

    return v0
.end method
