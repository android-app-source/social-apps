.class public final LX/B7t;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/B7v;


# direct methods
.method public constructor <init>(LX/B7v;)V
    .locals 0

    .prologue
    .line 1748260
    iput-object p1, p0, LX/B7t;->a:LX/B7v;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 13

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x2

    const v0, -0x5743e609

    invoke-static {v4, v5, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1748248
    iget-object v0, p0, LX/B7t;->a:LX/B7v;

    invoke-virtual {v0}, LX/B7v;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v2, "input_method"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 1748249
    iget-object v2, p0, LX/B7t;->a:LX/B7v;

    invoke-virtual {v2}, LX/B7v;->getRootView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1748250
    iget-object v0, p0, LX/B7t;->a:LX/B7v;

    iget-object v0, v0, LX/B7v;->c:LX/B7W;

    new-instance v2, LX/B7Z;

    const/4 v3, 0x0

    invoke-direct {v2, v5, v3}, LX/B7Z;-><init>(ZLandroid/view/View;)V

    invoke-virtual {v0, v2}, LX/0b4;->a(LX/0b7;)V

    .line 1748251
    iget-object v0, p0, LX/B7t;->a:LX/B7v;

    iget-object v0, v0, LX/B7v;->h:Ljava/util/Calendar;

    if-nez v0, :cond_0

    .line 1748252
    iget-object v0, p0, LX/B7t;->a:LX/B7v;

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 1748253
    iput-object v2, v0, LX/B7v;->h:Ljava/util/Calendar;

    .line 1748254
    :cond_0
    iget-object v0, p0, LX/B7t;->a:LX/B7v;

    .line 1748255
    new-instance v6, Landroid/app/DatePickerDialog;

    invoke-virtual {v0}, LX/B7v;->getContext()Landroid/content/Context;

    move-result-object v7

    new-instance v8, LX/B7u;

    invoke-direct {v8, v0}, LX/B7u;-><init>(LX/B7v;)V

    iget-object v9, v0, LX/B7v;->h:Ljava/util/Calendar;

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Ljava/util/Calendar;->get(I)I

    move-result v9

    iget-object v10, v0, LX/B7v;->h:Ljava/util/Calendar;

    const/4 v11, 0x2

    invoke-virtual {v10, v11}, Ljava/util/Calendar;->get(I)I

    move-result v10

    iget-object v11, v0, LX/B7v;->h:Ljava/util/Calendar;

    const/4 v12, 0x5

    invoke-virtual {v11, v12}, Ljava/util/Calendar;->get(I)I

    move-result v11

    invoke-direct/range {v6 .. v11}, Landroid/app/DatePickerDialog;-><init>(Landroid/content/Context;Landroid/app/DatePickerDialog$OnDateSetListener;III)V

    .line 1748256
    invoke-virtual {v6}, Landroid/app/DatePickerDialog;->getDatePicker()Landroid/widget/DatePicker;

    move-result-object v7

    .line 1748257
    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/DatePicker;->setCalendarViewShown(Z)V

    .line 1748258
    invoke-virtual {v6}, Landroid/app/DatePickerDialog;->show()V

    .line 1748259
    const v0, 0x2a90ca4

    invoke-static {v4, v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
