.class public LX/AXa;
.super LX/AWT;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1684364
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/AXa;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1684365
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1684366
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/AXa;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1684367
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1684368
    invoke-direct {p0, p1, p2, p3}, LX/AWT;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1684369
    new-instance v0, Landroid/view/View;

    invoke-direct {v0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 1684370
    const v1, 0x7f0a03ac

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 1684371
    invoke-virtual {p0, v0}, LX/AXa;->addView(Landroid/view/View;)V

    .line 1684372
    return-void
.end method
