.class public LX/Azp;
.super Landroid/widget/LinearLayout;
.source ""

# interfaces
.implements LX/Azj;


# instance fields
.field public a:LX/1b1;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0kL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1732980
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/Azp;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1732981
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1732982
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/Azp;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1732983
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 1732984
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1732985
    const-class v0, LX/Azp;

    invoke-static {v0, p0}, LX/Azp;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1732986
    const v0, 0x7f0307b5

    invoke-static {p1, v0, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1732987
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p0, v0}, LX/Azp;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1732988
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/Azp;->setOrientation(I)V

    .line 1732989
    invoke-virtual {p0}, LX/Azp;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1901

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 1732990
    invoke-virtual {p0, v0, v0, v0, v0}, LX/Azp;->setPadding(IIII)V

    .line 1732991
    const v0, 0x7f0d147e

    invoke-virtual {p0, v0}, LX/Azp;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, LX/Azo;

    invoke-direct {v1, p0}, LX/Azo;-><init>(LX/Azp;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1732992
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/Azp;

    invoke-static {p0}, LX/1b1;->b(LX/0QB;)LX/1b1;

    move-result-object v1

    check-cast v1, LX/1b1;

    invoke-static {p0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object p0

    check-cast p0, LX/0kL;

    iput-object v1, p1, LX/Azp;->a:LX/1b1;

    iput-object p0, p1, LX/Azp;->b:LX/0kL;

    return-void
.end method


# virtual methods
.method public final a()Landroid/view/View;
    .locals 0

    .prologue
    .line 1732993
    return-object p0
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 1732994
    return-void
.end method

.method public getAspectRatio()F
    .locals 1

    .prologue
    .line 1732995
    const/high16 v0, 0x40000000    # 2.0f

    return v0
.end method

.method public getGlyphLabel()I
    .locals 1

    .prologue
    .line 1732996
    const v0, 0x7f020313

    return v0
.end method

.method public getLoggingType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1732997
    const-string v0, "camera_tab"

    return-object v0
.end method
