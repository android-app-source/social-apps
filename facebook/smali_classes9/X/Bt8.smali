.class public LX/Bt8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/31j;


# instance fields
.field private final a:Lcom/facebook/content/SecureContextHelper;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/facebook/content/SecureContextHelper;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1828632
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1828633
    iput-object p1, p0, LX/Bt8;->a:Lcom/facebook/content/SecureContextHelper;

    .line 1828634
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;FF)V
    .locals 3

    .prologue
    .line 1828635
    iget-object v0, p0, LX/Bt8;->b:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1828636
    :goto_0
    return-void

    .line 1828637
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1828638
    const-string v1, "extra_instant_articles_id"

    iget-object v2, p0, LX/Bt8;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1828639
    iget-object v1, p0, LX/Bt8;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 1828640
    const-string v1, "extra_instant_articles_referrer"

    iget-object v2, p0, LX/Bt8;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1828641
    :cond_1
    iget-object v1, p0, LX/Bt8;->a:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0
.end method
