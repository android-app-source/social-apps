.class public LX/BZH;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnoreProperties;
    b = true
.end annotation


# instance fields
.field private A:Ljava/lang/String;

.field private B:LX/8vx;

.field private C:Ljava/lang/String;

.field private D:F

.field private E:Z

.field private F:Z

.field private G:[F

.field private H:[F

.field private I:[F

.field private J:Z

.field private a:Ljava/lang/String;

.field private b:LX/BZL;

.field private c:LX/BZN;

.field private d:Z

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:F

.field private j:Ljava/lang/String;

.field private k:LX/8vx;

.field private l:LX/BZO;

.field private m:Z

.field private n:Z

.field private o:Ljava/lang/Float;

.field private p:I

.field private q:F

.field private r:F

.field private s:Ljava/lang/Float;

.field private t:Ljava/lang/Float;

.field private u:I

.field private v:Ljava/lang/String;

.field private w:Ljava/lang/Float;

.field private x:Ljava/lang/Float;

.field private y:Ljava/lang/String;

.field private z:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1797089
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1797090
    iput-boolean v1, p0, LX/BZH;->d:Z

    .line 1797091
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, LX/BZH;->i:F

    .line 1797092
    iput-boolean v1, p0, LX/BZH;->n:Z

    .line 1797093
    return-void
.end method


# virtual methods
.method public getAnimation()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1797153
    iget-object v0, p0, LX/BZH;->C:Ljava/lang/String;

    return-object v0
.end method

.method public getBackground_influence()F
    .locals 1

    .prologue
    .line 1797152
    iget-object v0, p0, LX/BZH;->x:Ljava/lang/Float;

    if-nez v0, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/BZH;->x:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    goto :goto_0
.end method

.method public getBlend_mode()LX/8vx;
    .locals 1

    .prologue
    .line 1797151
    iget-object v0, p0, LX/BZH;->k:LX/8vx;

    return-object v0
.end method

.method public getBrightness()F
    .locals 1

    .prologue
    .line 1797150
    iget-object v0, p0, LX/BZH;->w:Ljava/lang/Float;

    if-nez v0, :cond_0

    const/high16 v0, 0x3f000000    # 0.5f

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/BZH;->w:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    goto :goto_0
.end method

.method public getChin_size()I
    .locals 1

    .prologue
    .line 1797149
    iget v0, p0, LX/BZH;->p:I

    return v0
.end method

.method public getDisplace_factor()F
    .locals 1

    .prologue
    .line 1797148
    iget v0, p0, LX/BZH;->i:F

    return v0
.end method

.method public getDisplacer()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1797147
    iget-object v0, p0, LX/BZH;->h:Ljava/lang/String;

    return-object v0
.end method

.method public getDisplacer_offsets()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1797146
    iget-object v0, p0, LX/BZH;->g:Ljava/lang/String;

    return-object v0
.end method

.method public getFace_index()I
    .locals 1

    .prologue
    .line 1797135
    iget v0, p0, LX/BZH;->u:I

    return v0
.end method

.method public getGeometry_type()LX/BZO;
    .locals 1

    .prologue
    .line 1797143
    iget-object v0, p0, LX/BZH;->l:LX/BZO;

    return-object v0
.end method

.method public getHigh_poly()Z
    .locals 1

    .prologue
    .line 1797142
    iget-boolean v0, p0, LX/BZH;->d:Z

    return v0
.end method

.method public getImage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1797141
    iget-object v0, p0, LX/BZH;->j:Ljava/lang/String;

    return-object v0
.end method

.method public getIndices()LX/BZN;
    .locals 1

    .prologue
    .line 1797140
    iget-object v0, p0, LX/BZH;->c:LX/BZN;

    return-object v0
.end method

.method public getIs_foreground()Z
    .locals 1

    .prologue
    .line 1797139
    iget-boolean v0, p0, LX/BZH;->J:Z

    return v0
.end method

.method public getIs_occluder()Z
    .locals 1

    .prologue
    .line 1797138
    iget-boolean v0, p0, LX/BZH;->E:Z

    return v0
.end method

.method public getLayer()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1797137
    iget-object v0, p0, LX/BZH;->v:Ljava/lang/String;

    return-object v0
.end method

.method public getLight_image()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1797136
    iget-object v0, p0, LX/BZH;->y:Ljava/lang/String;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1797171
    iget-object v0, p0, LX/BZH;->a:Ljava/lang/String;

    return-object v0
.end method

.method public getNo_depth_buffer()Z
    .locals 1

    .prologue
    .line 1797154
    iget-boolean v0, p0, LX/BZH;->F:Z

    return v0
.end method

.method public getObj()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1797170
    iget-object v0, p0, LX/BZH;->z:Ljava/lang/String;

    return-object v0
.end method

.method public getOffset3d()[F
    .locals 1

    .prologue
    .line 1797169
    iget-object v0, p0, LX/BZH;->H:[F

    return-object v0
.end method

.method public getOffsets()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1797168
    iget-object v0, p0, LX/BZH;->f:Ljava/lang/String;

    return-object v0
.end method

.method public getOpacity()F
    .locals 1

    .prologue
    .line 1797167
    iget-object v0, p0, LX/BZH;->o:Ljava/lang/Float;

    if-nez v0, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/BZH;->o:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    goto :goto_0
.end method

.method public getRender_type()LX/8vx;
    .locals 1

    .prologue
    .line 1797166
    iget-object v0, p0, LX/BZH;->B:LX/8vx;

    return-object v0
.end method

.method public getScale()F
    .locals 1

    .prologue
    .line 1797165
    iget v0, p0, LX/BZH;->D:F

    return v0
.end method

.method public getScale3d()[F
    .locals 1

    .prologue
    .line 1797164
    iget-object v0, p0, LX/BZH;->G:[F

    return-object v0
.end method

.method public getSizeX()F
    .locals 1

    .prologue
    .line 1797172
    iget-object v0, p0, LX/BZH;->s:Ljava/lang/Float;

    if-nez v0, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/BZH;->s:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    goto :goto_0
.end method

.method public getSizeY()F
    .locals 1

    .prologue
    .line 1797163
    iget-object v0, p0, LX/BZH;->t:Ljava/lang/Float;

    if-nez v0, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/BZH;->t:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    goto :goto_0
.end method

.method public getTexture()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1797162
    iget-object v0, p0, LX/BZH;->A:Ljava/lang/String;

    return-object v0
.end method

.method public getTransformMatrix3d()[F
    .locals 1

    .prologue
    .line 1797161
    iget-object v0, p0, LX/BZH;->I:[F

    return-object v0
.end method

.method public getType()LX/BZL;
    .locals 1

    .prologue
    .line 1797160
    iget-object v0, p0, LX/BZH;->b:LX/BZL;

    return-object v0
.end method

.method public getUv()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1797159
    iget-object v0, p0, LX/BZH;->e:Ljava/lang/String;

    return-object v0
.end method

.method public getX()F
    .locals 1

    .prologue
    .line 1797158
    iget v0, p0, LX/BZH;->q:F

    return v0
.end method

.method public getY()F
    .locals 1

    .prologue
    .line 1797157
    iget v0, p0, LX/BZH;->r:F

    return v0
.end method

.method public setAnimation(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1797144
    iput-object p1, p0, LX/BZH;->C:Ljava/lang/String;

    .line 1797145
    return-void
.end method

.method public setBackground_influence(Ljava/lang/Float;)V
    .locals 0

    .prologue
    .line 1797155
    iput-object p1, p0, LX/BZH;->x:Ljava/lang/Float;

    .line 1797156
    return-void
.end method

.method public setBlend_mode(LX/8vx;)V
    .locals 0

    .prologue
    .line 1797057
    iput-object p1, p0, LX/BZH;->k:LX/8vx;

    .line 1797058
    return-void
.end method

.method public setBrightness(Ljava/lang/Float;)V
    .locals 0

    .prologue
    .line 1797087
    iput-object p1, p0, LX/BZH;->w:Ljava/lang/Float;

    .line 1797088
    return-void
.end method

.method public setChin_size(I)V
    .locals 0

    .prologue
    .line 1797085
    iput p1, p0, LX/BZH;->p:I

    .line 1797086
    return-void
.end method

.method public setDisplace_factor(F)V
    .locals 0

    .prologue
    .line 1797083
    iput p1, p0, LX/BZH;->i:F

    .line 1797084
    return-void
.end method

.method public setDisplacer(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1797081
    iput-object p1, p0, LX/BZH;->h:Ljava/lang/String;

    .line 1797082
    return-void
.end method

.method public setDisplacer_offsets(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1797079
    iput-object p1, p0, LX/BZH;->g:Ljava/lang/String;

    .line 1797080
    return-void
.end method

.method public setDouble_sided(Z)V
    .locals 0

    .prologue
    .line 1797077
    iput-boolean p1, p0, LX/BZH;->m:Z

    .line 1797078
    return-void
.end method

.method public setFace_index(I)V
    .locals 0

    .prologue
    .line 1797075
    iput p1, p0, LX/BZH;->u:I

    .line 1797076
    return-void
.end method

.method public setGeometry_type(LX/BZO;)V
    .locals 0

    .prologue
    .line 1797073
    iput-object p1, p0, LX/BZH;->l:LX/BZO;

    .line 1797074
    return-void
.end method

.method public setHigh_poly(Z)V
    .locals 0

    .prologue
    .line 1797071
    iput-boolean p1, p0, LX/BZH;->d:Z

    .line 1797072
    return-void
.end method

.method public setImage(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1797069
    iput-object p1, p0, LX/BZH;->j:Ljava/lang/String;

    .line 1797070
    return-void
.end method

.method public setIndices(LX/BZN;)V
    .locals 0

    .prologue
    .line 1797067
    iput-object p1, p0, LX/BZH;->c:LX/BZN;

    .line 1797068
    return-void
.end method

.method public setIs_foreground(Z)V
    .locals 0

    .prologue
    .line 1797065
    iput-boolean p1, p0, LX/BZH;->J:Z

    .line 1797066
    return-void
.end method

.method public setIs_occluder(Z)V
    .locals 0

    .prologue
    .line 1797063
    iput-boolean p1, p0, LX/BZH;->E:Z

    .line 1797064
    return-void
.end method

.method public setLayer(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1797061
    iput-object p1, p0, LX/BZH;->v:Ljava/lang/String;

    .line 1797062
    return-void
.end method

.method public setLight_image(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1797059
    iput-object p1, p0, LX/BZH;->y:Ljava/lang/String;

    .line 1797060
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1797055
    iput-object p1, p0, LX/BZH;->a:Ljava/lang/String;

    .line 1797056
    return-void
.end method

.method public setNo_depth_buffer(Z)V
    .locals 0

    .prologue
    .line 1797113
    iput-boolean p1, p0, LX/BZH;->F:Z

    .line 1797114
    return-void
.end method

.method public setObj(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1797133
    iput-object p1, p0, LX/BZH;->z:Ljava/lang/String;

    .line 1797134
    return-void
.end method

.method public setOffset3d([F)V
    .locals 2

    .prologue
    .line 1797129
    if-eqz p1, :cond_0

    array-length v0, p1

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 1797130
    iput-object p1, p0, LX/BZH;->H:[F

    .line 1797131
    :goto_0
    return-void

    .line 1797132
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/BZH;->H:[F

    goto :goto_0
.end method

.method public setOffsets(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1797127
    iput-object p1, p0, LX/BZH;->f:Ljava/lang/String;

    .line 1797128
    return-void
.end method

.method public setOpacity(F)V
    .locals 1

    .prologue
    .line 1797125
    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, LX/BZH;->o:Ljava/lang/Float;

    .line 1797126
    return-void
.end method

.method public setRender_type(LX/8vx;)V
    .locals 0

    .prologue
    .line 1797123
    iput-object p1, p0, LX/BZH;->B:LX/8vx;

    .line 1797124
    return-void
.end method

.method public setScale(F)V
    .locals 0

    .prologue
    .line 1797121
    iput p1, p0, LX/BZH;->D:F

    .line 1797122
    return-void
.end method

.method public setScale3d([F)V
    .locals 2

    .prologue
    .line 1797117
    if-eqz p1, :cond_0

    array-length v0, p1

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 1797118
    iput-object p1, p0, LX/BZH;->G:[F

    .line 1797119
    :goto_0
    return-void

    .line 1797120
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/BZH;->G:[F

    goto :goto_0
.end method

.method public setSizeX(F)V
    .locals 1

    .prologue
    .line 1797115
    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, LX/BZH;->s:Ljava/lang/Float;

    .line 1797116
    return-void
.end method

.method public setSizeY(Ljava/lang/Float;)V
    .locals 0

    .prologue
    .line 1797094
    iput-object p1, p0, LX/BZH;->t:Ljava/lang/Float;

    .line 1797095
    return-void
.end method

.method public setSmooth_skin(Z)V
    .locals 0

    .prologue
    .line 1797111
    iput-boolean p1, p0, LX/BZH;->n:Z

    .line 1797112
    return-void
.end method

.method public setTexture(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1797109
    iput-object p1, p0, LX/BZH;->A:Ljava/lang/String;

    .line 1797110
    return-void
.end method

.method public setTransform3d([F)V
    .locals 2

    .prologue
    .line 1797105
    if-eqz p1, :cond_0

    array-length v0, p1

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    .line 1797106
    iput-object p1, p0, LX/BZH;->I:[F

    .line 1797107
    :goto_0
    return-void

    .line 1797108
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/BZH;->I:[F

    goto :goto_0
.end method

.method public setType(LX/BZL;)V
    .locals 0

    .prologue
    .line 1797103
    iput-object p1, p0, LX/BZH;->b:LX/BZL;

    .line 1797104
    return-void
.end method

.method public setUv(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1797101
    iput-object p1, p0, LX/BZH;->e:Ljava/lang/String;

    .line 1797102
    return-void
.end method

.method public setX(F)V
    .locals 0

    .prologue
    .line 1797099
    iput p1, p0, LX/BZH;->q:F

    .line 1797100
    return-void
.end method

.method public setY(F)V
    .locals 0

    .prologue
    .line 1797097
    iput p1, p0, LX/BZH;->r:F

    .line 1797098
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1797096
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Node{mName=\'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/BZH;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\', mType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/BZH;->b:LX/BZL;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mIndices="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/BZH;->c:LX/BZN;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mHigh_poly="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, LX/BZH;->d:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mUv=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/BZH;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\', mOffsets=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/BZH;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\', mImage=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/BZH;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\', mBlend_mode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/BZH;->k:LX/8vx;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mGeometry_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/BZH;->l:LX/BZO;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mDouble_sided="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, LX/BZH;->m:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mSmooth_skin="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, LX/BZH;->n:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mDisplace_factor"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LX/BZH;->i:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mOpacity="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/BZH;->o:Ljava/lang/Float;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mChin_Size="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LX/BZH;->p:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", x="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LX/BZH;->q:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", y="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LX/BZH;->r:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", sizeX="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/BZH;->s:Ljava/lang/Float;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", sizeY="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/BZH;->t:Ljava/lang/Float;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mFace_index="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LX/BZH;->u:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mLayer=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/BZH;->v:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\', mBrightness="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/BZH;->w:Ljava/lang/Float;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mBackgroundInfluence="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/BZH;->x:Ljava/lang/Float;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mLightImage=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/BZH;->y:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\', mObj=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/BZH;->z:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\', mTexture=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/BZH;->A:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\', mRender_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/BZH;->B:LX/8vx;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mAnimation=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/BZH;->C:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\', mScale="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LX/BZH;->D:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mIsOccluder="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, LX/BZH;->E:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mNoDepthBuffer="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, LX/BZH;->F:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mScale3D="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/BZH;->G:[F

    invoke-static {v1}, Ljava/util/Arrays;->toString([F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mOffset3D="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/BZH;->H:[F

    invoke-static {v1}, Ljava/util/Arrays;->toString([F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mTransformMatrix3D="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/BZH;->I:[F

    invoke-static {v1}, Ljava/util/Arrays;->toString([F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mIsForeground="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, LX/BZH;->J:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
