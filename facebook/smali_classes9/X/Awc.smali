.class public LX/Awc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/89u;


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:LX/AsK;

.field public final c:LX/AsL;

.field public final d:LX/AwZ;

.field public final e:LX/Arh;

.field public final f:LX/03V;

.field private final g:LX/86l;

.field public final h:Landroid/content/Context;

.field public i:Lcom/facebook/friendsharing/inspiration/model/InspirationModel;

.field public j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field public l:Ljava/io/File;

.field public m:Ljava/io/File;

.field private n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private p:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Landroid/net/Uri;

.field private r:Z

.field public s:Z

.field public t:Z

.field private u:Lcom/facebook/photos/creativeediting/model/SwipeableParams;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1726828
    const-class v0, LX/Awc;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Awc;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/Arh;LX/AsK;LX/AsL;LX/AwZ;LX/03V;LX/86l;Landroid/content/Context;)V
    .locals 2
    .param p1    # LX/Arh;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/AsK;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/AsL;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1726878
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1726879
    const/4 v0, 0x0

    iput-object v0, p0, LX/Awc;->q:Landroid/net/Uri;

    .line 1726880
    iput-boolean v1, p0, LX/Awc;->r:Z

    .line 1726881
    iput-boolean v1, p0, LX/Awc;->s:Z

    .line 1726882
    iput-boolean v1, p0, LX/Awc;->t:Z

    .line 1726883
    iput-object p1, p0, LX/Awc;->e:LX/Arh;

    .line 1726884
    iput-object p2, p0, LX/Awc;->b:LX/AsK;

    .line 1726885
    iput-object p3, p0, LX/Awc;->c:LX/AsL;

    .line 1726886
    iput-object p4, p0, LX/Awc;->d:LX/AwZ;

    .line 1726887
    iput-object p5, p0, LX/Awc;->f:LX/03V;

    .line 1726888
    iput-object p6, p0, LX/Awc;->g:LX/86l;

    .line 1726889
    iput-object p7, p0, LX/Awc;->h:Landroid/content/Context;

    .line 1726890
    return-void
.end method

.method public static b$redex0(LX/Awc;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1726874
    iget-object v0, p0, LX/Awc;->c:LX/AsL;

    invoke-virtual {v0}, LX/AsL;->d()V

    .line 1726875
    iget-object v0, p0, LX/Awc;->f:LX/03V;

    sget-object v1, LX/Awc;->a:Ljava/lang/String;

    invoke-static {v1, p1}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v1

    invoke-virtual {v1}, LX/0VK;->g()LX/0VG;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/03V;->a(LX/0VG;)V

    .line 1726876
    new-instance v0, Lcom/facebook/friendsharing/inspiration/styletransfer/InspirationStyleTransferFormatController$3;

    invoke-direct {v0, p0}, Lcom/facebook/friendsharing/inspiration/styletransfer/InspirationStyleTransferFormatController$3;-><init>(LX/Awc;)V

    invoke-static {v0}, LX/5fo;->a(Ljava/lang/Runnable;)V

    .line 1726877
    return-void
.end method

.method public static i(LX/Awc;)V
    .locals 15

    .prologue
    .line 1726850
    iget-object v0, p0, LX/Awc;->b:LX/AsK;

    .line 1726851
    iget-object v1, v0, LX/AsK;->a:LX/AsM;

    iget-object v1, v1, LX/AsM;->l:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0il;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    invoke-static {v1}, LX/87R;->a(LX/0io;)Z

    move-result v1

    move v0, v1

    .line 1726852
    if-eqz v0, :cond_0

    .line 1726853
    iget-object v1, p0, LX/Awc;->d:LX/AwZ;

    iget-object v2, p0, LX/Awc;->b:LX/AsK;

    .line 1726854
    iget-object v3, v2, LX/AsK;->a:LX/AsM;

    iget-object v3, v3, LX/AsM;->l:Ljava/lang/ref/WeakReference;

    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0il;

    invoke-interface {v3}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0io;

    .line 1726855
    invoke-static {v3}, LX/2rf;->b(LX/0io;)Lcom/facebook/composer/attachments/ComposerAttachment;

    move-result-object v2

    .line 1726856
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/facebook/composer/attachments/ComposerAttachment;->c()Landroid/net/Uri;

    move-result-object v2

    :goto_0
    move-object v3, v2

    .line 1726857
    move-object v2, v3

    .line 1726858
    iget-object v3, p0, LX/Awc;->b:LX/AsK;

    .line 1726859
    iget-object v4, v3, LX/AsK;->a:LX/AsM;

    iget-object v4, v4, LX/AsM;->l:Ljava/lang/ref/WeakReference;

    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0il;

    invoke-interface {v4}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0io;

    invoke-static {v4}, LX/2rf;->b(LX/0io;)Lcom/facebook/composer/attachments/ComposerAttachment;

    move-result-object v4

    .line 1726860
    invoke-virtual {v4}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v3

    .line 1726861
    iget v4, v3, Lcom/facebook/ipc/media/data/MediaData;->mOrientation:I

    move v3, v4

    .line 1726862
    move v4, v3

    .line 1726863
    move v3, v4

    .line 1726864
    iget-object v4, p0, LX/Awc;->l:Ljava/io/File;

    iget-object v5, p0, LX/Awc;->m:Ljava/io/File;

    new-instance v6, LX/Awb;

    invoke-direct {v6, p0}, LX/Awb;-><init>(LX/Awc;)V

    .line 1726865
    iget-object v7, v1, LX/AwZ;->e:Ljava/util/concurrent/Future;

    if-eqz v7, :cond_2

    iget-object v7, v1, LX/AwZ;->e:Ljava/util/concurrent/Future;

    invoke-interface {v7}, Ljava/util/concurrent/Future;->isCancelled()Z

    move-result v7

    if-nez v7, :cond_2

    iget-object v7, v1, LX/AwZ;->e:Ljava/util/concurrent/Future;

    invoke-interface {v7}, Ljava/util/concurrent/Future;->isDone()Z

    move-result v7

    if-nez v7, :cond_2

    .line 1726866
    :goto_1
    return-void

    .line 1726867
    :cond_0
    iget-object v0, p0, LX/Awc;->i:Lcom/facebook/friendsharing/inspiration/model/InspirationModel;

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->getStyleTransfer()Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;

    move-result-object v0

    if-nez v0, :cond_3

    .line 1726868
    iget-object v0, p0, LX/Awc;->f:LX/03V;

    sget-object v1, LX/Awc;->a:Ljava/lang/String;

    const-string v2, "Trying to apply a non style transfer inspiration"

    invoke-static {v1, v2}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v1

    invoke-virtual {v1}, LX/0VK;->g()LX/0VG;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/03V;->a(LX/0VG;)V

    .line 1726869
    :goto_2
    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    .line 1726870
    :cond_2
    iget-object v14, v1, LX/AwZ;->a:Ljava/util/concurrent/ExecutorService;

    new-instance v7, Lcom/facebook/friendsharing/inspiration/styletransfer/InspirationStyleTransferApplier$2;

    move-object v8, v1

    move-object v9, v2

    move v10, v3

    move-object v11, v4

    move-object v12, v5

    move-object v13, v6

    invoke-direct/range {v7 .. v13}, Lcom/facebook/friendsharing/inspiration/styletransfer/InspirationStyleTransferApplier$2;-><init>(LX/AwZ;Landroid/net/Uri;ILjava/io/File;Ljava/io/File;LX/Awb;)V

    const v8, 0x61b68055

    invoke-static {v14, v7, v8}, LX/03X;->a(Ljava/util/concurrent/ExecutorService;Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    move-result-object v7

    iput-object v7, v1, LX/AwZ;->e:Ljava/util/concurrent/Future;

    goto :goto_1

    .line 1726871
    :cond_3
    iget-object v0, p0, LX/Awc;->i:Lcom/facebook/friendsharing/inspiration/model/InspirationModel;

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->getStyleTransfer()Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;->getStyleTransferModel()Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;->a(Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel;)Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig$Builder;

    move-result-object v0

    iget-object v1, p0, LX/Awc;->l:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig$Builder;->setInitResPath(Ljava/lang/String;)Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig$Builder;

    move-result-object v0

    iget-object v1, p0, LX/Awc;->m:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig$Builder;->setInitPredictPath(Ljava/lang/String;)Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig$Builder;->a()Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;

    move-result-object v0

    .line 1726872
    iget-object v1, p0, LX/Awc;->e:LX/Arh;

    invoke-virtual {v1, v0}, LX/Arh;->a(Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;)V

    .line 1726873
    iget-object v0, p0, LX/Awc;->c:LX/AsL;

    iget-object v1, p0, LX/Awc;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/AsL;->a(Ljava/lang/String;)V

    goto :goto_2
.end method


# virtual methods
.method public final a()LX/86d;
    .locals 1

    .prologue
    .line 1726849
    iget-object v0, p0, LX/Awc;->g:LX/86l;

    return-object v0
.end method

.method public final a(Lcom/facebook/friendsharing/inspiration/model/InspirationModel;)V
    .locals 5

    .prologue
    .line 1726830
    iput-object p1, p0, LX/Awc;->i:Lcom/facebook/friendsharing/inspiration/model/InspirationModel;

    .line 1726831
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->getId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/Awc;->j:Ljava/lang/String;

    .line 1726832
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->getStyleTransfer()Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;

    move-result-object v0

    .line 1726833
    invoke-virtual {v0}, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;->getStyleTransferModel()Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel;->r()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, LX/Awc;->k:Ljava/lang/String;

    .line 1726834
    invoke-virtual {v0}, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;->getStyleTransferModel()Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel;

    move-result-object v1

    .line 1726835
    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel;->n()LX/1vs;

    move-result-object v2

    iget v2, v2, LX/1vs;->b:I

    if-eqz v2, :cond_0

    .line 1726836
    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel;->n()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 1726837
    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    :goto_0
    move-object v1, v1

    .line 1726838
    iput-object v1, p0, LX/Awc;->n:Ljava/lang/String;

    .line 1726839
    invoke-virtual {v0}, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;->getStyleTransferModel()Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel;

    move-result-object v1

    .line 1726840
    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel;->j()Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel$AttributionTextModel;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel;->j()Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel$AttributionTextModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel$AttributionTextModel;->a()Ljava/lang/String;

    move-result-object v1

    :goto_1
    move-object v1, v1

    .line 1726841
    iput-object v1, p0, LX/Awc;->o:Ljava/lang/String;

    .line 1726842
    invoke-virtual {v0}, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;->getStyleTransferModel()Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel;

    move-result-object v1

    .line 1726843
    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel;->k()Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel$AttributionThumbnailModel;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel;->k()Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel$AttributionThumbnailModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel$AttributionThumbnailModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    :goto_2
    move-object v1, v1

    .line 1726844
    iput-object v1, p0, LX/Awc;->p:Landroid/net/Uri;

    .line 1726845
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/Awc;->s:Z

    .line 1726846
    iget-object v1, p0, LX/Awc;->d:LX/AwZ;

    iget-object v2, p0, LX/Awc;->k:Ljava/lang/String;

    invoke-static {v0}, LX/63w;->a(Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0}, LX/63w;->b(Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;)Ljava/lang/String;

    move-result-object v0

    new-instance v4, LX/Awa;

    invoke-direct {v4, p0}, LX/Awa;-><init>(LX/Awc;)V

    .line 1726847
    iget-object p0, v1, LX/AwZ;->b:Lcom/facebook/friendsharing/inspiration/styletransfer/StyleTransferAssetDownloader;

    new-instance p1, LX/AwY;

    invoke-direct {p1, v1, v4}, LX/AwY;-><init>(LX/AwZ;LX/Awa;)V

    invoke-virtual {p0, v2, v3, v0, p1}, Lcom/facebook/friendsharing/inspiration/styletransfer/StyleTransferAssetDownloader;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/AwX;)V

    .line 1726848
    return-void

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    goto :goto_2
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 1726829
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 1726891
    iget-object v0, p0, LX/Awc;->d:LX/AwZ;

    .line 1726892
    iget-object v1, v0, LX/AwZ;->e:Ljava/util/concurrent/Future;

    if-eqz v1, :cond_0

    .line 1726893
    iget-object v1, v0, LX/AwZ;->e:Ljava/util/concurrent/Future;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 1726894
    :cond_0
    iget-object v0, p0, LX/Awc;->e:LX/Arh;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/Arh;->a(Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;)V

    .line 1726895
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Awc;->r:Z

    .line 1726896
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1726811
    const/4 v0, 0x0

    iput-object v0, p0, LX/Awc;->q:Landroid/net/Uri;

    .line 1726812
    iput-boolean v1, p0, LX/Awc;->r:Z

    .line 1726813
    iget-boolean v0, p0, LX/Awc;->s:Z

    if-eqz v0, :cond_0

    .line 1726814
    iput-boolean v1, p0, LX/Awc;->t:Z

    .line 1726815
    iget-object v0, p0, LX/Awc;->c:LX/AsL;

    invoke-virtual {v0}, LX/AsL;->b()V

    .line 1726816
    :goto_0
    return-void

    .line 1726817
    :cond_0
    iget-object v0, p0, LX/Awc;->l:Ljava/io/File;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/Awc;->m:Ljava/io/File;

    if-nez v0, :cond_2

    .line 1726818
    :cond_1
    const-string v0, "Failed to load models"

    invoke-static {p0, v0}, LX/Awc;->b$redex0(LX/Awc;Ljava/lang/String;)V

    goto :goto_0

    .line 1726819
    :cond_2
    iget-object v0, p0, LX/Awc;->c:LX/AsL;

    invoke-virtual {v0}, LX/AsL;->b()V

    .line 1726820
    invoke-static {p0}, LX/Awc;->i(LX/Awc;)V

    goto :goto_0
.end method

.method public final d()Lcom/facebook/photos/creativeediting/model/SwipeableParams;
    .locals 1

    .prologue
    .line 1726821
    iget-object v0, p0, LX/Awc;->u:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    if-nez v0, :cond_0

    .line 1726822
    iget-object v0, p0, LX/Awc;->j:Ljava/lang/String;

    invoke-static {v0}, LX/8GN;->a(Ljava/lang/String;)Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    move-result-object v0

    iput-object v0, p0, LX/Awc;->u:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    .line 1726823
    :cond_0
    iget-object v0, p0, LX/Awc;->u:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    return-object v0
.end method

.method public final e()Landroid/net/Uri;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1726824
    iget-boolean v0, p0, LX/Awc;->r:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Awc;->q:Landroid/net/Uri;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1726825
    iget-object v0, p0, LX/Awc;->n:Ljava/lang/String;

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1726826
    iget-object v0, p0, LX/Awc;->o:Ljava/lang/String;

    return-object v0
.end method

.method public final h()Landroid/net/Uri;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1726827
    iget-object v0, p0, LX/Awc;->p:Landroid/net/Uri;

    return-object v0
.end method
