.class public LX/BfJ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static volatile f:LX/BfJ;


# instance fields
.field public final b:LX/03V;

.field private final c:Lcom/facebook/content/SecureContextHelper;

.field public d:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "LX/BeD;",
            "LX/BfC;",
            ">;"
        }
    .end annotation
.end field

.field public e:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1805893
    const-class v0, LX/BfJ;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/BfJ;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/03V;Ljava/util/Set;Lcom/facebook/content/SecureContextHelper;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "Ljava/util/Set",
            "<",
            "LX/BfC;",
            ">;",
            "Lcom/facebook/content/SecureContextHelper;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1805864
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1805865
    iput-object p1, p0, LX/BfJ;->b:LX/03V;

    .line 1805866
    iput-object p3, p0, LX/BfJ;->c:Lcom/facebook/content/SecureContextHelper;

    .line 1805867
    new-instance v1, LX/0P2;

    invoke-direct {v1}, LX/0P2;-><init>()V

    .line 1805868
    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BfC;

    .line 1805869
    invoke-interface {v0}, LX/BfC;->a()LX/BeD;

    move-result-object p3

    invoke-virtual {v1, p3, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    goto :goto_0

    .line 1805870
    :cond_0
    invoke-virtual {v1}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    iput-object v0, p0, LX/BfJ;->d:LX/0P1;

    .line 1805871
    return-void
.end method

.method public static a(LX/0QB;)LX/BfJ;
    .locals 7

    .prologue
    .line 1805872
    sget-object v0, LX/BfJ;->f:LX/BfJ;

    if-nez v0, :cond_1

    .line 1805873
    const-class v1, LX/BfJ;

    monitor-enter v1

    .line 1805874
    :try_start_0
    sget-object v0, LX/BfJ;->f:LX/BfJ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1805875
    if-eqz v2, :cond_0

    .line 1805876
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1805877
    new-instance v5, LX/BfJ;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    .line 1805878
    new-instance v4, LX/0U8;

    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v6

    new-instance p0, LX/BfH;

    invoke-direct {p0, v0}, LX/BfH;-><init>(LX/0QB;)V

    invoke-direct {v4, v6, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v6, v4

    .line 1805879
    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v4

    check-cast v4, Lcom/facebook/content/SecureContextHelper;

    invoke-direct {v5, v3, v6, v4}, LX/BfJ;-><init>(LX/03V;Ljava/util/Set;Lcom/facebook/content/SecureContextHelper;)V

    .line 1805880
    move-object v0, v5

    .line 1805881
    sput-object v0, LX/BfJ;->f:LX/BfJ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1805882
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1805883
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1805884
    :cond_1
    sget-object v0, LX/BfJ;->f:LX/BfJ;

    return-object v0

    .line 1805885
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1805886
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/BeD;LX/BgS;Landroid/support/v4/app/Fragment;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "LX/BeD;",
            "LX/BgS;",
            "Landroid/support/v4/app/Fragment;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1805887
    iget-object v0, p0, LX/BfJ;->d:LX/0P1;

    invoke-virtual {v0, p2}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1805888
    iget-object v0, p0, LX/BfJ;->b:LX/03V;

    sget-object v1, LX/BfJ;->a:Ljava/lang/String;

    const-string v2, "No intent found in map for picker"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1805889
    :goto_0
    return-void

    .line 1805890
    :cond_0
    invoke-interface {p3}, LX/BgS;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/BfJ;->e:Ljava/lang/String;

    .line 1805891
    iget-object v0, p0, LX/BfJ;->d:LX/0P1;

    invoke-virtual {v0, p2}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BfC;

    invoke-interface {v0, p1, p4}, LX/BfC;->a(Ljava/lang/Object;Landroid/support/v4/app/Fragment;)Landroid/content/Intent;

    move-result-object v0

    .line 1805892
    iget-object v1, p0, LX/BfJ;->c:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p2}, LX/BeD;->ordinal()I

    move-result v2

    invoke-interface {v1, v0, v2, p4}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    goto :goto_0
.end method
