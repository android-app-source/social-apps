.class public LX/Atd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0iK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "LX/0io;",
        "DerivedData:",
        "Ljava/lang/Object;",
        "Mutation::",
        "Lcom/facebook/ipc/composer/dataaccessor/ComposerCanSave;",
        ":",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBoundsSpec$SetsInspirationPreviewBounds",
        "<TMutation;>;Services::",
        "LX/0il",
        "<TModelData;>;:",
        "LX/0ik",
        "<TDerivedData;>;:",
        "LX/0im",
        "<TMutation;>;>",
        "Ljava/lang/Object;",
        "LX/0iK",
        "<TModelData;TDerivedData;>;"
    }
.end annotation


# static fields
.field public static final a:LX/0jK;


# instance fields
.field public b:LX/0Sh;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public final c:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TServices;>;"
        }
    .end annotation
.end field

.field private final d:Lcom/facebook/friendsharing/inspiration/capture/cache/InspirationPhotoCaptureCache;

.field public final e:Landroid/widget/ImageView;

.field private final f:LX/Atc;

.field private final g:LX/1o9;

.field private h:LX/1FJ;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;"
        }
    .end annotation
.end field

.field public i:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1721606
    const-class v0, LX/Atd;

    invoke-static {v0}, LX/0jK;->a(Ljava/lang/Class;)LX/0jK;

    move-result-object v0

    sput-object v0, LX/Atd;->a:LX/0jK;

    return-void
.end method

.method public constructor <init>(LX/0il;Landroid/view/ViewStub;LX/0hB;Lcom/facebook/friendsharing/inspiration/capture/cache/InspirationPhotoCaptureCache;)V
    .locals 2
    .param p1    # LX/0il;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Landroid/view/ViewStub;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TServices;",
            "Landroid/view/ViewStub;",
            "LX/0hB;",
            "Lcom/facebook/friendsharing/inspiration/capture/cache/InspirationPhotoCaptureCache;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1721607
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1721608
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/Atd;->c:Ljava/lang/ref/WeakReference;

    .line 1721609
    invoke-virtual {p2}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/Atd;->e:Landroid/widget/ImageView;

    .line 1721610
    iput-object p4, p0, LX/Atd;->d:Lcom/facebook/friendsharing/inspiration/capture/cache/InspirationPhotoCaptureCache;

    .line 1721611
    iget-object v0, p0, LX/Atd;->e:Landroid/widget/ImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 1721612
    new-instance v0, LX/Atc;

    invoke-direct {v0, p0}, LX/Atc;-><init>(LX/Atd;)V

    move-object v0, v0

    .line 1721613
    iput-object v0, p0, LX/Atd;->f:LX/Atc;

    .line 1721614
    new-instance v0, LX/1o9;

    invoke-virtual {p3}, LX/0hB;->f()I

    move-result v1

    invoke-virtual {p3}, LX/0hB;->g()I

    move-result p2

    invoke-direct {v0, v1, p2}, LX/1o9;-><init>(II)V

    move-object v0, v0

    .line 1721615
    iput-object v0, p0, LX/Atd;->g:LX/1o9;

    .line 1721616
    invoke-interface {p1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    invoke-static {v0}, LX/2rf;->e(LX/0io;)Landroid/net/Uri;

    move-result-object v0

    .line 1721617
    if-eqz v0, :cond_0

    .line 1721618
    invoke-direct {p0, v0}, LX/Atd;->a(Landroid/net/Uri;)V

    .line 1721619
    :cond_0
    return-void
.end method

.method private a(Landroid/net/Uri;)V
    .locals 4

    .prologue
    .line 1721620
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Atd;->i:Z

    .line 1721621
    invoke-static {p1}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v0

    invoke-static {}, LX/1bd;->a()LX/1bd;

    move-result-object v1

    .line 1721622
    iput-object v1, v0, LX/1bX;->d:LX/1bd;

    .line 1721623
    move-object v0, v0

    .line 1721624
    iget-object v1, p0, LX/Atd;->g:LX/1o9;

    .line 1721625
    iput-object v1, v0, LX/1bX;->c:LX/1o9;

    .line 1721626
    move-object v0, v0

    .line 1721627
    invoke-virtual {v0}, LX/1bX;->n()LX/1bf;

    move-result-object v0

    .line 1721628
    iget-object v1, p0, LX/Atd;->d:Lcom/facebook/friendsharing/inspiration/capture/cache/InspirationPhotoCaptureCache;

    iget-object v2, p0, LX/Atd;->f:LX/Atc;

    .line 1721629
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1721630
    sget-object v3, Lcom/facebook/friendsharing/inspiration/capture/cache/InspirationPhotoCaptureCache;->a:Landroid/net/Uri;

    .line 1721631
    iget-object p0, v0, LX/1bf;->b:Landroid/net/Uri;

    move-object p0, p0

    .line 1721632
    invoke-static {v3, p0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1721633
    iget-object v3, v1, Lcom/facebook/friendsharing/inspiration/capture/cache/InspirationPhotoCaptureCache;->f:LX/1FJ;

    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1721634
    iget-object v3, v1, Lcom/facebook/friendsharing/inspiration/capture/cache/InspirationPhotoCaptureCache;->f:LX/1FJ;

    invoke-virtual {v3}, LX/1FJ;->d()Z

    move-result v3

    invoke-static {v3}, LX/0PB;->checkState(Z)V

    .line 1721635
    iget-object v3, v1, Lcom/facebook/friendsharing/inspiration/capture/cache/InspirationPhotoCaptureCache;->f:LX/1FJ;

    invoke-virtual {v2, v3}, LX/Atc;->a(LX/1FJ;)V

    .line 1721636
    :goto_0
    return-void

    .line 1721637
    :cond_0
    iget-object v3, v1, Lcom/facebook/friendsharing/inspiration/capture/cache/InspirationPhotoCaptureCache;->c:LX/1HI;

    sget-object p0, Lcom/facebook/friendsharing/inspiration/capture/cache/InspirationPhotoCaptureCache;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v3, v0, p0}, LX/1HI;->b(LX/1bf;Ljava/lang/Object;)LX/1ca;

    move-result-object v3

    .line 1721638
    new-instance p0, LX/Arv;

    invoke-direct {p0, v1, v2}, LX/Arv;-><init>(Lcom/facebook/friendsharing/inspiration/capture/cache/InspirationPhotoCaptureCache;LX/Atc;)V

    iget-object p1, v1, Lcom/facebook/friendsharing/inspiration/capture/cache/InspirationPhotoCaptureCache;->d:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v3, p0, p1}, LX/1ca;->a(LX/1cj;Ljava/util/concurrent/Executor;)V

    .line 1721639
    goto :goto_0
.end method

.method public static b(LX/Atd;LX/1FJ;)V
    .locals 2
    .param p0    # LX/Atd;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1721640
    iget-object v0, p0, LX/Atd;->h:LX/1FJ;

    if-eqz v0, :cond_0

    .line 1721641
    iget-object v0, p0, LX/Atd;->h:LX/1FJ;

    invoke-virtual {v0}, LX/1FJ;->close()V

    .line 1721642
    :cond_0
    if-eqz p1, :cond_1

    .line 1721643
    iget-object v0, p0, LX/Atd;->e:Landroid/widget/ImageView;

    invoke-static {p1}, LX/Atd;->c(LX/1FJ;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1721644
    invoke-virtual {p1}, LX/1FJ;->b()LX/1FJ;

    move-result-object v0

    iput-object v0, p0, LX/Atd;->h:LX/1FJ;

    .line 1721645
    :goto_0
    return-void

    .line 1721646
    :cond_1
    iget-object v0, p0, LX/Atd;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1721647
    iput-object v1, p0, LX/Atd;->h:LX/1FJ;

    goto :goto_0
.end method

.method public static c(LX/1FJ;)Landroid/graphics/Bitmap;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;)",
            "Landroid/graphics/Bitmap;"
        }
    .end annotation

    .prologue
    .line 1721648
    invoke-virtual {p0}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ln;

    .line 1721649
    instance-of v1, v0, LX/1lm;

    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 1721650
    check-cast v0, LX/1lm;

    invoke-virtual {v0}, LX/1lm;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/5L2;)V
    .locals 0

    .prologue
    .line 1721651
    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 1721652
    check-cast p1, LX/0io;

    .line 1721653
    iget-object v0, p0, LX/Atd;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    .line 1721654
    invoke-static {p1, v0}, LX/87R;->b(LX/0io;LX/0io;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1721655
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Atd;->i:Z

    .line 1721656
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/Atd;->b(LX/Atd;LX/1FJ;)V

    .line 1721657
    iget-object v0, p0, LX/Atd;->e:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1721658
    :cond_0
    :goto_0
    return-void

    .line 1721659
    :cond_1
    invoke-interface {v0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v1

    invoke-static {v1}, LX/7kq;->l(LX/0Px;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1721660
    invoke-static {p1}, LX/2rf;->e(LX/0io;)Landroid/net/Uri;

    move-result-object v1

    .line 1721661
    invoke-static {v0}, LX/2rf;->e(LX/0io;)Landroid/net/Uri;

    move-result-object v0

    .line 1721662
    invoke-static {v1, v0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1721663
    invoke-direct {p0, v0}, LX/Atd;->a(Landroid/net/Uri;)V

    goto :goto_0
.end method
