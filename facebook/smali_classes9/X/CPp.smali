.class public final LX/CPp;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/CPp;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/CPn;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/CPq;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1884926
    const/4 v0, 0x0

    sput-object v0, LX/CPp;->a:LX/CPp;

    .line 1884927
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/CPp;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1884928
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1884929
    new-instance v0, LX/CPq;

    invoke-direct {v0}, LX/CPq;-><init>()V

    iput-object v0, p0, LX/CPp;->c:LX/CPq;

    .line 1884930
    return-void
.end method

.method public static declared-synchronized q()LX/CPp;
    .locals 2

    .prologue
    .line 1884931
    const-class v1, LX/CPp;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/CPp;->a:LX/CPp;

    if-nez v0, :cond_0

    .line 1884932
    new-instance v0, LX/CPp;

    invoke-direct {v0}, LX/CPp;-><init>()V

    sput-object v0, LX/CPp;->a:LX/CPp;

    .line 1884933
    :cond_0
    sget-object v0, LX/CPp;->a:LX/CPp;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 1884934
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 1884935
    check-cast p2, LX/CPo;

    .line 1884936
    iget-object v0, p2, LX/CPo;->a:LX/CNb;

    iget-object v1, p2, LX/CPo;->b:LX/CNc;

    iget-object v2, p2, LX/CPo;->c:Ljava/util/List;

    .line 1884937
    const-string v3, "font-size"

    const/high16 v4, 0x41500000    # 13.0f

    invoke-virtual {v0, v3, v4}, LX/CNb;->a(Ljava/lang/String;F)F

    move-result v3

    .line 1884938
    const-string v4, "line-height"

    invoke-virtual {v0, v4, v3}, LX/CNb;->a(Ljava/lang/String;F)F

    move-result v4

    .line 1884939
    div-float v5, v4, v3

    move v4, v5

    .line 1884940
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v5

    const-string p0, "text"

    const-string p2, ""

    invoke-virtual {v0, p0, p2}, LX/CNb;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v5, p0}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, v3}, LX/1ne;->g(F)LX/1ne;

    move-result-object v3

    const-string v5, "color"

    const/high16 p0, -0x1000000

    invoke-virtual {v0, v5, p0}, LX/CNb;->b(Ljava/lang/String;I)I

    move-result v5

    invoke-virtual {v3, v5}, LX/1ne;->m(I)LX/1ne;

    move-result-object v3

    const-string v5, "horizontal-alignment"

    const-string p0, ""

    invoke-virtual {v0, v5, p0}, LX/CNb;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/CPv;->b(Ljava/lang/String;)Landroid/text/Layout$Alignment;

    move-result-object v5

    invoke-virtual {v3, v5}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v3

    const-string v5, "max-lines"

    const p0, 0x7fffffff

    invoke-virtual {v0, v5, p0}, LX/CNb;->a(Ljava/lang/String;I)I

    move-result v5

    invoke-virtual {v3, v5}, LX/1ne;->j(I)LX/1ne;

    move-result-object v3

    const-string v5, "truncation-mode"

    const-string p0, ""

    invoke-virtual {v0, v5, p0}, LX/CNb;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/CPv;->a(Ljava/lang/String;)Landroid/text/TextUtils$TruncateAt;

    move-result-object v5

    invoke-virtual {v3, v5}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, v4}, LX/1ne;->j(F)LX/1ne;

    move-result-object v3

    .line 1884941
    const-string v4, "font-family"

    const-string v5, ""

    invoke-virtual {v0, v4, v5}, LX/CNb;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 1884942
    const-string v4, "font-weight"

    invoke-static {v0, v4}, LX/CPv;->a(LX/CNb;Ljava/lang/String;)I

    move-result v4

    .line 1884943
    const-string v5, "font-family"

    const-string p0, ""

    invoke-virtual {v0, v5, p0}, LX/CNb;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1884944
    invoke-static {v5, v4}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v4

    .line 1884945
    invoke-virtual {v3, v4}, LX/1ne;->a(Landroid/graphics/Typeface;)LX/1ne;

    .line 1884946
    :cond_0
    :goto_0
    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    .line 1884947
    const-string v4, "touch-up-inside-actions"

    invoke-static {v1, v0, v2, v4}, LX/CPx;->a(LX/CNc;LX/CNb;Ljava/util/List;Ljava/lang/String;)LX/CNe;

    move-result-object v4

    .line 1884948
    if-eqz v4, :cond_1

    .line 1884949
    const v5, 0x5ca2c97b

    const/4 p0, 0x1

    new-array p0, p0, [Ljava/lang/Object;

    const/4 p2, 0x0

    aput-object v4, p0, p2

    invoke-static {p1, v5, p0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v5

    move-object v4, v5

    .line 1884950
    invoke-interface {v3, v4}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    .line 1884951
    :cond_1
    const-string v4, "long-press-actions"

    invoke-static {v1, v0, v2, v4}, LX/CPx;->a(LX/CNc;LX/CNb;Ljava/util/List;Ljava/lang/String;)LX/CNe;

    move-result-object v4

    .line 1884952
    if-eqz v4, :cond_2

    .line 1884953
    const v5, 0x41aa5418

    const/4 p0, 0x1

    new-array p0, p0, [Ljava/lang/Object;

    const/4 p2, 0x0

    aput-object v4, p0, p2

    invoke-static {p1, v5, p0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v5

    move-object v4, v5

    .line 1884954
    invoke-interface {v3, v4}, LX/1Di;->b(LX/1dQ;)LX/1Di;

    .line 1884955
    :cond_2
    invoke-static {v3, p1, v0, v2}, LX/CPx;->a(LX/1Di;LX/1De;LX/CNb;Ljava/util/List;)LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 1884956
    return-object v0

    .line 1884957
    :cond_3
    const-string v4, "font-weight"

    invoke-virtual {v0, v4}, LX/CNb;->c(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1884958
    const-string v4, "font-weight"

    invoke-static {v0, v4}, LX/CPv;->a(LX/CNb;Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v3, v4}, LX/1ne;->t(I)LX/1ne;

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 1884959
    invoke-static {}, LX/1dS;->b()V

    .line 1884960
    iget v0, p1, LX/1dQ;->b:I

    .line 1884961
    sparse-switch v0, :sswitch_data_0

    move-object v0, v1

    .line 1884962
    :goto_0
    return-object v0

    .line 1884963
    :sswitch_0
    iget-object v0, p1, LX/1dQ;->c:[Ljava/lang/Object;

    aget-object v0, v0, v2

    check-cast v0, LX/CNe;

    .line 1884964
    invoke-interface {v0}, LX/CNe;->a()V

    .line 1884965
    move-object v0, v1

    .line 1884966
    goto :goto_0

    .line 1884967
    :sswitch_1
    iget-object v0, p1, LX/1dQ;->c:[Ljava/lang/Object;

    aget-object v0, v0, v2

    check-cast v0, LX/CNe;

    .line 1884968
    invoke-interface {v0}, LX/CNe;->a()V

    .line 1884969
    const/4 v2, 0x1

    move v2, v2

    .line 1884970
    move v0, v2

    .line 1884971
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x41aa5418 -> :sswitch_1
        0x5ca2c97b -> :sswitch_0
    .end sparse-switch
.end method
