.class public LX/BVQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/61B;
.implements LX/6Jv;


# static fields
.field private static final a:[LX/7Sc;


# instance fields
.field public b:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/UrgentSingleThreadExecutorService;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:Landroid/content/Context;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public final d:Ljava/util/concurrent/locks/Lock;

.field public final e:Ljava/util/concurrent/locks/ReadWriteLock;

.field public f:LX/AMG;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mProcessLock"
    .end annotation
.end field

.field public volatile g:LX/AMF;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mRenderLock"
    .end annotation
.end field

.field public volatile h:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mRenderLock"
    .end annotation
.end field

.field private i:LX/5Pf;

.field private j:LX/5Pb;

.field private k:LX/5PR;

.field private l:LX/6Js;

.field public m:I

.field public n:I

.field public final o:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final p:Ljava/lang/Runnable;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1790962
    const/4 v0, 0x2

    new-array v0, v0, [LX/7Sc;

    const/4 v1, 0x0

    sget-object v2, LX/7Sc;->INPUT_PREVIEW:LX/7Sc;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, LX/7Sc;->INPUT_PREVIEW_SIZE:LX/7Sc;

    aput-object v2, v0, v1

    sput-object v0, LX/BVQ;->a:[LX/7Sc;

    return-void
.end method

.method public constructor <init>()V
    .locals 2
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1790986
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1790987
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, LX/BVQ;->d:Ljava/util/concurrent/locks/Lock;

    .line 1790988
    new-instance v0, Ljava/util/concurrent/locks/ReentrantReadWriteLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantReadWriteLock;-><init>()V

    iput-object v0, p0, LX/BVQ;->e:Ljava/util/concurrent/locks/ReadWriteLock;

    .line 1790989
    iput v1, p0, LX/BVQ;->m:I

    .line 1790990
    iput v1, p0, LX/BVQ;->n:I

    .line 1790991
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, LX/BVQ;->o:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 1790992
    new-instance v0, Lcom/facebook/videocodec/effects/styletransfer/StyleTransferRenderer$1;

    invoke-direct {v0, p0}, Lcom/facebook/videocodec/effects/styletransfer/StyleTransferRenderer$1;-><init>(LX/BVQ;)V

    iput-object v0, p0, LX/BVQ;->p:Ljava/lang/Runnable;

    .line 1790993
    return-void
.end method

.method public static a(LX/0QB;)LX/BVQ;
    .locals 1

    .prologue
    .line 1790985
    invoke-static {p0}, LX/BVQ;->b(LX/0QB;)LX/BVQ;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/BVQ;)V
    .locals 5

    .prologue
    .line 1790975
    iget-object v0, p0, LX/BVQ;->l:LX/6Js;

    if-eqz v0, :cond_0

    .line 1790976
    sget-object v1, LX/BVQ;->a:[LX/7Sc;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 1790977
    iget-object v4, p0, LX/BVQ;->l:LX/6Js;

    invoke-virtual {v4, p0, v3}, LX/6Js;->b(LX/6Jv;LX/7Sc;)V

    .line 1790978
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1790979
    :cond_0
    iget-object v0, p0, LX/BVQ;->e:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 1790980
    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, LX/BVQ;->f:LX/AMG;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1790981
    iget-object v0, p0, LX/BVQ;->e:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 1790982
    invoke-static {p0}, LX/BVQ;->d(LX/BVQ;)V

    .line 1790983
    return-void

    .line 1790984
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/BVQ;->e:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public static a(LX/BVQ;[LX/6JH;)V
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 1790968
    iget-object v0, p0, LX/BVQ;->e:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/ReadWriteLock;->readLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 1790969
    :try_start_0
    iget-object v0, p0, LX/BVQ;->f:LX/AMG;

    const/4 v1, 0x0

    aget-object v1, p1, v1

    invoke-interface {v1}, LX/6JH;->a()Ljava/nio/ByteBuffer;

    move-result-object v1

    const/4 v2, 0x0

    aget-object v2, p1, v2

    invoke-interface {v2}, LX/6JH;->c()I

    move-result v2

    const/4 v3, 0x0

    aget-object v3, p1, v3

    invoke-interface {v3}, LX/6JH;->b()I

    move-result v3

    const/4 v4, 0x1

    aget-object v4, p1, v4

    invoke-interface {v4}, LX/6JH;->a()Ljava/nio/ByteBuffer;

    move-result-object v4

    const/4 v5, 0x2

    aget-object v5, p1, v5

    invoke-interface {v5}, LX/6JH;->a()Ljava/nio/ByteBuffer;

    move-result-object v5

    const/4 v6, 0x1

    aget-object v6, p1, v6

    invoke-interface {v6}, LX/6JH;->c()I

    move-result v6

    const/4 v7, 0x1

    aget-object v7, p1, v7

    invoke-interface {v7}, LX/6JH;->b()I

    move-result v7

    iget v8, p0, LX/BVQ;->m:I

    iget v9, p0, LX/BVQ;->n:I

    invoke-virtual/range {v0 .. v9}, LX/AMG;->a(Ljava/nio/ByteBuffer;IILjava/nio/ByteBuffer;Ljava/nio/ByteBuffer;IIII)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1790970
    iget-object v0, p0, LX/BVQ;->e:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/ReadWriteLock;->readLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 1790971
    iget-object v0, p0, LX/BVQ;->o:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v10, v11}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1790972
    iget-object v0, p0, LX/BVQ;->b:Ljava/util/concurrent/ExecutorService;

    iget-object v1, p0, LX/BVQ;->p:Ljava/lang/Runnable;

    const v2, 0x279602f2

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1790973
    :cond_0
    return-void

    .line 1790974
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/BVQ;->e:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/ReadWriteLock;->readLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method private static a(Ljava/io/InputStream;)[B
    .locals 4

    .prologue
    .line 1790963
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 1790964
    const/16 v1, 0x400

    new-array v1, v1, [B

    .line 1790965
    :goto_0
    invoke-virtual {p0, v1}, Ljava/io/InputStream;->read([B)I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    .line 1790966
    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3, v2}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    goto :goto_0

    .line 1790967
    :cond_0
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/BVQ;
    .locals 3

    .prologue
    .line 1790958
    new-instance v2, LX/BVQ;

    invoke-direct {v2}, LX/BVQ;-><init>()V

    .line 1790959
    invoke-static {p0}, LX/0es;->b(LX/0QB;)LX/0TD;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ExecutorService;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    .line 1790960
    iput-object v0, v2, LX/BVQ;->b:Ljava/util/concurrent/ExecutorService;

    iput-object v1, v2, LX/BVQ;->c:Landroid/content/Context;

    .line 1790961
    return-object v2
.end method

.method public static b(LX/BVQ;LX/7So;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1790994
    iget-object v0, p0, LX/BVQ;->e:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 1790995
    :try_start_0
    new-instance v0, Ljava/io/FileInputStream;

    .line 1790996
    iget-object v2, p1, LX/7So;->a:Ljava/lang/String;

    move-object v2, v2

    .line 1790997
    invoke-direct {v0, v2}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, LX/BVQ;->a(Ljava/io/InputStream;)[B

    move-result-object v0

    .line 1790998
    new-instance v2, Ljava/io/FileInputStream;

    .line 1790999
    iget-object v3, p1, LX/7So;->b:Ljava/lang/String;

    move-object v3, v3

    .line 1791000
    invoke-direct {v2, v3}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, LX/BVQ;->a(Ljava/io/InputStream;)[B

    move-result-object v2

    .line 1791001
    new-instance v3, LX/AMG;

    invoke-direct {v3, v0, v2}, LX/AMG;-><init>([B[B)V

    iput-object v3, p0, LX/BVQ;->f:LX/AMG;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1791002
    iget-object v0, p0, LX/BVQ;->e:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 1791003
    :goto_0
    iget-object v0, p0, LX/BVQ;->l:LX/6Js;

    if-eqz v0, :cond_0

    .line 1791004
    sget-object v2, LX/BVQ;->a:[LX/7Sc;

    array-length v3, v2

    move v0, v1

    :goto_1
    if-ge v0, v3, :cond_0

    aget-object v1, v2, v0

    .line 1791005
    iget-object v4, p0, LX/BVQ;->l:LX/6Js;

    invoke-virtual {v4, p0, v1}, LX/6Js;->a(LX/6Jv;LX/7Sc;)V

    .line 1791006
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1791007
    :catch_0
    move-exception v0

    .line 1791008
    :try_start_1
    const-class v2, LX/BVQ;

    const-string v3, "Caffe2 model failed to load."

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v0, v3, v4}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1791009
    iget-object v0, p0, LX/BVQ;->e:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/BVQ;->e:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/ReadWriteLock;->writeLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    .line 1791010
    :cond_0
    return-void
.end method

.method public static d(LX/BVQ;)V
    .locals 2

    .prologue
    .line 1790949
    iget-object v0, p0, LX/BVQ;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 1790950
    :try_start_0
    iget-object v0, p0, LX/BVQ;->i:LX/5Pf;

    if-eqz v0, :cond_0

    .line 1790951
    iget-object v0, p0, LX/BVQ;->i:LX/5Pf;

    invoke-virtual {v0}, LX/5Pf;->a()V

    .line 1790952
    const/4 v0, 0x0

    iput-object v0, p0, LX/BVQ;->i:LX/5Pf;

    .line 1790953
    :cond_0
    iget-object v0, p0, LX/BVQ;->o:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 1790954
    const/4 v0, 0x0

    iput-object v0, p0, LX/BVQ;->g:LX/AMF;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1790955
    iget-object v0, p0, LX/BVQ;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 1790956
    return-void

    .line 1790957
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/BVQ;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method


# virtual methods
.method public final a(II)V
    .locals 0

    .prologue
    .line 1790948
    return-void
.end method

.method public final a(LX/5Pc;)V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x2

    .line 1790898
    invoke-static {p0}, LX/BVQ;->d(LX/BVQ;)V

    .line 1790899
    const v0, 0x7f070005

    const v1, 0x7f070004

    invoke-interface {p1, v0, v1}, LX/5Pc;->a(II)LX/5Pb;

    move-result-object v0

    iput-object v0, p0, LX/BVQ;->j:LX/5Pb;

    .line 1790900
    new-instance v0, LX/5PQ;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, LX/5PQ;-><init>(I)V

    const/4 v1, 0x5

    .line 1790901
    iput v1, v0, LX/5PQ;->a:I

    .line 1790902
    move-object v0, v0

    .line 1790903
    const-string v1, "aPosition"

    new-instance v2, LX/5Pg;

    new-array v3, v5, [F

    fill-array-data v3, :array_0

    invoke-direct {v2, v3, v4}, LX/5Pg;-><init>([FI)V

    invoke-virtual {v0, v1, v2}, LX/5PQ;->a(Ljava/lang/String;LX/5Pg;)LX/5PQ;

    move-result-object v0

    const-string v1, "aTextureCoord"

    new-instance v2, LX/5Pg;

    new-array v3, v5, [F

    fill-array-data v3, :array_1

    invoke-direct {v2, v3, v4}, LX/5Pg;-><init>([FI)V

    invoke-virtual {v0, v1, v2}, LX/5PQ;->a(Ljava/lang/String;LX/5Pg;)LX/5PQ;

    move-result-object v0

    invoke-virtual {v0}, LX/5PQ;->a()LX/5PR;

    move-result-object v0

    iput-object v0, p0, LX/BVQ;->k:LX/5PR;

    .line 1790904
    return-void

    nop

    :array_0
    .array-data 4
        -0x40800000    # -1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        -0x40800000    # -1.0f
        -0x40800000    # -1.0f
        0x3f800000    # 1.0f
        -0x40800000    # -1.0f
    .end array-data

    :array_1
    .array-data 4
        0x0
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x0
        0x0
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method public final a(LX/6Js;)V
    .locals 2

    .prologue
    .line 1790944
    iput-object p1, p0, LX/BVQ;->l:LX/6Js;

    .line 1790945
    iget-object v0, p0, LX/BVQ;->l:LX/6Js;

    if-eqz v0, :cond_0

    .line 1790946
    iget-object v0, p0, LX/BVQ;->l:LX/6Js;

    sget-object v1, LX/7Sc;->STYLE_TRANSFER:LX/7Sc;

    invoke-virtual {v0, p0, v1}, LX/6Js;->a(LX/6Jv;LX/7Sc;)V

    .line 1790947
    :cond_0
    return-void
.end method

.method public final a(LX/7Sb;)V
    .locals 3

    .prologue
    .line 1790920
    sget-object v0, LX/BVP;->a:[I

    invoke-interface {p1}, LX/7Sb;->a()LX/7Sc;

    move-result-object v1

    invoke-virtual {v1}, LX/7Sc;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1790921
    :goto_0
    return-void

    .line 1790922
    :pswitch_0
    check-cast p1, LX/7So;

    .line 1790923
    iget-object v0, p1, LX/7So;->a:Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p1, LX/7So;->b:Ljava/lang/String;

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 1790924
    if-eqz v0, :cond_3

    .line 1790925
    invoke-static {p0, p1}, LX/BVQ;->b(LX/BVQ;LX/7So;)V

    .line 1790926
    :goto_2
    goto :goto_0

    .line 1790927
    :pswitch_1
    check-cast p1, LX/7Sk;

    .line 1790928
    iget-object v0, p1, LX/7Sk;->a:[B

    move-object v0, v0

    .line 1790929
    if-nez v0, :cond_0

    .line 1790930
    iget-object v0, p1, LX/7Sk;->b:[LX/6JH;

    move-object v0, v0

    .line 1790931
    if-eqz v0, :cond_0

    .line 1790932
    iget-object v0, p1, LX/7Sk;->b:[LX/6JH;

    move-object v0, v0

    .line 1790933
    invoke-static {p0, v0}, LX/BVQ;->a(LX/BVQ;[LX/6JH;)V

    .line 1790934
    :cond_0
    goto :goto_0

    .line 1790935
    :pswitch_2
    check-cast p1, LX/7Sl;

    .line 1790936
    iget v0, p1, LX/7Sl;->a:I

    move v0, v0

    .line 1790937
    iget v1, p1, LX/7Sl;->b:I

    move v1, v1

    .line 1790938
    iget v2, p0, LX/BVQ;->m:I

    if-ne v0, v2, :cond_1

    iget v2, p0, LX/BVQ;->n:I

    if-eq v1, v2, :cond_2

    .line 1790939
    :cond_1
    iput v0, p0, LX/BVQ;->m:I

    .line 1790940
    iput v1, p0, LX/BVQ;->n:I

    .line 1790941
    invoke-static {p0}, LX/BVQ;->d(LX/BVQ;)V

    .line 1790942
    :cond_2
    goto :goto_0

    .line 1790943
    :cond_3
    invoke-static {p0}, LX/BVQ;->a(LX/BVQ;)V

    goto :goto_2

    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a([F[F[FJ)V
    .locals 5

    .prologue
    .line 1790908
    iget-object v0, p0, LX/BVQ;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->tryLock()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1790909
    :try_start_0
    iget-boolean v0, p0, LX/BVQ;->h:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/BVQ;->g:LX/AMF;

    if-eqz v0, :cond_1

    .line 1790910
    iget-object v0, p0, LX/BVQ;->i:LX/5Pf;

    if-eqz v0, :cond_0

    .line 1790911
    iget-object v0, p0, LX/BVQ;->i:LX/5Pf;

    invoke-virtual {v0}, LX/5Pf;->a()V

    .line 1790912
    :cond_0
    new-instance v0, LX/5Pe;

    invoke-direct {v0}, LX/5Pe;-><init>()V

    iget-object v1, p0, LX/BVQ;->g:LX/AMF;

    iget v1, v1, LX/AMF;->a:I

    iget-object v2, p0, LX/BVQ;->g:LX/AMF;

    iget v2, v2, LX/AMF;->b:I

    iget-object v3, p0, LX/BVQ;->g:LX/AMF;

    iget-object v3, v3, LX/AMF;->c:Ljava/nio/ByteBuffer;

    const v4, 0x80e1

    invoke-virtual {v0, v1, v2, v3, v4}, LX/5Pe;->a(IILjava/nio/ByteBuffer;I)LX/5Pe;

    move-result-object v0

    invoke-virtual {v0}, LX/5Pe;->a()LX/5Pf;

    move-result-object v0

    iput-object v0, p0, LX/BVQ;->i:LX/5Pf;

    .line 1790913
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/BVQ;->h:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1790914
    :cond_1
    iget-object v0, p0, LX/BVQ;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 1790915
    :cond_2
    iget-object v0, p0, LX/BVQ;->i:LX/5Pf;

    if-nez v0, :cond_3

    .line 1790916
    :goto_0
    return-void

    .line 1790917
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/BVQ;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    .line 1790918
    :cond_3
    const/16 v0, 0xb44

    invoke-static {v0}, Landroid/opengl/GLES20;->glDisable(I)V

    .line 1790919
    iget-object v0, p0, LX/BVQ;->j:LX/5Pb;

    invoke-virtual {v0}, LX/5Pb;->a()LX/5Pa;

    move-result-object v0

    const-string v1, "uSurfaceTransformMatrix"

    invoke-virtual {v0, v1, p1}, LX/5Pa;->a(Ljava/lang/String;[F)LX/5Pa;

    move-result-object v0

    const-string v1, "uVideoTransformMatrix"

    invoke-virtual {v0, v1, p2}, LX/5Pa;->a(Ljava/lang/String;[F)LX/5Pa;

    move-result-object v0

    const-string v1, "uSceneMatrix"

    invoke-virtual {v0, v1, p3}, LX/5Pa;->a(Ljava/lang/String;[F)LX/5Pa;

    move-result-object v0

    const-string v1, "sBitmap"

    iget-object v2, p0, LX/BVQ;->i:LX/5Pf;

    invoke-virtual {v0, v1, v2}, LX/5Pa;->a(Ljava/lang/String;LX/5Pf;)LX/5Pa;

    move-result-object v0

    iget-object v1, p0, LX/BVQ;->k:LX/5PR;

    invoke-virtual {v0, v1}, LX/5Pa;->a(LX/5PR;)LX/5Pa;

    goto :goto_0
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 1790906
    invoke-static {p0}, LX/BVQ;->d(LX/BVQ;)V

    .line 1790907
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1790905
    iget-object v0, p0, LX/BVQ;->f:LX/AMG;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
