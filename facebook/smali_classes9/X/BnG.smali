.class public LX/BnG;
.super LX/1OM;
.source ""

# interfaces
.implements LX/2ke;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;",
        "LX/2ke",
        "<",
        "Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel$EventThemesModel$NodesModel;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/2kM;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2kM",
            "<",
            "Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel$EventThemesModel$NodesModel;",
            ">;"
        }
    .end annotation
.end field

.field public b:I

.field private c:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1820011
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 1820012
    sget-object v0, LX/2kL;->a:LX/2kM;

    iput-object v0, p0, LX/BnG;->a:LX/2kM;

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1820013
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 1820014
    if-nez p2, :cond_1

    .line 1820015
    iget v0, p0, LX/BnG;->b:I

    if-ne v0, v3, :cond_0

    const v0, 0x7f030590

    :goto_0
    invoke-virtual {v1, v0, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1820016
    new-instance v0, Lcom/facebook/events/ui/themeselector/ThemeViewHolder;

    invoke-direct {v0, v1}, Lcom/facebook/events/ui/themeselector/ThemeViewHolder;-><init>(Landroid/view/View;)V

    .line 1820017
    :goto_1
    return-object v0

    .line 1820018
    :cond_0
    const v0, 0x7f030591

    goto :goto_0

    .line 1820019
    :cond_1
    if-ne p2, v3, :cond_2

    .line 1820020
    const v0, 0x7f030592

    invoke-virtual {v1, v0, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1820021
    new-instance v0, LX/BnF;

    invoke-direct {v0, v1}, LX/BnF;-><init>(Landroid/view/View;)V

    goto :goto_1

    .line 1820022
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(LX/1a1;I)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1819980
    invoke-virtual {p0, p2}, LX/1OM;->getItemViewType(I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1819981
    :cond_0
    :goto_0
    return-void

    .line 1819982
    :pswitch_0
    check-cast p1, Lcom/facebook/events/ui/themeselector/ThemeViewHolder;

    .line 1819983
    iget-object v0, p0, LX/BnG;->a:LX/2kM;

    invoke-interface {v0}, LX/2kM;->c()I

    move-result v0

    if-ge p2, v0, :cond_9

    .line 1819984
    iget-object v0, p0, LX/BnG;->a:LX/2kM;

    invoke-interface {v0, p2}, LX/2kM;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel$EventThemesModel$NodesModel;

    .line 1819985
    :goto_1
    move-object v3, v0

    .line 1819986
    if-eqz v3, :cond_2

    .line 1819987
    invoke-virtual {v3}, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel$EventThemesModel$NodesModel;->l()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 1819988
    if-eqz v0, :cond_1

    move v0, v1

    :goto_2
    if-eqz v0, :cond_4

    .line 1819989
    invoke-virtual {v3}, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel$EventThemesModel$NodesModel;->l()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 1819990
    invoke-virtual {v4, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    if-eqz v0, :cond_6

    .line 1819991
    invoke-virtual {v3}, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel$EventThemesModel$NodesModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 1819992
    if-eqz v0, :cond_5

    move v0, v1

    :goto_4
    if-eqz v0, :cond_8

    .line 1819993
    invoke-virtual {v3}, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel$EventThemesModel$NodesModel;->j()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 1819994
    invoke-virtual {v4, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_7

    :goto_5
    if-eqz v1, :cond_0

    .line 1819995
    invoke-virtual {v3}, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel$EventThemesModel$NodesModel;->l()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 1819996
    invoke-virtual {v3}, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel$EventThemesModel$NodesModel;->j()LX/1vs;

    move-result-object v4

    iget-object v5, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    .line 1819997
    invoke-virtual {v3}, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel$EventThemesModel$NodesModel;->k()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v4, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    .line 1819998
    iput-object v3, p1, Lcom/facebook/events/ui/themeselector/ThemeViewHolder;->m:Ljava/lang/String;

    .line 1819999
    iput-object v1, p1, Lcom/facebook/events/ui/themeselector/ThemeViewHolder;->n:Ljava/lang/String;

    .line 1820000
    iget-object v2, p1, Lcom/facebook/events/ui/themeselector/ThemeViewHolder;->l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    sget-object v5, Lcom/facebook/events/ui/themeselector/ThemeViewHolder;->p:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v4, v5}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1820001
    iget-object v2, p1, Lcom/facebook/events/ui/themeselector/ThemeViewHolder;->l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v4, p1, Lcom/facebook/events/ui/themeselector/ThemeViewHolder;->o:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1820002
    goto :goto_0

    :cond_1
    move v0, v2

    .line 1820003
    goto :goto_2

    :cond_2
    move v0, v2

    goto :goto_2

    :cond_3
    move v0, v2

    goto :goto_3

    :cond_4
    move v0, v2

    goto :goto_3

    :cond_5
    move v0, v2

    goto :goto_4

    :cond_6
    move v0, v2

    goto :goto_4

    :cond_7
    move v1, v2

    goto :goto_5

    :cond_8
    move v1, v2

    goto :goto_5

    .line 1820004
    :pswitch_1
    check-cast p1, LX/BnF;

    .line 1820005
    iget-boolean v0, p0, LX/BnG;->c:Z

    .line 1820006
    iget-object v2, p1, LX/BnF;->l:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_a

    const/4 v1, 0x0

    :goto_6
    invoke-virtual {v2, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1820007
    goto/16 :goto_0

    :cond_9
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 1820008
    :cond_a
    const/16 v1, 0x8

    goto :goto_6

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(LX/2kM;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2kM",
            "<",
            "Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel$EventThemesModel$NodesModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1820009
    iput-object p1, p0, LX/BnG;->a:LX/2kM;

    .line 1820010
    return-void
.end method

.method public final b(Z)V
    .locals 1

    .prologue
    .line 1819970
    iget-boolean v0, p0, LX/BnG;->c:Z

    if-eq v0, p1, :cond_0

    .line 1819971
    iput-boolean p1, p0, LX/BnG;->c:Z

    .line 1819972
    if-eqz p1, :cond_1

    .line 1819973
    iget-object v0, p0, LX/BnG;->a:LX/2kM;

    invoke-interface {v0}, LX/2kM;->c()I

    move-result v0

    invoke-virtual {p0, v0}, LX/1OM;->j_(I)V

    .line 1819974
    :cond_0
    :goto_0
    return-void

    .line 1819975
    :cond_1
    iget-object v0, p0, LX/BnG;->a:LX/2kM;

    invoke-interface {v0}, LX/2kM;->c()I

    move-result v0

    invoke-virtual {p0, v0}, LX/1OM;->d(I)V

    goto :goto_0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 1819976
    iget-object v0, p0, LX/BnG;->a:LX/2kM;

    invoke-interface {v0}, LX/2kM;->c()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 1819977
    const/4 v0, 0x0

    .line 1819978
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final ij_()I
    .locals 2

    .prologue
    .line 1819979
    iget-object v0, p0, LX/BnG;->a:LX/2kM;

    invoke-interface {v0}, LX/2kM;->c()I

    move-result v1

    iget-boolean v0, p0, LX/BnG;->c:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
