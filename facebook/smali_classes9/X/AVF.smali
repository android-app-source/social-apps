.class public LX/AVF;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/AVT;

.field public volatile b:LX/AVE;

.field public volatile c:LX/AVE;

.field public d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/1b9;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/AVT;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1679081
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1679082
    iput-object p1, p0, LX/AVF;->a:LX/AVT;

    .line 1679083
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/AVF;->d:Ljava/util/List;

    .line 1679084
    sget-object v0, LX/AVE;->UNINITIALIZED:LX/AVE;

    iput-object v0, p0, LX/AVF;->b:LX/AVE;

    .line 1679085
    sget-object v0, LX/AVE;->UNINITIALIZED:LX/AVE;

    iput-object v0, p0, LX/AVF;->c:LX/AVE;

    .line 1679086
    return-void
.end method

.method public static a(LX/0QB;)LX/AVF;
    .locals 2

    .prologue
    .line 1679102
    new-instance v1, LX/AVF;

    invoke-static {p0}, LX/AVT;->a(LX/0QB;)LX/AVT;

    move-result-object v0

    check-cast v0, LX/AVT;

    invoke-direct {v1, v0}, LX/AVF;-><init>(LX/AVT;)V

    .line 1679103
    move-object v0, v1

    .line 1679104
    return-object v0
.end method


# virtual methods
.method public final a(LX/1b9;)V
    .locals 1

    .prologue
    .line 1679100
    iget-object v0, p0, LX/AVF;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1679101
    return-void
.end method

.method public final a(LX/AVE;Ljava/lang/String;)V
    .locals 1
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1679098
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, LX/AVF;->a(LX/AVE;Ljava/lang/String;Lcom/facebook/video/videostreaming/LiveStreamingError;)V

    .line 1679099
    return-void
.end method

.method public final a(LX/AVE;Ljava/lang/String;Lcom/facebook/video/videostreaming/LiveStreamingError;)V
    .locals 10
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/video/videostreaming/LiveStreamingError;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1679091
    iget-object v0, p0, LX/AVF;->b:LX/AVE;

    iput-object v0, p0, LX/AVF;->c:LX/AVE;

    .line 1679092
    iput-object p1, p0, LX/AVF;->b:LX/AVE;

    .line 1679093
    iget-object v0, p0, LX/AVF;->a:LX/AVT;

    iget-object v1, p0, LX/AVF;->c:LX/AVE;

    iget-object v1, v1, LX/AVE;->loggerName:Ljava/lang/String;

    iget-object v2, p1, LX/AVE;->loggerName:Ljava/lang/String;

    .line 1679094
    const/4 v9, 0x0

    move-object v4, v0

    move-object v5, v1

    move-object v6, v2

    move-object v7, p2

    move-object v8, p3

    invoke-virtual/range {v4 .. v9}, LX/AVT;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/video/videostreaming/LiveStreamingError;Ljava/util/Map;)V

    .line 1679095
    iget-object v0, p0, LX/AVF;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1b9;

    .line 1679096
    iget-object v2, p0, LX/AVF;->b:LX/AVE;

    iget-object v3, p0, LX/AVF;->c:LX/AVE;

    invoke-interface {v0, v2, v3}, LX/1b9;->a(LX/AVE;LX/AVE;)V

    goto :goto_0

    .line 1679097
    :cond_0
    return-void
.end method

.method public final b(LX/1b9;)V
    .locals 1

    .prologue
    .line 1679089
    iget-object v0, p0, LX/AVF;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1679090
    return-void
.end method

.method public final b(LX/AVE;)V
    .locals 1

    .prologue
    .line 1679087
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/AVF;->a(LX/AVE;Ljava/lang/String;)V

    .line 1679088
    return-void
.end method
