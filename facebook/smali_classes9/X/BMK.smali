.class public LX/BMK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Alg;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<CONTEXT::",
        "LX/B5o;",
        ":",
        "Lcom/facebook/ipc/productionprompts/actioncontext/PromptActionContextInterfaces$HasComposerSessionId;",
        ":",
        "Lcom/facebook/ipc/productionprompts/actioncontext/PromptActionContextInterfaces$HasPromptSessionId;",
        ":",
        "Lcom/facebook/ipc/productionprompts/actioncontext/PromptActionContextInterfaces$HasSelectedFrameId;",
        ">",
        "Ljava/lang/Object;",
        "LX/Alg",
        "<TCONTEXT;>;"
    }
.end annotation


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Nq;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/1b0;


# direct methods
.method private constructor <init>(LX/1b0;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1777063
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1777064
    iput-object p1, p0, LX/BMK;->b:LX/1b0;

    .line 1777065
    return-void
.end method

.method private static a(LX/1RN;)Lcom/facebook/productionprompts/model/ProductionPrompt;
    .locals 2

    .prologue
    .line 1777058
    invoke-static {p0}, LX/1RN;->a(LX/1RN;)LX/1kK;

    move-result-object v0

    .line 1777059
    instance-of v1, v0, LX/1kW;

    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 1777060
    check-cast v0, LX/1kW;

    .line 1777061
    iget-object v1, v0, LX/1kW;->a:Lcom/facebook/productionprompts/model/ProductionPrompt;

    move-object v0, v1

    .line 1777062
    return-object v0
.end method

.method public static b(LX/0QB;)LX/BMK;
    .locals 2

    .prologue
    .line 1777017
    new-instance v1, LX/BMK;

    invoke-static {p0}, LX/1az;->a(LX/0QB;)LX/1az;

    move-result-object v0

    check-cast v0, LX/1b0;

    invoke-direct {v1, v0}, LX/BMK;-><init>(LX/1b0;)V

    .line 1777018
    const/16 v0, 0xbdd

    invoke-static {p0, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v0

    .line 1777019
    iput-object v0, v1, LX/BMK;->a:LX/0Or;

    .line 1777020
    return-object v1
.end method


# virtual methods
.method public final a(LX/88f;LX/1RN;)V
    .locals 0
    .param p1    # LX/88f;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1777057
    return-void
.end method

.method public final a(Landroid/view/View;LX/1RN;LX/B5o;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "LX/1RN;",
            "TCONTEXT;)V"
        }
    .end annotation

    .prologue
    const/4 v10, 0x1

    .line 1777022
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Landroid/app/Activity;

    invoke-static {v0, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/app/Activity;

    move-object v0, p3

    .line 1777023
    check-cast v0, LX/B5p;

    .line 1777024
    iget-object v1, v0, LX/B5p;->j:LX/0am;

    move-object v0, v1

    .line 1777025
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Ljava/lang/String;

    .line 1777026
    invoke-static {p2}, LX/BMK;->a(LX/1RN;)Lcom/facebook/productionprompts/model/ProductionPrompt;

    move-result-object v9

    .line 1777027
    iget-object v0, p0, LX/BMK;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, LX/1Nq;

    .line 1777028
    iget-object v0, p2, LX/1RN;->a:LX/1kK;

    invoke-interface {v0}, LX/1kK;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p2, LX/1RN;->b:LX/1lP;

    iget-object v1, v1, LX/1lP;->c:Ljava/lang/String;

    move-object v2, p3

    check-cast v2, LX/B5p;

    .line 1777029
    iget-object v3, v2, LX/B5p;->a:Ljava/lang/String;

    move-object v2, v3

    .line 1777030
    move-object v3, p3

    check-cast v3, LX/B5p;

    .line 1777031
    iget-object v4, v3, LX/B5p;->j:LX/0am;

    move-object v3, v4

    .line 1777032
    invoke-virtual {v3}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    iget-object v4, p2, LX/1RN;->b:LX/1lP;

    iget-object v4, v4, LX/1lP;->b:Ljava/lang/String;

    iget-object v5, p2, LX/1RN;->c:LX/32e;

    iget-object v5, v5, LX/32e;->a:LX/24P;

    invoke-virtual {v5}, LX/24P;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Lcom/facebook/productionprompts/logging/PromptAnalytics;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/productionprompts/logging/PromptAnalytics;

    move-result-object v0

    .line 1777033
    sget-object v1, LX/21D;->NEWSFEED:LX/21D;

    const-string v2, "framesPrompt"

    invoke-static {v1, v2}, LX/1nC;->a(LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    invoke-static {v9, v0, v10}, Lcom/facebook/productionprompts/composer/ProductionPromptsPluginConfig;->a(Lcom/facebook/productionprompts/model/ProductionPrompt;Lcom/facebook/productionprompts/logging/PromptAnalytics;Z)Lcom/facebook/productionprompts/composer/ProductionPromptsPluginConfig;

    move-result-object v0

    invoke-virtual {v8, v0}, LX/1Nq;->a(LX/88f;)Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setPluginConfig(Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    .line 1777034
    invoke-static {v1, v9}, LX/BMU;->a(Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;Lcom/facebook/productionprompts/model/ProductionPrompt;)V

    .line 1777035
    iget-object v8, p0, LX/BMK;->b:LX/1b0;

    const/16 v9, 0x20b4

    new-instance v0, LX/89V;

    invoke-direct {v0}, LX/89V;-><init>()V

    .line 1777036
    iput-boolean v10, v0, LX/89V;->b:Z

    .line 1777037
    move-object v0, v0

    .line 1777038
    sget-object v2, LX/4gI;->PHOTO_ONLY:LX/4gI;

    .line 1777039
    iput-object v2, v0, LX/89V;->f:LX/4gI;

    .line 1777040
    move-object v0, v0

    .line 1777041
    invoke-static {p2}, LX/BMK;->a(LX/1RN;)Lcom/facebook/productionprompts/model/ProductionPrompt;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/productionprompts/model/ProductionPrompt;->o()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;

    move-result-object v2

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    .line 1777042
    iput-object v2, v0, LX/89V;->h:LX/0Px;

    .line 1777043
    move-object v2, v0

    .line 1777044
    move-object v0, p3

    check-cast v0, LX/B5p;

    .line 1777045
    iget-object v3, v0, LX/B5p;->f:Ljava/lang/String;

    move-object v0, v3

    .line 1777046
    iput-object v0, v2, LX/89V;->i:Ljava/lang/String;

    .line 1777047
    move-object v0, v2

    .line 1777048
    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v1

    .line 1777049
    iput-object v1, v0, LX/89V;->k:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    .line 1777050
    move-object v0, v0

    .line 1777051
    invoke-virtual {v0}, LX/89V;->a()Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;

    move-result-object v10

    iget-object v0, p2, LX/1RN;->a:LX/1kK;

    invoke-interface {v0}, LX/1kK;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p2, LX/1RN;->b:LX/1lP;

    iget-object v1, v1, LX/1lP;->c:Ljava/lang/String;

    move-object v2, p3

    check-cast v2, LX/B5p;

    .line 1777052
    iget-object v3, v2, LX/B5p;->a:Ljava/lang/String;

    move-object v2, v3

    .line 1777053
    check-cast p3, LX/B5p;

    .line 1777054
    iget-object v3, p3, LX/B5p;->j:LX/0am;

    move-object v3, v3

    .line 1777055
    invoke-virtual {v3}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    iget-object v4, p2, LX/1RN;->b:LX/1lP;

    iget-object v4, v4, LX/1lP;->b:Ljava/lang/String;

    iget-object v5, p2, LX/1RN;->c:LX/32e;

    iget-object v5, v5, LX/32e;->a:LX/24P;

    invoke-virtual {v5}, LX/24P;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Lcom/facebook/productionprompts/logging/PromptAnalytics;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/productionprompts/logging/PromptAnalytics;

    move-result-object v5

    move-object v0, v8

    move-object v1, v6

    move v2, v9

    move-object v3, v10

    move-object v4, v7

    invoke-interface/range {v0 .. v5}, LX/1b0;->a(Landroid/app/Activity;ILcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;Ljava/lang/String;Lcom/facebook/productionprompts/logging/PromptAnalytics;)V

    .line 1777056
    return-void
.end method

.method public final b(LX/1RN;)Z
    .locals 1

    .prologue
    .line 1777021
    invoke-static {p1}, LX/BMK;->a(LX/1RN;)Lcom/facebook/productionprompts/model/ProductionPrompt;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/productionprompts/model/ProductionPrompt;->o()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
