.class public LX/C1x;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/MountSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/os/Handler;)V
    .locals 0
    .param p1    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1843646
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1843647
    iput-object p1, p0, LX/C1x;->a:Landroid/os/Handler;

    .line 1843648
    return-void
.end method

.method public static a(LX/0QB;)LX/C1x;
    .locals 4

    .prologue
    .line 1843649
    const-class v1, LX/C1x;

    monitor-enter v1

    .line 1843650
    :try_start_0
    sget-object v0, LX/C1x;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1843651
    sput-object v2, LX/C1x;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1843652
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1843653
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1843654
    new-instance p0, LX/C1x;

    invoke-static {v0}, LX/0Ss;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v3

    check-cast v3, Landroid/os/Handler;

    invoke-direct {p0, v3}, LX/C1x;-><init>(Landroid/os/Handler;)V

    .line 1843655
    move-object v0, p0

    .line 1843656
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1843657
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C1x;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1843658
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1843659
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
