.class public final LX/CWb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Bm9;


# instance fields
.field public final synthetic a:Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDateTimePickerView;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDateTimePickerView;)V
    .locals 0

    .prologue
    .line 1908434
    iput-object p1, p0, LX/CWb;->a:Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDateTimePickerView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/Calendar;)V
    .locals 5

    .prologue
    .line 1908435
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    .line 1908436
    const/4 v1, 0x2

    invoke-virtual {p1, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    .line 1908437
    const/4 v2, 0x5

    invoke-virtual {p1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    .line 1908438
    iget-object v3, p0, LX/CWb;->a:Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDateTimePickerView;

    iget-object v3, v3, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDateTimePickerView;->f:LX/CSY;

    const-string v4, "year"

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, LX/CSY;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1908439
    iget-object v0, p0, LX/CWb;->a:Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDateTimePickerView;

    iget-object v0, v0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDateTimePickerView;->f:LX/CSY;

    const-string v3, "month"

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, LX/CSY;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1908440
    iget-object v0, p0, LX/CWb;->a:Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDateTimePickerView;

    iget-object v0, v0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDateTimePickerView;->f:LX/CSY;

    const-string v1, "day"

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/CSY;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1908441
    iget-object v0, p0, LX/CWb;->a:Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDateTimePickerView;

    iget-object v0, v0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDateTimePickerView;->h:LX/CT7;

    iget-object v1, p0, LX/CWb;->a:Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDateTimePickerView;

    iget-object v1, v1, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDateTimePickerView;->g:LX/CTO;

    iget-object v1, v1, LX/CTJ;->o:Ljava/lang/String;

    iget-object v2, p0, LX/CWb;->a:Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDateTimePickerView;

    iget-object v2, v2, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDateTimePickerView;->g:LX/CTO;

    iget-object v2, v2, LX/CTK;->e:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    iget-object v3, p0, LX/CWb;->a:Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDateTimePickerView;

    iget-object v3, v3, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDateTimePickerView;->f:LX/CSY;

    invoke-virtual {v0, v1, v2, v3}, LX/CT7;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;LX/CSY;)V

    .line 1908442
    return-void
.end method
