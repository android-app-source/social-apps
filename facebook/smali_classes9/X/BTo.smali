.class public final enum LX/BTo;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/BTo;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/BTo;

.field public static final enum DOWNLOAD_ABORTED:LX/BTo;

.field public static final enum INSUFFICIENT_SPACE_DEVICE:LX/BTo;

.field public static final enum INSUFFICIENT_SPACE_INTERNAL:LX/BTo;

.field public static final enum INSUFFICIENT_SPACE_INTERNAL_SAVED_DASHBOARD:LX/BTo;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1788002
    new-instance v0, LX/BTo;

    const-string v1, "INSUFFICIENT_SPACE_INTERNAL"

    invoke-direct {v0, v1, v2}, LX/BTo;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BTo;->INSUFFICIENT_SPACE_INTERNAL:LX/BTo;

    .line 1788003
    new-instance v0, LX/BTo;

    const-string v1, "INSUFFICIENT_SPACE_INTERNAL_SAVED_DASHBOARD"

    invoke-direct {v0, v1, v3}, LX/BTo;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BTo;->INSUFFICIENT_SPACE_INTERNAL_SAVED_DASHBOARD:LX/BTo;

    .line 1788004
    new-instance v0, LX/BTo;

    const-string v1, "INSUFFICIENT_SPACE_DEVICE"

    invoke-direct {v0, v1, v4}, LX/BTo;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BTo;->INSUFFICIENT_SPACE_DEVICE:LX/BTo;

    .line 1788005
    new-instance v0, LX/BTo;

    const-string v1, "DOWNLOAD_ABORTED"

    invoke-direct {v0, v1, v5}, LX/BTo;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BTo;->DOWNLOAD_ABORTED:LX/BTo;

    .line 1788006
    const/4 v0, 0x4

    new-array v0, v0, [LX/BTo;

    sget-object v1, LX/BTo;->INSUFFICIENT_SPACE_INTERNAL:LX/BTo;

    aput-object v1, v0, v2

    sget-object v1, LX/BTo;->INSUFFICIENT_SPACE_INTERNAL_SAVED_DASHBOARD:LX/BTo;

    aput-object v1, v0, v3

    sget-object v1, LX/BTo;->INSUFFICIENT_SPACE_DEVICE:LX/BTo;

    aput-object v1, v0, v4

    sget-object v1, LX/BTo;->DOWNLOAD_ABORTED:LX/BTo;

    aput-object v1, v0, v5

    sput-object v0, LX/BTo;->$VALUES:[LX/BTo;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1788007
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/BTo;
    .locals 1

    .prologue
    .line 1788008
    const-class v0, LX/BTo;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/BTo;

    return-object v0
.end method

.method public static values()[LX/BTo;
    .locals 1

    .prologue
    .line 1788009
    sget-object v0, LX/BTo;->$VALUES:[LX/BTo;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/BTo;

    return-object v0
.end method
