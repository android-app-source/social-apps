.class public LX/AyG;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/AyG;


# instance fields
.field public final a:LX/AyH;

.field public final b:LX/0Sh;


# direct methods
.method public constructor <init>(LX/AyH;LX/0Sh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1729914
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1729915
    iput-object p1, p0, LX/AyG;->a:LX/AyH;

    .line 1729916
    iput-object p2, p0, LX/AyG;->b:LX/0Sh;

    .line 1729917
    return-void
.end method

.method public static a(LX/0QB;)LX/AyG;
    .locals 5

    .prologue
    .line 1729901
    sget-object v0, LX/AyG;->c:LX/AyG;

    if-nez v0, :cond_1

    .line 1729902
    const-class v1, LX/AyG;

    monitor-enter v1

    .line 1729903
    :try_start_0
    sget-object v0, LX/AyG;->c:LX/AyG;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1729904
    if-eqz v2, :cond_0

    .line 1729905
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1729906
    new-instance p0, LX/AyG;

    invoke-static {v0}, LX/AyH;->a(LX/0QB;)LX/AyH;

    move-result-object v3

    check-cast v3, LX/AyH;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v4

    check-cast v4, LX/0Sh;

    invoke-direct {p0, v3, v4}, LX/AyG;-><init>(LX/AyH;LX/0Sh;)V

    .line 1729907
    move-object v0, p0

    .line 1729908
    sput-object v0, LX/AyG;->c:LX/AyG;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1729909
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1729910
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1729911
    :cond_1
    sget-object v0, LX/AyG;->c:LX/AyG;

    return-object v0

    .line 1729912
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1729913
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/0P1;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1729883
    iget-object v0, p0, LX/AyG;->b:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 1729884
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 1729885
    const-string v1, "prompted_models"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1729886
    iget-object v1, p0, LX/AyG;->a:LX/AyH;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 1729887
    if-nez v1, :cond_0

    .line 1729888
    sget-object v0, LX/0Rg;->a:LX/0Rg;

    move-object v0, v0

    .line 1729889
    :goto_0
    return-object v0

    .line 1729890
    :cond_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1729891
    sget-object v0, LX/AyK;->a:LX/0U1;

    invoke-virtual {v0, v1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v0

    .line 1729892
    sget-object v2, LX/AyK;->b:LX/0U1;

    invoke-virtual {v2, v1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v2

    .line 1729893
    new-instance v3, LX/0P2;

    invoke-direct {v3}, LX/0P2;-><init>()V

    .line 1729894
    :cond_1
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 1729895
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 1729896
    invoke-virtual {v3}, LX/0P2;->b()LX/0P1;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1729897
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1729898
    sget-object v0, LX/0Rg;->a:LX/0Rg;

    move-object v0, v0

    .line 1729899
    goto :goto_0

    .line 1729900
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public final b()I
    .locals 4

    .prologue
    .line 1729881
    iget-object v0, p0, LX/AyG;->b:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 1729882
    iget-object v0, p0, LX/AyG;->a:LX/AyH;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "prompted_models"

    const-string v2, "1"

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    return v0
.end method
