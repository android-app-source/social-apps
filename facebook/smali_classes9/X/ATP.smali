.class public LX/ATP;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/ATO;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1676005
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 1676006
    return-void
.end method


# virtual methods
.method public final a(LX/0gc;Lcom/facebook/widget/ScrollingAwareScrollView;Landroid/widget/LinearLayout;ZZLjava/lang/Object;Z)LX/ATO;
    .locals 37
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ModelData:",
            "Ljava/lang/Object;",
            ":",
            "LX/0io;",
            ":",
            "LX/0iv;",
            ":",
            "LX/0j0;",
            ":",
            "LX/0j3;",
            ":",
            "LX/0j4;",
            ":",
            "LX/0jA;",
            ":",
            "LX/0j6;",
            "DerivedData:",
            "Ljava/lang/Object;",
            ":",
            "LX/5Qw;",
            ":",
            "LX/5Qz;",
            ":",
            "LX/5R0;",
            "Services:",
            "Ljava/lang/Object;",
            ":",
            "LX/0il",
            "<TModelData;>;:",
            "LX/0ik",
            "<TDerivedData;>;>(",
            "LX/0gc;",
            "Lcom/facebook/widget/ScrollingAwareScrollView;",
            "Landroid/widget/LinearLayout;",
            "ZZTServices;Z)",
            "LX/ATO",
            "<TModelData;TDerivedData;TServices;>;"
        }
    .end annotation

    .prologue
    .line 1676003
    new-instance v2, LX/ATO;

    const-class v3, Landroid/content/Context;

    move-object/from16 v0, p0

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static/range {p0 .. p0}, LX/1PK;->a(LX/0QB;)Landroid/view/LayoutInflater;

    move-result-object v4

    check-cast v4, Landroid/view/LayoutInflater;

    invoke-static/range {p0 .. p0}, LX/1Ck;->a(LX/0QB;)LX/1Ck;

    move-result-object v5

    check-cast v5, LX/1Ck;

    invoke-static/range {p0 .. p0}, LX/ASg;->a(LX/0QB;)LX/ASg;

    move-result-object v6

    check-cast v6, LX/ASg;

    invoke-static/range {p0 .. p0}, LX/ATQ;->a(LX/0QB;)LX/ATQ;

    move-result-object v7

    check-cast v7, LX/ATQ;

    invoke-static/range {p0 .. p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v8

    check-cast v8, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static/range {p0 .. p0}, LX/0wW;->a(LX/0QB;)LX/0wW;

    move-result-object v9

    check-cast v9, LX/0wW;

    invoke-static/range {p0 .. p0}, LX/7kn;->a(LX/0QB;)LX/7kn;

    move-result-object v10

    check-cast v10, LX/7kn;

    const/16 v11, 0x15d6

    move-object/from16 v0, p0

    invoke-static {v0, v11}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v11

    invoke-static/range {p0 .. p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v12

    check-cast v12, LX/03V;

    const-class v13, LX/9cA;

    move-object/from16 v0, p0

    invoke-interface {v0, v13}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v13

    check-cast v13, LX/9cA;

    invoke-static/range {p0 .. p0}, LX/74n;->a(LX/0QB;)LX/74n;

    move-result-object v14

    check-cast v14, LX/74n;

    invoke-static/range {p0 .. p0}, LX/8GT;->a(LX/0QB;)LX/8GT;

    move-result-object v15

    check-cast v15, LX/8GT;

    const-class v16, LX/ATk;

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v16

    check-cast v16, LX/ATk;

    const-class v17, LX/AT3;

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v17

    check-cast v17, LX/AT3;

    const-class v18, LX/ATt;

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v18

    check-cast v18, LX/ATt;

    const-class v19, LX/ASs;

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v19

    check-cast v19, LX/ASs;

    const-class v20, LX/ASo;

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v20

    check-cast v20, LX/ASo;

    const-class v21, LX/ATA;

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v21

    check-cast v21, LX/ATA;

    invoke-static/range {p0 .. p0}, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->a(LX/0QB;)Lcom/facebook/facerec/manager/FaceBoxPrioritizer;

    move-result-object v22

    check-cast v22, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;

    invoke-static/range {p0 .. p0}, LX/75F;->a(LX/0QB;)LX/75F;

    move-result-object v23

    check-cast v23, LX/75F;

    invoke-static/range {p0 .. p0}, LX/75Q;->a(LX/0QB;)LX/75Q;

    move-result-object v24

    check-cast v24, LX/75Q;

    invoke-static/range {p0 .. p0}, LX/1EZ;->a(LX/0QB;)LX/1EZ;

    move-result-object v25

    check-cast v25, LX/1EZ;

    invoke-static/range {p0 .. p0}, LX/9iZ;->a(LX/0QB;)LX/9iZ;

    move-result-object v26

    check-cast v26, LX/9iZ;

    invoke-static/range {p0 .. p0}, LX/ASb;->a(LX/0QB;)LX/ASb;

    move-result-object v27

    check-cast v27, LX/ASb;

    invoke-static/range {p0 .. p0}, LX/3iT;->a(LX/0QB;)LX/3iT;

    move-result-object v28

    check-cast v28, LX/3iT;

    invoke-static/range {p0 .. p0}, LX/BTg;->a(LX/0QB;)LX/BTg;

    move-result-object v29

    check-cast v29, LX/BTg;

    move-object/from16 v35, p6

    check-cast v35, LX/0il;

    move-object/from16 v30, p1

    move-object/from16 v31, p2

    move-object/from16 v32, p3

    move/from16 v33, p4

    move/from16 v34, p5

    move/from16 v36, p7

    invoke-direct/range {v2 .. v36}, LX/ATO;-><init>(Landroid/content/Context;Landroid/view/LayoutInflater;LX/1Ck;LX/ASg;LX/ATQ;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0wW;LX/7kn;LX/0Or;LX/03V;LX/9cA;LX/74n;LX/8GT;LX/ATk;LX/AT3;LX/ATt;LX/ASs;LX/ASo;LX/ATA;Lcom/facebook/facerec/manager/FaceBoxPrioritizer;LX/75F;LX/75Q;LX/1EZ;LX/9iZ;LX/ASb;LX/3iT;LX/BTg;LX/0gc;Lcom/facebook/widget/ScrollingAwareScrollView;Landroid/widget/LinearLayout;ZZLX/0il;Z)V

    .line 1676004
    return-object v2
.end method
