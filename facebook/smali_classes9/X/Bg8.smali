.class public LX/Bg8;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:LX/D9E;


# direct methods
.method public constructor <init>(LX/D9E;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1807130
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1807131
    iput-object p1, p0, LX/Bg8;->a:LX/D9E;

    .line 1807132
    return-void
.end method

.method private a(Landroid/content/Context;LX/97f;LX/BgS;LX/Bh0;)LX/0am;
    .locals 13
    .param p4    # LX/Bh0;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/97f;",
            "LX/BgS;",
            "Lcom/facebook/crowdsourcing/suggestedits/controller/SuggestEditsContextMenuManager$SuggestEditsContextOptionClickListener;",
            ")",
            "LX/0am",
            "<",
            "LX/5OM;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1807116
    invoke-interface {p2}, LX/97f;->k()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel$OptionsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, LX/97f;->k()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel$OptionsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel$OptionsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1807117
    :cond_0
    invoke-static {}, LX/0am;->absent()LX/0am;

    .line 1807118
    :cond_1
    new-instance v10, LX/6WS;

    invoke-direct {v10, p1}, LX/6WS;-><init>(Landroid/content/Context;)V

    .line 1807119
    invoke-virtual {v10}, LX/5OM;->c()LX/5OG;

    move-result-object v1

    .line 1807120
    invoke-interface {p2}, LX/97f;->k()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel$OptionsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel$OptionsModel;->a()LX/0Px;

    move-result-object v11

    invoke-virtual {v11}, LX/0Px;->size()I

    move-result v12

    const/4 v0, 0x0

    move v9, v0

    :goto_0
    if-ge v9, v12, :cond_5

    invoke-virtual {v11, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel$OptionsModel$EdgesModel;

    .line 1807121
    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel$OptionsModel$EdgesModel;->a()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsOptionModel;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel$OptionsModel$EdgesModel;->a()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsOptionModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsOptionModel;->c()Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    move-result-object v2

    invoke-static {v2}, LX/Bgg;->a(Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1807122
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;->ADD_VALUE:Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel$OptionsModel$EdgesModel;->a()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsOptionModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsOptionModel;->c()Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 1807123
    invoke-interface {p2}, LX/97e;->e()Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    move-result-object v3

    if-eqz v3, :cond_4

    invoke-interface {p2}, LX/97e;->e()Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    move-result-object v3

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel$OptionsModel$EdgesModel;->a()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsOptionModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsOptionModel;->c()Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v5, 0x1

    .line 1807124
    :goto_1
    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel$OptionsModel$EdgesModel;->a()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsOptionModel;

    move-result-object v3

    invoke-static {v3, v5}, LX/Bgb;->a(Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsOptionModel;Z)Ljava/lang/String;

    move-result-object v3

    .line 1807125
    if-eqz v2, :cond_2

    invoke-interface {p2}, LX/97e;->e()Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;->DOESNT_HAVE_VALUE:Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    invoke-virtual {v2, v4}, Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 1807126
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel$OptionsModel$EdgesModel;->a()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsOptionModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsOptionModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel$OptionsModel$EdgesModel;->a()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsOptionModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsOptionModel;->c()Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    move-result-object v7

    move-object v0, p0

    move-object v4, p2

    move-object/from16 v6, p4

    move-object/from16 v8, p3

    invoke-static/range {v0 .. v8}, LX/Bg8;->a(LX/Bg8;LX/5OG;Ljava/lang/String;Ljava/lang/String;LX/97f;ZLX/Bh0;Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;LX/BgS;)V

    .line 1807127
    :cond_3
    add-int/lit8 v0, v9, 0x1

    move v9, v0

    goto :goto_0

    .line 1807128
    :cond_4
    const/4 v5, 0x0

    goto :goto_1

    .line 1807129
    :cond_5
    invoke-virtual {v1}, LX/5OG;->getCount()I

    move-result v0

    if-lez v0, :cond_6

    invoke-static {v10}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    :goto_2
    return-object v0

    :cond_6
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    goto :goto_2
.end method

.method private static a(LX/Bg8;LX/5OG;Ljava/lang/String;Ljava/lang/String;LX/97f;ZLX/Bh0;Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;LX/BgS;)V
    .locals 7
    .param p5    # Z
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1807106
    iget-object v0, p0, LX/Bg8;->a:LX/D9E;

    invoke-virtual {v0, p1, p2}, LX/D9E;->a(LX/5OG;Ljava/lang/CharSequence;)Lcom/facebook/widget/popovermenuitemwithuriicon/PopoverMenuItemWithUriIcon;

    move-result-object v6

    .line 1807107
    if-eqz p6, :cond_0

    .line 1807108
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;->ADD_VALUE:Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    invoke-virtual {v0, p7}, Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 1807109
    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v6, v0}, LX/3Ai;->setCheckable(Z)Landroid/view/MenuItem;

    .line 1807110
    invoke-virtual {v6, p5}, LX/3Ai;->setChecked(Z)Landroid/view/MenuItem;

    .line 1807111
    new-instance v0, LX/Bg7;

    move-object v1, p0

    move-object v2, p6

    move-object v3, p7

    move-object v4, p4

    move-object v5, p8

    invoke-direct/range {v0 .. v5}, LX/Bg7;-><init>(LX/Bg8;LX/Bh0;Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;LX/97f;LX/BgS;)V

    invoke-virtual {v6, v0}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1807112
    invoke-virtual {v6, p3}, Lcom/facebook/widget/popovermenuitemwithuriicon/PopoverMenuItemWithUriIcon;->a(Ljava/lang/String;)Landroid/view/MenuItem;

    .line 1807113
    invoke-virtual {p1, v6}, LX/5OG;->a(LX/3Ai;)V

    .line 1807114
    return-void

    .line 1807115
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/Bg8;
    .locals 2

    .prologue
    .line 1807097
    new-instance v1, LX/Bg8;

    const-class v0, LX/D9E;

    invoke-interface {p0, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/D9E;

    invoke-direct {v1, v0}, LX/Bg8;-><init>(LX/D9E;)V

    .line 1807098
    return-object v1
.end method


# virtual methods
.method public final a(Landroid/view/View;LX/97f;LX/BgS;LX/Bh0;)Landroid/view/View$OnClickListener;
    .locals 2
    .param p4    # LX/Bh0;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1807099
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0, p2, p3, p4}, LX/Bg8;->a(Landroid/content/Context;LX/97f;LX/BgS;LX/Bh0;)LX/0am;

    move-result-object v1

    .line 1807100
    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1807101
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1807102
    const/4 v0, 0x0

    .line 1807103
    :goto_0
    return-object v0

    .line 1807104
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1807105
    new-instance v0, LX/Bg6;

    invoke-direct {v0, p0, v1, p1}, LX/Bg6;-><init>(LX/Bg8;LX/0am;Landroid/view/View;)V

    goto :goto_0
.end method
