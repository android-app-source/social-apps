.class public LX/AWa;
.super LX/AWT;
.source ""


# instance fields
.field public a:LX/AWf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/1FZ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/6Rf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final f:Landroid/widget/FrameLayout;

.field public final g:Lcom/facebook/user/tiles/UserTileView;

.field public final h:Landroid/widget/ImageView;

.field private final i:Lcom/facebook/widget/text/BetterTextView;

.field public j:LX/AUp;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1683114
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/AWa;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1683115
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1683116
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/AWa;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1683117
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1683118
    invoke-direct {p0, p1, p2, p3}, LX/AWT;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1683119
    const-class v0, LX/AWa;

    invoke-static {v0, p0}, LX/AWa;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1683120
    const v0, 0x7f0305a5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1683121
    const v0, 0x7f0d0f77

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, LX/AWa;->f:Landroid/widget/FrameLayout;

    .line 1683122
    const v0, 0x7f0d0f79

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/tiles/UserTileView;

    iput-object v0, p0, LX/AWa;->g:Lcom/facebook/user/tiles/UserTileView;

    .line 1683123
    const v0, 0x7f0d0f78

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/AWa;->h:Landroid/widget/ImageView;

    .line 1683124
    const v0, 0x7f0d0f7a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/AWa;->i:Lcom/facebook/widget/text/BetterTextView;

    .line 1683125
    iget-object v1, p0, LX/AWa;->i:Lcom/facebook/widget/text/BetterTextView;

    iget-object v0, p0, LX/AWa;->c:LX/6Rf;

    .line 1683126
    iget-object p0, v0, LX/6Rf;->a:LX/0Uh;

    const/16 p1, 0x339

    const/4 p2, 0x0

    invoke-virtual {p0, p1, p2}, LX/0Uh;->a(IZ)Z

    move-result p0

    move v0, p0

    .line 1683127
    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 1683128
    return-void

    .line 1683129
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p1, LX/AWa;

    invoke-static {v3}, LX/AWf;->b(LX/0QB;)LX/AWf;

    move-result-object v1

    check-cast v1, LX/AWf;

    invoke-static {v3}, LX/1Et;->a(LX/0QB;)LX/1FZ;

    move-result-object v2

    check-cast v2, LX/1FZ;

    new-instance v0, LX/6Rf;

    invoke-static {v3}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object p0

    check-cast p0, LX/0Uh;

    invoke-direct {v0, p0}, LX/6Rf;-><init>(LX/0Uh;)V

    move-object v3, v0

    check-cast v3, LX/6Rf;

    iput-object v1, p1, LX/AWa;->a:LX/AWf;

    iput-object v2, p1, LX/AWa;->b:LX/1FZ;

    iput-object v3, p1, LX/AWa;->c:LX/6Rf;

    return-void
.end method

.method public static g(LX/AWa;)V
    .locals 5

    .prologue
    .line 1683130
    iget-object v0, p0, LX/AWa;->j:LX/AUp;

    if-nez v0, :cond_0

    .line 1683131
    :goto_0
    return-void

    .line 1683132
    :cond_0
    iget-object v0, p0, LX/AWa;->j:LX/AUp;

    .line 1683133
    iget-object v1, p0, LX/AWa;->b:LX/1FZ;

    iget-object v2, p0, LX/AWa;->f:Landroid/widget/FrameLayout;

    invoke-virtual {v2}, Landroid/widget/FrameLayout;->getWidth()I

    move-result v2

    iget-object v3, p0, LX/AWa;->f:Landroid/widget/FrameLayout;

    invoke-virtual {v3}, Landroid/widget/FrameLayout;->getHeight()I

    move-result v3

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v1, v2, v3, v4}, LX/1FZ;->a(IILandroid/graphics/Bitmap$Config;)LX/1FJ;

    move-result-object v2

    .line 1683134
    invoke-virtual {v2}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Bitmap;

    .line 1683135
    if-nez v1, :cond_1

    .line 1683136
    invoke-virtual {v2}, LX/1FJ;->close()V

    .line 1683137
    const/4 v1, 0x0

    .line 1683138
    :goto_1
    move-object v1, v1

    .line 1683139
    invoke-virtual {v0, v1}, LX/AUp;->a(LX/1FJ;)V

    goto :goto_0

    .line 1683140
    :cond_1
    new-instance v3, Landroid/graphics/Canvas;

    invoke-direct {v3, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 1683141
    iget-object v1, p0, LX/AWa;->f:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v3}, Landroid/widget/FrameLayout;->draw(Landroid/graphics/Canvas;)V

    move-object v1, v2

    .line 1683142
    goto :goto_1
.end method
