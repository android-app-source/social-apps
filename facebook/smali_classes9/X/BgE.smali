.class public final LX/BgE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Landroid/content/Intent;

.field public final synthetic b:LX/BgK;


# direct methods
.method public constructor <init>(LX/BgK;Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 1807282
    iput-object p1, p0, LX/BgE;->b:LX/BgK;

    iput-object p2, p0, LX/BgE;->a:Landroid/content/Intent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x3ac53d19

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1807283
    iget-object v1, p0, LX/BgE;->b:LX/BgK;

    iget-object v1, v1, LX/BgK;->t:Lcom/facebook/crowdsourcing/suggestedits/fragment/SuggestEditsFragment;

    invoke-virtual {v1}, Lcom/facebook/crowdsourcing/suggestedits/fragment/SuggestEditsFragment;->b()V

    .line 1807284
    iget-object v1, p0, LX/BgE;->a:Landroid/content/Intent;

    if-eqz v1, :cond_0

    .line 1807285
    iget-object v1, p0, LX/BgE;->b:LX/BgK;

    iget-object v1, v1, LX/BgK;->i:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/BgE;->a:Landroid/content/Intent;

    iget-object v3, p0, LX/BgE;->b:LX/BgK;

    iget-object v3, v3, LX/BgK;->d:Landroid/content/Context;

    invoke-interface {v1, v2, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1807286
    :goto_0
    const v1, -0x6fd45bc5

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 1807287
    :cond_0
    iget-object v1, p0, LX/BgE;->b:LX/BgK;

    iget-object v1, v1, LX/BgK;->e:LX/03V;

    sget-object v2, LX/BgK;->a:Ljava/lang/String;

    const-string v3, "Could not launch Post Suggest Edits Upsell intent"

    invoke-virtual {v1, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
