.class public LX/C4a;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/C4Y;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/C4c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1847572
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/C4a;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/C4c;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1847573
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1847574
    iput-object p1, p0, LX/C4a;->b:LX/0Ot;

    .line 1847575
    return-void
.end method

.method public static a(LX/0QB;)LX/C4a;
    .locals 4

    .prologue
    .line 1847576
    const-class v1, LX/C4a;

    monitor-enter v1

    .line 1847577
    :try_start_0
    sget-object v0, LX/C4a;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1847578
    sput-object v2, LX/C4a;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1847579
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1847580
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1847581
    new-instance v3, LX/C4a;

    const/16 p0, 0x1efa

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/C4a;-><init>(LX/0Ot;)V

    .line 1847582
    move-object v0, v3

    .line 1847583
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1847584
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C4a;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1847585
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1847586
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;LX/1De;)LX/1dQ;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            "LX/1De;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1847587
    const v0, -0x584d861f

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 1847588
    check-cast p2, LX/C4Z;

    .line 1847589
    iget-object v0, p0, LX/C4a;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/C4c;

    iget-object v1, p2, LX/C4Z;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1847590
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    const/4 v3, 0x3

    const v4, 0x7f0b00d5

    invoke-interface {v2, v3, v4}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v2

    const/4 p0, 0x2

    .line 1847591
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, p0}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    const v4, 0x7f0b1bc4

    invoke-interface {v3, v4}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x6

    const v5, 0x7f0b1bc5

    invoke-interface {v3, v4, v5}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, p0}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x3

    invoke-interface {v3, v4}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v3

    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a00d5

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-interface {v3, v4}, LX/1Dh;->W(I)LX/1Dh;

    move-result-object v3

    .line 1847592
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v4

    .line 1847593
    invoke-static {v1}, LX/C4c;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v5

    .line 1847594
    if-eqz v5, :cond_0

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->x()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object p0

    if-eqz p0, :cond_0

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->x()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p0

    if-eqz p0, :cond_2

    .line 1847595
    :cond_0
    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const p0, 0x7f0828a3

    invoke-virtual {v5, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object v5, v5

    .line 1847596
    :goto_0
    move-object v5, v5

    .line 1847597
    invoke-virtual {v4, v5}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v4

    const v5, 0x7f0a00e5

    invoke-virtual {v4, v5}, LX/1ne;->n(I)LX/1ne;

    move-result-object v4

    const v5, 0x7f0b1bc6

    invoke-virtual {v4, v5}, LX/1ne;->q(I)LX/1ne;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, LX/1ne;->j(I)LX/1ne;

    move-result-object v4

    sget-object v5, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v4, v5}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v4

    sget-object v5, LX/1nd;->CENTER:LX/1nd;

    invoke-virtual {v4, v5}, LX/1ne;->a(LX/1nd;)LX/1ne;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    const/4 v5, 0x2

    const p0, 0x7f0b1bc5

    invoke-interface {v4, v5, p0}, LX/1Di;->c(II)LX/1Di;

    move-result-object v4

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-interface {v4, v5}, LX/1Di;->a(F)LX/1Di;

    move-result-object v4

    move-object v4, v4

    .line 1847598
    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    .line 1847599
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v4

    .line 1847600
    iget-object v5, v0, LX/C4c;->b:LX/23P;

    .line 1847601
    invoke-static {v1}, LX/C4c;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object p0

    .line 1847602
    if-eqz p0, :cond_1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aZ()Ljava/lang/String;

    move-result-object p2

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p2

    if-eqz p2, :cond_3

    .line 1847603
    :cond_1
    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    const p2, 0x7f0828a4

    invoke-virtual {p0, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    move-object p0, p0

    .line 1847604
    :goto_1
    move-object p0, p0

    .line 1847605
    const/4 p2, 0x0

    invoke-virtual {v5, p0, p2}, LX/23P;->getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v5

    move-object v5, v5

    .line 1847606
    invoke-virtual {v4, v5}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v4

    const v5, 0x7f0b1bc6

    invoke-virtual {v4, v5}, LX/1ne;->q(I)LX/1ne;

    move-result-object v4

    const v5, 0x7f0a00d2

    invoke-virtual {v4, v5}, LX/1ne;->n(I)LX/1ne;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, LX/1ne;->j(I)LX/1ne;

    move-result-object v4

    sget-object v5, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v4, v5}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    const v5, 0x7f020bbb

    invoke-interface {v4, v5}, LX/1Di;->x(I)LX/1Di;

    move-result-object v4

    const/4 v5, 0x7

    const p0, 0x7f0b1bc7

    invoke-interface {v4, v5, p0}, LX/1Di;->g(II)LX/1Di;

    move-result-object v4

    const/4 v5, 0x6

    const p0, 0x7f0b1bc5

    invoke-interface {v4, v5, p0}, LX/1Di;->g(II)LX/1Di;

    move-result-object v4

    const/16 v5, 0x8

    const p0, 0x7f0b1bc8

    invoke-interface {v4, v5, p0}, LX/1Di;->c(II)LX/1Di;

    move-result-object v4

    move-object v4, v4

    .line 1847607
    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    .line 1847608
    const v4, -0x584d861f

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 p0, 0x0

    aput-object p1, v5, p0

    invoke-static {p1, v4, v5}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v4

    move-object v4, v4

    .line 1847609
    invoke-interface {v3, v4}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v3

    move-object v3, v3

    .line 1847610
    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 1847611
    return-object v0

    :cond_2
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->x()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aZ()Ljava/lang/String;

    move-result-object p0

    goto :goto_1
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 1847612
    invoke-static {}, LX/1dS;->b()V

    .line 1847613
    iget v0, p1, LX/1dQ;->b:I

    .line 1847614
    packed-switch v0, :pswitch_data_0

    .line 1847615
    :goto_0
    return-object v3

    .line 1847616
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 1847617
    iget-object v0, p1, LX/1dQ;->c:[Ljava/lang/Object;

    const/4 v2, 0x0

    aget-object v0, v0, v2

    check-cast v0, LX/1De;

    iget-object v2, p1, LX/1dQ;->a:LX/1X1;

    .line 1847618
    check-cast v2, LX/C4Z;

    .line 1847619
    iget-object v4, p0, LX/C4a;->b:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/C4c;

    iget-object v5, v2, LX/C4Z;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v6, v2, LX/C4Z;->b:LX/0Px;

    .line 1847620
    invoke-virtual {v0}, LX/1De;->getBaseContext()Landroid/content/Context;

    move-result-object p2

    .line 1847621
    instance-of p1, p2, LX/0fC;

    if-eqz p1, :cond_0

    move-object p1, p2

    .line 1847622
    check-cast p1, LX/0fC;

    .line 1847623
    invoke-static {}, Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;->newBuilder()Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration$Builder;

    move-result-object v2

    .line 1847624
    iget-object p0, v5, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object p0, p0

    .line 1847625
    check-cast p0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v2, p0}, Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration$Builder;->setStoryId(Ljava/lang/String;)Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration$Builder;

    move-result-object p0

    invoke-virtual {p0, v6}, Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration$Builder;->setAppliedInspirations(LX/0Px;)Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration$Builder;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration$Builder;->a()Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;

    move-result-object p0

    invoke-interface {p1, p0}, LX/0fC;->a(Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;)V

    .line 1847626
    :cond_0
    instance-of p1, p2, LX/0fD;

    if-eqz p1, :cond_1

    .line 1847627
    check-cast p2, LX/0fD;

    .line 1847628
    invoke-interface {p2}, LX/0fD;->f()LX/0fK;

    move-result-object p1

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, LX/0fK;->a_(Z)V

    .line 1847629
    :cond_1
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x584d861f
        :pswitch_0
    .end packed-switch
.end method
