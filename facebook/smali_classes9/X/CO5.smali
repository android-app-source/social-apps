.class public LX/CO5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/CNe;


# instance fields
.field private final a:LX/CNb;

.field private final b:LX/CNq;


# direct methods
.method public constructor <init>(LX/CNb;LX/CNq;)V
    .locals 0

    .prologue
    .line 1882536
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1882537
    iput-object p1, p0, LX/CO5;->a:LX/CNb;

    .line 1882538
    iput-object p2, p0, LX/CO5;->b:LX/CNq;

    .line 1882539
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 12

    .prologue
    .line 1882540
    iget-object v0, p0, LX/CO5;->a:LX/CNb;

    const-string v1, "filter_type_sets"

    invoke-virtual {v0, v1}, LX/CNb;->d(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    .line 1882541
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v6

    .line 1882542
    if-eqz v0, :cond_0

    .line 1882543
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterTypeSet;

    .line 1882544
    invoke-static {v1}, LX/5eL;->a(Lcom/facebook/graphql/model/GraphQLGraphSearchQueryFilterTypeSet;)Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFiltersFragmentModel;

    move-result-object v1

    .line 1882545
    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFiltersFragmentModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v6, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 1882546
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 1882547
    :cond_0
    iget-object v0, p0, LX/CO5;->b:LX/CNq;

    iget-object v7, v0, LX/CNc;->c:Landroid/content/Context;

    .line 1882548
    iget-object v0, p0, LX/CO5;->b:LX/CNq;

    .line 1882549
    iget-object v1, v0, LX/CNq;->d:LX/CNs;

    iget-object v1, v1, LX/CNs;->j:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1nD;

    move-object v0, v1

    .line 1882550
    iget-object v1, p0, LX/CO5;->b:LX/CNq;

    invoke-virtual {v1}, LX/CNq;->e()Lcom/facebook/content/SecureContextHelper;

    move-result-object v8

    .line 1882551
    iget-object v1, p0, LX/CO5;->a:LX/CNb;

    const-string v2, "display_style"

    invoke-virtual {v1, v2}, LX/CNb;->d(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v2, p0, LX/CO5;->a:LX/CNb;

    const-string v3, "title"

    invoke-virtual {v2, v3}, LX/CNb;->d(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iget-object v3, p0, LX/CO5;->a:LX/CNb;

    const-string v4, "query_function"

    invoke-virtual {v3, v4}, LX/CNb;->d(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    iget-object v4, p0, LX/CO5;->a:LX/CNb;

    const-string v5, "source"

    invoke-virtual {v4, v5}, LX/CNb;->d(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    iget-object v5, p0, LX/CO5;->a:LX/CNb;

    const-string v9, "preloaded_story_ids"

    invoke-virtual {v5, v9}, LX/CNb;->d(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0Px;

    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v6

    const/4 p0, 0x0

    .line 1882552
    invoke-static {v1}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-result-object v9

    .line 1882553
    iget-object v10, v0, LX/1nD;->g:LX/0P1;

    invoke-virtual {v10, v9}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    .line 1882554
    if-nez v9, :cond_4

    .line 1882555
    iget-object v9, v0, LX/1nD;->f:LX/0Uh;

    sget v10, LX/2SU;->B:I

    invoke-virtual {v9, v10, p0}, LX/0Uh;->a(IZ)Z

    move-result v9

    if-eqz v9, :cond_3

    sget-object v9, LX/0cQ;->SEARCH_RESULTS_FRAGMENT:LX/0cQ;

    invoke-virtual {v9}, LX/0cQ;->ordinal()I

    move-result v9

    :goto_1
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    move-object v10, v9

    .line 1882556
    :goto_2
    new-instance v11, Landroid/content/Intent;

    invoke-direct {v11}, Landroid/content/Intent;-><init>()V

    iget-object v9, v0, LX/1nD;->c:LX/0Or;

    invoke-interface {v9}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/content/ComponentName;

    invoke-virtual {v11, v9}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v9

    const-string v11, "target_fragment"

    invoke-virtual {v9, v11, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v9

    const-string v10, "display_style"

    invoke-virtual {v9, v10, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v9

    const-string v10, "query_title"

    invoke-virtual {v9, v10, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v9

    const-string v10, "query_function"

    invoke-virtual {v9, v10, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v9

    const-string v10, "source"

    invoke-virtual {v9, v10, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v10

    .line 1882557
    if-eqz v5, :cond_1

    .line 1882558
    const-string v11, "preloaded_story_ids"

    new-array v9, p0, [Ljava/lang/String;

    invoke-virtual {v5, v9}, LX/0Px;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [Ljava/lang/String;

    invoke-virtual {v10, v11, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 1882559
    :cond_1
    if-eqz v6, :cond_2

    .line 1882560
    const-string v9, "filters"

    invoke-static {v10, v9, v6}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/util/List;)V

    .line 1882561
    :cond_2
    move-object v0, v10

    .line 1882562
    invoke-interface {v8, v0, v7}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1882563
    return-void

    .line 1882564
    :cond_3
    sget-object v9, LX/0cQ;->SEARCH_RESULTS_FEED_FRAGMENT:LX/0cQ;

    invoke-virtual {v9}, LX/0cQ;->ordinal()I

    move-result v9

    goto :goto_1

    :cond_4
    move-object v10, v9

    goto :goto_2
.end method
