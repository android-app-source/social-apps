.class public LX/CId;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/CGs;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "LX/0jT;",
        ">",
        "Ljava/lang/Object;",
        "LX/CGs",
        "<",
        "LX/0zO",
        "<TT;>;>;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Z

.field public h:Z

.field public final i:I

.field public final j:I

.field public k:I

.field public l:LX/0zS;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1874463
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1874464
    const/16 v0, 0x3e8

    iput v0, p0, LX/CId;->k:I

    .line 1874465
    sget-object v0, LX/Cj6;->c:LX/0zS;

    iput-object v0, p0, LX/CId;->l:LX/0zS;

    .line 1874466
    const/4 v0, 0x0

    iput-object v0, p0, LX/CId;->c:Ljava/lang/String;

    .line 1874467
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 1874468
    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v1, p0, LX/CId;->i:I

    .line 1874469
    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iput v0, p0, LX/CId;->j:I

    .line 1874470
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1874410
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1874411
    const/16 v0, 0x3e8

    iput v0, p0, LX/CId;->k:I

    .line 1874412
    sget-object v0, LX/Cj6;->c:LX/0zS;

    iput-object v0, p0, LX/CId;->l:LX/0zS;

    .line 1874413
    iput-object p2, p0, LX/CId;->c:Ljava/lang/String;

    .line 1874414
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 1874415
    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v1, p0, LX/CId;->i:I

    .line 1874416
    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iput v0, p0, LX/CId;->j:I

    .line 1874417
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 5

    .prologue
    .line 1874418
    iget-boolean v0, p0, LX/CId;->h:Z

    .line 1874419
    iget-object v1, p0, LX/CId;->d:Ljava/lang/String;

    move-object v1, v1

    .line 1874420
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1874421
    new-instance v1, LX/CHU;

    invoke-direct {v1}, LX/CHU;-><init>()V

    move-object v1, v1

    .line 1874422
    const-string v2, "documentID"

    .line 1874423
    iget-object v3, p0, LX/CId;->d:Ljava/lang/String;

    move-object v3, v3

    .line 1874424
    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "maxElements"

    .line 1874425
    iget v3, p0, LX/CId;->k:I

    move v3, v3

    .line 1874426
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "scale"

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v1

    move-object v1, v1

    .line 1874427
    :goto_0
    if-eqz v0, :cond_0

    .line 1874428
    const-string v2, "final_image_width"

    .line 1874429
    iget v3, p0, LX/CId;->i:I

    move v3, v3

    .line 1874430
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v2

    const-string v3, "final_image_width"

    .line 1874431
    iget v4, p0, LX/CId;->j:I

    move v4, v4

    .line 1874432
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1874433
    :cond_0
    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    move-object v0, v1

    .line 1874434
    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, LX/0zO;->a(J)LX/0zO;

    .line 1874435
    iget-object v1, p0, LX/CId;->l:LX/0zS;

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    .line 1874436
    iget-object v1, p0, LX/CId;->e:Ljava/lang/String;

    invoke-static {v1}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v1

    .line 1874437
    iput-object v1, v0, LX/0zO;->d:Ljava/util/Set;

    .line 1874438
    return-object v0

    .line 1874439
    :cond_1
    iget-object v1, p0, LX/CId;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1874440
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v1, 0x1

    .line 1874441
    :goto_1
    if-eqz v1, :cond_3

    .line 1874442
    new-instance v1, LX/CHT;

    invoke-direct {v1}, LX/CHT;-><init>()V

    move-object v1, v1

    .line 1874443
    const-string v2, "shoppingCatalogID"

    .line 1874444
    iget-object v3, p0, LX/CId;->c:Ljava/lang/String;

    move-object v3, v3

    .line 1874445
    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "maxElements"

    .line 1874446
    iget v3, p0, LX/CId;->k:I

    move v3, v3

    .line 1874447
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "product_id"

    .line 1874448
    iget-object v3, p0, LX/CId;->a:Ljava/lang/String;

    move-object v3, v3

    .line 1874449
    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "product_view"

    .line 1874450
    iget-object v3, p0, LX/CId;->b:Ljava/lang/String;

    move-object v3, v3

    .line 1874451
    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "scale"

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v1

    move-object v1, v1

    .line 1874452
    goto :goto_0

    .line 1874453
    :cond_2
    const/4 v1, 0x0

    goto :goto_1

    .line 1874454
    :cond_3
    new-instance v1, LX/CHS;

    invoke-direct {v1}, LX/CHS;-><init>()V

    move-object v1, v1

    .line 1874455
    const-string v2, "shoppingCatalogID"

    .line 1874456
    iget-object v3, p0, LX/CId;->c:Ljava/lang/String;

    move-object v3, v3

    .line 1874457
    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "view"

    .line 1874458
    iget-object v3, p0, LX/CId;->f:Ljava/lang/String;

    move-object v3, v3

    .line 1874459
    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "maxElements"

    .line 1874460
    iget v3, p0, LX/CId;->k:I

    move v3, v3

    .line 1874461
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "scale"

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v1

    move-object v1, v1

    .line 1874462
    goto/16 :goto_0
.end method
