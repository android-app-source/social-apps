.class public final LX/BbZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic b:LX/Bba;


# direct methods
.method public constructor <init>(LX/Bba;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 0

    .prologue
    .line 1800881
    iput-object p1, p0, LX/BbZ;->b:LX/Bba;

    iput-object p2, p0, LX/BbZ;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v4, 0x2

    const v0, -0x276a1529

    invoke-static {v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 1800882
    iget-object v0, p0, LX/BbZ;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 1800883
    if-nez v0, :cond_0

    .line 1800884
    iget-object v0, p0, LX/BbZ;->b:LX/Bba;

    iget-object v0, v0, LX/Bba;->e:LX/03V;

    sget-object v1, LX/Bba;->a:Ljava/lang/String;

    const-string v3, "Null story on Invite Friends CTA"

    invoke-virtual {v0, v1, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1800885
    const v0, -0x1487557b

    invoke-static {v4, v4, v0, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1800886
    :goto_0
    return-void

    .line 1800887
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v3

    .line 1800888
    if-nez v3, :cond_1

    .line 1800889
    iget-object v0, p0, LX/BbZ;->b:LX/Bba;

    iget-object v0, v0, LX/Bba;->e:LX/03V;

    sget-object v1, LX/Bba;->a:Ljava/lang/String;

    const-string v3, "Null story ID on Invite Friends CTA"

    invoke-virtual {v0, v1, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1800890
    const v0, -0x162e7b48

    invoke-static {v0, v2}, LX/02F;->a(II)V

    goto :goto_0

    .line 1800891
    :cond_1
    iget-object v0, p0, LX/BbZ;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1800892
    iget-object v4, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v4

    .line 1800893
    if-eqz v0, :cond_2

    iget-object v0, p0, LX/BbZ;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1800894
    iget-object v4, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v4

    .line 1800895
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/BbZ;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1800896
    iget-object v4, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v4

    .line 1800897
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_3

    .line 1800898
    :cond_2
    iget-object v0, p0, LX/BbZ;->b:LX/Bba;

    iget-object v0, v0, LX/Bba;->e:LX/03V;

    sget-object v1, LX/Bba;->a:Ljava/lang/String;

    const-string v3, "Null PlaceList ID on Invite Friends CTA"

    invoke-virtual {v0, v1, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1800899
    const v0, -0x7c474957

    invoke-static {v0, v2}, LX/02F;->a(II)V

    goto :goto_0

    .line 1800900
    :cond_3
    new-instance v4, Landroid/content/Intent;

    iget-object v0, p0, LX/BbZ;->b:LX/Bba;

    iget-object v0, v0, LX/Bba;->b:Landroid/content/Context;

    const-class v5, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;

    invoke-direct {v4, v0, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1800901
    const-string v0, "invite_friends_story_id"

    invoke-virtual {v4, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1800902
    const-string v3, "invite_friends_placelist_id"

    iget-object v0, p0, LX/BbZ;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1800903
    iget-object v5, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v5

    .line 1800904
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1800905
    iget-object v0, p0, LX/BbZ;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, LX/BbT;->f(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    .line 1800906
    :goto_1
    const-string v1, "invite_friends_can_search_all_friends"

    invoke-virtual {v4, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1800907
    iget-object v0, p0, LX/BbZ;->b:LX/Bba;

    iget-object v0, v0, LX/Bba;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    iget-object v1, p0, LX/BbZ;->b:LX/Bba;

    iget-object v1, v1, LX/Bba;->b:Landroid/content/Context;

    invoke-interface {v0, v4, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1800908
    const v0, 0x56309508

    invoke-static {v0, v2}, LX/02F;->a(II)V

    goto/16 :goto_0

    .line 1800909
    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method
