.class public final enum LX/Agq;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Agq;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Agq;

.field public static final enum CHECKOUT_LOADER:LX/Agq;

.field public static final enum PAYMENTS_COMPONENT_WITHOUT_UI_PROGRESS:LX/Agq;

.field public static final enum PAYMENTS_COMPONENT_WITH_UI_PROGRESS:LX/Agq;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1701539
    new-instance v0, LX/Agq;

    const-string v1, "CHECKOUT_LOADER"

    invoke-direct {v0, v1, v2}, LX/Agq;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Agq;->CHECKOUT_LOADER:LX/Agq;

    .line 1701540
    new-instance v0, LX/Agq;

    const-string v1, "PAYMENTS_COMPONENT_WITH_UI_PROGRESS"

    invoke-direct {v0, v1, v3}, LX/Agq;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Agq;->PAYMENTS_COMPONENT_WITH_UI_PROGRESS:LX/Agq;

    .line 1701541
    new-instance v0, LX/Agq;

    const-string v1, "PAYMENTS_COMPONENT_WITHOUT_UI_PROGRESS"

    invoke-direct {v0, v1, v4}, LX/Agq;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Agq;->PAYMENTS_COMPONENT_WITHOUT_UI_PROGRESS:LX/Agq;

    .line 1701542
    const/4 v0, 0x3

    new-array v0, v0, [LX/Agq;

    sget-object v1, LX/Agq;->CHECKOUT_LOADER:LX/Agq;

    aput-object v1, v0, v2

    sget-object v1, LX/Agq;->PAYMENTS_COMPONENT_WITH_UI_PROGRESS:LX/Agq;

    aput-object v1, v0, v3

    sget-object v1, LX/Agq;->PAYMENTS_COMPONENT_WITHOUT_UI_PROGRESS:LX/Agq;

    aput-object v1, v0, v4

    sput-object v0, LX/Agq;->$VALUES:[LX/Agq;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1701543
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Agq;
    .locals 1

    .prologue
    .line 1701544
    const-class v0, LX/Agq;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Agq;

    return-object v0
.end method

.method public static values()[LX/Agq;
    .locals 1

    .prologue
    .line 1701545
    sget-object v0, LX/Agq;->$VALUES:[LX/Agq;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Agq;

    return-object v0
.end method
