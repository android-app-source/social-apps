.class public final LX/BUn;
.super LX/3Ag;
.source ""


# instance fields
.field public final synthetic c:LX/BUo;

.field public d:Landroid/os/Handler;

.field public e:Ljava/lang/Runnable;

.field public f:Z


# direct methods
.method public constructor <init>(LX/BUo;Landroid/content/Context;)V
    .locals 10

    .prologue
    .line 1789573
    iput-object p1, p0, LX/BUn;->c:LX/BUo;

    .line 1789574
    invoke-direct {p0, p2}, LX/3Ag;-><init>(Landroid/content/Context;)V

    .line 1789575
    const/4 v3, 0x1

    .line 1789576
    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p0}, LX/BUn;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, LX/BUn;->d:Landroid/os/Handler;

    .line 1789577
    invoke-virtual {p0, v3}, LX/BUn;->requestWindowFeature(I)Z

    .line 1789578
    const v0, 0x7f031599

    invoke-virtual {p0, v0}, LX/BUn;->setContentView(I)V

    .line 1789579
    invoke-virtual {p0}, LX/BUn;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1789580
    invoke-virtual {p0}, LX/BUn;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 1789581
    const/4 v1, -0x1

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 1789582
    const/4 v1, -0x2

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 1789583
    const/16 v1, 0x50

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 1789584
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    and-int/lit8 v1, v1, -0x3

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 1789585
    invoke-virtual {p0}, LX/BUn;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 1789586
    const v0, 0x7f0d30b4

    invoke-virtual {p0, v0}, LX/BUn;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 1789587
    iget-object v1, p0, LX/BUn;->c:LX/BUo;

    iget-object v1, v1, LX/BUo;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f081a30

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, LX/BUn;->c:LX/BUo;

    iget-object v5, v5, LX/BUo;->b:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1789588
    const v0, 0x7f0d30b5

    invoke-virtual {p0, v0}, LX/BUn;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 1789589
    new-instance v1, LX/BUk;

    invoke-direct {v1, p0, v0}, LX/BUk;-><init>(LX/BUn;Lcom/facebook/resources/ui/FbTextView;)V

    .line 1789590
    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1789591
    iget-object v6, p0, LX/BUn;->c:LX/BUo;

    iget-wide v6, v6, LX/BUo;->c:J

    const-wide/16 v8, -0x1

    cmp-long v6, v6, v8

    if-eqz v6, :cond_0

    .line 1789592
    new-instance v6, Lcom/facebook/video/followvideos/FollowVideosNotificationUpsellDialogBuilder$NotificationDialog$2;

    invoke-direct {v6, p0}, Lcom/facebook/video/followvideos/FollowVideosNotificationUpsellDialogBuilder$NotificationDialog$2;-><init>(LX/BUn;)V

    iput-object v6, p0, LX/BUn;->e:Ljava/lang/Runnable;

    .line 1789593
    new-instance v6, LX/BUl;

    invoke-direct {v6, p0}, LX/BUl;-><init>(LX/BUn;)V

    invoke-virtual {p0, v6}, LX/BUn;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 1789594
    new-instance v6, LX/BUm;

    invoke-direct {v6, p0}, LX/BUm;-><init>(LX/BUn;)V

    invoke-virtual {p0, v6}, LX/BUn;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 1789595
    :cond_0
    return-void
.end method


# virtual methods
.method public final dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    .line 1789596
    iget-object v0, p0, LX/BUn;->c:LX/BUo;

    iget-wide v0, v0, LX/BUo;->c:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 1789597
    iget-object v0, p0, LX/BUn;->d:Landroid/os/Handler;

    iget-object v1, p0, LX/BUn;->e:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1789598
    iget-object v0, p0, LX/BUn;->d:Landroid/os/Handler;

    iget-object v1, p0, LX/BUn;->e:Ljava/lang/Runnable;

    iget-object v2, p0, LX/BUn;->c:LX/BUo;

    iget-wide v2, v2, LX/BUo;->c:J

    const v4, -0xfa7a82c

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 1789599
    :cond_0
    invoke-super {p0, p1}, LX/3Ag;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method
