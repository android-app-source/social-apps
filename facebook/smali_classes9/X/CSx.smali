.class public final LX/CSx;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformPartialScreenQueryModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/CT9;

.field public final synthetic b:LX/0Px;

.field public final synthetic c:LX/4Hw;

.field public final synthetic d:LX/CSz;


# direct methods
.method public constructor <init>(LX/CSz;LX/CT9;LX/0Px;LX/4Hw;)V
    .locals 0

    .prologue
    .line 1895159
    iput-object p1, p0, LX/CSx;->d:LX/CSz;

    iput-object p2, p0, LX/CSx;->a:LX/CT9;

    iput-object p3, p0, LX/CSx;->b:LX/0Px;

    iput-object p4, p0, LX/CSx;->c:LX/4Hw;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 1895160
    iget-object v0, p0, LX/CSx;->a:LX/CT9;

    if-eqz v0, :cond_0

    .line 1895161
    iget-object v0, p0, LX/CSx;->a:LX/CT9;

    invoke-virtual {v0}, LX/CT9;->a()V

    .line 1895162
    :cond_0
    iget-object v0, p0, LX/CSx;->d:LX/CSz;

    iget-object v0, v0, LX/CSz;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CXd;

    sget-object v1, LX/CXc;->DATA_FETCH:LX/CXc;

    const-string v2, "component_partial_screen_invalid_data"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Partial screen fetch failed for params:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, LX/CSx;->c:LX/4Hw;

    invoke-virtual {v4}, LX/0gS;->b()Ljava/util/Map;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, LX/CXd;->a(LX/CXc;Ljava/lang/String;Ljava/lang/String;)V

    .line 1895163
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 1895164
    check-cast p1, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformPartialScreenQueryModel;

    .line 1895165
    :try_start_0
    iget-object v0, p0, LX/CSx;->a:LX/CT9;

    if-eqz v0, :cond_0

    .line 1895166
    iget-object v0, p0, LX/CSx;->a:LX/CT9;

    iget-object v1, p0, LX/CSx;->b:LX/0Px;

    invoke-virtual {v0, v1, p1}, LX/CT9;->a(LX/0Px;Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformPartialScreenQueryModel;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1895167
    :cond_0
    :goto_0
    return-void

    .line 1895168
    :catch_0
    move-exception v0

    .line 1895169
    iget-object v1, p0, LX/CSx;->d:LX/CSz;

    iget-object v1, v1, LX/CSz;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-static {v0}, LX/CXd;->a(Ljava/lang/Exception;)V

    .line 1895170
    invoke-virtual {p0, v0}, LX/CSx;->onNonCancellationFailure(Ljava/lang/Throwable;)V

    goto :goto_0
.end method
