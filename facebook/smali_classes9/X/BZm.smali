.class public final enum LX/BZm;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/BZm;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/BZm;

.field public static final enum BOOKMARK_DRAWER:LX/BZm;

.field public static final enum CONSUMPTION_GALLERY:LX/BZm;

.field public static final enum DIVEBAR:LX/BZm;

.field public static final enum UFI_FLYOUT:LX/BZm;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1798001
    new-instance v0, LX/BZm;

    const-string v1, "CONSUMPTION_GALLERY"

    invoke-direct {v0, v1, v2}, LX/BZm;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BZm;->CONSUMPTION_GALLERY:LX/BZm;

    .line 1798002
    new-instance v0, LX/BZm;

    const-string v1, "UFI_FLYOUT"

    invoke-direct {v0, v1, v3}, LX/BZm;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BZm;->UFI_FLYOUT:LX/BZm;

    .line 1798003
    new-instance v0, LX/BZm;

    const-string v1, "BOOKMARK_DRAWER"

    invoke-direct {v0, v1, v4}, LX/BZm;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BZm;->BOOKMARK_DRAWER:LX/BZm;

    .line 1798004
    new-instance v0, LX/BZm;

    const-string v1, "DIVEBAR"

    invoke-direct {v0, v1, v5}, LX/BZm;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BZm;->DIVEBAR:LX/BZm;

    .line 1798005
    const/4 v0, 0x4

    new-array v0, v0, [LX/BZm;

    sget-object v1, LX/BZm;->CONSUMPTION_GALLERY:LX/BZm;

    aput-object v1, v0, v2

    sget-object v1, LX/BZm;->UFI_FLYOUT:LX/BZm;

    aput-object v1, v0, v3

    sget-object v1, LX/BZm;->BOOKMARK_DRAWER:LX/BZm;

    aput-object v1, v0, v4

    sget-object v1, LX/BZm;->DIVEBAR:LX/BZm;

    aput-object v1, v0, v5

    sput-object v0, LX/BZm;->$VALUES:[LX/BZm;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1797998
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/BZm;
    .locals 1

    .prologue
    .line 1797999
    const-class v0, LX/BZm;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/BZm;

    return-object v0
.end method

.method public static values()[LX/BZm;
    .locals 1

    .prologue
    .line 1798000
    sget-object v0, LX/BZm;->$VALUES:[LX/BZm;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/BZm;

    return-object v0
.end method
