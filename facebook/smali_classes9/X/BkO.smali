.class public LX/BkO;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Landroid/app/Activity;

.field public b:Lcom/facebook/intent/feed/IFeedIntentBuilder;

.field public c:Lcom/facebook/content/SecureContextHelper;

.field public d:Lcom/facebook/events/create/EventCompositionModel;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/facebook/intent/feed/IFeedIntentBuilder;Lcom/facebook/content/SecureContextHelper;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1814167
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1814168
    iput-object p1, p0, LX/BkO;->a:Landroid/app/Activity;

    .line 1814169
    iput-object p2, p0, LX/BkO;->b:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    .line 1814170
    iput-object p3, p0, LX/BkO;->c:Lcom/facebook/content/SecureContextHelper;

    .line 1814171
    return-void
.end method

.method public static a(LX/0QB;)LX/BkO;
    .locals 1

    .prologue
    .line 1814172
    invoke-static {p0}, LX/BkO;->b(LX/0QB;)LX/BkO;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/BkO;
    .locals 4

    .prologue
    .line 1814173
    new-instance v3, LX/BkO;

    invoke-static {p0}, LX/0kU;->b(LX/0QB;)Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-static {p0}, LX/1nB;->a(LX/0QB;)LX/1nB;

    move-result-object v1

    check-cast v1, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v2

    check-cast v2, Lcom/facebook/content/SecureContextHelper;

    invoke-direct {v3, v0, v1, v2}, LX/BkO;-><init>(Landroid/app/Activity;Lcom/facebook/intent/feed/IFeedIntentBuilder;Lcom/facebook/content/SecureContextHelper;)V

    .line 1814174
    return-object v3
.end method


# virtual methods
.method public final b()V
    .locals 4

    .prologue
    .line 1814175
    new-instance v0, LX/8AA;

    sget-object v1, LX/8AB;->EVENT:LX/8AB;

    invoke-direct {v0, v1}, LX/8AA;-><init>(LX/8AB;)V

    invoke-virtual {v0}, LX/8AA;->i()LX/8AA;

    move-result-object v0

    invoke-virtual {v0}, LX/8AA;->j()LX/8AA;

    move-result-object v0

    invoke-virtual {v0}, LX/8AA;->l()LX/8AA;

    move-result-object v0

    sget-object v1, LX/8A9;->NONE:LX/8A9;

    invoke-virtual {v0, v1}, LX/8AA;->a(LX/8A9;)LX/8AA;

    move-result-object v0

    .line 1814176
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, LX/BkO;->a:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/facebook/photos/simplepicker/launcher/SimplePickerLauncherActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1814177
    const-string v2, "extra_simple_picker_launcher_settings"

    invoke-virtual {v0}, LX/8AA;->w()Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1814178
    iget-object v0, p0, LX/BkO;->c:Lcom/facebook/content/SecureContextHelper;

    const/16 v2, 0x67

    iget-object v3, p0, LX/BkO;->a:Landroid/app/Activity;

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 1814179
    return-void
.end method

.method public final c()V
    .locals 4

    .prologue
    .line 1814180
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, LX/BkO;->a:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/facebook/events/ui/themeselector/ThemeSelectorActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1814181
    const-string v1, "extra_show_full_width_themes"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1814182
    iget-object v1, p0, LX/BkO;->d:Lcom/facebook/events/create/EventCompositionModel;

    .line 1814183
    iget-object v2, v1, Lcom/facebook/events/create/EventCompositionModel;->c:Ljava/lang/String;

    move-object v1, v2

    .line 1814184
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1814185
    const-string v1, "extra_theme_selector_event_name"

    iget-object v2, p0, LX/BkO;->d:Lcom/facebook/events/create/EventCompositionModel;

    .line 1814186
    iget-object v3, v2, Lcom/facebook/events/create/EventCompositionModel;->c:Ljava/lang/String;

    move-object v2, v3

    .line 1814187
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1814188
    :cond_0
    iget-object v1, p0, LX/BkO;->d:Lcom/facebook/events/create/EventCompositionModel;

    invoke-virtual {v1}, Lcom/facebook/events/create/EventCompositionModel;->c()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1814189
    const-string v1, "extra_theme_selector_event_description"

    iget-object v2, p0, LX/BkO;->d:Lcom/facebook/events/create/EventCompositionModel;

    .line 1814190
    iget-object v3, v2, Lcom/facebook/events/create/EventCompositionModel;->d:Ljava/lang/String;

    move-object v2, v3

    .line 1814191
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1814192
    :cond_1
    iget-object v1, p0, LX/BkO;->c:Lcom/facebook/content/SecureContextHelper;

    const/16 v2, 0x68

    iget-object v3, p0, LX/BkO;->a:Landroid/app/Activity;

    invoke-interface {v1, v0, v2, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 1814193
    return-void
.end method
