.class public final enum LX/ArO;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements LX/5oU;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/ArO;",
        ">;",
        "LX/5oU;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/ArO;

.field public static final enum COLD_START_REASON:LX/ArO;

.field public static final enum DRAG_DURATION:LX/ArO;

.field public static final enum DURATION:LX/ArO;

.field public static final enum EFFECT_TRAY:LX/ArO;

.field public static final enum FIRST_EFFECT:LX/ArO;

.field public static final enum FPS:LX/ArO;

.field public static final enum FROM:LX/ArO;

.field public static final enum HAS_CACHE:LX/ArO;

.field public static final enum HEIGHT:LX/ArO;

.field public static final enum PROMPT_ID:LX/ArO;

.field public static final enum SESSION_ID:LX/ArO;

.field public static final enum STARTUP_TYPE:LX/ArO;

.field public static final enum TIME_SINCE_STARTUP:LX/ArO;

.field public static final enum TO:LX/ArO;

.field public static final enum TRANSITION_TRIGGER_METHOD:LX/ArO;

.field public static final enum WIDTH:LX/ArO;


# instance fields
.field private final mName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1719073
    new-instance v0, LX/ArO;

    const-string v1, "COLD_START_REASON"

    const-string v2, "cold_start_reason"

    invoke-direct {v0, v1, v4, v2}, LX/ArO;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArO;->COLD_START_REASON:LX/ArO;

    .line 1719074
    new-instance v0, LX/ArO;

    const-string v1, "DRAG_DURATION"

    const-string v2, "drag_duration"

    invoke-direct {v0, v1, v5, v2}, LX/ArO;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArO;->DRAG_DURATION:LX/ArO;

    .line 1719075
    new-instance v0, LX/ArO;

    const-string v1, "DURATION"

    const-string v2, "duration"

    invoke-direct {v0, v1, v6, v2}, LX/ArO;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArO;->DURATION:LX/ArO;

    .line 1719076
    new-instance v0, LX/ArO;

    const-string v1, "EFFECT_TRAY"

    const-string v2, "effect_tray"

    invoke-direct {v0, v1, v7, v2}, LX/ArO;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArO;->EFFECT_TRAY:LX/ArO;

    .line 1719077
    new-instance v0, LX/ArO;

    const-string v1, "FIRST_EFFECT"

    const-string v2, "first_effect"

    invoke-direct {v0, v1, v8, v2}, LX/ArO;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArO;->FIRST_EFFECT:LX/ArO;

    .line 1719078
    new-instance v0, LX/ArO;

    const-string v1, "FPS"

    const/4 v2, 0x5

    const-string v3, "fps"

    invoke-direct {v0, v1, v2, v3}, LX/ArO;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArO;->FPS:LX/ArO;

    .line 1719079
    new-instance v0, LX/ArO;

    const-string v1, "FROM"

    const/4 v2, 0x6

    const-string v3, "from"

    invoke-direct {v0, v1, v2, v3}, LX/ArO;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArO;->FROM:LX/ArO;

    .line 1719080
    new-instance v0, LX/ArO;

    const-string v1, "HAS_CACHE"

    const/4 v2, 0x7

    const-string v3, "has_cache"

    invoke-direct {v0, v1, v2, v3}, LX/ArO;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArO;->HAS_CACHE:LX/ArO;

    .line 1719081
    new-instance v0, LX/ArO;

    const-string v1, "HEIGHT"

    const/16 v2, 0x8

    const-string v3, "height"

    invoke-direct {v0, v1, v2, v3}, LX/ArO;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArO;->HEIGHT:LX/ArO;

    .line 1719082
    new-instance v0, LX/ArO;

    const-string v1, "PROMPT_ID"

    const/16 v2, 0x9

    const-string v3, "prompt_id"

    invoke-direct {v0, v1, v2, v3}, LX/ArO;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArO;->PROMPT_ID:LX/ArO;

    .line 1719083
    new-instance v0, LX/ArO;

    const-string v1, "SESSION_ID"

    const/16 v2, 0xa

    const-string v3, "session_id"

    invoke-direct {v0, v1, v2, v3}, LX/ArO;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArO;->SESSION_ID:LX/ArO;

    .line 1719084
    new-instance v0, LX/ArO;

    const-string v1, "STARTUP_TYPE"

    const/16 v2, 0xb

    const-string v3, "startup_type"

    invoke-direct {v0, v1, v2, v3}, LX/ArO;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArO;->STARTUP_TYPE:LX/ArO;

    .line 1719085
    new-instance v0, LX/ArO;

    const-string v1, "TO"

    const/16 v2, 0xc

    const-string v3, "to"

    invoke-direct {v0, v1, v2, v3}, LX/ArO;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArO;->TO:LX/ArO;

    .line 1719086
    new-instance v0, LX/ArO;

    const-string v1, "TRANSITION_TRIGGER_METHOD"

    const/16 v2, 0xd

    const-string v3, "transition_trigger_method"

    invoke-direct {v0, v1, v2, v3}, LX/ArO;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArO;->TRANSITION_TRIGGER_METHOD:LX/ArO;

    .line 1719087
    new-instance v0, LX/ArO;

    const-string v1, "TIME_SINCE_STARTUP"

    const/16 v2, 0xe

    const-string v3, "time_since_startup"

    invoke-direct {v0, v1, v2, v3}, LX/ArO;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArO;->TIME_SINCE_STARTUP:LX/ArO;

    .line 1719088
    new-instance v0, LX/ArO;

    const-string v1, "WIDTH"

    const/16 v2, 0xf

    const-string v3, "width"

    invoke-direct {v0, v1, v2, v3}, LX/ArO;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArO;->WIDTH:LX/ArO;

    .line 1719089
    const/16 v0, 0x10

    new-array v0, v0, [LX/ArO;

    sget-object v1, LX/ArO;->COLD_START_REASON:LX/ArO;

    aput-object v1, v0, v4

    sget-object v1, LX/ArO;->DRAG_DURATION:LX/ArO;

    aput-object v1, v0, v5

    sget-object v1, LX/ArO;->DURATION:LX/ArO;

    aput-object v1, v0, v6

    sget-object v1, LX/ArO;->EFFECT_TRAY:LX/ArO;

    aput-object v1, v0, v7

    sget-object v1, LX/ArO;->FIRST_EFFECT:LX/ArO;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/ArO;->FPS:LX/ArO;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/ArO;->FROM:LX/ArO;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/ArO;->HAS_CACHE:LX/ArO;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/ArO;->HEIGHT:LX/ArO;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/ArO;->PROMPT_ID:LX/ArO;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/ArO;->SESSION_ID:LX/ArO;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/ArO;->STARTUP_TYPE:LX/ArO;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/ArO;->TO:LX/ArO;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/ArO;->TRANSITION_TRIGGER_METHOD:LX/ArO;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/ArO;->TIME_SINCE_STARTUP:LX/ArO;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/ArO;->WIDTH:LX/ArO;

    aput-object v2, v0, v1

    sput-object v0, LX/ArO;->$VALUES:[LX/ArO;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1719070
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1719071
    iput-object p3, p0, LX/ArO;->mName:Ljava/lang/String;

    .line 1719072
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/ArO;
    .locals 1

    .prologue
    .line 1719069
    const-class v0, LX/ArO;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/ArO;

    return-object v0
.end method

.method public static values()[LX/ArO;
    .locals 1

    .prologue
    .line 1719067
    sget-object v0, LX/ArO;->$VALUES:[LX/ArO;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/ArO;

    return-object v0
.end method


# virtual methods
.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1719068
    iget-object v0, p0, LX/ArO;->mName:Ljava/lang/String;

    return-object v0
.end method
