.class public final LX/AxQ;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 12

    .prologue
    .line 1728230
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1728231
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1728232
    if-eqz v0, :cond_5

    .line 1728233
    const-string v1, "node"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1728234
    const-wide/16 v4, 0x0

    .line 1728235
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1728236
    const/4 v2, 0x0

    invoke-virtual {p0, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1728237
    if-eqz v2, :cond_3

    .line 1728238
    const-string v3, "features"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1728239
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1728240
    const/4 v6, 0x0

    :goto_0
    invoke-virtual {p0, v2}, LX/15i;->c(I)I

    move-result v7

    if-ge v6, v7, :cond_2

    .line 1728241
    invoke-virtual {p0, v2, v6}, LX/15i;->q(II)I

    move-result v7

    const-wide/16 v10, 0x0

    .line 1728242
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1728243
    const/4 v8, 0x0

    invoke-virtual {p0, v7, v8}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v8

    .line 1728244
    if-eqz v8, :cond_0

    .line 1728245
    const-string v9, "feature_id"

    invoke-virtual {p2, v9}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1728246
    invoke-virtual {p2, v8}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1728247
    :cond_0
    const/4 v8, 0x1

    invoke-virtual {p0, v7, v8, v10, v11}, LX/15i;->a(IID)D

    move-result-wide v8

    .line 1728248
    cmpl-double v10, v8, v10

    if-eqz v10, :cond_1

    .line 1728249
    const-string v10, "feature_value"

    invoke-virtual {p2, v10}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1728250
    invoke-virtual {p2, v8, v9}, LX/0nX;->a(D)V

    .line 1728251
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1728252
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 1728253
    :cond_2
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1728254
    :cond_3
    const/4 v2, 0x1

    invoke-virtual {p0, v0, v2, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v2

    .line 1728255
    cmp-long v4, v2, v4

    if-eqz v4, :cond_4

    .line 1728256
    const-string v4, "ttl_sec"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1728257
    invoke-virtual {p2, v2, v3}, LX/0nX;->a(J)V

    .line 1728258
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1728259
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1728260
    return-void
.end method
