.class public final LX/CTQ;
.super LX/CTK;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:Z

.field public final b:Ljava/util/TreeSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeSet",
            "<",
            "LX/CU5;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;LX/CSS;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 1895746
    invoke-direct {p0, p1, p2, p3}, LX/CTK;-><init>(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;LX/CSS;Ljava/lang/String;)V

    .line 1895747
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;->r()Z

    move-result v0

    iput-boolean v0, p0, LX/CTQ;->a:Z

    .line 1895748
    new-instance v0, Ljava/util/TreeSet;

    new-instance v1, LX/CTP;

    invoke-direct {v1, p0}, LX/CTP;-><init>(LX/CTQ;)V

    invoke-direct {v0, v1}, Ljava/util/TreeSet;-><init>(Ljava/util/Comparator;)V

    iput-object v0, p0, LX/CTQ;->b:Ljava/util/TreeSet;

    .line 1895749
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;->G()LX/2uF;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1895750
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;->G()LX/2uF;

    move-result-object v0

    invoke-virtual {v0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, LX/2sN;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/2sN;->b()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 1895751
    iget-object v3, p0, LX/CTQ;->b:Ljava/util/TreeSet;

    new-instance v4, LX/CU5;

    invoke-direct {v4, v2, v1}, LX/CU5;-><init>(LX/15i;I)V

    invoke-virtual {v3, v4}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1895752
    :cond_0
    return-void
.end method
