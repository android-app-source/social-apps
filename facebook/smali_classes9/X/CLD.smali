.class public final LX/CLD;
.super LX/0zP;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0zP",
        "<",
        "Lcom/facebook/messaging/contactsyoumayknow/graphql/ContactsYouMayKnowMutationsModels$AddCYMKSuggestionModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 12

    .prologue
    .line 1878718
    const-class v1, Lcom/facebook/messaging/contactsyoumayknow/graphql/ContactsYouMayKnowMutationsModels$AddCYMKSuggestionModel;

    const v0, 0x7bffb7e6

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "AddCYMKSuggestion"

    const-string v6, "a262f1f1efc336095a4ab510195c789b"

    const-string v7, "cymk_suggestion_contact_add"

    const-string v8, "0"

    const-string v9, "10155069962836729"

    const/4 v10, 0x0

    .line 1878719
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v11, v0

    .line 1878720
    move-object v0, p0

    invoke-direct/range {v0 .. v11}, LX/0zP;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 1878721
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1878710
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1878711
    sparse-switch v0, :sswitch_data_0

    .line 1878712
    :goto_0
    return-object p1

    .line 1878713
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 1878714
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 1878715
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 1878716
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 1878717
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x719ba5ef -> :sswitch_2
        -0x55d248cb -> :sswitch_3
        -0x4e92d738 -> :sswitch_4
        0x5fb57ca -> :sswitch_0
        0x2956b75c -> :sswitch_1
    .end sparse-switch
.end method
