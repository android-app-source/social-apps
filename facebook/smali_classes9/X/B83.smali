.class public LX/B83;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements LX/B7m;


# static fields
.field public static final a:LX/B7j;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/B7j",
            "<",
            "LX/B83;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public b:LX/B8v;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/resources/ui/FbAutoCompleteTextView;

.field public d:Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;

.field public e:Lcom/facebook/resources/ui/FbTextView;

.field private f:Landroid/widget/TextView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1748389
    new-instance v0, LX/B82;

    invoke-direct {v0}, LX/B82;-><init>()V

    sput-object v0, LX/B83;->a:LX/B7j;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1748417
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 1748418
    const v0, 0x7f0309c8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1748419
    const v0, 0x7f0d18e5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbAutoCompleteTextView;

    iput-object v0, p0, LX/B83;->c:Lcom/facebook/resources/ui/FbAutoCompleteTextView;

    .line 1748420
    const v0, 0x7f0d18e1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/B83;->e:Lcom/facebook/resources/ui/FbTextView;

    .line 1748421
    const v0, 0x7f0d18d4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/B83;->f:Landroid/widget/TextView;

    .line 1748422
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, LX/B83;

    invoke-static {v0}, LX/B8v;->a(LX/0QB;)LX/B8v;

    move-result-object v0

    check-cast v0, LX/B8v;

    iput-object v0, p0, LX/B83;->b:LX/B8v;

    .line 1748423
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 1748416
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;LX/B7F;I)V
    .locals 3

    .prologue
    const/16 v1, 0x8

    .line 1748402
    iput-object p1, p0, LX/B83;->d:Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;

    .line 1748403
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->r()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->r()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1748404
    iget-object v1, p0, LX/B83;->c:Lcom/facebook/resources/ui/FbAutoCompleteTextView;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->r()LX/0Px;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbAutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1748405
    iget-object v0, p0, LX/B83;->c:Lcom/facebook/resources/ui/FbAutoCompleteTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbAutoCompleteTextView;->setKeyListener(Landroid/text/method/KeyListener;)V

    .line 1748406
    iget-object v0, p0, LX/B83;->d:Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->m()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1748407
    iget-object v0, p0, LX/B83;->e:Lcom/facebook/resources/ui/FbTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1748408
    iget-object v0, p0, LX/B83;->e:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, LX/B83;->d:Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->p()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1748409
    iget-object v0, p0, LX/B83;->c:Lcom/facebook/resources/ui/FbAutoCompleteTextView;

    invoke-virtual {p0}, LX/B83;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a074f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbAutoCompleteTextView;->setHintTextColor(I)V

    .line 1748410
    iget-object v0, p0, LX/B83;->c:Lcom/facebook/resources/ui/FbAutoCompleteTextView;

    iget-object v1, p0, LX/B83;->d:Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->q()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbAutoCompleteTextView;->setHint(Ljava/lang/CharSequence;)V

    .line 1748411
    :goto_0
    return-void

    .line 1748412
    :cond_0
    iget-object v0, p0, LX/B83;->c:Lcom/facebook/resources/ui/FbAutoCompleteTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbAutoCompleteTextView;->setVisibility(I)V

    .line 1748413
    iget-object v0, p0, LX/B83;->e:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_0

    .line 1748414
    :cond_1
    iget-object v0, p0, LX/B83;->c:Lcom/facebook/resources/ui/FbAutoCompleteTextView;

    iget-object v1, p0, LX/B83;->d:Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->p()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbAutoCompleteTextView;->setHint(Ljava/lang/CharSequence;)V

    .line 1748415
    iget-object v0, p0, LX/B83;->e:Lcom/facebook/resources/ui/FbTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1748400
    iget-object v0, p0, LX/B83;->f:Landroid/widget/TextView;

    invoke-static {v0, p1}, LX/B8v;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 1748401
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1748398
    iget-object v0, p0, LX/B83;->c:Lcom/facebook/resources/ui/FbAutoCompleteTextView;

    iget-object v1, p0, LX/B83;->f:Landroid/widget/TextView;

    invoke-static {v0, v1}, LX/B8v;->a(Landroid/view/View;Landroid/widget/TextView;)V

    .line 1748399
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1748396
    iget-object v0, p0, LX/B83;->f:Landroid/widget/TextView;

    invoke-static {v0}, LX/B8v;->a(Landroid/widget/TextView;)V

    .line 1748397
    return-void
.end method

.method public getBoundedInfoFieldData()Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;
    .locals 1

    .prologue
    .line 1748395
    iget-object v0, p0, LX/B83;->d:Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;

    return-object v0
.end method

.method public getInputCustomToken()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1748394
    invoke-virtual {p0}, LX/B83;->getInputValue()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getInputValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1748393
    iget-object v0, p0, LX/B83;->c:Lcom/facebook/resources/ui/FbAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setInputValue(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1748391
    iget-object v0, p0, LX/B83;->c:Lcom/facebook/resources/ui/FbAutoCompleteTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbAutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1748392
    return-void
.end method

.method public setOnDataChangeListener(LX/B7w;)V
    .locals 0

    .prologue
    .line 1748390
    return-void
.end method
