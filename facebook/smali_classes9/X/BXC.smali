.class public LX/BXC;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static a:I


# instance fields
.field public b:Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;

.field public c:Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

.field public d:Lcom/facebook/widget/friendselector/SelectedFriendItemView;

.field public e:I

.field public f:Z


# direct methods
.method public constructor <init>(Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;Lcom/facebook/widget/friendselector/SelectedFriendItemView;)V
    .locals 0

    .prologue
    .line 1792920
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1792921
    iput-object p1, p0, LX/BXC;->b:Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;

    .line 1792922
    iput-object p2, p0, LX/BXC;->d:Lcom/facebook/widget/friendselector/SelectedFriendItemView;

    .line 1792923
    invoke-direct {p0}, LX/BXC;->r()V

    .line 1792924
    return-void
.end method

.method public constructor <init>(Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;)V
    .locals 2

    .prologue
    .line 1792925
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1792926
    iput-object p1, p0, LX/BXC;->b:Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;

    .line 1792927
    iput-object p2, p0, LX/BXC;->c:Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    .line 1792928
    new-instance v0, Lcom/facebook/widget/friendselector/SelectedFriendItemView;

    invoke-virtual {p1}, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/widget/friendselector/SelectedFriendItemView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/BXC;->d:Lcom/facebook/widget/friendselector/SelectedFriendItemView;

    .line 1792929
    iget-object v0, p0, LX/BXC;->d:Lcom/facebook/widget/friendselector/SelectedFriendItemView;

    invoke-virtual {v0, p2}, Lcom/facebook/widget/friendselector/SelectedFriendItemView;->setToken(LX/8QL;)V

    .line 1792930
    invoke-direct {p0}, LX/BXC;->r()V

    .line 1792931
    return-void
.end method

.method private r()V
    .locals 2

    .prologue
    .line 1792932
    iget-object v0, p0, LX/BXC;->b:Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;

    invoke-virtual {v0}, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b04a8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/BXC;->e:I

    .line 1792933
    iget-object v0, p0, LX/BXC;->d:Lcom/facebook/widget/friendselector/SelectedFriendItemView;

    new-instance v1, LX/BX4;

    invoke-direct {v1, p0}, LX/BX4;-><init>(LX/BXC;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/friendselector/SelectedFriendItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1792934
    return-void
.end method

.method public static t(LX/BXC;)Landroid/animation/Animator;
    .locals 5

    .prologue
    .line 1792944
    iget-object v0, p0, LX/BXC;->d:Lcom/facebook/widget/friendselector/SelectedFriendItemView;

    sget v1, LX/BXC;->a:I

    .line 1792945
    const-string v2, "translationX"

    const/4 v3, 0x2

    new-array v3, v3, [F

    const/4 v4, 0x0

    neg-int p0, v1

    int-to-float p0, p0

    aput p0, v3, v4

    const/4 v4, 0x1

    const/4 p0, 0x0

    aput p0, v3, v4

    invoke-static {v0, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 1792946
    sget-object v3, LX/BXX;->a:Landroid/view/animation/Interpolator;

    move-object v3, v3

    .line 1792947
    invoke-virtual {v2, v3}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1792948
    invoke-static {v0}, LX/BXX;->f(Landroid/view/View;)Landroid/animation/AnimatorListenerAdapter;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1792949
    move-object v0, v2

    .line 1792950
    return-object v0
.end method

.method private u()Landroid/widget/LinearLayout$LayoutParams;
    .locals 5

    .prologue
    const/4 v1, -0x2

    .line 1792935
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1792936
    iget v1, p0, LX/BXC;->e:I

    iget v2, p0, LX/BXC;->e:I

    iget v3, p0, LX/BXC;->e:I

    iget v4, p0, LX/BXC;->e:I

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 1792937
    const/16 v1, 0x10

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 1792938
    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1792939
    iget-object v0, p0, LX/BXC;->b:Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;

    iget-object v1, p0, LX/BXC;->d:Lcom/facebook/widget/friendselector/SelectedFriendItemView;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->removeView(Landroid/view/View;)V

    .line 1792940
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/BXC;->f:Z

    .line 1792941
    return-void
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 1792942
    iget-object v0, p0, LX/BXC;->d:Lcom/facebook/widget/friendselector/SelectedFriendItemView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/friendselector/SelectedFriendItemView;->a(I)V

    .line 1792943
    return-void
.end method

.method public final a(Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;)V
    .locals 1

    .prologue
    .line 1792911
    iput-object p1, p0, LX/BXC;->c:Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    .line 1792912
    iget-object v0, p0, LX/BXC;->d:Lcom/facebook/widget/friendselector/SelectedFriendItemView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/friendselector/SelectedFriendItemView;->setToken(LX/8QL;)V

    .line 1792913
    return-void
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    .line 1792914
    iget-boolean v0, p0, LX/BXC;->f:Z

    if-eqz v0, :cond_0

    .line 1792915
    :goto_0
    return-void

    .line 1792916
    :cond_0
    iget-object v0, p0, LX/BXC;->b:Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;

    iget-object v1, p0, LX/BXC;->d:Lcom/facebook/widget/friendselector/SelectedFriendItemView;

    invoke-direct {p0}, LX/BXC;->u()Landroid/widget/LinearLayout$LayoutParams;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1792917
    if-eqz p1, :cond_1

    .line 1792918
    iget-object v0, p0, LX/BXC;->d:Lcom/facebook/widget/friendselector/SelectedFriendItemView;

    invoke-virtual {v0}, Lcom/facebook/widget/friendselector/SelectedFriendItemView;->a()V

    .line 1792919
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/BXC;->f:Z

    goto :goto_0
.end method

.method public final b(Z)Landroid/animation/Animator;
    .locals 2

    .prologue
    .line 1792908
    iget-object v0, p0, LX/BXC;->d:Lcom/facebook/widget/friendselector/SelectedFriendItemView;

    invoke-static {v0}, LX/BXX;->a(Landroid/view/View;)Landroid/animation/Animator;

    move-result-object v0

    .line 1792909
    new-instance v1, LX/BX9;

    invoke-direct {v1, p0, p1}, LX/BX9;-><init>(LX/BXC;Z)V

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1792910
    return-object v0
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 1792904
    iget-boolean v0, p0, LX/BXC;->f:Z

    if-eqz v0, :cond_0

    .line 1792905
    :goto_0
    return-void

    .line 1792906
    :cond_0
    iget-object v0, p0, LX/BXC;->b:Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;

    iget-object v1, p0, LX/BXC;->d:Lcom/facebook/widget/friendselector/SelectedFriendItemView;

    const/4 v2, 0x0

    invoke-direct {p0}, LX/BXC;->u()Landroid/widget/LinearLayout$LayoutParams;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 1792907
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/BXC;->f:Z

    goto :goto_0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 1792901
    iget-object v0, p0, LX/BXC;->d:Lcom/facebook/widget/friendselector/SelectedFriendItemView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/friendselector/SelectedFriendItemView;->setVisibility(I)V

    .line 1792902
    iget-object v0, p0, LX/BXC;->d:Lcom/facebook/widget/friendselector/SelectedFriendItemView;

    invoke-virtual {v0}, Lcom/facebook/widget/friendselector/SelectedFriendItemView;->requestLayout()V

    .line 1792903
    return-void
.end method

.method public final h()V
    .locals 1

    .prologue
    .line 1792899
    iget-object v0, p0, LX/BXC;->d:Lcom/facebook/widget/friendselector/SelectedFriendItemView;

    invoke-virtual {v0}, Lcom/facebook/widget/friendselector/SelectedFriendItemView;->b()V

    .line 1792900
    return-void
.end method

.method public final j()Landroid/animation/Animator;
    .locals 1

    .prologue
    .line 1792898
    iget-object v0, p0, LX/BXC;->d:Lcom/facebook/widget/friendselector/SelectedFriendItemView;

    invoke-static {v0}, LX/BXX;->b(Landroid/view/View;)Landroid/animation/Animator;

    move-result-object v0

    return-object v0
.end method

.method public final l()Landroid/animation/Animator;
    .locals 11

    .prologue
    .line 1792887
    iget-object v0, p0, LX/BXC;->d:Lcom/facebook/widget/friendselector/SelectedFriendItemView;

    sget v1, LX/BXC;->a:I

    const/4 v10, 0x2

    const/4 v9, 0x0

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 1792888
    new-instance v2, Landroid/animation/AnimatorSet;

    invoke-direct {v2}, Landroid/animation/AnimatorSet;-><init>()V

    .line 1792889
    const-string v3, "alpha"

    new-array v4, v8, [F

    aput v9, v4, v7

    invoke-static {v0, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    .line 1792890
    const-string v4, "translationX"

    new-array v5, v10, [F

    int-to-float v6, v1

    aput v6, v5, v7

    aput v9, v5, v8

    invoke-static {v0, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v4

    .line 1792891
    sget-object v5, LX/BXX;->a:Landroid/view/animation/Interpolator;

    move-object v5, v5

    .line 1792892
    invoke-virtual {v4, v5}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1792893
    new-array v5, v10, [Landroid/animation/Animator;

    aput-object v3, v5, v7

    aput-object v4, v5, v8

    invoke-virtual {v2, v5}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 1792894
    invoke-static {v0}, LX/BXX;->f(Landroid/view/View;)Landroid/animation/AnimatorListenerAdapter;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1792895
    move-object v0, v2

    .line 1792896
    new-instance v1, LX/BX5;

    invoke-direct {v1, p0}, LX/BX5;-><init>(LX/BXC;)V

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1792897
    return-object v0
.end method

.method public final n()Landroid/animation/Animator;
    .locals 5

    .prologue
    .line 1792880
    iget-object v0, p0, LX/BXC;->d:Lcom/facebook/widget/friendselector/SelectedFriendItemView;

    sget v1, LX/BXC;->a:I

    .line 1792881
    const-string v2, "translationX"

    const/4 v3, 0x2

    new-array v3, v3, [F

    const/4 v4, 0x0

    int-to-float p0, v1

    aput p0, v3, v4

    const/4 v4, 0x1

    const/4 p0, 0x0

    aput p0, v3, v4

    invoke-static {v0, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 1792882
    sget-object v3, LX/BXX;->a:Landroid/view/animation/Interpolator;

    move-object v3, v3

    .line 1792883
    invoke-virtual {v2, v3}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1792884
    invoke-static {v0}, LX/BXX;->f(Landroid/view/View;)Landroid/animation/AnimatorListenerAdapter;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1792885
    move-object v0, v2

    .line 1792886
    return-object v0
.end method

.method public final p()Landroid/animation/Animator;
    .locals 6

    .prologue
    .line 1792872
    iget-object v0, p0, LX/BXC;->d:Lcom/facebook/widget/friendselector/SelectedFriendItemView;

    sget v1, LX/BXC;->a:I

    .line 1792873
    const-string v2, "translationX"

    const/4 v3, 0x2

    new-array v3, v3, [F

    const/4 v4, 0x0

    const/4 v5, 0x0

    aput v5, v3, v4

    const/4 v4, 0x1

    int-to-float v5, v1

    aput v5, v3, v4

    invoke-static {v0, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 1792874
    sget-object v3, LX/BXX;->a:Landroid/view/animation/Interpolator;

    move-object v3, v3

    .line 1792875
    invoke-virtual {v2, v3}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1792876
    invoke-static {v0}, LX/BXX;->f(Landroid/view/View;)Landroid/animation/AnimatorListenerAdapter;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1792877
    move-object v0, v2

    .line 1792878
    new-instance v1, LX/BX8;

    invoke-direct {v1, p0}, LX/BX8;-><init>(LX/BXC;)V

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1792879
    return-object v0
.end method

.method public final q()Landroid/animation/Animator;
    .locals 2

    .prologue
    .line 1792869
    iget-object v0, p0, LX/BXC;->d:Lcom/facebook/widget/friendselector/SelectedFriendItemView;

    invoke-static {v0}, LX/BXX;->c(Landroid/view/View;)Landroid/animation/Animator;

    move-result-object v0

    .line 1792870
    new-instance v1, LX/BXA;

    invoke-direct {v1, p0}, LX/BXA;-><init>(LX/BXC;)V

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1792871
    return-object v0
.end method
