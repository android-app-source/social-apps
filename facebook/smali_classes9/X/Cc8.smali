.class public final LX/Cc8;
.super LX/1ci;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1ci",
        "<",
        "LX/1FJ",
        "<",
        "Lcom/facebook/imagepipeline/memory/PooledByteBuffer;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/CcO;


# direct methods
.method public constructor <init>(LX/CcO;)V
    .locals 0

    .prologue
    .line 1920663
    iput-object p1, p0, LX/Cc8;->a:LX/CcO;

    invoke-direct {p0}, LX/1ci;-><init>()V

    return-void
.end method


# virtual methods
.method public final e(LX/1ca;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "Lcom/facebook/imagepipeline/memory/PooledByteBuffer;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1920664
    invoke-interface {p1}, LX/1ca;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1920665
    :goto_0
    return-void

    .line 1920666
    :cond_0
    invoke-interface {p1}, LX/1ca;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FJ;

    .line 1920667
    new-instance v2, LX/1lZ;

    invoke-virtual {v0}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1FK;

    invoke-direct {v2, v1}, LX/1lZ;-><init>(LX/1FK;)V

    .line 1920668
    invoke-static {v2}, LX/1la;->b(Ljava/io/InputStream;)LX/1lW;

    move-result-object v1

    .line 1920669
    sget-object v3, LX/1ld;->a:LX/1lW;

    if-ne v1, v3, :cond_1

    .line 1920670
    invoke-virtual {v2}, LX/1lZ;->reset()V

    .line 1920671
    new-instance v1, LX/Cc7;

    invoke-direct {v1, p0, v2, v0}, LX/Cc7;-><init>(LX/Cc8;LX/1lZ;LX/1FJ;)V

    .line 1920672
    iget-object v0, p0, LX/Cc8;->a:LX/CcO;

    iget-object v0, v0, LX/CcO;->a:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->w:Ljava/util/concurrent/ExecutorService;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/io/InputStream;

    const/4 v4, 0x0

    aput-object v2, v3, v4

    invoke-virtual {v1, v0, v3}, LX/3nE;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 1920673
    :cond_1
    invoke-static {v2}, LX/0VN;->a(Ljava/io/InputStream;)V

    .line 1920674
    invoke-static {v0}, LX/1FJ;->c(LX/1FJ;)V

    .line 1920675
    iget-object v0, p0, LX/Cc8;->a:LX/CcO;

    iget-object v0, v0, LX/CcO;->h:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryActionsGenerator;

    iget-object v1, p0, LX/Cc8;->a:LX/CcO;

    iget-object v1, v1, LX/CcO;->e:LX/5kD;

    iget-object v2, p0, LX/Cc8;->a:LX/CcO;

    .line 1920676
    new-instance v3, LX/Cbz;

    invoke-direct {v3, v2}, LX/Cbz;-><init>(LX/CcO;)V

    move-object v2, v3

    .line 1920677
    invoke-static {v0, v1}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryActionsGenerator;->b(Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryActionsGenerator;LX/5kD;)LX/1ca;

    move-result-object v3

    iget-object v4, v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryActionsGenerator;->d:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v3, v2, v4}, LX/1ca;->a(LX/1cj;Ljava/util/concurrent/Executor;)V

    .line 1920678
    goto :goto_0
.end method

.method public final f(LX/1ca;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "Lcom/facebook/imagepipeline/memory/PooledByteBuffer;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1920679
    iget-object v0, p0, LX/Cc8;->a:LX/CcO;

    iget-object v0, v0, LX/CcO;->a:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->k:LX/03V;

    sget-object v1, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->c:Ljava/lang/String;

    const-string v2, "Could not save file (w/ Fresco + jpeg)"

    invoke-interface {p1}, LX/1ca;->e()Ljava/lang/Throwable;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1920680
    iget-object v0, p0, LX/Cc8;->a:LX/CcO;

    invoke-static {v0}, LX/CcO;->m(LX/CcO;)V

    .line 1920681
    return-void
.end method
