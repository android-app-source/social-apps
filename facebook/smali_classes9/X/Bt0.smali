.class public final LX/Bt0;
.super LX/37T;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/feed/rows/sections/text/BaseTextPartDefinition;

.field private final b:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

.field private final c:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/1KL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1KL",
            "<",
            "Ljava/lang/String;",
            "LX/1yT;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/sections/text/BaseTextPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1PT;)V
    .locals 6
    .param p3    # LX/1PT;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "LX/1PT;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1828568
    iput-object p1, p0, LX/Bt0;->a:Lcom/facebook/feed/rows/sections/text/BaseTextPartDefinition;

    invoke-direct {p0}, LX/37T;-><init>()V

    .line 1828569
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1828570
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aQ()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    iput-object v0, p0, LX/Bt0;->b:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1828571
    iput-object p2, p0, LX/Bt0;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1828572
    new-instance v0, LX/Bt1;

    iget-boolean v3, p1, Lcom/facebook/feed/rows/sections/text/BaseTextPartDefinition;->b:Z

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v4}, LX/Bt1;-><init>(Lcom/facebook/feed/rows/sections/text/BaseTextPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;ZLX/1PT;)V

    iput-object v0, p0, LX/Bt0;->d:LX/1KL;

    .line 1828573
    return-void
.end method


# virtual methods
.method public final a(Landroid/text/Spannable;)I
    .locals 2

    .prologue
    .line 1828565
    iget-object v0, p0, LX/Bt0;->b:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Bt0;->b:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->j()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Bt0;->b:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1828566
    :cond_0
    const/4 v0, 0x0

    .line 1828567
    :goto_0
    return v0

    :cond_1
    invoke-interface {p1}, Landroid/text/Spannable;->length()I

    move-result v0

    iget-object v1, p0, LX/Bt0;->b:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    sub-int/2addr v0, v1

    goto :goto_0
.end method

.method public final a()LX/1KL;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1KL",
            "<",
            "Ljava/lang/String;",
            "LX/1yT;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1828564
    iget-object v0, p0, LX/Bt0;->d:LX/1KL;

    return-object v0
.end method

.method public final b()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1828563
    iget-object v0, p0, LX/Bt0;->b:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final c()LX/0jW;
    .locals 1

    .prologue
    .line 1828562
    iget-object v0, p0, LX/Bt0;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, LX/1zW;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method
