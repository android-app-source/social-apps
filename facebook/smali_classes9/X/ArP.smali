.class public final enum LX/ArP;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/ArP;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/ArP;

.field public static final enum BUTTON_TAP:LX/ArP;

.field public static final enum SWIPE:LX/ArP;


# instance fields
.field private final tag:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1719090
    new-instance v0, LX/ArP;

    const-string v1, "BUTTON_TAP"

    const-string v2, "button"

    invoke-direct {v0, v1, v3, v2}, LX/ArP;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArP;->BUTTON_TAP:LX/ArP;

    .line 1719091
    new-instance v0, LX/ArP;

    const-string v1, "SWIPE"

    const-string v2, "swipe"

    invoke-direct {v0, v1, v4, v2}, LX/ArP;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/ArP;->SWIPE:LX/ArP;

    .line 1719092
    const/4 v0, 0x2

    new-array v0, v0, [LX/ArP;

    sget-object v1, LX/ArP;->BUTTON_TAP:LX/ArP;

    aput-object v1, v0, v3

    sget-object v1, LX/ArP;->SWIPE:LX/ArP;

    aput-object v1, v0, v4

    sput-object v0, LX/ArP;->$VALUES:[LX/ArP;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1719093
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1719094
    iput-object p3, p0, LX/ArP;->tag:Ljava/lang/String;

    .line 1719095
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/ArP;
    .locals 1

    .prologue
    .line 1719096
    const-class v0, LX/ArP;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/ArP;

    return-object v0
.end method

.method public static values()[LX/ArP;
    .locals 1

    .prologue
    .line 1719097
    sget-object v0, LX/ArP;->$VALUES:[LX/ArP;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/ArP;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1719098
    iget-object v0, p0, LX/ArP;->tag:Ljava/lang/String;

    return-object v0
.end method
