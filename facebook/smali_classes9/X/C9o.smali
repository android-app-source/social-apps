.class public final LX/C9o;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/C9p;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/1Pq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public c:LX/0Yb;

.field public final synthetic d:LX/C9p;


# direct methods
.method public constructor <init>(LX/C9p;)V
    .locals 1

    .prologue
    .line 1854539
    iput-object p1, p0, LX/C9o;->d:LX/C9p;

    .line 1854540
    move-object v0, p1

    .line 1854541
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1854542
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1854563
    const-string v0, "OfflineRetryComponentLogicShim"

    return-object v0
.end method

.method public final a(LX/1X1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1",
            "<",
            "LX/C9p;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1854560
    check-cast p1, LX/C9o;

    .line 1854561
    iget-object v0, p1, LX/C9o;->c:LX/0Yb;

    iput-object v0, p0, LX/C9o;->c:LX/0Yb;

    .line 1854562
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1854546
    if-ne p0, p1, :cond_1

    .line 1854547
    :cond_0
    :goto_0
    return v0

    .line 1854548
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1854549
    goto :goto_0

    .line 1854550
    :cond_3
    check-cast p1, LX/C9o;

    .line 1854551
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1854552
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1854553
    if-eq v2, v3, :cond_0

    .line 1854554
    iget-object v2, p0, LX/C9o;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/C9o;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/C9o;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1854555
    goto :goto_0

    .line 1854556
    :cond_5
    iget-object v2, p1, LX/C9o;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 1854557
    :cond_6
    iget-object v2, p0, LX/C9o;->b:LX/1Pq;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/C9o;->b:LX/1Pq;

    iget-object v3, p1, LX/C9o;->b:LX/1Pq;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1854558
    goto :goto_0

    .line 1854559
    :cond_7
    iget-object v2, p1, LX/C9o;->b:LX/1Pq;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public final g()LX/1X1;
    .locals 2

    .prologue
    .line 1854543
    invoke-super {p0}, LX/1X1;->g()LX/1X1;

    move-result-object v0

    check-cast v0, LX/C9o;

    .line 1854544
    const/4 v1, 0x0

    iput-object v1, v0, LX/C9o;->c:LX/0Yb;

    .line 1854545
    return-object v0
.end method
