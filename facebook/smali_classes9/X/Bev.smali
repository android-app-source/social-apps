.class public final LX/Bev;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Vj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Vj",
        "<",
        "Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;",
        "LX/BeP;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:I

.field public final synthetic b:Lcom/facebook/crowdsourcing/grapheditor/view/GraphEditorStackView;


# direct methods
.method public constructor <init>(Lcom/facebook/crowdsourcing/grapheditor/view/GraphEditorStackView;I)V
    .locals 0

    .prologue
    .line 1805502
    iput-object p1, p0, LX/Bev;->b:Lcom/facebook/crowdsourcing/grapheditor/view/GraphEditorStackView;

    iput p2, p0, LX/Bev;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/BeP;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1805498
    if-nez p1, :cond_0

    .line 1805499
    new-instance v0, LX/Bes;

    iget-object v1, p0, LX/Bev;->b:Lcom/facebook/crowdsourcing/grapheditor/view/GraphEditorStackView;

    invoke-virtual {v1}, Lcom/facebook/crowdsourcing/grapheditor/view/GraphEditorStackView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/Bes;-><init>(Landroid/content/Context;)V

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1805500
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, p0, LX/Bev;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/Bev;->b:Lcom/facebook/crowdsourcing/grapheditor/view/GraphEditorStackView;

    iget-object v1, v1, Lcom/facebook/crowdsourcing/grapheditor/view/GraphEditorStackView;->a:Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;

    iget-object v2, p0, LX/Bev;->b:Lcom/facebook/crowdsourcing/grapheditor/view/GraphEditorStackView;

    invoke-virtual {v2}, Lcom/facebook/crowdsourcing/grapheditor/view/GraphEditorStackView;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, LX/Bev;->b:Lcom/facebook/crowdsourcing/grapheditor/view/GraphEditorStackView;

    invoke-static {p1, v0, v1, v2, v3}, LX/Bg1;->b(Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;Ljava/lang/String;Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;Landroid/content/Context;LX/BeT;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1

    .prologue
    .line 1805501
    check-cast p1, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;

    invoke-direct {p0, p1}, LX/Bev;->a(Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
