.class public final enum LX/CMp;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/CMp;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/CMp;

.field public static final enum EMAIL:LX/CMp;

.field public static final enum GEO:LX/CMp;

.field public static final enum PHONE:LX/CMp;

.field private static final SCHEME_TO_TYPE_MAP:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/CMp;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum UNKNOWN:LX/CMp;

.field public static final enum WEB:LX/CMp;


# instance fields
.field private mUriSchemes:[Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 1880961
    new-instance v0, LX/CMp;

    const-string v2, "EMAIL"

    new-array v3, v5, [Ljava/lang/String;

    const-string v4, "mailto"

    aput-object v4, v3, v1

    invoke-direct {v0, v2, v1, v3}, LX/CMp;-><init>(Ljava/lang/String;I[Ljava/lang/String;)V

    sput-object v0, LX/CMp;->EMAIL:LX/CMp;

    .line 1880962
    new-instance v0, LX/CMp;

    const-string v2, "GEO"

    new-array v3, v5, [Ljava/lang/String;

    const-string v4, "geo"

    aput-object v4, v3, v1

    invoke-direct {v0, v2, v5, v3}, LX/CMp;-><init>(Ljava/lang/String;I[Ljava/lang/String;)V

    sput-object v0, LX/CMp;->GEO:LX/CMp;

    .line 1880963
    new-instance v0, LX/CMp;

    const-string v2, "PHONE"

    new-array v3, v5, [Ljava/lang/String;

    const-string v4, "tel"

    aput-object v4, v3, v1

    invoke-direct {v0, v2, v6, v3}, LX/CMp;-><init>(Ljava/lang/String;I[Ljava/lang/String;)V

    sput-object v0, LX/CMp;->PHONE:LX/CMp;

    .line 1880964
    new-instance v0, LX/CMp;

    const-string v2, "WEB"

    new-array v3, v6, [Ljava/lang/String;

    const-string v4, "http"

    aput-object v4, v3, v1

    const-string v4, "https"

    aput-object v4, v3, v5

    invoke-direct {v0, v2, v7, v3}, LX/CMp;-><init>(Ljava/lang/String;I[Ljava/lang/String;)V

    sput-object v0, LX/CMp;->WEB:LX/CMp;

    .line 1880965
    new-instance v0, LX/CMp;

    const-string v2, "UNKNOWN"

    new-array v3, v1, [Ljava/lang/String;

    invoke-direct {v0, v2, v8, v3}, LX/CMp;-><init>(Ljava/lang/String;I[Ljava/lang/String;)V

    sput-object v0, LX/CMp;->UNKNOWN:LX/CMp;

    .line 1880966
    const/4 v0, 0x5

    new-array v0, v0, [LX/CMp;

    sget-object v2, LX/CMp;->EMAIL:LX/CMp;

    aput-object v2, v0, v1

    sget-object v2, LX/CMp;->GEO:LX/CMp;

    aput-object v2, v0, v5

    sget-object v2, LX/CMp;->PHONE:LX/CMp;

    aput-object v2, v0, v6

    sget-object v2, LX/CMp;->WEB:LX/CMp;

    aput-object v2, v0, v7

    sget-object v2, LX/CMp;->UNKNOWN:LX/CMp;

    aput-object v2, v0, v8

    sput-object v0, LX/CMp;->$VALUES:[LX/CMp;

    .line 1880967
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, LX/CMp;->SCHEME_TO_TYPE_MAP:Ljava/util/Map;

    .line 1880968
    invoke-static {}, LX/CMp;->values()[LX/CMp;

    move-result-object v3

    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_1

    aget-object v5, v3, v2

    .line 1880969
    iget-object v6, v5, LX/CMp;->mUriSchemes:[Ljava/lang/String;

    array-length v7, v6

    move v0, v1

    :goto_1
    if-ge v0, v7, :cond_0

    aget-object v8, v6, v0

    .line 1880970
    sget-object v9, LX/CMp;->SCHEME_TO_TYPE_MAP:Ljava/util/Map;

    invoke-interface {v9, v8, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1880971
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1880972
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1880973
    :cond_1
    return-void
.end method

.method private varargs constructor <init>(Ljava/lang/String;I[Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1880974
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1880975
    iput-object p3, p0, LX/CMp;->mUriSchemes:[Ljava/lang/String;

    .line 1880976
    return-void
.end method

.method public static from(Landroid/net/Uri;)LX/CMp;
    .locals 2

    .prologue
    .line 1880959
    sget-object v0, LX/CMp;->SCHEME_TO_TYPE_MAP:Ljava/util/Map;

    invoke-virtual {p0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CMp;

    .line 1880960
    if-nez v0, :cond_0

    sget-object v0, LX/CMp;->UNKNOWN:LX/CMp;

    :cond_0
    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/CMp;
    .locals 1

    .prologue
    .line 1880958
    const-class v0, LX/CMp;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/CMp;

    return-object v0
.end method

.method public static values()[LX/CMp;
    .locals 1

    .prologue
    .line 1880957
    sget-object v0, LX/CMp;->$VALUES:[LX/CMp;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/CMp;

    return-object v0
.end method
