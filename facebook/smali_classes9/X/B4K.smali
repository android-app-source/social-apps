.class public final LX/B4K;
.super LX/B4J;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/B4J",
        "<",
        "LX/B4K;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/5QV;

.field public b:Lcom/facebook/heisman/protocol/FetchImageOverlayGraphQLModels$ImageOverlayCameraTitleFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/5QV;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1741502
    invoke-direct {p0, p2, p3}, LX/B4J;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1741503
    iput-object p1, p0, LX/B4K;->a:LX/5QV;

    .line 1741504
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;
    .locals 4

    .prologue
    .line 1741505
    invoke-virtual {p0}, LX/B4J;->c()Lcom/facebook/heisman/intent/ProfilePictureOverlayCommonParams;

    move-result-object v0

    .line 1741506
    iget-object v1, p0, LX/B4K;->a:LX/5QV;

    invoke-static {v1}, LX/5Qm;->a(LX/5QV;)Z

    move-result v1

    iget-object v2, p0, LX/B4K;->a:LX/5QV;

    invoke-static {v2}, LX/5Qm;->c(LX/5QV;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1741507
    new-instance v1, Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;

    iget-object v2, p0, LX/B4K;->a:LX/5QV;

    iget-object v3, p0, LX/B4K;->b:Lcom/facebook/heisman/protocol/FetchImageOverlayGraphQLModels$ImageOverlayCameraTitleFieldsModel;

    invoke-direct {v1, v0, v2, v3}, Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;-><init>(Lcom/facebook/heisman/intent/ProfilePictureOverlayCommonParams;LX/5QV;Lcom/facebook/heisman/protocol/FetchImageOverlayGraphQLModels$ImageOverlayCameraTitleFieldsModel;)V

    return-object v1
.end method

.method public final b()LX/B4J;
    .locals 1

    .prologue
    .line 1741508
    return-object p0
.end method
