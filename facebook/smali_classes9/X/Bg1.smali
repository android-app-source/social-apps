.class public LX/Bg1;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1806952
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/view/View;Ljava/lang/Object;)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Ljava/lang/Object;",
            ")",
            "LX/0Px",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1806953
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 1806954
    invoke-static {p0, v0, p1}, LX/Bg1;->a(Landroid/view/View;LX/0Pz;Ljava/lang/Object;)V

    .line 1806955
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;Ljava/lang/String;Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;Landroid/content/Context;LX/BeT;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsInterfaces$PlaceQuestionFields;",
            "Ljava/lang/String;",
            "Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;",
            "Landroid/content/Context;",
            "LX/BeT;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/BeP;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 1806956
    new-instance v1, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;

    invoke-direct {v1, p3}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;-><init>(Landroid/content/Context;)V

    .line 1806957
    invoke-virtual {v1, v4}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;->setContentPaddingTop(Z)V

    .line 1806958
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;->j()Lcom/facebook/graphql/enums/GraphQLPlaceQuestionOrientation;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPlaceQuestionOrientation;->VERTICAL:Lcom/facebook/graphql/enums/GraphQLPlaceQuestionOrientation;

    if-ne v0, v2, :cond_0

    :goto_0
    move-object v0, p2

    move-object v2, p0

    move-object v3, p1

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;->a(Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;Ljava/lang/String;ILX/BeT;)V

    .line 1806959
    invoke-static {v1}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0

    .line 1806960
    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method

.method private static a(Landroid/view/View;LX/0Pz;Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "LX/0Pz",
            "<",
            "Landroid/view/View;",
            ">;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1806961
    invoke-virtual {p0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1806962
    invoke-virtual {p1, p0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1806963
    :cond_0
    instance-of v0, p0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_1

    .line 1806964
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    move-object v0, p0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_1

    move-object v0, p0

    .line 1806965
    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0, p1, p2}, LX/Bg1;->a(Landroid/view/View;LX/0Pz;Ljava/lang/Object;)V

    .line 1806966
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1806967
    :cond_1
    return-void
.end method

.method public static b(Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;Ljava/lang/String;Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;Landroid/content/Context;LX/BeT;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsInterfaces$PlaceQuestionFields;",
            "Ljava/lang/String;",
            "Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;",
            "Landroid/content/Context;",
            "LX/BeT;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/BeP;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 1806968
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;->c()Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionPlaceInfoFieldsModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;->a(Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionPlaceInfoFieldsModel;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1806969
    invoke-static {p0}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;->b(Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;)Z

    move-result v0

    invoke-static {v0}, LX/0Tp;->b(Z)V

    .line 1806970
    invoke-static {p0, p1, p2, p3, p4}, LX/Bg1;->a(Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;Ljava/lang/String;Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;Landroid/content/Context;LX/BeT;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1806971
    :goto_0
    return-object v0

    .line 1806972
    :cond_0
    new-instance v1, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;

    invoke-direct {v1, p3}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;-><init>(Landroid/content/Context;)V

    .line 1806973
    invoke-virtual {v1, v4}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;->setContentPaddingTop(Z)V

    .line 1806974
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;->j()Lcom/facebook/graphql/enums/GraphQLPlaceQuestionOrientation;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPlaceQuestionOrientation;->VERTICAL:Lcom/facebook/graphql/enums/GraphQLPlaceQuestionOrientation;

    if-ne v0, v2, :cond_1

    :goto_1
    move-object v0, p2

    move-object v2, p0

    move-object v3, p1

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;->b(Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;Ljava/lang/String;ILX/BeT;)V

    .line 1806975
    invoke-static {v1}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0

    .line 1806976
    :cond_1
    const/4 v4, 0x0

    goto :goto_1
.end method
