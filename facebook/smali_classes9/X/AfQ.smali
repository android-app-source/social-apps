.class public final enum LX/AfQ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/AfQ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/AfQ;

.field public static final enum LIVE_COMMERCIAL_BREAK_ELIGIBLE:LX/AfQ;

.field public static final enum LIVE_COMMERCIAL_BREAK_REACH_VIEWER_COUNT:LX/AfQ;

.field public static final enum LIVE_COMMERCIAL_BREAK_SUPPORT:LX/AfQ;

.field public static final enum LIVE_COMMERCIAL_BREAK_VIOLATION:LX/AfQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1699288
    new-instance v0, LX/AfQ;

    const-string v1, "LIVE_COMMERCIAL_BREAK_SUPPORT"

    invoke-direct {v0, v1, v2}, LX/AfQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AfQ;->LIVE_COMMERCIAL_BREAK_SUPPORT:LX/AfQ;

    .line 1699289
    new-instance v0, LX/AfQ;

    const-string v1, "LIVE_COMMERCIAL_BREAK_ELIGIBLE"

    invoke-direct {v0, v1, v3}, LX/AfQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AfQ;->LIVE_COMMERCIAL_BREAK_ELIGIBLE:LX/AfQ;

    .line 1699290
    new-instance v0, LX/AfQ;

    const-string v1, "LIVE_COMMERCIAL_BREAK_VIOLATION"

    invoke-direct {v0, v1, v4}, LX/AfQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AfQ;->LIVE_COMMERCIAL_BREAK_VIOLATION:LX/AfQ;

    .line 1699291
    new-instance v0, LX/AfQ;

    const-string v1, "LIVE_COMMERCIAL_BREAK_REACH_VIEWER_COUNT"

    invoke-direct {v0, v1, v5}, LX/AfQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AfQ;->LIVE_COMMERCIAL_BREAK_REACH_VIEWER_COUNT:LX/AfQ;

    .line 1699292
    const/4 v0, 0x4

    new-array v0, v0, [LX/AfQ;

    sget-object v1, LX/AfQ;->LIVE_COMMERCIAL_BREAK_SUPPORT:LX/AfQ;

    aput-object v1, v0, v2

    sget-object v1, LX/AfQ;->LIVE_COMMERCIAL_BREAK_ELIGIBLE:LX/AfQ;

    aput-object v1, v0, v3

    sget-object v1, LX/AfQ;->LIVE_COMMERCIAL_BREAK_VIOLATION:LX/AfQ;

    aput-object v1, v0, v4

    sget-object v1, LX/AfQ;->LIVE_COMMERCIAL_BREAK_REACH_VIEWER_COUNT:LX/AfQ;

    aput-object v1, v0, v5

    sput-object v0, LX/AfQ;->$VALUES:[LX/AfQ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1699293
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/AfQ;
    .locals 1

    .prologue
    .line 1699294
    const-class v0, LX/AfQ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/AfQ;

    return-object v0
.end method

.method public static values()[LX/AfQ;
    .locals 1

    .prologue
    .line 1699295
    sget-object v0, LX/AfQ;->$VALUES:[LX/AfQ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/AfQ;

    return-object v0
.end method
