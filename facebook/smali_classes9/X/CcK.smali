.class public final LX/CcK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:Landroid/content/Context;

.field public final synthetic b:LX/5kD;

.field public final synthetic c:LX/CcO;


# direct methods
.method public constructor <init>(LX/CcO;Landroid/content/Context;LX/5kD;)V
    .locals 0

    .prologue
    .line 1920824
    iput-object p1, p0, LX/CcK;->c:LX/CcO;

    iput-object p2, p0, LX/CcK;->a:Landroid/content/Context;

    iput-object p3, p0, LX/CcK;->b:LX/5kD;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 9

    .prologue
    .line 1920825
    iget-object v0, p0, LX/CcK;->a:Landroid/content/Context;

    const-class v1, Landroid/support/v4/app/FragmentActivity;

    invoke-static {v0, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/support/v4/app/FragmentActivity;

    .line 1920826
    iget-object v0, p0, LX/CcK;->c:LX/CcO;

    iget-object v0, v0, LX/CcO;->a:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;

    iget-object v1, p0, LX/CcK;->b:LX/5kD;

    invoke-interface {v1}, LX/5kD;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v1

    iget-object v4, p0, LX/CcK;->c:LX/CcO;

    iget-object v4, v4, LX/CcO;->a:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;

    iget-object v4, v4, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->d:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    new-instance v6, Lcom/facebook/photos/base/photos/PhotoFetchInfo;

    sget-object v7, LX/3WA;->USER_INITIATED:LX/3WA;

    sget-object v8, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-direct {v6, v7, v8}, Lcom/facebook/photos/base/photos/PhotoFetchInfo;-><init>(LX/3WA;Lcom/facebook/common/callercontext/CallerContext;)V

    invoke-virtual/range {v0 .. v6}, Lcom/facebook/photos/albums/ProfilePicCoverPhotoEditHelper;->a(JLandroid/support/v4/app/FragmentActivity;JLcom/facebook/photos/base/photos/PhotoFetchInfo;)V

    .line 1920827
    const/4 v0, 0x1

    return v0
.end method
