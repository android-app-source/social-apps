.class public final LX/CE8;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/CE9;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/fig/components/newsfeed/FigStoryProfilePictureComponent$FigStoryProfilePictureComponentImpl;

.field private b:[Ljava/lang/String;

.field private c:I

.field private d:Ljava/util/BitSet;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1860632
    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 1860633
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "actorProfilePhotoUrl"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/CE8;->b:[Ljava/lang/String;

    .line 1860634
    iput v3, p0, LX/CE8;->c:I

    .line 1860635
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/CE8;->c:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/CE8;->d:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/CE8;LX/1De;IILcom/facebook/fig/components/newsfeed/FigStoryProfilePictureComponent$FigStoryProfilePictureComponentImpl;)V
    .locals 1

    .prologue
    .line 1860628
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 1860629
    iput-object p4, p0, LX/CE8;->a:Lcom/facebook/fig/components/newsfeed/FigStoryProfilePictureComponent$FigStoryProfilePictureComponentImpl;

    .line 1860630
    iget-object v0, p0, LX/CE8;->d:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 1860631
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/common/callercontext/CallerContext;)LX/CE8;
    .locals 1

    .prologue
    .line 1860626
    iget-object v0, p0, LX/CE8;->a:Lcom/facebook/fig/components/newsfeed/FigStoryProfilePictureComponent$FigStoryProfilePictureComponentImpl;

    iput-object p1, v0, Lcom/facebook/fig/components/newsfeed/FigStoryProfilePictureComponent$FigStoryProfilePictureComponentImpl;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 1860627
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 1860607
    invoke-super {p0}, LX/1X5;->a()V

    .line 1860608
    const/4 v0, 0x0

    iput-object v0, p0, LX/CE8;->a:Lcom/facebook/fig/components/newsfeed/FigStoryProfilePictureComponent$FigStoryProfilePictureComponentImpl;

    .line 1860609
    sget-object v0, LX/CE9;->a:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 1860610
    return-void
.end method

.method public final b(Ljava/lang/String;)LX/CE8;
    .locals 2

    .prologue
    .line 1860623
    iget-object v0, p0, LX/CE8;->a:Lcom/facebook/fig/components/newsfeed/FigStoryProfilePictureComponent$FigStoryProfilePictureComponentImpl;

    iput-object p1, v0, Lcom/facebook/fig/components/newsfeed/FigStoryProfilePictureComponent$FigStoryProfilePictureComponentImpl;->a:Ljava/lang/String;

    .line 1860624
    iget-object v0, p0, LX/CE8;->d:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1860625
    return-object p0
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/CE9;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1860613
    iget-object v1, p0, LX/CE8;->d:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/CE8;->d:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/CE8;->c:I

    if-ge v1, v2, :cond_2

    .line 1860614
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1860615
    :goto_0
    iget v2, p0, LX/CE8;->c:I

    if-ge v0, v2, :cond_1

    .line 1860616
    iget-object v2, p0, LX/CE8;->d:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1860617
    iget-object v2, p0, LX/CE8;->b:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1860618
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1860619
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1860620
    :cond_2
    iget-object v0, p0, LX/CE8;->a:Lcom/facebook/fig/components/newsfeed/FigStoryProfilePictureComponent$FigStoryProfilePictureComponentImpl;

    .line 1860621
    invoke-virtual {p0}, LX/CE8;->a()V

    .line 1860622
    return-object v0
.end method

.method public final h(I)LX/CE8;
    .locals 1
    .param p1    # I
        .annotation build Landroid/support/annotation/DimenRes;
        .end annotation
    .end param

    .prologue
    .line 1860611
    iget-object v0, p0, LX/CE8;->a:Lcom/facebook/fig/components/newsfeed/FigStoryProfilePictureComponent$FigStoryProfilePictureComponentImpl;

    iput p1, v0, Lcom/facebook/fig/components/newsfeed/FigStoryProfilePictureComponent$FigStoryProfilePictureComponentImpl;->b:I

    .line 1860612
    return-object p0
.end method
