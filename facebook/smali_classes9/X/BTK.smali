.class public LX/BTK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field private final a:LX/BTJ;

.field private final b:LX/BTP;

.field private final c:LX/BTN;

.field private final d:LX/BTX;

.field private final e:LX/BTL;

.field private final f:LX/BTZ;

.field private final g:I

.field public final h:I

.field public final i:I

.field public final j:Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;

.field public final k:Landroid/view/View;

.field public final l:Landroid/view/View;

.field private final m:LX/BTI;

.field private final n:LX/BT4;

.field private o:Z

.field private p:I


# direct methods
.method public constructor <init>(LX/BTJ;LX/BTP;LX/BTN;LX/BTX;LX/BTL;LX/BTZ;IIILcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;Landroid/view/View;Landroid/view/View;LX/BTI;LX/BT4;)V
    .locals 1

    .prologue
    .line 1787108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1787109
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/BTK;->o:Z

    .line 1787110
    iput-object p1, p0, LX/BTK;->a:LX/BTJ;

    .line 1787111
    iput-object p2, p0, LX/BTK;->b:LX/BTP;

    .line 1787112
    iput-object p3, p0, LX/BTK;->c:LX/BTN;

    .line 1787113
    iput-object p4, p0, LX/BTK;->d:LX/BTX;

    .line 1787114
    iput-object p5, p0, LX/BTK;->e:LX/BTL;

    .line 1787115
    iput-object p6, p0, LX/BTK;->f:LX/BTZ;

    .line 1787116
    iput p7, p0, LX/BTK;->g:I

    .line 1787117
    iput p8, p0, LX/BTK;->h:I

    .line 1787118
    iput p9, p0, LX/BTK;->i:I

    .line 1787119
    iput-object p10, p0, LX/BTK;->j:Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;

    .line 1787120
    iput-object p11, p0, LX/BTK;->k:Landroid/view/View;

    .line 1787121
    iput-object p12, p0, LX/BTK;->l:Landroid/view/View;

    .line 1787122
    iput-object p13, p0, LX/BTK;->m:LX/BTI;

    .line 1787123
    iput-object p14, p0, LX/BTK;->n:LX/BT4;

    .line 1787124
    return-void
.end method

.method private a(III)Z
    .locals 2

    .prologue
    .line 1787125
    add-int v0, p1, p3

    if-ltz v0, :cond_0

    add-int v0, p2, p3

    iget-object v1, p0, LX/BTK;->j:Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;

    invoke-virtual {v1}, Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;->getZoomedOutWidth()I

    move-result v1

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v4, 0x0

    .line 1787126
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    .line 1787127
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v1

    float-to-int v1, v1

    .line 1787128
    packed-switch v0, :pswitch_data_0

    .line 1787129
    :goto_0
    return v7

    .line 1787130
    :pswitch_0
    iget-object v0, p0, LX/BTK;->b:LX/BTP;

    iget-object v2, p0, LX/BTK;->m:LX/BTI;

    invoke-virtual {v0, v2}, LX/BTP;->a(LX/BTI;)V

    .line 1787131
    iput v1, p0, LX/BTK;->p:I

    goto :goto_0

    .line 1787132
    :pswitch_1
    iget-object v0, p0, LX/BTK;->c:LX/BTN;

    .line 1787133
    iget-boolean v1, v0, LX/BTN;->h:Z

    move v0, v1

    .line 1787134
    if-eqz v0, :cond_0

    .line 1787135
    iget-object v0, p0, LX/BTK;->c:LX/BTN;

    invoke-virtual {v0}, LX/BTN;->b()V

    .line 1787136
    iget-object v0, p0, LX/BTK;->c:LX/BTN;

    .line 1787137
    iput-boolean v4, v0, LX/BTN;->h:Z

    .line 1787138
    :cond_0
    iget-object v0, p0, LX/BTK;->b:LX/BTP;

    .line 1787139
    iget-boolean v1, v0, LX/BTP;->h:Z

    move v0, v1

    .line 1787140
    if-eqz v0, :cond_1

    .line 1787141
    iget-object v0, p0, LX/BTK;->a:LX/BTJ;

    iget-object v1, p0, LX/BTK;->m:LX/BTI;

    invoke-virtual {v0, v1}, LX/BTJ;->a(LX/BTI;)I

    move-result v0

    .line 1787142
    iget-object v1, p0, LX/BTK;->b:LX/BTP;

    iget-object v2, p0, LX/BTK;->m:LX/BTI;

    const/4 p2, 0x0

    .line 1787143
    iget-boolean v3, v1, LX/BTP;->h:Z

    if-nez v3, :cond_a

    .line 1787144
    :cond_1
    :goto_1
    iget-object v0, p0, LX/BTK;->b:LX/BTP;

    invoke-virtual {v0}, LX/BTP;->b()V

    .line 1787145
    iget-object v0, p0, LX/BTK;->n:LX/BT4;

    iget-boolean v1, p0, LX/BTK;->o:Z

    .line 1787146
    iget-object v2, v0, LX/BT4;->a:LX/BTI;

    sget-object v3, LX/BTI;->LEFT:LX/BTI;

    if-ne v2, v3, :cond_c

    .line 1787147
    iget-object v2, v0, LX/BT4;->b:LX/BT9;

    invoke-static {v2}, LX/BT9;->u$redex0(LX/BT9;)V

    .line 1787148
    iget-object v2, v0, LX/BT4;->b:LX/BT9;

    iget-object v2, v2, LX/BT9;->L:LX/BT8;

    if-eqz v2, :cond_2

    .line 1787149
    iget-object v2, v0, LX/BT4;->b:LX/BT9;

    iget-object v2, v2, LX/BT9;->L:LX/BT8;

    invoke-virtual {v2}, LX/0ht;->l()V

    .line 1787150
    :cond_2
    :goto_2
    if-eqz v1, :cond_3

    .line 1787151
    iget-object v2, v0, LX/BT4;->b:LX/BT9;

    iget-object v2, v2, LX/BT9;->W:Lcom/facebook/video/creativeediting/analytics/TrimmerLoggingParams;

    iget-object v3, v0, LX/BT4;->a:LX/BTI;

    .line 1787152
    sget-object v0, LX/BTI;->LEFT:LX/BTI;

    if-ne v3, v0, :cond_d

    .line 1787153
    iget v0, v2, Lcom/facebook/video/creativeediting/analytics/TrimmerLoggingParams;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, v2, Lcom/facebook/video/creativeediting/analytics/TrimmerLoggingParams;->a:I

    .line 1787154
    :cond_3
    :goto_3
    iget-object v0, p0, LX/BTK;->e:LX/BTL;

    invoke-virtual {v0}, LX/BTL;->d()V

    .line 1787155
    iput-boolean v4, p0, LX/BTK;->o:Z

    goto :goto_0

    .line 1787156
    :pswitch_2
    iget v0, p0, LX/BTK;->g:I

    .line 1787157
    if-gt v1, v0, :cond_e

    const/4 v2, 0x1

    :goto_4
    move v0, v2

    .line 1787158
    iget v2, p0, LX/BTK;->g:I

    .line 1787159
    iget-object v3, p0, LX/BTK;->j:Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;

    invoke-virtual {v3}, Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;->getZoomedOutWidth()I

    move-result v3

    sub-int/2addr v3, v2

    if-lt v1, v3, :cond_f

    const/4 v3, 0x1

    :goto_5
    move v2, v3

    .line 1787160
    iput-boolean v7, p0, LX/BTK;->o:Z

    .line 1787161
    iget-object v3, p0, LX/BTK;->c:LX/BTN;

    .line 1787162
    iget-boolean v5, v3, LX/BTN;->h:Z

    move v3, v5

    .line 1787163
    if-eqz v3, :cond_4

    if-nez v0, :cond_4

    if-nez v2, :cond_4

    .line 1787164
    iget-object v3, p0, LX/BTK;->c:LX/BTN;

    invoke-virtual {v3}, LX/BTN;->b()V

    .line 1787165
    iget-object v3, p0, LX/BTK;->c:LX/BTN;

    .line 1787166
    iput-boolean v4, v3, LX/BTN;->h:Z

    .line 1787167
    :cond_4
    iget-object v3, p0, LX/BTK;->b:LX/BTP;

    .line 1787168
    iget-boolean v4, v3, LX/BTP;->h:Z

    move v3, v4

    .line 1787169
    if-nez v3, :cond_7

    .line 1787170
    iget-object v0, p0, LX/BTK;->b:LX/BTP;

    iget-object v2, p0, LX/BTK;->m:LX/BTI;

    invoke-virtual {v0, v2}, LX/BTP;->a(LX/BTI;)V

    .line 1787171
    :cond_5
    :goto_6
    iget v0, p0, LX/BTK;->p:I

    sub-int v0, v1, v0

    .line 1787172
    iget-object v2, p0, LX/BTK;->m:LX/BTI;

    sget-object v3, LX/BTI;->LEFT:LX/BTI;

    if-ne v2, v3, :cond_9

    .line 1787173
    iget-object v2, p0, LX/BTK;->l:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v2

    iget v3, p0, LX/BTK;->i:I

    sub-int/2addr v2, v3

    iget v3, p0, LX/BTK;->h:I

    sub-int/2addr v2, v3

    if-lt v1, v2, :cond_10

    const/4 v2, 0x1

    :goto_7
    move v2, v2

    .line 1787174
    if-nez v2, :cond_9

    iget-object v2, p0, LX/BTK;->k:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v2

    iget-object v3, p0, LX/BTK;->k:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getRight()I

    move-result v3

    invoke-direct {p0, v2, v3, v0}, LX/BTK;->a(III)Z

    move-result v2

    if-eqz v2, :cond_9

    iget-object v2, p0, LX/BTK;->f:LX/BTZ;

    iget-object v3, p0, LX/BTK;->d:LX/BTX;

    iget-object v4, p0, LX/BTK;->b:LX/BTP;

    .line 1787175
    iget-boolean v5, v4, LX/BTP;->h:Z

    move v4, v5

    .line 1787176
    iget-object v5, p0, LX/BTK;->a:LX/BTJ;

    invoke-virtual {v5}, LX/BTJ;->a()I

    move-result v5

    add-int/2addr v5, v0

    iget-object v6, p0, LX/BTK;->a:LX/BTJ;

    invoke-virtual {v6}, LX/BTJ;->b()I

    move-result v6

    invoke-virtual {v3, v4, v5, v6}, LX/BTX;->a(ZII)I

    move-result v3

    invoke-virtual {v2, v3}, LX/BTZ;->a(I)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 1787177
    iget-object v2, p0, LX/BTK;->n:LX/BT4;

    invoke-virtual {v2, v0}, LX/BT4;->a(I)V

    .line 1787178
    iput v1, p0, LX/BTK;->p:I

    .line 1787179
    :cond_6
    :goto_8
    iget-object v0, p0, LX/BTK;->e:LX/BTL;

    invoke-virtual {v0}, LX/BTL;->c()V

    goto/16 :goto_0

    .line 1787180
    :cond_7
    iget-object v3, p0, LX/BTK;->c:LX/BTN;

    .line 1787181
    iget-boolean v4, v3, LX/BTN;->h:Z

    move v3, v4

    .line 1787182
    if-nez v3, :cond_5

    .line 1787183
    if-eqz v0, :cond_8

    .line 1787184
    iget-object v0, p0, LX/BTK;->c:LX/BTN;

    sget-object v2, LX/BTM;->LEFT:LX/BTM;

    iget-object v3, p0, LX/BTK;->m:LX/BTI;

    invoke-virtual {v0, v2, v3}, LX/BTN;->a(LX/BTM;LX/BTI;)V

    goto :goto_6

    .line 1787185
    :cond_8
    if-eqz v2, :cond_5

    .line 1787186
    iget-object v0, p0, LX/BTK;->c:LX/BTN;

    sget-object v2, LX/BTM;->RIGHT:LX/BTM;

    iget-object v3, p0, LX/BTK;->m:LX/BTI;

    invoke-virtual {v0, v2, v3}, LX/BTN;->a(LX/BTM;LX/BTI;)V

    goto :goto_6

    .line 1787187
    :cond_9
    iget-object v2, p0, LX/BTK;->m:LX/BTI;

    sget-object v3, LX/BTI;->RIGHT:LX/BTI;

    if-ne v2, v3, :cond_6

    .line 1787188
    iget-object v2, p0, LX/BTK;->k:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getRight()I

    move-result v2

    iget v3, p0, LX/BTK;->i:I

    add-int/2addr v2, v3

    iget v3, p0, LX/BTK;->h:I

    add-int/2addr v2, v3

    if-gt v1, v2, :cond_11

    const/4 v2, 0x1

    :goto_9
    move v2, v2

    .line 1787189
    if-nez v2, :cond_6

    iget-object v2, p0, LX/BTK;->l:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v2

    iget-object v3, p0, LX/BTK;->l:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getRight()I

    move-result v3

    invoke-direct {p0, v2, v3, v0}, LX/BTK;->a(III)Z

    move-result v2

    if-eqz v2, :cond_6

    iget-object v2, p0, LX/BTK;->f:LX/BTZ;

    iget-object v3, p0, LX/BTK;->d:LX/BTX;

    iget-object v4, p0, LX/BTK;->b:LX/BTP;

    .line 1787190
    iget-boolean v5, v4, LX/BTP;->h:Z

    move v4, v5

    .line 1787191
    iget-object v5, p0, LX/BTK;->a:LX/BTJ;

    invoke-virtual {v5}, LX/BTJ;->a()I

    move-result v5

    iget-object v6, p0, LX/BTK;->a:LX/BTJ;

    invoke-virtual {v6}, LX/BTJ;->b()I

    move-result v6

    add-int/2addr v6, v0

    invoke-virtual {v3, v4, v5, v6}, LX/BTX;->a(ZII)I

    move-result v3

    invoke-virtual {v2, v3}, LX/BTZ;->a(I)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 1787192
    iget-object v2, p0, LX/BTK;->n:LX/BT4;

    invoke-virtual {v2, v0}, LX/BT4;->a(I)V

    .line 1787193
    iput v1, p0, LX/BTK;->p:I

    goto :goto_8

    .line 1787194
    :cond_a
    iput-boolean p2, v1, LX/BTP;->h:Z

    .line 1787195
    iget-object v3, v1, LX/BTP;->b:LX/BTl;

    invoke-virtual {v3, v0}, LX/BTl;->a(I)I

    move-result v3

    .line 1787196
    sget-object v5, LX/BTI;->LEFT:LX/BTI;

    if-ne v2, v5, :cond_b

    .line 1787197
    iget-object v5, v1, LX/BTP;->c:LX/BTJ;

    invoke-virtual {v5, v3, p2}, LX/BTJ;->a(IZ)V

    .line 1787198
    iget-object v5, v1, LX/BTP;->c:LX/BTJ;

    iget-object v6, v1, LX/BTP;->b:LX/BTl;

    iget-object p1, v1, LX/BTP;->c:LX/BTJ;

    invoke-virtual {p1}, LX/BTJ;->b()I

    move-result p1

    invoke-virtual {v6, p1}, LX/BTl;->a(I)I

    move-result v6

    invoke-virtual {v5, v6, p2}, LX/BTJ;->b(IZ)V

    .line 1787199
    :goto_a
    iget-object v5, v1, LX/BTP;->f:Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;

    int-to-float v3, v3

    invoke-virtual {v5, v3}, Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;->a(F)V

    .line 1787200
    iget-object v3, v1, LX/BTP;->d:LX/BTL;

    invoke-virtual {v3}, LX/BTL;->d()V

    goto/16 :goto_1

    .line 1787201
    :cond_b
    iget-object v5, v1, LX/BTP;->c:LX/BTJ;

    invoke-virtual {v5, v3, p2}, LX/BTJ;->b(IZ)V

    .line 1787202
    iget-object v5, v1, LX/BTP;->c:LX/BTJ;

    iget-object v6, v1, LX/BTP;->b:LX/BTl;

    iget-object p1, v1, LX/BTP;->c:LX/BTJ;

    invoke-virtual {p1}, LX/BTJ;->a()I

    move-result p1

    invoke-virtual {v6, p1}, LX/BTl;->a(I)I

    move-result v6

    invoke-virtual {v5, v6, p2}, LX/BTJ;->a(IZ)V

    goto :goto_a

    .line 1787203
    :cond_c
    iget-object v2, v0, LX/BT4;->b:LX/BT9;

    .line 1787204
    iget-object v3, v2, LX/BT9;->r:LX/BTL;

    invoke-virtual {v3}, LX/BTL;->c()V

    .line 1787205
    iget-object v3, v2, LX/BT9;->o:LX/BTX;

    iget-object v5, v2, LX/BT9;->q:LX/BTJ;

    invoke-virtual {v5}, LX/BTJ;->b()I

    move-result v5

    const/4 v6, 0x0

    invoke-virtual {v3, v5, v6}, LX/BTX;->a(IZ)I

    move-result v3

    .line 1787206
    add-int/lit16 v3, v3, -0x3e8

    .line 1787207
    iget-object v5, v2, LX/BT9;->r:LX/BTL;

    iget-object v6, v2, LX/BT9;->o:LX/BTX;

    invoke-virtual {v6, v3}, LX/BTX;->a(I)I

    move-result v3

    iget-object v6, v2, LX/BT9;->K:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getWidth()I

    move-result v6

    sub-int/2addr v3, v6

    invoke-virtual {v5, v3}, LX/BTL;->a(I)V

    .line 1787208
    invoke-static {v2}, LX/BT9;->y(LX/BT9;)V

    .line 1787209
    iget-object v2, v0, LX/BT4;->b:LX/BT9;

    iget-object v2, v2, LX/BT9;->M:LX/BT8;

    if-eqz v2, :cond_2

    .line 1787210
    iget-object v2, v0, LX/BT4;->b:LX/BT9;

    iget-object v2, v2, LX/BT9;->M:LX/BT8;

    invoke-virtual {v2}, LX/0ht;->l()V

    goto/16 :goto_2

    .line 1787211
    :cond_d
    iget v0, v2, Lcom/facebook/video/creativeediting/analytics/TrimmerLoggingParams;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, v2, Lcom/facebook/video/creativeediting/analytics/TrimmerLoggingParams;->b:I

    goto/16 :goto_3

    :cond_e
    const/4 v2, 0x0

    goto/16 :goto_4

    :cond_f
    const/4 v3, 0x0

    goto/16 :goto_5

    :cond_10
    const/4 v2, 0x0

    goto/16 :goto_7

    :cond_11
    const/4 v2, 0x0

    goto/16 :goto_9

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
