.class public final LX/Atg;
.super LX/0wa;
.source ""


# instance fields
.field public final a:LX/0wY;

.field public b:Landroid/view/View;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:I

.field public final synthetic d:LX/Ath;


# direct methods
.method public constructor <init>(LX/Ath;LX/0wY;)V
    .locals 0

    .prologue
    .line 1721680
    iput-object p1, p0, LX/Atg;->d:LX/Ath;

    invoke-direct {p0}, LX/0wa;-><init>()V

    .line 1721681
    iput-object p2, p0, LX/Atg;->a:LX/0wY;

    .line 1721682
    return-void
.end method


# virtual methods
.method public final a(J)V
    .locals 4

    .prologue
    .line 1721683
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget v2, p0, LX/Atg;->c:I

    int-to-long v2, v2

    sub-long/2addr v0, v2

    long-to-int v0, v0

    .line 1721684
    iget-object v1, p0, LX/Atg;->d:LX/Ath;

    iget v1, v1, LX/Ath;->j:I

    if-ge v0, v1, :cond_0

    .line 1721685
    iget-object v1, p0, LX/Atg;->d:LX/Ath;

    invoke-static {v1}, LX/Ath;->c$redex0(LX/Ath;)Lcom/facebook/facecast/view/CircularProgressView;

    move-result-object v1

    int-to-float v0, v0

    iget-object v2, p0, LX/Atg;->d:LX/Ath;

    iget v2, v2, LX/Ath;->j:I

    int-to-float v2, v2

    div-float/2addr v0, v2

    invoke-virtual {v1, v0}, Lcom/facebook/facecast/view/CircularProgressView;->setProgress(F)V

    .line 1721686
    iget-object v0, p0, LX/Atg;->a:LX/0wY;

    invoke-interface {v0, p0}, LX/0wY;->a(LX/0wa;)V

    .line 1721687
    :goto_0
    return-void

    .line 1721688
    :cond_0
    iget-object v0, p0, LX/Atg;->d:LX/Ath;

    invoke-static {v0}, LX/Ath;->c$redex0(LX/Ath;)Lcom/facebook/facecast/view/CircularProgressView;

    move-result-object v0

    iget-object v1, p0, LX/Atg;->d:LX/Ath;

    iget v1, v1, LX/Ath;->j:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/facebook/facecast/view/CircularProgressView;->setProgress(F)V

    goto :goto_0
.end method
