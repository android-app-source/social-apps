.class public final enum LX/BVz;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/BVz;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/BVz;

.field public static final enum BACKGROUND:LX/BVz;

.field public static final enum FOCUSABLE:LX/BVz;

.field public static final enum ID:LX/BVz;

.field public static final enum MIN_WIDTH:LX/BVz;

.field public static final enum PADDING:LX/BVz;

.field public static final enum PADDING_BOTTOM:LX/BVz;

.field public static final enum PADDING_LEFT:LX/BVz;

.field public static final enum PADDING_RIGHT:LX/BVz;

.field public static final enum PADDING_TOP:LX/BVz;

.field public static final enum VISIBILITY:LX/BVz;


# instance fields
.field private final mValue:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1791588
    new-instance v0, LX/BVz;

    const-string v1, "PADDING_TOP"

    const-string v2, "paddingTop"

    invoke-direct {v0, v1, v4, v2}, LX/BVz;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BVz;->PADDING_TOP:LX/BVz;

    .line 1791589
    new-instance v0, LX/BVz;

    const-string v1, "PADDING_RIGHT"

    const-string v2, "paddingRight"

    invoke-direct {v0, v1, v5, v2}, LX/BVz;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BVz;->PADDING_RIGHT:LX/BVz;

    .line 1791590
    new-instance v0, LX/BVz;

    const-string v1, "PADDING_LEFT"

    const-string v2, "paddingLeft"

    invoke-direct {v0, v1, v6, v2}, LX/BVz;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BVz;->PADDING_LEFT:LX/BVz;

    .line 1791591
    new-instance v0, LX/BVz;

    const-string v1, "PADDING_BOTTOM"

    const-string v2, "paddingBottom"

    invoke-direct {v0, v1, v7, v2}, LX/BVz;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BVz;->PADDING_BOTTOM:LX/BVz;

    .line 1791592
    new-instance v0, LX/BVz;

    const-string v1, "PADDING"

    const-string v2, "padding"

    invoke-direct {v0, v1, v8, v2}, LX/BVz;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BVz;->PADDING:LX/BVz;

    .line 1791593
    new-instance v0, LX/BVz;

    const-string v1, "ID"

    const/4 v2, 0x5

    const-string v3, "id"

    invoke-direct {v0, v1, v2, v3}, LX/BVz;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BVz;->ID:LX/BVz;

    .line 1791594
    new-instance v0, LX/BVz;

    const-string v1, "FOCUSABLE"

    const/4 v2, 0x6

    const-string v3, "focusable"

    invoke-direct {v0, v1, v2, v3}, LX/BVz;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BVz;->FOCUSABLE:LX/BVz;

    .line 1791595
    new-instance v0, LX/BVz;

    const-string v1, "VISIBILITY"

    const/4 v2, 0x7

    const-string v3, "visibility"

    invoke-direct {v0, v1, v2, v3}, LX/BVz;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BVz;->VISIBILITY:LX/BVz;

    .line 1791596
    new-instance v0, LX/BVz;

    const-string v1, "BACKGROUND"

    const/16 v2, 0x8

    const-string v3, "background"

    invoke-direct {v0, v1, v2, v3}, LX/BVz;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BVz;->BACKGROUND:LX/BVz;

    .line 1791597
    new-instance v0, LX/BVz;

    const-string v1, "MIN_WIDTH"

    const/16 v2, 0x9

    const-string v3, "minWidth"

    invoke-direct {v0, v1, v2, v3}, LX/BVz;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BVz;->MIN_WIDTH:LX/BVz;

    .line 1791598
    const/16 v0, 0xa

    new-array v0, v0, [LX/BVz;

    sget-object v1, LX/BVz;->PADDING_TOP:LX/BVz;

    aput-object v1, v0, v4

    sget-object v1, LX/BVz;->PADDING_RIGHT:LX/BVz;

    aput-object v1, v0, v5

    sget-object v1, LX/BVz;->PADDING_LEFT:LX/BVz;

    aput-object v1, v0, v6

    sget-object v1, LX/BVz;->PADDING_BOTTOM:LX/BVz;

    aput-object v1, v0, v7

    sget-object v1, LX/BVz;->PADDING:LX/BVz;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/BVz;->ID:LX/BVz;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/BVz;->FOCUSABLE:LX/BVz;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/BVz;->VISIBILITY:LX/BVz;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/BVz;->BACKGROUND:LX/BVz;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/BVz;->MIN_WIDTH:LX/BVz;

    aput-object v2, v0, v1

    sput-object v0, LX/BVz;->$VALUES:[LX/BVz;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1791576
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1791577
    iput-object p3, p0, LX/BVz;->mValue:Ljava/lang/String;

    .line 1791578
    return-void
.end method

.method public static from(Ljava/lang/String;)LX/BVz;
    .locals 4

    .prologue
    .line 1791579
    invoke-static {}, LX/BVz;->values()[LX/BVz;

    move-result-object v2

    .line 1791580
    const/4 v0, 0x0

    :goto_0
    array-length v1, v2

    if-ge v0, v1, :cond_1

    .line 1791581
    aget-object v1, v2, v0

    .line 1791582
    iget-object v3, v1, LX/BVz;->mValue:Ljava/lang/String;

    invoke-virtual {v3, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move-object v0, v1

    .line 1791583
    :goto_1
    return-object v0

    .line 1791584
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1791585
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)LX/BVz;
    .locals 1

    .prologue
    .line 1791586
    const-class v0, LX/BVz;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/BVz;

    return-object v0
.end method

.method public static values()[LX/BVz;
    .locals 1

    .prologue
    .line 1791587
    sget-object v0, LX/BVz;->$VALUES:[LX/BVz;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/BVz;

    return-object v0
.end method
