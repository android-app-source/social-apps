.class public final LX/B89;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/B8A;


# direct methods
.method public constructor <init>(LX/B8A;)V
    .locals 0

    .prologue
    .line 1748459
    iput-object p1, p0, LX/B89;->a:LX/B8A;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const v0, -0x285d9417

    invoke-static {v5, v4, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1748460
    iget-object v0, p0, LX/B89;->a:LX/B8A;

    invoke-virtual {v0}, LX/B8A;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v2, "input_method"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 1748461
    iget-object v2, p0, LX/B89;->a:LX/B8A;

    invoke-virtual {v2}, LX/B8A;->getRootView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1748462
    iget-object v0, p0, LX/B89;->a:LX/B8A;

    iget-object v0, v0, LX/B8A;->d:LX/B7W;

    new-instance v2, LX/B7Z;

    const/4 v3, 0x0

    invoke-direct {v2, v4, v3}, LX/B7Z;-><init>(ZLandroid/view/View;)V

    invoke-virtual {v0, v2}, LX/0b4;->a(LX/0b7;)V

    .line 1748463
    iget-object v0, p0, LX/B89;->a:LX/B8A;

    iget-object v0, v0, LX/B8A;->l:LX/7Tj;

    if-nez v0, :cond_0

    .line 1748464
    iget-object v0, p0, LX/B89;->a:LX/B8A;

    iget-object v2, p0, LX/B89;->a:LX/B8A;

    iget-object v2, v2, LX/B8A;->b:LX/7Tk;

    iget-object v3, p0, LX/B89;->a:LX/B8A;

    invoke-virtual {v3}, LX/B8A;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3, v4}, LX/7Tk;->a(Landroid/content/Context;Z)LX/7Tj;

    move-result-object v2

    .line 1748465
    iput-object v2, v0, LX/B8A;->l:LX/7Tj;

    .line 1748466
    :cond_0
    iget-object v0, p0, LX/B89;->a:LX/B8A;

    iget-object v0, v0, LX/B8A;->l:LX/7Tj;

    new-instance v2, LX/B88;

    invoke-direct {v2, p0}, LX/B88;-><init>(LX/B89;)V

    .line 1748467
    iput-object v2, v0, LX/7Tj;->u:LX/6u6;

    .line 1748468
    iget-object v0, p0, LX/B89;->a:LX/B8A;

    iget-object v0, v0, LX/B8A;->l:LX/7Tj;

    invoke-virtual {v0, p1}, LX/0ht;->a(Landroid/view/View;)V

    .line 1748469
    const v0, -0x39a1b8b2

    invoke-static {v5, v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
