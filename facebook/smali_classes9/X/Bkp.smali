.class public final LX/Bkp;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/events/create/ui/ThemeSuggestifier;


# direct methods
.method public constructor <init>(Lcom/facebook/events/create/ui/ThemeSuggestifier;)V
    .locals 0

    .prologue
    .line 1814939
    iput-object p1, p0, LX/Bkp;->a:Lcom/facebook/events/create/ui/ThemeSuggestifier;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1814940
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1814941
    check-cast v0, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel;

    .line 1814942
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel;->a()LX/1vs;

    move-result-object v3

    iget v3, v3, LX/1vs;->b:I

    if-eqz v3, :cond_0

    move v1, v2

    :cond_0
    if-eqz v1, :cond_1

    .line 1814943
    invoke-virtual {v0}, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1814944
    const-class v3, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel$EventThemesModel$NodesModel;

    invoke-virtual {v1, v0, v2, v3}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v0

    iget-object v1, p0, LX/Bkp;->a:Lcom/facebook/events/create/ui/ThemeSuggestifier;

    iget-object v1, v1, Lcom/facebook/events/create/ui/ThemeSuggestifier;->k:LX/Bkr;

    if-eqz v0, :cond_2

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, LX/Bkr;->a(LX/0Px;)V

    .line 1814945
    :cond_1
    return-void

    .line 1814946
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 1814947
    :cond_2
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1814948
    goto :goto_0
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 1814938
    return-void
.end method

.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1814937
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    invoke-direct {p0, p1}, LX/Bkp;->a(Lcom/facebook/graphql/executor/GraphQLResult;)V

    return-void
.end method
