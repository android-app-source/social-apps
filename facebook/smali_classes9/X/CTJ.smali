.class public LX/CTJ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public a:Z

.field public final m:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

.field public final n:Ljava/lang/String;

.field public final o:Ljava/lang/String;

.field public final p:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;",
            "LX/CTv;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 1895663
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1895664
    iput-boolean v7, p0, LX/CTJ;->a:Z

    .line 1895665
    iget-object v0, p0, LX/CTJ;->m:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->FORM_FIELD:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;->l()Ljava/lang/String;

    move-result-object v0

    .line 1895666
    :goto_0
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;->j()Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    move-result-object v1

    iput-object v1, p0, LX/CTJ;->m:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    .line 1895667
    if-eqz p2, :cond_1

    :goto_1
    iput-object p2, p0, LX/CTJ;->n:Ljava/lang/String;

    .line 1895668
    invoke-virtual {p0, p1}, LX/CTJ;->a(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/CTJ;->o:Ljava/lang/String;

    .line 1895669
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/CTJ;->p:Ljava/util/HashMap;

    .line 1895670
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;->N()LX/2uF;

    move-result-object v0

    invoke-virtual {v0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 1895671
    iget-object v3, p0, LX/CTJ;->p:Ljava/util/HashMap;

    const/4 v4, 0x1

    const-class v5, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;

    const/4 v6, 0x0

    invoke-virtual {v2, v0, v4, v5, v6}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v4

    const-class v5, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformEventListenersFragmentModel$EventHandlerModel;

    invoke-virtual {v2, v0, v7, v5}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformEventListenersFragmentModel$EventHandlerModel;

    invoke-static {v0}, LX/CTv;->a(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformEventListenersFragmentModel$EventHandlerModel;)LX/CTv;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 1895672
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;->o()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    move-object p2, v0

    .line 1895673
    goto :goto_1

    .line 1895674
    :cond_2
    return-void
.end method


# virtual methods
.method public a(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1895675
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;->o()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/CXh;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
