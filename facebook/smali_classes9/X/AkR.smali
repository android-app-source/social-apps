.class public final enum LX/AkR;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/AkR;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/AkR;

.field public static final enum BOTTOM_SEEN:LX/AkR;

.field public static final enum TOP_SEEN:LX/AkR;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1709073
    new-instance v0, LX/AkR;

    const-string v1, "TOP_SEEN"

    invoke-direct {v0, v1, v2}, LX/AkR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AkR;->TOP_SEEN:LX/AkR;

    .line 1709074
    new-instance v0, LX/AkR;

    const-string v1, "BOTTOM_SEEN"

    invoke-direct {v0, v1, v3}, LX/AkR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AkR;->BOTTOM_SEEN:LX/AkR;

    .line 1709075
    const/4 v0, 0x2

    new-array v0, v0, [LX/AkR;

    sget-object v1, LX/AkR;->TOP_SEEN:LX/AkR;

    aput-object v1, v0, v2

    sget-object v1, LX/AkR;->BOTTOM_SEEN:LX/AkR;

    aput-object v1, v0, v3

    sput-object v0, LX/AkR;->$VALUES:[LX/AkR;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1709076
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/AkR;
    .locals 1

    .prologue
    .line 1709072
    const-class v0, LX/AkR;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/AkR;

    return-object v0
.end method

.method public static values()[LX/AkR;
    .locals 1

    .prologue
    .line 1709071
    sget-object v0, LX/AkR;->$VALUES:[LX/AkR;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/AkR;

    return-object v0
.end method
