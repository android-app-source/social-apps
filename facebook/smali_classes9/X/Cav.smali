.class public final LX/Cav;
.super LX/7UT;
.source ""


# instance fields
.field public final synthetic a:LX/Caz;


# direct methods
.method public constructor <init>(LX/Caz;)V
    .locals 0

    .prologue
    .line 1918705
    iput-object p1, p0, LX/Cav;->a:LX/Caz;

    invoke-direct {p0}, LX/7UT;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Matrix;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1918706
    iget-object v0, p0, LX/Cav;->a:LX/Caz;

    invoke-virtual {v0}, LX/Caz;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/Cav;->a:LX/Caz;

    iget-boolean v0, v0, LX/Caz;->t:Z

    if-eqz v0, :cond_2

    .line 1918707
    iget-object v0, p0, LX/Cav;->a:LX/Caz;

    .line 1918708
    iput-boolean v1, v0, LX/Caz;->t:Z

    .line 1918709
    iget-object v0, p0, LX/Cav;->a:LX/Caz;

    invoke-static {v0, v1}, LX/Caz;->b$redex0(LX/Caz;Z)V

    .line 1918710
    :cond_0
    :goto_0
    iget-object v0, p0, LX/Cav;->a:LX/Caz;

    iget-object v0, v0, LX/Caz;->s:LX/Cb5;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/Cav;->a:LX/Caz;

    iget-object v0, v0, LX/Caz;->s:LX/Cb5;

    invoke-virtual {v0}, LX/Cb5;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1918711
    iget-object v0, p0, LX/Cav;->a:LX/Caz;

    iget-object v0, v0, LX/Caz;->s:LX/Cb5;

    .line 1918712
    iget-object v1, v0, LX/Cb5;->i:LX/0am;

    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v1

    if-nez v1, :cond_3

    .line 1918713
    :cond_1
    :goto_1
    return-void

    .line 1918714
    :cond_2
    iget-object v0, p0, LX/Cav;->a:LX/Caz;

    iget-object v0, v0, LX/Caz;->b:LX/CbI;

    invoke-virtual {v0}, LX/CbI;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 1918715
    iget-object v0, p0, LX/Cav;->a:LX/Caz;

    invoke-virtual {v0}, LX/Caz;->d()V

    goto :goto_0

    .line 1918716
    :cond_3
    iget-object v1, v0, LX/Cb5;->i:LX/0am;

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/8Jd;

    invoke-virtual {v1, p1}, LX/8Jd;->setTransformMatrix(Landroid/graphics/Matrix;)V

    goto :goto_1
.end method

.method public final a(Landroid/graphics/PointF;Landroid/graphics/PointF;)V
    .locals 2
    .param p2    # Landroid/graphics/PointF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1918717
    iget-object v0, p0, LX/Cav;->a:LX/Caz;

    iget-object v0, v0, LX/Caz;->e:LX/CbH;

    .line 1918718
    iget-boolean v1, v0, LX/CbH;->g:Z

    move v0, v1

    .line 1918719
    if-eqz v0, :cond_1

    iget-object v0, p0, LX/Cav;->a:LX/Caz;

    iget-object v0, v0, LX/Caz;->q:LX/5kD;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/Cav;->a:LX/Caz;

    iget-object v0, v0, LX/Caz;->q:LX/5kD;

    invoke-interface {v0}, LX/5kD;->r()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1918720
    :cond_0
    :goto_0
    return-void

    .line 1918721
    :cond_1
    iget-object v0, p0, LX/Cav;->a:LX/Caz;

    iget-object v0, v0, LX/Caz;->e:LX/CbH;

    .line 1918722
    iget-boolean v1, v0, LX/CbH;->c:Z

    move v0, v1

    .line 1918723
    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    .line 1918724
    iget-object v0, p0, LX/Cav;->a:LX/Caz;

    iget-object v0, v0, LX/Caz;->b:LX/CbI;

    invoke-virtual {v0}, LX/CbI;->a()V

    .line 1918725
    iget-object v0, p0, LX/Cav;->a:LX/Caz;

    iget-object v0, v0, LX/Caz;->a:Lcom/facebook/photos/tagging/shared/FaceBoxesView;

    invoke-virtual {v0, p1}, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->a(Landroid/graphics/PointF;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1918726
    iget-object v0, p0, LX/Cav;->a:LX/Caz;

    invoke-static {v0, p2}, LX/Caz;->a$redex0(LX/Caz;Landroid/graphics/PointF;)V

    goto :goto_0
.end method

.method public final b(Landroid/graphics/PointF;Landroid/graphics/PointF;)V
    .locals 1
    .param p2    # Landroid/graphics/PointF;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1918727
    invoke-super {p0, p1, p2}, LX/7UT;->b(Landroid/graphics/PointF;Landroid/graphics/PointF;)V

    .line 1918728
    iget-object v0, p0, LX/Cav;->a:LX/Caz;

    iget-object v0, v0, LX/Caz;->e:LX/CbH;

    .line 1918729
    iget-boolean p1, v0, LX/CbH;->g:Z

    move v0, p1

    .line 1918730
    if-eqz v0, :cond_1

    iget-object v0, p0, LX/Cav;->a:LX/Caz;

    iget-object v0, v0, LX/Caz;->q:LX/5kD;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/Cav;->a:LX/Caz;

    iget-object v0, v0, LX/Caz;->q:LX/5kD;

    invoke-interface {v0}, LX/5kD;->r()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1918731
    :cond_0
    :goto_0
    return-void

    .line 1918732
    :cond_1
    iget-object v0, p0, LX/Cav;->a:LX/Caz;

    iget-object v0, v0, LX/Caz;->e:LX/CbH;

    .line 1918733
    iget-boolean p1, v0, LX/CbH;->c:Z

    move v0, p1

    .line 1918734
    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    .line 1918735
    iget-object v0, p0, LX/Cav;->a:LX/Caz;

    invoke-static {v0, p2}, LX/Caz;->a$redex0(LX/Caz;Landroid/graphics/PointF;)V

    goto :goto_0
.end method
