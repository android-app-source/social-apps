.class public final LX/All;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# instance fields
.field public final synthetic a:LX/Ale;


# direct methods
.method public constructor <init>(LX/Ale;)V
    .locals 0

    .prologue
    .line 1710237
    iput-object p1, p0, LX/All;->a:LX/Ale;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 1710238
    return-void
.end method

.method public final onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2

    .prologue
    .line 1710239
    iget-object v0, p0, LX/All;->a:LX/Ale;

    invoke-virtual {v0}, LX/Ale;->h()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1710240
    iget-object v0, p0, LX/All;->a:LX/Ale;

    iget-object v0, v0, LX/Ale;->b:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v1, p0, LX/All;->a:LX/Ale;

    iget v1, v1, LX/Ale;->h:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setTranslationX(F)V

    .line 1710241
    iget-object v0, p0, LX/All;->a:LX/Ale;

    .line 1710242
    iget-object v1, v0, LX/Ale;->b:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    const/4 p1, 0x0

    invoke-virtual {v1, p1}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setVisibility(I)V

    .line 1710243
    :cond_0
    iget-object v0, p0, LX/All;->a:LX/Ale;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/Ale;->setHasBeenShown(Z)V

    .line 1710244
    iget-object v0, p0, LX/All;->a:LX/Ale;

    iget-object v0, v0, LX/Ale;->f:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 1710245
    return-void
.end method

.method public final onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 1710246
    return-void
.end method

.method public final onAnimationStart(Landroid/animation/Animator;)V
    .locals 1

    .prologue
    .line 1710247
    iget-object v0, p0, LX/All;->a:LX/Ale;

    .line 1710248
    const/4 p0, 0x0

    .line 1710249
    invoke-virtual {v0, p0, p0, p0, p0}, LX/Ale;->setPadding(IIII)V

    .line 1710250
    return-void
.end method
