.class public LX/BUB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/BUB;


# instance fields
.field private final a:LX/0tQ;

.field private final b:LX/0Wd;

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/BUA;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/19w;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0tQ;LX/0Wd;LX/0Ot;LX/0Ot;)V
    .locals 0
    .param p2    # LX/0Wd;
        .annotation runtime Lcom/facebook/common/idleexecutor/DefaultIdleExecutor;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0tQ;",
            "Lcom/facebook/common/idleexecutor/IdleExecutor;",
            "LX/0Ot",
            "<",
            "LX/BUA;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/19w;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1788596
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1788597
    iput-object p1, p0, LX/BUB;->a:LX/0tQ;

    .line 1788598
    iput-object p2, p0, LX/BUB;->b:LX/0Wd;

    .line 1788599
    iput-object p3, p0, LX/BUB;->c:LX/0Ot;

    .line 1788600
    iput-object p4, p0, LX/BUB;->d:LX/0Ot;

    .line 1788601
    return-void
.end method

.method public static a(LX/0QB;)LX/BUB;
    .locals 7

    .prologue
    .line 1788602
    sget-object v0, LX/BUB;->e:LX/BUB;

    if-nez v0, :cond_1

    .line 1788603
    const-class v1, LX/BUB;

    monitor-enter v1

    .line 1788604
    :try_start_0
    sget-object v0, LX/BUB;->e:LX/BUB;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1788605
    if-eqz v2, :cond_0

    .line 1788606
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1788607
    new-instance v5, LX/BUB;

    invoke-static {v0}, LX/0tQ;->a(LX/0QB;)LX/0tQ;

    move-result-object v3

    check-cast v3, LX/0tQ;

    invoke-static {v0}, LX/0Wa;->a(LX/0QB;)LX/0Wd;

    move-result-object v4

    check-cast v4, LX/0Wd;

    const/16 v6, 0x37e9

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 p0, 0x132e

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v5, v3, v4, v6, p0}, LX/BUB;-><init>(LX/0tQ;LX/0Wd;LX/0Ot;LX/0Ot;)V

    .line 1788608
    move-object v0, v5

    .line 1788609
    sput-object v0, LX/BUB;->e:LX/BUB;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1788610
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1788611
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1788612
    :cond_1
    sget-object v0, LX/BUB;->e:LX/BUB;

    return-object v0

    .line 1788613
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1788614
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final init()V
    .locals 4

    .prologue
    .line 1788615
    iget-object v0, p0, LX/BUB;->a:LX/0tQ;

    const/4 v1, 0x0

    .line 1788616
    invoke-virtual {v0}, LX/0tQ;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, v0, LX/0tQ;->b:LX/0Uh;

    const/16 v3, 0x44b

    invoke-virtual {v2, v3, v1}, LX/0Uh;->a(IZ)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    move v0, v1

    .line 1788617
    if-eqz v0, :cond_1

    .line 1788618
    iget-object v0, p0, LX/BUB;->b:LX/0Wd;

    new-instance v1, Lcom/facebook/video/downloadmanager/DownloadManagerInitializer$1;

    invoke-direct {v1, p0}, Lcom/facebook/video/downloadmanager/DownloadManagerInitializer$1;-><init>(LX/BUB;)V

    invoke-interface {v0, v1}, LX/0TD;->a(Ljava/lang/Runnable;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1788619
    :cond_1
    return-void
.end method
