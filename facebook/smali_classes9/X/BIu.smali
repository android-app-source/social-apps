.class public final LX/BIu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/BIX;


# instance fields
.field public final synthetic a:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;)V
    .locals 0

    .prologue
    .line 1771017
    iput-object p1, p0, LX/BIu;->a:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 1771018
    iget-object v0, p0, LX/BIu;->a:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    .line 1771019
    iget-boolean v1, v0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->w:Z

    if-eqz v1, :cond_1

    .line 1771020
    iget-object v1, v0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->f:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;

    .line 1771021
    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->c:Z

    .line 1771022
    iget-object v2, v1, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v2, :cond_0

    .line 1771023
    iget-object v2, v1, Lcom/facebook/facerec/manager/FaceBoxPrioritizer;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 p0, 0x0

    invoke-interface {v2, p0}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 1771024
    :cond_0
    iget-object v1, v0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->T:LX/9iw;

    invoke-virtual {v1}, LX/9iw;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1771025
    iget-object v1, v0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->T:LX/9iw;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/9iw;->a(Z)V

    .line 1771026
    :cond_1
    const/4 v1, 0x0

    iput-object v1, v0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->S:LX/9ip;

    .line 1771027
    const/4 v1, -0x2

    iput v1, v0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->F:I

    .line 1771028
    return-void
.end method

.method public final a(LX/9iQ;I)V
    .locals 6

    .prologue
    .line 1771029
    iget-object v0, p0, LX/BIu;->a:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    .line 1771030
    const/4 v3, 0x0

    .line 1771031
    move-object v1, p1

    check-cast v1, LX/9ip;

    .line 1771032
    iget-boolean v2, v0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->w:Z

    if-eqz v2, :cond_1

    iget-boolean v2, v0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->t:Z

    if-eqz v2, :cond_1

    .line 1771033
    invoke-virtual {v1}, LX/9ip;->j()V

    .line 1771034
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1771035
    invoke-static {v0}, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->o(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;)Lcom/facebook/photos/base/media/PhotoItem;

    move-result-object v2

    const/4 p2, 0x0

    .line 1771036
    if-eqz v2, :cond_0

    .line 1771037
    iget-object p0, v2, Lcom/facebook/photos/base/media/PhotoItem;->i:Lcom/facebook/photos/base/photos/LocalPhoto;

    move-object p0, p0

    .line 1771038
    if-eqz p0, :cond_0

    .line 1771039
    iget-object p0, v2, Lcom/facebook/photos/base/media/PhotoItem;->i:Lcom/facebook/photos/base/photos/LocalPhoto;

    move-object p0, p0

    .line 1771040
    instance-of p0, p0, Lcom/facebook/photos/base/photos/LocalPhoto;

    if-nez p0, :cond_6

    :cond_0
    move p0, p2

    .line 1771041
    :goto_0
    move v2, p0

    .line 1771042
    if-nez v2, :cond_2

    move v2, v4

    .line 1771043
    :goto_1
    move v2, v2

    .line 1771044
    iput-boolean v2, v1, LX/9ip;->o:Z

    .line 1771045
    :cond_1
    invoke-virtual {v1}, LX/9iQ;->getZoomableImageView()Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;

    move-result-object v2

    .line 1771046
    iput-boolean v3, v2, LX/7UR;->i:Z

    .line 1771047
    invoke-virtual {v1}, LX/9iQ;->getZoomableImageView()Lcom/facebook/widget/images/zoomableimageview/ZoomableImageView;

    move-result-object v2

    .line 1771048
    iput-boolean v3, v2, LX/7UR;->j:Z

    .line 1771049
    new-instance v2, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment$10;

    invoke-direct {v2, v0, v1}, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment$10;-><init>(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;LX/9ip;)V

    invoke-virtual {v1, v2, v3}, LX/9iQ;->a(Ljava/lang/Runnable;Z)V

    .line 1771050
    new-instance v2, LX/BIm;

    invoke-direct {v2, v0}, LX/BIm;-><init>(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;)V

    .line 1771051
    iput-object v2, v1, LX/9ip;->h:LX/BIm;

    .line 1771052
    return-void

    .line 1771053
    :cond_2
    iget-object v2, p1, LX/9iQ;->b:LX/74w;

    move-object v2, v2

    .line 1771054
    check-cast v2, LX/74x;

    .line 1771055
    iget-object p0, v0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->c:LX/75F;

    invoke-virtual {p0, v2}, LX/75F;->b(LX/74x;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 1771056
    iget-object p0, v0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->c:LX/75F;

    invoke-virtual {p0, v2}, LX/75F;->a(LX/74x;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_3
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/photos/base/tagging/FaceBox;

    .line 1771057
    invoke-virtual {v2}, Lcom/facebook/photos/base/tagging/FaceBox;->o()Z

    move-result v2

    if-nez v2, :cond_3

    move v2, v5

    .line 1771058
    goto :goto_1

    :cond_4
    move v2, v4

    .line 1771059
    goto :goto_1

    :cond_5
    move v2, v5

    .line 1771060
    goto :goto_1

    .line 1771061
    :cond_6
    iget-object p0, v2, Lcom/facebook/photos/base/media/PhotoItem;->i:Lcom/facebook/photos/base/photos/LocalPhoto;

    move-object p0, p0

    .line 1771062
    check-cast p0, Lcom/facebook/photos/base/photos/LocalPhoto;

    .line 1771063
    iget-boolean v2, p0, Lcom/facebook/photos/base/photos/LocalPhoto;->f:Z

    move p0, v2

    .line 1771064
    if-nez p0, :cond_7

    const/4 p0, 0x1

    goto :goto_0

    :cond_7
    move p0, p2

    goto :goto_0
.end method

.method public final b(LX/9iQ;I)V
    .locals 1

    .prologue
    .line 1771065
    iget-object v0, p0, LX/BIu;->a:Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    invoke-static {v0, p1, p2}, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->b(Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;LX/9iQ;I)V

    .line 1771066
    return-void
.end method
