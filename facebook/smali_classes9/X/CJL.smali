.class public LX/CJL;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile f:LX/CJL;


# instance fields
.field private final b:LX/0Zb;

.field private final c:LX/0TD;

.field private final d:Landroid/content/Context;

.field private final e:Ljava/util/concurrent/Executor;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1875380
    const-class v0, LX/CJL;

    sput-object v0, LX/CJL;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Zb;LX/0TD;Ljava/util/concurrent/Executor;Landroid/content/Context;)V
    .locals 0
    .param p2    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p3    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1875381
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1875382
    iput-object p1, p0, LX/CJL;->b:LX/0Zb;

    .line 1875383
    iput-object p2, p0, LX/CJL;->c:LX/0TD;

    .line 1875384
    iput-object p4, p0, LX/CJL;->d:Landroid/content/Context;

    .line 1875385
    iput-object p3, p0, LX/CJL;->e:Ljava/util/concurrent/Executor;

    .line 1875386
    return-void
.end method

.method public static a(LX/0QB;)LX/CJL;
    .locals 7

    .prologue
    .line 1875387
    sget-object v0, LX/CJL;->f:LX/CJL;

    if-nez v0, :cond_1

    .line 1875388
    const-class v1, LX/CJL;

    monitor-enter v1

    .line 1875389
    :try_start_0
    sget-object v0, LX/CJL;->f:LX/CJL;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1875390
    if-eqz v2, :cond_0

    .line 1875391
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1875392
    new-instance p0, LX/CJL;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v4

    check-cast v4, LX/0TD;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/Executor;

    const-class v6, Landroid/content/Context;

    invoke-interface {v0, v6}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/Context;

    invoke-direct {p0, v3, v4, v5, v6}, LX/CJL;-><init>(LX/0Zb;LX/0TD;Ljava/util/concurrent/Executor;Landroid/content/Context;)V

    .line 1875393
    move-object v0, p0

    .line 1875394
    sput-object v0, LX/CJL;->f:LX/CJL;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1875395
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1875396
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1875397
    :cond_1
    sget-object v0, LX/CJL;->f:LX/CJL;

    return-object v0

    .line 1875398
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1875399
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/CJL;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1875375
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "app_id"

    invoke-virtual {v0, v1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "content_source"

    invoke-virtual {v0, v1, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 1875376
    iget-object v1, p0, LX/CJL;->b:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1875377
    return-void
.end method


# virtual methods
.method public final b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1875378
    const-string v0, "cancel_inline_reply_dialog_event"

    const-string v1, "platform_app"

    invoke-static {p0, v0, p1, v1}, LX/CJL;->a(LX/CJL;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1875379
    return-void
.end method
