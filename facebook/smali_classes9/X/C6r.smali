.class public LX/C6r;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/C6p;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/C6s;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1850426
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/C6r;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/C6s;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1850423
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1850424
    iput-object p1, p0, LX/C6r;->b:LX/0Ot;

    .line 1850425
    return-void
.end method

.method public static a(LX/0QB;)LX/C6r;
    .locals 4

    .prologue
    .line 1850427
    const-class v1, LX/C6r;

    monitor-enter v1

    .line 1850428
    :try_start_0
    sget-object v0, LX/C6r;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1850429
    sput-object v2, LX/C6r;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1850430
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1850431
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1850432
    new-instance v3, LX/C6r;

    const/16 p0, 0x1f7f

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/C6r;-><init>(LX/0Ot;)V

    .line 1850433
    move-object v0, v3

    .line 1850434
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1850435
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C6r;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1850436
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1850437
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 9

    .prologue
    .line 1850372
    check-cast p2, LX/C6q;

    .line 1850373
    iget-object v0, p0, LX/C6r;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/C6s;

    iget-object v1, p2, LX/C6q;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/C6q;->b:LX/1Pf;

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 1850374
    invoke-static {v1}, LX/1VY;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v3

    iput-object v3, v0, LX/C6s;->d:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 1850375
    invoke-virtual {v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->g()Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v3

    .line 1850376
    iget-object v4, v3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v4

    .line 1850377
    check-cast v3, Lcom/facebook/graphql/model/FeedUnit;

    .line 1850378
    new-instance v4, LX/C6X;

    invoke-direct {v4, v3}, LX/C6X;-><init>(LX/0jW;)V

    invoke-interface {v2, v4, v3}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/C6a;

    .line 1850379
    new-instance v5, LX/C6E;

    invoke-direct {v5, v3}, LX/C6E;-><init>(LX/0jW;)V

    invoke-interface {v2, v5, v3}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/C6H;

    .line 1850380
    iget-object v5, v0, LX/C6s;->d:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->M()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_2

    move v5, v6

    .line 1850381
    :goto_0
    iput-boolean v5, v4, LX/C6a;->a:Z

    .line 1850382
    iget-object v5, v0, LX/C6s;->d:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->M()Ljava/lang/String;

    move-result-object v5

    .line 1850383
    iput-object v5, v4, LX/C6a;->j:Ljava/lang/String;

    .line 1850384
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, v7}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v8

    iget-object v5, v0, LX/C6s;->a:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/C6v;

    invoke-virtual {v5, p1}, LX/C6v;->c(LX/1De;)LX/C6t;

    move-result-object v5

    invoke-virtual {v5, v1}, LX/C6t;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/C6t;

    move-result-object v5

    invoke-virtual {v5, v2}, LX/C6t;->a(LX/1Pq;)LX/C6t;

    move-result-object v5

    iget-object p0, v0, LX/C6s;->d:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->o()Ljava/lang/String;

    move-result-object p0

    .line 1850385
    iget-object p2, v5, LX/C6t;->a:LX/C6u;

    iput-object p0, p2, LX/C6u;->g:Ljava/lang/String;

    .line 1850386
    move-object v5, v5

    .line 1850387
    iget-object p0, v0, LX/C6s;->d:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->al()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v5, p0}, LX/C6t;->b(Ljava/lang/String;)LX/C6t;

    move-result-object v5

    iget-object p0, v0, LX/C6s;->d:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ak()LX/0Px;

    move-result-object p0

    invoke-virtual {v5, p0}, LX/C6t;->a(LX/0Px;)LX/C6t;

    move-result-object v5

    iget-object p0, v0, LX/C6s;->d:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->n()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v5, p0}, LX/C6t;->c(Ljava/lang/String;)LX/C6t;

    move-result-object v5

    invoke-virtual {v5, v6}, LX/C6t;->a(Z)LX/C6t;

    move-result-object v5

    invoke-virtual {v5, v4}, LX/C6t;->a(LX/C6a;)LX/C6t;

    move-result-object v5

    invoke-virtual {v5, v3}, LX/C6t;->a(LX/C6H;)LX/C6t;

    move-result-object v5

    invoke-interface {v8, v5}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v6

    iget-object v5, v0, LX/C6s;->a:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/C6v;

    invoke-virtual {v5, p1}, LX/C6v;->c(LX/1De;)LX/C6t;

    move-result-object v5

    invoke-virtual {v5, v1}, LX/C6t;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/C6t;

    move-result-object v5

    invoke-virtual {v5, v2}, LX/C6t;->a(LX/1Pq;)LX/C6t;

    move-result-object v5

    iget-object v8, v0, LX/C6s;->d:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->M()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, LX/C6t;->b(Ljava/lang/String;)LX/C6t;

    move-result-object v5

    iget-object v8, v0, LX/C6s;->d:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->L()LX/0Px;

    move-result-object v8

    invoke-virtual {v5, v8}, LX/C6t;->a(LX/0Px;)LX/C6t;

    move-result-object v5

    iget-object v8, v0, LX/C6s;->d:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->n()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, LX/C6t;->c(Ljava/lang/String;)LX/C6t;

    move-result-object v5

    invoke-virtual {v5, v7}, LX/C6t;->a(Z)LX/C6t;

    move-result-object v5

    invoke-virtual {v5, v4}, LX/C6t;->a(LX/C6a;)LX/C6t;

    move-result-object v5

    invoke-virtual {v5, v3}, LX/C6t;->a(LX/C6H;)LX/C6t;

    move-result-object v5

    invoke-interface {v6, v5}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v6

    iget-object v5, v0, LX/C6s;->b:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/C6n;

    const/4 v7, 0x0

    .line 1850388
    new-instance v8, LX/C6m;

    invoke-direct {v8, v5}, LX/C6m;-><init>(LX/C6n;)V

    .line 1850389
    iget-object p0, v5, LX/C6n;->b:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/C6l;

    .line 1850390
    if-nez p0, :cond_0

    .line 1850391
    new-instance p0, LX/C6l;

    invoke-direct {p0, v5}, LX/C6l;-><init>(LX/C6n;)V

    .line 1850392
    :cond_0
    invoke-static {p0, p1, v7, v7, v8}, LX/C6l;->a$redex0(LX/C6l;LX/1De;IILX/C6m;)V

    .line 1850393
    move-object v8, p0

    .line 1850394
    move-object v7, v8

    .line 1850395
    move-object v5, v7

    .line 1850396
    iget-object v7, v5, LX/C6l;->a:LX/C6m;

    iput-object v1, v7, LX/C6m;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1850397
    iget-object v7, v5, LX/C6l;->e:Ljava/util/BitSet;

    const/4 v8, 0x2

    invoke-virtual {v7, v8}, Ljava/util/BitSet;->set(I)V

    .line 1850398
    move-object v5, v5

    .line 1850399
    iget-object v7, v5, LX/C6l;->a:LX/C6m;

    iput-object v2, v7, LX/C6m;->d:LX/1Pq;

    .line 1850400
    iget-object v7, v5, LX/C6l;->e:Ljava/util/BitSet;

    const/4 v8, 0x3

    invoke-virtual {v7, v8}, Ljava/util/BitSet;->set(I)V

    .line 1850401
    move-object v5, v5

    .line 1850402
    iget-object v7, v5, LX/C6l;->a:LX/C6m;

    iput-object v4, v7, LX/C6m;->a:LX/C6a;

    .line 1850403
    iget-object v7, v5, LX/C6l;->e:Ljava/util/BitSet;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Ljava/util/BitSet;->set(I)V

    .line 1850404
    move-object v4, v5

    .line 1850405
    iget-object v5, v4, LX/C6l;->a:LX/C6m;

    iput-object v3, v5, LX/C6m;->b:LX/C6H;

    .line 1850406
    iget-object v5, v4, LX/C6l;->e:Ljava/util/BitSet;

    const/4 v7, 0x1

    invoke-virtual {v5, v7}, Ljava/util/BitSet;->set(I)V

    .line 1850407
    move-object v4, v4

    .line 1850408
    invoke-interface {v6, v4}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v5

    iget-object v4, v0, LX/C6s;->c:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/C6j;

    const/4 v6, 0x0

    .line 1850409
    new-instance v7, LX/C6i;

    invoke-direct {v7, v4}, LX/C6i;-><init>(LX/C6j;)V

    .line 1850410
    sget-object v8, LX/C6j;->a:LX/0Zi;

    invoke-virtual {v8}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/C6h;

    .line 1850411
    if-nez v8, :cond_1

    .line 1850412
    new-instance v8, LX/C6h;

    invoke-direct {v8}, LX/C6h;-><init>()V

    .line 1850413
    :cond_1
    invoke-static {v8, p1, v6, v6, v7}, LX/C6h;->a$redex0(LX/C6h;LX/1De;IILX/C6i;)V

    .line 1850414
    move-object v7, v8

    .line 1850415
    move-object v6, v7

    .line 1850416
    move-object v4, v6

    .line 1850417
    iget-object v6, v4, LX/C6h;->a:LX/C6i;

    iput-object v3, v6, LX/C6i;->a:LX/C6H;

    .line 1850418
    iget-object v6, v4, LX/C6h;->d:Ljava/util/BitSet;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Ljava/util/BitSet;->set(I)V

    .line 1850419
    move-object v3, v4

    .line 1850420
    invoke-interface {v5, v3}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 1850421
    return-object v0

    :cond_2
    move v5, v7

    .line 1850422
    goto/16 :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1850370
    invoke-static {}, LX/1dS;->b()V

    .line 1850371
    const/4 v0, 0x0

    return-object v0
.end method
