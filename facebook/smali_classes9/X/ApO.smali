.class public final LX/ApO;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:I

.field public e:I

.field public f:I

.field public g:I
    .annotation build Landroid/support/annotation/DrawableRes;
    .end annotation
.end field

.field public h:Landroid/content/res/ColorStateList;


# direct methods
.method public constructor <init>(LX/1De;I)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 1715934
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1715935
    invoke-static {p2}, Lcom/facebook/fig/button/FigButton;->b(I)[I

    move-result-object v3

    .line 1715936
    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_9

    aget v0, v3, v2

    .line 1715937
    sget-object v5, LX/03r;->FigButtonAttrs:[I

    invoke-virtual {p1, v0, v5}, LX/1De;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    move-result-object v5

    .line 1715938
    invoke-virtual {v5}, Landroid/content/res/TypedArray;->getIndexCount()I

    move-result v6

    move v0, v1

    .line 1715939
    :goto_1
    if-ge v0, v6, :cond_8

    .line 1715940
    invoke-virtual {v5, v0}, Landroid/content/res/TypedArray;->getIndex(I)I

    move-result v7

    .line 1715941
    const/16 v8, 0x2

    if-ne v7, v8, :cond_1

    .line 1715942
    invoke-virtual {v5, v7, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v7

    iput v7, p0, LX/ApO;->a:I

    .line 1715943
    :cond_0
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1715944
    :cond_1
    const/16 v8, 0x3

    if-ne v7, v8, :cond_2

    .line 1715945
    invoke-virtual {v5, v7, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v7

    iput v7, p0, LX/ApO;->b:I

    goto :goto_2

    .line 1715946
    :cond_2
    const/16 v8, 0x5

    if-ne v7, v8, :cond_3

    .line 1715947
    invoke-virtual {v5, v7, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v7

    iput v7, p0, LX/ApO;->c:I

    goto :goto_2

    .line 1715948
    :cond_3
    const/16 v8, 0x8

    if-ne v7, v8, :cond_4

    .line 1715949
    invoke-virtual {v5, v7, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v7

    iput v7, p0, LX/ApO;->d:I

    goto :goto_2

    .line 1715950
    :cond_4
    const/16 v8, 0x0

    if-ne v7, v8, :cond_5

    .line 1715951
    invoke-virtual {v5, v7, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v7

    iput v7, p0, LX/ApO;->f:I

    goto :goto_2

    .line 1715952
    :cond_5
    const/16 v8, 0x1

    if-ne v7, v8, :cond_6

    .line 1715953
    invoke-virtual {v5, v7, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v7

    iput v7, p0, LX/ApO;->g:I

    goto :goto_2

    .line 1715954
    :cond_6
    const/16 v8, 0x4

    if-ne v7, v8, :cond_7

    .line 1715955
    invoke-virtual {v5, v7}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v7

    iput-object v7, p0, LX/ApO;->h:Landroid/content/res/ColorStateList;

    goto :goto_2

    .line 1715956
    :cond_7
    const/16 v8, 0x6

    if-ne v7, v8, :cond_0

    .line 1715957
    invoke-virtual {v5, v7, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v7

    iput v7, p0, LX/ApO;->e:I

    goto :goto_2

    .line 1715958
    :cond_8
    invoke-virtual {v5}, Landroid/content/res/TypedArray;->recycle()V

    .line 1715959
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1715960
    :cond_9
    return-void
.end method
