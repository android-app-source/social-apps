.class public LX/BrI;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:LX/Ap5;


# direct methods
.method public constructor <init>(LX/Ap5;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1826256
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1826257
    iput-object p1, p0, LX/BrI;->a:LX/Ap5;

    .line 1826258
    return-void
.end method

.method public static a(LX/0QB;)LX/BrI;
    .locals 4

    .prologue
    .line 1826259
    const-class v1, LX/BrI;

    monitor-enter v1

    .line 1826260
    :try_start_0
    sget-object v0, LX/BrI;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1826261
    sput-object v2, LX/BrI;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1826262
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1826263
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1826264
    new-instance p0, LX/BrI;

    invoke-static {v0}, LX/Ap5;->a(LX/0QB;)LX/Ap5;

    move-result-object v3

    check-cast v3, LX/Ap5;

    invoke-direct {p0, v3}, LX/BrI;-><init>(LX/Ap5;)V

    .line 1826265
    move-object v0, p0

    .line 1826266
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1826267
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/BrI;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1826268
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1826269
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
