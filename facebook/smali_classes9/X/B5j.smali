.class public LX/B5j;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0ik;
.implements LX/0il;
.implements LX/0im;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "Lcom/facebook/ipc/composer/plugin/ComposerPluginDataProvider;",
        "DerivedData::",
        "Lcom/facebook/ipc/composer/plugin/ComposerPluginDerivedDataProvider;",
        "Mutation::",
        "Lcom/facebook/ipc/composer/plugin/ComposerPluginMutation",
        "<TMutation;>;>",
        "Ljava/lang/Object;",
        "LX/0ik",
        "<TDerivedData;>;",
        "LX/0il",
        "<TModelData;>;",
        "LX/0im",
        "<TMutation;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0il;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0il",
            "<TModelData;>;"
        }
    .end annotation
.end field

.field private final b:LX/0ik;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0ik",
            "<TDerivedData;>;"
        }
    .end annotation
.end field

.field private final c:LX/0im;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0im",
            "<TMutation;>;"
        }
    .end annotation
.end field

.field private final d:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/Ar6;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0il;LX/0ik;LX/0im;LX/Ar6;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0il",
            "<TModelData;>;",
            "LX/0ik",
            "<TDerivedData;>;",
            "LX/0im",
            "<TMutation;>;",
            "LX/Ar6;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1745862
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1745863
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    iput-object v0, p0, LX/B5j;->a:LX/0il;

    .line 1745864
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ik;

    iput-object v0, p0, LX/B5j;->b:LX/0ik;

    .line 1745865
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0im;

    iput-object v0, p0, LX/B5j;->c:LX/0im;

    .line 1745866
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-static {p4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/B5j;->d:Ljava/lang/ref/WeakReference;

    .line 1745867
    return-void
.end method


# virtual methods
.method public final synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1745861
    invoke-virtual {p0}, LX/B5j;->e()LX/2zG;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1745860
    invoke-virtual {p0}, LX/B5j;->d()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v0

    return-object v0
.end method

.method public final c()LX/0jJ;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/facebook/ipc/composer/dataaccessor/ComposerMutator",
            "<TMutation;>;"
        }
    .end annotation

    .prologue
    .line 1745856
    iget-object v0, p0, LX/B5j;->c:LX/0im;

    invoke-interface {v0}, LX/0im;->c()LX/0jJ;

    move-result-object v0

    return-object v0
.end method

.method public final d()Lcom/facebook/composer/system/model/ComposerModelImpl;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TModelData;"
        }
    .end annotation

    .prologue
    .line 1745859
    iget-object v0, p0, LX/B5j;->a:LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    return-object v0
.end method

.method public final e()LX/2zG;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TDerivedData;"
        }
    .end annotation

    .prologue
    .line 1745858
    iget-object v0, p0, LX/B5j;->b:LX/0ik;

    invoke-interface {v0}, LX/0ik;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2zG;

    return-object v0
.end method

.method public final f()LX/Ar6;
    .locals 1

    .prologue
    .line 1745857
    iget-object v0, p0, LX/B5j;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ar6;

    return-object v0
.end method
