.class public final LX/CDH;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/2nK;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/3EE;

.field public b:LX/1Pe;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public c:Ljava/lang/Boolean;

.field public d:Ljava/lang/Boolean;

.field public e:LX/1dV;

.field public f:LX/1no;

.field public final synthetic g:LX/2nK;


# direct methods
.method public constructor <init>(LX/2nK;)V
    .locals 1

    .prologue
    .line 1858942
    iput-object p1, p0, LX/CDH;->g:LX/2nK;

    .line 1858943
    move-object v0, p1

    .line 1858944
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1858945
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1858946
    const-string v0, "FloatingRichVideoAttachmentComponent"

    return-object v0
.end method

.method public final a(LX/1X1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1",
            "<",
            "LX/2nK;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1858947
    check-cast p1, LX/CDH;

    .line 1858948
    iget-object v0, p1, LX/CDH;->e:LX/1dV;

    iput-object v0, p0, LX/CDH;->e:LX/1dV;

    .line 1858949
    iget-object v0, p1, LX/CDH;->f:LX/1no;

    iput-object v0, p0, LX/CDH;->f:LX/1no;

    .line 1858950
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1858951
    if-ne p0, p1, :cond_1

    .line 1858952
    :cond_0
    :goto_0
    return v0

    .line 1858953
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1858954
    goto :goto_0

    .line 1858955
    :cond_3
    check-cast p1, LX/CDH;

    .line 1858956
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1858957
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1858958
    if-eq v2, v3, :cond_0

    .line 1858959
    iget-object v2, p0, LX/CDH;->a:LX/3EE;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/CDH;->a:LX/3EE;

    iget-object v3, p1, LX/CDH;->a:LX/3EE;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1858960
    goto :goto_0

    .line 1858961
    :cond_5
    iget-object v2, p1, LX/CDH;->a:LX/3EE;

    if-nez v2, :cond_4

    .line 1858962
    :cond_6
    iget-object v2, p0, LX/CDH;->b:LX/1Pe;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/CDH;->b:LX/1Pe;

    iget-object v3, p1, LX/CDH;->b:LX/1Pe;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1858963
    goto :goto_0

    .line 1858964
    :cond_8
    iget-object v2, p1, LX/CDH;->b:LX/1Pe;

    if-nez v2, :cond_7

    .line 1858965
    :cond_9
    iget-object v2, p0, LX/CDH;->c:Ljava/lang/Boolean;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/CDH;->c:Ljava/lang/Boolean;

    iget-object v3, p1, LX/CDH;->c:Ljava/lang/Boolean;

    invoke-virtual {v2, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 1858966
    goto :goto_0

    .line 1858967
    :cond_b
    iget-object v2, p1, LX/CDH;->c:Ljava/lang/Boolean;

    if-nez v2, :cond_a

    .line 1858968
    :cond_c
    iget-object v2, p0, LX/CDH;->d:Ljava/lang/Boolean;

    if-eqz v2, :cond_d

    iget-object v2, p0, LX/CDH;->d:Ljava/lang/Boolean;

    iget-object v3, p1, LX/CDH;->d:Ljava/lang/Boolean;

    invoke-virtual {v2, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1858969
    goto :goto_0

    .line 1858970
    :cond_d
    iget-object v2, p1, LX/CDH;->d:Ljava/lang/Boolean;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public final g()LX/1X1;
    .locals 2

    .prologue
    .line 1858971
    const/4 v1, 0x0

    .line 1858972
    invoke-super {p0}, LX/1X1;->g()LX/1X1;

    move-result-object v0

    check-cast v0, LX/CDH;

    .line 1858973
    iput-object v1, v0, LX/CDH;->e:LX/1dV;

    .line 1858974
    iput-object v1, v0, LX/CDH;->f:LX/1no;

    .line 1858975
    return-object v0
.end method
