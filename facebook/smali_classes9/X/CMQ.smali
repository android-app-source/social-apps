.class public final LX/CMQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/CMP;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/emoji/MessengerEmojiColorPickerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/emoji/MessengerEmojiColorPickerFragment;)V
    .locals 0

    .prologue
    .line 1880581
    iput-object p1, p0, LX/CMQ;->a:Lcom/facebook/messaging/emoji/MessengerEmojiColorPickerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/ui/emoji/model/Emoji;)V
    .locals 2
    .param p1    # Lcom/facebook/ui/emoji/model/Emoji;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1880582
    if-nez p1, :cond_0

    .line 1880583
    :goto_0
    return-void

    .line 1880584
    :cond_0
    iget-object v0, p0, LX/CMQ;->a:Lcom/facebook/messaging/emoji/MessengerEmojiColorPickerFragment;

    iget-object v0, v0, Lcom/facebook/messaging/emoji/MessengerEmojiColorPickerFragment;->m:LX/CML;

    .line 1880585
    iget v1, p1, Lcom/facebook/ui/emoji/model/Emoji;->c:I

    move v1, v1

    .line 1880586
    invoke-virtual {v0, v1}, LX/CML;->a(I)V

    .line 1880587
    iget-object v0, p0, LX/CMQ;->a:Lcom/facebook/messaging/emoji/MessengerEmojiColorPickerFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->c()V

    goto :goto_0
.end method
