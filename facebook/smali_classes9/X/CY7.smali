.class public LX/CY7;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:J

.field public final b:Ljava/lang/String;

.field public final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

.field public final e:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

.field public final f:LX/CYE;

.field public final g:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final h:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;


# direct methods
.method private constructor <init>(JLjava/lang/String;LX/0Px;Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;LX/CYE;LX/0P1;Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonInterfaces$PageCallToActionButtonData;",
            "Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;",
            "LX/CYE;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1910796
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1910797
    iput-wide p1, p0, LX/CY7;->a:J

    .line 1910798
    iput-object p3, p0, LX/CY7;->b:Ljava/lang/String;

    .line 1910799
    iput-object p4, p0, LX/CY7;->c:LX/0Px;

    .line 1910800
    iput-object p5, p0, LX/CY7;->d:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    .line 1910801
    iput-object p6, p0, LX/CY7;->e:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    .line 1910802
    iput-object p7, p0, LX/CY7;->f:LX/CYE;

    .line 1910803
    iput-object p8, p0, LX/CY7;->g:LX/0P1;

    .line 1910804
    iput-object p9, p0, LX/CY7;->h:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    .line 1910805
    return-void
.end method

.method public synthetic constructor <init>(JLjava/lang/String;LX/0Px;Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;LX/CYE;LX/0P1;Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;B)V
    .locals 1

    .prologue
    .line 1910806
    invoke-direct/range {p0 .. p9}, LX/CY7;-><init>(JLjava/lang/String;LX/0Px;Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;LX/CYE;LX/0P1;Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;)V

    return-void
.end method
