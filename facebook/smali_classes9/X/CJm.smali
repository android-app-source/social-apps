.class public final LX/CJm;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static h:LX/0Xm;


# instance fields
.field public a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/CJl;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/CJi;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CMq;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CJb;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/6eF;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1875780
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1875781
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1875782
    iput-object v0, p0, LX/CJm;->c:LX/0Ot;

    .line 1875783
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1875784
    iput-object v0, p0, LX/CJm;->d:LX/0Ot;

    .line 1875785
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1875786
    iput-object v0, p0, LX/CJm;->e:LX/0Ot;

    .line 1875787
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1875788
    iput-object v0, p0, LX/CJm;->f:LX/0Ot;

    .line 1875789
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1875790
    iput-object v0, p0, LX/CJm;->g:LX/0Ot;

    .line 1875791
    return-void
.end method

.method public static a(LX/0QB;)LX/CJm;
    .locals 10

    .prologue
    .line 1875792
    const-class v1, LX/CJm;

    monitor-enter v1

    .line 1875793
    :try_start_0
    sget-object v0, LX/CJm;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1875794
    sput-object v2, LX/CJm;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1875795
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1875796
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1875797
    new-instance v3, LX/CJm;

    invoke-direct {v3}, LX/CJm;-><init>()V

    .line 1875798
    new-instance v4, LX/0U8;

    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v5

    new-instance v6, LX/CJq;

    invoke-direct {v6, v0}, LX/CJq;-><init>(LX/0QB;)V

    invoke-direct {v4, v5, v6}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v4, v4

    .line 1875799
    invoke-static {v0}, LX/CJi;->b(LX/0QB;)LX/CJi;

    move-result-object v5

    check-cast v5, LX/CJi;

    const-class v6, Landroid/content/Context;

    invoke-interface {v0, v6}, LX/0QC;->getLazy(Ljava/lang/Class;)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x27c5

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x267d

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x27b6

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 p0, 0x455

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 1875800
    iput-object v4, v3, LX/CJm;->a:Ljava/util/Set;

    iput-object v5, v3, LX/CJm;->b:LX/CJi;

    iput-object v6, v3, LX/CJm;->c:LX/0Ot;

    iput-object v7, v3, LX/CJm;->d:LX/0Ot;

    iput-object v8, v3, LX/CJm;->e:LX/0Ot;

    iput-object v9, v3, LX/CJm;->f:LX/0Ot;

    iput-object p0, v3, LX/CJm;->g:LX/0Ot;

    .line 1875801
    move-object v0, v3

    .line 1875802
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1875803
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/CJm;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1875804
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1875805
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
