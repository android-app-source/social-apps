.class public final LX/BFT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/model/GraphQLFeedback;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic b:LX/BFU;


# direct methods
.method public constructor <init>(LX/BFU;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 0

    .prologue
    .line 1765887
    iput-object p1, p0, LX/BFT;->b:LX/BFU;

    iput-object p2, p0, LX/BFT;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1765888
    check-cast p1, Lcom/facebook/graphql/model/GraphQLFeedback;

    const/4 v2, 0x0

    .line 1765889
    if-nez p1, :cond_1

    .line 1765890
    :cond_0
    :goto_0
    return-object v2

    .line 1765891
    :cond_1
    iget-object v0, p0, LX/BFT;->b:LX/BFU;

    iget-object v0, v0, LX/BFU;->a:LX/189;

    iget-object v1, p0, LX/BFT;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1765892
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 1765893
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v3, p1}, LX/189;->a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v3

    .line 1765894
    invoke-virtual {v0, v3, v1}, LX/189;->a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v3

    move-object v0, v3

    .line 1765895
    invoke-static {v0}, LX/182;->f(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    .line 1765896
    instance-of v1, v0, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v1, :cond_0

    .line 1765897
    iget-object v1, p0, LX/BFT;->b:LX/BFU;

    iget-object v1, v1, LX/BFU;->b:LX/0QK;

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-interface {v1, v0}, LX/0QK;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
