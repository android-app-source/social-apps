.class public LX/AWJ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/util/concurrent/ScheduledExecutorService;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/AVe;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1682618
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static b(LX/0QB;)LX/AWJ;
    .locals 3

    .prologue
    .line 1682619
    new-instance v2, LX/AWJ;

    invoke-direct {v2}, LX/AWJ;-><init>()V

    .line 1682620
    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v1

    check-cast v1, LX/0tX;

    .line 1682621
    iput-object v0, v2, LX/AWJ;->a:Ljava/util/concurrent/ScheduledExecutorService;

    iput-object v1, v2, LX/AWJ;->b:LX/0tX;

    .line 1682622
    return-object v2
.end method


# virtual methods
.method public final a(LX/AVe;Z)V
    .locals 3

    .prologue
    .line 1682623
    iput-object p1, p0, LX/AWJ;->c:LX/AVe;

    .line 1682624
    new-instance v0, LX/AVx;

    invoke-direct {v0}, LX/AVx;-><init>()V

    move-object v0, v0

    .line 1682625
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 1682626
    if-eqz p2, :cond_0

    .line 1682627
    sget-object v1, LX/0zS;->a:LX/0zS;

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    .line 1682628
    :cond_0
    iget-object v1, p0, LX/AWJ;->b:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 1682629
    new-instance v1, LX/AWI;

    invoke-direct {v1, p0}, LX/AWI;-><init>(LX/AWJ;)V

    iget-object v2, p0, LX/AWJ;->a:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1682630
    return-void
.end method
