.class public final LX/Akv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1MS;


# instance fields
.field public final synthetic a:LX/1SX;

.field private final b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nonnull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1SX;Ljava/lang/String;)V
    .locals 0
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 1709372
    iput-object p1, p0, LX/Akv;->a:LX/1SX;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1709373
    iput-object p2, p0, LX/Akv;->b:Ljava/lang/String;

    .line 1709374
    return-void
.end method


# virtual methods
.method public final getExtraFileFromWorkerThread(Ljava/io/File;)Ljava/util/Map;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1709375
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    .line 1709376
    new-instance v1, Ljava/io/File;

    const-string v2, "serialized_story_data.txt"

    invoke-direct {v1, p1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 1709377
    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    .line 1709378
    iget-object v3, p0, LX/Akv;->a:LX/1SX;

    iget-object v3, v3, LX/1SX;->e:LX/0lC;

    iget-object v4, p0, LX/Akv;->b:Ljava/lang/String;

    invoke-virtual {v3, v1, v4}, LX/0lC;->a(Ljava/io/File;Ljava/lang/Object;)V

    .line 1709379
    const-string v1, "serialized_story_data.txt"

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 1709380
    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    return-object v0
.end method

.method public final prepareDataForWriting()V
    .locals 0

    .prologue
    .line 1709371
    return-void
.end method

.method public final shouldSendAsync()Z
    .locals 4

    .prologue
    .line 1709370
    iget-object v0, p0, LX/Akv;->a:LX/1SX;

    iget-object v0, v0, LX/1SX;->y:LX/0W3;

    sget-wide v2, LX/0X5;->bd:J

    const/4 v1, 0x0

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JZ)Z

    move-result v0

    return v0
.end method
