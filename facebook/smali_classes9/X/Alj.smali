.class public LX/Alj;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/1lR;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1710199
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1710200
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    .line 1710201
    iget-object v0, p0, LX/Alj;->a:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1710202
    iget-object v0, p0, LX/Alj;->a:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;

    const/4 v1, 0x1

    .line 1710203
    invoke-virtual {v0}, LX/Ale;->i()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1710204
    :cond_0
    :goto_0
    return-void

    .line 1710205
    :cond_1
    iget-object v4, v0, LX/Ale;->e:Landroid/animation/ValueAnimator;

    if-eqz v1, :cond_2

    const-wide/16 v2, 0x0

    :goto_1
    invoke-virtual {v4, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 1710206
    iget-object v2, v0, LX/Ale;->e:Landroid/animation/ValueAnimator;

    invoke-virtual {v2}, Landroid/animation/ValueAnimator;->start()V

    goto :goto_0

    .line 1710207
    :cond_2
    const-wide/16 v2, 0x1f4

    goto :goto_1
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1710208
    iget-object v1, p0, LX/Alj;->b:LX/1lR;

    .line 1710209
    iget-boolean v2, v1, LX/1lR;->i:Z

    move v1, v2

    .line 1710210
    if-eqz v1, :cond_0

    .line 1710211
    invoke-virtual {p0}, LX/Alj;->a()V

    .line 1710212
    iget-object v1, p0, LX/Alj;->b:LX/1lR;

    .line 1710213
    iput-boolean v0, v1, LX/1lR;->i:Z

    .line 1710214
    const/4 v0, 0x1

    .line 1710215
    :cond_0
    return v0
.end method
