.class public final LX/Aqo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "LX/0Px",
        "<",
        "Lcom/facebook/friendsharing/gif/model/GifModelContainer;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1718257
    iput-object p1, p0, LX/Aqo;->b:Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;

    iput-object p2, p0, LX/Aqo;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1718258
    :try_start_0
    iget-object v0, p0, LX/Aqo;->b:Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;

    iget-object v0, v0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->f:LX/Aqc;

    iget-object v1, p0, LX/Aqo;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/Aqc;->a(Ljava/lang/String;)LX/0Px;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1718259
    :goto_0
    return-object v0

    .line 1718260
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 1718261
    iget-object v0, p0, LX/Aqo;->b:Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;

    iget-object v0, v0, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v2, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->h:Ljava/lang/String;

    const-string v3, "Error while fetching Gifs"

    invoke-virtual {v0, v2, v3, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1718262
    iget-object v0, p0, LX/Aqo;->b:Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;

    sget-object v1, LX/Aqq;->ERROR:LX/Aqq;

    invoke-static {v0, v1}, Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;->a$redex0(Lcom/facebook/friendsharing/gif/activity/GifPickerFragment;LX/Aqq;)V

    .line 1718263
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1718264
    goto :goto_0
.end method
