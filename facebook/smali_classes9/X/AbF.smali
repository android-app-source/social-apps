.class public final LX/AbF;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    const-wide/16 v4, 0x0

    const/4 v1, 0x1

    const/4 v6, 0x0

    .line 1690481
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_7

    .line 1690482
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1690483
    :goto_0
    return v6

    .line 1690484
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_4

    .line 1690485
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 1690486
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1690487
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_0

    if-eqz v10, :cond_0

    .line 1690488
    const-string v11, "tip_giver_number"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 1690489
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v7

    move v9, v7

    move v7, v1

    goto :goto_1

    .line 1690490
    :cond_1
    const-string v11, "total_tip_amount"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 1690491
    invoke-virtual {p0}, LX/15w;->G()D

    move-result-wide v2

    move v0, v1

    goto :goto_1

    .line 1690492
    :cond_2
    const-string v11, "total_tipped_amount"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 1690493
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_1

    .line 1690494
    :cond_3
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1690495
    :cond_4
    const/4 v10, 0x3

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 1690496
    if-eqz v7, :cond_5

    .line 1690497
    invoke-virtual {p1, v6, v9, v6}, LX/186;->a(III)V

    .line 1690498
    :cond_5
    if-eqz v0, :cond_6

    move-object v0, p1

    .line 1690499
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1690500
    :cond_6
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 1690501
    invoke-virtual {p1}, LX/186;->d()I

    move-result v6

    goto :goto_0

    :cond_7
    move v0, v6

    move v7, v6

    move v8, v6

    move-wide v2, v4

    move v9, v6

    goto :goto_1
.end method
