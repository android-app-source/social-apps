.class public final LX/BGQ;
.super LX/1a1;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic l:Lcom/facebook/photos/creativeediting/swipeable/prompt/FramePromptScrollAdapter;

.field public final m:Lcom/facebook/photos/creativeediting/swipeable/common/FrameImageView;

.field public n:LX/5iG;

.field public o:Lcom/facebook/photos/creativeediting/model/SwipeableParams;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/creativeediting/swipeable/prompt/FramePromptScrollAdapter;Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1767027
    iput-object p1, p0, LX/BGQ;->l:Lcom/facebook/photos/creativeediting/swipeable/prompt/FramePromptScrollAdapter;

    .line 1767028
    invoke-direct {p0, p2}, LX/1a1;-><init>(Landroid/view/View;)V

    .line 1767029
    invoke-virtual {p2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1767030
    const v0, 0x7f0d0883

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/swipeable/common/FrameImageView;

    iput-object v0, p0, LX/BGQ;->m:Lcom/facebook/photos/creativeediting/swipeable/common/FrameImageView;

    .line 1767031
    iget-object v0, p0, LX/BGQ;->m:Lcom/facebook/photos/creativeediting/swipeable/common/FrameImageView;

    new-instance v1, Landroid/graphics/RectF;

    iget v2, p1, Lcom/facebook/photos/creativeediting/swipeable/prompt/FramePromptScrollAdapter;->d:I

    int-to-float v2, v2

    iget v3, p1, Lcom/facebook/photos/creativeediting/swipeable/prompt/FramePromptScrollAdapter;->d:I

    int-to-float v3, v3

    invoke-direct {v1, v4, v4, v2, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-virtual {v0, v1}, Lcom/facebook/photos/creativeediting/swipeable/common/FrameImageView;->setActualImageBounds(Landroid/graphics/RectF;)V

    .line 1767032
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x37f61294

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1767033
    iget-object v1, p0, LX/BGQ;->l:Lcom/facebook/photos/creativeediting/swipeable/prompt/FramePromptScrollAdapter;

    iget-object v1, v1, Lcom/facebook/photos/creativeediting/swipeable/prompt/FramePromptScrollAdapter;->j:LX/B5l;

    iget-object v2, p0, LX/BGQ;->l:Lcom/facebook/photos/creativeediting/swipeable/prompt/FramePromptScrollAdapter;

    iget-object v2, v2, Lcom/facebook/photos/creativeediting/swipeable/prompt/FramePromptScrollAdapter;->g:LX/1RN;

    invoke-virtual {v1, v2}, LX/B5l;->a(LX/1RN;)LX/B5n;

    move-result-object v1

    iget-object v2, p0, LX/BGQ;->o:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    .line 1767034
    iget-object v3, v2, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->b:Ljava/lang/String;

    move-object v2, v3

    .line 1767035
    iput-object v2, v1, LX/B5n;->g:Ljava/lang/String;

    .line 1767036
    move-object v1, v1

    .line 1767037
    const/4 v2, 0x1

    move v2, v2

    .line 1767038
    if-eqz v2, :cond_0

    .line 1767039
    iget-object v2, p0, LX/BGQ;->l:Lcom/facebook/photos/creativeediting/swipeable/prompt/FramePromptScrollAdapter;

    iget-object v2, v2, Lcom/facebook/photos/creativeediting/swipeable/prompt/FramePromptScrollAdapter;->f:LX/BMP;

    iget-object v3, p0, LX/BGQ;->l:Lcom/facebook/photos/creativeediting/swipeable/prompt/FramePromptScrollAdapter;

    iget-object v3, v3, Lcom/facebook/photos/creativeediting/swipeable/prompt/FramePromptScrollAdapter;->g:LX/1RN;

    invoke-virtual {v1}, LX/B5n;->a()LX/B5p;

    move-result-object v1

    invoke-virtual {v2, p1, v3, v1}, LX/B5s;->a(Landroid/view/View;LX/1RN;LX/B5o;)V

    .line 1767040
    :goto_0
    const v1, -0x79912ca9

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 1767041
    :cond_0
    iget-object v2, p0, LX/BGQ;->l:Lcom/facebook/photos/creativeediting/swipeable/prompt/FramePromptScrollAdapter;

    iget-object v2, v2, Lcom/facebook/photos/creativeediting/swipeable/prompt/FramePromptScrollAdapter;->e:LX/BMK;

    iget-object v3, p0, LX/BGQ;->l:Lcom/facebook/photos/creativeediting/swipeable/prompt/FramePromptScrollAdapter;

    iget-object v3, v3, Lcom/facebook/photos/creativeediting/swipeable/prompt/FramePromptScrollAdapter;->g:LX/1RN;

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, LX/B5n;->b(Ljava/lang/String;)LX/B5n;

    move-result-object v1

    invoke-virtual {v1}, LX/B5n;->a()LX/B5p;

    move-result-object v1

    invoke-virtual {v2, p1, v3, v1}, LX/BMK;->a(Landroid/view/View;LX/1RN;LX/B5o;)V

    goto :goto_0
.end method
