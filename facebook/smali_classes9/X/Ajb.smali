.class public LX/Ajb;
.super LX/93Q;
.source ""


# instance fields
.field private final a:J

.field private final b:LX/87v;

.field public final c:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(LX/93q;LX/0Or;LX/03V;LX/1Ck;LX/87v;Landroid/content/res/Resources;)V
    .locals 2
    .param p1    # LX/93q;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/93q;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/1Ck;",
            "LX/87v;",
            "Landroid/content/res/Resources;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1708185
    invoke-direct {p0, p1, p3, p4}, LX/93Q;-><init>(LX/93q;LX/03V;LX/1Ck;)V

    .line 1708186
    invoke-interface {p2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, LX/Ajb;->a:J

    .line 1708187
    iput-object p5, p0, LX/Ajb;->b:LX/87v;

    .line 1708188
    iput-object p6, p0, LX/Ajb;->c:Landroid/content/res/Resources;

    .line 1708189
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 1708163
    invoke-super {p0}, LX/93Q;->a()V

    .line 1708164
    const/4 v3, 0x1

    .line 1708165
    new-instance v0, LX/7lN;

    invoke-direct {v0}, LX/7lN;-><init>()V

    iget-object v1, p0, LX/Ajb;->c:Landroid/content/res/Resources;

    const v2, 0x7f0823b3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1708166
    iput-object v1, v0, LX/7lN;->b:Ljava/lang/String;

    .line 1708167
    move-object v0, v0

    .line 1708168
    iget-object v1, p0, LX/Ajb;->c:Landroid/content/res/Resources;

    const v2, 0x7f0823b4

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1708169
    iput-object v1, v0, LX/7lN;->c:Ljava/lang/String;

    .line 1708170
    move-object v0, v0

    .line 1708171
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->GOOD_FRIENDS:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    .line 1708172
    iput-object v1, v0, LX/7lN;->a:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    .line 1708173
    move-object v0, v0

    .line 1708174
    invoke-virtual {v0}, LX/7lN;->a()Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;

    move-result-object v0

    .line 1708175
    new-instance v1, LX/7lP;

    invoke-direct {v1}, LX/7lP;-><init>()V

    .line 1708176
    iput-boolean v3, v1, LX/7lP;->a:Z

    .line 1708177
    move-object v1, v1

    .line 1708178
    iput-boolean v3, v1, LX/7lP;->b:Z

    .line 1708179
    move-object v1, v1

    .line 1708180
    invoke-virtual {v1, v0}, LX/7lP;->a(Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;)LX/7lP;

    move-result-object v0

    invoke-virtual {v0}, LX/7lP;->a()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    move-result-object v0

    move-object v0, v0

    .line 1708181
    invoke-virtual {p0, v0}, LX/93Q;->a(Lcom/facebook/composer/privacy/model/ComposerPrivacyData;)V

    .line 1708182
    iget-object v1, p0, LX/Ajb;->b:LX/87v;

    const/4 v2, 0x1

    new-instance v3, LX/Aja;

    invoke-direct {v3, p0, v0}, LX/Aja;-><init>(LX/Ajb;Lcom/facebook/composer/privacy/model/ComposerPrivacyData;)V

    invoke-virtual {v1, v2, v3}, LX/87v;->a(ZLX/87u;)V

    .line 1708183
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1708184
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "goodfriends:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, LX/Ajb;->a:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
