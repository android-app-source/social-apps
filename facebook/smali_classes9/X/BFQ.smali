.class public LX/BFQ;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/0Yj;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/0Yj;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final c:Lcom/facebook/performancelogger/PerformanceLogger;

.field private final d:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field public final e:LX/0id;


# direct methods
.method public static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1765830
    const/4 v0, 0x6

    new-array v0, v0, [LX/0Yj;

    new-instance v1, LX/0Yj;

    const v2, 0xa0008

    const-string v3, "NNF_PermalinkFromFeedLoad"

    invoke-direct {v1, v2, v3}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v1, v0, v5

    new-instance v1, LX/0Yj;

    const v2, 0xa0030

    const-string v3, "NNF_PermalinkNotificationLoad"

    invoke-direct {v1, v2, v3}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v1, v0, v6

    new-instance v1, LX/0Yj;

    const v2, 0xa000a

    const-string v3, "PermalinkFromOnCreateToLoad"

    invoke-direct {v1, v2, v3}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v1, v0, v7

    new-instance v1, LX/0Yj;

    const v2, 0xa002f

    const-string v3, "PermalinkFromOnCreateToLoadIfNoNavigationalMetrics"

    invoke-direct {v1, v2, v3}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v1, v0, v8

    new-instance v1, LX/0Yj;

    const v2, 0xa0034

    const-string v3, "NNF_PermalinkFromAndroidNotificationColdLoad"

    invoke-direct {v1, v2, v3}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v1, v0, v9

    const/4 v1, 0x5

    new-instance v2, LX/0Yj;

    const v3, 0x350008

    const-string v4, "NotifPermalinkRefreshStoryTime"

    invoke-direct {v2, v3, v4}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, LX/BFQ;->a:Ljava/util/List;

    .line 1765831
    const/16 v0, 0x8

    new-array v0, v0, [LX/0Yj;

    new-instance v1, LX/0Yj;

    const v2, 0xa0008

    const-string v3, "NNF_PermalinkFromFeedLoad"

    invoke-direct {v1, v2, v3}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v1, v0, v5

    new-instance v1, LX/0Yj;

    const v2, 0xa0030

    const-string v3, "NNF_PermalinkNotificationLoad"

    invoke-direct {v1, v2, v3}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v1, v0, v6

    new-instance v1, LX/0Yj;

    const v2, 0xa000a

    const-string v3, "PermalinkFromOnCreateToLoad"

    invoke-direct {v1, v2, v3}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v1, v0, v7

    new-instance v1, LX/0Yj;

    const v2, 0xa002f

    const-string v3, "PermalinkFromOnCreateToLoadIfNoNavigationalMetrics"

    invoke-direct {v1, v2, v3}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v1, v0, v8

    new-instance v1, LX/0Yj;

    const v2, 0xa0034

    const-string v3, "NNF_PermalinkFromAndroidNotificationColdLoad"

    invoke-direct {v1, v2, v3}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v1, v0, v9

    const/4 v1, 0x5

    new-instance v2, LX/0Yj;

    const v3, 0xa0031

    const-string v4, "NNF_PermalinkFromAndroidNotificationWarmLoad"

    invoke-direct {v2, v3, v4}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-instance v2, LX/0Yj;

    const v3, 0x350007

    const-string v4, "NotifLockscreenPermalinkLoadTime"

    invoke-direct {v2, v3, v4}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v2, v0, v1

    const/4 v1, 0x7

    new-instance v2, LX/0Yj;

    const v3, 0x350008

    const-string v4, "NotifPermalinkRefreshStoryTime"

    invoke-direct {v2, v3, v4}, LX/0Yj;-><init>(ILjava/lang/String;)V

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, LX/BFQ;->b:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/performancelogger/PerformanceLogger;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0id;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1765832
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1765833
    iput-object p1, p0, LX/BFQ;->c:Lcom/facebook/performancelogger/PerformanceLogger;

    .line 1765834
    iput-object p2, p0, LX/BFQ;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 1765835
    iput-object p3, p0, LX/BFQ;->e:LX/0id;

    .line 1765836
    return-void
.end method

.method public static a(LX/0QB;)LX/BFQ;
    .locals 4

    .prologue
    .line 1765837
    new-instance v3, LX/BFQ;

    invoke-static {p0}, LX/0XW;->a(LX/0QB;)LX/0XW;

    move-result-object v0

    check-cast v0, Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-static {p0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v1

    check-cast v1, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static {p0}, LX/0id;->a(LX/0QB;)LX/0id;

    move-result-object v2

    check-cast v2, LX/0id;

    invoke-direct {v3, v0, v1, v2}, LX/BFQ;-><init>(Lcom/facebook/performancelogger/PerformanceLogger;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0id;)V

    .line 1765838
    move-object v0, v3

    .line 1765839
    return-object v0
.end method


# virtual methods
.method public final c()V
    .locals 3

    .prologue
    .line 1765840
    sget-object v0, LX/BFQ;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Yj;

    .line 1765841
    iget-object v2, p0, LX/BFQ;->c:Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-interface {v2, v0}, Lcom/facebook/performancelogger/PerformanceLogger;->d(LX/0Yj;)V

    goto :goto_0

    .line 1765842
    :cond_0
    iget-object v0, p0, LX/BFQ;->e:LX/0id;

    invoke-virtual {v0}, LX/0id;->b()V

    .line 1765843
    return-void
.end method
