.class public LX/Asx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0iK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "Lcom/facebook/friendsharing/inspiration/model/CameraStateSpec$ProvidesCameraState;",
        ":",
        "LX/0io;",
        ":",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationStateSpec$ProvidesInspirationState;",
        "DerivedData:",
        "Ljava/lang/Object;",
        "Services::",
        "LX/0il",
        "<TModelData;>;>",
        "Ljava/lang/Object;",
        "LX/0iK",
        "<TModelData;TDerivedData;>;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TServices;>;"
        }
    .end annotation
.end field

.field public final b:LX/0iH;

.field public final c:Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;

.field public final d:LX/Asv;


# direct methods
.method public constructor <init>(LX/0iH;Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;LX/0il;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0iH;",
            "Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;",
            "TServices;)V"
        }
    .end annotation

    .prologue
    .line 1721101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1721102
    new-instance v0, LX/Asw;

    invoke-direct {v0, p0}, LX/Asw;-><init>(LX/Asx;)V

    iput-object v0, p0, LX/Asx;->d:LX/Asv;

    .line 1721103
    iput-object p1, p0, LX/Asx;->b:LX/0iH;

    .line 1721104
    iput-object p2, p0, LX/Asx;->c:Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;

    .line 1721105
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/Asx;->a:Ljava/lang/ref/WeakReference;

    .line 1721106
    iget-object v0, p0, LX/Asx;->c:Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;

    iget-object v1, p0, LX/Asx;->d:LX/Asv;

    invoke-virtual {v0, v1}, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->a(LX/Asv;)V

    .line 1721107
    return-void
.end method


# virtual methods
.method public final a(LX/5L2;)V
    .locals 2

    .prologue
    .line 1721108
    sget-object v0, LX/5L2;->ON_DESTROY_VIEW:LX/5L2;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, LX/Asx;->c:Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;

    iget-object v1, p0, LX/Asx;->d:LX/Asv;

    invoke-virtual {v0, v1}, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->c(LX/Asv;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1721109
    iget-object v0, p0, LX/Asx;->c:Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;

    iget-object v1, p0, LX/Asx;->d:LX/Asv;

    invoke-virtual {v0, v1}, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->b(LX/Asv;)Z

    .line 1721110
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1721111
    check-cast p1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    .line 1721112
    iget-object v0, p0, LX/Asx;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 1721113
    iget-object v2, p0, LX/Asx;->b:LX/0iH;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    invoke-static {v1}, LX/87R;->a(LX/0io;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->getOpenBottomTray()LX/86o;

    move-result-object v1

    invoke-virtual {v1}, LX/86o;->shouldBlockOtherUIComponents()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-static {v1}, LX/Arx;->c(Lcom/facebook/composer/system/model/ComposerModelImpl;)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    .line 1721114
    :goto_0
    iput-boolean v1, v2, LX/0iH;->g:Z

    .line 1721115
    move-object v1, p1

    .line 1721116
    check-cast v1, LX/0io;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0io;

    invoke-static {v1, v2}, LX/87R;->a(LX/0io;LX/0io;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1721117
    iget-object v0, p0, LX/Asx;->c:Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;

    iget-object v1, p0, LX/Asx;->d:LX/Asv;

    invoke-virtual {v0, v1}, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->c(LX/Asv;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1721118
    iget-object v0, p0, LX/Asx;->c:Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;

    iget-object v1, p0, LX/Asx;->d:LX/Asv;

    invoke-virtual {v0, v1}, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->b(LX/Asv;)Z

    .line 1721119
    :cond_0
    :goto_1
    return-void

    .line 1721120
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 1721121
    :cond_2
    check-cast p1, LX/0io;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    invoke-static {p1, v0}, LX/87R;->b(LX/0io;LX/0io;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1721122
    iget-object v0, p0, LX/Asx;->c:Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;

    iget-object v1, p0, LX/Asx;->d:LX/Asv;

    invoke-virtual {v0, v1}, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->c(LX/Asv;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1721123
    iget-object v0, p0, LX/Asx;->c:Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;

    iget-object v1, p0, LX/Asx;->d:LX/Asv;

    invoke-virtual {v0, v1}, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->a(LX/Asv;)V

    goto :goto_1
.end method
