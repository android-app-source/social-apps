.class public LX/C5u;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/0bH;

.field public final b:LX/0Uh;


# direct methods
.method public constructor <init>(LX/0Uh;LX/0bH;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1849433
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1849434
    iput-object p2, p0, LX/C5u;->a:LX/0bH;

    .line 1849435
    iput-object p1, p0, LX/C5u;->b:LX/0Uh;

    .line 1849436
    return-void
.end method

.method public static a(LX/0QB;)LX/C5u;
    .locals 5

    .prologue
    .line 1849422
    const-class v1, LX/C5u;

    monitor-enter v1

    .line 1849423
    :try_start_0
    sget-object v0, LX/C5u;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1849424
    sput-object v2, LX/C5u;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1849425
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1849426
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1849427
    new-instance p0, LX/C5u;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    invoke-static {v0}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v4

    check-cast v4, LX/0bH;

    invoke-direct {p0, v3, v4}, LX/C5u;-><init>(LX/0Uh;LX/0bH;)V

    .line 1849428
    move-object v0, p0

    .line 1849429
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1849430
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C5u;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1849431
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1849432
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
