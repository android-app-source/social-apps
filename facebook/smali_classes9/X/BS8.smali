.class public LX/BS8;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:Lcom/facebook/uicontrib/fab/FabView;

.field public b:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1784948
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1784949
    const/4 v0, 0x0

    .line 1784950
    const p1, 0x7f03059c

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1784951
    const p1, 0x7f0d0f65

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/uicontrib/fab/FabView;

    iput-object p1, p0, LX/BS8;->a:Lcom/facebook/uicontrib/fab/FabView;

    .line 1784952
    const p1, 0x7f0d0f64

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, LX/BS8;->b:Landroid/widget/TextView;

    .line 1784953
    const/4 p1, 0x1

    invoke-virtual {p0, p1}, LX/BS8;->setClickable(Z)V

    .line 1784954
    iget-object p1, p0, LX/BS8;->a:Lcom/facebook/uicontrib/fab/FabView;

    invoke-virtual {p1, v0}, Lcom/facebook/uicontrib/fab/FabView;->a(Landroid/util/AttributeSet;)V

    .line 1784955
    invoke-static {p0, v0}, LX/BS8;->c(LX/BS8;Landroid/util/AttributeSet;)V

    .line 1784956
    invoke-virtual {p0}, LX/BS8;->a()V

    .line 1784957
    return-void
.end method

.method public static c(LX/BS8;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1784958
    invoke-virtual {p0}, LX/BS8;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [I

    const v2, 0x101014f

    aput v2, v1, v3

    invoke-virtual {v0, p1, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 1784959
    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 1784960
    if-eqz v0, :cond_0

    .line 1784961
    iget-object v2, p0, LX/BS8;->b:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1784962
    :cond_0
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 1784963
    return-void

    .line 1784964
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 1784942
    iget-object v0, p0, LX/BS8;->a:Lcom/facebook/uicontrib/fab/FabView;

    invoke-virtual {v0}, Lcom/facebook/uicontrib/fab/FabView;->getSize()LX/2iJ;

    move-result-object v0

    sget-object v1, LX/2iJ;->SMALL:LX/2iJ;

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, LX/BS8;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b01cd

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    move v1, v0

    .line 1784943
    :goto_0
    iget-object v0, p0, LX/BS8;->a:Lcom/facebook/uicontrib/fab/FabView;

    invoke-virtual {v0}, Lcom/facebook/uicontrib/fab/FabView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 1784944
    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    invoke-virtual {v0, v1, v2, v1, v3}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 1784945
    iget-object v1, p0, LX/BS8;->a:Lcom/facebook/uicontrib/fab/FabView;

    invoke-virtual {v1, v0}, Lcom/facebook/uicontrib/fab/FabView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1784946
    return-void

    .line 1784947
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 1784939
    iget-object v0, p0, LX/BS8;->a:Lcom/facebook/uicontrib/fab/FabView;

    invoke-virtual {v0, p1}, Lcom/facebook/uicontrib/fab/FabView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1784940
    iget-object v0, p0, LX/BS8;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1784941
    return-void
.end method
