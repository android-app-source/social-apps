.class public LX/CX3;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements Landroid/widget/Checkable;


# instance fields
.field public a:Lcom/facebook/resources/ui/FbCheckBox;

.field public b:Landroid/widget/LinearLayout;

.field public c:Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentProductView;

.field private d:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1909408
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, LX/CX3;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1909409
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1909410
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1909411
    const p1, 0x7f030f9a

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1909412
    const p1, 0x7f0d25c0

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    iput-object p1, p0, LX/CX3;->b:Landroid/widget/LinearLayout;

    .line 1909413
    const p1, 0x7f0d0298

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/resources/ui/FbCheckBox;

    iput-object p1, p0, LX/CX3;->a:Lcom/facebook/resources/ui/FbCheckBox;

    .line 1909414
    iget-object p1, p0, LX/CX3;->b:Landroid/widget/LinearLayout;

    sget-object p2, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->PRODUCT:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    invoke-static {p1, p2}, LX/CSi;->b(Landroid/view/ViewGroup;Lcom/facebook/graphql/enums/GraphQLScreenElementType;)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentProductView;

    iput-object p1, p0, LX/CX3;->c:Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentProductView;

    .line 1909415
    iget-object p1, p0, LX/CX3;->b:Landroid/widget/LinearLayout;

    iget-object p2, p0, LX/CX3;->c:Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentProductView;

    invoke-virtual {p1, p2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1909416
    return-void
.end method


# virtual methods
.method public final isChecked()Z
    .locals 1

    .prologue
    .line 1909417
    iget-boolean v0, p0, LX/CX3;->d:Z

    return v0
.end method

.method public final performClick()Z
    .locals 1

    .prologue
    .line 1909418
    invoke-virtual {p0}, LX/CX3;->toggle()V

    .line 1909419
    invoke-super {p0}, Lcom/facebook/widget/CustomRelativeLayout;->performClick()Z

    move-result v0

    return v0
.end method

.method public setChecked(Z)V
    .locals 1

    .prologue
    .line 1909420
    iget-boolean v0, p0, LX/CX3;->d:Z

    if-eq v0, p1, :cond_0

    .line 1909421
    iput-boolean p1, p0, LX/CX3;->d:Z

    .line 1909422
    iget-object v0, p0, LX/CX3;->a:Lcom/facebook/resources/ui/FbCheckBox;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbCheckBox;->setChecked(Z)V

    .line 1909423
    invoke-super {p0}, Lcom/facebook/widget/CustomRelativeLayout;->refreshDrawableState()V

    .line 1909424
    :cond_0
    return-void
.end method

.method public setEnabled(Z)V
    .locals 1

    .prologue
    .line 1909425
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;->setEnabled(Z)V

    .line 1909426
    iget-object v0, p0, LX/CX3;->a:Lcom/facebook/resources/ui/FbCheckBox;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbCheckBox;->setEnabled(Z)V

    .line 1909427
    return-void
.end method

.method public final toggle()V
    .locals 1

    .prologue
    .line 1909428
    iget-boolean v0, p0, LX/CX3;->d:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, LX/CX3;->setChecked(Z)V

    .line 1909429
    return-void

    .line 1909430
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
