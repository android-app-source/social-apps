.class public final LX/COY;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/COY;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/COW;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/COZ;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1883220
    const/4 v0, 0x0

    sput-object v0, LX/COY;->a:LX/COY;

    .line 1883221
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/COY;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1883222
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1883223
    new-instance v0, LX/COZ;

    invoke-direct {v0}, LX/COZ;-><init>()V

    iput-object v0, p0, LX/COY;->c:LX/COZ;

    .line 1883224
    return-void
.end method

.method public static declared-synchronized q()LX/COY;
    .locals 2

    .prologue
    .line 1883225
    const-class v1, LX/COY;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/COY;->a:LX/COY;

    if-nez v0, :cond_0

    .line 1883226
    new-instance v0, LX/COY;

    invoke-direct {v0}, LX/COY;-><init>()V

    sput-object v0, LX/COY;->a:LX/COY;

    .line 1883227
    :cond_0
    sget-object v0, LX/COY;->a:LX/COY;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 1883228
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 8

    .prologue
    .line 1883229
    check-cast p2, LX/COX;

    .line 1883230
    iget-object v0, p2, LX/COX;->a:LX/CNb;

    iget-object v1, p2, LX/COX;->b:LX/CNq;

    iget-object v2, p2, LX/COX;->c:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    const/high16 v6, 0x41500000    # 13.0f

    const/high16 p2, -0x1000000

    .line 1883231
    invoke-static {v2}, LX/2yc;->a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)LX/3Ab;

    move-result-object v3

    .line 1883232
    const-string v4, "font-size"

    invoke-virtual {v0, v4, v6}, LX/CNb;->a(Ljava/lang/String;F)F

    move-result v4

    .line 1883233
    const-string v5, "entity-size"

    invoke-virtual {v0, v5, v6}, LX/CNb;->a(Ljava/lang/String;F)F

    move-result v5

    .line 1883234
    const-string v6, "line-height"

    invoke-virtual {v0, v6, v4}, LX/CNb;->a(Ljava/lang/String;F)F

    move-result v6

    .line 1883235
    div-float v7, v6, v4

    move v6, v7

    .line 1883236
    iget-object v7, v1, LX/CNq;->d:LX/CNs;

    iget-object v7, v7, LX/CNs;->d:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/3AZ;

    move-object v7, v7

    .line 1883237
    invoke-virtual {v7, p1}, LX/3AZ;->c(LX/1De;)LX/3Aa;

    move-result-object v7

    invoke-virtual {v7, v3}, LX/3Aa;->a(LX/3Ab;)LX/3Aa;

    move-result-object v3

    const-string v7, "entity-interaction-enabled"

    const/4 p0, 0x0

    invoke-virtual {v0, v7, p0}, LX/CNb;->a(Ljava/lang/String;Z)Z

    move-result v7

    invoke-virtual {v3, v7}, LX/3Aa;->b(Z)LX/3Aa;

    move-result-object v3

    .line 1883238
    iget-object v7, v3, LX/3Aa;->a:LX/8yo;

    invoke-virtual {v3, v4}, LX/1Dp;->b(F)I

    move-result p0

    iput p0, v7, LX/8yo;->n:I

    .line 1883239
    move-object v3, v3

    .line 1883240
    iget-object v4, v3, LX/3Aa;->a:LX/8yo;

    invoke-virtual {v3, v5}, LX/1Dp;->b(F)I

    move-result v7

    iput v7, v4, LX/8yo;->o:I

    .line 1883241
    move-object v3, v3

    .line 1883242
    const-string v4, "color"

    invoke-virtual {v0, v4, p2}, LX/CNb;->b(Ljava/lang/String;I)I

    move-result v4

    .line 1883243
    iget-object v5, v3, LX/3Aa;->a:LX/8yo;

    iput v4, v5, LX/8yo;->j:I

    .line 1883244
    move-object v3, v3

    .line 1883245
    const-string v4, "horizontal-alignment"

    const-string v5, ""

    invoke-virtual {v0, v4, v5}, LX/CNb;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/CPv;->b(Ljava/lang/String;)Landroid/text/Layout$Alignment;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/3Aa;->a(Landroid/text/Layout$Alignment;)LX/3Aa;

    move-result-object v3

    const-string v4, "max-lines"

    const v5, 0x7fffffff

    invoke-virtual {v0, v4, v5}, LX/CNb;->a(Ljava/lang/String;I)I

    move-result v4

    invoke-virtual {v3, v4}, LX/3Aa;->h(I)LX/3Aa;

    move-result-object v3

    const-string v4, "truncation-mode"

    const-string v5, ""

    invoke-virtual {v0, v4, v5}, LX/CNb;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/CPv;->a(Ljava/lang/String;)Landroid/text/TextUtils$TruncateAt;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/3Aa;->a(Landroid/text/TextUtils$TruncateAt;)LX/3Aa;

    move-result-object v3

    .line 1883246
    iget-object v4, v3, LX/3Aa;->a:LX/8yo;

    iput v6, v4, LX/8yo;->i:F

    .line 1883247
    move-object v3, v3

    .line 1883248
    const-string v4, "shadow-color"

    invoke-virtual {v0, v4, p2}, LX/CNb;->b(Ljava/lang/String;I)I

    move-result v4

    .line 1883249
    iget-object v5, v3, LX/3Aa;->a:LX/8yo;

    iput v4, v5, LX/8yo;->u:I

    .line 1883250
    move-object v3, v3

    .line 1883251
    const-string v4, "shadow-offset-x"

    invoke-virtual {v0, v4, p1}, LX/CNb;->a(Ljava/lang/String;Landroid/content/Context;)I

    move-result v4

    .line 1883252
    iget-object v5, v3, LX/3Aa;->a:LX/8yo;

    iput v4, v5, LX/8yo;->s:I

    .line 1883253
    move-object v3, v3

    .line 1883254
    const-string v4, "shadow-offset-y"

    invoke-virtual {v0, v4, p1}, LX/CNb;->a(Ljava/lang/String;Landroid/content/Context;)I

    move-result v4

    .line 1883255
    iget-object v5, v3, LX/3Aa;->a:LX/8yo;

    iput v4, v5, LX/8yo;->t:I

    .line 1883256
    move-object v3, v3

    .line 1883257
    invoke-static {v0, p1}, LX/COM;->a(LX/CNb;Landroid/content/Context;)I

    move-result v4

    .line 1883258
    iget-object v5, v3, LX/3Aa;->a:LX/8yo;

    iput v4, v5, LX/8yo;->r:I

    .line 1883259
    move-object v3, v3

    .line 1883260
    const-string v4, "entity-color"

    invoke-virtual {v0, v4}, LX/CNb;->c(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1883261
    const-string v4, "entity-color"

    invoke-virtual {v0, v4, p2}, LX/CNb;->b(Ljava/lang/String;I)I

    move-result v4

    .line 1883262
    iget-object v5, v3, LX/3Aa;->a:LX/8yo;

    iput v4, v5, LX/8yo;->k:I

    .line 1883263
    :cond_0
    const-string v4, "font-family"

    invoke-virtual {v0, v4}, LX/CNb;->c(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1883264
    const-string v4, "font-weight"

    invoke-static {v0, v4}, LX/CPv;->a(LX/CNb;Ljava/lang/String;)I

    move-result v4

    .line 1883265
    const-string v5, "entity-weight"

    invoke-static {v0, v5}, LX/CPv;->a(LX/CNb;Ljava/lang/String;)I

    move-result v5

    .line 1883266
    const-string v6, "font-family"

    const-string v7, ""

    invoke-virtual {v0, v6, v7}, LX/CNb;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1883267
    invoke-static {v6, v4}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v4

    .line 1883268
    invoke-static {v6, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v5

    .line 1883269
    iget-object v6, v3, LX/3Aa;->a:LX/8yo;

    iput-object v4, v6, LX/8yo;->p:Landroid/graphics/Typeface;

    .line 1883270
    move-object v4, v3

    .line 1883271
    iget-object v6, v4, LX/3Aa;->a:LX/8yo;

    iput-object v5, v6, LX/8yo;->q:Landroid/graphics/Typeface;

    .line 1883272
    :cond_1
    :goto_0
    invoke-virtual {v3}, LX/1X5;->b()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 1883273
    return-object v0

    .line 1883274
    :cond_2
    const-string v4, "font-weight"

    invoke-virtual {v0, v4}, LX/CNb;->c(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1883275
    const-string v4, "font-weight"

    invoke-static {v0, v4}, LX/CPv;->a(LX/CNb;Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v3, v4}, LX/3Aa;->l(I)LX/3Aa;

    .line 1883276
    :cond_3
    const-string v4, "entity-weight"

    invoke-virtual {v0, v4}, LX/CNb;->c(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1883277
    const-string v4, "entity-weight"

    invoke-static {v0, v4}, LX/CPv;->a(LX/CNb;Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v3, v4}, LX/3Aa;->l(I)LX/3Aa;

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1883278
    invoke-static {}, LX/1dS;->b()V

    .line 1883279
    const/4 v0, 0x0

    return-object v0
.end method
