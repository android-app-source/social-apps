.class public final LX/C12;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;


# instance fields
.field public final synthetic a:LX/8xF;

.field public final synthetic b:LX/C13;


# direct methods
.method public constructor <init>(LX/C13;LX/8xF;)V
    .locals 0

    .prologue
    .line 1841864
    iput-object p1, p0, LX/C12;->b:LX/C13;

    iput-object p2, p0, LX/C12;->a:LX/8xF;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onDismiss(Landroid/content/DialogInterface;)V
    .locals 3

    .prologue
    .line 1841853
    iget-object v0, p0, LX/C12;->a:LX/8xF;

    const/4 v1, 0x0

    .line 1841854
    iput-boolean v1, v0, LX/8xF;->A:Z

    .line 1841855
    iget-object v0, p0, LX/C12;->a:LX/8xF;

    invoke-virtual {v0}, LX/8wv;->h()V

    .line 1841856
    iget-object v0, p0, LX/C12;->a:LX/8xF;

    .line 1841857
    iget-object v1, v0, LX/8wv;->o:Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;

    invoke-virtual {v1}, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->l()V

    .line 1841858
    iget-object v0, p0, LX/C12;->b:LX/C13;

    iget-object v1, v0, LX/C13;->a:LX/1xG;

    iget-object v0, p0, LX/C12;->b:LX/C13;

    iget-object v0, v0, LX/C13;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1841859
    iget-object v2, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v2

    .line 1841860
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, LX/C12;->b:LX/C13;

    iget-object v2, v2, LX/C13;->c:LX/7Dj;

    .line 1841861
    new-instance p0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p1, "spherical_photo_fullscreen_exited"

    invoke-direct {p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1841862
    invoke-static {v1, p0, v0, v2}, LX/1xG;->a(LX/1xG;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/7Dj;)V

    .line 1841863
    return-void
.end method
