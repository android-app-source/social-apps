.class public LX/C9Y;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static i:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

.field private final b:LX/0qn;

.field public final c:LX/9A1;

.field public final d:LX/7ly;

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8Mh;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8Mb;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/03V;

.field public final h:LX/0ad;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;LX/0qn;LX/9A1;LX/7ly;LX/0Ot;LX/0Ot;LX/03V;LX/0ad;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;",
            "LX/0qn;",
            "LX/9A1;",
            "LX/7ly;",
            "LX/0Ot",
            "<",
            "LX/8Mh;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/8Mb;",
            ">;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1854171
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1854172
    iput-object p1, p0, LX/C9Y;->a:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    .line 1854173
    iput-object p2, p0, LX/C9Y;->b:LX/0qn;

    .line 1854174
    iput-object p3, p0, LX/C9Y;->c:LX/9A1;

    .line 1854175
    iput-object p5, p0, LX/C9Y;->e:LX/0Ot;

    .line 1854176
    iput-object p6, p0, LX/C9Y;->f:LX/0Ot;

    .line 1854177
    iput-object p4, p0, LX/C9Y;->d:LX/7ly;

    .line 1854178
    iput-object p7, p0, LX/C9Y;->g:LX/03V;

    .line 1854179
    iput-object p8, p0, LX/C9Y;->h:LX/0ad;

    .line 1854180
    return-void
.end method

.method public static a(LX/0QB;)LX/C9Y;
    .locals 12

    .prologue
    .line 1854181
    const-class v1, LX/C9Y;

    monitor-enter v1

    .line 1854182
    :try_start_0
    sget-object v0, LX/C9Y;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1854183
    sput-object v2, LX/C9Y;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1854184
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1854185
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1854186
    new-instance v3, LX/C9Y;

    invoke-static {v0}, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->a(LX/0QB;)Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    move-result-object v4

    check-cast v4, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    invoke-static {v0}, LX/0qn;->a(LX/0QB;)LX/0qn;

    move-result-object v5

    check-cast v5, LX/0qn;

    invoke-static {v0}, LX/9A1;->a(LX/0QB;)LX/9A1;

    move-result-object v6

    check-cast v6, LX/9A1;

    const-class v7, LX/7ly;

    invoke-interface {v0, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/7ly;

    const/16 v8, 0x2eed

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x2ee9

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v10

    check-cast v10, LX/03V;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v11

    check-cast v11, LX/0ad;

    invoke-direct/range {v3 .. v11}, LX/C9Y;-><init>(Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;LX/0qn;LX/9A1;LX/7ly;LX/0Ot;LX/0Ot;LX/03V;LX/0ad;)V

    .line 1854187
    move-object v0, v3

    .line 1854188
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1854189
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C9Y;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1854190
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1854191
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a()Z
    .locals 3

    .prologue
    .line 1854192
    iget-object v0, p0, LX/C9Y;->h:LX/0ad;

    sget-short v1, LX/1aO;->M:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method private d(Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1854193
    iget-object v0, p0, LX/C9Y;->a:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->S()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->d(Ljava/lang/String;)Lcom/facebook/composer/publish/common/PendingStory;

    move-result-object v0

    .line 1854194
    iget-object v1, p0, LX/C9Y;->b:LX/0qn;

    invoke-virtual {v1, p1}, LX/0qn;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    move-result-object v1

    .line 1854195
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->TRANSCODING_FAILED:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    if-eq v1, v2, :cond_0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->FAILED:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    if-ne v1, v2, :cond_1

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/PendingStory;->g()Lcom/facebook/composer/publish/common/ErrorDetails;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/PendingStory;->g()Lcom/facebook/composer/publish/common/ErrorDetails;

    move-result-object v1

    iget-boolean v1, v1, Lcom/facebook/composer/publish/common/ErrorDetails;->isRetriable:Z

    if-nez v1, :cond_1

    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/PendingStory;->k()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLStory;Z)Z
    .locals 1

    .prologue
    .line 1854196
    if-eqz p2, :cond_0

    invoke-direct {p0}, LX/C9Y;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    if-nez p2, :cond_2

    invoke-direct {p0}, LX/C9Y;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1854197
    :cond_1
    const/4 v0, 0x0

    .line 1854198
    :goto_0
    return v0

    :cond_2
    invoke-direct {p0, p1}, LX/C9Y;->d(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    goto :goto_0
.end method
