.class public final LX/AWH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/AVe;


# instance fields
.field public final synthetic a:Lcom/facebook/facecast/broadcast/nux/protocol/LiveNuxPrefetcher;


# direct methods
.method public constructor <init>(Lcom/facebook/facecast/broadcast/nux/protocol/LiveNuxPrefetcher;)V
    .locals 0

    .prologue
    .line 1682560
    iput-object p1, p0, LX/AWH;->a:Lcom/facebook/facecast/broadcast/nux/protocol/LiveNuxPrefetcher;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;)V
    .locals 4

    .prologue
    .line 1682561
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->o()Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel$VideoModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->o()Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel$VideoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel$VideoModel;->k()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1682562
    :cond_0
    :goto_0
    return-void

    .line 1682563
    :cond_1
    new-instance v0, LX/39x;

    invoke-direct {v0}, LX/39x;-><init>()V

    new-instance v1, LX/4XB;

    invoke-direct {v1}, LX/4XB;-><init>()V

    invoke-virtual {p1}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->o()Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel$VideoModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel$VideoModel;->k()Ljava/lang/String;

    move-result-object v2

    .line 1682564
    iput-object v2, v1, LX/4XB;->T:Ljava/lang/String;

    .line 1682565
    move-object v1, v1

    .line 1682566
    invoke-virtual {p1}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->o()Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel$VideoModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel$VideoModel;->l()Ljava/lang/String;

    move-result-object v2

    .line 1682567
    iput-object v2, v1, LX/4XB;->bc:Ljava/lang/String;

    .line 1682568
    move-object v1, v1

    .line 1682569
    new-instance v2, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v3, 0x4ed245b

    invoke-direct {v2, v3}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    .line 1682570
    iput-object v2, v1, LX/4XB;->bQ:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1682571
    move-object v1, v1

    .line 1682572
    invoke-virtual {v1}, LX/4XB;->a()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    .line 1682573
    iput-object v1, v0, LX/39x;->j:Lcom/facebook/graphql/model/GraphQLMedia;

    .line 1682574
    move-object v0, v0

    .line 1682575
    invoke-virtual {v0}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 1682576
    iget-object v1, p0, LX/AWH;->a:Lcom/facebook/facecast/broadcast/nux/protocol/LiveNuxPrefetcher;

    iget-object v1, v1, Lcom/facebook/facecast/broadcast/nux/protocol/LiveNuxPrefetcher;->f:LX/1ly;

    invoke-virtual {v1, v0}, LX/1ly;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/Void;

    .line 1682577
    invoke-static {}, Lcom/facebook/facecast/broadcast/nux/protocol/LiveNuxPrefetcher;->f()Ljava/util/GregorianCalendar;

    move-result-object v0

    .line 1682578
    const/4 v1, 0x5

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/GregorianCalendar;->add(II)V

    .line 1682579
    iget-object v1, p0, LX/AWH;->a:Lcom/facebook/facecast/broadcast/nux/protocol/LiveNuxPrefetcher;

    iget-object v1, v1, Lcom/facebook/facecast/broadcast/nux/protocol/LiveNuxPrefetcher;->c:LX/AaZ;

    iget-object v2, p0, LX/AWH;->a:Lcom/facebook/facecast/broadcast/nux/protocol/LiveNuxPrefetcher;

    iget-object v2, v2, Lcom/facebook/facecast/broadcast/nux/protocol/LiveNuxPrefetcher;->e:Ljava/text/SimpleDateFormat;

    invoke-virtual {v0}, Ljava/util/GregorianCalendar;->getTime()Ljava/util/Date;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 1682580
    iget-object v2, v1, LX/AaZ;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    sget-object v3, LX/AaZ;->b:LX/0Tn;

    invoke-interface {v2, v3, v0}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v2

    invoke-interface {v2}, LX/0hN;->commit()V

    .line 1682581
    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1682582
    return-void
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1682583
    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    .line 1682584
    return-void
.end method
