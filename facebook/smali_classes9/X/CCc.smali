.class public final LX/CCc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/34L;


# instance fields
.field public final synthetic a:LX/1xz;

.field public final synthetic b:Lcom/facebook/feedplugins/spannable/SpannableInTextLayoutPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/spannable/SpannableInTextLayoutPartDefinition;LX/1xz;)V
    .locals 0

    .prologue
    .line 1858058
    iput-object p1, p0, LX/CCc;->b:Lcom/facebook/feedplugins/spannable/SpannableInTextLayoutPartDefinition;

    iput-object p2, p0, LX/CCc;->a:LX/1xz;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/CharSequence;II)V
    .locals 8

    .prologue
    .line 1858059
    iget-object v0, p0, LX/CCc;->a:LX/1xz;

    invoke-interface {v0}, LX/1xz;->c()LX/0jW;

    move-result-object v0

    .line 1858060
    invoke-interface {v0}, LX/0jW;->g()Ljava/lang/String;

    move-result-object v1

    .line 1858061
    iget-object v2, p0, LX/CCc;->b:Lcom/facebook/feedplugins/spannable/SpannableInTextLayoutPartDefinition;

    iget-object v2, v2, Lcom/facebook/feedplugins/spannable/SpannableInTextLayoutPartDefinition;->d:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v1

    .line 1858062
    if-nez v1, :cond_1

    .line 1858063
    :cond_0
    :goto_0
    return-void

    .line 1858064
    :cond_1
    sub-int v1, p2, p3

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    .line 1858065
    iget-object v2, p0, LX/CCc;->b:Lcom/facebook/feedplugins/spannable/SpannableInTextLayoutPartDefinition;

    iget-object v2, v2, Lcom/facebook/feedplugins/spannable/SpannableInTextLayoutPartDefinition;->a:LX/1DR;

    invoke-virtual {v2}, LX/1DR;->b()I

    move-result v2

    .line 1858066
    if-eq v1, v2, :cond_0

    .line 1858067
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v4

    const-wide v6, 0x3f50624dd2f1a9fcL    # 0.001

    cmpg-double v1, v4, v6

    if-gez v1, :cond_0

    .line 1858068
    iget-object v1, p0, LX/CCc;->b:Lcom/facebook/feedplugins/spannable/SpannableInTextLayoutPartDefinition;

    iget-object v1, v1, Lcom/facebook/feedplugins/spannable/SpannableInTextLayoutPartDefinition;->c:LX/03V;

    const-string v3, "WrongWidthForTextLayout"

    const-string v4, "Regenerating layout for text = %s\nexpected width = %s\nactual width = %s\nwidth-height difference = %s\npersistent spannble input = %s\nstory = %s"

    const/4 v5, 0x6

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p1, v5, v6

    const/4 v6, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v5, v6

    const/4 v2, 0x4

    iget-object v6, p0, LX/CCc;->a:LX/1xz;

    aput-object v6, v5, v2

    const/4 v2, 0x5

    aput-object v0, v5, v2

    invoke-static {v4, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
