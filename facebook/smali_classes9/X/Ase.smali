.class public LX/Ase;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationStateSpec$ProvidesInspirationState;",
        "Transaction::",
        "Lcom/facebook/ipc/composer/dataaccessor/ComposerCanSave;",
        ":",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationStateSpec$SetsInspirationState",
        "<TTransaction;>;Services::",
        "LX/0il",
        "<TModelData;>;:",
        "LX/0im",
        "<TTransaction;>;>",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final a:LX/0jK;


# instance fields
.field public final b:Landroid/content/Context;

.field public final c:LX/0il;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TServices;"
        }
    .end annotation
.end field

.field public final d:LX/AsA;

.field public final e:Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;

.field public f:Lcom/facebook/friendsharing/inspiration/controller/InspirationBottomTrayLinearLayout;

.field public g:Landroid/view/View;

.field public h:Landroid/view/View;

.field public i:Landroid/widget/LinearLayout;

.field public j:Landroid/view/View$OnClickListener;

.field public k:I

.field public l:Z

.field public final m:LX/Asb;

.field public final n:LX/Ard;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1720696
    const-class v0, LX/Ase;

    invoke-static {v0}, LX/0jK;->a(Ljava/lang/Class;)LX/0jK;

    move-result-object v0

    sput-object v0, LX/Ase;->a:LX/0jK;

    return-void
.end method

.method public constructor <init>(LX/AsA;Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;LX/0il;Landroid/content/Context;)V
    .locals 1
    .param p1    # LX/AsA;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/0il;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/AsA;",
            "Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;",
            "TServices;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1720697
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1720698
    new-instance v0, LX/Asc;

    invoke-direct {v0, p0}, LX/Asc;-><init>(LX/Ase;)V

    iput-object v0, p0, LX/Ase;->m:LX/Asb;

    .line 1720699
    new-instance v0, LX/Asd;

    invoke-direct {v0, p0}, LX/Asd;-><init>(LX/Ase;)V

    iput-object v0, p0, LX/Ase;->n:LX/Ard;

    .line 1720700
    iput-object p1, p0, LX/Ase;->d:LX/AsA;

    .line 1720701
    iput-object p2, p0, LX/Ase;->e:Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;

    .line 1720702
    iput-object p3, p0, LX/Ase;->c:LX/0il;

    .line 1720703
    iput-object p4, p0, LX/Ase;->b:Landroid/content/Context;

    .line 1720704
    iget-object v0, p0, LX/Ase;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 p1, 0x10e0000

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, LX/Ase;->k:I

    .line 1720705
    return-void
.end method

.method public static a$redex0(LX/Ase;LX/ArJ;)V
    .locals 3

    .prologue
    .line 1720706
    iget-object v0, p0, LX/Ase;->c:LX/0il;

    sget-object v1, LX/Ase;->a:LX/0jK;

    iget-object v2, p0, LX/Ase;->d:LX/AsA;

    invoke-interface {v2}, LX/AsA;->b()LX/86o;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/87N;->b(LX/0il;LX/0jK;LX/86o;)V

    .line 1720707
    iget-object v0, p0, LX/Ase;->d:LX/AsA;

    invoke-interface {v0, p1}, LX/AsA;->a(LX/ArJ;)V

    .line 1720708
    return-void
.end method

.method public static f$redex0(LX/Ase;)V
    .locals 3

    .prologue
    .line 1720709
    iget-object v0, p0, LX/Ase;->c:LX/0il;

    check-cast v0, LX/0im;

    invoke-interface {v0}, LX/0im;->c()LX/0jJ;

    move-result-object v0

    sget-object v1, LX/Ase;->a:LX/0jK;

    invoke-virtual {v0, v1}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v0

    check-cast v0, LX/0jL;

    iget-object v1, p0, LX/Ase;->c:LX/0il;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationState;)Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->setIsBottomTrayTransitioning(Z)Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0jL;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationState;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jL;

    invoke-virtual {v0}, LX/0jL;->a()V

    .line 1720710
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewStub;I)Landroid/view/View;
    .locals 3

    .prologue
    .line 1720711
    invoke-virtual {p1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/inspiration/controller/InspirationBottomTrayLinearLayout;

    .line 1720712
    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/controller/InspirationBottomTrayLinearLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, p2, v0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1720713
    iput-object v0, p0, LX/Ase;->f:Lcom/facebook/friendsharing/inspiration/controller/InspirationBottomTrayLinearLayout;

    .line 1720714
    iget-object v2, p0, LX/Ase;->f:Lcom/facebook/friendsharing/inspiration/controller/InspirationBottomTrayLinearLayout;

    const p1, 0x7f0d0756

    invoke-virtual {v2, p1}, Lcom/facebook/friendsharing/inspiration/controller/InspirationBottomTrayLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, LX/Ase;->h:Landroid/view/View;

    .line 1720715
    iget-object v2, p0, LX/Ase;->f:Lcom/facebook/friendsharing/inspiration/controller/InspirationBottomTrayLinearLayout;

    const p1, 0x7f0d02a6

    invoke-virtual {v2, p1}, Lcom/facebook/friendsharing/inspiration/controller/InspirationBottomTrayLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, LX/Ase;->g:Landroid/view/View;

    .line 1720716
    iget-object v2, p0, LX/Ase;->g:Landroid/view/View;

    .line 1720717
    iget-object p1, p0, LX/Ase;->j:Landroid/view/View$OnClickListener;

    if-nez p1, :cond_0

    .line 1720718
    new-instance p1, LX/Asa;

    invoke-direct {p1, p0}, LX/Asa;-><init>(LX/Ase;)V

    iput-object p1, p0, LX/Ase;->j:Landroid/view/View$OnClickListener;

    .line 1720719
    :cond_0
    iget-object p1, p0, LX/Ase;->j:Landroid/view/View$OnClickListener;

    move-object p1, p1

    .line 1720720
    invoke-virtual {v2, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1720721
    iget-object v2, p0, LX/Ase;->f:Lcom/facebook/friendsharing/inspiration/controller/InspirationBottomTrayLinearLayout;

    const p1, 0x7f0d0755

    invoke-virtual {v2, p1}, Lcom/facebook/friendsharing/inspiration/controller/InspirationBottomTrayLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, LX/Ase;->i:Landroid/widget/LinearLayout;

    .line 1720722
    iget-object v2, p0, LX/Ase;->d:LX/AsA;

    invoke-interface {v2}, LX/AsA;->b()LX/86o;

    move-result-object v2

    invoke-virtual {v2}, LX/86o;->shouldUseTrayBackgroundGradient()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1720723
    iget-object v2, p0, LX/Ase;->i:Landroid/widget/LinearLayout;

    iget-object p1, p0, LX/Ase;->b:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const p2, 0x7f020d94

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    invoke-static {v2, p1}, LX/0zj;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 1720724
    :cond_1
    iget-object v2, p0, LX/Ase;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1720725
    iget-object v2, p0, LX/Ase;->f:Lcom/facebook/friendsharing/inspiration/controller/InspirationBottomTrayLinearLayout;

    const/4 p1, 0x4

    invoke-virtual {v2, p1}, Lcom/facebook/friendsharing/inspiration/controller/InspirationBottomTrayLinearLayout;->setVisibility(I)V

    .line 1720726
    invoke-virtual {v1}, Landroid/view/View;->requestLayout()V

    .line 1720727
    return-object v1
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1720728
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Ase;->l:Z

    .line 1720729
    iget-object v0, p0, LX/Ase;->f:Lcom/facebook/friendsharing/inspiration/controller/InspirationBottomTrayLinearLayout;

    new-instance v1, Lcom/facebook/friendsharing/inspiration/controller/InspirationBottomTrayContainer$1;

    invoke-direct {v1, p0}, Lcom/facebook/friendsharing/inspiration/controller/InspirationBottomTrayContainer$1;-><init>(LX/Ase;)V

    invoke-static {v0, v1}, LX/Awl;->a(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 1720730
    return-void
.end method

.method public final c()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1720731
    iget-object v0, p0, LX/Ase;->f:Lcom/facebook/friendsharing/inspiration/controller/InspirationBottomTrayLinearLayout;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/Ase;->l:Z

    if-eqz v0, :cond_0

    .line 1720732
    iput-boolean v4, p0, LX/Ase;->l:Z

    .line 1720733
    new-instance v0, LX/AuH;

    iget-object v1, p0, LX/Ase;->i:Landroid/widget/LinearLayout;

    const/4 v2, 0x1

    iget v3, p0, LX/Ase;->k:I

    invoke-direct {v0, v1, v4, v2, v3}, LX/AuH;-><init>(Landroid/view/View;ZZI)V

    .line 1720734
    iget-object v1, p0, LX/Ase;->f:Lcom/facebook/friendsharing/inspiration/controller/InspirationBottomTrayLinearLayout;

    invoke-virtual {v1, v0}, Lcom/facebook/friendsharing/inspiration/controller/InspirationBottomTrayLinearLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1720735
    iget-object v1, p0, LX/Ase;->f:Lcom/facebook/friendsharing/inspiration/controller/InspirationBottomTrayLinearLayout;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/facebook/friendsharing/inspiration/controller/InspirationBottomTrayLinearLayout;->setVisibility(I)V

    .line 1720736
    new-instance v1, LX/AsZ;

    invoke-direct {v1, p0}, LX/AsZ;-><init>(LX/Ase;)V

    invoke-virtual {v0, v1}, LX/AuH;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1720737
    :cond_0
    return-void
.end method
