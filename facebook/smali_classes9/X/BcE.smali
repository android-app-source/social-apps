.class public LX/BcE;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/BcC;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/BcF;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1801692
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/BcE;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/BcF;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1801689
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1801690
    iput-object p1, p0, LX/BcE;->b:LX/0Ot;

    .line 1801691
    return-void
.end method

.method public static a(LX/0QB;)LX/BcE;
    .locals 4

    .prologue
    .line 1801678
    const-class v1, LX/BcE;

    monitor-enter v1

    .line 1801679
    :try_start_0
    sget-object v0, LX/BcE;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1801680
    sput-object v2, LX/BcE;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1801681
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1801682
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1801683
    new-instance v3, LX/BcE;

    const/16 p0, 0x1940

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/BcE;-><init>(LX/0Ot;)V

    .line 1801684
    move-object v0, v3

    .line 1801685
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1801686
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/BcE;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1801687
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1801688
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1801676
    invoke-static {}, LX/1dS;->b()V

    .line 1801677
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/1De;LX/1Dg;IILX/1no;LX/1X1;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x346cd39f    # 2.20562E-7f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1801670
    check-cast p6, LX/BcD;

    .line 1801671
    iget-object v1, p0, LX/BcE;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v1, p6, LX/BcD;->a:LX/5Jq;

    .line 1801672
    iget v2, v1, LX/5Jq;->a:I

    move v2, v2

    .line 1801673
    invoke-virtual {v1, p1, v2}, LX/3mY;->a(LX/1De;I)LX/1X1;

    move-result-object v2

    invoke-static {p1, v2}, LX/1cy;->a(LX/1De;LX/1X1;)LX/1me;

    move-result-object v2

    invoke-virtual {v2}, LX/1me;->b()LX/1dV;

    move-result-object v2

    .line 1801674
    invoke-virtual {v2, p3, p4, p5}, LX/1dV;->a(IILX/1no;)V

    .line 1801675
    const/16 v1, 0x1f

    const v2, 0x275e072

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(LX/1De;LX/1Dg;LX/1X1;)V
    .locals 1

    .prologue
    .line 1801666
    check-cast p3, LX/BcD;

    .line 1801667
    iget-object v0, p0, LX/BcE;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v0, p3, LX/BcD;->a:LX/5Jq;

    .line 1801668
    invoke-virtual {p2}, LX/1Dg;->c()I

    move-result p0

    invoke-virtual {p2}, LX/1Dg;->d()I

    move-result p1

    invoke-virtual {v0, p0, p1}, LX/3mY;->b(II)V

    .line 1801669
    return-void
.end method

.method public final b(LX/1De;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1801663
    iget-object v0, p0, LX/BcE;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    .line 1801664
    new-instance v0, Lcom/facebook/feed/rows/pager/RowViewPager;

    invoke-direct {v0, p1}, Lcom/facebook/feed/rows/pager/RowViewPager;-><init>(Landroid/content/Context;)V

    move-object v0, v0

    .line 1801665
    return-object v0
.end method

.method public final c(LX/1De;)LX/BcC;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1801693
    new-instance v1, LX/BcD;

    invoke-direct {v1, p0}, LX/BcD;-><init>(LX/BcE;)V

    .line 1801694
    sget-object v2, LX/BcE;->a:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/BcC;

    .line 1801695
    if-nez v2, :cond_0

    .line 1801696
    new-instance v2, LX/BcC;

    invoke-direct {v2}, LX/BcC;-><init>()V

    .line 1801697
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/BcC;->a$redex0(LX/BcC;LX/1De;IILX/BcD;)V

    .line 1801698
    move-object v1, v2

    .line 1801699
    move-object v0, v1

    .line 1801700
    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1801662
    const/4 v0, 0x1

    return v0
.end method

.method public final e(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 2

    .prologue
    .line 1801637
    check-cast p3, LX/BcD;

    .line 1801638
    iget-object v0, p0, LX/BcE;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    check-cast p2, Lcom/facebook/feed/rows/pager/RowViewPager;

    iget-object v0, p3, LX/BcD;->a:LX/5Jq;

    iget v1, p3, LX/BcD;->c:I

    .line 1801639
    invoke-virtual {p2, v1}, Landroid/support/v4/view/ViewPager;->setPageMargin(I)V

    .line 1801640
    invoke-virtual {v0, p2}, LX/3mY;->a(Landroid/view/ViewGroup;)V

    .line 1801641
    return-void
.end method

.method public final f()LX/1mv;
    .locals 1

    .prologue
    .line 1801661
    sget-object v0, LX/1mv;->VIEW:LX/1mv;

    return-object v0
.end method

.method public final f(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 1

    .prologue
    .line 1801657
    check-cast p3, LX/BcD;

    .line 1801658
    iget-object v0, p0, LX/BcE;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    check-cast p2, Lcom/facebook/feed/rows/pager/RowViewPager;

    iget-object v0, p3, LX/BcD;->a:LX/5Jq;

    .line 1801659
    invoke-virtual {v0, p2}, LX/3mY;->d(Landroid/view/ViewGroup;)V

    .line 1801660
    return-void
.end method

.method public final g(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 3

    .prologue
    .line 1801650
    check-cast p3, LX/BcD;

    .line 1801651
    iget-object v0, p0, LX/BcE;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BcF;

    check-cast p2, Lcom/facebook/feed/rows/pager/RowViewPager;

    iget-object v1, p3, LX/BcD;->a:LX/5Jq;

    iget-object v2, p3, LX/BcD;->b:LX/2dx;

    .line 1801652
    iget-object p0, v0, LX/BcF;->b:LX/198;

    invoke-static {}, LX/25e;->c()LX/25e;

    move-result-object p1

    invoke-virtual {p0, p1}, LX/198;->c(LX/1YD;)V

    .line 1801653
    invoke-virtual {v1, p2}, LX/3mY;->b(Landroid/view/ViewGroup;)V

    .line 1801654
    if-eqz v2, :cond_0

    .line 1801655
    iput-object p2, v2, LX/2dx;->a:Landroid/view/View;

    .line 1801656
    :cond_0
    return-void
.end method

.method public final h(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 2

    .prologue
    .line 1801643
    check-cast p3, LX/BcD;

    .line 1801644
    iget-object v0, p0, LX/BcE;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    check-cast p2, Lcom/facebook/feed/rows/pager/RowViewPager;

    iget-object v0, p3, LX/BcD;->a:LX/5Jq;

    iget-object v1, p3, LX/BcD;->b:LX/2dx;

    .line 1801645
    invoke-virtual {v0, p2}, LX/3mY;->c(Landroid/view/ViewGroup;)V

    .line 1801646
    if-eqz v1, :cond_0

    .line 1801647
    const/4 p0, 0x0

    .line 1801648
    iput-object p0, v1, LX/2dx;->a:Landroid/view/View;

    .line 1801649
    :cond_0
    return-void
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 1801642
    const/16 v0, 0xf

    return v0
.end method
