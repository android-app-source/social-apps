.class public LX/ArM;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0Zb;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1719057
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1719058
    return-void
.end method

.method public static a(LX/0QB;)LX/ArM;
    .locals 1

    .prologue
    .line 1719059
    invoke-static {p0}, LX/ArM;->b(LX/0QB;)LX/ArM;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/ArH;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 3

    .prologue
    .line 1719060
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "inline_composer_prompt_event"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    sget-object v1, LX/ArI;->ACTION:LX/ArI;

    invoke-virtual {v1}, LX/ArI;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, LX/ArH;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    sget-object v1, LX/ArI;->IS_CAMERA_SYSTEM:LX/ArI;

    invoke-virtual {v1}, LX/ArI;->getName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/ArM;
    .locals 2

    .prologue
    .line 1719061
    new-instance v1, LX/ArM;

    invoke-direct {v1}, LX/ArM;-><init>()V

    .line 1719062
    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    .line 1719063
    iput-object v0, v1, LX/ArM;->a:LX/0Zb;

    .line 1719064
    return-object v1
.end method
