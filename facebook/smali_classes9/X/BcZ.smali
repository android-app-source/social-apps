.class public LX/BcZ;
.super LX/BcQ;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/BcQ",
        "<",
        "LX/BcM;",
        ">;"
    }
.end annotation


# instance fields
.field private final d:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/BcY;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/BcY;)V
    .locals 2

    .prologue
    .line 1802332
    const/4 v0, 0x0

    const/4 v1, -0x1

    invoke-direct {p0, v0, v1}, LX/BcQ;-><init>(LX/BcO;I)V

    .line 1802333
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/BcZ;->d:Ljava/lang/ref/WeakReference;

    .line 1802334
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1802335
    check-cast p1, LX/BcM;

    .line 1802336
    iget-object v0, p0, LX/BcZ;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BcY;

    .line 1802337
    if-nez v0, :cond_0

    .line 1802338
    :goto_0
    return-void

    .line 1802339
    :cond_0
    iget-object v1, v0, LX/BcY;->h:LX/BcG;

    if-eqz v1, :cond_1

    .line 1802340
    iget-object v1, p1, LX/BcM;->b:LX/BcL;

    .line 1802341
    iget-boolean v2, p1, LX/BcM;->a:Z

    .line 1802342
    sget-object p0, LX/BcT;->a:[I

    invoke-virtual {v1}, LX/BcL;->ordinal()I

    move-result v1

    aget v1, p0, v1

    packed-switch v1, :pswitch_data_0

    .line 1802343
    :cond_1
    :goto_1
    :pswitch_0
    goto :goto_0

    .line 1802344
    :pswitch_1
    iget-object v1, v0, LX/BcY;->h:LX/BcG;

    invoke-interface {v1, v2}, LX/BcG;->b(Z)V

    goto :goto_1

    .line 1802345
    :pswitch_2
    iget-object v1, v0, LX/BcY;->h:LX/BcG;

    invoke-interface {v1}, LX/BcG;->a()V

    goto :goto_1

    .line 1802346
    :pswitch_3
    iget-object v1, v0, LX/BcY;->h:LX/BcG;

    invoke-interface {v1, v2}, LX/BcG;->a(Z)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
