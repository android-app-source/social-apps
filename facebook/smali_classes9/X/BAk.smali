.class public final LX/BAk;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel$IconModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:I

.field public e:I

.field public f:I

.field public g:Lcom/facebook/graphql/enums/GraphQLNotificationSeenFilter;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel$TitleModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1753795
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel;
    .locals 10

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    const/4 v9, 0x0

    .line 1753796
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1753797
    iget-object v1, p0, LX/BAk;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1753798
    iget-object v3, p0, LX/BAk;->b:Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;

    invoke-virtual {v0, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    .line 1753799
    iget-object v5, p0, LX/BAk;->c:Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel$IconModel;

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1753800
    iget-object v6, p0, LX/BAk;->g:Lcom/facebook/graphql/enums/GraphQLNotificationSeenFilter;

    invoke-virtual {v0, v6}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v6

    .line 1753801
    iget-object v7, p0, LX/BAk;->h:Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel$TitleModel;

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 1753802
    const/16 v8, 0x8

    invoke-virtual {v0, v8}, LX/186;->c(I)V

    .line 1753803
    invoke-virtual {v0, v9, v1}, LX/186;->b(II)V

    .line 1753804
    invoke-virtual {v0, v4, v3}, LX/186;->b(II)V

    .line 1753805
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 1753806
    const/4 v1, 0x3

    iget v3, p0, LX/BAk;->d:I

    invoke-virtual {v0, v1, v3, v9}, LX/186;->a(III)V

    .line 1753807
    const/4 v1, 0x4

    iget v3, p0, LX/BAk;->e:I

    invoke-virtual {v0, v1, v3, v9}, LX/186;->a(III)V

    .line 1753808
    const/4 v1, 0x5

    iget v3, p0, LX/BAk;->f:I

    invoke-virtual {v0, v1, v3, v9}, LX/186;->a(III)V

    .line 1753809
    const/4 v1, 0x6

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 1753810
    const/4 v1, 0x7

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 1753811
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 1753812
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1753813
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1753814
    invoke-virtual {v1, v9}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1753815
    new-instance v0, LX/15i;

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1753816
    new-instance v1, Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel;

    invoke-direct {v1, v0}, Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel;-><init>(LX/15i;)V

    .line 1753817
    return-object v1
.end method
