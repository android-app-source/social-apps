.class public final LX/BnK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/events/ui/themeselector/ThemeViewHolder;


# direct methods
.method public constructor <init>(Lcom/facebook/events/ui/themeselector/ThemeViewHolder;)V
    .locals 0

    .prologue
    .line 1820087
    iput-object p1, p0, LX/BnK;->a:Lcom/facebook/events/ui/themeselector/ThemeViewHolder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0x1c3410f9

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1820088
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 1820089
    const/high16 v0, 0x10000

    invoke-virtual {v2, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1820090
    const-string v0, "extra_selected_theme_id"

    iget-object v3, p0, LX/BnK;->a:Lcom/facebook/events/ui/themeselector/ThemeViewHolder;

    iget-object v3, v3, Lcom/facebook/events/ui/themeselector/ThemeViewHolder;->m:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1820091
    const-string v0, "extra_selected_theme_uri"

    iget-object v3, p0, LX/BnK;->a:Lcom/facebook/events/ui/themeselector/ThemeViewHolder;

    iget-object v3, v3, Lcom/facebook/events/ui/themeselector/ThemeViewHolder;->n:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1820092
    iget-object v0, p0, LX/BnK;->a:Lcom/facebook/events/ui/themeselector/ThemeViewHolder;

    iget-object v0, v0, Lcom/facebook/events/ui/themeselector/ThemeViewHolder;->l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v3, Landroid/app/Activity;

    invoke-static {v0, v3}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 1820093
    if-eqz v0, :cond_0

    .line 1820094
    const/4 v3, -0x1

    invoke-virtual {v0, v3, v2}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 1820095
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 1820096
    :cond_0
    const v0, 0x2fe177e5

    invoke-static {v4, v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
