.class public LX/C0A;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field public final a:LX/1qb;

.field public final b:LX/2sO;

.field public final c:LX/C0D;

.field public final d:LX/Ap5;


# direct methods
.method public constructor <init>(LX/1qb;LX/2sO;LX/C0D;LX/Ap5;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1840020
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1840021
    iput-object p1, p0, LX/C0A;->a:LX/1qb;

    .line 1840022
    iput-object p2, p0, LX/C0A;->b:LX/2sO;

    .line 1840023
    iput-object p3, p0, LX/C0A;->c:LX/C0D;

    .line 1840024
    iput-object p4, p0, LX/C0A;->d:LX/Ap5;

    .line 1840025
    return-void
.end method

.method public static a(LX/0QB;)LX/C0A;
    .locals 7

    .prologue
    .line 1840026
    const-class v1, LX/C0A;

    monitor-enter v1

    .line 1840027
    :try_start_0
    sget-object v0, LX/C0A;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1840028
    sput-object v2, LX/C0A;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1840029
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1840030
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1840031
    new-instance p0, LX/C0A;

    invoke-static {v0}, LX/1qb;->a(LX/0QB;)LX/1qb;

    move-result-object v3

    check-cast v3, LX/1qb;

    invoke-static {v0}, LX/2sO;->a(LX/0QB;)LX/2sO;

    move-result-object v4

    check-cast v4, LX/2sO;

    invoke-static {v0}, LX/C0D;->b(LX/0QB;)LX/C0D;

    move-result-object v5

    check-cast v5, LX/C0D;

    invoke-static {v0}, LX/Ap5;->a(LX/0QB;)LX/Ap5;

    move-result-object v6

    check-cast v6, LX/Ap5;

    invoke-direct {p0, v3, v4, v5, v6}, LX/C0A;-><init>(LX/1qb;LX/2sO;LX/C0D;LX/Ap5;)V

    .line 1840032
    move-object v0, p0

    .line 1840033
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1840034
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C0A;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1840035
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1840036
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
