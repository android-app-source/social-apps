.class public final LX/BFi;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/9bz;


# instance fields
.field public final synthetic a:LX/BFj;


# direct methods
.method public constructor <init>(LX/BFj;)V
    .locals 0

    .prologue
    .line 1766156
    iput-object p1, p0, LX/BFi;->a:LX/BFj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1766180
    return-void
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 1766179
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/photos/creativeediting/model/SwipeableParams;I)V
    .locals 3

    .prologue
    .line 1766161
    iget-object v0, p2, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->c:LX/5jI;

    move-object v0, v0

    .line 1766162
    sget-object v1, LX/5jI;->FRAME:LX/5jI;

    if-ne v0, v1, :cond_1

    .line 1766163
    iget-object v0, p0, LX/BFi;->a:LX/BFj;

    iget-object v0, v0, LX/BFj;->e:LX/9c7;

    iget-object v1, p0, LX/BFi;->a:LX/BFj;

    iget-object v1, v1, LX/BFj;->f:LX/89Z;

    .line 1766164
    iget-object v2, p2, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->b:Ljava/lang/String;

    move-object v2, v2

    .line 1766165
    new-instance p1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object p2, LX/9c4;->CREATIVECAM_FRAME_VIEWED:LX/9c4;

    invoke-virtual {p2}, LX/9c4;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p2, "composer"

    .line 1766166
    iput-object p2, p1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1766167
    move-object p1, p1

    .line 1766168
    sget-object p2, LX/9c5;->FILTER_NAME:LX/9c5;

    invoke-virtual {p2}, LX/9c5;->getParamKey()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    sget-object p2, LX/9c5;->CREATIVECAM_SOURCE:LX/9c5;

    invoke-virtual {p2}, LX/9c5;->getParamKey()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    .line 1766169
    invoke-static {v0, p1}, LX/9c7;->a(LX/9c7;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1766170
    iget-object v0, p0, LX/BFi;->a:LX/BFj;

    iget-object v0, v0, LX/BFj;->g:LX/B4P;

    if-eqz v0, :cond_1

    .line 1766171
    iget-object v0, p0, LX/BFi;->a:LX/BFj;

    iget-object v0, v0, LX/BFj;->g:LX/B4P;

    .line 1766172
    if-nez p3, :cond_2

    .line 1766173
    iget v1, v0, LX/B4P;->c:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, LX/B4P;->c:I

    .line 1766174
    :goto_0
    iget v1, v0, LX/B4P;->b:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    iget v1, v0, LX/B4P;->b:I

    if-eq v1, p3, :cond_0

    .line 1766175
    iget v1, v0, LX/B4P;->e:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, LX/B4P;->e:I

    .line 1766176
    :cond_0
    iput p3, v0, LX/B4P;->b:I

    .line 1766177
    :cond_1
    return-void

    .line 1766178
    :cond_2
    iget v1, v0, LX/B4P;->d:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, LX/B4P;->d:I

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 1766160
    return-void
.end method

.method public final c(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 1766159
    return-void
.end method

.method public final d(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 1766158
    return-void
.end method

.method public final e(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 1766157
    return-void
.end method
