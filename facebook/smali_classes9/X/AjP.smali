.class public LX/AjP;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/1My;

.field public final b:LX/0SG;

.field public c:LX/AjN;


# direct methods
.method public constructor <init>(LX/1My;LX/0SG;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1707900
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1707901
    const/4 v0, 0x0

    iput-object v0, p0, LX/AjP;->c:LX/AjN;

    .line 1707902
    iput-object p1, p0, LX/AjP;->a:LX/1My;

    .line 1707903
    iput-object p2, p0, LX/AjP;->b:LX/0SG;

    .line 1707904
    return-void
.end method

.method public static a(LX/0QB;)LX/AjP;
    .locals 1

    .prologue
    .line 1707905
    invoke-static {p0}, LX/AjP;->b(LX/0QB;)LX/AjP;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/AjP;
    .locals 3

    .prologue
    .line 1707906
    new-instance v2, LX/AjP;

    invoke-static {p0}, LX/1My;->b(LX/0QB;)LX/1My;

    move-result-object v0

    check-cast v0, LX/1My;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v1

    check-cast v1, LX/0SG;

    invoke-direct {v2, v0, v1}, LX/AjP;-><init>(LX/1My;LX/0SG;)V

    .line 1707907
    return-object v2
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1707908
    const/4 v0, 0x0

    iput-object v0, p0, LX/AjP;->c:LX/AjN;

    .line 1707909
    iget-object v0, p0, LX/AjP;->a:LX/1My;

    invoke-virtual {v0}, LX/1My;->a()V

    .line 1707910
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStory;Z)V
    .locals 9

    .prologue
    .line 1707911
    iget-object v0, p0, LX/AjP;->a:LX/1My;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1My;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1707912
    :goto_0
    return-void

    .line 1707913
    :cond_0
    invoke-static {p1}, LX/2vM;->a(Lcom/facebook/graphql/model/FeedUnit;)LX/0Rf;

    move-result-object v6

    .line 1707914
    iget-object v0, p0, LX/AjP;->a:LX/1My;

    new-instance v7, LX/AjO;

    invoke-direct {v7, p0, p1, p2}, LX/AjO;-><init>(LX/AjP;Lcom/facebook/graphql/model/GraphQLStory;Z)V

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v8

    new-instance v1, Lcom/facebook/graphql/executor/GraphQLResult;

    sget-object v3, LX/0ta;->FROM_CACHE_UP_TO_DATE:LX/0ta;

    const-wide/16 v4, 0x0

    move-object v2, p1

    invoke-direct/range {v1 .. v6}, Lcom/facebook/graphql/executor/GraphQLResult;-><init>(Ljava/lang/Object;LX/0ta;JLjava/util/Set;)V

    invoke-virtual {v0, v7, v8, v1}, LX/1My;->a(LX/0TF;Ljava/lang/String;Lcom/facebook/graphql/executor/GraphQLResult;)V

    .line 1707915
    iget-object v0, p0, LX/AjP;->a:LX/1My;

    invoke-virtual {v0}, LX/1My;->e()V

    .line 1707916
    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1707917
    iget-object v0, p0, LX/AjP;->a:LX/1My;

    invoke-virtual {v0}, LX/1My;->b()V

    .line 1707918
    return-void
.end method
