.class public LX/CPa;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/ReferenceSpec;
.end annotation


# static fields
.field public static final a:LX/CPX;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1884647
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    .line 1884648
    new-instance v0, LX/CPZ;

    invoke-direct {v0}, LX/CPZ;-><init>()V

    sput-object v0, LX/CPa;->a:LX/CPX;

    .line 1884649
    :goto_0
    return-void

    .line 1884650
    :cond_0
    new-instance v0, LX/CPY;

    invoke-direct {v0}, LX/CPY;-><init>()V

    sput-object v0, LX/CPa;->a:LX/CPX;

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1884651
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1884652
    return-void
.end method

.method public static b(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)Landroid/graphics/drawable/GradientDrawable;
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1884644
    new-instance v0, Landroid/graphics/drawable/GradientDrawable;

    invoke-direct {v0, p0, p1}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V

    .line 1884645
    invoke-virtual {v0, v1, v1}, Landroid/graphics/drawable/GradientDrawable;->setSize(II)V

    .line 1884646
    return-object v0
.end method
