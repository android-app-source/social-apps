.class public final enum LX/B76;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/B76;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/B76;

.field public static final enum FAILURE:LX/B76;

.field public static final enum SUCCESS:LX/B76;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1747707
    new-instance v0, LX/B76;

    const-string v1, "SUCCESS"

    invoke-direct {v0, v1, v2}, LX/B76;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/B76;->SUCCESS:LX/B76;

    new-instance v0, LX/B76;

    const-string v1, "FAILURE"

    invoke-direct {v0, v1, v3}, LX/B76;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/B76;->FAILURE:LX/B76;

    .line 1747708
    const/4 v0, 0x2

    new-array v0, v0, [LX/B76;

    sget-object v1, LX/B76;->SUCCESS:LX/B76;

    aput-object v1, v0, v2

    sget-object v1, LX/B76;->FAILURE:LX/B76;

    aput-object v1, v0, v3

    sput-object v0, LX/B76;->$VALUES:[LX/B76;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1747704
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/B76;
    .locals 1

    .prologue
    .line 1747706
    const-class v0, LX/B76;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/B76;

    return-object v0
.end method

.method public static values()[LX/B76;
    .locals 1

    .prologue
    .line 1747705
    sget-object v0, LX/B76;->$VALUES:[LX/B76;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/B76;

    return-object v0
.end method
