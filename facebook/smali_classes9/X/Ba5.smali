.class public LX/Ba5;
.super LX/BZz;
.source ""


# instance fields
.field public final a:LX/17T;

.field public final b:Lcom/facebook/content/SecureContextHelper;

.field public final c:Landroid/content/pm/PackageManager;


# direct methods
.method public constructor <init>(LX/17T;Lcom/facebook/content/SecureContextHelper;Landroid/content/pm/PackageManager;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1798471
    invoke-direct {p0}, LX/BZz;-><init>()V

    .line 1798472
    iput-object p1, p0, LX/Ba5;->a:LX/17T;

    .line 1798473
    iput-object p2, p0, LX/Ba5;->b:Lcom/facebook/content/SecureContextHelper;

    .line 1798474
    iput-object p3, p0, LX/Ba5;->c:Landroid/content/pm/PackageManager;

    .line 1798475
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;LX/2EJ;)V
    .locals 3

    .prologue
    .line 1798476
    const/4 v0, -0x1

    const v1, 0x7f08295b

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/Ba3;

    invoke-direct {v2, p0}, LX/Ba3;-><init>(LX/Ba5;)V

    invoke-virtual {p2, v0, v1, v2}, LX/2EJ;->a(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 1798477
    const/4 v0, -0x2

    const v1, 0x7f08295a

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/Ba4;

    invoke-direct {v2, p0}, LX/Ba4;-><init>(LX/Ba5;)V

    invoke-virtual {p2, v0, v1, v2}, LX/2EJ;->a(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 1798478
    return-void
.end method

.method public final a(Landroid/widget/ImageView;Landroid/widget/TextView;)V
    .locals 1

    .prologue
    .line 1798479
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1798480
    const v0, 0x7f082959

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setText(I)V

    .line 1798481
    return-void
.end method
