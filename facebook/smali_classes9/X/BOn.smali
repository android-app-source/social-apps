.class public LX/BOn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Closeable;


# instance fields
.field public final a:LX/1FJ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1FJ",
            "<",
            "Lcom/facebook/storyteller/AssetProvider4a;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Lcom/facebook/storyteller/ImageSimilarity4a;

.field private final c:Lcom/facebook/compactdisk/StoreManagerFactory;

.field private final d:LX/29a;

.field public final e:LX/BOo;

.field private final f:LX/BOm;

.field private g:Lcom/facebook/storyteller/StoryTeller$StorytellerHybrid;


# direct methods
.method public constructor <init>(Lcom/facebook/storyteller/AssetProvider4a;Lcom/facebook/storyteller/ImageSimilarity4a;Lcom/facebook/compactdisk/StoreManagerFactory;LX/29a;LX/BOo;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1780255
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1780256
    new-instance v0, LX/BOm;

    invoke-direct {v0, p0}, LX/BOm;-><init>(LX/BOn;)V

    iput-object v0, p0, LX/BOn;->f:LX/BOm;

    .line 1780257
    invoke-static {p1}, LX/1FJ;->a(Ljava/io/Closeable;)LX/1FJ;

    move-result-object v0

    iput-object v0, p0, LX/BOn;->a:LX/1FJ;

    .line 1780258
    iput-object p2, p0, LX/BOn;->b:Lcom/facebook/storyteller/ImageSimilarity4a;

    .line 1780259
    iput-object p3, p0, LX/BOn;->c:Lcom/facebook/compactdisk/StoreManagerFactory;

    .line 1780260
    iput-object p4, p0, LX/BOn;->d:LX/29a;

    .line 1780261
    iput-object p5, p0, LX/BOn;->e:LX/BOo;

    .line 1780262
    return-void
.end method

.method public static declared-synchronized c(LX/BOn;)Lcom/facebook/storyteller/StoryTeller$StorytellerHybrid;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1780263
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/BOn;->g:Lcom/facebook/storyteller/StoryTeller$StorytellerHybrid;

    if-nez v0, :cond_1

    .line 1780264
    iget-object v0, p0, LX/BOn;->e:LX/BOo;

    .line 1780265
    iget-object v2, v0, LX/BOo;->f:LX/0ad;

    sget-short v3, LX/1kO;->q:S

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, LX/0ad;->a(SZ)Z

    move-result v2

    move v0, v2

    .line 1780266
    if-eqz v0, :cond_2

    .line 1780267
    iget-object v0, p0, LX/BOn;->c:Lcom/facebook/compactdisk/StoreManagerFactory;

    const-string v2, "diskstoremanager_fb4a"

    invoke-virtual {v0, v2}, Lcom/facebook/compactdisk/StoreManagerFactory;->a(Ljava/lang/String;)Lcom/facebook/compactdisk/StoreManager;

    move-result-object v0

    .line 1780268
    const-string v2, "storyteller-pkvs"

    invoke-virtual {v0, v2}, Lcom/facebook/compactdisk/StoreManager;->a(Ljava/lang/String;)Lcom/facebook/compactdisk/PersistentKeyValueStore;

    move-result-object v0

    move-object v2, v0

    .line 1780269
    :goto_0
    new-instance v0, Lcom/facebook/storyteller/StoryTeller$StorytellerHybrid;

    iget-object v3, p0, LX/BOn;->e:LX/BOo;

    .line 1780270
    iget-object v4, v3, LX/BOo;->f:LX/0ad;

    sget-short v5, LX/1kO;->S:S

    const/4 v6, 0x1

    invoke-interface {v4, v5, v6}, LX/0ad;->a(SZ)Z

    move-result v4

    move v3, v4

    .line 1780271
    if-eqz v3, :cond_0

    iget-object v1, p0, LX/BOn;->d:LX/29a;

    .line 1780272
    iget-object v3, v1, LX/29a;->c:Lcom/facebook/xanalytics/XAnalyticsNative;

    move-object v1, v3

    .line 1780273
    :cond_0
    invoke-direct {v0, v2, v1}, Lcom/facebook/storyteller/StoryTeller$StorytellerHybrid;-><init>(Lcom/facebook/compactdisk/PersistentKeyValueStore;Lcom/facebook/xanalytics/XAnalyticsNative;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1780274
    :goto_1
    monitor-exit p0

    return-object v0

    :cond_1
    :try_start_1
    iget-object v0, p0, LX/BOn;->g:Lcom/facebook/storyteller/StoryTeller$StorytellerHybrid;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 1780275
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    move-object v2, v1

    goto :goto_0
.end method

.method public static d()LX/BOy;
    .locals 2

    .prologue
    .line 1780276
    new-instance v0, LX/0eX;

    const/16 v1, 0x100

    invoke-direct {v0, v1}, LX/0eX;-><init>(I)V

    .line 1780277
    invoke-static {v0}, LX/BOy;->a(LX/0eX;)V

    .line 1780278
    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/BOy;->a(LX/0eX;B)V

    .line 1780279
    invoke-static {v0}, LX/BOy;->b(LX/0eX;)I

    move-result v1

    .line 1780280
    invoke-virtual {v0, v1}, LX/0eX;->c(I)V

    .line 1780281
    iget-object v1, v0, LX/0eX;->a:Ljava/nio/ByteBuffer;

    move-object v0, v1

    .line 1780282
    invoke-static {v0}, LX/BOy;->a(Ljava/nio/ByteBuffer;)LX/BOy;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()LX/BOy;
    .locals 7

    .prologue
    .line 1780283
    iget-object v0, p0, LX/BOn;->e:LX/BOo;

    .line 1780284
    iget-object v1, v0, LX/BOo;->f:LX/0ad;

    sget-short v2, LX/1kO;->R:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    move v0, v1

    .line 1780285
    if-eqz v0, :cond_0

    .line 1780286
    iget-object v0, p0, LX/BOn;->f:LX/BOm;

    .line 1780287
    iget v1, v0, LX/BOm;->b:I

    if-ltz v1, :cond_6

    .line 1780288
    iget v1, v0, LX/BOm;->b:I

    iget-object v2, v0, LX/BOm;->a:[[B

    array-length v2, v2

    if-lt v1, v2, :cond_5

    .line 1780289
    invoke-static {}, LX/BOn;->d()LX/BOy;

    move-result-object v1

    .line 1780290
    :goto_0
    move-object v0, v1

    .line 1780291
    :goto_1
    return-object v0

    .line 1780292
    :cond_0
    iget-object v0, p0, LX/BOn;->e:LX/BOo;

    invoke-virtual {v0}, LX/BOo;->a()[B

    move-result-object v1

    .line 1780293
    invoke-static {v1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/BOu;->a(Ljava/nio/ByteBuffer;)LX/BOu;

    move-result-object v0

    .line 1780294
    invoke-static {p0}, LX/BOn;->c(LX/BOn;)Lcom/facebook/storyteller/StoryTeller$StorytellerHybrid;

    move-result-object v2

    .line 1780295
    const/4 v3, 0x0

    .line 1780296
    const/16 v4, 0xc

    invoke-virtual {v0, v4}, LX/0eW;->a(I)I

    move-result v4

    if-eqz v4, :cond_1

    iget-object v5, v0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v6, v0, LX/0eW;->a:I

    add-int/2addr v4, v6

    invoke-virtual {v5, v4}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v4

    if-eqz v4, :cond_1

    const/4 v3, 0x1

    :cond_1
    move v3, v3

    .line 1780297
    if-eqz v3, :cond_3

    .line 1780298
    new-instance v3, LX/BOr;

    invoke-direct {v3}, LX/BOr;-><init>()V

    .line 1780299
    const/16 v4, 0xe

    invoke-virtual {v0, v4}, LX/0eW;->a(I)I

    move-result v4

    if-eqz v4, :cond_a

    iget v5, v0, LX/0eW;->a:I

    add-int/2addr v4, v5

    invoke-virtual {v0, v4}, LX/0eW;->b(I)I

    move-result v4

    iget-object v5, v0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    .line 1780300
    iput v4, v3, LX/BOr;->a:I

    iput-object v5, v3, LX/BOr;->b:Ljava/nio/ByteBuffer;

    move-object v4, v3

    .line 1780301
    :goto_2
    move-object v3, v4

    .line 1780302
    move-object v0, v3

    .line 1780303
    const/4 v3, 0x0

    .line 1780304
    const/4 v4, 0x4

    invoke-virtual {v0, v4}, LX/0eW;->a(I)I

    move-result v4

    if-eqz v4, :cond_2

    iget-object v5, v0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v6, v0, LX/0eW;->a:I

    add-int/2addr v4, v6

    invoke-virtual {v5, v4}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v4

    if-eqz v4, :cond_2

    const/4 v3, 0x1

    :cond_2
    move v0, v3

    .line 1780305
    if-eqz v0, :cond_3

    .line 1780306
    invoke-static {}, LX/BOn;->d()LX/BOy;

    move-result-object v0

    goto :goto_1

    .line 1780307
    :cond_3
    iget-object v0, p0, LX/BOn;->a:LX/1FJ;

    invoke-virtual {v0}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/storyteller/AssetProvider4a;

    iget-object v3, p0, LX/BOn;->b:Lcom/facebook/storyteller/ImageSimilarity4a;

    invoke-virtual {v2, v0, v3, v1}, Lcom/facebook/storyteller/StoryTeller$StorytellerHybrid;->nextCluster(Lcom/facebook/storyteller/AssetProvider4a;Lcom/facebook/storyteller/ImageSimilarity4a;[B)[B

    move-result-object v0

    .line 1780308
    if-nez v0, :cond_4

    .line 1780309
    invoke-static {}, LX/BOn;->d()LX/BOy;

    move-result-object v0

    goto :goto_1

    .line 1780310
    :cond_4
    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/BOy;->a(Ljava/nio/ByteBuffer;)LX/BOy;

    move-result-object v0

    goto/16 :goto_1

    .line 1780311
    :cond_5
    iget-object v1, v0, LX/BOm;->a:[[B

    iget v2, v0, LX/BOm;->b:I

    add-int/lit8 v3, v2, 0x1

    iput v3, v0, LX/BOm;->b:I

    aget-object v1, v1, v2

    invoke-static {v1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-static {v1}, LX/BOy;->a(Ljava/nio/ByteBuffer;)LX/BOy;

    move-result-object v1

    goto/16 :goto_0

    .line 1780312
    :cond_6
    iget-object v1, v0, LX/BOm;->c:LX/BOn;

    iget-object v1, v1, LX/BOn;->a:LX/1FJ;

    invoke-virtual {v1}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/storyteller/AssetProvider4a;

    invoke-virtual {v1}, Lcom/facebook/storyteller/AssetProvider4a;->a()Ljava/util/List;

    move-result-object v1

    .line 1780313
    if-nez v1, :cond_7

    .line 1780314
    invoke-static {}, LX/BOn;->d()LX/BOy;

    move-result-object v1

    goto/16 :goto_0

    .line 1780315
    :cond_7
    iget-object v2, v0, LX/BOm;->c:LX/BOn;

    invoke-static {v2}, LX/BOn;->c(LX/BOn;)Lcom/facebook/storyteller/StoryTeller$StorytellerHybrid;

    move-result-object v2

    .line 1780316
    invoke-static {v1}, LX/BOl;->a(Ljava/util/List;)[B

    move-result-object v1

    iget-object v3, v0, LX/BOm;->c:LX/BOn;

    iget-object v3, v3, LX/BOn;->b:Lcom/facebook/storyteller/ImageSimilarity4a;

    iget-object p0, v0, LX/BOm;->c:LX/BOn;

    iget-object p0, p0, LX/BOn;->e:LX/BOo;

    invoke-virtual {p0}, LX/BOo;->a()[B

    move-result-object p0

    invoke-virtual {v2, v1, v3, p0}, Lcom/facebook/storyteller/StoryTeller$StorytellerHybrid;->allClustersFromAssets([BLcom/facebook/storyteller/ImageSimilarity4a;[B)[[B

    move-result-object v1

    iput-object v1, v0, LX/BOm;->a:[[B

    .line 1780317
    iget-object v1, v0, LX/BOm;->a:[[B

    if-eqz v1, :cond_8

    iget-object v1, v0, LX/BOm;->a:[[B

    array-length v1, v1

    if-nez v1, :cond_9

    .line 1780318
    :cond_8
    invoke-static {}, LX/BOn;->d()LX/BOy;

    move-result-object v1

    goto/16 :goto_0

    .line 1780319
    :cond_9
    const/4 v1, 0x0

    iput v1, v0, LX/BOm;->b:I

    .line 1780320
    iget-object v1, v0, LX/BOm;->a:[[B

    iget v2, v0, LX/BOm;->b:I

    add-int/lit8 v3, v2, 0x1

    iput v3, v0, LX/BOm;->b:I

    aget-object v1, v1, v2

    invoke-static {v1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-static {v1}, LX/BOy;->a(Ljava/nio/ByteBuffer;)LX/BOy;

    move-result-object v1

    goto/16 :goto_0

    :cond_a
    const/4 v4, 0x0

    goto/16 :goto_2
.end method

.method public final close()V
    .locals 1

    .prologue
    .line 1780321
    iget-object v0, p0, LX/BOn;->a:LX/1FJ;

    invoke-virtual {v0}, LX/1FJ;->close()V

    .line 1780322
    return-void
.end method
