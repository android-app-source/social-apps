.class public final enum LX/Cdj;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Cdj;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Cdj;

.field public static final enum HIGH:LX/Cdj;

.field public static final enum LOW:LX/Cdj;

.field public static final enum MEDIUM:LX/Cdj;

.field public static final enum UNKNOWN:LX/Cdj;


# instance fields
.field private final mLevel:I


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 1923187
    new-instance v0, LX/Cdj;

    const-string v1, "LOW"

    invoke-direct {v0, v1, v6, v3}, LX/Cdj;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/Cdj;->LOW:LX/Cdj;

    .line 1923188
    new-instance v0, LX/Cdj;

    const-string v1, "MEDIUM"

    invoke-direct {v0, v1, v3, v4}, LX/Cdj;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/Cdj;->MEDIUM:LX/Cdj;

    .line 1923189
    new-instance v0, LX/Cdj;

    const-string v1, "HIGH"

    invoke-direct {v0, v1, v4, v5}, LX/Cdj;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/Cdj;->HIGH:LX/Cdj;

    .line 1923190
    new-instance v0, LX/Cdj;

    const-string v1, "UNKNOWN"

    const/4 v2, -0x1

    invoke-direct {v0, v1, v5, v2}, LX/Cdj;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/Cdj;->UNKNOWN:LX/Cdj;

    .line 1923191
    const/4 v0, 0x4

    new-array v0, v0, [LX/Cdj;

    sget-object v1, LX/Cdj;->LOW:LX/Cdj;

    aput-object v1, v0, v6

    sget-object v1, LX/Cdj;->MEDIUM:LX/Cdj;

    aput-object v1, v0, v3

    sget-object v1, LX/Cdj;->HIGH:LX/Cdj;

    aput-object v1, v0, v4

    sget-object v1, LX/Cdj;->UNKNOWN:LX/Cdj;

    aput-object v1, v0, v5

    sput-object v0, LX/Cdj;->$VALUES:[LX/Cdj;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 1923184
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1923185
    iput p3, p0, LX/Cdj;->mLevel:I

    .line 1923186
    return-void
.end method

.method public static from(Lcom/facebook/graphql/enums/GraphQLBeaconScanResultConfidenceLevel;)LX/Cdj;
    .locals 2
    .param p0    # Lcom/facebook/graphql/enums/GraphQLBeaconScanResultConfidenceLevel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1923176
    if-nez p0, :cond_0

    .line 1923177
    sget-object v0, LX/Cdj;->UNKNOWN:LX/Cdj;

    .line 1923178
    :goto_0
    return-object v0

    .line 1923179
    :cond_0
    sget-object v0, LX/Cdi;->a:[I

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLBeaconScanResultConfidenceLevel;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1923180
    sget-object v0, LX/Cdj;->UNKNOWN:LX/Cdj;

    goto :goto_0

    .line 1923181
    :pswitch_0
    sget-object v0, LX/Cdj;->LOW:LX/Cdj;

    goto :goto_0

    .line 1923182
    :pswitch_1
    sget-object v0, LX/Cdj;->MEDIUM:LX/Cdj;

    goto :goto_0

    .line 1923183
    :pswitch_2
    sget-object v0, LX/Cdj;->HIGH:LX/Cdj;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static from(Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionConfidenceLevel;)LX/Cdj;
    .locals 2
    .param p0    # Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionConfidenceLevel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1923168
    if-nez p0, :cond_0

    .line 1923169
    sget-object v0, LX/Cdj;->UNKNOWN:LX/Cdj;

    .line 1923170
    :goto_0
    return-object v0

    .line 1923171
    :cond_0
    sget-object v0, LX/Cdi;->b:[I

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLPlaceSuggestionConfidenceLevel;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1923172
    sget-object v0, LX/Cdj;->UNKNOWN:LX/Cdj;

    goto :goto_0

    .line 1923173
    :pswitch_0
    sget-object v0, LX/Cdj;->LOW:LX/Cdj;

    goto :goto_0

    .line 1923174
    :pswitch_1
    sget-object v0, LX/Cdj;->MEDIUM:LX/Cdj;

    goto :goto_0

    .line 1923175
    :pswitch_2
    sget-object v0, LX/Cdj;->HIGH:LX/Cdj;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)LX/Cdj;
    .locals 1

    .prologue
    .line 1923167
    const-class v0, LX/Cdj;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Cdj;

    return-object v0
.end method

.method public static values()[LX/Cdj;
    .locals 1

    .prologue
    .line 1923166
    sget-object v0, LX/Cdj;->$VALUES:[LX/Cdj;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Cdj;

    return-object v0
.end method


# virtual methods
.method public final isEqualOrGreaterThan(LX/Cdj;)Z
    .locals 2

    .prologue
    .line 1923165
    iget v0, p0, LX/Cdj;->mLevel:I

    iget v1, p1, LX/Cdj;->mLevel:I

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
