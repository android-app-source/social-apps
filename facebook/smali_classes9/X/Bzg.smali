.class public LX/Bzg;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pm;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/35m;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Bzg",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/35m;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1839085
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1839086
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/Bzg;->b:LX/0Zi;

    .line 1839087
    iput-object p1, p0, LX/Bzg;->a:LX/0Ot;

    .line 1839088
    return-void
.end method

.method public static a(LX/0QB;)LX/Bzg;
    .locals 4

    .prologue
    .line 1839074
    const-class v1, LX/Bzg;

    monitor-enter v1

    .line 1839075
    :try_start_0
    sget-object v0, LX/Bzg;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1839076
    sput-object v2, LX/Bzg;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1839077
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1839078
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1839079
    new-instance v3, LX/Bzg;

    const/16 p0, 0x808

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/Bzg;-><init>(LX/0Ot;)V

    .line 1839080
    move-object v0, v3

    .line 1839081
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1839082
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Bzg;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1839083
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1839084
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 5

    .prologue
    .line 1839048
    check-cast p2, LX/Bzf;

    .line 1839049
    iget-object v0, p0, LX/Bzg;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/35m;

    iget-object v1, p2, LX/Bzf;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/Bzf;->b:LX/1Pm;

    .line 1839050
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 1839051
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1839052
    iget-object v4, v0, LX/35m;->a:LX/Ap5;

    invoke-virtual {v4, p1}, LX/Ap5;->c(LX/1De;)LX/Ap4;

    move-result-object v4

    const/4 p0, 0x2

    invoke-virtual {v4, p0}, LX/Ap4;->h(I)LX/Ap4;

    move-result-object v4

    invoke-virtual {v4, v2}, LX/Ap4;->a(LX/1Po;)LX/Ap4;

    move-result-object v4

    const/4 p0, 0x0

    .line 1839053
    if-nez v3, :cond_1

    .line 1839054
    :cond_0
    :goto_0
    move-object p0, p0

    .line 1839055
    invoke-virtual {v4, p0}, LX/Ap4;->a(Landroid/net/Uri;)LX/Ap4;

    move-result-object v4

    iget-object p0, v0, LX/35m;->c:LX/1qb;

    invoke-virtual {p0, v3}, LX/1qb;->c(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Landroid/text/SpannableStringBuilder;

    move-result-object p0

    invoke-virtual {v4, p0}, LX/Ap4;->a(Ljava/lang/CharSequence;)LX/Ap4;

    move-result-object v4

    invoke-static {v3}, LX/1qb;->f(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v4, p0}, LX/Ap4;->b(Ljava/lang/CharSequence;)LX/Ap4;

    move-result-object v4

    invoke-static {v3}, LX/1qb;->d(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Landroid/text/Spannable;

    move-result-object v3

    invoke-virtual {v4, v3}, LX/Ap4;->c(Ljava/lang/CharSequence;)LX/Ap4;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, LX/Ap4;->i(I)LX/Ap4;

    move-result-object v3

    .line 1839056
    const v4, 0xe6574d9

    const/4 p0, 0x0

    invoke-static {p1, v4, p0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v4

    move-object v4, v4

    .line 1839057
    invoke-virtual {v3, v4}, LX/Ap4;->a(LX/1dQ;)LX/Ap4;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->b()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 1839058
    return-object v0

    .line 1839059
    :cond_1
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object p2

    .line 1839060
    if-eqz p2, :cond_0

    .line 1839061
    iget-object v1, v0, LX/35m;->e:Landroid/content/res/Resources;

    const v2, 0x7f0b116b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 1839062
    invoke-static {p2, v1}, LX/1qa;->a(Lcom/facebook/graphql/model/GraphQLMedia;I)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object p2

    .line 1839063
    if-eqz p2, :cond_0

    invoke-static {p2}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object p0

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1839064
    invoke-static {}, LX/1dS;->b()V

    .line 1839065
    iget v0, p1, LX/1dQ;->b:I

    .line 1839066
    packed-switch v0, :pswitch_data_0

    .line 1839067
    :goto_0
    return-object v2

    .line 1839068
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 1839069
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1839070
    check-cast v1, LX/Bzf;

    .line 1839071
    iget-object v3, p0, LX/Bzg;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/35m;

    iget-object p1, v1, LX/Bzf;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object p2, v1, LX/Bzf;->b:LX/1Pm;

    .line 1839072
    iget-object p0, v3, LX/35m;->b:LX/C0D;

    invoke-virtual {p0, v0, p1, p2}, LX/C0D;->a(Landroid/view/View;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pq;)V

    .line 1839073
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0xe6574d9
        :pswitch_0
    .end packed-switch
.end method
