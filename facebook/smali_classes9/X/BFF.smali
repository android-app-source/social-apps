.class public final LX/BFF;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0Ve;

.field public final synthetic b:LX/BFI;


# direct methods
.method public constructor <init>(LX/BFI;LX/0Ve;)V
    .locals 0

    .prologue
    .line 1765490
    iput-object p1, p0, LX/BFF;->b:LX/BFI;

    iput-object p2, p0, LX/BFF;->a:LX/0Ve;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1765491
    iget-object v0, p0, LX/BFF;->a:LX/0Ve;

    invoke-interface {v0, p1}, LX/0TF;->onFailure(Ljava/lang/Throwable;)V

    .line 1765492
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 1765493
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1765494
    if-eqz p1, :cond_0

    .line 1765495
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1765496
    if-nez v0, :cond_1

    .line 1765497
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/BFF;->onNonCancellationFailure(Ljava/lang/Throwable;)V

    .line 1765498
    :goto_0
    return-void

    .line 1765499
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1765500
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1765501
    iget-object v1, p0, LX/BFF;->a:LX/0Ve;

    invoke-interface {v1, v0}, LX/0TF;->onSuccess(Ljava/lang/Object;)V

    goto :goto_0
.end method
