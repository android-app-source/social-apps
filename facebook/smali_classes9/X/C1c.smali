.class public final LX/C1c;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/157;


# instance fields
.field public final synthetic a:LX/C1d;


# direct methods
.method public constructor <init>(LX/C1d;)V
    .locals 0

    .prologue
    .line 1842580
    iput-object p1, p0, LX/C1c;->a:LX/C1d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1842581
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$LiveVideoBroadcastStatusUpdateStoryFragmentModel;)V
    .locals 7
    .param p3    # Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$LiveVideoBroadcastStatusUpdateStoryFragmentModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1842582
    if-nez p3, :cond_1

    .line 1842583
    :cond_0
    :goto_0
    return-void

    .line 1842584
    :cond_1
    iget-object v0, p0, LX/C1c;->a:LX/C1d;

    iget-object v0, v0, LX/C1d;->i:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1842585
    if-eqz v2, :cond_0

    .line 1842586
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->SCHEDULED_LIVE:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-eq p2, v0, :cond_0

    .line 1842587
    iget-object v0, p0, LX/C1c;->a:LX/C1d;

    iget-object v6, v0, LX/C1d;->f:LX/0Sh;

    new-instance v0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentBroadcastStatusManager$ScheduledLiveAttachmentBroadcastStatusUpdateListener$1;

    move-object v1, p0

    move-object v3, p2

    move-object v4, p3

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentBroadcastStatusManager$ScheduledLiveAttachmentBroadcastStatusUpdateListener$1;-><init>(LX/C1c;Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$LiveVideoBroadcastStatusUpdateStoryFragmentModel;Ljava/lang/String;)V

    invoke-virtual {v6, v0}, LX/0Sh;->b(Ljava/lang/Runnable;)V

    goto :goto_0
.end method
