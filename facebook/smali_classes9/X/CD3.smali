.class public LX/CD3;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/C2F;

.field private final b:LX/121;

.field private final c:Landroid/content/Context;

.field public d:Z

.field private e:Landroid/support/v4/app/FragmentActivity;

.field public f:I


# direct methods
.method public constructor <init>(Landroid/support/v4/app/FragmentActivity;LX/C2F;LX/121;Landroid/content/Context;)V
    .locals 3
    .param p2    # LX/C2F;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Landroid/content/Context;
        .annotation build Lcom/facebook/inject/ForAppContext;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1858639
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1858640
    iput-object p1, p0, LX/CD3;->e:Landroid/support/v4/app/FragmentActivity;

    .line 1858641
    iput-object p2, p0, LX/CD3;->a:LX/C2F;

    .line 1858642
    iget-object v0, p0, LX/CD3;->a:LX/C2F;

    .line 1858643
    sget-object v1, LX/04D;->FEED:LX/04D;

    new-instance v2, LX/C2B;

    invoke-direct {v2}, LX/C2B;-><init>()V

    invoke-static {v0, v1, v2}, LX/C2F;->a(LX/C2F;LX/04D;LX/C2B;)V

    .line 1858644
    iput-object p3, p0, LX/CD3;->b:LX/121;

    .line 1858645
    iput-object p4, p0, LX/CD3;->c:Landroid/content/Context;

    .line 1858646
    iget-object v0, p0, LX/CD3;->a:LX/C2F;

    new-instance v1, LX/CD2;

    invoke-direct {v1, p0}, LX/CD2;-><init>(LX/CD3;)V

    invoke-virtual {v0, v1}, LX/C2F;->a(LX/3J0;)V

    .line 1858647
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 1858648
    iget-boolean v0, p0, LX/CD3;->d:Z

    if-eqz v0, :cond_0

    .line 1858649
    :goto_0
    return-void

    .line 1858650
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/CD3;->d:Z

    .line 1858651
    iget-object v0, p0, LX/CD3;->b:LX/121;

    sget-object v1, LX/0yY;->VIDEO_PLAY_INTERSTITIAL:LX/0yY;

    iget-object v2, p0, LX/CD3;->c:Landroid/content/Context;

    const v3, 0x7f080e4a

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, LX/CD1;

    invoke-direct {v3, p0, p1}, LX/CD1;-><init>(LX/CD3;Landroid/view/View;)V

    invoke-virtual {v0, v1, v2, v3}, LX/121;->a(LX/0yY;Ljava/lang/String;LX/39A;)LX/121;

    .line 1858652
    iget-object v0, p0, LX/CD3;->b:LX/121;

    sget-object v1, LX/0yY;->VIDEO_PLAY_INTERSTITIAL:LX/0yY;

    iget-object v2, p0, LX/CD3;->e:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/121;->a(LX/0yY;LX/0gc;)V

    goto :goto_0
.end method
