.class public final LX/C38;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/C39;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/C33;

.field public b:LX/1Pb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public c:I

.field public final synthetic d:LX/C39;


# direct methods
.method public constructor <init>(LX/C39;)V
    .locals 1

    .prologue
    .line 1845104
    iput-object p1, p0, LX/C38;->d:LX/C39;

    .line 1845105
    move-object v0, p1

    .line 1845106
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1845107
    const/4 v0, 0x4

    iput v0, p0, LX/C38;->c:I

    .line 1845108
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1845109
    const-string v0, "CondensedStoryBodyComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1845110
    if-ne p0, p1, :cond_1

    .line 1845111
    :cond_0
    :goto_0
    return v0

    .line 1845112
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1845113
    goto :goto_0

    .line 1845114
    :cond_3
    check-cast p1, LX/C38;

    .line 1845115
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1845116
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1845117
    if-eq v2, v3, :cond_0

    .line 1845118
    iget-object v2, p0, LX/C38;->a:LX/C33;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/C38;->a:LX/C33;

    iget-object v3, p1, LX/C38;->a:LX/C33;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1845119
    goto :goto_0

    .line 1845120
    :cond_5
    iget-object v2, p1, LX/C38;->a:LX/C33;

    if-nez v2, :cond_4

    .line 1845121
    :cond_6
    iget-object v2, p0, LX/C38;->b:LX/1Pb;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/C38;->b:LX/1Pb;

    iget-object v3, p1, LX/C38;->b:LX/1Pb;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1845122
    goto :goto_0

    .line 1845123
    :cond_8
    iget-object v2, p1, LX/C38;->b:LX/1Pb;

    if-nez v2, :cond_7

    .line 1845124
    :cond_9
    iget v2, p0, LX/C38;->c:I

    iget v3, p1, LX/C38;->c:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 1845125
    goto :goto_0
.end method
