.class public LX/BUz;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x11
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static final j:J


# instance fields
.field private b:Landroid/net/Uri;

.field private c:LX/7Sv;

.field public d:Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;

.field public e:I

.field private f:I

.field private g:J

.field private h:J

.field private i:J

.field public k:Z

.field private l:Z

.field private m:Z

.field private n:Ljava/util/concurrent/ExecutorService;

.field private o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/61B;",
            ">;"
        }
    .end annotation
.end field

.field private p:LX/0SG;

.field private q:LX/03V;

.field public r:Landroid/media/MediaCodec;

.field public s:LX/BUx;

.field public t:Landroid/media/MediaExtractor;

.field private u:LX/BUy;

.field private v:LX/2MV;

.field private w:LX/60x;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1790074
    const-class v0, LX/BUz;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/BUz;->a:Ljava/lang/String;

    .line 1790075
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, "SAMSUNG-SGH-I747"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const-wide/32 v0, 0xc350

    :goto_0
    sput-wide v0, LX/BUz;->j:J

    return-void

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/net/Uri;Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;Ljava/util/List;LX/7Sv;Ljava/util/concurrent/ExecutorService;LX/0SG;LX/BUy;LX/2MV;LX/0Or;)V
    .locals 4
    .param p1    # Landroid/net/Uri;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Ljava/util/List;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/7Sv;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Lcom/facebook/video/scrubber/GLFrameRetriever$FrameRetrieverDelegate;",
            "Ljava/util/List",
            "<",
            "LX/61B;",
            ">;",
            "LX/7Sv;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/0SG;",
            "LX/BUy;",
            "LX/2MV;",
            "LX/0Or",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const-wide/16 v2, -0x1

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 1790053
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1790054
    iput v0, p0, LX/BUz;->f:I

    .line 1790055
    iput-wide v2, p0, LX/BUz;->g:J

    .line 1790056
    iput-wide v2, p0, LX/BUz;->h:J

    .line 1790057
    iput-wide v2, p0, LX/BUz;->i:J

    .line 1790058
    iput-boolean v0, p0, LX/BUz;->k:Z

    .line 1790059
    iput-boolean v0, p0, LX/BUz;->l:Z

    .line 1790060
    iput-boolean v0, p0, LX/BUz;->m:Z

    .line 1790061
    iput-object v1, p0, LX/BUz;->r:Landroid/media/MediaCodec;

    .line 1790062
    iput-object v1, p0, LX/BUz;->s:LX/BUx;

    .line 1790063
    iput-object v1, p0, LX/BUz;->t:Landroid/media/MediaExtractor;

    .line 1790064
    iput-object p3, p0, LX/BUz;->o:Ljava/util/List;

    .line 1790065
    iput-object p1, p0, LX/BUz;->b:Landroid/net/Uri;

    .line 1790066
    iput-object p2, p0, LX/BUz;->d:Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;

    .line 1790067
    iput-object p4, p0, LX/BUz;->c:LX/7Sv;

    .line 1790068
    iput-object p6, p0, LX/BUz;->p:LX/0SG;

    .line 1790069
    iput-object p5, p0, LX/BUz;->n:Ljava/util/concurrent/ExecutorService;

    .line 1790070
    iput-object p8, p0, LX/BUz;->v:LX/2MV;

    .line 1790071
    iput-object p7, p0, LX/BUz;->u:LX/BUy;

    .line 1790072
    invoke-interface {p9}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    iput-object v0, p0, LX/BUz;->q:LX/03V;

    .line 1790073
    return-void
.end method

.method private static a(LX/BUz;Landroid/media/MediaCodec$BufferInfo;J)I
    .locals 10

    .prologue
    const-wide/16 v8, 0x2710

    const/4 v3, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1790034
    iget-object v0, p0, LX/BUz;->r:Landroid/media/MediaCodec;

    invoke-virtual {v0, p1, v8, v9}, Landroid/media/MediaCodec;->dequeueOutputBuffer(Landroid/media/MediaCodec$BufferInfo;J)I

    move-result v4

    .line 1790035
    const/4 v0, -0x1

    if-eq v4, v0, :cond_0

    .line 1790036
    const/4 v0, -0x3

    if-eq v4, v0, :cond_0

    .line 1790037
    const/4 v0, -0x2

    if-ne v4, v0, :cond_2

    .line 1790038
    iget-object v0, p0, LX/BUz;->r:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->getOutputFormat()Landroid/media/MediaFormat;

    move-result-object v0

    .line 1790039
    new-array v1, v1, [Ljava/lang/Object;

    aput-object v0, v1, v2

    :cond_0
    move v2, v3

    .line 1790040
    :cond_1
    :goto_0
    return v2

    .line 1790041
    :cond_2
    if-gez v4, :cond_3

    .line 1790042
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unexpected result from decoder.dequeueOutputBuffer: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1790043
    :cond_3
    iput-boolean v1, p0, LX/BUz;->l:Z

    .line 1790044
    new-array v0, v3, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v0, v2

    iget v5, p1, Landroid/media/MediaCodec$BufferInfo;->size:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v0, v1

    .line 1790045
    iget v0, p1, Landroid/media/MediaCodec$BufferInfo;->flags:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_7

    move v0, v1

    .line 1790046
    :goto_1
    iget-wide v6, p0, LX/BUz;->g:J

    sub-long v6, p2, v6

    cmp-long v5, v6, v8

    if-lez v5, :cond_4

    if-eqz v0, :cond_6

    :cond_4
    move v0, v1

    .line 1790047
    :goto_2
    if-nez v0, :cond_5

    .line 1790048
    new-array v3, v3, [Ljava/lang/Object;

    iget v5, p1, Landroid/media/MediaCodec$BufferInfo;->size:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v2

    iget-wide v6, p0, LX/BUz;->g:J

    sub-long v6, p2, v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v1

    .line 1790049
    :cond_5
    iget-object v1, p0, LX/BUz;->r:Landroid/media/MediaCodec;

    invoke-virtual {v1, v4, v0}, Landroid/media/MediaCodec;->releaseOutputBuffer(IZ)V

    .line 1790050
    if-eqz v0, :cond_1

    .line 1790051
    const/4 v2, 0x3

    goto :goto_0

    :cond_6
    move v0, v2

    .line 1790052
    goto :goto_2

    :cond_7
    move v0, v2

    goto :goto_1
.end method

.method private static a(LX/BUz;[Ljava/nio/ByteBuffer;J)I
    .locals 12

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v2, 0x0

    .line 1790015
    iget-object v0, p0, LX/BUz;->r:Landroid/media/MediaCodec;

    const-wide/16 v4, 0x2710

    invoke-virtual {v0, v4, v5}, Landroid/media/MediaCodec;->dequeueInputBuffer(J)I

    move-result v1

    .line 1790016
    if-ltz v1, :cond_2

    .line 1790017
    iput-boolean v7, p0, LX/BUz;->l:Z

    .line 1790018
    aget-object v0, p1, v1

    .line 1790019
    iget-object v3, p0, LX/BUz;->t:Landroid/media/MediaExtractor;

    invoke-virtual {v3, v0, v2}, Landroid/media/MediaExtractor;->readSampleData(Ljava/nio/ByteBuffer;I)I

    move-result v3

    .line 1790020
    iget-object v4, p0, LX/BUz;->t:Landroid/media/MediaExtractor;

    invoke-virtual {v4}, Landroid/media/MediaExtractor;->getSampleTime()J

    move-result-wide v4

    .line 1790021
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 1790022
    if-gez v3, :cond_0

    .line 1790023
    iget-object v0, p0, LX/BUz;->r:Landroid/media/MediaCodec;

    const/4 v6, 0x4

    move v3, v2

    invoke-virtual/range {v0 .. v6}, Landroid/media/MediaCodec;->queueInputBuffer(IIIJI)V

    move v2, v7

    .line 1790024
    :goto_0
    return v2

    .line 1790025
    :cond_0
    iget-object v0, p0, LX/BUz;->t:Landroid/media/MediaExtractor;

    invoke-virtual {v0}, Landroid/media/MediaExtractor;->getSampleTrackIndex()I

    move-result v0

    iget v6, p0, LX/BUz;->e:I

    if-eq v0, v6, :cond_1

    .line 1790026
    sget-object v0, LX/BUz;->a:Ljava/lang/String;

    const-string v6, "WEIRD: got sample from track %d, expected %d"

    new-array v9, v8, [Ljava/lang/Object;

    iget-object v10, p0, LX/BUz;->t:Landroid/media/MediaExtractor;

    invoke-virtual {v10}, Landroid/media/MediaExtractor;->getSampleTrackIndex()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v2

    iget v10, p0, LX/BUz;->e:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v7

    invoke-static {v0, v6, v9}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1790027
    :cond_1
    iget-object v0, p0, LX/BUz;->r:Landroid/media/MediaCodec;

    move v6, v2

    invoke-virtual/range {v0 .. v6}, Landroid/media/MediaCodec;->queueInputBuffer(IIIJI)V

    .line 1790028
    new-array v0, v8, [Ljava/lang/Object;

    iget v1, p0, LX/BUz;->f:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v7

    .line 1790029
    iget v0, p0, LX/BUz;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/BUz;->f:I

    .line 1790030
    iget-object v0, p0, LX/BUz;->t:Landroid/media/MediaExtractor;

    invoke-virtual {v0}, Landroid/media/MediaExtractor;->getSampleTime()J

    move-result-wide v0

    iput-wide v0, p0, LX/BUz;->g:J

    .line 1790031
    new-array v0, v8, [Ljava/lang/Object;

    iget-wide v4, p0, LX/BUz;->g:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v0, v7

    .line 1790032
    iget-object v0, p0, LX/BUz;->t:Landroid/media/MediaExtractor;

    invoke-virtual {v0}, Landroid/media/MediaExtractor;->advance()Z

    goto :goto_0

    :cond_2
    move v2, v8

    .line 1790033
    goto :goto_0
.end method

.method private static a(LX/BUz;ZJ)LX/1FJ;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZJ)",
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1790000
    iget-object v0, p0, LX/BUz;->s:LX/BUx;

    invoke-virtual {v0}, LX/BUx;->d()V

    .line 1790001
    iget-object v0, p0, LX/BUz;->s:LX/BUx;

    invoke-virtual {v0, p2, p3}, LX/BUx;->a(J)V

    .line 1790002
    if-eqz p1, :cond_0

    .line 1790003
    iget-object v0, p0, LX/BUz;->t:Landroid/media/MediaExtractor;

    iget v1, p0, LX/BUz;->e:I

    invoke-virtual {v0, v1}, Landroid/media/MediaExtractor;->selectTrack(I)V

    .line 1790004
    iget-object v0, p0, LX/BUz;->r:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->flush()V

    .line 1790005
    iget-object v0, p0, LX/BUz;->r:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->start()V

    .line 1790006
    :cond_0
    iget-object v0, p0, LX/BUz;->s:LX/BUx;

    const/4 v2, 0x0

    .line 1790007
    iget-object v3, v0, LX/BUx;->p:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 1790008
    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    iget v4, v0, LX/BUx;->j:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v2

    const/4 v4, 0x1

    iget v5, v0, LX/BUx;->k:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    .line 1790009
    iget v4, v0, LX/BUx;->j:I

    iget v5, v0, LX/BUx;->k:I

    const/16 v6, 0x1908

    const/16 v7, 0x1401

    iget-object v8, v0, LX/BUx;->p:Ljava/nio/ByteBuffer;

    move v3, v2

    invoke-static/range {v2 .. v8}, Landroid/opengl/GLES20;->glReadPixels(IIIIIILjava/nio/Buffer;)V

    .line 1790010
    iget-object v2, v0, LX/BUx;->l:LX/1FZ;

    iget v3, v0, LX/BUx;->j:I

    iget v4, v0, LX/BUx;->k:I

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v2, v3, v4, v5}, LX/1FZ;->a(IILandroid/graphics/Bitmap$Config;)LX/1FJ;

    move-result-object v3

    .line 1790011
    iget-object v2, v0, LX/BUx;->p:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 1790012
    invoke-virtual {v3}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Bitmap;

    iget-object v4, v0, LX/BUx;->p:Ljava/nio/ByteBuffer;

    invoke-virtual {v2, v4}, Landroid/graphics/Bitmap;->copyPixelsFromBuffer(Ljava/nio/Buffer;)V

    .line 1790013
    move-object v0, v3

    .line 1790014
    return-object v0
.end method

.method private static a(LX/BUz;J)V
    .locals 13

    .prologue
    const-wide/32 v10, 0x30d40

    const-wide/16 v8, 0x0

    const/4 v3, 0x0

    const-wide/16 v4, -0x1

    .line 1789983
    iget-wide v0, p0, LX/BUz;->h:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_0

    iget-wide v0, p0, LX/BUz;->i:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_0

    iget-wide v0, p0, LX/BUz;->h:J

    cmp-long v0, p1, v0

    if-ltz v0, :cond_0

    iget-wide v0, p0, LX/BUz;->i:J

    cmp-long v0, p1, v0

    if-gez v0, :cond_0

    iget-wide v0, p0, LX/BUz;->g:J

    cmp-long v0, v0, p1

    if-ltz v0, :cond_5

    .line 1789984
    :cond_0
    iget-boolean v0, p0, LX/BUz;->l:Z

    if-eqz v0, :cond_1

    .line 1789985
    iget-object v0, p0, LX/BUz;->r:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->flush()V

    .line 1789986
    iput-boolean v3, p0, LX/BUz;->l:Z

    .line 1789987
    :cond_1
    iput-wide v4, p0, LX/BUz;->g:J

    .line 1789988
    invoke-static {p0}, LX/BUz;->b(LX/BUz;)LX/60x;

    move-result-object v0

    iget-wide v0, v0, LX/60x;->a:J

    const-wide/16 v4, 0x3e8

    mul-long/2addr v0, v4

    sub-long/2addr v0, v10

    invoke-static {v0, v1, p1, p2}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    .line 1789989
    iget-object v2, p0, LX/BUz;->t:Landroid/media/MediaExtractor;

    const/4 v4, 0x1

    invoke-virtual {v2, v0, v1, v4}, Landroid/media/MediaExtractor;->seekTo(JI)V

    .line 1789990
    iget-object v2, p0, LX/BUz;->t:Landroid/media/MediaExtractor;

    invoke-virtual {v2}, Landroid/media/MediaExtractor;->getSampleTime()J

    move-result-wide v4

    iput-wide v4, p0, LX/BUz;->i:J

    .line 1789991
    iget-wide v4, p0, LX/BUz;->i:J

    cmp-long v2, v4, p1

    if-gez v2, :cond_2

    .line 1789992
    const-wide/16 v4, 0x1

    add-long/2addr v4, p1

    iput-wide v4, p0, LX/BUz;->i:J

    .line 1789993
    :cond_2
    iget-object v4, p0, LX/BUz;->t:Landroid/media/MediaExtractor;

    sget-wide v6, LX/BUz;->j:J

    cmp-long v2, v0, v6

    if-gtz v2, :cond_4

    const/4 v2, 0x2

    :goto_0
    invoke-virtual {v4, v0, v1, v2}, Landroid/media/MediaExtractor;->seekTo(JI)V

    .line 1789994
    iget-object v2, p0, LX/BUz;->t:Landroid/media/MediaExtractor;

    invoke-virtual {v2}, Landroid/media/MediaExtractor;->getSampleTime()J

    move-result-wide v4

    iput-wide v4, p0, LX/BUz;->h:J

    .line 1789995
    sub-long/2addr v0, v10

    .line 1789996
    iget-wide v4, p0, LX/BUz;->h:J

    cmp-long v2, v4, v8

    if-gez v2, :cond_3

    cmp-long v2, v0, v8

    if-gez v2, :cond_2

    .line 1789997
    :cond_3
    :goto_1
    return-void

    :cond_4
    move v2, v3

    .line 1789998
    goto :goto_0

    .line 1789999
    :cond_5
    iget-object v0, p0, LX/BUz;->q:LX/03V;

    const-string v1, "profile_video_frame_retriever"

    const-string v2, "Not seeking!"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static a(LX/BUz;Landroid/graphics/RectF;)V
    .locals 13

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1789940
    iget-boolean v0, p0, LX/BUz;->k:Z

    if-eqz v0, :cond_0

    .line 1789941
    :goto_0
    return-void

    .line 1789942
    :cond_0
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    if-eq v0, v3, :cond_1

    move v0, v1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1789943
    new-instance v0, Ljava/io/File;

    iget-object v3, p0, LX/BUz;->b:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1789944
    invoke-virtual {v0}, Ljava/io/File;->canRead()Z

    move-result v3

    if-nez v3, :cond_2

    .line 1789945
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unable to read "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    move v0, v2

    .line 1789946
    goto :goto_1

    .line 1789947
    :cond_2
    new-instance v3, Landroid/media/MediaExtractor;

    invoke-direct {v3}, Landroid/media/MediaExtractor;-><init>()V

    iput-object v3, p0, LX/BUz;->t:Landroid/media/MediaExtractor;

    .line 1789948
    :try_start_0
    iget-object v3, p0, LX/BUz;->t:Landroid/media/MediaExtractor;

    invoke-virtual {v0}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/media/MediaExtractor;->setDataSource(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1789949
    iget-object v3, p0, LX/BUz;->t:Landroid/media/MediaExtractor;

    .line 1789950
    invoke-virtual {v3}, Landroid/media/MediaExtractor;->getTrackCount()I

    move-result v5

    .line 1789951
    const/4 v4, 0x0

    :goto_2
    if-ge v4, v5, :cond_7

    .line 1789952
    invoke-virtual {v3, v4}, Landroid/media/MediaExtractor;->getTrackFormat(I)Landroid/media/MediaFormat;

    move-result-object v6

    .line 1789953
    const-string v7, "mime"

    invoke-virtual {v6, v7}, Landroid/media/MediaFormat;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1789954
    const-string v7, "video/"

    invoke-virtual {v6, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 1789955
    :goto_3
    move v3, v4

    .line 1789956
    iput v3, p0, LX/BUz;->e:I

    .line 1789957
    iget v3, p0, LX/BUz;->e:I

    if-gez v3, :cond_3

    .line 1789958
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "No video track found in "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1789959
    :catch_0
    move-exception v0

    .line 1789960
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unable to set the data source: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1789961
    :cond_3
    iget-object v0, p0, LX/BUz;->t:Landroid/media/MediaExtractor;

    iget v3, p0, LX/BUz;->e:I

    invoke-virtual {v0, v3}, Landroid/media/MediaExtractor;->selectTrack(I)V

    .line 1789962
    iget-object v0, p0, LX/BUz;->t:Landroid/media/MediaExtractor;

    iget v3, p0, LX/BUz;->e:I

    invoke-virtual {v0, v3}, Landroid/media/MediaExtractor;->getTrackFormat(I)Landroid/media/MediaFormat;

    move-result-object v0

    .line 1789963
    const-string v3, "max-input-size"

    invoke-virtual {v0, v3, v2}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 1789964
    iget-object v2, p0, LX/BUz;->s:LX/BUx;

    if-eqz v2, :cond_4

    .line 1789965
    iget-object v2, p0, LX/BUz;->s:LX/BUx;

    invoke-virtual {v2}, LX/BUx;->a()V

    .line 1789966
    :cond_4
    iget-object v2, p0, LX/BUz;->s:LX/BUx;

    if-nez v2, :cond_5

    .line 1789967
    iget-object v2, p0, LX/BUz;->u:LX/BUy;

    invoke-static {p0}, LX/BUz;->b(LX/BUz;)LX/60x;

    move-result-object v3

    iget-object v4, p0, LX/BUz;->c:LX/7Sv;

    iget-object v5, p0, LX/BUz;->o:Ljava/util/List;

    .line 1789968
    new-instance v6, LX/BUx;

    invoke-static {v2}, LX/1Et;->a(LX/0QB;)LX/1FZ;

    move-result-object v11

    check-cast v11, LX/1FZ;

    invoke-static {v2}, LX/6Wx;->b(LX/0QB;)LX/6Wx;

    move-result-object v12

    check-cast v12, LX/5Pc;

    move-object v7, v3

    move-object v8, p1

    move-object v9, v4

    move-object v10, v5

    invoke-direct/range {v6 .. v12}, LX/BUx;-><init>(LX/60x;Landroid/graphics/RectF;LX/7Sv;Ljava/util/List;LX/1FZ;LX/5Pc;)V

    .line 1789969
    move-object v2, v6

    .line 1789970
    iput-object v2, p0, LX/BUz;->s:LX/BUx;

    .line 1789971
    :cond_5
    const-string v2, "mime"

    invoke-virtual {v0, v2}, Landroid/media/MediaFormat;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1789972
    :try_start_1
    invoke-direct {p0}, LX/BUz;->e()V

    .line 1789973
    invoke-static {v2}, Landroid/media/MediaCodec;->createDecoderByType(Ljava/lang/String;)Landroid/media/MediaCodec;

    move-result-object v2

    iput-object v2, p0, LX/BUz;->r:Landroid/media/MediaCodec;

    .line 1789974
    iget-object v2, p0, LX/BUz;->r:Landroid/media/MediaCodec;

    iget-object v3, p0, LX/BUz;->s:LX/BUx;

    .line 1789975
    iget-object v4, v3, LX/BUx;->d:Landroid/view/Surface;

    move-object v3, v4

    .line 1789976
    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2, v0, v3, v4, v5}, Landroid/media/MediaCodec;->configure(Landroid/media/MediaFormat;Landroid/view/Surface;Landroid/media/MediaCrypto;I)V

    .line 1789977
    iget-object v0, p0, LX/BUz;->r:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->start()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 1789978
    iput-boolean v1, p0, LX/BUz;->k:Z

    goto/16 :goto_0

    .line 1789979
    :catch_1
    move-exception v0

    .line 1789980
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unable to determine decoder: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1789981
    :cond_6
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_2

    .line 1789982
    :cond_7
    const/4 v4, -0x1

    goto/16 :goto_3
.end method

.method public static b(LX/BUz;JLandroid/graphics/RectF;J)LX/1FJ;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Landroid/graphics/RectF;",
            "J)",
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1790076
    iget-object v2, p0, LX/BUz;->r:Landroid/media/MediaCodec;

    invoke-virtual {v2}, Landroid/media/MediaCodec;->getInputBuffers()[Ljava/nio/ByteBuffer;

    move-result-object v7

    .line 1790077
    new-instance v10, Landroid/media/MediaCodec$BufferInfo;

    invoke-direct {v10}, Landroid/media/MediaCodec$BufferInfo;-><init>()V

    .line 1790078
    const/4 v2, 0x0

    iput v2, p0, LX/BUz;->f:I

    .line 1790079
    invoke-static/range {p0 .. p2}, LX/BUz;->a(LX/BUz;J)V

    .line 1790080
    const/4 v6, 0x0

    .line 1790081
    const/4 v5, 0x0

    .line 1790082
    const/4 v4, 0x0

    .line 1790083
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    move-wide v8, v2

    move v3, v5

    .line 1790084
    :goto_0
    if-nez v6, :cond_b

    .line 1790085
    const/4 v2, 0x0

    .line 1790086
    move-wide/from16 v0, p4

    invoke-static {p0, v0, v1}, LX/BUz;->b(LX/BUz;J)V

    .line 1790087
    if-nez v3, :cond_0

    .line 1790088
    move-wide/from16 v0, p1

    invoke-static {p0, v7, v0, v1}, LX/BUz;->a(LX/BUz;[Ljava/nio/ByteBuffer;J)I

    move-result v2

    .line 1790089
    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    const/4 v3, 0x1

    .line 1790090
    :goto_1
    const/4 v5, 0x2

    if-eq v2, v5, :cond_3

    const/4 v2, 0x1

    .line 1790091
    :cond_0
    :goto_2
    move-wide/from16 v0, p1

    invoke-static {p0, v10, v0, v1}, LX/BUz;->a(LX/BUz;Landroid/media/MediaCodec$BufferInfo;J)I

    move-result v11

    .line 1790092
    const/4 v5, 0x1

    if-ne v11, v5, :cond_4

    const/4 v5, 0x1

    move v6, v5

    .line 1790093
    :goto_3
    const/4 v5, 0x2

    if-eq v11, v5, :cond_5

    const/4 v5, 0x1

    .line 1790094
    :goto_4
    const/4 v12, 0x3

    if-ne v11, v12, :cond_6

    .line 1790095
    move-wide/from16 v0, p1

    invoke-static {p0, v3, v0, v1}, LX/BUz;->a(LX/BUz;ZJ)LX/1FJ;

    move-result-object v2

    .line 1790096
    if-eqz v3, :cond_1

    .line 1790097
    invoke-virtual {p0}, LX/BUz;->a()V

    .line 1790098
    :cond_1
    :goto_5
    return-object v2

    .line 1790099
    :cond_2
    const/4 v3, 0x0

    goto :goto_1

    .line 1790100
    :cond_3
    const/4 v2, 0x0

    goto :goto_2

    .line 1790101
    :cond_4
    const/4 v5, 0x0

    move v6, v5

    goto :goto_3

    .line 1790102
    :cond_5
    const/4 v5, 0x0

    goto :goto_4

    .line 1790103
    :cond_6
    if-nez v2, :cond_9

    if-nez v5, :cond_9

    .line 1790104
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v12

    sub-long/2addr v12, v8

    .line 1790105
    add-int/lit8 v2, v4, 0x1

    const/4 v4, 0x5

    if-le v2, v4, :cond_8

    const-wide/16 v4, 0x12c

    cmp-long v4, v12, v4

    if-lez v4, :cond_8

    .line 1790106
    sget-object v4, LX/BUz;->a:Ljava/lang/String;

    const-string v5, "Resetting decoder after %d ms"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v4, v5, v6}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1790107
    invoke-virtual {p0}, LX/BUz;->a()V

    .line 1790108
    const/16 v4, 0x32

    invoke-static {v4}, LX/BUz;->b(I)V

    .line 1790109
    move-object/from16 v0, p3

    invoke-static {p0, v0}, LX/BUz;->a(LX/BUz;Landroid/graphics/RectF;)V

    .line 1790110
    const/16 v4, 0xc8

    invoke-static {v4}, LX/BUz;->b(I)V

    .line 1790111
    const-wide/16 v4, -0x1

    iput-wide v4, p0, LX/BUz;->i:J

    iput-wide v4, p0, LX/BUz;->h:J

    iput-wide v4, p0, LX/BUz;->g:J

    .line 1790112
    const/4 v4, 0x0

    iput-boolean v4, p0, LX/BUz;->l:Z

    .line 1790113
    :goto_6
    if-eqz v3, :cond_a

    .line 1790114
    invoke-virtual {p0}, LX/BUz;->a()V

    .line 1790115
    :cond_7
    const/4 v2, 0x0

    goto :goto_5

    :cond_8
    move v4, v2

    .line 1790116
    goto/16 :goto_0

    .line 1790117
    :cond_9
    const/4 v2, 0x0

    .line 1790118
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    move-wide v8, v4

    move v4, v2

    .line 1790119
    goto/16 :goto_0

    .line 1790120
    :cond_a
    const/4 v3, 0x5

    if-lt v2, v3, :cond_7

    .line 1790121
    sget-object v2, LX/BUz;->a:Ljava/lang/String;

    const-string v3, "Unable to provide an image due to stuck input/output"

    invoke-static {v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1790122
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "Stuck on input/output streams"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_b
    move v2, v4

    goto :goto_6
.end method

.method public static b(LX/BUz;)LX/60x;
    .locals 2

    .prologue
    .line 1789937
    iget-object v0, p0, LX/BUz;->w:LX/60x;

    if-nez v0, :cond_0

    .line 1789938
    iget-object v0, p0, LX/BUz;->v:LX/2MV;

    iget-object v1, p0, LX/BUz;->b:Landroid/net/Uri;

    invoke-interface {v0, v1}, LX/2MV;->a(Landroid/net/Uri;)LX/60x;

    move-result-object v0

    iput-object v0, p0, LX/BUz;->w:LX/60x;

    .line 1789939
    :cond_0
    iget-object v0, p0, LX/BUz;->w:LX/60x;

    return-object v0
.end method

.method private static b(I)V
    .locals 2

    .prologue
    .line 1789935
    int-to-long v0, p0

    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1789936
    :goto_0
    return-void

    :catch_0
    goto :goto_0
.end method

.method private static b(LX/BUz;J)V
    .locals 5

    .prologue
    .line 1789931
    iget-object v0, p0, LX/BUz;->d:Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/BUz;->m:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/BUz;->p:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    sub-long/2addr v0, p1

    const-wide/16 v2, 0x12c

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 1789932
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/BUz;->m:Z

    .line 1789933
    iget-object v0, p0, LX/BUz;->n:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/video/scrubber/GLFrameRetriever$1;

    invoke-direct {v1, p0}, Lcom/facebook/video/scrubber/GLFrameRetriever$1;-><init>(LX/BUz;)V

    const v2, 0x4c11be62    # 3.8205832E7f

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1789934
    :cond_0
    return-void
.end method

.method private e()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1789923
    iget-object v0, p0, LX/BUz;->r:Landroid/media/MediaCodec;

    if-eqz v0, :cond_0

    .line 1789924
    :try_start_0
    iget-object v0, p0, LX/BUz;->r:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->stop()V

    .line 1789925
    iget-object v0, p0, LX/BUz;->r:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->release()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1789926
    iput-object v3, p0, LX/BUz;->r:Landroid/media/MediaCodec;

    .line 1789927
    :cond_0
    :goto_0
    return-void

    .line 1789928
    :catch_0
    move-exception v0

    .line 1789929
    :try_start_1
    sget-object v1, LX/BUz;->a:Ljava/lang/String;

    const-string v2, "Oddly, the decoder ran into issues releasing"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1789930
    iput-object v3, p0, LX/BUz;->r:Landroid/media/MediaCodec;

    goto :goto_0

    :catchall_0
    move-exception v0

    iput-object v3, p0, LX/BUz;->r:Landroid/media/MediaCodec;

    throw v0
.end method


# virtual methods
.method public final a(IF)LX/1FJ;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IF)",
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1789907
    const/high16 v7, 0x40000000    # 2.0f

    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 1789908
    invoke-static {p0}, LX/BUz;->b(LX/BUz;)LX/60x;

    move-result-object v3

    .line 1789909
    iget v0, v3, LX/60x;->d:I

    rem-int/lit16 v0, v0, 0xb4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 1789910
    :goto_0
    iget v2, v3, LX/60x;->b:I

    int-to-float v2, v2

    .line 1789911
    iget v3, v3, LX/60x;->c:I

    int-to-float v3, v3

    .line 1789912
    if-eqz v0, :cond_2

    move v0, v2

    .line 1789913
    :goto_1
    div-float v2, v3, p2

    .line 1789914
    mul-float v4, v0, p2

    .line 1789915
    cmpg-float v5, v2, v0

    if-gez v5, :cond_1

    .line 1789916
    sub-float v2, v0, v2

    div-float/2addr v2, v7

    .line 1789917
    div-float v0, v2, v0

    .line 1789918
    :goto_2
    new-instance v2, Landroid/graphics/RectF;

    sub-float v3, v6, v1

    sub-float v4, v6, v0

    invoke-direct {v2, v1, v0, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    move-object v0, v2

    .line 1789919
    invoke-virtual {p0, p1, v0}, LX/BUz;->a(ILandroid/graphics/RectF;)LX/1FJ;

    move-result-object v0

    return-object v0

    .line 1789920
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1789921
    :cond_1
    sub-float v0, v3, v4

    div-float/2addr v0, v7

    .line 1789922
    div-float/2addr v0, v3

    move v8, v1

    move v1, v0

    move v0, v8

    goto :goto_2

    :cond_2
    move v0, v3

    move v3, v2

    goto :goto_1
.end method

.method public final a(ILandroid/graphics/RectF;)LX/1FJ;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/graphics/RectF;",
            ")",
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1789897
    iget-object v0, p0, LX/BUz;->p:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v4

    .line 1789898
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/BUz;->m:Z

    .line 1789899
    mul-int/lit16 v0, p1, 0x3e8

    int-to-long v1, v0

    move-object v0, p0

    move-object v3, p2

    .line 1789900
    :try_start_0
    invoke-static {v0, v3}, LX/BUz;->a(LX/BUz;Landroid/graphics/RectF;)V

    .line 1789901
    iget-object v6, v0, LX/BUz;->s:LX/BUx;

    invoke-virtual {v6}, LX/BUx;->c()V

    .line 1789902
    invoke-static/range {v0 .. v5}, LX/BUz;->b(LX/BUz;JLandroid/graphics/RectF;J)LX/1FJ;

    move-result-object v6

    move-object v0, v6
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1789903
    :goto_0
    return-object v0

    .line 1789904
    :catch_0
    move-exception v0

    .line 1789905
    iget-object v1, p0, LX/BUz;->q:LX/03V;

    const-string v2, "profile_video_frame_retriever"

    const-string v3, "Unable to extract image. Something may be wrong with the video or decoder"

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1789906
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1789886
    invoke-direct {p0}, LX/BUz;->e()V

    .line 1789887
    iget-object v1, p0, LX/BUz;->t:Landroid/media/MediaExtractor;

    if-eqz v1, :cond_0

    .line 1789888
    iget-object v1, p0, LX/BUz;->t:Landroid/media/MediaExtractor;

    invoke-virtual {v1}, Landroid/media/MediaExtractor;->release()V

    .line 1789889
    const/4 v1, 0x0

    iput-object v1, p0, LX/BUz;->t:Landroid/media/MediaExtractor;

    .line 1789890
    :cond_0
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/BUz;->k:Z

    .line 1789891
    iget-object v1, p0, LX/BUz;->s:LX/BUx;

    if-eqz v1, :cond_1

    .line 1789892
    iget-object v1, p0, LX/BUz;->s:LX/BUx;

    invoke-virtual {v1}, LX/BUx;->a()V

    .line 1789893
    const/4 v1, 0x0

    iput-object v1, p0, LX/BUz;->s:LX/BUx;

    .line 1789894
    :cond_1
    iput-boolean v0, p0, LX/BUz;->l:Z

    .line 1789895
    iput-boolean v0, p0, LX/BUz;->k:Z

    .line 1789896
    return-void
.end method
