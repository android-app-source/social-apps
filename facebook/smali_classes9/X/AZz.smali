.class public LX/AZz;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:LX/0tX;

.field private final c:Landroid/os/Handler;

.field public final d:LX/1b4;

.field public final e:Ljava/lang/String;

.field public final f:LX/AY3;

.field public g:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$FetchCreativeToolsQueryModel;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1687729
    const-class v0, LX/AZz;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/AZz;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0tX;Landroid/os/Handler;LX/1b4;Ljava/lang/String;LX/AY3;)V
    .locals 0
    .param p2    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/AY3;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1687730
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1687731
    iput-object p1, p0, LX/AZz;->b:LX/0tX;

    .line 1687732
    iput-object p2, p0, LX/AZz;->c:Landroid/os/Handler;

    .line 1687733
    iput-object p3, p0, LX/AZz;->d:LX/1b4;

    .line 1687734
    iput-object p4, p0, LX/AZz;->e:Ljava/lang/String;

    .line 1687735
    iput-object p5, p0, LX/AZz;->f:LX/AY3;

    .line 1687736
    return-void
.end method

.method public static a$redex0(LX/AZz;Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$FetchCreativeToolsQueryModel;)V
    .locals 13

    .prologue
    const/4 v3, 0x0

    .line 1687737
    invoke-virtual {p1}, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$FetchCreativeToolsQueryModel;->j()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    sget-object v2, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    monitor-exit v2

    .line 1687738
    if-nez v0, :cond_1

    .line 1687739
    :cond_0
    :goto_0
    return-void

    .line 1687740
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1687741
    :cond_1
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1687742
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1687743
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 1687744
    const v2, -0x1fb21f93

    invoke-static {v1, v0, v3, v2}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_1
    invoke-virtual {v0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v7

    :cond_2
    invoke-interface {v7}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-interface {v7}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v8, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 1687745
    if-eqz v0, :cond_2

    .line 1687746
    invoke-virtual {v8, v0, v3}, LX/15i;->g(II)I

    move-result v9

    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1687747
    if-eqz v9, :cond_2

    .line 1687748
    const/4 v0, 0x2

    const-class v1, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoBasicAdjustmentFilterModel;

    invoke-virtual {v8, v9, v0, v1}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    move-object v1, v0

    :goto_2
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v10

    move v2, v3

    :goto_3
    if-ge v2, v10, :cond_6

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoBasicAdjustmentFilterModel;

    .line 1687749
    if-eqz v0, :cond_3

    .line 1687750
    invoke-static {v0}, LX/AZO;->a(Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoBasicAdjustmentFilterModel;)LX/AZO;

    move-result-object v0

    .line 1687751
    if-eqz v0, :cond_3

    .line 1687752
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1687753
    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    .line 1687754
    :cond_4
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_1

    .line 1687755
    :catchall_1
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    .line 1687756
    :cond_5
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1687757
    move-object v1, v0

    goto :goto_2

    .line 1687758
    :cond_6
    const/4 v0, 0x1

    invoke-virtual {v8, v9, v0}, LX/15i;->o(II)LX/22e;

    move-result-object v0

    if-eqz v0, :cond_8

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    move-object v1, v0

    :goto_4
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v10

    move v2, v3

    :goto_5
    if-ge v2, v10, :cond_9

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1687759
    const/4 v11, 0x0

    .line 1687760
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_11

    .line 1687761
    :goto_6
    move-object v0, v11

    .line 1687762
    if-eqz v0, :cond_7

    .line 1687763
    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1687764
    :cond_7
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_5

    .line 1687765
    :cond_8
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1687766
    move-object v1, v0

    goto :goto_4

    .line 1687767
    :cond_9
    const/4 v0, 0x3

    const-class v1, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel;

    invoke-virtual {v8, v9, v0, v1}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v0

    if-eqz v0, :cond_c

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    move-object v1, v0

    :goto_7
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v8

    move v2, v3

    .line 1687768
    :goto_8
    if-ge v2, v8, :cond_2

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel;

    .line 1687769
    const/4 v9, 0x0

    .line 1687770
    if-eqz v0, :cond_a

    invoke-virtual {v0}, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel;->a()Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel$MaskEffectModel;

    move-result-object v10

    if-eqz v10, :cond_a

    invoke-virtual {v0}, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel;->j()Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel$ThumbnailImageModel;

    move-result-object v10

    if-nez v10, :cond_13

    .line 1687771
    :cond_a
    :goto_9
    move-object v0, v9

    .line 1687772
    if-eqz v0, :cond_b

    .line 1687773
    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1687774
    :cond_b
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_8

    .line 1687775
    :cond_c
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1687776
    move-object v1, v0

    goto :goto_7

    .line 1687777
    :cond_d
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1687778
    invoke-static {v4}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 1687779
    sget-object v1, LX/AZO;->a:LX/AZO;

    invoke-interface {v4, v3, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 1687780
    new-instance v1, LX/AZa;

    invoke-direct {v1, v4}, LX/AZa;-><init>(Ljava/util/List;)V

    .line 1687781
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1687782
    :cond_e
    invoke-static {v5}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 1687783
    new-instance v1, LX/AZe;

    invoke-direct {v1, v5}, LX/AZe;-><init>(Ljava/util/List;)V

    .line 1687784
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1687785
    :cond_f
    invoke-static {v6}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 1687786
    sget-object v1, LX/AZl;->a:LX/AZl;

    invoke-interface {v6, v3, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 1687787
    new-instance v1, LX/AZm;

    invoke-direct {v1, v6}, LX/AZm;-><init>(Ljava/util/List;)V

    .line 1687788
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1687789
    :cond_10
    invoke-static {v0}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1687790
    iget-object v1, p0, LX/AZz;->c:Landroid/os/Handler;

    new-instance v2, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsModel$2;

    invoke-direct {v2, p0, v0}, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsModel$2;-><init>(LX/AZz;Ljava/util/List;)V

    const v0, -0x48845fea

    invoke-static {v1, v2, v0}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto/16 :goto_0

    .line 1687791
    :cond_11
    const/4 v12, 0x0

    invoke-virtual {v0, v12}, Ljava/lang/String;->codePointAt(I)I

    move-result v12

    const/16 p1, 0x23

    if-eq v12, p1, :cond_12

    .line 1687792
    new-instance v12, Ljava/lang/StringBuilder;

    const-string p1, "#"

    invoke-direct {v12, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1687793
    :cond_12
    :try_start_3
    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_0

    move-result v12

    .line 1687794
    new-instance v11, LX/AZb;

    invoke-direct {v11, v12}, LX/AZb;-><init>(I)V

    goto/16 :goto_6

    .line 1687795
    :catch_0
    goto/16 :goto_6

    .line 1687796
    :cond_13
    invoke-virtual {v0}, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel;->a()Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel$MaskEffectModel;

    move-result-object v10

    invoke-virtual {v10}, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel$MaskEffectModel;->j()Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$BestMaskPackageFragmentModel;

    move-result-object v10

    invoke-static {v10}, LX/AMT;->a(Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$BestMaskPackageFragmentModel;)LX/AMT;

    move-result-object v10

    .line 1687797
    if-eqz v10, :cond_a

    .line 1687798
    new-instance v9, Ljava/util/HashSet;

    invoke-direct {v9}, Ljava/util/HashSet;-><init>()V

    .line 1687799
    invoke-virtual {v0}, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel;->a()Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel$MaskEffectModel;

    move-result-object v11

    invoke-virtual {v11}, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel$MaskEffectModel;->j()Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$BestMaskPackageFragmentModel;

    move-result-object v11

    invoke-static {v11, v9}, LX/AMT;->a(Lcom/facebook/cameracore/assets/graphql/FetchBestMasksQueryModels$BestMaskPackageFragmentModel;Ljava/util/Set;)Ljava/util/List;

    move-result-object v11

    .line 1687800
    new-instance v9, LX/AZl;

    const/4 v12, 0x0

    invoke-direct {v9, v0, v10, v11, v12}, LX/AZl;-><init>(Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel;LX/AMT;Ljava/util/List;Z)V

    goto/16 :goto_9
.end method


# virtual methods
.method public final b()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1687801
    iget-object v0, p0, LX/AZz;->g:Lcom/google/common/util/concurrent/ListenableFuture;

    if-nez v0, :cond_0

    .line 1687802
    :goto_0
    return-void

    .line 1687803
    :cond_0
    iget-object v0, p0, LX/AZz;->g:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 1687804
    iput-object v2, p0, LX/AZz;->g:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1687805
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/AZz;->h:Z

    .line 1687806
    iget-object v0, p0, LX/AZz;->c:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    goto :goto_0
.end method
