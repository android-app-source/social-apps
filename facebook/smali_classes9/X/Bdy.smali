.class public final LX/Bdy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field public final synthetic a:Lcom/facebook/composer/ui/text/ComposerEditText;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/ui/text/ComposerEditText;)V
    .locals 0

    .prologue
    .line 1804400
    iput-object p1, p0, LX/Bdy;->a:Lcom/facebook/composer/ui/text/ComposerEditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onGlobalLayout()V
    .locals 3

    .prologue
    .line 1804390
    const/4 v0, 0x0

    .line 1804391
    iget-object v1, p0, LX/Bdy;->a:Lcom/facebook/composer/ui/text/ComposerEditText;

    invoke-virtual {v1}, Lcom/facebook/composer/ui/text/ComposerEditText;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    .line 1804392
    :goto_0
    if-eqz v1, :cond_1

    .line 1804393
    instance-of v2, v1, Landroid/widget/ScrollView;

    if-nez v2, :cond_0

    instance-of v2, v1, Landroid/support/v7/widget/RecyclerView;

    if-eqz v2, :cond_2

    .line 1804394
    :cond_0
    const/4 v0, 0x1

    .line 1804395
    :cond_1
    if-nez v0, :cond_3

    .line 1804396
    new-instance v0, LX/45X;

    const-string v1, "The wrapper of ComposerTextEdit must be put in a ScrollView or RecyclerView"

    invoke-direct {v0, v1}, LX/45X;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1804397
    :cond_2
    invoke-interface {v1}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    goto :goto_0

    .line 1804398
    :cond_3
    iget-object v0, p0, LX/Bdy;->a:Lcom/facebook/composer/ui/text/ComposerEditText;

    invoke-virtual {v0}, Lcom/facebook/composer/ui/text/ComposerEditText;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 1804399
    return-void
.end method
