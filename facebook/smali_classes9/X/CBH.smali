.class public final LX/CBH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/2mJ;

.field public final synthetic b:Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;

.field public final synthetic c:LX/13P;

.field public final synthetic d:Lcom/facebook/graphql/model/GraphQLQuickPromotion;


# direct methods
.method public constructor <init>(LX/2mJ;Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;LX/13P;Lcom/facebook/graphql/model/GraphQLQuickPromotion;)V
    .locals 0

    .prologue
    .line 1856232
    iput-object p1, p0, LX/CBH;->a:LX/2mJ;

    iput-object p2, p0, LX/CBH;->b:Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;

    iput-object p3, p0, LX/CBH;->c:LX/13P;

    iput-object p4, p0, LX/CBH;->d:Lcom/facebook/graphql/model/GraphQLQuickPromotion;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0x602b6d02

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1856233
    iget-object v1, p0, LX/CBH;->a:LX/2mJ;

    iget-object v2, p0, LX/CBH;->b:Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLQuickPromotionCreative;->q()Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLQuickPromotionAction;->j()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/2mJ;->a(Landroid/net/Uri;)V

    .line 1856234
    iget-object v1, p0, LX/CBH;->c:LX/13P;

    sget-object v2, LX/77m;->SECONDARY_ACTION:LX/77m;

    iget-object v3, p0, LX/CBH;->d:Lcom/facebook/graphql/model/GraphQLQuickPromotion;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLQuickPromotion;->k()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/13P;->a(LX/77m;Ljava/lang/String;)V

    .line 1856235
    const v1, 0x7ec8c5f4

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
