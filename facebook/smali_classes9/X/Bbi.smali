.class public final LX/Bbi;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLMutationsModels$PlaceListAskFriendsForRecommendationsMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/util/List;

.field public final synthetic b:LX/Bbj;


# direct methods
.method public constructor <init>(LX/Bbj;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 1801026
    iput-object p1, p0, LX/Bbi;->b:LX/Bbj;

    iput-object p2, p0, LX/Bbi;->a:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1801020
    iget-object v0, p0, LX/Bbi;->b:LX/Bbj;

    iget-object v0, v0, LX/Bbj;->a:Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;

    iget-object v0, v0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->u:LX/8yG;

    iget-object v1, p0, LX/Bbi;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    .line 1801021
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v2

    const-string v3, "num_friends_invited"

    invoke-virtual {v2, v3, v1}, LX/1rQ;->a(Ljava/lang/String;I)LX/1rQ;

    move-result-object v2

    .line 1801022
    iget-object v3, v0, LX/8yG;->a:LX/0if;

    sget-object v4, LX/0ig;->aA:LX/0ih;

    const-string v5, "invite_friends_notification_sent"

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v5, v6, v2}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 1801023
    iget-object v0, p0, LX/Bbi;->b:LX/Bbj;

    iget-object v0, v0, LX/Bbj;->a:Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;

    iget-object v0, v0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->u:LX/8yG;

    invoke-virtual {v0}, LX/8yG;->h()V

    .line 1801024
    iget-object v0, p0, LX/Bbi;->b:LX/Bbj;

    iget-object v0, v0, LX/Bbj;->a:Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;

    invoke-virtual {v0}, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->finish()V

    .line 1801025
    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1801017
    iget-object v0, p0, LX/Bbi;->b:LX/Bbj;

    iget-object v0, v0, LX/Bbj;->a:Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;

    iget-object v0, v0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->v:LX/0kL;

    new-instance v1, LX/27k;

    const-string v2, "Failed sending notification to friends"

    invoke-direct {v1, v2}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 1801018
    iget-object v0, p0, LX/Bbi;->b:LX/Bbj;

    iget-object v0, v0, LX/Bbj;->a:Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;

    iget-object v0, v0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->C:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->b()V

    .line 1801019
    return-void
.end method

.method public final synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1801016
    invoke-direct {p0}, LX/Bbi;->a()V

    return-void
.end method
