.class public final LX/CTq;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/CU1;

.field public final b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/CTp;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;LX/2uF;)V
    .locals 10
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "<init>"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    const/4 v9, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1896058
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1896059
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1896060
    if-eqz p1, :cond_0

    .line 1896061
    new-instance v0, LX/CU1;

    invoke-direct {v0, p1}, LX/CU1;-><init>(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;)V

    iput-object v0, p0, LX/CTq;->a:LX/CU1;

    .line 1896062
    :goto_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/CTq;->b:Ljava/util/ArrayList;

    .line 1896063
    invoke-virtual {p2}, LX/3Sa;->e()LX/3Sh;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v5, v0, LX/1vs;->b:I

    .line 1896064
    invoke-virtual {v4, v5, v1}, LX/15i;->g(II)I

    move-result v0

    const v6, 0x3ca4e33b

    invoke-static {v4, v0, v6}, LX/3Sm;->a(LX/15i;II)LX/1vs;

    .line 1896065
    invoke-virtual {v4, v5, v1}, LX/15i;->g(II)I

    move-result v0

    invoke-virtual {v4, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_2
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1896066
    invoke-virtual {v4, v5, v9}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_3
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1896067
    invoke-virtual {v4, v5, v1}, LX/15i;->g(II)I

    move-result v0

    .line 1896068
    iget-object v6, p0, LX/CTq;->b:Ljava/util/ArrayList;

    new-instance v7, LX/CTp;

    invoke-virtual {v4, v5, v2}, LX/15i;->h(II)Z

    move-result v8

    invoke-virtual {v4, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v5, v9}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v7, v8, v0, v4}, LX/CTp;-><init>(ZLjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1896069
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/CTq;->a:LX/CU1;

    goto :goto_0

    :cond_1
    move v0, v2

    .line 1896070
    goto :goto_2

    :cond_2
    move v0, v2

    .line 1896071
    goto :goto_3

    .line 1896072
    :cond_3
    return-void
.end method
