.class public final LX/BJO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Landroid/widget/FrameLayout;

.field public final synthetic b:LX/BJR;


# direct methods
.method public constructor <init>(LX/BJR;Landroid/widget/FrameLayout;)V
    .locals 0

    .prologue
    .line 1771858
    iput-object p1, p0, LX/BJO;->b:LX/BJR;

    iput-object p2, p0, LX/BJO;->a:Landroid/widget/FrameLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, 0x4a17d6d0    # 2487732.0f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1771859
    iget-object v1, p0, LX/BJO;->b:LX/BJR;

    iget-object v1, v1, LX/BJR;->h:LX/BK8;

    .line 1771860
    iget-object v2, v1, LX/BK8;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v2, v2, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v2, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v2}, Lcom/facebook/platform/composer/model/PlatformComposition;->p()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v2

    if-nez v2, :cond_1

    .line 1771861
    :cond_0
    :goto_0
    iget-object v1, p0, LX/BJO;->a:Landroid/widget/FrameLayout;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1771862
    const v1, -0x41c59156

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1771863
    :cond_1
    iget-object v2, v1, LX/BK8;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v2, v2, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v2, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v2}, Lcom/facebook/platform/composer/model/PlatformComposition;->p()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v2

    iget-object v2, v2, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->quoteText:Ljava/lang/String;

    .line 1771864
    if-eqz v2, :cond_0

    .line 1771865
    iget-object v2, v1, LX/BK8;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v4, v1, LX/BK8;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v4, v4, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    invoke-virtual {v4}, LX/BKm;->a()LX/BKl;

    move-result-object v4

    iget-object v5, v1, LX/BK8;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v5, v5, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v5, v5, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v5}, Lcom/facebook/platform/composer/model/PlatformComposition;->A()LX/BKp;

    move-result-object v5

    iget-object p1, v1, LX/BK8;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object p1, p1, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object p1, p1, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {p1}, Lcom/facebook/platform/composer/model/PlatformComposition;->p()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object p1

    iget-object p1, p1, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->linkForShare:Ljava/lang/String;

    invoke-static {p1}, LX/89G;->a(Ljava/lang/String;)LX/89G;

    move-result-object p1

    invoke-virtual {p1}, LX/89G;->b()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object p1

    .line 1771866
    iput-object p1, v5, LX/BKp;->u:Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    .line 1771867
    move-object v5, v5

    .line 1771868
    invoke-virtual {v5}, LX/BKp;->b()Lcom/facebook/platform/composer/model/PlatformComposition;

    move-result-object v5

    .line 1771869
    iput-object v5, v4, LX/BKl;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    .line 1771870
    move-object v4, v4

    .line 1771871
    invoke-virtual {v4}, LX/BKl;->a()LX/BKm;

    move-result-object v4

    .line 1771872
    iput-object v4, v2, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    .line 1771873
    goto :goto_0
.end method
