.class public final LX/CU8;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Z

.field public final e:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;

.field public final f:Lcom/facebook/graphql/enums/GraphQLPagesPlatformButtonGlyphType;


# direct methods
.method public constructor <init>(LX/15i;ILcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;)V
    .locals 6
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "<init>"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    const/4 v5, 0x4

    const/4 v1, 0x1

    const/4 v4, 0x3

    const/4 v2, 0x0

    .line 1896250
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1896251
    const v0, -0x30cf865f

    invoke-static {p1, p2, v0}, LX/3Sm;->a(LX/15i;II)LX/1vs;

    .line 1896252
    invoke-virtual {p1, p2, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1896253
    invoke-virtual {p1, p2, v4}, LX/15i;->g(II)I

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 1896254
    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1896255
    invoke-virtual {p1, p2, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/CXh;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/CU8;->a:Ljava/lang/String;

    .line 1896256
    invoke-virtual {p1, p2, v4}, LX/15i;->g(II)I

    move-result v0

    if-eqz v0, :cond_2

    .line 1896257
    invoke-virtual {p1, p2, v4}, LX/15i;->g(II)I

    move-result v0

    .line 1896258
    invoke-virtual {p1, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    :goto_1
    iput-object v0, p0, LX/CU8;->b:Ljava/lang/String;

    .line 1896259
    invoke-virtual {p1, p2, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/CU8;->c:Ljava/lang/String;

    .line 1896260
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->h(II)Z

    move-result v0

    iput-boolean v0, p0, LX/CU8;->d:Z

    .line 1896261
    const-class v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformButtonGlyphType;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPagesPlatformButtonGlyphType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformButtonGlyphType;

    invoke-virtual {p1, p2, v1, v0, v2}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformButtonGlyphType;

    iput-object v0, p0, LX/CU8;->f:Lcom/facebook/graphql/enums/GraphQLPagesPlatformButtonGlyphType;

    .line 1896262
    iput-object p3, p0, LX/CU8;->e:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;

    .line 1896263
    return-void

    .line 1896264
    :cond_0
    invoke-virtual {p1, p2, v4}, LX/15i;->g(II)I

    move-result v0

    invoke-virtual {p1, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_0

    .line 1896265
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method
