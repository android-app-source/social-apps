.class public final LX/BXS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/BWy;


# instance fields
.field public final synthetic a:Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;)V
    .locals 0

    .prologue
    .line 1793112
    iput-object p1, p0, LX/BXS;->a:Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1793110
    iget-object v0, p0, LX/BXS;->a:Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;

    invoke-virtual {v0}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->y()V

    .line 1793111
    return-void
.end method

.method public final a(LX/8QL;)V
    .locals 8

    .prologue
    .line 1793091
    iget-object v0, p0, LX/BXS;->a:Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;

    const/4 v6, 0x1

    const/4 p0, 0x0

    .line 1793092
    iget-object v1, v0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->F:LX/2EJ;

    if-nez v1, :cond_0

    .line 1793093
    new-instance v1, LX/BXD;

    iget-object v2, v0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->r:Ljava/util/List;

    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    invoke-direct {v1, v2}, LX/BXD;-><init>(LX/0Px;)V

    iput-object v1, v0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->G:LX/BXD;

    .line 1793094
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f0306f3

    .line 1793095
    iget-object v1, v0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v1, v1

    .line 1793096
    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v2, v3, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    iput-object v1, v0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->E:Landroid/widget/ListView;

    .line 1793097
    iget-object v1, v0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->E:Landroid/widget/ListView;

    iget-object v2, v0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->G:LX/BXD;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1793098
    new-instance v1, LX/BXF;

    invoke-direct {v1, v0}, LX/BXF;-><init>(Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;)V

    .line 1793099
    new-instance v2, LX/BXG;

    invoke-direct {v2, v0}, LX/BXG;-><init>(Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;)V

    .line 1793100
    new-instance v3, LX/0ju;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, LX/0ju;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f080be2

    new-array v6, v6, [Ljava/lang/Object;

    iget-object v7, v0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->G:LX/BXD;

    invoke-virtual {v7}, LX/BXD;->getCount()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, p0

    invoke-virtual {v4, v5, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v3

    iget-object v4, v0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->E:Landroid/widget/ListView;

    invoke-virtual {v3, v4}, LX/0ju;->b(Landroid/view/View;)LX/0ju;

    move-result-object v3

    const v4, 0x7f080be0

    invoke-virtual {v3, v4, v1}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v1

    const v3, 0x7f080be1

    invoke-virtual {v1, v3, v2}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v1

    invoke-virtual {v1, p0}, LX/0ju;->a(Z)LX/0ju;

    move-result-object v1

    invoke-virtual {v1}, LX/0ju;->a()LX/2EJ;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->F:LX/2EJ;

    .line 1793101
    iget-object v1, v0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->E:Landroid/widget/ListView;

    new-instance v2, LX/BXH;

    invoke-direct {v2, v0}, LX/BXH;-><init>(Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 1793102
    :goto_0
    iget-object v1, v0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->E:Landroid/widget/ListView;

    iget-object v2, v0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->G:LX/BXD;

    invoke-virtual {v2, p1}, LX/BXD;->a(LX/8QL;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setSelection(I)V

    .line 1793103
    iget-object v1, v0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->F:LX/2EJ;

    invoke-virtual {v1}, LX/2EJ;->show()V

    .line 1793104
    return-void

    .line 1793105
    :cond_0
    iget-object v1, v0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->G:LX/BXD;

    iget-object v2, v0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->r:Ljava/util/List;

    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/BXD;->a(LX/0Px;)V

    .line 1793106
    iget-object v1, v0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->F:LX/2EJ;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f080be2

    new-array v4, v6, [Ljava/lang/Object;

    iget-object v5, v0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->G:LX/BXD;

    invoke-virtual {v5}, LX/BXD;->getCount()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, p0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/2EJ;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 5

    .prologue
    .line 1793107
    iget-object v0, p0, LX/BXS;->a:Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;

    iget-object v1, v0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->D:Lcom/facebook/widget/listview/BetterListView;

    iget-object v0, p0, LX/BXS;->a:Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;

    iget-object v0, v0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->D:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0}, Lcom/facebook/widget/listview/BetterListView;->getPaddingLeft()I

    move-result v2

    iget-object v0, p0, LX/BXS;->a:Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;

    iget-object v0, v0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->D:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0}, Lcom/facebook/widget/listview/BetterListView;->getPaddingTop()I

    move-result v3

    iget-object v0, p0, LX/BXS;->a:Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;

    iget-object v0, v0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->D:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0}, Lcom/facebook/widget/listview/BetterListView;->getPaddingRight()I

    move-result v4

    if-eqz p1, :cond_0

    iget-object v0, p0, LX/BXS;->a:Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;

    iget-object v0, v0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->C:Lcom/facebook/widget/friendselector/FriendSelectorResultBar;

    invoke-virtual {v0}, Lcom/facebook/widget/friendselector/FriendSelectorResultBar;->getMeasuredHeight()I

    move-result v0

    :goto_0
    invoke-virtual {v1, v2, v3, v4, v0}, Lcom/facebook/widget/listview/BetterListView;->setPadding(IIII)V

    .line 1793108
    return-void

    .line 1793109
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
