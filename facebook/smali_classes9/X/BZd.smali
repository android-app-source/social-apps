.class public final LX/BZd;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    .line 1797857
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1797858
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 1797859
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 1797860
    const/4 v2, 0x0

    .line 1797861
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_6

    .line 1797862
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1797863
    :goto_1
    move v1, v2

    .line 1797864
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1797865
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0

    .line 1797866
    :cond_1
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1797867
    :cond_2
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_5

    .line 1797868
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1797869
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1797870
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_2

    if-eqz v4, :cond_2

    .line 1797871
    const-string v5, "locale"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1797872
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_2

    .line 1797873
    :cond_3
    const-string v5, "video_caption_items"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1797874
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1797875
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->START_ARRAY:LX/15z;

    if-ne v4, v5, :cond_4

    .line 1797876
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_ARRAY:LX/15z;

    if-eq v4, v5, :cond_4

    .line 1797877
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1797878
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v7, LX/15z;->START_OBJECT:LX/15z;

    if-eq v4, v7, :cond_e

    .line 1797879
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1797880
    :goto_4
    move v4, v5

    .line 1797881
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 1797882
    :cond_4
    invoke-static {v1, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v1

    move v1, v1

    .line 1797883
    goto :goto_2

    .line 1797884
    :cond_5
    const/4 v4, 0x2

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1797885
    invoke-virtual {p1, v2, v3}, LX/186;->b(II)V

    .line 1797886
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v1}, LX/186;->b(II)V

    .line 1797887
    invoke-virtual {p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_1

    :cond_6
    move v1, v2

    move v3, v2

    goto :goto_2

    .line 1797888
    :cond_7
    const-string v12, "end_time"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_9

    .line 1797889
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v7

    move v9, v7

    move v7, v6

    .line 1797890
    :cond_8
    :goto_5
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_b

    .line 1797891
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 1797892
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1797893
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_8

    if-eqz v11, :cond_8

    .line 1797894
    const-string v12, "caption_text"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 1797895
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p1, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto :goto_5

    .line 1797896
    :cond_9
    const-string v12, "start_time"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_a

    .line 1797897
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v4

    move v8, v4

    move v4, v6

    goto :goto_5

    .line 1797898
    :cond_a
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_5

    .line 1797899
    :cond_b
    const/4 v11, 0x3

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 1797900
    invoke-virtual {p1, v5, v10}, LX/186;->b(II)V

    .line 1797901
    if-eqz v7, :cond_c

    .line 1797902
    invoke-virtual {p1, v6, v9, v5}, LX/186;->a(III)V

    .line 1797903
    :cond_c
    if-eqz v4, :cond_d

    .line 1797904
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v8, v5}, LX/186;->a(III)V

    .line 1797905
    :cond_d
    invoke-virtual {p1}, LX/186;->d()I

    move-result v5

    goto/16 :goto_4

    :cond_e
    move v4, v5

    move v7, v5

    move v8, v5

    move v9, v5

    move v10, v5

    goto :goto_5
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 1797906
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1797907
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1797908
    if-eqz v0, :cond_0

    .line 1797909
    const-string v1, "locale"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1797910
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1797911
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1797912
    if-eqz v0, :cond_5

    .line 1797913
    const-string v1, "video_caption_items"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1797914
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1797915
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_4

    .line 1797916
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    const/4 p3, 0x0

    .line 1797917
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1797918
    invoke-virtual {p0, v2, p3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 1797919
    if-eqz v3, :cond_1

    .line 1797920
    const-string p1, "caption_text"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1797921
    invoke-virtual {p2, v3}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1797922
    :cond_1
    const/4 v3, 0x1

    invoke-virtual {p0, v2, v3, p3}, LX/15i;->a(III)I

    move-result v3

    .line 1797923
    if-eqz v3, :cond_2

    .line 1797924
    const-string p1, "end_time"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1797925
    invoke-virtual {p2, v3}, LX/0nX;->b(I)V

    .line 1797926
    :cond_2
    const/4 v3, 0x2

    invoke-virtual {p0, v2, v3, p3}, LX/15i;->a(III)I

    move-result v3

    .line 1797927
    if-eqz v3, :cond_3

    .line 1797928
    const-string p1, "start_time"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1797929
    invoke-virtual {p2, v3}, LX/0nX;->b(I)V

    .line 1797930
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1797931
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1797932
    :cond_4
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1797933
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1797934
    return-void
.end method
