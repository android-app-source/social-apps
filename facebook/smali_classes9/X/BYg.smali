.class public LX/BYg;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0ad;

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Zb;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0ad;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0ad;",
            "LX/0Ot",
            "<",
            "LX/0Zb;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1796441
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1796442
    iput-object p1, p0, LX/BYg;->a:LX/0ad;

    .line 1796443
    iput-object p2, p0, LX/BYg;->b:LX/0Ot;

    .line 1796444
    return-void
.end method

.method public static a(LX/0QB;)LX/BYg;
    .locals 1

    .prologue
    .line 1796445
    invoke-static {p0}, LX/BYg;->b(LX/0QB;)LX/BYg;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/BYg;
    .locals 3

    .prologue
    .line 1796446
    new-instance v1, LX/BYg;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v0

    check-cast v0, LX/0ad;

    const/16 v2, 0xbc

    invoke-static {p0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    invoke-direct {v1, v0, v2}, LX/BYg;-><init>(LX/0ad;LX/0Ot;)V

    .line 1796447
    return-object v1
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    .line 1796448
    iget-object v0, p0, LX/BYg;->a:LX/0ad;

    sget-short v1, LX/BYe;->e:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method
