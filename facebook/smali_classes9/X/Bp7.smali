.class public LX/Bp7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Bp6;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:LX/20h;

.field private final c:Ljava/util/concurrent/Callable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Callable",
            "<",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;>;"
        }
    .end annotation
.end field

.field private final d:LX/03V;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1822611
    const-class v0, LX/Bp7;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Bp7;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/20h;Ljava/util/concurrent/Callable;LX/03V;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/20h;",
            "Ljava/util/concurrent/Callable",
            "<",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;>;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1822612
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1822613
    iput-object p1, p0, LX/Bp7;->b:LX/20h;

    .line 1822614
    iput-object p2, p0, LX/Bp7;->c:Ljava/util/concurrent/Callable;

    .line 1822615
    iput-object p3, p0, LX/Bp7;->d:LX/03V;

    .line 1822616
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLFeedback;LX/1zt;LX/0Ve;)V
    .locals 4

    .prologue
    .line 1822617
    :try_start_0
    new-instance v0, LX/21A;

    invoke-direct {v0}, LX/21A;-><init>()V

    const-string v1, "photos_feed"

    .line 1822618
    iput-object v1, v0, LX/21A;->c:Ljava/lang/String;

    .line 1822619
    move-object v0, v0

    .line 1822620
    const-string v1, "photos_feed_ufi"

    .line 1822621
    iput-object v1, v0, LX/21A;->b:Ljava/lang/String;

    .line 1822622
    move-object v1, v0

    .line 1822623
    iget-object v0, p0, LX/Bp7;->c:Ljava/util/concurrent/Callable;

    invoke-interface {v0}, Ljava/util/concurrent/Callable;->call()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v0

    .line 1822624
    iput-object v0, v1, LX/21A;->a:LX/162;

    .line 1822625
    move-object v0, v1

    .line 1822626
    invoke-virtual {v0}, LX/21A;->b()Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-result-object v0

    .line 1822627
    iget-object v1, p0, LX/Bp7;->b:LX/20h;

    invoke-virtual {v1, p1, p2, v0, p3}, LX/20h;->a(Lcom/facebook/graphql/model/GraphQLFeedback;LX/1zt;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;LX/0Ve;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1822628
    :goto_0
    return-void

    .line 1822629
    :catch_0
    move-exception v0

    .line 1822630
    iget-object v1, p0, LX/Bp7;->d:LX/03V;

    sget-object v2, LX/Bp7;->a:Ljava/lang/String;

    const-string v3, "mStoryCallable threw an exception"

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
