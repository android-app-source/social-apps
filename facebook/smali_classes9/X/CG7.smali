.class public final LX/CG7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field public final synthetic a:LX/CG8;


# direct methods
.method public constructor <init>(LX/CG8;)V
    .locals 0

    .prologue
    .line 1864912
    iput-object p1, p0, LX/CG7;->a:LX/CG8;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onGlobalLayout()V
    .locals 2

    .prologue
    .line 1864913
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_0

    .line 1864914
    iget-object v0, p0, LX/CG7;->a:LX/CG8;

    iget-object v0, v0, LX/CG8;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 1864915
    :goto_0
    iget-object v0, p0, LX/CG7;->a:LX/CG8;

    invoke-static {v0}, LX/CG8;->a$redex0(LX/CG8;)V

    .line 1864916
    return-void

    .line 1864917
    :cond_0
    iget-object v0, p0, LX/CG7;->a:LX/CG8;

    iget-object v0, v0, LX/CG8;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    goto :goto_0
.end method
