.class public LX/AVL;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public b:LX/7Rn;

.field public c:LX/7Re;

.field public d:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1679227
    const-class v0, LX/AVL;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/AVL;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1679226
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final b()I
    .locals 1

    .prologue
    .line 1679228
    iget-object v0, p0, LX/AVL;->c:LX/7Re;

    .line 1679229
    iget p0, v0, LX/7Re;->a:I

    move v0, p0

    .line 1679230
    return v0
.end method

.method public final d()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1679215
    iget-object v0, p0, LX/AVL;->b:LX/7Rn;

    if-eqz v0, :cond_0

    .line 1679216
    :try_start_0
    iget-object v0, p0, LX/AVL;->b:LX/7Rn;

    invoke-virtual {v0}, LX/7Rm;->b()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1679217
    :goto_0
    :try_start_1
    iget-object v0, p0, LX/AVL;->b:LX/7Rn;

    invoke-virtual {v0}, LX/7Rn;->d()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1679218
    iput-object v3, p0, LX/AVL;->b:LX/7Rn;

    .line 1679219
    :cond_0
    :goto_1
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/AVL;->d:Z

    .line 1679220
    return-void

    .line 1679221
    :catch_0
    move-exception v0

    .line 1679222
    sget-object v1, LX/AVL;->a:Ljava/lang/String;

    const-string v2, "Failed to make the encoder input surface current."

    invoke-static {v1, v2, v0}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1679223
    :catch_1
    move-exception v0

    .line 1679224
    :try_start_2
    sget-object v1, LX/AVL;->a:Ljava/lang/String;

    const-string v2, "Failed to release the encoder input surface current."

    invoke-static {v1, v2, v0}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1679225
    iput-object v3, p0, LX/AVL;->b:LX/7Rn;

    goto :goto_1

    :catchall_0
    move-exception v0

    iput-object v3, p0, LX/AVL;->b:LX/7Rn;

    throw v0
.end method
