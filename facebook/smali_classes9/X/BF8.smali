.class public final LX/BF8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Ve;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6Ve",
        "<",
        "LX/8pz;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/BF9;


# direct methods
.method public constructor <init>(LX/BF9;)V
    .locals 0

    .prologue
    .line 1765412
    iput-object p1, p0, LX/BF8;->a:LX/BF9;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 1765413
    check-cast p1, LX/8pz;

    const/4 v1, 0x0

    .line 1765414
    iget-object v0, p1, LX/8pw;->a:Lcom/facebook/graphql/model/GraphQLComment;

    move-object v3, v0

    .line 1765415
    invoke-static {v3}, LX/36l;->i(Lcom/facebook/graphql/model/GraphQLComment;)LX/0Px;

    move-result-object v4

    .line 1765416
    if-nez v4, :cond_0

    .line 1765417
    :goto_0
    return-void

    .line 1765418
    :cond_0
    iget-object v0, p0, LX/BF8;->a:LX/BF9;

    iget-object v0, v0, LX/BF9;->d:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1765419
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move-object v2, v0

    :goto_1
    if-ge v1, v5, :cond_1

    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    .line 1765420
    invoke-static {v2, v0, v3}, LX/6Ou;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/graphql/model/GraphQLPage;Lcom/facebook/graphql/model/GraphQLComment;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v2

    .line 1765421
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1765422
    :cond_1
    iget-object v0, p0, LX/BF8;->a:LX/BF9;

    iget-object v1, p0, LX/BF8;->a:LX/BF9;

    iget-object v1, v1, LX/BF9;->d:LX/0Px;

    invoke-static {v1, v2}, LX/6Ot;->a(Ljava/util/List;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/0Px;

    move-result-object v1

    .line 1765423
    iput-object v1, v0, LX/BF9;->d:LX/0Px;

    .line 1765424
    iget-object v0, p0, LX/BF8;->a:LX/BF9;

    iget-object v0, v0, LX/BF9;->a:LX/0QK;

    iget-object v1, p0, LX/BF8;->a:LX/BF9;

    iget-object v1, v1, LX/BF9;->d:LX/0Px;

    invoke-interface {v0, v1}, LX/0QK;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
