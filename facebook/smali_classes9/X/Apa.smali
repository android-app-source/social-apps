.class public LX/Apa;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pp;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static a:Z

.field private static b:I

.field private static c:I

.field private static d:I

.field private static f:LX/0Xm;


# instance fields
.field private final e:LX/1nu;


# direct methods
.method public constructor <init>(LX/1nu;LX/1DR;)V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/high16 v3, 0x41a00000    # 20.0f

    .line 1716323
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1716324
    iput-object p1, p0, LX/Apa;->e:LX/1nu;

    .line 1716325
    sget-boolean v0, LX/Apa;->a:Z

    if-nez v0, :cond_0

    .line 1716326
    const/4 v0, 0x1

    sput-boolean v0, LX/Apa;->a:Z

    .line 1716327
    invoke-virtual {p2}, LX/1DR;->a()I

    move-result v0

    add-int/lit8 v0, v0, -0xc

    .line 1716328
    int-to-float v1, v0

    const v2, 0x3f570a3e    # 0.84000003f

    mul-float/2addr v1, v2

    sub-float/2addr v1, v3

    float-to-int v1, v1

    sput v1, LX/Apa;->b:I

    .line 1716329
    int-to-float v1, v0

    const v2, 0x3f46e979    # 0.777f

    mul-float/2addr v1, v2

    sub-float/2addr v1, v3

    float-to-int v1, v1

    sput v1, LX/Apa;->c:I

    .line 1716330
    int-to-float v0, v0

    const v1, 0x3f2e147b    # 0.68f

    mul-float/2addr v0, v1

    sub-float/2addr v0, v3

    float-to-int v0, v0

    sput v0, LX/Apa;->d:I

    .line 1716331
    :cond_0
    return-void
.end method

.method public static a(LX/0QB;)LX/Apa;
    .locals 5

    .prologue
    .line 1716332
    const-class v1, LX/Apa;

    monitor-enter v1

    .line 1716333
    :try_start_0
    sget-object v0, LX/Apa;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1716334
    sput-object v2, LX/Apa;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1716335
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1716336
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1716337
    new-instance p0, LX/Apa;

    invoke-static {v0}, LX/1nu;->a(LX/0QB;)LX/1nu;

    move-result-object v3

    check-cast v3, LX/1nu;

    invoke-static {v0}, LX/1DR;->a(LX/0QB;)LX/1DR;

    move-result-object v4

    check-cast v4, LX/1DR;

    invoke-direct {p0, v3, v4}, LX/Apa;-><init>(LX/1nu;LX/1DR;)V

    .line 1716338
    move-object v0, p0

    .line 1716339
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1716340
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Apa;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1716341
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1716342
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;ILjava/lang/CharSequence;Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;LX/1Po;LX/33B;Ljava/lang/CharSequence;Ljava/lang/CharSequence;LX/1dQ;LX/1X1;LX/1X1;Z)LX/1Dg;
    .locals 5
    .param p2    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation

        .annotation build Lcom/facebook/fig/components/hscroll/annotations/FigHscrollType;
        .end annotation
    .end param
    .param p3    # Ljava/lang/CharSequence;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->STRING:LX/32B;
        .end annotation
    .end param
    .param p4    # Landroid/net/Uri;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p5    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p6    # LX/1Po;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p7    # LX/33B;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p8    # Ljava/lang/CharSequence;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->STRING:LX/32B;
        .end annotation
    .end param
    .param p9    # Ljava/lang/CharSequence;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->STRING:LX/32B;
        .end annotation
    .end param
    .param p10    # LX/1dQ;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p11    # LX/1X1;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p12    # LX/1X1;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p13    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "I",
            "Ljava/lang/CharSequence;",
            "Landroid/net/Uri;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "TE;",
            "LX/33B;",
            "Ljava/lang/CharSequence;",
            "Ljava/lang/CharSequence;",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;",
            "LX/1X1",
            "<",
            "LX/Apj;",
            ">;",
            "LX/1X1",
            "<",
            "LX/Aph;",
            ">;Z)",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    .line 1716343
    packed-switch p2, :pswitch_data_0

    .line 1716344
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unsupported H-Scroll type="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0zj;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1716345
    :pswitch_0
    sget v2, LX/Apa;->b:I

    .line 1716346
    const v1, 0x3ff47ae1    # 1.91f

    .line 1716347
    :goto_0
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {v3, v4}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x4

    invoke-interface {v3, v4}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v2}, LX/1Dh;->F(I)LX/1Dh;

    move-result-object v2

    if-nez p4, :cond_0

    const/4 v1, 0x0

    :goto_1
    invoke-interface {v2, v1}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v1

    invoke-static {p1}, LX/ApS;->c(LX/1De;)LX/ApQ;

    move-result-object v2

    invoke-virtual {v2, p2}, LX/ApQ;->h(I)LX/ApQ;

    move-result-object v2

    invoke-virtual {v2, p3}, LX/ApQ;->a(Ljava/lang/CharSequence;)LX/ApQ;

    move-result-object v2

    invoke-virtual {v2, p8}, LX/ApQ;->b(Ljava/lang/CharSequence;)LX/ApQ;

    move-result-object v2

    invoke-virtual {v2, p9}, LX/ApQ;->c(Ljava/lang/CharSequence;)LX/ApQ;

    move-result-object v2

    invoke-virtual {v2, p10}, LX/ApQ;->a(LX/1dQ;)LX/ApQ;

    move-result-object v2

    move-object/from16 v0, p12

    invoke-virtual {v2, v0}, LX/ApQ;->a(LX/1X1;)LX/ApQ;

    move-result-object v2

    move-object/from16 v0, p11

    invoke-virtual {v2, v0}, LX/ApQ;->b(LX/1X1;)LX/ApQ;

    move-result-object v2

    move/from16 v0, p13

    invoke-virtual {v2, v0}, LX/ApQ;->a(Z)LX/ApQ;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    return-object v1

    .line 1716348
    :pswitch_1
    sget v2, LX/Apa;->c:I

    .line 1716349
    const/high16 v1, 0x3f800000    # 1.0f

    .line 1716350
    goto :goto_0

    .line 1716351
    :pswitch_2
    sget v2, LX/Apa;->d:I

    .line 1716352
    const v1, 0x3ff47ae1    # 1.91f

    .line 1716353
    goto :goto_0

    .line 1716354
    :pswitch_3
    sget v2, LX/Apa;->d:I

    .line 1716355
    const/high16 v1, 0x3f800000    # 1.0f

    .line 1716356
    goto :goto_0

    .line 1716357
    :cond_0
    iget-object v3, p0, LX/Apa;->e:LX/1nu;

    invoke-virtual {v3, p1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object v3

    check-cast p6, LX/1Pp;

    invoke-virtual {v3, p6}, LX/1nw;->a(LX/1Pp;)LX/1nw;

    move-result-object v3

    invoke-virtual {v3, p4}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object v3

    invoke-virtual {v3, p5}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object v3

    invoke-virtual {v3, p7}, LX/1nw;->a(LX/33B;)LX/1nw;

    move-result-object v3

    invoke-virtual {v3, v1}, LX/1nw;->c(F)LX/1nw;

    move-result-object v1

    sget-object v3, LX/1Up;->c:LX/1Up;

    invoke-virtual {v1, v3}, LX/1nw;->a(LX/1Up;)LX/1nw;

    move-result-object v1

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
