.class public final LX/Beo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Vj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Vj",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel;",
        ">;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Bep;


# direct methods
.method public constructor <init>(LX/Bep;)V
    .locals 0

    .prologue
    .line 1805388
    iput-object p1, p0, LX/Beo;->a:LX/Bep;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 7

    .prologue
    .line 1805389
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1805390
    if-eqz p1, :cond_0

    .line 1805391
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1805392
    if-eqz v0, :cond_0

    .line 1805393
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1805394
    check-cast v0, Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel;->a()Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel$ResultsModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1805395
    :cond_0
    iget-object v0, p0, LX/Beo;->a:LX/Bep;

    iget-object v0, v0, LX/Bep;->c:LX/03V;

    const-string v1, "crowdsourcing_graph_editor_question_store"

    const-string v2, "Unexpected null result received from MGE cards query"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1805396
    const/4 v0, 0x0

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1805397
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, LX/Beo;->a:LX/Bep;

    iget-object v1, v0, LX/Bep;->f:LX/Ber;

    .line 1805398
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1805399
    check-cast v0, Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel;->a()Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel$ResultsModel;

    move-result-object v0

    const/4 v3, 0x0

    .line 1805400
    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel$ResultsModel;->k()Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel$ResultsModel$PageInfoModel;

    move-result-object v2

    if-nez v2, :cond_2

    .line 1805401
    iget-object v2, v1, LX/Ber;->a:LX/03V;

    const-string v4, "crowdsourcing_graph_editor_question_store_state"

    const-string v5, "Received null page info"

    invoke-virtual {v2, v4, v5}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1805402
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel$ResultsModel;->a()I

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel$ResultsModel;->j()LX/0Px;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel$ResultsModel;->j()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_7

    :cond_3
    const/4 v2, 0x1

    :goto_1
    move v2, v2

    .line 1805403
    iput-boolean v2, v1, LX/Ber;->d:Z

    .line 1805404
    iget-boolean v2, v1, LX/Ber;->d:Z

    if-nez v2, :cond_6

    .line 1805405
    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel$ResultsModel;->k()Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel$ResultsModel$PageInfoModel;

    move-result-object v2

    if-nez v2, :cond_4

    const/4 v2, 0x0

    :goto_2
    iput-object v2, v1, LX/Ber;->e:Ljava/lang/String;

    .line 1805406
    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel$ResultsModel;->j()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v4, v3

    :goto_3
    if-ge v4, v6, :cond_6

    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;

    .line 1805407
    invoke-static {v2}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;->c(Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 1805408
    iget-object p0, v1, LX/Ber;->b:Ljava/util/ArrayList;

    invoke-virtual {p0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1805409
    add-int/lit8 v2, v3, 0x1

    .line 1805410
    :goto_4
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    move v3, v2

    goto :goto_3

    .line 1805411
    :cond_4
    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel$ResultsModel;->k()Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel$ResultsModel$PageInfoModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/crowdsourcing/protocol/graphql/GraphEditorQueriesModels$GraphEditorQuestionsQueryModel$ResultsModel$PageInfoModel;->a()Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    .line 1805412
    :cond_5
    iget-object v2, v1, LX/Ber;->a:LX/03V;

    const-string p0, "crowdsourcing_graph_editor_question_store_state"

    const-string p1, "Received invalid card"

    invoke-virtual {v2, p0, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v2, v3

    goto :goto_4

    .line 1805413
    :cond_6
    move v0, v3

    .line 1805414
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto/16 :goto_0

    :cond_7
    const/4 v2, 0x0

    goto :goto_1
.end method
