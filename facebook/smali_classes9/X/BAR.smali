.class public final LX/BAR;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public b:LX/BAT;

.field public c:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

.field public d:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1753044
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;->FALLBACK:Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/BAR;->a:LX/0Px;

    return-void
.end method

.method private constructor <init>(LX/BAT;Lcom/facebook/graphql/model/GraphQLTextWithEntities;)V
    .locals 1

    .prologue
    .line 1753045
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1753046
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1753047
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1753048
    iput-object p1, p0, LX/BAR;->b:LX/BAT;

    .line 1753049
    iput-object p2, p0, LX/BAR;->c:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1753050
    sget-object v0, LX/BAR;->a:LX/0Px;

    iput-object v0, p0, LX/BAR;->h:LX/0Px;

    .line 1753051
    return-void
.end method

.method public constructor <init>(LX/BAT;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1753052
    new-instance v0, LX/173;

    invoke-direct {v0}, LX/173;-><init>()V

    .line 1753053
    iput-object p2, v0, LX/173;->f:Ljava/lang/String;

    .line 1753054
    move-object v0, v0

    .line 1753055
    invoke-virtual {v0}, LX/173;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-direct {p0, p1, v0}, LX/BAR;-><init>(LX/BAT;Lcom/facebook/graphql/model/GraphQLTextWithEntities;)V

    .line 1753056
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1753057
    return-void
.end method
