.class public final LX/BUk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/resources/ui/FbTextView;

.field public final synthetic b:LX/BUn;


# direct methods
.method public constructor <init>(LX/BUn;Lcom/facebook/resources/ui/FbTextView;)V
    .locals 0

    .prologue
    .line 1789554
    iput-object p1, p0, LX/BUk;->b:LX/BUn;

    iput-object p2, p0, LX/BUk;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x2

    const v2, 0x69c339be

    invoke-static {v1, v0, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1789555
    iget-object v2, p0, LX/BUk;->b:LX/BUn;

    iget-object v3, p0, LX/BUk;->b:LX/BUn;

    iget-boolean v3, v3, LX/BUn;->f:Z

    if-nez v3, :cond_1

    .line 1789556
    :goto_0
    iput-boolean v0, v2, LX/BUn;->f:Z

    .line 1789557
    iget-object v0, p0, LX/BUk;->b:LX/BUn;

    iget-boolean v0, v0, LX/BUn;->f:Z

    if-eqz v0, :cond_2

    .line 1789558
    iget-object v0, p0, LX/BUk;->a:Lcom/facebook/resources/ui/FbTextView;

    iget-object v2, p0, LX/BUk;->b:LX/BUn;

    iget-object v2, v2, LX/BUn;->c:LX/BUo;

    iget-object v2, v2, LX/BUo;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f081a32

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1789559
    :goto_1
    iget-object v0, p0, LX/BUk;->b:LX/BUn;

    iget-object v0, v0, LX/BUn;->c:LX/BUo;

    iget-object v0, v0, LX/BUo;->d:LX/Aek;

    if-eqz v0, :cond_0

    .line 1789560
    iget-object v0, p0, LX/BUk;->b:LX/BUn;

    iget-object v0, v0, LX/BUn;->c:LX/BUo;

    iget-object v0, v0, LX/BUo;->d:LX/Aek;

    iget-object v2, p0, LX/BUk;->b:LX/BUn;

    iget-boolean v2, v2, LX/BUn;->f:Z

    invoke-interface {v0, v2}, LX/Aek;->a(Z)V

    .line 1789561
    :cond_0
    const v0, 0x77a6e968

    invoke-static {v0, v1}, LX/02F;->a(II)V

    return-void

    .line 1789562
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1789563
    :cond_2
    iget-object v0, p0, LX/BUk;->a:Lcom/facebook/resources/ui/FbTextView;

    iget-object v2, p0, LX/BUk;->b:LX/BUn;

    iget-object v2, v2, LX/BUn;->c:LX/BUo;

    iget-object v2, v2, LX/BUo;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f081a31    # 1.80911E38f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method
