.class public final LX/Bx6;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/Bx8;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

.field public c:LX/1Pm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public final synthetic d:LX/Bx8;


# direct methods
.method public constructor <init>(LX/Bx8;)V
    .locals 1

    .prologue
    .line 1834945
    iput-object p1, p0, LX/Bx6;->d:LX/Bx8;

    .line 1834946
    move-object v0, p1

    .line 1834947
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1834948
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1834949
    const-string v0, "ArticleChainingItemFooterComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1834950
    if-ne p0, p1, :cond_1

    .line 1834951
    :cond_0
    :goto_0
    return v0

    .line 1834952
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1834953
    goto :goto_0

    .line 1834954
    :cond_3
    check-cast p1, LX/Bx6;

    .line 1834955
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1834956
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1834957
    if-eq v2, v3, :cond_0

    .line 1834958
    iget-object v2, p0, LX/Bx6;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/Bx6;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/Bx6;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1834959
    goto :goto_0

    .line 1834960
    :cond_5
    iget-object v2, p1, LX/Bx6;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 1834961
    :cond_6
    iget-object v2, p0, LX/Bx6;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/Bx6;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    iget-object v3, p1, LX/Bx6;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v2, v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1834962
    goto :goto_0

    .line 1834963
    :cond_8
    iget-object v2, p1, LX/Bx6;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    if-nez v2, :cond_7

    .line 1834964
    :cond_9
    iget-object v2, p0, LX/Bx6;->c:LX/1Pm;

    if-eqz v2, :cond_a

    iget-object v2, p0, LX/Bx6;->c:LX/1Pm;

    iget-object v3, p1, LX/Bx6;->c:LX/1Pm;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1834965
    goto :goto_0

    .line 1834966
    :cond_a
    iget-object v2, p1, LX/Bx6;->c:LX/1Pm;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
