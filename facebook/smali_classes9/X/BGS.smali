.class public LX/BGS;
.super LX/Ale;
.source ""

# interfaces
.implements LX/AkB;
.implements LX/AkM;


# instance fields
.field public b:LX/BGR;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private c:Z

.field private d:Z

.field private e:Lcom/facebook/photos/creativeediting/swipeable/prompt/FramePromptScrollAdapter;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1767110
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/BGS;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1767111
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1767108
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/BGS;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1767109
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1767105
    invoke-direct {p0, p1, p2, p3}, LX/Ale;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1767106
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/BGS;->c:Z

    .line 1767107
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p1, LX/BGS;

    const-class p0, LX/BGR;

    invoke-interface {v1, p0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/BGR;

    iput-object v1, p1, LX/BGS;->b:LX/BGR;

    return-void
.end method


# virtual methods
.method public final a()Landroid/view/View;
    .locals 0

    .prologue
    .line 1767104
    return-object p0
.end method

.method public final a(LX/1RN;)V
    .locals 11

    .prologue
    .line 1767097
    iget-object v0, p0, LX/BGS;->b:LX/BGR;

    invoke-virtual {p0}, LX/BGS;->getImageTrayHeight()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 1767098
    new-instance v2, Lcom/facebook/photos/creativeediting/swipeable/prompt/FramePromptScrollAdapter;

    invoke-static {v0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v5

    check-cast v5, LX/1Ad;

    invoke-static {v0}, LX/8GN;->b(LX/0QB;)LX/8GN;

    move-result-object v6

    check-cast v6, LX/8GN;

    invoke-static {v0}, LX/BMK;->b(LX/0QB;)LX/BMK;

    move-result-object v7

    check-cast v7, LX/BMK;

    invoke-static {v0}, LX/BMP;->b(LX/0QB;)LX/BMP;

    move-result-object v8

    check-cast v8, LX/BMP;

    invoke-static {v0}, LX/B5l;->b(LX/0QB;)LX/B5l;

    move-result-object v9

    check-cast v9, LX/B5l;

    invoke-static {v0}, LX/1E1;->b(LX/0QB;)LX/1E1;

    move-result-object v10

    check-cast v10, LX/1E1;

    move-object v3, v1

    move-object v4, p1

    invoke-direct/range {v2 .. v10}, Lcom/facebook/photos/creativeediting/swipeable/prompt/FramePromptScrollAdapter;-><init>(Ljava/lang/Integer;LX/1RN;LX/1Ad;LX/8GN;LX/BMK;LX/BMP;LX/B5l;LX/1E1;)V

    .line 1767099
    move-object v0, v2

    .line 1767100
    iput-object v0, p0, LX/BGS;->e:Lcom/facebook/photos/creativeediting/swipeable/prompt/FramePromptScrollAdapter;

    .line 1767101
    iget-object v0, p0, LX/Ale;->b:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    move-object v0, v0

    .line 1767102
    iget-object v1, p0, LX/BGS;->e:Lcom/facebook/photos/creativeediting/swipeable/prompt/FramePromptScrollAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 1767103
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1767094
    const-class v0, LX/BGS;

    invoke-static {v0, p0}, LX/BGS;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1767095
    invoke-super {p0}, LX/Ale;->b()V

    .line 1767096
    return-void
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 1767093
    const/4 v0, 0x0

    return v0
.end method

.method public getCollapseAnimator()Landroid/animation/Animator;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1767091
    iget-object v0, p0, LX/Ale;->e:Landroid/animation/ValueAnimator;

    move-object v0, v0

    .line 1767092
    return-object v0
.end method

.method public getExpandAnimator()Landroid/animation/Animator;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1767089
    iget-object v0, p0, LX/Ale;->d:Landroid/animation/ValueAnimator;

    move-object v0, v0

    .line 1767090
    return-object v0
.end method

.method public getImageTrayHeight()I
    .locals 2

    .prologue
    .line 1767088
    invoke-virtual {p0}, LX/BGS;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0c16

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 1767087
    iget-boolean v0, p0, LX/BGS;->d:Z

    return v0
.end method

.method public final i()Z
    .locals 1

    .prologue
    .line 1767086
    iget-boolean v0, p0, LX/BGS;->c:Z

    return v0
.end method

.method public setHasBeenShown(Z)V
    .locals 0

    .prologue
    .line 1767084
    iput-boolean p1, p0, LX/BGS;->d:Z

    .line 1767085
    return-void
.end method

.method public setIsAnimationRunning(Z)V
    .locals 0

    .prologue
    .line 1767082
    iput-boolean p1, p0, LX/BGS;->c:Z

    .line 1767083
    return-void
.end method
