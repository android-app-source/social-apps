.class public LX/Bp3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Bp2;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:LX/1EN;

.field private c:Ljava/util/concurrent/Callable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Callable",
            "<",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;>;"
        }
    .end annotation
.end field

.field private d:LX/03V;

.field private e:LX/1Qt;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1822591
    const-class v0, LX/Bp3;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Bp3;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/1EN;Ljava/util/concurrent/Callable;LX/03V;LX/1Qt;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1EN;",
            "Ljava/util/concurrent/Callable",
            "<",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;>;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/1Qt;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1822581
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1822582
    iput-object p1, p0, LX/Bp3;->b:LX/1EN;

    .line 1822583
    iput-object p2, p0, LX/Bp3;->c:Ljava/util/concurrent/Callable;

    .line 1822584
    iput-object p3, p0, LX/Bp3;->d:LX/03V;

    .line 1822585
    iput-object p4, p0, LX/Bp3;->e:LX/1Qt;

    .line 1822586
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLFeedback;Landroid/view/View;LX/An0;)V
    .locals 7

    .prologue
    .line 1822587
    :try_start_0
    iget-object v0, p0, LX/Bp3;->b:LX/1EN;

    iget-object v1, p0, LX/Bp3;->c:Ljava/util/concurrent/Callable;

    invoke-interface {v1}, Ljava/util/concurrent/Callable;->call()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p0, LX/Bp3;->e:LX/1Qt;

    sget-object v6, LX/1EO;->PHOTOS_FEED:LX/1EO;

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-virtual/range {v0 .. v6}, LX/1EN;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Qt;Lcom/facebook/graphql/model/GraphQLFeedback;Landroid/view/View;LX/An0;LX/1EO;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1822588
    :goto_0
    return-void

    .line 1822589
    :catch_0
    move-exception v0

    .line 1822590
    iget-object v1, p0, LX/Bp3;->d:LX/03V;

    sget-object v2, LX/Bp3;->a:Ljava/lang/String;

    const-string v3, "mStoryCallable threw an exception"

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
