.class public final LX/AjD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/feed/protocol/FetchAggregatedStoryGraphQLModels$AggregatedStorySubstoryQueryModel$AllSubstoriesModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLStory;

.field public final synthetic b:I

.field public final synthetic c:LX/AjG;


# direct methods
.method public constructor <init>(LX/AjG;Lcom/facebook/graphql/model/GraphQLStory;I)V
    .locals 0

    .prologue
    .line 1707673
    iput-object p1, p0, LX/AjD;->c:LX/AjG;

    iput-object p2, p0, LX/AjD;->a:Lcom/facebook/graphql/model/GraphQLStory;

    iput p3, p0, LX/AjD;->b:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 6

    .prologue
    .line 1707674
    iget-object v0, p0, LX/AjD;->c:LX/AjG;

    iget-object v1, p0, LX/AjD;->a:Lcom/facebook/graphql/model/GraphQLStory;

    iget v2, p0, LX/AjD;->b:I

    .line 1707675
    new-instance v3, LX/808;

    invoke-direct {v3}, LX/808;-><init>()V

    move-object v3, v3

    .line 1707676
    const-string v4, "node_id"

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v4

    const-string v5, "after_cursor"

    invoke-static {v1}, LX/16y;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;->k()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPageInfo;->a()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v4, v5, p0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v4

    const-string v5, "num_substories"

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v4, v5, p0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1707677
    iget-object v4, v0, LX/AjG;->b:LX/0tX;

    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    invoke-virtual {v4, v3}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v3

    .line 1707678
    new-instance v4, LX/AjF;

    invoke-direct {v4, v0}, LX/AjF;-><init>(LX/AjG;)V

    iget-object v5, v0, LX/AjG;->d:Ljava/util/concurrent/ExecutorService;

    invoke-static {v3, v4, v5}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    move-object v0, v3

    .line 1707679
    return-object v0
.end method
