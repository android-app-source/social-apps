.class public final LX/CPg;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/CPh;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/CNb;

.field public b:LX/CNc;

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/CNb;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1884776
    invoke-static {}, LX/CPh;->q()LX/CPh;

    move-result-object v0

    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1884777
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1884775
    const-string v0, "NTRatioLayoutComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1884758
    if-ne p0, p1, :cond_1

    .line 1884759
    :cond_0
    :goto_0
    return v0

    .line 1884760
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1884761
    goto :goto_0

    .line 1884762
    :cond_3
    check-cast p1, LX/CPg;

    .line 1884763
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1884764
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1884765
    if-eq v2, v3, :cond_0

    .line 1884766
    iget-object v2, p0, LX/CPg;->a:LX/CNb;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/CPg;->a:LX/CNb;

    iget-object v3, p1, LX/CPg;->a:LX/CNb;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1884767
    goto :goto_0

    .line 1884768
    :cond_5
    iget-object v2, p1, LX/CPg;->a:LX/CNb;

    if-nez v2, :cond_4

    .line 1884769
    :cond_6
    iget-object v2, p0, LX/CPg;->b:LX/CNc;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/CPg;->b:LX/CNc;

    iget-object v3, p1, LX/CPg;->b:LX/CNc;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1884770
    goto :goto_0

    .line 1884771
    :cond_8
    iget-object v2, p1, LX/CPg;->b:LX/CNc;

    if-nez v2, :cond_7

    .line 1884772
    :cond_9
    iget-object v2, p0, LX/CPg;->c:Ljava/util/List;

    if-eqz v2, :cond_a

    iget-object v2, p0, LX/CPg;->c:Ljava/util/List;

    iget-object v3, p1, LX/CPg;->c:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1884773
    goto :goto_0

    .line 1884774
    :cond_a
    iget-object v2, p1, LX/CPg;->c:Ljava/util/List;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
