.class public final LX/Axn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Axt;


# direct methods
.method public constructor <init>(LX/Axt;)V
    .locals 0

    .prologue
    .line 1728857
    iput-object p1, p0, LX/Axn;->a:LX/Axt;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1728858
    check-cast p1, Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;

    .line 1728859
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1728860
    invoke-virtual {p1}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;->a()Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
