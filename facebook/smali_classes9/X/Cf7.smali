.class public LX/Cf7;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/Cf1;

.field private final c:Landroid/content/Context;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1925559
    const-class v0, LX/Cf7;

    sput-object v0, LX/Cf7;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/Cf1;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1925560
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1925561
    iput-object p1, p0, LX/Cf7;->c:Landroid/content/Context;

    .line 1925562
    iput-object p2, p0, LX/Cf7;->b:LX/Cf1;

    .line 1925563
    return-void
.end method

.method public static b(LX/0QB;)LX/Cf7;
    .locals 3

    .prologue
    .line 1925564
    new-instance v2, LX/Cf7;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/Cf1;->a(LX/0QB;)LX/Cf1;

    move-result-object v1

    check-cast v1, LX/Cf1;

    invoke-direct {v2, v0, v1}, LX/Cf7;-><init>(Landroid/content/Context;LX/Cf1;)V

    .line 1925565
    return-object v2
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 1925566
    iget-object v0, p0, LX/Cf7;->b:LX/Cf1;

    iget-object v0, v0, LX/1qk;->a:LX/1ql;

    invoke-virtual {v0}, LX/1ql;->c()V

    .line 1925567
    :try_start_0
    iget-object v0, p0, LX/Cf7;->c:Landroid/content/Context;

    const-class v1, Lcom/facebook/push/nna/NNAService;

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 1925568
    iget-object v0, p0, LX/Cf7;->c:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1925569
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 1925570
    :goto_0
    if-nez v0, :cond_0

    .line 1925571
    iget-object v0, p0, LX/Cf7;->b:LX/Cf1;

    iget-object v0, v0, LX/1qk;->a:LX/1ql;

    invoke-virtual {v0}, LX/1ql;->d()V

    .line 1925572
    sget-object v0, LX/Cf7;->a:Ljava/lang/Class;

    const-string v1, "Failed to start service"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 1925573
    :cond_0
    return-void

    .line 1925574
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1925575
    :catchall_0
    move-exception v0

    .line 1925576
    iget-object v1, p0, LX/Cf7;->b:LX/Cf1;

    iget-object v1, v1, LX/1qk;->a:LX/1ql;

    invoke-virtual {v1}, LX/1ql;->d()V

    .line 1925577
    sget-object v1, LX/Cf7;->a:Ljava/lang/Class;

    const-string v2, "Failed to start service"

    invoke-static {v1, v2}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    throw v0
.end method
