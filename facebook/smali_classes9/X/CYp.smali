.class public final LX/CYp;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1912827
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 1912828
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1912829
    :goto_0
    return v1

    .line 1912830
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1912831
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 1912832
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1912833
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1912834
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 1912835
    const-string v4, "focus"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1912836
    invoke-static {p0, p1}, LX/CYn;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 1912837
    :cond_2
    const-string v4, "photo"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1912838
    invoke-static {p0, p1}, LX/CYo;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 1912839
    :cond_3
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1912840
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1912841
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1912842
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    move v2, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 8

    .prologue
    .line 1912843
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1912844
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1912845
    if-eqz v0, :cond_2

    .line 1912846
    const-string v1, "focus"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1912847
    const-wide/16 v6, 0x0

    .line 1912848
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1912849
    const/4 v2, 0x0

    invoke-virtual {p0, v0, v2, v6, v7}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 1912850
    cmpl-double v4, v2, v6

    if-eqz v4, :cond_0

    .line 1912851
    const-string v4, "x"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1912852
    invoke-virtual {p2, v2, v3}, LX/0nX;->a(D)V

    .line 1912853
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {p0, v0, v2, v6, v7}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 1912854
    cmpl-double v4, v2, v6

    if-eqz v4, :cond_1

    .line 1912855
    const-string v4, "y"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1912856
    invoke-virtual {p2, v2, v3}, LX/0nX;->a(D)V

    .line 1912857
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1912858
    :cond_2
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1912859
    if-eqz v0, :cond_3

    .line 1912860
    const-string v1, "photo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1912861
    invoke-static {p0, v0, p2, p3}, LX/CYo;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1912862
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1912863
    return-void
.end method
