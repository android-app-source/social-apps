.class public final LX/Abv;
.super LX/3Gy;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/3Gy",
        "<",
        "LX/Adf;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Aby;

.field private b:Z


# direct methods
.method public constructor <init>(LX/Aby;)V
    .locals 1

    .prologue
    .line 1691603
    iput-object p1, p0, LX/Abv;->a:LX/Aby;

    invoke-direct {p0}, LX/3Gy;-><init>()V

    .line 1691604
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Abv;->b:Z

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/Adf;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1691605
    const-class v0, LX/Adf;

    return-object v0
.end method

.method public final b(LX/0b7;)V
    .locals 4

    .prologue
    .line 1691606
    check-cast p1, LX/Adf;

    .line 1691607
    iget-object v0, p0, LX/Abv;->a:LX/Aby;

    iget v1, p1, LX/Adf;->a:I

    .line 1691608
    iput v1, v0, LX/Aby;->O:I

    .line 1691609
    iget-boolean v0, p0, LX/Abv;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Abv;->a:LX/Aby;

    iget v0, v0, LX/Aby;->Q:I

    if-lez v0, :cond_0

    iget-object v0, p0, LX/Abv;->a:LX/Aby;

    iget v0, v0, LX/Aby;->O:I

    iget-object v1, p0, LX/Abv;->a:LX/Aby;

    iget v1, v1, LX/Aby;->Q:I

    if-lt v0, v1, :cond_0

    iget-object v0, p0, LX/Abv;->a:LX/Aby;

    iget-object v0, v0, LX/Aby;->b:LX/1b4;

    .line 1691610
    iget-object v1, v0, LX/1b4;->b:LX/0Uh;

    const/16 v2, 0x66a

    const/4 p1, 0x0

    invoke-virtual {v1, v2, p1}, LX/0Uh;->a(IZ)Z

    move-result v1

    move v0, v1

    .line 1691611
    if-eqz v0, :cond_0

    .line 1691612
    iget-object v0, p0, LX/Abv;->a:LX/Aby;

    iget-object v0, v0, LX/Aby;->a:LX/AcX;

    iget-object v1, p0, LX/Abv;->a:LX/Aby;

    iget v1, v1, LX/Aby;->Q:I

    .line 1691613
    iget-object v2, v0, LX/AcX;->n:LX/AeS;

    new-instance v3, LX/AfR;

    sget-object p1, LX/AfQ;->LIVE_COMMERCIAL_BREAK_REACH_VIEWER_COUNT:LX/AfQ;

    invoke-direct {v3, p1, v1}, LX/AfR;-><init>(LX/AfQ;I)V

    invoke-virtual {v2, v3}, LX/AeS;->a(LX/AeO;)V

    .line 1691614
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Abv;->b:Z

    .line 1691615
    :cond_0
    return-void
.end method
