.class public LX/Ax9;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/1S3;"
    }
.end annotation


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/AxC;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Ax9",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/AxC;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1727602
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1727603
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/Ax9;->b:LX/0Zi;

    .line 1727604
    iput-object p1, p0, LX/Ax9;->a:LX/0Ot;

    .line 1727605
    return-void
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 3

    .prologue
    .line 1727606
    check-cast p2, LX/Ax8;

    .line 1727607
    iget-object v0, p0, LX/Ax9;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AxC;

    iget-object v1, p2, LX/Ax8;->a:LX/1Pq;

    iget-object v2, p2, LX/Ax8;->b:LX/1Ri;

    invoke-virtual {v0, p1, v1, v2}, LX/AxC;->a(LX/1De;LX/1Pq;LX/1Ri;)LX/1Dg;

    move-result-object v0

    .line 1727608
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1727609
    invoke-static {}, LX/1dS;->b()V

    .line 1727610
    const/4 v0, 0x0

    return-object v0
.end method
