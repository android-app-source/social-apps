.class public final LX/CPZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/CPX;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation


# static fields
.field private static final a:LX/0Zk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zk",
            "<",
            "Landroid/graphics/drawable/GradientDrawable;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1884633
    new-instance v0, LX/0Zi;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/CPZ;->a:LX/0Zk;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1884634
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 1884635
    sget-object v0, LX/CPZ;->a:LX/0Zk;

    invoke-interface {v0}, LX/0Zk;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/GradientDrawable;

    .line 1884636
    if-nez v0, :cond_0

    .line 1884637
    invoke-static {p1, p2}, LX/CPa;->b(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)Landroid/graphics/drawable/GradientDrawable;

    move-result-object v0

    .line 1884638
    :goto_0
    return-object v0

    .line 1884639
    :cond_0
    invoke-virtual {v0}, Landroid/graphics/drawable/GradientDrawable;->mutate()Landroid/graphics/drawable/Drawable;

    .line 1884640
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/GradientDrawable;->setOrientation(Landroid/graphics/drawable/GradientDrawable$Orientation;)V

    .line 1884641
    invoke-virtual {v0, p2}, Landroid/graphics/drawable/GradientDrawable;->setColors([I)V

    goto :goto_0
.end method

.method public final a(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 1884642
    sget-object v0, LX/CPZ;->a:LX/0Zk;

    check-cast p1, Landroid/graphics/drawable/GradientDrawable;

    invoke-interface {v0, p1}, LX/0Zk;->a(Ljava/lang/Object;)Z

    .line 1884643
    return-void
.end method
