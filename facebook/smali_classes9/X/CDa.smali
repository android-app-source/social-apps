.class public LX/CDa;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/CDY;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CDc;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1859769
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/CDa;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/CDc;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1859770
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1859771
    iput-object p1, p0, LX/CDa;->b:LX/0Ot;

    .line 1859772
    return-void
.end method

.method public static a(LX/0QB;)LX/CDa;
    .locals 4

    .prologue
    .line 1859773
    const-class v1, LX/CDa;

    monitor-enter v1

    .line 1859774
    :try_start_0
    sget-object v0, LX/CDa;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1859775
    sput-object v2, LX/CDa;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1859776
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1859777
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1859778
    new-instance v3, LX/CDa;

    const/16 p0, 0x21ef

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/CDa;-><init>(LX/0Ot;)V

    .line 1859779
    move-object v0, v3

    .line 1859780
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1859781
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/CDa;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1859782
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1859783
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1859791
    invoke-static {}, LX/1dS;->b()V

    .line 1859792
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/1De;LX/1Dg;IILX/1no;LX/1X1;)V
    .locals 6

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x6900ca4b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1859784
    check-cast p6, LX/CDZ;

    .line 1859785
    iget-object v1, p0, LX/CDa;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v1, p6, LX/CDZ;->a:LX/2pa;

    .line 1859786
    iget-wide v4, v1, LX/2pa;->d:D

    double-to-float v4, v4

    invoke-static {p3, p4, v4, p5}, LX/1oC;->a(IIFLX/1no;)V

    .line 1859787
    const/16 v1, 0x1f

    const v2, 0x541d137b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(LX/1De;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1859788
    iget-object v0, p0, LX/CDa;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    .line 1859789
    new-instance v0, LX/CDW;

    invoke-direct {v0, p1}, LX/CDW;-><init>(Landroid/content/Context;)V

    move-object v0, v0

    .line 1859790
    return-object v0
.end method

.method public final b(LX/1De;LX/1X1;)V
    .locals 6

    .prologue
    .line 1859748
    check-cast p2, LX/CDZ;

    .line 1859749
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v5

    .line 1859750
    iget-object v0, p0, LX/CDa;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CDc;

    iget-object v1, p2, LX/CDZ;->a:LX/2pa;

    iget-object v2, p2, LX/CDZ;->b:LX/3Ge;

    iget-boolean v3, p2, LX/CDZ;->c:Z

    iget-object v4, p2, LX/CDZ;->d:LX/0Px;

    .line 1859751
    invoke-virtual {v2, v1}, LX/3Ge;->a(LX/2pa;)V

    .line 1859752
    if-eqz v3, :cond_0

    .line 1859753
    iget-object p0, v0, LX/CDc;->e:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/3Fb;

    iget-object p1, v1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object p1, p1, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    invoke-virtual {p0, v4, p1}, LX/3Fb;->a(LX/0Px;Ljava/lang/String;)LX/3Fa;

    move-result-object p0

    .line 1859754
    iput-object p0, v5, LX/1np;->a:Ljava/lang/Object;

    .line 1859755
    :cond_0
    invoke-virtual {v1}, LX/2pa;->a()Z

    move-result p0

    if-eqz p0, :cond_1

    iget-boolean p0, v0, LX/CDc;->b:Z

    if-eqz p0, :cond_1

    .line 1859756
    iget-object p0, v0, LX/CDc;->i:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/Bwf;

    invoke-virtual {p0, v1}, LX/Bwf;->a(LX/2pa;)V

    .line 1859757
    :cond_1
    iget-object v0, v5, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 1859758
    check-cast v0, LX/3Fa;

    iput-object v0, p2, LX/CDZ;->e:LX/3Fa;

    .line 1859759
    invoke-static {v5}, LX/1cy;->a(LX/1np;)V

    .line 1859760
    return-void
.end method

.method public final c(LX/1De;)LX/CDY;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1859761
    new-instance v1, LX/CDZ;

    invoke-direct {v1, p0}, LX/CDZ;-><init>(LX/CDa;)V

    .line 1859762
    sget-object v2, LX/CDa;->a:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/CDY;

    .line 1859763
    if-nez v2, :cond_0

    .line 1859764
    new-instance v2, LX/CDY;

    invoke-direct {v2}, LX/CDY;-><init>()V

    .line 1859765
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/CDY;->a$redex0(LX/CDY;LX/1De;IILX/CDZ;)V

    .line 1859766
    move-object v1, v2

    .line 1859767
    move-object v0, v1

    .line 1859768
    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1859735
    const/4 v0, 0x1

    return v0
.end method

.method public final e(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 20

    .prologue
    .line 1859732
    check-cast p3, LX/CDZ;

    .line 1859733
    move-object/from16 v0, p0

    iget-object v1, v0, LX/CDa;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/CDc;

    move-object/from16 v2, p2

    check-cast v2, LX/CDW;

    move-object/from16 v0, p3

    iget-object v3, v0, LX/CDZ;->a:LX/2pa;

    move-object/from16 v0, p3

    iget-object v4, v0, LX/CDZ;->f:LX/CDg;

    move-object/from16 v0, p3

    iget-object v5, v0, LX/CDZ;->b:LX/3Ge;

    move-object/from16 v0, p3

    iget-object v6, v0, LX/CDZ;->g:LX/04D;

    move-object/from16 v0, p3

    iget-object v7, v0, LX/CDZ;->h:LX/04H;

    move-object/from16 v0, p3

    iget-object v8, v0, LX/CDZ;->i:LX/3It;

    move-object/from16 v0, p3

    iget-object v9, v0, LX/CDZ;->j:LX/2oL;

    move-object/from16 v0, p3

    iget-object v10, v0, LX/CDZ;->k:LX/093;

    move-object/from16 v0, p3

    iget-object v11, v0, LX/CDZ;->l:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    move-object/from16 v0, p3

    iget-object v12, v0, LX/CDZ;->m:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-object/from16 v0, p3

    iget-object v13, v0, LX/CDZ;->n:LX/1Pe;

    move-object/from16 v0, p3

    iget-boolean v14, v0, LX/CDZ;->c:Z

    move-object/from16 v0, p3

    iget-boolean v15, v0, LX/CDZ;->o:Z

    move-object/from16 v0, p3

    iget-boolean v0, v0, LX/CDZ;->p:Z

    move/from16 v16, v0

    move-object/from16 v0, p3

    iget v0, v0, LX/CDZ;->q:I

    move/from16 v17, v0

    move-object/from16 v0, p3

    iget-object v0, v0, LX/CDZ;->r:LX/3Iv;

    move-object/from16 v18, v0

    move-object/from16 v0, p3

    iget-object v0, v0, LX/CDZ;->e:LX/3Fa;

    move-object/from16 v19, v0

    invoke-virtual/range {v1 .. v19}, LX/CDc;->a(LX/CDW;LX/2pa;LX/CDg;LX/3Ge;LX/04D;LX/04H;LX/3It;LX/2oL;LX/093;Lcom/facebook/video/analytics/VideoFeedStoryInfo;Lcom/facebook/graphql/model/GraphQLStoryAttachment;LX/1Pe;ZZZILX/3Iv;LX/3Fa;)V

    .line 1859734
    return-void
.end method

.method public final f()LX/1mv;
    .locals 1

    .prologue
    .line 1859747
    sget-object v0, LX/1mv;->VIEW:LX/1mv;

    return-object v0
.end method

.method public final f(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 7

    .prologue
    .line 1859736
    check-cast p3, LX/CDZ;

    .line 1859737
    iget-object v0, p0, LX/CDa;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CDc;

    move-object v1, p2

    check-cast v1, LX/CDW;

    iget-object v2, p3, LX/CDZ;->f:LX/CDg;

    iget-object v3, p3, LX/CDZ;->m:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    iget-object v4, p3, LX/CDZ;->n:LX/1Pe;

    iget-boolean v5, p3, LX/CDZ;->c:Z

    iget-object v6, p3, LX/CDZ;->e:LX/3Fa;

    invoke-virtual/range {v0 .. v6}, LX/CDc;->a(LX/CDW;LX/CDg;Lcom/facebook/graphql/model/GraphQLStoryAttachment;LX/1Pe;ZLX/3Fa;)V

    .line 1859738
    return-void
.end method

.method public final g(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 1

    .prologue
    .line 1859739
    iget-object v0, p0, LX/CDa;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CDc;

    .line 1859740
    iget-object p0, v0, LX/CDc;->c:LX/198;

    invoke-static {}, LX/3JE;->c()LX/3JE;

    move-result-object p1

    invoke-virtual {p0, p1}, LX/198;->c(LX/1YD;)V

    .line 1859741
    const-string p0, "RichVideoPlayerComponentSpec.onBind"

    const p1, -0x28c7e1b6

    invoke-static {p0, p1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 1859742
    const p0, 0xed3b08c

    invoke-static {p0}, LX/02m;->a(I)V

    .line 1859743
    return-void
.end method

.method public final h(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 1

    .prologue
    .line 1859744
    iget-object v0, p0, LX/CDa;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    .line 1859745
    return-void
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 1859746
    const/4 v0, 0x3

    return v0
.end method
