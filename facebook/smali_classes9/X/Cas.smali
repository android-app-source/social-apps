.class public final LX/Cas;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel;

.field public final synthetic b:LX/Cau;


# direct methods
.method public constructor <init>(LX/Cau;Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel;)V
    .locals 0

    .prologue
    .line 1918663
    iput-object p1, p0, LX/Cas;->b:LX/Cau;

    iput-object p2, p0, LX/Cas;->a:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1918664
    iget-object v0, p0, LX/Cas;->b:LX/Cau;

    iget-object v0, v0, LX/Cau;->a:LX/Caz;

    iget-object v0, v0, LX/Caz;->d:LX/9hh;

    iget-object v1, p0, LX/Cas;->b:LX/Cau;

    iget-object v1, v1, LX/Cau;->a:LX/Caz;

    iget-object v1, v1, LX/Caz;->q:LX/5kD;

    invoke-interface {v1}, LX/5kD;->d()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/Cas;->a:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel;

    invoke-virtual {v2}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel;->b()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/9hh;->a(Ljava/lang/String;Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
