.class public final LX/C1f;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/C1g;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/1Pq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public c:Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

.field public final synthetic d:LX/C1g;


# direct methods
.method public constructor <init>(LX/C1g;)V
    .locals 1

    .prologue
    .line 1842783
    iput-object p1, p0, LX/C1f;->d:LX/C1g;

    .line 1842784
    move-object v0, p1

    .line 1842785
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1842786
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1842807
    const-string v0, "ScheduledLiveAttachmentComponent"

    return-object v0
.end method

.method public final a(LX/1X1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1",
            "<",
            "LX/C1g;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1842804
    check-cast p1, LX/C1f;

    .line 1842805
    iget-object v0, p1, LX/C1f;->c:Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    iput-object v0, p0, LX/C1f;->c:Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    .line 1842806
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1842790
    if-ne p0, p1, :cond_1

    .line 1842791
    :cond_0
    :goto_0
    return v0

    .line 1842792
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1842793
    goto :goto_0

    .line 1842794
    :cond_3
    check-cast p1, LX/C1f;

    .line 1842795
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1842796
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1842797
    if-eq v2, v3, :cond_0

    .line 1842798
    iget-object v2, p0, LX/C1f;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/C1f;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/C1f;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1842799
    goto :goto_0

    .line 1842800
    :cond_5
    iget-object v2, p1, LX/C1f;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 1842801
    :cond_6
    iget-object v2, p0, LX/C1f;->b:LX/1Pq;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/C1f;->b:LX/1Pq;

    iget-object v3, p1, LX/C1f;->b:LX/1Pq;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1842802
    goto :goto_0

    .line 1842803
    :cond_7
    iget-object v2, p1, LX/C1f;->b:LX/1Pq;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public final g()LX/1X1;
    .locals 2

    .prologue
    .line 1842787
    invoke-super {p0}, LX/1X1;->g()LX/1X1;

    move-result-object v0

    check-cast v0, LX/C1f;

    .line 1842788
    const/4 v1, 0x0

    iput-object v1, v0, LX/C1f;->c:Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    .line 1842789
    return-object v0
.end method
