.class public final LX/C4v;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

.field public final synthetic b:Lcom/facebook/graphql/model/GraphQLCustomizedStory;

.field public final synthetic c:Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueLightweightHeaderPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueLightweightHeaderPartDefinition;Lcom/facebook/graphql/model/GraphQLStoryActionLink;Lcom/facebook/graphql/model/GraphQLCustomizedStory;)V
    .locals 0

    .prologue
    .line 1848096
    iput-object p1, p0, LX/C4v;->c:Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueLightweightHeaderPartDefinition;

    iput-object p2, p0, LX/C4v;->a:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    iput-object p3, p0, LX/C4v;->b:Lcom/facebook/graphql/model/GraphQLCustomizedStory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x3da96dc4

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1848097
    iget-object v0, p0, LX/C4v;->c:Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueLightweightHeaderPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueLightweightHeaderPartDefinition;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2mJ;

    iget-object v2, p0, LX/C4v;->a:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bc()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/2mJ;->a(Landroid/net/Uri;)V

    .line 1848098
    iget-object v0, p0, LX/C4v;->c:Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueLightweightHeaderPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/goodwill/dailydialogue/DailyDialogueLightweightHeaderPartDefinition;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Cn;

    iget-object v2, p0, LX/C4v;->b:Lcom/facebook/graphql/model/GraphQLCustomizedStory;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLCustomizedStory;->c()Ljava/lang/String;

    move-result-object v2

    const-string v3, "open_url"

    .line 1848099
    new-instance p0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object p1, LX/3lw;->DAILYDIALOGUE_LIGHTWEIGHT_HEADER_TAPPED:LX/3lw;

    iget-object p1, p1, LX/3lw;->name:Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p1, "goodwill"

    .line 1848100
    iput-object p1, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1848101
    move-object p0, p0

    .line 1848102
    const-string p1, "tracking"

    invoke-virtual {p0, p1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string p1, "action"

    invoke-virtual {p0, p1, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    .line 1848103
    iget-object p1, v0, LX/1Cn;->a:LX/0Zb;

    invoke-interface {p1, p0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1848104
    const v0, -0x69823751

    invoke-static {v4, v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
