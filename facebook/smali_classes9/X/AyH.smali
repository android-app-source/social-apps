.class public LX/AyH;
.super LX/1qS;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/AyH;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Tt;LX/1qU;LX/AyO;)V
    .locals 6
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1729918
    invoke-static {p4}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v4

    const-string v5, "souvenirs_db"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, LX/1qS;-><init>(Landroid/content/Context;LX/0Tt;LX/1qU;LX/0Px;Ljava/lang/String;)V

    .line 1729919
    return-void
.end method

.method public static a(LX/0QB;)LX/AyH;
    .locals 7

    .prologue
    .line 1729920
    sget-object v0, LX/AyH;->a:LX/AyH;

    if-nez v0, :cond_1

    .line 1729921
    const-class v1, LX/AyH;

    monitor-enter v1

    .line 1729922
    :try_start_0
    sget-object v0, LX/AyH;->a:LX/AyH;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1729923
    if-eqz v2, :cond_0

    .line 1729924
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1729925
    new-instance p0, LX/AyH;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0Ts;->a(LX/0QB;)LX/0Ts;

    move-result-object v4

    check-cast v4, LX/0Tt;

    invoke-static {v0}, LX/1qT;->a(LX/0QB;)LX/1qT;

    move-result-object v5

    check-cast v5, LX/1qU;

    invoke-static {v0}, LX/AyO;->a(LX/0QB;)LX/AyO;

    move-result-object v6

    check-cast v6, LX/AyO;

    invoke-direct {p0, v3, v4, v5, v6}, LX/AyH;-><init>(Landroid/content/Context;LX/0Tt;LX/1qU;LX/AyO;)V

    .line 1729926
    move-object v0, p0

    .line 1729927
    sput-object v0, LX/AyH;->a:LX/AyH;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1729928
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1729929
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1729930
    :cond_1
    sget-object v0, LX/AyH;->a:LX/AyH;

    return-object v0

    .line 1729931
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1729932
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
