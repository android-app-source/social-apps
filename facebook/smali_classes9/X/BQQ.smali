.class public LX/BQQ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final f:Ljava/lang/Object;


# instance fields
.field private final a:LX/BQT;

.field private final b:LX/BQP;

.field private final c:LX/0Uh;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1782124
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/BQQ;->f:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/BQT;LX/BQP;LX/0Uh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1782180
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1782181
    iput-object p1, p0, LX/BQQ;->a:LX/BQT;

    .line 1782182
    iput-object p2, p0, LX/BQQ;->b:LX/BQP;

    .line 1782183
    iput-object p3, p0, LX/BQQ;->c:LX/0Uh;

    .line 1782184
    return-void
.end method

.method public static a(LX/0QB;)LX/BQQ;
    .locals 9

    .prologue
    .line 1782151
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 1782152
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 1782153
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 1782154
    if-nez v1, :cond_0

    .line 1782155
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1782156
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 1782157
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 1782158
    sget-object v1, LX/BQQ;->f:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 1782159
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 1782160
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1782161
    :cond_1
    if-nez v1, :cond_4

    .line 1782162
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 1782163
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 1782164
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 1782165
    new-instance p0, LX/BQQ;

    invoke-static {v0}, LX/BQT;->b(LX/0QB;)LX/BQT;

    move-result-object v1

    check-cast v1, LX/BQT;

    invoke-static {v0}, LX/BQP;->a(LX/0QB;)LX/BQP;

    move-result-object v7

    check-cast v7, LX/BQP;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v8

    check-cast v8, LX/0Uh;

    invoke-direct {p0, v1, v7, v8}, LX/BQQ;-><init>(LX/BQT;LX/BQP;LX/0Uh;)V

    .line 1782166
    move-object v1, p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1782167
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 1782168
    if-nez v1, :cond_2

    .line 1782169
    sget-object v0, LX/BQQ;->f:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BQQ;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1782170
    :goto_1
    if-eqz v0, :cond_3

    .line 1782171
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 1782172
    :goto_3
    check-cast v0, LX/BQQ;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 1782173
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 1782174
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 1782175
    :catchall_1
    move-exception v0

    .line 1782176
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 1782177
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 1782178
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 1782179
    :cond_2
    :try_start_8
    sget-object v0, LX/BQQ;->f:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BQQ;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method public final a(Landroid/net/Uri;Ljava/lang/String;)V
    .locals 2
    .param p1    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1782144
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1782145
    if-eqz p1, :cond_0

    iget-object v0, p0, LX/BQQ;->c:LX/0Uh;

    const/16 v1, 0x250

    invoke-virtual {v0, v1}, LX/0Uh;->a(I)LX/03R;

    move-result-object v0

    sget-object v1, LX/03R;->YES:LX/03R;

    if-ne v0, v1, :cond_0

    .line 1782146
    iput-object p2, p0, LX/BQQ;->d:Ljava/lang/String;

    .line 1782147
    iget-object v0, p0, LX/BQQ;->a:LX/BQT;

    .line 1782148
    const-string v1, "fb4aOptimisticProfilePicture"

    invoke-static {v0, v1, p1}, LX/BQT;->a(LX/BQT;Ljava/lang/String;Landroid/net/Uri;)V

    .line 1782149
    iget-object v0, p0, LX/BQQ;->b:LX/BQP;

    invoke-virtual {v0}, LX/BQP;->a()V

    .line 1782150
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1782138
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1782139
    iget-object v0, p0, LX/BQQ;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1782140
    const/4 v0, 0x0

    iput-object v0, p0, LX/BQQ;->d:Ljava/lang/String;

    .line 1782141
    iget-object v0, p0, LX/BQQ;->a:LX/BQT;

    invoke-virtual {v0}, LX/BQT;->b()V

    .line 1782142
    iget-object v0, p0, LX/BQQ;->b:LX/BQP;

    invoke-virtual {v0}, LX/BQP;->a()V

    .line 1782143
    :cond_0
    return-void
.end method

.method public final b(Landroid/net/Uri;Ljava/lang/String;)V
    .locals 2
    .param p1    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1782131
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1782132
    if-eqz p1, :cond_0

    iget-object v0, p0, LX/BQQ;->c:LX/0Uh;

    const/16 v1, 0x24c

    invoke-virtual {v0, v1}, LX/0Uh;->a(I)LX/03R;

    move-result-object v0

    sget-object v1, LX/03R;->YES:LX/03R;

    if-ne v0, v1, :cond_0

    .line 1782133
    iput-object p2, p0, LX/BQQ;->e:Ljava/lang/String;

    .line 1782134
    iget-object v0, p0, LX/BQQ;->a:LX/BQT;

    .line 1782135
    const-string v1, "fb4aOptimisticCoverPhoto"

    invoke-static {v0, v1, p1}, LX/BQT;->a(LX/BQT;Ljava/lang/String;Landroid/net/Uri;)V

    .line 1782136
    iget-object v0, p0, LX/BQQ;->b:LX/BQP;

    invoke-virtual {v0}, LX/BQP;->c()V

    .line 1782137
    :cond_0
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1782125
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1782126
    iget-object v0, p0, LX/BQQ;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1782127
    const/4 v0, 0x0

    iput-object v0, p0, LX/BQQ;->e:Ljava/lang/String;

    .line 1782128
    iget-object v0, p0, LX/BQQ;->a:LX/BQT;

    invoke-virtual {v0}, LX/BQT;->d()V

    .line 1782129
    iget-object v0, p0, LX/BQQ;->b:LX/BQP;

    invoke-virtual {v0}, LX/BQP;->c()V

    .line 1782130
    :cond_0
    return-void
.end method
