.class public LX/AUg;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "LX/AUI;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# instance fields
.field private final a:LX/AUf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/AUg",
            "<TT;>.DispatchHandler;"
        }
    .end annotation
.end field

.field private final b:Landroid/os/Looper;

.field private c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/AUK",
            "<TT;>;>;"
        }
    .end annotation
.end field

.field private d:Z


# direct methods
.method public constructor <init>(Landroid/os/Looper;)V
    .locals 1

    .prologue
    .line 1678311
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1678312
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/AUg;->c:Ljava/util/ArrayList;

    .line 1678313
    new-instance v0, LX/AUf;

    invoke-direct {v0, p0, p1}, LX/AUf;-><init>(LX/AUg;Landroid/os/Looper;)V

    iput-object v0, p0, LX/AUg;->a:LX/AUf;

    .line 1678314
    iput-object p1, p0, LX/AUg;->b:Landroid/os/Looper;

    .line 1678315
    return-void
.end method

.method private declared-synchronized a()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "LX/AUK",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 1678306
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/AUg;->d:Z

    if-eqz v0, :cond_0

    .line 1678307
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Nested synchronous dispatching is not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1678308
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1678309
    :cond_0
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, LX/AUg;->d:Z

    .line 1678310
    iget-object v0, p0, LX/AUg;->c:Ljava/util/ArrayList;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object v0
.end method

.method private declared-synchronized a(Ljava/util/ArrayList;LX/AUK;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "LX/AUK",
            "<TT;>;>;",
            "LX/AUK",
            "<TT;>;)Z"
        }
    .end annotation

    .prologue
    .line 1678305
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/AUg;->c:Ljava/util/ArrayList;

    if-eq v0, p1, :cond_0

    iget-object v0, p0, LX/AUg;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized b()V
    .locals 1

    .prologue
    .line 1678302
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, LX/AUg;->d:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1678303
    monitor-exit p0

    return-void

    .line 1678304
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private c()V
    .locals 2

    .prologue
    .line 1678299
    iget-boolean v0, p0, LX/AUg;->d:Z

    if-eqz v0, :cond_0

    .line 1678300
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, LX/AUg;->c:Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, LX/AUg;->c:Ljava/util/ArrayList;

    .line 1678301
    :cond_0
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(LX/AUK;)I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/AUK",
            "<TT;>;)I"
        }
    .end annotation

    .prologue
    .line 1678266
    monitor-enter p0

    if-nez p1, :cond_0

    .line 1678267
    :try_start_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "eventSubscriber is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1678268
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1678269
    :cond_0
    :try_start_1
    invoke-direct {p0}, LX/AUg;->c()V

    .line 1678270
    iget-object v0, p0, LX/AUg;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1678271
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Subscriber "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is already subscribed"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1678272
    :cond_1
    iget-object v0, p0, LX/AUg;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1678273
    iget-object v0, p0, LX/AUg;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    monitor-exit p0

    return v0
.end method

.method public final a(LX/AUI;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 1678286
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    .line 1678287
    iget-object v1, p0, LX/AUg;->b:Landroid/os/Looper;

    if-eq v0, v1, :cond_0

    .line 1678288
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Cannot dispatch event from looper thread "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "; expected "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, LX/AUg;->b:Landroid/os/Looper;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1678289
    :cond_0
    invoke-direct {p0}, LX/AUg;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 1678290
    :try_start_0
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 1678291
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    .line 1678292
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AUK;

    .line 1678293
    invoke-direct {p0, v2, v0}, LX/AUg;->a(Ljava/util/ArrayList;LX/AUK;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1678294
    invoke-interface {v0, p1}, LX/AUK;->a(LX/AUI;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1678295
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1678296
    :cond_2
    invoke-direct {p0}, LX/AUg;->b()V

    .line 1678297
    return-void

    .line 1678298
    :catchall_0
    move-exception v0

    invoke-direct {p0}, LX/AUg;->b()V

    throw v0
.end method

.method public final declared-synchronized b(LX/AUK;)I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/AUK",
            "<TT;>;)I"
        }
    .end annotation

    .prologue
    .line 1678277
    monitor-enter p0

    if-nez p1, :cond_0

    .line 1678278
    :try_start_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "eventSubscriber is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1678279
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1678280
    :cond_0
    :try_start_1
    invoke-direct {p0}, LX/AUg;->c()V

    .line 1678281
    iget-object v0, p0, LX/AUg;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 1678282
    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 1678283
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Subscriber "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " was not registered"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1678284
    :cond_1
    iget-object v1, p0, LX/AUg;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 1678285
    iget-object v0, p0, LX/AUg;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    monitor-exit p0

    return v0
.end method

.method public final b(LX/AUI;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)Z"
        }
    .end annotation

    .prologue
    .line 1678274
    iget-object v0, p0, LX/AUg;->a:LX/AUf;

    .line 1678275
    const/4 p0, 0x1

    invoke-virtual {v0, p0, p1}, LX/AUf;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object p0

    invoke-virtual {v0, p0}, LX/AUf;->sendMessage(Landroid/os/Message;)Z

    move-result p0

    move v0, p0

    .line 1678276
    return v0
.end method
