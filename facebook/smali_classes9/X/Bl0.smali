.class public LX/Bl0;
.super LX/0Tv;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/Bl0;


# direct methods
.method public constructor <init>()V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1815262
    const-string v0, "events_common_part"

    const/16 v1, 0x20

    new-instance v2, LX/Bkz;

    invoke-direct {v2}, LX/Bkz;-><init>()V

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, LX/0Tv;-><init>(Ljava/lang/String;ILX/0Px;)V

    .line 1815263
    return-void
.end method

.method public static a(LX/0QB;)LX/Bl0;
    .locals 3

    .prologue
    .line 1815264
    sget-object v0, LX/Bl0;->a:LX/Bl0;

    if-nez v0, :cond_1

    .line 1815265
    const-class v1, LX/Bl0;

    monitor-enter v1

    .line 1815266
    :try_start_0
    sget-object v0, LX/Bl0;->a:LX/Bl0;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1815267
    if-eqz v2, :cond_0

    .line 1815268
    :try_start_1
    new-instance v0, LX/Bl0;

    invoke-direct {v0}, LX/Bl0;-><init>()V

    .line 1815269
    move-object v0, v0

    .line 1815270
    sput-object v0, LX/Bl0;->a:LX/Bl0;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1815271
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1815272
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1815273
    :cond_1
    sget-object v0, LX/Bl0;->a:LX/Bl0;

    return-object v0

    .line 1815274
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1815275
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 6

    .prologue
    .line 1815276
    invoke-super {p0, p1}, LX/0Tv;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1815277
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1815278
    const-string v0, "CREATE INDEX IF NOT EXISTS events_start_time_idx ON %s(%s)"

    new-array v1, v5, [Ljava/lang/Object;

    const-string v2, "events"

    aput-object v2, v1, v3

    sget-object v2, LX/Bkw;->I:LX/0U1;

    .line 1815279
    iget-object p0, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, p0

    .line 1815280
    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const v1, -0x42cce227

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x2e8661e4

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1815281
    const-string v0, "CREATE INDEX IF NOT EXISTS events_by_fbid_idx ON %s(%s)"

    new-array v1, v5, [Ljava/lang/Object;

    const-string v2, "events"

    aput-object v2, v1, v3

    sget-object v2, LX/Bkw;->b:LX/0U1;

    .line 1815282
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 1815283
    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const v1, 0xc819f5

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x3151a496

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1815284
    return-void
.end method

.method public final a(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 4

    .prologue
    .line 1815285
    const/16 v0, 0x1e

    if-ge p2, v0, :cond_1

    .line 1815286
    invoke-super {p0, p1, p2, p3}, LX/0Tv;->a(Landroid/database/sqlite/SQLiteDatabase;II)V

    .line 1815287
    :cond_0
    return-void

    .line 1815288
    :cond_1
    :goto_0
    if-ge p2, p3, :cond_0

    .line 1815289
    add-int/lit8 v0, p2, 0x1

    .line 1815290
    packed-switch p2, :pswitch_data_0

    .line 1815291
    :goto_1
    move p2, v0

    .line 1815292
    goto :goto_0

    .line 1815293
    :pswitch_0
    const-string v1, "ALTER TABLE %1$s ADD COLUMN %2$s %3$s"

    const-string v2, "events"

    sget-object v3, LX/Bkw;->al:LX/0U1;

    .line 1815294
    iget-object p0, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, p0

    .line 1815295
    sget-object p0, LX/Bkw;->al:LX/0U1;

    .line 1815296
    iget-object p2, p0, LX/0U1;->e:Ljava/lang/String;

    move-object p0, p2

    .line 1815297
    invoke-static {v1, v2, v3, p0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const v2, -0x302171ed

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, -0x12e2b06a

    invoke-static {v1}, LX/03h;->a(I)V

    .line 1815298
    goto :goto_1

    .line 1815299
    :pswitch_1
    const-string v1, "ALTER TABLE %1$s ADD COLUMN %2$s %3$s"

    const-string v2, "events"

    sget-object v3, LX/Bkw;->o:LX/0U1;

    .line 1815300
    iget-object p0, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, p0

    .line 1815301
    sget-object p0, LX/Bkw;->o:LX/0U1;

    .line 1815302
    iget-object p2, p0, LX/0U1;->e:Ljava/lang/String;

    move-object p0, p2

    .line 1815303
    invoke-static {v1, v2, v3, p0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const v2, -0x7deeb7f6

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, 0x27d1bdee

    invoke-static {v1}, LX/03h;->a(I)V

    .line 1815304
    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1e
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
