.class public LX/AsM;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "LX/0io;",
        "DerivedData:",
        "Ljava/lang/Object;",
        "Mutation::",
        "Lcom/facebook/ipc/composer/dataaccessor/ComposerCanSave;",
        "Services::",
        "LX/0il",
        "<TModelData;>;:",
        "LX/0ik",
        "<TDerivedData;>;:",
        "LX/0im",
        "<TMutation;>;>",
        "Ljava/lang/Object;",
        "Lcom/facebook/ipc/friendsharing/inspiration/InspirationViewController;"
    }
.end annotation


# instance fields
.field private final a:LX/AtQ;

.field public final b:LX/AvX;

.field public final c:LX/Avk;

.field public final d:LX/AwD;

.field public e:LX/AwL;

.field public final f:LX/Awd;

.field public final g:LX/Arh;

.field public final h:Landroid/app/Activity;

.field public final i:LX/Ats;

.field public final j:LX/At7;

.field public final k:LX/At3;

.field public final l:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TServices;>;"
        }
    .end annotation
.end field

.field public m:LX/AtP;

.field public n:Lcom/facebook/friendsharing/inspiration/model/InspirationModel;

.field public o:I

.field public p:I


# direct methods
.method public constructor <init>(LX/Arh;Landroid/app/Activity;Lcom/facebook/friendsharing/inspiration/model/InspirationModel;LX/Ats;LX/0il;LX/At7;LX/At3;LX/AvX;LX/Avk;LX/AwD;LX/Awd;LX/AwL;LX/AtQ;)V
    .locals 1
    .param p1    # LX/Arh;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Landroid/app/Activity;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/friendsharing/inspiration/model/InspirationModel;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/Ats;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/0il;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/friendsharing/inspiration/capture/InspirationCameraEffectsApplier;",
            "Landroid/app/Activity;",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationModel;",
            "Lcom/facebook/ipc/friendsharing/inspiration/InspirationViewController$Delegate;",
            "TServices;",
            "LX/At7;",
            "LX/At3;",
            "LX/AvX;",
            "LX/Avk;",
            "LX/AwD;",
            "LX/Awd;",
            "LX/AwL;",
            "LX/AtQ;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1720373
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1720374
    iput-object p1, p0, LX/AsM;->g:LX/Arh;

    .line 1720375
    iput-object p2, p0, LX/AsM;->h:Landroid/app/Activity;

    .line 1720376
    iput-object p6, p0, LX/AsM;->j:LX/At7;

    .line 1720377
    iput-object p7, p0, LX/AsM;->k:LX/At3;

    .line 1720378
    iput-object p8, p0, LX/AsM;->b:LX/AvX;

    .line 1720379
    iput-object p9, p0, LX/AsM;->c:LX/Avk;

    .line 1720380
    iput-object p10, p0, LX/AsM;->d:LX/AwD;

    .line 1720381
    iput-object p12, p0, LX/AsM;->e:LX/AwL;

    .line 1720382
    iput-object p11, p0, LX/AsM;->f:LX/Awd;

    .line 1720383
    iput-object p13, p0, LX/AsM;->a:LX/AtQ;

    .line 1720384
    iput-object p3, p0, LX/AsM;->n:Lcom/facebook/friendsharing/inspiration/model/InspirationModel;

    .line 1720385
    iput-object p4, p0, LX/AsM;->i:LX/Ats;

    .line 1720386
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p5}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/AsM;->l:Ljava/lang/ref/WeakReference;

    .line 1720387
    invoke-direct {p0}, LX/AsM;->c()V

    .line 1720388
    return-void
.end method

.method private c()V
    .locals 13

    .prologue
    .line 1720389
    new-instance v0, LX/AsK;

    invoke-direct {v0, p0}, LX/AsK;-><init>(LX/AsM;)V

    .line 1720390
    new-instance v1, LX/AsL;

    invoke-direct {v1, p0}, LX/AsL;-><init>(LX/AsM;)V

    .line 1720391
    iget-object v2, p0, LX/AsM;->n:Lcom/facebook/friendsharing/inspiration/model/InspirationModel;

    iget-object v3, p0, LX/AsM;->l:Ljava/lang/ref/WeakReference;

    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v3

    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .line 1720392
    iget-object v4, p0, LX/AsM;->j:LX/At7;

    .line 1720393
    new-instance v6, LX/At6;

    invoke-static {v4}, LX/86f;->a(LX/0QB;)LX/86f;

    move-result-object v5

    check-cast v5, LX/86f;

    invoke-direct {v6, v1, v5}, LX/At6;-><init>(LX/AsL;LX/86f;)V

    .line 1720394
    move-object v4, v6

    .line 1720395
    iget-object v5, p0, LX/AsM;->k:LX/At3;

    .line 1720396
    new-instance v7, LX/At2;

    invoke-static {v5}, LX/86e;->a(LX/0QB;)LX/86e;

    move-result-object v6

    check-cast v6, LX/86e;

    invoke-direct {v7, v1, v6}, LX/At2;-><init>(LX/AsL;LX/86e;)V

    .line 1720397
    move-object v5, v7

    .line 1720398
    iget-object v6, p0, LX/AsM;->b:LX/AvX;

    iget-object v7, p0, LX/AsM;->g:LX/Arh;

    invoke-virtual {v6, v7, v0, v1}, LX/AvX;->a(LX/Arh;LX/AsK;LX/AsL;)LX/AvW;

    move-result-object v6

    .line 1720399
    iget-object v7, p0, LX/AsM;->c:LX/Avk;

    iget-object v8, p0, LX/AsM;->g:LX/Arh;

    iget-object v9, p0, LX/AsM;->h:Landroid/app/Activity;

    invoke-virtual {v7, v8, v9, v1}, LX/Avk;->a(LX/Arh;Landroid/content/Context;LX/AsL;)LX/Avj;

    move-result-object v7

    .line 1720400
    iget-object v8, p0, LX/AsM;->d:LX/AwD;

    iget-object v9, p0, LX/AsM;->g:LX/Arh;

    invoke-virtual {v8, v9, v1}, LX/AwD;->a(LX/Arh;LX/AsL;)LX/AwC;

    move-result-object v8

    .line 1720401
    iget-object v9, p0, LX/AsM;->e:LX/AwL;

    iget-object v10, p0, LX/AsM;->g:LX/Arh;

    .line 1720402
    new-instance v12, LX/AwK;

    invoke-static {v9}, LX/86k;->a(LX/0QB;)LX/86k;

    move-result-object v11

    check-cast v11, LX/86k;

    invoke-direct {v12, v10, v1, v11}, LX/AwK;-><init>(LX/Arh;LX/AsL;LX/86k;)V

    .line 1720403
    const/16 v11, 0x2299

    invoke-static {v9, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    .line 1720404
    iput-object v11, v12, LX/AwK;->f:LX/0Ot;

    .line 1720405
    move-object v9, v12

    .line 1720406
    iget-object v10, p0, LX/AsM;->f:LX/Awd;

    iget-object v11, p0, LX/AsM;->g:LX/Arh;

    invoke-virtual {v10, v11, v0, v1}, LX/Awd;->a(LX/Arh;LX/AsK;LX/AsL;)LX/Awc;

    move-result-object v10

    .line 1720407
    invoke-static/range {v4 .. v10}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v6

    .line 1720408
    new-instance v7, LX/0Pz;

    invoke-direct {v7}, LX/0Pz;-><init>()V

    .line 1720409
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v8

    const/4 v4, 0x0

    move v5, v4

    :goto_0
    if-ge v5, v8, :cond_0

    invoke-virtual {v6, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/89u;

    .line 1720410
    invoke-virtual {v7, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1720411
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_0

    .line 1720412
    :cond_0
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    move-object v0, v4

    .line 1720413
    new-instance v1, LX/AtP;

    check-cast v3, LX/0il;

    invoke-direct {v1, v2, v3, v0}, LX/AtP;-><init>(Lcom/facebook/friendsharing/inspiration/model/InspirationModel;LX/0il;LX/0Px;)V

    .line 1720414
    move-object v0, v1

    .line 1720415
    iput-object v0, p0, LX/AsM;->m:LX/AtP;

    .line 1720416
    return-void
.end method
