.class public final LX/BeX;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/crowdsourcing/friendvote/graphql/CrowdsourcingFriendVoteMutationsModels$CrowdsourcingFriendVoteInviteModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/crowdsourcing/friendvote/fragment/FriendVoteInviteFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/crowdsourcing/friendvote/fragment/FriendVoteInviteFragment;)V
    .locals 0

    .prologue
    .line 1805070
    iput-object p1, p0, LX/BeX;->a:Lcom/facebook/crowdsourcing/friendvote/fragment/FriendVoteInviteFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 1805071
    iget-object v0, p0, LX/BeX;->a:Lcom/facebook/crowdsourcing/friendvote/fragment/FriendVoteInviteFragment;

    iget-object v0, v0, Lcom/facebook/crowdsourcing/friendvote/fragment/FriendVoteInviteFragment;->x:LX/0kL;

    new-instance v1, LX/27k;

    iget-object v2, p0, LX/BeX;->a:Lcom/facebook/crowdsourcing/friendvote/fragment/FriendVoteInviteFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08003c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 1805072
    iget-object v0, p0, LX/BeX;->a:Lcom/facebook/crowdsourcing/friendvote/fragment/FriendVoteInviteFragment;

    iget-object v0, v0, Lcom/facebook/crowdsourcing/friendvote/fragment/FriendVoteInviteFragment;->u:LX/03V;

    iget-object v1, p0, LX/BeX;->a:Lcom/facebook/crowdsourcing/friendvote/fragment/FriendVoteInviteFragment;

    iget-object v1, v1, Lcom/facebook/crowdsourcing/friendvote/fragment/FriendVoteInviteFragment;->y:Ljava/lang/String;

    const-string v2, "FriendVote inviter GraphQL mutation failed"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1805073
    return-void
.end method

.method public final bridge synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1805074
    return-void
.end method
