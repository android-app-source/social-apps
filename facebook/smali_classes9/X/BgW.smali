.class public LX/BgW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/BgS;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/BgS",
        "<",
        "Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsInterfaces$SuggestEditsHeader$;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/support/v4/app/Fragment;

.field private final b:LX/BgK;

.field private final c:Ljava/lang/String;

.field public d:LX/BhM;

.field public e:Lcom/facebook/crowdsourcing/suggestedits/data/SuggestEditsHeaderState;

.field public final f:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/support/v4/app/Fragment;LX/BgK;Landroid/view/View;Lcom/facebook/crowdsourcing/suggestedits/data/SuggestEditsHeaderState;Ljava/lang/String;LX/BhM;)V
    .locals 0
    .param p1    # Landroid/support/v4/app/Fragment;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/BgK;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Landroid/view/View;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/crowdsourcing/suggestedits/data/SuggestEditsHeaderState;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1807678
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1807679
    iput-object p1, p0, LX/BgW;->a:Landroid/support/v4/app/Fragment;

    .line 1807680
    iput-object p2, p0, LX/BgW;->b:LX/BgK;

    .line 1807681
    iput-object p3, p0, LX/BgW;->f:Landroid/view/View;

    .line 1807682
    iput-object p4, p0, LX/BgW;->e:Lcom/facebook/crowdsourcing/suggestedits/data/SuggestEditsHeaderState;

    .line 1807683
    iput-object p5, p0, LX/BgW;->c:Ljava/lang/String;

    .line 1807684
    iput-object p6, p0, LX/BgW;->d:LX/BhM;

    .line 1807685
    return-void
.end method

.method public static a(Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 1807672
    invoke-static {p0}, LX/Bgb;->d(Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;)Ljava/lang/String;

    move-result-object v1

    .line 1807673
    invoke-static {p1}, LX/Bgb;->d(Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;)Ljava/lang/String;

    move-result-object v2

    .line 1807674
    if-eqz v2, :cond_1

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1807675
    :cond_0
    :goto_0
    return v0

    .line 1807676
    :cond_1
    if-nez v2, :cond_2

    if-nez v1, :cond_0

    .line 1807677
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/BgW;Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;)V
    .locals 3

    .prologue
    .line 1807667
    iget-object v0, p0, LX/BgW;->e:Lcom/facebook/crowdsourcing/suggestedits/data/SuggestEditsHeaderState;

    iget-object v1, p0, LX/BgW;->e:Lcom/facebook/crowdsourcing/suggestedits/data/SuggestEditsHeaderState;

    .line 1807668
    iget-object v2, v1, Lcom/facebook/crowdsourcing/suggestedits/data/SuggestEditsHeaderState;->c:Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;

    move-object v1, v2

    .line 1807669
    iput-object v1, v0, Lcom/facebook/crowdsourcing/suggestedits/data/SuggestEditsHeaderState;->d:Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;

    .line 1807670
    invoke-virtual {p0, p1}, LX/BgW;->a(Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;)V

    .line 1807671
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1807666
    const-string v0, "header_field"

    return-object v0
.end method

.method public final a(Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;)V
    .locals 10

    .prologue
    .line 1807657
    iget-object v0, p0, LX/BgW;->e:Lcom/facebook/crowdsourcing/suggestedits/data/SuggestEditsHeaderState;

    .line 1807658
    iget-boolean v1, v0, Lcom/facebook/crowdsourcing/suggestedits/data/SuggestEditsHeaderState;->a:Z

    move v0, v1

    .line 1807659
    if-eqz v0, :cond_0

    .line 1807660
    iget-object v0, p0, LX/BgW;->b:LX/BgK;

    invoke-virtual {v0}, LX/BgK;->a()V

    .line 1807661
    iget-object v0, p0, LX/BgW;->e:Lcom/facebook/crowdsourcing/suggestedits/data/SuggestEditsHeaderState;

    .line 1807662
    iput-object p1, v0, Lcom/facebook/crowdsourcing/suggestedits/data/SuggestEditsHeaderState;->c:Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;

    .line 1807663
    :goto_0
    iget-object v0, p0, LX/BgW;->d:LX/BhM;

    iget-object v1, p0, LX/BgW;->f:Landroid/view/View;

    const/4 v3, 0x0

    sget-object v4, LX/BeE;->PAGE_HEADER:LX/BeE;

    iget-object v6, p0, LX/BgW;->b:LX/BgK;

    sget-object v7, LX/BeD;->PHOTO_PICKER:LX/BeD;

    iget-object v8, p0, LX/BgW;->a:Landroid/support/v4/app/Fragment;

    iget-object v9, p0, LX/BgW;->c:Ljava/lang/String;

    move-object v2, p0

    move-object v5, p0

    invoke-virtual/range {v0 .. v9}, LX/BhM;->a(Landroid/view/View;Ljava/lang/Object;Ljava/lang/Object;LX/BeE;LX/BgS;LX/BgK;LX/BeD;Landroid/support/v4/app/Fragment;Ljava/lang/String;)V

    .line 1807664
    return-void

    .line 1807665
    :cond_0
    new-instance v0, Lcom/facebook/crowdsourcing/suggestedits/data/SuggestEditsHeaderState;

    const/4 v1, 0x1

    invoke-direct {v0, p1, v1}, Lcom/facebook/crowdsourcing/suggestedits/data/SuggestEditsHeaderState;-><init>(Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;Z)V

    iput-object v0, p0, LX/BgW;->e:Lcom/facebook/crowdsourcing/suggestedits/data/SuggestEditsHeaderState;

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1807648
    check-cast p1, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;

    invoke-virtual {p0, p1}, LX/BgW;->a(Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;)V

    return-void
.end method

.method public final c()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;
    .locals 1

    .prologue
    .line 1807649
    iget-object v0, p0, LX/BgW;->e:Lcom/facebook/crowdsourcing/suggestedits/data/SuggestEditsHeaderState;

    .line 1807650
    iget-object p0, v0, Lcom/facebook/crowdsourcing/suggestedits/data/SuggestEditsHeaderState;->c:Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;

    move-object v0, p0

    .line 1807651
    return-object v0
.end method

.method public final e()Z
    .locals 2

    .prologue
    .line 1807652
    iget-object v0, p0, LX/BgW;->e:Lcom/facebook/crowdsourcing/suggestedits/data/SuggestEditsHeaderState;

    .line 1807653
    iget-object v1, v0, Lcom/facebook/crowdsourcing/suggestedits/data/SuggestEditsHeaderState;->b:Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;

    move-object v0, v1

    .line 1807654
    iget-object v1, p0, LX/BgW;->e:Lcom/facebook/crowdsourcing/suggestedits/data/SuggestEditsHeaderState;

    .line 1807655
    iget-object p0, v1, Lcom/facebook/crowdsourcing/suggestedits/data/SuggestEditsHeaderState;->c:Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;

    move-object v1, p0

    .line 1807656
    invoke-static {v0, v1}, LX/BgW;->a(Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;)Z

    move-result v0

    return v0
.end method
