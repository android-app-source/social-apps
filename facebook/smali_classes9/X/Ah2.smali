.class public LX/Ah2;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:I

.field private b:Z

.field public c:LX/Ags;

.field private final d:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarView;

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Agw;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/facecastdisplay/tipjar/LiveTipJarView;LX/0Ot;)V
    .locals 0
    .param p1    # Lcom/facebook/facecastdisplay/tipjar/LiveTipJarView;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/facecastdisplay/tipjar/LiveTipJarView;",
            "LX/0Ot",
            "<",
            "LX/Agw;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1702093
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1702094
    iput-object p1, p0, LX/Ah2;->d:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarView;

    .line 1702095
    iput-object p2, p0, LX/Ah2;->e:LX/0Ot;

    .line 1702096
    return-void
.end method

.method public static a$redex0(LX/Ah2;I)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 1702025
    iget-object v0, p0, LX/Ah2;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Agw;

    .line 1702026
    iget-object v1, v0, LX/Agw;->a:LX/3RX;

    const v2, 0x7f07008b

    iget-object v4, v0, LX/Agw;->b:LX/3RZ;

    invoke-virtual {v4}, LX/3RZ;->a()I

    move-result v4

    const v5, 0x3e19999a    # 0.15f

    invoke-virtual {v1, v2, v4, v5}, LX/3RX;->a(IIF)LX/7Cb;

    .line 1702027
    iget v0, p0, LX/Ah2;->a:I

    if-ne p1, v0, :cond_0

    .line 1702028
    iget-object v0, p0, LX/Ah2;->d:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarView;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarView;->d:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;

    iget v1, p0, LX/Ah2;->a:I

    invoke-virtual {v0, v1}, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->c(I)V

    .line 1702029
    :cond_0
    iget-object v0, p0, LX/Ah2;->d:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarView;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarView;->d:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;

    invoke-virtual {v0, p1}, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->e(I)V

    .line 1702030
    iget v0, p0, LX/Ah2;->a:I

    if-ne p1, v0, :cond_2

    .line 1702031
    :cond_1
    :goto_0
    return-void

    .line 1702032
    :cond_2
    iget-object v0, p0, LX/Ah2;->d:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarView;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarView;->d:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->d:[Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionButtonView;

    aget-object v0, v0, p1

    .line 1702033
    iget-object v1, v0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionButtonView;->c:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1702034
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionButtonView;->setSelected(Z)V

    .line 1702035
    iget v0, p0, LX/Ah2;->a:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_3

    .line 1702036
    iget-object v0, p0, LX/Ah2;->d:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarView;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarView;->d:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;

    iget v1, p0, LX/Ah2;->a:I

    invoke-virtual {v0, v1}, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->d(I)V

    .line 1702037
    iget-object v0, p0, LX/Ah2;->d:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarView;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarView;->d:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->d:[Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionButtonView;

    iget v1, p0, LX/Ah2;->a:I

    aget-object v0, v0, v1

    .line 1702038
    iget-object v1, v0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionButtonView;->c:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1702039
    invoke-virtual {v0, v3}, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionButtonView;->setSelected(Z)V

    .line 1702040
    :cond_3
    iput p1, p0, LX/Ah2;->a:I

    .line 1702041
    invoke-direct {p0}, LX/Ah2;->c()V

    .line 1702042
    iget-object v0, p0, LX/Ah2;->c:LX/Ags;

    if-eqz v0, :cond_1

    .line 1702043
    iget-object v0, p0, LX/Ah2;->c:LX/Ags;

    iget v1, p0, LX/Ah2;->a:I

    invoke-interface {v0, v1}, LX/Ags;->a(I)V

    goto :goto_0
.end method

.method private b()V
    .locals 3

    .prologue
    .line 1702089
    const/4 v0, 0x0

    :goto_0
    sget-object v1, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->a:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 1702090
    iget-object v1, p0, LX/Ah2;->d:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarView;

    iget-object v1, v1, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarView;->d:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;

    iget-object v1, v1, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->d:[Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionButtonView;

    aget-object v1, v1, v0

    new-instance v2, LX/Ah1;

    invoke-direct {v2, p0, v0}, LX/Ah1;-><init>(LX/Ah2;I)V

    invoke-virtual {v1, v2}, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionButtonView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1702091
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1702092
    :cond_0
    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 1702084
    iget-object v0, p0, LX/Ah2;->d:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarView;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarView;->f:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarBottomBarView;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarBottomBarView;->c:Lcom/facebook/resources/ui/FbButton;

    .line 1702085
    iget-boolean v1, p0, LX/Ah2;->b:Z

    if-eqz v1, :cond_0

    .line 1702086
    invoke-direct {p0}, LX/Ah2;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 1702087
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setEnabled(Z)V

    .line 1702088
    return-void
.end method

.method private d()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1702082
    iget-object v0, p0, LX/Ah2;->d:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarView;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080bcc

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1702083
    iget v1, p0, LX/Ah2;->a:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "USD"

    invoke-static {v1}, LX/5fx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->a:[Ljava/lang/String;

    iget v2, p0, LX/Ah2;->a:I

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1702079
    iget v0, p0, LX/Ah2;->a:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 1702080
    sget-object v0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarTipOptionsSelectorView;->a:[Ljava/lang/String;

    iget v1, p0, LX/Ah2;->a:I

    aget-object v0, v0, v1

    .line 1702081
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/payments/paymentmethods/model/PaymentMethod;Landroid/view/View$OnClickListener;)V
    .locals 7
    .param p1    # Lcom/facebook/payments/paymentmethods/model/PaymentMethod;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/16 v6, 0x8

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1702055
    if-nez p1, :cond_0

    const/4 v0, 0x0

    move-object v3, v0

    .line 1702056
    :goto_0
    if-eqz v3, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, LX/Ah2;->b:Z

    .line 1702057
    iget-object v0, p0, LX/Ah2;->d:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarView;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarView;->f:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarBottomBarView;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarBottomBarView;->a:Lcom/facebook/resources/ui/FbTextView;

    .line 1702058
    iget-object v4, p0, LX/Ah2;->d:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarView;

    iget-object v4, v4, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarView;->f:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarBottomBarView;

    iget-object v4, v4, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarBottomBarView;->b:Lcom/facebook/resources/ui/FbTextView;

    .line 1702059
    iget-boolean v5, p0, LX/Ah2;->b:Z

    if-eqz v5, :cond_2

    .line 1702060
    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1702061
    invoke-virtual {v4, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1702062
    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1702063
    invoke-virtual {v0, p2}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1702064
    invoke-virtual {v4, p2}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1702065
    :goto_2
    iget-object v0, p0, LX/Ah2;->d:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarView;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarView;->f:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarBottomBarView;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarBottomBarView;->c:Lcom/facebook/resources/ui/FbButton;

    .line 1702066
    iget-boolean v3, p0, LX/Ah2;->b:Z

    if-eqz v3, :cond_3

    .line 1702067
    invoke-direct {p0}, LX/Ah2;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 1702068
    new-instance v3, LX/Ah0;

    invoke-direct {v3, p0}, LX/Ah0;-><init>(LX/Ah2;)V

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1702069
    :goto_3
    iget v3, p0, LX/Ah2;->a:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_4

    :goto_4
    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setEnabled(Z)V

    .line 1702070
    return-void

    .line 1702071
    :cond_0
    iget-object v0, p0, LX/Ah2;->d:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarView;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/facebook/payments/paymentmethods/model/PaymentMethod;->a(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    goto :goto_0

    :cond_1
    move v0, v2

    .line 1702072
    goto :goto_1

    .line 1702073
    :cond_2
    invoke-virtual {v0, v6}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1702074
    invoke-virtual {v4, v6}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_2

    .line 1702075
    :cond_3
    iget-object v3, p0, LX/Ah2;->d:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarView;

    invoke-virtual {v3}, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080bcd

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1702076
    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 1702077
    invoke-virtual {v0, p2}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_3

    :cond_4
    move v1, v2

    .line 1702078
    goto :goto_4
.end method

.method public final a(Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1702047
    const/4 v0, -0x1

    iput v0, p0, LX/Ah2;->a:I

    .line 1702048
    iput-boolean v3, p0, LX/Ah2;->b:Z

    .line 1702049
    invoke-direct {p0}, LX/Ah2;->b()V

    .line 1702050
    if-nez p1, :cond_0

    .line 1702051
    iget-object v0, p0, LX/Ah2;->d:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarView;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarView;->a:Lcom/facebook/fbui/glyph/GlyphView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1702052
    iget-object v0, p0, LX/Ah2;->d:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarView;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarView;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 1702053
    iget-object v1, p0, LX/Ah2;->d:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarView;

    invoke-virtual {v1}, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0060

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v3, v1, v3, v3}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 1702054
    :cond_0
    return-void
.end method

.method public final b(Z)V
    .locals 1

    .prologue
    .line 1702044
    iget-object v0, p0, LX/Ah2;->d:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarView;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarView;->f:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarBottomBarView;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarBottomBarView;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setEnabled(Z)V

    .line 1702045
    iget-object v0, p0, LX/Ah2;->d:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarView;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarView;->f:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarBottomBarView;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarBottomBarView;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setEnabled(Z)V

    .line 1702046
    return-void
.end method
