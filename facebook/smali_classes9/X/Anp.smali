.class public final LX/Anp;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/Anq;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/graphql/model/GraphQLFeedback;


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1712938
    const-string v0, "UFINoReactionsFeedbackSummaryComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1712939
    if-ne p0, p1, :cond_1

    .line 1712940
    :cond_0
    :goto_0
    return v0

    .line 1712941
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1712942
    goto :goto_0

    .line 1712943
    :cond_3
    check-cast p1, LX/Anp;

    .line 1712944
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1712945
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1712946
    if-eq v2, v3, :cond_0

    .line 1712947
    iget-object v2, p0, LX/Anp;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/Anp;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    iget-object v3, p1, LX/Anp;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1712948
    goto :goto_0

    .line 1712949
    :cond_4
    iget-object v2, p1, LX/Anp;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
