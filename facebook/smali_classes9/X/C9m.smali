.class public LX/C9m;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pq;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/C9r;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/C9m",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/C9r;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1854496
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1854497
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/C9m;->b:LX/0Zi;

    .line 1854498
    iput-object p1, p0, LX/C9m;->a:LX/0Ot;

    .line 1854499
    return-void
.end method

.method public static a(LX/0QB;)LX/C9m;
    .locals 4

    .prologue
    .line 1854485
    const-class v1, LX/C9m;

    monitor-enter v1

    .line 1854486
    :try_start_0
    sget-object v0, LX/C9m;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1854487
    sput-object v2, LX/C9m;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1854488
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1854489
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1854490
    new-instance v3, LX/C9m;

    const/16 p0, 0x206f

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/C9m;-><init>(LX/0Ot;)V

    .line 1854491
    move-object v0, v3

    .line 1854492
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1854493
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C9m;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1854494
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1854495
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/1De;Ljava/lang/Integer;)V
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "updateLabel"
        processor = "com.facebook.thecount.transformer.Transformer"
    .end annotation

    .prologue
    .line 1854479
    iget-object v0, p0, LX/1De;->g:LX/1X1;

    move-object v0, v0

    .line 1854480
    if-nez v0, :cond_0

    .line 1854481
    :goto_0
    return-void

    .line 1854482
    :cond_0
    check-cast v0, LX/C9k;

    .line 1854483
    new-instance v1, LX/C9l;

    iget-object v2, v0, LX/C9k;->d:LX/C9m;

    invoke-direct {v1, v2, p1}, LX/C9l;-><init>(LX/C9m;Ljava/lang/Integer;)V

    move-object v0, v1

    .line 1854484
    invoke-virtual {p0, v0}, LX/1De;->a(LX/48B;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 7

    .prologue
    .line 1854443
    check-cast p2, LX/C9k;

    .line 1854444
    iget-object v0, p0, LX/C9m;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/C9r;

    iget-object v1, p2, LX/C9k;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/C9k;->b:LX/1Pn;

    iget-object v3, p2, LX/C9k;->c:Ljava/lang/Integer;

    const/4 v5, 0x2

    .line 1854445
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v5}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v5}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x3

    invoke-interface {v4, v5}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x7

    const v6, 0x7f0b1cc1

    invoke-interface {v4, v5, v6}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v4

    const v5, 0x7f020ac9

    invoke-interface {v4, v5}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v4

    .line 1854446
    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v5

    const v6, 0x7f02116c

    invoke-virtual {v5, v6}, LX/1o5;->h(I)LX/1o5;

    move-result-object v5

    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    const v6, 0x7f08109f

    invoke-interface {v5, v6}, LX/1Di;->A(I)LX/1Di;

    move-result-object v5

    const/4 v6, 0x6

    sget p0, LX/C9r;->a:I

    invoke-interface {v5, v6, p0}, LX/1Di;->g(II)LX/1Di;

    move-result-object v5

    .line 1854447
    const v6, -0x44bd2f56

    const/4 p0, 0x1

    new-array p0, p0, [Ljava/lang/Object;

    const/4 p2, 0x0

    aput-object p1, p0, p2

    invoke-static {p1, v6, p0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v6

    move-object v6, v6

    .line 1854448
    invoke-interface {v5, v6}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v5

    invoke-interface {v5}, LX/1Di;->k()LX/1Dg;

    move-result-object v5

    move-object v5, v5

    .line 1854449
    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, LX/C9r;->b(LX/1De;)LX/1X1;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1X1;)LX/1Dh;

    move-result-object v4

    const/4 v6, 0x0

    .line 1854450
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v5

    packed-switch v5, :pswitch_data_0

    move v5, v6

    .line 1854451
    :goto_0
    const p0, 0x7f0e0122

    invoke-static {p1, v6, p0}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v6

    invoke-virtual {v6, v5}, LX/1ne;->h(I)LX/1ne;

    move-result-object v5

    const v6, 0x7f0a05d2

    invoke-virtual {v5, v6}, LX/1ne;->n(I)LX/1ne;

    move-result-object v5

    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-interface {v5, v6}, LX/1Di;->a(F)LX/1Di;

    move-result-object v5

    const/4 v6, 0x6

    sget p0, LX/C9r;->a:I

    invoke-interface {v5, v6, p0}, LX/1Di;->g(II)LX/1Di;

    move-result-object v5

    invoke-interface {v5}, LX/1Di;->k()LX/1Dg;

    move-result-object v5

    move-object v5, v5

    .line 1854452
    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, LX/C9r;->b(LX/1De;)LX/1X1;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1X1;)LX/1Dh;

    move-result-object v4

    .line 1854453
    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v5

    const v6, 0x7f021167    # 1.7289E38f

    invoke-virtual {v5, v6}, LX/1o5;->h(I)LX/1o5;

    move-result-object v5

    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    const v6, 0x7f08109d

    invoke-interface {v5, v6}, LX/1Di;->A(I)LX/1Di;

    move-result-object v5

    const/4 v6, 0x6

    sget p0, LX/C9r;->a:I

    invoke-interface {v5, v6, p0}, LX/1Di;->g(II)LX/1Di;

    move-result-object v5

    .line 1854454
    const v6, 0xd447963

    const/4 p0, 0x0

    invoke-static {p1, v6, p0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v6

    move-object v6, v6

    .line 1854455
    invoke-interface {v5, v6}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v5

    invoke-interface {v5}, LX/1Di;->k()LX/1Dg;

    move-result-object v5

    move-object v5, v5

    .line 1854456
    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v4

    .line 1854457
    iget-object v5, v0, LX/C9r;->d:LX/C9p;

    const/4 v6, 0x0

    .line 1854458
    new-instance p0, LX/C9o;

    invoke-direct {p0, v5}, LX/C9o;-><init>(LX/C9p;)V

    .line 1854459
    iget-object v0, v5, LX/C9p;->b:LX/0Zi;

    invoke-virtual {v0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/C9n;

    .line 1854460
    if-nez v0, :cond_0

    .line 1854461
    new-instance v0, LX/C9n;

    invoke-direct {v0, v5}, LX/C9n;-><init>(LX/C9p;)V

    .line 1854462
    :cond_0
    invoke-static {v0, p1, v6, v6, p0}, LX/C9n;->a$redex0(LX/C9n;LX/1De;IILX/C9o;)V

    .line 1854463
    move-object p0, v0

    .line 1854464
    move-object v6, p0

    .line 1854465
    move-object v5, v6

    .line 1854466
    iget-object v6, v5, LX/C9n;->a:LX/C9o;

    iput-object v1, v6, LX/C9o;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1854467
    iget-object v6, v5, LX/C9n;->e:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v6, p0}, Ljava/util/BitSet;->set(I)V

    .line 1854468
    move-object v5, v5

    .line 1854469
    check-cast v2, LX/1Pq;

    .line 1854470
    iget-object v6, v5, LX/C9n;->a:LX/C9o;

    iput-object v2, v6, LX/C9o;->b:LX/1Pq;

    .line 1854471
    iget-object v6, v5, LX/C9n;->e:Ljava/util/BitSet;

    const/4 p0, 0x1

    invoke-virtual {v6, p0}, Ljava/util/BitSet;->set(I)V

    .line 1854472
    move-object v5, v5

    .line 1854473
    invoke-virtual {v5}, LX/1X5;->d()LX/1X1;

    move-result-object v5

    move-object v5, v5

    .line 1854474
    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1X1;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    move-object v0, v4

    .line 1854475
    return-object v0

    .line 1854476
    :pswitch_0
    const v5, 0x7f0810a2

    goto/16 :goto_0

    .line 1854477
    :pswitch_1
    const v5, 0x7f08003f

    goto/16 :goto_0

    .line 1854478
    :pswitch_2
    const v5, 0x7f0810a3

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 1854500
    invoke-static {}, LX/1dS;->b()V

    .line 1854501
    iget v0, p1, LX/1dQ;->b:I

    .line 1854502
    sparse-switch v0, :sswitch_data_0

    .line 1854503
    :goto_0
    return-object v3

    .line 1854504
    :sswitch_0
    check-cast p2, LX/3Ae;

    .line 1854505
    iget-object v0, p1, LX/1dQ;->c:[Ljava/lang/Object;

    const/4 v2, 0x0

    aget-object v0, v0, v2

    check-cast v0, LX/1De;

    iget-object v2, p1, LX/1dQ;->a:LX/1X1;

    .line 1854506
    check-cast v2, LX/C9k;

    .line 1854507
    iget-object v4, p0, LX/C9m;->a:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/C9r;

    iget-object p1, v2, LX/C9k;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object p2, v2, LX/C9k;->b:LX/1Pn;

    invoke-virtual {v4, v0, p1, p2}, LX/C9r;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)V

    .line 1854508
    goto :goto_0

    .line 1854509
    :sswitch_1
    check-cast p2, LX/3Ae;

    .line 1854510
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1854511
    check-cast v1, LX/C9k;

    .line 1854512
    iget-object v2, p0, LX/C9m;->a:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/C9r;

    iget-object v4, v1, LX/C9k;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1854513
    iget-object v1, v2, LX/C9r;->f:LX/C99;

    .line 1854514
    iget-object p0, v4, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object p0, p0

    .line 1854515
    check-cast p0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1, p0, v0}, LX/C99;->a(Lcom/facebook/graphql/model/GraphQLStory;Landroid/view/View;)V

    .line 1854516
    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x44bd2f56 -> :sswitch_0
        0xd447963 -> :sswitch_1
    .end sparse-switch
.end method

.method public final a(LX/1X1;LX/1X1;)V
    .locals 1

    .prologue
    .line 1854439
    check-cast p1, LX/C9k;

    .line 1854440
    check-cast p2, LX/C9k;

    .line 1854441
    iget-object v0, p1, LX/C9k;->c:Ljava/lang/Integer;

    iput-object v0, p2, LX/C9k;->c:Ljava/lang/Integer;

    .line 1854442
    return-void
.end method

.method public final c(LX/1De;)LX/C9j;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/C9m",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1854431
    new-instance v1, LX/C9k;

    invoke-direct {v1, p0}, LX/C9k;-><init>(LX/C9m;)V

    .line 1854432
    iget-object v2, p0, LX/C9m;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/C9j;

    .line 1854433
    if-nez v2, :cond_0

    .line 1854434
    new-instance v2, LX/C9j;

    invoke-direct {v2, p0}, LX/C9j;-><init>(LX/C9m;)V

    .line 1854435
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/C9j;->a$redex0(LX/C9j;LX/1De;IILX/C9k;)V

    .line 1854436
    move-object v1, v2

    .line 1854437
    move-object v0, v1

    .line 1854438
    return-object v0
.end method

.method public final d(LX/1De;LX/1X1;)V
    .locals 2

    .prologue
    .line 1854422
    check-cast p2, LX/C9k;

    .line 1854423
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v1

    .line 1854424
    iget-object v0, p0, LX/C9m;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/C9r;

    .line 1854425
    invoke-static {v0}, LX/C9r;->a$redex0(LX/C9r;)Ljava/lang/Integer;

    move-result-object p0

    .line 1854426
    iput-object p0, v1, LX/1np;->a:Ljava/lang/Object;

    .line 1854427
    iget-object v0, v1, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 1854428
    check-cast v0, Ljava/lang/Integer;

    iput-object v0, p2, LX/C9k;->c:Ljava/lang/Integer;

    .line 1854429
    invoke-static {v1}, LX/1cy;->a(LX/1np;)V

    .line 1854430
    return-void
.end method

.method public final p()Z
    .locals 1

    .prologue
    .line 1854421
    const/4 v0, 0x1

    return v0
.end method
