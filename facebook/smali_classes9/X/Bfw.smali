.class public final LX/Bfw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;

.field public final synthetic d:Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;


# direct methods
.method public constructor <init>(Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;)V
    .locals 0

    .prologue
    .line 1806807
    iput-object p1, p0, LX/Bfw;->d:Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;

    iput-object p2, p0, LX/Bfw;->a:Ljava/lang/String;

    iput-object p3, p0, LX/Bfw;->b:Ljava/lang/String;

    iput-object p4, p0, LX/Bfw;->c:Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, 0x291ff926

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1806800
    iget-object v1, p0, LX/Bfw;->d:Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;

    iget-object v1, v1, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;->b:Ljava/util/Set;

    iget-object v2, p0, LX/Bfw;->a:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1806801
    const v1, 0x4192ae0f

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1806802
    :goto_0
    return-void

    .line 1806803
    :cond_0
    iget-object v1, p0, LX/Bfw;->d:Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;

    iget-object v2, p0, LX/Bfw;->b:Ljava/lang/String;

    iget-object v3, p0, LX/Bfw;->c:Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;

    invoke-virtual {v3}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;->getSuggestionText()Ljava/lang/String;

    move-result-object v3

    .line 1806804
    invoke-static {v1, v2, v3}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;->a$redex0(Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;Ljava/lang/String;Ljava/lang/String;)V

    .line 1806805
    iget-object v1, p0, LX/Bfw;->d:Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;

    iget-object v1, v1, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;->b:Ljava/util/Set;

    iget-object v2, p0, LX/Bfw;->a:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1806806
    const v1, 0x7ad4bcd6

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto :goto_0
.end method
