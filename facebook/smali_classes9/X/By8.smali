.class public LX/By8;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final a:I

.field private static g:LX/0Xm;


# instance fields
.field private final b:LX/By0;

.field private final c:LX/38w;

.field private final d:LX/38u;

.field private final e:LX/38v;

.field private final f:LX/6RZ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1836802
    invoke-static {v0, v0}, LX/1mh;->a(II)I

    move-result v0

    sput v0, LX/By8;->a:I

    return-void
.end method

.method public constructor <init>(LX/By0;LX/38w;LX/38u;LX/38v;LX/6RZ;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1836803
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1836804
    iput-object p1, p0, LX/By8;->b:LX/By0;

    .line 1836805
    iput-object p2, p0, LX/By8;->c:LX/38w;

    .line 1836806
    iput-object p3, p0, LX/By8;->d:LX/38u;

    .line 1836807
    iput-object p4, p0, LX/By8;->e:LX/38v;

    .line 1836808
    iput-object p5, p0, LX/By8;->f:LX/6RZ;

    .line 1836809
    return-void
.end method

.method public static a(LX/0QB;)LX/By8;
    .locals 3

    .prologue
    .line 1836810
    const-class v1, LX/By8;

    monitor-enter v1

    .line 1836811
    :try_start_0
    sget-object v0, LX/By8;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1836812
    sput-object v2, LX/By8;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1836813
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1836814
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/By8;->b(LX/0QB;)LX/By8;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1836815
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/By8;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1836816
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1836817
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)LX/By8;
    .locals 6

    .prologue
    .line 1836818
    new-instance v0, LX/By8;

    invoke-static {p0}, LX/By0;->a(LX/0QB;)LX/By0;

    move-result-object v1

    check-cast v1, LX/By0;

    invoke-static {p0}, LX/38w;->b(LX/0QB;)LX/38w;

    move-result-object v2

    check-cast v2, LX/38w;

    const-class v3, LX/38u;

    invoke-interface {p0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/38u;

    const-class v4, LX/38v;

    invoke-interface {p0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/38v;

    invoke-static {p0}, LX/6RZ;->a(LX/0QB;)LX/6RZ;

    move-result-object v5

    check-cast v5, LX/6RZ;

    invoke-direct/range {v0 .. v5}, LX/By8;-><init>(LX/By0;LX/38w;LX/38u;LX/38v;LX/6RZ;)V

    .line 1836819
    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;ILcom/facebook/feed/rows/core/props/FeedProps;I)LX/1Dg;
    .locals 11
    .param p3    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "I",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;I)",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    const/4 v8, 0x2

    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 1836820
    invoke-static {p2}, LX/1mh;->b(I)I

    move-result v0

    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0b59

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0b5a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sub-int v1, v0, v1

    .line 1836821
    iget-object v0, p3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1836822
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1836823
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->a()LX/0Px;

    move-result-object v2

    invoke-static {v2}, LX/Bnh;->a(Ljava/util/List;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v2

    .line 1836824
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->E()Lcom/facebook/graphql/model/GraphQLEvent;

    move-result-object v2

    .line 1836825
    iget-object v3, p0, LX/By8;->d:LX/38u;

    invoke-virtual {v3, p3, v2}, LX/38u;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLEvent;)LX/By4;

    move-result-object v3

    .line 1836826
    iget-object v4, p0, LX/By8;->e:LX/38v;

    invoke-virtual {v4, v3}, LX/38v;->a(LX/Bni;)LX/Bne;

    move-result-object v3

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLEvent;->C()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-result-object v4

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLEvent;->by()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v5

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLEvent;->bF()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v2

    invoke-virtual {v3, v4, v5, v2}, LX/Bne;->a(Lcom/facebook/graphql/enums/GraphQLConnectionStyle;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)LX/BnW;

    move-result-object v2

    .line 1836827
    invoke-static {p1, v0}, LX/Bng;->a(Landroid/content/Context;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/Bnf;

    move-result-object v3

    .line 1836828
    iget-object v4, v3, LX/Bnf;->d:Ljava/util/Date;

    .line 1836829
    new-instance v5, LX/1no;

    invoke-direct {v5}, LX/1no;-><init>()V

    .line 1836830
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v0

    iget-object v6, v3, LX/Bnf;->a:Ljava/lang/String;

    invoke-virtual {v0, v6}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v0

    const v6, 0x7f0a010c

    invoke-virtual {v0, v6}, LX/1ne;->n(I)LX/1ne;

    move-result-object v0

    const v6, 0x7f0b0050

    invoke-virtual {v0, v6}, LX/1ne;->q(I)LX/1ne;

    move-result-object v0

    invoke-virtual {v0, v9}, LX/1ne;->t(I)LX/1ne;

    move-result-object v0

    sget-object v6, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v6}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v0

    const v6, 0x7f0b0034

    invoke-virtual {v0, v6}, LX/1ne;->s(I)LX/1ne;

    move-result-object v0

    invoke-virtual {v0, v8}, LX/1ne;->j(I)LX/1ne;

    move-result-object v0

    invoke-virtual {v0, v10}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v6

    .line 1836831
    sget v0, LX/By8;->a:I

    sget v7, LX/By8;->a:I

    invoke-virtual {v6, p1, v0, v7, v5}, LX/1X1;->a(LX/1De;IILX/1no;)V

    .line 1836832
    const/4 v0, 0x0

    .line 1836833
    iget-object v7, v3, LX/Bnf;->c:Ljava/lang/String;

    invoke-static {v7}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_0

    iget v5, v5, LX/1no;->a:I

    if-lt v1, v5, :cond_0

    .line 1836834
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v0

    iget-object v1, v3, LX/Bnf;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v0

    const v1, 0x7f0a00a4

    invoke-virtual {v0, v1}, LX/1ne;->n(I)LX/1ne;

    move-result-object v0

    const v1, 0x7f0b004e

    invoke-virtual {v0, v1}, LX/1ne;->q(I)LX/1ne;

    move-result-object v0

    invoke-virtual {v0, v10}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v0

    invoke-virtual {v0, v9}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v0

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 1836835
    :cond_0
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1, v8}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v1

    const v5, 0x7f0b0b58

    invoke-interface {v1, v5}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v1

    invoke-interface {v1, p4}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v1

    invoke-interface {v1, v8}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v1

    invoke-interface {v1, v9}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v1

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, v10}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, v8}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, v9}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v5

    const v7, 0x7f0b0b59

    invoke-interface {v5, v7}, LX/1Dh;->G(I)LX/1Dh;

    move-result-object v5

    const v7, 0x7f0b0b59

    invoke-interface {v5, v7}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v5

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v7

    iget-object v8, p0, LX/By8;->f:LX/6RZ;

    invoke-virtual {v8, v4}, LX/6RZ;->c(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v7

    const v8, 0x7f0a0094

    invoke-virtual {v7, v8}, LX/1ne;->n(I)LX/1ne;

    move-result-object v7

    const v8, 0x7f0b004e

    invoke-virtual {v7, v8}, LX/1ne;->q(I)LX/1ne;

    move-result-object v7

    invoke-virtual {v7, v9}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v7

    invoke-virtual {v7, v10}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v7

    invoke-interface {v5, v7}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v5

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v7

    iget-object v8, p0, LX/By8;->f:LX/6RZ;

    invoke-virtual {v8, v4}, LX/6RZ;->d(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v7, v4}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v4

    const v7, 0x7f0a00ab

    invoke-virtual {v4, v7}, LX/1ne;->n(I)LX/1ne;

    move-result-object v4

    const v7, 0x7f0b0056

    invoke-virtual {v4, v7}, LX/1ne;->q(I)LX/1ne;

    move-result-object v4

    sget-object v7, Landroid/graphics/Typeface;->SANS_SERIF:Landroid/graphics/Typeface;

    invoke-virtual {v4, v7}, LX/1ne;->a(Landroid/graphics/Typeface;)LX/1ne;

    move-result-object v4

    invoke-virtual {v4, v9}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v4

    invoke-virtual {v4, v10}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v4

    invoke-interface {v5, v4}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v4

    invoke-interface {v1, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v10}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-interface {v4, v5}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v9}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v6}, LX/1Dh;->a(LX/1X1;)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v5

    iget-object v3, v3, LX/Bnf;->b:Ljava/lang/CharSequence;

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v3

    const v5, 0x7f0a00a4

    invoke-virtual {v3, v5}, LX/1ne;->n(I)LX/1ne;

    move-result-object v3

    const v5, 0x7f0b004e

    invoke-virtual {v3, v5}, LX/1ne;->q(I)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, v10}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, v9}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v3

    sget-object v5, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v3, v5}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v3

    const v5, 0x7f0b0b62

    invoke-virtual {v3, v5}, LX/1ne;->s(I)LX/1ne;

    move-result-object v3

    invoke-interface {v4, v3}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v0}, LX/1Dh;->a(LX/1X1;)LX/1Dh;

    move-result-object v0

    invoke-interface {v1, v0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    iget-object v1, p0, LX/By8;->b:LX/By0;

    const/4 v3, 0x0

    .line 1836836
    new-instance v4, LX/Bxz;

    invoke-direct {v4, v1}, LX/Bxz;-><init>(LX/By0;)V

    .line 1836837
    sget-object v5, LX/By0;->a:LX/0Zi;

    invoke-virtual {v5}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/Bxy;

    .line 1836838
    if-nez v5, :cond_1

    .line 1836839
    new-instance v5, LX/Bxy;

    invoke-direct {v5}, LX/Bxy;-><init>()V

    .line 1836840
    :cond_1
    invoke-static {v5, p1, v3, v3, v4}, LX/Bxy;->a$redex0(LX/Bxy;LX/1De;IILX/Bxz;)V

    .line 1836841
    move-object v4, v5

    .line 1836842
    move-object v3, v4

    .line 1836843
    move-object v1, v3

    .line 1836844
    iget-object v3, v1, LX/Bxy;->a:LX/Bxz;

    iput-object v2, v3, LX/Bxz;->a:LX/BnW;

    .line 1836845
    iget-object v3, v1, LX/Bxy;->d:Ljava/util/BitSet;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/BitSet;->set(I)V

    .line 1836846
    move-object v1, v1

    .line 1836847
    invoke-virtual {v1}, LX/1X5;->d()LX/1X1;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1X1;)LX/1Dh;

    move-result-object v0

    invoke-static {p1}, LX/By7;->onClick(LX/1De;)LX/1dQ;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public onClick(Landroid/view/View;LX/1Pq;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;)V
    .locals 1
    .param p2    # LX/1Pq;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "TE;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1836848
    iget-object v0, p0, LX/By8;->c:LX/38w;

    invoke-virtual {v0, p1, p2, p3, p4}, LX/38w;->a(Landroid/view/View;LX/1Pq;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;)V

    .line 1836849
    return-void
.end method
