.class public LX/C3I;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pb;",
        ":",
        "LX/1Pc;",
        ":",
        "LX/1Pd;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/C3J;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/C3I",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/C3J;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1845349
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1845350
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/C3I;->b:LX/0Zi;

    .line 1845351
    iput-object p1, p0, LX/C3I;->a:LX/0Ot;

    .line 1845352
    return-void
.end method

.method public static a(LX/0QB;)LX/C3I;
    .locals 4

    .prologue
    .line 1845386
    const-class v1, LX/C3I;

    monitor-enter v1

    .line 1845387
    :try_start_0
    sget-object v0, LX/C3I;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1845388
    sput-object v2, LX/C3I;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1845389
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1845390
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1845391
    new-instance v3, LX/C3I;

    const/16 p0, 0x1ec0

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/C3I;-><init>(LX/0Ot;)V

    .line 1845392
    move-object v0, v3

    .line 1845393
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1845394
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C3I;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1845395
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1845396
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 1845363
    check-cast p2, LX/C3H;

    .line 1845364
    iget-object v0, p0, LX/C3I;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/C3J;

    iget-object v1, p2, LX/C3H;->a:LX/C33;

    iget-object v2, p2, LX/C3H;->b:LX/1Pb;

    const/4 v4, 0x0

    const/4 p2, 0x1

    .line 1845365
    iget-object v5, v1, LX/C33;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1845366
    sget-object v3, LX/C34;->DENSE_SEARCH_STORIES:LX/C34;

    iget-object p0, v1, LX/C33;->b:LX/C34;

    if-eq v3, p0, :cond_0

    sget-object v3, LX/C34;->COMPACT_GROUPS_FEED:LX/C34;

    iget-object p0, v1, LX/C33;->b:LX/C34;

    if-eq v3, p0, :cond_0

    sget-object v3, LX/C34;->GROUP_RELATED_STORIES:LX/C34;

    iget-object p0, v1, LX/C33;->b:LX/C34;

    if-ne v3, p0, :cond_3

    .line 1845367
    :cond_0
    sget-object v3, LX/C34;->COMPACT_GROUPS_FEED:LX/C34;

    iget-object p0, v1, LX/C33;->b:LX/C34;

    if-ne v3, p0, :cond_1

    iget-object v3, v1, LX/C33;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v3, :cond_1

    iget-object v3, v1, LX/C33;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1845368
    iget-object p0, v3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, p0

    .line 1845369
    if-eqz v3, :cond_1

    iget-object v3, v1, LX/C33;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1845370
    iget-object p0, v3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, p0

    .line 1845371
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 1845372
    iget-object v3, v0, LX/C3J;->c:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/3my;

    iget-object v4, v1, LX/C33;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1845373
    iget-object p0, v4, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, p0

    .line 1845374
    check-cast v4, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLProfile;->ad()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/3my;->b(Lcom/facebook/graphql/enums/GraphQLGroupCategory;)Z

    move-result v3

    move v4, v3

    .line 1845375
    :cond_1
    iget-object v3, v0, LX/C3J;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/1VD;

    invoke-virtual {v3, p1}, LX/1VD;->c(LX/1De;)LX/1X4;

    move-result-object v3

    invoke-virtual {v3, v5}, LX/1X4;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1X4;

    move-result-object v3

    .line 1845376
    iget-object v5, v3, LX/1X4;->a:LX/1X0;

    iput-boolean v4, v5, LX/1X0;->f:Z

    .line 1845377
    move-object v3, v3

    .line 1845378
    invoke-virtual {v3, v2}, LX/1X4;->a(LX/1Pb;)LX/1X4;

    move-result-object v3

    const v4, 0x7f0b1d76

    invoke-virtual {v3, v4}, LX/1X4;->h(I)LX/1X4;

    move-result-object v3

    invoke-virtual {v3, p2}, LX/1X4;->i(I)LX/1X4;

    move-result-object v3

    const v4, 0x7f0b1d7e

    invoke-virtual {v3, v4}, LX/1X4;->j(I)LX/1X4;

    move-result-object v3

    const v4, 0x7f0b1d7d

    invoke-virtual {v3, v4}, LX/1X4;->m(I)LX/1X4;

    move-result-object v3

    .line 1845379
    iget-object v4, v1, LX/C33;->b:LX/C34;

    sget-object v5, LX/C34;->COMPACT_GROUPS_FEED:LX/C34;

    if-ne v4, v5, :cond_2

    .line 1845380
    invoke-virtual {v3, p2}, LX/1X4;->a(Z)LX/1X4;

    .line 1845381
    :cond_2
    invoke-virtual {v3}, LX/1X5;->b()LX/1Dg;

    move-result-object v3

    .line 1845382
    :goto_0
    move-object v0, v3

    .line 1845383
    return-object v0

    .line 1845384
    :cond_3
    iget-object v3, v0, LX/C3J;->a:LX/1xv;

    invoke-virtual {v3, v5}, LX/1xv;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1y2;

    move-result-object v3

    invoke-virtual {v3}, LX/1y2;->a()LX/1y2;

    move-result-object v3

    invoke-virtual {v3}, LX/1y2;->c()LX/1y2;

    move-result-object v3

    invoke-virtual {v3}, LX/1y2;->d()Landroid/text/Spannable;

    move-result-object v3

    .line 1845385
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, v3}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v3

    const v5, 0x7f0b1d6b

    invoke-virtual {v3, v5}, LX/1ne;->q(I)LX/1ne;

    move-result-object v3

    const v5, 0x7f0a00a4

    invoke-virtual {v3, v5}, LX/1ne;->n(I)LX/1ne;

    move-result-object v3

    sget-object v5, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v3, v5}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, v4}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const v4, 0x7f0b1d6d

    invoke-interface {v3, p2, v4}, LX/1Di;->g(II)LX/1Di;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1845361
    invoke-static {}, LX/1dS;->b()V

    .line 1845362
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c(LX/1De;)LX/C3G;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/C3I",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1845353
    new-instance v1, LX/C3H;

    invoke-direct {v1, p0}, LX/C3H;-><init>(LX/C3I;)V

    .line 1845354
    iget-object v2, p0, LX/C3I;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/C3G;

    .line 1845355
    if-nez v2, :cond_0

    .line 1845356
    new-instance v2, LX/C3G;

    invoke-direct {v2, p0}, LX/C3G;-><init>(LX/C3I;)V

    .line 1845357
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/C3G;->a$redex0(LX/C3G;LX/1De;IILX/C3H;)V

    .line 1845358
    move-object v1, v2

    .line 1845359
    move-object v0, v1

    .line 1845360
    return-object v0
.end method
