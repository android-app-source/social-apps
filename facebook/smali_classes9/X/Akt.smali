.class public final LX/Akt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:Landroid/content/Context;

.field public final synthetic b:Lcom/facebook/graphql/model/FeedUnit;

.field public final synthetic c:LX/1wH;


# direct methods
.method public constructor <init>(LX/1wH;Landroid/content/Context;Lcom/facebook/graphql/model/FeedUnit;)V
    .locals 0

    .prologue
    .line 1709349
    iput-object p1, p0, LX/Akt;->c:LX/1wH;

    iput-object p2, p0, LX/Akt;->a:Landroid/content/Context;

    iput-object p3, p0, LX/Akt;->b:Lcom/facebook/graphql/model/FeedUnit;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 6

    .prologue
    .line 1709350
    iget-object v0, p0, LX/Akt;->c:LX/1wH;

    iget-object v0, v0, LX/1wH;->a:LX/1SX;

    invoke-virtual {v0}, LX/1SX;->c()LX/0Or;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1709351
    iget-object v0, p0, LX/Akt;->c:LX/1wH;

    iget-object v0, v0, LX/1wH;->a:LX/1SX;

    invoke-virtual {v0}, LX/1SX;->c()LX/0Or;

    move-result-object v0

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6G2;

    invoke-static {}, LX/6FY;->newBuilder()LX/6FX;

    move-result-object v1

    iget-object v2, p0, LX/Akt;->a:Landroid/content/Context;

    invoke-virtual {v1, v2}, LX/6FX;->a(Landroid/content/Context;)LX/6FX;

    move-result-object v1

    sget-object v2, LX/6Fb;->FEED_STORY:LX/6Fb;

    invoke-virtual {v1, v2}, LX/6FX;->a(LX/6Fb;)LX/6FX;

    move-result-object v1

    new-instance v2, LX/Akv;

    iget-object v3, p0, LX/Akt;->c:LX/1wH;

    iget-object v3, v3, LX/1wH;->a:LX/1SX;

    iget-object v4, p0, LX/Akt;->c:LX/1wH;

    iget-object v5, p0, LX/Akt;->b:Lcom/facebook/graphql/model/FeedUnit;

    invoke-static {v4, v5}, LX/1wH;->e(LX/1wH;Lcom/facebook/graphql/model/FeedUnit;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, LX/Akv;-><init>(LX/1SX;Ljava/lang/String;)V

    invoke-static {v2}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/6FX;->a(LX/0Rf;)LX/6FX;

    move-result-object v1

    const-wide v2, 0x110c733fce97aL

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/6FX;->a(Ljava/lang/Long;)LX/6FX;

    move-result-object v1

    invoke-virtual {v1}, LX/6FX;->a()LX/6FY;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/6G2;->a(LX/6FY;)V

    .line 1709352
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 1709353
    :cond_0
    iget-object v0, p0, LX/Akt;->c:LX/1wH;

    iget-object v1, p0, LX/Akt;->b:Lcom/facebook/graphql/model/FeedUnit;

    iget-object v2, p0, LX/Akt;->a:Landroid/content/Context;

    .line 1709354
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 1709355
    const-string v4, "android.intent.action.SEND"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1709356
    const-string v4, "plain/text"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 1709357
    const-string v4, "android.intent.extra.SUBJECT"

    const-string v5, "Feed story debug info"

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1709358
    const-string v4, "android.intent.extra.TEXT"

    invoke-static {v0, v1}, LX/1wH;->e(LX/1wH;Lcom/facebook/graphql/model/FeedUnit;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1709359
    iget-object v4, v0, LX/1wH;->a:LX/1SX;

    invoke-static {v4}, LX/1SX;->i(LX/1SX;)Lcom/facebook/content/SecureContextHelper;

    move-result-object v4

    const-string v5, "Send story debug info using:"

    invoke-static {v3, v5}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v3

    invoke-interface {v4, v3, v2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1709360
    goto :goto_0
.end method
