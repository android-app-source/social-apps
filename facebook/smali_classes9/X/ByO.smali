.class public final LX/ByO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Lcom/facebook/feedplugins/attachments/EventTicketAttachmentPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/attachments/EventTicketAttachmentPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1837174
    iput-object p1, p0, LX/ByO;->c:Lcom/facebook/feedplugins/attachments/EventTicketAttachmentPartDefinition;

    iput-object p2, p0, LX/ByO;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iput-object p3, p0, LX/ByO;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0x20cdbdb8

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1837175
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "open_link"

    invoke-direct {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1837176
    const-string v2, "tracking"

    iget-object v3, p0, LX/ByO;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v3}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1837177
    invoke-static {v1}, LX/1vZ;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1837178
    invoke-static {v1, p1}, LX/1vZ;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Landroid/view/View;)V

    .line 1837179
    :cond_0
    iget-object v2, p0, LX/ByO;->c:Lcom/facebook/feedplugins/attachments/EventTicketAttachmentPartDefinition;

    iget-object v2, v2, Lcom/facebook/feedplugins/attachments/EventTicketAttachmentPartDefinition;->e:LX/0Zb;

    invoke-interface {v2, v1}, LX/0Zb;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1837180
    iget-object v1, p0, LX/ByO;->c:Lcom/facebook/feedplugins/attachments/EventTicketAttachmentPartDefinition;

    iget-object v1, v1, Lcom/facebook/feedplugins/attachments/EventTicketAttachmentPartDefinition;->f:LX/1xP;

    iget-object v2, p0, LX/ByO;->c:Lcom/facebook/feedplugins/attachments/EventTicketAttachmentPartDefinition;

    iget-object v2, v2, Lcom/facebook/feedplugins/attachments/EventTicketAttachmentPartDefinition;->d:Landroid/content/Context;

    iget-object v3, p0, LX/ByO;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v5, v5}, LX/1xP;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/util/Map;)V

    .line 1837181
    const v1, -0x358c2f66    # -3994662.5f

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
