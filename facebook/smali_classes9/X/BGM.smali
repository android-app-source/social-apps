.class public LX/BGM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/AlW;


# instance fields
.field private final a:Landroid/graphics/drawable/Drawable;

.field private b:Landroid/content/Context;

.field private c:LX/1RN;

.field public d:Lcom/facebook/productionprompts/model/ProductionPrompt;

.field private e:LX/BGS;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1RN;LX/1bQ;LX/1E1;)V
    .locals 2
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/1RN;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1766988
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1766989
    iput-object p1, p0, LX/BGM;->b:Landroid/content/Context;

    .line 1766990
    iput-object p2, p0, LX/BGM;->c:LX/1RN;

    .line 1766991
    invoke-static {p2}, LX/1RN;->a(LX/1RN;)LX/1kK;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1kK;

    .line 1766992
    instance-of v1, v0, LX/1kW;

    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 1766993
    check-cast v0, LX/1kW;

    .line 1766994
    iget-object v1, v0, LX/1kW;->a:Lcom/facebook/productionprompts/model/ProductionPrompt;

    move-object v0, v1

    .line 1766995
    iput-object v0, p0, LX/BGM;->d:Lcom/facebook/productionprompts/model/ProductionPrompt;

    .line 1766996
    invoke-virtual {p4}, LX/1E1;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, LX/BGM;->a:Landroid/graphics/drawable/Drawable;

    .line 1766997
    return-void

    .line 1766998
    :cond_0
    invoke-virtual {p3}, LX/1bQ;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1766987
    iget-object v0, p0, LX/BGM;->d:Lcom/facebook/productionprompts/model/ProductionPrompt;

    invoke-virtual {v0}, Lcom/facebook/productionprompts/model/ProductionPrompt;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1767007
    iget-object v0, p0, LX/BGM;->d:Lcom/facebook/productionprompts/model/ProductionPrompt;

    invoke-virtual {v0}, Lcom/facebook/productionprompts/model/ProductionPrompt;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/Integer;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1767006
    const/4 v0, 0x0

    return-object v0
.end method

.method public final d()Landroid/graphics/drawable/Drawable;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1767005
    const/4 v0, 0x0

    return-object v0
.end method

.method public final e()Landroid/graphics/drawable/Drawable;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1767008
    iget-object v0, p0, LX/BGM;->a:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public final f()Landroid/net/Uri;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1767004
    iget-object v0, p0, LX/BGM;->d:Lcom/facebook/productionprompts/model/ProductionPrompt;

    invoke-virtual {v0}, Lcom/facebook/productionprompts/model/ProductionPrompt;->e()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public final g()LX/AkM;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1767000
    iget-object v0, p0, LX/BGM;->e:LX/BGS;

    if-nez v0, :cond_0

    .line 1767001
    new-instance v0, LX/BGS;

    iget-object v1, p0, LX/BGM;->b:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/BGS;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/BGM;->e:LX/BGS;

    .line 1767002
    iget-object v0, p0, LX/BGM;->e:LX/BGS;

    iget-object v1, p0, LX/BGM;->c:LX/1RN;

    invoke-virtual {v0, v1}, LX/BGS;->a(LX/1RN;)V

    .line 1767003
    :cond_0
    iget-object v0, p0, LX/BGM;->e:LX/BGS;

    return-object v0
.end method

.method public final h()Lcom/facebook/productionprompts/model/PromptDisplayReason;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1766999
    const/4 v0, 0x0

    return-object v0
.end method
