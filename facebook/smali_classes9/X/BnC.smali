.class public final LX/BnC;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeCategoriesModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/BnD;


# direct methods
.method public constructor <init>(LX/BnD;)V
    .locals 0

    .prologue
    .line 1819940
    iput-object p1, p0, LX/BnC;->a:LX/BnD;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1819941
    iget-object v0, p0, LX/BnC;->a:LX/BnD;

    iget-object v0, v0, LX/BnD;->c:LX/BnJ;

    .line 1819942
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 1819943
    invoke-virtual {v0, v1}, LX/BnJ;->a(LX/0Px;)V

    .line 1819944
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 1819945
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1819946
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1819947
    check-cast v0, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeCategoriesModel;

    .line 1819948
    if-nez v0, :cond_2

    .line 1819949
    iget-object v0, p0, LX/BnC;->a:LX/BnD;

    iget-object v0, v0, LX/BnD;->c:LX/BnJ;

    .line 1819950
    :cond_0
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 1819951
    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    :goto_0
    invoke-virtual {v1, v0}, LX/BnJ;->a(LX/0Px;)V

    .line 1819952
    :cond_1
    return-void

    .line 1819953
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeCategoriesModel;->a()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    if-eqz v1, :cond_1

    .line 1819954
    invoke-virtual {v0}, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeCategoriesModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeCategoriesModel$EventCoverPhotoThemeCategoriesModel$NodesModel;

    invoke-virtual {v1, v0, v2, v3}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v1

    .line 1819955
    iget-object v0, p0, LX/BnC;->a:LX/BnD;

    iget-object v0, v0, LX/BnD;->c:LX/BnJ;

    if-eqz v1, :cond_0

    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    goto :goto_0
.end method
