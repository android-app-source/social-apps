.class public LX/AUP;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:Z

.field public final e:Z

.field public final f:Z

.field public final g:Z

.field public final h:Z

.field public final i:LX/AUj;

.field public final j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p10    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p11    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1677958
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1677959
    iput-object p1, p0, LX/AUP;->a:Ljava/lang/String;

    .line 1677960
    iput-object p2, p0, LX/AUP;->b:Ljava/lang/String;

    .line 1677961
    iput-object p3, p0, LX/AUP;->c:Ljava/lang/String;

    .line 1677962
    iput-boolean p4, p0, LX/AUP;->d:Z

    .line 1677963
    iput-boolean p5, p0, LX/AUP;->e:Z

    .line 1677964
    iput-boolean p6, p0, LX/AUP;->f:Z

    .line 1677965
    iput-boolean p7, p0, LX/AUP;->g:Z

    .line 1677966
    iput-boolean p8, p0, LX/AUP;->h:Z

    .line 1677967
    invoke-static {p9}, LX/AUj;->valueOf(Ljava/lang/String;)LX/AUj;

    move-result-object v0

    iput-object v0, p0, LX/AUP;->i:LX/AUj;

    .line 1677968
    iput-object p10, p0, LX/AUP;->j:Ljava/lang/String;

    .line 1677969
    iput-object p11, p0, LX/AUP;->k:Ljava/lang/String;

    .line 1677970
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1677971
    if-ne p0, p1, :cond_1

    .line 1677972
    :cond_0
    :goto_0
    return v0

    .line 1677973
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    .line 1677974
    :cond_3
    check-cast p1, LX/AUP;

    .line 1677975
    iget-boolean v2, p0, LX/AUP;->d:Z

    iget-boolean v3, p1, LX/AUP;->d:Z

    if-eq v2, v3, :cond_4

    move v0, v1

    goto :goto_0

    .line 1677976
    :cond_4
    iget-boolean v2, p0, LX/AUP;->e:Z

    iget-boolean v3, p1, LX/AUP;->e:Z

    if-eq v2, v3, :cond_5

    move v0, v1

    goto :goto_0

    .line 1677977
    :cond_5
    iget-boolean v2, p0, LX/AUP;->f:Z

    iget-boolean v3, p1, LX/AUP;->f:Z

    if-eq v2, v3, :cond_6

    move v0, v1

    goto :goto_0

    .line 1677978
    :cond_6
    iget-boolean v2, p0, LX/AUP;->g:Z

    iget-boolean v3, p1, LX/AUP;->g:Z

    if-eq v2, v3, :cond_7

    move v0, v1

    goto :goto_0

    .line 1677979
    :cond_7
    iget-boolean v2, p0, LX/AUP;->h:Z

    iget-boolean v3, p1, LX/AUP;->h:Z

    if-eq v2, v3, :cond_8

    move v0, v1

    goto :goto_0

    .line 1677980
    :cond_8
    iget-object v2, p0, LX/AUP;->a:Ljava/lang/String;

    iget-object v3, p1, LX/AUP;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    move v0, v1

    goto :goto_0

    .line 1677981
    :cond_9
    iget-object v2, p0, LX/AUP;->b:Ljava/lang/String;

    iget-object v3, p1, LX/AUP;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    move v0, v1

    goto :goto_0

    .line 1677982
    :cond_a
    iget-object v2, p0, LX/AUP;->c:Ljava/lang/String;

    if-eqz v2, :cond_c

    iget-object v2, p0, LX/AUP;->c:Ljava/lang/String;

    iget-object v3, p1, LX/AUP;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    :cond_b
    move v0, v1

    .line 1677983
    goto :goto_0

    .line 1677984
    :cond_c
    iget-object v2, p1, LX/AUP;->c:Ljava/lang/String;

    if-nez v2, :cond_b

    .line 1677985
    :cond_d
    iget-object v2, p0, LX/AUP;->i:LX/AUj;

    iget-object v3, p1, LX/AUP;->i:LX/AUj;

    if-eq v2, v3, :cond_e

    move v0, v1

    goto :goto_0

    .line 1677986
    :cond_e
    iget-object v2, p0, LX/AUP;->j:Ljava/lang/String;

    if-eqz v2, :cond_10

    iget-object v2, p0, LX/AUP;->j:Ljava/lang/String;

    iget-object v3, p1, LX/AUP;->j:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_11

    :cond_f
    move v0, v1

    .line 1677987
    goto :goto_0

    .line 1677988
    :cond_10
    iget-object v2, p1, LX/AUP;->j:Ljava/lang/String;

    if-nez v2, :cond_f

    .line 1677989
    :cond_11
    iget-object v2, p0, LX/AUP;->k:Ljava/lang/String;

    if-eqz v2, :cond_13

    iget-object v2, p0, LX/AUP;->k:Ljava/lang/String;

    iget-object v3, p1, LX/AUP;->k:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_12
    move v0, v1

    goto/16 :goto_0

    :cond_13
    iget-object v2, p1, LX/AUP;->k:Ljava/lang/String;

    if-nez v2, :cond_12

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1677990
    iget-object v0, p0, LX/AUP;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1677991
    mul-int/lit8 v0, v0, 0x1f

    iget-object v3, p0, LX/AUP;->b:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    add-int/2addr v0, v3

    .line 1677992
    mul-int/lit8 v3, v0, 0x1f

    iget-object v0, p0, LX/AUP;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/AUP;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_0
    add-int/2addr v0, v3

    .line 1677993
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, LX/AUP;->d:Z

    if-eqz v0, :cond_2

    move v0, v2

    :goto_1
    add-int/2addr v0, v3

    .line 1677994
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, LX/AUP;->e:Z

    if-eqz v0, :cond_3

    move v0, v2

    :goto_2
    add-int/2addr v0, v3

    .line 1677995
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, LX/AUP;->f:Z

    if-eqz v0, :cond_4

    move v0, v2

    :goto_3
    add-int/2addr v0, v3

    .line 1677996
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, LX/AUP;->g:Z

    if-eqz v0, :cond_5

    move v0, v2

    :goto_4
    add-int/2addr v0, v3

    .line 1677997
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v3, p0, LX/AUP;->h:Z

    if-eqz v3, :cond_6

    :goto_5
    add-int/2addr v0, v2

    .line 1677998
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, LX/AUP;->i:LX/AUj;

    invoke-virtual {v2}, LX/AUj;->hashCode()I

    move-result v2

    add-int/2addr v0, v2

    .line 1677999
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, LX/AUP;->j:Ljava/lang/String;

    if-eqz v0, :cond_7

    iget-object v0, p0, LX/AUP;->j:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_6
    add-int/2addr v0, v2

    .line 1678000
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, LX/AUP;->k:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v1, p0, LX/AUP;->k:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    .line 1678001
    return v0

    :cond_1
    move v0, v1

    .line 1678002
    goto :goto_0

    :cond_2
    move v0, v1

    .line 1678003
    goto :goto_1

    :cond_3
    move v0, v1

    .line 1678004
    goto :goto_2

    :cond_4
    move v0, v1

    .line 1678005
    goto :goto_3

    :cond_5
    move v0, v1

    .line 1678006
    goto :goto_4

    :cond_6
    move v2, v1

    .line 1678007
    goto :goto_5

    :cond_7
    move v0, v1

    .line 1678008
    goto :goto_6
.end method
