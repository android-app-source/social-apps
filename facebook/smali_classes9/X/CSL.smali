.class public abstract LX/CSL;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/ipc/composer/intent/ComposerPageData;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1894498
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Landroid/content/Intent;
    .locals 2

    .prologue
    .line 1894497
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "IPageIdentityIntentBuilder newCreatePageIntent"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(JLjava/lang/String;Lcom/facebook/events/common/ActionMechanism;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 1894496
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "IPageIdentityIntentBuilder openEventCreation"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(JLjava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 1894495
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "IPageIdentityIntentBuilder newNonAdminPhotoIntent"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 1894494
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "IPageIdentityIntentBuilder launchPageUpcomingEventsListIntent"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(JLjava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 2

    .prologue
    .line 1894493
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "IPageIdentityIntentBuilder getCheckinConfigurationBuilder"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(JLjava/lang/String;Lcom/facebook/graphql/model/GraphQLPrivacyOption;Ljava/lang/String;I)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 2
    .param p4    # Lcom/facebook/graphql/model/GraphQLPrivacyOption;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/CollectionCurationMechanismsValue;
        .end annotation
    .end param

    .prologue
    .line 1894492
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "IPageIdentityIntentBuilder newPostRecommendationIntent"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(JLjava/lang/String;Ljava/lang/String;Lcom/facebook/auth/viewercontext/ViewerContext;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 2

    .prologue
    .line 1894491
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "IPageIdentityIntentBuilder getAdminStatusConfigurationBuilder"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(JLjava/lang/String;Ljava/lang/String;Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 2

    .prologue
    .line 1894490
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "IPageIdentityIntentBuilder getNonAdminPostConfigurationBuilder"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(Ljava/lang/String;ILjava/lang/String;JLcom/facebook/graphql/model/GraphQLPrivacyOption;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/CollectionCurationMechanismsValue;
        .end annotation
    .end param

    .prologue
    .line 1894483
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "IPageIdentityIntentBuilder newEditReviewIntent"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(J)V
    .locals 2

    .prologue
    .line 1894489
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "IPageIdentityIntentBuilder launchPageInfoIntent"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public b(JLjava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 1894488
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "IPageIdentityIntentBuilder newEditHomeIntent"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public b(JLjava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 1894487
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "IPageIdentityIntentBuilder launchPageIntent"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public b(JLjava/lang/String;Ljava/lang/String;Lcom/facebook/auth/viewercontext/ViewerContext;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 1894486
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "IPageIdentityIntentBuilder newAdminPhotoIntent"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final b()Z
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DeprecatedMethod"
        }
    .end annotation

    .prologue
    .line 1894485
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "IPageIdentityIntentBuilder openEventCreation"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public c(JLjava/lang/String;Ljava/lang/String;Lcom/facebook/auth/viewercontext/ViewerContext;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 1894484
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "IPageIdentityIntentBuilder newAdminVideoOnlyIntent"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
