.class public LX/BTc;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0Tn;


# instance fields
.field public final b:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/BTg;

.field public final e:Landroid/content/Context;

.field public final f:LX/BSm;

.field private final g:LX/0Zb;

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final i:Landroid/content/DialogInterface$OnClickListener;

.field public final j:Landroid/content/DialogInterface$OnClickListener;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1787805
    sget-object v0, LX/0Tm;->g:LX/0Tn;

    const-string v1, "show_upload_videos_in_hd_dialog"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/BTc;->a:LX/0Tn;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Ot;LX/BTg;LX/0Zb;Landroid/content/Context;LX/BSm;Ljava/lang/String;)V
    .locals 1
    .param p5    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/BSm;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;",
            "LX/BTg;",
            "LX/0Zb;",
            "Landroid/content/Context;",
            "LX/BSm;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1787806
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1787807
    new-instance v0, LX/BTa;

    invoke-direct {v0, p0}, LX/BTa;-><init>(LX/BTc;)V

    iput-object v0, p0, LX/BTc;->i:Landroid/content/DialogInterface$OnClickListener;

    .line 1787808
    new-instance v0, LX/BTb;

    invoke-direct {v0, p0}, LX/BTb;-><init>(LX/BTc;)V

    iput-object v0, p0, LX/BTc;->j:Landroid/content/DialogInterface$OnClickListener;

    .line 1787809
    iput-object p1, p0, LX/BTc;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1787810
    iput-object p2, p0, LX/BTc;->c:LX/0Ot;

    .line 1787811
    iput-object p3, p0, LX/BTc;->d:LX/BTg;

    .line 1787812
    iput-object p5, p0, LX/BTc;->e:Landroid/content/Context;

    .line 1787813
    iput-object p6, p0, LX/BTc;->f:LX/BSm;

    .line 1787814
    iput-object p4, p0, LX/BTc;->g:LX/0Zb;

    .line 1787815
    iput-object p7, p0, LX/BTc;->h:Ljava/lang/String;

    .line 1787816
    return-void
.end method

.method public static a$redex0(LX/BTc;Z)V
    .locals 2

    .prologue
    .line 1787817
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "hd_video_upload_upsell_dialog_action"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "composer"

    .line 1787818
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1787819
    move-object v0, v0

    .line 1787820
    iget-object v1, p0, LX/BTc;->h:Ljava/lang/String;

    .line 1787821
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->f:Ljava/lang/String;

    .line 1787822
    move-object v0, v0

    .line 1787823
    const-string v1, "selected"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 1787824
    iget-object v1, p0, LX/BTc;->g:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1787825
    return-void
.end method
