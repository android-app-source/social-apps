.class public final LX/CWz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:LX/CSY;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/CT7;

.field public final synthetic d:LX/CTo;

.field public final synthetic e:Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextCityTypeaheadView;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextCityTypeaheadView;LX/CSY;Ljava/lang/String;LX/CT7;LX/CTo;)V
    .locals 0

    .prologue
    .line 1909242
    iput-object p1, p0, LX/CWz;->e:Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextCityTypeaheadView;

    iput-object p2, p0, LX/CWz;->a:LX/CSY;

    iput-object p3, p0, LX/CWz;->b:Ljava/lang/String;

    iput-object p4, p0, LX/CWz;->c:LX/CT7;

    iput-object p5, p0, LX/CWz;->d:LX/CTo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 4

    .prologue
    .line 1909243
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1909244
    iget-object v1, p0, LX/CWz;->e:Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextCityTypeaheadView;

    .line 1909245
    invoke-static {v1, v0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextCityTypeaheadView;->a$redex0(Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextCityTypeaheadView;Ljava/lang/String;)V

    .line 1909246
    iget-object v1, p0, LX/CWz;->a:LX/CSY;

    iget-object v2, p0, LX/CWz;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, LX/CSY;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1909247
    iget-object v0, p0, LX/CWz;->c:LX/CT7;

    iget-object v1, p0, LX/CWz;->d:LX/CTo;

    iget-object v1, v1, LX/CTJ;->o:Ljava/lang/String;

    iget-object v2, p0, LX/CWz;->d:LX/CTo;

    iget-object v2, v2, LX/CTK;->e:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    iget-object v3, p0, LX/CWz;->a:LX/CSY;

    invoke-virtual {v0, v1, v2, v3}, LX/CT7;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;LX/CSY;)V

    .line 1909248
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1909249
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1909250
    return-void
.end method
