.class public final LX/BpZ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/1aZ;

.field public final b:Landroid/graphics/PointF;

.field public final c:Lcom/facebook/video/engine/VideoPlayerParams;

.field public final d:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public final e:I

.field public final f:I

.field public final g:F

.field public final h:LX/BqH;

.field public final i:LX/BpY;

.field public final j:Landroid/view/View$OnClickListener;

.field public final k:LX/2oV;


# direct methods
.method public constructor <init>(LX/1aZ;Landroid/graphics/PointF;Lcom/facebook/video/engine/VideoPlayerParams;LX/0P1;IIFLX/BqH;LX/2oV;Landroid/view/View$OnClickListener;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1aZ;",
            "Landroid/graphics/PointF;",
            "Lcom/facebook/video/engine/VideoPlayerParams;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;IIF",
            "LX/BqH;",
            "LX/2oV;",
            "Landroid/view/View$OnClickListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1823290
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1823291
    iput-object p1, p0, LX/BpZ;->a:LX/1aZ;

    .line 1823292
    iput-object p2, p0, LX/BpZ;->b:Landroid/graphics/PointF;

    .line 1823293
    iput-object p3, p0, LX/BpZ;->c:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 1823294
    iput-object p4, p0, LX/BpZ;->d:LX/0P1;

    .line 1823295
    iput p5, p0, LX/BpZ;->e:I

    .line 1823296
    iput p6, p0, LX/BpZ;->f:I

    .line 1823297
    iput p7, p0, LX/BpZ;->g:F

    .line 1823298
    iput-object p8, p0, LX/BpZ;->h:LX/BqH;

    .line 1823299
    iput-object p10, p0, LX/BpZ;->j:Landroid/view/View$OnClickListener;

    .line 1823300
    iput-object p9, p0, LX/BpZ;->k:LX/2oV;

    .line 1823301
    new-instance v0, LX/BpY;

    invoke-direct {v0, p0}, LX/BpY;-><init>(LX/BpZ;)V

    iput-object v0, p0, LX/BpZ;->i:LX/BpY;

    .line 1823302
    return-void
.end method
