.class public final LX/Bu0;
.super LX/2oa;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2oa",
        "<",
        "LX/7M5;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;)V
    .locals 0

    .prologue
    .line 1829763
    iput-object p1, p0, LX/Bu0;->a:Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

    invoke-direct {p0}, LX/2oa;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;B)V
    .locals 0

    .prologue
    .line 1829762
    invoke-direct {p0, p1}, LX/Bu0;-><init>(Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;)V

    return-void
.end method

.method private a(LX/7M5;)V
    .locals 2

    .prologue
    .line 1829759
    iget-object v0, p1, LX/7M5;->a:LX/45B;

    sget-object v1, LX/45B;->DISCONNECTED:LX/45B;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, LX/Bu0;->a:Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

    iget-object v0, v0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->I:Lcom/facebook/video/player/RichVideoPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Bu0;->a:Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

    iget-object v0, v0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->I:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1829760
    iget-object v0, p0, LX/Bu0;->a:Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

    iget-object v0, v0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->I:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v1, LX/04g;->BY_PLAYER:LX/04g;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/04g;)V

    .line 1829761
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/7M5;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1829758
    const-class v0, LX/7M5;

    return-object v0
.end method

.method public final synthetic b(LX/0b7;)V
    .locals 0

    .prologue
    .line 1829757
    check-cast p1, LX/7M5;

    invoke-direct {p0, p1}, LX/Bu0;->a(LX/7M5;)V

    return-void
.end method
