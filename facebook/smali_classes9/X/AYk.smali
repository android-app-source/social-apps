.class public LX/AYk;
.super LX/AWT;
.source ""


# instance fields
.field public a:Lcom/facebook/ipc/media/MediaItem;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:LX/0hB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final c:Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;

.field private final f:Lcom/facebook/fig/button/FigButton;

.field private final g:Landroid/view/View;

.field public h:LX/1bF;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1686042
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/AYk;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1686043
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1686044
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/AYk;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1686045
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1686046
    invoke-direct {p0, p1, p2, p3}, LX/AWT;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1686047
    const-class v0, LX/AYk;

    invoke-static {v0, p0}, LX/AYk;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1686048
    const v0, 0x7f0305a4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1686049
    const v0, 0x7f0d0f73

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;

    iput-object v0, p0, LX/AYk;->c:Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;

    .line 1686050
    const v0, 0x7f0d0f75

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/button/FigButton;

    iput-object v0, p0, LX/AYk;->f:Lcom/facebook/fig/button/FigButton;

    .line 1686051
    const v0, 0x7f0d0f74

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/AYk;->g:Landroid/view/View;

    .line 1686052
    iget-object v0, p0, LX/AYk;->f:Lcom/facebook/fig/button/FigButton;

    new-instance v1, LX/AYj;

    invoke-direct {v1, p0}, LX/AYj;-><init>(LX/AYk;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1686053
    iget-object v0, p0, LX/AYk;->g:Landroid/view/View;

    invoke-virtual {p0}, LX/AYk;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1}, LX/Ac1;->b(Landroid/content/res/Resources;)LX/Ac1;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1686054
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/AYk;

    invoke-static {p0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object p0

    check-cast p0, LX/0hB;

    iput-object p0, p1, LX/AYk;->b:LX/0hB;

    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1686055
    iget-object v0, p0, LX/AYk;->h:LX/1bF;

    if-eqz v0, :cond_0

    .line 1686056
    iget-object v0, p0, LX/AYk;->h:LX/1bF;

    const/4 v1, 0x0

    invoke-interface {v0, v1, v2, v2}, LX/1bF;->a(Lcom/facebook/ipc/media/MediaItem;FF)V

    .line 1686057
    const/4 v0, 0x1

    .line 1686058
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
