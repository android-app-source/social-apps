.class public LX/AjZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8qo;


# instance fields
.field private final a:LX/1nA;


# direct methods
.method public constructor <init>(LX/1nA;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1708140
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1708141
    iput-object p1, p0, LX/AjZ;->a:LX/1nA;

    .line 1708142
    return-void
.end method

.method public static a(LX/0QB;)LX/AjZ;
    .locals 1

    .prologue
    .line 1708143
    invoke-static {p0}, LX/AjZ;->b(LX/0QB;)LX/AjZ;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/AjZ;
    .locals 2

    .prologue
    .line 1708144
    new-instance v1, LX/AjZ;

    invoke-static {p0}, LX/1nA;->a(LX/0QB;)LX/1nA;

    move-result-object v0

    check-cast v0, LX/1nA;

    invoke-direct {v1, v0}, LX/AjZ;-><init>(LX/1nA;)V

    .line 1708145
    return-object v1
.end method


# virtual methods
.method public final a(Landroid/view/View;LX/1y5;Lcom/facebook/analytics/logger/HoneyClientEvent;Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 1708146
    iget-object v0, p0, LX/AjZ;->a:LX/1nA;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, v5

    invoke-virtual/range {v0 .. v6}, LX/1nA;->a(Landroid/view/View;LX/1y5;Lcom/facebook/analytics/logger/HoneyClientEvent;Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V

    .line 1708147
    return-void
.end method

.method public final a(Landroid/view/View;Lcom/facebook/graphql/model/GraphQLProfile;Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 7
    .param p3    # Lcom/facebook/analytics/logger/HoneyClientEvent;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x0

    .line 1708148
    iget-object v0, p0, LX/AjZ;->a:LX/1nA;

    invoke-static {p2}, LX/3Bd;->a(Lcom/facebook/graphql/model/GraphQLProfile;)LX/1y5;

    move-result-object v2

    move-object v1, p1

    move-object v3, p3

    move-object v5, v4

    move-object v6, v4

    invoke-virtual/range {v0 .. v6}, LX/1nA;->b(Landroid/view/View;LX/1y5;Lcom/facebook/analytics/logger/HoneyClientEvent;Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1708149
    return-void
.end method
