.class public final LX/AVd;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;)V
    .locals 0

    .prologue
    .line 1680145
    iput-object p1, p0, LX/AVd;->a:Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;B)V
    .locals 0

    .prologue
    .line 1680144
    invoke-direct {p0, p1}, LX/AVd;-><init>(Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 1680138
    iget-object v0, p0, LX/AVd;->a:Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;

    iget-object v0, v0, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->w:LX/AVh;

    const-string v1, "nux_video_skip"

    iget-object v2, p0, LX/AVd;->a:Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;

    iget-object v2, v2, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->C:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v2}, Lcom/facebook/video/player/RichVideoPlayer;->getCurrentPositionMs()I

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/AVh;->a(Ljava/lang/String;I)V

    .line 1680139
    iget-object v0, p0, LX/AVd;->a:Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;

    invoke-static {v0}, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->n(Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;)V

    .line 1680140
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 1680141
    iget-object v0, p0, LX/AVd;->a:Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;

    iget-object v0, v0, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->w:LX/AVh;

    const-string v1, "nux_video_finish"

    iget-object v2, p0, LX/AVd;->a:Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;

    iget-object v2, v2, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->C:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v2}, Lcom/facebook/video/player/RichVideoPlayer;->getCurrentPositionMs()I

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/AVh;->a(Ljava/lang/String;I)V

    .line 1680142
    iget-object v0, p0, LX/AVd;->a:Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;

    invoke-static {v0}, Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;->n(Lcom/facebook/facecast/broadcast/nux/FacecastBroadcastNuxActivity;)V

    .line 1680143
    return-void
.end method
