.class public LX/B2u;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1qM;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/B2v;


# direct methods
.method public constructor <init>(LX/0Or;LX/B2v;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;",
            "LX/B2v;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1739261
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1739262
    iput-object p1, p0, LX/B2u;->a:LX/0Or;

    .line 1739263
    iput-object p2, p0, LX/B2u;->b:LX/B2v;

    .line 1739264
    return-void
.end method

.method public static a(LX/0QB;)LX/B2u;
    .locals 5

    .prologue
    .line 1739265
    const-class v1, LX/B2u;

    monitor-enter v1

    .line 1739266
    :try_start_0
    sget-object v0, LX/B2u;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1739267
    sput-object v2, LX/B2u;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1739268
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1739269
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1739270
    new-instance v4, LX/B2u;

    const/16 v3, 0xb83

    invoke-static {v0, v3}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    .line 1739271
    new-instance v3, LX/B2v;

    invoke-direct {v3}, LX/B2v;-><init>()V

    .line 1739272
    move-object v3, v3

    .line 1739273
    move-object v3, v3

    .line 1739274
    check-cast v3, LX/B2v;

    invoke-direct {v4, p0, v3}, LX/B2u;-><init>(LX/0Or;LX/B2v;)V

    .line 1739275
    move-object v0, v4

    .line 1739276
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1739277
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/B2u;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1739278
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1739279
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 4

    .prologue
    .line 1739280
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v0, v0

    .line 1739281
    const-string v1, "group_blacklist_groups_you_should_join"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1739282
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 1739283
    const-string v1, "blacklistGroupsYouShouldJoinParamsKey"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1739284
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1739285
    sget-object v0, LX/1nY;->OTHER:LX/1nY;

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 1739286
    :goto_0
    move-object v0, v0

    .line 1739287
    return-object v0

    .line 1739288
    :cond_0
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "Unknown type"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1739289
    :cond_1
    iget-object v0, p0, LX/B2u;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11H;

    iget-object v2, p0, LX/B2u;->b:LX/B2v;

    .line 1739290
    iget-object v3, p1, LX/1qK;->mCallerContext:Lcom/facebook/common/callercontext/CallerContext;

    move-object v3, v3

    .line 1739291
    invoke-virtual {v0, v2, v1, v3}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 1739292
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1739293
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 1739294
    goto :goto_0

    :cond_2
    sget-object v0, LX/1nY;->OTHER:LX/1nY;

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0
.end method
