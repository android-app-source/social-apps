.class public LX/Bwd;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:LX/0xX;

.field public c:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "LX/Bwc;",
            ">;"
        }
    .end annotation
.end field

.field private d:LX/Ag7;

.field private e:LX/Ag7;

.field public f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/3Hc;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/3Ha;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1834097
    const-class v0, LX/Bwd;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Bwd;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/0Or;LX/0xX;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/3Hc;",
            ">;",
            "LX/0Or",
            "<",
            "LX/Ag7;",
            ">;",
            "LX/0xX;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1834101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1834102
    iput-object p3, p0, LX/Bwd;->b:LX/0xX;

    .line 1834103
    iput-object p1, p0, LX/Bwd;->f:LX/0Or;

    .line 1834104
    invoke-interface {p2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ag7;

    iput-object v0, p0, LX/Bwd;->e:LX/Ag7;

    .line 1834105
    iget-object v0, p0, LX/Bwd;->e:LX/Ag7;

    iget-object v1, p0, LX/Bwd;->b:LX/0xX;

    .line 1834106
    iget-object v2, v1, LX/0xX;->c:LX/0ad;

    sget p1, LX/0xY;->w:I

    const/16 p3, 0x3c

    invoke-interface {v2, p1, p3}, LX/0ad;->a(II)I

    move-result v2

    move v1, v2

    .line 1834107
    iput v1, v0, LX/Ag7;->p:I

    .line 1834108
    iget-object v0, p0, LX/Bwd;->e:LX/Ag7;

    const/4 v1, 0x1

    .line 1834109
    iput-boolean v1, v0, LX/Ag7;->o:Z

    .line 1834110
    iget-object v0, p0, LX/Bwd;->e:LX/Ag7;

    .line 1834111
    iput-object p0, v0, LX/Ag7;->m:LX/Bwd;

    .line 1834112
    invoke-interface {p2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ag7;

    iput-object v0, p0, LX/Bwd;->d:LX/Ag7;

    .line 1834113
    iget-object v0, p0, LX/Bwd;->d:LX/Ag7;

    const/4 v1, 0x0

    .line 1834114
    iput-boolean v1, v0, LX/Ag7;->o:Z

    .line 1834115
    iget-object v0, p0, LX/Bwd;->d:LX/Ag7;

    .line 1834116
    iput-object p0, v0, LX/Ag7;->m:LX/Bwd;

    .line 1834117
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/Bwd;->c:Ljava/util/HashMap;

    .line 1834118
    new-instance v0, Ljava/lang/ref/WeakReference;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/Bwd;->g:Ljava/lang/ref/WeakReference;

    .line 1834119
    return-void
.end method

.method public static a(LX/0QB;)LX/Bwd;
    .locals 4

    .prologue
    .line 1834098
    new-instance v1, LX/Bwd;

    const/16 v0, 0x537

    invoke-static {p0, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    const/16 v0, 0x1c15

    invoke-static {p0, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-static {p0}, LX/0xX;->a(LX/0QB;)LX/0xX;

    move-result-object v0

    check-cast v0, LX/0xX;

    invoke-direct {v1, v2, v3, v0}, LX/Bwd;-><init>(LX/0Or;LX/0Or;LX/0xX;)V

    .line 1834099
    move-object v0, v1

    .line 1834100
    return-object v0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 4

    .prologue
    .line 1834085
    monitor-enter p0

    :try_start_0
    new-instance v2, Ljava/util/ArrayList;

    iget-object v0, p0, LX/Bwd;->c:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1834086
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1834087
    invoke-virtual {p0, v0}, LX/Bwd;->b(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1834088
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1834089
    :cond_0
    monitor-exit p0

    return-void

    .line 1834090
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1834091
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Bwd;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Bwc;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1834092
    if-nez v0, :cond_0

    .line 1834093
    :goto_0
    monitor-exit p0

    return-void

    .line 1834094
    :cond_0
    const/4 v1, 0x1

    :try_start_1
    invoke-virtual {p0, p1, v1}, LX/Bwd;->a(Ljava/lang/String;Z)V

    .line 1834095
    iget-object v1, p0, LX/Bwd;->g:Ljava/lang/ref/WeakReference;

    iput-object v1, v0, LX/Bwc;->d:Ljava/lang/ref/WeakReference;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1834096
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;LX/3Ha;)V
    .locals 3

    .prologue
    .line 1834035
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Bwd;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Bwc;

    .line 1834036
    if-nez v0, :cond_1

    .line 1834037
    new-instance v0, LX/Bwc;

    invoke-direct {v0, p0}, LX/Bwc;-><init>(LX/Bwd;)V

    .line 1834038
    iput-object p1, v0, LX/Bwc;->a:Ljava/lang/String;

    .line 1834039
    iget-object v1, p0, LX/Bwd;->c:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1834040
    :cond_0
    :goto_0
    iput-object p2, v0, LX/Bwc;->b:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    .line 1834041
    iget-object v1, p0, LX/Bwd;->e:LX/Ag7;

    iput-object v1, v0, LX/Bwc;->e:LX/Ag7;

    .line 1834042
    iget-object v1, p0, LX/Bwd;->e:LX/Ag7;

    invoke-virtual {v1, p1}, LX/Ag7;->b(Ljava/lang/String;)V

    .line 1834043
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v1, v0, LX/Bwc;->d:Ljava/lang/ref/WeakReference;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1834044
    monitor-exit p0

    return-void

    .line 1834045
    :cond_1
    :try_start_1
    iget-object v1, v0, LX/Bwc;->e:LX/Ag7;

    iget-object v2, p0, LX/Bwd;->e:LX/Ag7;

    if-eq v1, v2, :cond_0

    .line 1834046
    iget-object v1, v0, LX/Bwc;->e:LX/Ag7;

    invoke-virtual {v1, p1}, LX/Ag7;->c(Ljava/lang/String;)V

    .line 1834047
    const/4 v1, 0x0

    iput-object v1, v0, LX/Bwc;->e:LX/Ag7;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1834048
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;Z)V
    .locals 3

    .prologue
    .line 1834049
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Bwd;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Bwc;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1834050
    if-nez v0, :cond_1

    .line 1834051
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 1834052
    :cond_1
    :try_start_1
    iget-object v1, v0, LX/Bwc;->e:LX/Ag7;

    iget-object v2, p0, LX/Bwd;->d:LX/Ag7;

    if-eq v1, v2, :cond_4

    const/4 v1, 0x1

    .line 1834053
    :goto_1
    if-eqz v1, :cond_2

    if-nez p2, :cond_0

    :cond_2
    if-nez v1, :cond_3

    if-eqz p2, :cond_0

    .line 1834054
    :cond_3
    iget-object v2, v0, LX/Bwc;->e:LX/Ag7;

    .line 1834055
    if-eqz p2, :cond_5

    iget-object v1, p0, LX/Bwd;->e:LX/Ag7;

    .line 1834056
    :goto_2
    invoke-virtual {v2, p1}, LX/Ag7;->c(Ljava/lang/String;)V

    .line 1834057
    invoke-virtual {v1, p1}, LX/Ag7;->b(Ljava/lang/String;)V

    .line 1834058
    iput-object v1, v0, LX/Bwc;->e:LX/Ag7;

    .line 1834059
    if-nez p2, :cond_0

    .line 1834060
    const-string v0, "videoMovedToActivePoller"

    invoke-virtual {v1, v0}, LX/Ag7;->a(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1834061
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1834062
    :cond_4
    const/4 v1, 0x0

    goto :goto_1

    .line 1834063
    :cond_5
    :try_start_2
    iget-object v1, p0, LX/Bwd;->d:LX/Ag7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2
.end method

.method public final declared-synchronized b()V
    .locals 2

    .prologue
    .line 1834064
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Bwd;->d:LX/Ag7;

    const/4 v1, 0x0

    .line 1834065
    iput-boolean v1, v0, LX/Ag7;->q:Z

    .line 1834066
    iget-object v0, p0, LX/Bwd;->e:LX/Ag7;

    const/4 v1, 0x0

    .line 1834067
    iput-boolean v1, v0, LX/Ag7;->q:Z

    .line 1834068
    iget-object v0, p0, LX/Bwd;->d:LX/Ag7;

    invoke-virtual {v0}, LX/Ag7;->a()V

    .line 1834069
    iget-object v0, p0, LX/Bwd;->e:LX/Ag7;

    invoke-virtual {v0}, LX/Ag7;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1834070
    monitor-exit p0

    return-void

    .line 1834071
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1834072
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Bwd;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Bwc;

    .line 1834073
    if-eqz v0, :cond_0

    iget-object v1, v0, LX/Bwc;->e:LX/Ag7;

    if-eqz v1, :cond_0

    .line 1834074
    iget-object v0, v0, LX/Bwc;->e:LX/Ag7;

    invoke-virtual {v0, p1}, LX/Ag7;->c(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1834075
    :cond_0
    monitor-exit p0

    return-void

    .line 1834076
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()V
    .locals 2

    .prologue
    .line 1834077
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Bwd;->d:LX/Ag7;

    const/4 v1, 0x1

    .line 1834078
    iput-boolean v1, v0, LX/Ag7;->q:Z

    .line 1834079
    iget-object v0, p0, LX/Bwd;->e:LX/Ag7;

    const/4 v1, 0x1

    .line 1834080
    iput-boolean v1, v0, LX/Ag7;->q:Z

    .line 1834081
    iget-object v0, p0, LX/Bwd;->d:LX/Ag7;

    const-string v1, "resumeAllPollers"

    invoke-virtual {v0, v1}, LX/Ag7;->a(Ljava/lang/String;)V

    .line 1834082
    iget-object v0, p0, LX/Bwd;->e:LX/Ag7;

    const-string v1, "resumeAllPollers"

    invoke-virtual {v0, v1}, LX/Ag7;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1834083
    monitor-exit p0

    return-void

    .line 1834084
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
