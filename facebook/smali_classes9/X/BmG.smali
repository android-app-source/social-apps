.class public final LX/BmG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;)V
    .locals 0

    .prologue
    .line 1818023
    iput-object p1, p0, LX/BmG;->a:Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const v0, 0x38500b89

    invoke-static {v5, v4, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1818024
    iget-object v0, p0, LX/BmG;->a:Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;

    iget-object v0, v0, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->v:Lcom/facebook/widget/CustomLinearLayout;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/facebook/widget/CustomLinearLayout;->setVisibility(I)V

    .line 1818025
    iget-object v0, p0, LX/BmG;->a:Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;

    iget-object v0, v0, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->y:Ljava/util/Calendar;

    if-nez v0, :cond_0

    .line 1818026
    iget-object v0, p0, LX/BmG;->a:Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    iput-object v2, v0, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->y:Ljava/util/Calendar;

    .line 1818027
    iget-object v0, p0, LX/BmG;->a:Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;

    iget-object v0, v0, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->y:Ljava/util/Calendar;

    iget-object v2, p0, LX/BmG;->a:Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;

    iget-object v2, v2, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->x:Ljava/util/Calendar;

    invoke-virtual {v2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 1818028
    iget-object v0, p0, LX/BmG;->a:Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;

    iget-object v0, v0, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->y:Ljava/util/Calendar;

    const/16 v2, 0xb

    const/4 v3, 0x3

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->add(II)V

    .line 1818029
    :cond_0
    iget-object v0, p0, LX/BmG;->a:Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;

    iget-object v0, v0, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->r:Lcom/facebook/uicontrib/calendar/CalendarView;

    invoke-virtual {v0, v4}, Lcom/facebook/uicontrib/calendar/CalendarView;->setIsSelectingSecondDate(Z)V

    .line 1818030
    iget-object v0, p0, LX/BmG;->a:Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;

    iput-boolean v4, v0, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->A:Z

    .line 1818031
    iget-object v0, p0, LX/BmG;->a:Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;

    iget-object v0, v0, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->r:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-object v2, p0, LX/BmG;->a:Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;

    iget-object v2, v2, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->y:Ljava/util/Calendar;

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/facebook/uicontrib/calendar/CalendarView;->setDate(J)V

    .line 1818032
    iget-object v0, p0, LX/BmG;->a:Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;

    iget-object v0, v0, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->t:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbTextView;->performClick()Z

    .line 1818033
    iget-object v0, p0, LX/BmG;->a:Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;

    iget-object v0, v0, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->z:Landroid/widget/TimePicker;

    invoke-virtual {v0}, Landroid/widget/TimePicker;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 1818034
    iget-object v2, p0, LX/BmG;->a:Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;

    invoke-virtual {v2}, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b156a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 1818035
    iget-object v0, p0, LX/BmG;->a:Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;

    iget-object v0, v0, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->u:Lcom/facebook/widget/CustomLinearLayout;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Lcom/facebook/widget/CustomLinearLayout;->setVisibility(I)V

    .line 1818036
    const v0, -0x3a1cfe1f

    invoke-static {v5, v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
