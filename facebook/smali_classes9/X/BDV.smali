.class public final LX/BDV;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1763344
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_5

    .line 1763345
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1763346
    :goto_0
    return v1

    .line 1763347
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1763348
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_4

    .line 1763349
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1763350
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1763351
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 1763352
    const-string v4, "highlight_operations"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1763353
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1763354
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->START_ARRAY:LX/15z;

    if-ne v3, v4, :cond_2

    .line 1763355
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_ARRAY:LX/15z;

    if-eq v3, v4, :cond_2

    .line 1763356
    invoke-static {p0, p1}, LX/BDS;->b(LX/15w;LX/186;)I

    move-result v3

    .line 1763357
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1763358
    :cond_2
    invoke-static {v2, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v2

    move v2, v2

    .line 1763359
    goto :goto_1

    .line 1763360
    :cond_3
    const-string v4, "reaction_unit"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1763361
    invoke-static {p0, p1}, LX/9rK;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 1763362
    :cond_4
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1763363
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1763364
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1763365
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v2, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1763366
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1763367
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1763368
    if-eqz v0, :cond_1

    .line 1763369
    const-string v1, "highlight_operations"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1763370
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1763371
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 1763372
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2, p3}, LX/BDS;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1763373
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1763374
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1763375
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1763376
    if-eqz v0, :cond_2

    .line 1763377
    const-string v1, "reaction_unit"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1763378
    invoke-static {p0, v0, p2, p3}, LX/9rK;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1763379
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1763380
    return-void
.end method
