.class public final LX/AlX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/AlW;


# instance fields
.field public final synthetic a:LX/1kG;

.field public final b:Lcom/facebook/feed/photoreminder/PhotoReminderV2View;

.field public c:LX/1lR;


# direct methods
.method public constructor <init>(LX/1kG;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1709764
    iput-object p1, p0, LX/AlX;->a:LX/1kG;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1709765
    new-instance v0, Lcom/facebook/feed/photoreminder/PhotoReminderV2View;

    invoke-direct {v0, p2}, Lcom/facebook/feed/photoreminder/PhotoReminderV2View;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/AlX;->b:Lcom/facebook/feed/photoreminder/PhotoReminderV2View;

    .line 1709766
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1709774
    iget-object v0, p0, LX/AlX;->c:LX/1lR;

    .line 1709775
    iget v1, v0, LX/1lR;->g:I

    move v0, v1

    .line 1709776
    if-nez v0, :cond_0

    .line 1709777
    iget-object v0, p0, LX/AlX;->a:LX/1kG;

    iget-object v0, v0, LX/1kG;->b:Landroid/content/Context;

    const v1, 0x7f081b33

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1709778
    :goto_0
    return-object v0

    .line 1709779
    :cond_0
    iget-object v0, p0, LX/AlX;->c:LX/1lR;

    .line 1709780
    iget v1, v0, LX/1lR;->f:I

    move v0, v1

    .line 1709781
    if-nez v0, :cond_1

    .line 1709782
    iget-object v0, p0, LX/AlX;->a:LX/1kG;

    iget-object v0, v0, LX/1kG;->b:Landroid/content/Context;

    const v1, 0x7f081b34

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1709783
    :cond_1
    iget-object v0, p0, LX/AlX;->a:LX/1kG;

    iget-object v0, v0, LX/1kG;->b:Landroid/content/Context;

    const v1, 0x7f081b35

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1709773
    iget-object v0, p0, LX/AlX;->a:LX/1kG;

    iget-object v0, v0, LX/1kG;->b:Landroid/content/Context;

    const v1, 0x7f081b36

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/Integer;
    .locals 2

    .prologue
    .line 1709772
    iget-object v0, p0, LX/AlX;->a:LX/1kG;

    iget-object v0, v0, LX/1kG;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00e5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final d()Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 1709771
    iget-object v0, p0, LX/AlX;->a:LX/1kG;

    iget-object v0, v0, LX/1kG;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0214dd

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public final e()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 1709770
    const/4 v0, 0x0

    return-object v0
.end method

.method public final f()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 1709769
    const v0, 0x7f02145e

    invoke-static {v0}, LX/1bQ;->a(I)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public final g()LX/AkM;
    .locals 1

    .prologue
    .line 1709768
    iget-object v0, p0, LX/AlX;->b:Lcom/facebook/feed/photoreminder/PhotoReminderV2View;

    return-object v0
.end method

.method public final h()Lcom/facebook/productionprompts/model/PromptDisplayReason;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1709767
    const/4 v0, 0x0

    return-object v0
.end method
