.class public final LX/Bcm;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<TEdge:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field public static final a:LX/Bcm;


# instance fields
.field public final b:I

.field public final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<[",
            "LX/Bcl",
            "<TTEdge;>;>;"
        }
    .end annotation
.end field

.field public final d:Z

.field public final e:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1802713
    new-instance v0, LX/Bcm;

    .line 1802714
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 1802715
    const/4 v2, 0x1

    invoke-direct {v0, v3, v1, v3, v2}, LX/Bcm;-><init>(ILX/0Px;ZZ)V

    sput-object v0, LX/Bcm;->a:LX/Bcm;

    return-void
.end method

.method public constructor <init>(ILX/0Px;ZZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "LX/0Px",
            "<[",
            "LX/Bcl",
            "<TTEdge;>;>;ZZ)V"
        }
    .end annotation

    .prologue
    .line 1802716
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1802717
    iput p1, p0, LX/Bcm;->b:I

    .line 1802718
    iput-object p2, p0, LX/Bcm;->c:LX/0Px;

    .line 1802719
    iput-boolean p3, p0, LX/Bcm;->d:Z

    .line 1802720
    iput-boolean p4, p0, LX/Bcm;->e:Z

    .line 1802721
    return-void
.end method

.method public static a(LX/Bcm;LX/Bcm;)LX/Bcm;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<TEdge:",
            "Ljava/lang/Object;",
            ">(",
            "LX/Bcm",
            "<TTEdge;>;",
            "LX/Bcm",
            "<TTEdge;>;)",
            "LX/Bcm",
            "<TTEdge;>;"
        }
    .end annotation

    .prologue
    .line 1802722
    new-instance v0, LX/Bcm;

    iget v1, p1, LX/Bcm;->b:I

    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    iget-object v3, p0, LX/Bcm;->c:LX/0Px;

    invoke-virtual {v2, v3}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v2

    iget-object v3, p1, LX/Bcm;->c:LX/0Px;

    invoke-virtual {v2, v3}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v2

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    iget-boolean v3, p1, LX/Bcm;->d:Z

    iget-boolean v4, p1, LX/Bcm;->e:Z

    invoke-direct {v0, v1, v2, v3, v4}, LX/Bcm;-><init>(ILX/0Px;ZZ)V

    return-object v0
.end method
