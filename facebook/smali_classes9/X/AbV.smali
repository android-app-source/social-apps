.class public final LX/AbV;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0Px;

.field public b:LX/0Px;

.field public c:LX/0Px;

.field public d:LX/0P1;


# direct methods
.method public constructor <init>(LX/0Px;Z)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1690802
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1690803
    iput-object v0, p0, LX/AbV;->a:LX/0Px;

    .line 1690804
    iput-object v0, p0, LX/AbV;->b:LX/0Px;

    .line 1690805
    iput-object v0, p0, LX/AbV;->c:LX/0Px;

    .line 1690806
    iput-object v0, p0, LX/AbV;->d:LX/0P1;

    .line 1690807
    if-eqz p1, :cond_b

    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_b

    .line 1690808
    const/4 v1, 0x0

    .line 1690809
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v4, v0

    move-object v2, v1

    move-object v3, v1

    :goto_0
    if-ge v4, v5, :cond_4

    invoke-virtual {p1, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    .line 1690810
    sget-object v6, LX/AbT;->b:[I

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->is_()Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    :goto_1
    move-object v0, v1

    move-object v1, v2

    move-object v2, v3

    .line 1690811
    :goto_2
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    move-object v3, v2

    move-object v2, v1

    move-object v1, v0

    goto :goto_0

    .line 1690812
    :pswitch_0
    if-nez v3, :cond_0

    .line 1690813
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1690814
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-object v0, v1

    move-object v1, v2

    move-object v2, v3

    .line 1690815
    goto :goto_2

    .line 1690816
    :pswitch_1
    if-nez v2, :cond_1

    .line 1690817
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1690818
    :cond_1
    invoke-static {v0}, LX/AbV;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;)LX/0P1;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-object v0, v1

    move-object v1, v2

    move-object v2, v3

    .line 1690819
    goto :goto_2

    .line 1690820
    :pswitch_2
    if-nez v1, :cond_2

    .line 1690821
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 1690822
    :cond_2
    if-eqz p2, :cond_3

    .line 1690823
    invoke-static {v0}, LX/AbV;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;)LX/0P1;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-object v0, v1

    move-object v1, v2

    move-object v2, v3

    goto :goto_2

    .line 1690824
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 1690825
    :cond_4
    if-eqz v3, :cond_5

    .line 1690826
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/AbV;->a:LX/0Px;

    .line 1690827
    :cond_5
    if-eqz v2, :cond_6

    .line 1690828
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/AbV;->b:LX/0Px;

    .line 1690829
    :cond_6
    if-eqz v1, :cond_7

    .line 1690830
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/AbV;->c:LX/0Px;

    .line 1690831
    :cond_7
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    .line 1690832
    iget-object v1, p0, LX/AbV;->a:LX/0Px;

    if-eqz v1, :cond_8

    .line 1690833
    const-string v1, "countries"

    iget-object v2, p0, LX/AbV;->a:LX/0Px;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 1690834
    :cond_8
    iget-object v1, p0, LX/AbV;->b:LX/0Px;

    if-eqz v1, :cond_9

    .line 1690835
    const-string v1, "regions"

    iget-object v2, p0, LX/AbV;->b:LX/0Px;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 1690836
    :cond_9
    iget-object v1, p0, LX/AbV;->c:LX/0Px;

    if-eqz v1, :cond_a

    .line 1690837
    const-string v1, "cities"

    iget-object v2, p0, LX/AbV;->c:LX/0Px;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 1690838
    :cond_a
    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    iput-object v0, p0, LX/AbV;->d:LX/0P1;

    .line 1690839
    :cond_b
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;)LX/0P1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;",
            ")",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1690840
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    const-string v1, "key"

    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "name"

    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "country"

    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    return-object v0
.end method
