.class public final LX/BpP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/20Z;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLFeedback;

.field public final synthetic b:LX/5kD;

.field public final synthetic c:LX/1Po;

.field public final synthetic d:Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentReactionsFooterPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentReactionsFooterPartDefinition;Lcom/facebook/graphql/model/GraphQLFeedback;LX/5kD;LX/1Po;)V
    .locals 0

    .prologue
    .line 1823035
    iput-object p1, p0, LX/BpP;->d:Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentReactionsFooterPartDefinition;

    iput-object p2, p0, LX/BpP;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    iput-object p3, p0, LX/BpP;->b:LX/5kD;

    iput-object p4, p0, LX/BpP;->c:LX/1Po;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;LX/20X;)V
    .locals 6

    .prologue
    .line 1823036
    iget-object v0, p0, LX/BpP;->d:Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentReactionsFooterPartDefinition;

    iget-object v3, p0, LX/BpP;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    iget-object v4, p0, LX/BpP;->b:LX/5kD;

    iget-object v5, p0, LX/BpP;->c:LX/1Po;

    move-object v1, p1

    move-object v2, p2

    .line 1823037
    sget-object p0, LX/BpR;->a:[I

    invoke-virtual {v2}, LX/20X;->ordinal()I

    move-result p1

    aget p0, p0, p1

    packed-switch p0, :pswitch_data_0

    .line 1823038
    :goto_0
    return-void

    .line 1823039
    :pswitch_0
    invoke-static {v0, v1, v3, v5}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentReactionsFooterPartDefinition;->a(Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentReactionsFooterPartDefinition;Landroid/view/View;Lcom/facebook/graphql/model/GraphQLFeedback;LX/1Po;)V

    goto :goto_0

    .line 1823040
    :pswitch_1
    invoke-interface {v4}, LX/5kD;->G()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;

    move-result-object p0

    invoke-static {p0}, LX/5k9;->a(Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object p1

    .line 1823041
    iget-object p0, v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentReactionsFooterPartDefinition;->d:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/3RX;

    const-string v2, "comment"

    invoke-virtual {p0, v2}, LX/3RX;->a(Ljava/lang/String;)V

    .line 1823042
    check-cast v5, LX/Bp2;

    sget-object p0, LX/An0;->PHOTOS_FEED_FOOTER:LX/An0;

    invoke-interface {v5, p1, v1, p0}, LX/Bp2;->a(Lcom/facebook/graphql/model/GraphQLFeedback;Landroid/view/View;LX/An0;)V

    .line 1823043
    goto :goto_0

    .line 1823044
    :pswitch_2
    iget-object p0, v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentReactionsFooterPartDefinition;->d:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/3RX;

    const-string p1, "share"

    invoke-virtual {p0, p1}, LX/3RX;->a(Ljava/lang/String;)V

    .line 1823045
    iget-object p1, v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentReactionsFooterPartDefinition;->n:LX/1Kf;

    const/4 p2, 0x0

    iget-object p0, v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentReactionsFooterPartDefinition;->q:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-interface {v4}, LX/5kD;->d()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v5}, LX/1Po;->c()LX/1PT;

    move-result-object v2

    invoke-static {v2}, LX/9Ir;->a(LX/1PT;)LX/21D;

    move-result-object v2

    const-string v3, "photosFeedAttachmentReactionsFooter"

    invoke-interface {p0, v1, v2, v3}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(Ljava/lang/String;LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object p0

    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setIsFireAndForget(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object p0

    iget-object v1, v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentReactionsFooterPartDefinition;->l:Landroid/content/Context;

    invoke-interface {p1, p2, p0, v1}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;Landroid/content/Context;)V

    .line 1823046
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
