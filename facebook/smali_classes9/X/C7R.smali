.class public final LX/C7R;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8pU;


# instance fields
.field public final synthetic a:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic b:LX/3AH;

.field public final synthetic c:LX/3AB;


# direct methods
.method public constructor <init>(LX/3AB;Lcom/facebook/feed/rows/core/props/FeedProps;LX/3AH;)V
    .locals 0

    .prologue
    .line 1851328
    iput-object p1, p0, LX/C7R;->c:LX/3AB;

    iput-object p2, p0, LX/C7R;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iput-object p3, p0, LX/C7R;->b:LX/3AH;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 5

    .prologue
    .line 1851329
    iget-object v0, p0, LX/C7R;->c:LX/3AB;

    iget-object v1, v0, LX/3AB;->c:LX/3AD;

    iget-object v0, p0, LX/C7R;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1851330
    iget-object v2, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v2

    .line 1851331
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    iget-object v2, p0, LX/C7R;->b:LX/3AH;

    .line 1851332
    iget-object v3, v2, LX/3AH;->a:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsGraphQLModels$ContentTranslationFragmentModel;

    if-nez v3, :cond_0

    .line 1851333
    const-string v3, ""

    .line 1851334
    :goto_0
    move-object v2, v3

    .line 1851335
    iget-object v3, p0, LX/C7R;->b:LX/3AH;

    .line 1851336
    iget-object p0, v3, LX/3AH;->a:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsGraphQLModels$ContentTranslationFragmentModel;

    if-nez p0, :cond_1

    .line 1851337
    const-string p0, ""

    .line 1851338
    :goto_1
    move-object v3, p0

    .line 1851339
    new-instance v4, LX/4Jw;

    invoke-direct {v4}, LX/4Jw;-><init>()V

    const-string p0, "SEE_TRANSLATION"

    invoke-virtual {v4, p0}, LX/4Jw;->d(Ljava/lang/String;)LX/4Jw;

    move-result-object v4

    invoke-virtual {v4, v2}, LX/4Jw;->c(Ljava/lang/String;)LX/4Jw;

    move-result-object v4

    invoke-virtual {v4, v3}, LX/4Jw;->b(Ljava/lang/String;)LX/4Jw;

    move-result-object v4

    .line 1851340
    invoke-static {v1, v4, v0, p1}, LX/3AD;->a(LX/3AD;LX/4Jw;Lcom/facebook/graphql/model/GraphQLStory;I)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1851341
    return-void

    :cond_0
    iget-object v3, v2, LX/3AH;->a:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsGraphQLModels$ContentTranslationFragmentModel;

    invoke-virtual {v3}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsGraphQLModels$ContentTranslationFragmentModel;->k()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    :cond_1
    iget-object p0, v3, LX/3AH;->a:Lcom/facebook/api/graphql/feed/NewsFeedDefaultsGraphQLModels$ContentTranslationFragmentModel;

    invoke-virtual {p0}, Lcom/facebook/api/graphql/feed/NewsFeedDefaultsGraphQLModels$ContentTranslationFragmentModel;->j()Ljava/lang/String;

    move-result-object p0

    goto :goto_1
.end method
