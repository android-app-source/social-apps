.class public LX/B3I;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1740102
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 1740103
    return-void
.end method


# virtual methods
.method public final a(Landroid/app/Activity;Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;)Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;
    .locals 18

    .prologue
    .line 1740099
    new-instance v2, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-direct {v2, v0, v1}, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;-><init>(Landroid/app/Activity;Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;)V

    .line 1740100
    const/16 v3, 0x15e7

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    const/16 v4, 0xc49

    move-object/from16 v0, p0

    invoke-static {v0, v4}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static/range {p0 .. p0}, LX/B3O;->a(LX/0QB;)LX/B3O;

    move-result-object v5

    check-cast v5, LX/B3O;

    invoke-static/range {p0 .. p0}, LX/B35;->a(LX/0QB;)LX/B35;

    move-result-object v6

    check-cast v6, LX/B35;

    invoke-static/range {p0 .. p0}, LX/B3R;->a(LX/0QB;)LX/B3R;

    move-result-object v7

    check-cast v7, LX/B3R;

    invoke-static/range {p0 .. p0}, Lcom/facebook/heisman/ProfilePictureOverlayCameraQueryExecutor;->a(LX/0QB;)Lcom/facebook/heisman/ProfilePictureOverlayCameraQueryExecutor;

    move-result-object v8

    check-cast v8, Lcom/facebook/heisman/ProfilePictureOverlayCameraQueryExecutor;

    invoke-static/range {p0 .. p0}, LX/B4H;->a(LX/0QB;)LX/B4H;

    move-result-object v9

    check-cast v9, LX/B4H;

    invoke-static/range {p0 .. p0}, LX/B4O;->a(LX/0QB;)LX/B4O;

    move-result-object v10

    check-cast v10, LX/B4O;

    invoke-static/range {p0 .. p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v11

    check-cast v11, Lcom/facebook/content/SecureContextHelper;

    const/16 v12, 0x259

    move-object/from16 v0, p0

    invoke-static {v0, v12}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v12

    invoke-static/range {p0 .. p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v13

    check-cast v13, LX/0TD;

    invoke-static/range {p0 .. p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v14

    check-cast v14, Ljava/util/concurrent/Executor;

    invoke-static/range {p0 .. p0}, LX/74n;->a(LX/0QB;)LX/74n;

    move-result-object v15

    check-cast v15, LX/74n;

    invoke-static/range {p0 .. p0}, LX/D23;->a(LX/0QB;)LX/D23;

    move-result-object v16

    check-cast v16, LX/B5y;

    invoke-static/range {p0 .. p0}, LX/B4D;->a(LX/0QB;)LX/B4D;

    move-result-object v17

    check-cast v17, LX/B4D;

    invoke-static/range {v2 .. v17}, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->a(Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;LX/0Or;LX/0Or;LX/B3O;LX/B35;LX/B3R;Lcom/facebook/heisman/ProfilePictureOverlayCameraQueryExecutor;LX/B4H;LX/B4O;Lcom/facebook/content/SecureContextHelper;LX/0Ot;LX/0TD;Ljava/util/concurrent/Executor;LX/74n;LX/B5y;LX/B4D;)V

    .line 1740101
    return-object v2
.end method
