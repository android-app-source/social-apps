.class public LX/CDV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3Iy;


# instance fields
.field public final a:LX/1Pq;

.field private final b:LX/1Pr;

.field public final c:LX/1CK;

.field private d:LX/CDg;

.field public final e:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

.field private final g:Lcom/facebook/video/analytics/VideoFeedStoryInfo;


# direct methods
.method public constructor <init>(LX/CDg;LX/1Pq;LX/1Pr;LX/1CK;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/video/analytics/VideoFeedStoryInfo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CDg;",
            "LX/1Pq;",
            "LX/1Pr;",
            "LX/1CK;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            "Lcom/facebook/video/analytics/VideoFeedStoryInfo;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1859455
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1859456
    iput-object p2, p0, LX/CDV;->a:LX/1Pq;

    .line 1859457
    iput-object p3, p0, LX/CDV;->b:LX/1Pr;

    .line 1859458
    iput-object p5, p0, LX/CDV;->e:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1859459
    iput-object p6, p0, LX/CDV;->f:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1859460
    iput-object p7, p0, LX/CDV;->g:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    .line 1859461
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1859462
    iput-object p1, p0, LX/CDV;->d:LX/CDg;

    .line 1859463
    iput-object p4, p0, LX/CDV;->c:LX/1CK;

    .line 1859464
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1859453
    iget-object v0, p0, LX/CDV;->d:LX/CDg;

    const/4 v1, 0x0

    iput-boolean v1, v0, LX/CDg;->i:Z

    .line 1859454
    return-void
.end method

.method public final a(LX/198;LX/04g;LX/7K4;)V
    .locals 6

    .prologue
    .line 1859431
    iget-object v0, p0, LX/CDV;->d:LX/CDg;

    iget-object v0, v0, LX/CDg;->j:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1859432
    iget-object v0, p0, LX/CDV;->d:LX/CDg;

    iget-object v0, v0, LX/CDg;->j:Lcom/facebook/video/player/RichVideoPlayer;

    .line 1859433
    iget-object v1, p0, LX/CDV;->d:LX/CDg;

    iget-object v2, p0, LX/CDV;->g:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-static/range {v0 .. v5}, LX/CDd;->a(Lcom/facebook/video/player/RichVideoPlayer;LX/CDg;Lcom/facebook/video/analytics/VideoFeedStoryInfo;LX/198;LX/04g;LX/7K4;)V

    .line 1859434
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 1859444
    iget-object v0, p0, LX/CDV;->d:LX/CDg;

    iget-object v0, v0, LX/CDg;->j:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1859445
    iget-object v0, p0, LX/CDV;->d:LX/CDg;

    iget-object v0, v0, LX/CDg;->j:Lcom/facebook/video/player/RichVideoPlayer;

    .line 1859446
    iget-object v1, p0, LX/CDV;->g:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    .line 1859447
    iget-object v2, v1, Lcom/facebook/video/analytics/VideoFeedStoryInfo;->b:LX/04g;

    move-object v1, v2

    .line 1859448
    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->setOriginalPlayReason(LX/04g;)V

    .line 1859449
    iget-object v1, p0, LX/CDV;->g:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    .line 1859450
    iget-object v2, v1, Lcom/facebook/video/analytics/VideoFeedStoryInfo;->c:LX/04H;

    move-object v1, v2

    .line 1859451
    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->setChannelEligibility(LX/04H;)V

    .line 1859452
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 1859442
    iget-object v0, p0, LX/CDV;->d:LX/CDg;

    iget-object v1, p0, LX/CDV;->g:Lcom/facebook/video/analytics/VideoFeedStoryInfo;

    sget-object v2, LX/04g;->BY_USER:LX/04g;

    invoke-static {v0, v1, v2}, LX/CDd;->a(LX/CDg;Lcom/facebook/video/analytics/VideoFeedStoryInfo;LX/04g;)V

    .line 1859443
    return-void
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 1859439
    iget-object v0, p0, LX/CDV;->b:LX/1Pr;

    iget-object v1, p0, LX/CDV;->d:LX/CDg;

    iget-object v1, v1, LX/CDg;->k:LX/2oK;

    iget-object v2, p0, LX/CDV;->d:LX/CDg;

    iget-object v2, v2, LX/CDg;->d:LX/2oL;

    invoke-interface {v0, v1, v2}, LX/1Pr;->a(LX/1KL;Ljava/lang/Object;)Z

    .line 1859440
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/facebook/feedplugins/video/components/RichVideoAutoplayTransitionManager$1;

    invoke-direct {v1, p0}, Lcom/facebook/feedplugins/video/components/RichVideoAutoplayTransitionManager$1;-><init>(LX/CDV;)V

    const v2, -0x4fe760f6

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1859441
    return-void
.end method

.method public final e()Landroid/view/View;
    .locals 1

    .prologue
    .line 1859438
    iget-object v0, p0, LX/CDV;->d:LX/CDg;

    iget-object v0, v0, LX/CDg;->j:Lcom/facebook/video/player/RichVideoPlayer;

    return-object v0
.end method

.method public final f()LX/2oO;
    .locals 1

    .prologue
    .line 1859437
    iget-object v0, p0, LX/CDV;->d:LX/CDg;

    iget-object v0, v0, LX/CDg;->c:LX/2oO;

    return-object v0
.end method

.method public final g()LX/2oL;
    .locals 1

    .prologue
    .line 1859436
    iget-object v0, p0, LX/CDV;->d:LX/CDg;

    iget-object v0, v0, LX/CDg;->d:LX/2oL;

    return-object v0
.end method

.method public final h()Lcom/facebook/graphql/model/GraphQLStoryAttachment;
    .locals 1

    .prologue
    .line 1859435
    iget-object v0, p0, LX/CDV;->f:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    return-object v0
.end method
