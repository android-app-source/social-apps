.class public final LX/Cbw;
.super LX/3nE;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/3nE",
        "<",
        "LX/1FJ",
        "<",
        "LX/1ln;",
        ">;",
        "Ljava/lang/Void;",
        "Landroid/net/Uri;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Cbx;


# direct methods
.method public constructor <init>(LX/Cbx;)V
    .locals 0

    .prologue
    .line 1920520
    iput-object p1, p0, LX/Cbw;->a:LX/Cbx;

    invoke-direct {p0}, LX/3nE;-><init>()V

    return-void
.end method


# virtual methods
.method public final a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 1920521
    check-cast p1, [LX/1FJ;

    const/4 v1, 0x0

    const/4 v6, 0x0

    .line 1920522
    const/4 v0, 0x0

    :try_start_0
    aget-object v0, p1, v0

    invoke-virtual {v0}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1lm;

    invoke-virtual {v0}, LX/1lm;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1920523
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1920524
    iget-object v2, p0, LX/Cbw;->a:LX/Cbx;

    iget-object v2, v2, LX/Cbx;->b:LX/CcO;

    iget-object v2, v2, LX/CcO;->h:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryActionsGenerator;

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryActionsGenerator;->a(Landroid/graphics/Bitmap;Ljava/io/File;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1920525
    aget-object v1, p1, v6

    invoke-static {v1}, LX/1FJ;->c(LX/1FJ;)V

    :goto_0
    return-object v0

    .line 1920526
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/Cbw;->a:LX/Cbx;

    iget-object v0, v0, LX/Cbx;->b:LX/CcO;

    iget-object v0, v0, LX/CcO;->a:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->k:LX/03V;

    sget-object v2, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->c:Ljava/lang/String;

    const-string v3, "Could not share file (w/ Fresco + non-jpeg) bitmap is recycled"

    invoke-virtual {v0, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1920527
    aget-object v0, p1, v6

    invoke-static {v0}, LX/1FJ;->c(LX/1FJ;)V

    move-object v0, v1

    goto :goto_0

    .line 1920528
    :catch_0
    move-exception v0

    .line 1920529
    :try_start_2
    iget-object v2, p0, LX/Cbw;->a:LX/Cbx;

    iget-object v2, v2, LX/Cbx;->b:LX/CcO;

    iget-object v2, v2, LX/CcO;->a:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;

    iget-object v2, v2, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->k:LX/03V;

    sget-object v3, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->c:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Could not share file (w/ Fresco + non-jpeg) "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1920530
    aget-object v0, p1, v6

    invoke-static {v0}, LX/1FJ;->c(LX/1FJ;)V

    move-object v0, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    aget-object v1, p1, v6

    invoke-static {v1}, LX/1FJ;->c(LX/1FJ;)V

    throw v0
.end method

.method public final onPostExecute(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1920531
    check-cast p1, Landroid/net/Uri;

    .line 1920532
    if-nez p1, :cond_0

    .line 1920533
    iget-object v0, p0, LX/Cbw;->a:LX/Cbx;

    iget-object v0, v0, LX/Cbx;->b:LX/CcO;

    iget-object v0, v0, LX/CcO;->a:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->k:LX/03V;

    sget-object v1, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->c:Ljava/lang/String;

    const-string v2, "Could not share file (w/ Fresco + non-jpeg) No temp uri"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1920534
    iget-object v0, p0, LX/Cbw;->a:LX/Cbx;

    iget-object v0, v0, LX/Cbx;->b:LX/CcO;

    invoke-static {v0}, LX/CcO;->n(LX/CcO;)V

    .line 1920535
    :goto_0
    return-void

    .line 1920536
    :cond_0
    iget-object v0, p0, LX/Cbw;->a:LX/Cbx;

    iget-object v0, v0, LX/Cbx;->b:LX/CcO;

    iget-object v1, p0, LX/Cbw;->a:LX/Cbx;

    iget-object v1, v1, LX/Cbx;->a:Landroid/content/Context;

    invoke-static {v0, p1, v1}, LX/CcO;->a$redex0(LX/CcO;Landroid/net/Uri;Landroid/content/Context;)V

    goto :goto_0
.end method
