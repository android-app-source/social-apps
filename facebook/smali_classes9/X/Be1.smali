.class public interface abstract LX/Be1;
.super Ljava/lang/Object;
.source ""


# virtual methods
.method public abstract setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
.end method

.method public abstract setGravity(I)V
.end method

.method public abstract setHintTextColor(I)V
.end method

.method public abstract setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
.end method

.method public abstract setLineSpacing(FF)V
.end method

.method public abstract setPadding(IIII)V
.end method

.method public abstract setTextColor(I)V
.end method

.method public abstract setTextSize(F)V
.end method

.method public abstract setTypeface(Landroid/graphics/Typeface;I)V
.end method
