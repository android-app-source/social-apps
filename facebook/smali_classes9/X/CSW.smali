.class public LX/CSW;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:[LX/CSV;

.field public static final b:[Lcom/facebook/graphql/enums/GraphQLScreenElementType;

.field public static final c:[LX/CSS;


# instance fields
.field private final d:LX/CT6;

.field private final e:LX/CT7;

.field private final f:LX/CSg;

.field private final g:LX/CT1;

.field private final h:LX/CSQ;

.field public i:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/CSV;",
            ">;"
        }
    .end annotation
.end field

.field public j:LX/CUD;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1894660
    invoke-static {}, LX/CSV;->values()[LX/CSV;

    move-result-object v0

    sput-object v0, LX/CSW;->a:[LX/CSV;

    .line 1894661
    invoke-static {}, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->values()[Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    move-result-object v0

    sput-object v0, LX/CSW;->b:[Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    .line 1894662
    invoke-static {}, LX/CSS;->values()[LX/CSS;

    move-result-object v0

    sput-object v0, LX/CSW;->c:[LX/CSS;

    return-void
.end method

.method public constructor <init>(LX/CT6;LX/CT7;LX/CSQ;LX/CSg;LX/CT1;LX/CUD;)V
    .locals 2

    .prologue
    .line 1894725
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 1894726
    iget-object v0, p6, LX/CUD;->a:LX/CUA;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1894727
    iget-object v0, p6, LX/CUD;->a:LX/CUA;

    iget-object v0, v0, LX/CUA;->d:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1894728
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/CSW;->i:Ljava/util/ArrayList;

    .line 1894729
    iput-object p1, p0, LX/CSW;->d:LX/CT6;

    .line 1894730
    iput-object p2, p0, LX/CSW;->e:LX/CT7;

    .line 1894731
    iput-object p4, p0, LX/CSW;->f:LX/CSg;

    .line 1894732
    iput-object p5, p0, LX/CSW;->g:LX/CT1;

    .line 1894733
    iput-object p3, p0, LX/CSW;->h:LX/CSQ;

    .line 1894734
    iput-object p6, p0, LX/CSW;->j:LX/CUD;

    .line 1894735
    iget-object v0, p0, LX/CSW;->j:LX/CUD;

    iget-object v0, v0, LX/CUD;->g:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1894736
    :cond_0
    :goto_0
    return-void

    .line 1894737
    :pswitch_0
    iget-object v0, p0, LX/CSW;->j:LX/CUD;

    check-cast v0, LX/CUG;

    iget-object v0, v0, LX/CUG;->i:LX/CUF;

    if-eqz v0, :cond_1

    .line 1894738
    iget-object v0, p0, LX/CSW;->i:Ljava/util/ArrayList;

    sget-object v1, LX/CSV;->FOOTER:LX/CSV;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1894739
    :cond_1
    iget-object v0, p0, LX/CSW;->j:LX/CUD;

    check-cast v0, LX/CUG;

    iget-object v0, v0, LX/CUG;->j:LX/CUE;

    if-eqz v0, :cond_0

    .line 1894740
    iget-object v0, p0, LX/CSW;->i:Ljava/util/ArrayList;

    sget-object v1, LX/CSV;->DISCLAIMER:LX/CSV;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x5de9ea56
        :pswitch_0
    .end packed-switch
.end method

.method private static a(LX/CSV;LX/CTJ;)I
    .locals 3

    .prologue
    .line 1894715
    const v0, 0xf4240

    invoke-virtual {p0}, LX/CSV;->ordinal()I

    move-result v1

    mul-int/2addr v0, v1

    .line 1894716
    sget-object v1, LX/CSU;->b:[I

    invoke-virtual {p0}, LX/CSV;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1894717
    :cond_0
    :goto_0
    return v0

    .line 1894718
    :pswitch_0
    iget-object v1, p1, LX/CTJ;->m:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->ordinal()I

    move-result v1

    mul-int/lit16 v1, v1, 0x2710

    add-int/2addr v0, v1

    .line 1894719
    iget-object v1, p1, LX/CTJ;->m:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->NAVIGABLE_ITEM:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    if-ne v1, v2, :cond_1

    .line 1894720
    check-cast p1, LX/CTe;

    .line 1894721
    iget-object v1, p1, LX/CTe;->a:LX/CTJ;

    iget-object v1, v1, LX/CTJ;->m:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->ordinal()I

    move-result v1

    mul-int/lit8 v1, v1, 0x64

    add-int/2addr v0, v1

    .line 1894722
    goto :goto_0

    :cond_1
    iget-object v1, p1, LX/CTJ;->m:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->FORM_FIELD:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    if-ne v1, v2, :cond_0

    .line 1894723
    check-cast p1, LX/CTK;

    .line 1894724
    iget-object v1, p1, LX/CTK;->f:LX/CSS;

    invoke-virtual {v1}, LX/CSS;->ordinal()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 6

    .prologue
    const v1, 0xf4240

    .line 1894680
    div-int v0, p2, v1

    .line 1894681
    rem-int v1, p2, v1

    div-int/lit16 v1, v1, 0x2710

    .line 1894682
    rem-int/lit16 v2, p2, 0x2710

    div-int/lit8 v2, v2, 0x64

    .line 1894683
    rem-int/lit16 v3, p2, 0x2710

    div-int/lit8 v3, v3, 0x1

    .line 1894684
    sget-object v4, LX/CSU;->b:[I

    sget-object v5, LX/CSW;->a:[LX/CSV;

    aget-object v5, v5, v0

    invoke-virtual {v5}, LX/CSV;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 1894685
    sget-object v1, LX/CSW;->a:[LX/CSV;

    aget-object v0, v1, v0

    .line 1894686
    new-instance v1, LX/CSm;

    const/4 v4, 0x0

    .line 1894687
    sget-object v2, LX/CSh;->b:[I

    invoke-virtual {v0}, LX/CSV;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_1

    .line 1894688
    const-string v2, "Unsupported layout item"

    invoke-static {v4, v2}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1894689
    const/4 v2, 0x0

    :goto_0
    move-object v2, v2

    .line 1894690
    invoke-direct {v1, v2, v0}, LX/CSm;-><init>(Landroid/view/View;LX/CSV;)V

    move-object v0, v1

    .line 1894691
    :goto_1
    return-object v0

    .line 1894692
    :pswitch_0
    sget-object v0, LX/CSU;->a:[I

    sget-object v4, LX/CSW;->b:[Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    aget-object v4, v4, v1

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->ordinal()I

    move-result v4

    aget v0, v0, v4

    packed-switch v0, :pswitch_data_2

    .line 1894693
    sget-object v0, LX/CSW;->b:[Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    aget-object v0, v0, v1

    .line 1894694
    new-instance v1, LX/CSo;

    invoke-static {p1, v0}, LX/CSi;->d(Landroid/view/ViewGroup;Lcom/facebook/graphql/enums/GraphQLScreenElementType;)Landroid/view/View;

    move-result-object v2

    invoke-direct {v1, v2, v0}, LX/CSo;-><init>(Landroid/view/View;Lcom/facebook/graphql/enums/GraphQLScreenElementType;)V

    move-object v0, v1

    .line 1894695
    goto :goto_1

    .line 1894696
    :pswitch_1
    sget-object v0, LX/CSW;->c:[LX/CSS;

    aget-object v0, v0, v3

    .line 1894697
    new-instance v1, LX/CSl;

    invoke-static {p1, v0}, LX/CSi;->a(Landroid/view/ViewGroup;LX/CSS;)Landroid/view/View;

    move-result-object v2

    invoke-direct {v1, v2, v0}, LX/CSl;-><init>(Landroid/view/View;LX/CSS;)V

    move-object v0, v1

    .line 1894698
    goto :goto_1

    .line 1894699
    :pswitch_2
    sget-object v0, LX/CSW;->b:[Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    aget-object v0, v0, v2

    .line 1894700
    new-instance v1, LX/CSn;

    invoke-static {p1, v0}, LX/CSi;->d(Landroid/view/ViewGroup;Lcom/facebook/graphql/enums/GraphQLScreenElementType;)Landroid/view/View;

    move-result-object v2

    invoke-direct {v1, v2}, LX/CSn;-><init>(Landroid/view/View;)V

    move-object v0, v1

    .line 1894701
    goto :goto_1

    .line 1894702
    :pswitch_3
    sget-object v0, LX/CSW;->b:[Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    aget-object v0, v0, v1

    sget-object v1, LX/CSW;->b:[Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    aget-object v1, v1, v2

    sget-object v2, LX/CSW;->c:[LX/CSS;

    aget-object v2, v2, v3

    .line 1894703
    new-instance v3, LX/CSp;

    const/4 p0, 0x0

    .line 1894704
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->NAVIGABLE_ITEM:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    if-ne v0, v4, :cond_0

    .line 1894705
    :goto_2
    sget-object v4, LX/CSh;->c:[I

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->ordinal()I

    move-result v5

    aget v4, v4, v5

    sparse-switch v4, :sswitch_data_0

    .line 1894706
    :goto_3
    new-instance v4, Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    :goto_4
    move-object v4, v4

    .line 1894707
    invoke-direct {v3, v4}, LX/CSp;-><init>(Landroid/view/View;)V

    move-object v0, v3

    .line 1894708
    goto :goto_1

    .line 1894709
    :pswitch_4
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f03165d

    invoke-virtual {v2, v3, p1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    goto :goto_0

    .line 1894710
    :pswitch_5
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f03165c

    invoke-virtual {v2, v3, p1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    goto/16 :goto_0

    .line 1894711
    :sswitch_0
    invoke-static {p1, v1}, LX/CSi;->d(Landroid/view/ViewGroup;Lcom/facebook/graphql/enums/GraphQLScreenElementType;)Landroid/view/View;

    move-result-object v4

    goto :goto_4

    .line 1894712
    :sswitch_1
    sget-object v4, LX/CSh;->a:[I

    invoke-virtual {v2}, LX/CSS;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_3

    goto :goto_3

    .line 1894713
    :pswitch_6
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    const v5, 0x7f030fdd

    invoke-virtual {v4, v5, p1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    goto :goto_4

    .line 1894714
    :pswitch_7
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    const v5, 0x7f030fdc

    invoke-virtual {v4, v5, p1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    goto :goto_4

    :cond_0
    move-object v1, v0

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_5
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0xe -> :sswitch_1
    .end sparse-switch

    :pswitch_data_3
    .packed-switch 0x7
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public final a(LX/1a1;I)V
    .locals 7

    .prologue
    .line 1894741
    move-object v0, p1

    check-cast v0, LX/CSk;

    .line 1894742
    const/4 v4, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1894743
    if-ltz p2, :cond_0

    move v1, v2

    :goto_0
    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 1894744
    iget-object v1, p0, LX/CSW;->i:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    iget-object v5, p0, LX/CSW;->j:LX/CUD;

    iget-object v5, v5, LX/CUD;->c:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    add-int/2addr v1, v5

    if-ge p2, v1, :cond_1

    :goto_1
    invoke-static {v2}, LX/0PB;->checkState(Z)V

    .line 1894745
    iget-object v1, p0, LX/CSW;->j:LX/CUD;

    iget-object v1, v1, LX/CUD;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lt p2, v1, :cond_2

    .line 1894746
    iget-object v1, p0, LX/CSW;->j:LX/CUD;

    iget-object v1, v1, LX/CUD;->g:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 1894747
    const-string v1, "Looking for an item that doesn\'t exist"

    invoke-static {v3, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    move-object v1, v4

    .line 1894748
    :goto_2
    move-object v1, v1

    .line 1894749
    iget-object v2, p0, LX/CSW;->h:LX/CSQ;

    iget-object v3, p0, LX/CSW;->e:LX/CT7;

    iget-object v4, p0, LX/CSW;->d:LX/CT6;

    iget-object v5, p0, LX/CSW;->f:LX/CSg;

    iget-object v6, p0, LX/CSW;->g:LX/CT1;

    invoke-interface/range {v0 .. v6}, LX/CSk;->a(Ljava/lang/Object;LX/CSQ;LX/CT7;LX/CT6;LX/CSg;LX/CT1;)V

    .line 1894750
    return-void

    :cond_0
    move v1, v3

    .line 1894751
    goto :goto_0

    :cond_1
    move v2, v3

    .line 1894752
    goto :goto_1

    .line 1894753
    :pswitch_0
    sget-object v2, LX/CSU;->b:[I

    iget-object v1, p0, LX/CSW;->i:Ljava/util/ArrayList;

    iget-object v5, p0, LX/CSW;->j:LX/CUD;

    iget-object v5, v5, LX/CUD;->c:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    sub-int v5, p2, v5

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/CSV;

    invoke-virtual {v1}, LX/CSV;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_1

    .line 1894754
    invoke-static {v3}, LX/0PB;->checkState(Z)V

    move-object v1, v4

    .line 1894755
    goto :goto_2

    .line 1894756
    :pswitch_1
    iget-object v1, p0, LX/CSW;->j:LX/CUD;

    check-cast v1, LX/CUG;

    iget-object v1, v1, LX/CUG;->i:LX/CUF;

    goto :goto_2

    .line 1894757
    :pswitch_2
    iget-object v1, p0, LX/CSW;->j:LX/CUD;

    check-cast v1, LX/CUG;

    iget-object v1, v1, LX/CUG;->j:LX/CUE;

    goto :goto_2

    .line 1894758
    :cond_2
    iget-object v1, p0, LX/CSW;->j:LX/CUD;

    iget-object v1, v1, LX/CUD;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x5de9ea56
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x3
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final d(LX/1a1;)V
    .locals 0

    .prologue
    .line 1894678
    check-cast p1, LX/CSk;

    invoke-interface {p1}, LX/CSk;->w()V

    .line 1894679
    return-void
.end method

.method public final getItemViewType(I)I
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1894666
    if-ltz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1894667
    iget-object v0, p0, LX/CSW;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget-object v3, p0, LX/CSW;->j:LX/CUD;

    iget-object v3, v3, LX/CUD;->c:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/2addr v0, v3

    if-ge p1, v0, :cond_1

    :goto_1
    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 1894668
    iget-object v0, p0, LX/CSW;->j:LX/CUD;

    iget-object v0, v0, LX/CUD;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p1, v0, :cond_2

    .line 1894669
    iget-object v0, p0, LX/CSW;->i:Ljava/util/ArrayList;

    iget-object v1, p0, LX/CSW;->j:LX/CUD;

    iget-object v1, v1, LX/CUD;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    sub-int v1, p1, v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CSV;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/CSW;->a(LX/CSV;LX/CTJ;)I

    move-result v0

    .line 1894670
    :goto_2
    return v0

    :cond_0
    move v0, v2

    .line 1894671
    goto :goto_0

    :cond_1
    move v1, v2

    .line 1894672
    goto :goto_1

    .line 1894673
    :cond_2
    iget-object v0, p0, LX/CSW;->j:LX/CUD;

    iget-object v0, v0, LX/CUD;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CTJ;

    .line 1894674
    iget-boolean v1, v0, LX/CTJ;->a:Z

    move v0, v1

    .line 1894675
    if-eqz v0, :cond_3

    .line 1894676
    sget-object v1, LX/CSV;->SHIMMERING_VIEW:LX/CSV;

    iget-object v0, p0, LX/CSW;->j:LX/CUD;

    iget-object v0, v0, LX/CUD;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CTJ;

    invoke-static {v1, v0}, LX/CSW;->a(LX/CSV;LX/CTJ;)I

    move-result v0

    goto :goto_2

    .line 1894677
    :cond_3
    sget-object v1, LX/CSV;->COMPONENTS:LX/CSV;

    iget-object v0, p0, LX/CSW;->j:LX/CUD;

    iget-object v0, v0, LX/CUD;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CTJ;

    invoke-static {v1, v0}, LX/CSW;->a(LX/CSV;LX/CTJ;)I

    move-result v0

    goto :goto_2
.end method

.method public final ij_()I
    .locals 2

    .prologue
    .line 1894663
    iget-object v0, p0, LX/CSW;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 1894664
    iget-object v1, p0, LX/CSW;->j:LX/CUD;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/CSW;->j:LX/CUD;

    iget-object v1, v1, LX/CUD;->c:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/CSW;->j:LX/CUD;

    iget-object v1, v1, LX/CUD;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    :goto_0
    move v1, v1

    .line 1894665
    add-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
