.class public final LX/CVI;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 10

    .prologue
    const-wide/16 v4, 0x0

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 1902897
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 1902898
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1902899
    :goto_0
    return v1

    .line 1902900
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_2

    .line 1902901
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 1902902
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1902903
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_0

    if-eqz v7, :cond_0

    .line 1902904
    const-string v8, "time"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 1902905
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v2

    move v0, v6

    goto :goto_1

    .line 1902906
    :cond_1
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1902907
    :cond_2
    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 1902908
    if-eqz v0, :cond_3

    move-object v0, p1

    .line 1902909
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1902910
    :cond_3
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    move-wide v2, v4

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 1902911
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1902912
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 1902913
    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    .line 1902914
    const-string v2, "time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1902915
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 1902916
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1902917
    return-void
.end method
