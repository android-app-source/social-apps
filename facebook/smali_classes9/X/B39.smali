.class public final LX/B39;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Ljava/util/List",
        "<",
        "Ljava/lang/Object;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;)V
    .locals 0

    .prologue
    .line 1739572
    iput-object p1, p0, LX/B39;->a:Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method

.method private a(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1739573
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1739574
    const/4 v1, 0x1

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    .line 1739575
    iget-object v2, p0, LX/B39;->a:Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;

    .line 1739576
    invoke-static {v2, v0, v1}, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->a$redex0(Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;Lcom/facebook/graphql/executor/GraphQLResult;Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;)V

    .line 1739577
    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1739578
    iget-object v0, p0, LX/B39;->a:Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;

    iget-object v0, v0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->F:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->c()V

    .line 1739579
    const-string v0, "ProfilePictureOverlayCameraActivity"

    const-string v1, "self profile pic fetch failed"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1739580
    return-void
.end method

.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1739581
    check-cast p1, Ljava/util/List;

    invoke-direct {p0, p1}, LX/B39;->a(Ljava/util/List;)V

    return-void
.end method
