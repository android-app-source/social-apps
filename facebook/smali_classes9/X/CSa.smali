.class public final enum LX/CSa;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/CSa;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/CSa;

.field public static final enum CREDIT_CARD:LX/CSa;

.field public static final enum JSON_ARRAY:LX/CSa;

.field public static final enum JSON_OBJECT:LX/CSa;

.field public static final enum STRING:LX/CSa;

.field public static final enum STRING_ARRAY:LX/CSa;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1894829
    new-instance v0, LX/CSa;

    const-string v1, "STRING"

    invoke-direct {v0, v1, v2}, LX/CSa;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CSa;->STRING:LX/CSa;

    .line 1894830
    new-instance v0, LX/CSa;

    const-string v1, "STRING_ARRAY"

    invoke-direct {v0, v1, v3}, LX/CSa;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CSa;->STRING_ARRAY:LX/CSa;

    .line 1894831
    new-instance v0, LX/CSa;

    const-string v1, "JSON_ARRAY"

    invoke-direct {v0, v1, v4}, LX/CSa;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CSa;->JSON_ARRAY:LX/CSa;

    .line 1894832
    new-instance v0, LX/CSa;

    const-string v1, "JSON_OBJECT"

    invoke-direct {v0, v1, v5}, LX/CSa;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CSa;->JSON_OBJECT:LX/CSa;

    .line 1894833
    new-instance v0, LX/CSa;

    const-string v1, "CREDIT_CARD"

    invoke-direct {v0, v1, v6}, LX/CSa;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CSa;->CREDIT_CARD:LX/CSa;

    .line 1894834
    const/4 v0, 0x5

    new-array v0, v0, [LX/CSa;

    sget-object v1, LX/CSa;->STRING:LX/CSa;

    aput-object v1, v0, v2

    sget-object v1, LX/CSa;->STRING_ARRAY:LX/CSa;

    aput-object v1, v0, v3

    sget-object v1, LX/CSa;->JSON_ARRAY:LX/CSa;

    aput-object v1, v0, v4

    sget-object v1, LX/CSa;->JSON_OBJECT:LX/CSa;

    aput-object v1, v0, v5

    sget-object v1, LX/CSa;->CREDIT_CARD:LX/CSa;

    aput-object v1, v0, v6

    sput-object v0, LX/CSa;->$VALUES:[LX/CSa;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1894835
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/CSa;
    .locals 1

    .prologue
    .line 1894836
    const-class v0, LX/CSa;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/CSa;

    return-object v0
.end method

.method public static values()[LX/CSa;
    .locals 1

    .prologue
    .line 1894837
    sget-object v0, LX/CSa;->$VALUES:[LX/CSa;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/CSa;

    return-object v0
.end method
