.class public final LX/BSh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field public final synthetic a:LX/BSj;

.field public b:I

.field public c:I


# direct methods
.method public constructor <init>(LX/BSj;)V
    .locals 0

    .prologue
    .line 1785690
    iput-object p1, p0, LX/BSh;->a:LX/BSj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 10

    .prologue
    const/4 v0, 0x1

    .line 1785691
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 1785692
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 1785693
    :pswitch_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, LX/BSh;->b:I

    .line 1785694
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, LX/BSh;->c:I

    .line 1785695
    goto :goto_0

    .line 1785696
    :pswitch_2
    iget v1, p0, LX/BSh;->b:I

    int-to-float v1, v1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    sub-float/2addr v1, v2

    float-to-int v1, v1

    .line 1785697
    iget v2, p0, LX/BSh;->c:I

    int-to-float v2, v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    sub-float/2addr v2, v3

    float-to-int v2, v2

    .line 1785698
    iget-object v3, p0, LX/BSh;->a:LX/BSj;

    .line 1785699
    iget-object v4, v3, LX/BSj;->b:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v4}, Lcom/facebook/video/player/RichVideoPlayer;->getAdjustedVideoSize()Landroid/graphics/RectF;

    move-result-object v4

    .line 1785700
    iget-object v5, v3, LX/BSj;->b:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v5}, Lcom/facebook/video/player/RichVideoPlayer;->getCropRect()Landroid/graphics/RectF;

    move-result-object v5

    .line 1785701
    if-eqz v5, :cond_0

    if-nez v4, :cond_1

    .line 1785702
    :cond_0
    :goto_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, LX/BSh;->b:I

    .line 1785703
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, LX/BSh;->c:I

    .line 1785704
    goto :goto_0

    .line 1785705
    :cond_1
    int-to-float v6, v1

    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v7

    div-float/2addr v6, v7

    .line 1785706
    int-to-float v7, v2

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v4

    div-float v4, v7, v4

    .line 1785707
    iget-object v7, v3, LX/BSj;->d:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getVisibility()I

    move-result v7

    if-nez v7, :cond_2

    .line 1785708
    iget-object v7, v3, LX/BSj;->d:Landroid/view/View;

    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Landroid/view/View;->setVisibility(I)V

    .line 1785709
    :cond_2
    iget v7, v5, Landroid/graphics/RectF;->left:F

    add-float/2addr v7, v6

    .line 1785710
    iget v8, v5, Landroid/graphics/RectF;->top:F

    add-float/2addr v8, v4

    .line 1785711
    iget v9, v5, Landroid/graphics/RectF;->right:F

    add-float/2addr v9, v6

    .line 1785712
    iget p1, v5, Landroid/graphics/RectF;->bottom:F

    add-float/2addr p1, v4

    .line 1785713
    invoke-static {v7, v9}, LX/BSj;->a(FF)Landroid/util/Pair;

    move-result-object v9

    .line 1785714
    invoke-static {v8, p1}, LX/BSj;->a(FF)Landroid/util/Pair;

    move-result-object v8

    .line 1785715
    new-instance p1, Landroid/graphics/RectF;

    iget-object v7, v9, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v7, Ljava/lang/Float;

    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget-object v7, v8, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v7, Ljava/lang/Float;

    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F

    move-result v2

    iget-object v7, v9, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v7, Ljava/lang/Float;

    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F

    move-result v9

    iget-object v7, v8, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v7, Ljava/lang/Float;

    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F

    move-result v7

    invoke-direct {p1, v1, v2, v9, v7}, Landroid/graphics/RectF;-><init>(FFFF)V

    move-object v4, p1

    .line 1785716
    iget-object v5, v3, LX/BSj;->b:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v5, v4}, Lcom/facebook/video/player/RichVideoPlayer;->setCropRect(Landroid/graphics/RectF;)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
