.class public final enum LX/BPx;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/BPx;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/BPx;

.field public static final enum FINAL_DATA:LX/BPx;

.field public static final enum PRELIM_DATA:LX/BPx;

.field public static final enum UNINITIALIZED:LX/BPx;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1781108
    new-instance v0, LX/BPx;

    const-string v1, "UNINITIALIZED"

    invoke-direct {v0, v1, v2}, LX/BPx;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BPx;->UNINITIALIZED:LX/BPx;

    .line 1781109
    new-instance v0, LX/BPx;

    const-string v1, "PRELIM_DATA"

    invoke-direct {v0, v1, v3}, LX/BPx;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BPx;->PRELIM_DATA:LX/BPx;

    .line 1781110
    new-instance v0, LX/BPx;

    const-string v1, "FINAL_DATA"

    invoke-direct {v0, v1, v4}, LX/BPx;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BPx;->FINAL_DATA:LX/BPx;

    .line 1781111
    const/4 v0, 0x3

    new-array v0, v0, [LX/BPx;

    sget-object v1, LX/BPx;->UNINITIALIZED:LX/BPx;

    aput-object v1, v0, v2

    sget-object v1, LX/BPx;->PRELIM_DATA:LX/BPx;

    aput-object v1, v0, v3

    sget-object v1, LX/BPx;->FINAL_DATA:LX/BPx;

    aput-object v1, v0, v4

    sput-object v0, LX/BPx;->$VALUES:[LX/BPx;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1781107
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/BPx;
    .locals 1

    .prologue
    .line 1781113
    const-class v0, LX/BPx;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/BPx;

    return-object v0
.end method

.method public static values()[LX/BPx;
    .locals 1

    .prologue
    .line 1781112
    sget-object v0, LX/BPx;->$VALUES:[LX/BPx;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/BPx;

    return-object v0
.end method
