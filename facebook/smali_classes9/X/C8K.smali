.class public final LX/C8K;
.super LX/4o9;
.source ""


# instance fields
.field public final synthetic a:LX/C8O;

.field public final synthetic b:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic c:LX/C8M;

.field public final synthetic d:Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;LX/C8O;Lcom/facebook/feed/rows/core/props/FeedProps;LX/C8M;)V
    .locals 0

    .prologue
    .line 1852618
    iput-object p1, p0, LX/C8K;->d:Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;

    iput-object p2, p0, LX/C8K;->a:LX/C8O;

    iput-object p3, p0, LX/C8K;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iput-object p4, p0, LX/C8K;->c:LX/C8M;

    invoke-direct {p0}, LX/4o9;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 7

    .prologue
    .line 1852619
    iget-object v0, p0, LX/C8K;->a:LX/C8O;

    iget-object v1, p0, LX/C8K;->a:LX/C8O;

    invoke-virtual {v1}, LX/C8O;->getMeasuredHeight()I

    move-result v1

    invoke-virtual {v0, v1}, LX/C8O;->setHeight(I)V

    .line 1852620
    iget-object v0, p0, LX/C8K;->a:LX/C8O;

    invoke-virtual {v0}, LX/C8O;->a()V

    .line 1852621
    iget-object v0, p0, LX/C8K;->d:Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;

    iget-object v1, p0, LX/C8K;->a:LX/C8O;

    iget-object v2, p0, LX/C8K;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p0, LX/C8K;->c:LX/C8M;

    .line 1852622
    iget-object v4, v0, Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;->e:LX/1du;

    new-instance v5, LX/C8L;

    invoke-direct {v5, v0, v2, v3, v1}, LX/C8L;-><init>(Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;LX/C8M;LX/C8O;)V

    .line 1852623
    iget-object v6, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v6, v6

    .line 1852624
    check-cast v6, Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;

    .line 1852625
    invoke-interface {v6}, Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;->o()Ljava/lang/String;

    move-result-object p0

    .line 1852626
    iget-object v6, v4, LX/1du;->f:Ljava/util/Map;

    invoke-interface {v6, p0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 1852627
    :goto_0
    return-void

    .line 1852628
    :cond_0
    iget-object v6, v4, LX/1du;->f:Ljava/util/Map;

    invoke-interface {v6, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Stack;

    invoke-virtual {v6}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$FetchCurationFlowModel$FeedCurationFlowStepModel;

    .line 1852629
    invoke-virtual {v6}, Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$FetchCurationFlowModel$FeedCurationFlowStepModel;->j()LX/1vs;

    move-result-object v6

    iget-object p1, v6, LX/1vs;->a:LX/15i;

    iget v6, v6, LX/1vs;->b:I

    .line 1852630
    const/4 v0, 0x0

    const-class v1, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    invoke-virtual {p1, v6, v0, v1, v3}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v6

    check-cast v6, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 1852631
    invoke-static {v4, p0}, LX/1du;->c(LX/1du;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 1852632
    invoke-static {v4, v2, v6, p1}, LX/1du;->a$redex0(LX/1du;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object p1

    .line 1852633
    invoke-static {v4, p0, v6}, LX/1du;->b(LX/1du;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;)V

    .line 1852634
    new-instance v6, LX/Aj9;

    invoke-direct {v6, v4, p0, v5}, LX/Aj9;-><init>(LX/1du;Ljava/lang/String;LX/0TF;)V

    iget-object p0, v4, LX/1du;->c:LX/0TD;

    invoke-static {p1, v6, p0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method
