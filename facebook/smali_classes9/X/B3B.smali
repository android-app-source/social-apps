.class public final LX/B3B;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/google/common/util/concurrent/SettableFuture;

.field public final synthetic b:Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;


# direct methods
.method public constructor <init>(Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;Lcom/google/common/util/concurrent/SettableFuture;)V
    .locals 0

    .prologue
    .line 1739777
    iput-object p1, p0, LX/B3B;->b:Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;

    iput-object p2, p0, LX/B3B;->a:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1739778
    iget-object v0, p0, LX/B3B;->b:Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;

    .line 1739779
    iput-object v2, v0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->v:LX/1Mv;

    .line 1739780
    iget-object v0, p0, LX/B3B;->b:Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;

    iget-object v0, v0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->g:LX/B3O;

    iget-object v1, p0, LX/B3B;->b:Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;

    iget-object v1, v1, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->g:LX/B3O;

    invoke-virtual {v1}, LX/B3O;->a()Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;->j()LX/B3N;

    move-result-object v1

    .line 1739781
    iput-object p1, v1, LX/B3N;->a:Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    .line 1739782
    move-object v1, v1

    .line 1739783
    invoke-virtual {v1}, LX/B3N;->a()Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/B3O;->a(Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;)V

    .line 1739784
    iget-object v0, p0, LX/B3B;->a:Lcom/google/common/util/concurrent/SettableFuture;

    const v1, -0x3c0dc334

    invoke-static {v0, v2, v1}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    .line 1739785
    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1739786
    iget-object v0, p0, LX/B3B;->b:Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;

    const/4 v1, 0x0

    .line 1739787
    iput-object v1, v0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->v:LX/1Mv;

    .line 1739788
    instance-of v0, p1, Ljava/io/IOException;

    if-nez v0, :cond_0

    .line 1739789
    iget-object v0, p0, LX/B3B;->b:Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;

    iget-object v0, v0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "heisman_fetch_image_overlay_failed"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1739790
    :cond_0
    iget-object v0, p0, LX/B3B;->a:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v0, p1}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    .line 1739791
    return-void
.end method

.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1739792
    check-cast p1, Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    invoke-direct {p0, p1}, LX/B3B;->a(Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;)V

    return-void
.end method
