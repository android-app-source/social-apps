.class public final LX/C8I;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$FetchCurationFlowModel$FeedCurationFlowStepModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/C8O;

.field public final synthetic b:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic c:LX/C8M;

.field public final synthetic d:Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;LX/C8O;Lcom/facebook/feed/rows/core/props/FeedProps;LX/C8M;)V
    .locals 0

    .prologue
    .line 1852596
    iput-object p1, p0, LX/C8I;->d:Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;

    iput-object p2, p0, LX/C8I;->a:LX/C8O;

    iput-object p3, p0, LX/C8I;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iput-object p4, p0, LX/C8I;->c:LX/C8M;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 1852597
    iget-object v0, p0, LX/C8I;->d:Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;

    iget-object v1, p0, LX/C8I;->a:LX/C8O;

    invoke-virtual {v1}, LX/C8O;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;->a$redex0(Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;Landroid/content/Context;)V

    .line 1852598
    iget-object v0, p0, LX/C8I;->d:Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;

    iget-object v1, p0, LX/C8I;->a:LX/C8O;

    iget-object v2, p0, LX/C8I;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p0, LX/C8I;->c:LX/C8M;

    .line 1852599
    invoke-virtual {v1}, LX/C8O;->e()V

    .line 1852600
    invoke-static {v0, v1, v3, v2}, Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;->a$redex0(Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;LX/C8O;LX/C8M;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1852601
    const/4 p0, 0x0

    .line 1852602
    iget-object v0, v1, LX/C8O;->d:Landroid/view/ViewGroup;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1852603
    iget-object v0, v1, LX/C8O;->g:LX/C8D;

    iget-object v2, v1, LX/C8O;->d:Landroid/view/ViewGroup;

    .line 1852604
    invoke-static {p0}, LX/C8D;->a(Landroid/view/animation/Animation$AnimationListener;)Landroid/view/animation/Animation;

    move-result-object v1

    iput-object v1, v0, LX/C8D;->a:Landroid/view/animation/Animation;

    .line 1852605
    iget-object v1, v0, LX/C8D;->a:Landroid/view/animation/Animation;

    invoke-virtual {v2, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1852606
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 10

    .prologue
    .line 1852607
    iget-object v0, p0, LX/C8I;->d:Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;

    iget-object v1, p0, LX/C8I;->a:LX/C8O;

    iget-object v2, p0, LX/C8I;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p0, LX/C8I;->c:LX/C8M;

    .line 1852608
    invoke-virtual {v1}, LX/C8O;->getHeight()I

    move-result v9

    .line 1852609
    new-instance v4, LX/C8J;

    move-object v5, v0

    move-object v6, v1

    move-object v7, v3

    move-object v8, v2

    invoke-direct/range {v4 .. v9}, LX/C8J;-><init>(Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;LX/C8O;LX/C8M;Lcom/facebook/feed/rows/core/props/FeedProps;I)V

    invoke-virtual {v1, v4}, LX/C8O;->a(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1852610
    return-void
.end method
