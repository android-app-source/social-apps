.class public LX/BHr;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field private final b:Landroid/content/ContentResolver;

.field private final c:LX/0Zb;

.field private final d:LX/0kb;

.field private final e:LX/0Uh;

.field private final f:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final g:LX/0SG;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1769791
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "media_type"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "mime_type"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "date_modified"

    aput-object v2, v0, v1

    sput-object v0, LX/BHr;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/ContentResolver;LX/0Zb;LX/0kb;LX/0Uh;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1769834
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1769835
    iput-object p1, p0, LX/BHr;->b:Landroid/content/ContentResolver;

    .line 1769836
    iput-object p2, p0, LX/BHr;->c:LX/0Zb;

    .line 1769837
    iput-object p3, p0, LX/BHr;->d:LX/0kb;

    .line 1769838
    iput-object p4, p0, LX/BHr;->e:LX/0Uh;

    .line 1769839
    iput-object p5, p0, LX/BHr;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1769840
    iput-object p6, p0, LX/BHr;->g:LX/0SG;

    .line 1769841
    return-void
.end method

.method private static a(LX/BHr;J)Landroid/database/Cursor;
    .locals 13

    .prologue
    const/4 v4, 0x2

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 1769821
    const-string v0, "%s>=? AND %s<=?"

    new-array v1, v4, [Ljava/lang/Object;

    const-string v2, "date_modified"

    aput-object v2, v1, v10

    const-string v2, "date_modified"

    aput-object v2, v1, v11

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1769822
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LX/4gI;->ALL:LX/4gI;

    invoke-static {v1}, LX/4gB;->a(LX/4gI;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1769823
    const-wide/32 v0, 0xa4cb800

    sub-long v0, p1, v0

    invoke-static {v0, v1}, LX/1lQ;->m(J)J

    move-result-wide v6

    .line 1769824
    invoke-static {p1, p2}, LX/1lQ;->m(J)J

    move-result-wide v8

    .line 1769825
    iget-object v0, p0, LX/BHr;->b:Landroid/content/ContentResolver;

    const-string v1, "external"

    invoke-static {v1}, Landroid/provider/MediaStore$Files;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, LX/BHr;->a:[Ljava/lang/String;

    new-array v4, v4, [Ljava/lang/String;

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v10

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v11

    const-string v5, "date_modified DESC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method private static a(LX/BHr;IIII)V
    .locals 3

    .prologue
    .line 1769826
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "picker_new_media_count"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1769827
    const-string v1, "reachability_status"

    iget-object v2, p0, LX/BHr;->d:LX/0kb;

    invoke-virtual {v2}, LX/0kb;->p()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1769828
    const-string v1, "photo_count_48_h"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1769829
    const-string v1, "photo_count_48_h_since_last_log"

    invoke-virtual {v0, v1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1769830
    const-string v1, "video_count_48_h"

    invoke-virtual {v0, v1, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1769831
    const-string v1, "video_count_48_h_since_last_log"

    invoke-virtual {v0, v1, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1769832
    iget-object v1, p0, LX/BHr;->c:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1769833
    return-void
.end method

.method public static b(LX/0QB;)LX/BHr;
    .locals 7

    .prologue
    .line 1769819
    new-instance v0, LX/BHr;

    invoke-static {p0}, LX/0cd;->b(LX/0QB;)Landroid/content/ContentResolver;

    move-result-object v1

    check-cast v1, Landroid/content/ContentResolver;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v2

    check-cast v2, LX/0Zb;

    invoke-static {p0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v3

    check-cast v3, LX/0kb;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v5

    check-cast v5, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v6

    check-cast v6, LX/0SG;

    invoke-direct/range {v0 .. v6}, LX/BHr;-><init>(Landroid/content/ContentResolver;LX/0Zb;LX/0kb;LX/0Uh;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0SG;)V

    .line 1769820
    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 18

    .prologue
    .line 1769792
    move-object/from16 v0, p0

    iget-object v2, v0, LX/BHr;->e:LX/0Uh;

    const/16 v3, 0x36b

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, LX/0Uh;->a(IZ)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1769793
    :cond_0
    :goto_0
    return-void

    .line 1769794
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, LX/BHr;->g:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v6

    .line 1769795
    move-object/from16 v0, p0

    iget-object v2, v0, LX/BHr;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/1Ip;->j:LX/0Tn;

    const-wide/16 v4, -0x1

    invoke-interface {v2, v3, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v8

    .line 1769796
    sub-long v2, v6, v8

    const-wide/32 v4, 0x927c0

    cmp-long v2, v2, v4

    if-ltz v2, :cond_0

    .line 1769797
    move-object/from16 v0, p0

    invoke-static {v0, v6, v7}, LX/BHr;->a(LX/BHr;J)Landroid/database/Cursor;

    move-result-object v10

    .line 1769798
    const/4 v5, 0x0

    .line 1769799
    const/4 v4, 0x0

    .line 1769800
    const/4 v3, 0x0

    .line 1769801
    const/4 v2, 0x0

    .line 1769802
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v11

    if-eqz v11, :cond_5

    .line 1769803
    const-string v11, "date_modified"

    invoke-interface {v10, v11}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v11

    .line 1769804
    const-string v12, "mime_type"

    invoke-interface {v10, v12}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v12

    .line 1769805
    :cond_2
    invoke-interface {v10, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v14

    const-wide/16 v16, 0x3e8

    mul-long v14, v14, v16

    .line 1769806
    invoke-interface {v10, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Lcom/facebook/ipc/media/data/MimeType;->a(Ljava/lang/String;)Lcom/facebook/ipc/media/data/MimeType;

    move-result-object v13

    .line 1769807
    invoke-virtual {v13}, Lcom/facebook/ipc/media/data/MimeType;->a()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v16 .. v16}, LX/46I;->a(Ljava/lang/String;)Z

    move-result v16

    if-eqz v16, :cond_6

    .line 1769808
    cmp-long v13, v14, v8

    if-lez v13, :cond_3

    .line 1769809
    add-int/lit8 v4, v4, 0x1

    .line 1769810
    :cond_3
    add-int/lit8 v5, v5, 0x1

    .line 1769811
    :cond_4
    :goto_1
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v13

    if-nez v13, :cond_2

    .line 1769812
    :cond_5
    move-object/from16 v0, p0

    invoke-static {v0, v5, v4, v3, v2}, LX/BHr;->a(LX/BHr;IIII)V

    .line 1769813
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 1769814
    move-object/from16 v0, p0

    iget-object v2, v0, LX/BHr;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    sget-object v3, LX/1Ip;->j:LX/0Tn;

    invoke-interface {v2, v3, v6, v7}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v2

    invoke-interface {v2}, LX/0hN;->commit()V

    goto :goto_0

    .line 1769815
    :cond_6
    invoke-virtual {v13}, Lcom/facebook/ipc/media/data/MimeType;->a()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, LX/46I;->b(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_4

    .line 1769816
    cmp-long v13, v14, v8

    if-lez v13, :cond_7

    .line 1769817
    add-int/lit8 v2, v2, 0x1

    .line 1769818
    :cond_7
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method
