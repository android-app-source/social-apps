.class public abstract LX/CGt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Ya;
.implements LX/CGs;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/0Ya;",
        "LX/CGs",
        "<",
        "LX/0zO",
        "<TT;>;>;"
    }
.end annotation


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0ad;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8bL;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Ljava/lang/String;

.field public d:I

.field public e:I

.field public f:I

.field public g:I

.field public h:LX/0wF;

.field public i:LX/0wC;

.field public j:J

.field public k:LX/0zS;

.field private l:I

.field public m:I

.field public n:I

.field public o:I

.field public p:Z

.field public q:Lcom/facebook/http/interfaces/RequestPriority;

.field private r:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/16 v1, 0x64

    .line 1865902
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1865903
    iput v1, p0, LX/CGt;->d:I

    .line 1865904
    const/16 v0, 0x3e8

    iput v0, p0, LX/CGt;->e:I

    .line 1865905
    iput v1, p0, LX/CGt;->f:I

    .line 1865906
    iput v1, p0, LX/CGt;->g:I

    .line 1865907
    sget-object v0, LX/Cj6;->a:LX/0wF;

    iput-object v0, p0, LX/CGt;->h:LX/0wF;

    .line 1865908
    sget-object v0, LX/Cj6;->b:LX/0wC;

    iput-object v0, p0, LX/CGt;->i:LX/0wC;

    .line 1865909
    const-wide/32 v0, 0x240c8400

    iput-wide v0, p0, LX/CGt;->j:J

    .line 1865910
    sget-object v0, LX/Cj6;->c:LX/0zS;

    iput-object v0, p0, LX/CGt;->k:LX/0zS;

    .line 1865911
    const/4 v0, 0x3

    iput v0, p0, LX/CGt;->l:I

    .line 1865912
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/CGt;->p:Z

    .line 1865913
    sget-object v0, LX/Cj6;->d:Lcom/facebook/http/interfaces/RequestPriority;

    iput-object v0, p0, LX/CGt;->q:Lcom/facebook/http/interfaces/RequestPriority;

    .line 1865914
    const-class v0, LX/CGt;

    invoke-static {v0, p0, p1}, LX/CGt;->a(Ljava/lang/Class;LX/0Ya;Landroid/content/Context;)V

    .line 1865915
    iput-object p2, p0, LX/CGt;->c:Ljava/lang/String;

    .line 1865916
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 1865917
    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    div-int/lit8 v1, v1, 0x2

    iput v1, p0, LX/CGt;->m:I

    .line 1865918
    iget v1, p0, LX/CGt;->m:I

    int-to-float v1, v1

    const v2, 0x3ff47ae1    # 1.91f

    div-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, p0, LX/CGt;->n:I

    .line 1865919
    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iput v0, p0, LX/CGt;->o:I

    .line 1865920
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/CGt;->r:Ljava/lang/String;

    .line 1865921
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/0Ya;Landroid/content/Context;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0Ya;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    invoke-static {p2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p1, LX/CGt;

    const/16 v1, 0x1032

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 p0, 0x3231

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v0

    iput-object v1, p1, LX/CGt;->a:LX/0Ot;

    iput-object v0, p1, LX/CGt;->b:LX/0Ot;

    return-void
.end method


# virtual methods
.method public final n()I
    .locals 3

    .prologue
    .line 1865922
    iget-object v0, p0, LX/CGt;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8bL;

    iget v1, p0, LX/CGt;->l:I

    .line 1865923
    iget-object v2, v0, LX/8bL;->b:LX/0ad;

    sget p0, LX/2yD;->O:I

    invoke-interface {v2, p0, v1}, LX/0ad;->a(II)I

    move-result v2

    move v0, v2

    .line 1865924
    return v0
.end method
