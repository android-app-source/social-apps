.class public LX/Bft;
.super LX/Bfd;
.source ""


# static fields
.field public static final b:Ljava/lang/String;


# instance fields
.field public a:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

.field public d:Lcom/facebook/resources/ui/FbCheckBox;

.field public e:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

.field public f:Lcom/facebook/resources/ui/FbEditText;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1806691
    const-class v0, LX/Bft;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Bft;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1806680
    invoke-direct {p0, p1}, LX/Bfd;-><init>(Landroid/content/Context;)V

    .line 1806681
    const/4 p1, 0x1

    .line 1806682
    const-class v0, LX/Bft;

    invoke-static {v0, p0}, LX/Bft;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1806683
    invoke-virtual {p0, p1}, LX/Bft;->setFocusableInTouchMode(Z)V

    .line 1806684
    const v0, 0x7f030f78

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1806685
    invoke-virtual {p0, p1}, LX/Bft;->setOrientation(I)V

    .line 1806686
    const v0, 0x7f0d2552

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbCheckBox;

    iput-object v0, p0, LX/Bft;->d:Lcom/facebook/resources/ui/FbCheckBox;

    .line 1806687
    const v0, 0x7f0d2542

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    iput-object v0, p0, LX/Bft;->e:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    .line 1806688
    const v0, 0x7f0d2551

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbEditText;

    iput-object v0, p0, LX/Bft;->f:Lcom/facebook/resources/ui/FbEditText;

    .line 1806689
    const v0, 0x7f0d2540

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    iput-object v0, p0, LX/Bft;->c:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    .line 1806690
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/Bft;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object p0

    check-cast p0, LX/03V;

    iput-object p0, p1, LX/Bft;->a:LX/03V;

    return-void
.end method

.method public static a(LX/175;)Z
    .locals 1
    .param p0    # LX/175;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1806679
    if-eqz p0, :cond_0

    invoke-interface {p0}, LX/175;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/Bft;Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionAnswerView;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 1806672
    iget-object v0, p0, LX/Bft;->f:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbEditText;->length()I

    move-result v0

    if-lez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionAnswerView;->setEnabled(Z)V

    .line 1806673
    iget-object v0, p0, LX/Bft;->f:Lcom/facebook/resources/ui/FbEditText;

    iget-object v2, p0, LX/Bft;->d:Lcom/facebook/resources/ui/FbCheckBox;

    invoke-virtual {v2}, Lcom/facebook/resources/ui/FbCheckBox;->isChecked()Z

    move-result v2

    if-nez v2, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setEnabled(Z)V

    .line 1806674
    iget-object v1, p0, LX/Bft;->f:Lcom/facebook/resources/ui/FbEditText;

    iget-object v0, p0, LX/Bft;->d:Lcom/facebook/resources/ui/FbCheckBox;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbCheckBox;->isChecked()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, LX/Bft;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0a010d

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    :goto_2
    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbEditText;->setTextColor(I)V

    .line 1806675
    return-void

    .line 1806676
    :cond_0
    iget-object v0, p0, LX/Bft;->d:Lcom/facebook/resources/ui/FbCheckBox;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbCheckBox;->isChecked()Z

    move-result v0

    goto :goto_0

    .line 1806677
    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    .line 1806678
    :cond_2
    invoke-virtual {p0}, LX/Bft;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0a00e8

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    goto :goto_2
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 1806671
    return-void
.end method
