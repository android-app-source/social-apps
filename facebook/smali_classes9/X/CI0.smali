.class public final LX/CI0;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 1871514
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1871515
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 1871516
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 1871517
    invoke-static {p0, p1}, LX/CI0;->b(LX/15w;LX/186;)I

    move-result v1

    .line 1871518
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1871519
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1871520
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1871521
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1871522
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    invoke-static {p0, v1, p2, p3}, LX/CI0;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1871523
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1871524
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1871525
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 13

    .prologue
    .line 1871526
    const/4 v9, 0x0

    .line 1871527
    const/4 v8, 0x0

    .line 1871528
    const/4 v7, 0x0

    .line 1871529
    const/4 v6, 0x0

    .line 1871530
    const/4 v5, 0x0

    .line 1871531
    const/4 v4, 0x0

    .line 1871532
    const/4 v3, 0x0

    .line 1871533
    const/4 v2, 0x0

    .line 1871534
    const/4 v1, 0x0

    .line 1871535
    const/4 v0, 0x0

    .line 1871536
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->START_OBJECT:LX/15z;

    if-eq v10, v11, :cond_1

    .line 1871537
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1871538
    const/4 v0, 0x0

    .line 1871539
    :goto_0
    return v0

    .line 1871540
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1871541
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_b

    .line 1871542
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 1871543
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1871544
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_1

    if-eqz v10, :cond_1

    .line 1871545
    const-string v11, "__type__"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_2

    const-string v11, "__typename"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 1871546
    :cond_2
    invoke-static {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v9

    goto :goto_1

    .line 1871547
    :cond_3
    const-string v11, "action"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 1871548
    invoke-static {p0, p1}, LX/CHy;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 1871549
    :cond_4
    const-string v11, "document_element_type"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 1871550
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v7

    goto :goto_1

    .line 1871551
    :cond_5
    const-string v11, "element_descriptor"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 1871552
    invoke-static {p0, p1}, LX/CHz;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 1871553
    :cond_6
    const-string v11, "element_text"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_7

    .line 1871554
    invoke-static {p0, p1}, LX/8al;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 1871555
    :cond_7
    const-string v11, "grid_width_percent"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_8

    .line 1871556
    const/4 v0, 0x1

    .line 1871557
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v4

    goto :goto_1

    .line 1871558
    :cond_8
    const-string v11, "image"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_9

    .line 1871559
    invoke-static {p0, p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v3

    goto/16 :goto_1

    .line 1871560
    :cond_9
    const-string v11, "logging_token"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_a

    .line 1871561
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto/16 :goto_1

    .line 1871562
    :cond_a
    const-string v11, "style_list"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 1871563
    invoke-static {p0, p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v1

    goto/16 :goto_1

    .line 1871564
    :cond_b
    const/16 v10, 0x9

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 1871565
    const/4 v10, 0x0

    invoke-virtual {p1, v10, v9}, LX/186;->b(II)V

    .line 1871566
    const/4 v9, 0x1

    invoke-virtual {p1, v9, v8}, LX/186;->b(II)V

    .line 1871567
    const/4 v8, 0x2

    invoke-virtual {p1, v8, v7}, LX/186;->b(II)V

    .line 1871568
    const/4 v7, 0x3

    invoke-virtual {p1, v7, v6}, LX/186;->b(II)V

    .line 1871569
    const/4 v6, 0x4

    invoke-virtual {p1, v6, v5}, LX/186;->b(II)V

    .line 1871570
    if-eqz v0, :cond_c

    .line 1871571
    const/4 v0, 0x5

    const/4 v5, 0x0

    invoke-virtual {p1, v0, v4, v5}, LX/186;->a(III)V

    .line 1871572
    :cond_c
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1871573
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1871574
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1871575
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x2

    const/4 v2, 0x0

    .line 1871576
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1871577
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1871578
    if-eqz v0, :cond_0

    .line 1871579
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1871580
    invoke-static {p0, p1, v2, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1871581
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1871582
    if-eqz v0, :cond_1

    .line 1871583
    const-string v1, "action"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1871584
    invoke-static {p0, v0, p2, p3}, LX/CHy;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1871585
    :cond_1
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 1871586
    if-eqz v0, :cond_2

    .line 1871587
    const-string v0, "document_element_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1871588
    invoke-virtual {p0, p1, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1871589
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1871590
    if-eqz v0, :cond_3

    .line 1871591
    const-string v1, "element_descriptor"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1871592
    invoke-static {p0, v0, p2, p3}, LX/CHz;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1871593
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1871594
    if-eqz v0, :cond_4

    .line 1871595
    const-string v1, "element_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1871596
    invoke-static {p0, v0, p2, p3}, LX/8al;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1871597
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1871598
    if-eqz v0, :cond_5

    .line 1871599
    const-string v1, "grid_width_percent"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1871600
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1871601
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1871602
    if-eqz v0, :cond_6

    .line 1871603
    const-string v1, "image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1871604
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 1871605
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1871606
    if-eqz v0, :cond_7

    .line 1871607
    const-string v1, "logging_token"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1871608
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1871609
    :cond_7
    invoke-virtual {p0, p1, v4}, LX/15i;->g(II)I

    move-result v0

    .line 1871610
    if-eqz v0, :cond_8

    .line 1871611
    const-string v0, "style_list"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1871612
    invoke-virtual {p0, p1, v4}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 1871613
    :cond_8
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1871614
    return-void
.end method
