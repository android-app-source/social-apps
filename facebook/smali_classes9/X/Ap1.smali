.class public LX/Ap1;
.super LX/25J;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/25J",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStorySet;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile k:LX/Ap1;


# instance fields
.field public final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0tc;

.field private final d:LX/0tX;

.field private final e:Ljava/util/concurrent/Executor;

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3iW;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/99Z;

.field private final h:LX/17Q;

.field private final i:LX/0Zb;

.field public final j:LX/0ad;


# direct methods
.method public constructor <init>(LX/0tc;LX/0tX;Ljava/util/concurrent/ExecutorService;LX/0Ot;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/17Q;LX/0Zb;LX/0ad;)V
    .locals 2
    .param p3    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0tc;",
            "LX/0tX;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/0Ot",
            "<",
            "LX/3iW;",
            ">;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/17Q;",
            "LX/0Zb;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1715190
    invoke-direct {p0}, LX/25J;-><init>()V

    .line 1715191
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LX/Ap1;->a:Ljava/util/Set;

    .line 1715192
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LX/Ap1;->b:Ljava/util/Set;

    .line 1715193
    iput-object p1, p0, LX/Ap1;->c:LX/0tc;

    .line 1715194
    iput-object p2, p0, LX/Ap1;->d:LX/0tX;

    .line 1715195
    iput-object p3, p0, LX/Ap1;->e:Ljava/util/concurrent/Executor;

    .line 1715196
    iput-object p4, p0, LX/Ap1;->f:LX/0Ot;

    .line 1715197
    new-instance v0, LX/99Z;

    sget-object v1, LX/Ap2;->a:LX/0Tn;

    invoke-direct {v0, p5, v1}, LX/99Z;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Tn;)V

    iput-object v0, p0, LX/Ap1;->g:LX/99Z;

    .line 1715198
    iput-object p6, p0, LX/Ap1;->h:LX/17Q;

    .line 1715199
    iput-object p7, p0, LX/Ap1;->i:LX/0Zb;

    .line 1715200
    iput-object p8, p0, LX/Ap1;->j:LX/0ad;

    .line 1715201
    return-void
.end method

.method public static a(LX/0QB;)LX/Ap1;
    .locals 12

    .prologue
    .line 1715177
    sget-object v0, LX/Ap1;->k:LX/Ap1;

    if-nez v0, :cond_1

    .line 1715178
    const-class v1, LX/Ap1;

    monitor-enter v1

    .line 1715179
    :try_start_0
    sget-object v0, LX/Ap1;->k:LX/Ap1;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1715180
    if-eqz v2, :cond_0

    .line 1715181
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1715182
    new-instance v3, LX/Ap1;

    invoke-static {v0}, LX/0tc;->a(LX/0QB;)LX/0tc;

    move-result-object v4

    check-cast v4, LX/0tc;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v5

    check-cast v5, LX/0tX;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/ExecutorService;

    const/16 v7, 0x472

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v8

    check-cast v8, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/17Q;->a(LX/0QB;)LX/17Q;

    move-result-object v9

    check-cast v9, LX/17Q;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v10

    check-cast v10, LX/0Zb;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v11

    check-cast v11, LX/0ad;

    invoke-direct/range {v3 .. v11}, LX/Ap1;-><init>(LX/0tc;LX/0tX;Ljava/util/concurrent/ExecutorService;LX/0Ot;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/17Q;LX/0Zb;LX/0ad;)V

    .line 1715183
    move-object v0, v3

    .line 1715184
    sput-object v0, LX/Ap1;->k:LX/Ap1;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1715185
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1715186
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1715187
    :cond_1
    sget-object v0, LX/Ap1;->k:LX/Ap1;

    return-object v0

    .line 1715188
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1715189
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLStorySet;)Z
    .locals 4

    .prologue
    .line 1715174
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStorySet;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStorySet;->u()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    .line 1715175
    iget-object v2, p0, LX/Ap1;->b:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, LX/Ap1;->j:LX/0ad;

    sget v3, LX/Aow;->e:I

    const/16 p1, 0x19

    invoke-interface {v2, v3, p1}, LX/0ad;->a(II)I

    move-result v2

    if-ge v1, v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    move v0, v2

    .line 1715176
    return v0

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStorySet;I)Z
    .locals 5

    .prologue
    .line 1715168
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStorySet;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStorySet;->u()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    const/4 v2, 0x0

    .line 1715169
    iget-object v3, p0, LX/Ap1;->j:LX/0ad;

    sget v4, LX/Aow;->g:I

    const/16 p1, 0x8

    invoke-interface {v3, v4, p1}, LX/0ad;->a(II)I

    move-result v3

    if-gtz v3, :cond_1

    .line 1715170
    :cond_0
    :goto_0
    move v0, v2

    .line 1715171
    return v0

    .line 1715172
    :cond_1
    iget-object v3, p0, LX/Ap1;->j:LX/0ad;

    sget v4, LX/Aow;->j:I

    const/4 p1, 0x5

    invoke-interface {v3, v4, p1}, LX/0ad;->a(II)I

    move-result v3

    .line 1715173
    iget-object v4, p0, LX/Ap1;->a:Ljava/util/Set;

    invoke-interface {v4, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    add-int/2addr v3, p2

    add-int/lit8 v4, v1, -0x1

    if-lt v3, v4, :cond_0

    const/4 v2, 0x1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)Z
    .locals 1

    .prologue
    .line 1715167
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStorySet;

    invoke-virtual {p0, p1}, LX/Ap1;->a(Lcom/facebook/graphql/model/GraphQLStorySet;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;I)Z
    .locals 1

    .prologue
    .line 1715165
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStorySet;

    invoke-virtual {p0, p1, p2}, LX/Ap1;->a(Lcom/facebook/graphql/model/GraphQLStorySet;I)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic b(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)V
    .locals 0

    .prologue
    .line 1715166
    return-void
.end method
