.class public final LX/Cge;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Landroid/widget/TextView;

.field public final synthetic b:I

.field public final synthetic c:Lcom/facebook/reviews/composer/ComposerRatingView;


# direct methods
.method public constructor <init>(Lcom/facebook/reviews/composer/ComposerRatingView;Landroid/widget/TextView;I)V
    .locals 0

    .prologue
    .line 1927323
    iput-object p1, p0, LX/Cge;->c:Lcom/facebook/reviews/composer/ComposerRatingView;

    iput-object p2, p0, LX/Cge;->a:Landroid/widget/TextView;

    iput p3, p0, LX/Cge;->b:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, 0x7b5c43c7    # 1.14368E36f

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1927324
    iget-object v0, p0, LX/Cge;->a:Landroid/widget/TextView;

    iget-object v2, p0, LX/Cge;->c:Lcom/facebook/reviews/composer/ComposerRatingView;

    invoke-virtual {v2}, Lcom/facebook/reviews/composer/ComposerRatingView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a00d2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1927325
    iget-object v0, p0, LX/Cge;->a:Landroid/widget/TextView;

    iget-object v2, p0, LX/Cge;->c:Lcom/facebook/reviews/composer/ComposerRatingView;

    iget v3, p0, LX/Cge;->b:I

    const v4, 0x7f0a00d2

    invoke-static {v2, v3, v4}, Lcom/facebook/reviews/composer/ComposerRatingView;->a(Lcom/facebook/reviews/composer/ComposerRatingView;II)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1927326
    iget-object v0, p0, LX/Cge;->c:Lcom/facebook/reviews/composer/ComposerRatingView;

    iget-object v0, v0, Lcom/facebook/reviews/composer/ComposerRatingView;->e:Lcom/facebook/widget/ratingbar/AnimatedRatingBar;

    iget v2, p0, LX/Cge;->b:I

    invoke-virtual {v0, v2}, Lcom/facebook/widget/ratingbar/BetterRatingBar;->setRating(I)V

    .line 1927327
    iget-object v0, p0, LX/Cge;->c:Lcom/facebook/reviews/composer/ComposerRatingView;

    iget v2, p0, LX/Cge;->b:I

    .line 1927328
    invoke-static {v0, v2}, Lcom/facebook/reviews/composer/ComposerRatingView;->c$redex0(Lcom/facebook/reviews/composer/ComposerRatingView;I)V

    .line 1927329
    iget-object v0, p0, LX/Cge;->c:Lcom/facebook/reviews/composer/ComposerRatingView;

    iget-object v0, v0, Lcom/facebook/reviews/composer/ComposerRatingView;->g:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1927330
    iget-object v0, p0, LX/Cge;->c:Lcom/facebook/reviews/composer/ComposerRatingView;

    iget-object v0, v0, Lcom/facebook/reviews/composer/ComposerRatingView;->g:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cgh;

    iget v2, p0, LX/Cge;->b:I

    invoke-interface {v0, v2}, LX/Cgh;->d(I)V

    .line 1927331
    :cond_0
    const v0, -0x2ef8e881

    invoke-static {v5, v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
