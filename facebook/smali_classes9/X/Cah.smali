.class public final LX/Cah;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;)V
    .locals 0

    .prologue
    .line 1918279
    iput-object p1, p0, LX/Cah;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, 0x4af64d68    # 8070836.0f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1918280
    iget-object v1, p0, LX/Cah;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;

    iget-object v1, v1, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->d:LX/CcX;

    .line 1918281
    iget-object v2, v1, LX/CcX;->a:LX/CaP;

    move-object v1, v2

    .line 1918282
    if-eqz v1, :cond_0

    .line 1918283
    iget-object v1, p0, LX/Cah;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;

    iget-object v1, v1, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->d:LX/CcX;

    .line 1918284
    iget-object v2, v1, LX/CcX;->a:LX/CaP;

    move-object v1, v2

    .line 1918285
    iget-object v2, p0, LX/Cah;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;

    iget-object v2, v2, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->j:Ljava/lang/String;

    .line 1918286
    iget-object v4, v1, LX/CaP;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v4, v4, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->N:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;

    if-eqz v4, :cond_0

    iget-object v4, v1, LX/CaP;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    invoke-static {v4, v2}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->a$redex0(Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 1918287
    :cond_0
    :goto_0
    const v1, 0x38b2acc8

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1918288
    :cond_1
    iget-object v4, v1, LX/CaP;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v4, v4, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->N:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;

    invoke-virtual {v4}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->c()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1918289
    iget-object v4, v1, LX/CaP;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v4, v4, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->B:LX/0if;

    sget-object p0, LX/0ig;->x:LX/0ih;

    const-string p1, "single_tap_on_video"

    invoke-virtual {v4, p0, p1}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1918290
    iget-object v4, v1, LX/CaP;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    invoke-static {v4}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->B(Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;)LX/8Hs;

    move-result-object v4

    invoke-virtual {v4}, LX/8Hs;->b()V

    goto :goto_0
.end method
