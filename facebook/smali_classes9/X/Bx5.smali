.class public LX/Bx5;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pm;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/articlechaining/components/ArticleChainingItemComponentSpec;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Bx5",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/articlechaining/components/ArticleChainingItemComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1834912
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1834913
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/Bx5;->b:LX/0Zi;

    .line 1834914
    iput-object p1, p0, LX/Bx5;->a:LX/0Ot;

    .line 1834915
    return-void
.end method

.method public static a(LX/0QB;)LX/Bx5;
    .locals 4

    .prologue
    .line 1834901
    const-class v1, LX/Bx5;

    monitor-enter v1

    .line 1834902
    :try_start_0
    sget-object v0, LX/Bx5;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1834903
    sput-object v2, LX/Bx5;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1834904
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1834905
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1834906
    new-instance v3, LX/Bx5;

    const/16 p0, 0x1e01

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/Bx5;-><init>(LX/0Ot;)V

    .line 1834907
    move-object v0, v3

    .line 1834908
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1834909
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Bx5;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1834910
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1834911
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(Landroid/view/View;LX/1X1;)V
    .locals 10

    .prologue
    .line 1834916
    check-cast p2, LX/Bx3;

    .line 1834917
    iget-object v0, p0, LX/Bx5;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/articlechaining/components/ArticleChainingItemComponentSpec;

    iget-object v1, p2, LX/Bx3;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/Bx3;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1834918
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->C()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1834919
    if-nez v4, :cond_0

    .line 1834920
    :goto_0
    return-void

    .line 1834921
    :cond_0
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 1834922
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v3}, LX/1Wr;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v3

    .line 1834923
    invoke-static {v3}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;LX/0Px;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v6

    .line 1834924
    iget-object v3, v0, Lcom/facebook/feedplugins/articlechaining/components/ArticleChainingItemComponentSpec;->e:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/17V;

    const/4 v5, 0x0

    invoke-static {v6}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v6

    const-string v7, "native_newsfeed"

    const/4 v8, 0x0

    invoke-static {v2}, LX/1VO;->z(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;

    move-result-object v9

    invoke-static/range {v4 .. v9}, LX/17V;->a(Ljava/lang/String;ZLX/0lF;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    .line 1834925
    iget-object v5, v0, Lcom/facebook/feedplugins/articlechaining/components/ArticleChainingItemComponentSpec;->f:LX/BxX;

    invoke-virtual {v5, p1, v4, v3}, LX/BxX;->a(Landroid/view/View;Ljava/lang/String;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 11

    .prologue
    .line 1834864
    check-cast p2, LX/Bx3;

    .line 1834865
    iget-object v0, p0, LX/Bx5;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/articlechaining/components/ArticleChainingItemComponentSpec;

    iget-object v1, p2, LX/Bx3;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/Bx3;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    iget-object v3, p2, LX/Bx3;->c:LX/1Pm;

    const/4 v9, 0x6

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 1834866
    iget-object v4, v0, Lcom/facebook/feedplugins/articlechaining/components/ArticleChainingItemComponentSpec;->b:LX/1DR;

    invoke-virtual {v4, p1}, LX/1DR;->a(Landroid/content/Context;)I

    move-result v4

    .line 1834867
    iget-object v5, v0, Lcom/facebook/feedplugins/articlechaining/components/ArticleChainingItemComponentSpec;->f:LX/BxX;

    sget-object v6, Lcom/facebook/feedplugins/articlechaining/components/ArticleChainingItemComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v5, v2, v6}, LX/BxX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1834868
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, v8}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, v4}, LX/1Dh;->H(I)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, v7}, LX/1Dh;->B(I)LX/1Dh;

    move-result-object v5

    const/4 v6, 0x2

    invoke-interface {v5, v9, v6}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v5

    const v6, 0x7f020a3c

    invoke-interface {v5, v6}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v5

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v6

    invoke-interface {v6, v8}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v6

    invoke-interface {v6, v7}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v6

    .line 1834869
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v8

    if-eqz v8, :cond_1

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLMedia;->Y()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v8

    if-eqz v8, :cond_1

    const/4 v8, 0x1

    .line 1834870
    :goto_0
    iget-object v10, v0, Lcom/facebook/feedplugins/articlechaining/components/ArticleChainingItemComponentSpec;->c:LX/1nu;

    invoke-virtual {v10, p1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object v10

    if-eqz v8, :cond_2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLMedia;->Y()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v8

    invoke-static {v8}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v8

    :goto_1
    invoke-virtual {v10, v8}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object v8

    sget-object v10, Lcom/facebook/feedplugins/articlechaining/components/ArticleChainingItemComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v8, v10}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object v8

    const v10, 0x7f0a045d

    invoke-virtual {v8, v10}, LX/1nw;->h(I)LX/1nw;

    move-result-object v8

    const v10, 0x3ff745d1

    invoke-virtual {v8, v10}, LX/1nw;->c(F)LX/1nw;

    move-result-object v8

    sget-object v10, LX/1Up;->g:LX/1Up;

    invoke-virtual {v8, v10}, LX/1nw;->a(LX/1Up;)LX/1nw;

    move-result-object v8

    invoke-virtual {v8}, LX/1X5;->c()LX/1Di;

    move-result-object v8

    add-int/lit8 v10, v4, -0x4

    invoke-interface {v8, v10}, LX/1Di;->j(I)LX/1Di;

    move-result-object v8

    move-object v4, v8

    .line 1834871
    invoke-interface {v6, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    const/4 p2, 0x2

    const/4 p0, 0x1

    .line 1834872
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v6

    const v8, 0x7f0b08ea

    invoke-interface {v6, p0, v8}, LX/1Dh;->q(II)LX/1Dh;

    move-result-object v6

    const/4 v8, 0x6

    const v10, 0x7f0b08e1

    invoke-interface {v6, v8, v10}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v6

    const/4 v8, 0x3

    const/4 v10, 0x4

    invoke-interface {v6, v8, v10}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v6

    const/4 v8, 0x0

    invoke-interface {v6, v8}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v6

    invoke-interface {v6, p0}, LX/1Dh;->B(I)LX/1Dh;

    move-result-object v6

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v8

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->A()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v8

    const v10, 0x7f0b0050

    invoke-virtual {v8, v10}, LX/1ne;->q(I)LX/1ne;

    move-result-object v8

    const v10, 0x7f0a0161

    invoke-virtual {v8, v10}, LX/1ne;->n(I)LX/1ne;

    move-result-object v8

    invoke-virtual {v8, p0}, LX/1ne;->t(I)LX/1ne;

    move-result-object v8

    sget-object v10, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v8, v10}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v8

    const/high16 v10, 0x3f000000    # 0.5f

    invoke-virtual {v8, v10}, LX/1ne;->i(F)LX/1ne;

    move-result-object v8

    invoke-virtual {v8, p2}, LX/1ne;->i(I)LX/1ne;

    move-result-object v8

    invoke-virtual {v8, p2}, LX/1ne;->j(I)LX/1ne;

    move-result-object v8

    invoke-interface {v6, v8}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v6

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v8

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->u()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v10

    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v8

    const v10, 0x7f0b004e

    invoke-virtual {v8, v10}, LX/1ne;->q(I)LX/1ne;

    move-result-object v8

    const v10, 0x7f0a0162

    invoke-virtual {v8, v10}, LX/1ne;->n(I)LX/1ne;

    move-result-object v8

    sget-object v10, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v8, v10}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v8

    invoke-virtual {v8, p0}, LX/1ne;->j(I)LX/1ne;

    move-result-object v8

    invoke-virtual {v8, p0}, LX/1ne;->i(I)LX/1ne;

    move-result-object v8

    invoke-virtual {v8}, LX/1X5;->c()LX/1Di;

    move-result-object v8

    invoke-interface {v8, p0, p2}, LX/1Di;->d(II)LX/1Di;

    move-result-object v8

    invoke-interface {v6, v8}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v6

    move-object v6, v6

    .line 1834873
    invoke-interface {v4, v6}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    const/16 p2, 0x18

    const/16 p0, 0xc

    const/4 v10, 0x1

    .line 1834874
    invoke-static {v2}, LX/1VO;->z(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;

    move-result-object v6

    if-nez v6, :cond_3

    .line 1834875
    const/4 v6, 0x0

    .line 1834876
    :goto_2
    move-object v6, v6

    .line 1834877
    invoke-interface {v4, v6}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    .line 1834878
    const v6, -0x674882c2

    const/4 v8, 0x0

    invoke-static {p1, v6, v8}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v6

    move-object v6, v6

    .line 1834879
    invoke-interface {v4, v6}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v4

    invoke-interface {v5, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, LX/25N;->c(LX/1De;)LX/25Q;

    move-result-object v5

    const v6, 0x7f0a0160

    invoke-virtual {v5, v6}, LX/25Q;->i(I)LX/25Q;

    move-result-object v5

    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    invoke-interface {v5, v7}, LX/1Di;->r(I)LX/1Di;

    move-result-object v5

    const v6, 0x7f0b08e1

    invoke-interface {v5, v7, v6}, LX/1Di;->c(II)LX/1Di;

    move-result-object v5

    const v6, 0x7f0b08e1

    invoke-interface {v5, v9, v6}, LX/1Di;->c(II)LX/1Di;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    iget-object v5, v0, Lcom/facebook/feedplugins/articlechaining/components/ArticleChainingItemComponentSpec;->d:LX/Bx8;

    const/4 v6, 0x0

    .line 1834880
    new-instance v7, LX/Bx6;

    invoke-direct {v7, v5}, LX/Bx6;-><init>(LX/Bx8;)V

    .line 1834881
    iget-object v8, v5, LX/Bx8;->b:LX/0Zi;

    invoke-virtual {v8}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/Bx7;

    .line 1834882
    if-nez v8, :cond_0

    .line 1834883
    new-instance v8, LX/Bx7;

    invoke-direct {v8, v5}, LX/Bx7;-><init>(LX/Bx8;)V

    .line 1834884
    :cond_0
    invoke-static {v8, p1, v6, v6, v7}, LX/Bx7;->a$redex0(LX/Bx7;LX/1De;IILX/Bx6;)V

    .line 1834885
    move-object v7, v8

    .line 1834886
    move-object v6, v7

    .line 1834887
    move-object v5, v6

    .line 1834888
    iget-object v6, v5, LX/Bx7;->a:LX/Bx6;

    iput-object v1, v6, LX/Bx6;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1834889
    iget-object v6, v5, LX/Bx7;->e:Ljava/util/BitSet;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Ljava/util/BitSet;->set(I)V

    .line 1834890
    move-object v5, v5

    .line 1834891
    iget-object v6, v5, LX/Bx7;->a:LX/Bx6;

    iput-object v2, v6, LX/Bx6;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1834892
    iget-object v6, v5, LX/Bx7;->e:Ljava/util/BitSet;

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Ljava/util/BitSet;->set(I)V

    .line 1834893
    move-object v5, v5

    .line 1834894
    iget-object v6, v5, LX/Bx7;->a:LX/Bx6;

    iput-object v3, v6, LX/Bx6;->c:LX/1Pm;

    .line 1834895
    iget-object v6, v5, LX/Bx7;->e:Ljava/util/BitSet;

    const/4 v7, 0x2

    invoke-virtual {v6, v7}, Ljava/util/BitSet;->set(I)V

    .line 1834896
    move-object v5, v5

    .line 1834897
    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    move-object v0, v4

    .line 1834898
    return-object v0

    .line 1834899
    :cond_1
    const/4 v8, 0x0

    goto/16 :goto_0

    .line 1834900
    :cond_2
    const/4 v8, 0x0

    goto/16 :goto_1

    :cond_3
    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v6

    const v8, 0x7f020cde

    invoke-virtual {v6, v8}, LX/1o5;->h(I)LX/1o5;

    move-result-object v6

    sget-object v8, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v6, v8}, LX/1o5;->a(Landroid/widget/ImageView$ScaleType;)LX/1o5;

    move-result-object v6

    invoke-virtual {v6}, LX/1X5;->c()LX/1Di;

    move-result-object v6

    invoke-interface {v6, p2}, LX/1Di;->j(I)LX/1Di;

    move-result-object v6

    invoke-interface {v6, p2}, LX/1Di;->r(I)LX/1Di;

    move-result-object v6

    invoke-interface {v6, v10}, LX/1Di;->c(I)LX/1Di;

    move-result-object v6

    invoke-interface {v6, v10, p0}, LX/1Di;->m(II)LX/1Di;

    move-result-object v6

    const/4 v8, 0x5

    invoke-interface {v6, v8, p0}, LX/1Di;->m(II)LX/1Di;

    move-result-object v6

    goto/16 :goto_2
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1834858
    invoke-static {}, LX/1dS;->b()V

    .line 1834859
    iget v0, p1, LX/1dQ;->b:I

    .line 1834860
    packed-switch v0, :pswitch_data_0

    .line 1834861
    :goto_0
    return-object v2

    .line 1834862
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 1834863
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    invoke-direct {p0, v0, v1}, LX/Bx5;->a(Landroid/view/View;LX/1X1;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x674882c2
        :pswitch_0
    .end packed-switch
.end method
