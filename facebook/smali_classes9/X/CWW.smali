.class public LX/CWW;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/CWF;

.field public final b:Z

.field public final c:Ljava/lang/String;

.field public final d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/CWQ;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/CWF;ZLjava/lang/String;LX/0Px;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CWF;",
            "Z",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "LX/CWQ;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1908296
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1908297
    iput-object p1, p0, LX/CWW;->a:LX/CWF;

    .line 1908298
    iput-boolean p2, p0, LX/CWW;->b:Z

    .line 1908299
    iput-object p3, p0, LX/CWW;->c:Ljava/lang/String;

    .line 1908300
    iput-object p4, p0, LX/CWW;->d:LX/0Px;

    .line 1908301
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1908302
    instance-of v1, p1, LX/CWW;

    if-nez v1, :cond_1

    .line 1908303
    :cond_0
    :goto_0
    return v0

    .line 1908304
    :cond_1
    check-cast p1, LX/CWW;

    .line 1908305
    iget-object v1, p0, LX/CWW;->a:LX/CWF;

    iget-object v2, p1, LX/CWW;->a:LX/CWF;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, LX/CWW;->b:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iget-boolean v2, p1, LX/CWW;->b:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/CWW;->c:Ljava/lang/String;

    iget-object v2, p1, LX/CWW;->c:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/CWW;->d:LX/0Px;

    iget-object v2, p1, LX/CWW;->d:LX/0Px;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1908306
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, LX/CWW;->a:LX/CWF;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-boolean v2, p0, LX/CWW;->b:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, LX/CWW;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, LX/CWW;->d:LX/0Px;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
