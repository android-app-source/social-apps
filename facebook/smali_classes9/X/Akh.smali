.class public final LX/Akh;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/analytics/logger/HoneyClientEvent;

.field public final synthetic b:Lcom/facebook/graphql/model/HideableUnit;

.field public final synthetic c:Landroid/view/View;

.field public final synthetic d:LX/1SX;


# direct methods
.method public constructor <init>(LX/1SX;Lcom/facebook/analytics/logger/HoneyClientEvent;Lcom/facebook/graphql/model/HideableUnit;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 1709262
    iput-object p1, p0, LX/Akh;->d:LX/1SX;

    iput-object p2, p0, LX/Akh;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    iput-object p3, p0, LX/Akh;->b:Lcom/facebook/graphql/model/HideableUnit;

    iput-object p4, p0, LX/Akh;->c:Landroid/view/View;

    invoke-direct {p0}, LX/2h0;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1709263
    iget-object v0, p0, LX/Akh;->d:LX/1SX;

    iget-object v0, v0, LX/1SX;->m:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kL;

    new-instance v1, LX/27k;

    const v3, 0x7f081046

    invoke-direct {v1, v3}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 1709264
    iget-object v0, p0, LX/Akh;->d:LX/1SX;

    iget-object v6, v0, LX/1SX;->l:LX/0bH;

    new-instance v0, LX/1Nd;

    iget-object v1, p0, LX/Akh;->b:Lcom/facebook/graphql/model/HideableUnit;

    invoke-interface {v1}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v1

    sget-object v4, Lcom/facebook/graphql/enums/StoryVisibility;->VISIBLE:Lcom/facebook/graphql/enums/StoryVisibility;

    iget-object v3, p0, LX/Akh;->c:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    move-object v3, v2

    invoke-direct/range {v0 .. v5}, LX/1Nd;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/StoryVisibility;I)V

    invoke-virtual {v6, v0}, LX/0b4;->a(LX/0b7;)V

    .line 1709265
    iget-object v0, p0, LX/Akh;->d:LX/1SX;

    iget-object v0, v0, LX/1SX;->l:LX/0bH;

    new-instance v1, LX/1YM;

    invoke-direct {v1}, LX/1YM;-><init>()V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1709266
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 1709267
    iget-object v0, p0, LX/Akh;->d:LX/1SX;

    iget-object v0, v0, LX/1SX;->t:LX/0Zb;

    iget-object v1, p0, LX/Akh;->a:Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1709268
    return-void
.end method
