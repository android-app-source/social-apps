.class public final LX/Agf;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/2EJ;

.field public final synthetic b:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;LX/2EJ;)V
    .locals 0

    .prologue
    .line 1701453
    iput-object p1, p0, LX/Agf;->b:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;

    iput-object p2, p0, LX/Agf;->a:LX/2EJ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x2

    const v0, 0x5cc87f8c

    invoke-static {v4, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1701454
    iget-object v0, p0, LX/Agf;->b:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->l:LX/Ah7;

    .line 1701455
    new-instance v3, LX/6Te;

    invoke-direct {v3}, LX/6Te;-><init>()V

    move-object v3, v3

    .line 1701456
    new-instance v5, LX/4JO;

    invoke-direct {v5}, LX/4JO;-><init>()V

    iget-object v6, v0, LX/Ah7;->f:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1701457
    iget-object p1, v6, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v6, p1

    .line 1701458
    const-string p1, "actor_id"

    invoke-virtual {v5, p1, v6}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1701459
    move-object v5, v5

    .line 1701460
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    .line 1701461
    const-string p1, "should_skip"

    invoke-virtual {v5, p1, v6}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1701462
    move-object v5, v5

    .line 1701463
    iget-object v6, v3, LX/0gW;->h:Ljava/lang/String;

    move-object v6, v6

    .line 1701464
    const-string p1, "client_mutation_id"

    invoke-virtual {v5, p1, v6}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1701465
    move-object v5, v5

    .line 1701466
    const-string v6, "input"

    invoke-virtual {v3, v6, v5}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1701467
    invoke-static {v3}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v3

    .line 1701468
    iget-object v5, v0, LX/Ah7;->b:LX/0tX;

    invoke-virtual {v5, v3}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1701469
    iget-object v0, p0, LX/Agf;->b:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v2, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->n:LX/0Tn;

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 1701470
    iget-object v0, p0, LX/Agf;->a:LX/2EJ;

    invoke-virtual {v0}, LX/2EJ;->dismiss()V

    .line 1701471
    const v0, -0x73b30aec

    invoke-static {v4, v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
