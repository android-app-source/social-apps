.class public LX/AbK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/facecast/protocol/VideoBroadcastCalculateStatsRequest;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1690546
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1690547
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 5

    .prologue
    .line 1690548
    check-cast p1, Lcom/facebook/facecast/protocol/VideoBroadcastCalculateStatsRequest;

    .line 1690549
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1690550
    const-string v1, "max_viewers"

    iget v2, p1, Lcom/facebook/facecast/protocol/VideoBroadcastCalculateStatsRequest;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1690551
    const-string v1, "recorded_audio_duration"

    iget-wide v2, p1, Lcom/facebook/facecast/protocol/VideoBroadcastCalculateStatsRequest;->c:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1690552
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, v0}, Lorg/json/JSONObject;-><init>(Ljava/util/Map;)V

    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1690553
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 1690554
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "calculate_stats"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1690555
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "broadcast_client_status"

    invoke-direct {v2, v3, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1690556
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v0

    const-string v2, "video_broadcast_update"

    .line 1690557
    iput-object v2, v0, LX/14O;->b:Ljava/lang/String;

    .line 1690558
    move-object v0, v0

    .line 1690559
    const-string v2, "POST"

    .line 1690560
    iput-object v2, v0, LX/14O;->c:Ljava/lang/String;

    .line 1690561
    move-object v0, v0

    .line 1690562
    iget-object v2, p1, Lcom/facebook/facecast/protocol/VideoBroadcastCalculateStatsRequest;->a:Ljava/lang/String;

    .line 1690563
    iput-object v2, v0, LX/14O;->d:Ljava/lang/String;

    .line 1690564
    move-object v0, v0

    .line 1690565
    sget-object v2, LX/14S;->JSON:LX/14S;

    .line 1690566
    iput-object v2, v0, LX/14O;->k:LX/14S;

    .line 1690567
    move-object v0, v0

    .line 1690568
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 1690569
    iput-object v1, v0, LX/14O;->g:Ljava/util/List;

    .line 1690570
    move-object v0, v0

    .line 1690571
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1690544
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 1690545
    const/4 v0, 0x0

    return-object v0
.end method
