.class public final LX/Bqd;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/Bqe;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/Bqc;

.field private b:[Ljava/lang/String;

.field private c:I

.field public d:Ljava/util/BitSet;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 1824865
    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 1824866
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "cancelClickHandler"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "isRealOfflineMutation"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/Bqd;->b:[Ljava/lang/String;

    .line 1824867
    iput v3, p0, LX/Bqd;->c:I

    .line 1824868
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/Bqd;->c:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/Bqd;->d:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/Bqd;LX/1De;IILX/Bqc;)V
    .locals 1

    .prologue
    .line 1824869
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 1824870
    iput-object p4, p0, LX/Bqd;->a:LX/Bqc;

    .line 1824871
    iget-object v0, p0, LX/Bqd;->d:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 1824872
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1824873
    invoke-super {p0}, LX/1X5;->a()V

    .line 1824874
    const/4 v0, 0x0

    iput-object v0, p0, LX/Bqd;->a:LX/Bqc;

    .line 1824875
    sget-object v0, LX/Bqe;->a:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 1824876
    return-void
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/Bqe;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1824877
    iget-object v1, p0, LX/Bqd;->d:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/Bqd;->d:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/Bqd;->c:I

    if-ge v1, v2, :cond_2

    .line 1824878
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1824879
    :goto_0
    iget v2, p0, LX/Bqd;->c:I

    if-ge v0, v2, :cond_1

    .line 1824880
    iget-object v2, p0, LX/Bqd;->d:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1824881
    iget-object v2, p0, LX/Bqd;->b:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1824882
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1824883
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1824884
    :cond_2
    iget-object v0, p0, LX/Bqd;->a:LX/Bqc;

    .line 1824885
    invoke-virtual {p0}, LX/Bqd;->a()V

    .line 1824886
    return-object v0
.end method
