.class public final LX/CcR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# instance fields
.field public final synthetic a:Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySeekBar;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySeekBar;)V
    .locals 0

    .prologue
    .line 1921099
    iput-object p1, p0, LX/CcR;->a:Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySeekBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySeekBar;B)V
    .locals 0

    .prologue
    .line 1921100
    invoke-direct {p0, p1}, LX/CcR;-><init>(Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySeekBar;)V

    return-void
.end method


# virtual methods
.method public final onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 4

    .prologue
    .line 1921101
    if-eqz p3, :cond_2

    .line 1921102
    iget-object v0, p0, LX/CcR;->a:Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySeekBar;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySeekBar;->a:LX/CcX;

    iget-object v1, p0, LX/CcR;->a:Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySeekBar;

    iget-object v1, v1, Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySeekBar;->h:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/widget/SeekBar;->getMax()I

    move-result v2

    .line 1921103
    iget-object v3, v0, LX/CcX;->b:Ljava/util/WeakHashMap;

    invoke-virtual {v3}, Ljava/util/WeakHashMap;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Cae;

    .line 1921104
    if-eqz v3, :cond_0

    .line 1921105
    iget-object p1, v3, LX/Cae;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;

    iget-object p1, p1, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->i:Lcom/facebook/video/player/RichVideoPlayer;

    if-eqz p1, :cond_1

    iget-object p1, v3, LX/Cae;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;

    invoke-static {p1, v1}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->a$redex0(Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, v3, LX/Cae;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;

    invoke-static {p1}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->b(Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;)Z

    move-result p1

    if-nez p1, :cond_3

    .line 1921106
    :cond_1
    :goto_1
    goto :goto_0

    .line 1921107
    :cond_2
    return-void

    .line 1921108
    :cond_3
    iget-object p1, v3, LX/Cae;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;

    iget-object p1, p1, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->i:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {p1}, Lcom/facebook/video/player/RichVideoPlayer;->getVideoDurationMs()I

    move-result p1

    mul-int/2addr p1, p2

    div-int/2addr p1, v2

    .line 1921109
    iget-object p3, v3, LX/Cae;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;

    iget-object p3, p3, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->i:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v0, LX/04g;->BY_USER:LX/04g;

    invoke-virtual {p3, p1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->a(ILX/04g;)V

    goto :goto_1
.end method

.method public final onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0

    .prologue
    .line 1921110
    return-void
.end method

.method public final onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0

    .prologue
    .line 1921111
    return-void
.end method
