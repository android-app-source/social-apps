.class public final LX/CFv;
.super Landroid/graphics/drawable/ShapeDrawable$ShaderFactory;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/greetingcards/verve/model/VMColor;

.field public final synthetic b:[I

.field public final synthetic c:[F


# direct methods
.method public constructor <init>(Lcom/facebook/greetingcards/verve/model/VMColor;[I[F)V
    .locals 0

    .prologue
    .line 1864643
    iput-object p1, p0, LX/CFv;->a:Lcom/facebook/greetingcards/verve/model/VMColor;

    iput-object p2, p0, LX/CFv;->b:[I

    iput-object p3, p0, LX/CFv;->c:[F

    invoke-direct {p0}, Landroid/graphics/drawable/ShapeDrawable$ShaderFactory;-><init>()V

    return-void
.end method


# virtual methods
.method public final resize(II)Landroid/graphics/Shader;
    .locals 10

    .prologue
    const/high16 v9, 0x43870000    # 270.0f

    const/high16 v8, 0x43340000    # 180.0f

    const/high16 v7, 0x42b40000    # 90.0f

    const/high16 v6, 0x40000000    # 2.0f

    const/4 v4, 0x0

    .line 1864644
    iget-object v0, p0, LX/CFv;->a:Lcom/facebook/greetingcards/verve/model/VMColor;

    iget v0, v0, Lcom/facebook/greetingcards/verve/model/VMColor;->angle:F

    cmpg-float v0, v0, v4

    if-gez v0, :cond_0

    const/high16 v0, 0x43b40000    # 360.0f

    iget-object v1, p0, LX/CFv;->a:Lcom/facebook/greetingcards/verve/model/VMColor;

    iget v1, v1, Lcom/facebook/greetingcards/verve/model/VMColor;->angle:F

    add-float/2addr v0, v1

    .line 1864645
    :goto_0
    cmpl-float v1, v0, v4

    if-nez v1, :cond_1

    .line 1864646
    int-to-float v0, p1

    div-float v3, v0, v6

    .line 1864647
    int-to-float v0, p2

    move v2, v0

    move v1, v3

    .line 1864648
    :goto_1
    new-instance v0, Landroid/graphics/LinearGradient;

    iget-object v5, p0, LX/CFv;->b:[I

    iget-object v6, p0, LX/CFv;->c:[F

    sget-object v7, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    invoke-direct/range {v0 .. v7}, Landroid/graphics/LinearGradient;-><init>(FFFF[I[FLandroid/graphics/Shader$TileMode;)V

    return-object v0

    .line 1864649
    :cond_0
    iget-object v0, p0, LX/CFv;->a:Lcom/facebook/greetingcards/verve/model/VMColor;

    iget v0, v0, Lcom/facebook/greetingcards/verve/model/VMColor;->angle:F

    goto :goto_0

    .line 1864650
    :cond_1
    cmpl-float v1, v0, v7

    if-nez v1, :cond_2

    .line 1864651
    int-to-float v0, p2

    div-float/2addr v0, v6

    .line 1864652
    int-to-float v3, p1

    move v2, v0

    move v1, v4

    move v4, v0

    .line 1864653
    goto :goto_1

    .line 1864654
    :cond_2
    cmpl-float v1, v0, v8

    if-nez v1, :cond_3

    .line 1864655
    int-to-float v0, p1

    div-float v3, v0, v6

    .line 1864656
    int-to-float v0, p2

    move v2, v4

    move v1, v3

    move v4, v0

    goto :goto_1

    .line 1864657
    :cond_3
    cmpl-float v1, v0, v9

    if-nez v1, :cond_4

    .line 1864658
    int-to-float v3, p1

    .line 1864659
    int-to-float v0, p2

    div-float/2addr v0, v6

    move v2, v0

    move v1, v3

    move v3, v4

    move v4, v0

    .line 1864660
    goto :goto_1

    .line 1864661
    :cond_4
    sub-float v1, v7, v0

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Math;->tan(D)D

    move-result-wide v2

    double-to-float v4, v2

    .line 1864662
    const/high16 v1, -0x40800000    # -1.0f

    div-float v5, v1, v4

    .line 1864663
    int-to-float v1, p2

    div-float/2addr v1, v6

    .line 1864664
    int-to-float v2, p1

    div-float v3, v2, v6

    .line 1864665
    cmpg-float v2, v0, v7

    if-gez v2, :cond_5

    move v0, v1

    move v2, v3

    .line 1864666
    :goto_2
    mul-float/2addr v2, v5

    sub-float/2addr v0, v2

    .line 1864667
    sub-float v2, v4, v5

    div-float v4, v0, v2

    .line 1864668
    mul-float v2, v5, v4

    add-float v5, v2, v0

    .line 1864669
    add-float v2, v3, v4

    .line 1864670
    sub-float v0, v1, v5

    .line 1864671
    sub-float/2addr v3, v4

    .line 1864672
    add-float v4, v1, v5

    move v1, v2

    move v2, v0

    goto :goto_1

    .line 1864673
    :cond_5
    cmpg-float v2, v0, v8

    if-gez v2, :cond_6

    .line 1864674
    neg-float v0, v1

    move v2, v3

    goto :goto_2

    .line 1864675
    :cond_6
    cmpg-float v0, v0, v9

    if-gez v0, :cond_7

    .line 1864676
    neg-float v2, v3

    .line 1864677
    neg-float v0, v1

    goto :goto_2

    .line 1864678
    :cond_7
    neg-float v0, v3

    move v2, v0

    move v0, v1

    .line 1864679
    goto :goto_2
.end method
