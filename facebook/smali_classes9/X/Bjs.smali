.class public LX/Bjs;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0tX;

.field public final b:Landroid/content/res/Resources;

.field public final c:LX/0hB;

.field public final d:LX/1Ck;


# direct methods
.method public constructor <init>(LX/0tX;Landroid/content/res/Resources;LX/0hB;LX/1Ck;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1812882
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1812883
    iput-object p1, p0, LX/Bjs;->a:LX/0tX;

    .line 1812884
    iput-object p2, p0, LX/Bjs;->b:Landroid/content/res/Resources;

    .line 1812885
    iput-object p3, p0, LX/Bjs;->c:LX/0hB;

    .line 1812886
    iput-object p4, p0, LX/Bjs;->d:LX/1Ck;

    .line 1812887
    return-void
.end method

.method public static a(LX/0QB;)LX/Bjs;
    .locals 1

    .prologue
    .line 1812881
    invoke-static {p0}, LX/Bjs;->b(LX/0QB;)LX/Bjs;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/Bjs;
    .locals 5

    .prologue
    .line 1812879
    new-instance v4, LX/Bjs;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v1

    check-cast v1, Landroid/content/res/Resources;

    invoke-static {p0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v2

    check-cast v2, LX/0hB;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v3

    check-cast v3, LX/1Ck;

    invoke-direct {v4, v0, v1, v2, v3}, LX/Bjs;-><init>(LX/0tX;Landroid/content/res/Resources;LX/0hB;LX/1Ck;)V

    .line 1812880
    return-object v4
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/Bis;)V
    .locals 4

    .prologue
    .line 1812875
    new-instance v0, LX/Bjo;

    invoke-direct {v0, p0, p1}, LX/Bjo;-><init>(LX/Bjs;Ljava/lang/String;)V

    .line 1812876
    new-instance v1, LX/Bjp;

    invoke-direct {v1, p0, p2}, LX/Bjp;-><init>(LX/Bjs;LX/Bis;)V

    .line 1812877
    iget-object v2, p0, LX/Bjs;->d:LX/1Ck;

    const-string v3, "fetchOwnedPages"

    invoke-virtual {v2, v3, v0, v1}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 1812878
    return-void
.end method
