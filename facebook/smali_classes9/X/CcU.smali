.class public final LX/CcU;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/CcV;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/graphql/model/GraphQLStory;

.field public final synthetic b:LX/CcV;


# direct methods
.method public constructor <init>(LX/CcV;)V
    .locals 1

    .prologue
    .line 1921235
    iput-object p1, p0, LX/CcU;->b:LX/CcV;

    .line 1921236
    move-object v0, p1

    .line 1921237
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1921238
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1921239
    const-string v0, "MediaGalleryUFIFeedbackSummaryComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1921240
    if-ne p0, p1, :cond_1

    .line 1921241
    :cond_0
    :goto_0
    return v0

    .line 1921242
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1921243
    goto :goto_0

    .line 1921244
    :cond_3
    check-cast p1, LX/CcU;

    .line 1921245
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1921246
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1921247
    if-eq v2, v3, :cond_0

    .line 1921248
    iget-object v2, p0, LX/CcU;->a:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/CcU;->a:Lcom/facebook/graphql/model/GraphQLStory;

    iget-object v3, p1, LX/CcU;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1921249
    goto :goto_0

    .line 1921250
    :cond_4
    iget-object v2, p1, LX/CcU;->a:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
