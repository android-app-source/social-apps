.class public LX/AeB;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static volatile k:LX/AeB;


# instance fields
.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0tX;

.field public final d:Ljava/util/concurrent/ExecutorService;

.field public final e:LX/03V;

.field public final f:Ljava/lang/String;

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoInvitedFriendsModel;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoRecentInviteesModel;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1696808
    const-class v0, LX/AeB;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/AeB;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/ExecutorService;LX/03V;Ljava/lang/String;LX/0tX;)V
    .locals 1
    .param p1    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1696782
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1696783
    iput-object p1, p0, LX/AeB;->d:Ljava/util/concurrent/ExecutorService;

    .line 1696784
    iput-object p4, p0, LX/AeB;->c:LX/0tX;

    .line 1696785
    iput-object p3, p0, LX/AeB;->f:Ljava/lang/String;

    .line 1696786
    iput-object p2, p0, LX/AeB;->e:LX/03V;

    .line 1696787
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/AeB;->b:Ljava/util/List;

    .line 1696788
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1696789
    iput-object v0, p0, LX/AeB;->j:LX/0Px;

    .line 1696790
    return-void
.end method

.method public static a(LX/0QB;)LX/AeB;
    .locals 7

    .prologue
    .line 1696795
    sget-object v0, LX/AeB;->k:LX/AeB;

    if-nez v0, :cond_1

    .line 1696796
    const-class v1, LX/AeB;

    monitor-enter v1

    .line 1696797
    :try_start_0
    sget-object v0, LX/AeB;->k:LX/AeB;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1696798
    if-eqz v2, :cond_0

    .line 1696799
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1696800
    new-instance p0, LX/AeB;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-static {v0}, LX/0dG;->b(LX/0QB;)Ljava/lang/String;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v6

    check-cast v6, LX/0tX;

    invoke-direct {p0, v3, v4, v5, v6}, LX/AeB;-><init>(Ljava/util/concurrent/ExecutorService;LX/03V;Ljava/lang/String;LX/0tX;)V

    .line 1696801
    move-object v0, p0

    .line 1696802
    sput-object v0, LX/AeB;->k:LX/AeB;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1696803
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1696804
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1696805
    :cond_1
    sget-object v0, LX/AeB;->k:LX/AeB;

    return-object v0

    .line 1696806
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1696807
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1696809
    if-nez p1, :cond_1

    .line 1696810
    :cond_0
    :goto_0
    return-void

    .line 1696811
    :cond_1
    iget-object v0, p0, LX/AeB;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1696812
    iput-object p1, p0, LX/AeB;->g:Ljava/lang/String;

    .line 1696813
    iget-object v0, p0, LX/AeB;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1696814
    iget-object v0, p0, LX/AeB;->h:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_2

    .line 1696815
    iget-object v0, p0, LX/AeB;->h:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 1696816
    :cond_2
    new-instance v0, LX/6S9;

    invoke-direct {v0}, LX/6S9;-><init>()V

    move-object v0, v0

    .line 1696817
    const-string v1, "targetID"

    iget-object v2, p0, LX/AeB;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "count"

    const/4 p1, 0x5

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v1, v2, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1696818
    iget-object v1, p0, LX/AeB;->c:LX/0tX;

    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    iput-object v0, p0, LX/AeB;->h:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1696819
    iget-object v0, p0, LX/AeB;->h:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v1, LX/Ae9;

    invoke-direct {v1, p0}, LX/Ae9;-><init>(LX/AeB;)V

    iget-object v2, p0, LX/AeB;->d:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1696820
    iget-object v0, p0, LX/AeB;->i:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_3

    .line 1696821
    iget-object v0, p0, LX/AeB;->i:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 1696822
    :cond_3
    new-instance v0, LX/6SB;

    invoke-direct {v0}, LX/6SB;-><init>()V

    move-object v0, v0

    .line 1696823
    const-string v1, "userID"

    iget-object v2, p0, LX/AeB;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1696824
    iget-object v1, p0, LX/AeB;->c:LX/0tX;

    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    iput-object v0, p0, LX/AeB;->i:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1696825
    iget-object v0, p0, LX/AeB;->i:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v1, LX/AeA;

    invoke-direct {v1, p0}, LX/AeA;-><init>(LX/AeB;)V

    iget-object v2, p0, LX/AeB;->d:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1696826
    goto :goto_0
.end method

.method public final a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1696792
    iget-object v0, p0, LX/AeB;->g:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 1696793
    :goto_0
    return-void

    .line 1696794
    :cond_0
    iget-object v0, p0, LX/AeB;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method public final c(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1696791
    iget-object v0, p0, LX/AeB;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
