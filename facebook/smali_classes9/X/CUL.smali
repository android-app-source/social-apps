.class public LX/CUL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Dp;


# instance fields
.field private final a:LX/6tI;


# direct methods
.method public constructor <init>(LX/6tI;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1896488
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1896489
    iput-object p1, p0, LX/CUL;->a:LX/6tI;

    .line 1896490
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;LX/6so;)LX/6E8;
    .locals 2

    .prologue
    .line 1896491
    sget-object v0, LX/CUK;->a:[I

    invoke-virtual {p2}, LX/6so;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1896492
    iget-object v0, p0, LX/CUL;->a:LX/6tI;

    invoke-virtual {v0, p1, p2}, LX/6tI;->a(Landroid/view/ViewGroup;LX/6so;)LX/6E8;

    move-result-object v0

    :goto_0
    return-object v0

    .line 1896493
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f031635

    const/4 p0, 0x0

    invoke-virtual {v0, v1, p1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/ui/checkout/InstantWorkflowTermsAndPoliciesCheckoutView;

    .line 1896494
    new-instance v1, LX/CWD;

    invoke-direct {v1, v0}, LX/CWD;-><init>(Lcom/facebook/pages/common/platform/ui/checkout/InstantWorkflowTermsAndPoliciesCheckoutView;)V

    move-object v0, v1

    .line 1896495
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
