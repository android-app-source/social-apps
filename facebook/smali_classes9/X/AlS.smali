.class public LX/AlS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/9DK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/9DK",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:LX/3iR;

.field public final b:LX/3iK;

.field public final c:LX/189;

.field public final d:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

.field public final e:LX/BFI;

.field public final f:LX/0QK;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QK",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/9DG;

.field public final h:LX/1Kx;

.field public final i:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActorCache;",
            ">;"
        }
    .end annotation
.end field

.field public final j:LX/20h;

.field public final k:LX/1rp;

.field public final l:LX/1rU;

.field public m:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

.field public n:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0QK;LX/9DG;LX/3iR;LX/3iG;LX/189;Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;LX/BFI;LX/1Kx;LX/0Or;LX/20h;LX/1rp;LX/1rU;)V
    .locals 1
    .param p1    # LX/0QK;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/9DG;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QK",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            "Ljava/lang/Void;",
            ">;",
            "LX/9DG;",
            "LX/3iR;",
            "LX/3iG;",
            "LX/189;",
            "Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;",
            "LX/BFI;",
            "LX/1Kx;",
            "LX/0Or",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActorCache;",
            ">;",
            "LX/20h;",
            "LX/1rp;",
            "LX/1rU;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1709705
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1709706
    iput-object p1, p0, LX/AlS;->f:LX/0QK;

    .line 1709707
    iput-object p2, p0, LX/AlS;->g:LX/9DG;

    .line 1709708
    iput-object p3, p0, LX/AlS;->a:LX/3iR;

    .line 1709709
    sget-object v0, Lcom/facebook/feedback/ui/FeedbackControllerParams;->a:Lcom/facebook/feedback/ui/FeedbackControllerParams;

    invoke-virtual {p4, v0}, LX/3iG;->a(Lcom/facebook/feedback/ui/FeedbackControllerParams;)LX/3iK;

    move-result-object v0

    iput-object v0, p0, LX/AlS;->b:LX/3iK;

    .line 1709710
    iput-object p5, p0, LX/AlS;->c:LX/189;

    .line 1709711
    iput-object p6, p0, LX/AlS;->d:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    .line 1709712
    iput-object p7, p0, LX/AlS;->e:LX/BFI;

    .line 1709713
    iput-object p8, p0, LX/AlS;->h:LX/1Kx;

    .line 1709714
    iput-object p9, p0, LX/AlS;->i:LX/0Or;

    .line 1709715
    iput-object p10, p0, LX/AlS;->j:LX/20h;

    .line 1709716
    iput-object p11, p0, LX/AlS;->k:LX/1rp;

    .line 1709717
    iput-object p12, p0, LX/AlS;->l:LX/1rU;

    .line 1709718
    iget-object v0, p0, LX/AlS;->h:LX/1Kx;

    new-instance p1, LX/AlJ;

    invoke-direct {p1, p0}, LX/AlJ;-><init>(LX/AlS;)V

    .line 1709719
    iput-object p1, v0, LX/1Kx;->c:LX/0TF;

    .line 1709720
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 1

    .prologue
    .line 1709721
    if-eqz p1, :cond_0

    invoke-static {p1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    :goto_0
    iput-object v0, p0, LX/AlS;->n:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1709722
    return-void

    .line 1709723
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1709724
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {p0, p1}, LX/AlS;->a(Lcom/facebook/graphql/model/GraphQLStory;)V

    return-void
.end method
