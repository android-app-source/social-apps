.class public final LX/COr;
.super LX/1OM;
.source ""

# interfaces
.implements LX/3mh;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/COq;",
        ">;",
        "LX/3mh;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/COu;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/COu;)V
    .locals 0

    .prologue
    .line 1883690
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 1883691
    iput-object p1, p0, LX/COr;->a:Landroid/content/Context;

    .line 1883692
    iput-object p2, p0, LX/COr;->b:LX/COu;

    .line 1883693
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 1883689
    new-instance v0, LX/COq;

    new-instance v1, Lcom/facebook/components/ComponentView;

    iget-object v2, p0, LX/COr;->a:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/facebook/components/ComponentView;-><init>(Landroid/content/Context;)V

    invoke-direct {v0, v1}, LX/COq;-><init>(Landroid/view/View;)V

    return-object v0
.end method

.method public final a(LX/1a1;I)V
    .locals 2

    .prologue
    .line 1883685
    check-cast p1, LX/COq;

    .line 1883686
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    check-cast v0, Lcom/facebook/components/ComponentView;

    .line 1883687
    iget-object v1, p0, LX/COr;->b:LX/COu;

    invoke-virtual {v1, p2}, LX/3mY;->d(I)LX/1dV;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/components/ComponentView;->setComponent(LX/1dV;)V

    .line 1883688
    return-void
.end method

.method public final a_(II)V
    .locals 0

    .prologue
    .line 1883674
    invoke-virtual {p0, p1, p2}, LX/1OM;->c(II)V

    .line 1883675
    return-void
.end method

.method public final bG_()V
    .locals 0

    .prologue
    .line 1883683
    invoke-virtual {p0}, LX/1OM;->notifyDataSetChanged()V

    .line 1883684
    return-void
.end method

.method public final c_(I)V
    .locals 0

    .prologue
    .line 1883681
    invoke-virtual {p0, p1}, LX/1OM;->j_(I)V

    .line 1883682
    return-void
.end method

.method public final e(I)V
    .locals 0

    .prologue
    .line 1883679
    invoke-virtual {p0, p1}, LX/1OM;->i_(I)V

    .line 1883680
    return-void
.end method

.method public final f(I)V
    .locals 0

    .prologue
    .line 1883677
    invoke-virtual {p0, p1}, LX/1OM;->d(I)V

    .line 1883678
    return-void
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 1883676
    iget-object v0, p0, LX/COr;->b:LX/COu;

    invoke-virtual {v0}, LX/COu;->e()I

    move-result v0

    return v0
.end method
