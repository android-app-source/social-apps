.class public final LX/CP2;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/CP3;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/CNb;

.field public b:LX/CNc;

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/CNb;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1883878
    invoke-static {}, LX/CP3;->q()LX/CP3;

    move-result-object v0

    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1883879
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1883880
    const-string v0, "NTActivityIndicatorComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1883881
    if-ne p0, p1, :cond_1

    .line 1883882
    :cond_0
    :goto_0
    return v0

    .line 1883883
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1883884
    goto :goto_0

    .line 1883885
    :cond_3
    check-cast p1, LX/CP2;

    .line 1883886
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1883887
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1883888
    if-eq v2, v3, :cond_0

    .line 1883889
    iget-object v2, p0, LX/CP2;->a:LX/CNb;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/CP2;->a:LX/CNb;

    iget-object v3, p1, LX/CP2;->a:LX/CNb;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1883890
    goto :goto_0

    .line 1883891
    :cond_5
    iget-object v2, p1, LX/CP2;->a:LX/CNb;

    if-nez v2, :cond_4

    .line 1883892
    :cond_6
    iget-object v2, p0, LX/CP2;->b:LX/CNc;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/CP2;->b:LX/CNc;

    iget-object v3, p1, LX/CP2;->b:LX/CNc;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1883893
    goto :goto_0

    .line 1883894
    :cond_8
    iget-object v2, p1, LX/CP2;->b:LX/CNc;

    if-nez v2, :cond_7

    .line 1883895
    :cond_9
    iget-object v2, p0, LX/CP2;->c:Ljava/util/List;

    if-eqz v2, :cond_a

    iget-object v2, p0, LX/CP2;->c:Ljava/util/List;

    iget-object v3, p1, LX/CP2;->c:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1883896
    goto :goto_0

    .line 1883897
    :cond_a
    iget-object v2, p1, LX/CP2;->c:Ljava/util/List;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
