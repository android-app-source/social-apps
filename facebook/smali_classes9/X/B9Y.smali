.class public LX/B9Y;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements LX/76V;


# instance fields
.field public a:LX/13A;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/3Rb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final d:Landroid/widget/TextView;

.field private final e:Landroid/widget/TextView;

.field private final f:Landroid/widget/TextView;

.field private final g:Landroid/widget/ImageView;

.field private final h:Landroid/view/View;

.field private final i:Lcom/facebook/fbui/facepile/FacepileView;

.field private final j:Landroid/widget/TextView;

.field private final k:Landroid/view/View;

.field private l:Z

.field private m:Z

.field private n:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

.field public o:LX/78A;

.field private p:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1751873
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 1751874
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/B9Y;->m:Z

    .line 1751875
    const-class v0, LX/B9Y;

    invoke-static {v0, p0}, LX/B9Y;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1751876
    const v0, 0x7f03019d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1751877
    const v0, 0x7f0d06fa

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/B9Y;->d:Landroid/widget/TextView;

    .line 1751878
    const v0, 0x7f0d06fb

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/B9Y;->e:Landroid/widget/TextView;

    .line 1751879
    const v0, 0x7f0d06fc

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/B9Y;->f:Landroid/widget/TextView;

    .line 1751880
    const v0, 0x7f0d06f9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/B9Y;->g:Landroid/widget/ImageView;

    .line 1751881
    const v0, 0x7f0d06fd

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/B9Y;->h:Landroid/view/View;

    .line 1751882
    const v0, 0x7f0d06fe

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/facepile/FacepileView;

    iput-object v0, p0, LX/B9Y;->i:Lcom/facebook/fbui/facepile/FacepileView;

    .line 1751883
    const v0, 0x7f0d06ff

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/B9Y;->j:Landroid/widget/TextView;

    .line 1751884
    const v0, 0x7f0d0700

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/B9Y;->k:Landroid/view/View;

    .line 1751885
    const v0, 0x7f0a00ea

    invoke-virtual {p0, v0}, LX/B9Y;->setBackgroundResource(I)V

    .line 1751886
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/B9Y;

    const-class v1, LX/13A;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/13A;

    invoke-static {p0}, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;->a(LX/0QB;)Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;

    move-result-object v2

    check-cast v2, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;

    invoke-static {p0}, LX/3Rb;->a(LX/0QB;)LX/3Rb;

    move-result-object p0

    check-cast p0, LX/3Rb;

    iput-object v1, p1, LX/B9Y;->a:LX/13A;

    iput-object v2, p1, LX/B9Y;->b:Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;

    iput-object p0, p1, LX/B9Y;->c:LX/3Rb;

    return-void
.end method

.method public static b$redex0(LX/B9Y;)V
    .locals 1

    .prologue
    .line 1751887
    iget-object v0, p0, LX/B9Y;->p:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 1751888
    iget-object v0, p0, LX/B9Y;->p:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 1751889
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/B9Y;->l:Z

    .line 1751890
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, LX/B9Y;->setVisibility(I)V

    .line 1751891
    return-void
.end method

.method private setFacepileUrls(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1751866
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, LX/B9Y;->setShowSocialContextContainer(Z)V

    .line 1751867
    iget-object v0, p0, LX/B9Y;->i:Lcom/facebook/fbui/facepile/FacepileView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/facepile/FacepileView;->setFaceUrls(Ljava/util/List;)V

    .line 1751868
    return-void

    .line 1751869
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setShowSocialContextContainer(Z)V
    .locals 2

    .prologue
    .line 1751870
    iget-object v1, p0, LX/B9Y;->h:Landroid/view/View;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1751871
    return-void

    .line 1751872
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Ljava/lang/String;Lcom/facebook/interstitial/manager/InterstitialTrigger;)V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 1751828
    iget-object v0, p0, LX/B9Y;->n:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    if-ne v0, p1, :cond_1

    .line 1751829
    iget-boolean v0, p0, LX/B9Y;->l:Z

    if-eqz v0, :cond_0

    .line 1751830
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, LX/B9Y;->setVisibility(I)V

    .line 1751831
    :cond_0
    :goto_0
    return-void

    .line 1751832
    :cond_1
    iput-object p1, p0, LX/B9Y;->n:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    .line 1751833
    iget-object v0, p0, LX/B9Y;->n:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    invoke-virtual {v0}, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;->c()Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;

    move-result-object v3

    .line 1751834
    if-nez v3, :cond_2

    .line 1751835
    invoke-static {p0}, LX/B9Y;->b$redex0(LX/B9Y;)V

    goto :goto_0

    .line 1751836
    :cond_2
    iget-object v0, p0, LX/B9Y;->a:LX/13A;

    iget-object v4, p0, LX/B9Y;->n:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    invoke-virtual {v0, v4, p2, v3, p3}, LX/13A;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Ljava/lang/String;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/78A;

    move-result-object v0

    iput-object v0, p0, LX/B9Y;->o:LX/78A;

    .line 1751837
    new-instance v0, LX/B9W;

    invoke-direct {v0, p0}, LX/B9W;-><init>(LX/B9Y;)V

    .line 1751838
    new-instance v4, LX/B9X;

    invoke-direct {v4, p0}, LX/B9X;-><init>(LX/B9Y;)V

    .line 1751839
    iget-object v5, p0, LX/B9Y;->f:Landroid/widget/TextView;

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1751840
    iget-object v0, p0, LX/B9Y;->k:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1751841
    iget-object v0, p0, LX/B9Y;->d:Landroid/widget/TextView;

    iget-object v4, v3, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->title:Ljava/lang/String;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1751842
    iget-object v0, p0, LX/B9Y;->e:Landroid/widget/TextView;

    iget-object v4, v3, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->content:Ljava/lang/String;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1751843
    sget-object v0, LX/76S;->STATIC:LX/76S;

    invoke-static {v3, v0}, Lcom/facebook/quickpromotion/asset/QuickPromotionImageFetcher;->b(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;LX/76S;)Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;

    move-result-object v0

    .line 1751844
    iget-object v4, p0, LX/B9Y;->g:Landroid/widget/ImageView;

    if-eqz v0, :cond_5

    iget-object v0, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ImageParameters;->uri:Ljava/lang/String;

    :goto_1
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/widget/ImageView;->setImageURI(Landroid/net/Uri;)V

    .line 1751845
    iget-object v0, v3, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->primaryAction:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;

    iget-object v4, p0, LX/B9Y;->f:Landroid/widget/TextView;

    .line 1751846
    if-eqz v0, :cond_3

    iget-object v5, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;->title:Ljava/lang/String;

    invoke-static {v5}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 1751847
    :cond_3
    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1751848
    :goto_2
    invoke-direct {p0, v1}, LX/B9Y;->setFacepileUrls(Ljava/util/List;)V

    .line 1751849
    iget-object v0, v3, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->socialContext:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$SocialContext;

    if-nez v0, :cond_6

    .line 1751850
    invoke-virtual {p0, v1}, LX/B9Y;->setSocialContextText(Ljava/lang/CharSequence;)V

    .line 1751851
    :cond_4
    :goto_3
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/B9Y;->m:Z

    .line 1751852
    iput-boolean v2, p0, LX/B9Y;->l:Z

    .line 1751853
    invoke-virtual {p0, v2}, LX/B9Y;->setVisibility(I)V

    goto :goto_0

    :cond_5
    move-object v0, v1

    .line 1751854
    goto :goto_1

    .line 1751855
    :cond_6
    iget-object v0, v3, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->socialContext:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$SocialContext;

    iget-object v0, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$SocialContext;->text:Ljava/lang/String;

    invoke-virtual {p0, v0}, LX/B9Y;->setSocialContextText(Ljava/lang/CharSequence;)V

    .line 1751856
    iget-object v0, v3, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Creative;->socialContext:Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$SocialContext;

    iget-object v3, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$SocialContext;->friendIds:LX/0Px;

    .line 1751857
    if-eqz v3, :cond_4

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 1751858
    invoke-virtual {p0}, LX/B9Y;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b19e6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 1751859
    invoke-static {v7}, LX/0R9;->b(I)Ljava/util/ArrayList;

    move-result-object v5

    move v1, v2

    .line 1751860
    :goto_4
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_7

    if-ge v1, v7, :cond_7

    .line 1751861
    iget-object v6, p0, LX/B9Y;->c:LX/3Rb;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v6, v0, v4, v4}, LX/3Rb;->a(Ljava/lang/String;II)Landroid/net/Uri;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1751862
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 1751863
    :cond_7
    invoke-direct {p0, v5}, LX/B9Y;->setFacepileUrls(Ljava/util/List;)V

    goto :goto_3

    .line 1751864
    :cond_8
    iget-object v5, v0, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$Action;->title:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1751865
    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2
.end method

.method public final onLayout(ZIIII)V
    .locals 1

    .prologue
    .line 1751821
    invoke-super/range {p0 .. p5}, Lcom/facebook/widget/CustomFrameLayout;->onLayout(ZIIII)V

    .line 1751822
    iget-boolean v0, p0, LX/B9Y;->m:Z

    if-eqz v0, :cond_0

    .line 1751823
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/B9Y;->m:Z

    .line 1751824
    new-instance v0, LX/77n;

    invoke-direct {v0}, LX/77n;-><init>()V

    .line 1751825
    iget-object p1, p0, LX/B9Y;->o:LX/78A;

    invoke-virtual {p1}, LX/78A;->a()V

    .line 1751826
    iget-object p1, p0, LX/B9Y;->o:LX/78A;

    invoke-virtual {p1, v0}, LX/78A;->a(LX/77n;)V

    .line 1751827
    :cond_0
    return-void
.end method

.method public final onMeasure(II)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1751811
    iget-boolean v0, p0, LX/B9Y;->l:Z

    if-eqz v0, :cond_0

    .line 1751812
    invoke-virtual {p0, v1, v1}, LX/B9Y;->setMeasuredDimension(II)V

    .line 1751813
    :goto_0
    return-void

    .line 1751814
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;->onMeasure(II)V

    goto :goto_0
.end method

.method public setOnDismiss(Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 1751819
    iput-object p1, p0, LX/B9Y;->p:Ljava/lang/Runnable;

    .line 1751820
    return-void
.end method

.method public setSocialContextText(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1751815
    iget-object v0, p0, LX/B9Y;->j:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1751816
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, LX/B9Y;->setShowSocialContextContainer(Z)V

    .line 1751817
    return-void

    .line 1751818
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
