.class public final LX/CBV;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/2mO;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Landroid/text/Layout$Alignment;

.field public b:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;",
            ">;"
        }
    .end annotation
.end field

.field public final synthetic c:LX/2mO;


# direct methods
.method public constructor <init>(LX/2mO;)V
    .locals 1

    .prologue
    .line 1856456
    iput-object p1, p0, LX/CBV;->c:LX/2mO;

    .line 1856457
    move-object v0, p1

    .line 1856458
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1856459
    sget-object v0, LX/CBW;->a:Landroid/text/Layout$Alignment;

    iput-object v0, p0, LX/CBV;->a:Landroid/text/Layout$Alignment;

    .line 1856460
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1856475
    const-string v0, "QuickPromotionSocialContextComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1856461
    if-ne p0, p1, :cond_1

    .line 1856462
    :cond_0
    :goto_0
    return v0

    .line 1856463
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1856464
    goto :goto_0

    .line 1856465
    :cond_3
    check-cast p1, LX/CBV;

    .line 1856466
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1856467
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1856468
    if-eq v2, v3, :cond_0

    .line 1856469
    iget-object v2, p0, LX/CBV;->a:Landroid/text/Layout$Alignment;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/CBV;->a:Landroid/text/Layout$Alignment;

    iget-object v3, p1, LX/CBV;->a:Landroid/text/Layout$Alignment;

    invoke-virtual {v2, v3}, Landroid/text/Layout$Alignment;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1856470
    goto :goto_0

    .line 1856471
    :cond_5
    iget-object v2, p1, LX/CBV;->a:Landroid/text/Layout$Alignment;

    if-nez v2, :cond_4

    .line 1856472
    :cond_6
    iget-object v2, p0, LX/CBV;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/CBV;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/CBV;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1856473
    goto :goto_0

    .line 1856474
    :cond_7
    iget-object v2, p1, LX/CBV;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
