.class public LX/ATl;
.super Lcom/facebook/widget/CustomViewGroup;
.source ""


# instance fields
.field private a:LX/ATG;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1676851
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;)V

    .line 1676852
    return-void
.end method


# virtual methods
.method public final onLayout(ZIIII)V
    .locals 8

    .prologue
    .line 1676853
    invoke-virtual {p0}, LX/ATl;->getPaddingLeft()I

    move-result v2

    .line 1676854
    invoke-virtual {p0}, LX/ATl;->getWidth()I

    move-result v0

    invoke-virtual {p0}, LX/ATl;->getPaddingRight()I

    move-result v1

    sub-int v3, v0, v1

    .line 1676855
    invoke-virtual {p0}, LX/ATl;->getPaddingTop()I

    move-result v1

    .line 1676856
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, LX/ATl;->getChildCount()I

    move-result v4

    if-ge v0, v4, :cond_1

    .line 1676857
    iget-object v4, p0, LX/ATl;->a:LX/ATG;

    invoke-virtual {v4, v0}, LX/ATG;->a(I)LX/ASn;

    move-result-object v4

    .line 1676858
    iget-object v5, p0, LX/ATl;->a:LX/ATG;

    invoke-virtual {v5, v0}, LX/ATG;->b(I)LX/ATE;

    move-result-object v5

    .line 1676859
    invoke-virtual {v5}, Landroid/view/View;->getVisibility()I

    move-result v6

    const/16 v7, 0x8

    if-eq v6, v7, :cond_0

    .line 1676860
    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    .line 1676861
    add-int v7, v1, v6

    invoke-virtual {v5, v2, v1, v3, v7}, Landroid/view/View;->layout(IIII)V

    .line 1676862
    int-to-float v1, v1

    int-to-float v5, v6

    invoke-interface {v4}, LX/ASn;->g()F

    move-result v4

    mul-float/2addr v4, v5

    add-float/2addr v1, v4

    float-to-int v1, v1

    .line 1676863
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1676864
    :cond_1
    return-void
.end method

.method public final onMeasure(II)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 1676865
    invoke-virtual {p0}, LX/ATl;->getPaddingTop()I

    move-result v1

    invoke-virtual {p0}, LX/ATl;->getPaddingBottom()I

    move-result v2

    add-int/2addr v2, v1

    .line 1676866
    invoke-virtual {p0}, LX/ATl;->getPaddingLeft()I

    move-result v1

    invoke-virtual {p0}, LX/ATl;->getPaddingRight()I

    move-result v3

    add-int/2addr v1, v3

    move v3, v2

    move v2, v1

    move v1, v0

    .line 1676867
    :goto_0
    invoke-virtual {p0}, LX/ATl;->getChildCount()I

    move-result v4

    if-ge v0, v4, :cond_2

    .line 1676868
    iget-object v4, p0, LX/ATl;->a:LX/ATG;

    invoke-virtual {v4, v0}, LX/ATG;->a(I)LX/ASn;

    move-result-object v4

    .line 1676869
    iget-object v5, p0, LX/ATl;->a:LX/ATG;

    invoke-virtual {v5, v0}, LX/ATG;->b(I)LX/ATE;

    move-result-object v5

    .line 1676870
    invoke-virtual {v5}, Landroid/view/View;->getVisibility()I

    move-result v6

    const/16 v7, 0x8

    if-eq v6, v7, :cond_1

    .line 1676871
    invoke-virtual {p0, v5, p1, p2}, LX/ATl;->measureChild(Landroid/view/View;II)V

    .line 1676872
    if-nez v1, :cond_0

    .line 1676873
    invoke-virtual {v5}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    add-int/2addr v2, v1

    .line 1676874
    const/4 v1, 0x1

    .line 1676875
    :cond_0
    int-to-float v3, v3

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    int-to-float v5, v5

    invoke-interface {v4}, LX/ASn;->g()F

    move-result v4

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    float-to-int v3, v3

    .line 1676876
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1676877
    :cond_2
    invoke-virtual {p0}, LX/ATl;->getSuggestedMinimumWidth()I

    move-result v0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-static {v0, p1}, Landroid/view/View;->resolveSize(II)I

    move-result v0

    invoke-virtual {p0}, LX/ATl;->getSuggestedMinimumHeight()I

    move-result v1

    invoke-static {v3, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-static {v1, p2}, Landroid/view/View;->resolveSize(II)I

    move-result v1

    invoke-virtual {p0, v0, v1}, LX/ATl;->setMeasuredDimension(II)V

    .line 1676878
    return-void
.end method

.method public setAttachmentViewAdapter(LX/ATG;)V
    .locals 0

    .prologue
    .line 1676879
    iput-object p1, p0, LX/ATl;->a:LX/ATG;

    .line 1676880
    return-void
.end method
