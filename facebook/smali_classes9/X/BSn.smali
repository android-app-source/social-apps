.class public final LX/BSn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/BSm;


# instance fields
.field public final synthetic a:LX/BSr;


# direct methods
.method public constructor <init>(LX/BSr;)V
    .locals 0

    .prologue
    .line 1785855
    iput-object p1, p0, LX/BSn;->a:LX/BSr;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1785856
    iget-object v0, p0, LX/BSn;->a:LX/BSr;

    iget-object v0, v0, LX/BSr;->E:LX/ATX;

    invoke-interface {v0}, LX/ATX;->a()V

    .line 1785857
    return-void
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 1785858
    iget-object v0, p0, LX/BSn;->a:LX/BSr;

    iget-object v0, v0, LX/BSr;->D:Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$State;

    iget-object v1, p0, LX/BSn;->a:LX/BSr;

    iget-object v1, v1, LX/BSr;->D:Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$State;

    .line 1785859
    iget-object p0, v1, Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$State;->b:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    move-object v1, p0

    .line 1785860
    invoke-static {v1}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->a(Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;)Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;->setRotationAngle(I)Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;->a()Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    move-result-object v1

    .line 1785861
    iput-object v1, v0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$State;->b:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    .line 1785862
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1785863
    iget-object v0, p0, LX/BSn;->a:LX/BSr;

    iget-object v0, v0, LX/BSr;->D:Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$State;

    .line 1785864
    iput-object p1, v0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$State;->d:Ljava/lang/String;

    .line 1785865
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 1785866
    iget-object v0, p0, LX/BSn;->a:LX/BSr;

    iget-object v0, v0, LX/BSr;->A:Lcom/facebook/video/creativeediting/analytics/AudioLoggingParams;

    .line 1785867
    iget v1, v0, Lcom/facebook/video/creativeediting/analytics/AudioLoggingParams;->a:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/facebook/video/creativeediting/analytics/AudioLoggingParams;->a:I

    .line 1785868
    iget-object v0, p0, LX/BSn;->a:LX/BSr;

    iget-object v0, v0, LX/BSr;->D:Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$State;

    iget-object v1, p0, LX/BSn;->a:LX/BSr;

    iget-object v1, v1, LX/BSr;->D:Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$State;

    .line 1785869
    iget-object p0, v1, Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$State;->b:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    move-object v1, p0

    .line 1785870
    invoke-static {v1}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->a(Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;)Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;->setIsVideoMuted(Z)Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;->a()Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    move-result-object v1

    .line 1785871
    iput-object v1, v0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragmentController$State;->b:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    .line 1785872
    return-void
.end method

.method public final b(Z)V
    .locals 2

    .prologue
    .line 1785873
    iget-object v0, p0, LX/BSn;->a:LX/BSr;

    iget-object v0, v0, LX/BSr;->o:LX/BT9;

    .line 1785874
    if-eqz p1, :cond_0

    .line 1785875
    iget-object v1, v0, LX/BT9;->r:LX/BTL;

    invoke-virtual {v1}, LX/BTL;->c()V

    .line 1785876
    iget-object v1, v0, LX/BT9;->b:LX/BSr;

    invoke-virtual {v1}, LX/BSr;->i()V

    .line 1785877
    :goto_0
    return-void

    .line 1785878
    :cond_0
    iget-object v1, v0, LX/BT9;->K:Landroid/view/View;

    const/4 p0, 0x0

    invoke-virtual {v1, p0}, Landroid/view/View;->setVisibility(I)V

    .line 1785879
    iget-object v1, v0, LX/BT9;->c:LX/BSr;

    invoke-virtual {v1}, LX/BSr;->k()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1785880
    invoke-static {v0}, LX/BT9;->u$redex0(LX/BT9;)V

    goto :goto_0

    .line 1785881
    :cond_1
    invoke-static {v0}, LX/BT9;->y(LX/BT9;)V

    .line 1785882
    iget-object v1, v0, LX/BT9;->r:LX/BTL;

    invoke-virtual {v1}, LX/BTL;->b()V

    goto :goto_0
.end method
