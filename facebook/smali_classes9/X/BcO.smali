.class public LX/BcO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<",
        "L:Lcom/facebook/components/list/SectionLifecycle;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# static fields
.field private static final e:Ljava/util/concurrent/atomic/AtomicInteger;


# instance fields
.field public a:LX/BcQ;

.field public b:LX/BcO;

.field public c:Z

.field public d:LX/BcP;

.field public final f:I

.field public g:I

.field public h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/BcO;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/BcS;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "T",
            "L;"
        }
    .end annotation
.end field

.field public j:Ljava/lang/String;

.field public k:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1801902
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, LX/BcO;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method public constructor <init>(LX/BcS;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(T",
            "L;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1801893
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1801894
    sget-object v0, LX/BcO;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v0

    iput v0, p0, LX/BcO;->f:I

    .line 1801895
    iput-object p1, p0, LX/BcO;->i:LX/BcS;

    .line 1801896
    iget-object v0, p0, LX/BcO;->i:LX/BcS;

    invoke-virtual {v0}, LX/BcS;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1801897
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/BcO;->h:Ljava/util/List;

    .line 1801898
    :cond_0
    iget-object v0, p0, LX/BcO;->i:LX/BcS;

    .line 1801899
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object p1

    move-object v0, p1

    .line 1801900
    iput-object v0, p0, LX/BcO;->k:Ljava/lang/String;

    .line 1801901
    return-void
.end method

.method public static b(LX/BcO;)Ljava/util/Map;
    .locals 6
    .param p0    # LX/BcO;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/BcO;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/BcO;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1801903
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 1801904
    if-nez p0, :cond_0

    move-object v0, v1

    .line 1801905
    :goto_0
    return-object v0

    .line 1801906
    :cond_0
    iget-object v0, p0, LX/BcO;->h:Ljava/util/List;

    move-object v3, v0

    .line 1801907
    const/4 v0, 0x0

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    move v2, v0

    :goto_1
    if-ge v2, v4, :cond_1

    .line 1801908
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BcO;

    .line 1801909
    iget-object v5, v0, LX/BcO;->j:Ljava/lang/String;

    move-object v5, v5

    .line 1801910
    invoke-virtual {v1, v5, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1801911
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_1
    move-object v0, v1

    .line 1801912
    goto :goto_0
.end method

.method public static c(LX/BcO;)V
    .locals 1

    .prologue
    .line 1801886
    const/4 v0, 0x1

    .line 1801887
    iput-boolean v0, p0, LX/BcO;->c:Z

    .line 1801888
    iget-object v0, p0, LX/BcO;->b:LX/BcO;

    move-object v0, v0

    .line 1801889
    if-eqz v0, :cond_0

    .line 1801890
    iget-object v0, p0, LX/BcO;->b:LX/BcO;

    move-object v0, v0

    .line 1801891
    invoke-static {v0}, LX/BcO;->c(LX/BcO;)V

    .line 1801892
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Z)LX/BcO;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "LX/BcO",
            "<T",
            "L;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1801876
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BcO;

    .line 1801877
    if-nez p1, :cond_1

    .line 1801878
    iget-object v1, v0, LX/BcO;->h:Ljava/util/List;

    if-eqz v1, :cond_0

    .line 1801879
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, LX/BcO;->h:Ljava/util/List;

    .line 1801880
    :cond_0
    const/4 v1, 0x0

    iput v1, v0, LX/BcO;->g:I

    .line 1801881
    const/4 v1, 0x0

    .line 1801882
    iput-boolean v1, v0, LX/BcO;->c:Z
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1801883
    :cond_1
    return-object v0

    .line 1801884
    :catch_0
    move-exception v0

    .line 1801885
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1801875
    iget-object v0, p0, LX/BcO;->i:LX/BcS;

    invoke-virtual {v0}, LX/BcS;->b()Z

    move-result v0

    return v0
.end method
