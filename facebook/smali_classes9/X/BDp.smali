.class public LX/BDp;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/notifications/server/NotificationsChangeSeenStateParams;",
        "LX/BDo;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1763863
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1763864
    return-void
.end method

.method private static a(Lcom/facebook/graphql/enums/GraphQLStorySeenState;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1763843
    sget-object v0, LX/BDn;->a:[I

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1763844
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1763845
    :pswitch_0
    const-string v0, "read"

    .line 1763846
    :goto_0
    return-object v0

    :pswitch_1
    const-string v0, "seen"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 7

    .prologue
    .line 1763847
    check-cast p1, Lcom/facebook/notifications/server/NotificationsChangeSeenStateParams;

    .line 1763848
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1763849
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v5

    .line 1763850
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "notif_ids"

    const-string v2, ","

    invoke-static {v2}, LX/0PO;->on(Ljava/lang/String;)LX/0PO;

    move-result-object v2

    .line 1763851
    iget-object v3, p1, Lcom/facebook/notifications/server/NotificationsChangeSeenStateParams;->a:LX/0Px;

    move-object v3, v3

    .line 1763852
    invoke-virtual {v2, v3}, LX/0PO;->join(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1763853
    iget-object v0, p1, Lcom/facebook/notifications/server/NotificationsChangeSeenStateParams;->b:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-object v0, v0

    .line 1763854
    invoke-static {v0}, LX/BDp;->a(Lcom/facebook/graphql/enums/GraphQLStorySeenState;)Ljava/lang/String;

    move-result-object v0

    .line 1763855
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "true"

    invoke-direct {v1, v0, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1763856
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "format"

    const-string v2, "json"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1763857
    new-instance v0, LX/14N;

    const-string v1, "graphNotificationsUpdateSeenState"

    const-string v2, "POST"

    const-string v3, "me/notifications"

    sget-object v4, Lcom/facebook/http/interfaces/RequestPriority;->NON_INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    sget-object v6, LX/14S;->JSON:LX/14S;

    invoke-direct/range {v0 .. v6}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/http/interfaces/RequestPriority;Ljava/util/List;LX/14S;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1763858
    check-cast p1, Lcom/facebook/notifications/server/NotificationsChangeSeenStateParams;

    .line 1763859
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1763860
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1763861
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 1763862
    new-instance v0, LX/BDo;

    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v1

    invoke-virtual {v1}, LX/0lF;->F()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {v0, v1, p1}, LX/BDo;-><init>(Ljava/lang/Boolean;Lcom/facebook/notifications/server/NotificationsChangeSeenStateParams;)V

    return-object v0
.end method
