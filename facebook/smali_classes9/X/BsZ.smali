.class public LX/BsZ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0bH;

.field public final b:LX/189;

.field public final c:LX/0tX;

.field public final d:LX/1Ck;


# direct methods
.method public constructor <init>(LX/0bH;LX/189;LX/0tX;LX/1Ck;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1827889
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1827890
    iput-object p1, p0, LX/BsZ;->a:LX/0bH;

    .line 1827891
    iput-object p2, p0, LX/BsZ;->b:LX/189;

    .line 1827892
    iput-object p3, p0, LX/BsZ;->c:LX/0tX;

    .line 1827893
    iput-object p4, p0, LX/BsZ;->d:LX/1Ck;

    .line 1827894
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1827895
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1827896
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1827897
    new-instance v1, LX/4D1;

    invoke-direct {v1}, LX/4D1;-><init>()V

    .line 1827898
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v2

    .line 1827899
    const-string v3, "story_id"

    invoke-virtual {v1, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1827900
    const-string v2, "charity_id"

    invoke-virtual {v1, v2, p2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1827901
    new-instance v2, LX/7zu;

    invoke-direct {v2}, LX/7zu;-><init>()V

    move-object v2, v2

    .line 1827902
    const-string v3, "input"

    invoke-virtual {v2, v3, v1}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1827903
    invoke-static {v2}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v1

    .line 1827904
    iget-object v2, p0, LX/BsZ;->d:LX/1Ck;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "task_key_associate_post_to_fundraiser_for_story:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, LX/BsZ;->c:LX/0tX;

    invoke-virtual {v3, v1}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    new-instance v3, LX/BsX;

    invoke-direct {v3, p0, p1}, LX/BsX;-><init>(LX/BsZ;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-virtual {v2, v0, v1, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1827905
    return-void
.end method
