.class public LX/C9D;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:I
    .annotation build Landroid/support/annotation/DimenRes;
    .end annotation
.end field

.field private static b:LX/0Xm;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1853843
    const v0, 0x7f0b1cc0

    sput v0, LX/C9D;->a:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1853841
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1853842
    return-void
.end method

.method public static a(LX/0QB;)LX/C9D;
    .locals 3

    .prologue
    .line 1853844
    const-class v1, LX/C9D;

    monitor-enter v1

    .line 1853845
    :try_start_0
    sget-object v0, LX/C9D;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1853846
    sput-object v2, LX/C9D;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1853847
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1853848
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 1853849
    new-instance v0, LX/C9D;

    invoke-direct {v0}, LX/C9D;-><init>()V

    .line 1853850
    move-object v0, v0

    .line 1853851
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1853852
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C9D;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1853853
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1853854
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
