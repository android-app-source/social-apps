.class public LX/Av5;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/AvU;

.field private final b:LX/Avf;

.field private final c:LX/Aw8;

.field private final d:Lcom/facebook/friendsharing/inspiration/styletransfer/StyleTransferAssetDownloader;

.field private final e:LX/AwI;

.field private f:I


# direct methods
.method public constructor <init>(LX/AvU;LX/Avf;LX/Aw8;LX/AwI;Lcom/facebook/friendsharing/inspiration/styletransfer/StyleTransferAssetDownloader;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1723691
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1723692
    const/4 v0, 0x0

    iput v0, p0, LX/Av5;->f:I

    .line 1723693
    iput-object p1, p0, LX/Av5;->a:LX/AvU;

    .line 1723694
    iput-object p2, p0, LX/Av5;->b:LX/Avf;

    .line 1723695
    iput-object p3, p0, LX/Av5;->c:LX/Aw8;

    .line 1723696
    iput-object p4, p0, LX/Av5;->e:LX/AwI;

    .line 1723697
    iput-object p5, p0, LX/Av5;->d:Lcom/facebook/friendsharing/inspiration/styletransfer/StyleTransferAssetDownloader;

    .line 1723698
    return-void
.end method

.method public static b(LX/0QB;)LX/Av5;
    .locals 7

    .prologue
    .line 1723699
    new-instance v0, LX/Av5;

    invoke-static {p0}, LX/AvU;->a(LX/0QB;)LX/AvU;

    move-result-object v1

    check-cast v1, LX/AvU;

    invoke-static {p0}, LX/Avf;->a(LX/0QB;)LX/Avf;

    move-result-object v2

    check-cast v2, LX/Avf;

    invoke-static {p0}, LX/Aw8;->b(LX/0QB;)LX/Aw8;

    move-result-object v3

    check-cast v3, LX/Aw8;

    .line 1723700
    new-instance v5, LX/AwI;

    invoke-direct {v5}, LX/AwI;-><init>()V

    .line 1723701
    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/ExecutorService;

    const/16 v6, 0x2299

    invoke-static {p0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    .line 1723702
    iput-object v4, v5, LX/AwI;->a:Ljava/util/concurrent/ExecutorService;

    iput-object v6, v5, LX/AwI;->b:LX/0Ot;

    .line 1723703
    move-object v4, v5

    .line 1723704
    check-cast v4, LX/AwI;

    invoke-static {p0}, Lcom/facebook/friendsharing/inspiration/styletransfer/StyleTransferAssetDownloader;->b(LX/0QB;)Lcom/facebook/friendsharing/inspiration/styletransfer/StyleTransferAssetDownloader;

    move-result-object v5

    check-cast v5, Lcom/facebook/friendsharing/inspiration/styletransfer/StyleTransferAssetDownloader;

    invoke-direct/range {v0 .. v5}, LX/Av5;-><init>(LX/AvU;LX/Avf;LX/Aw8;LX/AwI;Lcom/facebook/friendsharing/inspiration/styletransfer/StyleTransferAssetDownloader;)V

    .line 1723705
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/friendsharing/inspiration/model/InspirationModel;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1723706
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1723707
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->getFrame()Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1723708
    iget-object v0, p0, LX/Av5;->a:LX/AvU;

    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->getFrame()Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/effects/SwipeableFrameGLConfig;->getFrame()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, LX/AvU;->a(Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;LX/89p;)V

    .line 1723709
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->getMask()Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;

    move-result-object v0

    if-eqz v0, :cond_1

    iget v0, p0, LX/Av5;->f:I

    const/4 v1, 0x5

    if-gt v0, v1, :cond_1

    .line 1723710
    iget-object v0, p0, LX/Av5;->b:LX/Avf;

    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->getMask()Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, LX/Avf;->a(Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;LX/89p;)V

    .line 1723711
    iget v0, p0, LX/Av5;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/Av5;->f:I

    .line 1723712
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->getParticleEffect()Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1723713
    iget-object v0, p0, LX/Av5;->c:LX/Aw8;

    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->getParticleEffect()Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, LX/Aw8;->a(Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig;LX/89p;)V

    .line 1723714
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->getShaderFilter()Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1723715
    iget-object v0, p0, LX/Av5;->e:LX/AwI;

    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->getShaderFilter()Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;

    move-result-object v1

    .line 1723716
    iget-object v3, v0, LX/AwI;->a:Ljava/util/concurrent/ExecutorService;

    new-instance v4, Lcom/facebook/friendsharing/inspiration/shaderfilter/InspirationShaderFilterAssetLoader$1;

    invoke-direct {v4, v0, v1, v2}, Lcom/facebook/friendsharing/inspiration/shaderfilter/InspirationShaderFilterAssetLoader$1;-><init>(LX/AwI;Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;LX/89p;)V

    const v5, 0x7eafbc65

    invoke-static {v3, v4, v5}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1723717
    :cond_3
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->getStyleTransfer()Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1723718
    iget-object v0, p0, LX/Av5;->d:Lcom/facebook/friendsharing/inspiration/styletransfer/StyleTransferAssetDownloader;

    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->getStyleTransfer()Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;

    move-result-object v1

    .line 1723719
    invoke-virtual {v1}, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;->getStyleTransferModel()Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/photos/creativeediting/model/graphql/StyleTransferGraphQLModels$StyleTransferModel;->r()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1}, LX/63w;->a(Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1}, LX/63w;->b(Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, LX/Awf;

    invoke-direct {v5, v0}, LX/Awf;-><init>(Lcom/facebook/friendsharing/inspiration/styletransfer/StyleTransferAssetDownloader;)V

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/facebook/friendsharing/inspiration/styletransfer/StyleTransferAssetDownloader;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/AwX;)V

    .line 1723720
    :cond_4
    return-void
.end method
