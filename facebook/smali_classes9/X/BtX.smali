.class public final LX/BtX;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/model/GraphQLFeedback;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLStory;

.field public final synthetic b:LX/162;

.field public final synthetic c:LX/BtY;


# direct methods
.method public constructor <init>(LX/BtY;Lcom/facebook/graphql/model/GraphQLStory;LX/162;)V
    .locals 0

    .prologue
    .line 1829308
    iput-object p1, p0, LX/BtX;->c:LX/BtY;

    iput-object p2, p0, LX/BtX;->a:Lcom/facebook/graphql/model/GraphQLStory;

    iput-object p3, p0, LX/BtX;->b:LX/162;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 1829307
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1829300
    check-cast p1, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1829301
    iget-object v0, p0, LX/BtX;->c:LX/BtY;

    iget-object v0, v0, LX/BtY;->a:Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;

    iget-object v1, p0, LX/BtX;->a:Lcom/facebook/graphql/model/GraphQLStory;

    iget-object v2, p0, LX/BtX;->b:LX/162;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->a(Lcom/facebook/graphql/model/FeedUnit;LX/162;)V

    .line 1829302
    iget-object v0, p0, LX/BtX;->c:LX/BtY;

    iget-object v0, v0, LX/BtY;->a:Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;

    invoke-static {v0, p1}, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->a$redex0(Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;Lcom/facebook/graphql/model/GraphQLFeedback;)V

    .line 1829303
    iget-object v0, p0, LX/BtX;->c:LX/BtY;

    iget-object v0, v0, LX/BtY;->a:Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;

    iget-object v0, v0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ad:LX/9DG;

    iget-object v1, p0, LX/BtX;->c:LX/BtY;

    iget-object v1, v1, LX/BtY;->a:Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;

    iget-object v1, v1, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ah:Lcom/facebook/permalink/PermalinkParams;

    .line 1829304
    iget-object v2, v1, Lcom/facebook/permalink/PermalinkParams;->f:Ljava/lang/String;

    move-object v1, v2

    .line 1829305
    invoke-virtual {v0, v1}, LX/9DG;->a(Ljava/lang/String;)V

    .line 1829306
    return-void
.end method
