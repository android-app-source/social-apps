.class public LX/CPu;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1885095
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static varargs a(LX/CNb;LX/CNc;LX/1De;[Ljava/lang/String;)LX/3rL;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CNb;",
            "LX/CNc;",
            "LX/1De;",
            "[",
            "Ljava/lang/String;",
            ")",
            "LX/3rL",
            "<",
            "LX/CO8;",
            "LX/CNb;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1885096
    array-length v1, p3

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    aget-object v2, p3, v0

    .line 1885097
    invoke-virtual {p0, v2}, LX/CNb;->b(Ljava/lang/String;)LX/CNb;

    move-result-object v2

    .line 1885098
    if-eqz v2, :cond_0

    .line 1885099
    invoke-static {v2, p1, p2}, LX/CNd;->b(LX/CNb;LX/CNc;LX/1De;)LX/CO8;

    move-result-object v1

    .line 1885100
    new-instance v0, LX/3rL;

    invoke-direct {v0, v1, v2}, LX/3rL;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1885101
    :goto_1
    return-object v0

    .line 1885102
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1885103
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static varargs a(LX/CNb;[Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 1885104
    array-length v1, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    aget-object v2, p1, v0

    .line 1885105
    invoke-virtual {p0, v2}, LX/CNb;->c(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1885106
    const/4 v0, 0x0

    invoke-virtual {p0, v2, v0}, LX/CNb;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1885107
    :goto_1
    return-object v0

    .line 1885108
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1885109
    :cond_1
    const-string v0, ""

    goto :goto_1
.end method

.method public static varargs b(LX/CNb;[Ljava/lang/String;)I
    .locals 5

    .prologue
    const/high16 v0, -0x1000000

    .line 1885110
    array-length v2, p1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, p1, v1

    .line 1885111
    invoke-virtual {p0, v3}, LX/CNb;->c(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1885112
    invoke-virtual {p0, v3, v0}, LX/CNb;->b(Ljava/lang/String;I)I

    move-result v0

    .line 1885113
    :cond_0
    return v0

    .line 1885114
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
