.class public LX/AzQ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1732520
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1732521
    iput-object p1, p0, LX/AzQ;->a:LX/0Zb;

    .line 1732522
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 2

    .prologue
    .line 1732523
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "suggested_cover_photos_prompt"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1732524
    iput-object p0, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->f:Ljava/lang/String;

    .line 1732525
    move-object v0, v0

    .line 1732526
    const-string v1, "action"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/AzQ;
    .locals 2

    .prologue
    .line 1732518
    new-instance v1, LX/AzQ;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-direct {v1, v0}, LX/AzQ;-><init>(LX/0Zb;)V

    .line 1732519
    return-object v1
.end method


# virtual methods
.method public final g(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1732516
    iget-object v0, p0, LX/AzQ;->a:LX/0Zb;

    sget-object v1, LX/AzP;->CANCEL_EDITOR:LX/AzP;

    invoke-virtual {v1}, LX/AzP;->name()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, LX/AzQ;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1732517
    return-void
.end method
