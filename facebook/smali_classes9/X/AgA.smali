.class public final LX/AgA;
.super LX/Ag8;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Ag8",
        "<",
        "Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastLobbyFragmentModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic b:LX/3Hc;


# direct methods
.method public constructor <init>(LX/3Hc;)V
    .locals 1

    .prologue
    .line 1700363
    iput-object p1, p0, LX/AgA;->b:LX/3Hc;

    invoke-direct {p0, p1}, LX/Ag8;-><init>(LX/3Hc;)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 8

    .prologue
    .line 1700358
    check-cast p1, Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastLobbyFragmentModel;

    .line 1700359
    invoke-virtual {p1}, Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastLobbyFragmentModel;->j()I

    move-result v0

    invoke-virtual {p1}, Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastLobbyFragmentModel;->k()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1700360
    iget-object v1, p0, LX/AgA;->b:LX/3Hc;

    iget-object v1, v1, LX/3Hc;->k:LX/0Zc;

    iget-object v2, p0, LX/AgA;->b:LX/3Hc;

    iget-object v2, v2, LX/3Hc;->m:Ljava/lang/String;

    sget-object v3, LX/7IM;->LIVE_POLLER_SUCCEED:LX/7IM;

    const-string v4, "fetched lobby update, viewers %d"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v1, v2, v3, v4, v5}, LX/0Zc;->a(Ljava/lang/String;LX/7IM;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1700361
    iget-object v1, p0, LX/AgA;->b:LX/3Hc;

    invoke-static {v1, v0}, LX/3Hc;->a$redex0(LX/3Hc;I)V

    .line 1700362
    return-void
.end method
