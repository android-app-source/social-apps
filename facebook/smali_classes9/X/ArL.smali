.class public LX/ArL;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "Lcom/facebook/friendsharing/inspiration/model/CameraStateSpec$ProvidesCameraState;",
        ":",
        "LX/0io;",
        ":",
        "LX/0j0;",
        ":",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingDataSpec$ProvidesInspirationLoggingData;",
        ":",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationStateSpec$ProvidesInspirationState;",
        ":",
        "LX/0is;",
        "Services::",
        "LX/0il",
        "<TModelData;>;>",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:LX/0Zb;

.field private final b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TServices;>;"
        }
    .end annotation
.end field

.field public c:LX/87V;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:Ljava/lang/String;

.field public e:I


# direct methods
.method public constructor <init>(LX/0il;LX/0Zb;)V
    .locals 1
    .param p1    # LX/0il;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TServices;",
            "LX/0Zb;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1719038
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1719039
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/ArL;->b:Ljava/lang/ref/WeakReference;

    .line 1719040
    iput-object p2, p0, LX/ArL;->a:LX/0Zb;

    .line 1719041
    return-void
.end method

.method public static a(Ljava/util/List;)LX/162;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/162;"
        }
    .end annotation

    .prologue
    .line 1719042
    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-virtual {v0}, LX/0mC;->b()LX/162;

    move-result-object v1

    .line 1719043
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1719044
    invoke-virtual {v1, v0}, LX/162;->g(Ljava/lang/String;)LX/162;

    goto :goto_0

    .line 1719045
    :cond_0
    return-object v1
.end method

.method public static a(LX/ArL;LX/5oU;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 1

    .prologue
    .line 1719037
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, LX/ArL;->a(LX/ArL;LX/5oU;LX/ArJ;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/ArL;LX/5oU;LX/ArJ;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 4

    .prologue
    .line 1719028
    iget-object v0, p0, LX/ArL;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    .line 1719029
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "inline_composer_prompt_event"

    invoke-direct {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    sget-object v2, LX/ArI;->ACTION:LX/ArI;

    invoke-virtual {v2}, LX/ArI;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1}, LX/5oU;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    .line 1719030
    invoke-direct {p0, v0, v2}, LX/ArL;->a(Lcom/facebook/composer/system/model/ComposerModelImpl;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1719031
    if-eqz p2, :cond_0

    sget-object v1, LX/ArJ;->UNKNOWN:LX/ArJ;

    if-eq p2, v1, :cond_0

    .line 1719032
    sget-object v1, LX/ArI;->REASON:LX/ArI;

    invoke-virtual {v1}, LX/ArI;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, LX/ArJ;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    :cond_0
    move-object v1, v0

    .line 1719033
    check-cast v1, LX/0is;

    invoke-interface {v1}, LX/0is;->o()Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->getSelectedId()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1719034
    check-cast v0, LX/0is;

    invoke-static {v0}, LX/87Q;->a(LX/0is;)Lcom/facebook/friendsharing/inspiration/model/InspirationModel;

    move-result-object v0

    .line 1719035
    sget-object v1, LX/ArI;->PROMPT_ID:LX/ArI;

    invoke-virtual {v1}, LX/ArI;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->getId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    sget-object v3, LX/ArI;->TRACKING_STRING:LX/ArI;

    invoke-virtual {v3}, LX/ArI;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->getTrackingString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1719036
    :cond_1
    return-object v2
.end method

.method public static a(Lcom/facebook/analytics/logger/HoneyClientEvent;Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 5
    .param p1    # Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1719018
    if-eqz p1, :cond_0

    .line 1719019
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1719020
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;->getAppliedInspirations()LX/0Px;

    move-result-object v3

    .line 1719021
    if-nez v3, :cond_1

    .line 1719022
    :cond_0
    :goto_0
    return-object p0

    .line 1719023
    :cond_1
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_2

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;

    .line 1719024
    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1719025
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1719026
    :cond_2
    sget-object v0, LX/ArI;->FEED_CTA_STORY_ID:LX/ArI;

    invoke-virtual {v0}, LX/ArI;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;->getStoryId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1719027
    sget-object v0, LX/ArI;->FEED_STORY_ELIGIBLE_EFFECT_IDS:LX/ArI;

    invoke-virtual {v0}, LX/ArI;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2}, LX/ArL;->a(Ljava/util/List;)LX/162;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_0
.end method

.method public static a(LX/ArL;Lcom/facebook/ipc/media/MediaItem;)Ljava/util/Map;
    .locals 10
    .param p0    # LX/ArL;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/ipc/media/MediaItem;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1718996
    iget-object v0, p0, LX/ArL;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    .line 1718997
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 1718998
    if-nez p1, :cond_0

    move-object v0, v4

    .line 1718999
    :goto_0
    return-object v0

    .line 1719000
    :cond_0
    new-instance v5, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy-MM-dd HH:mm:ss Z"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v5, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 1719001
    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v6

    .line 1719002
    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->j()J

    move-result-wide v2

    const-wide/16 v8, 0x0

    cmp-long v1, v2, v8

    if-eqz v1, :cond_2

    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->j()J

    move-result-wide v2

    .line 1719003
    :goto_1
    sget-object v1, LX/ArI;->MEDIA_CONTENT_ID:LX/ArI;

    invoke-virtual {v1}, LX/ArI;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v7}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v4, v1, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1719004
    sget-object v1, LX/ArI;->MEDIA_DATE:LX/ArI;

    invoke-virtual {v1}, LX/ArI;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v4, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1719005
    sget-object v1, LX/ArI;->MEDIA_WIDTH:LX/ArI;

    invoke-virtual {v1}, LX/ArI;->getName()Ljava/lang/String;

    move-result-object v1

    .line 1719006
    iget v2, v6, Lcom/facebook/ipc/media/data/MediaData;->mWidth:I

    move v2, v2

    .line 1719007
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v4, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1719008
    sget-object v1, LX/ArI;->MEDIA_HEIGHT:LX/ArI;

    invoke-virtual {v1}, LX/ArI;->getName()Ljava/lang/String;

    move-result-object v1

    .line 1719009
    iget v2, v6, Lcom/facebook/ipc/media/data/MediaData;->mHeight:I

    move v2, v2

    .line 1719010
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v4, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1719011
    sget-object v1, LX/ArI;->MEDIA_TYPE:LX/ArI;

    invoke-virtual {v1}, LX/ArI;->getName()Ljava/lang/String;

    move-result-object v1

    .line 1719012
    iget-object v2, v6, Lcom/facebook/ipc/media/data/MediaData;->mType:LX/4gQ;

    move-object v2, v2

    .line 1719013
    invoke-virtual {v2}, LX/4gQ;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v2, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v4, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v1, v0

    .line 1719014
    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->l()Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->getMediaSource()LX/875;

    move-result-object v1

    sget-object v2, LX/875;->UNKNOWN:LX/875;

    if-eq v1, v2, :cond_1

    .line 1719015
    sget-object v1, LX/ArI;->MEDIA_SOURCE:LX/ArI;

    invoke-virtual {v1}, LX/ArI;->getName()Ljava/lang/String;

    move-result-object v1

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->l()Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->getMediaSource()LX/875;

    move-result-object v0

    invoke-virtual {v0}, LX/875;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v4, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    move-object v0, v4

    .line 1719016
    goto/16 :goto_0

    .line 1719017
    :cond_2
    new-instance v2, Ljava/io/File;

    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->lastModified()J

    move-result-wide v2

    goto/16 :goto_1
.end method

.method public static a(LX/ArL;Z)Ljava/util/Map;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1719046
    iget-object v0, p0, LX/ArL;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-object v1, v0

    .line 1719047
    check-cast v1, LX/0is;

    invoke-interface {v1}, LX/0is;->o()Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;

    move-result-object v2

    .line 1719048
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 1719049
    invoke-virtual {v2}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->getInspirationModels()LX/0Px;

    move-result-object v3

    .line 1719050
    invoke-virtual {v2}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->getSelectedId()Ljava/lang/String;

    move-result-object v2

    .line 1719051
    if-eqz v3, :cond_0

    if-nez v2, :cond_1

    :cond_0
    move-object v0, v1

    .line 1719052
    :goto_0
    return-object v0

    .line 1719053
    :cond_1
    sget-object v4, LX/ArI;->INDEX:LX/ArI;

    invoke-virtual {v4}, LX/ArI;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v2}, LX/87Q;->b(LX/0Px;Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1719054
    if-eqz p1, :cond_2

    .line 1719055
    sget-object v3, LX/ArI;->RELATIVE_INDEX:LX/ArI;

    invoke-virtual {v3}, LX/ArI;->getName()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/ArL;->c:LX/87V;

    check-cast v0, LX/0is;

    invoke-virtual {v4, v0}, LX/87V;->d(LX/0is;)LX/0Px;

    move-result-object v0

    invoke-static {v0, v2}, LX/87Q;->b(LX/0Px;Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    move-object v0, v1

    .line 1719056
    goto :goto_0
.end method

.method public static a(LX/ArL;Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 1

    .prologue
    .line 1718994
    iget-object v0, p0, LX/ArL;->a:LX/0Zb;

    invoke-interface {v0, p1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1718995
    return-void
.end method

.method private a(Lcom/facebook/composer/system/model/ComposerModelImpl;Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TModelData;",
            "Lcom/facebook/analytics/logger/HoneyClientEvent;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1718946
    invoke-virtual {p1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->j()Lcom/facebook/friendsharing/inspiration/model/CameraState;

    move-result-object v1

    .line 1718947
    sget-object v0, LX/ArI;->CAMERA_ORIENTATION:LX/ArI;

    invoke-virtual {v0}, LX/ArI;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/model/CameraState;->isCameraFrontFacing()Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "front"

    :goto_0
    invoke-virtual {p2, v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    sget-object v2, LX/ArI;->FLASH_MODE:LX/ArI;

    invoke-virtual {v2}, LX/ArI;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/model/CameraState;->getFlashMode()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    sget-object v0, LX/ArI;->SURFACE:LX/ArI;

    invoke-virtual {v0}, LX/ArI;->getName()Ljava/lang/String;

    move-result-object v2

    move-object v0, p1

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->l()Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->getLoggingSurface()LX/874;

    move-result-object v0

    .line 1718948
    if-nez v0, :cond_6

    .line 1718949
    const/4 v3, 0x0

    .line 1718950
    :goto_1
    move-object v0, v3

    .line 1718951
    invoke-virtual {v1, v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    sget-object v0, LX/ArI;->SESSION_IDS_MAP:LX/ArI;

    invoke-virtual {v0}, LX/ArI;->getName()Ljava/lang/String;

    move-result-object v2

    move-object v0, p1

    check-cast v0, LX/0j0;

    .line 1718952
    move-object v3, v0

    check-cast v3, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v3}, Lcom/facebook/composer/system/model/ComposerModelImpl;->l()Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    move-result-object v3

    .line 1718953
    sget-object v4, LX/0mC;->a:LX/0mC;

    invoke-virtual {v4}, LX/0mC;->c()LX/0m9;

    move-result-object v4

    .line 1718954
    invoke-interface {v0}, LX/0j0;->getSessionId()Ljava/lang/String;

    move-result-object v5

    .line 1718955
    iget-object v6, p0, LX/ArL;->d:Ljava/lang/String;

    invoke-static {v5, v6}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_7

    .line 1718956
    const/4 v6, 0x0

    iput v6, p0, LX/ArL;->e:I

    .line 1718957
    iput-object v5, p0, LX/ArL;->d:Ljava/lang/String;

    .line 1718958
    :goto_2
    sget-object v6, LX/ArK;->INSPIRATION_SESSION_ID:LX/ArK;

    invoke-virtual {v6}, LX/ArK;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6, v5}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1718959
    sget-object v5, LX/ArK;->SEQUENCE_NUMBER:LX/ArK;

    invoke-virtual {v5}, LX/ArK;->getName()Ljava/lang/String;

    move-result-object v5

    iget v6, p0, LX/ArL;->e:I

    invoke-virtual {v4, v5, v6}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 1718960
    sget-object v5, LX/ArK;->NUX_SESSION_ID:LX/ArK;

    invoke-virtual {v5}, LX/ArK;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->getNuxSessionId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1718961
    sget-object v5, LX/ArK;->CAMERA_SESSION_ID:LX/ArK;

    invoke-virtual {v5}, LX/ArK;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->getCameraSessionId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1718962
    sget-object v5, LX/ArK;->EDITING_SESSION_ID:LX/ArK;

    invoke-virtual {v5}, LX/ArK;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->getEditingSessionId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1718963
    sget-object v5, LX/ArK;->EFFECTS_TRAY_SESSION_ID:LX/ArK;

    invoke-virtual {v5}, LX/ArK;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->getEffectsTraySessionId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1718964
    sget-object v5, LX/ArK;->GALLERY_SESSION_ID:LX/ArK;

    invoke-virtual {v5}, LX/ArK;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->getGallerySessionId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1718965
    sget-object v5, LX/ArK;->DOODLE_SESSION_ID:LX/ArK;

    invoke-virtual {v5}, LX/ArK;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->getDoodleSessionId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1718966
    sget-object v5, LX/ArK;->TEXT_SESSION_ID:LX/ArK;

    invoke-virtual {v5}, LX/ArK;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->getTextSessionId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1718967
    sget-object v5, LX/ArK;->SHARE_SHEET_SESSION_ID:LX/ArK;

    invoke-virtual {v5}, LX/ArK;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->getShareSheetSessionId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v5, v3}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1718968
    move-object v0, v4

    .line 1718969
    invoke-virtual {v1, v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    sget-object v1, LX/ArI;->IS_CAMERA_SYSTEM:LX/ArI;

    invoke-virtual {v1}, LX/ArI;->getName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1718970
    move-object v0, p1

    check-cast v0, LX/0is;

    invoke-interface {v0}, LX/0is;->o()Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;

    move-result-object v0

    .line 1718971
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1718972
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1718973
    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->getLastSelectedPreCaptureId()Ljava/lang/String;

    move-result-object v1

    .line 1718974
    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->getSelectedId()Ljava/lang/String;

    move-result-object v4

    .line 1718975
    if-eqz v1, :cond_0

    const-string v0, "1752514608329267"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, p1

    .line 1718976
    check-cast v0, LX/0is;

    invoke-static {v0, v1}, LX/87Q;->a(LX/0is;Ljava/lang/String;)Lcom/facebook/friendsharing/inspiration/model/InspirationModel;

    move-result-object v0

    .line 1718977
    if-eqz v0, :cond_0

    invoke-static {v0}, LX/87V;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationModel;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1718978
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1718979
    :cond_0
    if-eqz v4, :cond_1

    const-string v0, "1752514608329267"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1718980
    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1718981
    :cond_1
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move-object v1, p1

    .line 1718982
    check-cast v1, LX/0is;

    invoke-static {v1, v0}, LX/87Q;->a(LX/0is;Ljava/lang/String;)Lcom/facebook/friendsharing/inspiration/model/InspirationModel;

    move-result-object v0

    .line 1718983
    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->getTrackingString()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 1718984
    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->getTrackingString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 1718985
    :cond_3
    sget-object v0, LX/ArI;->APPLIED_PROMPT_IDS:LX/ArI;

    invoke-virtual {v0}, LX/ArI;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2}, LX/ArL;->a(Ljava/util/List;)LX/162;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1718986
    sget-object v0, LX/ArI;->APPLIED_TRACKING_STRINGS:LX/ArI;

    invoke-virtual {v0}, LX/ArI;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3}, LX/ArL;->a(Ljava/util/List;)LX/162;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1718987
    move-object v0, p1

    .line 1718988
    check-cast v0, LX/0io;

    invoke-interface {v0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 1718989
    check-cast p1, LX/0io;

    invoke-interface {p1}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    .line 1718990
    invoke-static {p0, v0}, LX/ArL;->a(LX/ArL;Lcom/facebook/ipc/media/MediaItem;)Ljava/util/Map;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1718991
    :cond_4
    return-void

    .line 1718992
    :cond_5
    const-string v0, "back"

    goto/16 :goto_0

    :cond_6
    invoke-virtual {v0}, LX/874;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v3, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_1

    .line 1718993
    :cond_7
    iget v6, p0, LX/ArL;->e:I

    add-int/lit8 v6, v6, 0x1

    iput v6, p0, LX/ArL;->e:I

    goto/16 :goto_2
.end method

.method public static h(LX/ArL;)Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1718941
    iget-object v0, p0, LX/ArL;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    .line 1718942
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 1718943
    sget-object v2, LX/ArI;->PHOTO_STATE:LX/ArI;

    invoke-virtual {v2}, LX/ArI;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->j()Lcom/facebook/friendsharing/inspiration/model/CameraState;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/friendsharing/inspiration/model/CameraState;->getCameraRollPermissionState()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1718944
    sget-object v2, LX/ArI;->CAMERA_STATE:LX/ArI;

    invoke-virtual {v2}, LX/ArI;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->j()Lcom/facebook/friendsharing/inspiration/model/CameraState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/CameraState;->getCapturePermissionState()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1718945
    return-object v1
.end method

.method public static i(LX/ArL;)LX/0lF;
    .locals 12

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1718913
    iget-object v0, p0, LX/ArL;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    .line 1718914
    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-virtual {v1}, LX/0mC;->c()LX/0m9;

    move-result-object v5

    move-object v1, v0

    .line 1718915
    check-cast v1, LX/0io;

    invoke-interface {v1}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v1

    invoke-static {v1}, LX/7kq;->f(LX/0Px;)Lcom/facebook/composer/attachments/ComposerAttachment;

    move-result-object v1

    .line 1718916
    if-nez v1, :cond_0

    move-object v0, v5

    .line 1718917
    :goto_0
    return-object v0

    .line 1718918
    :cond_0
    invoke-virtual {v1}, Lcom/facebook/composer/attachments/ComposerAttachment;->e()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v8

    .line 1718919
    if-nez v8, :cond_1

    move-object v0, v5

    .line 1718920
    goto :goto_0

    .line 1718921
    :cond_1
    invoke-virtual {v8}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getTextParams()LX/0Px;

    move-result-object v9

    .line 1718922
    if-eqz v9, :cond_2

    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v1

    if-lez v1, :cond_2

    move v7, v4

    .line 1718923
    :goto_1
    if-eqz v7, :cond_6

    .line 1718924
    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v10

    move v6, v3

    move v2, v3

    :goto_2
    if-ge v6, v10, :cond_3

    invoke-virtual {v9, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/creativeediting/model/TextParams;

    .line 1718925
    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/TextParams;->l()Ljava/lang/String;

    move-result-object v1

    const-string v11, "\\s+"

    invoke-virtual {v1, v11}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    array-length v1, v1

    add-int/2addr v2, v1

    .line 1718926
    add-int/lit8 v1, v6, 0x1

    move v6, v1

    goto :goto_2

    :cond_2
    move v7, v3

    .line 1718927
    goto :goto_1

    :cond_3
    move v1, v2

    .line 1718928
    :goto_3
    sget-object v2, LX/ArI;->HAS_TEXT:LX/ArI;

    invoke-virtual {v2}, LX/ArI;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2, v7}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;

    .line 1718929
    sget-object v2, LX/ArI;->TEXT_COUNT:LX/ArI;

    invoke-virtual {v2}, LX/ArI;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2, v1}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 1718930
    invoke-virtual {v8}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getDoodleParams()LX/0Px;

    move-result-object v1

    .line 1718931
    if-eqz v1, :cond_4

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-lez v1, :cond_4

    move v3, v4

    .line 1718932
    :cond_4
    sget-object v1, LX/ArI;->HAS_DOODLE:LX/ArI;

    invoke-virtual {v1}, LX/ArI;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1, v3}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;

    .line 1718933
    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->l()Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->getInspirationDoodleExtraLoggingData()Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;

    move-result-object v0

    .line 1718934
    if-eqz v0, :cond_5

    .line 1718935
    sget-object v1, LX/ArI;->DOODLE_STROKE_COUNT:LX/ArI;

    invoke-virtual {v1}, LX/ArI;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;->getDoodleStrokeCount()I

    move-result v2

    invoke-virtual {v5, v1, v2}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 1718936
    sget-object v1, LX/ArI;->DOODLE_SIZE_COUNT:LX/ArI;

    invoke-virtual {v1}, LX/ArI;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;->getDoodleSizeCount()I

    move-result v2

    invoke-virtual {v5, v1, v2}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 1718937
    sget-object v1, LX/ArI;->DOODLE_COLOR_COUNT:LX/ArI;

    invoke-virtual {v1}, LX/ArI;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;->getDoodleColorCount()I

    move-result v2

    invoke-virtual {v5, v1, v2}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 1718938
    sget-object v1, LX/ArI;->DOODLE_MAX_BRUSH_SIZE:LX/ArI;

    invoke-virtual {v1}, LX/ArI;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;->getDoodleMaxBrushSize()F

    move-result v2

    invoke-virtual {v5, v1, v2}, LX/0m9;->a(Ljava/lang/String;F)LX/0m9;

    .line 1718939
    sget-object v1, LX/ArI;->DOODLE_UNDO_COUNT:LX/ArI;

    invoke-virtual {v1}, LX/ArI;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleExtraLoggingData;->getDoodleUndoCount()I

    move-result v0

    invoke-virtual {v5, v1, v0}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    :cond_5
    move-object v0, v5

    .line 1718940
    goto/16 :goto_0

    :cond_6
    move v1, v3

    goto :goto_3
.end method


# virtual methods
.method public final a(I)V
    .locals 2

    .prologue
    .line 1718911
    sget-object v0, LX/ArH;->NUX_SHOWN:LX/ArH;

    invoke-static {p0, v0}, LX/ArL;->a(LX/ArL;LX/5oU;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    sget-object v1, LX/ArI;->INDEX:LX/ArI;

    invoke-virtual {v1}, LX/ArI;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    invoke-static {p0}, LX/ArL;->h(LX/ArL;)Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    invoke-static {p0, v0}, LX/ArL;->a(LX/ArL;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1718912
    return-void
.end method

.method public final a(Lcom/facebook/ipc/media/MediaItem;F)V
    .locals 4

    .prologue
    .line 1718907
    sget-object v0, LX/ArH;->CAMERA_CAPTURE:LX/ArH;

    invoke-static {p0, v0}, LX/ArL;->a(LX/ArL;LX/5oU;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    sget-object v1, LX/ArI;->DURATION:LX/ArI;

    invoke-virtual {v1}, LX/ArI;->getName()Ljava/lang/String;

    move-result-object v1

    float-to-double v2, p2

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {p0, v1}, LX/ArL;->a(LX/ArL;Z)Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    invoke-static {p0, p1}, LX/ArL;->a(LX/ArL;Lcom/facebook/ipc/media/MediaItem;)Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    invoke-static {p0, v0}, LX/ArL;->a(LX/ArL;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1718908
    return-void
.end method

.method public final b(LX/ArJ;F)V
    .locals 4

    .prologue
    .line 1718909
    sget-object v0, LX/ArH;->POST_PROMPT:LX/ArH;

    invoke-static {p0, v0, p1}, LX/ArL;->a(LX/ArL;LX/5oU;LX/ArJ;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    sget-object v1, LX/ArI;->DURATION:LX/ArI;

    invoke-virtual {v1}, LX/ArI;->getName()Ljava/lang/String;

    move-result-object v1

    float-to-double v2, p2

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {p0, v1}, LX/ArL;->a(LX/ArL;Z)Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    sget-object v1, LX/ArI;->EXTRA_ANNOTATIONS_DATA:LX/ArI;

    invoke-virtual {v1}, LX/ArI;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0}, LX/ArL;->i(LX/ArL;)LX/0lF;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    invoke-static {p0, v0}, LX/ArL;->a(LX/ArL;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1718910
    return-void
.end method
