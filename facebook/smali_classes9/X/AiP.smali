.class public LX/AiP;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1706222
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FBFeedAwesomizerProfileListCardProfileFragmentModel;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1706223
    invoke-virtual {p0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FBFeedAwesomizerProfileListCardProfileFragmentModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v1, 0x41e065f

    if-ne v0, v1, :cond_1

    .line 1706224
    invoke-virtual {p0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FBFeedAwesomizerProfileListCardProfileFragmentModel;->k()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-nez v0, :cond_0

    .line 1706225
    const/4 v0, 0x0

    .line 1706226
    :goto_0
    return-object v0

    .line 1706227
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FBFeedAwesomizerProfileListCardProfileFragmentModel;->k()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const-class v2, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FBFeedAwesomizerProfileListCardProfileFragmentModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {v1, v0, v3, v2}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FBFeedAwesomizerProfileListCardProfileFragmentModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {v0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FBFeedAwesomizerProfileListCardProfileFragmentModel$CoverPhotoModel$PhotoModel;->j()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v1, v0, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1706228
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FBFeedAwesomizerProfileListCardProfileFragmentModel;->l()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v1, v0, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
