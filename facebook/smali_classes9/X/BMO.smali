.class public LX/BMO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Alg;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<CONTEXT::",
        "LX/B5o;",
        ":",
        "Lcom/facebook/ipc/productionprompts/actioncontext/PromptActionContextInterfaces$HasPromptSessionId;",
        ">",
        "Ljava/lang/Object;",
        "LX/Alg",
        "<TCONTEXT;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/B35;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/B35;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1777125
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1777126
    iput-object p1, p0, LX/BMO;->a:LX/0Or;

    .line 1777127
    return-void
.end method


# virtual methods
.method public final a(LX/88f;LX/1RN;)V
    .locals 0
    .param p1    # LX/88f;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1777128
    return-void
.end method

.method public final a(Landroid/view/View;LX/1RN;LX/B5o;)V
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "LX/1RN;",
            "TCONTEXT;)V"
        }
    .end annotation

    .prologue
    .line 1777129
    invoke-static/range {p2 .. p2}, LX/1RN;->a(LX/1RN;)LX/1kK;

    move-result-object v2

    .line 1777130
    instance-of v3, v2, LX/1kW;

    invoke-static {v3}, LX/0PB;->checkArgument(Z)V

    .line 1777131
    check-cast v2, LX/1kW;

    invoke-virtual {v2}, LX/1kW;->e()Lcom/facebook/productionprompts/model/ProductionPrompt;

    move-result-object v3

    .line 1777132
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    const-class v4, Landroid/app/Activity;

    invoke-static {v2, v4}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    move-object v12, v2

    check-cast v12, Landroid/app/Activity;

    .line 1777133
    if-eqz v12, :cond_0

    .line 1777134
    new-instance v2, LX/5QX;

    invoke-direct {v2}, LX/5QX;-><init>()V

    invoke-virtual {v3}, Lcom/facebook/productionprompts/model/ProductionPrompt;->m()Lcom/facebook/productionprompts/model/ProfilePictureOverlay;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/productionprompts/model/ProfilePictureOverlay;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, LX/5QX;->a(Ljava/lang/String;)LX/5QX;

    move-result-object v2

    new-instance v4, LX/5QY;

    invoke-direct {v4}, LX/5QY;-><init>()V

    invoke-virtual {v3}, Lcom/facebook/productionprompts/model/ProductionPrompt;->m()Lcom/facebook/productionprompts/model/ProfilePictureOverlay;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/productionprompts/model/ProfilePictureOverlay;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, LX/5QY;->a(Ljava/lang/String;)LX/5QY;

    move-result-object v3

    invoke-virtual {v3}, LX/5QY;->a()Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel$ImageModel;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/5QX;->a(Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel$ImageModel;)LX/5QX;

    move-result-object v2

    invoke-virtual {v2}, LX/5QX;->a()Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    move-result-object v14

    .line 1777135
    move-object/from16 v0, p0

    iget-object v2, v0, LX/BMO;->a:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    move-object v13, v2

    check-cast v13, LX/B35;

    sget-object v15, Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;->SINGLE_OVERLAY:Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;

    const/16 v16, 0x0

    const-string v17, "composer_prompt"

    const-wide/16 v8, 0x0

    move-object/from16 v0, p2

    iget-object v2, v0, LX/1RN;->a:LX/1kK;

    invoke-interface {v2}, LX/1kK;->b()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p2

    iget-object v3, v0, LX/1RN;->b:LX/1lP;

    iget-object v3, v3, LX/1lP;->c:Ljava/lang/String;

    check-cast p3, LX/B5p;

    invoke-virtual/range {p3 .. p3}, LX/B5p;->b()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    move-object/from16 v0, p2

    iget-object v6, v0, LX/1RN;->b:LX/1lP;

    iget-object v6, v6, LX/1lP;->b:Ljava/lang/String;

    move-object/from16 v0, p2

    iget-object v7, v0, LX/1RN;->c:LX/32e;

    iget-object v7, v7, LX/32e;->a:LX/24P;

    invoke-virtual {v7}, LX/24P;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-static/range {v2 .. v7}, Lcom/facebook/productionprompts/logging/PromptAnalytics;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/productionprompts/logging/PromptAnalytics;

    move-result-object v10

    const/4 v11, 0x0

    move-object v2, v13

    move-object v3, v12

    move-object v4, v15

    move-object/from16 v5, v16

    move-object/from16 v6, v17

    move-object v7, v14

    invoke-virtual/range {v2 .. v11}, LX/B35;->a(Landroid/app/Activity;Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;Ljava/lang/String;Ljava/lang/String;LX/5QV;JLcom/facebook/productionprompts/logging/PromptAnalytics;I)V

    .line 1777136
    :cond_0
    return-void
.end method

.method public final b(LX/1RN;)Z
    .locals 2

    .prologue
    .line 1777137
    invoke-static {p1}, LX/1RN;->a(LX/1RN;)LX/1kK;

    move-result-object v0

    .line 1777138
    instance-of v1, v0, LX/1kW;

    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 1777139
    check-cast v0, LX/1kW;

    .line 1777140
    iget-object v1, v0, LX/1kW;->a:Lcom/facebook/productionprompts/model/ProductionPrompt;

    move-object v0, v1

    .line 1777141
    invoke-virtual {v0}, Lcom/facebook/productionprompts/model/ProductionPrompt;->m()Lcom/facebook/productionprompts/model/ProfilePictureOverlay;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
