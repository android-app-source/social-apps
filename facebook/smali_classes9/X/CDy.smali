.class public final LX/CDy;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/CE0;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/CDz;

.field private b:[Ljava/lang/String;

.field private c:I

.field private d:Ljava/util/BitSet;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 1860408
    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 1860409
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "actorProfilePhoto"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/CDy;->b:[Ljava/lang/String;

    .line 1860410
    iput v3, p0, LX/CDy;->c:I

    .line 1860411
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/CDy;->c:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/CDy;->d:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/CDy;LX/1De;IILX/CDz;)V
    .locals 1

    .prologue
    .line 1860404
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 1860405
    iput-object p4, p0, LX/CDy;->a:LX/CDz;

    .line 1860406
    iget-object v0, p0, LX/CDy;->d:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 1860407
    return-void
.end method


# virtual methods
.method public final a(LX/1X1;)LX/CDy;
    .locals 2

    .prologue
    .line 1860400
    iget-object v1, p0, LX/CDy;->a:LX/CDz;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, v1, LX/CDz;->b:LX/1X1;

    .line 1860401
    iget-object v0, p0, LX/CDy;->d:Ljava/util/BitSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1860402
    return-object p0

    .line 1860403
    :cond_0
    invoke-virtual {p1}, LX/1X1;->g()LX/1X1;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/CharSequence;)LX/CDy;
    .locals 2

    .prologue
    .line 1860397
    iget-object v0, p0, LX/CDy;->a:LX/CDz;

    iput-object p1, v0, LX/CDz;->a:Ljava/lang/CharSequence;

    .line 1860398
    iget-object v0, p0, LX/CDy;->d:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1860399
    return-object p0
.end method

.method public final a(Z)LX/CDy;
    .locals 1

    .prologue
    .line 1860375
    iget-object v0, p0, LX/CDy;->a:LX/CDz;

    iput-boolean p1, v0, LX/CDz;->f:Z

    .line 1860376
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 1860393
    invoke-super {p0}, LX/1X5;->a()V

    .line 1860394
    const/4 v0, 0x0

    iput-object v0, p0, LX/CDy;->a:LX/CDz;

    .line 1860395
    sget-object v0, LX/CE0;->a:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 1860396
    return-void
.end method

.method public final b(Ljava/lang/CharSequence;)LX/CDy;
    .locals 1

    .prologue
    .line 1860391
    iget-object v0, p0, LX/CDy;->a:LX/CDz;

    iput-object p1, v0, LX/CDz;->d:Ljava/lang/CharSequence;

    .line 1860392
    return-object p0
.end method

.method public final b(Z)LX/CDy;
    .locals 1

    .prologue
    .line 1860389
    iget-object v0, p0, LX/CDy;->a:LX/CDz;

    iput-boolean p1, v0, LX/CDz;->l:Z

    .line 1860390
    return-object p0
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/CE0;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1860379
    iget-object v1, p0, LX/CDy;->d:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/CDy;->d:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/CDy;->c:I

    if-ge v1, v2, :cond_2

    .line 1860380
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1860381
    :goto_0
    iget v2, p0, LX/CDy;->c:I

    if-ge v0, v2, :cond_1

    .line 1860382
    iget-object v2, p0, LX/CDy;->d:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1860383
    iget-object v2, p0, LX/CDy;->b:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1860384
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1860385
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1860386
    :cond_2
    iget-object v0, p0, LX/CDy;->a:LX/CDz;

    .line 1860387
    invoke-virtual {p0}, LX/CDy;->a()V

    .line 1860388
    return-object v0
.end method

.method public final h(I)LX/CDy;
    .locals 1

    .prologue
    .line 1860377
    iget-object v0, p0, LX/CDy;->a:LX/CDz;

    iput p1, v0, LX/CDz;->c:I

    .line 1860378
    return-object p0
.end method
