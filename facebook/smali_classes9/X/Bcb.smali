.class public LX/Bcb;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static a:LX/Bcb;


# instance fields
.field public final b:Ljava/lang/ref/ReferenceQueue;

.field public final c:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/Bca;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/facebook/components/list/ServiceRegistry$ReferenceQueueThread;


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    .line 1802365
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1802366
    new-instance v0, Ljava/lang/ref/ReferenceQueue;

    invoke-direct {v0}, Ljava/lang/ref/ReferenceQueue;-><init>()V

    iput-object v0, p0, LX/Bcb;->b:Ljava/lang/ref/ReferenceQueue;

    .line 1802367
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/Bcb;->c:Ljava/util/HashMap;

    .line 1802368
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/Bcb;->d:Ljava/util/Set;

    .line 1802369
    new-instance v0, Lcom/facebook/components/list/ServiceRegistry$ReferenceQueueThread;

    invoke-direct {v0, p0}, Lcom/facebook/components/list/ServiceRegistry$ReferenceQueueThread;-><init>(LX/Bcb;)V

    iput-object v0, p0, LX/Bcb;->e:Lcom/facebook/components/list/ServiceRegistry$ReferenceQueueThread;

    .line 1802370
    iget-object v0, p0, LX/Bcb;->e:Lcom/facebook/components/list/ServiceRegistry$ReferenceQueueThread;

    invoke-virtual {v0}, Lcom/facebook/components/list/ServiceRegistry$ReferenceQueueThread;->start()V

    .line 1802371
    return-void
.end method

.method public static declared-synchronized a()LX/Bcb;
    .locals 2

    .prologue
    .line 1802372
    const-class v1, LX/Bcb;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/Bcb;->a:LX/Bcb;

    if-nez v0, :cond_0

    .line 1802373
    new-instance v0, LX/Bcb;

    invoke-direct {v0}, LX/Bcb;-><init>()V

    sput-object v0, LX/Bcb;->a:LX/Bcb;

    .line 1802374
    :cond_0
    sget-object v0, LX/Bcb;->a:LX/Bcb;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 1802375
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
