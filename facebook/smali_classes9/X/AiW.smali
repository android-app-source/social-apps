.class public final enum LX/AiW;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/AiW;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/AiW;

.field public static final enum DISCOVER:LX/AiW;

.field public static final enum REFOLLOW:LX/AiW;

.field public static final enum SEEFIRST:LX/AiW;

.field public static final enum UNFOLLOW:LX/AiW;


# instance fields
.field private mName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1706276
    new-instance v0, LX/AiW;

    const-string v1, "SEEFIRST"

    const-string v2, "see_first"

    invoke-direct {v0, v1, v3, v2}, LX/AiW;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/AiW;->SEEFIRST:LX/AiW;

    .line 1706277
    new-instance v0, LX/AiW;

    const-string v1, "UNFOLLOW"

    const-string v2, "following"

    invoke-direct {v0, v1, v4, v2}, LX/AiW;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/AiW;->UNFOLLOW:LX/AiW;

    .line 1706278
    new-instance v0, LX/AiW;

    const-string v1, "REFOLLOW"

    const-string v2, "unfollowed"

    invoke-direct {v0, v1, v5, v2}, LX/AiW;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/AiW;->REFOLLOW:LX/AiW;

    .line 1706279
    new-instance v0, LX/AiW;

    const-string v1, "DISCOVER"

    const-string v2, "discover"

    invoke-direct {v0, v1, v6, v2}, LX/AiW;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/AiW;->DISCOVER:LX/AiW;

    .line 1706280
    const/4 v0, 0x4

    new-array v0, v0, [LX/AiW;

    sget-object v1, LX/AiW;->SEEFIRST:LX/AiW;

    aput-object v1, v0, v3

    sget-object v1, LX/AiW;->UNFOLLOW:LX/AiW;

    aput-object v1, v0, v4

    sget-object v1, LX/AiW;->REFOLLOW:LX/AiW;

    aput-object v1, v0, v5

    sget-object v1, LX/AiW;->DISCOVER:LX/AiW;

    aput-object v1, v0, v6

    sput-object v0, LX/AiW;->$VALUES:[LX/AiW;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1706273
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1706274
    iput-object p3, p0, LX/AiW;->mName:Ljava/lang/String;

    .line 1706275
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/AiW;
    .locals 1

    .prologue
    .line 1706281
    const-class v0, LX/AiW;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/AiW;

    return-object v0
.end method

.method public static values()[LX/AiW;
    .locals 1

    .prologue
    .line 1706272
    sget-object v0, LX/AiW;->$VALUES:[LX/AiW;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/AiW;

    return-object v0
.end method


# virtual methods
.method public final getIndex()I
    .locals 1

    .prologue
    .line 1706271
    invoke-virtual {p0}, LX/AiW;->ordinal()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1706270
    iget-object v0, p0, LX/AiW;->mName:Ljava/lang/String;

    return-object v0
.end method
