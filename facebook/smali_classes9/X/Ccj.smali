.class public LX/Ccj;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field public final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1WN;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1WM;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17Y;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/2g9;


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/2g9;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/1WN;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1WM;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/17Y;",
            ">;",
            "LX/2g9;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1921579
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1921580
    iput-object p1, p0, LX/Ccj;->a:LX/0Ot;

    .line 1921581
    iput-object p2, p0, LX/Ccj;->b:LX/0Ot;

    .line 1921582
    iput-object p3, p0, LX/Ccj;->c:LX/0Ot;

    .line 1921583
    iput-object p5, p0, LX/Ccj;->e:LX/2g9;

    .line 1921584
    iput-object p4, p0, LX/Ccj;->d:LX/0Ot;

    .line 1921585
    return-void
.end method

.method public static a(LX/0QB;)LX/Ccj;
    .locals 9

    .prologue
    .line 1921586
    const-class v1, LX/Ccj;

    monitor-enter v1

    .line 1921587
    :try_start_0
    sget-object v0, LX/Ccj;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1921588
    sput-object v2, LX/Ccj;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1921589
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1921590
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1921591
    new-instance v3, LX/Ccj;

    const/16 v4, 0xf45

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0xf44

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x455

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0xc49

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static {v0}, LX/2g9;->a(LX/0QB;)LX/2g9;

    move-result-object v8

    check-cast v8, LX/2g9;

    invoke-direct/range {v3 .. v8}, LX/Ccj;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/2g9;)V

    .line 1921592
    move-object v0, v3

    .line 1921593
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1921594
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Ccj;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1921595
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1921596
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/Ccj;Landroid/content/Context;Ljava/lang/String;IZ)Landroid/text/SpannableString;
    .locals 5

    .prologue
    .line 1921597
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1921598
    invoke-virtual {v0, p3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1921599
    const v2, 0x7f0826fd

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1921600
    new-instance v3, LX/47x;

    invoke-direct {v3, v0}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    invoke-virtual {v3, v1}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    move-result-object v0

    const-string v1, "link_change_content_filter"

    .line 1921601
    new-instance v3, LX/Cci;

    invoke-direct {v3, p0, p1, p4, p2}, LX/Cci;-><init>(LX/Ccj;Landroid/content/Context;ZLjava/lang/String;)V

    move-object v3, v3

    .line 1921602
    const/16 v4, 0x21

    invoke-virtual {v0, v1, v2, v3, v4}, LX/47x;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;I)LX/47x;

    move-result-object v0

    .line 1921603
    invoke-virtual {v0}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v0

    return-object v0
.end method
