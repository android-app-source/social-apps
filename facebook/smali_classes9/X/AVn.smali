.class public LX/AVn;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Landroid/os/Handler;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/AVj;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/AVt;

.field public d:LX/AVq;

.field public e:LX/AVo;

.field public f:LX/AVp;

.field public g:LX/AVl;

.field public h:LX/AVr;

.field public i:LX/AVd;


# direct methods
.method public constructor <init>(LX/AVq;LX/AVo;LX/AVp;LX/AVl;LX/AVr;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1680351
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1680352
    iput-object p1, p0, LX/AVn;->d:LX/AVq;

    .line 1680353
    iput-object p2, p0, LX/AVn;->e:LX/AVo;

    .line 1680354
    iput-object p3, p0, LX/AVn;->f:LX/AVp;

    .line 1680355
    iput-object p4, p0, LX/AVn;->g:LX/AVl;

    .line 1680356
    iput-object p5, p0, LX/AVn;->h:LX/AVr;

    .line 1680357
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/AVn;->b:Ljava/util/List;

    .line 1680358
    iget-object v0, p0, LX/AVn;->b:Ljava/util/List;

    iget-object v1, p0, LX/AVn;->d:LX/AVq;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1680359
    iget-object v0, p0, LX/AVn;->b:Ljava/util/List;

    iget-object v1, p0, LX/AVn;->e:LX/AVo;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1680360
    iget-object v0, p0, LX/AVn;->b:Ljava/util/List;

    iget-object v1, p0, LX/AVn;->f:LX/AVp;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1680361
    iget-object v0, p0, LX/AVn;->b:Ljava/util/List;

    iget-object v1, p0, LX/AVn;->g:LX/AVl;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1680362
    iget-object v0, p0, LX/AVn;->b:Ljava/util/List;

    iget-object v1, p0, LX/AVn;->h:LX/AVr;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1680363
    return-void
.end method

.method public static b(LX/0QB;)LX/AVn;
    .locals 6

    .prologue
    .line 1680325
    new-instance v0, LX/AVn;

    .line 1680326
    new-instance v2, LX/AVq;

    invoke-direct {v2}, LX/AVq;-><init>()V

    .line 1680327
    invoke-static {p0}, LX/AVw;->a(LX/0QB;)LX/AVw;

    move-result-object v1

    check-cast v1, LX/AVw;

    .line 1680328
    iput-object v1, v2, LX/AVq;->d:LX/AVw;

    .line 1680329
    move-object v1, v2

    .line 1680330
    check-cast v1, LX/AVq;

    .line 1680331
    new-instance v3, LX/AVo;

    invoke-direct {v3}, LX/AVo;-><init>()V

    .line 1680332
    invoke-static {p0}, LX/AVw;->a(LX/0QB;)LX/AVw;

    move-result-object v2

    check-cast v2, LX/AVw;

    .line 1680333
    iput-object v2, v3, LX/AVo;->d:LX/AVw;

    .line 1680334
    move-object v2, v3

    .line 1680335
    check-cast v2, LX/AVo;

    .line 1680336
    new-instance v3, LX/AVp;

    invoke-direct {v3}, LX/AVp;-><init>()V

    .line 1680337
    move-object v3, v3

    .line 1680338
    move-object v3, v3

    .line 1680339
    check-cast v3, LX/AVp;

    .line 1680340
    new-instance v4, LX/AVl;

    invoke-direct {v4}, LX/AVl;-><init>()V

    .line 1680341
    move-object v4, v4

    .line 1680342
    move-object v4, v4

    .line 1680343
    check-cast v4, LX/AVl;

    .line 1680344
    new-instance v5, LX/AVr;

    invoke-direct {v5}, LX/AVr;-><init>()V

    .line 1680345
    move-object v5, v5

    .line 1680346
    move-object v5, v5

    .line 1680347
    check-cast v5, LX/AVr;

    invoke-direct/range {v0 .. v5}, LX/AVn;-><init>(LX/AVq;LX/AVo;LX/AVp;LX/AVl;LX/AVr;)V

    .line 1680348
    invoke-static {p0}, LX/0Ss;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v1

    check-cast v1, Landroid/os/Handler;

    .line 1680349
    iput-object v1, v0, LX/AVn;->a:Landroid/os/Handler;

    .line 1680350
    return-object v0
.end method

.method public static d(LX/AVn;)V
    .locals 5

    .prologue
    .line 1680297
    iget-object v0, p0, LX/AVn;->a:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/facecast/broadcast/nux/controllers/LiveNuxControllerManager$1;

    invoke-direct {v1, p0}, Lcom/facebook/facecast/broadcast/nux/controllers/LiveNuxControllerManager$1;-><init>(LX/AVn;)V

    const-wide/16 v2, 0xc8

    const v4, 0x3d050bde

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 1680298
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;)V
    .locals 3

    .prologue
    .line 1680301
    if-nez p1, :cond_0

    .line 1680302
    :goto_0
    return-void

    .line 1680303
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->p()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1680304
    iget-object v0, p0, LX/AVn;->d:LX/AVq;

    invoke-virtual {p1}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->p()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/AVj;->a(LX/0Px;)V

    .line 1680305
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->q()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1680306
    iget-object v0, p0, LX/AVn;->e:LX/AVo;

    invoke-virtual {p1}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->q()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/AVj;->a(LX/0Px;)V

    .line 1680307
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->m()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1680308
    iget-object v0, p0, LX/AVn;->f:LX/AVp;

    invoke-virtual {p1}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->m()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/AVj;->a(LX/0Px;)V

    .line 1680309
    :cond_3
    invoke-virtual {p1}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->k()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1680310
    iget-object v0, p0, LX/AVn;->f:LX/AVp;

    invoke-virtual {p1}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->k()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/AVk;->b(LX/0Px;)V

    .line 1680311
    :cond_4
    invoke-virtual {p1}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1680312
    iget-object v0, p0, LX/AVn;->g:LX/AVl;

    invoke-virtual {p1}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/AVj;->a(LX/0Px;)V

    .line 1680313
    :cond_5
    invoke-virtual {p1}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->j()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1680314
    iget-object v0, p0, LX/AVn;->g:LX/AVl;

    invoke-virtual {p1}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->j()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/AVk;->b(LX/0Px;)V

    .line 1680315
    :cond_6
    invoke-virtual {p1}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->n()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1680316
    iget-object v0, p0, LX/AVn;->h:LX/AVr;

    invoke-virtual {p1}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->n()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/AVj;->a(LX/0Px;)V

    .line 1680317
    :cond_7
    invoke-virtual {p1}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->l()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1680318
    iget-object v0, p0, LX/AVn;->h:LX/AVr;

    invoke-virtual {p1}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->l()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/AVk;->b(LX/0Px;)V

    .line 1680319
    :cond_8
    iget-object v0, p0, LX/AVn;->h:LX/AVr;

    invoke-virtual {p1}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->o()Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel$VideoModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel$VideoModel;->k()Ljava/lang/String;

    move-result-object v1

    .line 1680320
    iget-object v2, v0, LX/AVi;->a:Landroid/view/View;

    move-object v2, v2

    .line 1680321
    check-cast v2, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;

    const/4 p0, 0x0

    const/4 p1, 0x0

    invoke-virtual {v2, v1, p0, p1}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 1680322
    iget-object v2, v0, LX/AVi;->a:Landroid/view/View;

    move-object v2, v2

    .line 1680323
    check-cast v2, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;

    invoke-virtual {v2}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->b()V

    .line 1680324
    goto/16 :goto_0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 1680299
    iget-object v0, p0, LX/AVn;->a:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 1680300
    return-void
.end method
