.class public final LX/C2D;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/394;


# instance fields
.field public final synthetic a:LX/C2F;


# direct methods
.method public constructor <init>(LX/C2F;)V
    .locals 0

    .prologue
    .line 1843970
    iput-object p1, p0, LX/C2D;->a:LX/C2F;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/04g;)V
    .locals 8

    .prologue
    .line 1843971
    iget-object v0, p0, LX/C2D;->a:LX/C2F;

    iget-object v0, v0, LX/C2F;->x:LX/1Yd;

    iget-object v1, p0, LX/C2D;->a:LX/C2F;

    iget-object v1, v1, LX/C2F;->s:LX/395;

    invoke-virtual {v0, v1}, LX/1Yd;->a(LX/395;)V

    .line 1843972
    iget-object v0, p0, LX/C2D;->a:LX/C2F;

    iget-object v0, v0, LX/C2F;->t:LX/0hE;

    invoke-interface {v0}, LX/0hE;->i()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1843973
    iget-object v0, p0, LX/C2D;->a:LX/C2F;

    iget-object v0, v0, LX/C2F;->y:LX/1C2;

    iget-object v1, p0, LX/C2D;->a:LX/C2F;

    iget-object v1, v1, LX/C2F;->s:LX/395;

    invoke-virtual {v1}, LX/395;->s()LX/162;

    move-result-object v1

    sget-object v2, LX/04G;->INLINE_PLAYER:LX/04G;

    iget-object v3, p1, LX/04g;->value:Ljava/lang/String;

    iget-object v4, p0, LX/C2D;->a:LX/C2F;

    iget-object v4, v4, LX/C2F;->s:LX/395;

    invoke-virtual {v4}, LX/395;->p()I

    move-result v4

    iget-object v5, p0, LX/C2D;->a:LX/C2F;

    iget-object v5, v5, LX/C2F;->s:LX/395;

    invoke-virtual {v5}, LX/395;->y()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, LX/C2D;->a:LX/C2F;

    iget-object v6, v6, LX/C2F;->s:LX/395;

    .line 1843974
    iget-object v7, v6, LX/395;->d:Lcom/facebook/video/analytics/VideoPlayerInfo;

    move-object v6, v7

    .line 1843975
    iget-object v7, v6, Lcom/facebook/video/analytics/VideoPlayerInfo;->b:LX/04D;

    move-object v6, v7

    .line 1843976
    iget-object v7, p0, LX/C2D;->a:LX/C2F;

    iget-object v7, v7, LX/C2F;->s:LX/395;

    invoke-virtual/range {v0 .. v7}, LX/1C2;->b(LX/0lF;LX/04G;Ljava/lang/String;ILjava/lang/String;LX/04D;LX/098;)LX/1C2;

    .line 1843977
    :cond_0
    iget-object v0, p0, LX/C2D;->a:LX/C2F;

    iget-object v0, v0, LX/C2F;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 1843978
    if-eqz v0, :cond_1

    .line 1843979
    invoke-static {v0}, LX/182;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 1843980
    iget-object v2, p0, LX/C2D;->a:LX/C2F;

    iget-object v2, v2, LX/C2F;->e:LX/0bH;

    new-instance v3, LX/1Ze;

    .line 1843981
    iget-object v4, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v4

    .line 1843982
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v4

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-direct {v3, v4, v0}, LX/1Ze;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, LX/0b4;->a(LX/0b7;)V

    .line 1843983
    :cond_1
    return-void

    .line 1843984
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/04g;LX/7Jv;)V
    .locals 8

    .prologue
    .line 1843985
    iget-object v0, p0, LX/C2D;->a:LX/C2F;

    iget-object v0, v0, LX/C2F;->t:LX/0hE;

    invoke-interface {v0}, LX/0hE;->i()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1843986
    iget-object v0, p0, LX/C2D;->a:LX/C2F;

    iget-object v0, v0, LX/C2F;->y:LX/1C2;

    iget-object v1, p0, LX/C2D;->a:LX/C2F;

    iget-object v1, v1, LX/C2F;->s:LX/395;

    invoke-virtual {v1}, LX/395;->s()LX/162;

    move-result-object v1

    sget-object v2, LX/04G;->INLINE_PLAYER:LX/04G;

    iget-object v3, p1, LX/04g;->value:Ljava/lang/String;

    iget v4, p2, LX/7Jv;->c:I

    iget-object v5, p0, LX/C2D;->a:LX/C2F;

    iget-object v5, v5, LX/C2F;->s:LX/395;

    invoke-virtual {v5}, LX/395;->y()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, LX/C2D;->a:LX/C2F;

    iget-object v6, v6, LX/C2F;->s:LX/395;

    .line 1843987
    iget-object v7, v6, LX/395;->d:Lcom/facebook/video/analytics/VideoPlayerInfo;

    move-object v6, v7

    .line 1843988
    iget-object v7, v6, Lcom/facebook/video/analytics/VideoPlayerInfo;->b:LX/04D;

    move-object v6, v7

    .line 1843989
    iget-object v7, p0, LX/C2D;->a:LX/C2F;

    iget-object v7, v7, LX/C2F;->s:LX/395;

    invoke-virtual/range {v0 .. v7}, LX/1C2;->a(LX/0lF;LX/04G;Ljava/lang/String;ILjava/lang/String;LX/04D;LX/098;)LX/1C2;

    .line 1843990
    :cond_0
    iget-object v0, p0, LX/C2D;->a:LX/C2F;

    iget-object v0, v0, LX/C2F;->w:LX/3J0;

    if-eqz v0, :cond_1

    .line 1843991
    iget-object v0, p0, LX/C2D;->a:LX/C2F;

    iget-object v0, v0, LX/C2F;->w:LX/3J0;

    invoke-interface {v0, p2}, LX/3J0;->a(LX/7Jv;)V

    .line 1843992
    :cond_1
    iget-object v0, p0, LX/C2D;->a:LX/C2F;

    iget-object v0, v0, LX/C2F;->x:LX/1Yd;

    invoke-virtual {v0}, LX/1Yd;->a()V

    .line 1843993
    return-void
.end method
