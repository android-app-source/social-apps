.class public LX/BzD;
.super Ljava/lang/Object;
.source ""


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1838387
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1838388
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 4

    .prologue
    .line 1838389
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->l()LX/0Px;

    move-result-object v2

    .line 1838390
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    .line 1838391
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAttachmentProperty;

    .line 1838392
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAttachmentProperty;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1838393
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAttachmentProperty;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    .line 1838394
    :goto_1
    return-object v0

    .line 1838395
    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 1838396
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
