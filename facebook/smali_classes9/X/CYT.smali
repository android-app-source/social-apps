.class public LX/CYT;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/CYT;


# instance fields
.field public final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/CYS;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1911224
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1911225
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/CYT;->a:Ljava/util/Map;

    .line 1911226
    return-void
.end method

.method public static a(LX/0QB;)LX/CYT;
    .locals 3

    .prologue
    .line 1911212
    sget-object v0, LX/CYT;->b:LX/CYT;

    if-nez v0, :cond_1

    .line 1911213
    const-class v1, LX/CYT;

    monitor-enter v1

    .line 1911214
    :try_start_0
    sget-object v0, LX/CYT;->b:LX/CYT;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1911215
    if-eqz v2, :cond_0

    .line 1911216
    :try_start_1
    new-instance v0, LX/CYT;

    invoke-direct {v0}, LX/CYT;-><init>()V

    .line 1911217
    move-object v0, v0

    .line 1911218
    sput-object v0, LX/CYT;->b:LX/CYT;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1911219
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1911220
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1911221
    :cond_1
    sget-object v0, LX/CYT;->b:LX/CYT;

    return-object v0

    .line 1911222
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1911223
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1911209
    iget-object v0, p0, LX/CYT;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1911210
    :goto_0
    return-void

    .line 1911211
    :cond_0
    iget-object v0, p0, LX/CYT;->a:Ljava/util/Map;

    new-instance v1, LX/CYS;

    invoke-direct {v1}, LX/CYS;-><init>()V

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1911204
    iget-object v0, p0, LX/CYT;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1911205
    iget-object v0, p0, LX/CYT;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CYS;

    .line 1911206
    const/4 v1, 0x1

    .line 1911207
    iput-boolean v1, v0, LX/CYS;->a:Z

    .line 1911208
    :cond_0
    return-void
.end method

.method public final c(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1911227
    iget-object v0, p0, LX/CYT;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/CYT;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CYS;

    iget-boolean v0, v0, LX/CYS;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1911199
    iget-object v0, p0, LX/CYT;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1911200
    iget-object v0, p0, LX/CYT;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CYS;

    .line 1911201
    const/4 v1, 0x1

    .line 1911202
    iput-boolean v1, v0, LX/CYS;->b:Z

    .line 1911203
    :cond_0
    return-void
.end method

.method public final e(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1911194
    iget-object v0, p0, LX/CYT;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1911195
    iget-object v0, p0, LX/CYT;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CYS;

    .line 1911196
    const/4 v1, 0x1

    .line 1911197
    iput-boolean v1, v0, LX/CYS;->c:Z

    .line 1911198
    :cond_0
    return-void
.end method

.method public final f(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1911192
    iget-object v0, p0, LX/CYT;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/CYT;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CYS;

    iget-boolean v0, v0, LX/CYS;->c:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1911193
    iget-object v0, p0, LX/CYT;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/CYT;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CYS;

    iget-boolean v0, v0, LX/CYS;->b:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
