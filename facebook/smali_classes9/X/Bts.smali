.class public final LX/Bts;
.super LX/2oa;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2oa",
        "<",
        "LX/2ou;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/feed/video/fullscreen/ClickToPlayNoPausePlugin;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/video/fullscreen/ClickToPlayNoPausePlugin;)V
    .locals 0

    .prologue
    .line 1829553
    iput-object p1, p0, LX/Bts;->a:Lcom/facebook/feed/video/fullscreen/ClickToPlayNoPausePlugin;

    invoke-direct {p0}, LX/2oa;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/2ou;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1829554
    const-class v0, LX/2ou;

    return-object v0
.end method

.method public final b(LX/0b7;)V
    .locals 3

    .prologue
    .line 1829555
    check-cast p1, LX/2ou;

    .line 1829556
    iget-object v0, p0, LX/Bts;->a:Lcom/facebook/feed/video/fullscreen/ClickToPlayNoPausePlugin;

    iget-object v0, v0, Lcom/facebook/feed/video/fullscreen/ClickToPlayNoPausePlugin;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Bts;->a:Lcom/facebook/feed/video/fullscreen/ClickToPlayNoPausePlugin;

    iget-object v0, v0, Lcom/facebook/feed/video/fullscreen/ClickToPlayNoPausePlugin;->c:Ljava/lang/String;

    iget-object v1, p1, LX/2ou;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1829557
    :cond_0
    :goto_0
    return-void

    .line 1829558
    :cond_1
    iget-object v0, p1, LX/2ou;->b:LX/2qV;

    sget-object v1, LX/2qV;->PAUSED:LX/2qV;

    if-ne v0, v1, :cond_2

    .line 1829559
    iget-object v0, p0, LX/Bts;->a:Lcom/facebook/feed/video/fullscreen/ClickToPlayNoPausePlugin;

    const v1, 0x7f020bbf

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/facebook/feed/video/fullscreen/ClickToPlayNoPausePlugin;->a(ILandroid/animation/Animator$AnimatorListener;)V

    goto :goto_0

    .line 1829560
    :cond_2
    iget-object v0, p1, LX/2ou;->b:LX/2qV;

    sget-object v1, LX/2qV;->PLAYBACK_COMPLETE:LX/2qV;

    if-ne v0, v1, :cond_3

    .line 1829561
    iget-object v0, p0, LX/Bts;->a:Lcom/facebook/feed/video/fullscreen/ClickToPlayNoPausePlugin;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/facebook/feed/video/fullscreen/ClickToPlayNoPausePlugin;->e:Z

    goto :goto_0

    .line 1829562
    :cond_3
    iget-object v0, p0, LX/Bts;->a:Lcom/facebook/feed/video/fullscreen/ClickToPlayNoPausePlugin;

    iget v0, v0, Lcom/facebook/feed/video/fullscreen/ClickToPlayNoPausePlugin;->b:I

    const v1, 0x7f020bbf

    if-ne v0, v1, :cond_0

    .line 1829563
    iget-object v0, p0, LX/Bts;->a:Lcom/facebook/feed/video/fullscreen/ClickToPlayNoPausePlugin;

    invoke-virtual {v0}, Lcom/facebook/feed/video/fullscreen/ClickToPlayNoPausePlugin;->h()V

    goto :goto_0
.end method
