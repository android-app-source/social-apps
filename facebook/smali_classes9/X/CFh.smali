.class public final LX/CFh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/CFf;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/CFf;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 1863909
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1863910
    iput-object p1, p0, LX/CFh;->a:LX/0QB;

    .line 1863911
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1863908
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/CFh;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 12

    .prologue
    .line 1863900
    packed-switch p2, :pswitch_data_0

    .line 1863901
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1863902
    :pswitch_0
    new-instance v1, LX/CFg;

    invoke-static {p1}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-direct {v1, v0}, LX/CFg;-><init>(LX/0Zb;)V

    .line 1863903
    move-object v0, v1

    .line 1863904
    :goto_0
    return-object v0

    .line 1863905
    :pswitch_1
    new-instance v2, LX/JbE;

    invoke-static {p1}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {p1}, LX/HYg;->b(LX/0QB;)LX/HYg;

    move-result-object v4

    check-cast v4, LX/HYg;

    invoke-static {p1}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v5

    check-cast v5, Lcom/facebook/content/SecureContextHelper;

    const/16 v6, 0xb3e

    invoke-static {p1, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x2e3

    invoke-static {p1, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static {p1}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v8

    check-cast v8, LX/0lC;

    invoke-static {p1}, LX/0Uq;->a(LX/0QB;)LX/0Uq;

    move-result-object v9

    check-cast v9, LX/0Uq;

    invoke-static {p1}, LX/2Af;->b(LX/0QB;)LX/2Af;

    move-result-object v10

    check-cast v10, LX/2Af;

    const-class v11, Landroid/content/Context;

    invoke-interface {p1, v11}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/content/Context;

    invoke-direct/range {v2 .. v11}, LX/JbE;-><init>(LX/0Zb;LX/HYg;Lcom/facebook/content/SecureContextHelper;LX/0Ot;LX/0Ot;LX/0lC;LX/0Uq;LX/2Af;Landroid/content/Context;)V

    .line 1863906
    move-object v0, v2

    .line 1863907
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 1863899
    const/4 v0, 0x2

    return v0
.end method
