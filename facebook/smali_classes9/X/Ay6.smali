.class public final LX/Ay6;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Pz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Pz",
            "<",
            "Lcom/facebook/friendsharing/souvenirs/models/SouvenirItem;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1729436
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1729437
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    iput-object v0, p0, LX/Ay6;->a:LX/0Pz;

    .line 1729438
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/friendsharing/souvenirs/models/SouvenirItem;)LX/Ay6;
    .locals 1

    .prologue
    .line 1729439
    iget-object v0, p0, LX/Ay6;->a:LX/0Pz;

    invoke-virtual {v0, p1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1729440
    return-object p0
.end method

.method public final a()Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1729441
    iget-object v0, p0, LX/Ay6;->a:LX/0Pz;

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    .line 1729442
    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1729443
    new-instance v3, Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;

    iget-object v0, p0, LX/Ay6;->b:Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;

    invoke-direct {v3, v0, v2}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;-><init>(Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;LX/0Px;)V

    return-object v3

    :cond_0
    move v0, v1

    .line 1729444
    goto :goto_0
.end method
