.class public LX/BQN;
.super Landroid/preference/PreferenceCategory;
.source ""


# instance fields
.field public a:LX/17W;

.field public b:LX/1mR;

.field public c:Ljava/util/concurrent/Executor;

.field private d:Landroid/widget/Toast;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 1782024
    invoke-direct {p0, p1}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 1782025
    const/4 v0, 0x0

    iput-object v0, p0, LX/BQN;->d:Landroid/widget/Toast;

    .line 1782026
    invoke-virtual {p0}, Landroid/preference/PreferenceGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, LX/BQN;

    invoke-static {v0}, LX/1mR;->a(LX/0QB;)LX/1mR;

    move-result-object v2

    check-cast v2, LX/1mR;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object p1

    check-cast p1, LX/17W;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v2, p0, LX/BQN;->b:LX/1mR;

    iput-object p1, p0, LX/BQN;->a:LX/17W;

    iput-object v0, p0, LX/BQN;->c:Ljava/util/concurrent/Executor;

    .line 1782027
    return-void
.end method

.method public static a$redex0(LX/BQN;Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1782053
    iget-object v0, p0, LX/BQN;->d:Landroid/widget/Toast;

    if-eqz v0, :cond_0

    .line 1782054
    iget-object v0, p0, LX/BQN;->d:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    .line 1782055
    :cond_0
    const/4 v0, 0x0

    invoke-static {p1, p2, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, LX/BQN;->d:Landroid/widget/Toast;

    .line 1782056
    iget-object v0, p0, LX/BQN;->d:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1782057
    return-void
.end method


# virtual methods
.method public final onAttachedToHierarchy(Landroid/preference/PreferenceManager;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1782028
    invoke-super {p0, p1}, Landroid/preference/PreferenceCategory;->onAttachedToHierarchy(Landroid/preference/PreferenceManager;)V

    .line 1782029
    invoke-virtual {p0}, LX/BQN;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1782030
    const-string v1, "Timeline - internal"

    invoke-virtual {p0, v1}, LX/BQN;->setTitle(Ljava/lang/CharSequence;)V

    .line 1782031
    new-instance v1, Landroid/preference/Preference;

    invoke-direct {v1, v0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 1782032
    const-string v2, "Clear Feed Cache"

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 1782033
    const-string v2, "Clear cache across newsfeed and my timeline"

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 1782034
    new-instance v2, LX/BQI;

    invoke-direct {v2, p0, v0}, LX/BQI;-><init>(LX/BQN;Landroid/content/Context;)V

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 1782035
    invoke-virtual {p0, v1}, LX/BQN;->addPreference(Landroid/preference/Preference;)Z

    .line 1782036
    new-instance v1, LX/4ok;

    invoke-direct {v1, v0}, LX/4ok;-><init>(Landroid/content/Context;)V

    .line 1782037
    sget-object v2, LX/BQO;->a:LX/0Tn;

    invoke-virtual {v1, v2}, LX/4oi;->a(LX/0Tn;)V

    .line 1782038
    const-string v2, "Pause Updates"

    invoke-virtual {v1, v2}, LX/4ok;->setTitle(Ljava/lang/CharSequence;)V

    .line 1782039
    const-string v2, "Disables fetch of new Timeline units (for scroll perf integration test)"

    invoke-virtual {v1, v2}, LX/4ok;->setSummary(Ljava/lang/CharSequence;)V

    .line 1782040
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/4ok;->setDefaultValue(Ljava/lang/Object;)V

    .line 1782041
    invoke-virtual {p0, v1}, LX/BQN;->addPreference(Landroid/preference/Preference;)Z

    .line 1782042
    new-instance v1, Landroid/preference/Preference;

    invoke-virtual {p0}, LX/BQN;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 1782043
    const-string v2, "Go to timeline"

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 1782044
    const-string v2, "Opens user timeline by uid"

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 1782045
    new-instance v2, LX/BQM;

    invoke-direct {v2, p0}, LX/BQM;-><init>(LX/BQN;)V

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 1782046
    invoke-virtual {p0, v1}, LX/BQN;->addPreference(Landroid/preference/Preference;)Z

    .line 1782047
    new-instance v1, LX/4ok;

    invoke-direct {v1, v0}, LX/4ok;-><init>(Landroid/content/Context;)V

    .line 1782048
    sget-object v0, LX/BQO;->c:LX/0Tn;

    invoke-virtual {v1, v0}, LX/4oi;->a(LX/0Tn;)V

    .line 1782049
    const-string v0, "Show debug overlay"

    invoke-virtual {v1, v0}, LX/4ok;->setTitle(Ljava/lang/CharSequence;)V

    .line 1782050
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/4ok;->setDefaultValue(Ljava/lang/Object;)V

    .line 1782051
    invoke-virtual {p0, v1}, LX/BQN;->addPreference(Landroid/preference/Preference;)Z

    .line 1782052
    return-void
.end method
