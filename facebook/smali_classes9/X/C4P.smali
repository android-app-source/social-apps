.class public LX/C4P;
.super LX/0ht;
.source ""


# instance fields
.field private final a:I

.field private l:Landroid/animation/AnimatorSet;


# virtual methods
.method public final a(Landroid/view/View;ZLandroid/view/WindowManager$LayoutParams;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1847230
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    .line 1847231
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v1

    .line 1847232
    const/4 v2, 0x2

    new-array v2, v2, [I

    .line 1847233
    invoke-virtual {p1, v2}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 1847234
    iget v3, p0, LX/C4P;->a:I

    mul-int/lit8 v3, v3, 0x2

    iput v3, p3, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 1847235
    iget v3, p0, LX/C4P;->a:I

    mul-int/lit8 v3, v3, 0x2

    iput v3, p3, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 1847236
    aget v3, v2, v5

    iget v4, p0, LX/C4P;->a:I

    sub-int/2addr v3, v4

    div-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v3

    iput v0, p3, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 1847237
    const/4 v0, 0x1

    aget v0, v2, v0

    iget v2, p0, LX/C4P;->a:I

    sub-int/2addr v0, v2

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    iput v0, p3, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 1847238
    const/16 v0, 0x33

    iput v0, p3, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 1847239
    iget-object v0, p0, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    sget-object v1, LX/5OS;->NONE:LX/5OS;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->setNubShown(LX/5OS;)V

    .line 1847240
    iget-object v0, p0, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    invoke-virtual {v0}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1847241
    iput v5, p3, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    .line 1847242
    :cond_0
    iget v0, p3, Landroid/view/WindowManager$LayoutParams;->flags:I

    or-int/lit8 v0, v0, 0x38

    iput v0, p3, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 1847243
    return-void
.end method

.method public final l()V
    .locals 1

    .prologue
    .line 1847244
    invoke-super {p0}, LX/0ht;->l()V

    .line 1847245
    iget-object v0, p0, LX/C4P;->l:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    .line 1847246
    return-void
.end method
