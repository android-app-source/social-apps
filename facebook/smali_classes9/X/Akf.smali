.class public final LX/Akf;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic c:Landroid/content/Context;

.field public final synthetic d:LX/1SX;


# direct methods
.method public constructor <init>(LX/1SX;ZLcom/facebook/feed/rows/core/props/FeedProps;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1709249
    iput-object p1, p0, LX/Akf;->d:LX/1SX;

    iput-boolean p2, p0, LX/Akf;->a:Z

    iput-object p3, p0, LX/Akf;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iput-object p4, p0, LX/Akf;->c:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5

    .prologue
    .line 1709250
    iget-boolean v0, p0, LX/Akf;->a:Z

    if-eqz v0, :cond_0

    .line 1709251
    iget-object v0, p0, LX/Akf;->d:LX/1SX;

    iget-object v1, p0, LX/Akf;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p0, LX/Akf;->c:Landroid/content/Context;

    .line 1709252
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 1709253
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1709254
    iget-object v4, v0, LX/1SX;->d:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/9LP;

    sget-object p0, LX/6Vy;->DELETE_INTERCEPT:LX/6Vy;

    invoke-virtual {v4, v3, p0}, LX/9LP;->a(Lcom/facebook/graphql/model/GraphQLStory;LX/6Vy;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    .line 1709255
    iget-object p0, v0, LX/1SX;->c:LX/1Ck;

    new-instance p1, Ljava/lang/StringBuilder;

    const-string p2, "toggle_availability"

    invoke-direct {p1, p2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance p1, LX/Akg;

    invoke-direct {p1, v0, v1, v2}, LX/Akg;-><init>(LX/1SX;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/content/Context;)V

    invoke-virtual {p0, v3, v4, p1}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1709256
    :goto_0
    return-void

    .line 1709257
    :cond_0
    iget-object v0, p0, LX/Akf;->d:LX/1SX;

    iget-object v0, v0, LX/1SX;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9LP;

    iget-object v1, p0, LX/Akf;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    sget-object v2, LX/6Vy;->DELETE_INTERCEPT:LX/6Vy;

    invoke-virtual {v0, v1, v2}, LX/9LP;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/6Vy;)V

    goto :goto_0
.end method
