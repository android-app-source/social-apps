.class public final enum LX/CGQ;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements LX/CEj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/CGQ;",
        ">;",
        "LX/CEj;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/CGQ;

.field public static final enum BUTTON:LX/CGQ;

.field public static final enum GROUP:LX/CGQ;

.field public static final enum LABEL:LX/CGQ;

.field public static final enum MEDIA_WITH_IMAGE:LX/CGQ;

.field public static final enum MEDIA_WITH_VIDEO:LX/CGQ;

.field public static final enum PLACEHOLDER:LX/CGQ;

.field public static final enum RECT:LX/CGQ;

.field public static final enum SEQUENCE:LX/CGQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1865484
    new-instance v0, LX/CGQ;

    const-string v1, "LABEL"

    invoke-direct {v0, v1, v3}, LX/CGQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CGQ;->LABEL:LX/CGQ;

    .line 1865485
    new-instance v0, LX/CGQ;

    const-string v1, "BUTTON"

    invoke-direct {v0, v1, v4}, LX/CGQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CGQ;->BUTTON:LX/CGQ;

    .line 1865486
    new-instance v0, LX/CGQ;

    const-string v1, "SEQUENCE"

    invoke-direct {v0, v1, v5}, LX/CGQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CGQ;->SEQUENCE:LX/CGQ;

    .line 1865487
    new-instance v0, LX/CGQ;

    const-string v1, "GROUP"

    invoke-direct {v0, v1, v6}, LX/CGQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CGQ;->GROUP:LX/CGQ;

    .line 1865488
    new-instance v0, LX/CGQ;

    const-string v1, "RECT"

    invoke-direct {v0, v1, v7}, LX/CGQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CGQ;->RECT:LX/CGQ;

    .line 1865489
    new-instance v0, LX/CGQ;

    const-string v1, "PLACEHOLDER"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/CGQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CGQ;->PLACEHOLDER:LX/CGQ;

    .line 1865490
    new-instance v0, LX/CGQ;

    const-string v1, "MEDIA_WITH_IMAGE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/CGQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CGQ;->MEDIA_WITH_IMAGE:LX/CGQ;

    .line 1865491
    new-instance v0, LX/CGQ;

    const-string v1, "MEDIA_WITH_VIDEO"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/CGQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CGQ;->MEDIA_WITH_VIDEO:LX/CGQ;

    .line 1865492
    const/16 v0, 0x8

    new-array v0, v0, [LX/CGQ;

    sget-object v1, LX/CGQ;->LABEL:LX/CGQ;

    aput-object v1, v0, v3

    sget-object v1, LX/CGQ;->BUTTON:LX/CGQ;

    aput-object v1, v0, v4

    sget-object v1, LX/CGQ;->SEQUENCE:LX/CGQ;

    aput-object v1, v0, v5

    sget-object v1, LX/CGQ;->GROUP:LX/CGQ;

    aput-object v1, v0, v6

    sget-object v1, LX/CGQ;->RECT:LX/CGQ;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/CGQ;->PLACEHOLDER:LX/CGQ;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/CGQ;->MEDIA_WITH_IMAGE:LX/CGQ;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/CGQ;->MEDIA_WITH_VIDEO:LX/CGQ;

    aput-object v2, v0, v1

    sput-object v0, LX/CGQ;->$VALUES:[LX/CGQ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1865481
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/CGQ;
    .locals 1

    .prologue
    .line 1865482
    const-class v0, LX/CGQ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/CGQ;

    return-object v0
.end method

.method public static values()[LX/CGQ;
    .locals 1

    .prologue
    .line 1865483
    sget-object v0, LX/CGQ;->$VALUES:[LX/CGQ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/CGQ;

    return-object v0
.end method
