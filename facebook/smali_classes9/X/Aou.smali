.class public final enum LX/Aou;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Aou;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Aou;

.field public static final enum CONTACT_SHARE:LX/Aou;

.field public static final enum COPY_LINK:LX/Aou;

.field public static final enum SEND_AS_MESSAGE:LX/Aou;

.field public static final enum SHARE_EXTERNAL:LX/Aou;

.field public static final enum SHARE_NOW:LX/Aou;

.field public static final enum SHARE_TO_PAGE:LX/Aou;

.field public static final enum WRITE_POST:LX/Aou;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1714968
    new-instance v0, LX/Aou;

    const-string v1, "SHARE_NOW"

    invoke-direct {v0, v1, v3}, LX/Aou;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Aou;->SHARE_NOW:LX/Aou;

    .line 1714969
    new-instance v0, LX/Aou;

    const-string v1, "WRITE_POST"

    invoke-direct {v0, v1, v4}, LX/Aou;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Aou;->WRITE_POST:LX/Aou;

    .line 1714970
    new-instance v0, LX/Aou;

    const-string v1, "SEND_AS_MESSAGE"

    invoke-direct {v0, v1, v5}, LX/Aou;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Aou;->SEND_AS_MESSAGE:LX/Aou;

    .line 1714971
    new-instance v0, LX/Aou;

    const-string v1, "SHARE_EXTERNAL"

    invoke-direct {v0, v1, v6}, LX/Aou;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Aou;->SHARE_EXTERNAL:LX/Aou;

    .line 1714972
    new-instance v0, LX/Aou;

    const-string v1, "COPY_LINK"

    invoke-direct {v0, v1, v7}, LX/Aou;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Aou;->COPY_LINK:LX/Aou;

    .line 1714973
    new-instance v0, LX/Aou;

    const-string v1, "CONTACT_SHARE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/Aou;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Aou;->CONTACT_SHARE:LX/Aou;

    .line 1714974
    new-instance v0, LX/Aou;

    const-string v1, "SHARE_TO_PAGE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/Aou;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Aou;->SHARE_TO_PAGE:LX/Aou;

    .line 1714975
    const/4 v0, 0x7

    new-array v0, v0, [LX/Aou;

    sget-object v1, LX/Aou;->SHARE_NOW:LX/Aou;

    aput-object v1, v0, v3

    sget-object v1, LX/Aou;->WRITE_POST:LX/Aou;

    aput-object v1, v0, v4

    sget-object v1, LX/Aou;->SEND_AS_MESSAGE:LX/Aou;

    aput-object v1, v0, v5

    sget-object v1, LX/Aou;->SHARE_EXTERNAL:LX/Aou;

    aput-object v1, v0, v6

    sget-object v1, LX/Aou;->COPY_LINK:LX/Aou;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/Aou;->CONTACT_SHARE:LX/Aou;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/Aou;->SHARE_TO_PAGE:LX/Aou;

    aput-object v2, v0, v1

    sput-object v0, LX/Aou;->$VALUES:[LX/Aou;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1714966
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Aou;
    .locals 1

    .prologue
    .line 1714967
    const-class v0, LX/Aou;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Aou;

    return-object v0
.end method

.method public static values()[LX/Aou;
    .locals 1

    .prologue
    .line 1714965
    sget-object v0, LX/Aou;->$VALUES:[LX/Aou;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Aou;

    return-object v0
.end method
