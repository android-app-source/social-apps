.class public LX/AWy;
.super LX/AWT;
.source ""


# static fields
.field private static final r:Ljava/lang/String;


# instance fields
.field public A:LX/AWx;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public B:I

.field private C:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

.field public D:Lcom/facebook/facecast/model/FacecastPrivacyData;

.field public E:I

.field public F:Z

.field public G:Z

.field public H:Ljava/lang/String;

.field public I:Ljava/lang/String;

.field private final J:LX/93q;

.field public a:LX/AWf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/8RJ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/privacy/PrivacyOperationsClient;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Sh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/Ajc;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/93j;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/93n;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/93d;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:Landroid/view/inputmethod/InputMethodManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/8Sa;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/1b4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:Lcom/facebook/auth/viewercontext/ViewerContext;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/AbS;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private t:LX/8zA;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final u:Landroid/view/View;

.field public final v:Lcom/facebook/privacy/ui/PrivacyOptionView;

.field public final w:Lcom/facebook/facecast/plugin/boostpost/BoostPostOptionView;

.field public final x:Lcom/facebook/widget/text/BetterEditTextView;

.field public final y:Landroid/widget/LinearLayout;

.field public final z:Landroid/widget/RelativeLayout;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1683605
    const-class v0, LX/AWy;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/AWy;->r:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1683603
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/AWy;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1683604
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1683601
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/AWy;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1683602
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/annotation/TargetApi;
        value = 0x12
    .end annotation

    .prologue
    .line 1683580
    invoke-direct {p0, p1, p2, p3}, LX/AWT;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1683581
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1683582
    iput-object v0, p0, LX/AWy;->s:LX/0Ot;

    .line 1683583
    new-instance v0, LX/AWS;

    invoke-direct {v0}, LX/AWS;-><init>()V

    invoke-virtual {v0}, LX/AWS;->a()Lcom/facebook/facecast/model/FacecastPrivacyData;

    move-result-object v0

    iput-object v0, p0, LX/AWy;->D:Lcom/facebook/facecast/model/FacecastPrivacyData;

    .line 1683584
    const/4 v0, -0x1

    iput v0, p0, LX/AWy;->E:I

    .line 1683585
    new-instance v0, LX/AWr;

    invoke-direct {v0, p0}, LX/AWr;-><init>(LX/AWy;)V

    iput-object v0, p0, LX/AWy;->J:LX/93q;

    .line 1683586
    const v0, 0x7f0305cd

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1683587
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p0, v1}, LX/AWy;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1683588
    const v0, 0x7f0d0fed

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/AWy;->u:Landroid/view/View;

    .line 1683589
    const v0, 0x7f0d0ff3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/ui/PrivacyOptionView;

    iput-object v0, p0, LX/AWy;->v:Lcom/facebook/privacy/ui/PrivacyOptionView;

    .line 1683590
    const v0, 0x7f0d0ff5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecast/plugin/boostpost/BoostPostOptionView;

    iput-object v0, p0, LX/AWy;->w:Lcom/facebook/facecast/plugin/boostpost/BoostPostOptionView;

    .line 1683591
    const v0, 0x7f0d0ff6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterEditTextView;

    iput-object v0, p0, LX/AWy;->x:Lcom/facebook/widget/text/BetterEditTextView;

    .line 1683592
    const v0, 0x7f0d0fee

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/AWy;->y:Landroid/widget/LinearLayout;

    .line 1683593
    const v0, 0x7f0d0fef

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, LX/AWy;->z:Landroid/widget/RelativeLayout;

    .line 1683594
    iget-object v0, p0, LX/AWy;->p:LX/1b4;

    iget-object v1, p0, LX/AWy;->q:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1683595
    iget-boolean p1, v1, Lcom/facebook/auth/viewercontext/ViewerContext;->d:Z

    move v1, p1

    .line 1683596
    invoke-virtual {v0, v1}, LX/1b4;->a(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1683597
    const v0, 0x7f0d0fec

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0}, LX/AWy;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1}, LX/Ac1;->b(Landroid/content/res/Resources;)LX/Ac1;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1683598
    :cond_0
    iget-object v0, p0, LX/AWy;->x:Lcom/facebook/widget/text/BetterEditTextView;

    new-instance v1, Landroid/text/method/ScrollingMovementMethod;

    invoke-direct {v1}, Landroid/text/method/ScrollingMovementMethod;-><init>()V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterEditTextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 1683599
    iget-object v0, p0, LX/AWy;->y:Landroid/widget/LinearLayout;

    new-instance v1, LX/AWp;

    invoke-direct {v1, p0}, LX/AWp;-><init>(LX/AWy;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 1683600
    return-void
.end method

.method private static a(LX/AWy;LX/AWf;LX/8RJ;Lcom/facebook/privacy/PrivacyOperationsClient;LX/0Sh;LX/Ajc;LX/93j;LX/93n;LX/03V;LX/93d;Landroid/view/inputmethod/InputMethodManager;LX/8Sa;LX/0ad;Lcom/facebook/content/SecureContextHelper;LX/0Ot;LX/8zA;LX/1b4;Lcom/facebook/auth/viewercontext/ViewerContext;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/AWy;",
            "LX/AWf;",
            "LX/8RJ;",
            "Lcom/facebook/privacy/PrivacyOperationsClient;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/Ajc;",
            "LX/93j;",
            "LX/93n;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/93d;",
            "Landroid/view/inputmethod/InputMethodManager;",
            "LX/8Sa;",
            "LX/0ad;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/0Ot",
            "<",
            "LX/AbS;",
            ">;",
            "LX/8zA;",
            "LX/1b4;",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1683579
    iput-object p1, p0, LX/AWy;->a:LX/AWf;

    iput-object p2, p0, LX/AWy;->b:LX/8RJ;

    iput-object p3, p0, LX/AWy;->c:Lcom/facebook/privacy/PrivacyOperationsClient;

    iput-object p4, p0, LX/AWy;->f:LX/0Sh;

    iput-object p5, p0, LX/AWy;->g:LX/Ajc;

    iput-object p6, p0, LX/AWy;->h:LX/93j;

    iput-object p7, p0, LX/AWy;->i:LX/93n;

    iput-object p8, p0, LX/AWy;->j:LX/03V;

    iput-object p9, p0, LX/AWy;->k:LX/93d;

    iput-object p10, p0, LX/AWy;->l:Landroid/view/inputmethod/InputMethodManager;

    iput-object p11, p0, LX/AWy;->m:LX/8Sa;

    iput-object p12, p0, LX/AWy;->n:LX/0ad;

    iput-object p13, p0, LX/AWy;->o:Lcom/facebook/content/SecureContextHelper;

    iput-object p14, p0, LX/AWy;->s:LX/0Ot;

    move-object/from16 v0, p15

    iput-object v0, p0, LX/AWy;->t:LX/8zA;

    move-object/from16 v0, p16

    iput-object v0, p0, LX/AWy;->p:LX/1b4;

    move-object/from16 v0, p17

    iput-object v0, p0, LX/AWy;->q:Lcom/facebook/auth/viewercontext/ViewerContext;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 19

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v18

    move-object/from16 v1, p0

    check-cast v1, LX/AWy;

    invoke-static/range {v18 .. v18}, LX/AWf;->a(LX/0QB;)LX/AWf;

    move-result-object v2

    check-cast v2, LX/AWf;

    invoke-static/range {v18 .. v18}, LX/8RJ;->a(LX/0QB;)LX/8RJ;

    move-result-object v3

    check-cast v3, LX/8RJ;

    invoke-static/range {v18 .. v18}, Lcom/facebook/privacy/PrivacyOperationsClient;->a(LX/0QB;)Lcom/facebook/privacy/PrivacyOperationsClient;

    move-result-object v4

    check-cast v4, Lcom/facebook/privacy/PrivacyOperationsClient;

    invoke-static/range {v18 .. v18}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v5

    check-cast v5, LX/0Sh;

    const-class v6, LX/Ajc;

    move-object/from16 v0, v18

    invoke-interface {v0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/Ajc;

    const-class v7, LX/93j;

    move-object/from16 v0, v18

    invoke-interface {v0, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/93j;

    const-class v8, LX/93n;

    move-object/from16 v0, v18

    invoke-interface {v0, v8}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/93n;

    invoke-static/range {v18 .. v18}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v9

    check-cast v9, LX/03V;

    const-class v10, LX/93d;

    move-object/from16 v0, v18

    invoke-interface {v0, v10}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v10

    check-cast v10, LX/93d;

    invoke-static/range {v18 .. v18}, LX/10d;->a(LX/0QB;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v11

    check-cast v11, Landroid/view/inputmethod/InputMethodManager;

    invoke-static/range {v18 .. v18}, LX/8Sa;->a(LX/0QB;)LX/8Sa;

    move-result-object v12

    check-cast v12, LX/8Sa;

    invoke-static/range {v18 .. v18}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v13

    check-cast v13, LX/0ad;

    invoke-static/range {v18 .. v18}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v14

    check-cast v14, Lcom/facebook/content/SecureContextHelper;

    const/16 v15, 0x1bf0

    move-object/from16 v0, v18

    invoke-static {v0, v15}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v15

    invoke-static/range {v18 .. v18}, LX/8zA;->a(LX/0QB;)LX/8zA;

    move-result-object v16

    check-cast v16, LX/8zA;

    invoke-static/range {v18 .. v18}, LX/1b4;->a(LX/0QB;)LX/1b4;

    move-result-object v17

    check-cast v17, LX/1b4;

    invoke-static/range {v18 .. v18}, LX/0eQ;->a(LX/0QB;)Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v18

    check-cast v18, Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-static/range {v1 .. v18}, LX/AWy;->a(LX/AWy;LX/AWf;LX/8RJ;Lcom/facebook/privacy/PrivacyOperationsClient;LX/0Sh;LX/Ajc;LX/93j;LX/93n;LX/03V;LX/93d;Landroid/view/inputmethod/InputMethodManager;LX/8Sa;LX/0ad;Lcom/facebook/content/SecureContextHelper;LX/0Ot;LX/8zA;LX/1b4;Lcom/facebook/auth/viewercontext/ViewerContext;)V

    return-void
.end method

.method public static b(Landroid/view/View;)I
    .locals 2

    .prologue
    .line 1683577
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1683578
    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v0, v1

    return v0
.end method

.method public static j(LX/AWy;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 1683551
    const v0, 0x7f0d0ff0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/tiles/UserTileView;

    .line 1683552
    const v1, 0x7f0d0ff1

    invoke-virtual {p0, v1}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/text/BetterTextView;

    .line 1683553
    iget-object v2, p0, LX/AWy;->a:LX/AWf;

    iget-object v3, p0, LX/AWy;->C:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    invoke-virtual {p0}, LX/AWy;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b0585

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, LX/AWf;->a(Lcom/facebook/ipc/composer/intent/ComposerTargetData;IZ)LX/8t9;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/user/tiles/UserTileView;->setParams(LX/8t9;)V

    .line 1683554
    const/4 v0, 0x0

    .line 1683555
    iget-object v2, p0, LX/AWy;->H:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 1683556
    new-instance v0, LX/5m9;

    invoke-direct {v0}, LX/5m9;-><init>()V

    iget-object v2, p0, LX/AWy;->H:Ljava/lang/String;

    .line 1683557
    iput-object v2, v0, LX/5m9;->f:Ljava/lang/String;

    .line 1683558
    move-object v0, v0

    .line 1683559
    iget-object v2, p0, LX/AWy;->I:Ljava/lang/String;

    .line 1683560
    iput-object v2, v0, LX/5m9;->h:Ljava/lang/String;

    .line 1683561
    move-object v0, v0

    .line 1683562
    invoke-virtual {v0}, LX/5m9;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v0

    .line 1683563
    :cond_0
    new-instance v2, LX/8z6;

    invoke-direct {v2}, LX/8z6;-><init>()V

    .line 1683564
    iput-boolean v6, v2, LX/8z6;->f:Z

    .line 1683565
    move-object v2, v2

    .line 1683566
    iput-boolean v6, v2, LX/8z6;->g:Z

    .line 1683567
    move-object v2, v2

    .line 1683568
    iput-object v0, v2, LX/8z6;->b:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 1683569
    move-object v0, v2

    .line 1683570
    new-instance v2, Landroid/text/SpannedString;

    iget-object v3, p0, LX/AWy;->t:LX/8zA;

    invoke-virtual {v0}, LX/8z6;->a()LX/8z5;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/8zA;->a(LX/8z5;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    invoke-direct {v2, v0}, Landroid/text/SpannedString;-><init>(Ljava/lang/CharSequence;)V

    .line 1683571
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, LX/AWy;->a:LX/AWf;

    iget-object v4, p0, LX/AWy;->C:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    .line 1683572
    if-eqz v4, :cond_1

    iget-object v5, v4, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    sget-object v6, LX/2rw;->PAGE:LX/2rw;

    if-ne v5, v6, :cond_1

    .line 1683573
    iget-object v5, v4, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetName:Ljava/lang/String;

    .line 1683574
    :goto_0
    move-object v3, v5

    .line 1683575
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1683576
    return-void

    :cond_1
    iget-object v5, v3, LX/AWf;->a:Lcom/facebook/user/model/User;

    invoke-virtual {v5}, Lcom/facebook/user/model/User;->i()Ljava/lang/String;

    move-result-object v5

    goto :goto_0
.end method

.method public static k(LX/AWy;)V
    .locals 2

    .prologue
    .line 1683606
    iget-object v0, p0, LX/AWy;->v:Lcom/facebook/privacy/ui/PrivacyOptionView;

    new-instance v1, LX/AWq;

    invoke-direct {v1, p0}, LX/AWq;-><init>(LX/AWy;)V

    invoke-virtual {v0, v1}, Lcom/facebook/privacy/ui/PrivacyOptionView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1683607
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/facecast/model/FacecastPrivacyData;)V
    .locals 5
    .param p1    # Lcom/facebook/facecast/model/FacecastPrivacyData;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1683524
    if-eqz p1, :cond_1

    .line 1683525
    iput-object p1, p0, LX/AWy;->D:Lcom/facebook/facecast/model/FacecastPrivacyData;

    .line 1683526
    iget-object v0, p1, Lcom/facebook/facecast/model/FacecastPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-object v0, v0

    .line 1683527
    if-eqz v0, :cond_1

    .line 1683528
    iget-object v0, p0, LX/AWy;->v:Lcom/facebook/privacy/ui/PrivacyOptionView;

    .line 1683529
    iget-object v1, p1, Lcom/facebook/facecast/model/FacecastPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-object v1, v1

    .line 1683530
    iget-object v2, v1, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v1, v2

    .line 1683531
    invoke-virtual {v0, v1}, Lcom/facebook/privacy/ui/PrivacyOptionView;->setPrivacyOption(LX/1oT;)V

    .line 1683532
    invoke-static {p0}, LX/AWy;->k(LX/AWy;)V

    .line 1683533
    :cond_0
    :goto_0
    return-void

    .line 1683534
    :cond_1
    iget-object v0, p0, LX/AWy;->v:Lcom/facebook/privacy/ui/PrivacyOptionView;

    invoke-virtual {p0}, LX/AWy;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080024

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/privacy/ui/PrivacyOptionView;->setText(Ljava/lang/CharSequence;)V

    .line 1683535
    iget-object v0, p0, LX/AWy;->D:Lcom/facebook/facecast/model/FacecastPrivacyData;

    if-eqz v0, :cond_2

    sget-object v0, Lcom/facebook/feed/goodfriends/composer/GoodFriendsComposerPluginConfig;->a:Ljava/lang/String;

    iget-object v1, p0, LX/AWy;->D:Lcom/facebook/facecast/model/FacecastPrivacyData;

    .line 1683536
    iget-object v2, v1, Lcom/facebook/facecast/model/FacecastPrivacyData;->a:Ljava/lang/String;

    move-object v1, v2

    .line 1683537
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1683538
    iget-object v0, p0, LX/AWy;->g:LX/Ajc;

    iget-object v1, p0, LX/AWy;->J:LX/93q;

    invoke-virtual {v0, v1}, LX/Ajc;->a(LX/93q;)LX/Ajb;

    move-result-object v0

    .line 1683539
    invoke-virtual {v0}, LX/93Q;->a()V

    goto :goto_0

    .line 1683540
    :cond_2
    iget-object v0, p0, LX/AWy;->C:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    if-eqz v0, :cond_0

    .line 1683541
    sget-object v0, LX/AWu;->a:[I

    iget-object v1, p0, LX/AWy;->C:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    iget-object v1, v1, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    invoke-virtual {v1}, LX/2rw;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1683542
    iget-object v0, p0, LX/AWy;->j:LX/03V;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, LX/AWy;->r:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_updatePrivacySelector"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unsupported type "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/AWy;->C:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    iget-object v3, v3, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1683543
    :pswitch_0
    iget-object v0, p0, LX/AWy;->h:LX/93j;

    iget-object v1, p0, LX/AWy;->J:LX/93q;

    iget-object v2, p0, LX/AWy;->C:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    iget-wide v2, v2, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/93j;->a(LX/93q;Ljava/lang/Long;)LX/93i;

    move-result-object v0

    .line 1683544
    invoke-virtual {v0}, LX/93Q;->a()V

    goto/16 :goto_0

    .line 1683545
    :pswitch_1
    iget-object v0, p0, LX/AWy;->i:LX/93n;

    iget-object v1, p0, LX/AWy;->J:LX/93q;

    invoke-virtual {p0}, LX/AWy;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08131d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/AWy;->C:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    iget-object v3, v3, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, LX/93n;->a(LX/93q;Ljava/lang/String;Ljava/lang/String;)LX/93m;

    move-result-object v0

    .line 1683546
    invoke-virtual {v0}, LX/93Q;->a()V

    goto/16 :goto_0

    .line 1683547
    :pswitch_2
    iget-object v0, p0, LX/AWy;->k:LX/93d;

    iget-object v1, p0, LX/AWy;->J:LX/93q;

    iget-object v2, p0, LX/AWy;->C:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    iget-wide v2, v2, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iget-object v3, p0, LX/AWy;->C:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    iget-object v3, v3, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetName:Ljava/lang/String;

    iget-object v4, p0, LX/AWy;->C:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    iget-object v4, v4, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetPrivacy:Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    invoke-virtual {v0, v1, v2, v3, v4}, LX/93d;->a(LX/93q;Ljava/lang/Long;Ljava/lang/String;LX/2rX;)LX/93c;

    move-result-object v0

    .line 1683548
    invoke-virtual {v0}, LX/93Q;->a()V

    goto/16 :goto_0

    .line 1683549
    :pswitch_3
    iget-object v0, p0, LX/AWy;->c:Lcom/facebook/privacy/PrivacyOperationsClient;

    sget-object v1, LX/0rS;->STALE_DATA_OKAY:LX/0rS;

    invoke-virtual {v0, v1}, Lcom/facebook/privacy/PrivacyOperationsClient;->a(LX/0rS;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1683550
    iget-object v1, p0, LX/AWy;->f:LX/0Sh;

    new-instance v2, LX/AWs;

    invoke-direct {v2, p0}, LX/AWs;-><init>(LX/AWy;)V

    invoke-virtual {v1, v0, v2}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1683521
    iget-object v0, p0, LX/AWy;->x:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterEditTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1683522
    iget-object v0, p0, LX/AWy;->x:Lcom/facebook/widget/text/BetterEditTextView;

    iget-object v1, p0, LX/AWy;->x:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v1}, Lcom/facebook/widget/text/BetterEditTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterEditTextView;->setSelection(I)V

    .line 1683523
    return-void
.end method

.method public final g()V
    .locals 4

    .prologue
    .line 1683517
    iget-object v0, p0, LX/AWy;->l:Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, LX/AWy;->x:Lcom/facebook/widget/text/BetterEditTextView;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1683518
    new-instance v0, Lcom/facebook/facecast/plugin/FacecastEditTitlePlugin$6;

    invoke-direct {v0, p0}, Lcom/facebook/facecast/plugin/FacecastEditTitlePlugin$6;-><init>(LX/AWy;)V

    .line 1683519
    const-wide/16 v2, 0x64

    invoke-virtual {p0, v0, v2, v3}, LX/AWy;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1683520
    :cond_0
    return-void
.end method

.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1683516
    iget-object v0, p0, LX/AWy;->x:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterEditTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final h()V
    .locals 3

    .prologue
    .line 1683514
    iget-object v0, p0, LX/AWy;->l:Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, LX/AWy;->x:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v1}, Lcom/facebook/widget/text/BetterEditTextView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1683515
    return-void
.end method

.method public setComposerTargetData(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)V
    .locals 3
    .param p1    # Lcom/facebook/ipc/composer/intent/ComposerTargetData;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1683502
    iput-object p1, p0, LX/AWy;->C:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    .line 1683503
    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    sget-object v1, LX/2rw;->PAGE:LX/2rw;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LX/AWy;->G:Z

    .line 1683504
    invoke-static {p0}, LX/AWy;->j(LX/AWy;)V

    .line 1683505
    iget-boolean v0, p0, LX/AWy;->G:Z

    if-nez v0, :cond_1

    .line 1683506
    :goto_1
    return-void

    .line 1683507
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1683508
    :cond_1
    const v0, 0x7f0d0ff4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/internal/widget/ViewStubCompat;

    .line 1683509
    iget-object v1, p0, LX/AWy;->s:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/AbS;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ViewStubCompat;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1683510
    iput-object v0, v1, LX/AbS;->d:Landroid/widget/TextView;

    .line 1683511
    iget-object v2, v1, LX/AbS;->d:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1683512
    iget-object v2, v1, LX/AbS;->b:LX/AVT;

    const-string p1, "geotargeting_button_impression"

    const/4 p0, 0x0

    invoke-virtual {v2, p1, p0}, LX/AVT;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1683513
    goto :goto_1
.end method

.method public setFullScreen(Z)V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1683497
    iput-boolean p1, p0, LX/AWy;->F:Z

    .line 1683498
    if-eqz p1, :cond_0

    .line 1683499
    iget-object v0, p0, LX/AWy;->u:Landroid/view/View;

    new-instance v1, LX/Ac1;

    new-array v2, v7, [I

    aput v5, v2, v5

    invoke-virtual {p0}, LX/AWy;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0380

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    aput v3, v2, v6

    invoke-direct {v1, v2}, LX/Ac1;-><init>([I)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1683500
    :goto_0
    return-void

    .line 1683501
    :cond_0
    iget-object v0, p0, LX/AWy;->u:Landroid/view/View;

    new-instance v1, LX/Ac1;

    const/4 v2, 0x3

    new-array v2, v2, [I

    invoke-virtual {p0}, LX/AWy;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a037d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    aput v3, v2, v5

    invoke-virtual {p0}, LX/AWy;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a03a8

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    aput v3, v2, v6

    const/high16 v3, -0x1000000

    aput v3, v2, v7

    invoke-direct {v1, v2}, LX/Ac1;-><init>([I)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method
