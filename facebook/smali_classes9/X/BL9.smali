.class public final LX/BL9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;)V
    .locals 0

    .prologue
    .line 1775318
    iput-object p1, p0, LX/BL9;->a:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 1775321
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1775322
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 1

    .prologue
    .line 1775319
    iget-object v0, p0, LX/BL9;->a:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;

    iget-object v0, v0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;->a:Lcom/facebook/platform/composer/targetprivacy/PlatformGroupSelectorAdapter;

    invoke-virtual {v0}, Lcom/facebook/platform/composer/targetprivacy/PlatformGroupSelectorAdapter;->getFilter()Landroid/widget/Filter;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/Filter;->filter(Ljava/lang/CharSequence;)V

    .line 1775320
    return-void
.end method
