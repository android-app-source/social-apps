.class public LX/BSj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/BSi;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x12
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field public final b:Lcom/facebook/video/player/RichVideoPlayer;

.field private final c:Lcom/facebook/video/creativeediting/ui/fresco/CropGridView;

.field public final d:Landroid/view/View;

.field public e:Z


# direct methods
.method public constructor <init>(Lcom/facebook/video/player/RichVideoPlayer;Lcom/facebook/video/creativeediting/ui/fresco/CropGridView;Landroid/view/View;Landroid/content/Context;)V
    .locals 0
    .param p1    # Lcom/facebook/video/player/RichVideoPlayer;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/video/creativeediting/ui/fresco/CropGridView;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Landroid/view/View;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1785741
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1785742
    iput-object p4, p0, LX/BSj;->a:Landroid/content/Context;

    .line 1785743
    iput-object p2, p0, LX/BSj;->c:Lcom/facebook/video/creativeediting/ui/fresco/CropGridView;

    .line 1785744
    iput-object p3, p0, LX/BSj;->d:Landroid/view/View;

    .line 1785745
    iput-object p1, p0, LX/BSj;->b:Lcom/facebook/video/player/RichVideoPlayer;

    .line 1785746
    return-void
.end method

.method public static a(FF)Landroid/util/Pair;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(FF)",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Float;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/high16 v0, 0x3f800000    # 1.0f

    .line 1785735
    cmpg-float v2, p0, v1

    if-gez v2, :cond_1

    .line 1785736
    sub-float/2addr p1, p0

    move p0, v1

    .line 1785737
    :cond_0
    :goto_0
    new-instance v0, Landroid/util/Pair;

    invoke-static {p0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0

    .line 1785738
    :cond_1
    cmpl-float v1, p1, v0

    if-lez v1, :cond_0

    .line 1785739
    sub-float v1, p1, v0

    sub-float/2addr p0, v1

    move p1, v0

    .line 1785740
    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1785734
    iget-object v0, p0, LX/BSj;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0813e3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 1785730
    iget-object v0, p0, LX/BSj;->b:Lcom/facebook/video/player/RichVideoPlayer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1785731
    iget-object v0, p0, LX/BSj;->c:Lcom/facebook/video/creativeediting/ui/fresco/CropGridView;

    invoke-virtual {v0, v2}, Lcom/facebook/video/creativeediting/ui/fresco/CropGridView;->setVisibility(I)V

    .line 1785732
    iget-object v0, p0, LX/BSj;->d:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1785733
    return-void
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 1785729
    return-void
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 1785727
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 1785747
    const/4 v0, 0x0

    return v0
.end method

.method public final f()V
    .locals 0

    .prologue
    .line 1785728
    return-void
.end method

.method public final g()V
    .locals 0

    .prologue
    .line 1785726
    return-void
.end method

.method public final h()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1785721
    iget-object v0, p0, LX/BSj;->b:Lcom/facebook/video/player/RichVideoPlayer;

    new-instance v1, LX/BSh;

    invoke-direct {v1, p0}, LX/BSh;-><init>(LX/BSj;)V

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1785722
    iget-object v0, p0, LX/BSj;->c:Lcom/facebook/video/creativeediting/ui/fresco/CropGridView;

    invoke-virtual {v0, v2}, Lcom/facebook/video/creativeediting/ui/fresco/CropGridView;->setVisibility(I)V

    .line 1785723
    iget-object v0, p0, LX/BSj;->d:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1785724
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/BSj;->e:Z

    .line 1785725
    return-void
.end method

.method public final i()V
    .locals 2

    .prologue
    .line 1785719
    iget-object v0, p0, LX/BSj;->b:Lcom/facebook/video/player/RichVideoPlayer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1785720
    return-void
.end method

.method public final j()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1785718
    sget-object v0, LX/5SK;->CROP:LX/5SK;

    return-object v0
.end method

.method public final k()V
    .locals 0

    .prologue
    .line 1785717
    return-void
.end method
