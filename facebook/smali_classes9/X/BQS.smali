.class public LX/BQS;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/9fy;

.field public final b:LX/BQP;

.field public final c:LX/BQQ;

.field public final d:LX/0kL;

.field private final e:Ljava/util/concurrent/Executor;


# direct methods
.method public constructor <init>(LX/9fy;LX/BQP;LX/BQQ;LX/0kL;Ljava/util/concurrent/Executor;)V
    .locals 0
    .param p5    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1782210
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1782211
    iput-object p1, p0, LX/BQS;->a:LX/9fy;

    .line 1782212
    iput-object p2, p0, LX/BQS;->b:LX/BQP;

    .line 1782213
    iput-object p3, p0, LX/BQS;->c:LX/BQQ;

    .line 1782214
    iput-object p4, p0, LX/BQS;->d:LX/0kL;

    .line 1782215
    iput-object p5, p0, LX/BQS;->e:Ljava/util/concurrent/Executor;

    .line 1782216
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;J)V
    .locals 8

    .prologue
    .line 1782196
    iget-object v0, p1, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->h:Ljava/lang/String;

    move-object v0, v0

    .line 1782197
    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1782198
    iget-object v0, p0, LX/BQS;->c:LX/BQQ;

    .line 1782199
    iget-object v1, p1, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->b:Landroid/net/Uri;

    move-object v1, v1

    .line 1782200
    iget-object v2, p1, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->d:Ljava/lang/String;

    move-object v2, v2

    .line 1782201
    invoke-virtual {v0, v1, v2}, LX/BQQ;->a(Landroid/net/Uri;Ljava/lang/String;)V

    .line 1782202
    iget-object v0, p0, LX/BQS;->a:LX/9fy;

    .line 1782203
    iget-object v1, p1, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->f:Ljava/lang/String;

    move-object v1, v1

    .line 1782204
    iget-object v2, p1, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->g:Landroid/graphics/RectF;

    move-object v2, v2

    .line 1782205
    const-string v3, "existing"

    .line 1782206
    iget-object v4, p1, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->h:Ljava/lang/String;

    move-object v6, v4

    .line 1782207
    move-wide v4, p2

    invoke-virtual/range {v0 .. v6}, LX/9fy;->a(Ljava/lang/String;Landroid/graphics/RectF;Ljava/lang/String;JLjava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, LX/BQR;

    invoke-direct {v1, p0, p1}, LX/BQR;-><init>(LX/BQS;Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;)V

    iget-object v2, p0, LX/BQS;->e:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1782208
    return-void

    .line 1782209
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
