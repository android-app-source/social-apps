.class public final LX/BA1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/BA2;


# direct methods
.method public constructor <init>(LX/BA2;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1752619
    iput-object p1, p0, LX/BA1;->b:LX/BA2;

    iput-object p2, p0, LX/BA1;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1752620
    iget-object v0, p0, LX/BA1;->b:LX/BA2;

    iget-object v0, v0, LX/BA2;->b:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 1752621
    const-string v0, "decodePayload"

    const v1, -0xdcc58bf

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 1752622
    :try_start_0
    iget-object v0, p0, LX/BA1;->b:LX/BA2;

    iget-object v1, p0, LX/BA1;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/BA2;->a(Ljava/lang/String;)[B

    move-result-object v0

    .line 1752623
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 1752624
    const/4 v2, 0x1

    iput-boolean v2, v1, Landroid/graphics/BitmapFactory$Options;->inMutable:Z

    .line 1752625
    const/4 v2, 0x0

    array-length v3, v0

    invoke-static {v0, v2, v3, v1}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1752626
    const v1, 0x51b23174

    invoke-static {v1}, LX/02m;->a(I)V

    return-object v0

    :catchall_0
    move-exception v0

    const v1, -0x6efa7f06

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method
