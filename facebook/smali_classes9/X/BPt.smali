.class public abstract LX/BPt;
.super LX/0b2;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0b2",
        "<",
        "LX/BPr;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/os/ParcelUuid;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1780970
    invoke-direct {p0}, LX/0b2;-><init>()V

    .line 1780971
    const/4 v0, 0x0

    iput-object v0, p0, LX/BPt;->a:Landroid/os/ParcelUuid;

    .line 1780972
    return-void
.end method

.method public constructor <init>(Landroid/os/ParcelUuid;)V
    .locals 0

    .prologue
    .line 1780973
    invoke-direct {p0}, LX/0b2;-><init>()V

    .line 1780974
    iput-object p1, p0, LX/BPt;->a:Landroid/os/ParcelUuid;

    .line 1780975
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/BPr;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1780976
    const-class v0, LX/BPr;

    return-object v0
.end method

.method public final a(LX/0b7;)Z
    .locals 2

    .prologue
    .line 1780977
    check-cast p1, LX/BPr;

    .line 1780978
    iget-object v0, p0, LX/BPt;->a:Landroid/os/ParcelUuid;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/BPt;->a:Landroid/os/ParcelUuid;

    .line 1780979
    iget-object v1, p1, LX/BPr;->a:Landroid/os/ParcelUuid;

    move-object v1, v1

    .line 1780980
    invoke-virtual {v0, v1}, Landroid/os/ParcelUuid;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
