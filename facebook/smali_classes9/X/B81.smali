.class public LX/B81;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1748385
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1748386
    return-void
.end method

.method public static a(LX/0QB;)LX/B81;
    .locals 3

    .prologue
    .line 1748374
    const-class v1, LX/B81;

    monitor-enter v1

    .line 1748375
    :try_start_0
    sget-object v0, LX/B81;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1748376
    sput-object v2, LX/B81;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1748377
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1748378
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 1748379
    new-instance v0, LX/B81;

    invoke-direct {v0}, LX/B81;-><init>()V

    .line 1748380
    move-object v0, v0

    .line 1748381
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1748382
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/B81;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1748383
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1748384
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Landroid/view/ViewGroup;Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;)Landroid/view/View;
    .locals 5

    .prologue
    .line 1748358
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->l()Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->k()Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->s()Lcom/facebook/graphql/model/GraphQLLeadGenQuestionValidationSpec;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->n()Z

    move-result v3

    .line 1748359
    if-eqz v1, :cond_0

    .line 1748360
    sget-object v4, LX/B80;->a:[I

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;->ordinal()I

    move-result p1

    aget v4, v4, p1

    packed-switch v4, :pswitch_data_0

    .line 1748361
    :cond_0
    if-eqz v2, :cond_1

    .line 1748362
    sget-object v4, LX/B80;->b:[I

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLLeadGenQuestionValidationSpec;->a()Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationType;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLLeadGenQuestionValidationType;->ordinal()I

    move-result p1

    aget v4, v4, p1

    packed-switch v4, :pswitch_data_1

    .line 1748363
    :cond_1
    sget-object v4, LX/B80;->c:[I

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;->ordinal()I

    move-result p1

    aget v4, v4, p1

    packed-switch v4, :pswitch_data_2

    .line 1748364
    sget-object v4, LX/B8K;->a:LX/B7j;

    :goto_0
    move-object v0, v4

    .line 1748365
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {v0, v1}, LX/B7j;->a(Landroid/content/Context;)Landroid/view/View;

    move-result-object v0

    return-object v0

    .line 1748366
    :pswitch_0
    sget-object v4, LX/B8A;->a:LX/B7j;

    goto :goto_0

    .line 1748367
    :pswitch_1
    sget-object v4, LX/B8R;->a:LX/B7j;

    goto :goto_0

    .line 1748368
    :pswitch_2
    sget-object v4, LX/B7r;->a:LX/B7j;

    goto :goto_0

    .line 1748369
    :pswitch_3
    sget-object v4, LX/B7v;->a:LX/B7j;

    goto :goto_0

    .line 1748370
    :pswitch_4
    if-eqz v3, :cond_2

    sget-object v4, LX/B8K;->a:LX/B7j;

    goto :goto_0

    :cond_2
    sget-object v4, LX/B83;->a:LX/B7j;

    goto :goto_0

    .line 1748371
    :pswitch_5
    sget-object v4, LX/B7z;->a:LX/B7j;

    goto :goto_0

    .line 1748372
    :pswitch_6
    sget-object v4, LX/B8F;->a:LX/B7j;

    goto :goto_0

    .line 1748373
    :pswitch_7
    sget-object v4, LX/B7n;->a:LX/B7j;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method
