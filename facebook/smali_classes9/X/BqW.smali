.class public final LX/BqW;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/BqW;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/BqV;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/BqX;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1824768
    const/4 v0, 0x0

    sput-object v0, LX/BqW;->a:LX/BqW;

    .line 1824769
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/BqW;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1824789
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1824790
    new-instance v0, LX/BqX;

    invoke-direct {v0}, LX/BqX;-><init>()V

    iput-object v0, p0, LX/BqW;->c:LX/BqX;

    .line 1824791
    return-void
.end method

.method public static c(LX/1De;)LX/BqV;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1824777
    new-instance v1, LX/BqU;

    invoke-direct {v1}, LX/BqU;-><init>()V

    .line 1824778
    sget-object v2, LX/BqW;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/BqV;

    .line 1824779
    if-nez v2, :cond_0

    .line 1824780
    new-instance v2, LX/BqV;

    invoke-direct {v2}, LX/BqV;-><init>()V

    .line 1824781
    :cond_0
    invoke-static {v2, p0, v0, v0, v1}, LX/BqV;->a$redex0(LX/BqV;LX/1De;IILX/BqU;)V

    .line 1824782
    move-object v1, v2

    .line 1824783
    move-object v0, v1

    .line 1824784
    return-object v0
.end method

.method public static declared-synchronized q()LX/BqW;
    .locals 2

    .prologue
    .line 1824785
    const-class v1, LX/BqW;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/BqW;->a:LX/BqW;

    if-nez v0, :cond_0

    .line 1824786
    new-instance v0, LX/BqW;

    invoke-direct {v0}, LX/BqW;-><init>()V

    sput-object v0, LX/BqW;->a:LX/BqW;

    .line 1824787
    :cond_0
    sget-object v0, LX/BqW;->a:LX/BqW;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 1824788
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 9

    .prologue
    .line 1824772
    check-cast p2, LX/BqU;

    .line 1824773
    iget v0, p2, LX/BqU;->a:I

    iget-object v1, p2, LX/BqU;->b:Ljava/lang/CharSequence;

    iget v2, p2, LX/BqU;->c:I

    const/4 v6, 0x0

    .line 1824774
    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget-object v3, v3, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-static {v3}, Ljava/text/NumberFormat;->getInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v3

    .line 1824775
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x2

    invoke-interface {v4, v5}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v4

    const v5, 0x7f0e05e4

    invoke-static {p1, v6, v5}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, v2}, LX/1ne;->m(I)LX/1ne;

    move-result-object v5

    int-to-long v7, v0

    invoke-virtual {v3, v7, v8}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v3

    invoke-interface {v4, v3}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    const v4, 0x7f0e05e3

    invoke-static {p1, v6, v4}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v4

    invoke-virtual {v4, v1}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-interface {v3, v4}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 1824776
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1824770
    invoke-static {}, LX/1dS;->b()V

    .line 1824771
    const/4 v0, 0x0

    return-object v0
.end method
