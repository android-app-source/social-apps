.class public LX/B7O;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

.field private d:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;

.field private e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private f:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private g:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:Ljava/lang/String;

.field private l:Lcom/facebook/graphql/model/GraphQLLeadGenQuestionValidationSpec;


# direct methods
.method public constructor <init>(Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;)V
    .locals 2

    .prologue
    .line 1747988
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1747989
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->p()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/B7O;->a:Ljava/lang/String;

    .line 1747990
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/B7O;->b:Ljava/lang/String;

    .line 1747991
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->l()Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

    move-result-object v0

    iput-object v0, p0, LX/B7O;->c:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

    .line 1747992
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->k()Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;

    move-result-object v0

    iput-object v0, p0, LX/B7O;->d:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;

    .line 1747993
    iget-object v0, p0, LX/B7O;->c:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;->INLINE_SELECT:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/B7O;->c:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;->SELECT:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

    if-ne v0, v1, :cond_1

    .line 1747994
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->r()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/B7O;->e:LX/0Px;

    .line 1747995
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->a()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/B7O;->f:LX/0Px;

    .line 1747996
    :goto_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->o()Z

    move-result v0

    iput-boolean v0, p0, LX/B7O;->h:Z

    .line 1747997
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->n()Z

    move-result v0

    iput-boolean v0, p0, LX/B7O;->i:Z

    .line 1747998
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->m()Z

    move-result v0

    iput-boolean v0, p0, LX/B7O;->j:Z

    .line 1747999
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->q()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/B7O;->k:Ljava/lang/String;

    .line 1748000
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->s()Lcom/facebook/graphql/model/GraphQLLeadGenQuestionValidationSpec;

    move-result-object v0

    iput-object v0, p0, LX/B7O;->l:Lcom/facebook/graphql/model/GraphQLLeadGenQuestionValidationSpec;

    .line 1748001
    return-void

    .line 1748002
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->r()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/B7O;->g:LX/0Px;

    goto :goto_0
.end method
