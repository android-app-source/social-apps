.class public final LX/CY8;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:LX/CYB;

.field public final c:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/CYC;

.field public final f:LX/CY9;

.field public final g:LX/CYA;


# direct methods
.method public constructor <init>(Ljava/lang/String;LX/CYB;LX/0am;LX/0am;LX/CYC;LX/CY9;LX/CYA;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/CYB;",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/CYC;",
            "LX/CY9;",
            "LX/CYA;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1910807
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1910808
    iput-object p1, p0, LX/CY8;->a:Ljava/lang/String;

    .line 1910809
    iput-object p2, p0, LX/CY8;->b:LX/CYB;

    .line 1910810
    iput-object p3, p0, LX/CY8;->c:LX/0am;

    .line 1910811
    iput-object p4, p0, LX/CY8;->d:LX/0am;

    .line 1910812
    iput-object p5, p0, LX/CY8;->e:LX/CYC;

    .line 1910813
    iput-object p6, p0, LX/CY8;->f:LX/CY9;

    .line 1910814
    iput-object p7, p0, LX/CY8;->g:LX/CYA;

    .line 1910815
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1910816
    instance-of v1, p1, LX/CY8;

    if-nez v1, :cond_1

    .line 1910817
    :cond_0
    :goto_0
    return v0

    .line 1910818
    :cond_1
    check-cast p1, LX/CY8;

    .line 1910819
    iget-object v1, p0, LX/CY8;->a:Ljava/lang/String;

    iget-object v2, p1, LX/CY8;->a:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/CY8;->b:LX/CYB;

    iget-object v2, p1, LX/CY8;->b:LX/CYB;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/CY8;->c:LX/0am;

    iget-object v2, p1, LX/CY8;->c:LX/0am;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/CY8;->d:LX/0am;

    iget-object v2, p1, LX/CY8;->d:LX/0am;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/CY8;->e:LX/CYC;

    iget-object v2, p1, LX/CY8;->e:LX/CYC;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/CY8;->f:LX/CY9;

    iget-object v2, p1, LX/CY8;->f:LX/CY9;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/CY8;->g:LX/CYA;

    iget-object v2, p1, LX/CY8;->g:LX/CYA;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1910820
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, LX/CY8;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, LX/CY8;->b:LX/CYB;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, LX/CY8;->c:LX/0am;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, LX/CY8;->d:LX/0am;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, LX/CY8;->e:LX/CYC;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, LX/CY8;->f:LX/CY9;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p0, LX/CY8;->g:LX/CYA;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
