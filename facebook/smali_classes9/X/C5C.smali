.class public LX/C5C;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pk;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/C5D;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/C5C",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/C5D;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1848494
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1848495
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/C5C;->b:LX/0Zi;

    .line 1848496
    iput-object p1, p0, LX/C5C;->a:LX/0Ot;

    .line 1848497
    return-void
.end method

.method public static a(LX/0QB;)LX/C5C;
    .locals 4

    .prologue
    .line 1848498
    const-class v1, LX/C5C;

    monitor-enter v1

    .line 1848499
    :try_start_0
    sget-object v0, LX/C5C;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1848500
    sput-object v2, LX/C5C;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1848501
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1848502
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1848503
    new-instance v3, LX/C5C;

    const/16 p0, 0x1f64

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/C5C;-><init>(LX/0Ot;)V

    .line 1848504
    move-object v0, v3

    .line 1848505
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1848506
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C5C;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1848507
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1848508
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 5

    .prologue
    .line 1848509
    check-cast p2, LX/C5B;

    .line 1848510
    iget-object v0, p0, LX/C5C;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/C5D;

    iget-object v1, p2, LX/C5B;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/C5B;->b:LX/1Pq;

    const/4 p2, 0x1

    .line 1848511
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 1848512
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v3}, LX/1Wr;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    .line 1848513
    invoke-interface {v3}, Lcom/facebook/graphql/model/ItemListFeedUnit;->Q_()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Lcom/facebook/graphql/model/ItemListFeedUnit;->Q_()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1848514
    :cond_0
    const/4 v3, 0x0

    .line 1848515
    :goto_0
    move-object v0, v3

    .line 1848516
    return-object v0

    :cond_1
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    const/4 p0, 0x2

    invoke-interface {v4, p0}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object p0

    invoke-interface {v3}, Lcom/facebook/graphql/model/ItemListFeedUnit;->Q_()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v3

    const/high16 p0, 0x41600000    # 14.0f

    invoke-virtual {v3, p0}, LX/1ne;->g(F)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, p2}, LX/1ne;->t(I)LX/1ne;

    move-result-object v3

    const p0, 0x7f0a00e6

    invoke-virtual {v3, p0}, LX/1ne;->n(I)LX/1ne;

    move-result-object v3

    const/4 p0, 0x0

    invoke-virtual {v3, p0}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const p0, 0x7f0b00f9

    invoke-interface {v3, p2, p0}, LX/1Di;->g(II)LX/1Di;

    move-result-object v3

    const/4 p0, 0x3

    const p2, 0x7f0b09ae

    invoke-interface {v3, p0, p2}, LX/1Di;->g(II)LX/1Di;

    move-result-object v3

    const/high16 p0, 0x3f800000    # 1.0f

    invoke-interface {v3, p0}, LX/1Di;->a(F)LX/1Di;

    move-result-object v3

    invoke-interface {v4, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    iget-object v4, v0, LX/C5D;->a:LX/C5G;

    const/4 p0, 0x0

    .line 1848517
    new-instance p2, LX/C5F;

    invoke-direct {p2, v4}, LX/C5F;-><init>(LX/C5G;)V

    .line 1848518
    iget-object v0, v4, LX/C5G;->b:LX/0Zi;

    invoke-virtual {v0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/C5E;

    .line 1848519
    if-nez v0, :cond_2

    .line 1848520
    new-instance v0, LX/C5E;

    invoke-direct {v0, v4}, LX/C5E;-><init>(LX/C5G;)V

    .line 1848521
    :cond_2
    invoke-static {v0, p1, p0, p0, p2}, LX/C5E;->a$redex0(LX/C5E;LX/1De;IILX/C5F;)V

    .line 1848522
    move-object p2, v0

    .line 1848523
    move-object p0, p2

    .line 1848524
    move-object v4, p0

    .line 1848525
    iget-object p0, v4, LX/C5E;->a:LX/C5F;

    iput-object v1, p0, LX/C5F;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1848526
    iget-object p0, v4, LX/C5E;->e:Ljava/util/BitSet;

    const/4 p2, 0x0

    invoke-virtual {p0, p2}, Ljava/util/BitSet;->set(I)V

    .line 1848527
    move-object v4, v4

    .line 1848528
    iget-object p0, v4, LX/C5E;->a:LX/C5F;

    iput-object v2, p0, LX/C5F;->b:LX/1Pq;

    .line 1848529
    iget-object p0, v4, LX/C5E;->e:Ljava/util/BitSet;

    const/4 p2, 0x1

    invoke-virtual {p0, p2}, Ljava/util/BitSet;->set(I)V

    .line 1848530
    move-object v4, v4

    .line 1848531
    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x4

    const p0, 0x7f0b00bd

    invoke-interface {v3, v4, p0}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    goto/16 :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1848532
    invoke-static {}, LX/1dS;->b()V

    .line 1848533
    const/4 v0, 0x0

    return-object v0
.end method
