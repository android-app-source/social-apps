.class public final LX/Bbo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;)V
    .locals 0

    .prologue
    .line 1801086
    iput-object p1, p0, LX/Bbo;->a:Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;B)V
    .locals 0

    .prologue
    .line 1801087
    invoke-direct {p0, p1}, LX/Bbo;-><init>(Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;)V

    return-void
.end method

.method private a(Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 1801088
    iget-object v0, p0, LX/Bbo;->a:Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;

    iget-object v0, v0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->H:LX/0Px;

    if-nez v0, :cond_0

    iget-object v0, p0, LX/Bbo;->a:Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;

    iget-object v0, v0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->I:LX/0Px;

    if-nez v0, :cond_0

    .line 1801089
    :goto_0
    return-void

    .line 1801090
    :cond_0
    invoke-static {p1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/Bbo;->a:Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;

    iget-boolean v0, v0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->K:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/Bbo;->a:Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;

    iget-object v0, v0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->H:LX/0Px;

    if-eqz v0, :cond_2

    .line 1801091
    iget-object v0, p0, LX/Bbo;->a:Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;

    iget-object v0, v0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->p:LX/8tC;

    iget-object v1, p0, LX/Bbo;->a:Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;

    iget-object v1, v1, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->H:LX/0Px;

    invoke-virtual {v0, v1}, LX/8tB;->a(Ljava/util/List;)V

    .line 1801092
    iget-object v0, p0, LX/Bbo;->a:Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;

    const/4 v1, 0x0

    .line 1801093
    iput-boolean v1, v0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->K:Z

    .line 1801094
    :cond_1
    :goto_1
    iget-object v0, p0, LX/Bbo;->a:Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;

    iget-object v0, v0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->p:LX/8tC;

    invoke-virtual {v0}, LX/3Tf;->a()LX/333;

    move-result-object v0

    invoke-interface {v0, p1}, LX/333;->a(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1801095
    :cond_2
    invoke-static {p1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, LX/Bbo;->a:Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;

    iget-boolean v0, v0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->K:Z

    if-nez v0, :cond_1

    iget-object v0, p0, LX/Bbo;->a:Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;

    iget-object v0, v0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->I:LX/0Px;

    if-eqz v0, :cond_1

    .line 1801096
    iget-object v0, p0, LX/Bbo;->a:Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;

    iget-object v0, v0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->p:LX/8tC;

    iget-object v1, p0, LX/Bbo;->a:Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;

    iget-object v1, v1, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->I:LX/0Px;

    invoke-virtual {v0, v1}, LX/8tB;->a(Ljava/util/List;)V

    .line 1801097
    iget-object v0, p0, LX/Bbo;->a:Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;

    const/4 v1, 0x1

    .line 1801098
    iput-boolean v1, v0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->K:Z

    .line 1801099
    goto :goto_1
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 5

    .prologue
    .line 1801100
    iget-object v0, p0, LX/Bbo;->a:Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;

    iget-object v0, v0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->J:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    .line 1801101
    invoke-virtual {v0}, LX/8QK;->b()Ljava/lang/String;

    move-result-object v2

    .line 1801102
    const/4 v3, 0x0

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v4

    invoke-interface {p1, v3, v4}, Landroid/text/Editable;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1801103
    iget-object v2, p0, LX/Bbo;->a:Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;

    iget-object v2, v2, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->J:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1801104
    :cond_1
    iget-object v0, p0, LX/Bbo;->a:Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;

    iget-object v0, v0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->B:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getUserEnteredPlainText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-direct {p0, v0}, LX/Bbo;->a(Ljava/lang/CharSequence;)V

    .line 1801105
    iget-object v0, p0, LX/Bbo;->a:Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;

    invoke-static {v0}, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->o(Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;)V

    .line 1801106
    iget-object v0, p0, LX/Bbo;->a:Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;

    invoke-static {v0}, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->m(Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;)V

    .line 1801107
    iget-object v0, p0, LX/Bbo;->a:Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;

    iget-object v0, v0, Lcom/facebook/checkin/socialsearch/invitefriends/SocialSearchInviteFriendsActivity;->p:LX/8tC;

    const v1, 0x424af39f

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1801108
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1801109
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1801110
    return-void
.end method
