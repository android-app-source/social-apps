.class public final LX/C4U;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/C4V;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public b:I

.field public c:LX/1Pb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public final synthetic d:LX/C4V;


# direct methods
.method public constructor <init>(LX/C4V;)V
    .locals 1

    .prologue
    .line 1847345
    iput-object p1, p0, LX/C4U;->d:LX/C4V;

    .line 1847346
    move-object v0, p1

    .line 1847347
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1847348
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1847344
    const-string v0, "FullBleedPhotoStoryComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1847328
    if-ne p0, p1, :cond_1

    .line 1847329
    :cond_0
    :goto_0
    return v0

    .line 1847330
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1847331
    goto :goto_0

    .line 1847332
    :cond_3
    check-cast p1, LX/C4U;

    .line 1847333
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1847334
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1847335
    if-eq v2, v3, :cond_0

    .line 1847336
    iget-object v2, p0, LX/C4U;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/C4U;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/C4U;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1847337
    goto :goto_0

    .line 1847338
    :cond_5
    iget-object v2, p1, LX/C4U;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 1847339
    :cond_6
    iget v2, p0, LX/C4U;->b:I

    iget v3, p1, LX/C4U;->b:I

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 1847340
    goto :goto_0

    .line 1847341
    :cond_7
    iget-object v2, p0, LX/C4U;->c:LX/1Pb;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/C4U;->c:LX/1Pb;

    iget-object v3, p1, LX/C4U;->c:LX/1Pb;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1847342
    goto :goto_0

    .line 1847343
    :cond_8
    iget-object v2, p1, LX/C4U;->c:LX/1Pb;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
