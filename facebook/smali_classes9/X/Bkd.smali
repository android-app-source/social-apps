.class public LX/Bkd;
.super LX/1a1;
.source ""


# instance fields
.field private l:Lcom/facebook/resources/ui/FbTextView;

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;

.field private o:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 7

    .prologue
    const/16 v6, 0x21

    .line 1814643
    invoke-direct {p0, p1}, LX/1a1;-><init>(Landroid/view/View;)V

    .line 1814644
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 1814645
    const v0, 0x7f0d0de9

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/Bkd;->l:Lcom/facebook/resources/ui/FbTextView;

    .line 1814646
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f082151

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/Bkd;->m:Ljava/lang/String;

    .line 1814647
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f082152

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/Bkd;->n:Ljava/lang/String;

    .line 1814648
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f082153

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/Bkd;->o:Ljava/lang/String;

    .line 1814649
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f082150

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "[[ticketing_information]]"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "[[promotion_tools]]"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "[[easier_to_find]]"

    aput-object v4, v2, v3

    invoke-static {v0, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1814650
    new-instance v2, LX/47x;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-direct {v2, v3}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    invoke-virtual {v2, v0}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    move-result-object v0

    const-string v2, "[[ticketing_information]]"

    iget-object v3, p0, LX/Bkd;->m:Ljava/lang/String;

    new-instance v4, Landroid/text/style/TextAppearanceSpan;

    const v5, 0x7f0e08af

    invoke-direct {v4, v1, v5}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v0, v2, v3, v4, v6}, LX/47x;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;I)LX/47x;

    move-result-object v0

    const-string v2, "[[promotion_tools]]"

    iget-object v3, p0, LX/Bkd;->n:Ljava/lang/String;

    new-instance v4, Landroid/text/style/TextAppearanceSpan;

    const v5, 0x7f0e08af

    invoke-direct {v4, v1, v5}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v0, v2, v3, v4, v6}, LX/47x;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;I)LX/47x;

    move-result-object v0

    const-string v2, "[[easier_to_find]]"

    iget-object v3, p0, LX/Bkd;->o:Ljava/lang/String;

    new-instance v4, Landroid/text/style/TextAppearanceSpan;

    const v5, 0x7f0e08af

    invoke-direct {v4, v1, v5}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v0, v2, v3, v4, v6}, LX/47x;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;I)LX/47x;

    move-result-object v0

    invoke-virtual {v0}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v0

    .line 1814651
    iget-object v1, p0, LX/Bkd;->l:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1814652
    return-void
.end method
