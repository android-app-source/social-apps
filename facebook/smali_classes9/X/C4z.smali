.class public LX/C4z;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pp;",
        ":",
        "LX/1Pt;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/1Vm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Vm",
            "<TE;>;"
        }
    .end annotation
.end field

.field public final b:LX/1xP;


# direct methods
.method public constructor <init>(LX/1Vm;LX/1xP;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1848149
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1848150
    iput-object p1, p0, LX/C4z;->a:LX/1Vm;

    .line 1848151
    iput-object p2, p0, LX/C4z;->b:LX/1xP;

    .line 1848152
    return-void
.end method

.method public static a(LX/0QB;)LX/C4z;
    .locals 5

    .prologue
    .line 1848153
    const-class v1, LX/C4z;

    monitor-enter v1

    .line 1848154
    :try_start_0
    sget-object v0, LX/C4z;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1848155
    sput-object v2, LX/C4z;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1848156
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1848157
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1848158
    new-instance p0, LX/C4z;

    invoke-static {v0}, LX/1Vm;->a(LX/0QB;)LX/1Vm;

    move-result-object v3

    check-cast v3, LX/1Vm;

    invoke-static {v0}, LX/1xP;->a(LX/0QB;)LX/1xP;

    move-result-object v4

    check-cast v4, LX/1xP;

    invoke-direct {p0, v3, v4}, LX/C4z;-><init>(LX/1Vm;LX/1xP;)V

    .line 1848159
    move-object v0, p0

    .line 1848160
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1848161
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C4z;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1848162
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1848163
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
