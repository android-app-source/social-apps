.class public LX/BHv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1BP;


# instance fields
.field public final a:Z

.field public final b:Z

.field private final c:Ljava/util/Random;

.field private final d:LX/1BW;

.field private final e:J

.field public final f:J

.field private final g:Lcom/facebook/imagepipeline/instrumentation/FbFrescoPprHoneyClientEventUtil;


# direct methods
.method public constructor <init>(Lcom/facebook/imagepipeline/instrumentation/FbFrescoPprHoneyClientEventUtil;LX/1BQ;Ljava/util/Random;LX/1BW;)V
    .locals 4
    .param p3    # Ljava/util/Random;
        .annotation runtime Lcom/facebook/common/random/InsecureRandom;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1769889
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1769890
    invoke-virtual {p2}, LX/1BQ;->a()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/BHv;->a:Z

    .line 1769891
    invoke-virtual {p2}, LX/1BQ;->b()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/BHv;->b:Z

    .line 1769892
    iput-object p3, p0, LX/BHv;->c:Ljava/util/Random;

    .line 1769893
    iput-object p4, p0, LX/BHv;->d:LX/1BW;

    .line 1769894
    iget-wide v2, p2, LX/1BQ;->b:J

    move-wide v0, v2

    .line 1769895
    iput-wide v0, p0, LX/BHv;->e:J

    .line 1769896
    iget-wide v2, p2, LX/1BQ;->c:J

    move-wide v0, v2

    .line 1769897
    iput-wide v0, p0, LX/BHv;->f:J

    .line 1769898
    iput-object p1, p0, LX/BHv;->g:Lcom/facebook/imagepipeline/instrumentation/FbFrescoPprHoneyClientEventUtil;

    .line 1769899
    return-void
.end method

.method public static a(LX/0QB;)LX/BHv;
    .locals 5

    .prologue
    .line 1769900
    new-instance v4, LX/BHv;

    invoke-static {p0}, Lcom/facebook/imagepipeline/instrumentation/FbFrescoPprHoneyClientEventUtil;->b(LX/0QB;)Lcom/facebook/imagepipeline/instrumentation/FbFrescoPprHoneyClientEventUtil;

    move-result-object v0

    check-cast v0, Lcom/facebook/imagepipeline/instrumentation/FbFrescoPprHoneyClientEventUtil;

    invoke-static {p0}, LX/1BQ;->b(LX/0QB;)LX/1BQ;

    move-result-object v1

    check-cast v1, LX/1BQ;

    invoke-static {p0}, LX/0U5;->a(LX/0QB;)Ljava/util/Random;

    move-result-object v2

    check-cast v2, Ljava/util/Random;

    invoke-static {p0}, LX/1BW;->a(LX/0QB;)LX/1BW;

    move-result-object v3

    check-cast v3, LX/1BW;

    invoke-direct {v4, v0, v1, v2, v3}, LX/BHv;-><init>(Lcom/facebook/imagepipeline/instrumentation/FbFrescoPprHoneyClientEventUtil;LX/1BQ;Ljava/util/Random;LX/1BW;)V

    .line 1769901
    move-object v0, v4

    .line 1769902
    return-object v0
.end method


# virtual methods
.method public final a(LX/2xo;)V
    .locals 4

    .prologue
    .line 1769903
    iget-object v0, p0, LX/BHv;->d:LX/1BW;

    invoke-virtual {v0, p1}, LX/1BW;->a(LX/2xo;)V

    .line 1769904
    iget-boolean v0, p0, LX/BHv;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/BHv;->c:Ljava/util/Random;

    invoke-virtual {v0}, Ljava/util/Random;->nextInt()I

    move-result v0

    int-to-long v0, v0

    iget-wide v2, p0, LX/BHv;->e:J

    rem-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 1769905
    iget-object v0, p0, LX/BHv;->g:Lcom/facebook/imagepipeline/instrumentation/FbFrescoPprHoneyClientEventUtil;

    invoke-virtual {v0, p1}, Lcom/facebook/imagepipeline/instrumentation/FbFrescoPprHoneyClientEventUtil;->a(LX/2xo;)V

    .line 1769906
    :cond_0
    return-void
.end method
