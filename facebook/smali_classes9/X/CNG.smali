.class public final LX/CNG;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/ui/media/attachments/MediaResource;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;)V
    .locals 0

    .prologue
    .line 1881461
    iput-object p1, p0, LX/CNG;->a:Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/ui/media/attachments/MediaResource;)V
    .locals 2

    .prologue
    .line 1881455
    iget-object v0, p1, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v1, LX/2MK;->VIDEO:LX/2MK;

    if-ne v0, v1, :cond_1

    .line 1881456
    iget-object v0, p0, LX/CNG;->a:Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;

    invoke-static {v0, p1}, Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;->setupOverlayImage(Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;Lcom/facebook/ui/media/attachments/MediaResource;)V

    .line 1881457
    iget-object v0, p0, LX/CNG;->a:Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;

    invoke-static {v0, p1}, Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;->setupInlineVideo(Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;Lcom/facebook/ui/media/attachments/MediaResource;)V

    .line 1881458
    :cond_0
    :goto_0
    return-void

    .line 1881459
    :cond_1
    iget-object v0, p1, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v1, LX/2MK;->AUDIO:LX/2MK;

    if-ne v0, v1, :cond_0

    .line 1881460
    iget-object v0, p0, LX/CNG;->a:Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;

    invoke-static {v0, p1}, Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;->setupAudioPlaceholder(Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;Lcom/facebook/ui/media/attachments/MediaResource;)V

    goto :goto_0
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    const/4 v2, 0x4

    .line 1881462
    iget-object v0, p0, LX/CNG;->a:Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;

    iget-object v0, v0, Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;->o:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 1881463
    iget-object v0, p0, LX/CNG;->a:Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;

    iget-object v0, v0, Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;->o:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1881464
    :cond_0
    iget-object v0, p0, LX/CNG;->a:Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;

    iget-object v0, v0, Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;->m:Lcom/facebook/video/player/InlineVideoView;

    if-eqz v0, :cond_1

    .line 1881465
    iget-object v0, p0, LX/CNG;->a:Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;

    iget-object v0, v0, Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;->m:Lcom/facebook/video/player/InlineVideoView;

    invoke-virtual {v0, v2}, Lcom/facebook/video/player/InlineVideoView;->setVisibility(I)V

    .line 1881466
    :cond_1
    iget-object v0, p0, LX/CNG;->a:Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;

    iget-object v0, v0, Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;->n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    if-eqz v0, :cond_2

    .line 1881467
    iget-object v0, p0, LX/CNG;->a:Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;

    iget-object v0, v0, Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;->n:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1881468
    :cond_2
    iget-object v0, p0, LX/CNG;->a:Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;

    iget-object v0, v0, Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;->p:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1881469
    iget-object v0, p0, LX/CNG;->a:Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;

    iget-object v0, v0, Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;->f:LX/03V;

    sget-object v1, Lcom/facebook/messaging/sharing/mediapreview/MediaSharePreviewPlayableView;->j:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Failed to fetch media resource for playable"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1881470
    return-void
.end method

.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1881454
    check-cast p1, Lcom/facebook/ui/media/attachments/MediaResource;

    invoke-direct {p0, p1}, LX/CNG;->a(Lcom/facebook/ui/media/attachments/MediaResource;)V

    return-void
.end method
