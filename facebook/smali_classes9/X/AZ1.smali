.class public final LX/AZ1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/AZ4;


# direct methods
.method public constructor <init>(LX/AZ4;)V
    .locals 0

    .prologue
    .line 1686420
    iput-object p1, p0, LX/AZ1;->a:LX/AZ4;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 11

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, -0x6595c137

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1686421
    iget-object v1, p0, LX/AZ1;->a:LX/AZ4;

    iget-object v1, v1, LX/AZ4;->q:LX/AY9;

    if-eqz v1, :cond_0

    .line 1686422
    iget-object v1, p0, LX/AZ1;->a:LX/AZ4;

    iget-object v1, v1, LX/AZ4;->q:LX/AY9;

    .line 1686423
    iget-object v3, v1, LX/AY9;->a:LX/AYE;

    iget-object v4, v1, LX/AY9;->a:LX/AYE;

    iget-wide v5, v4, LX/AYE;->G:J

    .line 1686424
    iput-wide v5, v3, LX/AYE;->H:J

    .line 1686425
    iget-object v3, v1, LX/AY9;->a:LX/AYE;

    iget-object v3, v3, LX/AYE;->c:LX/AYq;

    sget-object v4, LX/AYp;->COMMERCIAL_BREAK_STARTED:LX/AYp;

    invoke-virtual {v3, v4}, LX/AYq;->a(LX/AYp;)V

    .line 1686426
    iget-object v3, v1, LX/AY9;->a:LX/AYE;

    iget-object v3, v3, LX/AYE;->h:LX/AYs;

    .line 1686427
    iget-object v4, v3, LX/AYs;->a:LX/0if;

    sget-object v5, LX/0ig;->C:LX/0ih;

    const-string v6, "commercial_break_start"

    const/4 v7, 0x0

    invoke-static {v3}, LX/AYs;->i(LX/AYs;)LX/1rQ;

    move-result-object v8

    invoke-virtual {v4, v5, v6, v7, v8}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 1686428
    iget-object v3, v1, LX/AY9;->a:LX/AYE;

    iget-object v3, v3, LX/AYE;->i:LX/AYr;

    iget-object v4, v1, LX/AY9;->a:LX/AYE;

    iget-object v4, v4, LX/AYE;->n:LX/AXm;

    invoke-virtual {v4}, LX/AXm;->getCurrentViewerCount()I

    move-result v4

    .line 1686429
    new-instance v7, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v8, "commercial_break_broadcaster_break"

    invoke-direct {v7, v8}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v8, "commercial_break_broadcaster"

    .line 1686430
    iput-object v8, v7, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1686431
    move-object v7, v7

    .line 1686432
    const-string v8, "broadcaster_id"

    iget-object v9, v3, LX/AYr;->b:Ljava/lang/String;

    invoke-virtual {v7, v8, v9}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string v8, "video_id"

    iget-object v9, v3, LX/AYr;->c:Ljava/lang/String;

    invoke-virtual {v7, v8, v9}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string v8, "time_offset_ms"

    iget-wide v9, v3, LX/AYr;->d:J

    invoke-virtual {v7, v8, v9, v10}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string v8, "concurrent_viewer_count"

    invoke-virtual {v7, v8, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    .line 1686433
    iget-object v8, v3, LX/AYr;->a:LX/0Zb;

    invoke-interface {v8, v7}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1686434
    :cond_0
    const v1, -0x3466119

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
