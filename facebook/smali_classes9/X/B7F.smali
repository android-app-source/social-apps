.class public LX/B7F;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

.field private c:Lcom/facebook/graphql/model/GraphQLStory;

.field public d:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

.field public e:Z

.field public f:Z

.field private g:LX/B7K;

.field public h:LX/0if;

.field public i:LX/2sb;

.field private j:LX/0W9;

.field private k:LX/3Lz;

.field private l:Landroid/content/Context;


# direct methods
.method public constructor <init>(LX/0if;LX/2sb;LX/0W9;LX/3Lz;Landroid/content/Context;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 1
    .param p6    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/funnellogger/FunnelLogger;",
            "LX/2sb;",
            "LX/0W9;",
            "LX/3Lz;",
            "Landroid/content/Context;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1747885
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1747886
    iput-boolean v0, p0, LX/B7F;->e:Z

    .line 1747887
    iput-boolean v0, p0, LX/B7F;->f:Z

    .line 1747888
    iput-object p1, p0, LX/B7F;->h:LX/0if;

    .line 1747889
    iput-object p2, p0, LX/B7F;->i:LX/2sb;

    .line 1747890
    iput-object p3, p0, LX/B7F;->j:LX/0W9;

    .line 1747891
    iput-object p4, p0, LX/B7F;->k:LX/3Lz;

    .line 1747892
    iput-object p5, p0, LX/B7F;->l:Landroid/content/Context;

    .line 1747893
    iput-object p6, p0, LX/B7F;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1747894
    invoke-static {p6}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    iput-object v0, p0, LX/B7F;->c:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1747895
    iget-object v0, p6, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1747896
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    iput-object v0, p0, LX/B7F;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1747897
    iget-object v0, p0, LX/B7F;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v0}, LX/2sb;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    iput-object v0, p0, LX/B7F;->d:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 1747898
    return-void
.end method

.method private static C(LX/B7F;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1747899
    iget-object v0, p0, LX/B7F;->d:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    if-nez v0, :cond_0

    .line 1747900
    const-string v0, ""

    .line 1747901
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/B7F;->d:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->F()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static D(LX/B7F;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1747902
    iget-object v0, p0, LX/B7F;->d:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    if-nez v0, :cond_0

    .line 1747903
    const-string v0, ""

    .line 1747904
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/B7F;->d:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->G()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private H()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1747905
    iget-object v0, p0, LX/B7F;->d:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    if-nez v0, :cond_0

    .line 1747906
    const/4 v0, 0x0

    .line 1747907
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/B7F;->d:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bj()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/B7F;Lcom/facebook/graphql/model/GraphQLLeadGenPage;)LX/B7E;
    .locals 9
    .param p0    # LX/B7F;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1747908
    new-instance v0, LX/B7E;

    invoke-static {p0}, LX/B7F;->y(LX/B7F;)Lcom/facebook/graphql/model/GraphQLLeadGenData;

    move-result-object v2

    invoke-static {p0}, LX/B7F;->D(LX/B7F;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0}, LX/B7F;->C(LX/B7F;)Ljava/lang/String;

    move-result-object v4

    .line 1747909
    iget-object v1, p0, LX/B7F;->d:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    if-nez v1, :cond_0

    .line 1747910
    const-string v1, ""

    .line 1747911
    :goto_0
    move-object v5, v1

    .line 1747912
    iget-object v1, p0, LX/B7F;->d:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    if-nez v1, :cond_1

    .line 1747913
    const-string v1, ""

    .line 1747914
    :goto_1
    move-object v6, v1

    .line 1747915
    invoke-virtual {p0}, LX/B7F;->s()LX/B7K;

    move-result-object v7

    iget-object v1, p0, LX/B7F;->l:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v8, 0x7f0824ea

    invoke-virtual {v1, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    move-object v1, p1

    invoke-direct/range {v0 .. v8}, LX/B7E;-><init>(Lcom/facebook/graphql/model/GraphQLLeadGenPage;Lcom/facebook/graphql/model/GraphQLLeadGenData;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/B7K;Ljava/lang/String;)V

    return-object v0

    :cond_0
    iget-object v1, p0, LX/B7F;->d:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ay()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_1
    iget-object v1, p0, LX/B7F;->d:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aH()Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method

.method public static b(LX/B7F;Lcom/facebook/graphql/model/GraphQLLeadGenPage;)LX/B7N;
    .locals 6

    .prologue
    .line 1747916
    new-instance v0, LX/B7N;

    iget-object v1, p0, LX/B7F;->d:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aJ()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0}, LX/B7F;->D(LX/B7F;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0}, LX/B7F;->C(LX/B7F;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, LX/B7F;->s()LX/B7K;

    move-result-object v5

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, LX/B7N;-><init>(Lcom/facebook/graphql/model/GraphQLLeadGenPage;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/B7K;)V

    return-object v0
.end method

.method public static y(LX/B7F;)Lcom/facebook/graphql/model/GraphQLLeadGenData;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1747917
    iget-object v1, p0, LX/B7F;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    if-nez v1, :cond_1

    .line 1747918
    :cond_0
    :goto_0
    return-object v0

    .line 1747919
    :cond_1
    iget-object v1, p0, LX/B7F;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    const v2, 0x46a1c4a4

    invoke-static {v1, v2}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v1

    .line 1747920
    if-eqz v1, :cond_0

    .line 1747921
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->X()Lcom/facebook/graphql/model/GraphQLLeadGenData;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/0Px;)LX/0Px;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1747922
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1747923
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1747924
    :try_start_0
    iget-object v4, p0, LX/B7F;->k:LX/3Lz;

    const/4 v5, 0x0

    invoke-virtual {v4, v0, v5}, LX/3Lz;->parse(Ljava/lang/String;Ljava/lang/String;)LX/4hT;

    move-result-object v0

    .line 1747925
    iget-object v4, p0, LX/B7F;->k:LX/3Lz;

    invoke-virtual {v4, v0}, LX/3Lz;->isValidNumber(LX/4hT;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1747926
    iget-object v4, p0, LX/B7F;->k:LX/3Lz;

    invoke-virtual {v4, v0}, LX/3Lz;->getNationalSignificantNumber(LX/4hT;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch LX/4hE; {:try_start_0 .. :try_end_0} :catch_0

    .line 1747927
    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1747928
    :cond_1
    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    return-object v0

    :catch_0
    goto :goto_1
.end method

.method public final a(Ljava/lang/String;)LX/7Tl;
    .locals 5
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 1747929
    if-eqz p1, :cond_0

    .line 1747930
    :try_start_0
    iget-object v1, p0, LX/B7F;->k:LX/3Lz;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, LX/3Lz;->parse(Ljava/lang/String;Ljava/lang/String;)LX/4hT;

    move-result-object v1

    .line 1747931
    iget-object v2, p0, LX/B7F;->k:LX/3Lz;

    invoke-virtual {v2, v1}, LX/3Lz;->isValidNumber(LX/4hT;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1747932
    iget-object v2, p0, LX/B7F;->k:LX/3Lz;

    invoke-virtual {v2, v1}, LX/3Lz;->getRegionCodeForNumber(LX/4hT;)Ljava/lang/String;
    :try_end_0
    .catch LX/4hE; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1747933
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 1747934
    invoke-direct {p0}, LX/B7F;->H()Ljava/lang/String;

    move-result-object v0

    .line 1747935
    :cond_1
    if-nez v0, :cond_2

    .line 1747936
    iget-object v0, p0, LX/B7F;->j:LX/0W9;

    invoke-virtual {v0}, LX/0W9;->b()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getISO3Country()Ljava/lang/String;

    move-result-object v0

    .line 1747937
    :cond_2
    if-nez v0, :cond_3

    .line 1747938
    const-string v0, "US"

    .line 1747939
    :cond_3
    new-instance v1, LX/7Tl;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "+"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/B7F;->k:LX/3Lz;

    invoke-virtual {v3, v0}, LX/3Lz;->getCountryCodeForRegion(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/util/Locale;

    iget-object v4, p0, LX/B7F;->j:LX/0W9;

    invoke-virtual {v4}, LX/0W9;->b()Ljava/util/Locale;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Locale;->getDisplayLanguage()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4, v0}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v4, p0, LX/B7F;->j:LX/0W9;

    invoke-virtual {v4}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/Locale;->getDisplayCountry(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v0, v2, v3}, LX/7Tl;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v1

    :catch_0
    goto :goto_0
.end method

.method public final a(LX/B77;)Ljava/lang/String;
    .locals 7

    .prologue
    .line 1747862
    iget-object v0, p0, LX/B7F;->d:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 1747863
    sget-object v1, LX/B77;->PRIVACY_CHECKBOX_ERROR:LX/B77;

    if-ne p1, v1, :cond_1

    .line 1747864
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aw()Ljava/lang/String;

    move-result-object v2

    .line 1747865
    :cond_0
    :goto_0
    move-object v0, v2

    .line 1747866
    return-object v0

    .line 1747867
    :cond_1
    const/4 v2, 0x0

    .line 1747868
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->B()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v1, 0x0

    move v3, v1

    :goto_1
    if-ge v3, v5, :cond_0

    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLLeadGenErrorNode;

    .line 1747869
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLLeadGenErrorNode;->j()Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

    move-result-object v6

    iget-object p0, p1, LX/B77;->fieldInputType:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

    if-ne v6, p0, :cond_2

    .line 1747870
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLLeadGenErrorNode;->a()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 1747871
    :cond_2
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLLeadGenErrorNode;->j()Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

    move-result-object v6

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;->TEXT:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

    if-ne v6, p0, :cond_3

    .line 1747872
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLLeadGenErrorNode;->a()Ljava/lang/String;

    move-result-object v1

    .line 1747873
    :goto_2
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move-object v2, v1

    goto :goto_1

    :cond_3
    move-object v1, v2

    goto :goto_2
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1747884
    invoke-static {p0}, LX/B7F;->y(LX/B7F;)Lcom/facebook/graphql/model/GraphQLLeadGenData;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1747940
    iget-object v0, p0, LX/B7F;->c:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/B7F;->c:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1747875
    iget-object v0, p0, LX/B7F;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    const/4 v1, 0x0

    .line 1747876
    if-nez v0, :cond_1

    .line 1747877
    :cond_0
    :goto_0
    move-object v0, v1

    .line 1747878
    return-object v0

    .line 1747879
    :cond_1
    const p0, 0x46a1c4a4

    invoke-static {v0, p0}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object p0

    .line 1747880
    if-eqz p0, :cond_0

    .line 1747881
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->as()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object p0

    .line 1747882
    if-eqz p0, :cond_0

    .line 1747883
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPage;->Q()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1747874
    iget-object v0, p0, LX/B7F;->d:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->av()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final f()Z
    .locals 2

    .prologue
    .line 1747853
    iget-object v0, p0, LX/B7F;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    const/4 v1, 0x0

    .line 1747854
    if-nez v0, :cond_1

    .line 1747855
    :cond_0
    :goto_0
    move v0, v1

    .line 1747856
    return v0

    .line 1747857
    :cond_1
    const p0, 0x46a1c4a4

    invoke-static {v0, p0}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object p0

    .line 1747858
    if-eqz p0, :cond_0

    .line 1747859
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->X()Lcom/facebook/graphql/model/GraphQLLeadGenData;

    move-result-object p0

    .line 1747860
    if-eqz p0, :cond_0

    .line 1747861
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLLeadGenData;->p()Z

    move-result v1

    goto :goto_0
.end method

.method public final h()Landroid/net/Uri;
    .locals 3

    .prologue
    .line 1747845
    iget-object v0, p0, LX/B7F;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    const/4 v1, 0x0

    .line 1747846
    invoke-static {v0}, LX/2sb;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v2

    .line 1747847
    if-nez v2, :cond_1

    .line 1747848
    :cond_0
    :goto_0
    move-object v0, v1

    .line 1747849
    return-object v0

    .line 1747850
    :cond_1
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->as()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v2

    .line 1747851
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPage;->ai()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object p0

    if-eqz p0, :cond_0

    .line 1747852
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPage;->ai()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-static {v1}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v1

    goto :goto_0
.end method

.method public final i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1747842
    invoke-virtual {p0}, LX/B7F;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1747843
    invoke-static {p0}, LX/B7F;->y(LX/B7F;)Lcom/facebook/graphql/model/GraphQLLeadGenData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLLeadGenData;->n()Ljava/lang/String;

    move-result-object v0

    .line 1747844
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public final j()I
    .locals 1

    .prologue
    .line 1747841
    iget-object v0, p0, LX/B7F;->d:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->X()Lcom/facebook/graphql/model/GraphQLLeadGenData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLLeadGenData;->m()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method

.method public final n()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1747838
    iget-object v0, p0, LX/B7F;->d:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    if-nez v0, :cond_0

    .line 1747839
    const/4 v0, 0x0

    .line 1747840
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/B7F;->d:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->l()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final q()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1747832
    const-string v0, ""

    .line 1747833
    invoke-virtual {p0}, LX/B7F;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1747834
    invoke-static {p0}, LX/B7F;->y(LX/B7F;)Lcom/facebook/graphql/model/GraphQLLeadGenData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLLeadGenData;->a()Lcom/facebook/graphql/model/GraphQLLeadGenContextPage;

    move-result-object v1

    .line 1747835
    if-eqz v1, :cond_0

    .line 1747836
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLLeadGenContextPage;->l()Ljava/lang/String;

    move-result-object v0

    .line 1747837
    :cond_0
    return-object v0
.end method

.method public final s()LX/B7K;
    .locals 5

    .prologue
    .line 1747821
    iget-object v0, p0, LX/B7F;->g:LX/B7K;

    if-nez v0, :cond_0

    .line 1747822
    invoke-virtual {p0}, LX/B7F;->u()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1747823
    new-instance v0, LX/B7R;

    invoke-static {p0}, LX/B7F;->y(LX/B7F;)Lcom/facebook/graphql/model/GraphQLLeadGenData;

    invoke-virtual {p0}, LX/B7F;->h()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p0}, LX/B7F;->c()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/B7R;-><init>(Landroid/net/Uri;Ljava/lang/String;)V

    .line 1747824
    :goto_0
    move-object v0, v0

    .line 1747825
    iput-object v0, p0, LX/B7F;->g:LX/B7K;

    .line 1747826
    :cond_0
    iget-object v0, p0, LX/B7F;->g:LX/B7K;

    return-object v0

    :cond_1
    new-instance v0, LX/B7L;

    invoke-static {p0}, LX/B7F;->y(LX/B7F;)Lcom/facebook/graphql/model/GraphQLLeadGenData;

    move-result-object v1

    const/4 v2, 0x0

    .line 1747827
    iget-object v3, p0, LX/B7F;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    if-nez v3, :cond_3

    .line 1747828
    :cond_2
    :goto_1
    move-object v2, v2

    .line 1747829
    invoke-virtual {p0}, LX/B7F;->h()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {p0}, LX/B7F;->c()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, LX/B7L;-><init>(Lcom/facebook/graphql/model/GraphQLLeadGenData;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;)V

    goto :goto_0

    .line 1747830
    :cond_3
    iget-object v3, p0, LX/B7F;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v3}, LX/1VO;->r(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 1747831
    iget-object v2, p0, LX/B7F;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v2}, LX/1VO;->t(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    invoke-static {v2}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v2

    goto :goto_1
.end method

.method public final u()Z
    .locals 2

    .prologue
    .line 1747820
    iget-object v0, p0, LX/B7F;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/B7F;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/B7F;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v1, 0x4ed245b

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final w()Lcom/facebook/common/locale/Country;
    .locals 1

    .prologue
    .line 1747814
    invoke-direct {p0}, LX/B7F;->H()Ljava/lang/String;

    move-result-object v0

    .line 1747815
    if-nez v0, :cond_0

    .line 1747816
    iget-object v0, p0, LX/B7F;->j:LX/0W9;

    invoke-virtual {v0}, LX/0W9;->b()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getISO3Country()Ljava/lang/String;

    move-result-object v0

    .line 1747817
    :cond_0
    if-nez v0, :cond_1

    .line 1747818
    const-string v0, "US"

    .line 1747819
    :cond_1
    invoke-static {v0}, Lcom/facebook/common/locale/Country;->a(Ljava/lang/String;)Lcom/facebook/common/locale/Country;

    move-result-object v0

    return-object v0
.end method
