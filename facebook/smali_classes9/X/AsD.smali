.class public final LX/AsD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/ViewTreeObserver$OnDrawListener;


# instance fields
.field public final synthetic a:LX/AsI;


# direct methods
.method public constructor <init>(LX/AsI;)V
    .locals 0

    .prologue
    .line 1720234
    iput-object p1, p0, LX/AsD;->a:LX/AsI;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onDraw()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1720235
    const/4 v0, 0x2

    new-array v0, v0, [I

    .line 1720236
    iget-object v1, p0, LX/AsD;->a:LX/AsI;

    iget-object v1, v1, LX/AsI;->j:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->getLocationInWindow([I)V

    .line 1720237
    aget v1, v0, v2

    if-eqz v1, :cond_0

    .line 1720238
    iget-object v1, p0, LX/AsD;->a:LX/AsI;

    iget-object v1, v1, LX/AsI;->j:Landroid/view/View;

    aget v0, v0, v2

    neg-int v0, v0

    int-to-float v0, v0

    iget-object v2, p0, LX/AsD;->a:LX/AsI;

    iget-object v2, v2, LX/AsI;->j:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getTranslationY()F

    move-result v2

    add-float/2addr v0, v2

    invoke-virtual {v1, v0}, Landroid/view/View;->setTranslationY(F)V

    .line 1720239
    :cond_0
    return-void
.end method
