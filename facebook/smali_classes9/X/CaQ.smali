.class public final LX/CaQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;)V
    .locals 0

    .prologue
    .line 1917539
    iput-object p1, p0, LX/CaQ;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x2bbfa53e

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1917540
    iget-object v0, p0, LX/CaQ;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->P:LX/CaZ;

    iget-object v2, p0, LX/CaQ;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v2, v2, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->O:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v2

    invoke-virtual {v0, v2}, LX/CaZ;->e(I)LX/5kD;

    move-result-object v2

    .line 1917541
    invoke-interface {v2}, LX/5kD;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1917542
    iget-object v0, p0, LX/CaQ;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->w:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9jH;

    iget-object v3, p0, LX/CaQ;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    invoke-virtual {v3}, Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;->b()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2}, LX/5kD;->E()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$ExplicitPlaceModel;

    move-result-object v2

    iget-object v4, p0, LX/CaQ;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    .line 1917543
    sget-object v5, LX/9jG;->PHOTO:LX/9jG;

    const-string p0, "edit_photo_location"

    invoke-static {v5, p0, v2}, LX/9jH;->a(LX/9jG;Ljava/lang/String;Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$ExplicitPlaceModel;)LX/9jF;

    move-result-object v5

    .line 1917544
    iput-object v3, v5, LX/9jF;->n:Ljava/lang/String;

    .line 1917545
    move-object v5, v5

    .line 1917546
    invoke-virtual {v5}, LX/9jF;->a()Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    move-result-object p0

    .line 1917547
    iget-object v5, v0, LX/9jH;->a:LX/0Or;

    invoke-interface {v5}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {v4}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1, p0}, LX/9jD;->a(Landroid/content/Context;Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;)Landroid/content/Intent;

    move-result-object p0

    const/16 p1, 0x138a

    invoke-interface {v5, p0, p1, v4}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 1917548
    :goto_0
    const v0, -0x1bc0da28

    invoke-static {v0, v1}, LX/02F;->a(II)V

    return-void

    .line 1917549
    :cond_0
    iget-object v0, p0, LX/CaQ;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;->w:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9jH;

    iget-object v3, p0, LX/CaQ;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    invoke-virtual {v3}, Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;->b()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2}, LX/5kD;->E()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$ExplicitPlaceModel;

    move-result-object v2

    iget-object v4, p0, LX/CaQ;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryFragment;

    .line 1917550
    sget-object v5, LX/9jG;->PHOTO:LX/9jG;

    const-string p0, "suggest_photo_location"

    invoke-static {v5, p0, v2}, LX/9jH;->a(LX/9jG;Ljava/lang/String;Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataWithoutFeedbackModel$ExplicitPlaceModel;)LX/9jF;

    move-result-object v5

    .line 1917551
    iput-object v3, v5, LX/9jF;->n:Ljava/lang/String;

    .line 1917552
    move-object v5, v5

    .line 1917553
    invoke-virtual {v5}, LX/9jF;->a()Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    move-result-object p0

    .line 1917554
    iget-object v5, v0, LX/9jH;->a:LX/0Or;

    invoke-interface {v5}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {v4}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1, p0}, LX/9jD;->a(Landroid/content/Context;Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;)Landroid/content/Intent;

    move-result-object p0

    const/16 p1, 0x138a

    invoke-interface {v5, p0, p1, v4}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 1917555
    goto :goto_0
.end method
