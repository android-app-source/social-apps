.class public LX/AYZ;
.super LX/AWT;
.source ""


# instance fields
.field public a:Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1685828
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/AYZ;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1685829
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1685826
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/AYZ;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1685827
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1685810
    invoke-direct {p0, p1, p2, p3}, LX/AWT;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1685811
    return-void
.end method

.method private static setContentViewAndInitializeVariables(LX/AYZ;I)V
    .locals 1

    .prologue
    .line 1685823
    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1685824
    const v0, 0x7f0d104f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;

    iput-object v0, p0, LX/AYZ;->a:Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;

    .line 1685825
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;LX/AVF;)V
    .locals 1

    .prologue
    .line 1685820
    invoke-super {p0, p1, p2}, LX/AWT;->a(Landroid/view/ViewGroup;LX/AVF;)V

    .line 1685821
    iget-object v0, p0, LX/AYZ;->a:Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->g()V

    .line 1685822
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 1685812
    if-eqz p1, :cond_0

    .line 1685813
    const v0, 0x7f0305ec

    invoke-static {p0, v0}, LX/AYZ;->setContentViewAndInitializeVariables(LX/AYZ;I)V

    .line 1685814
    const v0, 0x7f0d1050

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/FacecastPercentFrameLayout;

    .line 1685815
    const/high16 v1, 0x3f000000    # 0.5f

    invoke-virtual {v0, v1}, Lcom/facebook/facecastdisplay/FacecastPercentFrameLayout;->setPercent(F)V

    .line 1685816
    iget-object v0, p0, LX/AYZ;->a:Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;

    const/4 v1, 0x1

    .line 1685817
    iput-boolean v1, v0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsView;->D:Z

    .line 1685818
    :goto_0
    return-void

    .line 1685819
    :cond_0
    const v0, 0x7f0305eb

    invoke-static {p0, v0}, LX/AYZ;->setContentViewAndInitializeVariables(LX/AYZ;I)V

    goto :goto_0
.end method
