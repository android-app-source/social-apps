.class public final enum LX/Cff;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Cff;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Cff;

.field public static final enum REACTION_ATTACHMENTS_CLOSED:LX/Cff;

.field public static final enum REACTION_ATTACHMENT_INVALID:LX/Cff;

.field public static final enum REACTION_ATTACHMENT_INVALIDATED:LX/Cff;

.field public static final enum REACTION_ERROR:LX/Cff;

.field public static final enum REACTION_FETCH:LX/Cff;

.field public static final enum REACTION_FORWARD_SCROLL:LX/Cff;

.field public static final enum REACTION_HEADER_DISPLAYED:LX/Cff;

.field public static final enum REACTION_HEADER_INTERACTION:LX/Cff;

.field public static final enum REACTION_HSCROLL_COMPONENT_IMPRESSION:LX/Cff;

.field public static final enum REACTION_OVERLAY_CLOSED:LX/Cff;

.field public static final enum REACTION_OVERLAY_CLOSED_WITH_PLACE:LX/Cff;

.field public static final enum REACTION_OVERLAY_DISPLAYED:LX/Cff;

.field public static final enum REACTION_OVERLAY_DISPLAY_ABORTED:LX/Cff;

.field public static final enum REACTION_OVERLAY_ERROR:LX/Cff;

.field public static final enum REACTION_PAGE_ERROR:LX/Cff;

.field public static final enum REACTION_PAGE_LOAD:LX/Cff;

.field public static final enum REACTION_SCROLLED_TO_BOTTOM:LX/Cff;

.field public static final enum REACTION_UNIT_IMPRESSION:LX/Cff;

.field public static final enum REACTION_UNIT_INTERACTION:LX/Cff;


# instance fields
.field public final name:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1926672
    new-instance v0, LX/Cff;

    const-string v1, "REACTION_ATTACHMENT_INVALID"

    const-string v2, "reaction_attachment_invalid"

    invoke-direct {v0, v1, v4, v2}, LX/Cff;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cff;->REACTION_ATTACHMENT_INVALID:LX/Cff;

    .line 1926673
    new-instance v0, LX/Cff;

    const-string v1, "REACTION_ATTACHMENT_INVALIDATED"

    const-string v2, "reaction_attachment_invalidated"

    invoke-direct {v0, v1, v5, v2}, LX/Cff;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cff;->REACTION_ATTACHMENT_INVALIDATED:LX/Cff;

    .line 1926674
    new-instance v0, LX/Cff;

    const-string v1, "REACTION_ATTACHMENTS_CLOSED"

    const-string v2, "reaction_attachments_closed"

    invoke-direct {v0, v1, v6, v2}, LX/Cff;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cff;->REACTION_ATTACHMENTS_CLOSED:LX/Cff;

    .line 1926675
    new-instance v0, LX/Cff;

    const-string v1, "REACTION_ERROR"

    const-string v2, "reaction_error"

    invoke-direct {v0, v1, v7, v2}, LX/Cff;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cff;->REACTION_ERROR:LX/Cff;

    .line 1926676
    new-instance v0, LX/Cff;

    const-string v1, "REACTION_FETCH"

    const-string v2, "reaction_fetch"

    invoke-direct {v0, v1, v8, v2}, LX/Cff;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cff;->REACTION_FETCH:LX/Cff;

    .line 1926677
    new-instance v0, LX/Cff;

    const-string v1, "REACTION_FORWARD_SCROLL"

    const/4 v2, 0x5

    const-string v3, "reaction_forward_scroll"

    invoke-direct {v0, v1, v2, v3}, LX/Cff;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cff;->REACTION_FORWARD_SCROLL:LX/Cff;

    .line 1926678
    new-instance v0, LX/Cff;

    const-string v1, "REACTION_HEADER_DISPLAYED"

    const/4 v2, 0x6

    const-string v3, "reaction_header_displayed"

    invoke-direct {v0, v1, v2, v3}, LX/Cff;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cff;->REACTION_HEADER_DISPLAYED:LX/Cff;

    .line 1926679
    new-instance v0, LX/Cff;

    const-string v1, "REACTION_HEADER_INTERACTION"

    const/4 v2, 0x7

    const-string v3, "reaction_header_interaction"

    invoke-direct {v0, v1, v2, v3}, LX/Cff;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cff;->REACTION_HEADER_INTERACTION:LX/Cff;

    .line 1926680
    new-instance v0, LX/Cff;

    const-string v1, "REACTION_HSCROLL_COMPONENT_IMPRESSION"

    const/16 v2, 0x8

    const-string v3, "reaction_hscroll_component_impression"

    invoke-direct {v0, v1, v2, v3}, LX/Cff;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cff;->REACTION_HSCROLL_COMPONENT_IMPRESSION:LX/Cff;

    .line 1926681
    new-instance v0, LX/Cff;

    const-string v1, "REACTION_OVERLAY_CLOSED"

    const/16 v2, 0x9

    const-string v3, "reaction_overlay_closed"

    invoke-direct {v0, v1, v2, v3}, LX/Cff;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cff;->REACTION_OVERLAY_CLOSED:LX/Cff;

    .line 1926682
    new-instance v0, LX/Cff;

    const-string v1, "REACTION_OVERLAY_CLOSED_WITH_PLACE"

    const/16 v2, 0xa

    const-string v3, "reaction_overlay_closed_with_place"

    invoke-direct {v0, v1, v2, v3}, LX/Cff;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cff;->REACTION_OVERLAY_CLOSED_WITH_PLACE:LX/Cff;

    .line 1926683
    new-instance v0, LX/Cff;

    const-string v1, "REACTION_OVERLAY_DISPLAY_ABORTED"

    const/16 v2, 0xb

    const-string v3, "reaction_overlay_display_aborted"

    invoke-direct {v0, v1, v2, v3}, LX/Cff;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cff;->REACTION_OVERLAY_DISPLAY_ABORTED:LX/Cff;

    .line 1926684
    new-instance v0, LX/Cff;

    const-string v1, "REACTION_OVERLAY_DISPLAYED"

    const/16 v2, 0xc

    const-string v3, "reaction_overlay_displayed"

    invoke-direct {v0, v1, v2, v3}, LX/Cff;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cff;->REACTION_OVERLAY_DISPLAYED:LX/Cff;

    .line 1926685
    new-instance v0, LX/Cff;

    const-string v1, "REACTION_OVERLAY_ERROR"

    const/16 v2, 0xd

    const-string v3, "reaction_overlay_error"

    invoke-direct {v0, v1, v2, v3}, LX/Cff;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cff;->REACTION_OVERLAY_ERROR:LX/Cff;

    .line 1926686
    new-instance v0, LX/Cff;

    const-string v1, "REACTION_SCROLLED_TO_BOTTOM"

    const/16 v2, 0xe

    const-string v3, "reaction_scrolled_to_bottom"

    invoke-direct {v0, v1, v2, v3}, LX/Cff;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cff;->REACTION_SCROLLED_TO_BOTTOM:LX/Cff;

    .line 1926687
    new-instance v0, LX/Cff;

    const-string v1, "REACTION_PAGE_ERROR"

    const/16 v2, 0xf

    const-string v3, "reaction_page_error"

    invoke-direct {v0, v1, v2, v3}, LX/Cff;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cff;->REACTION_PAGE_ERROR:LX/Cff;

    .line 1926688
    new-instance v0, LX/Cff;

    const-string v1, "REACTION_PAGE_LOAD"

    const/16 v2, 0x10

    const-string v3, "reaction_page_load"

    invoke-direct {v0, v1, v2, v3}, LX/Cff;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cff;->REACTION_PAGE_LOAD:LX/Cff;

    .line 1926689
    new-instance v0, LX/Cff;

    const-string v1, "REACTION_UNIT_IMPRESSION"

    const/16 v2, 0x11

    const-string v3, "reaction_unit_impression"

    invoke-direct {v0, v1, v2, v3}, LX/Cff;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cff;->REACTION_UNIT_IMPRESSION:LX/Cff;

    .line 1926690
    new-instance v0, LX/Cff;

    const-string v1, "REACTION_UNIT_INTERACTION"

    const/16 v2, 0x12

    const-string v3, "reaction_unit_interaction"

    invoke-direct {v0, v1, v2, v3}, LX/Cff;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cff;->REACTION_UNIT_INTERACTION:LX/Cff;

    .line 1926691
    const/16 v0, 0x13

    new-array v0, v0, [LX/Cff;

    sget-object v1, LX/Cff;->REACTION_ATTACHMENT_INVALID:LX/Cff;

    aput-object v1, v0, v4

    sget-object v1, LX/Cff;->REACTION_ATTACHMENT_INVALIDATED:LX/Cff;

    aput-object v1, v0, v5

    sget-object v1, LX/Cff;->REACTION_ATTACHMENTS_CLOSED:LX/Cff;

    aput-object v1, v0, v6

    sget-object v1, LX/Cff;->REACTION_ERROR:LX/Cff;

    aput-object v1, v0, v7

    sget-object v1, LX/Cff;->REACTION_FETCH:LX/Cff;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/Cff;->REACTION_FORWARD_SCROLL:LX/Cff;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/Cff;->REACTION_HEADER_DISPLAYED:LX/Cff;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/Cff;->REACTION_HEADER_INTERACTION:LX/Cff;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/Cff;->REACTION_HSCROLL_COMPONENT_IMPRESSION:LX/Cff;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/Cff;->REACTION_OVERLAY_CLOSED:LX/Cff;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/Cff;->REACTION_OVERLAY_CLOSED_WITH_PLACE:LX/Cff;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/Cff;->REACTION_OVERLAY_DISPLAY_ABORTED:LX/Cff;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/Cff;->REACTION_OVERLAY_DISPLAYED:LX/Cff;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/Cff;->REACTION_OVERLAY_ERROR:LX/Cff;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/Cff;->REACTION_SCROLLED_TO_BOTTOM:LX/Cff;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/Cff;->REACTION_PAGE_ERROR:LX/Cff;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/Cff;->REACTION_PAGE_LOAD:LX/Cff;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/Cff;->REACTION_UNIT_IMPRESSION:LX/Cff;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/Cff;->REACTION_UNIT_INTERACTION:LX/Cff;

    aput-object v2, v0, v1

    sput-object v0, LX/Cff;->$VALUES:[LX/Cff;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1926692
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1926693
    iput-object p3, p0, LX/Cff;->name:Ljava/lang/String;

    .line 1926694
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Cff;
    .locals 1

    .prologue
    .line 1926695
    const-class v0, LX/Cff;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Cff;

    return-object v0
.end method

.method public static values()[LX/Cff;
    .locals 1

    .prologue
    .line 1926696
    sget-object v0, LX/Cff;->$VALUES:[LX/Cff;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Cff;

    return-object v0
.end method
