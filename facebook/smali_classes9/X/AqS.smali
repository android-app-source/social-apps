.class public LX/AqS;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1717933
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/1De;LX/1X1;LX/1X1;LX/1X1;)LX/1Dg;
    .locals 6
    .param p1    # LX/1X1;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p2    # LX/1X1;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # LX/1X1;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/1X1",
            "<*>;",
            "LX/1X1",
            "<*>;",
            "LX/1X1",
            "<*>;)",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x5

    const/4 v4, 0x1

    .line 1717934
    invoke-static {p0}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    const/4 v1, 0x2

    invoke-interface {v0, v1}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, v4}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v1

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-interface {v1, v0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    invoke-static {p0, p2}, LX/1n9;->a(LX/1De;LX/1X1;)LX/1Di;

    move-result-object v1

    const v2, 0x7f0b00e8

    invoke-interface {v1, v4, v2}, LX/1Di;->g(II)LX/1Di;

    move-result-object v1

    const v2, 0x7f0b00e5

    invoke-interface {v1, v5, v2}, LX/1Di;->c(II)LX/1Di;

    move-result-object v1

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-interface {v1, v2}, LX/1Di;->a(F)LX/1Di;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, p3}, LX/1Dh;->a(LX/1X1;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    return-object v0

    :cond_0
    invoke-static {p0, p1}, LX/1n9;->a(LX/1De;LX/1X1;)LX/1Di;

    move-result-object v0

    const v2, 0x7f0b00e2

    invoke-interface {v0, v2}, LX/1Di;->i(I)LX/1Di;

    move-result-object v0

    const v2, 0x7f0b00e1

    invoke-interface {v0, v2}, LX/1Di;->q(I)LX/1Di;

    move-result-object v0

    const/4 v2, 0x4

    const v3, 0x7f0b00dd

    invoke-interface {v0, v2, v3}, LX/1Di;->g(II)LX/1Di;

    move-result-object v0

    const v2, 0x7f0b00d9

    invoke-interface {v0, v4, v2}, LX/1Di;->g(II)LX/1Di;

    move-result-object v0

    const v2, 0x7f0b00df

    invoke-interface {v0, v5, v2}, LX/1Di;->g(II)LX/1Di;

    move-result-object v0

    const/4 v2, 0x3

    const v3, 0x7f0b00d4

    invoke-interface {v0, v2, v3}, LX/1Di;->g(II)LX/1Di;

    move-result-object v0

    goto :goto_0
.end method
