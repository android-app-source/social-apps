.class public final LX/ApU;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/ApW;",
        ">;"
    }
.end annotation


# static fields
.field private static b:[Ljava/lang/String;

.field private static c:I


# instance fields
.field public a:LX/ApV;

.field private d:Ljava/util/BitSet;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1716120
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "title"

    aput-object v2, v0, v1

    sput-object v0, LX/ApU;->b:[Ljava/lang/String;

    .line 1716121
    sput v3, LX/ApU;->c:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1716122
    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 1716123
    new-instance v0, Ljava/util/BitSet;

    sget v1, LX/ApU;->c:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/ApU;->d:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/ApU;LX/1De;IILX/ApV;)V
    .locals 1

    .prologue
    .line 1716127
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 1716128
    iput-object p4, p0, LX/ApU;->a:LX/ApV;

    .line 1716129
    iget-object v0, p0, LX/ApU;->d:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 1716130
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/CharSequence;)LX/ApU;
    .locals 2

    .prologue
    .line 1716124
    iget-object v0, p0, LX/ApU;->a:LX/ApV;

    iput-object p1, v0, LX/ApV;->a:Ljava/lang/CharSequence;

    .line 1716125
    iget-object v0, p0, LX/ApU;->d:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1716126
    return-object p0
.end method

.method public final a(Z)LX/ApU;
    .locals 1

    .prologue
    .line 1716118
    iget-object v0, p0, LX/ApU;->a:LX/ApV;

    iput-boolean p1, v0, LX/ApV;->d:Z

    .line 1716119
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 1716100
    invoke-super {p0}, LX/1X5;->a()V

    .line 1716101
    const/4 v0, 0x0

    iput-object v0, p0, LX/ApU;->a:LX/ApV;

    .line 1716102
    sget-object v0, LX/ApW;->b:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 1716103
    return-void
.end method

.method public final b(Ljava/lang/CharSequence;)LX/ApU;
    .locals 1

    .prologue
    .line 1716116
    iget-object v0, p0, LX/ApU;->a:LX/ApV;

    iput-object p1, v0, LX/ApV;->b:Ljava/lang/CharSequence;

    .line 1716117
    return-object p0
.end method

.method public final c(Ljava/lang/CharSequence;)LX/ApU;
    .locals 1

    .prologue
    .line 1716114
    iget-object v0, p0, LX/ApU;->a:LX/ApV;

    iput-object p1, v0, LX/ApV;->c:Ljava/lang/CharSequence;

    .line 1716115
    return-object p0
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/ApW;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1716104
    iget-object v1, p0, LX/ApU;->d:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/ApU;->d:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    sget v2, LX/ApU;->c:I

    if-ge v1, v2, :cond_2

    .line 1716105
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1716106
    :goto_0
    sget v2, LX/ApU;->c:I

    if-ge v0, v2, :cond_1

    .line 1716107
    iget-object v2, p0, LX/ApU;->d:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1716108
    sget-object v2, LX/ApU;->b:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1716109
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1716110
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1716111
    :cond_2
    iget-object v0, p0, LX/ApU;->a:LX/ApV;

    .line 1716112
    invoke-virtual {p0}, LX/ApU;->a()V

    .line 1716113
    return-object v0
.end method
