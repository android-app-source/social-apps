.class public final LX/COo;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/COp;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/CNb;

.field public b:LX/CNc;

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/CNb;",
            ">;"
        }
    .end annotation
.end field

.field public d:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1883597
    invoke-static {}, LX/COp;->q()LX/COp;

    move-result-object v0

    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1883598
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1883619
    const-string v0, "NTHScrollComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1883599
    if-ne p0, p1, :cond_1

    .line 1883600
    :cond_0
    :goto_0
    return v0

    .line 1883601
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1883602
    goto :goto_0

    .line 1883603
    :cond_3
    check-cast p1, LX/COo;

    .line 1883604
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1883605
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1883606
    if-eq v2, v3, :cond_0

    .line 1883607
    iget-object v2, p0, LX/COo;->a:LX/CNb;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/COo;->a:LX/CNb;

    iget-object v3, p1, LX/COo;->a:LX/CNb;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1883608
    goto :goto_0

    .line 1883609
    :cond_5
    iget-object v2, p1, LX/COo;->a:LX/CNb;

    if-nez v2, :cond_4

    .line 1883610
    :cond_6
    iget-object v2, p0, LX/COo;->b:LX/CNc;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/COo;->b:LX/CNc;

    iget-object v3, p1, LX/COo;->b:LX/CNc;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1883611
    goto :goto_0

    .line 1883612
    :cond_8
    iget-object v2, p1, LX/COo;->b:LX/CNc;

    if-nez v2, :cond_7

    .line 1883613
    :cond_9
    iget-object v2, p0, LX/COo;->c:Ljava/util/List;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/COo;->c:Ljava/util/List;

    iget-object v3, p1, LX/COo;->c:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 1883614
    goto :goto_0

    .line 1883615
    :cond_b
    iget-object v2, p1, LX/COo;->c:Ljava/util/List;

    if-nez v2, :cond_a

    .line 1883616
    :cond_c
    iget-object v2, p0, LX/COo;->d:Ljava/util/HashMap;

    if-eqz v2, :cond_d

    iget-object v2, p0, LX/COo;->d:Ljava/util/HashMap;

    iget-object v3, p1, LX/COo;->d:Ljava/util/HashMap;

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1883617
    goto :goto_0

    .line 1883618
    :cond_d
    iget-object v2, p1, LX/COo;->d:Ljava/util/HashMap;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
