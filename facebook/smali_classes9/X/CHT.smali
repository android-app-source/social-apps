.class public final LX/CHT;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingProductQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 1867669
    const-class v1, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingProductQueryModel;

    const v0, -0x38339ffb

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "InstantShoppingProductQuery"

    const-string v6, "941a92cec3b164c73cf978f78dc86759"

    const-string v7, "node"

    const-string v8, "10155217777506729"

    const-string v9, "10155259089036729"

    .line 1867670
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 1867671
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 1867672
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1867646
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1867647
    sparse-switch v0, :sswitch_data_0

    .line 1867648
    :goto_0
    return-object p1

    .line 1867649
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 1867650
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 1867651
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 1867652
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 1867653
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 1867654
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    .line 1867655
    :sswitch_6
    const-string p1, "6"

    goto :goto_0

    .line 1867656
    :sswitch_7
    const-string p1, "7"

    goto :goto_0

    .line 1867657
    :sswitch_8
    const-string p1, "8"

    goto :goto_0

    .line 1867658
    :sswitch_9
    const-string p1, "9"

    goto :goto_0

    .line 1867659
    :sswitch_a
    const-string p1, "10"

    goto :goto_0

    .line 1867660
    :sswitch_b
    const-string p1, "11"

    goto :goto_0

    .line 1867661
    :sswitch_c
    const-string p1, "12"

    goto :goto_0

    .line 1867662
    :sswitch_d
    const-string p1, "13"

    goto :goto_0

    .line 1867663
    :sswitch_e
    const-string p1, "14"

    goto :goto_0

    .line 1867664
    :sswitch_f
    const-string p1, "15"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x65578195 -> :sswitch_f
        -0x33f8633e -> :sswitch_2
        -0x15afd767 -> :sswitch_7
        -0x38aa96a -> :sswitch_b
        0x2ef6341 -> :sswitch_4
        0x683094a -> :sswitch_d
        0x2e61b03a -> :sswitch_a
        0x39f70a6c -> :sswitch_e
        0x3c79e3f5 -> :sswitch_6
        0x45e5f0b4 -> :sswitch_8
        0x66c8fb9c -> :sswitch_9
        0x687cca6b -> :sswitch_5
        0x7191d8b1 -> :sswitch_3
        0x73a026b5 -> :sswitch_1
        0x78668257 -> :sswitch_c
        0x78a3267b -> :sswitch_0
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1867665
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_1

    .line 1867666
    :goto_1
    return v0

    .line 1867667
    :pswitch_0
    const-string v2, "15"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    .line 1867668
    :pswitch_1
    const/16 v0, 0x14

    const-string v1, "%s"

    invoke-static {p2, v0, v1}, LX/0wE;->a(Ljava/lang/Object;ILjava/lang/String;)Z

    move-result v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x624
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method
