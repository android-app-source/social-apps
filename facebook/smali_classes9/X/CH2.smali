.class public final enum LX/CH2;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/CH2;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/CH2;

.field public static final enum ASYNC_FEED:LX/CH2;

.field public static final enum CAROUSEL:LX/CH2;

.field public static final enum FEED:LX/CH2;

.field public static final enum FEED_CHAINING:LX/CH2;

.field public static final enum INSTANT_ARTICLE:LX/CH2;

.field public static final enum MESSENGER:LX/CH2;

.field public static final enum OFFLINE_FEED:LX/CH2;

.field public static final enum TAROT_CARD:LX/CH2;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1866137
    new-instance v0, LX/CH2;

    const-string v1, "FEED"

    invoke-direct {v0, v1, v3}, LX/CH2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CH2;->FEED:LX/CH2;

    .line 1866138
    new-instance v0, LX/CH2;

    const-string v1, "ASYNC_FEED"

    invoke-direct {v0, v1, v4}, LX/CH2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CH2;->ASYNC_FEED:LX/CH2;

    .line 1866139
    new-instance v0, LX/CH2;

    const-string v1, "INSTANT_ARTICLE"

    invoke-direct {v0, v1, v5}, LX/CH2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CH2;->INSTANT_ARTICLE:LX/CH2;

    .line 1866140
    new-instance v0, LX/CH2;

    const-string v1, "FEED_CHAINING"

    invoke-direct {v0, v1, v6}, LX/CH2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CH2;->FEED_CHAINING:LX/CH2;

    .line 1866141
    new-instance v0, LX/CH2;

    const-string v1, "MESSENGER"

    invoke-direct {v0, v1, v7}, LX/CH2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CH2;->MESSENGER:LX/CH2;

    .line 1866142
    new-instance v0, LX/CH2;

    const-string v1, "CAROUSEL"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/CH2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CH2;->CAROUSEL:LX/CH2;

    .line 1866143
    new-instance v0, LX/CH2;

    const-string v1, "OFFLINE_FEED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/CH2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CH2;->OFFLINE_FEED:LX/CH2;

    .line 1866144
    new-instance v0, LX/CH2;

    const-string v1, "TAROT_CARD"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/CH2;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CH2;->TAROT_CARD:LX/CH2;

    .line 1866145
    const/16 v0, 0x8

    new-array v0, v0, [LX/CH2;

    sget-object v1, LX/CH2;->FEED:LX/CH2;

    aput-object v1, v0, v3

    sget-object v1, LX/CH2;->ASYNC_FEED:LX/CH2;

    aput-object v1, v0, v4

    sget-object v1, LX/CH2;->INSTANT_ARTICLE:LX/CH2;

    aput-object v1, v0, v5

    sget-object v1, LX/CH2;->FEED_CHAINING:LX/CH2;

    aput-object v1, v0, v6

    sget-object v1, LX/CH2;->MESSENGER:LX/CH2;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/CH2;->CAROUSEL:LX/CH2;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/CH2;->OFFLINE_FEED:LX/CH2;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/CH2;->TAROT_CARD:LX/CH2;

    aput-object v2, v0, v1

    sput-object v0, LX/CH2;->$VALUES:[LX/CH2;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1866146
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/CH2;
    .locals 1

    .prologue
    .line 1866147
    const-class v0, LX/CH2;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/CH2;

    return-object v0
.end method

.method public static values()[LX/CH2;
    .locals 1

    .prologue
    .line 1866148
    sget-object v0, LX/CH2;->$VALUES:[LX/CH2;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/CH2;

    return-object v0
.end method
