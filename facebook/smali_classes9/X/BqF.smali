.class public LX/BqF;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static j:LX/0Xm;


# instance fields
.field public final a:LX/0ad;

.field private b:Ljava/lang/Boolean;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private c:LX/2jw;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Ljava/lang/Boolean;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Ljava/lang/Boolean;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/Boolean;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/Boolean;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/Boolean;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/lang/Boolean;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0ad;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1824449
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1824450
    iput-object v0, p0, LX/BqF;->b:Ljava/lang/Boolean;

    .line 1824451
    iput-object v0, p0, LX/BqF;->c:LX/2jw;

    .line 1824452
    iput-object v0, p0, LX/BqF;->d:Ljava/lang/Boolean;

    .line 1824453
    iput-object v0, p0, LX/BqF;->e:Ljava/lang/Boolean;

    .line 1824454
    iput-object v0, p0, LX/BqF;->f:Ljava/lang/Boolean;

    .line 1824455
    iput-object v0, p0, LX/BqF;->g:Ljava/lang/Boolean;

    .line 1824456
    iput-object v0, p0, LX/BqF;->h:Ljava/lang/Boolean;

    .line 1824457
    iput-object v0, p0, LX/BqF;->i:Ljava/lang/Boolean;

    .line 1824458
    iput-object p1, p0, LX/BqF;->a:LX/0ad;

    .line 1824459
    return-void
.end method

.method public static a(LX/0QB;)LX/BqF;
    .locals 4

    .prologue
    .line 1824460
    const-class v1, LX/BqF;

    monitor-enter v1

    .line 1824461
    :try_start_0
    sget-object v0, LX/BqF;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1824462
    sput-object v2, LX/BqF;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1824463
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1824464
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1824465
    new-instance p0, LX/BqF;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-direct {p0, v3}, LX/BqF;-><init>(LX/0ad;)V

    .line 1824466
    move-object v0, p0

    .line 1824467
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1824468
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/BqF;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1824469
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1824470
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    .line 1824471
    iget-object v0, p0, LX/BqF;->b:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 1824472
    iget-object v0, p0, LX/BqF;->a:LX/0ad;

    sget-short v1, LX/BqE;->e:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LX/BqF;->b:Ljava/lang/Boolean;

    .line 1824473
    :cond_0
    iget-object v0, p0, LX/BqF;->b:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method
