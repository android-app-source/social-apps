.class public final LX/B2x;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Landroid/content/Intent;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/4BY;

.field public final synthetic b:Landroid/app/Activity;

.field public final synthetic c:LX/B2y;


# direct methods
.method public constructor <init>(LX/B2y;LX/4BY;Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 1739351
    iput-object p1, p0, LX/B2x;->c:LX/B2y;

    iput-object p2, p0, LX/B2x;->a:LX/4BY;

    iput-object p3, p0, LX/B2x;->b:Landroid/app/Activity;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method

.method private a(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 1739352
    iget-object v0, p0, LX/B2x;->a:LX/4BY;

    invoke-virtual {v0}, LX/4BY;->dismiss()V

    .line 1739353
    if-eqz p1, :cond_0

    .line 1739354
    iget-object v0, p0, LX/B2x;->c:LX/B2y;

    iget-object v0, v0, LX/B2y;->b:Lcom/facebook/content/SecureContextHelper;

    iget-object v1, p0, LX/B2x;->b:Landroid/app/Activity;

    invoke-interface {v0, p1, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1739355
    :cond_0
    iget-object v0, p0, LX/B2x;->b:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 1739356
    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1739357
    iget-object v0, p0, LX/B2x;->a:LX/4BY;

    invoke-virtual {v0}, LX/4BY;->dismiss()V

    .line 1739358
    return-void
.end method

.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1739359
    check-cast p1, Landroid/content/Intent;

    invoke-direct {p0, p1}, LX/B2x;->a(Landroid/content/Intent;)V

    return-void
.end method
