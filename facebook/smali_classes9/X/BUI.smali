.class public final LX/BUI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lorg/apache/http/client/ResponseHandler;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lorg/apache/http/client/ResponseHandler",
        "<",
        "Ljava/lang/Long;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/video/downloadmanager/VideoDownloadHandler;

.field private final b:Ljava/io/File;

.field private final c:Landroid/net/Uri;

.field private final d:Ljava/lang/String;

.field private final e:LX/BTy;

.field public f:Ljava/util/concurrent/Future;

.field private g:Ljava/lang/Exception;

.field private h:J


# direct methods
.method public constructor <init>(Lcom/facebook/video/downloadmanager/VideoDownloadHandler;Ljava/lang/String;Landroid/net/Uri;Ljava/io/File;LX/BTy;J)V
    .locals 0

    .prologue
    .line 1788811
    iput-object p1, p0, LX/BUI;->a:Lcom/facebook/video/downloadmanager/VideoDownloadHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1788812
    iput-object p4, p0, LX/BUI;->b:Ljava/io/File;

    .line 1788813
    iput-object p3, p0, LX/BUI;->c:Landroid/net/Uri;

    .line 1788814
    iput-object p2, p0, LX/BUI;->d:Ljava/lang/String;

    .line 1788815
    iput-object p5, p0, LX/BUI;->e:LX/BTy;

    .line 1788816
    iput-wide p6, p0, LX/BUI;->h:J

    .line 1788817
    return-void
.end method

.method private a(Lorg/apache/http/HttpResponse;)Ljava/lang/Long;
    .locals 14

    .prologue
    const/4 v1, 0x0

    const/16 v8, 0xce

    const/4 v10, -0x1

    .line 1788819
    iget-object v0, p0, LX/BUI;->b:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v6

    .line 1788820
    :try_start_0
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/StatusLine;->getStatusCode()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v9

    .line 1788821
    const/16 v0, 0xc8

    if-eq v9, v0, :cond_3

    if-eq v9, v8, :cond_3

    .line 1788822
    :try_start_1
    new-instance v0, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid HTTP Status code "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1788823
    :catch_0
    move-exception v0

    move-object v2, v0

    move v3, v9

    move-object v4, v1

    move-object v5, v1

    move-wide v0, v6

    .line 1788824
    :goto_0
    :try_start_2
    invoke-direct {p0}, LX/BUI;->a()Z

    move-result v6

    if-nez v6, :cond_0

    .line 1788825
    iget-object v7, p0, LX/BUI;->d:Ljava/lang/String;

    iget-object v8, p0, LX/BUI;->e:LX/BTy;

    .line 1788826
    invoke-static {v7, v2, v8, v3}, Lcom/facebook/video/downloadmanager/VideoDownloadHandler;->a(Ljava/lang/String;Ljava/lang/Exception;LX/BTy;I)V

    .line 1788827
    :cond_0
    iput-object v2, p0, LX/BUI;->g:Ljava/lang/Exception;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_4

    .line 1788828
    if-eqz v5, :cond_1

    .line 1788829
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V

    .line 1788830
    :cond_1
    if-eqz v4, :cond_2

    .line 1788831
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V

    .line 1788832
    :cond_2
    :goto_1
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0

    .line 1788833
    :cond_3
    :try_start_3
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    .line 1788834
    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v11

    .line 1788835
    :try_start_4
    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->getContentLength()J

    move-result-wide v2

    iget-object v4, p0, LX/BUI;->b:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->length()J

    move-result-wide v4

    add-long/2addr v4, v2

    .line 1788836
    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->getContentLength()J

    .line 1788837
    iget-object v2, p0, LX/BUI;->b:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_5

    if-ne v9, v8, :cond_5

    .line 1788838
    new-instance v12, Ljava/io/FileOutputStream;

    iget-object v0, p0, LX/BUI;->b:Ljava/io/File;

    const/4 v2, 0x1

    invoke-direct {v12, v0, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1788839
    :goto_2
    const-wide/16 v0, -0x1

    cmp-long v0, v4, v0

    if-nez v0, :cond_4

    .line 1788840
    :try_start_5
    iget-wide v4, p0, LX/BUI;->h:J

    .line 1788841
    :cond_4
    iget-object v1, p0, LX/BUI;->a:Lcom/facebook/video/downloadmanager/VideoDownloadHandler;

    iget-object v2, p0, LX/BUI;->d:Ljava/lang/String;

    iget-object v3, p0, LX/BUI;->c:Landroid/net/Uri;

    iget-object v8, p0, LX/BUI;->e:LX/BTy;

    invoke-static/range {v1 .. v8}, Lcom/facebook/video/downloadmanager/VideoDownloadHandler;->a(Lcom/facebook/video/downloadmanager/VideoDownloadHandler;Ljava/lang/String;Landroid/net/Uri;JJLX/BTy;)V

    .line 1788842
    const/high16 v0, 0x10000

    new-array v13, v0, [B

    .line 1788843
    const-wide/32 v0, 0x10000

    sub-long v2, v4, v6

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    long-to-int v0, v0

    .line 1788844
    :goto_3
    if-lez v0, :cond_6

    .line 1788845
    const/4 v1, 0x0

    invoke-virtual {v11, v13, v1, v0}, Ljava/io/InputStream;->read([BII)I

    move-result v0

    .line 1788846
    if-eq v0, v10, :cond_6

    .line 1788847
    const/4 v1, 0x0

    invoke-virtual {v12, v13, v1, v0}, Ljava/io/FileOutputStream;->write([BII)V

    .line 1788848
    int-to-long v0, v0

    add-long/2addr v6, v0

    .line 1788849
    iget-object v1, p0, LX/BUI;->a:Lcom/facebook/video/downloadmanager/VideoDownloadHandler;

    iget-object v2, p0, LX/BUI;->d:Ljava/lang/String;

    iget-object v3, p0, LX/BUI;->c:Landroid/net/Uri;

    iget-object v8, p0, LX/BUI;->e:LX/BTy;

    invoke-static/range {v1 .. v8}, Lcom/facebook/video/downloadmanager/VideoDownloadHandler;->a(Lcom/facebook/video/downloadmanager/VideoDownloadHandler;Ljava/lang/String;Landroid/net/Uri;JJLX/BTy;)V

    .line 1788850
    invoke-direct {p0}, LX/BUI;->a()Z

    move-result v0

    if-nez v0, :cond_6

    .line 1788851
    const-wide/32 v0, 0x10000

    sub-long v2, v4, v6

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_4
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    move-result-wide v0

    long-to-int v0, v0

    goto :goto_3

    .line 1788852
    :cond_5
    :try_start_6
    new-instance v2, Ljava/io/FileOutputStream;

    iget-object v3, p0, LX/BUI;->b:Ljava/io/File;

    invoke-direct {v2, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 1788853
    :try_start_7
    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->getContentLength()J
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_3
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    move-result-wide v4

    .line 1788854
    const-wide/16 v6, 0x0

    move-object v12, v2

    goto :goto_2

    :cond_6
    move-wide v0, v6

    .line 1788855
    cmp-long v2, v0, v4

    if-eqz v2, :cond_7

    :try_start_8
    invoke-direct {p0}, LX/BUI;->a()Z

    move-result v2

    if-nez v2, :cond_7

    .line 1788856
    iget-object v3, p0, LX/BUI;->d:Ljava/lang/String;

    new-instance v4, Ljava/io/IOException;

    const-string v5, "Unexpected end of stream"

    invoke-direct {v4, v5}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, LX/BUI;->e:LX/BTy;

    .line 1788857
    invoke-static {v3, v4, v5, v9}, Lcom/facebook/video/downloadmanager/VideoDownloadHandler;->a(Ljava/lang/String;Ljava/lang/Exception;LX/BTy;I)V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_5
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    .line 1788858
    :cond_7
    invoke-virtual {v12}, Ljava/io/FileOutputStream;->close()V

    .line 1788859
    if-eqz v11, :cond_2

    .line 1788860
    invoke-virtual {v11}, Ljava/io/InputStream;->close()V

    goto/16 :goto_1

    .line 1788861
    :catchall_0
    move-exception v0

    move-object v11, v1

    :goto_4
    if-eqz v1, :cond_8

    .line 1788862
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    .line 1788863
    :cond_8
    if-eqz v11, :cond_9

    .line 1788864
    invoke-virtual {v11}, Ljava/io/InputStream;->close()V

    :cond_9
    throw v0

    .line 1788865
    :catchall_1
    move-exception v0

    goto :goto_4

    :catchall_2
    move-exception v0

    move-object v1, v2

    goto :goto_4

    :catchall_3
    move-exception v0

    move-object v1, v12

    goto :goto_4

    :catchall_4
    move-exception v0

    move-object v11, v4

    move-object v1, v5

    goto :goto_4

    .line 1788866
    :catch_1
    move-exception v0

    move-object v2, v0

    move v3, v10

    move-object v4, v1

    move-object v5, v1

    move-wide v0, v6

    goto/16 :goto_0

    :catch_2
    move-exception v0

    move-object v2, v0

    move v3, v9

    move-object v4, v11

    move-object v5, v1

    move-wide v0, v6

    goto/16 :goto_0

    :catch_3
    move-exception v0

    move v3, v9

    move-object v4, v11

    move-object v5, v2

    move-object v2, v0

    move-wide v0, v6

    goto/16 :goto_0

    :catch_4
    move-exception v0

    move-object v2, v0

    move v3, v9

    move-object v4, v11

    move-object v5, v12

    move-wide v0, v6

    goto/16 :goto_0

    :catch_5
    move-exception v2

    move v3, v9

    move-object v4, v11

    move-object v5, v12

    goto/16 :goto_0
.end method

.method private a()Z
    .locals 1

    .prologue
    .line 1788867
    iget-object v0, p0, LX/BUI;->f:Ljava/util/concurrent/Future;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/BUI;->f:Ljava/util/concurrent/Future;

    invoke-interface {v0}, Ljava/util/concurrent/Future;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final synthetic handleResponse(Lorg/apache/http/HttpResponse;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1788818
    invoke-direct {p0, p1}, LX/BUI;->a(Lorg/apache/http/HttpResponse;)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method
