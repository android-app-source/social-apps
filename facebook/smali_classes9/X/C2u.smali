.class public LX/C2u;
.super LX/5Jq;
.source ""


# instance fields
.field private final b:LX/1De;

.field private final c:LX/C2s;

.field private final d:LX/1DR;

.field private final e:F

.field private final f:Lcom/facebook/common/callercontext/CallerContext;

.field private final g:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/C2l;


# direct methods
.method public constructor <init>(LX/C2s;LX/1DR;LX/1De;IFLcom/facebook/common/callercontext/CallerContext;LX/0Px;LX/C2l;)V
    .locals 1
    .param p3    # LX/1De;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # I
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # F
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # LX/0Px;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p8    # LX/C2l;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/C2s;",
            "LX/1DR;",
            "LX/1De;",
            "IF",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/C2l;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1844755
    invoke-direct {p0, p3, p4, p5}, LX/5Jq;-><init>(Landroid/content/Context;IF)V

    .line 1844756
    iput-object p1, p0, LX/C2u;->c:LX/C2s;

    .line 1844757
    iput-object p2, p0, LX/C2u;->d:LX/1DR;

    .line 1844758
    iput-object p3, p0, LX/C2u;->b:LX/1De;

    .line 1844759
    iput p5, p0, LX/C2u;->e:F

    .line 1844760
    iput-object p6, p0, LX/C2u;->f:Lcom/facebook/common/callercontext/CallerContext;

    .line 1844761
    invoke-static {p7}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    iput-object v0, p0, LX/C2u;->g:LX/0Px;

    .line 1844762
    iput-object p8, p0, LX/C2u;->h:LX/C2l;

    .line 1844763
    return-void
.end method


# virtual methods
.method public final a(LX/1De;I)LX/1X1;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "I)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1844764
    iget-object v0, p0, LX/C2u;->c:LX/C2s;

    iget-object v3, p0, LX/C2u;->b:LX/1De;

    const/4 v4, 0x0

    .line 1844765
    new-instance v5, Lcom/facebook/feedplugins/commerce/SingleProductImageComponent$SingleProductImageComponentImpl;

    invoke-direct {v5, v0}, Lcom/facebook/feedplugins/commerce/SingleProductImageComponent$SingleProductImageComponentImpl;-><init>(LX/C2s;)V

    .line 1844766
    sget-object p1, LX/C2s;->a:LX/0Zi;

    invoke-virtual {p1}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/C2r;

    .line 1844767
    if-nez p1, :cond_0

    .line 1844768
    new-instance p1, LX/C2r;

    invoke-direct {p1}, LX/C2r;-><init>()V

    .line 1844769
    :cond_0
    invoke-static {p1, v3, v4, v4, v5}, LX/C2r;->a$redex0(LX/C2r;LX/1De;IILcom/facebook/feedplugins/commerce/SingleProductImageComponent$SingleProductImageComponentImpl;)V

    .line 1844770
    move-object v5, p1

    .line 1844771
    move-object v4, v5

    .line 1844772
    move-object v0, v4

    .line 1844773
    iget-object v3, p0, LX/C2u;->d:LX/1DR;

    invoke-virtual {v3}, LX/1DR;->a()I

    move-result v3

    int-to-float v3, v3

    iget v4, p0, LX/C2u;->e:F

    mul-float/2addr v3, v4

    float-to-int v3, v3

    .line 1844774
    iget-object v4, v0, LX/C2r;->a:Lcom/facebook/feedplugins/commerce/SingleProductImageComponent$SingleProductImageComponentImpl;

    iput v3, v4, Lcom/facebook/feedplugins/commerce/SingleProductImageComponent$SingleProductImageComponentImpl;->a:I

    .line 1844775
    iget-object v4, v0, LX/C2r;->d:Ljava/util/BitSet;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Ljava/util/BitSet;->set(I)V

    .line 1844776
    move-object v3, v0

    .line 1844777
    iget-object v0, p0, LX/C2u;->g:LX/0Px;

    invoke-virtual {v0, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1844778
    iget-object v4, v3, LX/C2r;->a:Lcom/facebook/feedplugins/commerce/SingleProductImageComponent$SingleProductImageComponentImpl;

    iput-object v0, v4, Lcom/facebook/feedplugins/commerce/SingleProductImageComponent$SingleProductImageComponentImpl;->b:Ljava/lang/String;

    .line 1844779
    iget-object v4, v3, LX/C2r;->d:Ljava/util/BitSet;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Ljava/util/BitSet;->set(I)V

    .line 1844780
    move-object v0, v3

    .line 1844781
    iget-object v3, p0, LX/C2u;->h:LX/C2l;

    .line 1844782
    iget-object v4, v0, LX/C2r;->a:Lcom/facebook/feedplugins/commerce/SingleProductImageComponent$SingleProductImageComponentImpl;

    iput-object v3, v4, Lcom/facebook/feedplugins/commerce/SingleProductImageComponent$SingleProductImageComponentImpl;->f:LX/C2l;

    .line 1844783
    move-object v0, v0

    .line 1844784
    iget-object v3, p0, LX/C2u;->f:Lcom/facebook/common/callercontext/CallerContext;

    .line 1844785
    iget-object v4, v0, LX/C2r;->a:Lcom/facebook/feedplugins/commerce/SingleProductImageComponent$SingleProductImageComponentImpl;

    iput-object v3, v4, Lcom/facebook/feedplugins/commerce/SingleProductImageComponent$SingleProductImageComponentImpl;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 1844786
    iget-object v4, v0, LX/C2r;->d:Ljava/util/BitSet;

    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Ljava/util/BitSet;->set(I)V

    .line 1844787
    move-object v3, v0

    .line 1844788
    if-nez p2, :cond_1

    move v0, v1

    .line 1844789
    :goto_0
    iget-object v4, v3, LX/C2r;->a:Lcom/facebook/feedplugins/commerce/SingleProductImageComponent$SingleProductImageComponentImpl;

    iput-boolean v0, v4, Lcom/facebook/feedplugins/commerce/SingleProductImageComponent$SingleProductImageComponentImpl;->d:Z

    .line 1844790
    move-object v0, v3

    .line 1844791
    iget-object v3, p0, LX/C2u;->g:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ne p2, v3, :cond_2

    .line 1844792
    :goto_1
    iget-object v2, v0, LX/C2r;->a:Lcom/facebook/feedplugins/commerce/SingleProductImageComponent$SingleProductImageComponentImpl;

    iput-boolean v1, v2, Lcom/facebook/feedplugins/commerce/SingleProductImageComponent$SingleProductImageComponentImpl;->e:Z

    .line 1844793
    move-object v0, v0

    .line 1844794
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 1844795
    iget-object v0, p0, LX/C2u;->g:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 1844796
    const/4 v0, 0x0

    return v0
.end method
