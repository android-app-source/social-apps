.class public final LX/Alb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# instance fields
.field public final synthetic a:Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;)V
    .locals 0

    .prologue
    .line 1709920
    iput-object p1, p0, LX/Alb;->a:Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 1709907
    return-void
.end method

.method public final onAnimationEnd(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 1709919
    return-void
.end method

.method public final onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 1709918
    return-void
.end method

.method public final onAnimationStart(Landroid/animation/Animator;)V
    .locals 3

    .prologue
    .line 1709908
    iget-object v0, p0, LX/Alb;->a:Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;

    iget-object v0, v0, Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;->i:LX/Ali;

    invoke-virtual {v0}, LX/Ali;->b()V

    .line 1709909
    iget-object v0, p0, LX/Alb;->a:Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;

    iget-object v0, v0, Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;->d:LX/Alf;

    iget-object v1, p0, LX/Alb;->a:Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;

    iget-object v1, v1, Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;->g:Ljava/lang/String;

    iget-object v2, p0, LX/Alb;->a:Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;

    iget-object v2, v2, Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;->h:LX/1lR;

    invoke-virtual {v2}, LX/1lR;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/Alf;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1709910
    iget-object v0, p0, LX/Alb;->a:Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;

    iget-object v0, v0, Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;->c:LX/1kG;

    invoke-virtual {v0}, LX/1kG;->c()V

    .line 1709911
    iget-object v0, p0, LX/Alb;->a:Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;

    iget-object v0, v0, Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Alb;->a:Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;

    iget-object v0, v0, Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;->j:Ljava/lang/String;

    iget-object v1, p0, LX/Alb;->a:Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;

    iget-object v1, v1, Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;->h:LX/1lR;

    invoke-virtual {v1}, LX/1lR;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1709912
    :cond_0
    iget-object v0, p0, LX/Alb;->a:Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;

    iget-object v1, p0, LX/Alb;->a:Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;

    iget-object v1, v1, Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;->h:LX/1lR;

    invoke-virtual {v1}, LX/1lR;->b()Ljava/lang/String;

    move-result-object v1

    .line 1709913
    iput-object v1, v0, Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;->j:Ljava/lang/String;

    .line 1709914
    iget-object v0, p0, LX/Alb;->a:Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1709915
    iput-object v1, v0, Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;->g:Ljava/lang/String;

    .line 1709916
    iget-object v0, p0, LX/Alb;->a:Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;

    iget-object v1, p0, LX/Alb;->a:Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;

    iget-object v1, v1, Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;->setSessionID(Ljava/lang/String;)V

    .line 1709917
    :cond_1
    return-void
.end method
