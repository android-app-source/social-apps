.class public LX/CAA;
.super LX/CA5;
.source ""

# interfaces
.implements LX/CA4;


# instance fields
.field public a:LX/0SG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0qn;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final d:Landroid/widget/ProgressBar;

.field private final e:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private f:LX/0TF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0TF",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field private g:LX/0TF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0TF",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1855016
    invoke-direct {p0, p1}, LX/CA5;-><init>(Landroid/content/Context;)V

    .line 1855017
    const-class v0, LX/CAA;

    invoke-static {v0, p0}, LX/CAA;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1855018
    const v0, 0x7f030c62

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1855019
    const v0, 0x7f0d0008

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, LX/CAA;->d:Landroid/widget/ProgressBar;

    .line 1855020
    iget-object v0, p0, LX/CAA;->d:Landroid/widget/ProgressBar;

    const/16 v1, 0x3e8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 1855021
    new-instance v1, LX/0zw;

    const v0, 0x7f0d1e77

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-direct {v1, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    iput-object v1, p0, LX/CAA;->e:LX/0zw;

    .line 1855022
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/CAA;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v1

    check-cast v1, LX/0SG;

    invoke-static {p0}, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->a(LX/0QB;)Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    move-result-object v2

    check-cast v2, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    invoke-static {p0}, LX/0qn;->a(LX/0QB;)LX/0qn;

    move-result-object p0

    check-cast p0, LX/0qn;

    iput-object v1, p1, LX/CAA;->a:LX/0SG;

    iput-object v2, p1, LX/CAA;->b:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    iput-object p0, p1, LX/CAA;->c:LX/0qn;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1855014
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, LX/CAA;->setVisibility(I)V

    .line 1855015
    return-void
.end method

.method public final a(Landroid/view/View$OnClickListener;Landroid/view/View$OnTouchListener;)V
    .locals 2
    .param p1    # Landroid/view/View$OnClickListener;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Landroid/view/View$OnTouchListener;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1855007
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 1855008
    iget-object v0, p0, LX/CAA;->e:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/FbImageView;

    .line 1855009
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/FbImageView;->setVisibility(I)V

    .line 1855010
    invoke-virtual {v0, p1}, Lcom/facebook/widget/FbImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1855011
    invoke-virtual {v0, p2}, Lcom/facebook/widget/FbImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1855012
    :goto_0
    return-void

    .line 1855013
    :cond_0
    iget-object v0, p0, LX/CAA;->e:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->c()V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1854994
    iget-object v0, p0, LX/CAA;->b:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->S()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->d(Ljava/lang/String;)Lcom/facebook/composer/publish/common/PendingStory;

    move-result-object v0

    .line 1854995
    if-nez v0, :cond_1

    .line 1854996
    :cond_0
    :goto_0
    return-void

    .line 1854997
    :cond_1
    iget-object v1, p0, LX/CAA;->c:LX/0qn;

    invoke-virtual {v1, p1}, LX/0qn;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->FAILED:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    if-ne v1, v2, :cond_2

    .line 1854998
    iget-object v1, p0, LX/CAA;->a:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/facebook/composer/publish/common/PendingStory;->b(J)V

    .line 1854999
    :cond_2
    iget-object v1, p0, LX/CAA;->a:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/facebook/composer/publish/common/PendingStory;->a(J)I

    move-result v1

    .line 1855000
    invoke-virtual {p0, v1}, LX/CA5;->setProgress(I)V

    .line 1855001
    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/PendingStory;->k()Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, LX/CAA;->f:LX/0TF;

    if-eqz v1, :cond_3

    .line 1855002
    iget-object v0, p0, LX/CAA;->f:LX/0TF;

    invoke-interface {v0, p1}, LX/0TF;->onSuccess(Ljava/lang/Object;)V

    .line 1855003
    iput-object v4, p0, LX/CAA;->f:LX/0TF;

    goto :goto_0

    .line 1855004
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/PendingStory;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/CAA;->g:LX/0TF;

    if-eqz v0, :cond_0

    .line 1855005
    iget-object v0, p0, LX/CAA;->g:LX/0TF;

    invoke-interface {v0, p1}, LX/0TF;->onSuccess(Ljava/lang/Object;)V

    .line 1855006
    iput-object v4, p0, LX/CAA;->g:LX/0TF;

    goto :goto_0
.end method

.method public getOfflineHeaderView()Landroid/view/View;
    .locals 1

    .prologue
    .line 1854984
    iget-object v0, p0, LX/CAA;->d:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method public setCallbackOnProgressComplete(LX/0TF;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0TF",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1854992
    iput-object p1, p0, LX/CAA;->f:LX/0TF;

    .line 1854993
    return-void
.end method

.method public setCallbackOnProgressStarted(LX/0TF;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0TF",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1854990
    iput-object p1, p0, LX/CAA;->g:LX/0TF;

    .line 1854991
    return-void
.end method

.method public setProgress(I)V
    .locals 2

    .prologue
    .line 1854986
    const/16 v0, 0x3e8

    if-gt p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "argument must be less than 1000"

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1854987
    iget-object v0, p0, LX/CAA;->d:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 1854988
    return-void

    .line 1854989
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setStoryIsWaitingForWifi(Z)V
    .locals 0

    .prologue
    .line 1854985
    return-void
.end method
