.class public LX/BVS;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/BVh;


# direct methods
.method public constructor <init>(LX/BVh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1791013
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1791014
    iput-object p1, p0, LX/BVS;->a:LX/BVh;

    .line 1791015
    return-void
.end method

.method public static b(LX/0QB;)LX/BVS;
    .locals 2

    .prologue
    .line 1791035
    new-instance v1, LX/BVS;

    invoke-static {p0}, LX/BVh;->a(LX/0QB;)LX/BVh;

    move-result-object v0

    check-cast v0, LX/BVh;

    invoke-direct {v1, v0}, LX/BVS;-><init>(LX/BVh;)V

    .line 1791036
    return-object v1
.end method


# virtual methods
.method public final a(Ljava/lang/String;Landroid/view/ViewGroup;Landroid/content/Context;)Landroid/view/View;
    .locals 4

    .prologue
    .line 1791020
    :try_start_0
    invoke-static {}, Ljavax/xml/parsers/DocumentBuilderFactory;->newInstance()Ljavax/xml/parsers/DocumentBuilderFactory;

    move-result-object v0

    .line 1791021
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljavax/xml/parsers/DocumentBuilderFactory;->setNamespaceAware(Z)V

    .line 1791022
    invoke-virtual {v0}, Ljavax/xml/parsers/DocumentBuilderFactory;->newDocumentBuilder()Ljavax/xml/parsers/DocumentBuilder;

    move-result-object v0

    .line 1791023
    new-instance v1, Lorg/xml/sax/InputSource;

    new-instance v2, Ljava/io/StringReader;

    invoke-direct {v2, p1}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Lorg/xml/sax/InputSource;-><init>(Ljava/io/Reader;)V

    invoke-virtual {v0, v1}, Ljavax/xml/parsers/DocumentBuilder;->parse(Lorg/xml/sax/InputSource;)Lorg/w3c/dom/Document;

    move-result-object v0

    .line 1791024
    invoke-interface {v0}, Lorg/w3c/dom/Document;->getDocumentElement()Lorg/w3c/dom/Element;

    move-result-object v0

    .line 1791025
    move-object v0, v0

    .line 1791026
    iget-object v1, p0, LX/BVS;->a:LX/BVh;

    invoke-interface {v0}, Lorg/w3c/dom/Element;->getNodeName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/BVh;->a(Ljava/lang/String;)LX/BVT;

    move-result-object v1

    iget-object v2, p0, LX/BVS;->a:LX/BVh;

    invoke-virtual {v1, v0, p2, v2, p3}, LX/BVT;->a(Lorg/w3c/dom/Node;Landroid/view/ViewGroup;LX/BVh;Landroid/content/Context;)Landroid/view/View;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljavax/xml/parsers/ParserConfigurationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lorg/xml/sax/SAXException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    move-result-object v0

    return-object v0

    .line 1791027
    :catch_0
    move-exception v0

    .line 1791028
    new-instance v1, LX/BVR;

    invoke-direct {v1, v0}, LX/BVR;-><init>(Ljava/lang/Exception;)V

    throw v1

    .line 1791029
    :catch_1
    move-exception v0

    .line 1791030
    new-instance v1, LX/BVR;

    invoke-direct {v1, v0}, LX/BVR;-><init>(Ljava/lang/Exception;)V

    throw v1

    .line 1791031
    :catch_2
    move-exception v0

    .line 1791032
    new-instance v1, LX/BVR;

    invoke-direct {v1, v0}, LX/BVR;-><init>(Ljava/lang/Exception;)V

    throw v1

    .line 1791033
    :catch_3
    move-exception v0

    .line 1791034
    new-instance v1, LX/BVR;

    invoke-direct {v1, v0}, LX/BVR;-><init>(Ljava/lang/Exception;)V

    throw v1
.end method

.method public final a(Ljava/lang/String;)Ljava/lang/Integer;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1791016
    iget-object v0, p0, LX/BVS;->a:LX/BVh;

    .line 1791017
    iget-object p0, v0, LX/BVh;->g:LX/BW5;

    move-object v0, p0

    .line 1791018
    iget-object p0, v0, LX/BW5;->a:Ljava/util/Map;

    invoke-interface {p0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/Integer;

    move-object v0, p0

    .line 1791019
    return-object v0
.end method
