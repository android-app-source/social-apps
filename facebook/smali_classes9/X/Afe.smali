.class public LX/Afe;
.super LX/AeL;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/AeL",
        "<",
        "LX/Afd;",
        ">;"
    }
.end annotation


# instance fields
.field private final m:Lcom/facebook/resources/ui/FbTextView;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1699478
    invoke-direct {p0, p1}, LX/AeL;-><init>(Landroid/view/View;)V

    .line 1699479
    const v0, 0x7f0d197e

    invoke-virtual {p0, v0}, LX/AeK;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/Afe;->m:Lcom/facebook/resources/ui/FbTextView;

    .line 1699480
    return-void
.end method

.method private a(LX/Afd;LX/AeU;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 1699481
    invoke-super {p0, p1, p2}, LX/AeL;->a(LX/AeP;LX/AeU;)V

    .line 1699482
    iget-object v0, p0, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1699483
    iget-object v1, p2, LX/AeU;->a:Ljava/lang/String;

    iget-object v2, p1, LX/Afd;->c:LX/Aeu;

    iget-object v2, v2, LX/AeP;->a:LX/AcC;

    iget-object v2, v2, LX/AcC;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 1699484
    new-instance v2, LX/47x;

    invoke-direct {v2, v0}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    .line 1699485
    iget-object v3, p1, LX/Afd;->b:LX/AcC;

    iget-object v4, p1, LX/Afd;->c:LX/Aeu;

    iget-object v4, v4, LX/AeP;->a:LX/AcC;

    invoke-static {v3, v4}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    .line 1699486
    iget-object v4, p0, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    .line 1699487
    if-eqz v1, :cond_0

    .line 1699488
    const v1, 0x7f080bf8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    .line 1699489
    invoke-static {v5, v4, v3, v2}, LX/3Hk;->a(ILandroid/content/Context;Ljava/util/List;LX/47x;)V

    .line 1699490
    const-string v0, "%2$s"

    iget-object v1, p1, LX/Afd;->c:LX/Aeu;

    iget-object v1, v1, LX/Aeu;->b:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, LX/47x;->a(Ljava/lang/String;Ljava/lang/CharSequence;)LX/47x;

    .line 1699491
    :goto_0
    iget-object v0, p0, LX/Afe;->m:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v2}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1699492
    return-void

    .line 1699493
    :cond_0
    const v1, 0x7f080bf9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    .line 1699494
    invoke-static {v5, v4, v3, v2}, LX/3Hk;->a(ILandroid/content/Context;Ljava/util/List;LX/47x;)V

    .line 1699495
    const/4 v0, 0x2

    invoke-static {v0, v4, v3, v2}, LX/3Hk;->a(ILandroid/content/Context;Ljava/util/List;LX/47x;)V

    .line 1699496
    const-string v0, "%3$s"

    iget-object v1, p1, LX/Afd;->c:LX/Aeu;

    iget-object v1, v1, LX/Aeu;->b:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, LX/47x;->a(Ljava/lang/String;Ljava/lang/CharSequence;)LX/47x;

    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic a(LX/AeO;LX/AeU;)V
    .locals 0

    .prologue
    .line 1699497
    check-cast p1, LX/Afd;

    invoke-direct {p0, p1, p2}, LX/Afe;->a(LX/Afd;LX/AeU;)V

    return-void
.end method

.method public final bridge synthetic a(LX/AeP;LX/AeU;)V
    .locals 0

    .prologue
    .line 1699498
    check-cast p1, LX/Afd;

    invoke-direct {p0, p1, p2}, LX/Afe;->a(LX/Afd;LX/AeU;)V

    return-void
.end method
