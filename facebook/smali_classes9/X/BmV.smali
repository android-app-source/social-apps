.class public final LX/BmV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/BmO;


# instance fields
.field public final synthetic a:Lcom/facebook/events/ui/date/StartAndEndTimePicker;


# direct methods
.method public constructor <init>(Lcom/facebook/events/ui/date/StartAndEndTimePicker;)V
    .locals 0

    .prologue
    .line 1818410
    iput-object p1, p0, LX/BmV;->a:Lcom/facebook/events/ui/date/StartAndEndTimePicker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/Calendar;)V
    .locals 3

    .prologue
    .line 1818411
    if-nez p1, :cond_1

    .line 1818412
    iget-object v0, p0, LX/BmV;->a:Lcom/facebook/events/ui/date/StartAndEndTimePicker;

    iget-object v0, v0, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->e:Lcom/facebook/events/ui/date/DatePickerView;

    invoke-virtual {v0}, Lcom/facebook/events/ui/date/DatePickerView;->a()V

    .line 1818413
    :cond_0
    :goto_0
    iget-object v0, p0, LX/BmV;->a:Lcom/facebook/events/ui/date/StartAndEndTimePicker;

    invoke-static {v0}, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->b(Lcom/facebook/events/ui/date/StartAndEndTimePicker;)V

    .line 1818414
    iget-object v0, p0, LX/BmV;->a:Lcom/facebook/events/ui/date/StartAndEndTimePicker;

    iget-object v0, v0, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->f:Lcom/facebook/events/ui/date/TimePickerView;

    iget-object v1, p0, LX/BmV;->a:Lcom/facebook/events/ui/date/StartAndEndTimePicker;

    invoke-virtual {v1}, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->getEndDate()Ljava/util/Date;

    move-result-object v1

    iget-object v2, p0, LX/BmV;->a:Lcom/facebook/events/ui/date/StartAndEndTimePicker;

    invoke-virtual {v2}, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->a(Lcom/facebook/events/ui/date/TimePickerView;Ljava/util/Date;Ljava/util/TimeZone;)V

    .line 1818415
    return-void

    .line 1818416
    :cond_1
    iget-object v0, p0, LX/BmV;->a:Lcom/facebook/events/ui/date/StartAndEndTimePicker;

    iget-object v0, v0, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->e:Lcom/facebook/events/ui/date/DatePickerView;

    .line 1818417
    iget-object v1, v0, Lcom/facebook/events/ui/date/DatePickerView;->d:Ljava/util/Calendar;

    move-object v0, v1

    .line 1818418
    if-nez v0, :cond_0

    .line 1818419
    iget-object v0, p0, LX/BmV;->a:Lcom/facebook/events/ui/date/StartAndEndTimePicker;

    iget-object v0, v0, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->b:Lcom/facebook/events/ui/date/DatePickerView;

    .line 1818420
    iget-object v1, v0, Lcom/facebook/events/ui/date/DatePickerView;->d:Ljava/util/Calendar;

    move-object v0, v1

    .line 1818421
    invoke-virtual {v0}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    .line 1818422
    const/4 v1, 0x5

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V

    .line 1818423
    iget-object v1, p0, LX/BmV;->a:Lcom/facebook/events/ui/date/StartAndEndTimePicker;

    iget-object v1, v1, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->e:Lcom/facebook/events/ui/date/DatePickerView;

    invoke-virtual {v1, v0}, Lcom/facebook/events/ui/date/DatePickerView;->setDate(Ljava/util/Calendar;)V

    goto :goto_0
.end method
