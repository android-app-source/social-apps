.class public final LX/Cbu;
.super LX/3nE;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/3nE",
        "<",
        "Ljava/io/InputStream;",
        "Ljava/lang/Void;",
        "Landroid/net/Uri;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/1FJ;

.field public final synthetic b:LX/Cbv;


# direct methods
.method public constructor <init>(LX/Cbv;LX/1FJ;)V
    .locals 0

    .prologue
    .line 1920483
    iput-object p1, p0, LX/Cbu;->b:LX/Cbv;

    iput-object p2, p0, LX/Cbu;->a:LX/1FJ;

    invoke-direct {p0}, LX/3nE;-><init>()V

    return-void
.end method


# virtual methods
.method public final a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 1920484
    check-cast p1, [Ljava/io/InputStream;

    const/4 v0, 0x0

    .line 1920485
    const/4 v1, 0x0

    aget-object v2, p1, v1

    .line 1920486
    :try_start_0
    iget-object v1, p0, LX/Cbu;->b:LX/Cbv;

    iget-object v1, v1, LX/Cbv;->b:LX/CcO;

    iget-object v1, v1, LX/CcO;->h:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryActionsGenerator;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryActionsGenerator;->a(Ljava/io/InputStream;Ljava/io/File;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1920487
    invoke-static {v2}, LX/0VN;->a(Ljava/io/InputStream;)V

    .line 1920488
    iget-object v1, p0, LX/Cbu;->a:LX/1FJ;

    invoke-static {v1}, LX/1FJ;->c(LX/1FJ;)V

    :goto_0
    return-object v0

    .line 1920489
    :catch_0
    move-exception v1

    .line 1920490
    :try_start_1
    iget-object v3, p0, LX/Cbu;->b:LX/Cbv;

    iget-object v3, v3, LX/Cbv;->b:LX/CcO;

    iget-object v3, v3, LX/CcO;->a:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;

    iget-object v3, v3, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->k:LX/03V;

    sget-object v4, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->c:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Could not share file (w/ Fresco + jpeg) "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1920491
    invoke-static {v2}, LX/0VN;->a(Ljava/io/InputStream;)V

    .line 1920492
    iget-object v1, p0, LX/Cbu;->a:LX/1FJ;

    invoke-static {v1}, LX/1FJ;->c(LX/1FJ;)V

    goto :goto_0

    .line 1920493
    :catchall_0
    move-exception v0

    invoke-static {v2}, LX/0VN;->a(Ljava/io/InputStream;)V

    .line 1920494
    iget-object v1, p0, LX/Cbu;->a:LX/1FJ;

    invoke-static {v1}, LX/1FJ;->c(LX/1FJ;)V

    throw v0
.end method

.method public final onPostExecute(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1920495
    check-cast p1, Landroid/net/Uri;

    .line 1920496
    if-nez p1, :cond_0

    .line 1920497
    iget-object v0, p0, LX/Cbu;->b:LX/Cbv;

    iget-object v0, v0, LX/Cbv;->b:LX/CcO;

    iget-object v0, v0, LX/CcO;->a:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->k:LX/03V;

    sget-object v1, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->c:Ljava/lang/String;

    const-string v2, "Could not share file (w/ Fresco + jpeg) No temp uri"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1920498
    iget-object v0, p0, LX/Cbu;->b:LX/Cbv;

    iget-object v0, v0, LX/Cbv;->b:LX/CcO;

    invoke-static {v0}, LX/CcO;->n(LX/CcO;)V

    .line 1920499
    :goto_0
    return-void

    .line 1920500
    :cond_0
    iget-object v0, p0, LX/Cbu;->b:LX/Cbv;

    iget-object v0, v0, LX/Cbv;->b:LX/CcO;

    iget-object v1, p0, LX/Cbu;->b:LX/Cbv;

    iget-object v1, v1, LX/Cbv;->a:Landroid/content/Context;

    invoke-static {v0, p1, v1}, LX/CcO;->a$redex0(LX/CcO;Landroid/net/Uri;Landroid/content/Context;)V

    goto :goto_0
.end method
