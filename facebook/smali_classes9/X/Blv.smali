.class public final LX/Blv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/facebook/events/protocol/CreateEventParams;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1816765
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 1816766
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1816767
    new-instance v0, LX/Blw;

    invoke-direct {v0, p1}, LX/Blw;-><init>(Landroid/os/Parcel;)V

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    .line 1816768
    iput-object v3, v0, LX/Blw;->b:Ljava/lang/Long;

    .line 1816769
    move-object v0, v0

    .line 1816770
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 1816771
    iput-object v3, v0, LX/Blw;->c:Ljava/lang/String;

    .line 1816772
    move-object v0, v0

    .line 1816773
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 1816774
    iput-object v3, v0, LX/Blw;->d:Ljava/lang/String;

    .line 1816775
    move-object v0, v0

    .line 1816776
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    .line 1816777
    iput-wide v4, v0, LX/Blw;->e:J

    .line 1816778
    move-object v0, v0

    .line 1816779
    new-instance v3, Ljava/util/Date;

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Ljava/util/Date;-><init>(J)V

    .line 1816780
    iput-object v3, v0, LX/Blw;->f:Ljava/util/Date;

    .line 1816781
    move-object v3, v0

    .line 1816782
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    .line 1816783
    :goto_0
    iput-boolean v0, v3, LX/Blw;->g:Z

    .line 1816784
    move-object v3, v3

    .line 1816785
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 1816786
    :goto_1
    if-eqz v0, :cond_0

    .line 1816787
    new-instance v0, Ljava/util/Date;

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    invoke-direct {v0, v4, v5}, Ljava/util/Date;-><init>(J)V

    .line 1816788
    iput-object v0, v3, LX/Blw;->h:Ljava/util/Date;

    .line 1816789
    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    .line 1816790
    iput-object v0, v3, LX/Blw;->i:Ljava/util/TimeZone;

    .line 1816791
    move-object v4, v3

    .line 1816792
    const-class v0, Lcom/facebook/events/model/EventType;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/model/EventType;

    .line 1816793
    iput-object v0, v4, LX/Blw;->n:Lcom/facebook/events/model/EventType;

    .line 1816794
    move-object v4, v4

    .line 1816795
    const-class v0, Lcom/facebook/events/model/PrivacyType;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/model/PrivacyType;

    .line 1816796
    iput-object v0, v4, LX/Blw;->j:Lcom/facebook/events/model/PrivacyType;

    .line 1816797
    move-object v0, v4

    .line 1816798
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-ne v4, v1, :cond_3

    .line 1816799
    :goto_2
    iput-boolean v1, v0, LX/Blw;->k:Z

    .line 1816800
    move-object v0, v0

    .line 1816801
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 1816802
    iput-object v1, v0, LX/Blw;->l:Ljava/lang/String;

    .line 1816803
    new-instance v0, Lcom/facebook/events/protocol/CreateEventParams;

    invoke-direct {v0, v3}, Lcom/facebook/events/protocol/CreateEventParams;-><init>(LX/Blw;)V

    move-object v0, v0

    .line 1816804
    return-object v0

    :cond_1
    move v0, v2

    .line 1816805
    goto :goto_0

    :cond_2
    move v0, v2

    .line 1816806
    goto :goto_1

    :cond_3
    move v1, v2

    .line 1816807
    goto :goto_2
.end method

.method public final newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1816808
    new-array v0, p1, [Lcom/facebook/events/protocol/CreateEventParams;

    return-object v0
.end method
