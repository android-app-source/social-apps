.class public LX/CAN;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public final a:LX/1WX;

.field public final b:LX/CAJ;

.field public final c:LX/1VI;


# direct methods
.method public constructor <init>(LX/1WX;LX/CAJ;LX/1VI;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1855342
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1855343
    iput-object p1, p0, LX/CAN;->a:LX/1WX;

    .line 1855344
    iput-object p2, p0, LX/CAN;->b:LX/CAJ;

    .line 1855345
    iput-object p3, p0, LX/CAN;->c:LX/1VI;

    .line 1855346
    return-void
.end method

.method public static a(LX/0QB;)LX/CAN;
    .locals 6

    .prologue
    .line 1855331
    const-class v1, LX/CAN;

    monitor-enter v1

    .line 1855332
    :try_start_0
    sget-object v0, LX/CAN;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1855333
    sput-object v2, LX/CAN;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1855334
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1855335
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1855336
    new-instance p0, LX/CAN;

    invoke-static {v0}, LX/1WX;->a(LX/0QB;)LX/1WX;

    move-result-object v3

    check-cast v3, LX/1WX;

    invoke-static {v0}, LX/CAJ;->a(LX/0QB;)LX/CAJ;

    move-result-object v4

    check-cast v4, LX/CAJ;

    invoke-static {v0}, LX/1VI;->a(LX/0QB;)LX/1VI;

    move-result-object v5

    check-cast v5, LX/1VI;

    invoke-direct {p0, v3, v4, v5}, LX/CAN;-><init>(LX/1WX;LX/CAJ;LX/1VI;)V

    .line 1855337
    move-object v0, p0

    .line 1855338
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1855339
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/CAN;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1855340
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1855341
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
