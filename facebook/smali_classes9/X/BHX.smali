.class public LX/BHX;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1769322
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 1769323
    return-void
.end method


# virtual methods
.method public final a(Landroid/database/Cursor;LX/BH5;LX/BHJ;LX/0am;ZZZ)Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;
    .locals 24
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "Lcom/facebook/photos/simplepicker/SimplePickerFragment$BitmapRenderedCallback;",
            "LX/BHJ;",
            "LX/0am",
            "<",
            "Lcom/facebook/photos/simplepicker/view/PickerLongPressProgressBar;",
            ">;ZZZ)",
            "Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;"
        }
    .end annotation

    .prologue
    .line 1769320
    new-instance v1, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;

    const-class v2, Landroid/content/Context;

    const-class v3, Lcom/facebook/inject/ForAppContext;

    move-object/from16 v0, p0

    invoke-interface {v0, v2, v3}, LX/0QC;->getInstance(Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/content/Context;

    invoke-static/range {p0 .. p0}, LX/8I3;->a(LX/0QB;)LX/8I2;

    move-result-object v10

    check-cast v10, LX/8I2;

    const-class v2, LX/BID;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v11

    check-cast v11, LX/BID;

    invoke-static/range {p0 .. p0}, LX/11h;->a(LX/0QB;)LX/11h;

    move-result-object v12

    check-cast v12, LX/11i;

    const/16 v2, 0x259

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v13

    invoke-static/range {p0 .. p0}, LX/BHY;->a(LX/0QB;)LX/BHY;

    move-result-object v14

    check-cast v14, LX/BHY;

    invoke-static/range {p0 .. p0}, LX/0tb;->a(LX/0QB;)LX/0TD;

    move-result-object v15

    check-cast v15, Ljava/util/concurrent/ExecutorService;

    invoke-static/range {p0 .. p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v16

    check-cast v16, LX/0Sh;

    invoke-static/range {p0 .. p0}, LX/1Ad;->a(LX/0QB;)LX/1Ad;

    move-result-object v17

    check-cast v17, LX/1Ad;

    invoke-static/range {p0 .. p0}, LX/BHE;->a(LX/0QB;)LX/BHE;

    move-result-object v18

    check-cast v18, LX/BHE;

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v19

    check-cast v19, LX/0ad;

    invoke-static/range {p0 .. p0}, LX/7Dh;->a(LX/0QB;)LX/7Dh;

    move-result-object v20

    check-cast v20, LX/7Dh;

    invoke-static/range {p0 .. p0}, Lcom/facebook/photos/simplepicker/controller/RecognitionManager;->a(LX/0QB;)Lcom/facebook/photos/simplepicker/controller/RecognitionManager;

    move-result-object v21

    check-cast v21, Lcom/facebook/photos/simplepicker/controller/RecognitionManager;

    invoke-static/range {p0 .. p0}, LX/0sX;->a(LX/0QB;)LX/0sX;

    move-result-object v22

    check-cast v22, LX/0sX;

    invoke-static/range {p0 .. p0}, LX/BHv;->a(LX/0QB;)LX/BHv;

    move-result-object v23

    check-cast v23, LX/BHv;

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move/from16 v6, p5

    move/from16 v7, p6

    move/from16 v8, p7

    invoke-direct/range {v1 .. v23}, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;-><init>(Landroid/database/Cursor;LX/BH5;LX/BHJ;LX/0am;ZZZLandroid/content/Context;LX/8I2;LX/BID;LX/11i;LX/0Ot;LX/BHY;Ljava/util/concurrent/ExecutorService;LX/0Sh;LX/1Ad;LX/BHE;LX/0ad;LX/7Dh;Lcom/facebook/photos/simplepicker/controller/RecognitionManager;LX/0sX;LX/BHv;)V

    .line 1769321
    return-object v1
.end method
