.class public LX/Alh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Alg;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<CONTEXT::",
        "Lcom/facebook/ipc/productionprompts/actioncontext/photoreminder/PhotoReminderPromptActionContext;",
        ">",
        "Ljava/lang/Object;",
        "LX/Alg",
        "<TCONTEXT;>;"
    }
.end annotation


# instance fields
.field public final a:LX/1kG;

.field public final b:LX/74n;

.field private final c:LX/1Ns;

.field public final d:LX/1Nq;

.field public final e:LX/Alf;


# direct methods
.method public constructor <init>(LX/1kG;LX/74n;LX/1Ns;LX/1Nq;LX/Alf;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1710080
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1710081
    iput-object p1, p0, LX/Alh;->a:LX/1kG;

    .line 1710082
    iput-object p2, p0, LX/Alh;->b:LX/74n;

    .line 1710083
    iput-object p3, p0, LX/Alh;->c:LX/1Ns;

    .line 1710084
    iput-object p4, p0, LX/Alh;->d:LX/1Nq;

    .line 1710085
    iput-object p5, p0, LX/Alh;->e:LX/Alf;

    .line 1710086
    return-void
.end method

.method public static a(LX/Alh;)LX/1aw;
    .locals 3

    .prologue
    .line 1710087
    iget-object v0, p0, LX/Alh;->c:LX/1Ns;

    new-instance v1, LX/1Qg;

    invoke-direct {v1}, LX/1Qg;-><init>()V

    new-instance v2, LX/AnN;

    invoke-direct {v2}, LX/AnN;-><init>()V

    invoke-virtual {v0, v1, v2}, LX/1Ns;->a(LX/1Qh;LX/1DQ;)LX/1aw;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/app/Activity;LX/B5p;Lcom/facebook/productionprompts/logging/PromptAnalytics;)V
    .locals 12

    .prologue
    .line 1710088
    iget-object v0, p2, LX/B5p;->e:Landroid/net/Uri;

    move-object v0, v0

    .line 1710089
    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p3

    .line 1710090
    if-nez v1, :cond_0

    .line 1710091
    :goto_0
    return-void

    .line 1710092
    :cond_0
    iget-object v6, v0, LX/Alh;->a:LX/1kG;

    invoke-virtual {v6}, LX/1kG;->d()V

    .line 1710093
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v7

    .line 1710094
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_1
    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/net/Uri;

    .line 1710095
    iget-object v9, v0, LX/Alh;->b:LX/74n;

    sget-object v10, LX/74j;->DEFAULT:LX/74j;

    invoke-virtual {v9, v6, v10}, LX/74n;->a(Landroid/net/Uri;LX/74j;)Lcom/facebook/ipc/media/MediaItem;

    move-result-object v6

    .line 1710096
    if-eqz v6, :cond_1

    .line 1710097
    invoke-virtual {v7, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 1710098
    :cond_2
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v9

    .line 1710099
    invoke-static {v0}, LX/Alh;->a(LX/Alh;)LX/1aw;

    move-result-object v6

    iget-object v7, v2, Lcom/facebook/productionprompts/logging/PromptAnalytics;->composerSessionId:Ljava/lang/String;

    const-string v8, "photoReminderPrompt"

    iget-object v10, v0, LX/Alh;->d:LX/1Nq;

    invoke-static {v2}, Lcom/facebook/feed/photoreminder/composer/PhotoReminderPluginConfig;->a(Lcom/facebook/productionprompts/logging/PromptAnalytics;)Lcom/facebook/feed/photoreminder/composer/PhotoReminderPluginConfig;

    move-result-object v11

    invoke-virtual {v10, v11}, LX/1Nq;->a(LX/88f;)Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;

    move-result-object v10

    move-object v11, v1

    invoke-virtual/range {v6 .. v11}, LX/1aw;->a(Ljava/lang/String;Ljava/lang/String;LX/0Px;Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;Landroid/app/Activity;)V

    .line 1710100
    iget-object v6, v0, LX/Alh;->e:LX/Alf;

    .line 1710101
    iget-object v7, v6, LX/Alf;->b:LX/5oW;

    invoke-virtual {v7, v2}, LX/5oW;->b(Lcom/facebook/productionprompts/logging/PromptAnalytics;)V

    .line 1710102
    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/Alh;
    .locals 6

    .prologue
    .line 1710103
    new-instance v0, LX/Alh;

    invoke-static {p0}, LX/1kG;->a(LX/0QB;)LX/1kG;

    move-result-object v1

    check-cast v1, LX/1kG;

    invoke-static {p0}, LX/74n;->b(LX/0QB;)LX/74n;

    move-result-object v2

    check-cast v2, LX/74n;

    const-class v3, LX/1Ns;

    invoke-interface {p0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/1Ns;

    invoke-static {p0}, LX/1Nq;->b(LX/0QB;)LX/1Nq;

    move-result-object v4

    check-cast v4, LX/1Nq;

    invoke-static {p0}, LX/Alf;->a(LX/0QB;)LX/Alf;

    move-result-object v5

    check-cast v5, LX/Alf;

    invoke-direct/range {v0 .. v5}, LX/Alh;-><init>(LX/1kG;LX/74n;LX/1Ns;LX/1Nq;LX/Alf;)V

    .line 1710104
    return-object v0
.end method


# virtual methods
.method public final a(LX/88f;LX/1RN;)V
    .locals 5
    .param p1    # LX/88f;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1710105
    iget-object v0, p0, LX/Alh;->a:LX/1kG;

    invoke-virtual {v0}, LX/1kG;->d()V

    .line 1710106
    iget-object v0, p0, LX/Alh;->a:LX/1kG;

    .line 1710107
    iget-object v1, v0, LX/1kG;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/1kp;->c:LX/0Tn;

    .line 1710108
    sget-object v3, LX/0SF;->a:LX/0SF;

    move-object v3, v3

    .line 1710109
    invoke-virtual {v3}, LX/0SF;->a()J

    move-result-wide v3

    invoke-interface {v1, v2, v3, v4}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 1710110
    return-void
.end method

.method public final bridge synthetic a(Landroid/view/View;LX/1RN;LX/B5o;)V
    .locals 0

    .prologue
    .line 1710111
    check-cast p3, LX/B5p;

    invoke-virtual {p0, p1, p2, p3}, LX/Alh;->a(Landroid/view/View;LX/1RN;LX/B5p;)V

    return-void
.end method

.method public final a(Landroid/view/View;LX/1RN;LX/B5p;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "LX/1RN;",
            "TCONTEXT;)V"
        }
    .end annotation

    .prologue
    .line 1710112
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Landroid/app/Activity;

    invoke-static {v0, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/app/Activity;

    .line 1710113
    iget-object v0, p2, LX/1RN;->a:LX/1kK;

    invoke-interface {v0}, LX/1kK;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p2, LX/1RN;->b:LX/1lP;

    iget-object v1, v1, LX/1lP;->c:Ljava/lang/String;

    .line 1710114
    iget-object v2, p3, LX/B5p;->a:Ljava/lang/String;

    move-object v2, v2

    .line 1710115
    iget-object v3, p3, LX/B5p;->j:LX/0am;

    move-object v3, v3

    .line 1710116
    invoke-virtual {v3}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    iget-object v4, p2, LX/1RN;->b:LX/1lP;

    iget-object v4, v4, LX/1lP;->b:Ljava/lang/String;

    iget-object v5, p2, LX/1RN;->c:LX/32e;

    iget-object v5, v5, LX/32e;->a:LX/24P;

    invoke-virtual {v5}, LX/24P;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Lcom/facebook/productionprompts/logging/PromptAnalytics;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/productionprompts/logging/PromptAnalytics;

    move-result-object v0

    .line 1710117
    sget-object v1, LX/B5m;->PHOTO_REMINDER_TAP_ON_MEDIA:LX/B5m;

    iget-object v2, p3, LX/B5p;->b:LX/B5m;

    invoke-virtual {v1, v2}, LX/B5m;->equals(Ljava/lang/Object;)Z

    move-result v1

    move v1, v1

    .line 1710118
    if-eqz v1, :cond_1

    .line 1710119
    invoke-direct {p0, v6, p3, v0}, LX/Alh;->a(Landroid/app/Activity;LX/B5p;Lcom/facebook/productionprompts/logging/PromptAnalytics;)V

    .line 1710120
    :cond_0
    :goto_0
    return-void

    .line 1710121
    :cond_1
    sget-object v1, LX/B5m;->PHOTO_REMINDER_TAP_ON_MORE:LX/B5m;

    iget-object v2, p3, LX/B5p;->b:LX/B5m;

    invoke-virtual {v1, v2}, LX/B5m;->equals(Ljava/lang/Object;)Z

    move-result v1

    move v1, v1

    .line 1710122
    if-eqz v1, :cond_0

    .line 1710123
    iget-object v1, p0, LX/Alh;->e:LX/Alf;

    .line 1710124
    iget-object v2, v1, LX/Alf;->b:LX/5oW;

    invoke-virtual {v2, v0}, LX/5oW;->b(Lcom/facebook/productionprompts/logging/PromptAnalytics;)V

    .line 1710125
    invoke-static {p0}, LX/Alh;->a(LX/Alh;)LX/1aw;

    move-result-object v1

    iget-object v2, v0, Lcom/facebook/productionprompts/logging/PromptAnalytics;->composerSessionId:Ljava/lang/String;

    const-string v3, "photoReminderMediaPicker"

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v6, v3, v4}, LX/1aw;->a(Ljava/lang/String;Landroid/app/Activity;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;)V

    .line 1710126
    goto :goto_0
.end method

.method public final b(LX/1RN;)Z
    .locals 1

    .prologue
    .line 1710127
    invoke-static {p1}, LX/1RN;->a(LX/1RN;)LX/1kK;

    move-result-object v0

    .line 1710128
    instance-of v0, v0, LX/1kJ;

    return v0
.end method
