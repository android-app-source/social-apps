.class public LX/CFg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/CFf;


# instance fields
.field private final a:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1863889
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1863890
    iput-object p1, p0, LX/CFg;->a:LX/0Zb;

    .line 1863891
    return-void
.end method


# virtual methods
.method public final a(LX/0P1;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1863892
    iget-object v0, p0, LX/CFg;->a:LX/0Zb;

    const-string v1, "google_play_referrer"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v2

    .line 1863893
    invoke-virtual {v2}, LX/0oG;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1863894
    const-string v0, "growth"

    invoke-virtual {v2, v0}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 1863895
    invoke-virtual {p1}, LX/0P1;->entrySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1863896
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v1, v0}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    goto :goto_0

    .line 1863897
    :cond_0
    invoke-virtual {v2}, LX/0oG;->d()V

    .line 1863898
    :cond_1
    return-void
.end method
