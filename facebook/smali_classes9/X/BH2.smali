.class public final LX/BH2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/74q;


# instance fields
.field public final synthetic a:Lcom/facebook/photos/simplepicker/SimplePickerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/simplepicker/SimplePickerFragment;)V
    .locals 0

    .prologue
    .line 1767594
    iput-object p1, p0, LX/BH2;->a:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 1767595
    return-void
.end method

.method public final a(Lcom/facebook/ipc/media/MediaItem;LX/74p;)V
    .locals 3

    .prologue
    .line 1767596
    iget-object v0, p0, LX/BH2;->a:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    iget-object v0, v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->E:LX/BHh;

    iget-object v1, p0, LX/BH2;->a:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    iget-object v1, v1, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->c:LX/BHJ;

    invoke-virtual {v1}, LX/BHJ;->b()LX/0Px;

    move-result-object v1

    .line 1767597
    iget-object v2, v0, LX/BHh;->c:Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;

    if-nez v2, :cond_0

    iget-object v2, v0, LX/BHh;->a:Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;

    .line 1767598
    iget-boolean p0, v2, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->z:Z

    move v2, p0

    .line 1767599
    if-nez v2, :cond_1

    :cond_0
    iget-object v2, v0, LX/BHh;->c:Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;

    if-eqz v2, :cond_2

    iget-object v2, v0, LX/BHh;->c:Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;

    invoke-virtual {v2}, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1767600
    :cond_1
    invoke-static {v0, v1}, LX/BHh;->b(LX/BHh;LX/0Px;)V

    .line 1767601
    :goto_0
    return-void

    .line 1767602
    :cond_2
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v2

    iget-object p0, v0, LX/BHh;->a:Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;

    .line 1767603
    iget p1, p0, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->x:I

    move p0, p1

    .line 1767604
    if-ge v2, p0, :cond_3

    .line 1767605
    invoke-static {v0}, LX/BHh;->e(LX/BHh;)V

    goto :goto_0

    .line 1767606
    :cond_3
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result p1

    const/4 v2, 0x0

    move p0, v2

    :goto_1
    if-ge p0, p1, :cond_5

    invoke-virtual {v1, p0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/ipc/media/MediaItem;

    .line 1767607
    invoke-virtual {v2}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v2

    .line 1767608
    iget-object p2, v2, Lcom/facebook/ipc/media/data/MediaData;->mType:LX/4gQ;

    move-object v2, p2

    .line 1767609
    sget-object p2, LX/4gQ;->Photo:LX/4gQ;

    if-eq v2, p2, :cond_4

    .line 1767610
    invoke-static {v0}, LX/BHh;->e(LX/BHh;)V

    goto :goto_0

    .line 1767611
    :cond_4
    add-int/lit8 v2, p0, 0x1

    move p0, v2

    goto :goto_1

    .line 1767612
    :cond_5
    invoke-static {v0, v1}, LX/BHh;->b(LX/BHh;LX/0Px;)V

    goto :goto_0
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 1767613
    return-void
.end method
