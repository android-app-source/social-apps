.class public final LX/AXl;
.super Landroid/graphics/drawable/ShapeDrawable;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 1

    .prologue
    .line 1684469
    new-instance v0, Landroid/graphics/drawable/shapes/RectShape;

    invoke-direct {v0}, Landroid/graphics/drawable/shapes/RectShape;-><init>()V

    invoke-direct {p0, v0}, Landroid/graphics/drawable/ShapeDrawable;-><init>(Landroid/graphics/drawable/shapes/Shape;)V

    .line 1684470
    invoke-direct {p0, p1}, LX/AXl;->a(Landroid/content/res/Resources;)Landroid/graphics/drawable/ShapeDrawable$ShaderFactory;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/AXl;->setShaderFactory(Landroid/graphics/drawable/ShapeDrawable$ShaderFactory;)V

    .line 1684471
    return-void
.end method

.method private a(Landroid/content/res/Resources;)Landroid/graphics/drawable/ShapeDrawable$ShaderFactory;
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    .line 1684472
    new-array v0, v3, [I

    const v1, 0x7f0a03a7

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    aput v1, v0, v2

    const/4 v1, 0x1

    aput v2, v0, v1

    .line 1684473
    new-array v1, v3, [F

    fill-array-data v1, :array_0

    .line 1684474
    new-instance v2, LX/AXk;

    invoke-direct {v2, p0, v0, v1}, LX/AXk;-><init>(LX/AXl;[I[F)V

    return-object v2

    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method
