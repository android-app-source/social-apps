.class public final LX/AmD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field public final synthetic a:Lcom/facebook/feed/prefs/NativeFeedSettingsActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/prefs/NativeFeedSettingsActivity;)V
    .locals 0

    .prologue
    .line 1710543
    iput-object p1, p0, LX/AmD;->a:Lcom/facebook/feed/prefs/NativeFeedSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 4

    .prologue
    .line 1710544
    const-string v0, "all"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1710545
    const v0, 0x7fffffff

    .line 1710546
    :goto_0
    new-instance v1, LX/1NL;

    invoke-direct {v1, v0}, LX/1NL;-><init>(I)V

    .line 1710547
    iget-object v2, p0, LX/AmD;->a:Lcom/facebook/feed/prefs/NativeFeedSettingsActivity;

    iget-object v2, v2, Lcom/facebook/feed/prefs/NativeFeedSettingsActivity;->a:LX/0bH;

    invoke-virtual {v2, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1710548
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Cache: cleared "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "stories"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1710549
    const/4 v0, 0x0

    return v0

    .line 1710550
    :cond_0
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method
