.class public final LX/BmJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:LX/BmL;


# direct methods
.method public constructor <init>(LX/BmL;)V
    .locals 0

    .prologue
    .line 1818155
    iput-object p1, p0, LX/BmJ;->a:LX/BmL;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 10

    .prologue
    .line 1818156
    iget-object v0, p0, LX/BmJ;->a:LX/BmL;

    iget-object v0, v0, LX/BmL;->c:Landroid/text/format/Time;

    iget-object v1, p0, LX/BmJ;->a:LX/BmL;

    iget-object v1, v1, LX/BmL;->e:Landroid/widget/TimePicker;

    invoke-virtual {v1}, Landroid/widget/TimePicker;->getCurrentHour()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object v2, p0, LX/BmJ;->a:LX/BmL;

    iget-object v2, v2, LX/BmL;->e:Landroid/widget/TimePicker;

    invoke-virtual {v2}, Landroid/widget/TimePicker;->getCurrentMinute()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v0, v1, v2}, LX/Bmb;->a(Landroid/text/format/Time;II)Landroid/text/format/Time;

    move-result-object v0

    .line 1818157
    iget-object v1, p0, LX/BmJ;->a:LX/BmL;

    .line 1818158
    iget-object v3, v1, LX/BmL;->f:LX/BmY;

    if-eqz v3, :cond_1

    iget-object v3, v1, LX/BmL;->d:Landroid/text/format/Time;

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 1818159
    if-ne v3, v0, :cond_2

    .line 1818160
    :cond_0
    :goto_0
    move v3, v4

    .line 1818161
    if-nez v3, :cond_1

    .line 1818162
    iget-object v3, v1, LX/BmL;->f:LX/BmY;

    invoke-interface {v3, v0}, LX/BmY;->a(Landroid/text/format/Time;)V

    .line 1818163
    iput-object v0, v1, LX/BmL;->d:Landroid/text/format/Time;

    .line 1818164
    :cond_1
    return-void

    .line 1818165
    :cond_2
    if-eqz v3, :cond_3

    if-nez v0, :cond_4

    :cond_3
    move v4, v5

    .line 1818166
    goto :goto_0

    .line 1818167
    :cond_4
    iget-boolean v6, v3, Landroid/text/format/Time;->allDay:Z

    iget-boolean v7, v0, Landroid/text/format/Time;->allDay:Z

    if-ne v6, v7, :cond_5

    invoke-virtual {v3, v4}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v6

    invoke-virtual {v0, v4}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v8

    cmp-long v6, v6, v8

    if-eqz v6, :cond_0

    :cond_5
    move v4, v5

    goto :goto_0
.end method
