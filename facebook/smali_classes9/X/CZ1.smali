.class public final LX/CZ1;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1913922
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 1913923
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1913924
    :goto_0
    return v1

    .line 1913925
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1913926
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 1913927
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1913928
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1913929
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 1913930
    const-string v4, "collection_product_items"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1913931
    const/4 v3, 0x0

    .line 1913932
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v4, :cond_9

    .line 1913933
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1913934
    :goto_2
    move v2, v3

    .line 1913935
    goto :goto_1

    .line 1913936
    :cond_2
    const-string v4, "id"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1913937
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 1913938
    :cond_3
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1913939
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1913940
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1913941
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    move v2, v1

    goto :goto_1

    .line 1913942
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1913943
    :cond_6
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_8

    .line 1913944
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1913945
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1913946
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_6

    if-eqz v4, :cond_6

    .line 1913947
    const-string v5, "nodes"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1913948
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1913949
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->START_ARRAY:LX/15z;

    if-ne v4, v5, :cond_7

    .line 1913950
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_ARRAY:LX/15z;

    if-eq v4, v5, :cond_7

    .line 1913951
    invoke-static {p0, p1}, LX/CZ0;->b(LX/15w;LX/186;)I

    move-result v4

    .line 1913952
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 1913953
    :cond_7
    invoke-static {v2, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v2

    move v2, v2

    .line 1913954
    goto :goto_3

    .line 1913955
    :cond_8
    const/4 v4, 0x1

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1913956
    invoke-virtual {p1, v3, v2}, LX/186;->b(II)V

    .line 1913957
    invoke-virtual {p1}, LX/186;->d()I

    move-result v3

    goto :goto_2

    :cond_9
    move v2, v3

    goto :goto_3
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1913958
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1913959
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1913960
    if-eqz v0, :cond_2

    .line 1913961
    const-string v1, "collection_product_items"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1913962
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1913963
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v1

    .line 1913964
    if-eqz v1, :cond_1

    .line 1913965
    const-string v2, "nodes"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1913966
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1913967
    const/4 v2, 0x0

    :goto_0
    invoke-virtual {p0, v1}, LX/15i;->c(I)I

    move-result v0

    if-ge v2, v0, :cond_0

    .line 1913968
    invoke-virtual {p0, v1, v2}, LX/15i;->q(II)I

    move-result v0

    invoke-static {p0, v0, p2, p3}, LX/CZ0;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1913969
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1913970
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1913971
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1913972
    :cond_2
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1913973
    if-eqz v0, :cond_3

    .line 1913974
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1913975
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1913976
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1913977
    return-void
.end method
