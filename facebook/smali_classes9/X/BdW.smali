.class public final LX/BdW;
.super LX/BcS;
.source ""


# static fields
.field public static a:LX/BdW;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/BdT;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public c:LX/BdY;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1803889
    const/4 v0, 0x0

    sput-object v0, LX/BdW;->a:LX/BdW;

    .line 1803890
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/BdW;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1803837
    invoke-direct {p0}, LX/BcS;-><init>()V

    .line 1803838
    new-instance v0, LX/BdY;

    invoke-direct {v0}, LX/BdY;-><init>()V

    iput-object v0, p0, LX/BdW;->c:LX/BdY;

    .line 1803839
    return-void
.end method

.method public static a(LX/BcP;Z)V
    .locals 2

    .prologue
    .line 1803882
    invoke-virtual {p0}, LX/BcP;->i()LX/BcO;

    move-result-object v0

    .line 1803883
    if-nez v0, :cond_0

    .line 1803884
    :goto_0
    return-void

    .line 1803885
    :cond_0
    check-cast v0, LX/BdU;

    .line 1803886
    new-instance v1, LX/BdV;

    invoke-direct {v1, p1}, LX/BdV;-><init>(Z)V

    move-object v1, v1

    .line 1803887
    move-object v0, v1

    .line 1803888
    invoke-virtual {p0, v0}, LX/BcP;->a(LX/BcR;)V

    goto :goto_0
.end method

.method public static declared-synchronized d()LX/BdW;
    .locals 2

    .prologue
    .line 1803878
    const-class v1, LX/BdW;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/BdW;->a:LX/BdW;

    if-nez v0, :cond_0

    .line 1803879
    new-instance v0, LX/BdW;

    invoke-direct {v0}, LX/BdW;-><init>()V

    sput-object v0, LX/BdW;->a:LX/BdW;

    .line 1803880
    :cond_0
    sget-object v0, LX/BdW;->a:LX/BdW;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 1803881
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/BcQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 10

    .prologue
    const/4 v6, 0x0

    .line 1803856
    invoke-static {}, LX/1dS;->b()V

    .line 1803857
    iget v0, p1, LX/BcQ;->b:I

    .line 1803858
    packed-switch v0, :pswitch_data_0

    .line 1803859
    :goto_0
    return-object v6

    .line 1803860
    :pswitch_0
    check-cast p2, LX/BcM;

    .line 1803861
    iget-boolean v1, p2, LX/BcM;->a:Z

    iget-object v2, p2, LX/BcM;->b:LX/BcL;

    iget-object v3, p2, LX/BcM;->c:Ljava/lang/Throwable;

    iget-object v0, p1, LX/BcQ;->c:[Ljava/lang/Object;

    const/4 v4, 0x0

    aget-object v4, v0, v4

    check-cast v4, LX/BcP;

    iget-object v5, p1, LX/BcQ;->a:LX/BcO;

    .line 1803862
    check-cast v5, LX/BdU;

    .line 1803863
    iget p0, v5, LX/BdU;->c:I

    iget-object p1, v5, LX/BdU;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget-boolean p2, v5, LX/BdU;->e:Z

    move v0, v1

    move-object v7, v2

    move-object v8, v3

    move-object v9, v4

    const/4 v3, 0x1

    .line 1803864
    sget-object v1, LX/BdX;->a:[I

    invoke-virtual {v7}, LX/BcL;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_1

    .line 1803865
    :cond_0
    :goto_1
    invoke-static {v9, v0, v7, v8}, LX/BcS;->a(LX/BcP;ZLX/BcL;Ljava/lang/Throwable;)V

    .line 1803866
    goto :goto_0

    .line 1803867
    :pswitch_1
    if-nez p2, :cond_0

    .line 1803868
    const/16 v1, 0x18

    invoke-interface {p1, p0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IS)V

    .line 1803869
    const/4 v1, 0x0

    invoke-static {v9, v1}, LX/BdW;->a(LX/BcP;Z)V

    goto :goto_1

    .line 1803870
    :pswitch_2
    if-nez p2, :cond_0

    .line 1803871
    const/16 v1, 0x66

    invoke-interface {p1, p0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IS)V

    goto :goto_1

    .line 1803872
    :pswitch_3
    if-nez p2, :cond_0

    .line 1803873
    const/4 v1, 0x2

    invoke-interface {p1, p0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 1803874
    invoke-static {v9, v3}, LX/BdW;->a(LX/BcP;Z)V

    goto :goto_1

    .line 1803875
    :pswitch_4
    if-nez p2, :cond_0

    .line 1803876
    const/16 v1, 0x61

    invoke-interface {p1, p0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 1803877
    invoke-static {v9, v3}, LX/BdW;->a(LX/BcP;Z)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch -0x18b49cfc
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final a(LX/BcO;LX/BcO;)V
    .locals 1

    .prologue
    .line 1803852
    check-cast p1, LX/BdU;

    .line 1803853
    check-cast p2, LX/BdU;

    .line 1803854
    iget-boolean v0, p1, LX/BdU;->e:Z

    iput-boolean v0, p2, LX/BdU;->e:Z

    .line 1803855
    return-void
.end method

.method public final a(LX/BcP;Ljava/util/List;LX/BcO;)V
    .locals 2

    .prologue
    .line 1803847
    check-cast p3, LX/BdU;

    .line 1803848
    iget-object v0, p3, LX/BdU;->b:Lcom/facebook/components/list/fb/fragment/ListComponentFragment;

    .line 1803849
    const v1, -0x18b49cfc

    const/4 p0, 0x1

    new-array p0, p0, [Ljava/lang/Object;

    const/4 p3, 0x0

    aput-object p1, p0, p3

    invoke-static {p1, v1, p0}, LX/BcS;->a(LX/BcP;I[Ljava/lang/Object;)LX/BcQ;

    move-result-object v1

    move-object v1, v1

    .line 1803850
    invoke-virtual {v0, p1, v1}, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->a(LX/BcP;LX/BcQ;)LX/BcO;

    move-result-object v1

    invoke-interface {p2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1803851
    return-void
.end method

.method public final c(LX/BcP;LX/BcO;)V
    .locals 1

    .prologue
    .line 1803840
    check-cast p2, LX/BdU;

    .line 1803841
    invoke-static {}, LX/BcS;->a()LX/1np;

    move-result-object v0

    .line 1803842
    const/4 p0, 0x0

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    .line 1803843
    iput-object p0, v0, LX/1np;->a:Ljava/lang/Object;

    .line 1803844
    iget-object p0, v0, LX/1np;->a:Ljava/lang/Object;

    move-object v0, p0

    .line 1803845
    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p2, LX/BdU;->e:Z

    .line 1803846
    return-void
.end method
