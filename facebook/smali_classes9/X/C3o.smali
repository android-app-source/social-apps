.class public final LX/C3o;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/C3p;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public b:I

.field public c:Z

.field public final synthetic d:LX/C3p;


# direct methods
.method public constructor <init>(LX/C3p;)V
    .locals 1

    .prologue
    .line 1846267
    iput-object p1, p0, LX/C3o;->d:LX/C3p;

    .line 1846268
    move-object v0, p1

    .line 1846269
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1846270
    sget v0, Lcom/facebook/feedplugins/condensedstory/thumbnail/CondensedStoryPhotoComponentSpec;->a:I

    iput v0, p0, LX/C3o;->b:I

    .line 1846271
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1846287
    const-string v0, "CondensedStoryPhotoComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1846272
    if-ne p0, p1, :cond_1

    .line 1846273
    :cond_0
    :goto_0
    return v0

    .line 1846274
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1846275
    goto :goto_0

    .line 1846276
    :cond_3
    check-cast p1, LX/C3o;

    .line 1846277
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1846278
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1846279
    if-eq v2, v3, :cond_0

    .line 1846280
    iget-object v2, p0, LX/C3o;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/C3o;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/C3o;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1846281
    goto :goto_0

    .line 1846282
    :cond_5
    iget-object v2, p1, LX/C3o;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 1846283
    :cond_6
    iget v2, p0, LX/C3o;->b:I

    iget v3, p1, LX/C3o;->b:I

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 1846284
    goto :goto_0

    .line 1846285
    :cond_7
    iget-boolean v2, p0, LX/C3o;->c:Z

    iget-boolean v3, p1, LX/C3o;->c:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 1846286
    goto :goto_0
.end method
