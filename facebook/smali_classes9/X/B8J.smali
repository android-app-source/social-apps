.class public final LX/B8J;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:LX/B8K;


# direct methods
.method public constructor <init>(LX/B8K;)V
    .locals 0

    .prologue
    .line 1748668
    iput-object p1, p0, LX/B8J;->a:LX/B8K;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 1

    .prologue
    .line 1748669
    iget-object v0, p0, LX/B8J;->a:LX/B8K;

    iget-object v0, v0, LX/B8K;->g:LX/B7w;

    if-eqz v0, :cond_0

    .line 1748670
    iget-object v0, p0, LX/B8J;->a:LX/B8K;

    iget-object v0, v0, LX/B8K;->f:Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->j()Ljava/lang/String;

    iget-object v0, p0, LX/B8J;->a:LX/B8K;

    invoke-virtual {v0}, LX/B8K;->getInputValue()Ljava/lang/String;

    .line 1748671
    :cond_0
    iget-object v0, p0, LX/B8J;->a:LX/B8K;

    .line 1748672
    iget-object p1, v0, LX/B8K;->i:Landroid/widget/TextView;

    invoke-virtual {p1}, Landroid/widget/TextView;->getVisibility()I

    move-result p1

    if-nez p1, :cond_2

    const/4 p1, 0x1

    :goto_0
    move v0, p1

    .line 1748673
    if-eqz v0, :cond_1

    .line 1748674
    iget-object v0, p0, LX/B8J;->a:LX/B8K;

    invoke-static {v0}, LX/B8K;->g(LX/B8K;)V

    .line 1748675
    iget-object v0, p0, LX/B8J;->a:LX/B8K;

    iget-object v0, v0, LX/B8K;->i:Landroid/widget/TextView;

    invoke-static {v0}, LX/B8v;->a(Landroid/widget/TextView;)V

    .line 1748676
    :cond_1
    return-void

    :cond_2
    const/4 p1, 0x0

    goto :goto_0
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1748677
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1748678
    return-void
.end method
