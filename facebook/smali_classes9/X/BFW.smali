.class public LX/BFW;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/BFW;


# instance fields
.field public a:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1765928
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1765929
    return-void
.end method

.method public static a(LX/0QB;)LX/BFW;
    .locals 3

    .prologue
    .line 1765930
    sget-object v0, LX/BFW;->b:LX/BFW;

    if-nez v0, :cond_1

    .line 1765931
    const-class v1, LX/BFW;

    monitor-enter v1

    .line 1765932
    :try_start_0
    sget-object v0, LX/BFW;->b:LX/BFW;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1765933
    if-eqz v2, :cond_0

    .line 1765934
    :try_start_1
    new-instance v0, LX/BFW;

    invoke-direct {v0}, LX/BFW;-><init>()V

    .line 1765935
    move-object v0, v0

    .line 1765936
    sput-object v0, LX/BFW;->b:LX/BFW;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1765937
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1765938
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1765939
    :cond_1
    sget-object v0, LX/BFW;->b:LX/BFW;

    return-object v0

    .line 1765940
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1765941
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
