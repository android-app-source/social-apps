.class public abstract LX/BeU;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements LX/BeT;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Landroid/view/View;",
        ":",
        "LX/BeO;",
        ">",
        "Lcom/facebook/widget/CustomFrameLayout;",
        "LX/BeT;"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public b:I

.field public c:Z

.field public d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/Bfc",
            "<TT;>;>;"
        }
    .end annotation
.end field

.field public k:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1804960
    const-class v0, LX/BeU;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/BeU;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1804961
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 1804962
    const/4 v0, 0x0

    iput v0, p0, LX/BeU;->b:I

    .line 1804963
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/BeU;->c:Z

    .line 1804964
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/BeU;->d:Ljava/util/List;

    .line 1804965
    const-class v0, LX/BeU;

    invoke-static {v0, p0}, LX/BeU;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1804966
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1804967
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1804968
    const/4 v0, 0x0

    iput v0, p0, LX/BeU;->b:I

    .line 1804969
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/BeU;->c:Z

    .line 1804970
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/BeU;->d:Ljava/util/List;

    .line 1804971
    const-class v0, LX/BeU;

    invoke-static {v0, p0}, LX/BeU;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1804972
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1804973
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1804974
    const/4 v0, 0x0

    iput v0, p0, LX/BeU;->b:I

    .line 1804975
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/BeU;->c:Z

    .line 1804976
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/BeU;->d:Ljava/util/List;

    .line 1804977
    const-class v0, LX/BeU;

    invoke-static {v0, p0}, LX/BeU;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1804978
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/BeU;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v1

    check-cast v1, LX/03V;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object p0

    check-cast p0, Ljava/util/concurrent/Executor;

    iput-object v1, p1, LX/BeU;->k:LX/03V;

    iput-object p0, p1, LX/BeU;->l:Ljava/util/concurrent/Executor;

    return-void
.end method

.method public static a$redex0(LX/BeU;Lcom/google/common/util/concurrent/ListenableFuture;LX/Bfc;Lcom/google/common/util/concurrent/ListenableFuture;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/Bfc",
            "<TT;>;>;",
            "LX/Bfc",
            "<TT;>;",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<+",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1804979
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v1, 0x0

    .line 1804980
    new-instance v2, LX/Bfm;

    invoke-direct {v2, p0}, LX/Bfm;-><init>(LX/BeU;)V

    iget-object v3, p0, LX/BeU;->l:Ljava/util/concurrent/Executor;

    invoke-static {p3, v2, v3}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    move-object v2, v2

    .line 1804981
    new-instance v3, LX/Bfq;

    invoke-direct {v3, p0, p2}, LX/Bfq;-><init>(LX/BeU;LX/Bfc;)V

    iget-object v4, p0, LX/BeU;->l:Ljava/util/concurrent/Executor;

    invoke-static {v2, v3, v4}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    move-object v2, v2

    .line 1804982
    aput-object v2, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    invoke-static {v0}, LX/0Vg;->a([Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1804983
    new-instance v1, LX/Bfo;

    invoke-direct {v1, p0, p2, p1}, LX/Bfo;-><init>(LX/BeU;LX/Bfc;Lcom/google/common/util/concurrent/ListenableFuture;)V

    iget-object v2, p0, LX/BeU;->l:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1804984
    return-void
.end method

.method public static f(LX/BeU;I)LX/Bfc;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/Bfc",
            "<TT;>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1804985
    if-ltz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1804986
    iget-object v0, p0, LX/BeU;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_1

    :goto_1
    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 1804987
    iget-object v0, p0, LX/BeU;->d:Ljava/util/List;

    iget-object v1, p0, LX/BeU;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    sub-int/2addr v1, p1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Bfc;

    return-object v0

    :cond_0
    move v0, v2

    .line 1804988
    goto :goto_0

    :cond_1
    move v1, v2

    .line 1804989
    goto :goto_1
.end method

.method public static f(LX/BeU;)V
    .locals 6

    .prologue
    .line 1804990
    invoke-virtual {p0}, LX/BeU;->getChildCount()I

    move-result v0

    .line 1804991
    invoke-virtual {p0}, LX/BeU;->getNumQuestions()I

    move-result v1

    iget v2, p0, LX/BeU;->b:I

    sub-int/2addr v1, v2

    .line 1804992
    const/4 v2, 0x3

    invoke-static {v2, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    move v1, v1

    .line 1804993
    :goto_0
    if-ge v0, v1, :cond_1

    .line 1804994
    new-instance v2, LX/Bfc;

    invoke-virtual {p0}, LX/BeU;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3, v0}, LX/Bfc;-><init>(Landroid/content/Context;I)V

    .line 1804995
    const/4 v3, 0x0

    .line 1804996
    invoke-virtual {p0, v2, v3}, LX/BeU;->addView(Landroid/view/View;I)V

    .line 1804997
    iget-object v4, p0, LX/BeU;->d:Ljava/util/List;

    invoke-interface {v4, v3, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 1804998
    if-eqz v0, :cond_0

    .line 1804999
    add-int/lit8 v2, v0, -0x1

    invoke-static {p0, v2}, LX/BeU;->f(LX/BeU;I)LX/Bfc;

    move-result-object v2

    const/4 v3, 0x1

    .line 1805000
    iget-object v5, v2, LX/Bfc;->c:Landroid/view/View;

    if-eqz v3, :cond_2

    const/4 v4, 0x0

    :goto_1
    invoke-virtual {v5, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1805001
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1805002
    :cond_1
    return-void

    .line 1805003
    :cond_2
    const/16 v4, 0x8

    goto :goto_1
.end method


# virtual methods
.method public abstract a(I)Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<TT;>;"
        }
    .end annotation
.end method

.method public final a()V
    .locals 3

    .prologue
    .line 1804954
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/BeU;->c:Z

    .line 1804955
    invoke-virtual {p0}, LX/BeU;->removeAllViews()V

    .line 1804956
    invoke-virtual {p0}, LX/BeU;->getNumQuestions()I

    move-result v0

    iget v1, p0, LX/BeU;->b:I

    sub-int/2addr v0, v1

    if-nez v0, :cond_0

    .line 1804957
    :goto_0
    return-void

    .line 1804958
    :cond_0
    invoke-static {p0}, LX/BeU;->f(LX/BeU;)V

    .line 1804959
    const/4 v0, 0x0

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p0, v1}, LX/BeU;->f(LX/BeU;I)LX/Bfc;

    move-result-object v1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v2}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    invoke-static {p0, v0, v1, v2}, LX/BeU;->a$redex0(LX/BeU;Lcom/google/common/util/concurrent/ListenableFuture;LX/Bfc;Lcom/google/common/util/concurrent/ListenableFuture;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/common/util/concurrent/ListenableFuture;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<+",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1804939
    invoke-virtual {p0}, LX/BeU;->getChildCount()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 1804940
    iget v2, p0, LX/BeU;->b:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, LX/BeU;->b:I

    .line 1804941
    const/4 v2, 0x0

    invoke-static {p0, v2}, LX/BeU;->f(LX/BeU;I)LX/Bfc;

    move-result-object v2

    .line 1804942
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v4

    .line 1804943
    new-instance v5, LX/Bg2;

    const/high16 v6, 0x3fa00000    # 1.25f

    invoke-direct {v5, v2, v6}, LX/Bg2;-><init>(Landroid/view/View;F)V

    .line 1804944
    invoke-virtual {p0}, LX/BeU;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0c0054

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v6

    int-to-long v6, v6

    invoke-virtual {v5, v6, v7}, LX/Bg2;->setDuration(J)V

    .line 1804945
    new-instance v6, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v7, 0x40000000    # 2.0f

    invoke-direct {v6, v7}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    invoke-virtual {v5, v6}, LX/Bg2;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 1804946
    new-instance v6, LX/Bfp;

    invoke-direct {v6, p0, v2, v4}, LX/Bfp;-><init>(LX/BeU;LX/Bfc;Lcom/google/common/util/concurrent/SettableFuture;)V

    invoke-virtual {v5, v6}, LX/Bg2;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1804947
    invoke-virtual {v2, v5}, LX/Bfc;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1804948
    move-object v2, v4

    .line 1804949
    const/4 v3, 0x1

    invoke-static {p0, v3}, LX/BeU;->f(LX/BeU;I)LX/Bfc;

    move-result-object v3

    invoke-static {p0, v2, v3, p1}, LX/BeU;->a$redex0(LX/BeU;Lcom/google/common/util/concurrent/ListenableFuture;LX/Bfc;Lcom/google/common/util/concurrent/ListenableFuture;)V

    .line 1804950
    :goto_0
    return-void

    .line 1804951
    :cond_0
    iget-object v0, p0, LX/BeU;->k:LX/03V;

    sget-object v1, LX/BeU;->a:Ljava/lang/String;

    const-string v2, "onChildFinished() called by the last card in the stack"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1804952
    invoke-virtual {p0}, LX/BeU;->removeAllViews()V

    .line 1804953
    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1804937
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/BeU;->a(Lcom/google/common/util/concurrent/ListenableFuture;)V

    .line 1804938
    return-void
.end method

.method public abstract a(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract b(I)V
.end method

.method public final b(Lcom/google/common/util/concurrent/ListenableFuture;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<+",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1804935
    invoke-virtual {p0, p1}, LX/BeU;->a(Lcom/google/common/util/concurrent/ListenableFuture;)V

    .line 1804936
    return-void
.end method

.method public abstract getNumQuestions()I
.end method

.method public final kD_()V
    .locals 1

    .prologue
    .line 1804933
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/BeU;->a(Lcom/google/common/util/concurrent/ListenableFuture;)V

    .line 1804934
    return-void
.end method

.method public final onMeasure(II)V
    .locals 3

    .prologue
    .line 1804921
    const/4 v0, 0x0

    .line 1804922
    invoke-virtual {p0}, LX/BeU;->getChildCount()I

    move-result v1

    const/4 v2, 0x2

    if-ge v1, v2, :cond_1

    .line 1804923
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;->onMeasure(II)V

    .line 1804924
    return-void

    .line 1804925
    :cond_1
    invoke-static {p0, v0}, LX/BeU;->f(LX/BeU;I)LX/Bfc;

    move-result-object v1

    .line 1804926
    iget v2, v1, LX/Bfc;->g:I

    if-eqz v2, :cond_2

    .line 1804927
    :goto_0
    invoke-static {p0, v0}, LX/BeU;->f(LX/BeU;I)LX/Bfc;

    move-result-object v1

    .line 1804928
    iget v2, v1, LX/Bfc;->f:I

    move v1, v2

    .line 1804929
    :goto_1
    invoke-virtual {p0}, LX/BeU;->getChildCount()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 1804930
    invoke-static {p0, v0}, LX/BeU;->f(LX/BeU;I)LX/Bfc;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/Bfc;->setContentHeight(I)V

    .line 1804931
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1804932
    :cond_2
    invoke-static {v1, p1}, LX/Bfc;->d(LX/Bfc;I)V

    goto :goto_0
.end method

.method public final removeAllViews()V
    .locals 1

    .prologue
    .line 1804918
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->removeAllViews()V

    .line 1804919
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/BeU;->d:Ljava/util/List;

    .line 1804920
    return-void
.end method
