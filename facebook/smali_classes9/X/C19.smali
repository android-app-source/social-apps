.class public final LX/C19;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/C1A;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/1Po;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public c:Ljava/lang/String;

.field public final synthetic d:LX/C1A;


# direct methods
.method public constructor <init>(LX/C1A;)V
    .locals 1

    .prologue
    .line 1841954
    iput-object p1, p0, LX/C19;->d:LX/C1A;

    .line 1841955
    move-object v0, p1

    .line 1841956
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1841957
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1841958
    const-string v0, "PollAddOptionComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1841959
    if-ne p0, p1, :cond_1

    .line 1841960
    :cond_0
    :goto_0
    return v0

    .line 1841961
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1841962
    goto :goto_0

    .line 1841963
    :cond_3
    check-cast p1, LX/C19;

    .line 1841964
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1841965
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1841966
    if-eq v2, v3, :cond_0

    .line 1841967
    iget-object v2, p0, LX/C19;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/C19;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/C19;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1841968
    goto :goto_0

    .line 1841969
    :cond_5
    iget-object v2, p1, LX/C19;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 1841970
    :cond_6
    iget-object v2, p0, LX/C19;->b:LX/1Po;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/C19;->b:LX/1Po;

    iget-object v3, p1, LX/C19;->b:LX/1Po;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1841971
    goto :goto_0

    .line 1841972
    :cond_8
    iget-object v2, p1, LX/C19;->b:LX/1Po;

    if-nez v2, :cond_7

    .line 1841973
    :cond_9
    iget-object v2, p0, LX/C19;->c:Ljava/lang/String;

    if-eqz v2, :cond_a

    iget-object v2, p0, LX/C19;->c:Ljava/lang/String;

    iget-object v3, p1, LX/C19;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1841974
    goto :goto_0

    .line 1841975
    :cond_a
    iget-object v2, p1, LX/C19;->c:Ljava/lang/String;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
