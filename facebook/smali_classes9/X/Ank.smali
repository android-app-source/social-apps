.class public final LX/Ank;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ":",
        "LX/3VG;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/feedplugins/base/blingbar/BlingBarAnimationBuilder$BlingBarAnimationBuilderCallbacks",
        "<TV;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/feedplugins/base/blingbar/BlingBarAnimationsPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/base/blingbar/BlingBarAnimationsPartDefinition;)V
    .locals 0

    .prologue
    .line 1712829
    iput-object p1, p0, LX/Ank;->a:Lcom/facebook/feedplugins/base/blingbar/BlingBarAnimationsPartDefinition;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Landroid/view/View;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;Z)V"
        }
    .end annotation

    .prologue
    .line 1712830
    if-eqz p2, :cond_0

    const/4 v0, -0x2

    .line 1712831
    :goto_0
    check-cast p1, LX/3VG;

    invoke-interface {p1, v0}, LX/3VG;->setHeight(I)V

    .line 1712832
    return-void

    .line 1712833
    :cond_0
    iget-object v0, p0, LX/Ank;->a:Lcom/facebook/feedplugins/base/blingbar/BlingBarAnimationsPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/base/blingbar/BlingBarAnimationsPartDefinition;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ann;

    .line 1712834
    iget p0, v0, LX/Ann;->b:I

    move v0, p0

    .line 1712835
    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/view/View;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1712836
    check-cast p1, LX/3VG;

    invoke-interface {p1}, LX/3VG;->getLikersCountView()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/view/View;ZIII)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;ZIII)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1712837
    move-object v0, p1

    check-cast v0, LX/3VG;

    invoke-interface {v0, p2}, LX/3VG;->setIsExpanded(Z)V

    .line 1712838
    invoke-direct {p0, p1, p2}, LX/Ank;->a(Landroid/view/View;Z)V

    .line 1712839
    if-eqz p2, :cond_0

    move-object v0, p1

    .line 1712840
    check-cast v0, LX/3VG;

    invoke-interface {v0, p3}, LX/3VG;->setLikes(I)V

    move-object v0, p1

    .line 1712841
    check-cast v0, LX/3VG;

    invoke-interface {v0, p4}, LX/3VG;->setComments(I)V

    .line 1712842
    check-cast p1, LX/3VG;

    invoke-interface {p1, p5}, LX/3VG;->setShares(I)V

    .line 1712843
    :goto_0
    return-void

    :cond_0
    move-object v0, p1

    .line 1712844
    check-cast v0, LX/3VG;

    invoke-interface {v0, v1}, LX/3VG;->setLikes(I)V

    move-object v0, p1

    .line 1712845
    check-cast v0, LX/3VG;

    invoke-interface {v0, v1}, LX/3VG;->setComments(I)V

    .line 1712846
    check-cast p1, LX/3VG;

    invoke-interface {p1, v1}, LX/3VG;->setShares(I)V

    goto :goto_0
.end method

.method public final b(Landroid/view/View;)Landroid/view/View;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)",
            "Landroid/view/View;"
        }
    .end annotation

    .prologue
    .line 1712847
    check-cast p1, LX/3VG;

    invoke-interface {p1}, LX/3VG;->getContainerView()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final c(Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 1712848
    check-cast p1, LX/3VG;

    invoke-interface {p1}, LX/3VG;->getLikersCountView()Landroid/view/View;

    move-result-object v0

    .line 1712849
    invoke-virtual {v0, v1}, Landroid/view/View;->setScaleX(F)V

    .line 1712850
    invoke-virtual {v0, v1}, Landroid/view/View;->setScaleY(F)V

    .line 1712851
    return-void
.end method
