.class public final LX/CJ8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/CJ7;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/attribution/InlineReplyFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/attribution/InlineReplyFragment;)V
    .locals 0

    .prologue
    .line 1875090
    iput-object p1, p0, LX/CJ8;->a:Lcom/facebook/messaging/attribution/InlineReplyFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1875072
    iget-object v0, p0, LX/CJ8;->a:Lcom/facebook/messaging/attribution/InlineReplyFragment;

    .line 1875073
    iget-object v1, v0, Lcom/facebook/messaging/attribution/InlineReplyFragment;->y:LX/3if;

    invoke-virtual {v1}, LX/3if;->a()V

    .line 1875074
    iget-boolean v1, v0, Lcom/facebook/messaging/attribution/InlineReplyFragment;->I:Z

    if-eqz v1, :cond_0

    .line 1875075
    iget-object v1, v0, Lcom/facebook/messaging/attribution/InlineReplyFragment;->s:LX/CJL;

    iget-object p0, v0, Lcom/facebook/messaging/attribution/InlineReplyFragment;->C:Ljava/lang/String;

    invoke-virtual {v1, p0}, LX/CJL;->b(Ljava/lang/String;)V

    .line 1875076
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 7

    .prologue
    .line 1875077
    iget-object v0, p0, LX/CJ8;->a:Lcom/facebook/messaging/attribution/InlineReplyFragment;

    .line 1875078
    iget-object v1, v0, Lcom/facebook/messaging/attribution/InlineReplyFragment;->m:LX/0Sh;

    invoke-virtual {v1}, LX/0Sh;->a()V

    .line 1875079
    iget-object v1, v0, Lcom/facebook/messaging/attribution/InlineReplyFragment;->M:LX/CJP;

    if-eqz v1, :cond_1

    .line 1875080
    iget-object v1, v0, Lcom/facebook/messaging/attribution/InlineReplyFragment;->M:LX/CJP;

    iget-object v2, v0, Lcom/facebook/messaging/attribution/InlineReplyFragment;->A:Lcom/facebook/ui/media/attachments/MediaResource;

    iget-object v3, v0, Lcom/facebook/messaging/attribution/InlineReplyFragment;->B:Landroid/content/Intent;

    iget-object v4, v0, Lcom/facebook/messaging/attribution/InlineReplyFragment;->D:Ljava/lang/String;

    iget-object v6, v0, Lcom/facebook/messaging/attribution/InlineReplyFragment;->N:Lcom/facebook/messaging/model/attribution/ContentAppAttribution;

    .line 1875081
    iget-object p0, v1, LX/CJP;->a:LX/CJR;

    iget-object p0, p0, LX/CJR;->v:LX/CJQ;

    if-eqz p0, :cond_1

    .line 1875082
    if-nez v6, :cond_0

    .line 1875083
    iget-object p0, v1, LX/CJP;->a:LX/CJR;

    iget-object p0, p0, LX/CJR;->c:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/3N1;

    invoke-virtual {p0, v3, v4}, LX/3N1;->a(Landroid/content/Intent;Ljava/lang/String;)Lcom/facebook/messaging/model/attribution/ContentAppAttribution;

    .line 1875084
    :cond_0
    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    .line 1875085
    :cond_1
    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->d()V

    .line 1875086
    iget-boolean v1, v0, Lcom/facebook/messaging/attribution/InlineReplyFragment;->I:Z

    if-eqz v1, :cond_2

    .line 1875087
    iget-object v1, v0, Lcom/facebook/messaging/attribution/InlineReplyFragment;->s:LX/CJL;

    iget-object v2, v0, Lcom/facebook/messaging/attribution/InlineReplyFragment;->C:Ljava/lang/String;

    .line 1875088
    const-string v3, "send_inline_reply_dialog_event"

    const-string v4, "platform_app"

    invoke-static {v1, v3, v2, v4}, LX/CJL;->a(LX/CJL;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1875089
    :cond_2
    return-void
.end method
