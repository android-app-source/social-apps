.class public final LX/BEy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/payments/paymentmethods/provider/model/NmorPaymentProvider;

.field public final synthetic b:LX/BF1;


# direct methods
.method public constructor <init>(LX/BF1;Lcom/facebook/payments/paymentmethods/provider/model/NmorPaymentProvider;)V
    .locals 0

    .prologue
    .line 1765286
    iput-object p1, p0, LX/BEy;->b:LX/BF1;

    iput-object p2, p0, LX/BEy;->a:Lcom/facebook/payments/paymentmethods/provider/model/NmorPaymentProvider;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, 0x394240fc

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1765287
    iget-object v1, p0, LX/BEy;->b:LX/BF1;

    iget-object v1, v1, LX/BF1;->a:Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;

    if-eqz v1, :cond_0

    .line 1765288
    iget-object v1, p0, LX/BEy;->b:LX/BF1;

    iget-object v1, v1, LX/BF1;->a:Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;

    iget-object v2, p0, LX/BEy;->a:Lcom/facebook/payments/paymentmethods/provider/model/NmorPaymentProvider;

    .line 1765289
    iput-object v2, v1, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;->f:Lcom/facebook/payments/paymentmethods/provider/model/NmorPaymentProvider;

    .line 1765290
    instance-of v4, v2, Lcom/facebook/payments/paymentmethods/provider/model/OnboardedWebPaymentProvider;

    if-eqz v4, :cond_1

    .line 1765291
    iget-object v4, v1, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;->b:Lcom/facebook/content/SecureContextHelper;

    check-cast v2, Lcom/facebook/payments/paymentmethods/provider/model/OnboardedWebPaymentProvider;

    .line 1765292
    iget-object p0, v2, Lcom/facebook/payments/paymentmethods/provider/model/OnboardedWebPaymentProvider;->c:Landroid/net/Uri;

    move-object p0, p0

    .line 1765293
    invoke-static {p0}, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;->a(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object p0

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-interface {v4, p0, p1}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1765294
    :cond_0
    :goto_0
    const v1, 0x56a144c8

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1765295
    :cond_1
    instance-of v4, v2, Lcom/facebook/payments/paymentmethods/provider/model/AvailableWebPaymentProvider;

    if-eqz v4, :cond_0

    .line 1765296
    iget-object v4, v1, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;->b:Lcom/facebook/content/SecureContextHelper;

    check-cast v2, Lcom/facebook/payments/paymentmethods/provider/model/AvailableWebPaymentProvider;

    .line 1765297
    iget-object p0, v2, Lcom/facebook/payments/paymentmethods/provider/model/AvailableWebPaymentProvider;->c:Landroid/net/Uri;

    move-object p0, p0

    .line 1765298
    invoke-static {p0}, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;->a(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object p0

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-interface {v4, p0, p1}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0
.end method
