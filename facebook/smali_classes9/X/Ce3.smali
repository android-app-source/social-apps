.class public final LX/Ce3;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1923970
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 1923971
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1923972
    :goto_0
    return v1

    .line 1923973
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1923974
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 1923975
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1923976
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1923977
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 1923978
    const-string v4, "event"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1923979
    invoke-static {p0, p1}, LX/Ce2;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 1923980
    :cond_2
    const-string v4, "primary_text"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1923981
    invoke-static {p0, p1}, LX/4ap;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 1923982
    :cond_3
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1923983
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1923984
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1923985
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    move v2, v1

    goto :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1923986
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1923987
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1923988
    if-eqz v0, :cond_0

    .line 1923989
    const-string v1, "event"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1923990
    invoke-static {p0, v0, p2, p3}, LX/Ce2;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1923991
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1923992
    if-eqz v0, :cond_1

    .line 1923993
    const-string v1, "primary_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1923994
    invoke-static {p0, v0, p2, p3}, LX/4ap;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1923995
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1923996
    return-void
.end method
