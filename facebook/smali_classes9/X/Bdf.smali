.class public LX/Bdf;
.super LX/5Jl;
.source ""

# interfaces
.implements LX/BcX;


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/Bdd;",
            ">;"
        }
    .end annotation
.end field

.field private b:I


# direct methods
.method public constructor <init>(Landroid/content/Context;IZI)V
    .locals 3

    .prologue
    .line 1803959
    const/high16 v0, -0x80000000

    if-ne p4, v0, :cond_0

    .line 1803960
    new-instance v0, LX/1P1;

    invoke-direct {v0, p2, p3}, LX/1P1;-><init>(IZ)V

    .line 1803961
    :goto_0
    move-object v0, v0

    .line 1803962
    invoke-direct {p0, p1, v0}, LX/5Jl;-><init>(Landroid/content/Context;LX/1P1;)V

    .line 1803963
    const/4 v0, -0x1

    iput v0, p0, LX/Bdf;->b:I

    .line 1803964
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/Bdf;->a:Ljava/util/List;

    .line 1803965
    new-instance v0, LX/Bde;

    iget-object v1, p0, LX/Bdf;->a:Ljava/util/List;

    invoke-direct {v0, v1}, LX/Bde;-><init>(Ljava/util/List;)V

    invoke-virtual {p0, v0}, LX/3mY;->a(LX/3me;)V

    .line 1803966
    return-void

    :cond_0
    new-instance v0, LX/62u;

    invoke-direct {v0, p1, p2, p3, p4}, LX/62u;-><init>(Landroid/content/Context;IZI)V

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1De;I)LX/1X1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "I)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1803936
    iget-object v0, p0, LX/Bdf;->a:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Bdd;

    iget-object v0, v0, LX/Bdd;->a:LX/1X1;

    return-object v0
.end method

.method public final a(ILX/1X1;Z)V
    .locals 2

    .prologue
    .line 1803956
    iget-object v0, p0, LX/Bdf;->a:Ljava/util/List;

    new-instance v1, LX/Bdd;

    invoke-direct {v1, p2, p3}, LX/Bdd;-><init>(LX/1X1;Z)V

    invoke-interface {v0, p1, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 1803957
    invoke-virtual {p0, p1}, LX/3mY;->u_(I)V

    .line 1803958
    return-void
.end method

.method public final a(Landroid/support/v7/widget/RecyclerView;)V
    .locals 2

    .prologue
    .line 1803952
    invoke-super {p0, p1}, LX/5Jl;->a(Landroid/support/v7/widget/RecyclerView;)V

    .line 1803953
    iget v0, p0, LX/Bdf;->b:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 1803954
    iget v0, p0, LX/Bdf;->b:I

    invoke-virtual {p1, v0}, Landroid/support/v7/widget/RecyclerView;->g_(I)V

    .line 1803955
    :cond_0
    return-void
.end method

.method public final a(I)Z
    .locals 1

    .prologue
    .line 1803951
    iget-object v0, p0, LX/Bdf;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Bdd;

    iget-boolean v0, v0, LX/Bdd;->b:Z

    return v0
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 1803948
    iget-object v0, p0, LX/Bdf;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 1803949
    invoke-virtual {p0, p1}, LX/3mY;->c(I)V

    .line 1803950
    return-void
.end method

.method public final b(ILX/1X1;Z)V
    .locals 2

    .prologue
    .line 1803944
    iget-object v0, p0, LX/Bdf;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 1803945
    iget-object v0, p0, LX/Bdf;->a:Ljava/util/List;

    new-instance v1, LX/Bdd;

    invoke-direct {v1, p2, p3}, LX/Bdd;-><init>(LX/1X1;Z)V

    invoke-interface {v0, p1, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 1803946
    invoke-virtual {p0, p1}, LX/3mY;->b_(I)V

    .line 1803947
    return-void
.end method

.method public final b(Landroid/support/v7/widget/RecyclerView;)V
    .locals 1

    .prologue
    .line 1803941
    invoke-virtual {p0}, LX/5Je;->k()LX/1OR;

    move-result-object v0

    check-cast v0, LX/1P1;

    invoke-virtual {v0}, LX/1P1;->l()I

    move-result v0

    iput v0, p0, LX/Bdf;->b:I

    .line 1803942
    invoke-super {p0, p1}, LX/5Jl;->b(Landroid/support/v7/widget/RecyclerView;)V

    .line 1803943
    return-void
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 1803940
    iget-object v0, p0, LX/Bdf;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final synthetic e(Landroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 1803939
    check-cast p1, Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {p0, p1}, LX/Bdf;->a(Landroid/support/v7/widget/RecyclerView;)V

    return-void
.end method

.method public final synthetic f(Landroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 1803938
    check-cast p1, Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {p0, p1}, LX/Bdf;->b(Landroid/support/v7/widget/RecyclerView;)V

    return-void
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 1803937
    const/4 v0, 0x1

    return v0
.end method
