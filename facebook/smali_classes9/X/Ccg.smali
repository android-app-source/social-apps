.class public final LX/Ccg;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/Cch;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;

.field public b:Z

.field public c:Z

.field public d:LX/1Pq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public e:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public final synthetic f:LX/Cch;


# direct methods
.method public constructor <init>(LX/Cch;)V
    .locals 1

    .prologue
    .line 1921521
    iput-object p1, p0, LX/Ccg;->f:LX/Cch;

    .line 1921522
    move-object v0, p1

    .line 1921523
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1921524
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1921520
    const-string v0, "ObjectionableContentWarningComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1921499
    if-ne p0, p1, :cond_1

    .line 1921500
    :cond_0
    :goto_0
    return v0

    .line 1921501
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1921502
    goto :goto_0

    .line 1921503
    :cond_3
    check-cast p1, LX/Ccg;

    .line 1921504
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1921505
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1921506
    if-eq v2, v3, :cond_0

    .line 1921507
    iget-object v2, p0, LX/Ccg;->a:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/Ccg;->a:Ljava/lang/String;

    iget-object v3, p1, LX/Ccg;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1921508
    goto :goto_0

    .line 1921509
    :cond_5
    iget-object v2, p1, LX/Ccg;->a:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 1921510
    :cond_6
    iget-boolean v2, p0, LX/Ccg;->b:Z

    iget-boolean v3, p1, LX/Ccg;->b:Z

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 1921511
    goto :goto_0

    .line 1921512
    :cond_7
    iget-boolean v2, p0, LX/Ccg;->c:Z

    iget-boolean v3, p1, LX/Ccg;->c:Z

    if-eq v2, v3, :cond_8

    move v0, v1

    .line 1921513
    goto :goto_0

    .line 1921514
    :cond_8
    iget-object v2, p0, LX/Ccg;->d:LX/1Pq;

    if-eqz v2, :cond_a

    iget-object v2, p0, LX/Ccg;->d:LX/1Pq;

    iget-object v3, p1, LX/Ccg;->d:LX/1Pq;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    :cond_9
    move v0, v1

    .line 1921515
    goto :goto_0

    .line 1921516
    :cond_a
    iget-object v2, p1, LX/Ccg;->d:LX/1Pq;

    if-nez v2, :cond_9

    .line 1921517
    :cond_b
    iget-object v2, p0, LX/Ccg;->e:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_c

    iget-object v2, p0, LX/Ccg;->e:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/Ccg;->e:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1921518
    goto :goto_0

    .line 1921519
    :cond_c
    iget-object v2, p1, LX/Ccg;->e:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
