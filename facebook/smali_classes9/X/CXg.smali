.class public final LX/CXg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:LX/CSY;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/CT7;

.field public final synthetic d:LX/CTK;


# direct methods
.method public constructor <init>(LX/CSY;Ljava/lang/String;LX/CT7;LX/CTK;)V
    .locals 0

    .prologue
    .line 1910559
    iput-object p1, p0, LX/CXg;->a:LX/CSY;

    iput-object p2, p0, LX/CXg;->b:Ljava/lang/String;

    iput-object p3, p0, LX/CXg;->c:LX/CT7;

    iput-object p4, p0, LX/CXg;->d:LX/CTK;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 1910564
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1910563
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 4

    .prologue
    .line 1910560
    iget-object v0, p0, LX/CXg;->a:LX/CSY;

    iget-object v1, p0, LX/CXg;->b:Ljava/lang/String;

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/CSY;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1910561
    iget-object v0, p0, LX/CXg;->c:LX/CT7;

    iget-object v1, p0, LX/CXg;->d:LX/CTK;

    iget-object v1, v1, LX/CTJ;->o:Ljava/lang/String;

    iget-object v2, p0, LX/CXg;->d:LX/CTK;

    iget-object v2, v2, LX/CTK;->e:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    iget-object v3, p0, LX/CXg;->a:LX/CSY;

    invoke-virtual {v0, v1, v2, v3}, LX/CT7;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;LX/CSY;)V

    .line 1910562
    return-void
.end method
