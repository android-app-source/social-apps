.class public LX/CGp;
.super LX/5OG;
.source ""


# instance fields
.field private c:LX/CGk;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/CGk;)V
    .locals 0

    .prologue
    .line 1865879
    invoke-direct {p0, p1}, LX/5OG;-><init>(Landroid/content/Context;)V

    .line 1865880
    iput-object p2, p0, LX/CGp;->c:LX/CGk;

    .line 1865881
    return-void
.end method


# virtual methods
.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 1865882
    new-instance v0, LX/CGr;

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/CGr;-><init>(Landroid/content/Context;)V

    .line 1865883
    invoke-virtual {p0, p1}, LX/5OG;->getItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 1865884
    iget-object v2, p0, LX/CGp;->c:LX/CGk;

    .line 1865885
    iget-object p0, v0, LX/CGr;->j:Lcom/facebook/widget/SwitchCompat;

    invoke-interface {v1}, Landroid/view/MenuItem;->isChecked()Z

    move-result p1

    invoke-virtual {p0, p1}, Lcom/facebook/widget/SwitchCompat;->setChecked(Z)V

    .line 1865886
    iget-object p0, v0, LX/CGr;->j:Lcom/facebook/widget/SwitchCompat;

    invoke-interface {v1}, Landroid/view/MenuItem;->getTitle()Ljava/lang/CharSequence;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/facebook/widget/SwitchCompat;->setText(Ljava/lang/CharSequence;)V

    .line 1865887
    sget p0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 p1, 0x11

    if-ge p0, p1, :cond_0

    .line 1865888
    iget-object p0, v0, LX/CGr;->j:Lcom/facebook/widget/SwitchCompat;

    const/16 p1, 0x13

    invoke-virtual {p0, p1}, Lcom/facebook/widget/SwitchCompat;->setGravity(I)V

    .line 1865889
    :goto_0
    iget-object p0, v0, LX/CGr;->j:Lcom/facebook/widget/SwitchCompat;

    new-instance p1, LX/CGq;

    invoke-direct {p1, v0, v2, v1}, LX/CGq;-><init>(LX/CGr;LX/CGk;Landroid/view/MenuItem;)V

    invoke-virtual {p0, p1}, Lcom/facebook/widget/SwitchCompat;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 1865890
    return-object v0

    .line 1865891
    :cond_0
    iget-object p0, v0, LX/CGr;->j:Lcom/facebook/widget/SwitchCompat;

    const/4 p1, 0x5

    invoke-virtual {p0, p1}, Lcom/facebook/widget/SwitchCompat;->setTextAlignment(I)V

    goto :goto_0
.end method
