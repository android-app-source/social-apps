.class public final LX/Bjg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/app/DatePickerDialog$OnDateSetListener;


# instance fields
.field public final synthetic a:LX/Bji;


# direct methods
.method public constructor <init>(LX/Bji;)V
    .locals 0

    .prologue
    .line 1812700
    iput-object p1, p0, LX/Bjg;->a:LX/Bji;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onDateSet(Landroid/widget/DatePicker;III)V
    .locals 4

    .prologue
    .line 1812701
    iget-object v0, p0, LX/Bjg;->a:LX/Bji;

    iget-object v0, v0, LX/Bji;->e:Ljava/util/Calendar;

    iget-object v1, p0, LX/Bjg;->a:LX/Bji;

    iget-object v1, v1, LX/Bji;->f:Ljava/util/Calendar;

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 1812702
    iget-object v0, p0, LX/Bjg;->a:LX/Bji;

    iget-object v0, v0, LX/Bji;->e:Ljava/util/Calendar;

    invoke-virtual {v0, p2, p3, p4}, Ljava/util/Calendar;->set(III)V

    .line 1812703
    iget-object v0, p0, LX/Bjg;->a:LX/Bji;

    iget-object v1, p0, LX/Bjg;->a:LX/Bji;

    iget-object v1, v1, LX/Bji;->e:Ljava/util/Calendar;

    invoke-static {v0, v1}, LX/Bji;->a$redex0(LX/Bji;Ljava/util/Calendar;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1812704
    iget-object v0, p0, LX/Bjg;->a:LX/Bji;

    iget-object v0, v0, LX/Bji;->f:Ljava/util/Calendar;

    invoke-virtual {v0, p2, p3, p4}, Ljava/util/Calendar;->set(III)V

    .line 1812705
    iget-object v0, p0, LX/Bjg;->a:LX/Bji;

    invoke-static {v0}, LX/Bji;->d$redex0(LX/Bji;)V

    .line 1812706
    :cond_0
    return-void
.end method
