.class public LX/C02;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements LX/35q;


# instance fields
.field private final a:Lcom/facebook/attachments/angora/CoverPhotoWithPlayIconView;

.field public final b:Landroid/widget/ImageView;

.field public final c:Landroid/widget/LinearLayout;

.field public final d:Landroid/widget/TextView;

.field public final e:Landroid/widget/TextView;

.field public final f:Landroid/widget/ImageView;

.field public final g:Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1839807
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/C02;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1839808
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1839809
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/C02;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1839810
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1839811
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1839812
    const v0, 0x7f030bad

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1839813
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, LX/C02;->setBackgroundColor(I)V

    .line 1839814
    const v0, 0x7f0d1cff

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/attachments/angora/CoverPhotoWithPlayIconView;

    iput-object v0, p0, LX/C02;->a:Lcom/facebook/attachments/angora/CoverPhotoWithPlayIconView;

    .line 1839815
    const v0, 0x7f0d1d00

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/C02;->g:Landroid/widget/LinearLayout;

    .line 1839816
    const v0, 0x7f0d1d04

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/C02;->c:Landroid/widget/LinearLayout;

    .line 1839817
    const v0, 0x7f0d1d02

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/C02;->b:Landroid/widget/ImageView;

    .line 1839818
    const v0, 0x7f0d1d01

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/C02;->d:Landroid/widget/TextView;

    .line 1839819
    const v0, 0x7f0d1d03

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/C02;->e:Landroid/widget/TextView;

    .line 1839820
    const v0, 0x7f0d1d07

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/C02;->f:Landroid/widget/ImageView;

    .line 1839821
    return-void
.end method

.method public static a(Landroid/view/View;II)V
    .locals 3

    .prologue
    .line 1839822
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1839823
    if-nez v0, :cond_0

    .line 1839824
    :goto_0
    return-void

    .line 1839825
    :cond_0
    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    invoke-virtual {v0, v1, p1, v2, p2}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 1839826
    invoke-virtual {p0, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method


# virtual methods
.method public setLargeImageAspectRatio(F)V
    .locals 1

    .prologue
    .line 1839827
    iget-object v0, p0, LX/C02;->a:Lcom/facebook/attachments/angora/CoverPhotoWithPlayIconView;

    invoke-virtual {v0, p1}, Lcom/facebook/drawee/view/DraweeView;->setAspectRatio(F)V

    .line 1839828
    return-void
.end method

.method public setLargeImageController(LX/1aZ;)V
    .locals 1
    .param p1    # LX/1aZ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1839829
    iget-object v0, p0, LX/C02;->a:Lcom/facebook/attachments/angora/CoverPhotoWithPlayIconView;

    invoke-virtual {v0, p1}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 1839830
    return-void
.end method
