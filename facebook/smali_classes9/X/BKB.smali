.class public final LX/BKB;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/share/model/ComposerAppAttribution;

.field public final synthetic b:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;Lcom/facebook/share/model/ComposerAppAttribution;)V
    .locals 0

    .prologue
    .line 1772685
    iput-object p1, p0, LX/BKB;->b:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iput-object p2, p0, LX/BKB;->a:Lcom/facebook/share/model/ComposerAppAttribution;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1772702
    iget-object v0, p0, LX/BKB;->b:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v1, p0, LX/BKB;->b:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08249e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, p1, v1}, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->a$redex0(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 1772703
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 7
    .param p1    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1772686
    check-cast p1, Ljava/lang/String;

    .line 1772687
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1772688
    iget-object v0, p0, LX/BKB;->b:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    const/4 v1, 0x0

    iget-object v2, p0, LX/BKB;->b:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08249e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->a$redex0(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 1772689
    :goto_0
    return-void

    .line 1772690
    :cond_0
    iget-object v0, p0, LX/BKB;->b:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v0, v0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    if-eqz v0, :cond_1

    .line 1772691
    iget-object v0, p0, LX/BKB;->b:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v1, p0, LX/BKB;->b:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v1, v1, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    invoke-virtual {v1}, LX/BKm;->a()LX/BKl;

    move-result-object v1

    iget-object v2, p0, LX/BKB;->b:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v2, v2, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v2, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v2}, Lcom/facebook/platform/composer/model/PlatformComposition;->A()LX/BKp;

    move-result-object v2

    new-instance v3, Lcom/facebook/share/model/ComposerAppAttribution;

    iget-object v4, p0, LX/BKB;->a:Lcom/facebook/share/model/ComposerAppAttribution;

    invoke-virtual {v4}, Lcom/facebook/share/model/ComposerAppAttribution;->a()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, LX/BKB;->a:Lcom/facebook/share/model/ComposerAppAttribution;

    invoke-virtual {v5}, Lcom/facebook/share/model/ComposerAppAttribution;->c()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, LX/BKB;->a:Lcom/facebook/share/model/ComposerAppAttribution;

    invoke-virtual {v6}, Lcom/facebook/share/model/ComposerAppAttribution;->d()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v4, p1, v5, v6}, Lcom/facebook/share/model/ComposerAppAttribution;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1772692
    iput-object v3, v2, LX/BKp;->w:Lcom/facebook/share/model/ComposerAppAttribution;

    .line 1772693
    move-object v2, v2

    .line 1772694
    invoke-virtual {v2}, LX/BKp;->b()Lcom/facebook/platform/composer/model/PlatformComposition;

    move-result-object v2

    .line 1772695
    iput-object v2, v1, LX/BKl;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    .line 1772696
    move-object v1, v1

    .line 1772697
    invoke-virtual {v1}, LX/BKl;->a()LX/BKm;

    move-result-object v1

    .line 1772698
    iput-object v1, v0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    .line 1772699
    :cond_1
    iget-object v0, p0, LX/BKB;->b:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v0, v0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->G:Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;

    if-eqz v0, :cond_2

    .line 1772700
    iget-object v0, p0, LX/BKB;->b:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v0, v0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->G:Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;

    invoke-virtual {v0}, Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;->b()V

    .line 1772701
    :cond_2
    iget-object v0, p0, LX/BKB;->b:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    invoke-static {v0}, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->t$redex0(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V

    goto :goto_0
.end method
