.class public LX/AUG;
.super LX/AUE;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/AUE",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/AUA;

.field private final b:LX/AU9;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/AU9",
            "<*>;"
        }
    .end annotation
.end field

.field private c:Landroid/database/Cursor;

.field private d:LX/AUF;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final e:Ljava/util/concurrent/atomic/AtomicInteger;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/concurrent/ExecutorService;LX/AUA;LX/AU9;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/AUA;",
            "LX/AU9",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 1677895
    invoke-direct {p0, p1, p2}, LX/AUE;-><init>(Landroid/content/Context;Ljava/util/concurrent/ExecutorService;)V

    .line 1677896
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, LX/AUG;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 1677897
    iput-object p3, p0, LX/AUG;->a:LX/AUA;

    .line 1677898
    iput-object p4, p0, LX/AUG;->b:LX/AU9;

    .line 1677899
    return-void
.end method

.method private a(Landroid/database/Cursor;)V
    .locals 2
    .annotation build Landroid/support/annotation/MainThread;
    .end annotation

    .prologue
    .line 1677900
    iget-boolean v0, p0, LX/0k9;->r:Z

    move v0, v0

    .line 1677901
    if-eqz v0, :cond_1

    .line 1677902
    if-eqz p1, :cond_0

    .line 1677903
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 1677904
    :cond_0
    :goto_0
    return-void

    .line 1677905
    :cond_1
    iget-object v0, p0, LX/AUG;->c:Landroid/database/Cursor;

    .line 1677906
    iput-object p1, p0, LX/AUG;->c:Landroid/database/Cursor;

    .line 1677907
    iget-boolean v1, p0, LX/0k9;->p:Z

    move v1, v1

    .line 1677908
    if-eqz v1, :cond_2

    .line 1677909
    invoke-super {p0, p1}, LX/AUE;->b(Ljava/lang/Object;)V

    .line 1677910
    :cond_2
    if-eqz v0, :cond_0

    if-eq v0, p1, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1677911
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 1
    .annotation build Landroid/support/annotation/MainThread;
    .end annotation

    .prologue
    .line 1677886
    check-cast p1, Landroid/database/Cursor;

    .line 1677887
    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1677888
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    .line 1677889
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1677880
    invoke-super {p0, p1, p2, p3, p4}, LX/AUE;->a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 1677881
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "mQueryProvider="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/AUG;->b:LX/AU9;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1677882
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "mCursor="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/AUG;->c:Landroid/database/Cursor;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1677883
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "mLoadInvocationCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/AUG;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1677884
    return-void
.end method

.method public final b()Ljava/lang/Object;
    .locals 2
    .annotation build Landroid/support/annotation/WorkerThread;
    .end annotation

    .prologue
    .line 1677890
    iget-object v0, p0, LX/AUG;->e:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    .line 1677891
    iget-object v0, p0, LX/AUG;->a:LX/AUA;

    iget-object v1, p0, LX/AUG;->b:LX/AU9;

    invoke-virtual {v0, v1}, LX/AUA;->b(LX/AU9;)Landroid/database/Cursor;

    move-result-object v0

    .line 1677892
    if-eqz v0, :cond_0

    .line 1677893
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    .line 1677894
    :cond_0
    return-object v0
.end method

.method public final synthetic b(Ljava/lang/Object;)V
    .locals 0
    .annotation build Landroid/support/annotation/MainThread;
    .end annotation

    .prologue
    .line 1677885
    check-cast p1, Landroid/database/Cursor;

    invoke-direct {p0, p1}, LX/AUG;->a(Landroid/database/Cursor;)V

    return-void
.end method

.method public final g()V
    .locals 6
    .annotation build Landroid/support/annotation/MainThread;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1677869
    iget-object v1, p0, LX/AUG;->c:Landroid/database/Cursor;

    if-eqz v1, :cond_0

    .line 1677870
    iget-object v1, p0, LX/AUG;->c:Landroid/database/Cursor;

    invoke-direct {p0, v1}, LX/AUG;->a(Landroid/database/Cursor;)V

    .line 1677871
    :cond_0
    invoke-virtual {p0}, LX/0k9;->t()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, LX/AUG;->c:Landroid/database/Cursor;

    if-nez v1, :cond_2

    .line 1677872
    :cond_1
    invoke-virtual {p0}, LX/0k9;->a()V

    .line 1677873
    :cond_2
    iget-object v1, p0, LX/AUG;->d:LX/AUF;

    if-nez v1, :cond_3

    .line 1677874
    new-instance v1, LX/AUF;

    invoke-direct {v1, p0}, LX/AUF;-><init>(LX/AUG;)V

    iput-object v1, p0, LX/AUG;->d:LX/AUF;

    .line 1677875
    iget-object v1, p0, LX/AUG;->b:LX/AU9;

    invoke-interface {v1}, LX/AU9;->a()[Ljava/lang/Object;

    move-result-object v1

    array-length v2, v1

    :goto_0
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 1677876
    sget-object v4, LX/AUM;->a:LX/AUM;

    move-object v4, v4

    .line 1677877
    iget-object v5, p0, LX/AUG;->d:LX/AUF;

    invoke-virtual {v4, v3, v5}, LX/AUM;->a(Ljava/lang/Object;LX/AUF;)V

    .line 1677878
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1677879
    :cond_3
    return-void
.end method

.method public final h()V
    .locals 0
    .annotation build Landroid/support/annotation/MainThread;
    .end annotation

    .prologue
    .line 1677867
    invoke-virtual {p0}, LX/AUE;->c()Z

    .line 1677868
    return-void
.end method

.method public final i()V
    .locals 7
    .annotation build Landroid/support/annotation/MainThread;
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 1677855
    invoke-super {p0}, LX/AUE;->i()V

    .line 1677856
    invoke-virtual {p0}, LX/AUG;->h()V

    .line 1677857
    iget-object v0, p0, LX/AUG;->c:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/AUG;->c:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1677858
    iget-object v0, p0, LX/AUG;->c:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 1677859
    :cond_0
    iput-object v6, p0, LX/AUG;->c:Landroid/database/Cursor;

    .line 1677860
    iget-object v0, p0, LX/AUG;->d:LX/AUF;

    if-eqz v0, :cond_2

    .line 1677861
    iget-object v0, p0, LX/AUG;->b:LX/AU9;

    invoke-interface {v0}, LX/AU9;->a()[Ljava/lang/Object;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 1677862
    sget-object v4, LX/AUM;->a:LX/AUM;

    move-object v4, v4

    .line 1677863
    iget-object v5, p0, LX/AUG;->d:LX/AUF;

    invoke-virtual {v4, v3, v5}, LX/AUM;->b(Ljava/lang/Object;LX/AUF;)V

    .line 1677864
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1677865
    :cond_1
    iput-object v6, p0, LX/AUG;->d:LX/AUF;

    .line 1677866
    :cond_2
    return-void
.end method
