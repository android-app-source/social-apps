.class public LX/BVh;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile n:LX/BVh;


# instance fields
.field private final a:LX/BVU;

.field private final b:LX/BVt;

.field private final c:LX/BVY;

.field private final d:LX/BVa;

.field private final e:LX/BVl;

.field private final f:LX/BVk;

.field public final g:LX/BW5;

.field private final h:LX/BVV;

.field private final i:LX/BW0;

.field private final j:LX/BVb;

.field private final k:LX/BVc;

.field private final l:LX/BW3;

.field private final m:LX/BVo;


# direct methods
.method public constructor <init>(LX/BW4;LX/BW5;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1791330
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1791331
    iput-object p2, p0, LX/BVh;->g:LX/BW5;

    .line 1791332
    new-instance v0, LX/BVU;

    invoke-direct {v0, p1, p2}, LX/BVU;-><init>(LX/BW4;LX/BW5;)V

    iput-object v0, p0, LX/BVh;->a:LX/BVU;

    .line 1791333
    new-instance v0, LX/BVt;

    invoke-direct {v0, p1, p2}, LX/BVt;-><init>(LX/BW4;LX/BW5;)V

    iput-object v0, p0, LX/BVh;->b:LX/BVt;

    .line 1791334
    new-instance v0, LX/BVY;

    invoke-direct {v0, p1, p2}, LX/BVY;-><init>(LX/BW4;LX/BW5;)V

    iput-object v0, p0, LX/BVh;->c:LX/BVY;

    .line 1791335
    new-instance v0, LX/BVa;

    invoke-direct {v0, p1, p2}, LX/BVa;-><init>(LX/BW4;LX/BW5;)V

    iput-object v0, p0, LX/BVh;->d:LX/BVa;

    .line 1791336
    new-instance v0, LX/BVl;

    invoke-direct {v0, p1, p2}, LX/BVl;-><init>(LX/BW4;LX/BW5;)V

    iput-object v0, p0, LX/BVh;->e:LX/BVl;

    .line 1791337
    new-instance v0, LX/BVk;

    invoke-direct {v0, p1, p2}, LX/BVk;-><init>(LX/BW4;LX/BW5;)V

    iput-object v0, p0, LX/BVh;->f:LX/BVk;

    .line 1791338
    new-instance v0, LX/BVV;

    invoke-direct {v0, p1, p2}, LX/BVV;-><init>(LX/BW4;LX/BW5;)V

    iput-object v0, p0, LX/BVh;->h:LX/BVV;

    .line 1791339
    new-instance v0, LX/BW0;

    invoke-direct {v0, p1, p2}, LX/BW0;-><init>(LX/BW4;LX/BW5;)V

    iput-object v0, p0, LX/BVh;->i:LX/BW0;

    .line 1791340
    new-instance v0, LX/BVb;

    invoke-direct {v0, p1, p2}, LX/BVb;-><init>(LX/BW4;LX/BW5;)V

    iput-object v0, p0, LX/BVh;->j:LX/BVb;

    .line 1791341
    new-instance v0, LX/BVc;

    invoke-direct {v0, p1, p2}, LX/BVc;-><init>(LX/BW4;LX/BW5;)V

    iput-object v0, p0, LX/BVh;->k:LX/BVc;

    .line 1791342
    new-instance v0, LX/BW3;

    invoke-direct {v0, p1, p2}, LX/BW3;-><init>(LX/BW4;LX/BW5;)V

    iput-object v0, p0, LX/BVh;->l:LX/BW3;

    .line 1791343
    new-instance v0, LX/BVo;

    invoke-direct {v0, p1, p2}, LX/BVo;-><init>(LX/BW4;LX/BW5;)V

    iput-object v0, p0, LX/BVh;->m:LX/BVo;

    .line 1791344
    return-void
.end method

.method public static a(LX/0QB;)LX/BVh;
    .locals 5

    .prologue
    .line 1791360
    sget-object v0, LX/BVh;->n:LX/BVh;

    if-nez v0, :cond_1

    .line 1791361
    const-class v1, LX/BVh;

    monitor-enter v1

    .line 1791362
    :try_start_0
    sget-object v0, LX/BVh;->n:LX/BVh;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1791363
    if-eqz v2, :cond_0

    .line 1791364
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1791365
    new-instance p0, LX/BVh;

    .line 1791366
    new-instance v4, LX/BW4;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-direct {v4, v3}, LX/BW4;-><init>(Landroid/content/res/Resources;)V

    .line 1791367
    move-object v3, v4

    .line 1791368
    check-cast v3, LX/BW4;

    .line 1791369
    new-instance v4, LX/BW5;

    invoke-direct {v4}, LX/BW5;-><init>()V

    .line 1791370
    move-object v4, v4

    .line 1791371
    move-object v4, v4

    .line 1791372
    check-cast v4, LX/BW5;

    invoke-direct {p0, v3, v4}, LX/BVh;-><init>(LX/BW4;LX/BW5;)V

    .line 1791373
    move-object v0, p0

    .line 1791374
    sput-object v0, LX/BVh;->n:LX/BVh;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1791375
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1791376
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1791377
    :cond_1
    sget-object v0, LX/BVh;->n:LX/BVh;

    return-object v0

    .line 1791378
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1791379
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/BVT;
    .locals 3

    .prologue
    .line 1791345
    sget-object v0, LX/BVf;->a:[I

    invoke-static {p1}, LX/BVg;->from(Ljava/lang/String;)LX/BVg;

    move-result-object v1

    invoke-virtual {v1}, LX/BVg;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1791346
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unhandled view = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1791347
    :pswitch_0
    iget-object v0, p0, LX/BVh;->a:LX/BVU;

    .line 1791348
    :goto_0
    return-object v0

    .line 1791349
    :pswitch_1
    iget-object v0, p0, LX/BVh;->b:LX/BVt;

    goto :goto_0

    .line 1791350
    :pswitch_2
    iget-object v0, p0, LX/BVh;->c:LX/BVY;

    goto :goto_0

    .line 1791351
    :pswitch_3
    iget-object v0, p0, LX/BVh;->d:LX/BVa;

    goto :goto_0

    .line 1791352
    :pswitch_4
    iget-object v0, p0, LX/BVh;->e:LX/BVl;

    goto :goto_0

    .line 1791353
    :pswitch_5
    iget-object v0, p0, LX/BVh;->f:LX/BVk;

    goto :goto_0

    .line 1791354
    :pswitch_6
    iget-object v0, p0, LX/BVh;->h:LX/BVV;

    goto :goto_0

    .line 1791355
    :pswitch_7
    iget-object v0, p0, LX/BVh;->i:LX/BW0;

    goto :goto_0

    .line 1791356
    :pswitch_8
    iget-object v0, p0, LX/BVh;->j:LX/BVb;

    goto :goto_0

    .line 1791357
    :pswitch_9
    iget-object v0, p0, LX/BVh;->k:LX/BVc;

    goto :goto_0

    .line 1791358
    :pswitch_a
    iget-object v0, p0, LX/BVh;->l:LX/BW3;

    goto :goto_0

    .line 1791359
    :pswitch_b
    iget-object v0, p0, LX/BVh;->m:LX/BVo;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method
