.class public abstract LX/AU1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/AU0;


# instance fields
.field public final a:Landroid/database/Cursor;

.field private final b:I


# direct methods
.method public constructor <init>(Landroid/database/Cursor;)V
    .locals 2

    .prologue
    .line 1677777
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1677778
    if-nez p1, :cond_0

    .line 1677779
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "cursor is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1677780
    :cond_0
    iput-object p1, p0, LX/AU1;->a:Landroid/database/Cursor;

    .line 1677781
    const-string v0, "_id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/AU1;->b:I

    .line 1677782
    return-void
.end method


# virtual methods
.method public final a(LX/FZ4;)LX/AU0;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/AU0;",
            ">(",
            "Lcom/facebook/crudolib/dao/DAOItem$Creator",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 1677759
    new-instance v1, Landroid/database/MatrixCursor;

    iget-object v0, p0, LX/AU1;->a:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getColumnNames()[Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 1677760
    iget-object v0, p0, LX/AU1;->a:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getColumnCount()I

    move-result v2

    .line 1677761
    new-array v3, v2, [Ljava/lang/Object;

    .line 1677762
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    .line 1677763
    iget-object v4, p0, LX/AU1;->a:Landroid/database/Cursor;

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getType(I)I

    move-result v4

    .line 1677764
    packed-switch v4, :pswitch_data_0

    .line 1677765
    :pswitch_0
    iget-object v4, p0, LX/AU1;->a:Landroid/database/Cursor;

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    .line 1677766
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1677767
    :pswitch_1
    iget-object v4, p0, LX/AU1;->a:Landroid/database/Cursor;

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v0

    goto :goto_1

    .line 1677768
    :pswitch_2
    iget-object v4, p0, LX/AU1;->a:Landroid/database/Cursor;

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v4

    aput-object v4, v3, v0

    goto :goto_1

    .line 1677769
    :pswitch_3
    iget-object v4, p0, LX/AU1;->a:Landroid/database/Cursor;

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getFloat(I)F

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v3, v0

    goto :goto_1

    .line 1677770
    :pswitch_4
    const/4 v4, 0x0

    aput-object v4, v3, v0

    goto :goto_1

    .line 1677771
    :cond_0
    invoke-virtual {v1, v3}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 1677772
    const/4 v2, 0x0

    .line 1677773
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1677774
    new-instance v0, LX/BO3;

    invoke-direct {v0, v2, v2}, LX/BO3;-><init>(II)V

    .line 1677775
    invoke-static {v1}, LX/BO3;->b(Landroid/database/Cursor;)LX/BO1;

    move-result-object v0

    move-object v0, v0

    .line 1677776
    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public final a()Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 1677758
    iget-object v0, p0, LX/AU1;->a:Landroid/database/Cursor;

    return-object v0
.end method

.method public final a(I)Z
    .locals 1

    .prologue
    .line 1677753
    iget-object v0, p0, LX/AU1;->a:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    return v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 1677757
    iget-object v0, p0, LX/AU1;->a:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getPosition()I

    move-result v0

    return v0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1677755
    iget-object v0, p0, LX/AU1;->a:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 1677756
    return-void
.end method

.method public d()J
    .locals 2

    .prologue
    .line 1677754
    iget-object v0, p0, LX/AU1;->a:Landroid/database/Cursor;

    iget v1, p0, LX/AU1;->b:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method
