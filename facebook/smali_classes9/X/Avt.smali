.class public LX/Avt;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/0iJ;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1724690
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 1724691
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/ArL;LX/ArT;LX/0gc;Landroid/app/Activity;LX/0zw;)LX/0iJ;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ModelData:",
            "Ljava/lang/Object;",
            ":",
            "Lcom/facebook/friendsharing/inspiration/model/CameraStateSpec$ProvidesCameraState;",
            ":",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingDataSpec$ProvidesInspirationLoggingData;",
            ":",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationStateSpec$ProvidesInspirationState;",
            "DerivedData:",
            "Ljava/lang/Object;",
            "Mutation:",
            "Ljava/lang/Object;",
            ":",
            "Lcom/facebook/ipc/composer/dataaccessor/ComposerCanSave;",
            ":",
            "Lcom/facebook/friendsharing/inspiration/model/CameraStateSpec$SetsCameraState",
            "<TMutation;>;:",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingDataSpec$SetsInspirationLoggingData",
            "<TMutation;>;:",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationStateSpec$SetsInspirationState",
            "<TMutation;>;Services:",
            "Ljava/lang/Object;",
            ":",
            "LX/0il",
            "<TModelData;>;:",
            "LX/0im",
            "<TMutation;>;>(TServices;",
            "LX/ArL;",
            "LX/ArT;",
            "LX/0gc;",
            "Landroid/app/Activity;",
            "LX/0zw",
            "<",
            "Landroid/view/View;",
            ">;)",
            "LX/0iJ",
            "<TModelData;TDerivedData;TMutation;TServices;>;"
        }
    .end annotation

    .prologue
    .line 1724687
    new-instance v0, LX/0iJ;

    move-object v1, p1

    check-cast v1, LX/0il;

    const-class v2, LX/0i4;

    invoke-interface {p0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/0i4;

    invoke-static {p0}, LX/Avn;->b(LX/0QB;)LX/Avn;

    move-result-object v8

    check-cast v8, LX/Avn;

    invoke-static {p0}, LX/0iA;->a(LX/0QB;)LX/0iA;

    move-result-object v9

    check-cast v9, LX/0iA;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v10

    check-cast v10, Lcom/facebook/prefs/shared/FbSharedPreferences;

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    invoke-direct/range {v0 .. v10}, LX/0iJ;-><init>(LX/0il;LX/ArL;LX/ArT;LX/0gc;Landroid/app/Activity;LX/0zw;LX/0i4;LX/Avn;LX/0iA;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 1724688
    const/16 v1, 0xa8e

    invoke-static {p0, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    invoke-static {v0, v1}, LX/0iJ;->a(LX/0iJ;LX/0Or;)V

    .line 1724689
    return-object v0
.end method
