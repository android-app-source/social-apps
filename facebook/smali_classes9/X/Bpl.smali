.class public LX/Bpl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/9DK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/9DK",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:LX/0QK;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QK",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/3iK;

.field public final c:LX/20h;

.field public d:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0QK;LX/3iG;LX/20h;)V
    .locals 1
    .param p1    # LX/0QK;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QK",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            "Ljava/lang/Void;",
            ">;",
            "LX/3iG;",
            "LX/20h;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1823543
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1823544
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0QK;

    iput-object v0, p0, LX/Bpl;->a:LX/0QK;

    .line 1823545
    sget-object v0, Lcom/facebook/feedback/ui/FeedbackControllerParams;->a:Lcom/facebook/feedback/ui/FeedbackControllerParams;

    invoke-virtual {p2, v0}, LX/3iG;->a(Lcom/facebook/feedback/ui/FeedbackControllerParams;)LX/3iK;

    move-result-object v0

    iput-object v0, p0, LX/Bpl;->b:LX/3iK;

    .line 1823546
    iput-object p3, p0, LX/Bpl;->c:LX/20h;

    .line 1823547
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 1

    .prologue
    .line 1823548
    invoke-static {p1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    iput-object v0, p0, LX/Bpl;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1823549
    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1823550
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {p0, p1}, LX/Bpl;->a(Lcom/facebook/graphql/model/GraphQLStory;)V

    return-void
.end method
