.class public final LX/CSI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<TT;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/2PZ;


# direct methods
.method public constructor <init>(LX/2PZ;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1894459
    iput-object p1, p0, LX/CSI;->b:LX/2PZ;

    iput-object p2, p0, LX/CSI;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1894460
    iget-object v0, p0, LX/CSI;->b:LX/2PZ;

    iget-object v1, p0, LX/CSI;->a:Ljava/lang/String;

    invoke-static {v0, v1}, LX/2PZ;->b$redex0(LX/2PZ;Ljava/lang/String;)V

    .line 1894461
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1894462
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1894463
    if-nez p1, :cond_1

    .line 1894464
    :cond_0
    :goto_0
    return-void

    .line 1894465
    :cond_1
    iget-object v0, p0, LX/CSI;->b:LX/2PZ;

    iget-object v1, p0, LX/CSI;->a:Ljava/lang/String;

    invoke-static {v0, v1}, LX/2PZ;->c(LX/2PZ;Ljava/lang/String;)LX/CSG;

    move-result-object v2

    .line 1894466
    iget-object v0, p0, LX/CSI;->b:LX/2PZ;

    iget-object v0, v0, LX/2PZ;->j:LX/2Pc;

    iget-object v1, p0, LX/CSI;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/2Pc;->a(Ljava/lang/String;)V

    .line 1894467
    if-nez v2, :cond_2

    .line 1894468
    iget-object v0, p0, LX/CSI;->b:LX/2PZ;

    iget-object v0, v0, LX/2PZ;->h:LX/13Q;

    const-string v1, "spotty_ads_offline_mutation_success_no_key_record"

    invoke-virtual {v0, v1}, LX/13Q;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 1894469
    :cond_2
    iget-object v0, p0, LX/CSI;->b:LX/2PZ;

    iget-object v1, v2, LX/CSG;->d:[B

    sget-object v3, LX/8wW;->SUBMIT_FLOW:LX/8wW;

    invoke-static {v0, v1, v3}, LX/2PZ;->a$redex0(LX/2PZ;[BLX/8wW;)V

    .line 1894470
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1894471
    iget-object v0, p0, LX/CSI;->b:LX/2PZ;

    iget-object v1, v0, LX/2PZ;->d:Ljava/util/List;

    monitor-enter v1

    .line 1894472
    :try_start_0
    iget-object v0, p0, LX/CSI;->b:LX/2PZ;

    iget-object v0, v0, LX/2PZ;->d:Ljava/util/List;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 1894473
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1894474
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_0

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 1894475
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Bqu;

    .line 1894476
    if-eqz v0, :cond_3

    .line 1894477
    const/4 p0, 0x1

    invoke-static {v0, v2, p0}, LX/Bqu;->a(LX/Bqu;LX/CSG;Z)V

    .line 1894478
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1894479
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
