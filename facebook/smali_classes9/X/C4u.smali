.class public LX/C4u;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements LX/24a;


# instance fields
.field public final a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public final b:Lcom/facebook/fbui/glyph/GlyphView;

.field public final c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public d:Z

.field public e:Lcom/facebook/graphql/model/GraphQLImage;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1848011
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 1848012
    const v0, 0x7f0303ca

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1848013
    const v0, 0x7f0d0bde

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, LX/C4u;->b:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1848014
    const v0, 0x7f0d0bbe

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, LX/C4u;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1848015
    const v0, 0x7f0d0bdf

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, LX/C4u;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1848016
    return-void
.end method


# virtual methods
.method public final a(LX/0hs;)V
    .locals 1

    .prologue
    .line 1848017
    iget-object v0, p0, LX/C4u;->b:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {p1, v0}, LX/0ht;->f(Landroid/view/View;)V

    .line 1848018
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1848019
    iget-object v0, p0, LX/C4u;->b:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0}, Lcom/facebook/fbui/glyph/GlyphView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 1848020
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onMeasure(II)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1848021
    iget-object v0, p0, LX/C4u;->e:Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz v0, :cond_0

    .line 1848022
    iget-object v0, p0, LX/C4u;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 1848023
    iget-boolean v1, p0, LX/C4u;->d:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/C4u;->e:Lcom/facebook/graphql/model/GraphQLImage;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v1

    if-lez v1, :cond_1

    .line 1848024
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 1848025
    const/4 v2, -0x1

    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1848026
    iget-object v2, p0, LX/C4u;->e:Lcom/facebook/graphql/model/GraphQLImage;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v2

    mul-int/2addr v1, v2

    iget-object v2, p0, LX/C4u;->e:Lcom/facebook/graphql/model/GraphQLImage;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v2

    div-int/2addr v1, v2

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1848027
    invoke-virtual {p0}, LX/C4u;->getPaddingTop()I

    move-result v0

    invoke-virtual {p0}, LX/C4u;->getPaddingBottom()I

    move-result v1

    invoke-virtual {p0, v3, v0, v3, v1}, LX/C4u;->setPadding(IIII)V

    .line 1848028
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;->onMeasure(II)V

    .line 1848029
    return-void

    .line 1848030
    :cond_1
    iget-object v1, p0, LX/C4u;->e:Lcom/facebook/graphql/model/GraphQLImage;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1848031
    iget-object v1, p0, LX/C4u;->e:Lcom/facebook/graphql/model/GraphQLImage;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto :goto_0
.end method

.method public setMenuButtonActive(Z)V
    .locals 2

    .prologue
    .line 1848032
    iget-object v1, p0, LX/C4u;->b:Lcom/facebook/fbui/glyph/GlyphView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1848033
    return-void

    .line 1848034
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method
