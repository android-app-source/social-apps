.class public LX/BFS;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1765873
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(ZLandroid/view/View;)LX/0g8;
    .locals 2

    .prologue
    .line 1765874
    if-eqz p0, :cond_0

    .line 1765875
    const v0, 0x7f0d24c4

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 1765876
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 1765877
    new-instance v1, LX/1Oz;

    invoke-direct {v1, v0}, LX/1Oz;-><init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 1765878
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setWillNotDraw(Z)V

    .line 1765879
    new-instance v1, LX/0g7;

    invoke-direct {v1, v0}, LX/0g7;-><init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)V

    move-object v0, v1

    .line 1765880
    :goto_0
    return-object v0

    .line 1765881
    :cond_0
    const v0, 0x7f0d24c5

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 1765882
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    .line 1765883
    new-instance v1, LX/2iI;

    invoke-direct {v1, v0}, LX/2iI;-><init>(Lcom/facebook/widget/listview/BetterListView;)V

    move-object v0, v1

    goto :goto_0
.end method

.method public static a(LX/0ad;LX/0tF;Landroid/os/Bundle;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1765884
    if-eqz p2, :cond_0

    const-string v1, "use_recycler_view"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    :cond_0
    sget-short v1, LX/0fe;->aM:S

    invoke-interface {p0, v1, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1765885
    iget-object v1, p1, LX/0tF;->a:LX/0ad;

    sget-short p0, LX/0wn;->aj:S

    const/4 p2, 0x0

    invoke-interface {v1, p0, p2}, LX/0ad;->a(SZ)Z

    move-result v1

    move v1, v1

    .line 1765886
    if-eqz v1, :cond_2

    :cond_1
    const/4 v0, 0x1

    :cond_2
    :goto_0
    return v0

    :cond_3
    const-string v0, "use_recycler_view"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method
