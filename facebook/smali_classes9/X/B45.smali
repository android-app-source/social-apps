.class public final LX/B45;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:LX/B4A;


# direct methods
.method public constructor <init>(LX/B4A;)V
    .locals 0

    .prologue
    .line 1741273
    iput-object p1, p0, LX/B45;->a:LX/B4A;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 1741274
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1741275
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    .prologue
    .line 1741276
    iget-object v0, p0, LX/B45;->a:LX/B4A;

    iget-object v0, v0, LX/B4A;->p:LX/B40;

    if-eqz v0, :cond_1

    .line 1741277
    iget-object v0, p0, LX/B45;->a:LX/B4A;

    iget-object v0, v0, LX/B4A;->o:Landroid/widget/Filter$FilterListener;

    if-nez v0, :cond_0

    .line 1741278
    iget-object v0, p0, LX/B45;->a:LX/B4A;

    new-instance v1, LX/B44;

    invoke-direct {v1, p0}, LX/B44;-><init>(LX/B45;)V

    .line 1741279
    iput-object v1, v0, LX/B4A;->o:Landroid/widget/Filter$FilterListener;

    .line 1741280
    :cond_0
    iget-object v0, p0, LX/B45;->a:LX/B4A;

    iget-object v0, v0, LX/B4A;->p:LX/B40;

    invoke-virtual {v0}, LX/B40;->getFilter()Landroid/widget/Filter;

    move-result-object v0

    iget-object v1, p0, LX/B45;->a:LX/B4A;

    iget-object v1, v1, LX/B4A;->o:Landroid/widget/Filter$FilterListener;

    invoke-virtual {v0, p1, v1}, Landroid/widget/Filter;->filter(Ljava/lang/CharSequence;Landroid/widget/Filter$FilterListener;)V

    .line 1741281
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1741282
    iget-object v0, p0, LX/B45;->a:LX/B4A;

    iget-object v0, v0, LX/B4A;->f:Landroid/widget/ImageButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1741283
    :cond_1
    :goto_0
    return-void

    .line 1741284
    :cond_2
    iget-object v0, p0, LX/B45;->a:LX/B4A;

    iget-object v0, v0, LX/B4A;->f:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0
.end method
