.class public final LX/Ab1;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 23

    .prologue
    .line 1689998
    const-wide/16 v16, 0x0

    .line 1689999
    const-wide/16 v14, 0x0

    .line 1690000
    const-wide/16 v12, 0x0

    .line 1690001
    const/4 v11, 0x0

    .line 1690002
    const/4 v10, 0x0

    .line 1690003
    const-wide/16 v8, 0x0

    .line 1690004
    const/4 v7, 0x0

    .line 1690005
    const/4 v6, 0x0

    .line 1690006
    const/4 v5, 0x0

    .line 1690007
    const/4 v4, 0x0

    .line 1690008
    const/4 v3, 0x0

    .line 1690009
    const/4 v2, 0x0

    .line 1690010
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v18

    sget-object v19, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-eq v0, v1, :cond_e

    .line 1690011
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1690012
    const/4 v2, 0x0

    .line 1690013
    :goto_0
    return v2

    .line 1690014
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v6, :cond_8

    .line 1690015
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 1690016
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1690017
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_0

    if-eqz v2, :cond_0

    .line 1690018
    const-string v6, "brightness"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1690019
    const/4 v2, 0x1

    .line 1690020
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v4

    move v3, v2

    goto :goto_1

    .line 1690021
    :cond_1
    const-string v6, "contrast"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1690022
    const/4 v2, 0x1

    .line 1690023
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v11, v2

    move-wide/from16 v20, v6

    goto :goto_1

    .line 1690024
    :cond_2
    const-string v6, "hue"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1690025
    const/4 v2, 0x1

    .line 1690026
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v10, v2

    move-wide/from16 v18, v6

    goto :goto_1

    .line 1690027
    :cond_3
    const-string v6, "hue_colorize"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1690028
    const/4 v2, 0x1

    .line 1690029
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move v9, v2

    move/from16 v16, v6

    goto :goto_1

    .line 1690030
    :cond_4
    const-string v6, "id"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 1690031
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v13, v2

    goto :goto_1

    .line 1690032
    :cond_5
    const-string v6, "saturation"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 1690033
    const/4 v2, 0x1

    .line 1690034
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v8, v2

    move-wide v14, v6

    goto :goto_1

    .line 1690035
    :cond_6
    const-string v6, "title"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 1690036
    invoke-static/range {p0 .. p1}, LX/Ab0;->a(LX/15w;LX/186;)I

    move-result v2

    move v12, v2

    goto/16 :goto_1

    .line 1690037
    :cond_7
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 1690038
    :cond_8
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 1690039
    if-eqz v3, :cond_9

    .line 1690040
    const/4 v3, 0x0

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1690041
    :cond_9
    if-eqz v11, :cond_a

    .line 1690042
    const/4 v3, 0x1

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v20

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1690043
    :cond_a
    if-eqz v10, :cond_b

    .line 1690044
    const/4 v3, 0x2

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v18

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1690045
    :cond_b
    if-eqz v9, :cond_c

    .line 1690046
    const/4 v2, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 1690047
    :cond_c
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 1690048
    if-eqz v8, :cond_d

    .line 1690049
    const/4 v3, 0x5

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide v4, v14

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1690050
    :cond_d
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 1690051
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_e
    move-wide/from16 v18, v12

    move-wide/from16 v20, v14

    move v12, v7

    move-wide v14, v8

    move v13, v10

    move v10, v4

    move v8, v2

    move v9, v3

    move v3, v6

    move/from16 v22, v5

    move-wide/from16 v4, v16

    move/from16 v16, v11

    move/from16 v11, v22

    goto/16 :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 1690052
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1690053
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 1690054
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_0

    .line 1690055
    const-string v2, "brightness"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1690056
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 1690057
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 1690058
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_1

    .line 1690059
    const-string v2, "contrast"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1690060
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 1690061
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 1690062
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_2

    .line 1690063
    const-string v2, "hue"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1690064
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 1690065
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1690066
    if-eqz v0, :cond_3

    .line 1690067
    const-string v1, "hue_colorize"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1690068
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1690069
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1690070
    if-eqz v0, :cond_4

    .line 1690071
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1690072
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1690073
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 1690074
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_5

    .line 1690075
    const-string v2, "saturation"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1690076
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 1690077
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1690078
    if-eqz v0, :cond_7

    .line 1690079
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1690080
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1690081
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1690082
    if-eqz v1, :cond_6

    .line 1690083
    const-string v2, "text"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1690084
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1690085
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1690086
    :cond_7
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1690087
    return-void
.end method
