.class public LX/BWm;
.super LX/8RF;
.source ""

# interfaces
.implements LX/BWl;


# instance fields
.field public a:Landroid/view/LayoutInflater;

.field private b:Z


# direct methods
.method public constructor <init>(ZZ)V
    .locals 0

    .prologue
    .line 1792361
    invoke-direct {p0, p1}, LX/8RF;-><init>(Z)V

    .line 1792362
    iput-boolean p2, p0, LX/BWm;->b:Z

    .line 1792363
    return-void
.end method

.method public static a(Landroid/view/View;Z)V
    .locals 1

    .prologue
    .line 1792352
    instance-of v0, p0, Lcom/facebook/widget/friendselector/CaspianFriendSelectorItemRow;

    if-eqz v0, :cond_0

    .line 1792353
    check-cast p0, Lcom/facebook/widget/friendselector/CaspianFriendSelectorItemRow;

    invoke-virtual {p0, p1}, Lcom/facebook/widget/friendselector/CaspianFriendSelectorItemRow;->setAsHeaderItem(Z)V

    .line 1792354
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 1792355
    new-instance v0, Lcom/facebook/widget/friendselector/CaspianFriendSelectorItemRow;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/widget/friendselector/CaspianFriendSelectorItemRow;-><init>(Landroid/content/Context;)V

    .line 1792356
    iget-boolean v1, p0, LX/BWm;->b:Z

    if-nez v1, :cond_0

    .line 1792357
    invoke-virtual {v0}, Lcom/facebook/widget/friendselector/CaspianFriendSelectorItemRow;->a()V

    .line 1792358
    :cond_0
    return-object v0
.end method

.method public final a(Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)Landroid/view/View;
    .locals 2

    .prologue
    .line 1792359
    iput-object p2, p0, LX/BWm;->a:Landroid/view/LayoutInflater;

    .line 1792360
    const v0, 0x7f0306fb

    const/4 v1, 0x0

    invoke-virtual {p2, v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/view/View;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 1792366
    const v0, 0x7f0d12af

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method public final a(Landroid/view/View;LX/621;)V
    .locals 1

    .prologue
    .line 1792364
    check-cast p1, LX/BWk;

    invoke-interface {p2}, LX/621;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/BWk;->a(Ljava/lang/String;)V

    .line 1792365
    return-void
.end method

.method public final a(Landroid/view/View;LX/8QL;Z)V
    .locals 0

    .prologue
    .line 1792349
    check-cast p1, Lcom/facebook/widget/friendselector/CaspianFriendSelectorItemRow;

    invoke-virtual {p1, p2, p3}, Lcom/facebook/widget/friendselector/CaspianFriendSelectorItemRow;->a(LX/8QL;Z)V

    .line 1792350
    return-void
.end method

.method public final b(Landroid/view/View;)Landroid/view/View;
    .locals 1

    .prologue
    .line 1792351
    const v0, 0x7f0d12ad

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final b(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 1792341
    new-instance v0, Lcom/facebook/widget/friendselector/CaspianFriendSelectorItemRow;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/widget/friendselector/CaspianFriendSelectorItemRow;-><init>(Landroid/content/Context;)V

    .line 1792342
    iget-boolean v1, p0, LX/BWm;->b:Z

    if-nez v1, :cond_0

    .line 1792343
    invoke-virtual {v0}, Lcom/facebook/widget/friendselector/CaspianFriendSelectorItemRow;->a()V

    .line 1792344
    :cond_0
    return-object v0
.end method

.method public final b(Landroid/view/View;LX/8QL;Z)V
    .locals 0

    .prologue
    .line 1792339
    check-cast p1, Lcom/facebook/widget/friendselector/CaspianFriendSelectorItemRow;

    invoke-virtual {p1, p2, p3}, Lcom/facebook/widget/friendselector/CaspianFriendSelectorItemRow;->a(LX/8QL;Z)V

    .line 1792340
    return-void
.end method

.method public final c(Landroid/view/View;)Lcom/facebook/widget/listview/BetterListView;
    .locals 1

    .prologue
    .line 1792338
    const v0, 0x7f0d12ae

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    return-object v0
.end method

.method public final d(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 1792348
    new-instance v0, LX/BWk;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/BWk;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public final d(Landroid/view/View;)Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;
    .locals 2

    .prologue
    .line 1792335
    const v0, 0x7f0d12ac

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    .line 1792336
    sget-object v1, LX/8us;->WHILE_EDITING:LX/8us;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->setClearButtonMode(LX/8us;)V

    .line 1792337
    return-object v0
.end method

.method public final e(Landroid/view/View;)Landroid/view/View;
    .locals 1

    .prologue
    .line 1792345
    const v0, 0x7f0d12aa

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final f(Landroid/view/View;)Landroid/view/View;
    .locals 1

    .prologue
    .line 1792346
    const v0, 0x7f0d12ab

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final g(Landroid/view/View;)Landroid/view/ViewStub;
    .locals 1

    .prologue
    .line 1792347
    const v0, 0x7f0d12b0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    return-object v0
.end method
