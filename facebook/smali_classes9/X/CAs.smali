.class public final LX/CAs;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "LX/CB0;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/feedplugins/prompts/PromptFriendSelectorFragment;

.field public final synthetic b:LX/CAt;


# direct methods
.method public constructor <init>(LX/CAt;Lcom/facebook/feedplugins/prompts/PromptFriendSelectorFragment;)V
    .locals 0

    .prologue
    .line 1855789
    iput-object p1, p0, LX/CAs;->b:LX/CAt;

    iput-object p2, p0, LX/CAs;->a:Lcom/facebook/feedplugins/prompts/PromptFriendSelectorFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 1855790
    iget-object v0, p0, LX/CAs;->b:LX/CAt;

    iget-object v0, v0, LX/CAt;->g:LX/0kL;

    new-instance v1, LX/27k;

    iget-object v2, p0, LX/CAs;->a:Lcom/facebook/feedplugins/prompts/PromptFriendSelectorFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08003c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 1855791
    iget-object v0, p0, LX/CAs;->b:LX/CAt;

    iget-object v0, v0, LX/CAt;->h:LX/03V;

    sget-object v1, LX/CAt;->a:Ljava/lang/String;

    const-string v2, "Social Prompt invite friends GraphQL mutation failed"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1855792
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 1855793
    iget-object v0, p0, LX/CAs;->b:LX/CAt;

    iget-object v1, p0, LX/CAs;->a:Lcom/facebook/feedplugins/prompts/PromptFriendSelectorFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 1855794
    iget-object v2, v0, LX/CAt;->g:LX/0kL;

    new-instance v3, LX/27k;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const p1, 0x7f082a3c

    invoke-virtual {v4, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v2, v3}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 1855795
    iget-object v0, p0, LX/CAs;->a:Lcom/facebook/feedplugins/prompts/PromptFriendSelectorFragment;

    invoke-virtual {v0}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->z()V

    .line 1855796
    return-void
.end method
