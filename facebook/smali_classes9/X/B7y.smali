.class public final LX/B7y;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/RadioGroup$OnCheckedChangeListener;


# instance fields
.field public final synthetic a:LX/B7z;


# direct methods
.method public constructor <init>(LX/B7z;)V
    .locals 0

    .prologue
    .line 1748299
    iput-object p1, p0, LX/B7y;->a:LX/B7z;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCheckedChanged(Landroid/widget/RadioGroup;I)V
    .locals 4

    .prologue
    .line 1748300
    iget-object v0, p0, LX/B7y;->a:LX/B7z;

    invoke-virtual {v0}, LX/B7z;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 1748301
    iget-object v1, p0, LX/B7y;->a:LX/B7z;

    invoke-virtual {v1}, LX/B7z;->getRootView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1748302
    iget-object v0, p0, LX/B7y;->a:LX/B7z;

    iget-object v0, v0, LX/B7z;->d:LX/B7W;

    new-instance v1, LX/B7Z;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, LX/B7Z;-><init>(ZLandroid/view/View;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1748303
    iget-object v0, p0, LX/B7y;->a:LX/B7z;

    iget-object v0, v0, LX/B7z;->i:LX/B7w;

    if-eqz v0, :cond_0

    .line 1748304
    iget-object v0, p0, LX/B7y;->a:LX/B7z;

    iget-object v0, v0, LX/B7z;->e:Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->j()Ljava/lang/String;

    iget-object v0, p0, LX/B7y;->a:LX/B7z;

    invoke-virtual {v0}, LX/B7z;->getInputValue()Ljava/lang/String;

    .line 1748305
    :cond_0
    iget-object v0, p0, LX/B7y;->a:LX/B7z;

    .line 1748306
    iget-object v1, v0, LX/B7z;->h:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1748307
    return-void
.end method
