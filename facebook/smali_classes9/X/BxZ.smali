.class public final LX/BxZ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/view/View;

.field public final b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public final c:Landroid/widget/TextView;

.field public final d:Landroid/widget/TextView;

.field public final synthetic e:LX/Bxa;

.field public final f:Landroid/view/ViewStub;

.field public final g:Landroid/view/View;

.field public h:Landroid/view/View;


# direct methods
.method public constructor <init>(LX/Bxa;)V
    .locals 2

    .prologue
    .line 1835664
    iput-object p1, p0, LX/BxZ;->e:LX/Bxa;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1835665
    const v0, 0x7f0d05bc

    .line 1835666
    invoke-virtual {p1, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v1

    move-object v0, v1

    .line 1835667
    iput-object v0, p0, LX/BxZ;->a:Landroid/view/View;

    .line 1835668
    const v0, 0x7f0d05bd

    .line 1835669
    invoke-virtual {p1, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v1

    move-object v0, v1

    .line 1835670
    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, LX/BxZ;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1835671
    const v0, 0x7f0d05bf

    .line 1835672
    invoke-virtual {p1, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v1

    move-object v0, v1

    .line 1835673
    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/BxZ;->c:Landroid/widget/TextView;

    .line 1835674
    const v0, 0x7f0d05c0

    .line 1835675
    invoke-virtual {p1, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v1

    move-object v0, v1

    .line 1835676
    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/BxZ;->d:Landroid/widget/TextView;

    .line 1835677
    const v0, 0x7f0d05c1

    .line 1835678
    invoke-virtual {p1, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v1

    move-object v0, v1

    .line 1835679
    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, LX/BxZ;->f:Landroid/view/ViewStub;

    .line 1835680
    const v0, 0x7f0d05be

    .line 1835681
    invoke-virtual {p1, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v1

    move-object v0, v1

    .line 1835682
    iput-object v0, p0, LX/BxZ;->g:Landroid/view/View;

    .line 1835683
    iget-object v0, p0, LX/BxZ;->c:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 1835684
    iget-object v0, p0, LX/BxZ;->d:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 1835685
    iget-object v0, p0, LX/BxZ;->c:Landroid/widget/TextView;

    sget-object v1, LX/1vY;->TITLE:LX/1vY;

    invoke-static {v0, v1}, LX/1vZ;->a(Landroid/view/View;LX/1vY;)V

    .line 1835686
    iget-object v0, p0, LX/BxZ;->d:Landroid/widget/TextView;

    sget-object v1, LX/1vY;->SUBTITLE:LX/1vY;

    invoke-static {v0, v1}, LX/1vZ;->a(Landroid/view/View;LX/1vY;)V

    .line 1835687
    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/drawable/Drawable;I)V
    .locals 2

    .prologue
    .line 1835688
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/BxZ;->a(Z)V

    .line 1835689
    iget-object v0, p0, LX/BxZ;->e:LX/Bxa;

    const v1, 0x7f0d05b7

    .line 1835690
    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object p0

    move-object v0, p0

    .line 1835691
    check-cast v0, Lcom/facebook/fbui/widget/text/ImageWithTextView;

    .line 1835692
    invoke-virtual {v0, p2}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setText(I)V

    .line 1835693
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setSoundEffectsEnabled(Z)V

    .line 1835694
    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1835695
    return-void
.end method

.method public final a(Z)V
    .locals 5

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 1835696
    iget-object v0, p0, LX/BxZ;->e:LX/Bxa;

    const v3, 0x7f0d05b8

    .line 1835697
    invoke-virtual {v0, v3}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v4

    move-object v3, v4

    .line 1835698
    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1835699
    iget-object v0, p0, LX/BxZ;->e:LX/Bxa;

    const v3, 0x7f0d05b7

    .line 1835700
    invoke-virtual {v0, v3}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v4

    move-object v0, v4

    .line 1835701
    if-eqz p1, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1835702
    return-void

    :cond_0
    move v0, v2

    .line 1835703
    goto :goto_0

    :cond_1
    move v1, v2

    .line 1835704
    goto :goto_1
.end method

.method public final b(Landroid/view/View$OnClickListener;)V
    .locals 2

    .prologue
    .line 1835705
    iget-object v0, p0, LX/BxZ;->h:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1835706
    iget-object v0, p0, LX/BxZ;->e:LX/Bxa;

    const v1, 0x7f0d05b7

    .line 1835707
    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object p0

    move-object v0, p0

    .line 1835708
    check-cast v0, Lcom/facebook/fbui/widget/text/ImageWithTextView;

    .line 1835709
    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1835710
    :cond_0
    return-void
.end method
