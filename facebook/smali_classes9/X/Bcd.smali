.class public final LX/Bcd;
.super LX/BcO;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/BcO",
        "<",
        "LX/Bce;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public b:LX/1X1;

.field public c:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1802402
    invoke-static {}, LX/Bce;->d()LX/Bce;

    move-result-object v0

    invoke-direct {p0, v0}, LX/BcO;-><init>(LX/BcS;)V

    .line 1802403
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1802404
    if-ne p0, p1, :cond_1

    .line 1802405
    :cond_0
    :goto_0
    return v0

    .line 1802406
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1802407
    goto :goto_0

    .line 1802408
    :cond_3
    check-cast p1, LX/Bcd;

    .line 1802409
    iget-object v2, p0, LX/Bcd;->b:LX/1X1;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/Bcd;->b:LX/1X1;

    iget-object v3, p1, LX/Bcd;->b:LX/1X1;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1802410
    goto :goto_0

    .line 1802411
    :cond_5
    iget-object v2, p1, LX/Bcd;->b:LX/1X1;

    if-nez v2, :cond_4

    .line 1802412
    :cond_6
    iget-object v2, p0, LX/Bcd;->c:Ljava/lang/Boolean;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/Bcd;->c:Ljava/lang/Boolean;

    iget-object v3, p1, LX/Bcd;->c:Ljava/lang/Boolean;

    invoke-virtual {v2, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1802413
    goto :goto_0

    .line 1802414
    :cond_7
    iget-object v2, p1, LX/Bcd;->c:Ljava/lang/Boolean;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
