.class public LX/Bib;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile j:LX/Bib;


# instance fields
.field public final a:LX/8OJ;

.field public final b:LX/Bl6;

.field private final c:Ljava/util/concurrent/ExecutorService;

.field public final d:LX/03V;

.field public final e:LX/0tX;

.field public final f:LX/8Ne;

.field private final g:LX/74n;

.field public final h:LX/73w;

.field public final i:LX/1Ck;


# direct methods
.method public constructor <init>(LX/8OJ;LX/Bl6;Ljava/util/concurrent/ExecutorService;LX/03V;LX/0tX;LX/8Ne;LX/74n;LX/73w;LX/1Ck;)V
    .locals 0
    .param p3    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1810569
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1810570
    iput-object p1, p0, LX/Bib;->a:LX/8OJ;

    .line 1810571
    iput-object p4, p0, LX/Bib;->d:LX/03V;

    .line 1810572
    iput-object p2, p0, LX/Bib;->b:LX/Bl6;

    .line 1810573
    iput-object p3, p0, LX/Bib;->c:Ljava/util/concurrent/ExecutorService;

    .line 1810574
    iput-object p5, p0, LX/Bib;->e:LX/0tX;

    .line 1810575
    iput-object p6, p0, LX/Bib;->f:LX/8Ne;

    .line 1810576
    iput-object p7, p0, LX/Bib;->g:LX/74n;

    .line 1810577
    iput-object p8, p0, LX/Bib;->h:LX/73w;

    .line 1810578
    iput-object p9, p0, LX/Bib;->i:LX/1Ck;

    .line 1810579
    return-void
.end method

.method public static a(LX/0QB;)LX/Bib;
    .locals 13

    .prologue
    .line 1810580
    sget-object v0, LX/Bib;->j:LX/Bib;

    if-nez v0, :cond_1

    .line 1810581
    const-class v1, LX/Bib;

    monitor-enter v1

    .line 1810582
    :try_start_0
    sget-object v0, LX/Bib;->j:LX/Bib;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1810583
    if-eqz v2, :cond_0

    .line 1810584
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1810585
    new-instance v3, LX/Bib;

    invoke-static {v0}, LX/8OJ;->b(LX/0QB;)LX/8OJ;

    move-result-object v4

    check-cast v4, LX/8OJ;

    invoke-static {v0}, LX/Bl6;->a(LX/0QB;)LX/Bl6;

    move-result-object v5

    check-cast v5, LX/Bl6;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v7

    check-cast v7, LX/03V;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v8

    check-cast v8, LX/0tX;

    invoke-static {v0}, LX/8LE;->b(LX/0QB;)LX/8Ne;

    move-result-object v9

    check-cast v9, LX/8Ne;

    invoke-static {v0}, LX/74n;->b(LX/0QB;)LX/74n;

    move-result-object v10

    check-cast v10, LX/74n;

    invoke-static {v0}, LX/73w;->b(LX/0QB;)LX/73w;

    move-result-object v11

    check-cast v11, LX/73w;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v12

    check-cast v12, LX/1Ck;

    invoke-direct/range {v3 .. v12}, LX/Bib;-><init>(LX/8OJ;LX/Bl6;Ljava/util/concurrent/ExecutorService;LX/03V;LX/0tX;LX/8Ne;LX/74n;LX/73w;LX/1Ck;)V

    .line 1810586
    move-object v0, v3

    .line 1810587
    sput-object v0, LX/Bib;->j:LX/Bib;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1810588
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1810589
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1810590
    :cond_1
    sget-object v0, LX/Bib;->j:LX/Bib;

    return-object v0

    .line 1810591
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1810592
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/os/Handler;Landroid/net/Uri;JLcom/facebook/events/common/EventAnalyticsParams;Lcom/facebook/events/common/ActionMechanism;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Handler;",
            "Landroid/net/Uri;",
            "J",
            "Lcom/facebook/events/common/EventAnalyticsParams;",
            "Lcom/facebook/events/common/ActionMechanism;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1810593
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v8

    .line 1810594
    :try_start_0
    iget-object v0, p0, LX/Bib;->g:LX/74n;

    sget-object v1, LX/74j;->DEFAULT:LX/74j;

    invoke-virtual {v0, p2, v1}, LX/74n;->a(Landroid/net/Uri;LX/74j;)Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    .line 1810595
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1810596
    iget-object v2, p0, LX/Bib;->h:LX/73w;

    invoke-virtual {v2, v1}, LX/73w;->a(Ljava/lang/String;)V

    .line 1810597
    new-instance v1, LX/8NJ;

    new-instance v2, Lcom/facebook/photos/upload/protocol/UploadPhotoSource;

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->o()J

    move-result-wide v4

    invoke-direct {v2, v3, v4, v5}, Lcom/facebook/photos/upload/protocol/UploadPhotoSource;-><init>(Ljava/lang/String;J)V

    invoke-direct {v1, v2}, LX/8NJ;-><init>(Lcom/facebook/photos/upload/protocol/UploadPhotoSource;)V

    .line 1810598
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/facebook/photos/upload/protocol/UploadPhotoParams;

    const/4 v2, 0x0

    invoke-virtual {v1}, LX/8NJ;->a()Lcom/facebook/photos/upload/protocol/UploadPhotoParams;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-static {v0}, LX/0RA;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v0

    .line 1810599
    new-instance v1, LX/BiZ;

    move-object v2, p0

    move-object v3, p1

    move-wide v4, p3

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    invoke-direct/range {v1 .. v7}, LX/BiZ;-><init>(LX/Bib;Landroid/os/Handler;JLcom/facebook/events/common/EventAnalyticsParams;Lcom/facebook/events/common/ActionMechanism;)V

    .line 1810600
    new-instance v6, LX/8OL;

    invoke-direct {v6}, LX/8OL;-><init>()V

    .line 1810601
    iget-object v9, p0, LX/Bib;->c:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lcom/facebook/events/create/EventCoverPhotoUploadHandler$2;

    move-object v3, p0

    move-object v4, v0

    move-object v5, v1

    move-object v7, v8

    invoke-direct/range {v2 .. v7}, Lcom/facebook/events/create/EventCoverPhotoUploadHandler$2;-><init>(LX/Bib;Ljava/util/HashSet;LX/8O7;LX/8OL;Lcom/google/common/util/concurrent/SettableFuture;)V

    const v0, 0x2f3b26dc

    invoke-static {v9, v2, v0}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 1810602
    :goto_0
    return-object v8

    .line 1810603
    :catch_0
    move-exception v0

    .line 1810604
    iget-object v1, p0, LX/Bib;->d:LX/03V;

    const-class v2, LX/Bib;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Failed to upload event cover photo"

    invoke-virtual {v1, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1810605
    invoke-virtual {v8, v0}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    goto :goto_0
.end method
