.class public final LX/BsX;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic b:LX/BsZ;


# direct methods
.method public constructor <init>(LX/BsZ;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 0

    .prologue
    .line 1827698
    iput-object p1, p0, LX/BsX;->b:LX/BsZ;

    iput-object p2, p0, LX/BsX;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 1827699
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 14

    .prologue
    .line 1827700
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1827701
    if-eqz p1, :cond_0

    .line 1827702
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1827703
    if-nez v0, :cond_1

    .line 1827704
    :cond_0
    :goto_0
    return-void

    .line 1827705
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1827706
    check-cast v0, Lcom/facebook/feed/protocol/AssociatePostToFundraiserForStoryModels$AssociatePostToFundraiserForStoryFieldsModel;

    .line 1827707
    invoke-virtual {v0}, Lcom/facebook/feed/protocol/AssociatePostToFundraiserForStoryModels$AssociatePostToFundraiserForStoryFieldsModel;->a()Lcom/facebook/feed/protocol/AssociatePostToFundraiserForStoryModels$AssociatePostToFundraiserForStoryFieldsModel$StoryModel;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1827708
    invoke-virtual {v0}, Lcom/facebook/feed/protocol/AssociatePostToFundraiserForStoryModels$AssociatePostToFundraiserForStoryFieldsModel;->a()Lcom/facebook/feed/protocol/AssociatePostToFundraiserForStoryModels$AssociatePostToFundraiserForStoryFieldsModel$StoryModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/feed/protocol/AssociatePostToFundraiserForStoryModels$AssociatePostToFundraiserForStoryFieldsModel$StoryModel;->j()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    .line 1827709
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/protocol/AssociatePostToFundraiserForStoryModels$AssociatePostToFundraiserForStoryAttachmentFieldsModel;

    .line 1827710
    if-nez v0, :cond_3

    .line 1827711
    const/4 v4, 0x0

    .line 1827712
    :goto_2
    move-object v0, v4

    .line 1827713
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object v4

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->FUNDRAISER_FOR_STORY:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-virtual {v4, v5}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1827714
    iget-object v1, p0, LX/BsX;->b:LX/BsZ;

    iget-object v1, v1, LX/BsZ;->b:LX/189;

    iget-object v2, p0, LX/BsX;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1827715
    iget-object v6, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v6, v6

    .line 1827716
    check-cast v6, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1827717
    invoke-static {v6}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object v7

    iget-object v8, v1, LX/189;->i:LX/0SG;

    invoke-interface {v8}, LX/0SG;->a()J

    move-result-wide v8

    .line 1827718
    iput-wide v8, v7, LX/23u;->G:J

    .line 1827719
    move-object v7, v7

    .line 1827720
    new-instance v8, LX/0Pz;

    invoke-direct {v8}, LX/0Pz;-><init>()V

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v6

    invoke-virtual {v8, v6}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v6

    invoke-virtual {v6, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v6

    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v6

    .line 1827721
    iput-object v6, v7, LX/23u;->k:LX/0Px;

    .line 1827722
    move-object v6, v7

    .line 1827723
    const/4 v7, 0x0

    .line 1827724
    iput-object v7, v6, LX/23u;->ay:Lcom/facebook/graphql/model/GraphQLStoryHeader;

    .line 1827725
    move-object v6, v6

    .line 1827726
    invoke-virtual {v6}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v6

    .line 1827727
    invoke-virtual {v1, v6, v2}, LX/189;->a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v6

    move-object v0, v6

    .line 1827728
    invoke-static {v0}, LX/182;->f(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    .line 1827729
    iget-object v1, p0, LX/BsX;->b:LX/BsZ;

    iget-object v1, v1, LX/BsZ;->a:LX/0bH;

    new-instance v2, LX/1Ne;

    invoke-direct {v2, v0}, LX/1Ne;-><init>(Lcom/facebook/graphql/model/FeedUnit;)V

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b7;)V

    goto :goto_0

    .line 1827730
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1827731
    :cond_3
    new-instance v4, LX/39x;

    invoke-direct {v4}, LX/39x;-><init>()V

    .line 1827732
    invoke-virtual {v0}, Lcom/facebook/feed/protocol/AssociatePostToFundraiserForStoryModels$AssociatePostToFundraiserForStoryAttachmentFieldsModel;->a()LX/0Px;

    move-result-object v5

    .line 1827733
    iput-object v5, v4, LX/39x;->p:LX/0Px;

    .line 1827734
    invoke-virtual {v0}, Lcom/facebook/feed/protocol/AssociatePostToFundraiserForStoryModels$AssociatePostToFundraiserForStoryAttachmentFieldsModel;->b()LX/57w;

    move-result-object v5

    .line 1827735
    if-nez v5, :cond_4

    .line 1827736
    const/4 v6, 0x0

    .line 1827737
    :goto_3
    move-object v5, v6

    .line 1827738
    iput-object v5, v4, LX/39x;->s:Lcom/facebook/graphql/model/GraphQLNode;

    .line 1827739
    invoke-virtual {v4}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v4

    goto :goto_2

    .line 1827740
    :cond_4
    new-instance v6, LX/4XR;

    invoke-direct {v6}, LX/4XR;-><init>()V

    .line 1827741
    new-instance v7, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v8, 0x291507f7

    invoke-direct {v7, v8}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    .line 1827742
    iput-object v7, v6, LX/4XR;->qc:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1827743
    invoke-interface {v5}, LX/57w;->x()LX/176;

    move-result-object v7

    const/4 v10, 0x0

    .line 1827744
    if-nez v7, :cond_5

    .line 1827745
    const/4 v8, 0x0

    .line 1827746
    :goto_4
    move-object v7, v8

    .line 1827747
    iput-object v7, v6, LX/4XR;->F:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1827748
    invoke-interface {v5}, LX/57w;->y()LX/584;

    move-result-object v7

    .line 1827749
    if-nez v7, :cond_d

    .line 1827750
    const/4 v8, 0x0

    .line 1827751
    :goto_5
    move-object v7, v8

    .line 1827752
    iput-object v7, v6, LX/4XR;->bL:Lcom/facebook/graphql/model/GraphQLCharity;

    .line 1827753
    invoke-interface {v5}, LX/57w;->z()Ljava/lang/String;

    move-result-object v7

    .line 1827754
    iput-object v7, v6, LX/4XR;->cZ:Ljava/lang/String;

    .line 1827755
    invoke-interface {v5}, LX/57w;->A()LX/585;

    move-result-object v7

    .line 1827756
    if-nez v7, :cond_f

    .line 1827757
    const/4 v8, 0x0

    .line 1827758
    :goto_6
    move-object v7, v8

    .line 1827759
    iput-object v7, v6, LX/4XR;->dv:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1827760
    invoke-interface {v5}, LX/57w;->B()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel$FriendDonorsModel;

    move-result-object v7

    .line 1827761
    if-nez v7, :cond_10

    .line 1827762
    const/4 v8, 0x0

    .line 1827763
    :goto_7
    move-object v7, v8

    .line 1827764
    iput-object v7, v6, LX/4XR;->eZ:Lcom/facebook/graphql/model/GraphQLFundraiserFriendDonorsConnection;

    .line 1827765
    invoke-interface {v5}, LX/57w;->m()Ljava/lang/String;

    move-result-object v7

    .line 1827766
    iput-object v7, v6, LX/4XR;->fO:Ljava/lang/String;

    .line 1827767
    invoke-interface {v5}, LX/57w;->C()Ljava/lang/String;

    move-result-object v7

    .line 1827768
    iput-object v7, v6, LX/4XR;->is:Ljava/lang/String;

    .line 1827769
    invoke-virtual {v6}, LX/4XR;->a()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v6

    goto :goto_3

    .line 1827770
    :cond_5
    new-instance v11, LX/173;

    invoke-direct {v11}, LX/173;-><init>()V

    .line 1827771
    invoke-interface {v7}, LX/176;->c()LX/0Px;

    move-result-object v8

    if-eqz v8, :cond_7

    .line 1827772
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v12

    move v9, v10

    .line 1827773
    :goto_8
    invoke-interface {v7}, LX/176;->c()LX/0Px;

    move-result-object v8

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v8

    if-ge v9, v8, :cond_6

    .line 1827774
    invoke-interface {v7}, LX/176;->c()LX/0Px;

    move-result-object v8

    invoke-virtual {v8, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/1jz;

    .line 1827775
    if-nez v8, :cond_a

    .line 1827776
    const/4 v13, 0x0

    .line 1827777
    :goto_9
    move-object v8, v13

    .line 1827778
    invoke-virtual {v12, v8}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1827779
    add-int/lit8 v8, v9, 0x1

    move v9, v8

    goto :goto_8

    .line 1827780
    :cond_6
    invoke-virtual {v12}, LX/0Pz;->b()LX/0Px;

    move-result-object v8

    .line 1827781
    iput-object v8, v11, LX/173;->b:LX/0Px;

    .line 1827782
    :cond_7
    invoke-interface {v7}, LX/176;->b()LX/0Px;

    move-result-object v8

    if-eqz v8, :cond_9

    .line 1827783
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v9

    .line 1827784
    :goto_a
    invoke-interface {v7}, LX/176;->b()LX/0Px;

    move-result-object v8

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v8

    if-ge v10, v8, :cond_8

    .line 1827785
    invoke-interface {v7}, LX/176;->b()LX/0Px;

    move-result-object v8

    invoke-virtual {v8, v10}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/1W5;

    .line 1827786
    if-nez v8, :cond_b

    .line 1827787
    const/4 v12, 0x0

    .line 1827788
    :goto_b
    move-object v8, v12

    .line 1827789
    invoke-virtual {v9, v8}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1827790
    add-int/lit8 v10, v10, 0x1

    goto :goto_a

    .line 1827791
    :cond_8
    invoke-virtual {v9}, LX/0Pz;->b()LX/0Px;

    move-result-object v8

    .line 1827792
    iput-object v8, v11, LX/173;->e:LX/0Px;

    .line 1827793
    :cond_9
    invoke-interface {v7}, LX/176;->a()Ljava/lang/String;

    move-result-object v8

    .line 1827794
    iput-object v8, v11, LX/173;->f:Ljava/lang/String;

    .line 1827795
    invoke-virtual {v11}, LX/173;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v8

    goto/16 :goto_4

    .line 1827796
    :cond_a
    new-instance v13, LX/4Vo;

    invoke-direct {v13}, LX/4Vo;-><init>()V

    .line 1827797
    invoke-interface {v8}, LX/1jz;->a()I

    move-result p1

    .line 1827798
    iput p1, v13, LX/4Vo;->b:I

    .line 1827799
    invoke-interface {v8}, LX/1jz;->b()I

    move-result p1

    .line 1827800
    iput p1, v13, LX/4Vo;->c:I

    .line 1827801
    invoke-interface {v8}, LX/1jz;->c()I

    move-result p1

    .line 1827802
    iput p1, v13, LX/4Vo;->d:I

    .line 1827803
    invoke-virtual {v13}, LX/4Vo;->a()Lcom/facebook/graphql/model/GraphQLAggregatedEntitiesAtRange;

    move-result-object v13

    goto :goto_9

    .line 1827804
    :cond_b
    new-instance v12, LX/4W6;

    invoke-direct {v12}, LX/4W6;-><init>()V

    .line 1827805
    invoke-interface {v8}, LX/1W5;->a()LX/171;

    move-result-object v13

    .line 1827806
    if-nez v13, :cond_c

    .line 1827807
    const/4 p1, 0x0

    .line 1827808
    :goto_c
    move-object v13, p1

    .line 1827809
    iput-object v13, v12, LX/4W6;->b:Lcom/facebook/graphql/model/GraphQLEntity;

    .line 1827810
    invoke-interface {v8}, LX/1W5;->b()I

    move-result v13

    .line 1827811
    iput v13, v12, LX/4W6;->c:I

    .line 1827812
    invoke-interface {v8}, LX/1W5;->c()I

    move-result v13

    .line 1827813
    iput v13, v12, LX/4W6;->d:I

    .line 1827814
    invoke-virtual {v12}, LX/4W6;->a()Lcom/facebook/graphql/model/GraphQLEntityAtRange;

    move-result-object v12

    goto :goto_b

    .line 1827815
    :cond_c
    new-instance p1, LX/170;

    invoke-direct {p1}, LX/170;-><init>()V

    .line 1827816
    invoke-interface {v13}, LX/171;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    .line 1827817
    iput-object v0, p1, LX/170;->ab:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1827818
    invoke-interface {v13}, LX/171;->c()LX/0Px;

    move-result-object v0

    .line 1827819
    iput-object v0, p1, LX/170;->b:LX/0Px;

    .line 1827820
    invoke-interface {v13}, LX/171;->d()Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    move-result-object v0

    .line 1827821
    iput-object v0, p1, LX/170;->l:Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    .line 1827822
    invoke-interface {v13}, LX/171;->e()Ljava/lang/String;

    move-result-object v0

    .line 1827823
    iput-object v0, p1, LX/170;->o:Ljava/lang/String;

    .line 1827824
    invoke-interface {v13}, LX/171;->v_()Ljava/lang/String;

    move-result-object v0

    .line 1827825
    iput-object v0, p1, LX/170;->A:Ljava/lang/String;

    .line 1827826
    invoke-interface {v13}, LX/171;->w_()Ljava/lang/String;

    move-result-object v0

    .line 1827827
    iput-object v0, p1, LX/170;->X:Ljava/lang/String;

    .line 1827828
    invoke-interface {v13}, LX/171;->j()Ljava/lang/String;

    move-result-object v0

    .line 1827829
    iput-object v0, p1, LX/170;->Y:Ljava/lang/String;

    .line 1827830
    invoke-virtual {p1}, LX/170;->a()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object p1

    goto :goto_c

    .line 1827831
    :cond_d
    new-instance v8, LX/4Vt;

    invoke-direct {v8}, LX/4Vt;-><init>()V

    .line 1827832
    invoke-interface {v7}, LX/584;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v9

    .line 1827833
    iput-object v9, v8, LX/4Vt;->h:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1827834
    invoke-interface {v7}, LX/584;->b()LX/583;

    move-result-object v9

    .line 1827835
    if-nez v9, :cond_e

    .line 1827836
    const/4 v10, 0x0

    .line 1827837
    :goto_d
    move-object v9, v10

    .line 1827838
    iput-object v9, v8, LX/4Vt;->f:Lcom/facebook/graphql/model/GraphQLPage;

    .line 1827839
    invoke-interface {v7}, LX/584;->c()Ljava/lang/String;

    move-result-object v9

    .line 1827840
    iput-object v9, v8, LX/4Vt;->g:Ljava/lang/String;

    .line 1827841
    new-instance v9, Lcom/facebook/graphql/model/GraphQLCharity;

    invoke-direct {v9, v8}, Lcom/facebook/graphql/model/GraphQLCharity;-><init>(LX/4Vt;)V

    .line 1827842
    move-object v8, v9

    .line 1827843
    goto/16 :goto_5

    .line 1827844
    :cond_e
    new-instance v10, LX/4XY;

    invoke-direct {v10}, LX/4XY;-><init>()V

    .line 1827845
    invoke-interface {v9}, LX/583;->b()Ljava/lang/String;

    move-result-object v11

    .line 1827846
    iput-object v11, v10, LX/4XY;->ag:Ljava/lang/String;

    .line 1827847
    invoke-virtual {v10}, LX/4XY;->a()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v10

    goto :goto_d

    .line 1827848
    :cond_f
    new-instance v8, LX/173;

    invoke-direct {v8}, LX/173;-><init>()V

    .line 1827849
    invoke-interface {v7}, LX/585;->a()Ljava/lang/String;

    move-result-object v9

    .line 1827850
    iput-object v9, v8, LX/173;->f:Ljava/lang/String;

    .line 1827851
    invoke-virtual {v8}, LX/173;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v8

    goto/16 :goto_6

    .line 1827852
    :cond_10
    new-instance v10, LX/4WS;

    invoke-direct {v10}, LX/4WS;-><init>()V

    .line 1827853
    invoke-virtual {v7}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel$FriendDonorsModel;->a()LX/0Px;

    move-result-object v8

    if-eqz v8, :cond_12

    .line 1827854
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v11

    .line 1827855
    const/4 v8, 0x0

    move v9, v8

    :goto_e
    invoke-virtual {v7}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel$FriendDonorsModel;->a()LX/0Px;

    move-result-object v8

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v8

    if-ge v9, v8, :cond_11

    .line 1827856
    invoke-virtual {v7}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel$FriendDonorsModel;->a()LX/0Px;

    move-result-object v8

    invoke-virtual {v8, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel$FriendDonorsModel$NodesModel;

    .line 1827857
    if-nez v8, :cond_13

    .line 1827858
    const/4 v12, 0x0

    .line 1827859
    :goto_f
    move-object v8, v12

    .line 1827860
    invoke-virtual {v11, v8}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1827861
    add-int/lit8 v8, v9, 0x1

    move v9, v8

    goto :goto_e

    .line 1827862
    :cond_11
    invoke-virtual {v11}, LX/0Pz;->b()LX/0Px;

    move-result-object v8

    .line 1827863
    iput-object v8, v10, LX/4WS;->b:LX/0Px;

    .line 1827864
    :cond_12
    new-instance v8, Lcom/facebook/graphql/model/GraphQLFundraiserFriendDonorsConnection;

    invoke-direct {v8, v10}, Lcom/facebook/graphql/model/GraphQLFundraiserFriendDonorsConnection;-><init>(LX/4WS;)V

    .line 1827865
    move-object v8, v8

    .line 1827866
    goto/16 :goto_7

    .line 1827867
    :cond_13
    new-instance v12, LX/33O;

    invoke-direct {v12}, LX/33O;-><init>()V

    .line 1827868
    invoke-virtual {v8}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel$FriendDonorsModel$NodesModel;->b()Ljava/lang/String;

    move-result-object v13

    .line 1827869
    iput-object v13, v12, LX/33O;->U:Ljava/lang/String;

    .line 1827870
    invoke-virtual {v8}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel$FriendDonorsModel$NodesModel;->c()Ljava/lang/String;

    move-result-object v13

    .line 1827871
    iput-object v13, v12, LX/33O;->aI:Ljava/lang/String;

    .line 1827872
    invoke-virtual {v8}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$FundraiserForStoryAttachmentFragmentModel$FriendDonorsModel$NodesModel;->d()LX/1Fb;

    move-result-object v13

    .line 1827873
    if-nez v13, :cond_14

    .line 1827874
    const/4 p1, 0x0

    .line 1827875
    :goto_10
    move-object v13, p1

    .line 1827876
    iput-object v13, v12, LX/33O;->ba:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1827877
    invoke-virtual {v12}, LX/33O;->a()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v12

    goto :goto_f

    .line 1827878
    :cond_14
    new-instance p1, LX/2dc;

    invoke-direct {p1}, LX/2dc;-><init>()V

    .line 1827879
    invoke-interface {v13}, LX/1Fb;->a()I

    move-result v8

    .line 1827880
    iput v8, p1, LX/2dc;->c:I

    .line 1827881
    invoke-interface {v13}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v8

    .line 1827882
    iput-object v8, p1, LX/2dc;->h:Ljava/lang/String;

    .line 1827883
    invoke-interface {v13}, LX/1Fb;->c()I

    move-result v8

    .line 1827884
    iput v8, p1, LX/2dc;->i:I

    .line 1827885
    invoke-virtual {p1}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object p1

    goto :goto_10
.end method
