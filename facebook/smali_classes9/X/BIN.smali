.class public final LX/BIN;
.super LX/2oa;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2oa",
        "<",
        "LX/2ou;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/BIP;


# direct methods
.method public constructor <init>(LX/BIP;)V
    .locals 0

    .prologue
    .line 1770267
    iput-object p1, p0, LX/BIN;->a:LX/BIP;

    invoke-direct {p0}, LX/2oa;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/2ou;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1770268
    const-class v0, LX/2ou;

    return-object v0
.end method

.method public final b(LX/0b7;)V
    .locals 2

    .prologue
    .line 1770269
    iget-object v0, p0, LX/BIN;->a:LX/BIP;

    iget-object v0, v0, LX/BIP;->i:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->getPlayerState()LX/2qV;

    move-result-object v0

    .line 1770270
    iget-object v1, p0, LX/BIN;->a:LX/BIP;

    iget-object v1, v1, LX/BIP;->i:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v1}, Lcom/facebook/video/player/RichVideoPlayer;->getWidth()I

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/BIN;->a:LX/BIP;

    iget-object v1, v1, LX/BIP;->i:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v1}, Lcom/facebook/video/player/RichVideoPlayer;->getHeight()I

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, LX/2qV;->ATTEMPT_TO_PLAY:LX/2qV;

    if-eq v0, v1, :cond_0

    sget-object v1, LX/2qV;->PLAYING:LX/2qV;

    if-ne v0, v1, :cond_1

    .line 1770271
    :cond_0
    iget-object v0, p0, LX/BIN;->a:LX/BIP;

    iget-object v0, v0, LX/BIP;->i:Lcom/facebook/video/player/RichVideoPlayer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->setVisibility(I)V

    .line 1770272
    :cond_1
    return-void
.end method
