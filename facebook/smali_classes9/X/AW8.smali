.class public final LX/AW8;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 11

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1682110
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_9

    .line 1682111
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1682112
    :goto_0
    return v1

    .line 1682113
    :cond_0
    const-string v9, "time_offset_ms"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 1682114
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v3, v0

    move v0, v2

    .line 1682115
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_7

    .line 1682116
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 1682117
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1682118
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_1

    if-eqz v8, :cond_1

    .line 1682119
    const-string v9, "__type__"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_2

    const-string v9, "__typename"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 1682120
    :cond_2
    invoke-static {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v7

    goto :goto_1

    .line 1682121
    :cond_3
    const-string v9, "body_text"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 1682122
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 1682123
    :cond_4
    const-string v9, "commenter_name"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 1682124
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_1

    .line 1682125
    :cond_5
    const-string v9, "commenter_photo"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 1682126
    invoke-static {p0, p1}, LX/AW7;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 1682127
    :cond_6
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1682128
    :cond_7
    const/4 v8, 0x5

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 1682129
    invoke-virtual {p1, v1, v7}, LX/186;->b(II)V

    .line 1682130
    invoke-virtual {p1, v2, v6}, LX/186;->b(II)V

    .line 1682131
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v5}, LX/186;->b(II)V

    .line 1682132
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v4}, LX/186;->b(II)V

    .line 1682133
    if-eqz v0, :cond_8

    .line 1682134
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3, v1}, LX/186;->a(III)V

    .line 1682135
    :cond_8
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_9
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    goto/16 :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1682136
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1682137
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1682138
    if-eqz v0, :cond_0

    .line 1682139
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1682140
    invoke-static {p0, p1, v2, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1682141
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1682142
    if-eqz v0, :cond_1

    .line 1682143
    const-string v1, "body_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1682144
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1682145
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1682146
    if-eqz v0, :cond_2

    .line 1682147
    const-string v1, "commenter_name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1682148
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1682149
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1682150
    if-eqz v0, :cond_3

    .line 1682151
    const-string v1, "commenter_photo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1682152
    invoke-static {p0, v0, p2, p3}, LX/AW7;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1682153
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1682154
    if-eqz v0, :cond_4

    .line 1682155
    const-string v1, "time_offset_ms"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1682156
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1682157
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1682158
    return-void
.end method
