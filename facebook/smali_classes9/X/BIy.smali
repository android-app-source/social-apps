.class public final enum LX/BIy;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/BIy;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/BIy;

.field public static final enum NEXT:LX/BIy;

.field public static final enum PREVIOUS:LX/BIy;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1771120
    new-instance v0, LX/BIy;

    const-string v1, "PREVIOUS"

    invoke-direct {v0, v1, v2}, LX/BIy;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BIy;->PREVIOUS:LX/BIy;

    .line 1771121
    new-instance v0, LX/BIy;

    const-string v1, "NEXT"

    invoke-direct {v0, v1, v3}, LX/BIy;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BIy;->NEXT:LX/BIy;

    .line 1771122
    const/4 v0, 0x2

    new-array v0, v0, [LX/BIy;

    sget-object v1, LX/BIy;->PREVIOUS:LX/BIy;

    aput-object v1, v0, v2

    sget-object v1, LX/BIy;->NEXT:LX/BIy;

    aput-object v1, v0, v3

    sput-object v0, LX/BIy;->$VALUES:[LX/BIy;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1771123
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/BIy;
    .locals 1

    .prologue
    .line 1771124
    const-class v0, LX/BIy;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/BIy;

    return-object v0
.end method

.method public static values()[LX/BIy;
    .locals 1

    .prologue
    .line 1771125
    sget-object v0, LX/BIy;->$VALUES:[LX/BIy;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/BIy;

    return-object v0
.end method
