.class public final LX/Buv;
.super LX/Bup;
.source ""


# instance fields
.field public final synthetic b:LX/Buw;


# direct methods
.method public constructor <init>(LX/Buw;)V
    .locals 0

    .prologue
    .line 1831827
    iput-object p1, p0, LX/Buv;->b:LX/Buw;

    invoke-direct {p0, p1}, LX/Bup;-><init>(LX/Bur;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/Menu;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1831828
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v0

    .line 1831829
    check-cast v2, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1831830
    iget-object v0, p0, LX/Buv;->b:LX/Buw;

    iput-object p2, v0, LX/Buw;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1831831
    invoke-static {v2}, LX/17E;->y(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v3

    .line 1831832
    invoke-virtual {p3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    .line 1831833
    invoke-virtual {p0, p2}, LX/1wH;->d(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1831834
    iget-object v0, p0, LX/Buv;->b:LX/Buw;

    .line 1831835
    invoke-virtual {v0, p1, p2, p3}, LX/1SX;->a(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;)V

    .line 1831836
    :cond_0
    if-eqz v3, :cond_1

    move-object v0, p0

    move-object v1, p1

    move-object v4, p3

    .line 1831837
    iget-object v6, v0, LX/Buv;->b:LX/Buw;

    move-object v7, v1

    move-object p0, v2

    move-object p1, v3

    move-object p2, v4

    move-object p3, v5

    .line 1831838
    const v0, 0x7f081142

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p3, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1831839
    invoke-interface {v7, v0}, Landroid/view/Menu;->add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    .line 1831840
    new-instance v1, LX/Buu;

    invoke-direct {v1, v6, p1, p0, p2}, LX/Buu;-><init>(LX/Buw;Lcom/facebook/graphql/model/GraphQLActor;Lcom/facebook/graphql/model/GraphQLStory;Landroid/view/View;)V

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1831841
    const v1, 0x7f0208b2

    move v1, v1

    .line 1831842
    invoke-virtual {v6, v0, v1, p0}, LX/1SX;->a(Landroid/view/MenuItem;ILcom/facebook/graphql/model/FeedUnit;)V

    .line 1831843
    :cond_1
    return-void
.end method
