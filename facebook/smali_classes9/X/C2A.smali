.class public LX/C2A;
.super Landroid/widget/TextView;
.source ""


# instance fields
.field public a:LX/Abg;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final b:LX/Abf;

.field public c:LX/1Pq;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public d:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public e:Landroid/animation/Animator;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1843955
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/C2A;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1843956
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1843953
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/C2A;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1843954
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1843957
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1843958
    const-class v0, LX/C2A;

    invoke-static {v0, p0}, LX/C2A;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1843959
    new-instance v0, LX/C29;

    invoke-direct {v0, p0}, LX/C29;-><init>(LX/C2A;)V

    iput-object v0, p0, LX/C2A;->b:LX/Abf;

    .line 1843960
    iget-object v0, p0, LX/C2A;->a:LX/Abg;

    iget-object v1, p0, LX/C2A;->b:LX/Abf;

    .line 1843961
    iput-object v1, v0, LX/Abg;->g:LX/Abf;

    .line 1843962
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/C2A;

    invoke-static {p0}, LX/Abg;->b(LX/0QB;)LX/Abg;

    move-result-object p0

    check-cast p0, LX/Abg;

    iput-object p0, p1, LX/C2A;->a:LX/Abg;

    return-void
.end method
