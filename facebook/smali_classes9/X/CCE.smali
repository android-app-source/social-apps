.class public LX/CCE;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/CCC;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/richtextpicker/RichTextPickerStyleComponentSpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1857380
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/CCE;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/richtextpicker/RichTextPickerStyleComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1857381
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1857382
    iput-object p1, p0, LX/CCE;->b:LX/0Ot;

    .line 1857383
    return-void
.end method

.method public static a(LX/0QB;)LX/CCE;
    .locals 4

    .prologue
    .line 1857384
    const-class v1, LX/CCE;

    monitor-enter v1

    .line 1857385
    :try_start_0
    sget-object v0, LX/CCE;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1857386
    sput-object v2, LX/CCE;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1857387
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1857388
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1857389
    new-instance v3, LX/CCE;

    const/16 p0, 0x2163

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/CCE;-><init>(LX/0Ot;)V

    .line 1857390
    move-object v0, v3

    .line 1857391
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1857392
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/CCE;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1857393
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1857394
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1857395
    const v0, 0x23eba4cd

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 9

    .prologue
    .line 1857396
    check-cast p2, LX/CCD;

    .line 1857397
    iget-object v0, p0, LX/CCE;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/richtextpicker/RichTextPickerStyleComponentSpec;

    iget-object v1, p2, LX/CCD;->a:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    iget-object v2, p2, LX/CCD;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p2, LX/CCD;->c:LX/1Pr;

    const/high16 v5, 0x3f800000    # 1.0f

    .line 1857398
    new-instance v6, LX/CCF;

    .line 1857399
    iget-object v4, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, v4

    .line 1857400
    check-cast v4, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-direct {v6, v4}, LX/CCF;-><init>(Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 1857401
    iget-object v4, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, v4

    .line 1857402
    check-cast v4, LX/0jW;

    invoke-interface {v3, v6, v4}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    invoke-static {v1, v4}, LX/87Y;->a(Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;)Z

    move-result v6

    .line 1857403
    invoke-static {p1}, LX/1um;->c(LX/1De;)LX/1up;

    move-result-object v7

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getBackgroundImageUrl()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v4, 0x0

    :goto_0
    invoke-virtual {v7, v4}, LX/1up;->a(LX/1aZ;)LX/1up;

    move-result-object v7

    invoke-static {}, LX/1n3;->b()LX/1n5;

    move-result-object v8

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getGradientEndColor()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    new-instance v4, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getBackgroundColor()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result p0

    invoke-direct {v4, p0}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    :goto_1
    invoke-virtual {v8, v4}, LX/1n5;->a(Landroid/graphics/drawable/Drawable;)LX/1n5;

    move-result-object v4

    invoke-virtual {v4}, LX/1n6;->b()LX/1dc;

    move-result-object v4

    invoke-virtual {v7, v4}, LX/1up;->a(LX/1dc;)LX/1up;

    move-result-object v7

    invoke-static {}, LX/4Ab;->e()LX/4Ab;

    move-result-object v8

    const/4 p0, -0x1

    if-eqz v6, :cond_2

    const/high16 v4, 0x40c00000    # 6.0f

    :goto_2
    invoke-virtual {v8, p0, v4}, LX/4Ab;->a(IF)LX/4Ab;

    move-result-object v4

    invoke-virtual {v4, v5}, LX/4Ab;->d(F)LX/4Ab;

    move-result-object v4

    const v5, 0x7f0a009a

    invoke-static {p1, v5}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v5

    invoke-virtual {v4, v5}, LX/4Ab;->a(I)LX/4Ab;

    move-result-object v4

    invoke-virtual {v7, v4}, LX/1up;->a(LX/4Ab;)LX/1up;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    const v5, 0x7f0b1da9

    invoke-interface {v4, v5}, LX/1Di;->i(I)LX/1Di;

    move-result-object v4

    const v5, 0x7f0b1da9

    invoke-interface {v4, v5}, LX/1Di;->q(I)LX/1Di;

    move-result-object v4

    .line 1857404
    const v5, 0x23eba4cd

    const/4 v6, 0x0

    invoke-static {p1, v5, v6}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v5

    move-object v5, v5

    .line 1857405
    invoke-interface {v4, v5}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v4

    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    move-object v0, v4

    .line 1857406
    return-object v0

    :cond_0
    iget-object v4, v0, Lcom/facebook/feedplugins/richtextpicker/RichTextPickerStyleComponentSpec;->b:LX/1Ad;

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getBackgroundImageUrl()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    invoke-static {v8}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v8

    invoke-virtual {v8}, LX/1bX;->n()LX/1bf;

    move-result-object v8

    invoke-virtual {v4, v8}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v4

    check-cast v4, LX/1Ad;

    sget-object v8, Lcom/facebook/feedplugins/richtextpicker/RichTextPickerStyleComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v4, v8}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v4

    invoke-virtual {v4}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v4

    goto/16 :goto_0

    :cond_1
    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getBackgroundColor()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getGradientEndColor()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getGradientDirection()Ljava/lang/String;

    move-result-object p2

    invoke-static {v4, p0, p2}, LX/87X;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/GradientDrawable;

    move-result-object v4

    goto/16 :goto_1

    :cond_2
    move v4, v5

    goto :goto_2
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 1857407
    invoke-static {}, LX/1dS;->b()V

    .line 1857408
    iget v0, p1, LX/1dQ;->b:I

    .line 1857409
    packed-switch v0, :pswitch_data_0

    .line 1857410
    :goto_0
    return-object v2

    .line 1857411
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 1857412
    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1857413
    check-cast v1, LX/CCD;

    .line 1857414
    iget-object v3, p0, LX/CCE;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/richtextpicker/RichTextPickerStyleComponentSpec;

    iget-object v4, v1, LX/CCD;->a:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    iget-object p1, v1, LX/CCD;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object p2, v1, LX/CCD;->c:LX/1Pr;

    .line 1857415
    new-instance v1, LX/CCF;

    .line 1857416
    iget-object p0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object p0, p0

    .line 1857417
    check-cast p0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-direct {v1, p0}, LX/CCF;-><init>(Lcom/facebook/graphql/model/GraphQLStory;)V

    invoke-interface {p2, v1, v4}, LX/1Pr;->a(LX/1KL;Ljava/lang/Object;)Z

    .line 1857418
    iget-object v1, v3, Lcom/facebook/feedplugins/richtextpicker/RichTextPickerStyleComponentSpec;->d:LX/CC2;

    .line 1857419
    iget-object p0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object p0, p0

    .line 1857420
    check-cast p0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1, p0, v4}, LX/CC2;->a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;)V

    .line 1857421
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x23eba4cd
        :pswitch_0
    .end packed-switch
.end method
