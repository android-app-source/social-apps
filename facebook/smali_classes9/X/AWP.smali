.class public final LX/AWP;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Lcom/facebook/facecast/model/FacecastPrivacyData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Lcom/facebook/ipc/composer/intent/ComposerTargetData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:LX/21D;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Z

.field public n:Z

.field public o:Z

.field public p:Z

.field public q:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1682790
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1682791
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1682792
    iput-object v0, p0, LX/AWP;->j:LX/0Px;

    .line 1682793
    return-void
.end method

.method public constructor <init>(Lcom/facebook/facecast/model/FacecastCompositionData;)V
    .locals 1

    .prologue
    .line 1682794
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1682795
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1682796
    iput-object v0, p0, LX/AWP;->j:LX/0Px;

    .line 1682797
    iget-object v0, p1, Lcom/facebook/facecast/model/FacecastCompositionData;->a:Ljava/lang/String;

    iput-object v0, p0, LX/AWP;->a:Ljava/lang/String;

    .line 1682798
    iget-object v0, p1, Lcom/facebook/facecast/model/FacecastCompositionData;->b:Lcom/facebook/facecast/model/FacecastPrivacyData;

    iput-object v0, p0, LX/AWP;->b:Lcom/facebook/facecast/model/FacecastPrivacyData;

    .line 1682799
    iget-object v0, p1, Lcom/facebook/facecast/model/FacecastCompositionData;->c:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    iput-object v0, p0, LX/AWP;->c:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    .line 1682800
    iget-object v0, p1, Lcom/facebook/facecast/model/FacecastCompositionData;->d:Ljava/lang/String;

    iput-object v0, p0, LX/AWP;->d:Ljava/lang/String;

    .line 1682801
    iget-object v0, p1, Lcom/facebook/facecast/model/FacecastCompositionData;->e:Ljava/lang/String;

    iput-object v0, p0, LX/AWP;->e:Ljava/lang/String;

    .line 1682802
    iget-object v0, p1, Lcom/facebook/facecast/model/FacecastCompositionData;->f:Ljava/lang/String;

    iput-object v0, p0, LX/AWP;->f:Ljava/lang/String;

    .line 1682803
    iget-object v0, p1, Lcom/facebook/facecast/model/FacecastCompositionData;->g:Ljava/lang/String;

    iput-object v0, p0, LX/AWP;->g:Ljava/lang/String;

    .line 1682804
    iget-object v0, p1, Lcom/facebook/facecast/model/FacecastCompositionData;->j:LX/0Px;

    iput-object v0, p0, LX/AWP;->j:LX/0Px;

    .line 1682805
    iget-object v0, p1, Lcom/facebook/facecast/model/FacecastCompositionData;->k:LX/21D;

    iput-object v0, p0, LX/AWP;->k:LX/21D;

    .line 1682806
    iget-object v0, p1, Lcom/facebook/facecast/model/FacecastCompositionData;->l:Ljava/lang/String;

    iput-object v0, p0, LX/AWP;->l:Ljava/lang/String;

    .line 1682807
    iget-boolean v0, p1, Lcom/facebook/facecast/model/FacecastCompositionData;->m:Z

    iput-boolean v0, p0, LX/AWP;->m:Z

    .line 1682808
    iget-boolean v0, p1, Lcom/facebook/facecast/model/FacecastCompositionData;->q:Z

    iput-boolean v0, p0, LX/AWP;->n:Z

    .line 1682809
    iget-boolean v0, p1, Lcom/facebook/facecast/model/FacecastCompositionData;->r:Z

    iput-boolean v0, p0, LX/AWP;->o:Z

    .line 1682810
    iget-boolean v0, p1, Lcom/facebook/facecast/model/FacecastCompositionData;->n:Z

    iput-boolean v0, p0, LX/AWP;->p:Z

    .line 1682811
    iget-object v0, p1, Lcom/facebook/facecast/model/FacecastCompositionData;->o:Ljava/lang/String;

    iput-object v0, p0, LX/AWP;->q:Ljava/lang/String;

    .line 1682812
    return-void
.end method

.method public static b(LX/AWP;)LX/AWS;
    .locals 1

    .prologue
    .line 1682813
    iget-object v0, p0, LX/AWP;->b:Lcom/facebook/facecast/model/FacecastPrivacyData;

    if-nez v0, :cond_0

    .line 1682814
    new-instance v0, LX/AWS;

    invoke-direct {v0}, LX/AWS;-><init>()V

    invoke-virtual {v0}, LX/AWS;->a()Lcom/facebook/facecast/model/FacecastPrivacyData;

    move-result-object v0

    iput-object v0, p0, LX/AWP;->b:Lcom/facebook/facecast/model/FacecastPrivacyData;

    .line 1682815
    :cond_0
    iget-object v0, p0, LX/AWP;->b:Lcom/facebook/facecast/model/FacecastPrivacyData;

    invoke-virtual {v0}, Lcom/facebook/facecast/model/FacecastPrivacyData;->d()LX/AWS;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/facebook/facecast/model/FacecastCompositionData;
    .locals 1

    .prologue
    .line 1682816
    new-instance v0, Lcom/facebook/facecast/model/FacecastCompositionData;

    invoke-direct {v0, p0}, Lcom/facebook/facecast/model/FacecastCompositionData;-><init>(LX/AWP;)V

    return-object v0
.end method
