.class public final LX/AfW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonateEventSubscriptionModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/AfX;


# direct methods
.method public constructor <init>(LX/AfX;)V
    .locals 0

    .prologue
    .line 1699378
    iput-object p1, p0, LX/AfW;->a:LX/AfX;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1699387
    iget-object v0, p0, LX/AfW;->a:LX/AfX;

    iget-object v0, v0, LX/AfX;->d:LX/03V;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, LX/AfX;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_graphFailure"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Failed to get donation subscription."

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1699388
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1699379
    check-cast p1, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonateEventSubscriptionModel;

    .line 1699380
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonateEventSubscriptionModel;->j()Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1699381
    :cond_0
    :goto_0
    return-void

    .line 1699382
    :cond_1
    iget-object v0, p0, LX/AfW;->a:LX/AfX;

    iget-object v0, v0, LX/AfX;->g:LX/AeV;

    if-eqz v0, :cond_0

    .line 1699383
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 1699384
    new-instance v1, LX/AfV;

    invoke-virtual {p1}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonateEventSubscriptionModel;->a()Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonateEventSubscriptionModel$DonationModel;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonateEventSubscriptionModel;->j()Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    move-result-object v3

    invoke-direct {v1, v2, v3}, LX/AfV;-><init>(Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonateEventSubscriptionModel$DonationModel;Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;)V

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 1699385
    iget-object v1, p0, LX/AfW;->a:LX/AfX;

    iget-object v1, v1, LX/AfX;->g:LX/AeV;

    sget-object v2, LX/AeN;->LIVE_DONATION_EVENT:LX/AeN;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, v3}, LX/AeV;->a(LX/AeN;Ljava/util/List;Z)V

    .line 1699386
    iget-object v0, p0, LX/AfW;->a:LX/AfX;

    iget-object v0, v0, LX/AfX;->e:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/facecastdisplay/liveevent/donation/LiveDonationEventSubscription$1$1;

    invoke-direct {v1, p0, p1}, Lcom/facebook/facecastdisplay/liveevent/donation/LiveDonationEventSubscription$1$1;-><init>(LX/AfW;Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonateEventSubscriptionModel;)V

    const v2, -0x7d88280c

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto :goto_0
.end method
