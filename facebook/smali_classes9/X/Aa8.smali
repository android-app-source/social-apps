.class public LX/Aa8;
.super LX/AWT;
.source ""


# instance fields
.field public final a:Lcom/facebook/fbui/glyph/GlyphView;

.field public b:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1687909
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/Aa8;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1687910
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1687911
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/Aa8;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1687912
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1687913
    invoke-direct {p0, p1, p2, p3}, LX/AWT;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1687914
    const v0, 0x7f0305bc

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1687915
    const v0, 0x7f0d0fcd

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, LX/Aa8;->a:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1687916
    invoke-static {p0}, LX/Aa8;->j(LX/Aa8;)V

    .line 1687917
    return-void
.end method

.method public static j(LX/Aa8;)V
    .locals 2

    .prologue
    .line 1687918
    iget-object v0, p0, LX/Aa8;->a:Lcom/facebook/fbui/glyph/GlyphView;

    iget-boolean v1, p0, LX/Aa8;->b:Z

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setSelected(Z)V

    .line 1687919
    iget-object v1, p0, LX/Aa8;->a:Lcom/facebook/fbui/glyph/GlyphView;

    iget-boolean v0, p0, LX/Aa8;->b:Z

    if-eqz v0, :cond_0

    const v0, 0x7f020818

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 1687920
    return-void

    .line 1687921
    :cond_0
    const v0, 0x7f020919

    goto :goto_0
.end method


# virtual methods
.method public setButtonEnabled(Z)V
    .locals 1

    .prologue
    .line 1687922
    iget-object v0, p0, LX/Aa8;->a:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/glyph/GlyphView;->setEnabled(Z)V

    .line 1687923
    return-void
.end method
