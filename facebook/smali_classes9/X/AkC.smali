.class public LX/AkC;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements LX/AkB;
.implements LX/1aQ;


# instance fields
.field public a:LX/0hB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:Landroid/view/View;

.field private c:Landroid/view/View;

.field private d:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1708868
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1708869
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/AkC;->d:Z

    .line 1708870
    const-class v0, LX/AkC;

    invoke-static {v0, p0}, LX/AkC;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1708871
    const v0, 0x7f03109a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1708872
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/AkC;

    invoke-static {p0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object p0

    check-cast p0, LX/0hB;

    iput-object p0, p1, LX/AkC;->a:LX/0hB;

    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 6

    .prologue
    .line 1708859
    invoke-virtual {p0}, LX/AkC;->getFlyoutView()Landroid/view/View;

    move-result-object v1

    .line 1708860
    invoke-virtual {v1}, Landroid/view/View;->getPaddingTop()I

    move-result v2

    .line 1708861
    invoke-virtual {v1}, Landroid/view/View;->getPaddingLeft()I

    move-result v3

    .line 1708862
    invoke-virtual {v1}, Landroid/view/View;->getPaddingBottom()I

    move-result v4

    .line 1708863
    invoke-virtual {v1}, Landroid/view/View;->getPaddingRight()I

    move-result v5

    .line 1708864
    if-eqz p1, :cond_0

    const v0, 0x7f02152f

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    .line 1708865
    invoke-virtual {v1, v3, v2, v5, v4}, Landroid/view/View;->setPadding(IIII)V

    .line 1708866
    return-void

    .line 1708867
    :cond_0
    const v0, 0x7f02152e

    goto :goto_0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1708858
    const v0, 0x7f0d2794

    invoke-virtual {p0, v0}, LX/AkC;->findViewById(I)Landroid/view/View;

    move-result-object v0

    instance-of v0, v0, Landroid/view/ViewStub;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getAttachmentInsertPoint()Landroid/widget/FrameLayout;
    .locals 1

    .prologue
    .line 1708857
    const v0, 0x7f0d2797

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    return-object v0
.end method

.method public getCollapsedHeight()I
    .locals 1

    .prologue
    .line 1708856
    const/4 v0, 0x0

    return v0
.end method

.method public getExpandedFlyoutHeight()I
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 1708850
    invoke-virtual {p0}, LX/AkC;->getFlyoutView()Landroid/view/View;

    move-result-object v0

    .line 1708851
    iget-object v1, p0, LX/AkC;->a:LX/0hB;

    invoke-virtual {v1}, LX/0hB;->c()I

    move-result v1

    invoke-virtual {p0}, LX/AkC;->getPaddingLeft()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, LX/AkC;->getPaddingRight()I

    move-result v2

    sub-int v2, v1, v2

    .line 1708852
    invoke-virtual {p0}, LX/AkC;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1708853
    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    move v1, v1

    .line 1708854
    const/high16 v2, -0x80000000

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-static {v3, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->measure(II)V

    .line 1708855
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    return v0

    :cond_0
    iget v4, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    sub-int/2addr v2, v4

    iget v1, v1, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    sub-int v1, v2, v1

    goto :goto_0
.end method

.method public getFlyoutView()Landroid/view/View;
    .locals 1

    .prologue
    .line 1708873
    const v0, 0x7f0d2793

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getFlyoutXoutButton()Landroid/view/View;
    .locals 1

    .prologue
    .line 1708849
    const v0, 0x7f0d2795

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getPhotoTray()Landroid/view/View;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1708847
    invoke-virtual {p0}, LX/AkC;->getV2AttachmentView()Landroid/view/View;

    move-result-object v0

    .line 1708848
    instance-of v1, v0, LX/AkB;

    if-eqz v1, :cond_0

    check-cast v0, LX/AkB;

    invoke-interface {v0}, LX/AkB;->getPhotoTray()Landroid/view/View;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getPromptDisplayReasonView()Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;
    .locals 1

    .prologue
    .line 1708844
    invoke-virtual {p0}, LX/AkC;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1708845
    const v0, 0x7f0d2794

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 1708846
    :cond_0
    const v0, 0x7f0d2798

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    return-object v0
.end method

.method public getPromptSubtitleView()Lcom/facebook/widget/text/BetterTextView;
    .locals 1

    .prologue
    .line 1708843
    const v0, 0x7f0d0b8e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    return-object v0
.end method

.method public getPromptTitleView()Lcom/facebook/widget/text/BetterTextView;
    .locals 1

    .prologue
    .line 1708824
    const v0, 0x7f0d0b8d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    return-object v0
.end method

.method public getV2AttachmentView()Landroid/view/View;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1708842
    invoke-virtual {p0}, LX/AkC;->getAttachmentInsertPoint()Landroid/widget/FrameLayout;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1708828
    iget-object v0, p0, LX/AkC;->b:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/AkC;->c:Landroid/view/View;

    if-nez v0, :cond_1

    .line 1708829
    :cond_0
    const v0, 0x7f0d2795

    invoke-virtual {p0, v0}, LX/AkC;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/AkC;->b:Landroid/view/View;

    .line 1708830
    iget-object v0, p0, LX/AkC;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, LX/AkC;->c:Landroid/view/View;

    .line 1708831
    :cond_1
    iget-object v0, p0, LX/AkC;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    iget-object v3, p0, LX/AkC;->b:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v3

    add-int/2addr v0, v3

    .line 1708832
    iget-object v3, p0, LX/AkC;->c:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getRight()I

    move-result v3

    iget-object v4, p0, LX/AkC;->b:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getWidth()I

    move-result v4

    sub-int/2addr v3, v4

    sub-int v0, v3, v0

    .line 1708833
    invoke-virtual {p0}, LX/AkC;->getRight()I

    move-result v3

    .line 1708834
    invoke-virtual {p0}, LX/AkC;->getRight()I

    move-result v4

    iget-object v5, p0, LX/AkC;->c:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getRight()I

    move-result v5

    sub-int/2addr v4, v5

    .line 1708835
    iget-object v5, p0, LX/AkC;->c:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getTop()I

    move-result v5

    iget-object v6, p0, LX/AkC;->b:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getHeight()I

    move-result v6

    add-int/2addr v5, v6

    add-int/2addr v4, v5

    .line 1708836
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    int-to-float v3, v3

    cmpg-float v3, v5, v3

    if-gez v3, :cond_2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    int-to-float v0, v0

    cmpl-float v0, v3, v0

    if-lez v0, :cond_2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    const/4 v3, 0x0

    cmpl-float v0, v0, v3

    if-lez v0, :cond_2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    int-to-float v3, v4

    cmpg-float v0, v0, v3

    if-gez v0, :cond_2

    .line 1708837
    iget-object v0, p0, LX/AkC;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->performClick()Z

    .line 1708838
    iput-boolean v1, p0, LX/AkC;->d:Z

    move v0, v1

    .line 1708839
    :goto_0
    return v0

    .line 1708840
    :cond_2
    iput-boolean v2, p0, LX/AkC;->d:Z

    move v0, v2

    .line 1708841
    goto :goto_0
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v3, 0x2

    const v1, -0x4b8adca1

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1708825
    iget-boolean v2, p0, LX/AkC;->d:Z

    if-eqz v2, :cond_0

    .line 1708826
    const v2, 0x1833de4b

    invoke-static {v3, v3, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1708827
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    const v2, -0x6c63e7df

    invoke-static {v2, v1}, LX/02F;->a(II)V

    goto :goto_0
.end method
