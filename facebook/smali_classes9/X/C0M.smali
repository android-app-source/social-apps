.class public LX/C0M;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/35k;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/C0M",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/35k;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1840411
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1840412
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/C0M;->b:LX/0Zi;

    .line 1840413
    iput-object p1, p0, LX/C0M;->a:LX/0Ot;

    .line 1840414
    return-void
.end method

.method public static a(LX/0QB;)LX/C0M;
    .locals 4

    .prologue
    .line 1840400
    const-class v1, LX/C0M;

    monitor-enter v1

    .line 1840401
    :try_start_0
    sget-object v0, LX/C0M;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1840402
    sput-object v2, LX/C0M;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1840403
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1840404
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1840405
    new-instance v3, LX/C0M;

    const/16 p0, 0x818

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/C0M;-><init>(LX/0Ot;)V

    .line 1840406
    move-object v0, v3

    .line 1840407
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1840408
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C0M;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1840409
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1840410
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 8

    .prologue
    .line 1840360
    check-cast p2, LX/C0L;

    .line 1840361
    iget-object v0, p0, LX/C0M;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/35k;

    iget-object v2, p2, LX/C0L;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p2, LX/C0L;->b:LX/1Po;

    iget-boolean v4, p2, LX/C0L;->c:Z

    iget-boolean v5, p2, LX/C0L;->d:Z

    move-object v1, p1

    .line 1840362
    iget-object v6, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v6, v6

    .line 1840363
    check-cast v6, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1840364
    iget-object v7, v0, LX/35k;->a:LX/Ap5;

    invoke-virtual {v7, v1}, LX/Ap5;->c(LX/1De;)LX/Ap4;

    move-result-object v7

    const/4 p0, 0x4

    invoke-virtual {v7, p0}, LX/Ap4;->h(I)LX/Ap4;

    move-result-object v7

    invoke-virtual {v7, v3}, LX/Ap4;->a(LX/1Po;)LX/Ap4;

    move-result-object v7

    const/4 p0, 0x0

    .line 1840365
    if-eqz v4, :cond_2

    .line 1840366
    :cond_0
    :goto_0
    move-object p0, p0

    .line 1840367
    invoke-virtual {v7, p0}, LX/Ap4;->a(LX/1X1;)LX/Ap4;

    move-result-object v7

    iget-object p0, v0, LX/35k;->d:LX/1qb;

    invoke-virtual {p0, v6}, LX/1qb;->c(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Landroid/text/SpannableStringBuilder;

    move-result-object p0

    invoke-virtual {v7, p0}, LX/Ap4;->a(Ljava/lang/CharSequence;)LX/Ap4;

    move-result-object v7

    invoke-static {v6}, LX/1qb;->d(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Landroid/text/Spannable;

    move-result-object v6

    invoke-virtual {v7, v6}, LX/Ap4;->c(Ljava/lang/CharSequence;)LX/Ap4;

    move-result-object v6

    const/4 v7, 0x0

    .line 1840368
    if-nez v5, :cond_3

    .line 1840369
    :cond_1
    :goto_1
    move-object v7, v7

    .line 1840370
    invoke-virtual {v6, v7}, LX/Ap4;->b(LX/1X1;)LX/Ap4;

    move-result-object v6

    invoke-virtual {v6}, LX/1X5;->c()LX/1Di;

    move-result-object v6

    .line 1840371
    const v7, 0x3c6bce08

    const/4 p0, 0x0

    invoke-static {v1, v7, p0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v7

    move-object v7, v7

    .line 1840372
    invoke-interface {v6, v7}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v6

    invoke-interface {v6}, LX/1Di;->k()LX/1Dg;

    move-result-object v6

    move-object v0, v6

    .line 1840373
    return-object v0

    .line 1840374
    :cond_2
    if-eqz v6, :cond_0

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object p1

    if-eqz p1, :cond_0

    .line 1840375
    iget-object p1, v0, LX/35k;->g:Landroid/content/res/Resources;

    const p2, 0x7f0b0044

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    .line 1840376
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object p2

    invoke-static {p2, p1}, LX/1qa;->a(Lcom/facebook/graphql/model/GraphQLMedia;I)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object p1

    .line 1840377
    if-eqz p1, :cond_0

    .line 1840378
    iget-object p0, v0, LX/35k;->b:LX/ApL;

    invoke-virtual {p0, v1}, LX/ApL;->c(LX/1De;)LX/ApK;

    move-result-object p0

    const/4 p2, 0x2

    invoke-virtual {p0, p2}, LX/ApK;->h(I)LX/ApK;

    move-result-object p0

    invoke-static {p1}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object p1

    invoke-virtual {p0, p1}, LX/ApK;->a(Landroid/net/Uri;)LX/ApK;

    move-result-object p0

    invoke-virtual {p0}, LX/1X5;->d()LX/1X1;

    move-result-object p0

    goto :goto_0

    .line 1840379
    :cond_3
    iget-object p0, v0, LX/35k;->f:LX/2sO;

    invoke-virtual {p0, v2}, LX/2sO;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/2y5;

    move-result-object p0

    .line 1840380
    if-eqz p0, :cond_1

    .line 1840381
    const/4 v7, 0x1

    invoke-virtual {p0, v1, v3, v2, v7}, LX/2y5;->b(LX/1De;LX/1PW;Lcom/facebook/feed/rows/core/props/FeedProps;Z)LX/1X1;

    move-result-object v7

    goto :goto_1
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1840390
    invoke-static {}, LX/1dS;->b()V

    .line 1840391
    iget v0, p1, LX/1dQ;->b:I

    .line 1840392
    packed-switch v0, :pswitch_data_0

    .line 1840393
    :goto_0
    return-object v2

    .line 1840394
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 1840395
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1840396
    check-cast v1, LX/C0L;

    .line 1840397
    iget-object v3, p0, LX/C0M;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/35k;

    iget-object p1, v1, LX/C0L;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object p2, v1, LX/C0L;->b:LX/1Po;

    .line 1840398
    iget-object p0, v3, LX/35k;->c:LX/C0D;

    check-cast p2, LX/1Pq;

    invoke-virtual {p0, v0, p1, p2}, LX/C0D;->a(Landroid/view/View;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pq;)V

    .line 1840399
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x3c6bce08
        :pswitch_0
    .end packed-switch
.end method

.method public final c(LX/1De;)LX/C0K;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/C0M",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1840382
    new-instance v1, LX/C0L;

    invoke-direct {v1, p0}, LX/C0L;-><init>(LX/C0M;)V

    .line 1840383
    iget-object v2, p0, LX/C0M;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/C0K;

    .line 1840384
    if-nez v2, :cond_0

    .line 1840385
    new-instance v2, LX/C0K;

    invoke-direct {v2, p0}, LX/C0K;-><init>(LX/C0M;)V

    .line 1840386
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/C0K;->a$redex0(LX/C0K;LX/1De;IILX/C0L;)V

    .line 1840387
    move-object v1, v2

    .line 1840388
    move-object v0, v1

    .line 1840389
    return-object v0
.end method
