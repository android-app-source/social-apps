.class public final LX/ATU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;)V
    .locals 0

    .prologue
    .line 1676095
    iput-object p1, p0, LX/ATU;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x4bf6d54c

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v8

    .line 1676096
    iget-object v0, p0, LX/ATU;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v0, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aF:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbButton;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    instance-of v0, v0, Landroid/graphics/drawable/AnimationDrawable;

    if-eqz v0, :cond_0

    .line 1676097
    iget-object v0, p0, LX/ATU;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v0, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aF:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbButton;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/AnimationDrawable;

    .line 1676098
    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->stop()V

    .line 1676099
    iget-object v0, p0, LX/ATU;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    invoke-static {v0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->F(Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1676100
    iget-object v0, p0, LX/ATU;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v0, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aF:Lcom/facebook/resources/ui/FbButton;

    const v1, 0x7f020424

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setBackgroundResource(I)V

    .line 1676101
    :cond_0
    :goto_0
    iget-object v0, p0, LX/ATU;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v0, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->af:LX/ASg;

    iget-object v1, p0, LX/ATU;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v1, v1, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v1}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/ASg;->a(Lcom/facebook/ipc/media/MediaItem;)F

    move-result v0

    .line 1676102
    iget-object v1, p0, LX/ATU;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget v1, v1, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ai:I

    int-to-float v1, v1

    div-float v0, v1, v0

    float-to-int v3, v0

    .line 1676103
    iget-object v0, p0, LX/ATU;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v0, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ae:LX/9bz;

    iget-object v1, p0, LX/ATU;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v1, v1, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v1}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/media/MediaItem;->d()Lcom/facebook/ipc/media/MediaIdKey;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/media/MediaIdKey;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LX/9bz;->a(Ljava/lang/String;)V

    .line 1676104
    sget-object v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->K:Landroid/graphics/RectF;

    .line 1676105
    iget-object v1, p0, LX/ATU;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v1, v1, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v1}, Lcom/facebook/composer/attachments/ComposerAttachment;->e()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/ATU;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v1, v1, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v1}, Lcom/facebook/composer/attachments/ComposerAttachment;->e()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getCropBox()Lcom/facebook/photos/creativeediting/model/PersistableRect;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 1676106
    iget-object v0, p0, LX/ATU;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v0, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->e()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getCropBox()Lcom/facebook/photos/creativeediting/model/PersistableRect;

    move-result-object v0

    invoke-static {v0}, LX/63w;->c(Lcom/facebook/photos/creativeediting/model/PersistableRect;)Landroid/graphics/RectF;

    move-result-object v0

    move-object v7, v0

    .line 1676107
    :goto_1
    new-instance v1, LX/5Rw;

    iget-object v0, p0, LX/ATU;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v0, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aJ:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;

    invoke-direct {v1, v0}, LX/5Rw;-><init>(Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;)V

    iget-object v0, p0, LX/ATU;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    invoke-static {v0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->B(Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v0

    .line 1676108
    iput-object v0, v1, LX/5Rw;->k:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 1676109
    move-object v0, v1

    .line 1676110
    invoke-virtual {v0}, LX/5Rw;->a()Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;

    move-result-object v4

    .line 1676111
    iget-object v0, p0, LX/ATU;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v0, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ax:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9fD;

    iget-object v1, p0, LX/ATU;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v1, v1, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v1}, Lcom/facebook/composer/attachments/ComposerAttachment;->c()Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, LX/ATU;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget v2, v2, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ai:I

    iget-object v5, p0, LX/ATU;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v5, v5, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->M:LX/9el;

    iget-object v6, p0, LX/ATU;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v6, v6, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aq:LX/0am;

    invoke-virtual {v6}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/AT6;

    invoke-virtual {v6, v7}, LX/AT6;->b(Landroid/graphics/RectF;)Ljava/util/List;

    move-result-object v6

    iget-object v7, p0, LX/ATU;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v7, v7, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->S:Landroid/widget/FrameLayout;

    invoke-static {v7}, LX/9fh;->a(Landroid/view/View;)LX/9fh;

    move-result-object v7

    invoke-virtual/range {v0 .. v7}, LX/9fD;->a(Landroid/net/Uri;IILcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;LX/9el;Ljava/util/List;LX/9fh;)V

    .line 1676112
    const v0, -0x756ab29c

    invoke-static {v0, v8}, LX/02F;->a(II)V

    return-void

    .line 1676113
    :cond_1
    iget-object v0, p0, LX/ATU;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v0, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aF:Lcom/facebook/resources/ui/FbButton;

    const v1, 0x7f020422

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setBackgroundResource(I)V

    goto/16 :goto_0

    :cond_2
    move-object v7, v0

    goto :goto_1
.end method
