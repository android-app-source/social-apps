.class public LX/C7u;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/C7s;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/C7v;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1852036
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/C7u;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/C7v;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1852033
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1852034
    iput-object p1, p0, LX/C7u;->b:LX/0Ot;

    .line 1852035
    return-void
.end method

.method public static a(LX/0QB;)LX/C7u;
    .locals 4

    .prologue
    .line 1852022
    const-class v1, LX/C7u;

    monitor-enter v1

    .line 1852023
    :try_start_0
    sget-object v0, LX/C7u;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1852024
    sput-object v2, LX/C7u;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1852025
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1852026
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1852027
    new-instance v3, LX/C7u;

    const/16 p0, 0x1fa1

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/C7u;-><init>(LX/0Ot;)V

    .line 1852028
    move-object v0, v3

    .line 1852029
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1852030
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C7u;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1852031
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1852032
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 1852004
    check-cast p2, LX/C7t;

    .line 1852005
    iget-object v0, p0, LX/C7u;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/C7v;

    iget-object v1, p2, LX/C7t;->a:Lcom/facebook/graphql/model/GraphQLGroup;

    const/4 p2, 0x6

    const/4 p0, 0x1

    .line 1852006
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v2, v3}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    const/4 v3, 0x2

    invoke-interface {v2, v3}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v2

    const v3, 0x7f0b1d56

    invoke-interface {v2, v3}, LX/1Dh;->G(I)LX/1Dh;

    move-result-object v2

    const v3, 0x7f0b1d56

    invoke-interface {v2, v3}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v2

    const v3, 0x7f020a43

    invoke-interface {v2, v3}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v2

    const/4 v3, 0x3

    const v4, 0x7f0b1d62

    invoke-interface {v2, v3, v4}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v3

    const v4, 0x7f082a06

    invoke-virtual {v3, v4}, LX/1ne;->h(I)LX/1ne;

    move-result-object v3

    const v4, 0x7f0b1d4f

    invoke-virtual {v3, v4}, LX/1ne;->q(I)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, p0}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v3

    sget-object v4, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v3, v4}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const/4 v4, 0x7

    const v5, 0x7f0b1d4e

    invoke-interface {v3, v4, v5}, LX/1Di;->g(II)LX/1Di;

    move-result-object v3

    const v4, 0x7f0b1d52

    invoke-interface {v3, p2, v4}, LX/1Di;->g(II)LX/1Di;

    move-result-object v3

    invoke-interface {v3, p0}, LX/1Di;->b(I)LX/1Di;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/3Ac;->c(LX/1De;)LX/3Ad;

    move-result-object v3

    invoke-virtual {v3, p0}, LX/3Ad;->h(I)LX/3Ad;

    move-result-object v3

    const v4, 0x7f0b1d68

    invoke-virtual {v3, v4}, LX/3Ad;->j(I)LX/3Ad;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v2

    iget-object v3, v0, LX/C7v;->a:LX/C7y;

    const/4 v4, 0x0

    .line 1852007
    new-instance v5, LX/C7x;

    invoke-direct {v5, v3}, LX/C7x;-><init>(LX/C7y;)V

    .line 1852008
    sget-object p0, LX/C7y;->a:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/C7w;

    .line 1852009
    if-nez p0, :cond_0

    .line 1852010
    new-instance p0, LX/C7w;

    invoke-direct {p0}, LX/C7w;-><init>()V

    .line 1852011
    :cond_0
    invoke-static {p0, p1, v4, v4, v5}, LX/C7w;->a$redex0(LX/C7w;LX/1De;IILX/C7x;)V

    .line 1852012
    move-object v5, p0

    .line 1852013
    move-object v4, v5

    .line 1852014
    move-object v3, v4

    .line 1852015
    iget-object v4, v3, LX/C7w;->a:LX/C7x;

    iput-object v1, v4, LX/C7x;->a:Lcom/facebook/graphql/model/GraphQLGroup;

    .line 1852016
    iget-object v4, v3, LX/C7w;->d:Ljava/util/BitSet;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Ljava/util/BitSet;->set(I)V

    .line 1852017
    move-object v3, v3

    .line 1852018
    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const v4, 0x7f0b1d68

    invoke-interface {v3, p2, v4}, LX/1Di;->c(II)LX/1Di;

    move-result-object v3

    const v4, 0x7f0b1d63

    invoke-interface {v3, v4}, LX/1Di;->q(I)LX/1Di;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 1852019
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1852020
    invoke-static {}, LX/1dS;->b()V

    .line 1852021
    const/4 v0, 0x0

    return-object v0
.end method
