.class public final LX/Ahh;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:LX/15i;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "coverPhoto"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:I
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "coverPhoto"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:LX/15i;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "feedAwesomizerProfilePicture"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:I
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "feedAwesomizerProfilePicture"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1703420
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1703421
    return-void
.end method

.method public static a(Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FBFeedAwesomizerProfileListCardProfileFragmentModel;)LX/Ahh;
    .locals 4

    .prologue
    .line 1703445
    new-instance v0, LX/Ahh;

    invoke-direct {v0}, LX/Ahh;-><init>()V

    .line 1703446
    invoke-virtual {p0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FBFeedAwesomizerProfileListCardProfileFragmentModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    iput-object v1, v0, LX/Ahh;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1703447
    invoke-virtual {p0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FBFeedAwesomizerProfileListCardProfileFragmentModel;->k()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    iput-object v2, v0, LX/Ahh;->b:LX/15i;

    iput v1, v0, LX/Ahh;->c:I

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1703448
    invoke-virtual {p0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FBFeedAwesomizerProfileListCardProfileFragmentModel;->l()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_1
    iput-object v2, v0, LX/Ahh;->d:LX/15i;

    iput v1, v0, LX/Ahh;->e:I

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1703449
    invoke-virtual {p0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FBFeedAwesomizerProfileListCardProfileFragmentModel;->m()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/Ahh;->f:Ljava/lang/String;

    .line 1703450
    invoke-virtual {p0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FBFeedAwesomizerProfileListCardProfileFragmentModel;->n()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/Ahh;->g:Ljava/lang/String;

    .line 1703451
    invoke-virtual {p0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FBFeedAwesomizerProfileListCardProfileFragmentModel;->o()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v1

    iput-object v1, v0, LX/Ahh;->h:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 1703452
    return-object v0

    .line 1703453
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 1703454
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FBFeedAwesomizerProfileListCardProfileFragmentModel;
    .locals 11

    .prologue
    const/4 v4, 0x1

    const/4 v10, 0x0

    const/4 v2, 0x0

    .line 1703422
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1703423
    iget-object v1, p0, LX/Ahh;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1703424
    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    iget-object v5, p0, LX/Ahh;->b:LX/15i;

    iget v6, p0, LX/Ahh;->c:I

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const v3, 0x40a9e7c5

    invoke-static {v5, v6, v3}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$DraculaImplementation;

    move-result-object v3

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1703425
    sget-object v5, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v5

    :try_start_1
    iget-object v6, p0, LX/Ahh;->d:LX/15i;

    iget v7, p0, LX/Ahh;->e:I

    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    const v5, 0x4237b83a

    invoke-static {v6, v7, v5}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$DraculaImplementation;

    move-result-object v5

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1703426
    iget-object v6, p0, LX/Ahh;->f:Ljava/lang/String;

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1703427
    iget-object v7, p0, LX/Ahh;->g:Ljava/lang/String;

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1703428
    iget-object v8, p0, LX/Ahh;->h:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    invoke-virtual {v0, v8}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v8

    .line 1703429
    const/4 v9, 0x6

    invoke-virtual {v0, v9}, LX/186;->c(I)V

    .line 1703430
    invoke-virtual {v0, v10, v1}, LX/186;->b(II)V

    .line 1703431
    invoke-virtual {v0, v4, v3}, LX/186;->b(II)V

    .line 1703432
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 1703433
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 1703434
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 1703435
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v8}, LX/186;->b(II)V

    .line 1703436
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 1703437
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1703438
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1703439
    invoke-virtual {v1, v10}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1703440
    new-instance v0, LX/15i;

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1703441
    new-instance v1, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FBFeedAwesomizerProfileListCardProfileFragmentModel;

    invoke-direct {v1, v0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FBFeedAwesomizerProfileListCardProfileFragmentModel;-><init>(LX/15i;)V

    .line 1703442
    return-object v1

    .line 1703443
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 1703444
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
