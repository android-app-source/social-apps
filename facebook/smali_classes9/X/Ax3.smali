.class public LX/Ax3;
.super LX/Aqa;
.source ""


# instance fields
.field public a:LX/0ad;

.field public b:LX/Aww;


# direct methods
.method public constructor <init>(LX/0ad;LX/Aww;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1727502
    invoke-direct {p0}, LX/Aqa;-><init>()V

    .line 1727503
    iput-object p1, p0, LX/Ax3;->a:LX/0ad;

    .line 1727504
    iput-object p2, p0, LX/Ax3;->b:LX/Aww;

    .line 1727505
    return-void
.end method

.method public static b(LX/0QB;)LX/Ax3;
    .locals 3

    .prologue
    .line 1727494
    new-instance v2, LX/Ax3;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v0

    check-cast v0, LX/0ad;

    const-class v1, LX/Aww;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/Aww;

    invoke-direct {v2, v0, v1}, LX/Ax3;-><init>(LX/0ad;LX/Aww;)V

    .line 1727495
    return-object v2
.end method


# virtual methods
.method public final a(LX/1RN;Landroid/content/Context;)LX/AlW;
    .locals 1

    .prologue
    .line 1727496
    new-instance v0, LX/Ax2;

    invoke-direct {v0, p2, p1}, LX/Ax2;-><init>(Landroid/content/Context;LX/1RN;)V

    return-object v0
.end method

.method public final a(LX/2xq;LX/1RN;)V
    .locals 2
    .param p1    # LX/2xq;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1727497
    instance-of v0, p1, LX/2xq;

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1727498
    check-cast p1, LX/2xq;

    .line 1727499
    iget-object v0, p0, LX/Ax3;->b:LX/Aww;

    invoke-virtual {v0, p2}, LX/Aww;->a(LX/1RN;)LX/Awv;

    move-result-object v0

    .line 1727500
    iget-object v1, p1, LX/2xq;->a:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1727501
    return-void
.end method

.method public final b(LX/2xq;LX/1RN;)V
    .locals 2
    .param p1    # LX/2xq;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1727484
    instance-of v0, p1, LX/2xq;

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1727485
    check-cast p1, LX/2xq;

    .line 1727486
    iget-object v0, p1, LX/2xq;->a:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1727487
    return-void
.end method

.method public final e(LX/1RN;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1727488
    invoke-static {p1}, LX/1RN;->a(LX/1RN;)LX/1kK;

    move-result-object v0

    .line 1727489
    instance-of v2, v0, LX/1kW;

    invoke-static {v2}, LX/0PB;->checkState(Z)V

    .line 1727490
    iget-object v2, p0, LX/Ax3;->a:LX/0ad;

    sget-short v3, LX/Awu;->a:S

    invoke-interface {v2, v3, v1}, LX/0ad;->a(SZ)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1727491
    :goto_0
    return v0

    :cond_0
    check-cast v0, LX/1kW;

    .line 1727492
    iget-object v2, v0, LX/1kW;->a:Lcom/facebook/productionprompts/model/ProductionPrompt;

    move-object v0, v2

    .line 1727493
    invoke-virtual {v0}, Lcom/facebook/productionprompts/model/ProductionPrompt;->z()Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeCategoryFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method
