.class public final synthetic LX/Ace;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final synthetic a:[I

.field public static final synthetic b:[I


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1693015
    invoke-static {}, LX/3HY;->values()[LX/3HY;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/Ace;->b:[I

    :try_start_0
    sget-object v0, LX/Ace;->b:[I

    sget-object v1, LX/3HY;->SMALL:LX/3HY;

    invoke-virtual {v1}, LX/3HY;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_6

    :goto_0
    :try_start_1
    sget-object v0, LX/Ace;->b:[I

    sget-object v1, LX/3HY;->REGULAR:LX/3HY;

    invoke-virtual {v1}, LX/3HY;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_5

    :goto_1
    :try_start_2
    sget-object v0, LX/Ace;->b:[I

    sget-object v1, LX/3HY;->EXTRA_SMALL:LX/3HY;

    invoke-virtual {v1}, LX/3HY;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_4

    .line 1693016
    :goto_2
    invoke-static {}, LX/3Hg;->values()[LX/3Hg;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/Ace;->a:[I

    :try_start_3
    sget-object v0, LX/Ace;->a:[I

    sget-object v1, LX/3Hg;->LIVE:LX/3Hg;

    invoke-virtual {v1}, LX/3Hg;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3

    :goto_3
    :try_start_4
    sget-object v0, LX/Ace;->a:[I

    sget-object v1, LX/3Hg;->BROADCAST_COMMERCIAL_BREAK:LX/3Hg;

    invoke-virtual {v1}, LX/3Hg;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_2

    :goto_4
    :try_start_5
    sget-object v0, LX/Ace;->a:[I

    sget-object v1, LX/3Hg;->VIEWER_COMMERCIAL_BREAK_INLINE:LX/3Hg;

    invoke-virtual {v1}, LX/3Hg;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_1

    :goto_5
    :try_start_6
    sget-object v0, LX/Ace;->a:[I

    sget-object v1, LX/3Hg;->VIEWER_COMMERCIAL_BREAK_FULLSCREEN:LX/3Hg;

    invoke-virtual {v1}, LX/3Hg;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_0

    :goto_6
    return-void

    :catch_0
    goto :goto_6

    :catch_1
    goto :goto_5

    :catch_2
    goto :goto_4

    :catch_3
    goto :goto_3

    :catch_4
    goto :goto_2

    :catch_5
    goto :goto_1

    :catch_6
    goto :goto_0
.end method
