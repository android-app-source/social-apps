.class public LX/BOQ;
.super LX/4oD;
.source ""


# instance fields
.field private final a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private final b:Landroid/widget/TextView;

.field private final c:Landroid/widget/TextView;

.field private final d:Landroid/widget/TextView;

.field private final e:Landroid/widget/TextView;

.field private final f:Landroid/view/View;

.field private final g:Landroid/widget/ProgressBar;

.field private h:Z

.field private i:LX/BOP;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1779768
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, LX/BOQ;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1779769
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1779770
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/BOQ;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1779771
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 1779772
    invoke-direct {p0, p1, p2, p3}, LX/4oD;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1779773
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/BOQ;->h:Z

    .line 1779774
    sget-object v0, LX/BOP;->LINK_SHARE:LX/BOP;

    iput-object v0, p0, LX/BOQ;->i:LX/BOP;

    .line 1779775
    const v0, 0x7f030642

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1779776
    const/16 v0, 0x0

    const/4 v1, 0x0

    invoke-interface {p2, v0, v1}, Landroid/util/AttributeSet;->getAttributeBooleanValue(IZ)Z

    move-result v0

    .line 1779777
    if-eqz v0, :cond_0

    .line 1779778
    const v0, 0x7f0d0077

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 1779779
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1779780
    :cond_0
    const v0, 0x7f0d114c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, LX/BOQ;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1779781
    const v0, 0x7f0d1151

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/BOQ;->b:Landroid/widget/TextView;

    .line 1779782
    const v0, 0x7f0d1152

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/BOQ;->c:Landroid/widget/TextView;

    .line 1779783
    const v0, 0x7f0d1153

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/BOQ;->d:Landroid/widget/TextView;

    .line 1779784
    const v0, 0x7f0d114a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/BOQ;->e:Landroid/widget/TextView;

    .line 1779785
    const v0, 0x7f0d114e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/BOQ;->f:Landroid/view/View;

    .line 1779786
    const v0, 0x7f0d114d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, LX/BOQ;->g:Landroid/widget/ProgressBar;

    .line 1779787
    return-void
.end method


# virtual methods
.method public final onMeasure(II)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1779788
    invoke-super {p0, p1, p2}, LX/4oD;->onMeasure(II)V

    .line 1779789
    iget-boolean v0, p0, LX/BOQ;->h:Z

    if-nez v0, :cond_1

    .line 1779790
    :cond_0
    :goto_0
    :sswitch_0
    return-void

    .line 1779791
    :cond_1
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 1779792
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    .line 1779793
    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    .line 1779794
    :sswitch_1
    invoke-virtual {p0}, LX/BOQ;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 1779795
    const v0, 0x7f0b03ee

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    const v3, 0x7f0b03ef

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    mul-int/lit8 v3, v3, 0x2

    add-int/2addr v3, v0

    .line 1779796
    invoke-virtual {p0}, LX/BOQ;->getHeight()I

    move-result v0

    .line 1779797
    if-nez v0, :cond_2

    .line 1779798
    const v0, 0x7f0b00b9

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 1779799
    :cond_2
    add-int/2addr v0, v3

    if-ge v1, v0, :cond_0

    .line 1779800
    invoke-virtual {p0, v4, v4}, LX/BOQ;->setMeasuredDimension(II)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_1
        0x0 -> :sswitch_0
        0x40000000 -> :sswitch_0
    .end sparse-switch
.end method

.method public setHideable(Z)V
    .locals 0

    .prologue
    .line 1779801
    iput-boolean p1, p0, LX/BOQ;->h:Z

    .line 1779802
    return-void
.end method
