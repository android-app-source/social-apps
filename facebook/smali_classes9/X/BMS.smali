.class public LX/BMS;
.super Lcom/facebook/fbui/widget/layout/ImageBlockLayout;
.source ""

# interfaces
.implements LX/1aS;


# instance fields
.field private final j:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private final k:Lcom/facebook/widget/text/BetterTextView;

.field private final l:Lcom/facebook/widget/text/BetterTextView;

.field public final m:Lcom/facebook/widget/text/BetterTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1777341
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/BMS;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1777342
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1777339
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/BMS;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1777340
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/16 v1, 0x10

    .line 1777330
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1777331
    const v0, 0x7f031043

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 1777332
    const v0, 0x7f0d2225

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, LX/BMS;->j:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1777333
    const v0, 0x7f0d0b8d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/BMS;->k:Lcom/facebook/widget/text/BetterTextView;

    .line 1777334
    const v0, 0x7f0d0b8e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/BMS;->l:Lcom/facebook/widget/text/BetterTextView;

    .line 1777335
    const v0, 0x7f0d2709

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/BMS;->m:Lcom/facebook/widget/text/BetterTextView;

    .line 1777336
    invoke-virtual {p0, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setGravity(I)V

    .line 1777337
    invoke-virtual {p0, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailGravity(I)V

    .line 1777338
    return-void
.end method


# virtual methods
.method public final a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 1

    .prologue
    .line 1777328
    iget-object v0, p0, LX/BMS;->j:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1777329
    return-void
.end method

.method public getCallToActionView()Landroid/view/View;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1777327
    iget-object v0, p0, LX/BMS;->m:Lcom/facebook/widget/text/BetterTextView;

    return-object v0
.end method

.method public getPromptSubtitleView()Lcom/facebook/widget/text/BetterTextView;
    .locals 1

    .prologue
    .line 1777325
    iget-object v0, p0, LX/BMS;->l:Lcom/facebook/widget/text/BetterTextView;

    return-object v0
.end method

.method public getPromptTitleView()Lcom/facebook/widget/text/BetterTextView;
    .locals 1

    .prologue
    .line 1777326
    iget-object v0, p0, LX/BMS;->k:Lcom/facebook/widget/text/BetterTextView;

    return-object v0
.end method
