.class public LX/Ay2;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1729325
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1729326
    return-void
.end method

.method public static a(Landroid/os/Parcel;)LX/0Px;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Parcel;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/friendsharing/souvenirs/models/SouvenirItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1729306
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 1729307
    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    .line 1729308
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 1729309
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/Ay0;->valueOf(Ljava/lang/String;)LX/Ay0;

    move-result-object v0

    .line 1729310
    sget-object v4, LX/Ay1;->a:[I

    invoke-virtual {v0}, LX/Ay0;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 1729311
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "readList: invalid type "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1729312
    :pswitch_0
    const-class v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirPhotoItem;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    .line 1729313
    :goto_1
    invoke-virtual {p0, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirItem;

    .line 1729314
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1729315
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1729316
    :pswitch_1
    const-class v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirVideoItem;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    goto :goto_1

    .line 1729317
    :pswitch_2
    const-class v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirBurstItem;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    goto :goto_1

    .line 1729318
    :cond_0
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a(LX/0Px;Landroid/os/Parcel;I)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/friendsharing/souvenirs/models/SouvenirItem;",
            ">;",
            "Landroid/os/Parcel;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 1729319
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1729320
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirItem;

    .line 1729321
    invoke-interface {v0}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirItem;->iq_()LX/Ay0;

    move-result-object v3

    invoke-virtual {v3}, LX/Ay0;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1729322
    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1729323
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1729324
    :cond_0
    return-void
.end method
