.class public LX/C5V;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/followup/components/PoliticalPivotFollowUpComponentSpec;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/C5V",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/graphqlstory/followup/components/PoliticalPivotFollowUpComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1848955
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1848956
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/C5V;->b:LX/0Zi;

    .line 1848957
    iput-object p1, p0, LX/C5V;->a:LX/0Ot;

    .line 1848958
    return-void
.end method

.method public static a(LX/1De;Lcom/facebook/graphql/model/GraphQLPage;Lcom/facebook/graphql/model/GraphQLPoliticalIssueView;)LX/1dQ;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/graphql/model/GraphQLPage;",
            "Lcom/facebook/graphql/model/GraphQLPoliticalIssueView;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1848959
    const v0, 0x46a55f39

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v2, 0x1

    aput-object p2, v1, v2

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)LX/C5V;
    .locals 4

    .prologue
    .line 1848960
    const-class v1, LX/C5V;

    monitor-enter v1

    .line 1848961
    :try_start_0
    sget-object v0, LX/C5V;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1848962
    sput-object v2, LX/C5V;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1848963
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1848964
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1848965
    new-instance v3, LX/C5V;

    const/16 p0, 0x1f6c

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/C5V;-><init>(LX/0Ot;)V

    .line 1848966
    move-object v0, v3

    .line 1848967
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1848968
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C5V;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1848969
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1848970
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 12

    .prologue
    .line 1848971
    check-cast p2, LX/C5U;

    .line 1848972
    iget-object v0, p0, LX/C5V;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/graphqlstory/followup/components/PoliticalPivotFollowUpComponentSpec;

    iget-object v1, p2, LX/C5U;->a:LX/1Pq;

    iget-object v2, p2, LX/C5U;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1848973
    iget-object v3, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 1848974
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v3}, LX/1Wr;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v3

    move-object v9, v3

    check-cast v9, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;

    .line 1848975
    if-eqz v9, :cond_0

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->k()Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotItemsConnection;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->k()Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotItemsConnection;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotItemsConnection;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1848976
    :cond_0
    const/4 v3, 0x0

    .line 1848977
    :goto_0
    move-object v0, v3

    .line 1848978
    return-object v0

    .line 1848979
    :cond_1
    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->k()Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotItemsConnection;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotItemsConnection;->a()LX/0Px;

    move-result-object v3

    .line 1848980
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x0

    invoke-interface {v4, v5}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x1

    const v6, 0x7f0b1ccb

    invoke-interface {v4, v5, v6}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v4

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    .line 1848981
    if-eqz v5, :cond_2

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1848982
    :cond_2
    const/4 v6, 0x0

    .line 1848983
    :goto_1
    move-object v5, v6

    .line 1848984
    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v10

    .line 1848985
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_2
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/graphql/model/GraphQLPoliticalIssueView;

    .line 1848986
    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLPoliticalIssuePivotFeedUnit;->r()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v8

    move-object v3, v0

    move-object v4, p1

    move-object v6, v1

    move-object v7, v2

    invoke-static/range {v3 .. v8}, Lcom/facebook/feedplugins/graphqlstory/followup/components/PoliticalPivotFollowUpComponentSpec;->a(Lcom/facebook/feedplugins/graphqlstory/followup/components/PoliticalPivotFollowUpComponentSpec;LX/1De;Lcom/facebook/graphql/model/GraphQLPoliticalIssueView;LX/1Pq;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLTextWithEntities;)LX/1Dg;

    move-result-object v3

    invoke-interface {v10, v3}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    goto :goto_2

    .line 1848987
    :cond_3
    invoke-interface {v10}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    goto :goto_0

    .line 1848988
    :cond_4
    iget-object v6, v0, Lcom/facebook/feedplugins/graphqlstory/followup/components/PoliticalPivotFollowUpComponentSpec;->f:LX/1vg;

    invoke-virtual {v6, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v6

    const v7, 0x7f0a00e7

    invoke-virtual {v6, v7}, LX/2xv;->j(I)LX/2xv;

    move-result-object v6

    const v7, 0x7f02081c

    invoke-virtual {v6, v7}, LX/2xv;->h(I)LX/2xv;

    move-result-object v6

    invoke-virtual {v6}, LX/1n6;->b()LX/1dc;

    move-result-object v6

    .line 1848989
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v7

    const/4 v8, 0x2

    invoke-interface {v7, v8}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v7

    const/4 v8, 0x1

    invoke-interface {v7, v8}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v7

    const v8, 0x7f0a00d5

    invoke-interface {v7, v8}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v7

    const v8, 0x7f0b1ccc

    invoke-interface {v7, v8}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v7

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v8

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v8

    const v10, 0x7f0b0050

    invoke-virtual {v8, v10}, LX/1ne;->q(I)LX/1ne;

    move-result-object v8

    invoke-virtual {v8}, LX/1X5;->c()LX/1Di;

    move-result-object v8

    const/high16 v10, 0x3f800000    # 1.0f

    invoke-interface {v8, v10}, LX/1Di;->a(F)LX/1Di;

    move-result-object v8

    const/4 v10, 0x4

    const v11, 0x7f0b1ccd

    invoke-interface {v8, v10, v11}, LX/1Di;->g(II)LX/1Di;

    move-result-object v8

    const/4 v10, 0x7

    const v11, 0x7f0b1cce

    invoke-interface {v8, v10, v11}, LX/1Di;->g(II)LX/1Di;

    move-result-object v8

    sget-object v10, Lcom/facebook/feedplugins/graphqlstory/followup/components/PoliticalPivotFollowUpComponentSpec;->b:Landroid/util/SparseArray;

    invoke-interface {v8, v10}, LX/1Di;->a(Landroid/util/SparseArray;)LX/1Di;

    move-result-object v8

    invoke-interface {v7, v8}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v7

    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v8

    invoke-virtual {v8, v6}, LX/1o5;->a(LX/1dc;)LX/1o5;

    move-result-object v6

    invoke-virtual {v6}, LX/1X5;->c()LX/1Di;

    move-result-object v6

    const/16 v8, 0x8

    const v10, 0x7f0b1cd5

    invoke-interface {v6, v8, v10}, LX/1Di;->g(II)LX/1Di;

    move-result-object v6

    .line 1848990
    const v8, 0x19a6c384

    const/4 v10, 0x0

    invoke-static {p1, v8, v10}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v8

    move-object v8, v8

    .line 1848991
    invoke-interface {v6, v8}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v6

    invoke-interface {v7, v6}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v6

    invoke-interface {v6}, LX/1Di;->k()LX/1Dg;

    move-result-object v6

    goto/16 :goto_1
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 1848992
    invoke-static {}, LX/1dS;->b()V

    .line 1848993
    iget v0, p1, LX/1dQ;->b:I

    .line 1848994
    sparse-switch v0, :sswitch_data_0

    .line 1848995
    :goto_0
    return-object v4

    .line 1848996
    :sswitch_0
    check-cast p2, LX/3Ae;

    .line 1848997
    iget-object v2, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v0, p1, LX/1dQ;->c:[Ljava/lang/Object;

    aget-object v0, v0, v1

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    iget-object v1, p1, LX/1dQ;->c:[Ljava/lang/Object;

    aget-object v1, v1, v3

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPoliticalIssueView;

    iget-object v3, p1, LX/1dQ;->a:LX/1X1;

    .line 1848998
    check-cast v3, LX/C5U;

    .line 1848999
    iget-object v5, p0, LX/C5V;->a:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/graphqlstory/followup/components/PoliticalPivotFollowUpComponentSpec;

    iget-object v6, v3, LX/C5U;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1849000
    const p1, 0x25d6af

    const/4 p2, 0x1

    new-array p2, p2, [Ljava/lang/Object;

    const/4 p0, 0x0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v3

    aput-object v3, p2, p0

    invoke-static {p1, p2}, LX/1nG;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 1849001
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result p2

    if-eqz p2, :cond_2

    .line 1849002
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->aE()Ljava/lang/String;

    move-result-object p1

    move-object p2, p1

    .line 1849003
    :goto_1
    iget-object p0, v5, Lcom/facebook/feedplugins/graphqlstory/followup/components/PoliticalPivotFollowUpComponentSpec;->j:LX/C5c;

    .line 1849004
    iget-object p1, v6, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object p1, p1

    .line 1849005
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1849006
    sget-object v3, LX/C5b;->OPEN_POLITICIAN_PAGE:LX/C5b;

    invoke-static {p0, v3, p1, v0, v1}, LX/C5c;->a(LX/C5c;LX/C5b;Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/model/GraphQLPage;Lcom/facebook/graphql/model/GraphQLPoliticalIssueView;)V

    .line 1849007
    iget-object p1, v5, Lcom/facebook/feedplugins/graphqlstory/followup/components/PoliticalPivotFollowUpComponentSpec;->h:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-interface {p1, p0, p2}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 1849008
    goto :goto_0

    .line 1849009
    :sswitch_1
    check-cast p2, LX/3Ae;

    .line 1849010
    iget-object v2, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v0, p1, LX/1dQ;->c:[Ljava/lang/Object;

    aget-object v0, v0, v1

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    iget-object v1, p1, LX/1dQ;->c:[Ljava/lang/Object;

    aget-object v1, v1, v3

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPoliticalIssueView;

    iget-object v3, p1, LX/1dQ;->a:LX/1X1;

    .line 1849011
    check-cast v3, LX/C5U;

    .line 1849012
    iget-object v5, p0, LX/C5V;->a:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/graphqlstory/followup/components/PoliticalPivotFollowUpComponentSpec;

    iget-object v6, v3, LX/C5U;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1849013
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_3

    .line 1849014
    :cond_0
    :goto_2
    goto/16 :goto_0

    .line 1849015
    :sswitch_2
    check-cast p2, LX/3Ae;

    .line 1849016
    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1849017
    check-cast v1, LX/C5U;

    .line 1849018
    iget-object v2, p0, LX/C5V;->a:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/feedplugins/graphqlstory/followup/components/PoliticalPivotFollowUpComponentSpec;

    iget-object v3, v1, LX/C5U;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v5, v1, LX/C5U;->a:LX/1Pq;

    .line 1849019
    iget-object p0, v2, Lcom/facebook/feedplugins/graphqlstory/followup/components/PoliticalPivotFollowUpComponentSpec;->j:LX/C5c;

    .line 1849020
    iget-object v6, v3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v6, v6

    .line 1849021
    check-cast v6, Lcom/facebook/graphql/model/GraphQLStory;

    const/4 v1, 0x0

    .line 1849022
    sget-object v0, LX/C5b;->HIDE_PIVOT:LX/C5b;

    invoke-static {p0, v0, v6, v1, v1}, LX/C5c;->a(LX/C5c;LX/C5b;Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/model/GraphQLPage;Lcom/facebook/graphql/model/GraphQLPoliticalIssueView;)V

    .line 1849023
    iget-object v6, v3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v6, v6

    .line 1849024
    check-cast v6, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 1849025
    iget-object p0, v2, Lcom/facebook/feedplugins/graphqlstory/followup/components/PoliticalPivotFollowUpComponentSpec;->d:LX/3iV;

    .line 1849026
    iget-object v6, v3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v6, v6

    .line 1849027
    check-cast v6, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v6

    const/4 v0, 0x0

    invoke-virtual {p0, v6, v0}, LX/3iV;->a(Ljava/lang/String;Lcom/facebook/graphql/model/FeedUnit;)V

    .line 1849028
    const/4 v6, 0x1

    new-array v6, v6, [Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 p0, 0x0

    aput-object v3, v6, p0

    invoke-interface {v5, v6}, LX/1Pq;->a([Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1849029
    :cond_1
    goto/16 :goto_0

    :cond_2
    move-object p2, p1

    goto/16 :goto_1

    .line 1849030
    :cond_3
    iget-object p0, v5, Lcom/facebook/feedplugins/graphqlstory/followup/components/PoliticalPivotFollowUpComponentSpec;->j:LX/C5c;

    .line 1849031
    iget-object p1, v6, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object p1, p1

    .line 1849032
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1849033
    sget-object v3, LX/C5b;->SEE_POSITION_CLICK:LX/C5b;

    invoke-static {p0, v3, p1, v0, v1}, LX/C5c;->a(LX/C5c;LX/C5b;Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/model/GraphQLPage;Lcom/facebook/graphql/model/GraphQLPoliticalIssueView;)V

    .line 1849034
    iget-object p1, v5, Lcom/facebook/feedplugins/graphqlstory/followup/components/PoliticalPivotFollowUpComponentSpec;->h:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p0

    .line 1849035
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_5

    .line 1849036
    :cond_4
    const/4 v3, 0x0

    .line 1849037
    :goto_3
    move-object v3, v3

    .line 1849038
    invoke-interface {p1, p0, v3}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(Landroid/content/Context;Ljava/lang/String;)Z

    goto :goto_2

    .line 1849039
    :cond_5
    iget-object v3, v5, Lcom/facebook/feedplugins/graphqlstory/followup/components/PoliticalPivotFollowUpComponentSpec;->e:LX/1eA;

    invoke-virtual {v3}, LX/1eA;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_6

    iget-object v3, v5, Lcom/facebook/feedplugins/graphqlstory/followup/components/PoliticalPivotFollowUpComponentSpec;->e:LX/1eA;

    invoke-virtual {v3}, LX/1eA;->c()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 1849040
    :cond_6
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v2, "https://www.facebook.com/pg/"

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v2, "/issues"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_3

    .line 1849041
    :cond_7
    iget-object v3, v5, Lcom/facebook/feedplugins/graphqlstory/followup/components/PoliticalPivotFollowUpComponentSpec;->e:LX/1eA;

    invoke-virtual {v3}, LX/1eA;->b()Ljava/lang/String;

    move-result-object v3

    iget-object v2, v5, Lcom/facebook/feedplugins/graphqlstory/followup/components/PoliticalPivotFollowUpComponentSpec;->e:LX/1eA;

    invoke-virtual {v2}, LX/1eA;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v2, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    goto :goto_3

    :sswitch_data_0
    .sparse-switch
        0x19a6c384 -> :sswitch_2
        0x46a55f39 -> :sswitch_0
        0x46a57ddc -> :sswitch_1
    .end sparse-switch
.end method
