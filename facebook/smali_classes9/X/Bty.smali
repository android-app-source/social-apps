.class public final LX/Bty;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/video/protocol/VideoQueryModels$VideoCreationStoryFromVideoQueryModel;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;)V
    .locals 0

    .prologue
    .line 1829656
    iput-object p1, p0, LX/Bty;->a:Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 2
    .param p1    # Lcom/facebook/graphql/executor/GraphQLResult;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/video/protocol/VideoQueryModels$VideoCreationStoryFromVideoQueryModel;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1829657
    if-eqz p1, :cond_0

    .line 1829658
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1829659
    if-eqz v0, :cond_0

    .line 1829660
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1829661
    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1829662
    :cond_0
    :goto_0
    return-void

    .line 1829663
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1829664
    check-cast v0, Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/protocol/VideoQueryModels$VideoCreationStoryFromVideoQueryModel;

    invoke-virtual {v0}, Lcom/facebook/video/protocol/VideoQueryModels$VideoCreationStoryFromVideoQueryModel;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 1829665
    iget-object v1, p0, LX/Bty;->a:Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

    iget-object v1, v1, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->al:LX/AjN;

    invoke-interface {v1, v0}, LX/AjN;->a(Lcom/facebook/graphql/model/GraphQLStory;)V

    goto :goto_0
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 1829666
    return-void
.end method

.method public final synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1829667
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    invoke-direct {p0, p1}, LX/Bty;->a(Lcom/facebook/graphql/executor/GraphQLResult;)V

    return-void
.end method
