.class public LX/CfP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0zT;


# instance fields
.field private final c:Ljava/lang/String;
    .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
    .end annotation
.end field

.field private final d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
        .end annotation
    .end param

    .prologue
    .line 1925937
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1925938
    iput-object p1, p0, LX/CfP;->c:Ljava/lang/String;

    .line 1925939
    iput-object p2, p0, LX/CfP;->d:Ljava/lang/String;

    .line 1925940
    return-void
.end method


# virtual methods
.method public final a(LX/0zO;LX/0w5;LX/0t2;)Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0zO;",
            "LX/0w5",
            "<*>;",
            "LX/0t2;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 1925941
    :try_start_0
    invoke-virtual {p1}, LX/0zO;->d()LX/0w7;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "102"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "95"

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {p3, v0, v1}, LX/0t2;->a(LX/0w7;Ljava/util/Collection;)LX/1l6;

    move-result-object v0

    .line 1925942
    iget-object v1, p0, LX/CfP;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/1l6;->a(Ljava/lang/Object;)LX/1l6;

    move-result-object v0

    iget-object v1, p0, LX/CfP;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/1l6;->a(Ljava/lang/Object;)LX/1l6;

    move-result-object v0

    invoke-virtual {v0}, LX/1l6;->hashCode()I

    move-result v0

    .line 1925943
    iget-object v1, p1, LX/0zO;->m:LX/0gW;

    move-object v1, v1

    .line 1925944
    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v1, p2, v0}, LX/0t2;->a(LX/0gW;LX/0w5;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch LX/28F; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 1925945
    :catch_0
    move-exception v0

    .line 1925946
    invoke-static {v0}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method
