.class public LX/AnW;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:I
    .annotation build Landroid/support/annotation/StyleRes;
    .end annotation
.end field

.field public static final b:I
    .annotation build Landroid/support/annotation/StyleRes;
    .end annotation
.end field

.field public static final c:I
    .annotation build Landroid/support/annotation/StyleRes;
    .end annotation
.end field

.field private static i:LX/0Xm;


# instance fields
.field public final d:LX/Anb;

.field public final e:LX/Ana;

.field private final f:LX/Anc;

.field public final g:LX/1vg;

.field public final h:LX/AnS;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1712609
    const v0, 0x7f0e0129

    sput v0, LX/AnW;->a:I

    .line 1712610
    const v0, 0x7f0e0133

    sput v0, LX/AnW;->b:I

    .line 1712611
    const v0, 0x7f0e012d

    sput v0, LX/AnW;->c:I

    return-void
.end method

.method public constructor <init>(LX/Anb;LX/Ana;LX/Anc;LX/1vg;LX/AnS;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1712649
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1712650
    iput-object p1, p0, LX/AnW;->d:LX/Anb;

    .line 1712651
    iput-object p2, p0, LX/AnW;->e:LX/Ana;

    .line 1712652
    iput-object p3, p0, LX/AnW;->f:LX/Anc;

    .line 1712653
    iput-object p4, p0, LX/AnW;->g:LX/1vg;

    .line 1712654
    iput-object p5, p0, LX/AnW;->h:LX/AnS;

    .line 1712655
    return-void
.end method

.method public static a(LX/0QB;)LX/AnW;
    .locals 9

    .prologue
    .line 1712638
    const-class v1, LX/AnW;

    monitor-enter v1

    .line 1712639
    :try_start_0
    sget-object v0, LX/AnW;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1712640
    sput-object v2, LX/AnW;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1712641
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1712642
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1712643
    new-instance v3, LX/AnW;

    invoke-static {v0}, LX/Anb;->a(LX/0QB;)LX/Anb;

    move-result-object v4

    check-cast v4, LX/Anb;

    invoke-static {v0}, LX/Ana;->a(LX/0QB;)LX/Ana;

    move-result-object v5

    check-cast v5, LX/Ana;

    invoke-static {v0}, LX/Anc;->a(LX/0QB;)LX/Anc;

    move-result-object v6

    check-cast v6, LX/Anc;

    invoke-static {v0}, LX/1vg;->a(LX/0QB;)LX/1vg;

    move-result-object v7

    check-cast v7, LX/1vg;

    invoke-static {v0}, LX/AnS;->a(LX/0QB;)LX/AnS;

    move-result-object v8

    check-cast v8, LX/AnS;

    invoke-direct/range {v3 .. v8}, LX/AnW;-><init>(LX/Anb;LX/Ana;LX/Anc;LX/1vg;LX/AnS;)V

    .line 1712644
    move-object v0, v3

    .line 1712645
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1712646
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/AnW;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1712647
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1712648
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static c(LX/AnW;Landroid/view/View;Lcom/facebook/graphql/model/GraphQLStoryActionLink;LX/162;Z)V
    .locals 3

    .prologue
    .line 1712634
    iget-object v1, p0, LX/AnW;->d:LX/Anb;

    if-eqz p4, :cond_0

    const-string v0, "composer"

    :goto_0
    const-string v2, "background"

    invoke-virtual {v1, p3, v0, v2}, LX/Anb;->a(LX/162;Ljava/lang/String;Ljava/lang/String;)V

    .line 1712635
    iget-object v0, p0, LX/AnW;->f:LX/Anc;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bc()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/Anc;->a(Ljava/lang/String;Landroid/content/Context;)V

    .line 1712636
    return-void

    .line 1712637
    :cond_0
    const-string v0, "native_newsfeed"

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1De;Lcom/facebook/graphql/model/GraphQLStoryActionLink;)LX/1Dg;
    .locals 13
    .param p2    # Lcom/facebook/graphql/model/GraphQLStoryActionLink;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x3

    .line 1712612
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    const/4 v1, 0x2

    invoke-interface {v0, v1}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v0

    const/4 v1, 0x6

    invoke-interface {v0, v1, v2}, LX/1Dh;->r(II)LX/1Dh;

    move-result-object v0

    const/4 v1, 0x5

    invoke-interface {v0, v2, v1}, LX/1Dh;->r(II)LX/1Dh;

    move-result-object v0

    const v1, -0x90808

    invoke-interface {v0, v1}, LX/1Dh;->W(I)LX/1Dh;

    move-result-object v0

    const/16 v8, 0x1c

    .line 1712613
    iget-object v3, p0, LX/AnW;->h:LX/AnS;

    .line 1712614
    iget-object v9, v3, LX/AnS;->a:LX/0W3;

    sget-wide v11, LX/0X5;->ku:J

    invoke-interface {v9, v11, v12}, LX/0W4;->f(J)Ljava/lang/String;

    move-result-object v9

    move-object v3, v9

    .line 1712615
    const-string v4, "gray"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    .line 1712616
    if-eqz v5, :cond_0

    const v3, -0x686869

    move v4, v3

    .line 1712617
    :goto_0
    if-eqz v5, :cond_1

    const v3, 0x7f0207c4

    .line 1712618
    :goto_1
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    const/4 v6, 0x0

    invoke-interface {v5, v6}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v5

    const/4 v6, 0x1

    invoke-interface {v5, v6}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v5

    const/4 v6, 0x7

    const/16 v7, 0xa

    invoke-interface {v5, v6, v7}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v5

    const/4 v6, 0x4

    const/16 v7, 0xc

    invoke-interface {v5, v6, v7}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v5

    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v6

    iget-object v7, p0, LX/AnW;->g:LX/1vg;

    invoke-virtual {v7, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v7

    invoke-virtual {v7, v3}, LX/2xv;->h(I)LX/2xv;

    move-result-object v3

    invoke-virtual {v3, v4}, LX/2xv;->i(I)LX/2xv;

    move-result-object v3

    invoke-virtual {v6, v3}, LX/1o5;->a(LX/1n6;)LX/1o5;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    invoke-interface {v3, v8}, LX/1Di;->j(I)LX/1Di;

    move-result-object v3

    invoke-interface {v3, v8}, LX/1Di;->r(I)LX/1Di;

    move-result-object v3

    invoke-interface {v5, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/AnV;->d(LX/1De;)LX/1dQ;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v1, v3

    .line 1712619
    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v0

    const/4 v2, 0x0

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1712620
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1, v5}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v1

    const/4 v3, 0x7

    const/16 v4, 0xa

    invoke-interface {v1, v3, v4}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v1

    const/4 v3, 0x4

    const/16 v4, 0x8

    invoke-interface {v1, v3, v4}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v1

    invoke-interface {v1, v6}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v1

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-interface {v1, v3}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v3

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aZ()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    move-object v1, v2

    :goto_2
    invoke-interface {v3, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bN()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    :goto_3
    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    invoke-static {p1}, LX/AnV;->d(LX/1De;)LX/1dQ;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    move-object v1, v1

    .line 1712621
    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v0

    .line 1712622
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bO()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bO()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_4

    const/16 p2, 0x14

    .line 1712623
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v1

    const/4 v2, 0x4

    const/16 v3, 0x10

    invoke-interface {v1, v2, v3}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v1

    const/4 v2, 0x5

    const/4 v3, 0x6

    invoke-interface {v1, v2, v3}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v1, v2}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v1

    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v2

    iget-object v3, p0, LX/AnW;->g:LX/1vg;

    invoke-virtual {v3, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v3

    const v4, 0x7f020725

    invoke-virtual {v3, v4}, LX/2xv;->h(I)LX/2xv;

    move-result-object v3

    const v4, -0x6f6b64

    invoke-virtual {v3, v4}, LX/2xv;->i(I)LX/2xv;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/1o5;->a(LX/1n6;)LX/1o5;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    invoke-interface {v2, p2}, LX/1Di;->j(I)LX/1Di;

    move-result-object v2

    invoke-interface {v2, p2}, LX/1Di;->r(I)LX/1Di;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    const v2, 0x7f0828a8

    invoke-interface {v1, v2}, LX/1Dh;->Y(I)LX/1Dh;

    move-result-object v1

    .line 1712624
    const v2, -0x5407e3a4

    const/4 v3, 0x0

    invoke-static {p1, v2, v3}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v2

    move-object v2, v2

    .line 1712625
    invoke-interface {v1, v2}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    move-object v1, v1

    .line 1712626
    :goto_4
    move-object v1, v1

    .line 1712627
    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    return-object v0

    .line 1712628
    :cond_0
    const/4 v3, -0x1

    move v4, v3

    goto/16 :goto_0

    .line 1712629
    :cond_1
    const v3, 0x7f020f16

    goto/16 :goto_1

    :cond_2
    sget v1, LX/AnW;->a:I

    invoke-static {p1, v5, v1}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v1

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aZ()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v1

    const/4 v4, 0x3

    invoke-virtual {v1, v4}, LX/1ne;->j(I)LX/1ne;

    move-result-object v1

    sget-object v4, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v1, v4}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v1

    invoke-virtual {v1, v5}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aZ()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v4}, LX/1Di;->a(Ljava/lang/CharSequence;)LX/1Di;

    move-result-object v1

    goto/16 :goto_2

    :cond_3
    sget v2, LX/AnW;->b:I

    invoke-static {p1, v5, v2}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v2

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bN()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, v6}, LX/1ne;->j(I)LX/1ne;

    move-result-object v2

    sget-object v3, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v2, v3}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, v5}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bN()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Di;->a(Ljava/lang/CharSequence;)LX/1Di;

    move-result-object v2

    goto/16 :goto_3

    :cond_4
    const/4 v3, 0x0

    .line 1712630
    sget v1, LX/AnW;->c:I

    invoke-static {p1, v3, v1}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v1

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bI()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v1

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, LX/1ne;->j(I)LX/1ne;

    move-result-object v1

    sget-object v2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v1, v2}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v1

    invoke-virtual {v1, v3}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v1

    sget-object v2, LX/1nd;->CENTER:LX/1nd;

    invoke-virtual {v1, v2}, LX/1ne;->a(LX/1nd;)LX/1ne;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    const/4 v2, 0x7

    const/16 v3, 0xa

    invoke-interface {v1, v2, v3}, LX/1Di;->h(II)LX/1Di;

    move-result-object v1

    const/4 v2, 0x4

    const/16 v3, 0x8

    invoke-interface {v1, v2, v3}, LX/1Di;->h(II)LX/1Di;

    move-result-object v1

    const/4 v2, 0x5

    const/16 v3, 0xc

    invoke-interface {v1, v2, v3}, LX/1Di;->h(II)LX/1Di;

    move-result-object v1

    const/16 v2, 0x64

    invoke-interface {v1, v2}, LX/1Di;->n(I)LX/1Di;

    move-result-object v1

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bI()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1Di;->a(Ljava/lang/CharSequence;)LX/1Di;

    move-result-object v1

    .line 1712631
    const v2, -0x5407dcfb

    const/4 v3, 0x0

    invoke-static {p1, v2, v3}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v2

    move-object v2, v2

    .line 1712632
    invoke-interface {v1, v2}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    move-object v1, v1

    .line 1712633
    goto/16 :goto_4
.end method
