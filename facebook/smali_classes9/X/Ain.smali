.class public final LX/Ain;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<TT;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;)V
    .locals 0

    .prologue
    .line 1706841
    iput-object p1, p0, LX/Ain;->a:Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1706853
    iget-object v0, p0, LX/Ain;->a:Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;

    .line 1706854
    iget-object v1, v0, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->k:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    if-nez v1, :cond_1

    .line 1706855
    :cond_0
    :goto_0
    return-void

    .line 1706856
    :cond_1
    iget-object v1, v0, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->k:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    const p1, 0x7f08006f

    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    new-instance p1, LX/Aim;

    invoke-direct {p1, v0}, LX/Aim;-><init>(Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;)V

    invoke-virtual {v1, p0, p1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a(Ljava/lang/String;LX/1DI;)V

    goto :goto_0
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 7
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1706842
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1706843
    iget-object v0, p0, LX/Ain;->a:Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;

    .line 1706844
    iget-object v2, v0, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->k:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v2}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->b()V

    .line 1706845
    iget-object v2, v0, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->i:LX/Aiv;

    .line 1706846
    iget-object v3, v2, LX/Aiv;->b:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v3

    iget-wide v5, v2, LX/Aiv;->g:J

    sub-long/2addr v3, v5

    iput-wide v3, v2, LX/Aiv;->f:J

    .line 1706847
    iget-object v2, v0, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->d:LX/3wu;

    invoke-virtual {v2}, LX/1P1;->n()I

    move-result v2

    iput v2, v0, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->g:I

    .line 1706848
    if-nez p1, :cond_0

    .line 1706849
    iget-object v0, p0, LX/Ain;->a:Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;

    iget-object v0, v0, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->a:Lcom/facebook/resources/ui/FbTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1706850
    :goto_0
    return-void

    .line 1706851
    :cond_0
    iget-object v0, p0, LX/Ain;->a:Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;

    iget-object v0, v0, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->e:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    const/4 v1, -0x1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1706852
    iget-object v0, p0, LX/Ain;->a:Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;

    invoke-virtual {v0, p1}, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->a(Lcom/facebook/graphql/executor/GraphQLResult;)V

    goto :goto_0
.end method
