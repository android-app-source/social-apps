.class public final LX/CbT;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;)V
    .locals 0

    .prologue
    .line 1919551
    iput-object p1, p0, LX/CbT;->a:Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;B)V
    .locals 0

    .prologue
    .line 1919579
    invoke-direct {p0, p1}, LX/CbT;-><init>(Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;)V

    return-void
.end method


# virtual methods
.method public final onDoubleTapEvent(Landroid/view/MotionEvent;)Z
    .locals 9

    .prologue
    .line 1919566
    iget-object v0, p0, LX/CbT;->a:Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;

    invoke-virtual {v0}, Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;->getZoomableController()LX/5ue;

    move-result-object v1

    .line 1919567
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    .line 1919568
    new-instance v4, Landroid/graphics/PointF;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-direct {v4, v0, v2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1919569
    invoke-virtual {v1, v4}, LX/5ua;->a(Landroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v3

    .line 1919570
    iget v0, v1, LX/5ua;->i:F

    move v0, v0

    .line 1919571
    iget v2, v1, LX/5ua;->j:F

    move v2, v2

    .line 1919572
    add-float/2addr v0, v2

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v0, v2

    .line 1919573
    invoke-virtual {v1}, LX/5ua;->m()F

    move-result v2

    cmpl-float v0, v2, v0

    if-lez v0, :cond_1

    .line 1919574
    iget v0, v1, LX/5ua;->i:F

    move v2, v0

    .line 1919575
    :goto_0
    const/4 v5, 0x7

    const-wide/16 v6, 0x190

    const/4 v8, 0x0

    invoke-virtual/range {v1 .. v8}, LX/5ub;->a(FLandroid/graphics/PointF;Landroid/graphics/PointF;IJLjava/lang/Runnable;)V

    .line 1919576
    :cond_0
    const/4 v0, 0x0

    return v0

    .line 1919577
    :cond_1
    iget v0, v1, LX/5ua;->j:F

    move v2, v0

    .line 1919578
    goto :goto_0
.end method

.method public final onLongPress(Landroid/view/MotionEvent;)V
    .locals 4

    .prologue
    .line 1919561
    new-instance v1, Landroid/graphics/PointF;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-direct {v1, v0, v2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1919562
    iget-object v0, p0, LX/CbT;->a:Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;

    invoke-virtual {v0}, Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;->getZoomableController()LX/5ue;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/5ua;->a(Landroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v2

    .line 1919563
    iget-object v0, p0, LX/CbT;->a:Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7US;

    .line 1919564
    invoke-interface {v0, v1, v2}, LX/7US;->b(Landroid/graphics/PointF;Landroid/graphics/PointF;)V

    goto :goto_0

    .line 1919565
    :cond_0
    return-void
.end method

.method public final onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    .line 1919556
    new-instance v1, Landroid/graphics/PointF;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-direct {v1, v0, v2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1919557
    iget-object v0, p0, LX/CbT;->a:Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;

    invoke-virtual {v0}, Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;->getZoomableController()LX/5ue;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/5ua;->a(Landroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v2

    .line 1919558
    iget-object v0, p0, LX/CbT;->a:Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7US;

    .line 1919559
    invoke-interface {v0, v1, v2}, LX/7US;->a(Landroid/graphics/PointF;Landroid/graphics/PointF;)V

    goto :goto_0

    .line 1919560
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public final onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    .line 1919552
    new-instance v0, Landroid/graphics/PointF;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 1919553
    iget-object v1, p0, LX/CbT;->a:Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;

    invoke-virtual {v1}, Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;->getZoomableController()LX/5ue;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/5ua;->a(Landroid/graphics/PointF;)Landroid/graphics/PointF;

    .line 1919554
    iget-object v0, p0, LX/CbT;->a:Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_0

    .line 1919555
    :cond_0
    const/4 v0, 0x0

    return v0
.end method
