.class public final LX/BYH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$FetchTopComposerGroupsModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Ajt;

.field public final synthetic b:LX/BYI;


# direct methods
.method public constructor <init>(LX/BYI;LX/Ajt;)V
    .locals 0

    .prologue
    .line 1795372
    iput-object p1, p0, LX/BYH;->b:LX/BYI;

    iput-object p2, p0, LX/BYH;->a:LX/Ajt;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 1795373
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 11

    .prologue
    .line 1795374
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1795375
    iget-object v0, p0, LX/BYH;->a:LX/Ajt;

    if-nez v0, :cond_0

    .line 1795376
    :goto_0
    return-void

    .line 1795377
    :cond_0
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1795378
    if-eqz v0, :cond_1

    .line 1795379
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1795380
    check-cast v0, Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$FetchTopComposerGroupsModel;

    invoke-virtual {v0}, Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$FetchTopComposerGroupsModel;->a()Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$FetchTopComposerGroupsModel$AccountUserModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1795381
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1795382
    check-cast v0, Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$FetchTopComposerGroupsModel;

    invoke-virtual {v0}, Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$FetchTopComposerGroupsModel;->a()Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$FetchTopComposerGroupsModel$AccountUserModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$FetchTopComposerGroupsModel$AccountUserModel;->a()Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$FetchTopComposerGroupsModel$AccountUserModel$GroupsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1795383
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1795384
    check-cast v0, Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$FetchTopComposerGroupsModel;

    invoke-virtual {v0}, Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$FetchTopComposerGroupsModel;->a()Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$FetchTopComposerGroupsModel$AccountUserModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$FetchTopComposerGroupsModel$AccountUserModel;->a()Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$FetchTopComposerGroupsModel$AccountUserModel$GroupsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$FetchTopComposerGroupsModel$AccountUserModel$GroupsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1795385
    :cond_1
    iget-object v0, p0, LX/BYH;->a:LX/Ajt;

    .line 1795386
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 1795387
    invoke-virtual {v0, v1}, LX/Ajt;->a(LX/0Px;)V

    goto :goto_0

    .line 1795388
    :cond_2
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1795389
    check-cast v0, Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$FetchTopComposerGroupsModel;

    invoke-virtual {v0}, Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$FetchTopComposerGroupsModel;->a()Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$FetchTopComposerGroupsModel$AccountUserModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$FetchTopComposerGroupsModel$AccountUserModel;->a()Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$FetchTopComposerGroupsModel$AccountUserModel$GroupsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$FetchTopComposerGroupsModel$AccountUserModel$GroupsModel;->a()LX/0Px;

    move-result-object v2

    .line 1795390
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1795391
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_5

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$FetchTopComposerGroupsModel$AccountUserModel$GroupsModel$NodesModel;

    .line 1795392
    new-instance v5, LX/BYG;

    invoke-virtual {v0}, Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$FetchTopComposerGroupsModel$AccountUserModel$GroupsModel$NodesModel;->l()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0}, Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$FetchTopComposerGroupsModel$AccountUserModel$GroupsModel$NodesModel;->m()Ljava/lang/String;

    move-result-object v7

    .line 1795393
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 1795394
    invoke-virtual {v0}, Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$FetchTopComposerGroupsModel$AccountUserModel$GroupsModel$NodesModel;->j()LX/1vs;

    move-result-object v10

    iget v10, v10, LX/1vs;->b:I

    if-nez v10, :cond_6

    move v10, v8

    :goto_2
    if-eqz v10, :cond_8

    :cond_3
    :goto_3
    if-eqz v8, :cond_9

    .line 1795395
    const/4 v8, 0x0

    .line 1795396
    :goto_4
    move-object v8, v8

    .line 1795397
    if-nez v8, :cond_4

    .line 1795398
    invoke-static {v0}, LX/BYI;->c(Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$FetchTopComposerGroupsModel$AccountUserModel$GroupsModel$NodesModel;)Landroid/net/Uri;

    move-result-object v8

    .line 1795399
    :cond_4
    move-object v0, v8

    .line 1795400
    invoke-direct {v5, v6, v7, v0}, LX/BYG;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v3, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1795401
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1795402
    :cond_5
    iget-object v0, p0, LX/BYH;->a:LX/Ajt;

    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Ajt;->a(LX/0Px;)V

    goto/16 :goto_0

    .line 1795403
    :cond_6
    invoke-virtual {v0}, Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$FetchTopComposerGroupsModel$AccountUserModel$GroupsModel$NodesModel;->j()LX/1vs;

    move-result-object v10

    iget-object p1, v10, LX/1vs;->a:LX/15i;

    iget v10, v10, LX/1vs;->b:I

    .line 1795404
    invoke-virtual {p1, v10, v9}, LX/15i;->g(II)I

    move-result v10

    if-nez v10, :cond_7

    move v10, v8

    goto :goto_2

    :cond_7
    move v10, v9

    goto :goto_2

    .line 1795405
    :cond_8
    invoke-virtual {v0}, Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$FetchTopComposerGroupsModel$AccountUserModel$GroupsModel$NodesModel;->j()LX/1vs;

    move-result-object v10

    iget-object p1, v10, LX/1vs;->a:LX/15i;

    iget v10, v10, LX/1vs;->b:I

    .line 1795406
    invoke-virtual {p1, v10, v9}, LX/15i;->g(II)I

    move-result v10

    invoke-virtual {p1, v10, v9}, LX/15i;->g(II)I

    move-result v10

    if-eqz v10, :cond_3

    move v8, v9

    goto :goto_3

    .line 1795407
    :cond_9
    invoke-virtual {v0}, Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$FetchTopComposerGroupsModel$AccountUserModel$GroupsModel$NodesModel;->j()LX/1vs;

    move-result-object v8

    iget-object v10, v8, LX/1vs;->a:LX/15i;

    iget v8, v8, LX/1vs;->b:I

    invoke-virtual {v10, v8, v9}, LX/15i;->g(II)I

    move-result v8

    invoke-virtual {v10, v8, v9}, LX/15i;->g(II)I

    move-result v8

    invoke-virtual {v10, v8, v9}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    goto :goto_4
.end method
