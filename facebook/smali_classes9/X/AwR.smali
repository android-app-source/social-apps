.class public LX/AwR;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/compactdisk/DiskCache;


# direct methods
.method public constructor <init>(Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig;Lcom/facebook/compactdisk/StoreManagerFactory;)V
    .locals 9
    .param p1    # Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v8, 0x1

    .line 1726636
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1726637
    new-instance v0, Lcom/facebook/compactdisk/DiskCacheConfig;

    invoke-direct {v0}, Lcom/facebook/compactdisk/DiskCacheConfig;-><init>()V

    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/compactdisk/DiskCacheConfig;->name(Ljava/lang/String;)Lcom/facebook/compactdisk/DiskCacheConfig;

    move-result-object v0

    invoke-virtual {v0, v8}, Lcom/facebook/compactdisk/DiskCacheConfig;->sessionScoped(Z)Lcom/facebook/compactdisk/DiskCacheConfig;

    move-result-object v0

    sget-object v1, Lcom/facebook/compactdisk/DiskArea;->DOCUMENTS:Lcom/facebook/compactdisk/DiskArea;

    invoke-virtual {v0, v1}, Lcom/facebook/compactdisk/DiskCacheConfig;->diskArea(Lcom/facebook/compactdisk/DiskArea;)Lcom/facebook/compactdisk/DiskCacheConfig;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig;->getVersion()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/compactdisk/DiskCacheConfig;->version(Ljava/lang/Long;)Lcom/facebook/compactdisk/DiskCacheConfig;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig;->getCapacity()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/compactdisk/DiskCacheConfig;->maxCapacity(Ljava/lang/Long;)Lcom/facebook/compactdisk/DiskCacheConfig;

    move-result-object v0

    new-instance v1, Lcom/facebook/compactdisk/ManagedConfig;

    invoke-direct {v1}, Lcom/facebook/compactdisk/ManagedConfig;-><init>()V

    new-instance v2, Lcom/facebook/compactdisk/StalePruningConfig;

    invoke-direct {v2}, Lcom/facebook/compactdisk/StalePruningConfig;-><init>()V

    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig;->getStaleDays()Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-wide/32 v6, 0x15180

    mul-long/2addr v4, v6

    invoke-virtual {v2, v4, v5}, Lcom/facebook/compactdisk/StalePruningConfig;->a(J)Lcom/facebook/compactdisk/StalePruningConfig;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/compactdisk/ManagedConfig;->a(Lcom/facebook/compactdisk/StalePruningConfig;)Lcom/facebook/compactdisk/ManagedConfig;

    move-result-object v1

    new-instance v2, Lcom/facebook/compactdisk/EvictionConfig;

    invoke-direct {v2}, Lcom/facebook/compactdisk/EvictionConfig;-><init>()V

    invoke-virtual {v2, v8}, Lcom/facebook/compactdisk/EvictionConfig;->strictEnforcement(Z)Lcom/facebook/compactdisk/EvictionConfig;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig;->getMaxSize()Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const/16 v3, 0x14

    shl-long/2addr v4, v3

    invoke-virtual {v2, v4, v5}, Lcom/facebook/compactdisk/EvictionConfig;->maxSize(J)Lcom/facebook/compactdisk/EvictionConfig;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/compactdisk/ManagedConfig;->a(Lcom/facebook/compactdisk/EvictionConfig;)Lcom/facebook/compactdisk/ManagedConfig;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/compactdisk/DiskCacheConfig;->subConfig(Lcom/facebook/compactdisk/SubConfig;)Lcom/facebook/compactdisk/DiskCacheConfig;

    move-result-object v0

    .line 1726638
    invoke-virtual {p2, v0}, Lcom/facebook/compactdisk/StoreManagerFactory;->a(Lcom/facebook/compactdisk/DiskCacheConfig;)Lcom/facebook/compactdisk/StoreManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/facebook/compactdisk/StoreManager;->a(Lcom/facebook/compactdisk/DiskCacheConfig;)Lcom/facebook/compactdisk/DiskCache;

    move-result-object v0

    iput-object v0, p0, LX/AwR;->a:Lcom/facebook/compactdisk/DiskCache;

    .line 1726639
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1726635
    iget-object v0, p0, LX/AwR;->a:Lcom/facebook/compactdisk/DiskCache;

    invoke-virtual {v0}, Lcom/facebook/compactdisk/PersistentKeyValueStore;->getDirectoryPath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1726629
    iget-object v0, p0, LX/AwR;->a:Lcom/facebook/compactdisk/DiskCache;

    invoke-virtual {v0, p1}, Lcom/facebook/compactdisk/PersistentKeyValueStore;->fetchPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/io/InputStream;)Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1726630
    iget-object v0, p0, LX/AwR;->a:Lcom/facebook/compactdisk/DiskCache;

    invoke-virtual {v0, p1}, Lcom/facebook/compactdisk/PersistentKeyValueStore;->fetchPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1726631
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1726632
    iget-object v0, p0, LX/AwR;->a:Lcom/facebook/compactdisk/DiskCache;

    new-instance v1, LX/AwQ;

    invoke-direct {v1, p0, p2}, LX/AwQ;-><init>(LX/AwR;Ljava/io/InputStream;)V

    invoke-virtual {v0, p1, v1}, Lcom/facebook/compactdisk/PersistentKeyValueStore;->storeToPath(Ljava/lang/String;Lcom/facebook/compactdisk/WriteCallback;)V

    .line 1726633
    iget-object v0, p0, LX/AwR;->a:Lcom/facebook/compactdisk/DiskCache;

    invoke-virtual {v0, p1}, Lcom/facebook/compactdisk/PersistentKeyValueStore;->fetchPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1726634
    :cond_0
    return-object v0
.end method
