.class public LX/BaO;
.super LX/1Cv;
.source ""

# interfaces
.implements LX/0Vf;


# instance fields
.field public final a:LX/BaP;

.field public final b:LX/AK0;

.field public final c:LX/7ga;

.field private final d:LX/BaN;

.field public e:Z

.field public f:LX/AJz;

.field public g:Landroid/content/Context;

.field public h:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;",
            ">;"
        }
    .end annotation
.end field

.field public i:Lcom/facebook/audience/snacks/model/SnacksMyUserThread;

.field private j:LX/AKm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/AKm",
            "<",
            "Landroid/support/v7/widget/RecyclerView;",
            ">;"
        }
    .end annotation
.end field

.field public k:Z

.field public l:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/BaQ;LX/AK0;LX/0fD;LX/7ga;)V
    .locals 3
    .param p4    # LX/0fD;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1798939
    invoke-direct {p0}, LX/1Cv;-><init>()V

    .line 1798940
    new-instance v0, LX/BaN;

    invoke-direct {v0, p0}, LX/BaN;-><init>(LX/BaO;)V

    iput-object v0, p0, LX/BaO;->d:LX/BaN;

    .line 1798941
    iput-boolean v1, p0, LX/BaO;->e:Z

    .line 1798942
    sget-object v0, LX/AJz;->UNKNOWN:LX/AJz;

    iput-object v0, p0, LX/BaO;->f:LX/AJz;

    .line 1798943
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1798944
    iput-object v0, p0, LX/BaO;->h:LX/0Px;

    .line 1798945
    iput-boolean v1, p0, LX/BaO;->l:Z

    .line 1798946
    iput-object p1, p0, LX/BaO;->g:Landroid/content/Context;

    .line 1798947
    new-instance v2, LX/BaP;

    const-class v0, LX/BaT;

    invoke-interface {p2, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/BaT;

    const/16 v1, 0x12cb

    invoke-static {p2, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p1

    invoke-static {p2}, LX/7ga;->a(LX/0QB;)LX/7ga;

    move-result-object v1

    check-cast v1, LX/7ga;

    invoke-direct {v2, v0, p1, p4, v1}, LX/BaP;-><init>(LX/BaT;LX/0Or;LX/0fD;LX/7ga;)V

    .line 1798948
    move-object v0, v2

    .line 1798949
    iput-object v0, p0, LX/BaO;->a:LX/BaP;

    .line 1798950
    iput-object p3, p0, LX/BaO;->b:LX/AK0;

    .line 1798951
    iput-object p5, p0, LX/BaO;->c:LX/7ga;

    .line 1798952
    return-void
.end method

.method public static a(LX/0Px;)I
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;",
            ">;)I"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1798968
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v3

    move v2, v0

    move v1, v0

    :goto_0
    if-ge v2, v3, :cond_0

    invoke-virtual {p0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;

    .line 1798969
    iget v4, v0, Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;->f:I

    move v0, v4

    .line 1798970
    if-eqz v0, :cond_1

    .line 1798971
    add-int/lit8 v0, v1, 0x1

    .line 1798972
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    .line 1798973
    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method


# virtual methods
.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 1798953
    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    .line 1798954
    const/4 v0, 0x0

    .line 1798955
    :goto_0
    return-object v0

    .line 1798956
    :cond_0
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1798957
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030170

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1798958
    new-instance v2, LX/AKm;

    const v0, 0x7f0d068d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-direct {v2, v0}, LX/AKm;-><init>(Landroid/view/ViewStub;)V

    iput-object v2, p0, LX/BaO;->j:LX/AKm;

    .line 1798959
    const/4 p2, 0x0

    .line 1798960
    iget-object v0, p0, LX/BaO;->c:LX/7ga;

    const/4 v2, 0x1

    sget-object p1, LX/7gd;->DEFAULT:LX/7gd;

    invoke-virtual {v0, p2, v2, p2, p1}, LX/7ga;->a(ZIILX/7gd;)V

    .line 1798961
    iget-object v0, p0, LX/BaO;->b:LX/AK0;

    iget-object v2, p0, LX/BaO;->d:LX/BaN;

    .line 1798962
    iget-object p1, v0, LX/AK0;->d:LX/7gh;

    invoke-virtual {p1, v2}, LX/7gh;->a(Ljava/lang/Object;)V

    .line 1798963
    iget-object v0, p0, LX/BaO;->b:LX/AK0;

    sget-object v2, LX/AJz;->LOAD_UI:LX/AJz;

    .line 1798964
    const/4 p0, 0x0

    invoke-virtual {v0, p0, v2}, LX/AK0;->a(ZLX/AJz;)V

    .line 1798965
    const/4 p0, 0x1

    invoke-virtual {v0, p0, v2}, LX/AK0;->a(ZLX/AJz;)V

    .line 1798966
    move-object v0, v1

    .line 1798967
    goto :goto_0
.end method

.method public final a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1798913
    const/4 v0, 0x1

    if-ne p4, v0, :cond_1

    iget-object v0, p0, LX/BaO;->i:Lcom/facebook/audience/snacks/model/SnacksMyUserThread;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/BaO;->i:Lcom/facebook/audience/snacks/model/SnacksMyUserThread;

    .line 1798914
    iget-object v1, v0, Lcom/facebook/audience/snacks/model/SnacksMyUserThread;->e:LX/0Px;

    move-object v0, v1

    .line 1798915
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    iget-object v0, p0, LX/BaO;->h:LX/0Px;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/BaO;->h:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-nez v0, :cond_2

    .line 1798916
    :cond_1
    :goto_0
    return-void

    .line 1798917
    :cond_2
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1798918
    iget-object p1, p0, LX/BaO;->f:LX/AJz;

    sget-object p2, LX/AJz;->UNKNOWN:LX/AJz;

    if-ne p1, p2, :cond_3

    .line 1798919
    iget-object p1, p0, LX/BaO;->c:LX/7ga;

    invoke-virtual {p1, v1, v0, v1}, LX/7ga;->a(ZII)V

    .line 1798920
    :goto_1
    iget-object v0, p0, LX/BaO;->j:LX/AKm;

    invoke-virtual {v0}, LX/AKm;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    .line 1798921
    new-instance v1, LX/1P1;

    invoke-virtual {p5}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    invoke-direct {v1, v2, v2}, LX/1P1;-><init>(IZ)V

    .line 1798922
    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 1798923
    iget-object v1, p0, LX/BaO;->a:LX/BaP;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 1798924
    iget-boolean v0, p0, LX/BaO;->k:Z

    if-nez v0, :cond_1

    .line 1798925
    new-instance v3, LX/AhZ;

    const/4 v4, 0x0

    iget-object v5, p0, LX/BaO;->g:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b1b58

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    invoke-direct {v3, p3, v4, v5}, LX/AhZ;-><init>(Landroid/view/View;II)V

    .line 1798926
    const-wide/16 v5, 0xc8

    invoke-virtual {v3, v5, v6}, LX/AhZ;->setDuration(J)V

    .line 1798927
    invoke-virtual {p3, v3}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1798928
    const/4 v3, 0x1

    iput-boolean v3, p0, LX/BaO;->k:Z

    .line 1798929
    goto :goto_0

    .line 1798930
    :cond_3
    iget-object p1, p0, LX/BaO;->c:LX/7ga;

    iget-object p2, p0, LX/BaO;->i:Lcom/facebook/audience/snacks/model/SnacksMyUserThread;

    .line 1798931
    iget-object p4, p2, Lcom/facebook/audience/snacks/model/SnacksMyUserThread;->e:LX/0Px;

    move-object p2, p4

    .line 1798932
    invoke-virtual {p2}, LX/0Px;->isEmpty()Z

    move-result p2

    if-nez p2, :cond_4

    :goto_2
    iget-object v1, p0, LX/BaO;->h:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    iget-object p2, p0, LX/BaO;->h:LX/0Px;

    invoke-static {p2}, LX/BaO;->a(LX/0Px;)I

    move-result p2

    invoke-virtual {p1, v0, v1, p2}, LX/7ga;->a(ZII)V

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_2
.end method

.method public final dispose()V
    .locals 2

    .prologue
    .line 1798933
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/BaO;->e:Z

    .line 1798934
    sget-object v0, LX/AJz;->UNKNOWN:LX/AJz;

    iput-object v0, p0, LX/BaO;->f:LX/AJz;

    .line 1798935
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/BaO;->l:Z

    .line 1798936
    iget-object v0, p0, LX/BaO;->b:LX/AK0;

    iget-object v1, p0, LX/BaO;->d:LX/BaN;

    .line 1798937
    iget-object p0, v0, LX/AK0;->d:LX/7gh;

    invoke-virtual {p0, v1}, LX/7gh;->b(Ljava/lang/Object;)V

    .line 1798938
    return-void
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 1798912
    const/4 v0, 0x1

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1798911
    iget-object v0, p0, LX/BaO;->h:LX/0Px;

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 1798910
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 1798909
    const/4 v0, 0x1

    return v0
.end method

.method public final isDisposed()Z
    .locals 1

    .prologue
    .line 1798908
    iget-boolean v0, p0, LX/BaO;->l:Z

    return v0
.end method
