.class public LX/BS2;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1784673
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(I)Ljava/lang/String;
    .locals 3
    .param p0    # I
        .annotation build Lcom/facebook/timeline/widget/actionbar/PersonActionBarItems;
        .end annotation
    .end param

    .prologue
    .line 1784650
    packed-switch p0, :pswitch_data_0

    .line 1784651
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected item: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1784652
    :pswitch_0
    const-string v0, "manage_friendship"

    .line 1784653
    :goto_0
    return-object v0

    .line 1784654
    :pswitch_1
    const-string v0, "follow"

    goto :goto_0

    .line 1784655
    :pswitch_2
    const-string v0, "message"

    goto :goto_0

    .line 1784656
    :pswitch_3
    const-string v0, "manage"

    goto :goto_0

    .line 1784657
    :pswitch_4
    const-string v0, "poke"

    goto :goto_0

    .line 1784658
    :pswitch_5
    const-string v0, "see_friendship"

    goto :goto_0

    .line 1784659
    :pswitch_6
    const-string v0, "suggest_friends"

    goto :goto_0

    .line 1784660
    :pswitch_7
    const-string v0, "block"

    goto :goto_0

    .line 1784661
    :pswitch_8
    const-string v0, "report"

    goto :goto_0

    .line 1784662
    :pswitch_9
    const-string v0, "update_info"

    goto :goto_0

    .line 1784663
    :pswitch_a
    const-string v0, "change_profile_pic"

    goto :goto_0

    .line 1784664
    :pswitch_b
    const-string v0, "change_cover_photo"

    goto :goto_0

    .line 1784665
    :pswitch_c
    const-string v0, "privacy_shortcuts"

    goto :goto_0

    .line 1784666
    :pswitch_d
    const-string v0, "activity_log"

    goto :goto_0

    .line 1784667
    :pswitch_e
    const-string v0, "add_to_groups"

    goto :goto_0

    .line 1784668
    :pswitch_f
    const-string v0, "copy_profile_link"

    goto :goto_0

    .line 1784669
    :pswitch_10
    const-string v0, "edit_profile"

    goto :goto_0

    .line 1784670
    :pswitch_11
    const-string v0, "save"

    goto :goto_0

    .line 1784671
    :pswitch_12
    const-string v0, "saved"

    goto :goto_0

    .line 1784672
    :pswitch_13
    const-string v0, "view_profile_insight"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
    .end packed-switch
.end method
