.class public final LX/BaS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:Lcom/facebook/audience/snacks/model/SnacksMyUserThread;

.field public final synthetic c:I

.field public final synthetic d:Lcom/facebook/audience/snacks/view/snacks_row/ProfileViewHolder;


# direct methods
.method public constructor <init>(Lcom/facebook/audience/snacks/view/snacks_row/ProfileViewHolder;ZLcom/facebook/audience/snacks/model/SnacksMyUserThread;I)V
    .locals 0

    .prologue
    .line 1799080
    iput-object p1, p0, LX/BaS;->d:Lcom/facebook/audience/snacks/view/snacks_row/ProfileViewHolder;

    iput-boolean p2, p0, LX/BaS;->a:Z

    iput-object p3, p0, LX/BaS;->b:Lcom/facebook/audience/snacks/model/SnacksMyUserThread;

    iput p4, p0, LX/BaS;->c:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v0, 0x2

    const v1, 0x40e1828f

    invoke-static {v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v7

    .line 1799081
    iget-boolean v0, p0, LX/BaS;->a:Z

    if-eqz v0, :cond_0

    .line 1799082
    iget-object v0, p0, LX/BaS;->d:Lcom/facebook/audience/snacks/view/snacks_row/ProfileViewHolder;

    iget-object v0, v0, Lcom/facebook/audience/snacks/view/snacks_row/ProfileViewHolder;->q:LX/BaM;

    iget-object v1, p0, LX/BaS;->b:Lcom/facebook/audience/snacks/model/SnacksMyUserThread;

    iget-object v2, p0, LX/BaS;->d:Lcom/facebook/audience/snacks/view/snacks_row/ProfileViewHolder;

    iget-object v2, v2, Lcom/facebook/audience/snacks/view/snacks_row/ProfileViewHolder;->p:Landroid/content/Context;

    .line 1799083
    iget-object v4, v0, LX/BaM;->a:LX/17Y;

    sget-object v5, LX/0ax;->gr:Ljava/lang/String;

    invoke-interface {v4, v2, v5}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v5

    .line 1799084
    const-string v4, "extra_snack_user_thread_id"

    .line 1799085
    iget-object v6, v1, Lcom/facebook/audience/snacks/model/SnacksMyUserThread;->c:Lcom/facebook/audience/model/AudienceControlData;

    move-object v6, v6

    .line 1799086
    invoke-virtual {v6}, Lcom/facebook/audience/model/AudienceControlData;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1799087
    const-string v4, "extra_snack_is_my_snack"

    const/4 v6, 0x1

    invoke-virtual {v5, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1799088
    const-class v4, Landroid/app/Activity;

    invoke-static {v2, v4}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/Activity;

    .line 1799089
    iget-object v6, v0, LX/BaM;->b:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v6, v5, v4}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1799090
    invoke-static {v4}, LX/BaM;->a(Landroid/app/Activity;)V

    .line 1799091
    iget-object v0, p0, LX/BaS;->d:Lcom/facebook/audience/snacks/view/snacks_row/ProfileViewHolder;

    iget-object v0, v0, Lcom/facebook/audience/snacks/view/snacks_row/ProfileViewHolder;->r:LX/7ga;

    iget-object v1, p0, LX/BaS;->b:Lcom/facebook/audience/snacks/model/SnacksMyUserThread;

    .line 1799092
    iget-object v2, v1, Lcom/facebook/audience/snacks/model/SnacksMyUserThread;->c:Lcom/facebook/audience/model/AudienceControlData;

    move-object v1, v2

    .line 1799093
    invoke-virtual {v1}, Lcom/facebook/audience/model/AudienceControlData;->getId()Ljava/lang/String;

    move-result-object v1

    iget v2, p0, LX/BaS;->c:I

    sget-object v4, LX/7gZ;->SELF:LX/7gZ;

    iget-object v5, p0, LX/BaS;->b:Lcom/facebook/audience/snacks/model/SnacksMyUserThread;

    .line 1799094
    iget-object v6, v5, Lcom/facebook/audience/snacks/model/SnacksMyUserThread;->e:LX/0Px;

    move-object v5, v6

    .line 1799095
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v5

    sget-object v6, LX/7gd;->STORY_TRAY:LX/7gd;

    invoke-virtual/range {v0 .. v6}, LX/7ga;->a(Ljava/lang/String;IZLX/7gZ;ILX/7gd;)V

    .line 1799096
    :goto_0
    const v0, -0x4a74e888

    invoke-static {v0, v7}, LX/02F;->a(II)V

    return-void

    .line 1799097
    :cond_0
    iget-object v0, p0, LX/BaS;->d:Lcom/facebook/audience/snacks/view/snacks_row/ProfileViewHolder;

    iget-object v0, v0, Lcom/facebook/audience/snacks/view/snacks_row/ProfileViewHolder;->s:LX/0fD;

    invoke-interface {v0}, LX/0fD;->f()LX/0fK;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0fK;->a_(Z)V

    .line 1799098
    iget-object v0, p0, LX/BaS;->d:Lcom/facebook/audience/snacks/view/snacks_row/ProfileViewHolder;

    iget-object v0, v0, Lcom/facebook/audience/snacks/view/snacks_row/ProfileViewHolder;->r:LX/7ga;

    iget-object v1, p0, LX/BaS;->b:Lcom/facebook/audience/snacks/model/SnacksMyUserThread;

    .line 1799099
    iget-object v2, v1, Lcom/facebook/audience/snacks/model/SnacksMyUserThread;->c:Lcom/facebook/audience/model/AudienceControlData;

    move-object v1, v2

    .line 1799100
    invoke-virtual {v1}, Lcom/facebook/audience/model/AudienceControlData;->getId()Ljava/lang/String;

    move-result-object v1

    iget v2, p0, LX/BaS;->c:I

    sget-object v4, LX/7gZ;->SELF:LX/7gZ;

    sget-object v6, LX/7gd;->STORY_TRAY:LX/7gd;

    move v5, v3

    invoke-virtual/range {v0 .. v6}, LX/7ga;->a(Ljava/lang/String;IZLX/7gZ;ILX/7gd;)V

    goto :goto_0
.end method
