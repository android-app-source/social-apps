.class public final LX/BaL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# instance fields
.field public final synthetic a:Lcom/google/common/util/concurrent/SettableFuture;

.field public final synthetic b:LX/7gj;

.field public final synthetic c:LX/1FJ;

.field public final synthetic d:LX/1Em;


# direct methods
.method public constructor <init>(LX/1Em;Lcom/google/common/util/concurrent/SettableFuture;LX/7gj;LX/1FJ;)V
    .locals 0

    .prologue
    .line 1798900
    iput-object p1, p0, LX/BaL;->d:LX/1Em;

    iput-object p2, p0, LX/BaL;->a:Lcom/google/common/util/concurrent/SettableFuture;

    iput-object p3, p0, LX/BaL;->b:LX/7gj;

    iput-object p4, p0, LX/BaL;->c:LX/1FJ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1798894
    iget-object v0, p0, LX/BaL;->a:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v0, p1}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    .line 1798895
    iget-object v0, p0, LX/BaL;->c:LX/1FJ;

    invoke-virtual {v0}, LX/1FJ;->close()V

    .line 1798896
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1798897
    iget-object v0, p0, LX/BaL;->a:Lcom/google/common/util/concurrent/SettableFuture;

    iget-object v1, p0, LX/BaL;->b:LX/7gj;

    const v2, -0x3cd56723

    invoke-static {v0, v1, v2}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    .line 1798898
    iget-object v0, p0, LX/BaL;->c:LX/1FJ;

    invoke-virtual {v0}, LX/1FJ;->close()V

    .line 1798899
    return-void
.end method
