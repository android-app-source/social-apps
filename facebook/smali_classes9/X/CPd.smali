.class public final LX/CPd;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/CPd;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/CPb;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/CPe;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1884733
    const/4 v0, 0x0

    sput-object v0, LX/CPd;->a:LX/CPd;

    .line 1884734
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/CPd;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1884695
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1884696
    new-instance v0, LX/CPe;

    invoke-direct {v0}, LX/CPe;-><init>()V

    iput-object v0, p0, LX/CPd;->c:LX/CPe;

    .line 1884697
    return-void
.end method

.method public static declared-synchronized q()LX/CPd;
    .locals 2

    .prologue
    .line 1884729
    const-class v1, LX/CPd;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/CPd;->a:LX/CPd;

    if-nez v0, :cond_0

    .line 1884730
    new-instance v0, LX/CPd;

    invoke-direct {v0}, LX/CPd;-><init>()V

    sput-object v0, LX/CPd;->a:LX/CPd;

    .line 1884731
    :cond_0
    sget-object v0, LX/CPd;->a:LX/CPd;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 1884732
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 1884711
    check-cast p2, LX/CPc;

    .line 1884712
    iget-object v0, p2, LX/CPc;->a:LX/CNb;

    iget-object v1, p2, LX/CPc;->b:LX/CNc;

    iget-object v2, p2, LX/CPc;->c:Ljava/util/List;

    .line 1884713
    const-string v3, "image"

    invoke-virtual {v0, v3}, LX/CNb;->b(Ljava/lang/String;)LX/CNb;

    move-result-object v3

    .line 1884714
    if-nez v3, :cond_0

    .line 1884715
    const/4 v3, 0x0

    .line 1884716
    :goto_0
    move-object v0, v3

    .line 1884717
    return-object v0

    .line 1884718
    :cond_0
    invoke-static {v3, v1, p1}, LX/CNd;->b(LX/CNb;LX/CNc;LX/1De;)LX/CO8;

    move-result-object v4

    .line 1884719
    const-string v5, "resizing-mode"

    const-string p0, ""

    invoke-virtual {v0, v5, p0}, LX/CNb;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, p1, v3, v5, v1}, LX/CO8;->a(LX/1De;LX/CNb;Ljava/lang/String;LX/CNc;)LX/1Di;

    move-result-object v3

    .line 1884720
    const-string v4, "touch-up-inside-actions"

    invoke-static {v1, v0, v2, v4}, LX/CPx;->a(LX/CNc;LX/CNb;Ljava/util/List;Ljava/lang/String;)LX/CNe;

    move-result-object v4

    .line 1884721
    if-eqz v4, :cond_1

    .line 1884722
    const v5, 0x794d3411

    const/4 p0, 0x1

    new-array p0, p0, [Ljava/lang/Object;

    const/4 p2, 0x0

    aput-object v4, p0, p2

    invoke-static {p1, v5, p0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v5

    move-object v4, v5

    .line 1884723
    invoke-interface {v3, v4}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    .line 1884724
    :cond_1
    const-string v4, "long-press-actions"

    invoke-static {v1, v0, v2, v4}, LX/CPx;->a(LX/CNc;LX/CNb;Ljava/util/List;Ljava/lang/String;)LX/CNe;

    move-result-object v4

    .line 1884725
    if-eqz v4, :cond_2

    .line 1884726
    const v5, 0x5e54beae

    const/4 p0, 0x1

    new-array p0, p0, [Ljava/lang/Object;

    const/4 p2, 0x0

    aput-object v4, p0, p2

    invoke-static {p1, v5, p0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v5

    move-object v4, v5

    .line 1884727
    invoke-interface {v3, v4}, LX/1Di;->b(LX/1dQ;)LX/1Di;

    .line 1884728
    :cond_2
    invoke-static {v3, p1, v0, v2}, LX/CPx;->a(LX/1Di;LX/1De;LX/CNb;Ljava/util/List;)LX/1Dg;

    move-result-object v3

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 1884698
    invoke-static {}, LX/1dS;->b()V

    .line 1884699
    iget v0, p1, LX/1dQ;->b:I

    .line 1884700
    sparse-switch v0, :sswitch_data_0

    move-object v0, v1

    .line 1884701
    :goto_0
    return-object v0

    .line 1884702
    :sswitch_0
    iget-object v0, p1, LX/1dQ;->c:[Ljava/lang/Object;

    aget-object v0, v0, v2

    check-cast v0, LX/CNe;

    .line 1884703
    invoke-interface {v0}, LX/CNe;->a()V

    .line 1884704
    move-object v0, v1

    .line 1884705
    goto :goto_0

    .line 1884706
    :sswitch_1
    iget-object v0, p1, LX/1dQ;->c:[Ljava/lang/Object;

    aget-object v0, v0, v2

    check-cast v0, LX/CNe;

    .line 1884707
    invoke-interface {v0}, LX/CNe;->a()V

    .line 1884708
    const/4 v2, 0x1

    move v2, v2

    .line 1884709
    move v0, v2

    .line 1884710
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x5e54beae -> :sswitch_1
        0x794d3411 -> :sswitch_0
    .end sparse-switch
.end method
