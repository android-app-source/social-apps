.class public LX/Abg;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0SG;

.field public final b:Landroid/os/Handler;

.field private final c:Lcom/facebook/facecast/scheduledlive/FacecastScheduledLiveStateManager$FacecastScheduledLiveStateTransitionRunnable;

.field public final d:Lcom/facebook/facecast/scheduledlive/FacecastScheduledLiveStateManager$CountdownSecondRunnable;

.field private e:Z

.field public f:Z

.field public g:LX/Abf;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public i:LX/Abe;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0SG;Landroid/os/Handler;)V
    .locals 2
    .param p2    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1691178
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1691179
    iput-object p1, p0, LX/Abg;->a:LX/0SG;

    .line 1691180
    iput-object p2, p0, LX/Abg;->b:Landroid/os/Handler;

    .line 1691181
    new-instance v0, Lcom/facebook/facecast/scheduledlive/FacecastScheduledLiveStateManager$FacecastScheduledLiveStateTransitionRunnable;

    invoke-direct {v0, p0}, Lcom/facebook/facecast/scheduledlive/FacecastScheduledLiveStateManager$FacecastScheduledLiveStateTransitionRunnable;-><init>(LX/Abg;)V

    iput-object v0, p0, LX/Abg;->c:Lcom/facebook/facecast/scheduledlive/FacecastScheduledLiveStateManager$FacecastScheduledLiveStateTransitionRunnable;

    .line 1691182
    new-instance v0, Lcom/facebook/facecast/scheduledlive/FacecastScheduledLiveStateManager$CountdownSecondRunnable;

    invoke-direct {v0, p0}, Lcom/facebook/facecast/scheduledlive/FacecastScheduledLiveStateManager$CountdownSecondRunnable;-><init>(LX/Abg;)V

    iput-object v0, p0, LX/Abg;->d:Lcom/facebook/facecast/scheduledlive/FacecastScheduledLiveStateManager$CountdownSecondRunnable;

    .line 1691183
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Abg;->f:Z

    .line 1691184
    return-void
.end method

.method public static a$redex0(LX/Abg;J)J
    .locals 5

    .prologue
    .line 1691177
    invoke-direct {p0}, LX/Abg;->e()J

    move-result-wide v0

    iget-object v2, p0, LX/Abg;->h:Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->m()J

    move-result-wide v2

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    sub-long/2addr v0, p1

    return-wide v0
.end method

.method public static b(LX/0QB;)LX/Abg;
    .locals 3

    .prologue
    .line 1691175
    new-instance v2, LX/Abg;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v0

    check-cast v0, LX/0SG;

    invoke-static {p0}, LX/0Ss;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v1

    check-cast v1, Landroid/os/Handler;

    invoke-direct {v2, v0, v1}, LX/Abg;-><init>(LX/0SG;Landroid/os/Handler;)V

    .line 1691176
    return-object v2
.end method

.method public static b$redex0(LX/Abg;J)J
    .locals 5

    .prologue
    .line 1691174
    invoke-direct {p0}, LX/Abg;->e()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    sub-long/2addr v0, p1

    return-wide v0
.end method

.method public static c$redex0(LX/Abg;J)J
    .locals 5

    .prologue
    .line 1691173
    invoke-direct {p0}, LX/Abg;->e()J

    move-result-wide v0

    iget-object v2, p0, LX/Abg;->h:Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->w()J

    move-result-wide v2

    add-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    sub-long/2addr v0, p1

    return-wide v0
.end method

.method public static d(LX/Abg;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 1691161
    iget-object v0, p0, LX/Abg;->h:Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1691162
    iget-object v0, p0, LX/Abg;->a:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    .line 1691163
    invoke-static {p0, v0, v1}, LX/Abg;->a$redex0(LX/Abg;J)J

    move-result-wide v2

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    .line 1691164
    sget-object v0, LX/Abe;->PRELOBBY:LX/Abe;

    iput-object v0, p0, LX/Abg;->i:LX/Abe;

    .line 1691165
    :goto_0
    return-void

    .line 1691166
    :cond_0
    invoke-static {p0, v0, v1}, LX/Abg;->b$redex0(LX/Abg;J)J

    move-result-wide v2

    cmp-long v2, v2, v4

    if-lez v2, :cond_1

    .line 1691167
    sget-object v0, LX/Abe;->COUNTDOWN_STARTED:LX/Abe;

    iput-object v0, p0, LX/Abg;->i:LX/Abe;

    goto :goto_0

    .line 1691168
    :cond_1
    invoke-static {p0, v0, v1}, LX/Abg;->c$redex0(LX/Abg;J)J

    move-result-wide v2

    cmp-long v2, v2, v4

    if-lez v2, :cond_2

    .line 1691169
    sget-object v0, LX/Abe;->COUNTDOWN_ENDED:LX/Abe;

    iput-object v0, p0, LX/Abg;->i:LX/Abe;

    goto :goto_0

    .line 1691170
    :cond_2
    invoke-static {p0, v0, v1}, LX/Abg;->d$redex0(LX/Abg;J)J

    move-result-wide v0

    cmp-long v0, v0, v4

    if-lez v0, :cond_3

    .line 1691171
    sget-object v0, LX/Abe;->RUNNING_LATE:LX/Abe;

    iput-object v0, p0, LX/Abg;->i:LX/Abe;

    goto :goto_0

    .line 1691172
    :cond_3
    sget-object v0, LX/Abe;->TIMED_OUT:LX/Abe;

    iput-object v0, p0, LX/Abg;->i:LX/Abe;

    goto :goto_0
.end method

.method public static d$redex0(LX/Abg;J)J
    .locals 5

    .prologue
    .line 1691185
    invoke-direct {p0}, LX/Abg;->e()J

    move-result-wide v0

    iget-object v2, p0, LX/Abg;->h:Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->j()J

    move-result-wide v2

    add-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    sub-long/2addr v0, p1

    return-wide v0
.end method

.method private e()J
    .locals 4

    .prologue
    .line 1691159
    iget-object v0, p0, LX/Abg;->h:Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1691160
    iget-object v0, p0, LX/Abg;->h:Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->n()J

    move-result-wide v0

    iget-object v2, p0, LX/Abg;->h:Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->D()J

    move-result-wide v2

    add-long/2addr v0, v2

    return-wide v0
.end method


# virtual methods
.method public final a()LX/Abe;
    .locals 3

    .prologue
    .line 1691150
    iget-boolean v0, p0, LX/Abg;->e:Z

    if-eqz v0, :cond_0

    .line 1691151
    iget-object v0, p0, LX/Abg;->i:LX/Abe;

    .line 1691152
    :goto_0
    return-object v0

    .line 1691153
    :cond_0
    iget-object v0, p0, LX/Abg;->h:Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1691154
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Abg;->e:Z

    .line 1691155
    invoke-static {p0}, LX/Abg;->d(LX/Abg;)V

    .line 1691156
    iget-object v0, p0, LX/Abg;->i:LX/Abe;

    sget-object v1, LX/Abe;->TIMED_OUT:LX/Abe;

    if-eq v0, v1, :cond_1

    .line 1691157
    iget-object v0, p0, LX/Abg;->b:Landroid/os/Handler;

    iget-object v1, p0, LX/Abg;->c:Lcom/facebook/facecast/scheduledlive/FacecastScheduledLiveStateManager$FacecastScheduledLiveStateTransitionRunnable;

    const v2, 0x2451da21

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1691158
    :cond_1
    iget-object v0, p0, LX/Abg;->i:LX/Abe;

    goto :goto_0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;)V
    .locals 1

    .prologue
    .line 1691146
    iget-object v0, p0, LX/Abg;->h:Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    if-eq v0, p1, :cond_0

    .line 1691147
    invoke-virtual {p0}, LX/Abg;->b()V

    .line 1691148
    iput-object p1, p0, LX/Abg;->h:Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    .line 1691149
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1691140
    iget-boolean v0, p0, LX/Abg;->e:Z

    if-nez v0, :cond_0

    .line 1691141
    :goto_0
    return-void

    .line 1691142
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Abg;->e:Z

    .line 1691143
    const/4 v0, 0x0

    iput-object v0, p0, LX/Abg;->i:LX/Abe;

    .line 1691144
    iget-object v0, p0, LX/Abg;->b:Landroid/os/Handler;

    iget-object v1, p0, LX/Abg;->c:Lcom/facebook/facecast/scheduledlive/FacecastScheduledLiveStateManager$FacecastScheduledLiveStateTransitionRunnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1691145
    iget-object v0, p0, LX/Abg;->b:Landroid/os/Handler;

    iget-object v1, p0, LX/Abg;->d:Lcom/facebook/facecast/scheduledlive/FacecastScheduledLiveStateManager$CountdownSecondRunnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public final c()LX/Abe;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 1691138
    invoke-static {p0}, LX/Abg;->d(LX/Abg;)V

    .line 1691139
    iget-object v0, p0, LX/Abg;->i:LX/Abe;

    return-object v0
.end method
