.class public final LX/BXJ;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "LX/0P1",
        "<",
        "Ljava/lang/String;",
        "LX/0Px",
        "<",
        "Lcom/facebook/user/model/User;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;)V
    .locals 0

    .prologue
    .line 1793000
    iput-object p1, p0, LX/BXJ;->a:Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1793001
    iget-object v0, p0, LX/BXJ;->a:Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;

    .line 1793002
    sget-object v1, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->v:Ljava/lang/Class;

    const-string p0, "Default loader could not load Users for contact db"

    invoke-static {v1, p0, p1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1793003
    iget-object v1, v0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->J:Landroid/widget/TextView;

    const p0, 0x7f080039

    invoke-virtual {v1, p0}, Landroid/widget/TextView;->setText(I)V

    .line 1793004
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 1793005
    invoke-virtual {v0, v1}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->a(Ljava/util/Map;)V

    .line 1793006
    iget-object v1, v0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->t:Landroid/view/View;

    const/16 p0, 0x8

    invoke-virtual {v1, p0}, Landroid/view/View;->setVisibility(I)V

    .line 1793007
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 1793008
    check-cast p1, LX/0P1;

    .line 1793009
    iget-object v0, p0, LX/BXJ;->a:Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->a(LX/0P1;)V

    .line 1793010
    return-void
.end method
