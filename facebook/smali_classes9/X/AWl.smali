.class public LX/AWl;
.super LX/AWT;
.source ""

# interfaces
.implements LX/1b9;
.implements LX/AWk;
.implements LX/1bG;


# instance fields
.field public final a:Lcom/facebook/facecastdisplay/FacecastInteractionView;

.field public final b:Landroid/view/View;

.field public c:Z

.field public f:LX/Aby;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/sounds/FBSoundUtil;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3RZ;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/3HT;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/1b4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/AYq;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/AWi;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final m:Lcom/facebook/facecast/plugin/FacecastToolbarContainer;

.field public final n:Lcom/facebook/resources/ui/FbButton;

.field public final o:LX/Ac3;

.field private final p:LX/AWj;

.field public final q:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1683321
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/AWl;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1683322
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1683319
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/AWl;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1683320
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 9

    .prologue
    const/4 v4, 0x0

    .line 1683299
    invoke-direct {p0, p1, p2, p3}, LX/AWT;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1683300
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, LX/AWl;

    invoke-static {v0}, LX/Aby;->b(LX/0QB;)LX/Aby;

    move-result-object v3

    check-cast v3, LX/Aby;

    const/16 v5, 0x3567

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x11ee

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {v0}, LX/3HT;->a(LX/0QB;)LX/3HT;

    move-result-object v7

    check-cast v7, LX/3HT;

    invoke-static {v0}, LX/1b4;->b(LX/0QB;)LX/1b4;

    move-result-object v8

    check-cast v8, LX/1b4;

    invoke-static {v0}, LX/AYq;->a(LX/0QB;)LX/AYq;

    move-result-object v0

    check-cast v0, LX/AYq;

    iput-object v3, v2, LX/AWl;->f:LX/Aby;

    iput-object v5, v2, LX/AWl;->g:LX/0Ot;

    iput-object v6, v2, LX/AWl;->h:LX/0Ot;

    iput-object v7, v2, LX/AWl;->i:LX/3HT;

    iput-object v8, v2, LX/AWl;->j:LX/1b4;

    iput-object v0, v2, LX/AWl;->k:LX/AYq;

    .line 1683301
    const v0, 0x7f0305a9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1683302
    const v0, 0x7f0d0f83

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/FacecastInteractionView;

    iput-object v0, p0, LX/AWl;->a:Lcom/facebook/facecastdisplay/FacecastInteractionView;

    .line 1683303
    const v0, 0x7f0d0f84

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/AWl;->b:Landroid/view/View;

    .line 1683304
    const v0, 0x7f0d0f85

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;

    iput-object v0, p0, LX/AWl;->m:Lcom/facebook/facecast/plugin/FacecastToolbarContainer;

    .line 1683305
    const v0, 0x7f0d0f86

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, LX/AWl;->n:Lcom/facebook/resources/ui/FbButton;

    .line 1683306
    iget-object v0, p0, LX/AWl;->n:Lcom/facebook/resources/ui/FbButton;

    new-instance v1, LX/AWg;

    invoke-direct {v1, p0}, LX/AWg;-><init>(LX/AWl;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1683307
    iget-object v0, p0, LX/AWl;->m:Lcom/facebook/facecast/plugin/FacecastToolbarContainer;

    .line 1683308
    iput-object p0, v0, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;->m:LX/AWk;

    .line 1683309
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget-object v1, LX/03r;->FacecastBottomContainer:[I

    const v2, 0x7f01045c

    invoke-virtual {v0, p2, v1, v2, p3}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1683310
    const/16 v1, 0x0

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    .line 1683311
    const/16 v2, 0x1

    invoke-virtual {v0, v2, v4}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v2

    .line 1683312
    const/16 v3, 0x2

    invoke-static {p1, v0, v3}, LX/1z0;->a(Landroid/content/Context;Landroid/content/res/TypedArray;I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, LX/AWl;->q:Ljava/lang/String;

    .line 1683313
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1683314
    new-instance v0, LX/Ac3;

    invoke-direct {v0, v1, v2}, LX/Ac3;-><init>(II)V

    iput-object v0, p0, LX/AWl;->o:LX/Ac3;

    .line 1683315
    iget-object v0, p0, LX/AWl;->b:Landroid/view/View;

    iget-object v1, p0, LX/AWl;->o:LX/Ac3;

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1683316
    new-instance v0, LX/AWj;

    invoke-direct {v0, p0}, LX/AWj;-><init>(LX/AWl;)V

    iput-object v0, p0, LX/AWl;->p:LX/AWj;

    .line 1683317
    iget-object v0, p0, LX/AWl;->k:LX/AYq;

    invoke-virtual {v0, p0}, LX/AYq;->a(LX/1bG;)V

    .line 1683318
    return-void
.end method


# virtual methods
.method public final a(LX/AVE;LX/AVE;)V
    .locals 2

    .prologue
    .line 1683293
    sget-object v0, LX/AWh;->a:[I

    invoke-virtual {p2}, LX/AVE;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1683294
    :goto_0
    sget-object v0, LX/AWh;->a:[I

    invoke-virtual {p1}, LX/AVE;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    .line 1683295
    :goto_1
    return-void

    .line 1683296
    :pswitch_0
    iget-object v0, p0, LX/AWl;->m:Lcom/facebook/facecast/plugin/FacecastToolbarContainer;

    invoke-virtual {v0}, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;->a()V

    goto :goto_0

    .line 1683297
    :pswitch_1
    iget-object v0, p0, LX/AWl;->f:LX/Aby;

    invoke-virtual {v0}, LX/Aby;->f()V

    .line 1683298
    iget-object v0, p0, LX/AWl;->f:LX/Aby;

    invoke-virtual {v0}, LX/AVi;->b()V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_1
    .end packed-switch
.end method

.method public final a(LX/AYp;LX/AYp;)V
    .locals 2

    .prologue
    .line 1683288
    iget-boolean v0, p0, LX/AWl;->c:Z

    if-nez v0, :cond_0

    .line 1683289
    :goto_0
    return-void

    .line 1683290
    :cond_0
    sget-object v0, LX/AWh;->b:[I

    invoke-virtual {p1}, LX/AYp;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 1683291
    :pswitch_0
    iget-object v0, p0, LX/AWl;->f:LX/Aby;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, LX/Aby;->b(I)V

    goto :goto_0

    .line 1683292
    :pswitch_1
    iget-object v0, p0, LX/AWl;->f:LX/Aby;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/Aby;->b(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Landroid/view/ViewGroup;LX/AVF;)V
    .locals 2

    .prologue
    .line 1683284
    invoke-super {p0, p1, p2}, LX/AWT;->a(Landroid/view/ViewGroup;LX/AVF;)V

    .line 1683285
    invoke-virtual {p2, p0}, LX/AVF;->b(LX/1b9;)V

    .line 1683286
    iget-object v0, p0, LX/AWl;->i:LX/3HT;

    iget-object v1, p0, LX/AWl;->p:LX/AWj;

    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 1683287
    return-void
.end method

.method public final a(Ljava/lang/String;ILX/AcC;)V
    .locals 1

    .prologue
    .line 1683279
    iget-object v0, p0, LX/AWl;->f:LX/Aby;

    .line 1683280
    invoke-static {v0}, LX/Aby;->l(LX/Aby;)V

    .line 1683281
    iget-object p0, v0, LX/Aby;->a:LX/AcX;

    .line 1683282
    invoke-static {p0, p1, p2, p3}, LX/AcX;->b(LX/AcX;Ljava/lang/String;ILX/AcC;)V

    .line 1683283
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerTargetData;ZFZZZLcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;)V
    .locals 4

    .prologue
    .line 1683230
    iput-boolean p5, p0, LX/AWl;->c:Z

    .line 1683231
    iget-object v0, p0, LX/AWl;->f:LX/Aby;

    iget-object v1, p0, LX/AWl;->a:Lcom/facebook/facecastdisplay/FacecastInteractionView;

    invoke-virtual {v0, v1}, LX/AVi;->a(Landroid/view/View;)V

    .line 1683232
    iget-object v0, p0, LX/AWl;->f:LX/Aby;

    float-to-double v2, p4

    invoke-virtual {v0, v2, v3}, LX/Aby;->a(D)V

    .line 1683233
    iget-object v0, p0, LX/AWl;->f:LX/Aby;

    invoke-virtual {v0, p3}, LX/Aby;->b(Z)V

    .line 1683234
    iget-object v1, p0, LX/AWl;->f:LX/Aby;

    if-eqz p7, :cond_2

    iget-object v0, p0, LX/AWl;->j:LX/1b4;

    .line 1683235
    iget-object v2, v0, LX/1b4;->b:LX/0Uh;

    const/16 v3, 0x344

    const/4 p3, 0x0

    invoke-virtual {v2, v3, p3}, LX/0Uh;->a(IZ)Z

    move-result v2

    move v0, v2

    .line 1683236
    :goto_0
    iput-boolean v0, v1, LX/Aby;->y:Z

    .line 1683237
    iget-object v0, p0, LX/AWl;->f:LX/Aby;

    .line 1683238
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/Aby;->P:Z

    .line 1683239
    invoke-static {v0}, LX/Aby;->k(LX/Aby;)V

    .line 1683240
    iget-object v1, v0, LX/Aby;->a:LX/AcX;

    const/4 p3, 0x1

    .line 1683241
    iput-object p1, v1, LX/AcX;->t:Ljava/lang/String;

    .line 1683242
    sget-object v2, LX/Abz;->BROADCASTING:LX/Abz;

    iput-object v2, v1, LX/AcX;->x:LX/Abz;

    .line 1683243
    invoke-static {v1, p3}, LX/AcX;->e(LX/AcX;Z)V

    .line 1683244
    iget-object v2, v1, LX/AcX;->r:LX/AeJ;

    iget-object v3, v1, LX/AcX;->p:Ljava/lang/String;

    invoke-interface {v2, v3}, LX/AeJ;->c(Ljava/lang/String;)V

    .line 1683245
    iget-object v2, v1, LX/AcX;->r:LX/AeJ;

    invoke-interface {v2, p3}, LX/AeJ;->a(Z)V

    .line 1683246
    iget-object v2, v1, LX/AcX;->p:Ljava/lang/String;

    invoke-static {v2}, LX/AeT;->a(Ljava/lang/String;)LX/AeT;

    move-result-object v2

    iget-object v3, v1, LX/AcX;->p:Ljava/lang/String;

    .line 1683247
    iput-object v3, v2, LX/AeT;->f:Ljava/lang/String;

    .line 1683248
    move-object v2, v2

    .line 1683249
    iput-boolean p3, v2, LX/AeT;->b:Z

    .line 1683250
    goto :goto_1

    :goto_1
    const/4 v3, 0x1

    iput-boolean v3, v2, LX/AeT;->a:Z

    .line 1683251
    move-object v2, v2

    .line 1683252
    iget-object v3, v1, LX/AcX;->t:Ljava/lang/String;

    .line 1683253
    iput-object v3, v2, LX/AeT;->e:Ljava/lang/String;

    .line 1683254
    move-object v2, v2

    .line 1683255
    iput-boolean p6, v2, LX/AeT;->c:Z

    .line 1683256
    move-object v2, v2

    .line 1683257
    invoke-virtual {v2}, LX/AeT;->a()LX/AeU;

    move-result-object v2

    .line 1683258
    iget-object v3, v1, LX/AcX;->n:LX/AeS;

    .line 1683259
    iput-object v2, v3, LX/AeS;->h:LX/AeU;

    .line 1683260
    iget-object v3, v1, LX/AcX;->k:LX/Aev;

    invoke-virtual {v2}, LX/AeU;->a()Z

    move-result p3

    .line 1683261
    iput-boolean p3, v3, LX/Aev;->h:Z

    .line 1683262
    iget-object v3, v1, LX/AcX;->d:LX/1b4;

    invoke-virtual {v3}, LX/1b4;->D()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1683263
    invoke-static {v1, v2}, LX/AcX;->a(LX/AcX;LX/AeU;)V

    .line 1683264
    :cond_0
    iget-object v1, v0, LX/AVi;->a:Landroid/view/View;

    move-object v1, v1

    .line 1683265
    check-cast v1, Lcom/facebook/facecastdisplay/FacecastInteractionView;

    iget-object v1, v1, Lcom/facebook/facecastdisplay/FacecastInteractionView;->i:Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;

    if-eqz v1, :cond_1

    if-eqz p1, :cond_1

    .line 1683266
    iget-object v1, v0, LX/AVi;->a:Landroid/view/View;

    move-object v1, v1

    .line 1683267
    check-cast v1, Lcom/facebook/facecastdisplay/FacecastInteractionView;

    iget-object v1, v1, Lcom/facebook/facecastdisplay/FacecastInteractionView;->i:Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;

    invoke-virtual {v1, p1}, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;->setVideoId(Ljava/lang/String;)V

    .line 1683268
    :cond_1
    new-instance v1, LX/Ade;

    invoke-direct {v1}, LX/Ade;-><init>()V

    iput-object v1, v0, LX/Aby;->t:LX/Ade;

    .line 1683269
    invoke-static {v0, p6}, LX/Aby;->j(LX/Aby;Z)V

    .line 1683270
    iget-object v0, p0, LX/AWl;->f:LX/Aby;

    invoke-virtual {v0, p5}, LX/Aby;->c(Z)V

    .line 1683271
    iget-object v0, p0, LX/AWl;->f:LX/Aby;

    invoke-virtual {v0, p8}, LX/Aby;->a(Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;)V

    .line 1683272
    iget-object v0, p0, LX/AWl;->m:Lcom/facebook/facecast/plugin/FacecastToolbarContainer;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerTargetData;)V

    .line 1683273
    iget-object v0, p0, LX/AWl;->f:LX/Aby;

    invoke-virtual {v0}, LX/Aby;->e()V

    .line 1683274
    return-void

    .line 1683275
    :cond_2
    iget-object v0, p0, LX/AWl;->j:LX/1b4;

    .line 1683276
    iget-object v2, v0, LX/1b4;->a:LX/0ad;

    sget-short v3, LX/1v6;->X:S

    const/4 p3, 0x0

    invoke-interface {v2, v3, p3}, LX/0ad;->a(SZ)Z

    move-result v2

    move v0, v2

    .line 1683277
    goto/16 :goto_0
    .line 1683278
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 1683212
    iget-object v0, p0, LX/AWl;->n:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbButton;->setEnabled(Z)V

    .line 1683213
    return-void
.end method

.method public getToolbarContainer()Landroid/view/View;
    .locals 1

    .prologue
    .line 1683229
    iget-object v0, p0, LX/AWl;->m:Lcom/facebook/facecast/plugin/FacecastToolbarContainer;

    return-object v0
.end method

.method public final iF_()V
    .locals 2

    .prologue
    .line 1683225
    invoke-super {p0}, LX/AWT;->iF_()V

    .line 1683226
    iget-object v0, p0, LX/AWT;->e:LX/AVF;

    invoke-virtual {v0, p0}, LX/AVF;->a(LX/1b9;)V

    .line 1683227
    iget-object v0, p0, LX/AWl;->i:LX/3HT;

    iget-object v1, p0, LX/AWl;->p:LX/AWj;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 1683228
    return-void
.end method

.method public setListener(LX/AWi;)V
    .locals 0

    .prologue
    .line 1683223
    iput-object p1, p0, LX/AWl;->l:LX/AWi;

    .line 1683224
    return-void
.end method

.method public setSuspended(Z)V
    .locals 2

    .prologue
    .line 1683214
    iget-object v0, p0, LX/AWl;->f:LX/Aby;

    .line 1683215
    iget-object v1, v0, LX/Aby;->a:LX/AcX;

    .line 1683216
    iget-object p0, v1, LX/AcX;->r:LX/AeJ;

    invoke-static {p0}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1683217
    iget-object p0, v1, LX/AcX;->r:LX/AeJ;

    invoke-interface {p0, p1}, LX/AeJ;->b(Z)V

    .line 1683218
    iget-object v1, v0, LX/AVi;->a:Landroid/view/View;

    move-object v1, v1

    .line 1683219
    check-cast v1, Lcom/facebook/facecastdisplay/FacecastInteractionView;

    iget-object v1, v1, Lcom/facebook/facecastdisplay/FacecastInteractionView;->i:Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;

    if-eqz v1, :cond_0

    .line 1683220
    iget-object v1, v0, LX/AVi;->a:Landroid/view/View;

    move-object v1, v1

    .line 1683221
    check-cast v1, Lcom/facebook/facecastdisplay/FacecastInteractionView;

    iget-object v1, v1, Lcom/facebook/facecastdisplay/FacecastInteractionView;->i:Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;

    invoke-virtual {v1, p1}, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;->setSuspended(Z)V

    .line 1683222
    :cond_0
    return-void
.end method
