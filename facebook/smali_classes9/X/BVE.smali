.class public LX/BVE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/hardware/SensorEventListener;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/BVE;


# instance fields
.field public final a:Landroid/hardware/SensorManager;

.field public b:Landroid/hardware/Sensor;

.field public c:Landroid/hardware/Sensor;

.field public final d:LX/BVD;

.field public final e:LX/BVD;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1790275
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1790276
    new-instance v0, LX/BVD;

    invoke-direct {v0}, LX/BVD;-><init>()V

    iput-object v0, p0, LX/BVE;->d:LX/BVD;

    .line 1790277
    new-instance v0, LX/BVD;

    invoke-direct {v0}, LX/BVD;-><init>()V

    iput-object v0, p0, LX/BVE;->e:LX/BVD;

    .line 1790278
    const-string v0, "sensor"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, LX/BVE;->a:Landroid/hardware/SensorManager;

    .line 1790279
    return-void
.end method

.method public static a(LX/0QB;)LX/BVE;
    .locals 4

    .prologue
    .line 1790280
    sget-object v0, LX/BVE;->f:LX/BVE;

    if-nez v0, :cond_1

    .line 1790281
    const-class v1, LX/BVE;

    monitor-enter v1

    .line 1790282
    :try_start_0
    sget-object v0, LX/BVE;->f:LX/BVE;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1790283
    if-eqz v2, :cond_0

    .line 1790284
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1790285
    new-instance p0, LX/BVE;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-direct {p0, v3}, LX/BVE;-><init>(Landroid/content/Context;)V

    .line 1790286
    move-object v0, p0

    .line 1790287
    sput-object v0, LX/BVE;->f:LX/BVE;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1790288
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1790289
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1790290
    :cond_1
    sget-object v0, LX/BVE;->f:LX/BVE;

    return-object v0

    .line 1790291
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1790292
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0

    .prologue
    .line 1790293
    return-void
.end method

.method public final onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1790294
    iget-object v0, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v0}, Landroid/hardware/Sensor;->getType()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 1790295
    :goto_0
    return-void

    .line 1790296
    :sswitch_0
    iget-object v0, p0, LX/BVE;->d:LX/BVD;

    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v1, v1, v2

    iput v1, v0, LX/BVD;->a:F

    .line 1790297
    iget-object v0, p0, LX/BVE;->d:LX/BVD;

    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v1, v1, v3

    iput v1, v0, LX/BVD;->b:F

    .line 1790298
    iget-object v0, p0, LX/BVE;->d:LX/BVD;

    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v1, v1, v4

    iput v1, v0, LX/BVD;->c:F

    goto :goto_0

    .line 1790299
    :sswitch_1
    iget-object v0, p0, LX/BVE;->e:LX/BVD;

    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v1, v1, v2

    iput v1, v0, LX/BVD;->a:F

    .line 1790300
    iget-object v0, p0, LX/BVE;->e:LX/BVD;

    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v1, v1, v3

    iput v1, v0, LX/BVD;->b:F

    .line 1790301
    iget-object v0, p0, LX/BVE;->e:LX/BVD;

    iget-object v1, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v1, v1, v4

    iput v1, v0, LX/BVD;->c:F

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_1
        0xa -> :sswitch_0
    .end sparse-switch
.end method
