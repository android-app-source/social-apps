.class public final LX/Amc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/4cn;


# instance fields
.field public final synthetic a:LX/1qH;

.field public final synthetic b:Z

.field public final synthetic c:Lcom/facebook/feed/server/NewsFeedServiceImplementation;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/server/NewsFeedServiceImplementation;LX/1qH;Z)V
    .locals 0

    .prologue
    .line 1711121
    iput-object p1, p0, LX/Amc;->c:Lcom/facebook/feed/server/NewsFeedServiceImplementation;

    iput-object p2, p0, LX/Amc;->a:LX/1qH;

    iput-boolean p3, p0, LX/Amc;->b:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 1711122
    instance-of v0, p1, LX/4cm;

    if-nez v0, :cond_0

    iget-object v0, p0, LX/Amc;->a:LX/1qH;

    if-eqz v0, :cond_0

    .line 1711123
    iget-object v0, p0, LX/Amc;->a:LX/1qH;

    invoke-static {p1}, Lcom/facebook/fbservice/service/OperationResult;->forException(Ljava/lang/Throwable;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1qH;->onOperationProgress(Lcom/facebook/fbservice/service/OperationResult;)V

    .line 1711124
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1711116
    iget-object v0, p0, LX/Amc;->a:LX/1qH;

    if-eqz v0, :cond_0

    .line 1711117
    iget-object v0, p0, LX/Amc;->c:Lcom/facebook/feed/server/NewsFeedServiceImplementation;

    iget-object v0, v0, Lcom/facebook/feed/server/NewsFeedServiceImplementation;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/util/injection/FeedClientSideInjectionTool;

    check-cast p1, Lcom/facebook/api/feed/FetchFeedResult;

    invoke-virtual {v0, p1}, Lcom/facebook/feed/util/injection/FeedClientSideInjectionTool;->a(Lcom/facebook/api/feed/FetchFeedResult;)Lcom/facebook/api/feed/FetchFeedResult;

    move-result-object v1

    .line 1711118
    iget-object v0, p0, LX/Amc;->c:Lcom/facebook/feed/server/NewsFeedServiceImplementation;

    iget-object v0, v0, Lcom/facebook/feed/server/NewsFeedServiceImplementation;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0qU;

    iget-boolean v2, p0, LX/Amc;->b:Z

    invoke-virtual {v0, v1, v2}, LX/0qU;->a(Lcom/facebook/api/feed/FetchFeedResult;Z)Lcom/facebook/api/feed/FetchFeedResult;

    move-result-object v0

    .line 1711119
    iget-object v1, p0, LX/Amc;->a:LX/1qH;

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    invoke-interface {v1, v0}, LX/1qH;->onOperationProgress(Lcom/facebook/fbservice/service/OperationResult;)V

    .line 1711120
    :cond_0
    return-void
.end method
