.class public final LX/AgT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/AgU;


# direct methods
.method public constructor <init>(LX/AgU;)V
    .locals 0

    .prologue
    .line 1700886
    iput-object p1, p0, LX/AgT;->a:LX/AgU;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 13

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x2

    const v2, -0x1895015f

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 1700887
    iget-object v0, p0, LX/AgT;->a:LX/AgU;

    .line 1700888
    iget-object v3, v0, LX/AVi;->a:Landroid/view/View;

    move-object v3, v3

    .line 1700889
    move-object v0, v3

    .line 1700890
    check-cast v0, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsInputView;

    invoke-virtual {v0, p1}, Lcom/facebook/facecastdisplay/streamingreactions/StreamingReactionsInputView;->setSelectedReaction(Landroid/view/View;)V

    .line 1700891
    check-cast p1, LX/AgD;

    .line 1700892
    iget v0, p1, LX/AgD;->a:I

    move v3, v0

    .line 1700893
    iget-object v0, p0, LX/AgT;->a:LX/AgU;

    iget-object v0, v0, LX/AgU;->c:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v4

    .line 1700894
    iget-object v0, p0, LX/AgT;->a:LX/AgU;

    iget-wide v6, v0, LX/AgU;->f:J

    sub-long v6, v4, v6

    .line 1700895
    const-wide/16 v8, 0x190

    cmp-long v0, v6, v8

    if-lez v0, :cond_1

    iget-object v0, p0, LX/AgT;->a:LX/AgU;

    iget-object v0, v0, LX/AgU;->e:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_1

    .line 1700896
    iget-object v0, p0, LX/AgT;->a:LX/AgU;

    iget-object v0, v0, LX/AgU;->b:LX/AgV;

    iget-object v6, p0, LX/AgT;->a:LX/AgU;

    iget-object v6, v6, LX/AgU;->e:Lcom/facebook/graphql/model/GraphQLStory;

    iget-object v7, p0, LX/AgT;->a:LX/AgU;

    iget v7, v7, LX/AgU;->g:F

    float-to-int v7, v7

    .line 1700897
    if-eqz v6, :cond_0

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v8

    if-nez v8, :cond_4

    .line 1700898
    :cond_0
    :goto_0
    iget-object v0, p0, LX/AgT;->a:LX/AgU;

    .line 1700899
    iput-wide v4, v0, LX/AgU;->f:J

    .line 1700900
    :cond_1
    iget-object v0, p0, LX/AgT;->a:LX/AgU;

    iget-object v0, v0, LX/AgU;->h:LX/Adu;

    if-eqz v0, :cond_2

    .line 1700901
    iget-object v0, p0, LX/AgT;->a:LX/AgU;

    iget-object v6, v0, LX/AgU;->h:LX/Adu;

    iget-object v0, p0, LX/AgT;->a:LX/AgU;

    iget-wide v8, v0, LX/AgU;->f:J

    cmp-long v0, v4, v8

    if-nez v0, :cond_3

    move v0, v1

    .line 1700902
    :goto_1
    iget-object v1, v6, LX/Adu;->a:LX/Ae2;

    invoke-static {v1}, LX/Ae2;->E(LX/Ae2;)V

    .line 1700903
    iget-object v1, v6, LX/Adu;->a:LX/Ae2;

    iget-object v1, v1, LX/Ae2;->Q:LX/3Gt;

    if-eqz v1, :cond_2

    .line 1700904
    iget-object v1, v6, LX/Adu;->a:LX/Ae2;

    iget-object v1, v1, LX/Ae2;->Q:LX/3Gt;

    invoke-interface {v1, v3, v0}, LX/3Gt;->a(IZ)V

    .line 1700905
    :cond_2
    const v0, -0x52a911f0

    invoke-static {v0, v2}, LX/02F;->a(II)V

    return-void

    .line 1700906
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 1700907
    :cond_4
    invoke-static {v6}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v8

    .line 1700908
    if-eqz v8, :cond_0

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v9

    if-eqz v9, :cond_0

    .line 1700909
    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLMedia;->ao()Z

    move-result v8

    .line 1700910
    new-instance v9, LX/6Rm;

    invoke-direct {v9}, LX/6Rm;-><init>()V

    move-object v9, v9

    .line 1700911
    new-instance v10, LX/4Ej;

    invoke-direct {v10}, LX/4Ej;-><init>()V

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v11

    invoke-virtual {v11}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v11

    .line 1700912
    const-string v12, "feedback_id"

    invoke-virtual {v10, v12, v11}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1700913
    move-object v10, v10

    .line 1700914
    iget-object v11, v0, LX/AgV;->b:Ljava/lang/String;

    .line 1700915
    const-string v12, "actor_id"

    invoke-virtual {v10, v12, v11}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1700916
    move-object v10, v10

    .line 1700917
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    .line 1700918
    const-string v12, "reaction_key"

    invoke-virtual {v10, v12, v11}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1700919
    move-object v10, v10

    .line 1700920
    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/String;

    const/4 v12, 0x0

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStory;->c()Ljava/lang/String;

    move-result-object p1

    aput-object p1, v11, v12

    invoke-static {v11}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v11

    .line 1700921
    const-string v12, "tracking"

    invoke-virtual {v10, v12, v11}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 1700922
    move-object v10, v10

    .line 1700923
    if-eqz v8, :cond_5

    .line 1700924
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    .line 1700925
    const-string v11, "content_time_offset"

    invoke-virtual {v10, v11, v8}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1700926
    :goto_2
    const-string v8, "input"

    invoke-virtual {v9, v8, v10}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1700927
    invoke-static {v9}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v8

    .line 1700928
    iget-object v9, v0, LX/AgV;->a:LX/0tX;

    invoke-virtual {v9, v8}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto/16 :goto_0

    .line 1700929
    :cond_5
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    .line 1700930
    const-string v11, "on_demand_content_time_offset"

    invoke-virtual {v10, v11, v8}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1700931
    goto :goto_2
.end method
