.class public LX/BfG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/BfC;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/BfC",
        "<",
        "Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsInterfaces$SuggestEditsHeader$;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static volatile d:LX/BfG;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:LX/03V;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1805812
    const-class v0, LX/BfG;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/BfG;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1805813
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1805814
    iput-object p1, p0, LX/BfG;->b:Landroid/content/Context;

    .line 1805815
    iput-object p2, p0, LX/BfG;->c:LX/03V;

    .line 1805816
    return-void
.end method

.method public static a(LX/0QB;)LX/BfG;
    .locals 5

    .prologue
    .line 1805817
    sget-object v0, LX/BfG;->d:LX/BfG;

    if-nez v0, :cond_1

    .line 1805818
    const-class v1, LX/BfG;

    monitor-enter v1

    .line 1805819
    :try_start_0
    sget-object v0, LX/BfG;->d:LX/BfG;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1805820
    if-eqz v2, :cond_0

    .line 1805821
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1805822
    new-instance p0, LX/BfG;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-direct {p0, v3, v4}, LX/BfG;-><init>(Landroid/content/Context;LX/03V;)V

    .line 1805823
    move-object v0, p0

    .line 1805824
    sput-object v0, LX/BfG;->d:LX/BfG;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1805825
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1805826
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1805827
    :cond_1
    sget-object v0, LX/BfG;->d:LX/BfG;

    return-object v0

    .line 1805828
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1805829
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/BeD;
    .locals 1

    .prologue
    .line 1805830
    sget-object v0, LX/BeD;->PHOTO_PICKER:LX/BeD;

    return-object v0
.end method

.method public final a(Ljava/lang/Object;Landroid/support/v4/app/Fragment;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 1805831
    iget-object v0, p0, LX/BfG;->b:Landroid/content/Context;

    new-instance v1, LX/8AA;

    sget-object v2, LX/8AB;->SUGGEST_EDITS:LX/8AB;

    invoke-direct {v1, v2}, LX/8AA;-><init>(LX/8AB;)V

    invoke-virtual {v1}, LX/8AA;->l()LX/8AA;

    move-result-object v1

    invoke-virtual {v1}, LX/8AA;->i()LX/8AA;

    move-result-object v1

    invoke-virtual {v1}, LX/8AA;->j()LX/8AA;

    move-result-object v1

    sget-object v2, LX/8A9;->LAUNCH_GENERIC_CROPPER:LX/8A9;

    invoke-virtual {v1, v2}, LX/8AA;->a(LX/8A9;)LX/8AA;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/ipc/simplepicker/SimplePickerIntent;->a(Landroid/content/Context;LX/8AA;)Landroid/content/Intent;

    move-result-object v0

    .line 1805832
    return-object v0
.end method

.method public final a(Landroid/content/Intent;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1805833
    check-cast p2, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;

    .line 1805834
    sget-object v0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;

    .line 1805835
    if-eqz v0, :cond_0

    .line 1805836
    iget-object v1, v0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->b:Landroid/net/Uri;

    move-object v1, v1

    .line 1805837
    if-nez v1, :cond_1

    .line 1805838
    :cond_0
    iget-object v0, p0, LX/BfG;->c:LX/03V;

    sget-object v1, LX/BfG;->a:Ljava/lang/String;

    const-string v2, "No cropped photo URI returned"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1805839
    const/4 v0, 0x0

    .line 1805840
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "file://"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, v0}, LX/BgV;->b(Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;Ljava/lang/String;)Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;

    move-result-object v0

    .line 1805841
    return-object v0

    .line 1805842
    :cond_1
    iget-object v1, v0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->b:Landroid/net/Uri;

    move-object v0, v1

    .line 1805843
    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
