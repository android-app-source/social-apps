.class public LX/Ast;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/Ass;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1721043
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 1721044
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/Ar2;Landroid/view/ViewStub;LX/Ata;)LX/Ass;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ModelData:",
            "Ljava/lang/Object;",
            ":",
            "Lcom/facebook/friendsharing/inspiration/model/CameraStateSpec$ProvidesCameraState;",
            ":",
            "LX/0io;",
            ":",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationStateSpec$ProvidesInspirationState;",
            "DerivedData:",
            "Ljava/lang/Object;",
            "Mutation:",
            "Ljava/lang/Object;",
            ":",
            "Lcom/facebook/ipc/composer/dataaccessor/ComposerCanSave;",
            ":",
            "Lcom/facebook/friendsharing/inspiration/model/CameraStateSpec$SetsCameraState",
            "<TMutation;>;:",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationStateSpec$SetsInspirationState",
            "<TMutation;>;Services:",
            "Ljava/lang/Object;",
            ":",
            "LX/0il",
            "<TModelData;>;:",
            "LX/0ik",
            "<TDerivedData;>;:",
            "LX/0im",
            "<TMutation;>;>(TServices;",
            "Lcom/facebook/friendsharing/inspiration/controller/InspirationCaptureButtonController$Delegate;",
            "Landroid/view/ViewStub;",
            "LX/Ata;",
            ")",
            "LX/Ass",
            "<TModelData;TDerivedData;TMutation;TServices;>;"
        }
    .end annotation

    .prologue
    .line 1721045
    new-instance v0, LX/Ass;

    move-object v1, p1

    check-cast v1, LX/0il;

    invoke-static {p0}, LX/0wW;->b(LX/0QB;)LX/0wW;

    move-result-object v5

    check-cast v5, LX/0wW;

    const-class v2, Landroid/content/Context;

    invoke-interface {p0, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/Context;

    invoke-static {p0}, LX/Aw7;->b(LX/0QB;)LX/Aw7;

    move-result-object v7

    check-cast v7, LX/Aw7;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v8

    check-cast v8, LX/03V;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v9

    check-cast v9, LX/0SG;

    invoke-static {p0}, LX/0fO;->b(LX/0QB;)LX/0fO;

    move-result-object v10

    check-cast v10, LX/0fO;

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v10}, LX/Ass;-><init>(LX/0il;LX/Ar2;Landroid/view/ViewStub;LX/Ata;LX/0wW;Landroid/content/Context;LX/Aw7;LX/03V;LX/0SG;LX/0fO;)V

    .line 1721046
    return-object v0
.end method
