.class public final LX/Bjm;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageEventCategoriesQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Bin;

.field public final synthetic b:LX/Bjn;


# direct methods
.method public constructor <init>(LX/Bjn;LX/Bin;)V
    .locals 0

    .prologue
    .line 1812805
    iput-object p1, p0, LX/Bjm;->b:LX/Bjn;

    iput-object p2, p0, LX/Bjm;->a:LX/Bin;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1812806
    iget-object v0, p0, LX/Bjm;->a:LX/Bin;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/Bin;->a(LX/15i;I)V

    .line 1812807
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1812808
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1812809
    if-eqz p1, :cond_0

    .line 1812810
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1812811
    if-eqz v0, :cond_0

    .line 1812812
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1812813
    check-cast v0, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageEventCategoriesQueryModel;

    invoke-virtual {v0}, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageEventCategoriesQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 1812814
    sget-object v2, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1812815
    iget-object v2, p0, LX/Bjm;->a:LX/Bin;

    invoke-virtual {v2, v1, v0}, LX/Bin;->a(LX/15i;I)V

    .line 1812816
    :cond_0
    return-void

    .line 1812817
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
