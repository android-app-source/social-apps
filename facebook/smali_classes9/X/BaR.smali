.class public final LX/BaR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;

.field public final synthetic b:I

.field public final synthetic c:Lcom/facebook/audience/snacks/view/snacks_row/ProfileViewHolder;


# direct methods
.method public constructor <init>(Lcom/facebook/audience/snacks/view/snacks_row/ProfileViewHolder;Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;I)V
    .locals 0

    .prologue
    .line 1799079
    iput-object p1, p0, LX/BaR;->c:Lcom/facebook/audience/snacks/view/snacks_row/ProfileViewHolder;

    iput-object p2, p0, LX/BaR;->a:Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;

    iput p3, p0, LX/BaR;->b:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x2

    const v1, -0x1ac61a11

    invoke-static {v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v7

    .line 1799057
    iget-object v0, p0, LX/BaR;->a:Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;

    .line 1799058
    iget-object v1, v0, Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;->e:LX/0Px;

    move-object v0, v1

    .line 1799059
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/BaR;->a:Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;

    .line 1799060
    iget-object v1, v0, Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;->e:LX/0Px;

    move-object v0, v1

    .line 1799061
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1799062
    iget-object v0, p0, LX/BaR;->c:Lcom/facebook/audience/snacks/view/snacks_row/ProfileViewHolder;

    iget-object v0, v0, Lcom/facebook/audience/snacks/view/snacks_row/ProfileViewHolder;->q:LX/BaM;

    iget-object v1, p0, LX/BaR;->c:Lcom/facebook/audience/snacks/view/snacks_row/ProfileViewHolder;

    iget-object v1, v1, Lcom/facebook/audience/snacks/view/snacks_row/ProfileViewHolder;->p:Landroid/content/Context;

    iget-object v2, p0, LX/BaR;->a:Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;

    .line 1799063
    iget-object v4, v0, LX/BaM;->a:LX/17Y;

    sget-object v5, LX/0ax;->gr:Ljava/lang/String;

    invoke-interface {v4, v1, v5}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v5

    .line 1799064
    const-string v4, "extra_snack_user_thread_id"

    .line 1799065
    iget-object v6, v2, Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;->c:Lcom/facebook/audience/model/AudienceControlData;

    move-object v6, v6

    .line 1799066
    invoke-virtual {v6}, Lcom/facebook/audience/model/AudienceControlData;->getId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1799067
    const-class v4, Landroid/app/Activity;

    invoke-static {v1, v4}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/Activity;

    .line 1799068
    iget-object v6, v0, LX/BaM;->b:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v6, v5, v4}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1799069
    invoke-static {v4}, LX/BaM;->a(Landroid/app/Activity;)V

    .line 1799070
    iget-object v0, p0, LX/BaR;->c:Lcom/facebook/audience/snacks/view/snacks_row/ProfileViewHolder;

    iget-object v0, v0, Lcom/facebook/audience/snacks/view/snacks_row/ProfileViewHolder;->r:LX/7ga;

    iget-object v1, p0, LX/BaR;->a:Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;

    .line 1799071
    iget-object v2, v1, Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;->c:Lcom/facebook/audience/model/AudienceControlData;

    move-object v1, v2

    .line 1799072
    invoke-virtual {v1}, Lcom/facebook/audience/model/AudienceControlData;->getId()Ljava/lang/String;

    move-result-object v1

    iget v2, p0, LX/BaR;->b:I

    iget-object v4, p0, LX/BaR;->a:Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;

    .line 1799073
    iget v5, v4, Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;->f:I

    move v4, v5

    .line 1799074
    if-eqz v4, :cond_1

    :goto_0
    sget-object v4, LX/7gZ;->FRIEND:LX/7gZ;

    iget-object v5, p0, LX/BaR;->a:Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;

    .line 1799075
    iget-object v6, v5, Lcom/facebook/audience/snacks/model/SnacksFriendUserThread;->e:LX/0Px;

    move-object v5, v6

    .line 1799076
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v5

    sget-object v6, LX/7gd;->STORY_TRAY:LX/7gd;

    invoke-virtual/range {v0 .. v6}, LX/7ga;->a(Ljava/lang/String;IZLX/7gZ;ILX/7gd;)V

    .line 1799077
    :cond_0
    const v0, 0x53fe6340

    invoke-static {v0, v7}, LX/02F;->a(II)V

    return-void

    .line 1799078
    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method
