.class public LX/AXY;
.super LX/AWT;
.source ""

# interfaces
.implements LX/AXW;
.implements LX/AXX;


# static fields
.field public static final x:Ljava/lang/String;

.field public static final y:LX/0Tn;

.field public static final z:LX/0Tn;


# instance fields
.field public A:Lcom/facebook/facecast/view/FacecastVideoPlaybackView;

.field private B:Lcom/facebook/fig/button/FigButton;

.field private C:Lcom/facebook/fig/button/FigButton;

.field public final D:Landroid/view/ViewStub;

.field private final E:Landroid/view/ViewStub;

.field private final F:Landroid/view/ViewStub;

.field private final G:Landroid/view/ViewStub;

.field public H:Lcom/facebook/facecast/FacecastFacepileView;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public I:Lcom/facebook/facecast/view/FacecastPreviewSaveButton;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public J:Lcom/facebook/facecast/view/FacecastHdUploadButton;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private K:Lcom/facebook/widget/text/BetterTextView;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private L:Lcom/facebook/widget/text/BetterTextView;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final M:Lcom/facebook/widget/text/BetterTextView;

.field public final N:Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;

.field public final O:Lcom/facebook/facecast/view/FacecastVideoPlaybackContainer;

.field private P:Landroid/view/View;

.field private final Q:Landroid/widget/ProgressBar;

.field private final R:Ljava/lang/String;

.field public final S:Ljava/lang/String;

.field public final T:Ljava/lang/String;

.field public final U:Ljava/lang/String;

.field public V:LX/AUt;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public W:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public a:Lcom/facebook/facecast/protocol/FacecastNetworker;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public aa:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ab:Ljava/io/File;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public ac:J

.field public ad:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

.field public ae:Lcom/facebook/facecast/model/FacecastPrivacyData;

.field private af:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

.field private ag:Z

.field public ah:J

.field public ai:LX/AYh;

.field public aj:LX/AXD;

.field public final ak:LX/AXF;

.field public b:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/AYi;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/6Rg;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:Lcom/facebook/video/videostreaming/RtmpLiveStreamer;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/AVK;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/AV9;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/0kL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/AVT;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/AXi;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/8RJ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:Lcom/facebook/privacy/PrivacyOperationsClient;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/3Hc;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/Acy;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/AaX;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/AaQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/3kp;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/3kp;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/1b4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:LX/39G;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1684190
    const-class v0, LX/AXY;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/AXY;->x:Ljava/lang/String;

    .line 1684191
    sget-object v0, LX/0Tm;->g:LX/0Tn;

    const-string v1, "tip_jar_end_screen_turn_on_nux_shown"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/AXY;->y:LX/0Tn;

    .line 1684192
    sget-object v0, LX/0Tm;->g:LX/0Tn;

    const-string v1, "donation_end_screen_turn_on_nux_shown"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/AXY;->z:LX/0Tn;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1684193
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/AXY;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1684194
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1684195
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/AXY;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1684196
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 8
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const-wide/16 v6, 0x0

    const/4 v4, 0x0

    .line 1684197
    invoke-direct {p0, p1, p2, p3}, LX/AWT;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1684198
    iput-wide v6, p0, LX/AXY;->ac:J

    .line 1684199
    new-instance v0, LX/AWS;

    invoke-direct {v0}, LX/AWS;-><init>()V

    invoke-virtual {v0}, LX/AWS;->a()Lcom/facebook/facecast/model/FacecastPrivacyData;

    move-result-object v0

    iput-object v0, p0, LX/AXY;->ae:Lcom/facebook/facecast/model/FacecastPrivacyData;

    .line 1684200
    new-instance v0, LX/AXF;

    invoke-direct {v0, p0}, LX/AXF;-><init>(LX/AXY;)V

    iput-object v0, p0, LX/AXY;->ak:LX/AXF;

    .line 1684201
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p0, v1}, LX/AXY;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1684202
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget-object v1, LX/03r;->FacecastEndScreen:[I

    const v2, 0x7f010460

    invoke-virtual {v0, p2, v1, v2, p3}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1684203
    const/16 v1, 0x0

    invoke-static {p1, v0, v1}, LX/1z0;->a(Landroid/content/Context;Landroid/content/res/TypedArray;I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, LX/AXY;->R:Ljava/lang/String;

    .line 1684204
    const/16 v1, 0x1

    invoke-static {p1, v0, v1}, LX/1z0;->a(Landroid/content/Context;Landroid/content/res/TypedArray;I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, LX/AXY;->S:Ljava/lang/String;

    .line 1684205
    const/16 v1, 0x2

    invoke-static {p1, v0, v1}, LX/1z0;->a(Landroid/content/Context;Landroid/content/res/TypedArray;I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, LX/AXY;->T:Ljava/lang/String;

    .line 1684206
    const/16 v1, 0x4

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    .line 1684207
    const/16 v2, 0x3

    invoke-static {p1, v0, v2}, LX/1z0;->a(Landroid/content/Context;Landroid/content/res/TypedArray;I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, LX/AXY;->U:Ljava/lang/String;

    .line 1684208
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1684209
    const v0, 0x7f0305dc

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1684210
    const v0, 0x7f0d0f72

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecast/view/FacecastVideoPlaybackView;

    iput-object v0, p0, LX/AXY;->A:Lcom/facebook/facecast/view/FacecastVideoPlaybackView;

    .line 1684211
    const v0, 0x7f0d0ffc

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, LX/AXY;->D:Landroid/view/ViewStub;

    .line 1684212
    const v0, 0x7f0d0ffd

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/AXY;->M:Lcom/facebook/widget/text/BetterTextView;

    .line 1684213
    const v0, 0x7f0d1027

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;

    iput-object v0, p0, LX/AXY;->N:Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;

    .line 1684214
    const v0, 0x7f0d101e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, LX/AXY;->Q:Landroid/widget/ProgressBar;

    .line 1684215
    const v0, 0x7f0d101d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecast/view/FacecastVideoPlaybackContainer;

    iput-object v0, p0, LX/AXY;->O:Lcom/facebook/facecast/view/FacecastVideoPlaybackContainer;

    .line 1684216
    const v0, 0x7f0d1025

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, LX/AXY;->E:Landroid/view/ViewStub;

    .line 1684217
    const v0, 0x7f0d1024

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, LX/AXY;->F:Landroid/view/ViewStub;

    .line 1684218
    const v0, 0x7f0d1026

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, LX/AXY;->G:Landroid/view/ViewStub;

    .line 1684219
    iget-object v0, p0, LX/AXY;->g:Lcom/facebook/video/videostreaming/RtmpLiveStreamer;

    invoke-virtual {v0}, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->s()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/AXY;->g:Lcom/facebook/video/videostreaming/RtmpLiveStreamer;

    invoke-virtual {v0}, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->t()Ljava/io/File;

    move-result-object v0

    :goto_0
    iput-object v0, p0, LX/AXY;->ab:Ljava/io/File;

    .line 1684220
    iget-object v0, p0, LX/AXY;->ab:Ljava/io/File;

    if-nez v0, :cond_3

    .line 1684221
    iget-object v0, p0, LX/AXY;->b:LX/03V;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, LX/AXY;->x:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_<cstr>"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Recorded local file from live does not exist"

    invoke-virtual {v0, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1684222
    iget-object v0, p0, LX/AXY;->Q:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1684223
    :goto_1
    const v0, 0x7f0d0ff8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    .line 1684224
    iget-object v2, p0, LX/AXY;->R:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1684225
    invoke-direct {p0}, LX/AXY;->g()V

    .line 1684226
    invoke-direct {p0}, LX/AXY;->h()V

    .line 1684227
    iget-object v0, p0, LX/AXY;->ab:Ljava/io/File;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/AXY;->ab:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v2

    cmp-long v0, v2, v6

    if-lez v0, :cond_1

    .line 1684228
    iget-object v0, p0, LX/AXY;->f:LX/6Rg;

    invoke-virtual {v0}, LX/6Rg;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1684229
    iget-object v0, p0, LX/AXY;->i:LX/AV9;

    iget-object v2, p0, LX/AXY;->ab:Ljava/io/File;

    iget-object v3, p0, LX/AXY;->ak:LX/AXF;

    .line 1684230
    new-instance v6, LX/AV8;

    invoke-static {v0}, LX/2Ic;->a(LX/0QB;)Ljava/lang/String;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v0}, LX/2Ib;->a(LX/0QB;)LX/2Ib;

    move-result-object v5

    check-cast v5, LX/2Ib;

    invoke-direct {v6, v2, v3, v4, v5}, LX/AV8;-><init>(Ljava/io/File;LX/AXF;Ljava/lang/String;LX/2Ib;)V

    .line 1684231
    move-object v2, v6

    .line 1684232
    invoke-virtual {v2}, LX/AV8;->a()Z

    move-result v0

    if-nez v0, :cond_4

    .line 1684233
    :cond_0
    :goto_2
    iget-object v0, p0, LX/AXY;->f:LX/6Rg;

    invoke-virtual {v0}, LX/6Rg;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz v1, :cond_1

    .line 1684234
    invoke-direct {p0}, LX/AXY;->n()V

    .line 1684235
    :cond_1
    const v0, 0x7f0d0ff7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 1684236
    const v1, 0x7f0d101f

    invoke-virtual {p0, v1}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 1684237
    const v2, 0x7f0d101b

    invoke-virtual {p0, v2}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ScrollView;

    .line 1684238
    new-instance v3, LX/AXD;

    iget-object v4, p0, LX/AXY;->O:Lcom/facebook/facecast/view/FacecastVideoPlaybackContainer;

    invoke-direct {v3, v0, v2, v4, v1}, LX/AXD;-><init>(Landroid/view/ViewGroup;Landroid/widget/ScrollView;Lcom/facebook/facecast/view/FacecastVideoPlaybackContainer;Landroid/view/ViewGroup;)V

    iput-object v3, p0, LX/AXY;->aj:LX/AXD;

    .line 1684239
    iget-object v0, p0, LX/AXY;->aj:LX/AXD;

    new-instance v1, LX/AXP;

    invoke-direct {v1, p0}, LX/AXP;-><init>(LX/AXY;)V

    .line 1684240
    iput-object v1, v0, LX/AXD;->i:LX/AXP;

    .line 1684241
    return-void

    .line 1684242
    :cond_2
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 1684243
    :cond_3
    iget-object v0, p0, LX/AXY;->ab:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v2

    iput-wide v2, p0, LX/AXY;->ac:J

    .line 1684244
    iget-object v0, p0, LX/AXY;->ab:Ljava/io/File;

    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {p0, v0}, LX/AXY;->a$redex0(LX/AXY;Landroid/net/Uri;)V

    goto/16 :goto_1

    .line 1684245
    :cond_4
    const v0, 0x7f0d1021

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 1684246
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecast/view/FacecastPreviewSaveButton;

    iput-object v0, p0, LX/AXY;->I:Lcom/facebook/facecast/view/FacecastPreviewSaveButton;

    .line 1684247
    iget-object v0, p0, LX/AXY;->I:Lcom/facebook/facecast/view/FacecastPreviewSaveButton;

    new-instance v3, LX/AXR;

    invoke-direct {v3, p0, v2}, LX/AXR;-><init>(LX/AXY;LX/AV8;)V

    invoke-virtual {v0, v3}, Lcom/facebook/facecast/view/FacecastPreviewSaveButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_2
.end method

.method private static a(LX/AXY;Lcom/facebook/facecast/protocol/FacecastNetworker;LX/03V;LX/AYi;LX/6Rg;Lcom/facebook/video/videostreaming/RtmpLiveStreamer;LX/AVK;LX/AV9;LX/0kL;LX/AVT;LX/AXi;LX/8RJ;Lcom/facebook/privacy/PrivacyOperationsClient;LX/1Ck;LX/3Hc;LX/Acy;LX/0Ot;LX/AaQ;LX/3kp;LX/3kp;LX/1b4;LX/39G;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/AXY;",
            "Lcom/facebook/facecast/protocol/FacecastNetworker;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/AYi;",
            "LX/6Rg;",
            "Lcom/facebook/video/videostreaming/RtmpLiveStreamer;",
            "LX/AVK;",
            "LX/AV9;",
            "LX/0kL;",
            "LX/AVT;",
            "LX/AXi;",
            "LX/8RJ;",
            "Lcom/facebook/privacy/PrivacyOperationsClient;",
            "LX/1Ck;",
            "LX/3Hc;",
            "LX/Acy;",
            "LX/0Ot",
            "<",
            "LX/AaX;",
            ">;",
            "LX/AaQ;",
            "LX/3kp;",
            "LX/3kp;",
            "LX/1b4;",
            "LX/39G;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1684248
    iput-object p1, p0, LX/AXY;->a:Lcom/facebook/facecast/protocol/FacecastNetworker;

    iput-object p2, p0, LX/AXY;->b:LX/03V;

    iput-object p3, p0, LX/AXY;->c:LX/AYi;

    iput-object p4, p0, LX/AXY;->f:LX/6Rg;

    iput-object p5, p0, LX/AXY;->g:Lcom/facebook/video/videostreaming/RtmpLiveStreamer;

    iput-object p6, p0, LX/AXY;->h:LX/AVK;

    iput-object p7, p0, LX/AXY;->i:LX/AV9;

    iput-object p8, p0, LX/AXY;->j:LX/0kL;

    iput-object p9, p0, LX/AXY;->k:LX/AVT;

    iput-object p10, p0, LX/AXY;->l:LX/AXi;

    iput-object p11, p0, LX/AXY;->m:LX/8RJ;

    iput-object p12, p0, LX/AXY;->n:Lcom/facebook/privacy/PrivacyOperationsClient;

    iput-object p13, p0, LX/AXY;->o:LX/1Ck;

    iput-object p14, p0, LX/AXY;->p:LX/3Hc;

    move-object/from16 v0, p15

    iput-object v0, p0, LX/AXY;->q:LX/Acy;

    move-object/from16 v0, p16

    iput-object v0, p0, LX/AXY;->r:LX/0Ot;

    move-object/from16 v0, p17

    iput-object v0, p0, LX/AXY;->s:LX/AaQ;

    move-object/from16 v0, p18

    iput-object v0, p0, LX/AXY;->t:LX/3kp;

    move-object/from16 v0, p19

    iput-object v0, p0, LX/AXY;->u:LX/3kp;

    move-object/from16 v0, p20

    iput-object v0, p0, LX/AXY;->v:LX/1b4;

    move-object/from16 v0, p21

    iput-object v0, p0, LX/AXY;->w:LX/39G;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 24

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v23

    move-object/from16 v2, p0

    check-cast v2, LX/AXY;

    invoke-static/range {v23 .. v23}, Lcom/facebook/facecast/protocol/FacecastNetworker;->a(LX/0QB;)Lcom/facebook/facecast/protocol/FacecastNetworker;

    move-result-object v3

    check-cast v3, Lcom/facebook/facecast/protocol/FacecastNetworker;

    invoke-static/range {v23 .. v23}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    const-class v5, LX/AYi;

    move-object/from16 v0, v23

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/AYi;

    invoke-static/range {v23 .. v23}, LX/6Rg;->a(LX/0QB;)LX/6Rg;

    move-result-object v6

    check-cast v6, LX/6Rg;

    invoke-static/range {v23 .. v23}, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->a(LX/0QB;)Lcom/facebook/video/videostreaming/RtmpLiveStreamer;

    move-result-object v7

    check-cast v7, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;

    invoke-static/range {v23 .. v23}, LX/AVK;->a(LX/0QB;)LX/AVK;

    move-result-object v8

    check-cast v8, LX/AVK;

    const-class v9, LX/AV9;

    move-object/from16 v0, v23

    invoke-interface {v0, v9}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v9

    check-cast v9, LX/AV9;

    invoke-static/range {v23 .. v23}, LX/0kL;->a(LX/0QB;)LX/0kL;

    move-result-object v10

    check-cast v10, LX/0kL;

    invoke-static/range {v23 .. v23}, LX/AVT;->a(LX/0QB;)LX/AVT;

    move-result-object v11

    check-cast v11, LX/AVT;

    invoke-static/range {v23 .. v23}, LX/AXi;->a(LX/0QB;)LX/AXi;

    move-result-object v12

    check-cast v12, LX/AXi;

    invoke-static/range {v23 .. v23}, LX/8RJ;->a(LX/0QB;)LX/8RJ;

    move-result-object v13

    check-cast v13, LX/8RJ;

    invoke-static/range {v23 .. v23}, Lcom/facebook/privacy/PrivacyOperationsClient;->a(LX/0QB;)Lcom/facebook/privacy/PrivacyOperationsClient;

    move-result-object v14

    check-cast v14, Lcom/facebook/privacy/PrivacyOperationsClient;

    invoke-static/range {v23 .. v23}, LX/1Ck;->a(LX/0QB;)LX/1Ck;

    move-result-object v15

    check-cast v15, LX/1Ck;

    invoke-static/range {v23 .. v23}, LX/3Hc;->a(LX/0QB;)LX/3Hc;

    move-result-object v16

    check-cast v16, LX/3Hc;

    invoke-static/range {v23 .. v23}, LX/Acy;->a(LX/0QB;)LX/Acy;

    move-result-object v17

    check-cast v17, LX/Acy;

    const/16 v18, 0x1be3    # 1.0004E-41f

    move-object/from16 v0, v23

    move/from16 v1, v18

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v18

    invoke-static/range {v23 .. v23}, LX/AaQ;->a(LX/0QB;)LX/AaQ;

    move-result-object v19

    check-cast v19, LX/AaQ;

    invoke-static/range {v23 .. v23}, LX/3kp;->a(LX/0QB;)LX/3kp;

    move-result-object v20

    check-cast v20, LX/3kp;

    invoke-static/range {v23 .. v23}, LX/3kp;->a(LX/0QB;)LX/3kp;

    move-result-object v21

    check-cast v21, LX/3kp;

    invoke-static/range {v23 .. v23}, LX/1b4;->a(LX/0QB;)LX/1b4;

    move-result-object v22

    check-cast v22, LX/1b4;

    invoke-static/range {v23 .. v23}, LX/39G;->a(LX/0QB;)LX/39G;

    move-result-object v23

    check-cast v23, LX/39G;

    invoke-static/range {v2 .. v23}, LX/AXY;->a(LX/AXY;Lcom/facebook/facecast/protocol/FacecastNetworker;LX/03V;LX/AYi;LX/6Rg;Lcom/facebook/video/videostreaming/RtmpLiveStreamer;LX/AVK;LX/AV9;LX/0kL;LX/AVT;LX/AXi;LX/8RJ;Lcom/facebook/privacy/PrivacyOperationsClient;LX/1Ck;LX/3Hc;LX/Acy;LX/0Ot;LX/AaQ;LX/3kp;LX/3kp;LX/1b4;LX/39G;)V

    return-void
.end method

.method public static a$redex0(LX/AXY;LX/AVS;)V
    .locals 7

    .prologue
    .line 1684256
    invoke-direct {p0}, LX/AXY;->k()V

    .line 1684257
    iget-object v0, p0, LX/AXY;->ae:Lcom/facebook/facecast/model/FacecastPrivacyData;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/AXY;->ae:Lcom/facebook/facecast/model/FacecastPrivacyData;

    .line 1684258
    iget-object v1, v0, Lcom/facebook/facecast/model/FacecastPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-object v0, v1

    .line 1684259
    if-eqz v0, :cond_0

    .line 1684260
    iget-object v0, p0, LX/AXY;->ae:Lcom/facebook/facecast/model/FacecastPrivacyData;

    .line 1684261
    iget-object v1, v0, Lcom/facebook/facecast/model/FacecastPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-object v0, v1

    .line 1684262
    iget-object v1, v0, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v0, v1

    .line 1684263
    const/4 v6, 0x0

    .line 1684264
    iget-object v1, p0, LX/AXY;->o:LX/1Ck;

    const-string v2, "end_screen_set_privacy"

    iget-object v3, p0, LX/AXY;->n:Lcom/facebook/privacy/PrivacyOperationsClient;

    iget-object v4, p0, LX/AXY;->aa:Ljava/lang/String;

    iget-object v5, p0, LX/AXY;->aa:Ljava/lang/String;

    invoke-virtual {v3, v4, v6, v5, v0}, Lcom/facebook/privacy/PrivacyOperationsClient;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLPrivacyOption;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v6}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1684265
    :cond_0
    invoke-static {p0, p1}, LX/AXY;->b$redex0(LX/AXY;LX/AVS;)V

    .line 1684266
    invoke-static {p0}, LX/AXY;->l$redex0(LX/AXY;)V

    .line 1684267
    return-void
.end method

.method public static a$redex0(LX/AXY;Landroid/net/Uri;)V
    .locals 3

    .prologue
    .line 1684249
    iget-object v0, p0, LX/AXY;->Q:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1684250
    iget-object v0, p0, LX/AXY;->A:Lcom/facebook/facecast/view/FacecastVideoPlaybackView;

    invoke-virtual {v0, p1}, Lcom/facebook/facecast/view/FacecastVideoPlaybackView;->a(Landroid/net/Uri;)V

    .line 1684251
    iget-object v0, p0, LX/AXY;->f:LX/6Rg;

    .line 1684252
    iget-object v1, v0, LX/6Rg;->a:LX/0ad;

    sget-short v2, LX/1v6;->s:S

    const/4 p1, 0x0

    invoke-interface {v1, v2, p1}, LX/0ad;->a(SZ)Z

    move-result v1

    move v0, v1

    .line 1684253
    if-eqz v0, :cond_0

    .line 1684254
    iget-object v0, p0, LX/AXY;->A:Lcom/facebook/facecast/view/FacecastVideoPlaybackView;

    new-instance v1, LX/AXM;

    invoke-direct {v1, p0}, LX/AXM;-><init>(LX/AXY;)V

    invoke-virtual {v0, v1}, Lcom/facebook/facecast/view/FacecastVideoPlaybackView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1684255
    :cond_0
    return-void
.end method

.method public static b$redex0(LX/AXY;LX/AVS;)V
    .locals 9

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1684280
    iget-object v2, p0, LX/AXY;->f:LX/6Rg;

    invoke-virtual {v2}, LX/6Rg;->b()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, LX/AXY;->I:Lcom/facebook/facecast/view/FacecastPreviewSaveButton;

    if-eqz v2, :cond_1

    iget-object v2, p0, LX/AXY;->I:Lcom/facebook/facecast/view/FacecastPreviewSaveButton;

    .line 1684281
    iget-object v3, v2, Lcom/facebook/facecast/view/FacecastPreviewSaveButton;->d:LX/Abm;

    move-object v2, v3

    .line 1684282
    sget-object v3, LX/Abm;->SAVED:LX/Abm;

    if-ne v2, v3, :cond_0

    move v4, v0

    .line 1684283
    :goto_0
    iget-object v2, p0, LX/AXY;->f:LX/6Rg;

    invoke-virtual {v2}, LX/6Rg;->c()Z

    move-result v2

    if-eqz v2, :cond_2

    sget-object v2, LX/AVS;->POST_BUTTON:LX/AVS;

    if-ne p1, v2, :cond_2

    iget-object v2, p0, LX/AXY;->J:Lcom/facebook/facecast/view/FacecastHdUploadButton;

    if-eqz v2, :cond_2

    iget-object v2, p0, LX/AXY;->J:Lcom/facebook/facecast/view/FacecastHdUploadButton;

    invoke-virtual {v2}, Lcom/facebook/facecast/view/FacecastHdUploadButton;->isChecked()Z

    move-result v2

    if-eqz v2, :cond_2

    move v5, v0

    .line 1684284
    :goto_1
    iget-object v2, p0, LX/AXY;->ab:Ljava/io/File;

    if-eqz v2, :cond_3

    move v6, v0

    .line 1684285
    :goto_2
    iget-object v0, p0, LX/AXY;->k:LX/AVT;

    iget-wide v2, p0, LX/AXY;->ah:J

    move-object v1, p1

    .line 1684286
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    .line 1684287
    const-string v8, "broadcast_duration"

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object p0

    invoke-interface {v7, v8, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1684288
    const-string v8, "save_to_camera_roll"

    invoke-static {v4}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object p0

    invoke-interface {v7, v8, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1684289
    const-string v8, "upload_hd"

    invoke-static {v5}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object p0

    invoke-interface {v7, v8, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1684290
    const-string v8, "dvr_file_available"

    invoke-static {v6}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object p0

    invoke-interface {v7, v8, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1684291
    const-string v8, "exit_source"

    invoke-virtual {v1}, LX/AVS;->name()Ljava/lang/String;

    move-result-object p0

    invoke-interface {v7, v8, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1684292
    const-string v8, "facecast_pre_broadcast_exit"

    invoke-static {v0, v8, v7}, LX/AVT;->b(LX/AVT;Ljava/lang/String;Ljava/util/Map;)V

    .line 1684293
    return-void

    :cond_0
    move v4, v1

    .line 1684294
    goto :goto_0

    :cond_1
    move v4, v1

    goto :goto_0

    :cond_2
    move v5, v1

    .line 1684295
    goto :goto_1

    :cond_3
    move v6, v1

    .line 1684296
    goto :goto_2
.end method

.method public static c(LX/AXY;I)V
    .locals 2

    .prologue
    .line 1684297
    iget-object v0, p0, LX/AXY;->j:LX/0kL;

    new-instance v1, LX/27k;

    invoke-direct {v1, p1}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 1684298
    return-void
.end method

.method private g()V
    .locals 5

    .prologue
    .line 1684274
    const v0, 0x7f0d101a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/button/FigButton;

    iput-object v0, p0, LX/AXY;->B:Lcom/facebook/fig/button/FigButton;

    .line 1684275
    iget-object v0, p0, LX/AXY;->B:Lcom/facebook/fig/button/FigButton;

    new-instance v1, LX/AXN;

    invoke-direct {v1, p0}, LX/AXN;-><init>(LX/AXY;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1684276
    iget-object v0, p0, LX/AXY;->B:Lcom/facebook/fig/button/FigButton;

    iget-object v1, p0, LX/AXY;->f:LX/6Rg;

    invoke-virtual {p0}, LX/AXY;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 1684277
    iget-object v3, v1, LX/6Rg;->a:LX/0ad;

    sget-char v4, LX/1v6;->u:C

    const p0, 0x7f080cbc

    invoke-virtual {v2, p0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    invoke-interface {v3, v4, p0}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object v1, v3

    .line 1684278
    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setText(Ljava/lang/CharSequence;)V

    .line 1684279
    return-void
.end method

.method private h()V
    .locals 5

    .prologue
    .line 1684268
    const v0, 0x7f0d1019

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/button/FigButton;

    iput-object v0, p0, LX/AXY;->C:Lcom/facebook/fig/button/FigButton;

    .line 1684269
    iget-object v0, p0, LX/AXY;->C:Lcom/facebook/fig/button/FigButton;

    new-instance v1, LX/AXO;

    invoke-direct {v1, p0}, LX/AXO;-><init>(LX/AXY;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1684270
    iget-object v0, p0, LX/AXY;->C:Lcom/facebook/fig/button/FigButton;

    iget-object v1, p0, LX/AXY;->f:LX/6Rg;

    invoke-virtual {p0}, LX/AXY;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 1684271
    iget-object v3, v1, LX/6Rg;->a:LX/0ad;

    sget-char v4, LX/1v6;->r:C

    const p0, 0x7f080cbd

    invoke-virtual {v2, p0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    invoke-interface {v3, v4, p0}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object v1, v3

    .line 1684272
    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setText(Ljava/lang/CharSequence;)V

    .line 1684273
    return-void
.end method

.method private static j(LX/AXY;)V
    .locals 2

    .prologue
    .line 1684124
    iget-object v0, p0, LX/AXY;->p:LX/3Hc;

    new-instance v1, LX/AXQ;

    invoke-direct {v1, p0}, LX/AXQ;-><init>(LX/AXY;)V

    .line 1684125
    iput-object v1, v0, LX/3Hc;->l:LX/3Ha;

    .line 1684126
    iget-object v0, p0, LX/AXY;->p:LX/3Hc;

    iget-object v1, p0, LX/AXY;->aa:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/3Hc;->a(Ljava/lang/String;)V

    .line 1684127
    return-void
.end method

.method private k()V
    .locals 14

    .prologue
    .line 1684128
    iget-object v0, p0, LX/AXY;->g:Lcom/facebook/video/videostreaming/RtmpLiveStreamer;

    invoke-virtual {v0}, Lcom/facebook/video/videostreaming/RtmpLiveStreamer;->s()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1684129
    iget-object v0, p0, LX/AXY;->J:Lcom/facebook/facecast/view/FacecastHdUploadButton;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/AXY;->J:Lcom/facebook/facecast/view/FacecastHdUploadButton;

    invoke-virtual {v0}, Lcom/facebook/facecast/view/FacecastHdUploadButton;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 1684130
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/AXY;->f:LX/6Rg;

    invoke-virtual {v0}, LX/6Rg;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1684131
    iget-object v0, p0, LX/AXY;->ab:Ljava/io/File;

    .line 1684132
    if-eqz v0, :cond_1

    .line 1684133
    iget-object v1, p0, LX/AXY;->h:LX/AVK;

    iget-object v2, p0, LX/AXY;->W:Ljava/lang/String;

    .line 1684134
    :try_start_0
    iget-object v3, v1, LX/AVK;->b:LX/8LV;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    const/4 v12, 0x0

    .line 1684135
    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    invoke-static {v6}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/net/Uri;

    .line 1684136
    new-instance v7, LX/74m;

    invoke-direct {v7}, LX/74m;-><init>()V

    invoke-virtual {v7, v4}, LX/74m;->c(Ljava/lang/String;)LX/74m;

    move-result-object v7

    iget-object v8, v3, LX/8LV;->c:Lcom/facebook/ui/media/attachments/MediaResourceHelper;

    invoke-virtual {v8, v6}, Lcom/facebook/ui/media/attachments/MediaResourceHelper;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, LX/74m;->d(Ljava/lang/String;)LX/74m;

    move-result-object v6

    invoke-virtual {v6}, LX/74m;->a()Lcom/facebook/photos/base/media/VideoItem;

    move-result-object v6

    .line 1684137
    new-instance v7, LX/8LQ;

    invoke-direct {v7}, LX/8LQ;-><init>()V

    invoke-static {v6}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v6

    .line 1684138
    iput-object v6, v7, LX/8LQ;->b:LX/0Px;

    .line 1684139
    move-object v6, v7

    .line 1684140
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v7

    .line 1684141
    iput-object v7, v6, LX/8LQ;->a:Ljava/lang/String;

    .line 1684142
    move-object v6, v6

    .line 1684143
    sget-object v7, Lcom/facebook/ipc/composer/model/MinutiaeTag;->a:Lcom/facebook/ipc/composer/model/MinutiaeTag;

    invoke-virtual {v6, v7}, LX/8LQ;->a(Lcom/facebook/ipc/composer/model/MinutiaeTag;)LX/8LQ;

    move-result-object v7

    iget-object v6, v3, LX/8LV;->a:LX/0Or;

    invoke-interface {v6}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    .line 1684144
    iput-wide v8, v7, LX/8LQ;->h:J

    .line 1684145
    move-object v6, v7

    .line 1684146
    const-string v7, "facecast_live_video"

    .line 1684147
    iput-object v7, v6, LX/8LQ;->I:Ljava/lang/String;

    .line 1684148
    move-object v6, v6

    .line 1684149
    const-string v7, "own_timeline"

    .line 1684150
    iput-object v7, v6, LX/8LQ;->i:Ljava/lang/String;

    .line 1684151
    move-object v6, v6

    .line 1684152
    sget-object v7, LX/8LS;->LIVE_VIDEO:LX/8LS;

    .line 1684153
    iput-object v7, v6, LX/8LQ;->q:LX/8LS;

    .line 1684154
    move-object v6, v6

    .line 1684155
    iput-object v2, v6, LX/8LQ;->ae:Ljava/lang/String;

    .line 1684156
    move-object v6, v6

    .line 1684157
    sget-object v7, LX/0Q7;->a:LX/0Px;

    move-object v7, v7

    .line 1684158
    iput-object v7, v6, LX/8LQ;->d:LX/0Px;

    .line 1684159
    move-object v6, v6

    .line 1684160
    const-wide/16 v8, -0x1

    .line 1684161
    iput-wide v8, v6, LX/8LQ;->j:J

    .line 1684162
    move-object v6, v6

    .line 1684163
    iput-boolean v12, v6, LX/8LQ;->m:Z

    .line 1684164
    move-object v6, v6

    .line 1684165
    sget-object v7, Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;->a:Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    .line 1684166
    iput-object v7, v6, LX/8LQ;->o:Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    .line 1684167
    move-object v6, v6

    .line 1684168
    iget-object v7, v3, LX/8LV;->b:LX/0SG;

    invoke-interface {v7}, LX/0SG;->a()J

    move-result-wide v8

    const-wide/16 v10, 0x3e8

    div-long/2addr v8, v10

    .line 1684169
    iput-wide v8, v6, LX/8LQ;->x:J

    .line 1684170
    move-object v6, v6

    .line 1684171
    iput-boolean v12, v6, LX/8LQ;->z:Z

    .line 1684172
    move-object v6, v6

    .line 1684173
    iput-boolean v12, v6, LX/8LQ;->A:Z

    .line 1684174
    move-object v6, v6

    .line 1684175
    const/16 v7, 0x28

    invoke-virtual {v6, v7}, LX/8LQ;->e(I)LX/8LQ;

    move-result-object v6

    sget-object v7, LX/5Rn;->SAVE_DRAFT:LX/5Rn;

    .line 1684176
    iput-object v7, v6, LX/8LQ;->B:LX/5Rn;

    .line 1684177
    move-object v6, v6

    .line 1684178
    sget-object v7, LX/8LR;->VIDEO_TARGET:LX/8LR;

    .line 1684179
    iput-object v7, v6, LX/8LQ;->p:LX/8LR;

    .line 1684180
    move-object v6, v6

    .line 1684181
    const/4 v7, -0x2

    .line 1684182
    iput v7, v6, LX/8LQ;->W:I

    .line 1684183
    move-object v6, v6

    .line 1684184
    invoke-virtual {v6}, LX/8LQ;->a()Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v6

    move-object v3, v6

    .line 1684185
    iget-object v4, v1, LX/AVK;->c:LX/1EZ;

    invoke-virtual {v4, v3}, LX/1EZ;->a(Lcom/facebook/photos/upload/operation/UploadOperation;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1684186
    :cond_0
    :goto_1
    return-void

    .line 1684187
    :cond_1
    sget-object v0, LX/AXY;->x:Ljava/lang/String;

    const-string v1, "DVR File is null"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 1684188
    :catch_0
    move-exception v3

    .line 1684189
    sget-object v4, LX/AVK;->a:Ljava/lang/String;

    const-string v5, "Failed to Upload DVR "

    invoke-static {v4, v5, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public static l$redex0(LX/AXY;)V
    .locals 1

    .prologue
    .line 1683998
    iget-object v0, p0, LX/AXY;->V:LX/AUt;

    if-eqz v0, :cond_0

    .line 1683999
    iget-object v0, p0, LX/AXY;->V:LX/AUt;

    invoke-virtual {v0}, LX/AUt;->a()V

    .line 1684000
    :cond_0
    return-void
.end method

.method private n()V
    .locals 10

    .prologue
    .line 1684001
    const v0, 0x7f0d1020

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 1684002
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecast/view/FacecastHdUploadButton;

    iput-object v0, p0, LX/AXY;->J:Lcom/facebook/facecast/view/FacecastHdUploadButton;

    .line 1684003
    iget-object v0, p0, LX/AXY;->J:Lcom/facebook/facecast/view/FacecastHdUploadButton;

    new-instance v1, LX/AXT;

    invoke-direct {v1, p0}, LX/AXT;-><init>(LX/AXY;)V

    .line 1684004
    iput-object v1, v0, Lcom/facebook/facecast/view/FacecastHdUploadButton;->b:LX/AXS;

    .line 1684005
    iget-object v0, p0, LX/AXY;->J:Lcom/facebook/facecast/view/FacecastHdUploadButton;

    iget-object v1, p0, LX/AXY;->l:LX/AXi;

    iget-wide v2, p0, LX/AXY;->ac:J

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1684006
    iget-object v6, v1, LX/AXi;->b:LX/6Rg;

    .line 1684007
    iget-object v7, v6, LX/6Rg;->a:LX/0ad;

    sget-short v8, LX/1v6;->q:S

    const/4 v9, 0x0

    invoke-interface {v7, v8, v9}, LX/0ad;->a(SZ)Z

    move-result v7

    move v6, v7

    .line 1684008
    if-nez v6, :cond_1

    .line 1684009
    :cond_0
    :goto_0
    move v1, v4

    .line 1684010
    invoke-virtual {v0, v1}, Lcom/facebook/facecast/view/FacecastHdUploadButton;->setChecked(Z)V

    .line 1684011
    return-void

    .line 1684012
    :cond_1
    iget-object v6, v1, LX/AXi;->a:LX/0kb;

    invoke-virtual {v6}, LX/0kb;->v()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1684013
    const-wide/32 v6, 0x6400000

    cmp-long v6, v2, v6

    if-gtz v6, :cond_0

    move v4, v5

    .line 1684014
    goto :goto_0

    .line 1684015
    :cond_2
    const-wide/32 v6, 0x1400000

    cmp-long v6, v2, v6

    if-gtz v6, :cond_0

    move v4, v5

    .line 1684016
    goto :goto_0
.end method

.method private static o(LX/AXY;)V
    .locals 3

    .prologue
    .line 1684017
    const v0, 0x7f0d1023

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecast/view/FacecastEndScreenPrivacyPill;

    .line 1684018
    iget-object v1, p0, LX/AXY;->ae:Lcom/facebook/facecast/model/FacecastPrivacyData;

    iget-object v2, p0, LX/AXY;->ad:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/facecast/view/FacecastEndScreenPrivacyPill;->a(Lcom/facebook/facecast/model/FacecastPrivacyData;Lcom/facebook/ipc/composer/intent/ComposerTargetData;)V

    .line 1684019
    iget-object v1, p0, LX/AXY;->ae:Lcom/facebook/facecast/model/FacecastPrivacyData;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/AXY;->ae:Lcom/facebook/facecast/model/FacecastPrivacyData;

    .line 1684020
    iget-object v2, v1, Lcom/facebook/facecast/model/FacecastPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-object v1, v2

    .line 1684021
    if-eqz v1, :cond_0

    .line 1684022
    new-instance v1, LX/AXU;

    invoke-direct {v1, p0}, LX/AXU;-><init>(LX/AXY;)V

    invoke-virtual {v0, v1}, Lcom/facebook/facecast/view/FacecastEndScreenPrivacyPill;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1684023
    :cond_0
    return-void
.end method

.method public static q(LX/AXY;)V
    .locals 14

    .prologue
    .line 1684024
    new-instance v0, LX/0ju;

    invoke-virtual {p0}, LX/AXY;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0ju;-><init>(Landroid/content/Context;)V

    .line 1684025
    invoke-virtual {p0}, LX/AXY;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f080cc0

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    .line 1684026
    invoke-virtual {p0}, LX/AXY;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f080cc1

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-wide v6, p0, LX/AXY;->ac:J

    .line 1684027
    const-wide/16 v8, 0x1

    long-to-double v10, v6

    const-wide/high16 v12, 0x4130000000000000L    # 1048576.0

    div-double/2addr v10, v12

    invoke-static {v10, v11}, Ljava/lang/Math;->round(D)J

    move-result-wide v10

    invoke-static {v8, v9, v10, v11}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v8

    .line 1684028
    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v8

    move-object v5, v8

    .line 1684029
    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    .line 1684030
    invoke-virtual {p0}, LX/AXY;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f080cbf

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/AXJ;

    invoke-direct {v2, p0}, LX/AXJ;-><init>(LX/AXY;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 1684031
    invoke-virtual {p0}, LX/AXY;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f080cbe

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/AXK;

    invoke-direct {v2, p0}, LX/AXK;-><init>(LX/AXY;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 1684032
    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 1684033
    return-void
.end method

.method public static setUpMonetizationTip(LX/AXY;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1684034
    iget-object v0, p0, LX/AXY;->P:Landroid/view/View;

    if-nez v0, :cond_0

    .line 1684035
    iget-object v0, p0, LX/AXY;->G:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/AXY;->P:Landroid/view/View;

    .line 1684036
    :cond_0
    iget-object v0, p0, LX/AXY;->P:Landroid/view/View;

    const v1, 0x7f0d100c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    .line 1684037
    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1684038
    return-void
.end method


# virtual methods
.method public final a(JJLjava/lang/String;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerTargetData;Lcom/facebook/facecast/model/FacecastPrivacyData;FLcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;ZZ)V
    .locals 9

    .prologue
    .line 1684039
    move-object/from16 v0, p7

    iput-object v0, p0, LX/AXY;->ad:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    .line 1684040
    move-object/from16 v0, p8

    iput-object v0, p0, LX/AXY;->ae:Lcom/facebook/facecast/model/FacecastPrivacyData;

    .line 1684041
    iput-wide p3, p0, LX/AXY;->ah:J

    .line 1684042
    iput-object p5, p0, LX/AXY;->W:Ljava/lang/String;

    .line 1684043
    iput-object p6, p0, LX/AXY;->aa:Ljava/lang/String;

    .line 1684044
    iget-object v2, p0, LX/AXY;->O:Lcom/facebook/facecast/view/FacecastVideoPlaybackContainer;

    move/from16 v0, p9

    invoke-virtual {v2, v0}, Lcom/facebook/facecast/view/FacecastVideoPlaybackContainer;->setAspectRatio(F)V

    .line 1684045
    move-object/from16 v0, p10

    iput-object v0, p0, LX/AXY;->af:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    .line 1684046
    move/from16 v0, p11

    iput-boolean v0, p0, LX/AXY;->ag:Z

    .line 1684047
    iget-object v2, p0, LX/AXY;->c:LX/AYi;

    new-instance v6, LX/AXG;

    move/from16 v0, p12

    invoke-direct {v6, p0, v0}, LX/AXG;-><init>(LX/AXY;Z)V

    invoke-virtual {p0}, LX/AXY;->getContext()Landroid/content/Context;

    move-result-object v7

    move-object v3, p6

    move-wide v4, p1

    invoke-virtual/range {v2 .. v7}, LX/AYi;->a(Ljava/lang/String;JLX/AX5;Landroid/content/Context;)LX/AYh;

    move-result-object v2

    iput-object v2, p0, LX/AXY;->ai:LX/AYh;

    .line 1684048
    invoke-static {p0}, LX/AXY;->o(LX/AXY;)V

    .line 1684049
    iget-object v2, p0, LX/AXY;->ab:Ljava/io/File;

    if-nez v2, :cond_0

    .line 1684050
    invoke-static {p0}, LX/AXY;->j(LX/AXY;)V

    .line 1684051
    :cond_0
    return-void
.end method

.method public final a(Landroid/view/ViewGroup;LX/AVF;)V
    .locals 1

    .prologue
    .line 1684052
    invoke-super {p0, p1, p2}, LX/AWT;->a(Landroid/view/ViewGroup;LX/AVF;)V

    .line 1684053
    iget-object v0, p0, LX/AXY;->ai:LX/AYh;

    if-eqz v0, :cond_0

    .line 1684054
    iget-object v0, p0, LX/AXY;->ai:LX/AYh;

    invoke-virtual {v0}, LX/AYh;->b()V

    .line 1684055
    :cond_0
    iget-object v0, p0, LX/AXY;->A:Lcom/facebook/facecast/view/FacecastVideoPlaybackView;

    invoke-virtual {v0}, Lcom/facebook/facecast/view/FacecastVideoPlaybackView;->z()V

    .line 1684056
    return-void
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x1

    .line 1684057
    if-lez p2, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x2

    if-lt v0, v1, :cond_0

    .line 1684058
    iget-object v0, p0, LX/AXY;->F:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v1

    .line 1684059
    const v0, 0x7f0d1016

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 1684060
    const v2, 0x7f0d1017

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbTextView;

    .line 1684061
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1684062
    invoke-virtual {p1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1684063
    :goto_0
    return-void

    .line 1684064
    :cond_0
    invoke-virtual {p0}, LX/AXY;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080d20

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, LX/AXY;->setUpMonetizationTip(LX/AXY;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 1684065
    iget-object v0, p0, LX/AXY;->aj:LX/AXD;

    .line 1684066
    iget-boolean v2, v0, LX/AXD;->g:Z

    move v0, v2

    .line 1684067
    if-eqz v0, :cond_0

    .line 1684068
    :goto_0
    return v1

    .line 1684069
    :cond_0
    iget-object v0, p0, LX/AXY;->aj:LX/AXD;

    .line 1684070
    iget-boolean v2, v0, LX/AXD;->f:Z

    move v0, v2

    .line 1684071
    if-eqz v0, :cond_1

    .line 1684072
    iget-object v0, p0, LX/AXY;->aj:LX/AXD;

    .line 1684073
    iget-object v2, v0, LX/AXD;->h:LX/AX2;

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1684074
    const/4 v2, 0x0

    iput-boolean v2, v0, LX/AXD;->f:Z

    .line 1684075
    iget-object v2, v0, LX/AXD;->h:LX/AX2;

    .line 1684076
    iget-object p0, v2, LX/AX2;->b:Landroid/animation/AnimatorSet;

    invoke-virtual {p0}, Landroid/animation/AnimatorSet;->start()V

    .line 1684077
    const/4 v2, 0x0

    iput-object v2, v0, LX/AXD;->h:LX/AX2;

    .line 1684078
    goto :goto_0

    .line 1684079
    :cond_1
    sget-object v0, LX/AVS;->BACK_BUTTON:LX/AVS;

    invoke-static {p0, v0}, LX/AXY;->a$redex0(LX/AXY;LX/AVS;)V

    goto :goto_0
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 1684080
    invoke-super {p0}, LX/AWT;->e()V

    .line 1684081
    iget-object v0, p0, LX/AXY;->A:Lcom/facebook/facecast/view/FacecastVideoPlaybackView;

    invoke-virtual {v0}, Lcom/facebook/facecast/view/FacecastVideoPlaybackView;->b()V

    .line 1684082
    return-void
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 1684083
    invoke-super {p0}, LX/AWT;->f()V

    .line 1684084
    iget-object v0, p0, LX/AXY;->A:Lcom/facebook/facecast/view/FacecastVideoPlaybackView;

    invoke-virtual {v0}, Lcom/facebook/facecast/view/FacecastVideoPlaybackView;->a()V

    .line 1684085
    return-void
.end method

.method public final iF_()V
    .locals 2

    .prologue
    .line 1684086
    invoke-super {p0}, LX/AWT;->iF_()V

    .line 1684087
    iget-object v0, p0, LX/AXY;->ai:LX/AYh;

    if-nez v0, :cond_0

    .line 1684088
    :goto_0
    return-void

    .line 1684089
    :cond_0
    iget-object v0, p0, LX/AXY;->af:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    if-eqz v0, :cond_2

    .line 1684090
    iget-object v0, p0, LX/AXY;->u:LX/3kp;

    sget-object v1, LX/AXY;->z:LX/0Tn;

    invoke-virtual {v0, v1}, LX/3kp;->a(LX/0Tn;)V

    .line 1684091
    iget-object v0, p0, LX/AXY;->u:LX/3kp;

    invoke-virtual {v0}, LX/3kp;->b()V

    .line 1684092
    iget-object v0, p0, LX/AXY;->q:LX/Acy;

    iget-object v1, p0, LX/AXY;->aa:Ljava/lang/String;

    invoke-virtual {v0, v1, p0}, LX/Acy;->a(Ljava/lang/String;LX/AXX;)V

    .line 1684093
    :cond_1
    :goto_1
    iget-object v0, p0, LX/AXY;->s:LX/AaQ;

    invoke-virtual {v0}, LX/AaQ;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1684094
    iget-object v0, p0, LX/AXY;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AaX;

    iget-object v1, p0, LX/AXY;->aa:Ljava/lang/String;

    invoke-virtual {v0, p0, v1}, LX/AaX;->a(LX/AXW;Ljava/lang/String;)V

    .line 1684095
    iget-object v0, p0, LX/AXY;->t:LX/3kp;

    sget-object v1, LX/AXY;->y:LX/0Tn;

    invoke-virtual {v0, v1}, LX/3kp;->a(LX/0Tn;)V

    .line 1684096
    iget-object v0, p0, LX/AXY;->t:LX/3kp;

    invoke-virtual {v0}, LX/3kp;->b()V

    .line 1684097
    :goto_2
    iget-object v0, p0, LX/AXY;->ai:LX/AYh;

    invoke-virtual {v0}, LX/AYh;->a()V

    goto :goto_0

    .line 1684098
    :cond_2
    iget-boolean v0, p0, LX/AXY;->ag:Z

    if-eqz v0, :cond_1

    .line 1684099
    iget-object v0, p0, LX/AXY;->u:LX/3kp;

    const/4 v1, 0x3

    .line 1684100
    iput v1, v0, LX/3kp;->b:I

    .line 1684101
    iget-object v0, p0, LX/AXY;->u:LX/3kp;

    sget-object v1, LX/AXY;->z:LX/0Tn;

    invoke-virtual {v0, v1}, LX/3kp;->a(LX/0Tn;)V

    .line 1684102
    iget-object v0, p0, LX/AXY;->u:LX/3kp;

    invoke-virtual {v0}, LX/3kp;->c()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1684103
    invoke-virtual {p0}, LX/AXY;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080cdb

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, LX/AXY;->setUpMonetizationTip(LX/AXY;Ljava/lang/String;)V

    .line 1684104
    :cond_3
    goto :goto_1

    .line 1684105
    :cond_4
    iget-object v0, p0, LX/AXY;->t:LX/3kp;

    const/4 v1, 0x3

    .line 1684106
    iput v1, v0, LX/3kp;->b:I

    .line 1684107
    iget-object v0, p0, LX/AXY;->t:LX/3kp;

    sget-object v1, LX/AXY;->y:LX/0Tn;

    invoke-virtual {v0, v1}, LX/3kp;->a(LX/0Tn;)V

    .line 1684108
    iget-object v0, p0, LX/AXY;->t:LX/3kp;

    invoke-virtual {v0}, LX/3kp;->c()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, LX/AXY;->v:LX/1b4;

    invoke-virtual {v0}, LX/1b4;->s()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1684109
    iget-object v0, p0, LX/AXY;->t:LX/3kp;

    invoke-virtual {v0}, LX/3kp;->a()V

    .line 1684110
    invoke-virtual {p0}, LX/AXY;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080d21

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, LX/AXY;->setUpMonetizationTip(LX/AXY;Ljava/lang/String;)V

    .line 1684111
    :cond_5
    goto :goto_2
.end method

.method public final r_(I)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1684112
    if-lez p1, :cond_0

    .line 1684113
    iget-object v0, p0, LX/AXY;->E:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v1

    .line 1684114
    const v0, 0x7f0d0ffe

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/AXY;->K:Lcom/facebook/widget/text/BetterTextView;

    .line 1684115
    const v0, 0x7f0d0fff

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/AXY;->L:Lcom/facebook/widget/text/BetterTextView;

    .line 1684116
    iget-object v0, p0, LX/AXY;->af:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;->p()Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel$CharityInterfaceModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel$CharityInterfaceModel;->l()Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel$CharityInterfaceModel$PageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel$CharityInterfaceModel$PageModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/AXY;->af:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;->p()Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel$CharityInterfaceModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel$CharityInterfaceModel;->k()Ljava/lang/String;

    move-result-object v0

    .line 1684117
    :goto_0
    invoke-virtual {p0}, LX/AXY;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080cf6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    aput-object v0, v2, v6

    invoke-static {v1, v2}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1684118
    new-instance v2, Landroid/text/SpannableString;

    invoke-direct {v2, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 1684119
    new-instance v3, Landroid/text/style/TextAppearanceSpan;

    invoke-virtual {p0}, LX/AXY;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f0e03c1

    invoke-direct {v3, v4, v5}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v1, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v1, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/2addr v0, v1

    invoke-virtual {v2, v3, v4, v0, v6}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 1684120
    iget-object v0, p0, LX/AXY;->L:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1684121
    iget-object v0, p0, LX/AXY;->K:Lcom/facebook/widget/text/BetterTextView;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1684122
    :cond_0
    return-void

    .line 1684123
    :cond_1
    iget-object v0, p0, LX/AXY;->af:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;->p()Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel$CharityInterfaceModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel$CharityInterfaceModel;->l()Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel$CharityInterfaceModel$PageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel$CharityInterfaceModel$PageModel;->j()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
