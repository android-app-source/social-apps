.class public LX/BZ1;
.super LX/BYo;
.source ""


# instance fields
.field private a:F

.field private b:F

.field private c:I

.field private d:I

.field private e:F

.field private f:F

.field private g:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1796703
    invoke-direct {p0}, LX/BYo;-><init>()V

    return-void
.end method


# virtual methods
.method public getHorizontalAlignment()I
    .locals 1

    .prologue
    .line 1796702
    iget v0, p0, LX/BZ1;->c:I

    return v0
.end method

.method public getHorizontalSpacing()F
    .locals 1

    .prologue
    .line 1796701
    iget v0, p0, LX/BZ1;->e:F

    return v0
.end method

.method public getMaterialIdentifier()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1796700
    iget-object v0, p0, LX/BZ1;->g:Ljava/lang/String;

    return-object v0
.end method

.method public getTextureHeight()F
    .locals 1

    .prologue
    .line 1796699
    iget v0, p0, LX/BZ1;->b:F

    return v0
.end method

.method public getTextureWidth()F
    .locals 1

    .prologue
    .line 1796698
    iget v0, p0, LX/BZ1;->a:F

    return v0
.end method

.method public getVerticalAlignment()I
    .locals 1

    .prologue
    .line 1796697
    iget v0, p0, LX/BZ1;->d:I

    return v0
.end method

.method public getVerticalSpacing()F
    .locals 1

    .prologue
    .line 1796704
    iget v0, p0, LX/BZ1;->f:F

    return v0
.end method

.method public setHorizontalAlignment(I)V
    .locals 0

    .prologue
    .line 1796695
    iput p1, p0, LX/BZ1;->c:I

    .line 1796696
    return-void
.end method

.method public setHorizontalSpacing(F)V
    .locals 0

    .prologue
    .line 1796693
    iput p1, p0, LX/BZ1;->e:F

    .line 1796694
    return-void
.end method

.method public setMaterialIdentifier(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1796691
    iput-object p1, p0, LX/BZ1;->g:Ljava/lang/String;

    .line 1796692
    return-void
.end method

.method public setTextureHeight(F)V
    .locals 0

    .prologue
    .line 1796689
    iput p1, p0, LX/BZ1;->b:F

    .line 1796690
    return-void
.end method

.method public setTextureWidth(F)V
    .locals 0

    .prologue
    .line 1796683
    iput p1, p0, LX/BZ1;->a:F

    .line 1796684
    return-void
.end method

.method public setVerticalAlignment(I)V
    .locals 0

    .prologue
    .line 1796687
    iput p1, p0, LX/BZ1;->d:I

    .line 1796688
    return-void
.end method

.method public setVerticalSpacing(F)V
    .locals 0

    .prologue
    .line 1796685
    iput p1, p0, LX/BZ1;->f:F

    .line 1796686
    return-void
.end method
