.class public final LX/BtL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field public final synthetic a:LX/BtM;


# direct methods
.method public constructor <init>(LX/BtM;)V
    .locals 0

    .prologue
    .line 1829000
    iput-object p1, p0, LX/BtL;->a:LX/BtM;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onGlobalLayout()V
    .locals 5

    .prologue
    .line 1828992
    iget-object v0, p0, LX/BtL;->a:LX/BtM;

    invoke-static {v0, p0}, LX/1r0;->a(Landroid/view/View;Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 1828993
    iget-object v0, p0, LX/BtL;->a:LX/BtM;

    const/4 v2, 0x0

    .line 1828994
    invoke-virtual {v0}, LX/BtM;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, LX/BtM;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    .line 1828995
    :goto_0
    iget-object v3, v0, LX/BtM;->e:Landroid/widget/RelativeLayout;

    new-instance v4, Landroid/widget/FrameLayout$LayoutParams;

    const/4 p0, -0x1

    invoke-direct {v4, p0, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1828996
    iget v1, v0, LX/BtM;->d:I

    iget v3, v0, LX/BtM;->d:I

    invoke-virtual {v0, v1, v2, v3, v2}, LX/BtM;->setPadding(IIII)V

    .line 1828997
    invoke-virtual {v0}, LX/BtM;->invalidate()V

    .line 1828998
    return-void

    :cond_0
    move v1, v2

    .line 1828999
    goto :goto_0
.end method
