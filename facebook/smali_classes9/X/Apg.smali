.class public final LX/Apg;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/Aph;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Ljava/lang/CharSequence;

.field public c:I

.field public d:Ljava/lang/CharSequence;

.field public e:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/1dQ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation
.end field

.field public g:Landroid/view/View$OnClickListener;

.field public h:LX/1dQ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dQ",
            "<",
            "LX/Apr;",
            ">;"
        }
    .end annotation
.end field

.field public i:Landroid/widget/CompoundButton$OnCheckedChangeListener;

.field public j:LX/1X1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1X1",
            "<*>;"
        }
    .end annotation
.end field

.field public final synthetic k:LX/Aph;


# direct methods
.method public constructor <init>(LX/Aph;)V
    .locals 1

    .prologue
    .line 1716471
    iput-object p1, p0, LX/Apg;->k:LX/Aph;

    .line 1716472
    move-object v0, p1

    .line 1716473
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1716474
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1716515
    const-string v0, "FigHscrollFooterActionComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1716479
    if-ne p0, p1, :cond_1

    .line 1716480
    :cond_0
    :goto_0
    return v0

    .line 1716481
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1716482
    goto :goto_0

    .line 1716483
    :cond_3
    check-cast p1, LX/Apg;

    .line 1716484
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1716485
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1716486
    if-eq v2, v3, :cond_0

    .line 1716487
    iget v2, p0, LX/Apg;->a:I

    iget v3, p1, LX/Apg;->a:I

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 1716488
    goto :goto_0

    .line 1716489
    :cond_4
    iget-object v2, p0, LX/Apg;->b:Ljava/lang/CharSequence;

    if-eqz v2, :cond_6

    iget-object v2, p0, LX/Apg;->b:Ljava/lang/CharSequence;

    iget-object v3, p1, LX/Apg;->b:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    :cond_5
    move v0, v1

    .line 1716490
    goto :goto_0

    .line 1716491
    :cond_6
    iget-object v2, p1, LX/Apg;->b:Ljava/lang/CharSequence;

    if-nez v2, :cond_5

    .line 1716492
    :cond_7
    iget v2, p0, LX/Apg;->c:I

    iget v3, p1, LX/Apg;->c:I

    if-eq v2, v3, :cond_8

    move v0, v1

    .line 1716493
    goto :goto_0

    .line 1716494
    :cond_8
    iget-object v2, p0, LX/Apg;->d:Ljava/lang/CharSequence;

    if-eqz v2, :cond_a

    iget-object v2, p0, LX/Apg;->d:Ljava/lang/CharSequence;

    iget-object v3, p1, LX/Apg;->d:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    :cond_9
    move v0, v1

    .line 1716495
    goto :goto_0

    .line 1716496
    :cond_a
    iget-object v2, p1, LX/Apg;->d:Ljava/lang/CharSequence;

    if-nez v2, :cond_9

    .line 1716497
    :cond_b
    iget-object v2, p0, LX/Apg;->e:Landroid/util/SparseArray;

    if-eqz v2, :cond_d

    iget-object v2, p0, LX/Apg;->e:Landroid/util/SparseArray;

    iget-object v3, p1, LX/Apg;->e:Landroid/util/SparseArray;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_e

    :cond_c
    move v0, v1

    .line 1716498
    goto :goto_0

    .line 1716499
    :cond_d
    iget-object v2, p1, LX/Apg;->e:Landroid/util/SparseArray;

    if-nez v2, :cond_c

    .line 1716500
    :cond_e
    iget-object v2, p0, LX/Apg;->f:LX/1dQ;

    if-eqz v2, :cond_10

    iget-object v2, p0, LX/Apg;->f:LX/1dQ;

    iget-object v3, p1, LX/Apg;->f:LX/1dQ;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_11

    :cond_f
    move v0, v1

    .line 1716501
    goto :goto_0

    .line 1716502
    :cond_10
    iget-object v2, p1, LX/Apg;->f:LX/1dQ;

    if-nez v2, :cond_f

    .line 1716503
    :cond_11
    iget-object v2, p0, LX/Apg;->g:Landroid/view/View$OnClickListener;

    if-eqz v2, :cond_13

    iget-object v2, p0, LX/Apg;->g:Landroid/view/View$OnClickListener;

    iget-object v3, p1, LX/Apg;->g:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_14

    :cond_12
    move v0, v1

    .line 1716504
    goto/16 :goto_0

    .line 1716505
    :cond_13
    iget-object v2, p1, LX/Apg;->g:Landroid/view/View$OnClickListener;

    if-nez v2, :cond_12

    .line 1716506
    :cond_14
    iget-object v2, p0, LX/Apg;->h:LX/1dQ;

    if-eqz v2, :cond_16

    iget-object v2, p0, LX/Apg;->h:LX/1dQ;

    iget-object v3, p1, LX/Apg;->h:LX/1dQ;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_17

    :cond_15
    move v0, v1

    .line 1716507
    goto/16 :goto_0

    .line 1716508
    :cond_16
    iget-object v2, p1, LX/Apg;->h:LX/1dQ;

    if-nez v2, :cond_15

    .line 1716509
    :cond_17
    iget-object v2, p0, LX/Apg;->i:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    if-eqz v2, :cond_19

    iget-object v2, p0, LX/Apg;->i:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    iget-object v3, p1, LX/Apg;->i:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1a

    :cond_18
    move v0, v1

    .line 1716510
    goto/16 :goto_0

    .line 1716511
    :cond_19
    iget-object v2, p1, LX/Apg;->i:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    if-nez v2, :cond_18

    .line 1716512
    :cond_1a
    iget-object v2, p0, LX/Apg;->j:LX/1X1;

    if-eqz v2, :cond_1b

    iget-object v2, p0, LX/Apg;->j:LX/1X1;

    iget-object v3, p1, LX/Apg;->j:LX/1X1;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1716513
    goto/16 :goto_0

    .line 1716514
    :cond_1b
    iget-object v2, p1, LX/Apg;->j:LX/1X1;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public final g()LX/1X1;
    .locals 2

    .prologue
    .line 1716475
    invoke-super {p0}, LX/1X1;->g()LX/1X1;

    move-result-object v0

    check-cast v0, LX/Apg;

    .line 1716476
    iget-object v1, v0, LX/Apg;->j:LX/1X1;

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/Apg;->j:LX/1X1;

    invoke-virtual {v1}, LX/1X1;->g()LX/1X1;

    move-result-object v1

    :goto_0
    iput-object v1, v0, LX/Apg;->j:LX/1X1;

    .line 1716477
    return-object v0

    .line 1716478
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
