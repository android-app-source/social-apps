.class public abstract LX/AZX;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field private final a:F

.field private final b:Landroid/graphics/Paint;

.field public final c:LX/4oI;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private d:Landroid/view/View;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:LX/AZT;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1687102
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/AZX;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1687103
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1687083
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/AZX;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1687084
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1687085
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1687086
    invoke-virtual {p0}, LX/AZX;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b05ed

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, LX/AZX;->a:F

    .line 1687087
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, LX/AZX;->b:Landroid/graphics/Paint;

    .line 1687088
    iget-object v0, p0, LX/AZX;->b:Landroid/graphics/Paint;

    iget v1, p0, LX/AZX;->a:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1687089
    iget-object v0, p0, LX/AZX;->b:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1687090
    iget-object v0, p0, LX/AZX;->b:Landroid/graphics/Paint;

    invoke-virtual {p0}, LX/AZX;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a03ba

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1687091
    invoke-virtual {p0}, LX/AZX;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, LX/03r;->CreativeTools:[I

    invoke-virtual {v0, v1}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1687092
    const/16 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    .line 1687093
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1687094
    if-eqz v1, :cond_0

    .line 1687095
    new-instance v0, LX/4oI;

    invoke-direct {v0}, LX/4oI;-><init>()V

    iput-object v0, p0, LX/AZX;->c:LX/4oI;

    .line 1687096
    iget-object v0, p0, LX/AZX;->c:LX/4oI;

    invoke-virtual {v0, v1}, LX/4oI;->a(I)V

    .line 1687097
    iget-object v0, p0, LX/AZX;->c:LX/4oI;

    invoke-virtual {p0}, LX/AZX;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b05ee

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    .line 1687098
    invoke-virtual {v0, v1, v1, v1, v1}, LX/4oI;->a(FFFF)V

    .line 1687099
    iget-object v0, p0, LX/AZX;->c:LX/4oI;

    invoke-virtual {v0, p0}, LX/4oI;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 1687100
    :goto_0
    return-void

    .line 1687101
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/AZX;->c:LX/4oI;

    goto :goto_0
.end method


# virtual methods
.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    const/high16 v5, 0x40000000    # 2.0f

    .line 1687077
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 1687078
    iget-object v0, p0, LX/AZX;->d:Landroid/view/View;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/AZX;->isSelected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1687079
    iget-object v0, p0, LX/AZX;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, LX/AZX;->a:F

    div-float/2addr v1, v5

    add-float/2addr v1, v0

    iget-object v0, p0, LX/AZX;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    int-to-float v0, v0

    iget v2, p0, LX/AZX;->a:F

    div-float/2addr v2, v5

    add-float/2addr v2, v0

    iget-object v0, p0, LX/AZX;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v0

    int-to-float v0, v0

    iget v3, p0, LX/AZX;->a:F

    div-float/2addr v3, v5

    sub-float v3, v0, v3

    iget-object v0, p0, LX/AZX;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v0

    int-to-float v0, v0

    iget v4, p0, LX/AZX;->a:F

    div-float/2addr v4, v5

    sub-float v4, v0, v4

    iget-object v5, p0, LX/AZX;->b:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1687080
    :cond_0
    iget-object v0, p0, LX/AZX;->c:LX/4oI;

    if-eqz v0, :cond_1

    .line 1687081
    iget-object v0, p0, LX/AZX;->c:LX/4oI;

    invoke-virtual {v0, p1}, LX/4oI;->draw(Landroid/graphics/Canvas;)V

    .line 1687082
    :cond_1
    return-void
.end method

.method public setTileView(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1687072
    iput-object p1, p0, LX/AZX;->d:Landroid/view/View;

    .line 1687073
    iget-object v0, p0, LX/AZX;->c:LX/4oI;

    if-eqz v0, :cond_0

    .line 1687074
    iget-object v0, p0, LX/AZX;->d:Landroid/view/View;

    new-instance v1, LX/Aa4;

    invoke-direct {v1, p0}, LX/Aa4;-><init>(LX/AZX;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 1687075
    :cond_0
    iget-object v0, p0, LX/AZX;->d:Landroid/view/View;

    new-instance v1, LX/Aa5;

    invoke-direct {v1, p0}, LX/Aa5;-><init>(LX/AZX;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1687076
    return-void
.end method
