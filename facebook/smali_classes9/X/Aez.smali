.class public LX/Aez;
.super LX/AeL;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/AeL",
        "<",
        "LX/Aeu;",
        ">;"
    }
.end annotation


# static fields
.field private static final p:[I


# instance fields
.field private final A:Landroid/view/View$OnClickListener;

.field public final m:Lcom/facebook/resources/ui/FbTextView;

.field public final n:Lcom/facebook/widget/springbutton/SpringScaleButton;

.field public final o:Landroid/view/ViewStub;

.field public final q:Ljava/lang/CharSequence;

.field public final r:LX/1b4;

.field private final s:LX/AaZ;

.field public final t:LX/Aev;

.field public final u:Landroid/content/res/Resources;

.field public final v:Ljava/lang/String;

.field public final w:Z

.field public x:Ljava/lang/CharSequence;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public y:Ljava/lang/CharSequence;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public z:LX/Aeu;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1698574
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x10100a0

    aput v2, v0, v1

    sput-object v0, LX/Aez;->p:[I

    return-void
.end method

.method public constructor <init>(Landroid/view/View;LX/215;LX/Aev;LX/1b4;LX/AaZ;)V
    .locals 4
    .param p1    # Landroid/view/View;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 1698552
    invoke-direct {p0, p1}, LX/AeL;-><init>(Landroid/view/View;)V

    .line 1698553
    new-instance v0, LX/Aew;

    invoke-direct {v0, p0}, LX/Aew;-><init>(LX/Aez;)V

    iput-object v0, p0, LX/Aez;->A:Landroid/view/View$OnClickListener;

    .line 1698554
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, LX/Aez;->u:Landroid/content/res/Resources;

    .line 1698555
    const v0, 0x7f0d1940

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/Aez;->m:Lcom/facebook/resources/ui/FbTextView;

    .line 1698556
    const v0, 0x7f0d193f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/springbutton/SpringScaleButton;

    check-cast v0, Lcom/facebook/widget/springbutton/SpringScaleButton;

    iput-object v0, p0, LX/Aez;->n:Lcom/facebook/widget/springbutton/SpringScaleButton;

    .line 1698557
    const v0, 0x7f0d193e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, LX/Aez;->o:Landroid/view/ViewStub;

    .line 1698558
    iget-object v0, p0, LX/Aez;->n:Lcom/facebook/widget/springbutton/SpringScaleButton;

    invoke-virtual {v0, p2}, Lcom/facebook/widget/springbutton/SpringScaleButton;->a(LX/215;)V

    .line 1698559
    iput-object p3, p0, LX/Aez;->t:LX/Aev;

    .line 1698560
    iput-object p4, p0, LX/Aez;->r:LX/1b4;

    .line 1698561
    iput-object p5, p0, LX/Aez;->s:LX/AaZ;

    .line 1698562
    iget-object v0, p0, LX/Aez;->u:Landroid/content/res/Resources;

    const v2, 0x7f080c03

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/Aez;->v:Ljava/lang/String;

    .line 1698563
    iget-object v0, p0, LX/Aez;->r:LX/1b4;

    invoke-virtual {v0}, LX/1b4;->F()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1698564
    iget-object v0, p0, LX/Aez;->u:Landroid/content/res/Resources;

    const v2, 0x7f080c05

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1698565
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/CharSequence;

    const/4 v3, 0x0

    const-string p2, "\n"

    aput-object p2, v2, v3

    const/4 v3, 0x1

    iget-object p2, p0, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-static {v0, p2}, LX/3Hk;->b(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-static {v2}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    move-object v0, v0

    .line 1698566
    :goto_0
    iput-object v0, p0, LX/Aez;->q:Ljava/lang/CharSequence;

    .line 1698567
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 1698568
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    const v3, 0x7f01041c

    invoke-virtual {v2, v3, v0, v1}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 1698569
    iget v0, v0, Landroid/util/TypedValue;->data:I

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, LX/Aez;->w:Z

    .line 1698570
    iget-object v0, p0, LX/Aez;->n:Lcom/facebook/widget/springbutton/SpringScaleButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/springbutton/SpringScaleButton;->setVisibility(I)V

    .line 1698571
    return-void

    .line 1698572
    :cond_0
    const-string v0, ""

    goto :goto_0

    .line 1698573
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private a(LX/Aeu;LX/AeU;)V
    .locals 13

    .prologue
    const/4 v1, 0x0

    .line 1698504
    invoke-super {p0, p1, p2}, LX/AeL;->a(LX/AeP;LX/AeU;)V

    .line 1698505
    iput-object p1, p0, LX/Aez;->z:LX/Aeu;

    .line 1698506
    iget-object v0, p0, LX/Aez;->m:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setClickable(Z)V

    .line 1698507
    iget-object v0, p0, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    .line 1698508
    iget-boolean v0, p2, LX/AeU;->f:Z

    iget-object v1, p0, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1698509
    if-eqz v0, :cond_4

    .line 1698510
    new-array v2, v6, [Ljava/lang/CharSequence;

    iget-object v3, p1, LX/AeP;->a:LX/AcC;

    iget-object v3, v3, LX/AcC;->a:Ljava/lang/String;

    invoke-static {v3, v1}, LX/3Hk;->a(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v3

    aput-object v3, v2, v4

    const-string v3, "\n"

    aput-object v3, v2, v5

    invoke-static {v2}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 1698511
    :goto_0
    move-object v0, v2

    .line 1698512
    invoke-static {p0, p1}, LX/Aez;->a(LX/Aez;LX/Aeu;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p1, LX/Aeu;->q:Z

    if-nez v1, :cond_1

    :cond_0
    iget-boolean v1, p1, LX/Aeu;->p:Z

    if-eqz v1, :cond_3

    .line 1698513
    :cond_1
    invoke-static {p0, p1, v0}, LX/Aez;->a$redex0(LX/Aez;LX/Aeu;Ljava/lang/CharSequence;)V

    .line 1698514
    :goto_1
    iget-boolean v0, p0, LX/Aez;->w:Z

    if-eqz v0, :cond_8

    .line 1698515
    :goto_2
    invoke-virtual {p2}, LX/AeU;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1698516
    iget-boolean v0, p1, LX/Aeu;->o:Z

    const/high16 v3, 0x3f800000    # 1.0f

    const/high16 v2, 0x3f000000    # 0.5f

    .line 1698517
    if-eqz v0, :cond_a

    .line 1698518
    iget-object v1, p0, LX/AeL;->l:Lcom/facebook/facecastdisplay/FacecastUserTileView;

    invoke-virtual {v1, v2}, Lcom/facebook/facecastdisplay/FacecastUserTileView;->setAlpha(F)V

    .line 1698519
    iget-object v1, p0, LX/Aez;->m:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setAlpha(F)V

    .line 1698520
    iget-object v1, p0, LX/Aez;->o:Landroid/view/ViewStub;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/ViewStub;->setVisibility(I)V

    .line 1698521
    :goto_3
    iget-boolean v0, p1, LX/Aeu;->o:Z

    if-nez v0, :cond_2

    iget-object v0, p1, LX/AeP;->a:LX/AcC;

    iget-object v0, v0, LX/AcC;->b:Ljava/lang/String;

    iget-object v1, p2, LX/AeU;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1698522
    iget-object v0, p0, LX/1a1;->a:Landroid/view/View;

    iget-object v1, p0, LX/Aez;->A:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1698523
    :cond_2
    return-void

    .line 1698524
    :cond_3
    invoke-static {p0, p1}, LX/Aez;->a(LX/Aez;LX/Aeu;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 1698525
    iget-object v2, p1, LX/Aeu;->k:Ljava/lang/String;

    .line 1698526
    iget-object v1, p0, LX/Aez;->q:Ljava/lang/CharSequence;

    .line 1698527
    :goto_4
    iget-object v3, p0, LX/Aez;->m:Lcom/facebook/resources/ui/FbTextView;

    const/4 v4, 0x5

    new-array v4, v4, [Ljava/lang/CharSequence;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    const/4 v5, 0x1

    aput-object v2, v4, v5

    const/4 v2, 0x2

    const-string v5, "\u2026 "

    aput-object v5, v4, v2

    const/4 v2, 0x3

    iget-object v5, p0, LX/Aez;->v:Ljava/lang/String;

    aput-object v5, v4, v2

    const/4 v2, 0x4

    aput-object v1, v4, v2

    invoke-static {v4}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v3, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1698528
    iget-object v1, p0, LX/Aez;->m:Lcom/facebook/resources/ui/FbTextView;

    new-instance v2, LX/Aex;

    invoke-direct {v2, p0, p1, v0}, LX/Aex;-><init>(LX/Aez;LX/Aeu;Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1698529
    goto :goto_1

    :cond_4
    const/4 v2, 0x6

    new-array v2, v2, [Ljava/lang/CharSequence;

    iget-object v3, p1, LX/AeP;->a:LX/AcC;

    iget-object v3, v3, LX/AcC;->a:Ljava/lang/String;

    invoke-static {v3, v1}, LX/3Hk;->a(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v3

    aput-object v3, v2, v4

    const-string v3, " "

    aput-object v3, v2, v5

    iget-boolean v3, p1, LX/Aeu;->l:Z

    .line 1698530
    iget-object v4, p0, LX/Aez;->x:Ljava/lang/CharSequence;

    if-nez v4, :cond_5

    .line 1698531
    const/4 v4, 0x1

    iget-object v5, p0, LX/Aez;->u:Landroid/content/res/Resources;

    invoke-static {v4, v5}, LX/3Hk;->a(ZLandroid/content/res/Resources;)Ljava/lang/CharSequence;

    move-result-object v4

    iput-object v4, p0, LX/Aez;->x:Ljava/lang/CharSequence;

    .line 1698532
    :cond_5
    iget-object v4, p0, LX/Aez;->y:Ljava/lang/CharSequence;

    if-nez v4, :cond_6

    .line 1698533
    const/4 v4, 0x0

    iget-object v5, p0, LX/Aez;->u:Landroid/content/res/Resources;

    invoke-static {v4, v5}, LX/3Hk;->a(ZLandroid/content/res/Resources;)Ljava/lang/CharSequence;

    move-result-object v4

    iput-object v4, p0, LX/Aez;->y:Ljava/lang/CharSequence;

    .line 1698534
    :cond_6
    if-eqz v3, :cond_7

    iget-object v4, p0, LX/Aez;->x:Ljava/lang/CharSequence;

    :goto_5
    move-object v3, v4

    .line 1698535
    aput-object v3, v2, v6

    const/4 v3, 0x3

    const-string v4, " "

    aput-object v4, v2, v3

    const/4 v3, 0x4

    iget v4, p1, LX/Aeu;->m:I

    .line 1698536
    new-instance v7, Landroid/text/SpannableString;

    int-to-long v9, v4

    const-wide/16 v11, 0x3e8

    mul-long/2addr v9, v11

    invoke-static {v9, v10}, LX/7LQ;->a(J)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 1698537
    new-instance v8, Landroid/text/style/ForegroundColorSpan;

    iget-object v9, p0, LX/Aez;->u:Landroid/content/res/Resources;

    const v10, 0x7f0a038f

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    invoke-direct {v8, v9}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    .line 1698538
    const/4 v9, 0x0

    invoke-virtual {v7}, Landroid/text/SpannableString;->length()I

    move-result v10

    const/16 v11, 0x21

    invoke-virtual {v7, v8, v9, v10, v11}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 1698539
    move-object v4, v7

    .line 1698540
    aput-object v4, v2, v3

    const/4 v3, 0x5

    const-string v4, "\n"

    aput-object v4, v2, v3

    invoke-static {v2}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v2

    goto/16 :goto_0

    :cond_7
    iget-object v4, p0, LX/Aez;->y:Ljava/lang/CharSequence;

    goto :goto_5

    .line 1698541
    :cond_8
    iget-boolean v0, p1, LX/Aeu;->h:Z

    if-eqz v0, :cond_9

    iget-boolean v0, p1, LX/Aeu;->o:Z

    if-nez v0, :cond_9

    .line 1698542
    iget-object v0, p0, LX/Aez;->n:Lcom/facebook/widget/springbutton/SpringScaleButton;

    new-instance v1, LX/Aey;

    invoke-direct {v1, p0, p1, p2}, LX/Aey;-><init>(LX/Aez;LX/Aeu;LX/AeU;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/springbutton/SpringScaleButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1698543
    iget-boolean v0, p1, LX/Aeu;->g:Z

    invoke-virtual {p0, v0}, LX/Aez;->b(Z)V

    .line 1698544
    iget-object v0, p0, LX/Aez;->n:Lcom/facebook/widget/springbutton/SpringScaleButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/springbutton/SpringScaleButton;->setVisibility(I)V

    goto/16 :goto_2

    .line 1698545
    :cond_9
    iget-object v0, p0, LX/Aez;->n:Lcom/facebook/widget/springbutton/SpringScaleButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/springbutton/SpringScaleButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1698546
    iget-object v0, p0, LX/Aez;->n:Lcom/facebook/widget/springbutton/SpringScaleButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/springbutton/SpringScaleButton;->setVisibility(I)V

    goto/16 :goto_2

    .line 1698547
    :cond_a
    iget-object v1, p0, LX/AeL;->l:Lcom/facebook/facecastdisplay/FacecastUserTileView;

    invoke-virtual {v1, v3}, Lcom/facebook/facecastdisplay/FacecastUserTileView;->setAlpha(F)V

    .line 1698548
    iget-object v1, p0, LX/Aez;->m:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v3}, Lcom/facebook/resources/ui/FbTextView;->setAlpha(F)V

    .line 1698549
    iget-object v1, p0, LX/Aez;->o:Landroid/view/ViewStub;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/ViewStub;->setVisibility(I)V

    goto/16 :goto_3

    .line 1698550
    :cond_b
    iget-object v2, p1, LX/Aeu;->c:Ljava/lang/String;

    .line 1698551
    const-string v1, ""

    goto/16 :goto_4
.end method

.method public static a(LX/Aez;LX/Aeu;)Z
    .locals 1

    .prologue
    .line 1698503
    iget-object v0, p0, LX/Aez;->s:LX/AaZ;

    invoke-virtual {v0}, LX/AaZ;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/Aeu;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a$redex0(LX/Aez;LX/Aeu;Ljava/lang/CharSequence;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1698494
    invoke-static {p0, p1}, LX/Aez;->a(LX/Aez;LX/Aeu;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1698495
    iget-object v1, p1, LX/Aeu;->j:Ljava/lang/String;

    .line 1698496
    iget-object v0, p0, LX/Aez;->q:Ljava/lang/CharSequence;

    .line 1698497
    :goto_0
    iget-object v2, p0, LX/Aez;->m:Lcom/facebook/resources/ui/FbTextView;

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/CharSequence;

    aput-object p2, v3, v5

    const/4 v4, 0x1

    aput-object v1, v3, v4

    const/4 v1, 0x2

    aput-object v0, v3, v1

    invoke-static {v3}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1698498
    iget-object v0, p0, LX/Aez;->m:Lcom/facebook/resources/ui/FbTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1698499
    iget-object v0, p0, LX/Aez;->m:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v5}, Lcom/facebook/resources/ui/FbTextView;->setClickable(Z)V

    .line 1698500
    return-void

    .line 1698501
    :cond_0
    iget-object v1, p1, LX/Aeu;->b:Ljava/lang/String;

    .line 1698502
    const-string v0, ""

    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic a(LX/AeO;LX/AeU;)V
    .locals 0

    .prologue
    .line 1698493
    check-cast p1, LX/Aeu;

    invoke-direct {p0, p1, p2}, LX/Aez;->a(LX/Aeu;LX/AeU;)V

    return-void
.end method

.method public final bridge synthetic a(LX/AeP;LX/AeU;)V
    .locals 0

    .prologue
    .line 1698492
    check-cast p1, LX/Aeu;

    invoke-direct {p0, p1, p2}, LX/Aez;->a(LX/Aeu;LX/AeU;)V

    return-void
.end method

.method public final b(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1698486
    if-eqz p1, :cond_0

    .line 1698487
    iget-object v0, p0, LX/Aez;->n:Lcom/facebook/widget/springbutton/SpringScaleButton;

    sget-object v1, LX/Aez;->p:[I

    invoke-virtual {v0, v1, v2}, Lcom/facebook/widget/springbutton/SpringScaleButton;->setImageState([IZ)V

    .line 1698488
    iget-object v0, p0, LX/Aez;->n:Lcom/facebook/widget/springbutton/SpringScaleButton;

    iget-object v1, p0, LX/Aez;->u:Landroid/content/res/Resources;

    const v2, 0x7f080c2e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/springbutton/SpringScaleButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1698489
    :goto_0
    return-void

    .line 1698490
    :cond_0
    iget-object v0, p0, LX/Aez;->n:Lcom/facebook/widget/springbutton/SpringScaleButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/facebook/widget/springbutton/SpringScaleButton;->setImageState([IZ)V

    .line 1698491
    iget-object v0, p0, LX/Aez;->n:Lcom/facebook/widget/springbutton/SpringScaleButton;

    iget-object v1, p0, LX/Aez;->u:Landroid/content/res/Resources;

    const v2, 0x7f080c2d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/springbutton/SpringScaleButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
