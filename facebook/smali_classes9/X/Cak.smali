.class public LX/Cak;
.super LX/8Hb;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final a:LX/Cbs;

.field private final b:Lcom/facebook/content/SecureContextHelper;

.field private final c:LX/0wM;

.field private final d:LX/9hJ;

.field private final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/215;",
            ">;"
        }
    .end annotation
.end field

.field private f:LX/5kD;

.field private g:Lcom/facebook/base/fragment/FbFragment;


# direct methods
.method public constructor <init>(LX/Cbs;Lcom/facebook/content/SecureContextHelper;LX/0wM;LX/9hJ;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Cbs;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/0wM;",
            "LX/9hJ;",
            "LX/0Or",
            "<",
            "LX/215;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1918550
    invoke-direct {p0}, LX/8Hb;-><init>()V

    .line 1918551
    iput-object p1, p0, LX/Cak;->a:LX/Cbs;

    .line 1918552
    iput-object p2, p0, LX/Cak;->b:Lcom/facebook/content/SecureContextHelper;

    .line 1918553
    iput-object p3, p0, LX/Cak;->c:LX/0wM;

    .line 1918554
    iput-object p4, p0, LX/Cak;->d:LX/9hJ;

    .line 1918555
    iput-object p5, p0, LX/Cak;->e:LX/0Or;

    .line 1918556
    return-void
.end method


# virtual methods
.method public final a(LX/5kD;Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;Lcom/facebook/base/fragment/FbFragment;Lcom/facebook/photos/galleryutil/GalleryDeepLinkBinder$DeepLinkBinderConfig;)Z
    .locals 3

    .prologue
    .line 1918532
    iput-object p1, p0, LX/Cak;->f:LX/5kD;

    .line 1918533
    iput-object p3, p0, LX/Cak;->g:Lcom/facebook/base/fragment/FbFragment;

    .line 1918534
    iget-object v0, p0, LX/Cak;->f:LX/5kD;

    invoke-interface {v0}, LX/5kD;->u()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Cak;->f:LX/5kD;

    invoke-static {v0}, LX/9hJ;->a(LX/5kD;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1918535
    :cond_0
    const/4 v0, 0x0

    .line 1918536
    :goto_0
    return v0

    .line 1918537
    :cond_1
    iget-object v0, p0, LX/Cak;->c:LX/0wM;

    const v1, 0x7f02089c

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1918538
    const v0, 0x7f0819e6

    invoke-virtual {p2, v0}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setText(I)V

    .line 1918539
    invoke-virtual {p2, p0}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1918540
    iget-object v0, p0, LX/Cak;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/215;

    invoke-virtual {p2, v0}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setSpring(LX/215;)V

    .line 1918541
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1918547
    iput-object v0, p0, LX/Cak;->f:LX/5kD;

    .line 1918548
    iput-object v0, p0, LX/Cak;->g:Lcom/facebook/base/fragment/FbFragment;

    .line 1918549
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, 0x72894ce0

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1918542
    iget-object v1, p0, LX/Cak;->f:LX/5kD;

    invoke-interface {v1}, LX/5kD;->u()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/Cak;->f:LX/5kD;

    invoke-static {v1}, LX/9hJ;->a(LX/5kD;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1918543
    :cond_0
    const v1, 0x60893e9

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1918544
    :goto_0
    return-void

    .line 1918545
    :cond_1
    iget-object v1, p0, LX/Cak;->b:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/Cak;->a:LX/Cbs;

    iget-object v3, p0, LX/Cak;->f:LX/5kD;

    invoke-virtual {v2, v3}, LX/Cbs;->a(LX/5kD;)Landroid/content/Intent;

    move-result-object v2

    const/16 v3, 0x1389

    iget-object v4, p0, LX/Cak;->g:Lcom/facebook/base/fragment/FbFragment;

    invoke-interface {v1, v2, v3, v4}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 1918546
    const v1, 0x6c8987a3

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto :goto_0
.end method
