.class public final LX/Bvo;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/feed/video/fullscreen/SubtitleDialog;

.field private b:[Ljava/util/Locale;

.field public c:[Ljava/lang/String;

.field public d:I


# direct methods
.method public constructor <init>(Lcom/facebook/feed/video/fullscreen/SubtitleDialog;LX/0Px;Ljava/lang/String;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v6, -0x1

    const/4 v5, 0x0

    .line 1832833
    iput-object p1, p0, LX/Bvo;->a:Lcom/facebook/feed/video/fullscreen/SubtitleDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1832834
    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v7

    .line 1832835
    new-array v0, v7, [Ljava/util/Locale;

    iput-object v0, p0, LX/Bvo;->b:[Ljava/util/Locale;

    .line 1832836
    add-int/lit8 v0, v7, 0x1

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, LX/Bvo;->c:[Ljava/lang/String;

    .line 1832837
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v8

    .line 1832838
    iget-object v0, p1, Lcom/facebook/feed/video/fullscreen/SubtitleDialog;->r:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/1mI;->k:LX/0Tn;

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0e9;->a(Ljava/lang/String;)Ljava/util/Locale;

    move-result-object v9

    move v1, v5

    move v3, v6

    move v2, v6

    .line 1832839
    :goto_0
    if-ge v1, v7, :cond_0

    .line 1832840
    iget-object v10, p0, LX/Bvo;->b:[Ljava/util/Locale;

    invoke-virtual {p2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, LX/0e9;->a(Ljava/lang/String;)Ljava/util/Locale;

    move-result-object v0

    aput-object v0, v10, v1

    .line 1832841
    iget-object v0, p0, LX/Bvo;->c:[Ljava/lang/String;

    iget-object v10, p0, LX/Bvo;->b:[Ljava/util/Locale;

    aget-object v10, v10, v1

    invoke-static {v10}, LX/0e9;->a(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v0, v1

    .line 1832842
    iget-object v0, p0, LX/Bvo;->b:[Ljava/util/Locale;

    aget-object v0, v0, v1

    invoke-virtual {v0, v8}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    move v0, v1

    .line 1832843
    :goto_1
    iget-object v2, p0, LX/Bvo;->b:[Ljava/util/Locale;

    aget-object v2, v2, v1

    invoke-virtual {v2, v9}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    move v2, v1

    .line 1832844
    :goto_2
    add-int/lit8 v1, v1, 0x1

    move v3, v2

    move v2, v0

    goto :goto_0

    .line 1832845
    :cond_0
    if-ne v3, v2, :cond_2

    move v0, v4

    .line 1832846
    :goto_3
    iget-object v1, p0, LX/Bvo;->c:[Ljava/lang/String;

    aput-object p3, v1, v7

    .line 1832847
    if-eq v3, v6, :cond_3

    .line 1832848
    invoke-direct {p0, v5, v3}, LX/Bvo;->a(II)V

    .line 1832849
    if-nez v2, :cond_7

    .line 1832850
    :goto_4
    iput v5, p0, LX/Bvo;->d:I

    .line 1832851
    if-eq v3, v6, :cond_1

    if-nez v0, :cond_1

    .line 1832852
    invoke-direct {p0, v4, v3}, LX/Bvo;->a(II)V

    .line 1832853
    const/4 v4, 0x2

    .line 1832854
    :cond_1
    :goto_5
    add-int/lit8 v1, v4, 0x1

    :goto_6
    if-ge v1, v7, :cond_5

    move v0, v1

    .line 1832855
    :goto_7
    if-le v0, v4, :cond_4

    iget-object v2, p0, LX/Bvo;->c:[Ljava/lang/String;

    add-int/lit8 v3, v0, -0x1

    aget-object v2, v2, v3

    iget-object v3, p0, LX/Bvo;->c:[Ljava/lang/String;

    aget-object v3, v3, v0

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-lez v2, :cond_4

    .line 1832856
    add-int/lit8 v2, v0, -0x1

    invoke-direct {p0, v0, v2}, LX/Bvo;->a(II)V

    .line 1832857
    add-int/lit8 v0, v0, -0x1

    goto :goto_7

    :cond_2
    move v0, v5

    .line 1832858
    goto :goto_3

    .line 1832859
    :cond_3
    if-eq v2, v6, :cond_6

    .line 1832860
    invoke-direct {p0, v5, v2}, LX/Bvo;->a(II)V

    .line 1832861
    :goto_8
    iput v7, p0, LX/Bvo;->d:I

    goto :goto_5

    .line 1832862
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 1832863
    :cond_5
    return-void

    :cond_6
    move v4, v5

    goto :goto_8

    :cond_7
    move v3, v2

    goto :goto_4

    :cond_8
    move v2, v3

    goto :goto_2

    :cond_9
    move v0, v2

    goto :goto_1
.end method

.method private a(II)V
    .locals 4

    .prologue
    .line 1832867
    iget-object v0, p0, LX/Bvo;->b:[Ljava/util/Locale;

    aget-object v0, v0, p1

    .line 1832868
    iget-object v1, p0, LX/Bvo;->c:[Ljava/lang/String;

    aget-object v1, v1, p1

    .line 1832869
    iget-object v2, p0, LX/Bvo;->b:[Ljava/util/Locale;

    iget-object v3, p0, LX/Bvo;->b:[Ljava/util/Locale;

    aget-object v3, v3, p2

    aput-object v3, v2, p1

    .line 1832870
    iget-object v2, p0, LX/Bvo;->c:[Ljava/lang/String;

    iget-object v3, p0, LX/Bvo;->c:[Ljava/lang/String;

    aget-object v3, v3, p2

    aput-object v3, v2, p1

    .line 1832871
    iget-object v2, p0, LX/Bvo;->b:[Ljava/util/Locale;

    aput-object v0, v2, p2

    .line 1832872
    iget-object v0, p0, LX/Bvo;->c:[Ljava/lang/String;

    aput-object v1, v0, p2

    .line 1832873
    return-void
.end method

.method public static a$redex0(LX/Bvo;I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1832864
    iget-object v0, p0, LX/Bvo;->b:[Ljava/util/Locale;

    array-length v0, v0

    if-ge p1, v0, :cond_0

    if-gez p1, :cond_1

    .line 1832865
    :cond_0
    const-string v0, ""

    .line 1832866
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, LX/Bvo;->b:[Ljava/util/Locale;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
