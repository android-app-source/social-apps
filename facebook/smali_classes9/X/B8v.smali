.class public LX/B8v;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public a:LX/B6l;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:LX/B6k;

.field private c:Landroid/content/Context;


# direct methods
.method public constructor <init>(LX/B6k;Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1750774
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1750775
    iput-object p1, p0, LX/B8v;->b:LX/B6k;

    .line 1750776
    iput-object p2, p0, LX/B8v;->c:Landroid/content/Context;

    .line 1750777
    return-void
.end method

.method public static a(LX/0QB;)LX/B8v;
    .locals 5

    .prologue
    .line 1750761
    const-class v1, LX/B8v;

    monitor-enter v1

    .line 1750762
    :try_start_0
    sget-object v0, LX/B8v;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1750763
    sput-object v2, LX/B8v;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1750764
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1750765
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1750766
    new-instance p0, LX/B8v;

    const-class v3, LX/B6k;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/B6k;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-direct {p0, v3, v4}, LX/B8v;-><init>(LX/B6k;Landroid/content/Context;)V

    .line 1750767
    invoke-static {v0}, LX/B6l;->a(LX/0QB;)LX/B6l;

    move-result-object v3

    check-cast v3, LX/B6l;

    .line 1750768
    iput-object v3, p0, LX/B8v;->a:LX/B6l;

    .line 1750769
    move-object v0, p0

    .line 1750770
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1750771
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/B8v;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1750772
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1750773
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Landroid/view/View;Landroid/widget/TextView;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 1750745
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 1750746
    :goto_0
    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/facebook/widget/FbScrollView;

    if-nez v1, :cond_0

    .line 1750747
    invoke-interface {v0}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    goto :goto_0

    .line 1750748
    :cond_0
    check-cast v0, Lcom/facebook/widget/FbScrollView;

    .line 1750749
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 1750750
    invoke-virtual {v0, v1}, Lcom/facebook/widget/FbScrollView;->getHitRect(Landroid/graphics/Rect;)V

    .line 1750751
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b09ef

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 1750752
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b1985

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 1750753
    const/4 v4, 0x2

    new-array v4, v4, [I

    .line 1750754
    invoke-virtual {p0, v4}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 1750755
    invoke-virtual {p0, v1}, Landroid/view/View;->getLocalVisibleRect(Landroid/graphics/Rect;)Z

    move-result v5

    if-eqz v5, :cond_1

    aget v5, v4, v6

    if-lt v5, v3, :cond_1

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->getLocalVisibleRect(Landroid/graphics/Rect;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 1750756
    :cond_1
    aget v1, v4, v6

    invoke-virtual {p0}, Landroid/view/View;->getHeight()I

    move-result v4

    add-int/2addr v1, v4

    invoke-virtual {p1}, Landroid/widget/TextView;->getHeight()I

    move-result v4

    add-int/2addr v1, v4

    add-int/2addr v1, v2

    .line 1750757
    if-ge v1, v3, :cond_2

    .line 1750758
    add-int/2addr v1, v3

    .line 1750759
    :cond_2
    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Lcom/facebook/widget/FbScrollView;->scrollTo(II)V

    .line 1750760
    :cond_3
    return-void
.end method

.method public static a(Landroid/widget/TextView;)V
    .locals 1

    .prologue
    .line 1750740
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1750741
    return-void
.end method

.method public static a(Landroid/widget/TextView;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1750742
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1750743
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1750744
    return-void
.end method
