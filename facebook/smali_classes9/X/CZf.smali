.class public LX/CZf;
.super Lcom/facebook/resources/ui/FbTextView;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1916370
    invoke-direct {p0, p1}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;)V

    .line 1916371
    invoke-virtual {p0}, LX/CZf;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0050

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    invoke-virtual {p0}, LX/CZf;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->scaledDensity:F

    div-float/2addr v0, v1

    .line 1916372
    const/4 v1, 0x2

    invoke-virtual {p0, v1, v0}, LX/CZf;->setTextSize(IF)V

    .line 1916373
    invoke-virtual {p0}, LX/CZf;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0158

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, LX/CZf;->setTextColor(I)V

    .line 1916374
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/CZf;->setIncludeFontPadding(Z)V

    .line 1916375
    invoke-virtual {p0}, LX/CZf;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0159

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, LX/CZf;->setHighlightColor(I)V

    .line 1916376
    return-void
.end method
