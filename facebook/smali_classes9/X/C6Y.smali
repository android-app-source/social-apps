.class public final LX/C6Y;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/17z;


# instance fields
.field public final a:Z

.field public final b:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/C6a;

.field public final h:LX/C6H;


# direct methods
.method public constructor <init>(ZLcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0Px;LX/C6a;LX/C6H;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/C6a;",
            "LX/C6H;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1849988
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1849989
    iput-boolean p1, p0, LX/C6Y;->a:Z

    .line 1849990
    iput-object p2, p0, LX/C6Y;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1849991
    iput-object p3, p0, LX/C6Y;->c:Ljava/lang/String;

    .line 1849992
    iput-object p4, p0, LX/C6Y;->d:Ljava/lang/String;

    .line 1849993
    iput-object p5, p0, LX/C6Y;->e:Ljava/lang/String;

    .line 1849994
    iput-object p6, p0, LX/C6Y;->f:LX/0Px;

    .line 1849995
    iput-object p7, p0, LX/C6Y;->g:LX/C6a;

    .line 1849996
    iput-object p8, p0, LX/C6Y;->h:LX/C6H;

    .line 1849997
    return-void
.end method


# virtual methods
.method public final g()Lcom/facebook/feed/rows/core/props/FeedProps;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1849998
    iget-object v0, p0, LX/C6Y;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    return-object v0
.end method
