.class public final LX/Adw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:LX/Ae2;


# direct methods
.method public constructor <init>(LX/Ae2;)V
    .locals 0

    .prologue
    .line 1695699
    iput-object p1, p0, LX/Adw;->a:LX/Ae2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    .line 1695700
    iget-object v0, p0, LX/Adw;->a:LX/Ae2;

    .line 1695701
    iget-object v1, v0, LX/AVi;->a:Landroid/view/View;

    move-object v1, v1

    .line 1695702
    check-cast v1, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;

    invoke-virtual {v1}, Lcom/facebook/facecastdisplay/feedback/LiveFeedbackInputViewContainer;->getContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, LX/0ew;

    invoke-static {v1, v2}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0ew;

    .line 1695703
    if-eqz v1, :cond_0

    iget-object v2, v0, LX/Ae2;->O:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v2, :cond_0

    iget-object v2, v0, LX/Ae2;->O:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v2}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, v0, LX/Ae2;->O:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v2}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    if-nez v2, :cond_1

    .line 1695704
    :cond_0
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 1695705
    :cond_1
    invoke-interface {v1}, LX/0ew;->iC_()LX/0gc;

    move-result-object v2

    const-string p0, "live_video_invite_friend_dialog"

    invoke-virtual {v2, p0}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    if-nez v2, :cond_0

    .line 1695706
    new-instance v2, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoFriendInviterDialog;

    invoke-direct {v2}, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoFriendInviterDialog;-><init>()V

    iput-object v2, v0, LX/Ae2;->R:Lcom/facebook/facecastdisplay/friendInviter/LiveVideoFriendInviterDialog;

    .line 1695707
    iget-object v2, v0, LX/Ae2;->R:Lcom/facebook/facecastdisplay/friendInviter/LiveVideoFriendInviterDialog;

    iget-object p0, v0, LX/Ae2;->W:Ljava/lang/String;

    .line 1695708
    iput-object p0, v2, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoFriendInviterDialog;->o:Ljava/lang/String;

    .line 1695709
    iget-object v2, v0, LX/Ae2;->O:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 1695710
    iget-object v2, v0, LX/Ae2;->R:Lcom/facebook/facecastdisplay/friendInviter/LiveVideoFriendInviterDialog;

    iget-object p0, v0, LX/Ae2;->O:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object p0

    .line 1695711
    iput-object p0, v2, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoFriendInviterDialog;->q:Ljava/lang/String;

    .line 1695712
    iget-object v2, v0, LX/Ae2;->O:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLProfile;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 1695713
    iget-object v2, v0, LX/Ae2;->R:Lcom/facebook/facecastdisplay/friendInviter/LiveVideoFriendInviterDialog;

    iget-object p0, v0, LX/Ae2;->O:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLProfile;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result p0

    .line 1695714
    iput p0, v2, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoFriendInviterDialog;->r:I

    .line 1695715
    :cond_2
    iget-object v2, v0, LX/Ae2;->R:Lcom/facebook/facecastdisplay/friendInviter/LiveVideoFriendInviterDialog;

    invoke-interface {v1}, LX/0ew;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    const-string p0, "live_video_invite_friend_dialog"

    const/4 p1, 0x1

    invoke-virtual {v2, v1, p0, p1}, Landroid/support/v4/app/DialogFragment;->a(LX/0hH;Ljava/lang/String;Z)I

    .line 1695716
    iget-object v1, v0, LX/Ae2;->S:LX/Adv;

    if-nez v1, :cond_3

    .line 1695717
    new-instance v1, LX/Adv;

    invoke-direct {v1, v0}, LX/Adv;-><init>(LX/Ae2;)V

    iput-object v1, v0, LX/Ae2;->S:LX/Adv;

    .line 1695718
    :cond_3
    iget-object v1, v0, LX/Ae2;->R:Lcom/facebook/facecastdisplay/friendInviter/LiveVideoFriendInviterDialog;

    iget-object v2, v0, LX/Ae2;->S:LX/Adv;

    .line 1695719
    iput-object v2, v1, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoFriendInviterDialog;->p:LX/Adv;

    .line 1695720
    iget-object v1, v0, LX/Ae2;->k:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 1695721
    const-string v1, "invite_friend_tapped"

    invoke-static {v0, v1}, LX/Ae2;->a$redex0(LX/Ae2;Ljava/lang/String;)V

    goto :goto_0
.end method
