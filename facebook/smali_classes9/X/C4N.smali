.class public LX/C4N;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ":",
        "LX/1Pg;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Po;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static i:LX/0Xm;


# instance fields
.field public final a:LX/1We;

.field public final b:LX/1DR;

.field public final c:LX/1VF;

.field public final d:LX/14w;

.field private final e:LX/0qn;

.field public final f:LX/39e;

.field private final g:LX/1WX;

.field private final h:LX/1XK;


# direct methods
.method public constructor <init>(LX/1We;LX/1DR;LX/1VF;LX/14w;LX/0qn;LX/39e;LX/1WX;LX/1XK;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1847159
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1847160
    iput-object p1, p0, LX/C4N;->a:LX/1We;

    .line 1847161
    iput-object p2, p0, LX/C4N;->b:LX/1DR;

    .line 1847162
    iput-object p3, p0, LX/C4N;->c:LX/1VF;

    .line 1847163
    iput-object p4, p0, LX/C4N;->d:LX/14w;

    .line 1847164
    iput-object p5, p0, LX/C4N;->e:LX/0qn;

    .line 1847165
    iput-object p6, p0, LX/C4N;->f:LX/39e;

    .line 1847166
    iput-object p7, p0, LX/C4N;->g:LX/1WX;

    .line 1847167
    iput-object p8, p0, LX/C4N;->h:LX/1XK;

    .line 1847168
    return-void
.end method

.method private static a(LX/C4N;LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pr;ZI)LX/1Di;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;ZI)",
            "LX/1Di;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1847169
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1847170
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1847171
    invoke-static {p2}, LX/14w;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {v0, v2, v2}, LX/1WY;->a(Lcom/facebook/graphql/model/GraphQLStory;ZZ)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1847172
    :cond_0
    const/4 v0, 0x0

    .line 1847173
    :goto_0
    return-object v0

    .line 1847174
    :cond_1
    iget-object v1, p0, LX/C4N;->g:LX/1WX;

    invoke-virtual {v1, p1, v2, p5}, LX/1WX;->a(LX/1De;II)LX/1XJ;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/1XJ;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/1XJ;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/1XJ;->c(Z)LX/1XJ;

    move-result-object v0

    .line 1847175
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    if-nez p4, :cond_2

    :goto_1
    invoke-interface {v1, v0}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v0

    const/16 v1, 0x8

    const v2, 0x7f0b00d6

    invoke-interface {v0, v1, v2}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v0

    goto :goto_0

    :cond_2
    iget-object v2, p0, LX/C4N;->h:LX/1XK;

    invoke-virtual {v2, p1}, LX/1XK;->c(LX/1De;)LX/1XM;

    move-result-object v2

    invoke-virtual {v2, p2}, LX/1XM;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1XM;

    move-result-object v2

    check-cast p3, LX/1Po;

    invoke-virtual {v2, p3}, LX/1XM;->a(LX/1Po;)LX/1XM;

    move-result-object v2

    invoke-virtual {v2, v0}, LX/1XM;->a(LX/1X5;)LX/1XM;

    move-result-object v0

    goto :goto_1
.end method

.method public static a(LX/0QB;)LX/C4N;
    .locals 12

    .prologue
    .line 1847176
    const-class v1, LX/C4N;

    monitor-enter v1

    .line 1847177
    :try_start_0
    sget-object v0, LX/C4N;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1847178
    sput-object v2, LX/C4N;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1847179
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1847180
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1847181
    new-instance v3, LX/C4N;

    invoke-static {v0}, LX/1We;->a(LX/0QB;)LX/1We;

    move-result-object v4

    check-cast v4, LX/1We;

    invoke-static {v0}, LX/1DR;->a(LX/0QB;)LX/1DR;

    move-result-object v5

    check-cast v5, LX/1DR;

    invoke-static {v0}, LX/1VF;->a(LX/0QB;)LX/1VF;

    move-result-object v6

    check-cast v6, LX/1VF;

    invoke-static {v0}, LX/14w;->a(LX/0QB;)LX/14w;

    move-result-object v7

    check-cast v7, LX/14w;

    invoke-static {v0}, LX/0qn;->a(LX/0QB;)LX/0qn;

    move-result-object v8

    check-cast v8, LX/0qn;

    invoke-static {v0}, LX/39e;->a(LX/0QB;)LX/39e;

    move-result-object v9

    check-cast v9, LX/39e;

    invoke-static {v0}, LX/1WX;->a(LX/0QB;)LX/1WX;

    move-result-object v10

    check-cast v10, LX/1WX;

    invoke-static {v0}, LX/1XK;->a(LX/0QB;)LX/1XK;

    move-result-object v11

    check-cast v11, LX/1XK;

    invoke-direct/range {v3 .. v11}, LX/C4N;-><init>(LX/1We;LX/1DR;LX/1VF;LX/14w;LX/0qn;LX/39e;LX/1WX;LX/1XK;)V

    .line 1847182
    move-object v0, v3

    .line 1847183
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1847184
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C4N;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1847185
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1847186
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pr;IIZZ)LX/1Dg;
    .locals 8
    .param p2    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # LX/1Pr;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p4    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->INT:LX/32B;
        .end annotation
    .end param
    .param p5    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->COLOR:LX/32B;
        .end annotation
    .end param
    .param p6    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p7    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;IIZZ)",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    .line 1847187
    iget-object v1, p0, LX/C4N;->e:LX/0qn;

    .line 1847188
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1847189
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1, v0}, LX/0qn;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v6

    .line 1847190
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v0

    const/4 v1, 0x4

    invoke-interface {v0, v1}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v7

    if-eqz p7, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-interface {v7, v0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    if-eqz v6, :cond_1

    .line 1847191
    invoke-static {p1}, LX/25N;->c(LX/1De;)LX/25Q;

    move-result-object v0

    invoke-virtual {v0, p5}, LX/25Q;->h(I)LX/25Q;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    const/4 v2, 0x1

    invoke-interface {v0, v2}, LX/1Di;->r(I)LX/1Di;

    move-result-object v0

    const/4 v2, 0x6

    const v3, 0x7f0b00d6

    invoke-interface {v0, v2, v3}, LX/1Di;->g(II)LX/1Di;

    move-result-object v0

    move-object v0, v0

    .line 1847192
    :goto_1
    invoke-interface {v1, v0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    if-eqz v6, :cond_2

    .line 1847193
    iget-object v0, p0, LX/C4N;->a:LX/1We;

    iget-object v2, p0, LX/C4N;->c:LX/1VF;

    iget-object v3, p0, LX/C4N;->d:LX/14w;

    invoke-static {p2, p3, v2, v3}, LX/1Wp;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pr;LX/1VF;LX/14w;)LX/1Wi;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/1We;->a(LX/1Wi;)LX/1Wj;

    move-result-object v0

    .line 1847194
    iget-object v2, p0, LX/C4N;->b:LX/1DR;

    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-static {v0, v2, v3}, Lcom/facebook/feedplugins/base/footer/FooterButtonStylePartDefinition;->a(LX/1Wj;LX/1DR;Landroid/content/res/Resources;)I

    move-result v0

    .line 1847195
    iget-object v2, p0, LX/C4N;->f:LX/39e;

    invoke-virtual {v2, p1}, LX/39e;->c(LX/1De;)LX/C48;

    move-result-object v2

    invoke-virtual {v2, p2}, LX/C48;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/C48;

    move-result-object v2

    check-cast p3, LX/1Po;

    invoke-virtual {v2, p3}, LX/C48;->a(LX/1Po;)LX/C48;

    move-result-object v2

    invoke-virtual {v2, p6}, LX/C48;->a(Z)LX/C48;

    move-result-object v2

    invoke-virtual {v2, v0}, LX/C48;->h(I)LX/C48;

    move-result-object v0

    move-object v0, v0

    .line 1847196
    :goto_2
    invoke-interface {v1, v0}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    return-object v0

    :cond_0
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p6

    move v5, p4

    invoke-static/range {v0 .. v5}, LX/C4N;->a(LX/C4N;LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pr;ZI)LX/1Di;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_2
.end method
