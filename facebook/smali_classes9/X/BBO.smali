.class public final LX/BBO;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1757127
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 1757128
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1757129
    :goto_0
    return v1

    .line 1757130
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1757131
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 1757132
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1757133
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1757134
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 1757135
    const-string v4, "edges"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1757136
    invoke-static {p0, p1}, LX/BBa;->b(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 1757137
    :cond_2
    const-string v4, "page_info"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1757138
    invoke-static {p0, p1}, LX/BBN;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 1757139
    :cond_3
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1757140
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1757141
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1757142
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    move v2, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1757143
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1757144
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1757145
    if-eqz v0, :cond_0

    .line 1757146
    const-string v1, "edges"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1757147
    invoke-static {p0, v0, p2, p3}, LX/BBa;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1757148
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1757149
    if-eqz v0, :cond_1

    .line 1757150
    const-string v1, "page_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1757151
    invoke-static {p0, v0, p2}, LX/BBN;->a(LX/15i;ILX/0nX;)V

    .line 1757152
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1757153
    return-void
.end method
