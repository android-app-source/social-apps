.class public final LX/CVa;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 1904134
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_6

    .line 1904135
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1904136
    :goto_0
    return v1

    .line 1904137
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1904138
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_5

    .line 1904139
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 1904140
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1904141
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_1

    if-eqz v5, :cond_1

    .line 1904142
    const-string v6, "entities"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1904143
    invoke-static {p0, p1}, LX/CVY;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 1904144
    :cond_2
    const-string v6, "font_size"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1904145
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextSize;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextSize;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    goto :goto_1

    .line 1904146
    :cond_3
    const-string v6, "inline_styles"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1904147
    invoke-static {p0, p1}, LX/CVZ;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 1904148
    :cond_4
    const-string v6, "plain_text"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1904149
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 1904150
    :cond_5
    const/4 v5, 0x4

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 1904151
    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 1904152
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 1904153
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1904154
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1904155
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_6
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    goto :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1904156
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1904157
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1904158
    if-eqz v0, :cond_0

    .line 1904159
    const-string v1, "entities"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1904160
    invoke-static {p0, v0, p2, p3}, LX/CVY;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1904161
    :cond_0
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1904162
    if-eqz v0, :cond_1

    .line 1904163
    const-string v0, "font_size"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1904164
    invoke-virtual {p0, p1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1904165
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1904166
    if-eqz v0, :cond_2

    .line 1904167
    const-string v1, "inline_styles"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1904168
    invoke-static {p0, v0, p2, p3}, LX/CVZ;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1904169
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1904170
    if-eqz v0, :cond_3

    .line 1904171
    const-string v1, "plain_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1904172
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1904173
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1904174
    return-void
.end method
