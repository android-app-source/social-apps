.class public LX/Avj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/89u;
.implements LX/Avi;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:LX/AwR;

.field public final d:LX/AsL;

.field public final e:LX/Avf;

.field public final f:LX/Arh;

.field private final g:LX/86h;

.field public final h:LX/Arb;

.field public final i:LX/0kL;

.field public j:Z

.field private k:Ljava/lang/String;

.field public l:Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;

.field public m:Ljava/lang/String;

.field public n:Z

.field private o:Z

.field private p:I

.field private q:Lcom/facebook/photos/creativeediting/model/SwipeableParams;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private r:Lcom/facebook/photos/creativeediting/model/SwipeableParams;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1724537
    const-class v0, LX/Avj;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Avj;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/Arh;Landroid/content/Context;LX/AsL;LX/Avf;LX/AwR;LX/86h;LX/0kL;)V
    .locals 1
    .param p1    # LX/Arh;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/AsL;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/AwR;
        .annotation runtime Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdCache;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1724612
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1724613
    const/4 v0, -0x1

    iput v0, p0, LX/Avj;->p:I

    .line 1724614
    iput-object p2, p0, LX/Avj;->b:Landroid/content/Context;

    .line 1724615
    iput-object p1, p0, LX/Avj;->f:LX/Arh;

    .line 1724616
    iput-object p3, p0, LX/Avj;->d:LX/AsL;

    .line 1724617
    iput-object p4, p0, LX/Avj;->e:LX/Avf;

    .line 1724618
    iput-object p5, p0, LX/Avj;->c:LX/AwR;

    .line 1724619
    iput-object p6, p0, LX/Avj;->g:LX/86h;

    .line 1724620
    iput-object p7, p0, LX/Avj;->i:LX/0kL;

    .line 1724621
    new-instance v0, LX/Avg;

    invoke-direct {v0, p0}, LX/Avg;-><init>(LX/Avj;)V

    iput-object v0, p0, LX/Avj;->h:LX/Arb;

    .line 1724622
    return-void
.end method

.method private j()V
    .locals 3

    .prologue
    .line 1724609
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Avj;->n:Z

    .line 1724610
    iget-object v0, p0, LX/Avj;->e:LX/Avf;

    iget-object v1, p0, LX/Avj;->l:Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;

    new-instance v2, LX/Avh;

    invoke-direct {v2, p0}, LX/Avh;-><init>(LX/Avj;)V

    invoke-virtual {v0, v1, v2}, LX/Avf;->a(Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;LX/89p;)V

    .line 1724611
    return-void
.end method

.method private l()Lcom/facebook/photos/creativeediting/model/SwipeableParams;
    .locals 1

    .prologue
    .line 1724606
    iget-object v0, p0, LX/Avj;->r:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    if-nez v0, :cond_0

    .line 1724607
    iget-object v0, p0, LX/Avj;->k:Ljava/lang/String;

    invoke-static {v0}, LX/8GN;->a(Ljava/lang/String;)Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    move-result-object v0

    iput-object v0, p0, LX/Avj;->r:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    .line 1724608
    :cond_0
    iget-object v0, p0, LX/Avj;->r:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    return-object v0
.end method

.method public static n(LX/Avj;)V
    .locals 5

    .prologue
    .line 1724595
    new-instance v1, LX/BA5;

    invoke-direct {v1}, LX/BA5;-><init>()V

    .line 1724596
    iget-object v0, p0, LX/Avj;->b:Landroid/content/Context;

    .line 1724597
    iput-object v0, v1, LX/BA5;->a:Landroid/content/Context;

    .line 1724598
    iget-object v0, p0, LX/Avj;->c:LX/AwR;

    const-string v2, "face_detect_model.bin"

    invoke-virtual {v0, v2}, LX/AwR;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, LX/Avj;->c:LX/AwR;

    const-string v3, "face_align_model.bin"

    invoke-virtual {v2, v3}, LX/AwR;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/Avj;->c:LX/AwR;

    const-string v4, "pdm_multires.bin"

    invoke-virtual {v3, v4}, LX/AwR;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1, v0, v2, v3, v4}, LX/BA5;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)LX/BA5;

    .line 1724599
    iget-object v0, p0, LX/Avj;->m:Ljava/lang/String;

    .line 1724600
    iput-object v0, v1, LX/BA5;->f:Ljava/lang/String;

    .line 1724601
    iget-boolean v0, p0, LX/Avj;->o:Z

    if-eqz v0, :cond_0

    const-string v0, "0"

    .line 1724602
    :goto_0
    iput-object v0, v1, LX/BA5;->m:Ljava/lang/String;

    .line 1724603
    iget-object v0, p0, LX/Avj;->f:LX/Arh;

    invoke-virtual {v1}, LX/BA5;->a()LX/BA6;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Arh;->a(LX/BA6;)V

    .line 1724604
    return-void

    .line 1724605
    :cond_0
    iget-object v0, p0, LX/Avj;->l:Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;

    invoke-virtual {v0}, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;->getMaskModel()Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$MaskModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$MaskModel;->k()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/86d;
    .locals 1

    .prologue
    .line 1724594
    iget-object v0, p0, LX/Avj;->g:LX/86h;

    return-object v0
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 1724588
    iput p1, p0, LX/Avj;->p:I

    .line 1724589
    iget-object v0, p0, LX/Avj;->d:LX/AsL;

    invoke-virtual {v0}, LX/AsL;->a()V

    .line 1724590
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    .line 1724591
    iget-object v0, p0, LX/Avj;->d:LX/AsL;

    invoke-virtual {v0}, LX/AsL;->b()V

    .line 1724592
    :goto_0
    return-void

    .line 1724593
    :cond_0
    iget-object v0, p0, LX/Avj;->d:LX/AsL;

    iget-object v1, p0, LX/Avj;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/AsL;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/friendsharing/inspiration/model/InspirationModel;)V
    .locals 1

    .prologue
    .line 1724582
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->getId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/Avj;->k:Ljava/lang/String;

    .line 1724583
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->getMask()Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;

    move-result-object v0

    iput-object v0, p0, LX/Avj;->l:Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;

    .line 1724584
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->isLoggingDisabled()Z

    move-result v0

    iput-boolean v0, p0, LX/Avj;->o:Z

    .line 1724585
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Avj;->j:Z

    .line 1724586
    invoke-direct {p0}, LX/Avj;->j()V

    .line 1724587
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 1724623
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1724575
    iget-object v0, p0, LX/Avj;->f:LX/Arh;

    iget-object v1, p0, LX/Avj;->h:LX/Arb;

    invoke-virtual {v0, v1}, LX/Arh;->b(LX/Arb;)V

    .line 1724576
    iget-object v0, p0, LX/Avj;->f:LX/Arh;

    .line 1724577
    iget-object v1, v0, LX/Arh;->j:LX/Arq;

    .line 1724578
    iget-object v0, v1, LX/Arq;->m:LX/BAC;

    invoke-virtual {v0, p0}, LX/BAC;->b(LX/Avi;)V

    .line 1724579
    iget-object v0, p0, LX/Avj;->f:LX/Arh;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/Arh;->a(LX/BA6;)V

    .line 1724580
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Avj;->j:Z

    .line 1724581
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1724571
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Avj;->j:Z

    .line 1724572
    iget-object v0, p0, LX/Avj;->d:LX/AsL;

    invoke-virtual {v0}, LX/AsL;->b()V

    .line 1724573
    invoke-direct {p0}, LX/Avj;->j()V

    .line 1724574
    return-void
.end method

.method public final d()Lcom/facebook/photos/creativeediting/model/SwipeableParams;
    .locals 9

    .prologue
    .line 1724544
    iget-boolean v0, p0, LX/Avj;->n:Z

    if-nez v0, :cond_0

    iget v0, p0, LX/Avj;->p:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, LX/Avj;->p:I

    const/4 v1, -0x2

    if-ne v0, v1, :cond_1

    .line 1724545
    :cond_0
    invoke-direct {p0}, LX/Avj;->l()Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    move-result-object v0

    .line 1724546
    :goto_0
    return-object v0

    .line 1724547
    :cond_1
    iget-boolean v0, p0, LX/Avj;->j:Z

    if-eqz v0, :cond_2

    iget v0, p0, LX/Avj;->p:I

    if-nez v0, :cond_4

    .line 1724548
    :cond_2
    iget-object v0, p0, LX/Avj;->q:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    if-nez v0, :cond_3

    .line 1724549
    iget-object v0, p0, LX/Avj;->k:Ljava/lang/String;

    const/4 v7, 0x0

    .line 1724550
    new-instance v2, Landroid/net/Uri$Builder;

    invoke-direct {v2}, Landroid/net/Uri$Builder;-><init>()V

    const-string v3, "res"

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const v3, 0x7f020da5

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    .line 1724551
    new-instance v3, LX/5jG;

    const-string v4, ""

    invoke-direct {v3, v2, v4}, LX/5jG;-><init>(Landroid/net/Uri;Ljava/lang/String;)V

    const v2, 0x3e1fbe77    # 0.156f

    .line 1724552
    iput v2, v3, LX/5jG;->h:F

    .line 1724553
    move-object v2, v3

    .line 1724554
    const v3, 0x3e4ccccd    # 0.2f

    .line 1724555
    iput v3, v2, LX/5jG;->g:F

    .line 1724556
    move-object v2, v2

    .line 1724557
    const/4 v3, 0x1

    .line 1724558
    iput-boolean v3, v2, LX/5jG;->k:Z

    .line 1724559
    move-object v2, v2

    .line 1724560
    const v3, 0x3ecccccd    # 0.4f

    .line 1724561
    iput v3, v2, LX/5jG;->e:F

    .line 1724562
    move-object v2, v2

    .line 1724563
    const v3, 0x3ec6a7f0    # 0.388f

    .line 1724564
    iput v3, v2, LX/5jG;->f:F

    .line 1724565
    move-object v2, v2

    .line 1724566
    invoke-virtual {v2}, LX/5jG;->a()Lcom/facebook/photos/creativeediting/model/StickerParams;

    move-result-object v3

    .line 1724567
    new-instance v2, Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    invoke-static {v3}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    sget-object v5, LX/5jI;->FRAME:LX/5jI;

    const-string v6, ""

    move-object v4, v0

    move-object v8, v7

    invoke-direct/range {v2 .. v8}, Lcom/facebook/photos/creativeediting/model/SwipeableParams;-><init>(Ljava/util/List;Ljava/lang/String;LX/5jI;Ljava/lang/String;Lcom/facebook/videocodec/effects/model/ColorFilter;Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;)V

    move-object v0, v2

    .line 1724568
    iput-object v0, p0, LX/Avj;->q:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    .line 1724569
    :cond_3
    iget-object v0, p0, LX/Avj;->q:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    goto :goto_0

    .line 1724570
    :cond_4
    invoke-direct {p0}, LX/Avj;->l()Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    move-result-object v0

    goto :goto_0
.end method

.method public final e()Landroid/net/Uri;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1724543
    const/4 v0, 0x0

    return-object v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1724540
    iget-boolean v0, p0, LX/Avj;->j:Z

    if-eqz v0, :cond_0

    iget v0, p0, LX/Avj;->p:I

    if-lez v0, :cond_0

    .line 1724541
    iget-object v0, p0, LX/Avj;->l:Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;

    invoke-virtual {v0}, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;->getInstructionText()Ljava/lang/String;

    move-result-object v0

    .line 1724542
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1724539
    iget-boolean v0, p0, LX/Avj;->j:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Avj;->l:Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;

    invoke-virtual {v0}, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;->getAttributionText()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()Landroid/net/Uri;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1724538
    iget-boolean v0, p0, LX/Avj;->j:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Avj;->l:Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;

    invoke-virtual {v0}, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;->getAttributionThumbnail()Landroid/net/Uri;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
