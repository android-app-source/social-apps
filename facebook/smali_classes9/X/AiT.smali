.class public final LX/AiT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "LX/6Vr;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/feed/awesomizer/ui/AwesomizerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/awesomizer/ui/AwesomizerFragment;)V
    .locals 0

    .prologue
    .line 1706240
    iput-object p1, p0, LX/AiT;->a:Lcom/facebook/feed/awesomizer/ui/AwesomizerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 1706241
    check-cast p1, LX/6Vr;

    .line 1706242
    iget-object v0, p0, LX/AiT;->a:Lcom/facebook/feed/awesomizer/ui/AwesomizerFragment;

    .line 1706243
    iget-object v1, v0, Lcom/facebook/feed/awesomizer/ui/AwesomizerFragment;->b:LX/Aiv;

    iget-wide v3, p1, LX/6Vr;->c:J

    .line 1706244
    iget-wide v5, v1, LX/Aiv;->k:J

    add-long/2addr v5, v3

    iput-wide v5, v1, LX/Aiv;->k:J

    .line 1706245
    iget-boolean v1, p1, LX/6Vr;->b:Z

    if-eqz v1, :cond_0

    .line 1706246
    iget-object v1, v0, Lcom/facebook/feed/awesomizer/ui/AwesomizerFragment;->i:Ljava/util/Set;

    iget v2, p1, LX/6Vr;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1706247
    iget v1, p1, LX/6Vr;->a:I

    invoke-virtual {v0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/awesomizer/ui/AwesomizerLandscreenCardView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/feed/awesomizer/ui/AwesomizerLandscreenCardView;->setCardCheckVisibility(I)V

    .line 1706248
    :cond_0
    iget-boolean v1, p1, LX/6Vr;->b:Z

    if-nez v1, :cond_1

    iget-object v1, v0, Lcom/facebook/feed/awesomizer/ui/AwesomizerFragment;->i:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_2

    .line 1706249
    :cond_1
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020a4d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/feed/awesomizer/ui/AwesomizerFragment;->a(Lcom/facebook/feed/awesomizer/ui/AwesomizerFragment;Landroid/graphics/drawable/Drawable;)V

    .line 1706250
    :goto_0
    const/4 v0, 0x0

    return-object v0

    .line 1706251
    :cond_2
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020a4c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/feed/awesomizer/ui/AwesomizerFragment;->a(Lcom/facebook/feed/awesomizer/ui/AwesomizerFragment;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method
