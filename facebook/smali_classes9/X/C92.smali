.class public LX/C92;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0pQ;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public static final b:LX/0Tn;

.field private static volatile h:LX/C92;


# instance fields
.field private final c:LX/0ad;

.field private final d:LX/0SG;

.field public final e:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final f:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public g:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1853592
    const-class v0, LX/C92;

    sput-object v0, LX/C92;->a:Ljava/lang/Class;

    .line 1853593
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "moments_call_to_action_prefs/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    const-string v1, "impressions"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/C92;->b:LX/0Tn;

    return-void
.end method

.method public constructor <init>(LX/0ad;LX/0SG;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1853586
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1853587
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/C92;->f:Ljava/util/Set;

    .line 1853588
    iput-object p1, p0, LX/C92;->c:LX/0ad;

    .line 1853589
    iput-object p2, p0, LX/C92;->d:LX/0SG;

    .line 1853590
    iput-object p3, p0, LX/C92;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1853591
    return-void
.end method

.method public static a(LX/0QB;)LX/C92;
    .locals 6

    .prologue
    .line 1853573
    sget-object v0, LX/C92;->h:LX/C92;

    if-nez v0, :cond_1

    .line 1853574
    const-class v1, LX/C92;

    monitor-enter v1

    .line 1853575
    :try_start_0
    sget-object v0, LX/C92;->h:LX/C92;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1853576
    if-eqz v2, :cond_0

    .line 1853577
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1853578
    new-instance p0, LX/C92;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v5

    check-cast v5, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {p0, v3, v4, v5}, LX/C92;-><init>(LX/0ad;LX/0SG;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 1853579
    move-object v0, p0

    .line 1853580
    sput-object v0, LX/C92;->h:LX/C92;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1853581
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1853582
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1853583
    :cond_1
    sget-object v0, LX/C92;->h:LX/C92;

    return-object v0

    .line 1853584
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1853585
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static b(LX/C92;)I
    .locals 3

    .prologue
    .line 1853572
    iget-object v0, p0, LX/C92;->c:LX/0ad;

    sget v1, LX/C91;->b:I

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v0

    return v0
.end method

.method public static c(LX/C92;)J
    .locals 4

    .prologue
    .line 1853571
    iget-object v0, p0, LX/C92;->c:LX/0ad;

    sget v1, LX/C91;->a:I

    const/16 v2, 0x18

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v0

    int-to-long v0, v0

    const-wide/16 v2, 0xe10

    mul-long/2addr v0, v2

    return-wide v0
.end method

.method public static c(LX/C92;Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 2

    .prologue
    .line 1853524
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1853525
    const/4 v0, 0x0

    .line 1853526
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/C92;->f:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public static d(LX/C92;)J
    .locals 4

    .prologue
    .line 1853570
    iget-object v0, p0, LX/C92;->d:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    return-wide v0
.end method

.method public static e(LX/C92;)J
    .locals 9

    .prologue
    .line 1853562
    invoke-static {p0}, LX/C92;->c(LX/C92;)J

    move-result-wide v4

    .line 1853563
    const-wide/16 v0, 0x0

    .line 1853564
    invoke-static {p0}, LX/C92;->d(LX/C92;)J

    move-result-wide v6

    .line 1853565
    iget-object v2, p0, LX/C92;->g:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move-wide v2, v0

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 1853566
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    sub-long v0, v6, v0

    cmp-long v0, v0, v4

    if-gtz v0, :cond_1

    .line 1853567
    const-wide/16 v0, 0x1

    add-long/2addr v0, v2

    :goto_1
    move-wide v2, v0

    .line 1853568
    goto :goto_0

    .line 1853569
    :cond_0
    return-wide v2

    :cond_1
    move-wide v0, v2

    goto :goto_1
.end method

.method public static g(LX/C92;)V
    .locals 11

    .prologue
    .line 1853551
    iget-object v0, p0, LX/C92;->g:Ljava/util/LinkedList;

    if-nez v0, :cond_0

    .line 1853552
    const/4 v2, 0x0

    .line 1853553
    iget-object v1, p0, LX/C92;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/C92;->b:LX/0Tn;

    const-string v4, ""

    invoke-interface {v1, v3, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1853554
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    iput-object v1, p0, LX/C92;->g:Ljava/util/LinkedList;

    .line 1853555
    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1853556
    :cond_0
    :goto_0
    return-void

    .line 1853557
    :cond_1
    :try_start_0
    const-string v1, ","

    invoke-virtual {v3, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    array-length v5, v4

    move v1, v2

    :goto_1
    if-ge v1, v5, :cond_0

    aget-object v6, v4, v1

    .line 1853558
    iget-object v7, p0, LX/C92;->g:Ljava/util/LinkedList;

    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v7, v6}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1853559
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1853560
    :catch_0
    move-exception v1

    .line 1853561
    sget-object v4, LX/C92;->a:Ljava/lang/Class;

    const-string v5, "Failed to read Moments CTA impressions from: \'%s\'"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v3, v6, v2

    invoke-static {v4, v1, v5, v6}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "LX/0Tn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1853550
    sget-object v0, LX/C92;->b:LX/0Tn;

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 14

    .prologue
    .line 1853527
    invoke-static {p0, p1}, LX/C92;->c(LX/C92;Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1853528
    :goto_0
    return-void

    .line 1853529
    :cond_0
    invoke-static {p0}, LX/C92;->g(LX/C92;)V

    .line 1853530
    invoke-static {p0}, LX/C92;->b(LX/C92;)I

    move-result v5

    .line 1853531
    invoke-static {p0}, LX/C92;->c(LX/C92;)J

    move-result-wide v6

    .line 1853532
    invoke-static {p0}, LX/C92;->d(LX/C92;)J

    move-result-wide v8

    .line 1853533
    iget-object v4, p0, LX/C92;->g:Ljava/util/LinkedList;

    invoke-virtual {v4}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_1
    :goto_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1853534
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    .line 1853535
    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    sub-long v12, v8, v12

    cmp-long v4, v12, v6

    if-lez v4, :cond_1

    .line 1853536
    invoke-interface {v10}, Ljava/util/Iterator;->remove()V

    goto :goto_1

    .line 1853537
    :cond_2
    iget-object v4, p0, LX/C92;->g:Ljava/util/LinkedList;

    invoke-virtual {v4}, Ljava/util/LinkedList;->size()I

    move-result v4

    if-le v4, v5, :cond_3

    .line 1853538
    iget-object v4, p0, LX/C92;->g:Ljava/util/LinkedList;

    iget-object v6, p0, LX/C92;->g:Ljava/util/LinkedList;

    invoke-virtual {v6}, Ljava/util/LinkedList;->size()I

    move-result v6

    invoke-virtual {v4, v5, v6}, Ljava/util/LinkedList;->subList(II)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->clear()V

    .line 1853539
    :cond_3
    iget-object v0, p0, LX/C92;->g:Ljava/util/LinkedList;

    invoke-static {p0}, LX/C92;->d(LX/C92;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 1853540
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1853541
    iget-object v0, p0, LX/C92;->g:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 1853542
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    if-lez v3, :cond_4

    .line 1853543
    const/16 v3, 0x2c

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1853544
    :cond_4
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 1853545
    :cond_5
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1853546
    iget-object v1, p0, LX/C92;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/C92;->b:LX/0Tn;

    invoke-interface {v1, v2, v0}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 1853547
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_6

    .line 1853548
    :goto_3
    goto/16 :goto_0

    .line 1853549
    :cond_6
    iget-object v0, p0, LX/C92;->f:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_3
.end method
