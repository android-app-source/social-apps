.class public LX/Adk;
.super LX/AVi;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/AVi",
        "<",
        "Lcom/facebook/facecastdisplay/feedback/LiveEventCommentComposer;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/AYb;

.field private final b:LX/AVU;

.field private final c:LX/3iG;

.field private final d:Landroid/view/View$OnClickListener;

.field private e:Lcom/facebook/graphql/model/GraphQLFeedback;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:LX/Adj;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/facecastdisplay/LiveEventCommentDialogFragment;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Z
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:F
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/AVU;LX/3iG;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1695531
    invoke-direct {p0}, LX/AVi;-><init>()V

    .line 1695532
    new-instance v0, LX/Adh;

    invoke-direct {v0, p0}, LX/Adh;-><init>(LX/Adk;)V

    iput-object v0, p0, LX/Adk;->a:LX/AYb;

    .line 1695533
    iput-object p1, p0, LX/Adk;->b:LX/AVU;

    .line 1695534
    iput-object p2, p0, LX/Adk;->c:LX/3iG;

    .line 1695535
    new-instance v0, LX/Adi;

    invoke-direct {v0, p0}, LX/Adi;-><init>(LX/Adk;)V

    iput-object v0, p0, LX/Adk;->d:Landroid/view/View$OnClickListener;

    .line 1695536
    return-void
.end method

.method public static a(LX/0QB;)LX/Adk;
    .locals 1

    .prologue
    .line 1695530
    invoke-static {p0}, LX/Adk;->b(LX/0QB;)LX/Adk;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/facebook/facecastdisplay/feedback/LiveEventCommentComposer;Lcom/facebook/facecastdisplay/feedback/LiveEventCommentComposer;)V
    .locals 0

    .prologue
    .line 1695527
    invoke-static {p2}, LX/Adk;->c(Lcom/facebook/facecastdisplay/feedback/LiveEventCommentComposer;)V

    .line 1695528
    invoke-direct {p0, p1}, LX/Adk;->b(Lcom/facebook/facecastdisplay/feedback/LiveEventCommentComposer;)V

    .line 1695529
    return-void
.end method

.method public static a$redex0(LX/Adk;Landroid/text/Editable;)V
    .locals 3

    .prologue
    .line 1695522
    const/4 v0, 0x0

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v1

    const-class v2, LX/7Gl;

    invoke-interface {p1, v0, v1, v2}, Landroid/text/Editable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7Gl;

    .line 1695523
    if-eqz v0, :cond_0

    array-length v1, v0

    if-lez v1, :cond_0

    .line 1695524
    iget-object v1, p0, LX/Adk;->b:LX/AVU;

    array-length v0, v0

    .line 1695525
    const-string v2, "facecast_comment_mentions"

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p0

    invoke-static {v1, v2, p0}, LX/AVU;->a(LX/AVU;Ljava/lang/String;Ljava/lang/String;)V

    .line 1695526
    :cond_0
    return-void
.end method

.method public static a$redex0(LX/Adk;Ljava/lang/String;)V
    .locals 10

    .prologue
    const/4 v6, 0x0

    const/4 v4, 0x0

    .line 1695518
    iget-object v0, p0, LX/Adk;->e:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Adk;->f:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    if-nez v0, :cond_1

    .line 1695519
    :cond_0
    :goto_0
    return-void

    .line 1695520
    :cond_1
    new-instance v0, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;

    iget-object v1, p0, LX/Adk;->e:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/Adk;->e:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v2

    iget-boolean v8, p0, LX/Adk;->i:Z

    iget v3, p0, LX/Adk;->j:F

    float-to-int v9, v3

    move-object v3, p1

    move v5, v4

    move-object v7, v6

    invoke-direct/range {v0 .. v9}, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLcom/facebook/ipc/media/MediaItem;Lcom/facebook/ipc/media/StickerItem;ZI)V

    .line 1695521
    iget-object v1, p0, LX/Adk;->c:LX/3iG;

    sget-object v2, Lcom/facebook/feedback/ui/FeedbackControllerParams;->a:Lcom/facebook/feedback/ui/FeedbackControllerParams;

    invoke-virtual {v1, v2}, LX/3iG;->a(Lcom/facebook/feedback/ui/FeedbackControllerParams;)LX/3iK;

    move-result-object v1

    iget-object v2, p0, LX/Adk;->e:Lcom/facebook/graphql/model/GraphQLFeedback;

    iget-object v3, p0, LX/Adk;->f:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    invoke-virtual {v1, v0, v2, v3}, LX/3iK;->a(Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)V

    goto :goto_0
.end method

.method private static b(LX/0QB;)LX/Adk;
    .locals 3

    .prologue
    .line 1695516
    new-instance v2, LX/Adk;

    invoke-static {p0}, LX/AVU;->a(LX/0QB;)LX/AVU;

    move-result-object v0

    check-cast v0, LX/AVU;

    const-class v1, LX/3iG;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/3iG;

    invoke-direct {v2, v0, v1}, LX/Adk;-><init>(LX/AVU;LX/3iG;)V

    .line 1695517
    return-object v2
.end method

.method private b(Lcom/facebook/facecastdisplay/feedback/LiveEventCommentComposer;)V
    .locals 2

    .prologue
    .line 1695514
    iget-object v0, p1, Lcom/facebook/facecastdisplay/feedback/LiveEventCommentComposer;->a:Lcom/facebook/widget/text/BetterEditTextView;

    iget-object v1, p0, LX/Adk;->d:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterEditTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1695515
    return-void
.end method

.method private static c(Lcom/facebook/facecastdisplay/feedback/LiveEventCommentComposer;)V
    .locals 2

    .prologue
    .line 1695485
    iget-object v0, p0, Lcom/facebook/facecastdisplay/feedback/LiveEventCommentComposer;->a:Lcom/facebook/widget/text/BetterEditTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterEditTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1695486
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Landroid/view/View;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 1695513
    check-cast p1, Lcom/facebook/facecastdisplay/feedback/LiveEventCommentComposer;

    check-cast p2, Lcom/facebook/facecastdisplay/feedback/LiveEventCommentComposer;

    invoke-direct {p0, p1, p2}, LX/Adk;->a(Lcom/facebook/facecastdisplay/feedback/LiveEventCommentComposer;Lcom/facebook/facecastdisplay/feedback/LiveEventCommentComposer;)V

    return-void
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1695509
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1695510
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    iput-object v0, p0, LX/Adk;->e:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1695511
    new-instance v0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    invoke-static {p1}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v1

    const-string v2, "newsfeed_ufi"

    const-string v3, "video_fullscreen_player"

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;-><init>(LX/162;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, LX/Adk;->f:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 1695512
    return-void
.end method

.method public final synthetic b(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 1695508
    check-cast p1, Lcom/facebook/facecastdisplay/feedback/LiveEventCommentComposer;

    invoke-direct {p0, p1}, LX/Adk;->b(Lcom/facebook/facecastdisplay/feedback/LiveEventCommentComposer;)V

    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1695505
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1695506
    check-cast v0, Lcom/facebook/facecastdisplay/feedback/LiveEventCommentComposer;

    invoke-static {v0}, LX/Adk;->c(Lcom/facebook/facecastdisplay/feedback/LiveEventCommentComposer;)V

    .line 1695507
    return-void
.end method

.method public final d()V
    .locals 4

    .prologue
    .line 1695490
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1695491
    check-cast v0, Lcom/facebook/facecastdisplay/feedback/LiveEventCommentComposer;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/feedback/LiveEventCommentComposer;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, LX/0ew;

    invoke-static {v0, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ew;

    .line 1695492
    if-eqz v0, :cond_0

    invoke-interface {v0}, LX/0ew;->iC_()LX/0gc;

    move-result-object v1

    const-string v2, "live_event_comment_dialog"

    invoke-virtual {v1, v2}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1695493
    :cond_0
    :goto_0
    return-void

    .line 1695494
    :cond_1
    iget-object v1, p0, LX/Adk;->h:Lcom/facebook/facecastdisplay/LiveEventCommentDialogFragment;

    if-nez v1, :cond_2

    .line 1695495
    new-instance v1, Lcom/facebook/facecastdisplay/LiveEventCommentDialogFragment;

    invoke-direct {v1}, Lcom/facebook/facecastdisplay/LiveEventCommentDialogFragment;-><init>()V

    iput-object v1, p0, LX/Adk;->h:Lcom/facebook/facecastdisplay/LiveEventCommentDialogFragment;

    .line 1695496
    iget-object v1, p0, LX/Adk;->h:Lcom/facebook/facecastdisplay/LiveEventCommentDialogFragment;

    iget-object v2, p0, LX/Adk;->a:LX/AYb;

    .line 1695497
    iput-object v2, v1, Lcom/facebook/facecastdisplay/LiveEventCommentDialogFragment;->u:LX/AYb;

    .line 1695498
    :cond_2
    iget-object v1, p0, LX/Adk;->h:Lcom/facebook/facecastdisplay/LiveEventCommentDialogFragment;

    iget-object v2, p0, LX/Adk;->e:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1695499
    iput-object v2, v1, Lcom/facebook/facecastdisplay/LiveEventCommentDialogFragment;->t:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1695500
    iget-object v3, v1, Lcom/facebook/facecastdisplay/LiveEventCommentDialogFragment;->m:LX/8nK;

    if-eqz v3, :cond_3

    .line 1695501
    iget-object v3, v1, Lcom/facebook/facecastdisplay/LiveEventCommentDialogFragment;->m:LX/8nK;

    invoke-virtual {v3, v2}, LX/8nK;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)V

    .line 1695502
    :cond_3
    iget-object v1, p0, LX/Adk;->h:Lcom/facebook/facecastdisplay/LiveEventCommentDialogFragment;

    invoke-interface {v0}, LX/0ew;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const-string v2, "live_event_comment_dialog"

    const/4 v3, 0x1

    invoke-virtual {v1, v0, v2, v3}, Landroid/support/v4/app/DialogFragment;->a(LX/0hH;Ljava/lang/String;Z)I

    .line 1695503
    iget-object v0, p0, LX/Adk;->g:LX/Adj;

    if-eqz v0, :cond_0

    .line 1695504
    iget-object v0, p0, LX/Adk;->g:LX/Adj;

    invoke-interface {v0}, LX/Adj;->a()V

    goto :goto_0
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 1695487
    iget-object v0, p0, LX/Adk;->h:Lcom/facebook/facecastdisplay/LiveEventCommentDialogFragment;

    if-eqz v0, :cond_0

    .line 1695488
    iget-object v0, p0, LX/Adk;->h:Lcom/facebook/facecastdisplay/LiveEventCommentDialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->d()V

    .line 1695489
    :cond_0
    return-void
.end method
