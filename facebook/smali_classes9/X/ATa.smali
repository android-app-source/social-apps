.class public final LX/ATa;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8GH;


# instance fields
.field public final synthetic a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

.field private final b:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;)V
    .locals 1

    .prologue
    .line 1676232
    iput-object p1, p0, LX/ATa;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1676233
    new-instance v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView$4$1;

    invoke-direct {v0, p0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView$4$1;-><init>(LX/ATa;)V

    iput-object v0, p0, LX/ATa;->b:Ljava/lang/Runnable;

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/photos/creativeediting/model/SwipeableParams;)V
    .locals 0
    .param p1    # Lcom/facebook/photos/creativeediting/model/SwipeableParams;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1676231
    return-void
.end method

.method public final a(Lcom/facebook/photos/creativeediting/model/SwipeableParams;ZI)V
    .locals 0
    .param p1    # Lcom/facebook/photos/creativeediting/model/SwipeableParams;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1676234
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 1676230
    return-void
.end method

.method public final b(Lcom/facebook/photos/creativeediting/model/SwipeableParams;)V
    .locals 4
    .param p1    # Lcom/facebook/photos/creativeediting/model/SwipeableParams;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1676204
    iget-object v0, p0, LX/ATa;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v0, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    if-nez v0, :cond_0

    .line 1676205
    :goto_0
    return-void

    .line 1676206
    :cond_0
    iget-object v0, p0, LX/ATa;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v1, p0, LX/ATa;->b:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 1676207
    iget-object v0, p0, LX/ATa;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v1, p0, LX/ATa;->b:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1676208
    if-eqz p1, :cond_1

    .line 1676209
    iget-object v0, p0, LX/ATa;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v0, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aN:LX/9c7;

    .line 1676210
    iget-object v1, p1, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->c:LX/5jI;

    move-object v1, v1

    .line 1676211
    sget-object v2, LX/5jI;->FRAME:LX/5jI;

    if-ne v1, v2, :cond_2

    const/4 v1, 0x1

    .line 1676212
    :goto_1
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    if-eqz v1, :cond_3

    sget-object v1, LX/9c4;->COMPOSER_FRAME_VIEWED:LX/9c4;

    invoke-virtual {v1}, LX/9c4;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_2
    invoke-direct {v2, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "composer"

    .line 1676213
    iput-object v1, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1676214
    move-object v1, v2

    .line 1676215
    sget-object v2, LX/9c5;->FILTER_NAME:LX/9c5;

    invoke-virtual {v2}, LX/9c5;->getParamKey()Ljava/lang/String;

    move-result-object v2

    .line 1676216
    iget-object v3, p1, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->b:Ljava/lang/String;

    move-object v3, v3

    .line 1676217
    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 1676218
    invoke-static {v0, v1}, LX/9c7;->a(LX/9c7;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1676219
    iget-object v0, p0, LX/ATa;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-boolean v0, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aM:Z

    if-nez v0, :cond_1

    .line 1676220
    iget-object v0, p1, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->c:LX/5jI;

    move-object v0, v0

    .line 1676221
    sget-object v1, LX/5jI;->FRAME:LX/5jI;

    if-ne v0, v1, :cond_1

    .line 1676222
    iget-object v0, p0, LX/ATa;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v0, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->C:LX/8G7;

    .line 1676223
    sget-object v1, LX/0SF;->a:LX/0SF;

    move-object v1, v1

    .line 1676224
    invoke-virtual {v1}, LX/0SF;->a()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, LX/8G7;->b(J)V

    .line 1676225
    :cond_1
    iget-object v0, p0, LX/ATa;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    const/4 v1, 0x1

    .line 1676226
    iput-boolean v1, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aM:Z

    .line 1676227
    goto :goto_0

    .line 1676228
    :cond_2
    const/4 v1, 0x0

    goto :goto_1

    .line 1676229
    :cond_3
    sget-object v1, LX/9c4;->COMPOSER_FILTER_VIEWED:LX/9c4;

    invoke-virtual {v1}, LX/9c4;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_2
.end method

.method public final c(Lcom/facebook/photos/creativeediting/model/SwipeableParams;)V
    .locals 0
    .param p1    # Lcom/facebook/photos/creativeediting/model/SwipeableParams;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1676203
    return-void
.end method
