.class public final LX/BzR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Qd;


# instance fields
.field public final synthetic a:LX/BzZ;

.field public final synthetic b:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

.field public final synthetic c:LX/1Po;

.field public final synthetic d:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic e:Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoWithDirectInstallOverlayPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoWithDirectInstallOverlayPartDefinition;LX/BzZ;Lcom/facebook/graphql/model/GraphQLStoryActionLink;LX/1Po;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 0

    .prologue
    .line 1838710
    iput-object p1, p0, LX/BzR;->e:Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoWithDirectInstallOverlayPartDefinition;

    iput-object p2, p0, LX/BzR;->a:LX/BzZ;

    iput-object p3, p0, LX/BzR;->b:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    iput-object p4, p0, LX/BzR;->c:LX/1Po;

    iput-object p5, p0, LX/BzR;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;)V
    .locals 4

    .prologue
    .line 1838711
    if-eqz p1, :cond_1

    iget-object v0, p0, LX/BzR;->a:LX/BzZ;

    .line 1838712
    iget-object v1, v0, LX/BzZ;->b:Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    move-object v0, v1

    .line 1838713
    if-eq p1, v0, :cond_1

    .line 1838714
    iget-object v0, p0, LX/BzR;->a:LX/BzZ;

    .line 1838715
    iput-object p1, v0, LX/BzZ;->b:Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    .line 1838716
    iget-object v0, p0, LX/BzR;->e:Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoWithDirectInstallOverlayPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoWithDirectInstallOverlayPartDefinition;->d:LX/2v5;

    invoke-virtual {v0, p1}, LX/2v5;->a(Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;)Ljava/lang/String;

    move-result-object v0

    .line 1838717
    if-nez v0, :cond_0

    .line 1838718
    iget-object v0, p0, LX/BzR;->b:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aZ()Ljava/lang/String;

    move-result-object v0

    .line 1838719
    :cond_0
    iget-object v1, p0, LX/BzR;->a:LX/BzZ;

    .line 1838720
    iput-object v0, v1, LX/BzZ;->f:Ljava/lang/String;

    .line 1838721
    iget-object v0, p0, LX/BzR;->c:LX/1Po;

    check-cast v0, LX/1Pr;

    new-instance v2, LX/BzY;

    iget-object v1, p0, LX/BzR;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1838722
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v3

    .line 1838723
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-direct {v2, v1}, LX/BzY;-><init>(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    iget-object v1, p0, LX/BzR;->a:LX/BzZ;

    invoke-interface {v0, v2, v1}, LX/1Pr;->a(LX/1KL;Ljava/lang/Object;)Z

    .line 1838724
    iget-object v0, p0, LX/BzR;->c:LX/1Po;

    check-cast v0, LX/1Pq;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v2, 0x0

    iget-object v3, p0, LX/BzR;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    aput-object v3, v1, v2

    invoke-interface {v0, v1}, LX/1Pq;->a([Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1838725
    :cond_1
    return-void
.end method
