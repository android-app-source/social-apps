.class public final LX/CYq;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 38

    .prologue
    .line 1912951
    const/16 v29, 0x0

    .line 1912952
    const/16 v28, 0x0

    .line 1912953
    const/16 v27, 0x0

    .line 1912954
    const/16 v26, 0x0

    .line 1912955
    const-wide/16 v24, 0x0

    .line 1912956
    const/16 v23, 0x0

    .line 1912957
    const/16 v22, 0x0

    .line 1912958
    const/16 v21, 0x0

    .line 1912959
    const/16 v20, 0x0

    .line 1912960
    const/16 v19, 0x0

    .line 1912961
    const/16 v18, 0x0

    .line 1912962
    const/16 v17, 0x0

    .line 1912963
    const/16 v16, 0x0

    .line 1912964
    const/4 v15, 0x0

    .line 1912965
    const/4 v14, 0x0

    .line 1912966
    const-wide/16 v12, 0x0

    .line 1912967
    const/4 v11, 0x0

    .line 1912968
    const/4 v10, 0x0

    .line 1912969
    const/4 v9, 0x0

    .line 1912970
    const/4 v8, 0x0

    .line 1912971
    const/4 v7, 0x0

    .line 1912972
    const/4 v6, 0x0

    .line 1912973
    const/4 v5, 0x0

    .line 1912974
    const/4 v4, 0x0

    .line 1912975
    const/4 v3, 0x0

    .line 1912976
    const/4 v2, 0x0

    .line 1912977
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v30

    sget-object v31, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v30

    move-object/from16 v1, v31

    if-eq v0, v1, :cond_1c

    .line 1912978
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1912979
    const/4 v2, 0x0

    .line 1912980
    :goto_0
    return v2

    .line 1912981
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v31, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v31

    if-eq v2, v0, :cond_16

    .line 1912982
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 1912983
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1912984
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v31

    sget-object v32, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v31

    move-object/from16 v1, v32

    if-eq v0, v1, :cond_0

    if-eqz v2, :cond_0

    .line 1912985
    const-string v31, "can_viewer_change_guest_status"

    move-object/from16 v0, v31

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_1

    .line 1912986
    const/4 v2, 0x1

    .line 1912987
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v7

    move/from16 v30, v7

    move v7, v2

    goto :goto_1

    .line 1912988
    :cond_1
    const-string v31, "can_viewer_join"

    move-object/from16 v0, v31

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_2

    .line 1912989
    const/4 v2, 0x1

    .line 1912990
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move/from16 v29, v6

    move v6, v2

    goto :goto_1

    .line 1912991
    :cond_2
    const-string v31, "connection_style"

    move-object/from16 v0, v31

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_3

    .line 1912992
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLConnectionStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    move/from16 v28, v2

    goto :goto_1

    .line 1912993
    :cond_3
    const-string v31, "cover_photo"

    move-object/from16 v0, v31

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_4

    .line 1912994
    invoke-static/range {p0 .. p1}, LX/CYp;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v27, v2

    goto :goto_1

    .line 1912995
    :cond_4
    const-string v31, "end_timestamp"

    move-object/from16 v0, v31

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_5

    .line 1912996
    const/4 v2, 0x1

    .line 1912997
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto/16 :goto_1

    .line 1912998
    :cond_5
    const-string v31, "event_declines"

    move-object/from16 v0, v31

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_6

    .line 1912999
    invoke-static/range {p0 .. p1}, LX/7s4;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v26, v2

    goto/16 :goto_1

    .line 1913000
    :cond_6
    const-string v31, "event_maybes"

    move-object/from16 v0, v31

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_7

    .line 1913001
    invoke-static/range {p0 .. p1}, LX/7s5;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v25, v2

    goto/16 :goto_1

    .line 1913002
    :cond_7
    const-string v31, "event_members"

    move-object/from16 v0, v31

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_8

    .line 1913003
    invoke-static/range {p0 .. p1}, LX/7s6;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v24, v2

    goto/16 :goto_1

    .line 1913004
    :cond_8
    const-string v31, "event_place"

    move-object/from16 v0, v31

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_9

    .line 1913005
    invoke-static/range {p0 .. p1}, LX/7rw;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v23, v2

    goto/16 :goto_1

    .line 1913006
    :cond_9
    const-string v31, "friendEventMaybesFirst5"

    move-object/from16 v0, v31

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_a

    .line 1913007
    invoke-static/range {p0 .. p1}, LX/7s8;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v22, v2

    goto/16 :goto_1

    .line 1913008
    :cond_a
    const-string v31, "friendEventMembersFirst5"

    move-object/from16 v0, v31

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_b

    .line 1913009
    invoke-static/range {p0 .. p1}, LX/7sA;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v21, v2

    goto/16 :goto_1

    .line 1913010
    :cond_b
    const-string v31, "friendEventWatchersFirst5"

    move-object/from16 v0, v31

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_c

    .line 1913011
    invoke-static/range {p0 .. p1}, LX/7sC;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v20, v2

    goto/16 :goto_1

    .line 1913012
    :cond_c
    const-string v31, "id"

    move-object/from16 v0, v31

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_d

    .line 1913013
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v19, v2

    goto/16 :goto_1

    .line 1913014
    :cond_d
    const-string v31, "is_all_day"

    move-object/from16 v0, v31

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_e

    .line 1913015
    const/4 v2, 0x1

    .line 1913016
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v9

    move/from16 v18, v9

    move v9, v2

    goto/16 :goto_1

    .line 1913017
    :cond_e
    const-string v31, "name"

    move-object/from16 v0, v31

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_f

    .line 1913018
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v15, v2

    goto/16 :goto_1

    .line 1913019
    :cond_f
    const-string v31, "start_timestamp"

    move-object/from16 v0, v31

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_10

    .line 1913020
    const/4 v2, 0x1

    .line 1913021
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v16

    move v8, v2

    goto/16 :goto_1

    .line 1913022
    :cond_10
    const-string v31, "suggested_event_context_sentence"

    move-object/from16 v0, v31

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_11

    .line 1913023
    invoke-static/range {p0 .. p1}, LX/7sD;->a(LX/15w;LX/186;)I

    move-result v2

    move v14, v2

    goto/16 :goto_1

    .line 1913024
    :cond_11
    const-string v31, "timezone"

    move-object/from16 v0, v31

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_12

    .line 1913025
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v13, v2

    goto/16 :goto_1

    .line 1913026
    :cond_12
    const-string v31, "viewer_guest_status"

    move-object/from16 v0, v31

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_13

    .line 1913027
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    move v12, v2

    goto/16 :goto_1

    .line 1913028
    :cond_13
    const-string v31, "viewer_inviters"

    move-object/from16 v0, v31

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v31

    if-eqz v31, :cond_14

    .line 1913029
    invoke-static/range {p0 .. p1}, LX/7u6;->b(LX/15w;LX/186;)I

    move-result v2

    move v11, v2

    goto/16 :goto_1

    .line 1913030
    :cond_14
    const-string v31, "viewer_watch_status"

    move-object/from16 v0, v31

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_15

    .line 1913031
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    move v10, v2

    goto/16 :goto_1

    .line 1913032
    :cond_15
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 1913033
    :cond_16
    const/16 v2, 0x15

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 1913034
    if-eqz v7, :cond_17

    .line 1913035
    const/4 v2, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 1913036
    :cond_17
    if-eqz v6, :cond_18

    .line 1913037
    const/4 v2, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 1913038
    :cond_18
    const/4 v2, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1913039
    const/4 v2, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1913040
    if-eqz v3, :cond_19

    .line 1913041
    const/4 v3, 0x4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1913042
    :cond_19
    const/4 v2, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1913043
    const/4 v2, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1913044
    const/4 v2, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1913045
    const/16 v2, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1913046
    const/16 v2, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1913047
    const/16 v2, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1913048
    const/16 v2, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1913049
    const/16 v2, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1913050
    if-eqz v9, :cond_1a

    .line 1913051
    const/16 v2, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 1913052
    :cond_1a
    const/16 v2, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 1913053
    if-eqz v8, :cond_1b

    .line 1913054
    const/16 v3, 0xf

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v16

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1913055
    :cond_1b
    const/16 v2, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 1913056
    const/16 v2, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 1913057
    const/16 v2, 0x12

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 1913058
    const/16 v2, 0x13

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 1913059
    const/16 v2, 0x14

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 1913060
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_1c
    move/from16 v30, v29

    move/from16 v29, v28

    move/from16 v28, v27

    move/from16 v27, v26

    move/from16 v26, v23

    move/from16 v23, v20

    move/from16 v20, v17

    move/from16 v33, v9

    move v9, v3

    move v3, v4

    move/from16 v34, v11

    move v11, v8

    move v8, v2

    move/from16 v35, v14

    move/from16 v14, v34

    move/from16 v36, v15

    move/from16 v15, v35

    move/from16 v37, v16

    move-wide/from16 v16, v12

    move v13, v10

    move/from16 v12, v33

    move v10, v7

    move v7, v6

    move v6, v5

    move-wide/from16 v4, v24

    move/from16 v25, v22

    move/from16 v24, v21

    move/from16 v22, v19

    move/from16 v21, v18

    move/from16 v18, v36

    move/from16 v19, v37

    goto/16 :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 7

    .prologue
    const/16 v6, 0x14

    const/16 v3, 0x12

    const/4 v2, 0x2

    const-wide/16 v4, 0x0

    .line 1912864
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1912865
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1912866
    if-eqz v0, :cond_0

    .line 1912867
    const-string v1, "can_viewer_change_guest_status"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1912868
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1912869
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1912870
    if-eqz v0, :cond_1

    .line 1912871
    const-string v1, "can_viewer_join"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1912872
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1912873
    :cond_1
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1912874
    if-eqz v0, :cond_2

    .line 1912875
    const-string v0, "connection_style"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1912876
    invoke-virtual {p0, p1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1912877
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1912878
    if-eqz v0, :cond_3

    .line 1912879
    const-string v1, "cover_photo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1912880
    invoke-static {p0, v0, p2, p3}, LX/CYp;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1912881
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 1912882
    cmp-long v2, v0, v4

    if-eqz v2, :cond_4

    .line 1912883
    const-string v2, "end_timestamp"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1912884
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 1912885
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1912886
    if-eqz v0, :cond_5

    .line 1912887
    const-string v1, "event_declines"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1912888
    invoke-static {p0, v0, p2}, LX/7s4;->a(LX/15i;ILX/0nX;)V

    .line 1912889
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1912890
    if-eqz v0, :cond_6

    .line 1912891
    const-string v1, "event_maybes"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1912892
    invoke-static {p0, v0, p2}, LX/7s5;->a(LX/15i;ILX/0nX;)V

    .line 1912893
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1912894
    if-eqz v0, :cond_7

    .line 1912895
    const-string v1, "event_members"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1912896
    invoke-static {p0, v0, p2}, LX/7s6;->a(LX/15i;ILX/0nX;)V

    .line 1912897
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1912898
    if-eqz v0, :cond_8

    .line 1912899
    const-string v1, "event_place"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1912900
    invoke-static {p0, v0, p2, p3}, LX/7rw;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1912901
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1912902
    if-eqz v0, :cond_9

    .line 1912903
    const-string v1, "friendEventMaybesFirst5"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1912904
    invoke-static {p0, v0, p2, p3}, LX/7s8;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1912905
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1912906
    if-eqz v0, :cond_a

    .line 1912907
    const-string v1, "friendEventMembersFirst5"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1912908
    invoke-static {p0, v0, p2, p3}, LX/7sA;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1912909
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1912910
    if-eqz v0, :cond_b

    .line 1912911
    const-string v1, "friendEventWatchersFirst5"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1912912
    invoke-static {p0, v0, p2, p3}, LX/7sC;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1912913
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1912914
    if-eqz v0, :cond_c

    .line 1912915
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1912916
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1912917
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1912918
    if-eqz v0, :cond_d

    .line 1912919
    const-string v1, "is_all_day"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1912920
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1912921
    :cond_d
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1912922
    if-eqz v0, :cond_e

    .line 1912923
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1912924
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1912925
    :cond_e
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 1912926
    cmp-long v2, v0, v4

    if-eqz v2, :cond_f

    .line 1912927
    const-string v2, "start_timestamp"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1912928
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 1912929
    :cond_f
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1912930
    if-eqz v0, :cond_10

    .line 1912931
    const-string v1, "suggested_event_context_sentence"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1912932
    invoke-static {p0, v0, p2}, LX/7sD;->a(LX/15i;ILX/0nX;)V

    .line 1912933
    :cond_10
    const/16 v0, 0x11

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1912934
    if-eqz v0, :cond_11

    .line 1912935
    const-string v1, "timezone"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1912936
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1912937
    :cond_11
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 1912938
    if-eqz v0, :cond_12

    .line 1912939
    const-string v0, "viewer_guest_status"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1912940
    invoke-virtual {p0, p1, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1912941
    :cond_12
    const/16 v0, 0x13

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1912942
    if-eqz v0, :cond_13

    .line 1912943
    const-string v1, "viewer_inviters"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1912944
    invoke-static {p0, v0, p2, p3}, LX/7u6;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1912945
    :cond_13
    invoke-virtual {p0, p1, v6}, LX/15i;->g(II)I

    move-result v0

    .line 1912946
    if-eqz v0, :cond_14

    .line 1912947
    const-string v0, "viewer_watch_status"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1912948
    invoke-virtual {p0, p1, v6}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1912949
    :cond_14
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1912950
    return-void
.end method
