.class public final LX/Cb4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8JT;


# instance fields
.field public final synthetic a:LX/Cb5;


# direct methods
.method public constructor <init>(LX/Cb5;)V
    .locals 0

    .prologue
    .line 1918980
    iput-object p1, p0, LX/Cb4;->a:LX/Cb5;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 1918981
    iget-object v0, p0, LX/Cb4;->a:LX/Cb5;

    iget-object v0, v0, LX/Cb5;->k:LX/8Jo;

    iget-object v1, p0, LX/Cb4;->a:LX/Cb5;

    iget-object v1, v1, LX/Cb5;->e:LX/CbL;

    .line 1918982
    iget-object v2, v1, LX/CbL;->m:LX/8nB;

    move-object v1, v2

    .line 1918983
    invoke-virtual {v1}, LX/8nB;->b()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/Cb4;->a:LX/Cb5;

    iget-object v2, v2, LX/Cb5;->e:LX/CbL;

    .line 1918984
    iget-object v3, v2, LX/CbL;->m:LX/8nB;

    move-object v2, v3

    .line 1918985
    invoke-virtual {v2}, LX/8nB;->e()Ljava/lang/String;

    move-result-object v2

    .line 1918986
    const-string v3, "cancel"

    invoke-static {v0, v1, v2, v3}, LX/8Jo;->a(LX/8Jo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1918987
    iget-object v0, p0, LX/Cb4;->a:LX/Cb5;

    invoke-static {v0}, LX/Cb5;->e(LX/Cb5;)V

    .line 1918988
    return-void
.end method

.method public final a(Lcom/facebook/tagging/model/TaggingProfile;ILjava/lang/String;)V
    .locals 9

    .prologue
    .line 1918989
    iget-object v0, p0, LX/Cb4;->a:LX/Cb5;

    iget-object v0, v0, LX/Cb5;->f:LX/9hh;

    iget-object v1, p0, LX/Cb4;->a:LX/Cb5;

    iget-object v1, v1, LX/Cb5;->g:Ljava/lang/String;

    iget-object v2, p0, LX/Cb4;->a:LX/Cb5;

    iget-object v2, v2, LX/Cb5;->e:LX/CbL;

    .line 1918990
    iget-object v3, v2, LX/CbL;->g:Landroid/graphics/RectF;

    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1918991
    new-instance v3, Landroid/graphics/PointF;

    iget-object v4, v2, LX/CbL;->g:Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->centerX()F

    move-result v4

    iget-object v5, v2, LX/CbL;->g:Landroid/graphics/RectF;

    invoke-virtual {v5}, Landroid/graphics/RectF;->centerY()F

    move-result v5

    invoke-direct {v3, v4, v5}, Landroid/graphics/PointF;-><init>(FF)V

    move-object v2, v3

    .line 1918992
    invoke-virtual {v0, v1, p1, v2}, LX/9hh;->a(Ljava/lang/String;Lcom/facebook/tagging/model/TaggingProfile;Landroid/graphics/PointF;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1918993
    iget-object v0, p0, LX/Cb4;->a:LX/Cb5;

    invoke-static {v0, p1, p2}, LX/Cb5;->a$redex0(LX/Cb5;Lcom/facebook/tagging/model/TaggingProfile;I)V

    .line 1918994
    iget-object v0, p0, LX/Cb4;->a:LX/Cb5;

    iget-object v0, v0, LX/Cb5;->k:LX/8Jo;

    iget-object v1, p0, LX/Cb4;->a:LX/Cb5;

    iget-object v1, v1, LX/Cb5;->e:LX/CbL;

    .line 1918995
    iget-object v2, v1, LX/CbL;->m:LX/8nB;

    move-object v1, v2

    .line 1918996
    invoke-virtual {v1}, LX/8nB;->e()Ljava/lang/String;

    move-result-object v1

    .line 1918997
    iget-object v3, v0, LX/8Jo;->b:LX/0am;

    invoke-virtual {v3}, LX/0am;->isPresent()Z

    move-result v3

    if-nez v3, :cond_1

    .line 1918998
    :goto_0
    iget-object v0, p0, LX/Cb4;->a:LX/Cb5;

    iget-object v0, v0, LX/Cb5;->e:LX/CbL;

    invoke-virtual {v0}, LX/CbL;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1918999
    iget-object v0, p0, LX/Cb4;->a:LX/Cb5;

    const/4 v1, 0x0

    .line 1919000
    iput-boolean v1, v0, LX/Cb5;->m:Z

    .line 1919001
    iget-object v0, p0, LX/Cb4;->a:LX/Cb5;

    iget-object v0, v0, LX/Cb5;->l:Lcom/facebook/photos/tagging/shared/TagTypeahead;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/photos/tagging/shared/TagTypeahead;->setVisibility(I)V

    .line 1919002
    iget-object v0, p0, LX/Cb4;->a:LX/Cb5;

    iget-object v0, v0, LX/Cb5;->e:LX/CbL;

    .line 1919003
    invoke-virtual {v0}, LX/CbL;->g()Z

    move-result v3

    invoke-static {v3}, LX/0PB;->checkState(Z)V

    .line 1919004
    iget-object v3, v0, LX/CbL;->r:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxInfoModel;

    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1919005
    iget-object v3, v0, LX/CbL;->f:LX/0am;

    invoke-virtual {v3}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    iget-object v4, v0, LX/CbL;->r:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxInfoModel;

    invoke-interface {v3, v4}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v5

    .line 1919006
    const/4 v3, -0x1

    if-ne v5, v3, :cond_2

    .line 1919007
    :goto_1
    iget-object v0, p0, LX/Cb4;->a:LX/Cb5;

    invoke-static {v0}, LX/Cb5;->c(LX/Cb5;)V

    .line 1919008
    :goto_2
    return-void

    .line 1919009
    :cond_0
    iget-object v0, p0, LX/Cb4;->a:LX/Cb5;

    invoke-static {v0}, LX/Cb5;->e(LX/Cb5;)V

    goto :goto_2

    .line 1919010
    :cond_1
    const-string v3, "selection"

    invoke-static {v0, v3}, LX/8Jo;->a(LX/8Jo;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    .line 1919011
    const-string v4, "selected_result_type"

    .line 1919012
    iget-object v5, p1, Lcom/facebook/tagging/model/TaggingProfile;->e:LX/7Gr;

    move-object v5, v5

    .line 1919013
    invoke-virtual {v3, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1919014
    const-string v4, "selected_result_id"

    .line 1919015
    iget-wide v7, p1, Lcom/facebook/tagging/model/TaggingProfile;->b:J

    move-wide v5, v7

    .line 1919016
    invoke-virtual {v3, v4, v5, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1919017
    const-string v4, "selected_result_display_text"

    invoke-virtual {p1}, Lcom/facebook/tagging/model/TaggingProfile;->j()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1919018
    const-string v4, "data_source"

    .line 1919019
    iget-object v5, p1, Lcom/facebook/tagging/model/TaggingProfile;->i:Ljava/lang/String;

    move-object v5, v5

    .line 1919020
    invoke-virtual {v3, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1919021
    const-string v4, "selected_result_typeaehad_type"

    .line 1919022
    iget-object v5, p1, Lcom/facebook/tagging/model/TaggingProfile;->h:Ljava/lang/String;

    move-object v5, v5

    .line 1919023
    invoke-virtual {v3, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1919024
    const-string v4, "selected_input_query"

    invoke-virtual {v3, v4, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1919025
    const-string v4, "media_container_id"

    invoke-virtual {v3, v4, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1919026
    iget-object v4, v0, LX/8Jo;->a:LX/0Zb;

    invoke-interface {v4, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1919027
    iget-object v3, p1, Lcom/facebook/tagging/model/TaggingProfile;->i:Ljava/lang/String;

    move-object v3, v3

    .line 1919028
    const-string v4, "select"

    invoke-static {v0, v3, v1, v4}, LX/8Jo;->a(LX/8Jo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1919029
    :cond_2
    add-int/lit8 v4, v5, 0x1

    iget-object v3, v0, LX/CbL;->f:LX/0am;

    invoke-virtual {v3}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v4, v3, :cond_3

    add-int/lit8 v3, v5, 0x1

    move v4, v3

    .line 1919030
    :goto_3
    iget-object v3, v0, LX/CbL;->f:LX/0am;

    invoke-virtual {v3}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxInfoModel;

    iput-object v3, v0, LX/CbL;->r:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxInfoModel;

    .line 1919031
    iget-object v3, v0, LX/CbL;->f:LX/0am;

    invoke-virtual {v3}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    invoke-interface {v3, v5}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 1919032
    iget-object v3, v0, LX/CbL;->r:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxInfoModel;

    invoke-static {v3}, LX/Caq;->a(Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxInfoModel;)Landroid/graphics/RectF;

    move-result-object v3

    iput-object v3, v0, LX/CbL;->g:Landroid/graphics/RectF;

    .line 1919033
    invoke-static {v0}, LX/CbL;->m(LX/CbL;)V

    .line 1919034
    new-instance v3, LX/CbN;

    iget-object v4, v0, LX/CbL;->r:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxInfoModel;

    invoke-virtual {v4}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxInfoModel;->d()Ljava/lang/String;

    move-result-object v4

    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v5

    invoke-static {v0}, LX/CbL;->n(LX/CbL;)Landroid/graphics/PointF;

    move-result-object v6

    invoke-static {v0}, LX/CbL;->o(LX/CbL;)Landroid/graphics/PointF;

    move-result-object v7

    const/4 v8, 0x1

    invoke-direct/range {v3 .. v8}, LX/CbN;-><init>(Ljava/lang/String;Ljava/util/List;Landroid/graphics/PointF;Landroid/graphics/PointF;Z)V

    iput-object v3, v0, LX/CbL;->j:LX/CbN;

    goto/16 :goto_1

    .line 1919035
    :cond_3
    const/4 v3, 0x0

    move v4, v3

    goto :goto_3
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 1919036
    iget-object v0, p0, LX/Cb4;->a:LX/Cb5;

    iget-object v0, v0, LX/Cb5;->k:LX/8Jo;

    iget-object v1, p0, LX/Cb4;->a:LX/Cb5;

    iget-object v1, v1, LX/Cb5;->e:LX/CbL;

    .line 1919037
    iget-object v2, v1, LX/CbL;->m:LX/8nB;

    move-object v1, v2

    .line 1919038
    invoke-virtual {v1}, LX/8nB;->b()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/Cb4;->a:LX/Cb5;

    iget-object v2, v2, LX/Cb5;->e:LX/CbL;

    .line 1919039
    iget-object p0, v2, LX/CbL;->m:LX/8nB;

    move-object v2, p0

    .line 1919040
    invoke-virtual {v2}, LX/8nB;->e()Ljava/lang/String;

    move-result-object v2

    .line 1919041
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1919042
    invoke-static {v3}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v3

    iput-object v3, v0, LX/8Jo;->b:LX/0am;

    .line 1919043
    const-string v3, "session_start"

    invoke-static {v0, v3}, LX/8Jo;->a(LX/8Jo;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    .line 1919044
    const-string p0, "data_source"

    invoke-virtual {v3, p0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1919045
    const-string p0, "media_container_id"

    invoke-virtual {v3, p0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1919046
    iget-object p0, v0, LX/8Jo;->a:LX/0Zb;

    invoke-interface {p0, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1919047
    return-void
.end method
