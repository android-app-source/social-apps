.class public final LX/Av9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel;",
        ">;",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:I

.field public final synthetic b:J

.field public final synthetic c:LX/AvB;


# direct methods
.method public constructor <init>(LX/AvB;IJ)V
    .locals 1

    .prologue
    .line 1723783
    iput-object p1, p0, LX/Av9;->c:LX/AvB;

    iput p2, p0, LX/Av9;->a:I

    iput-wide p3, p0, LX/Av9;->b:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1723784
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1723785
    if-nez p1, :cond_1

    .line 1723786
    iget-object v0, p0, LX/Av9;->c:LX/AvB;

    iget-object v0, v0, LX/AvB;->i:LX/ArS;

    iget v1, p0, LX/Av9;->a:I

    .line 1723787
    const v2, 0xb60001

    invoke-static {v0, v2, v1}, LX/ArS;->b(LX/ArS;II)V

    .line 1723788
    const v2, 0xb60004

    invoke-static {v0, v2, v1}, LX/ArS;->b(LX/ArS;II)V

    .line 1723789
    :goto_0
    if-eqz p1, :cond_0

    .line 1723790
    iget-object v0, p1, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v0, v0

    .line 1723791
    sget-object v1, LX/0ta;->FROM_SERVER:LX/0ta;

    if-ne v0, v1, :cond_0

    .line 1723792
    iget-object v0, p0, LX/Av9;->c:LX/AvB;

    iget-object v0, v0, LX/AvB;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/AwE;->h:LX/0Tn;

    iget-wide v2, p0, LX/Av9;->b:J

    invoke-interface {v0, v1, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 1723793
    :cond_0
    return-object p1

    .line 1723794
    :cond_1
    iget-object v0, p1, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v0, v0

    .line 1723795
    sget-object v1, LX/0ta;->NO_DATA:LX/0ta;

    if-ne v0, v1, :cond_2

    .line 1723796
    iget-object v0, p0, LX/Av9;->c:LX/AvB;

    iget-object v0, v0, LX/AvB;->i:LX/ArS;

    iget v1, p0, LX/Av9;->a:I

    const/4 v3, 0x3

    .line 1723797
    const v2, 0xb60004

    invoke-static {v0, v2, v1, v3}, LX/ArS;->a(LX/ArS;IIS)V

    .line 1723798
    const v2, 0xb60001

    invoke-static {v0, v2, v1, v3}, LX/ArS;->a(LX/ArS;IIS)V

    .line 1723799
    goto :goto_0

    .line 1723800
    :cond_2
    iget-object v0, p0, LX/Av9;->c:LX/AvB;

    iget-object v1, v0, LX/AvB;->i:LX/ArS;

    .line 1723801
    iget-object v0, p1, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v0, v0

    .line 1723802
    sget-object v2, LX/0ta;->FROM_SERVER:LX/0ta;

    if-ne v0, v2, :cond_3

    const/4 v0, 0x1

    :goto_1
    iget v2, p0, LX/Av9;->a:I

    const v5, 0xb60004

    const v4, 0xb60001

    const/4 v3, 0x2

    .line 1723803
    if-eqz v0, :cond_4

    .line 1723804
    invoke-static {v1, v5, v2, v3}, LX/ArS;->a(LX/ArS;IIS)V

    .line 1723805
    invoke-static {v1, v4, v2}, LX/ArS;->b(LX/ArS;II)V

    .line 1723806
    :goto_2
    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 1723807
    :cond_4
    invoke-static {v1, v4, v2, v3}, LX/ArS;->a(LX/ArS;IIS)V

    .line 1723808
    invoke-static {v1, v5, v2}, LX/ArS;->b(LX/ArS;II)V

    goto :goto_2
.end method
