.class public final LX/Cbt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:LX/5kD;

.field public final synthetic b:Landroid/content/Context;

.field public final synthetic c:LX/CcO;


# direct methods
.method public constructor <init>(LX/CcO;LX/5kD;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1920471
    iput-object p1, p0, LX/Cbt;->c:LX/CcO;

    iput-object p2, p0, LX/Cbt;->a:LX/5kD;

    iput-object p3, p0, LX/Cbt;->b:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    .line 1920472
    iget-object v0, p0, LX/Cbt;->a:LX/5kD;

    const/4 v1, 0x0

    .line 1920473
    if-nez v0, :cond_1

    .line 1920474
    :cond_0
    :goto_0
    move-object v0, v1

    .line 1920475
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1920476
    iget-object v1, p0, LX/Cbt;->c:LX/CcO;

    iget-object v1, v1, LX/CcO;->a:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;

    iget-object v1, v1, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->m:LX/8Q3;

    invoke-virtual {v1, v0}, LX/8Q3;->a(Lcom/facebook/graphql/model/GraphQLStory;)Landroid/content/Intent;

    move-result-object v0

    .line 1920477
    iget-object v1, p0, LX/Cbt;->c:LX/CcO;

    iget-object v1, v1, LX/CcO;->a:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;

    iget-object v1, v1, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->f:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/Cbt;->b:Landroid/content/Context;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1920478
    const/4 v0, 0x1

    return v0

    .line 1920479
    :cond_1
    invoke-interface {v0}, LX/5kD;->ae()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyAndContainerStoryModel$PrivacyScopeModel;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, LX/5kD;->C()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataCreationStoryModel;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, LX/5kD;->ae()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyAndContainerStoryModel$PrivacyScopeModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyAndContainerStoryModel$PrivacyScopeModel;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1920480
    invoke-interface {v0}, LX/5kD;->C()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataCreationStoryModel;

    move-result-object v1

    invoke-static {v1}, LX/5k9;->a(Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataCreationStoryModel;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    goto :goto_0

    .line 1920481
    :cond_2
    invoke-interface {v0}, LX/5kD;->A()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyContainerStoryModel;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, LX/5kD;->A()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyContainerStoryModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyContainerStoryModel;->e()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyContainerStoryModel$PrivacyScopeModel;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, LX/5kD;->A()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyContainerStoryModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyContainerStoryModel;->e()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyContainerStoryModel$PrivacyScopeModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyContainerStoryModel$PrivacyScopeModel;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1920482
    invoke-interface {v0}, LX/5kD;->A()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyContainerStoryModel;

    move-result-object v1

    invoke-static {v1}, LX/5k9;->a(Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaPrivacyContainerStoryModel;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    goto :goto_0
.end method
