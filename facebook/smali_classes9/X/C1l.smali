.class public LX/C1l;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/MountSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:LX/Abd;


# direct methods
.method public constructor <init>(LX/Abd;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1843214
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1843215
    iput-object p1, p0, LX/C1l;->a:LX/Abd;

    .line 1843216
    return-void
.end method

.method public static a(LX/0QB;)LX/C1l;
    .locals 4

    .prologue
    .line 1843217
    const-class v1, LX/C1l;

    monitor-enter v1

    .line 1843218
    :try_start_0
    sget-object v0, LX/C1l;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1843219
    sput-object v2, LX/C1l;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1843220
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1843221
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1843222
    new-instance p0, LX/C1l;

    invoke-static {v0}, LX/Abd;->a(LX/0QB;)LX/Abd;

    move-result-object v3

    check-cast v3, LX/Abd;

    invoke-direct {p0, v3}, LX/C1l;-><init>(LX/Abd;)V

    .line 1843223
    move-object v0, p0

    .line 1843224
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1843225
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C1l;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1843226
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1843227
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/1De;LX/C2A;J)V
    .locals 4

    .prologue
    .line 1843228
    invoke-static {p2, p3}, LX/3Hk;->a(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/C2A;->setText(Ljava/lang/CharSequence;)V

    .line 1843229
    const/4 v0, -0x1

    invoke-virtual {p1, v0}, LX/C2A;->setTextColor(I)V

    .line 1843230
    const/4 v0, 0x0

    invoke-virtual {p0}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0f3f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/C2A;->setTextSize(IF)V

    .line 1843231
    sget-object v0, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {p1, v0}, LX/C2A;->setTypeface(Landroid/graphics/Typeface;)V

    .line 1843232
    return-void
.end method


# virtual methods
.method public final a(LX/1De;IILX/1no;Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1843233
    iget-object v0, p0, LX/C1l;->a:LX/Abd;

    invoke-virtual {v0, p5}, LX/Abd;->b(Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1843234
    new-instance v0, LX/C2A;

    invoke-direct {v0, p1}, LX/C2A;-><init>(Landroid/content/Context;)V

    .line 1843235
    const-wide/16 v2, 0x0

    invoke-static {p1, v0, v2, v3}, LX/C1l;->a(LX/1De;LX/C2A;J)V

    .line 1843236
    invoke-static {p2}, LX/1mh;->b(I)I

    move-result v1

    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-static {p3}, LX/1oC;->a(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/C2A;->measure(II)V

    .line 1843237
    invoke-virtual {v0}, LX/C2A;->getMeasuredWidth()I

    move-result v1

    iput v1, p4, LX/1no;->a:I

    .line 1843238
    invoke-virtual {v0}, LX/C2A;->getMeasuredHeight()I

    move-result v0

    iput v0, p4, LX/1no;->b:I

    .line 1843239
    :goto_0
    return-void

    .line 1843240
    :cond_0
    iput v1, p4, LX/1no;->a:I

    .line 1843241
    iput v1, p4, LX/1no;->b:I

    goto :goto_0
.end method
