.class public final enum LX/Cf4;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Cf4;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Cf4;

.field public static final enum CURRENT:LX/Cf4;

.field public static final enum EXPIRED:LX/Cf4;

.field public static final enum NONE:LX/Cf4;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1925413
    new-instance v0, LX/Cf4;

    const-string v1, "CURRENT"

    invoke-direct {v0, v1, v2}, LX/Cf4;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cf4;->CURRENT:LX/Cf4;

    .line 1925414
    new-instance v0, LX/Cf4;

    const-string v1, "EXPIRED"

    invoke-direct {v0, v1, v3}, LX/Cf4;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cf4;->EXPIRED:LX/Cf4;

    .line 1925415
    new-instance v0, LX/Cf4;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v4}, LX/Cf4;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Cf4;->NONE:LX/Cf4;

    .line 1925416
    const/4 v0, 0x3

    new-array v0, v0, [LX/Cf4;

    sget-object v1, LX/Cf4;->CURRENT:LX/Cf4;

    aput-object v1, v0, v2

    sget-object v1, LX/Cf4;->EXPIRED:LX/Cf4;

    aput-object v1, v0, v3

    sget-object v1, LX/Cf4;->NONE:LX/Cf4;

    aput-object v1, v0, v4

    sput-object v0, LX/Cf4;->$VALUES:[LX/Cf4;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1925419
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Cf4;
    .locals 1

    .prologue
    .line 1925418
    const-class v0, LX/Cf4;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Cf4;

    return-object v0
.end method

.method public static values()[LX/Cf4;
    .locals 1

    .prologue
    .line 1925417
    sget-object v0, LX/Cf4;->$VALUES:[LX/Cf4;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Cf4;

    return-object v0
.end method
