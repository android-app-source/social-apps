.class public LX/AlI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1ZC;


# instance fields
.field private final a:LX/1JA;

.field private final b:LX/2t9;

.field public final c:LX/AlG;

.field private d:I


# direct methods
.method public constructor <init>(LX/1JA;LX/323;LX/AlH;LX/0g2;Landroid/view/ViewGroup;LX/0fz;)V
    .locals 1
    .param p4    # LX/0g2;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Landroid/view/ViewGroup;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/0fz;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1709621
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1709622
    iput-object p1, p0, LX/AlI;->a:LX/1JA;

    .line 1709623
    new-instance p1, LX/2t9;

    invoke-static {p2}, LX/0y5;->a(LX/0QB;)LX/0y5;

    move-result-object v0

    check-cast v0, LX/0y5;

    invoke-direct {p1, v0, p6}, LX/2t9;-><init>(LX/0y5;LX/0fz;)V

    .line 1709624
    move-object v0, p1

    .line 1709625
    iput-object v0, p0, LX/AlI;->b:LX/2t9;

    .line 1709626
    iget-object v0, p0, LX/AlI;->b:LX/2t9;

    .line 1709627
    new-instance p1, LX/AlG;

    invoke-direct {p1, v0, p4, p5}, LX/AlG;-><init>(LX/2t9;LX/0g2;Landroid/view/ViewGroup;)V

    .line 1709628
    move-object v0, p1

    .line 1709629
    iput-object v0, p0, LX/AlI;->c:LX/AlG;

    .line 1709630
    return-void
.end method

.method public static c(LX/AlI;)Z
    .locals 3

    .prologue
    .line 1709618
    invoke-static {p0}, LX/AlI;->d(LX/AlI;)Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, LX/AlI;->d:I

    iget-object v1, p0, LX/AlI;->b:LX/2t9;

    invoke-virtual {v1}, LX/2t9;->a()I

    move-result v1

    iget-object v2, p0, LX/AlI;->a:LX/1JA;

    .line 1709619
    iget p0, v2, LX/1JA;->b:I

    move v2, p0

    .line 1709620
    sub-int/2addr v1, v2

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static d(LX/AlI;)Z
    .locals 1

    .prologue
    .line 1709603
    iget-object v0, p0, LX/AlI;->c:LX/AlG;

    .line 1709604
    iget-object p0, v0, LX/AlG;->d:Landroid/view/View;

    if-eqz p0, :cond_0

    iget-object p0, v0, LX/AlG;->d:Landroid/view/View;

    invoke-virtual {p0}, Landroid/view/View;->getVisibility()I

    move-result p0

    if-nez p0, :cond_0

    const/4 p0, 0x1

    :goto_0
    move v0, p0

    .line 1709605
    return v0

    :cond_0
    const/4 p0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1709617
    const/4 v0, -0x1

    return v0
.end method

.method public final a(LX/0g8;I)V
    .locals 0

    .prologue
    .line 1709616
    return-void
.end method

.method public final a(LX/0g8;III)V
    .locals 2

    .prologue
    .line 1709606
    instance-of v0, p1, LX/0g5;

    if-eqz v0, :cond_0

    check-cast p1, LX/0g5;

    invoke-virtual {p1}, LX/0g5;->D()I

    move-result v0

    .line 1709607
    :goto_0
    iget v1, p0, LX/AlI;->d:I

    if-ne v1, v0, :cond_1

    .line 1709608
    :goto_1
    return-void

    .line 1709609
    :cond_0
    const/4 v0, -0x1

    goto :goto_0

    .line 1709610
    :cond_1
    iput v0, p0, LX/AlI;->d:I

    .line 1709611
    invoke-static {p0}, LX/AlI;->c(LX/AlI;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1709612
    iget-object v0, p0, LX/AlI;->c:LX/AlG;

    .line 1709613
    const/4 p0, 0x0

    invoke-static {v0, p0}, LX/AlG;->a(LX/AlG;I)V

    .line 1709614
    :goto_2
    goto :goto_1

    .line 1709615
    :cond_2
    iget-object v0, p0, LX/AlI;->c:LX/AlG;

    invoke-virtual {v0}, LX/AlG;->b()V

    goto :goto_2
.end method
