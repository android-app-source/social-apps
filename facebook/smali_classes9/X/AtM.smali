.class public LX/AtM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0iK;
.implements LX/As8;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "Lcom/facebook/friendsharing/inspiration/model/CameraStateSpec$ProvidesCameraState;",
        ":",
        "LX/0io;",
        ":",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParamsSpec$ProvidesInspirationDoodleParams;",
        ":",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationStateSpec$ProvidesInspirationState;",
        ":",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingDataSpec$ProvidesInspirationLoggingData;",
        ":",
        "LX/0is;",
        ":",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationTextParamsSpec$ProvidesInspirationTextParams;",
        "DerivedData:",
        "Ljava/lang/Object;",
        "Mutation::",
        "Lcom/facebook/ipc/composer/dataaccessor/ComposerCanSave;",
        ":",
        "Lcom/facebook/friendsharing/inspiration/model/CameraStateSpec$SetsCameraState;",
        ":",
        "Lcom/facebook/composer/attachments/ComposerAttachment$SetsAttachments",
        "<TMutation;>;:",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingDataSpec$SetsInspirationLoggingData",
        "<TMutation;>;:",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationStateSpec$SetsInspirationState",
        "<TMutation;>;:",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModelSpec$SetsInspirationSwipeableModel",
        "<TMutation;>;Services::",
        "LX/0il",
        "<TModelData;>;:",
        "LX/0ik",
        "<TDerivedData;>;:",
        "LX/0im",
        "<TMutation;>;>",
        "Ljava/lang/Object;",
        "LX/0iK",
        "<TModelData;TDerivedData;>;",
        "LX/As8;"
    }
.end annotation


# static fields
.field public static final a:LX/0jK;


# instance fields
.field private A:Lcom/facebook/friendsharing/inspiration/controller/InspirationCameraRollButtonController;

.field private B:LX/At9;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private C:LX/At4;

.field private D:LX/Au0;

.field private E:LX/Asy;

.field private F:LX/Atp;

.field public G:LX/0fO;

.field public H:LX/87O;

.field public I:LX/870;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public J:LX/0hB;

.field public K:LX/0wd;

.field public L:LX/0wW;

.field public M:I

.field public N:Landroid/view/View$OnClickListener;

.field public final b:LX/Ar8;

.field public final c:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TServices;>;"
        }
    .end annotation
.end field

.field private final d:LX/ArL;

.field public final e:LX/ArT;

.field public final f:LX/Ata;

.field public final g:Landroid/content/Context;

.field private final h:LX/AtA;

.field private final i:LX/Asi;

.field private final j:LX/At5;

.field private final k:LX/Au1;

.field private final l:LX/Asz;

.field private final m:LX/Atq;

.field private final n:Z

.field public o:Landroid/view/View;

.field public p:Lcom/facebook/resources/ui/FbFrameLayout;

.field public q:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public r:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public s:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public t:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public u:Landroid/view/View;

.field public v:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private w:LX/6WJ;

.field public x:Lcom/facebook/widget/CustomFrameLayout;

.field public y:LX/Ar3;

.field public z:LX/Ar4;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1721398
    const-class v0, LX/AtM;

    invoke-static {v0}, LX/0jK;->a(Ljava/lang/Class;)LX/0jK;

    move-result-object v0

    sput-object v0, LX/AtM;->a:LX/0jK;

    return-void
.end method

.method public constructor <init>(LX/0il;LX/ArL;LX/ArT;LX/Ata;Landroid/content/Context;LX/AtA;LX/Asi;LX/At5;LX/Au1;LX/Asz;LX/Atq;LX/0fO;LX/0hB;LX/0wW;LX/0W3;)V
    .locals 4
    .param p1    # LX/0il;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/ArL;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/ArT;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/Ata;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TServices;",
            "LX/ArL;",
            "LX/ArT;",
            "LX/Ata;",
            "Landroid/content/Context;",
            "LX/AtA;",
            "LX/Asi;",
            "LX/At5;",
            "LX/Au1;",
            "LX/Asz;",
            "LX/Atq;",
            "LX/0fO;",
            "LX/0hB;",
            "LX/0wW;",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1721405
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1721406
    new-instance v2, LX/AtD;

    invoke-direct {v2, p0}, LX/AtD;-><init>(LX/AtM;)V

    iput-object v2, p0, LX/AtM;->b:LX/Ar8;

    .line 1721407
    sget-object v2, LX/87O;->CAPTURE:LX/87O;

    iput-object v2, p0, LX/AtM;->H:LX/87O;

    .line 1721408
    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v2, p0, LX/AtM;->c:Ljava/lang/ref/WeakReference;

    .line 1721409
    iput-object p2, p0, LX/AtM;->d:LX/ArL;

    .line 1721410
    iput-object p3, p0, LX/AtM;->e:LX/ArT;

    .line 1721411
    iput-object p4, p0, LX/AtM;->f:LX/Ata;

    .line 1721412
    iput-object p5, p0, LX/AtM;->g:Landroid/content/Context;

    .line 1721413
    iput-object p6, p0, LX/AtM;->h:LX/AtA;

    .line 1721414
    iput-object p7, p0, LX/AtM;->i:LX/Asi;

    .line 1721415
    iput-object p8, p0, LX/AtM;->j:LX/At5;

    .line 1721416
    iput-object p9, p0, LX/AtM;->k:LX/Au1;

    .line 1721417
    iput-object p10, p0, LX/AtM;->l:LX/Asz;

    .line 1721418
    iput-object p11, p0, LX/AtM;->m:LX/Atq;

    .line 1721419
    move-object/from16 v0, p12

    iput-object v0, p0, LX/AtM;->G:LX/0fO;

    .line 1721420
    move-object/from16 v0, p13

    iput-object v0, p0, LX/AtM;->J:LX/0hB;

    .line 1721421
    move-object/from16 v0, p14

    iput-object v0, p0, LX/AtM;->L:LX/0wW;

    .line 1721422
    sget-wide v2, LX/0X5;->jR:J

    move-object/from16 v0, p15

    invoke-interface {v0, v2, v3}, LX/0W4;->a(J)Z

    move-result v2

    iput-boolean v2, p0, LX/AtM;->n:Z

    .line 1721423
    return-void
.end method

.method public static a(LX/AtM;LX/87O;)V
    .locals 3

    .prologue
    .line 1721402
    iget-object v0, p0, LX/AtM;->H:LX/87O;

    if-ne v0, p1, :cond_0

    .line 1721403
    :goto_0
    return-void

    .line 1721404
    :cond_0
    iget-object v2, p0, LX/AtM;->K:LX/0wd;

    sget-object v0, LX/87O;->PREVIEW:LX/87O;

    if-ne p1, v0, :cond_1

    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    :goto_1
    invoke-virtual {v2, v0, v1}, LX/0wd;->b(D)LX/0wd;

    goto :goto_0

    :cond_1
    const-wide/16 v0, 0x0

    goto :goto_1
.end method

.method public static a(LX/AtM;Z)V
    .locals 4

    .prologue
    .line 1721399
    iget-object v0, p0, LX/AtM;->o:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1721400
    iget-object v0, p0, LX/AtM;->o:Landroid/view/View;

    new-instance v1, LX/Aui;

    iget-object v2, p0, LX/AtM;->o:Landroid/view/View;

    iget v3, p0, LX/AtM;->M:I

    invoke-direct {v1, v2, p1, v3}, LX/Aui;-><init>(Landroid/view/View;ZI)V

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1721401
    :cond_0
    return-void
.end method

.method private static a(Lcom/facebook/drawee/fbpipeline/FbDraweeView;)V
    .locals 1

    .prologue
    .line 1721270
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1721271
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1721272
    return-void
.end method

.method private a(Lcom/facebook/drawee/fbpipeline/FbDraweeView;LX/AsV;)V
    .locals 1

    .prologue
    .line 1721393
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1721394
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p1, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setAlpha(F)V

    .line 1721395
    invoke-interface {p2, p1}, LX/AsV;->a(Lcom/facebook/drawee/fbpipeline/FbDraweeView;)V

    .line 1721396
    new-instance v0, LX/AtL;

    invoke-direct {v0, p0, p2}, LX/AtL;-><init>(LX/AtM;LX/AsV;)V

    invoke-virtual {p1, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1721397
    return-void
.end method

.method private static b(Lcom/facebook/drawee/fbpipeline/FbDraweeView;LX/AsV;)V
    .locals 2

    .prologue
    .line 1721387
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1721388
    invoke-virtual {p0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 1721389
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    .line 1721390
    invoke-interface {p1, p0}, LX/AsV;->a(Lcom/facebook/drawee/fbpipeline/FbDraweeView;)V

    .line 1721391
    const v0, 0x3e99999a    # 0.3f

    invoke-virtual {p0, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setAlpha(F)V

    .line 1721392
    return-void
.end method

.method public static g(LX/AtM;)V
    .locals 13

    .prologue
    .line 1721357
    const/4 v0, 0x1

    .line 1721358
    invoke-static {}, Landroid/hardware/Camera;->getNumberOfCameras()I

    move-result v1

    if-le v1, v0, :cond_1

    :goto_0
    move v0, v0

    .line 1721359
    if-eqz v0, :cond_0

    .line 1721360
    iget-object v0, p0, LX/AtM;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, LX/AtM;->d:LX/ArL;

    .line 1721361
    new-instance v2, LX/At9;

    check-cast v0, LX/0il;

    invoke-direct {v2, v0, v1}, LX/At9;-><init>(LX/0il;LX/ArL;)V

    .line 1721362
    move-object v0, v2

    .line 1721363
    iput-object v0, p0, LX/AtM;->B:LX/At9;

    .line 1721364
    :cond_0
    iget-object v0, p0, LX/AtM;->i:LX/Asi;

    new-instance v1, LX/AtF;

    invoke-direct {v1, p0}, LX/AtF;-><init>(LX/AtM;)V

    .line 1721365
    new-instance v5, Lcom/facebook/friendsharing/inspiration/controller/InspirationCameraRollButtonController;

    const-class v6, Landroid/content/Context;

    invoke-interface {v0, v6}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/Context;

    invoke-static {v0}, LX/1qZ;->b(LX/0QB;)LX/1Uo;

    move-result-object v7

    check-cast v7, LX/1Uo;

    invoke-static {v0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v8

    check-cast v8, LX/1Ad;

    invoke-static {v0}, LX/1Et;->a(LX/0QB;)LX/1FZ;

    move-result-object v9

    check-cast v9, LX/1FZ;

    move-object v10, v1

    invoke-direct/range {v5 .. v10}, Lcom/facebook/friendsharing/inspiration/controller/InspirationCameraRollButtonController;-><init>(Landroid/content/Context;LX/1Uo;LX/1Ad;LX/1FZ;LX/AsT;)V

    .line 1721366
    move-object v0, v5

    .line 1721367
    iput-object v0, p0, LX/AtM;->A:Lcom/facebook/friendsharing/inspiration/controller/InspirationCameraRollButtonController;

    .line 1721368
    new-instance v0, LX/AtG;

    invoke-direct {v0, p0}, LX/AtG;-><init>(LX/AtM;)V

    .line 1721369
    new-instance v1, LX/At4;

    invoke-direct {v1, v0}, LX/At4;-><init>(LX/AsT;)V

    .line 1721370
    move-object v0, v1

    .line 1721371
    iput-object v0, p0, LX/AtM;->C:LX/At4;

    .line 1721372
    new-instance v0, LX/AtH;

    invoke-direct {v0, p0}, LX/AtH;-><init>(LX/AtM;)V

    .line 1721373
    new-instance v1, LX/Au0;

    invoke-direct {v1, v0}, LX/Au0;-><init>(LX/AsT;)V

    .line 1721374
    move-object v0, v1

    .line 1721375
    iput-object v0, p0, LX/AtM;->D:LX/Au0;

    .line 1721376
    new-instance v0, LX/AtI;

    invoke-direct {v0, p0}, LX/AtI;-><init>(LX/AtM;)V

    .line 1721377
    new-instance v1, LX/Asy;

    invoke-direct {v1, v0}, LX/Asy;-><init>(LX/AsT;)V

    .line 1721378
    move-object v0, v1

    .line 1721379
    iput-object v0, p0, LX/AtM;->E:LX/Asy;

    .line 1721380
    iget-object v0, p0, LX/AtM;->m:LX/Atq;

    iget-object v1, p0, LX/AtM;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, LX/AtM;->d:LX/ArL;

    iget-object v3, p0, LX/AtM;->f:LX/Ata;

    iget-object v4, p0, LX/AtM;->x:Lcom/facebook/widget/CustomFrameLayout;

    .line 1721381
    new-instance v5, LX/Atp;

    move-object v6, v1

    check-cast v6, LX/0il;

    const-class v7, Landroid/content/Context;

    invoke-interface {v0, v7}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/content/Context;

    invoke-static {v0}, LX/0wW;->b(LX/0QB;)LX/0wW;

    move-result-object v11

    check-cast v11, LX/0wW;

    move-object v7, v2

    move-object v8, v3

    move-object v9, v4

    invoke-direct/range {v5 .. v11}, LX/Atp;-><init>(LX/0il;LX/ArL;LX/Ata;Lcom/facebook/widget/CustomFrameLayout;Landroid/content/Context;LX/0wW;)V

    .line 1721382
    const/16 v6, 0x259

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x22ac

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x22a9

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x2e13

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x228e

    invoke-static {v0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x1430

    invoke-static {v0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0x2f0b

    invoke-static {v0, v12}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v12

    .line 1721383
    iput-object v6, v5, LX/Atp;->r:LX/0Ot;

    iput-object v7, v5, LX/Atp;->s:LX/0Ot;

    iput-object v8, v5, LX/Atp;->t:LX/0Ot;

    iput-object v9, v5, LX/Atp;->u:LX/0Ot;

    iput-object v10, v5, LX/Atp;->v:LX/0Ot;

    iput-object v11, v5, LX/Atp;->w:LX/0Ot;

    iput-object v12, v5, LX/Atp;->x:LX/0Ot;

    .line 1721384
    move-object v0, v5

    .line 1721385
    iput-object v0, p0, LX/AtM;->F:LX/Atp;

    .line 1721386
    return-void

    :cond_1
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method public static j(LX/AtM;)V
    .locals 2

    .prologue
    .line 1721424
    iget-object v0, p0, LX/AtM;->p:Lcom/facebook/resources/ui/FbFrameLayout;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbFrameLayout;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    .line 1721425
    :goto_0
    return-void

    .line 1721426
    :cond_0
    iget-object v0, p0, LX/AtM;->p:Lcom/facebook/resources/ui/FbFrameLayout;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbFrameLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method public static k(LX/AtM;)V
    .locals 2

    .prologue
    .line 1721354
    iget-object v0, p0, LX/AtM;->p:Lcom/facebook/resources/ui/FbFrameLayout;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbFrameLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 1721355
    :goto_0
    return-void

    .line 1721356
    :cond_0
    iget-object v0, p0, LX/AtM;->p:Lcom/facebook/resources/ui/FbFrameLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbFrameLayout;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/5L2;)V
    .locals 0

    .prologue
    .line 1721353
    return-void
.end method

.method public final a(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 1721345
    iget-object v0, p0, LX/AtM;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    check-cast v0, LX/0io;

    invoke-interface {v0}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/7kq;->j(LX/0Px;)Z

    move-result v2

    .line 1721346
    if-eqz v2, :cond_0

    const v0, 0x7f082795

    move v1, v0

    .line 1721347
    :goto_0
    if-eqz v2, :cond_1

    const v0, 0x7f082799

    .line 1721348
    :goto_1
    new-instance v2, LX/6WI;

    invoke-direct {v2, p1}, LX/6WI;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v1}, LX/6WI;->a(I)LX/6WI;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/6WI;->b(I)LX/6WI;

    move-result-object v0

    const v1, 0x7f08279a

    new-instance v2, LX/AtK;

    invoke-direct {v2, p0}, LX/AtK;-><init>(LX/AtM;)V

    invoke-virtual {v0, v1, v2}, LX/6WI;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/6WI;

    move-result-object v0

    const v1, 0x7f08279b

    new-instance v2, LX/AtJ;

    invoke-direct {v2, p0}, LX/AtJ;-><init>(LX/AtM;)V

    invoke-virtual {v0, v1, v2}, LX/6WI;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/6WI;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/6WI;->a(Z)LX/6WI;

    move-result-object v0

    invoke-virtual {v0}, LX/6WI;->a()LX/6WJ;

    move-result-object v0

    iput-object v0, p0, LX/AtM;->w:LX/6WJ;

    .line 1721349
    iget-object v0, p0, LX/AtM;->w:LX/6WJ;

    invoke-virtual {v0}, LX/6WJ;->show()V

    .line 1721350
    return-void

    .line 1721351
    :cond_0
    const v0, 0x7f082794

    move v1, v0

    goto :goto_0

    .line 1721352
    :cond_1
    const v0, 0x7f082798

    goto :goto_1
.end method

.method public final a(Lcom/facebook/media/util/model/MediaModel;)V
    .locals 1

    .prologue
    .line 1721342
    iget-object v0, p0, LX/AtM;->A:Lcom/facebook/friendsharing/inspiration/controller/InspirationCameraRollButtonController;

    invoke-virtual {v0, p1}, Lcom/facebook/friendsharing/inspiration/controller/InspirationCameraRollButtonController;->a(Lcom/facebook/media/util/model/MediaModel;)V

    .line 1721343
    invoke-virtual {p0}, LX/AtM;->b()V

    .line 1721344
    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 1721319
    check-cast p1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    .line 1721320
    iget-object v0, p0, LX/AtM;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 1721321
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    .line 1721322
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v0

    .line 1721323
    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->getFormatMode()LX/870;

    move-result-object v0

    const/4 p2, 0x1

    const/4 v4, 0x0

    .line 1721324
    iget-object v2, p0, LX/AtM;->I:LX/870;

    if-ne v0, v2, :cond_3

    .line 1721325
    :goto_0
    move-object v0, p1

    .line 1721326
    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v2

    move-object v0, v1

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v0

    sget-object v3, LX/86o;->NONE:LX/86o;

    invoke-static {v2, v0, v3}, LX/87N;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationState;Lcom/facebook/friendsharing/inspiration/model/InspirationState;LX/86o;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1721327
    invoke-static {p0}, LX/AtM;->k(LX/AtM;)V

    .line 1721328
    :cond_0
    :goto_1
    invoke-static {p1, v1}, LX/Arx;->a(Lcom/facebook/composer/system/model/ComposerModelImpl;Lcom/facebook/composer/system/model/ComposerModelImpl;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1721329
    sget-object v0, LX/87O;->RECORDING:LX/87O;

    invoke-static {p0, v0}, LX/AtM;->a(LX/AtM;LX/87O;)V

    .line 1721330
    sget-object v0, LX/87O;->RECORDING:LX/87O;

    iput-object v0, p0, LX/AtM;->H:LX/87O;

    .line 1721331
    invoke-virtual {p0}, LX/AtM;->b()V

    .line 1721332
    :cond_1
    return-void

    :cond_2
    move-object v0, p1

    .line 1721333
    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v2

    move-object v0, v1

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v0

    sget-object v3, LX/86o;->NONE:LX/86o;

    invoke-static {v2, v0, v3}, LX/87N;->b(Lcom/facebook/friendsharing/inspiration/model/InspirationState;Lcom/facebook/friendsharing/inspiration/model/InspirationState;LX/86o;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1721334
    invoke-static {p0}, LX/AtM;->j(LX/AtM;)V

    goto :goto_1

    .line 1721335
    :cond_3
    iput-object v0, p0, LX/AtM;->I:LX/870;

    .line 1721336
    sget-object v2, LX/AtC;->a:[I

    invoke-virtual {v0}, LX/870;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 1721337
    :pswitch_0
    iget-object v2, p0, LX/AtM;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0il;

    sget-object v3, LX/AtM;->a:LX/0jK;

    sget-object v4, LX/86o;->COLOR_PICKER:LX/86o;

    invoke-static {v2, v3, v4}, LX/87N;->b(LX/0il;LX/0jK;LX/86o;)V

    .line 1721338
    iget-object v2, p0, LX/AtM;->o:Landroid/view/View;

    invoke-virtual {v2, p2}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_0

    .line 1721339
    :pswitch_1
    invoke-static {p0, p2}, LX/AtM;->a(LX/AtM;Z)V

    goto :goto_0

    .line 1721340
    :pswitch_2
    invoke-static {p0, v4}, LX/AtM;->a(LX/AtM;Z)V

    goto :goto_0

    .line 1721341
    :pswitch_3
    iget-object v2, p0, LX/AtM;->o:Landroid/view/View;

    invoke-virtual {v2, v4}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final a()Z
    .locals 3

    .prologue
    .line 1721312
    iget-object v0, p0, LX/AtM;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    const/4 v2, 0x0

    .line 1721313
    invoke-static {v0}, LX/2rf;->b(LX/0io;)Lcom/facebook/composer/attachments/ComposerAttachment;

    move-result-object v1

    .line 1721314
    if-nez v1, :cond_0

    move v1, v2

    .line 1721315
    :goto_0
    move v0, v1

    .line 1721316
    return v0

    .line 1721317
    :cond_0
    invoke-virtual {v1}, Lcom/facebook/composer/attachments/ComposerAttachment;->e()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object p0

    move-object v1, v0

    .line 1721318
    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->p()Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;

    move-result-object v1

    if-eqz v1, :cond_1

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->p()Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    if-eqz p0, :cond_3

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getDoodleParams()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getDoodleParams()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-lez v1, :cond_3

    :cond_2
    const/4 v1, 0x1

    goto :goto_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public final b()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    const/16 v3, 0x8

    .line 1721273
    iget-object v0, p0, LX/AtM;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 1721274
    iget-object v1, p0, LX/AtM;->z:LX/Ar4;

    .line 1721275
    iget-object v2, v1, LX/Ar4;->a:Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;

    iget-object v2, v2, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->V:LX/0iJ;

    .line 1721276
    iget-boolean v1, v2, LX/0iJ;->r:Z

    move v2, v1

    .line 1721277
    move v1, v2

    .line 1721278
    if-eqz v1, :cond_3

    .line 1721279
    iget-boolean v0, p0, LX/AtM;->n:Z

    if-eqz v0, :cond_0

    .line 1721280
    iget-object v0, p0, LX/AtM;->q:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v1, p0, LX/AtM;->C:LX/At4;

    invoke-static {v0, v1}, LX/AtM;->b(Lcom/facebook/drawee/fbpipeline/FbDraweeView;LX/AsV;)V

    .line 1721281
    :cond_0
    iget-object v0, p0, LX/AtM;->B:LX/At9;

    if-eqz v0, :cond_1

    .line 1721282
    iget-object v0, p0, LX/AtM;->r:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v1, p0, LX/AtM;->B:LX/At9;

    invoke-static {v0, v1}, LX/AtM;->b(Lcom/facebook/drawee/fbpipeline/FbDraweeView;LX/AsV;)V

    .line 1721283
    :cond_1
    iget-object v0, p0, LX/AtM;->s:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v1, p0, LX/AtM;->A:Lcom/facebook/friendsharing/inspiration/controller/InspirationCameraRollButtonController;

    invoke-static {v0, v1}, LX/AtM;->b(Lcom/facebook/drawee/fbpipeline/FbDraweeView;LX/AsV;)V

    .line 1721284
    iget-object v0, p0, LX/AtM;->x:Lcom/facebook/widget/CustomFrameLayout;

    invoke-virtual {v0, v3}, Lcom/facebook/widget/CustomFrameLayout;->setVisibility(I)V

    .line 1721285
    iget-object v0, p0, LX/AtM;->t:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1721286
    iget-object v0, p0, LX/AtM;->o:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setEnabled(Z)V

    .line 1721287
    :cond_2
    :goto_0
    return-void

    .line 1721288
    :cond_3
    iget-object v1, p0, LX/AtM;->o:Landroid/view/View;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 1721289
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-static {v0}, LX/Arx;->b(Lcom/facebook/composer/system/model/ComposerModelImpl;)Z

    move-result v0

    .line 1721290
    if-nez v0, :cond_6

    iget-boolean v1, p0, LX/AtM;->n:Z

    if-eqz v1, :cond_6

    .line 1721291
    iget-object v1, p0, LX/AtM;->q:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v2, p0, LX/AtM;->C:LX/At4;

    invoke-direct {p0, v1, v2}, LX/AtM;->a(Lcom/facebook/drawee/fbpipeline/FbDraweeView;LX/AsV;)V

    .line 1721292
    :goto_1
    iget-object v1, p0, LX/AtM;->z:LX/Ar4;

    invoke-virtual {v1}, LX/Ar4;->a()Z

    move-result v1

    if-nez v1, :cond_7

    .line 1721293
    iget-object v1, p0, LX/AtM;->B:LX/At9;

    if-eqz v1, :cond_4

    .line 1721294
    iget-object v1, p0, LX/AtM;->r:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v2, p0, LX/AtM;->B:LX/At9;

    invoke-direct {p0, v1, v2}, LX/AtM;->a(Lcom/facebook/drawee/fbpipeline/FbDraweeView;LX/AsV;)V

    .line 1721295
    :cond_4
    :goto_2
    if-nez v0, :cond_8

    iget-object v1, p0, LX/AtM;->z:LX/Ar4;

    invoke-virtual {v1}, LX/Ar4;->a()Z

    move-result v1

    if-nez v1, :cond_8

    .line 1721296
    iget-object v1, p0, LX/AtM;->s:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v2, p0, LX/AtM;->A:Lcom/facebook/friendsharing/inspiration/controller/InspirationCameraRollButtonController;

    invoke-direct {p0, v1, v2}, LX/AtM;->a(Lcom/facebook/drawee/fbpipeline/FbDraweeView;LX/AsV;)V

    .line 1721297
    :cond_5
    :goto_3
    if-nez v0, :cond_a

    iget-object v1, p0, LX/AtM;->z:LX/Ar4;

    invoke-virtual {v1}, LX/Ar4;->a()Z

    move-result v1

    if-nez v1, :cond_a

    .line 1721298
    iget-object v0, p0, LX/AtM;->x:Lcom/facebook/widget/CustomFrameLayout;

    invoke-virtual {v0, v3}, Lcom/facebook/widget/CustomFrameLayout;->setVisibility(I)V

    .line 1721299
    iget-object v0, p0, LX/AtM;->t:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 1721300
    :cond_6
    iget-object v1, p0, LX/AtM;->q:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {v1}, LX/AtM;->a(Lcom/facebook/drawee/fbpipeline/FbDraweeView;)V

    goto :goto_1

    .line 1721301
    :cond_7
    iget-object v1, p0, LX/AtM;->r:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v2, p0, LX/AtM;->D:LX/Au0;

    invoke-direct {p0, v1, v2}, LX/AtM;->a(Lcom/facebook/drawee/fbpipeline/FbDraweeView;LX/AsV;)V

    goto :goto_2

    .line 1721302
    :cond_8
    if-eqz v0, :cond_9

    .line 1721303
    iget-object v1, p0, LX/AtM;->s:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {v1}, LX/AtM;->a(Lcom/facebook/drawee/fbpipeline/FbDraweeView;)V

    goto :goto_3

    .line 1721304
    :cond_9
    iget-object v1, p0, LX/AtM;->z:LX/Ar4;

    invoke-virtual {v1}, LX/Ar4;->a()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1721305
    iget-object v1, p0, LX/AtM;->s:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v2, p0, LX/AtM;->E:LX/Asy;

    invoke-direct {p0, v1, v2}, LX/AtM;->a(Lcom/facebook/drawee/fbpipeline/FbDraweeView;LX/AsV;)V

    goto :goto_3

    .line 1721306
    :cond_a
    if-eqz v0, :cond_b

    .line 1721307
    iget-object v0, p0, LX/AtM;->x:Lcom/facebook/widget/CustomFrameLayout;

    invoke-virtual {v0, v3}, Lcom/facebook/widget/CustomFrameLayout;->setVisibility(I)V

    .line 1721308
    iget-object v0, p0, LX/AtM;->t:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    .line 1721309
    :cond_b
    iget-object v0, p0, LX/AtM;->z:LX/Ar4;

    invoke-virtual {v0}, LX/Ar4;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1721310
    iget-object v0, p0, LX/AtM;->x:Lcom/facebook/widget/CustomFrameLayout;

    invoke-virtual {v0, v5}, Lcom/facebook/widget/CustomFrameLayout;->setVisibility(I)V

    .line 1721311
    iget-object v0, p0, LX/AtM;->t:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v1, p0, LX/AtM;->F:LX/Atp;

    invoke-direct {p0, v0, v1}, LX/AtM;->a(Lcom/facebook/drawee/fbpipeline/FbDraweeView;LX/AsV;)V

    goto/16 :goto_0
.end method
