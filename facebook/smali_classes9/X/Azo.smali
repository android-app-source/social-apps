.class public final LX/Azo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/Azp;


# direct methods
.method public constructor <init>(LX/Azp;)V
    .locals 0

    .prologue
    .line 1732979
    iput-object p1, p0, LX/Azo;->a:LX/Azp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x50e57505

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1732970
    iget-object v1, p0, LX/Azo;->a:LX/Azp;

    const/4 v2, 0x0

    .line 1732971
    invoke-virtual {v1}, LX/Azp;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string p1, "airplane_mode_on"

    invoke-static {v3, p1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-eqz v3, :cond_0

    const/4 v2, 0x1

    :cond_0
    move v1, v2

    .line 1732972
    if-eqz v1, :cond_1

    .line 1732973
    iget-object v1, p0, LX/Azo;->a:LX/Azp;

    iget-object v1, v1, LX/Azp;->b:LX/0kL;

    new-instance v2, LX/27k;

    const v3, 0x7f082403

    invoke-direct {v2, v3}, LX/27k;-><init>(I)V

    invoke-virtual {v1, v2}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 1732974
    :goto_0
    const v1, 0x2f6adf8d

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 1732975
    :cond_1
    iget-object v1, p0, LX/Azo;->a:LX/Azp;

    iget-object v1, v1, LX/Azp;->a:LX/1b1;

    .line 1732976
    iget-object v2, v1, LX/1b1;->a:LX/17Y;

    iget-object v3, v1, LX/1b1;->c:Landroid/content/Context;

    sget-object v4, LX/0ax;->iH:Ljava/lang/String;

    invoke-interface {v2, v3, v4}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    .line 1732977
    iget-object v4, v1, LX/1b1;->b:Lcom/facebook/content/SecureContextHelper;

    const/16 p0, 0x66

    iget-object v2, v1, LX/1b1;->c:Landroid/content/Context;

    const-class p1, Landroid/app/Activity;

    invoke-static {v2, p1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/Activity;

    invoke-interface {v4, v3, p0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 1732978
    goto :goto_0
.end method
