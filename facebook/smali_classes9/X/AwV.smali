.class public LX/AwV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "LX/AwW;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1726667
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1726668
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 1726681
    check-cast p1, LX/AwW;

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1726682
    iget-object v0, p1, LX/AwW;->c:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1726683
    iget-object v0, p1, LX/AwW;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p1, LX/AwW;->a:LX/4cq;

    if-eqz v0, :cond_3

    :cond_0
    move v0, v2

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1726684
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 1726685
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "style"

    iget-object v5, p1, LX/AwW;->c:Ljava/lang/String;

    invoke-direct {v3, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1726686
    iget-object v3, p1, LX/AwW;->b:Ljava/lang/String;

    if-eqz v3, :cond_1

    .line 1726687
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "url"

    iget-object v5, p1, LX/AwW;->b:Ljava/lang/String;

    invoke-direct {v3, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1726688
    :cond_1
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v3

    const-string v4, "image_style_transfer"

    .line 1726689
    iput-object v4, v3, LX/14O;->b:Ljava/lang/String;

    .line 1726690
    move-object v3, v3

    .line 1726691
    const-string v4, "POST"

    .line 1726692
    iput-object v4, v3, LX/14O;->c:Ljava/lang/String;

    .line 1726693
    move-object v3, v3

    .line 1726694
    const-string v4, "/ai_demos/image_style"

    .line 1726695
    iput-object v4, v3, LX/14O;->d:Ljava/lang/String;

    .line 1726696
    move-object v3, v3

    .line 1726697
    sget-object v4, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v3, v4}, LX/14O;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/14O;

    move-result-object v3

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 1726698
    iput-object v0, v3, LX/14O;->g:Ljava/util/List;

    .line 1726699
    move-object v0, v3

    .line 1726700
    sget-object v3, LX/14S;->JSON:LX/14S;

    .line 1726701
    iput-object v3, v0, LX/14O;->k:LX/14S;

    .line 1726702
    move-object v0, v0

    .line 1726703
    iget-object v3, p1, LX/AwW;->a:LX/4cq;

    if-eqz v3, :cond_2

    .line 1726704
    new-array v2, v2, [LX/4cQ;

    new-instance v3, LX/4cQ;

    const-string v4, "image"

    iget-object v5, p1, LX/AwW;->a:LX/4cq;

    invoke-direct {v3, v4, v5}, LX/4cQ;-><init>(Ljava/lang/String;LX/4cO;)V

    aput-object v3, v2, v1

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    .line 1726705
    iput-object v1, v0, LX/14O;->l:Ljava/util/List;

    .line 1726706
    :cond_2
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0

    :cond_3
    move v0, v1

    .line 1726707
    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1726669
    const/4 v3, 0x1

    .line 1726670
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 1726671
    :try_start_0
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 1726672
    const-string v1, "result_url"

    invoke-virtual {v0, v1}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1726673
    new-instance v0, LX/2Oo;

    const/4 v1, 0x1

    const-string v2, "Invalid response: Missing result_url key"

    invoke-static {v1, v2}, Lcom/facebook/http/protocol/ApiErrorResult;->a(ILjava/lang/String;)LX/2AV;

    move-result-object v1

    invoke-virtual {v1}, LX/2AV;->a()Lcom/facebook/http/protocol/ApiErrorResult;

    move-result-object v1

    invoke-direct {v0, v1}, LX/2Oo;-><init>(Lcom/facebook/http/protocol/ApiErrorResult;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1726674
    :catch_0
    move-exception v0

    .line 1726675
    new-instance v1, LX/2Oo;

    const-string v2, "Invalid response: Parse error"

    invoke-static {v3, v2}, Lcom/facebook/http/protocol/ApiErrorResult;->a(ILjava/lang/String;)LX/2AV;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1726676
    iput-object v0, v2, LX/2AV;->d:Ljava/lang/String;

    .line 1726677
    move-object v0, v2

    .line 1726678
    invoke-virtual {v0}, LX/2AV;->a()Lcom/facebook/http/protocol/ApiErrorResult;

    move-result-object v0

    invoke-direct {v1, v0}, LX/2Oo;-><init>(Lcom/facebook/http/protocol/ApiErrorResult;)V

    throw v1

    .line 1726679
    :cond_0
    :try_start_1
    const-string v1, "result_url"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-virtual {v0}, LX/0lF;->B()Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    .line 1726680
    return-object v0
.end method
