.class public final LX/BmH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;)V
    .locals 0

    .prologue
    .line 1818037
    iput-object p1, p0, LX/BmH;->a:Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x28ddf226

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1818038
    iget-object v0, p0, LX/BmH;->a:Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;

    const/4 v2, 0x0

    iput-object v2, v0, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->y:Ljava/util/Calendar;

    .line 1818039
    iget-object v0, p0, LX/BmH;->a:Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;

    iget-object v0, v0, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->r:Lcom/facebook/uicontrib/calendar/CalendarView;

    invoke-virtual {v0}, Lcom/facebook/uicontrib/calendar/CalendarView;->a()V

    .line 1818040
    iget-object v0, p0, LX/BmH;->a:Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;

    iget-object v0, v0, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->s:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbTextView;->performClick()Z

    .line 1818041
    iget-object v0, p0, LX/BmH;->a:Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;

    iget-object v0, v0, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->z:Landroid/widget/TimePicker;

    invoke-virtual {v0}, Landroid/widget/TimePicker;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 1818042
    iput v4, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 1818043
    iget-object v0, p0, LX/BmH;->a:Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;

    iget-object v0, v0, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->v:Lcom/facebook/widget/CustomLinearLayout;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Lcom/facebook/widget/CustomLinearLayout;->setVisibility(I)V

    .line 1818044
    iget-object v0, p0, LX/BmH;->a:Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;

    iget-object v0, v0, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->u:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v0, v4}, Lcom/facebook/widget/CustomLinearLayout;->setVisibility(I)V

    .line 1818045
    const v0, -0x2035f075

    invoke-static {v3, v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
