.class public LX/BJi;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/BJ7;

.field private final b:LX/0kb;

.field private final c:LX/0ad;

.field public final d:LX/ATy;

.field public final e:LX/1RW;

.field public f:Ljava/lang/String;

.field public g:I

.field public h:I


# direct methods
.method public constructor <init>(LX/BJ7;LX/0kb;LX/0ad;LX/ATy;LX/1RW;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1772321
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1772322
    iput-object p1, p0, LX/BJi;->a:LX/BJ7;

    .line 1772323
    iput-object p2, p0, LX/BJi;->b:LX/0kb;

    .line 1772324
    iput-object p3, p0, LX/BJi;->c:LX/0ad;

    .line 1772325
    iput-object p4, p0, LX/BJi;->d:LX/ATy;

    .line 1772326
    iput-object p5, p0, LX/BJi;->e:LX/1RW;

    .line 1772327
    return-void
.end method

.method public static a(LX/0QB;)LX/BJi;
    .locals 7

    .prologue
    .line 1772318
    new-instance v1, LX/BJi;

    invoke-static {p0}, LX/BJ7;->a(LX/0QB;)LX/BJ7;

    move-result-object v2

    check-cast v2, LX/BJ7;

    invoke-static {p0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v3

    check-cast v3, LX/0kb;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-static {p0}, LX/ATy;->b(LX/0QB;)LX/ATy;

    move-result-object v5

    check-cast v5, LX/ATy;

    invoke-static {p0}, LX/1RW;->b(LX/0QB;)LX/1RW;

    move-result-object v6

    check-cast v6, LX/1RW;

    invoke-direct/range {v1 .. v6}, LX/BJi;-><init>(LX/BJ7;LX/0kb;LX/0ad;LX/ATy;LX/1RW;)V

    .line 1772319
    move-object v0, v1

    .line 1772320
    return-object v0
.end method

.method public static a$redex0(LX/BJi;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 1772316
    iget-object v0, p0, LX/BJi;->e:LX/1RW;

    const-string v2, "platform"

    iget-object v3, p0, LX/BJi;->f:Ljava/lang/String;

    iget v4, p0, LX/BJi;->g:I

    iget v5, p0, LX/BJi;->h:I

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, LX/1RW;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    .line 1772317
    return-void
.end method

.method public static b(LX/BJi;LX/BKm;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1772286
    iget-object v0, p1, LX/BKm;->b:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getLaunchLoggingParams()Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerLaunchLoggingParams;->getSourceSurface()LX/21D;

    move-result-object v0

    sget-object v3, LX/21D;->THIRD_PARTY_APP_VIA_INTENT:LX/21D;

    if-ne v0, v3, :cond_0

    move v0, v1

    .line 1772287
    :goto_0
    iget-object v3, p1, LX/BKm;->b:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    invoke-virtual {v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialShareParams()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v3

    if-eqz v3, :cond_1

    iget-object v3, p1, LX/BKm;->b:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    invoke-virtual {v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getInitialShareParams()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v3

    iget-object v3, v3, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->linkForShare:Ljava/lang/String;

    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    move v3, v1

    .line 1772288
    :goto_1
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    iget-object v0, p1, LX/BKm;->e:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    iget-object v0, v0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    sget-object v3, LX/2rw;->UNDIRECTED:LX/2rw;

    if-ne v0, v3, :cond_2

    iget-object v0, p0, LX/BJi;->b:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/BJi;->c:LX/0ad;

    sget-short v3, LX/1EB;->d:S

    invoke-interface {v0, v3, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_2

    :goto_2
    return v1

    :cond_0
    move v0, v2

    .line 1772289
    goto :goto_0

    :cond_1
    move v3, v2

    .line 1772290
    goto :goto_1

    :cond_2
    move v1, v2

    .line 1772291
    goto :goto_2
.end method

.method public static d(LX/BJi;LX/BKm;)V
    .locals 12

    .prologue
    .line 1772297
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1772298
    iget-object v0, p1, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v0}, Lcom/facebook/platform/composer/model/PlatformComposition;->u()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/model/FacebookProfile;

    .line 1772299
    iget-wide v6, v0, Lcom/facebook/ipc/model/FacebookProfile;->mId:J

    const-wide/16 v8, -0x1

    cmp-long v5, v6, v8

    if-eqz v5, :cond_0

    iget v5, v0, Lcom/facebook/ipc/model/FacebookProfile;->a:I

    if-nez v5, :cond_0

    .line 1772300
    iget-wide v6, v0, Lcom/facebook/ipc/model/FacebookProfile;->mId:J

    invoke-static {v6, v7}, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;->a(J)LX/5Rc;

    move-result-object v5

    iget-object v6, v0, Lcom/facebook/ipc/model/FacebookProfile;->mDisplayName:Ljava/lang/String;

    .line 1772301
    iput-object v6, v5, LX/5Rc;->b:Ljava/lang/String;

    .line 1772302
    move-object v5, v5

    .line 1772303
    iget-object v0, v0, Lcom/facebook/ipc/model/FacebookProfile;->mImageUrl:Ljava/lang/String;

    .line 1772304
    iput-object v0, v5, LX/5Rc;->c:Ljava/lang/String;

    .line 1772305
    move-object v0, v5

    .line 1772306
    invoke-virtual {v0}, LX/5Rc;->a()Lcom/facebook/ipc/composer/model/ComposerTaggedUser;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1772307
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1772308
    :cond_1
    iget-object v0, p0, LX/BJi;->d:LX/ATy;

    sget-object v1, LX/8K5;->PLATFORM_COMPOSER_CANCEL_DIALOG:LX/8K5;

    iget-object v2, p1, LX/BKm;->a:Ljava/lang/String;

    iget-object v3, p1, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v3}, Lcom/facebook/platform/composer/model/PlatformComposition;->f()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    invoke-static {v4}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v4

    iget-object v5, p1, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v5}, Lcom/facebook/platform/composer/model/PlatformComposition;->i()Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v5

    iget-object v6, p1, LX/BKm;->d:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    iget-object v7, p1, LX/BKm;->e:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    iget-object v8, p1, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v8}, Lcom/facebook/platform/composer/model/PlatformComposition;->h()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v8

    iget-object v9, p1, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v9}, Lcom/facebook/platform/composer/model/PlatformComposition;->o()Lcom/facebook/ipc/composer/model/ComposerStickerData;

    move-result-object v9

    new-instance v10, LX/ATv;

    invoke-direct {v10}, LX/ATv;-><init>()V

    iget-object v11, p1, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v11}, Lcom/facebook/platform/composer/model/PlatformComposition;->p()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v11

    .line 1772309
    iput-object v11, v10, LX/ATv;->a:Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    .line 1772310
    move-object v10, v10

    .line 1772311
    iget-object v11, p1, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v11}, Lcom/facebook/platform/composer/model/PlatformComposition;->e()LX/0Px;

    move-result-object v11

    .line 1772312
    iput-object v11, v10, LX/ATv;->b:LX/0Px;

    .line 1772313
    move-object v10, v10

    .line 1772314
    invoke-virtual {v10}, LX/ATv;->a()LX/ATw;

    move-result-object v10

    invoke-virtual/range {v0 .. v10}, LX/ATy;->a(LX/8K5;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLTextWithEntities;LX/0Px;Lcom/facebook/composer/minutiae/model/MinutiaeObject;Lcom/facebook/composer/privacy/model/ComposerPrivacyData;Lcom/facebook/ipc/composer/intent/ComposerTargetData;Lcom/facebook/ipc/composer/model/ComposerLocationInfo;Lcom/facebook/ipc/composer/model/ComposerStickerData;LX/ATw;)V

    .line 1772315
    return-void
.end method


# virtual methods
.method public final a(LX/BKm;)Z
    .locals 11

    .prologue
    .line 1772292
    invoke-static {p0, p1}, LX/BJi;->b(LX/BJi;LX/BKm;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1772293
    iget-object v1, p0, LX/BJi;->e:LX/1RW;

    iget-object v2, p1, LX/BKm;->a:Ljava/lang/String;

    sget-object v3, LX/8K4;->SERVER_DRAFT:LX/8K4;

    iget-object v3, v3, LX/8K4;->analyticsName:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, LX/1RW;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1772294
    const/4 v1, 0x0

    .line 1772295
    :goto_0
    move v0, v1

    .line 1772296
    return v0

    :cond_0
    iget-object v1, p0, LX/BJi;->d:LX/ATy;

    iget-object v2, p1, LX/BKm;->a:Ljava/lang/String;

    iget-object v3, p1, LX/BKm;->e:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    iget-object v4, p1, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v4}, Lcom/facebook/platform/composer/model/PlatformComposition;->z()Z

    move-result v4

    iget-object v5, p1, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v5}, Lcom/facebook/platform/composer/model/PlatformComposition;->p()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v5

    iget-object v6, p1, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v6}, Lcom/facebook/platform/composer/model/PlatformComposition;->c()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v6

    iget-object v7, p1, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v7}, Lcom/facebook/platform/composer/model/PlatformComposition;->w()Lcom/facebook/share/model/ComposerAppAttribution;

    move-result-object v7

    iget-object v8, p1, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v8}, Lcom/facebook/platform/composer/model/PlatformComposition;->o()Lcom/facebook/ipc/composer/model/ComposerStickerData;

    move-result-object v8

    iget-object v9, p1, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v9}, Lcom/facebook/platform/composer/model/PlatformComposition;->h()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v9

    iget-object v10, p1, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v10}, Lcom/facebook/platform/composer/model/PlatformComposition;->e()LX/0Px;

    move-result-object v10

    invoke-virtual/range {v1 .. v10}, LX/ATy;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerTargetData;ZLcom/facebook/ipc/composer/intent/ComposerShareParams;Lcom/facebook/graphql/model/GraphQLAlbum;Lcom/facebook/share/model/ComposerAppAttribution;Lcom/facebook/ipc/composer/model/ComposerStickerData;Lcom/facebook/ipc/composer/model/ComposerLocationInfo;LX/0Px;)Z

    move-result v1

    goto :goto_0
.end method
