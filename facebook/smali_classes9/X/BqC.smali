.class public LX/BqC;
.super LX/95a;
.source ""


# annotations
.annotation build Landroid/support/annotation/UiThread;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Edge:",
        "Ljava/lang/Object;",
        "UserInfo:",
        "Ljava/lang/Object;",
        ">",
        "LX/95a",
        "<TEdge;TUserInfo;>;"
    }
.end annotation


# instance fields
.field private final a:LX/2kW;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/controller/connectioncontroller/common/ConnectionController",
            "<TEdge;TUserInfo;>;"
        }
    .end annotation
.end field

.field private final b:LX/Bpx;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/photosfeed/connectioncontroller/InitialPositionLoadedListener$ItemAction",
            "<TEdge;>;"
        }
    .end annotation
.end field

.field private final c:I

.field private d:Z


# direct methods
.method public constructor <init>(LX/2kW;ILX/Bpx;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/controller/connectioncontroller/common/ConnectionController",
            "<TEdge;TUserInfo;>;I",
            "Lcom/facebook/feed/rows/photosfeed/connectioncontroller/InitialPositionLoadedListener$ItemAction",
            "<TEdge;>;)V"
        }
    .end annotation

    .prologue
    .line 1824414
    invoke-direct {p0}, LX/95a;-><init>()V

    .line 1824415
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/BqC;->d:Z

    .line 1824416
    iput-object p1, p0, LX/BqC;->a:LX/2kW;

    .line 1824417
    iput-object p3, p0, LX/BqC;->b:LX/Bpx;

    .line 1824418
    iput p2, p0, LX/BqC;->c:I

    .line 1824419
    return-void
.end method


# virtual methods
.method public final a(LX/0Px;LX/3Cb;LX/2kM;LX/2kM;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/3CY;",
            ">;",
            "LX/3Cb;",
            "LX/2kM",
            "<TEdge;>;",
            "LX/2kM",
            "<TEdge;>;)V"
        }
    .end annotation

    .prologue
    .line 1824420
    iget-boolean v0, p0, LX/BqC;->d:Z

    if-nez v0, :cond_0

    invoke-interface {p4}, LX/2kM;->c()I

    move-result v0

    iget v1, p0, LX/BqC;->c:I

    if-le v0, v1, :cond_0

    .line 1824421
    iget-object v0, p0, LX/BqC;->b:LX/Bpx;

    iget v1, p0, LX/BqC;->c:I

    invoke-interface {p4, v1}, LX/2kM;->a(I)Ljava/lang/Object;

    move-result-object v1

    .line 1824422
    check-cast v1, LX/5kD;

    .line 1824423
    invoke-static {v1}, LX/5k9;->a(LX/5kD;)Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v3

    .line 1824424
    iget-object p1, v0, LX/Bpx;->b:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;

    invoke-static {p1, v3}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->a$redex0(Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;Lcom/facebook/graphql/model/GraphQLMedia;)I

    move-result v3

    .line 1824425
    iget-object p1, v0, LX/Bpx;->b:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;

    iget-object p1, p1, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->G:LX/0g8;

    iget-object p2, v0, LX/Bpx;->b:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;

    iget p3, v0, LX/Bpx;->a:I

    const/4 p4, 0x0

    invoke-static {p2, p3, v3, p4}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->a$redex0(Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;III)Ljava/lang/Runnable;

    move-result-object v3

    invoke-interface {p1, v3}, LX/0g8;->a(Ljava/lang/Runnable;)V

    .line 1824426
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/BqC;->d:Z

    .line 1824427
    iget-object v0, p0, LX/BqC;->a:LX/2kW;

    invoke-virtual {v0, p0}, LX/2kW;->b(LX/1vq;)V

    .line 1824428
    :cond_0
    return-void
.end method
