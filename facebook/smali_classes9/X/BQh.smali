.class public final LX/BQh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoParams;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1782578
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1782579
    new-instance v0, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoParams;

    invoke-direct {v0, p1}, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoParams;-><init>(Landroid/os/Parcel;)V

    return-object v0
.end method

.method public final newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1782580
    new-array v0, p1, [Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoParams;

    return-object v0
.end method
