.class public final LX/BwZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic b:LX/Bwa;


# direct methods
.method public constructor <init>(LX/Bwa;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 0

    .prologue
    .line 1833970
    iput-object p1, p0, LX/BwZ;->b:LX/Bwa;

    iput-object p2, p0, LX/BwZ;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x2da3b925

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1833971
    iget-object v1, p0, LX/BwZ;->b:LX/Bwa;

    iget-object v2, p0, LX/BwZ;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 1833972
    iget-object v4, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, v4

    .line 1833973
    check-cast v4, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->aD()Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;->m()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v4

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLSavedState;->SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-ne v4, v7, :cond_0

    move v4, v5

    .line 1833974
    :goto_0
    if-eqz v4, :cond_1

    .line 1833975
    iget-object v7, v1, LX/Bwa;->o:LX/1Sj;

    const-string v8, "overlay_toggle_button"

    const-string p0, "native_story"

    invoke-virtual {v7, v2, v8, p0}, LX/1Sj;->b(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Ljava/lang/String;)V

    .line 1833976
    :goto_1
    invoke-virtual {v1}, LX/3Gn;->j()V

    .line 1833977
    if-nez v4, :cond_2

    move v4, v5

    :goto_2
    invoke-virtual {v1, v4}, LX/Bwa;->a(Z)V

    .line 1833978
    const v1, -0x3aeda60d

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    :cond_0
    move v4, v6

    .line 1833979
    goto :goto_0

    .line 1833980
    :cond_1
    iget-object v7, v1, LX/Bwa;->o:LX/1Sj;

    const-string v8, "overlay_toggle_button"

    const-string p0, "native_story"

    invoke-virtual {v7, v2, v8, p0}, LX/1Sj;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Ljava/lang/String;)V

    .line 1833981
    iget-object v7, v1, LX/Bwa;->s:LX/0kL;

    new-instance v8, LX/27k;

    iget-object p0, v1, LX/Bwa;->q:LX/15X;

    invoke-virtual {v1}, LX/Bwa;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p0, p1}, LX/15X;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p0

    invoke-direct {v8, p0}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v7, v8}, LX/0kL;->b(LX/27k;)LX/27l;

    goto :goto_1

    :cond_2
    move v4, v6

    .line 1833982
    goto :goto_2
.end method
