.class public final LX/AmP;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/AmP;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/AmN;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/AmQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1710795
    const/4 v0, 0x0

    sput-object v0, LX/AmP;->a:LX/AmP;

    .line 1710796
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/AmP;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1710797
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1710798
    new-instance v0, LX/AmQ;

    invoke-direct {v0}, LX/AmQ;-><init>()V

    iput-object v0, p0, LX/AmP;->c:LX/AmQ;

    .line 1710799
    return-void
.end method

.method public static c(LX/1De;)LX/AmN;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1710800
    new-instance v1, LX/AmO;

    invoke-direct {v1}, LX/AmO;-><init>()V

    .line 1710801
    sget-object v2, LX/AmP;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/AmN;

    .line 1710802
    if-nez v2, :cond_0

    .line 1710803
    new-instance v2, LX/AmN;

    invoke-direct {v2}, LX/AmN;-><init>()V

    .line 1710804
    :cond_0
    invoke-static {v2, p0, v0, v0, v1}, LX/AmN;->a$redex0(LX/AmN;LX/1De;IILX/AmO;)V

    .line 1710805
    move-object v1, v2

    .line 1710806
    move-object v0, v1

    .line 1710807
    return-object v0
.end method

.method public static declared-synchronized q()LX/AmP;
    .locals 2

    .prologue
    .line 1710808
    const-class v1, LX/AmP;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/AmP;->a:LX/AmP;

    if-nez v0, :cond_0

    .line 1710809
    new-instance v0, LX/AmP;

    invoke-direct {v0}, LX/AmP;-><init>()V

    sput-object v0, LX/AmP;->a:LX/AmP;

    .line 1710810
    :cond_0
    sget-object v0, LX/AmP;->a:LX/AmP;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 1710811
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 1

    .prologue
    .line 1710812
    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v0

    const p0, 0x7f020caa

    invoke-virtual {v0, p0}, LX/1o5;->h(I)LX/1o5;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    const p0, 0x7f0b0648

    invoke-interface {v0, p0}, LX/1Di;->q(I)LX/1Di;

    move-result-object v0

    const p0, 0x7f0b0648

    invoke-interface {v0, p0}, LX/1Di;->i(I)LX/1Di;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    move-object v0, v0

    .line 1710813
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1710814
    invoke-static {}, LX/1dS;->b()V

    .line 1710815
    const/4 v0, 0x0

    return-object v0
.end method
