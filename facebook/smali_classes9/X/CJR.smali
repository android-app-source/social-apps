.class public LX/CJR;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:LX/CJP;

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3N1;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final d:Landroid/content/Context;

.field private final e:LX/17T;

.field private f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CNE;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final g:Lcom/facebook/ui/media/attachments/MediaResourceHelper;

.field private final h:LX/CJL;

.field private final i:LX/3N3;

.field private j:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CJO;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final k:LX/CJS;

.field private final l:LX/CJT;

.field private final m:LX/CJU;

.field private final n:Landroid/content/res/Resources;

.field private final o:Lcom/facebook/content/SecureContextHelper;

.field private final p:LX/1qI;

.field private final q:LX/CJM;

.field private r:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CJ5;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final s:LX/1CX;

.field private final t:Ljava/util/concurrent/Executor;

.field private final u:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public v:LX/CJQ;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1875422
    const-class v0, LX/CJR;

    invoke-virtual {v0}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/CJR;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/1qI;Landroid/content/Context;LX/1CX;LX/17T;Lcom/facebook/ui/media/attachments/MediaResourceHelper;LX/3N3;LX/CJM;LX/CJL;LX/CJS;LX/CJT;LX/CJU;Landroid/content/res/Resources;Lcom/facebook/content/SecureContextHelper;Ljava/util/concurrent/Executor;LX/0Or;)V
    .locals 2
    .param p14    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p15    # LX/0Or;
        .annotation runtime Lcom/facebook/messaging/annotations/IsPlatformInstallTimeCheckEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1qI;",
            "Landroid/content/Context;",
            "LX/1CX;",
            "LX/17T;",
            "Lcom/facebook/ui/media/attachments/MediaResourceHelper;",
            "LX/3N3;",
            "LX/CJM;",
            "LX/CJL;",
            "LX/CJS;",
            "LX/CJT;",
            "LX/CJU;",
            "Landroid/content/res/Resources;",
            "Lcom/facebook/content/SecureContextHelper;",
            "Ljava/util/concurrent/Executor;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1875423
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1875424
    new-instance v1, LX/CJP;

    invoke-direct {v1, p0}, LX/CJP;-><init>(LX/CJR;)V

    iput-object v1, p0, LX/CJR;->b:LX/CJP;

    .line 1875425
    invoke-static {}, LX/0Op;->a()LX/0Ot;

    move-result-object v1

    iput-object v1, p0, LX/CJR;->c:LX/0Ot;

    .line 1875426
    invoke-static {}, LX/0Op;->a()LX/0Ot;

    move-result-object v1

    iput-object v1, p0, LX/CJR;->f:LX/0Ot;

    .line 1875427
    invoke-static {}, LX/0Op;->a()LX/0Ot;

    move-result-object v1

    iput-object v1, p0, LX/CJR;->j:LX/0Ot;

    .line 1875428
    invoke-static {}, LX/0Op;->a()LX/0Ot;

    move-result-object v1

    iput-object v1, p0, LX/CJR;->r:LX/0Ot;

    .line 1875429
    iput-object p1, p0, LX/CJR;->p:LX/1qI;

    .line 1875430
    iput-object p2, p0, LX/CJR;->d:Landroid/content/Context;

    .line 1875431
    iput-object p3, p0, LX/CJR;->s:LX/1CX;

    .line 1875432
    iput-object p4, p0, LX/CJR;->e:LX/17T;

    .line 1875433
    iput-object p5, p0, LX/CJR;->g:Lcom/facebook/ui/media/attachments/MediaResourceHelper;

    .line 1875434
    iput-object p6, p0, LX/CJR;->i:LX/3N3;

    .line 1875435
    iput-object p7, p0, LX/CJR;->q:LX/CJM;

    .line 1875436
    iput-object p8, p0, LX/CJR;->h:LX/CJL;

    .line 1875437
    iput-object p9, p0, LX/CJR;->k:LX/CJS;

    .line 1875438
    iput-object p10, p0, LX/CJR;->l:LX/CJT;

    .line 1875439
    iput-object p11, p0, LX/CJR;->m:LX/CJU;

    .line 1875440
    iput-object p12, p0, LX/CJR;->n:Landroid/content/res/Resources;

    .line 1875441
    iput-object p13, p0, LX/CJR;->o:Lcom/facebook/content/SecureContextHelper;

    .line 1875442
    move-object/from16 v0, p14

    iput-object v0, p0, LX/CJR;->t:Ljava/util/concurrent/Executor;

    .line 1875443
    move-object/from16 v0, p15

    iput-object v0, p0, LX/CJR;->u:LX/0Or;

    .line 1875444
    return-void
.end method
