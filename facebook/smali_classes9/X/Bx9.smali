.class public LX/Bx9;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pm;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/BxI;

.field public final b:LX/BxC;


# direct methods
.method public constructor <init>(LX/BxI;LX/BxC;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1835051
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1835052
    iput-object p1, p0, LX/Bx9;->a:LX/BxI;

    .line 1835053
    iput-object p2, p0, LX/Bx9;->b:LX/BxC;

    .line 1835054
    return-void
.end method

.method public static a(LX/0QB;)LX/Bx9;
    .locals 5

    .prologue
    .line 1835055
    const-class v1, LX/Bx9;

    monitor-enter v1

    .line 1835056
    :try_start_0
    sget-object v0, LX/Bx9;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1835057
    sput-object v2, LX/Bx9;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1835058
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1835059
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1835060
    new-instance p0, LX/Bx9;

    invoke-static {v0}, LX/BxI;->a(LX/0QB;)LX/BxI;

    move-result-object v3

    check-cast v3, LX/BxI;

    invoke-static {v0}, LX/BxC;->a(LX/0QB;)LX/BxC;

    move-result-object v4

    check-cast v4, LX/BxC;

    invoke-direct {p0, v3, v4}, LX/Bx9;-><init>(LX/BxI;LX/BxC;)V

    .line 1835061
    move-object v0, p0

    .line 1835062
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1835063
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Bx9;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1835064
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1835065
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
