.class public LX/C6W;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/24J;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ">",
        "Ljava/lang/Object;",
        "LX/24J",
        "<",
        "LX/C6V;",
        "TV;>;"
    }
.end annotation


# instance fields
.field public final a:LX/1Rg;

.field private final b:I


# direct methods
.method public constructor <init>(LX/1Rg;I)V
    .locals 0

    .prologue
    .line 1849965
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1849966
    iput-object p1, p0, LX/C6W;->a:LX/1Rg;

    .line 1849967
    iput p2, p0, LX/C6W;->b:I

    .line 1849968
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Ljava/lang/Object;Landroid/view/View;)Ljava/lang/Runnable;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 1849969
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/util/List;Ljava/lang/Object;Ljava/lang/Object;Landroid/view/View;)V
    .locals 11

    .prologue
    .line 1849970
    check-cast p3, LX/C6V;

    .line 1849971
    iget-boolean v0, p3, LX/C6V;->b:Z

    if-eqz v0, :cond_1

    iget-object v0, p3, LX/C6V;->a:LX/C6a;

    .line 1849972
    iget-boolean v1, v0, LX/C6a;->g:Z

    move v0, v1

    .line 1849973
    if-nez v0, :cond_1

    .line 1849974
    iget v0, p0, LX/C6W;->b:I

    .line 1849975
    iget-object v4, p0, LX/C6W;->a:LX/1Rg;

    const-wide/16 v6, 0x12c

    const/4 v8, 0x0

    new-instance v10, LX/C6U;

    invoke-direct {v10, p0, p4, p3}, LX/C6U;-><init>(LX/C6W;Landroid/view/View;LX/C6V;)V

    move-object v5, p4

    move v9, v0

    invoke-virtual/range {v4 .. v10}, LX/1Rg;->a(Landroid/view/View;JIILandroid/animation/Animator$AnimatorListener;)LX/8sj;

    move-result-object v4

    move-object v0, v4

    .line 1849976
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1849977
    :cond_0
    :goto_0
    return-void

    .line 1849978
    :cond_1
    iget-boolean v0, p3, LX/C6V;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p3, LX/C6V;->a:LX/C6a;

    .line 1849979
    iget-boolean v1, v0, LX/C6a;->i:Z

    move v0, v1

    .line 1849980
    if-nez v0, :cond_0

    .line 1849981
    iget v0, p0, LX/C6W;->b:I

    const/16 v1, 0x12c

    iget-object v2, p0, LX/C6W;->a:LX/1Rg;

    iget-object v3, p3, LX/C6V;->d:LX/3Ii;

    invoke-static {p4, v0, v1, v2, v3}, LX/C6J;->a(Landroid/view/View;IILX/1Rg;LX/3Ii;)LX/8sj;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method
