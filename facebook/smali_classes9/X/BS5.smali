.class public LX/BS5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/BRt;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/BRt",
        "<",
        "LX/5wQ;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile h:LX/BS5;


# instance fields
.field public volatile a:LX/0Or;
    .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public volatile b:LX/0Or;
    .annotation runtime Lcom/facebook/timeline/widget/actionbar/IsAddToGroupsMenuItemEnabled;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field public volatile c:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/IsMeUserAWorkUser;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field public volatile d:LX/0Or;
    .annotation runtime Lcom/facebook/timeline/widget/actionbar/IsCopyProfileLinkEnabled;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public volatile e:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0ad;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public g:LX/0W3;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1784706
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1784707
    return-void
.end method

.method public static a(LX/0QB;)LX/BS5;
    .locals 10

    .prologue
    .line 1784708
    sget-object v0, LX/BS5;->h:LX/BS5;

    if-nez v0, :cond_1

    .line 1784709
    const-class v1, LX/BS5;

    monitor-enter v1

    .line 1784710
    :try_start_0
    sget-object v0, LX/BS5;->h:LX/BS5;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1784711
    if-eqz v2, :cond_0

    .line 1784712
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1784713
    new-instance v3, LX/BS5;

    invoke-direct {v3}, LX/BS5;-><init>()V

    .line 1784714
    const/16 v4, 0x1467

    invoke-static {v0, v4}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 v5, 0x36b

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 v6, 0x2fc

    invoke-static {v0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const/16 v7, 0x1588

    invoke-static {v0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    const/16 v8, 0x15e7

    invoke-static {v0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v9

    check-cast v9, LX/0ad;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object p0

    check-cast p0, LX/0W3;

    .line 1784715
    iput-object v4, v3, LX/BS5;->a:LX/0Or;

    iput-object v5, v3, LX/BS5;->b:LX/0Or;

    iput-object v6, v3, LX/BS5;->c:LX/0Or;

    iput-object v7, v3, LX/BS5;->d:LX/0Or;

    iput-object v8, v3, LX/BS5;->e:LX/0Or;

    iput-object v9, v3, LX/BS5;->f:LX/0ad;

    iput-object p0, v3, LX/BS5;->g:LX/0W3;

    .line 1784716
    move-object v0, v3

    .line 1784717
    sput-object v0, LX/BS5;->h:LX/BS5;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1784718
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1784719
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1784720
    :cond_1
    sget-object v0, LX/BS5;->h:LX/BS5;

    return-object v0

    .line 1784721
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1784722
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/5wQ;LX/BS1;Z)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1784723
    iget-object v0, p0, LX/BS5;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    .line 1784724
    iget-object v1, p0, LX/BS5;->f:LX/0ad;

    invoke-static {p1, v0, v1, p2, p3}, LX/BRx;->a(LX/5wN;LX/03R;LX/0ad;LX/BS1;Z)V

    .line 1784725
    invoke-static {p1, p2}, LX/BS4;->a(LX/5wP;LX/BS1;)V

    .line 1784726
    iget-object v0, p0, LX/BS5;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    .line 1784727
    iget-object v1, p0, LX/BS5;->f:LX/0ad;

    invoke-static {v1, p1, v0, p2}, LX/BRy;->a(LX/0ad;LX/5wO;LX/03R;LX/BS1;)V

    .line 1784728
    iget-object v0, p0, LX/BS5;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 1784729
    iget-object v1, p0, LX/BS5;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    .line 1784730
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, LX/5wQ;->r()Z

    move-result v1

    if-nez v1, :cond_0

    move v1, v2

    .line 1784731
    :goto_0
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    if-nez v1, :cond_1

    :goto_1
    invoke-static {v2, p2}, LX/BRv;->a(ZLX/BS1;)V

    .line 1784732
    return-void

    :cond_0
    move v1, v3

    .line 1784733
    goto :goto_0

    :cond_1
    move v2, v3

    .line 1784734
    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;LX/BS1;Z)V
    .locals 2

    .prologue
    .line 1784735
    check-cast p1, LX/5wQ;

    .line 1784736
    iget-object v0, p0, LX/BS5;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1784737
    invoke-interface {p1}, LX/5wQ;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    .line 1784738
    if-eqz v0, :cond_0

    .line 1784739
    invoke-interface {p1}, LX/5wQ;->t()Z

    move-result v0

    invoke-virtual {p0, v0, p2}, LX/BS5;->a(ZLX/BS1;)V

    .line 1784740
    :goto_0
    return-void

    .line 1784741
    :cond_0
    invoke-virtual {p0, p1, p2, p3}, LX/BS5;->a(LX/5wQ;LX/BS1;Z)V

    goto :goto_0
.end method

.method public final a(ZLX/BS1;)V
    .locals 12

    .prologue
    .line 1784742
    const/4 v1, 0x0

    .line 1784743
    if-eqz p1, :cond_0

    iget-object v2, p0, LX/BS5;->g:LX/0W3;

    sget-wide v3, LX/0X5;->lG:J

    invoke-interface {v2, v3, v4, v1}, LX/0W4;->a(JZ)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    move v0, v1

    .line 1784744
    if-eqz v0, :cond_1

    .line 1784745
    const/4 v6, 0x1

    .line 1784746
    const/16 v2, 0x13

    const v3, 0x7f081556

    const v4, 0x7f020772

    const/4 v5, 0x0

    move-object v1, p2

    move v7, v6

    invoke-interface/range {v1 .. v7}, LX/BS1;->a(IIIIZZ)V

    .line 1784747
    :cond_1
    const/4 v6, 0x1

    .line 1784748
    const/16 v2, 0xa

    const v3, 0x7f081555

    const v4, 0x7f02098f

    const/4 v5, 0x0

    move-object v1, p2

    move v7, v6

    invoke-interface/range {v1 .. v7}, LX/BS1;->a(IIIIZZ)V

    .line 1784749
    const/4 v6, 0x1

    .line 1784750
    const/16 v2, 0xb

    const v3, 0x7f081557

    const v4, 0x7f02095e

    const/4 v5, 0x0

    move-object v1, p2

    move v7, v6

    invoke-interface/range {v1 .. v7}, LX/BS1;->a(IIIIZZ)V

    .line 1784751
    const/4 v6, 0x1

    .line 1784752
    const/16 v2, 0xc

    const v3, 0x7f081553

    const v4, 0x7f02098b

    const/4 v5, 0x0

    move-object v1, p2

    move v7, v6

    invoke-interface/range {v1 .. v7}, LX/BS1;->a(IIIIZZ)V

    .line 1784753
    const/4 v6, 0x1

    .line 1784754
    const/16 v2, 0xd

    const v3, 0x7f081554

    const v4, 0x7f020902

    const/4 v5, 0x2

    move-object v1, p2

    move v7, v6

    invoke-interface/range {v1 .. v7}, LX/BS1;->a(IIIIZZ)V

    .line 1784755
    const/4 v5, 0x2

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 1784756
    iget-object v1, p0, LX/BS5;->c:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/03R;

    .line 1784757
    iget-object v8, p0, LX/BS5;->g:LX/0W3;

    sget-wide v10, LX/0X5;->jQ:J

    const/4 v9, 0x0

    invoke-interface {v8, v10, v11, v9}, LX/0W4;->a(JZ)Z

    move-result v8

    move v2, v8

    .line 1784758
    if-eqz v2, :cond_3

    .line 1784759
    const/16 v2, 0x10

    const v3, 0x7f081551

    const v4, 0x7f02088a

    invoke-virtual {v1, v7}, LX/03R;->asBoolean(Z)Z

    move-result v1

    if-nez v1, :cond_2

    move v7, v6

    :cond_2
    move-object v1, p2

    invoke-interface/range {v1 .. v7}, LX/BS1;->a(IIIIZZ)V

    .line 1784760
    :goto_0
    iget-object v0, p0, LX/BS5;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 1784761
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-static {v0, p2}, LX/BRv;->a(ZLX/BS1;)V

    .line 1784762
    return-void

    .line 1784763
    :cond_3
    const/16 v2, 0x9

    const v3, 0x7f081550

    const v4, 0x7f02088a

    invoke-virtual {v1, v7}, LX/03R;->asBoolean(Z)Z

    move-result v1

    if-nez v1, :cond_4

    move v7, v6

    :cond_4
    move-object v1, p2

    invoke-interface/range {v1 .. v7}, LX/BS1;->a(IIIIZZ)V

    goto :goto_0
.end method
