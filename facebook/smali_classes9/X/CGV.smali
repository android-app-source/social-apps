.class public LX/CGV;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/CGV;


# instance fields
.field public final a:LX/1Kf;

.field public b:Ljava/lang/String;

.field public final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/1g8;


# direct methods
.method public constructor <init>(LX/1Kf;LX/0Or;LX/1g8;)V
    .locals 1
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Kf;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/1g8;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1865565
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1865566
    const/4 v0, 0x0

    iput-object v0, p0, LX/CGV;->b:Ljava/lang/String;

    .line 1865567
    iput-object p1, p0, LX/CGV;->a:LX/1Kf;

    .line 1865568
    iput-object p2, p0, LX/CGV;->c:LX/0Or;

    .line 1865569
    iput-object p3, p0, LX/CGV;->d:LX/1g8;

    .line 1865570
    return-void
.end method

.method public static a(LX/0QB;)LX/CGV;
    .locals 6

    .prologue
    .line 1865571
    sget-object v0, LX/CGV;->e:LX/CGV;

    if-nez v0, :cond_1

    .line 1865572
    const-class v1, LX/CGV;

    monitor-enter v1

    .line 1865573
    :try_start_0
    sget-object v0, LX/CGV;->e:LX/CGV;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1865574
    if-eqz v2, :cond_0

    .line 1865575
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1865576
    new-instance v5, LX/CGV;

    invoke-static {v0}, LX/1Ke;->a(LX/0QB;)LX/1Ke;

    move-result-object v3

    check-cast v3, LX/1Kf;

    const/16 v4, 0x15e7

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/1g8;->a(LX/0QB;)LX/1g8;

    move-result-object v4

    check-cast v4, LX/1g8;

    invoke-direct {v5, v3, p0, v4}, LX/CGV;-><init>(LX/1Kf;LX/0Or;LX/1g8;)V

    .line 1865577
    move-object v0, v5

    .line 1865578
    sput-object v0, LX/CGV;->e:LX/CGV;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1865579
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1865580
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1865581
    :cond_1
    sget-object v0, LX/CGV;->e:LX/CGV;

    return-object v0

    .line 1865582
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1865583
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
