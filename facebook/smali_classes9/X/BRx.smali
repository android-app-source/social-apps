.class public LX/BRx;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1784561
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1784562
    return-void
.end method

.method private static a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)I
    .locals 3
    .annotation build Landroid/support/annotation/StringRes;
    .end annotation

    .prologue
    .line 1784563
    sget-object v0, LX/BRw;->a:[I

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1784564
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected friendship status: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1784565
    :pswitch_0
    const v0, 0x7f08153a

    .line 1784566
    :goto_0
    return v0

    .line 1784567
    :pswitch_1
    const v0, 0x7f080f7b

    goto :goto_0

    .line 1784568
    :pswitch_2
    const v0, 0x7f080f7b

    goto :goto_0

    .line 1784569
    :pswitch_3
    const v0, 0x7f080f7f

    goto :goto_0

    .line 1784570
    :pswitch_4
    const v0, 0x7f08153d

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static a(LX/5wN;LX/03R;LX/0ad;LX/BS1;Z)V
    .locals 9

    .prologue
    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 1784571
    invoke-interface {p0}, LX/5wN;->cd_()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    .line 1784572
    if-eqz v0, :cond_0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne v0, v2, :cond_1

    .line 1784573
    :cond_0
    :goto_0
    return-void

    .line 1784574
    :cond_1
    if-nez p4, :cond_2

    sget-short v2, LX/0wf;->L:S

    invoke-interface {p2, v2, v1}, LX/0ad;->a(SZ)Z

    move-result v2

    if-eqz v2, :cond_2

    sget-short v2, LX/0wf;->K:S

    invoke-interface {p2, v2, v1}, LX/0ad;->a(SZ)Z

    move-result v2

    if-nez v2, :cond_2

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-eq v0, v2, :cond_0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->OUTGOING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-eq v0, v2, :cond_0

    .line 1784575
    :cond_2
    invoke-static {v0}, LX/BRx;->a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)I

    move-result v2

    .line 1784576
    invoke-static {v0}, LX/BRx;->b(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)I

    move-result v3

    .line 1784577
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CANNOT_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-eq v0, v4, :cond_6

    const/4 v4, 0x1

    :goto_1
    move v5, v4

    .line 1784578
    invoke-virtual {p1, v1}, LX/03R;->asBoolean(Z)Z

    move-result v4

    if-nez v4, :cond_4

    if-eqz v5, :cond_4

    move v6, v7

    .line 1784579
    :goto_2
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ARE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-eq v0, v4, :cond_3

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->OUTGOING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne v0, v4, :cond_5

    :cond_3
    move v8, v7

    .line 1784580
    :goto_3
    const/4 v4, 0x2

    move-object v0, p3

    invoke-interface/range {v0 .. v8}, LX/BS1;->a(IIIIZZZZ)V

    goto :goto_0

    :cond_4
    move v6, v1

    .line 1784581
    goto :goto_2

    :cond_5
    move v8, v1

    .line 1784582
    goto :goto_3

    :cond_6
    const/4 v4, 0x0

    goto :goto_1
.end method

.method private static b(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)I
    .locals 3
    .annotation build Landroid/support/annotation/DrawableRes;
    .end annotation

    .prologue
    .line 1784583
    sget-object v0, LX/BRw;->a:[I

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1784584
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected friendship status: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1784585
    :pswitch_0
    const v0, 0x7f020889

    .line 1784586
    :goto_0
    return v0

    .line 1784587
    :pswitch_1
    const v0, 0x7f020886

    goto :goto_0

    .line 1784588
    :pswitch_2
    const v0, 0x7f02089a

    goto :goto_0

    .line 1784589
    :pswitch_3
    const v0, 0x7f020889

    goto :goto_0

    .line 1784590
    :pswitch_4
    const v0, 0x7f0208a0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
