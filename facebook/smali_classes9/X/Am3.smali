.class public final enum LX/Am3;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Am3;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Am3;

.field public static final enum ANIMATING_TO_LOADED_TEXT:LX/Am3;

.field public static final enum ANIMATING_TO_LOADING_TEXT:LX/Am3;

.field public static final enum ANIMATING_TO_SPINNER:LX/Am3;

.field public static final enum HIDDEN:LX/Am3;

.field public static final enum SHOWING_LOADED_TEXT:LX/Am3;

.field public static final enum SHOWING_LOADING_TEXT:LX/Am3;

.field public static final enum SHOWING_SPINNER:LX/Am3;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1710433
    new-instance v0, LX/Am3;

    const-string v1, "SHOWING_SPINNER"

    invoke-direct {v0, v1, v3}, LX/Am3;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Am3;->SHOWING_SPINNER:LX/Am3;

    .line 1710434
    new-instance v0, LX/Am3;

    const-string v1, "SHOWING_LOADING_TEXT"

    invoke-direct {v0, v1, v4}, LX/Am3;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Am3;->SHOWING_LOADING_TEXT:LX/Am3;

    .line 1710435
    new-instance v0, LX/Am3;

    const-string v1, "SHOWING_LOADED_TEXT"

    invoke-direct {v0, v1, v5}, LX/Am3;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Am3;->SHOWING_LOADED_TEXT:LX/Am3;

    .line 1710436
    new-instance v0, LX/Am3;

    const-string v1, "ANIMATING_TO_SPINNER"

    invoke-direct {v0, v1, v6}, LX/Am3;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Am3;->ANIMATING_TO_SPINNER:LX/Am3;

    .line 1710437
    new-instance v0, LX/Am3;

    const-string v1, "ANIMATING_TO_LOADING_TEXT"

    invoke-direct {v0, v1, v7}, LX/Am3;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Am3;->ANIMATING_TO_LOADING_TEXT:LX/Am3;

    .line 1710438
    new-instance v0, LX/Am3;

    const-string v1, "ANIMATING_TO_LOADED_TEXT"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/Am3;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Am3;->ANIMATING_TO_LOADED_TEXT:LX/Am3;

    .line 1710439
    new-instance v0, LX/Am3;

    const-string v1, "HIDDEN"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/Am3;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Am3;->HIDDEN:LX/Am3;

    .line 1710440
    const/4 v0, 0x7

    new-array v0, v0, [LX/Am3;

    sget-object v1, LX/Am3;->SHOWING_SPINNER:LX/Am3;

    aput-object v1, v0, v3

    sget-object v1, LX/Am3;->SHOWING_LOADING_TEXT:LX/Am3;

    aput-object v1, v0, v4

    sget-object v1, LX/Am3;->SHOWING_LOADED_TEXT:LX/Am3;

    aput-object v1, v0, v5

    sget-object v1, LX/Am3;->ANIMATING_TO_SPINNER:LX/Am3;

    aput-object v1, v0, v6

    sget-object v1, LX/Am3;->ANIMATING_TO_LOADING_TEXT:LX/Am3;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/Am3;->ANIMATING_TO_LOADED_TEXT:LX/Am3;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/Am3;->HIDDEN:LX/Am3;

    aput-object v2, v0, v1

    sput-object v0, LX/Am3;->$VALUES:[LX/Am3;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1710441
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Am3;
    .locals 1

    .prologue
    .line 1710432
    const-class v0, LX/Am3;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Am3;

    return-object v0
.end method

.method public static values()[LX/Am3;
    .locals 1

    .prologue
    .line 1710431
    sget-object v0, LX/Am3;->$VALUES:[LX/Am3;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Am3;

    return-object v0
.end method
