.class public LX/C5L;
.super LX/3mX;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<PageProp:",
        "Ljava/lang/Object;",
        "E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/3mX",
        "<TPageProp;TE;>;"
    }
.end annotation


# static fields
.field private static final c:Ljava/lang/String;


# instance fields
.field private final d:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

.field private final f:LX/1Pq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/AnA;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1848760
    const-class v0, LX/C5L;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/C5L;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;LX/0Px;LX/1Pq;LX/25M;LX/0Ot;LX/0Ot;)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/0Px;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/1Pq;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/25M;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;",
            "LX/0Px",
            "<TPageProp;>;TE;",
            "LX/25M;",
            "LX/0Ot",
            "<",
            "LX/AnA;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1848761
    invoke-direct {p0, p1, p4, p5, p6}, LX/3mX;-><init>(Landroid/content/Context;LX/0Px;LX/1Pq;LX/25M;)V

    .line 1848762
    iput-object p2, p0, LX/C5L;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1848763
    iput-object p3, p0, LX/C5L;->e:Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    .line 1848764
    iput-object p5, p0, LX/C5L;->f:LX/1Pq;

    .line 1848765
    iput-object p7, p0, LX/C5L;->g:LX/0Ot;

    .line 1848766
    iput-object p8, p0, LX/C5L;->h:LX/0Ot;

    .line 1848767
    return-void
.end method


# virtual methods
.method public final a(LX/1De;)LX/1X1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1848768
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/1De;Ljava/lang/Object;)LX/1X1;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "TPageProp;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1848769
    iget-object v0, p0, LX/C5L;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AnA;

    iget-object v1, p0, LX/C5L;->e:Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/AnA;->a(Ljava/lang/Class;)LX/An9;

    move-result-object v0

    .line 1848770
    if-nez v0, :cond_0

    .line 1848771
    iget-object v0, p0, LX/C5L;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v1, LX/C5L;->c:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Missing controller for feedunit type : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/C5L;->e:Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1848772
    const/4 v0, 0x0

    .line 1848773
    :goto_0
    return-object v0

    :cond_0
    iget-object v2, p0, LX/C5L;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1848774
    iget-object v1, p0, LX/3mX;->l:LX/3mj;

    move-object v3, v1

    .line 1848775
    iget-object v5, p0, LX/C5L;->f:LX/1Pq;

    move-object v1, p1

    move-object v4, p2

    invoke-virtual/range {v0 .. v5}, LX/An9;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/3mj;Ljava/lang/Object;Ljava/lang/Object;)LX/1X1;

    move-result-object v0

    goto :goto_0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 1848776
    const/4 v0, 0x0

    return v0
.end method
