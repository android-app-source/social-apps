.class public final LX/Bz1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Aj7;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/Aj7",
        "<",
        "LX/26M;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0Px;

.field public final synthetic b:I

.field public final synthetic c:[LX/1bf;

.field public final synthetic d:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic e:LX/1Po;

.field public final synthetic f:Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;LX/0Px;I[LX/1bf;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Po;)V
    .locals 0

    .prologue
    .line 1838036
    iput-object p1, p0, LX/Bz1;->f:Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;

    iput-object p2, p0, LX/Bz1;->a:LX/0Px;

    iput p3, p0, LX/Bz1;->b:I

    iput-object p4, p0, LX/Bz1;->c:[LX/1bf;

    iput-object p5, p0, LX/Bz1;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    iput-object p6, p0, LX/Bz1;->e:LX/1Po;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/feed/collage/ui/CollageAttachmentView;LX/26N;I)V
    .locals 7

    .prologue
    .line 1838037
    check-cast p2, LX/26M;

    .line 1838038
    iget-object v0, p0, LX/Bz1;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    iget v1, p0, LX/Bz1;->b:I

    invoke-static {p3, p2, v0, v1}, LX/26D;->a(ILX/26M;II)Z

    move-result v0

    .line 1838039
    if-eqz v0, :cond_0

    .line 1838040
    iget-object v0, p0, LX/Bz1;->c:[LX/1bf;

    aget-object v4, v0, p3

    .line 1838041
    iget-object v0, p0, LX/Bz1;->f:Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/26D;

    invoke-virtual {p1}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, LX/Bz1;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {p1, v4}, LX/Bz5;->a(Lcom/facebook/feed/collage/ui/CollageAttachmentView;LX/1bf;)LX/9hN;

    move-result-object v5

    move v3, p3

    invoke-virtual/range {v0 .. v5}, LX/26D;->a(Landroid/content/Context;Lcom/facebook/feed/rows/core/props/FeedProps;ILX/1bf;LX/9hN;)V

    .line 1838042
    :goto_0
    return-void

    .line 1838043
    :cond_0
    iget-object v0, p0, LX/Bz1;->f:Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/26D;

    iget-object v2, p0, LX/Bz1;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v5, p0, LX/Bz1;->e:LX/1Po;

    const/4 v6, 0x0

    move-object v1, p1

    move v3, p3

    move-object v4, p2

    invoke-virtual/range {v0 .. v6}, LX/26D;->a(Landroid/view/View;Lcom/facebook/feed/rows/core/props/FeedProps;ILX/26M;LX/1Po;LX/1aZ;)V

    goto :goto_0
.end method
