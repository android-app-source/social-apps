.class public final LX/Aax;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$FetchCreativeToolsQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 1689462
    const-class v1, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$FetchCreativeToolsQueryModel;

    const v0, -0x6542266e

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "FetchCreativeToolsQuery"

    const-string v6, "b873444b5195ab51f262d2477b50c455"

    const-string v7, "video"

    const-string v8, "10155183291236729"

    const-string v9, "10155259087321729"

    .line 1689463
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 1689464
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 1689465
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1689450
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1689451
    sparse-switch v0, :sswitch_data_0

    .line 1689452
    :goto_0
    return-object p1

    .line 1689453
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 1689454
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 1689455
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 1689456
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 1689457
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 1689458
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    .line 1689459
    :sswitch_6
    const-string p1, "6"

    goto :goto_0

    .line 1689460
    :sswitch_7
    const-string p1, "7"

    goto :goto_0

    .line 1689461
    :sswitch_8
    const-string p1, "8"

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x6c782e51 -> :sswitch_6
        -0x50800eb5 -> :sswitch_5
        -0x402eedd7 -> :sswitch_8
        -0x8ca6426 -> :sswitch_3
        0x31c0900 -> :sswitch_7
        0x6234bbb -> :sswitch_1
        0x1afcead6 -> :sswitch_0
        0x7c305c37 -> :sswitch_4
        0x7c6b80b3 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 1689445
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    packed-switch v1, :pswitch_data_1

    .line 1689446
    :goto_1
    return v0

    .line 1689447
    :pswitch_1
    const-string v3, "5"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v1, v0

    goto :goto_0

    :pswitch_2
    const-string v3, "7"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v1, v2

    goto :goto_0

    .line 1689448
    :pswitch_3
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 1689449
    :pswitch_4
    const-string v0, "%s"

    invoke-static {p2, v2, v0}, LX/0wE;->a(Ljava/lang/Object;ILjava/lang/String;)Z

    move-result v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x35
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
