.class public final LX/C0w;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/1w9;

.field public final b:Lcom/facebook/components/feed/FeedComponentView;

.field public final c:LX/C0x;

.field public final d:I

.field public final e:I

.field public final f:I

.field public g:LX/0hs;

.field public h:I

.field public i:I

.field private j:I

.field private k:I


# direct methods
.method public constructor <init>(LX/1w9;Lcom/facebook/components/feed/FeedComponentView;III)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1841636
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1841637
    const/4 v0, 0x0

    iput-object v0, p0, LX/C0w;->g:LX/0hs;

    .line 1841638
    iput v1, p0, LX/C0w;->j:I

    .line 1841639
    iput v1, p0, LX/C0w;->k:I

    .line 1841640
    iput-object p1, p0, LX/C0w;->a:LX/1w9;

    .line 1841641
    iput-object p2, p0, LX/C0w;->b:Lcom/facebook/components/feed/FeedComponentView;

    .line 1841642
    iput p3, p0, LX/C0w;->d:I

    .line 1841643
    iput p4, p0, LX/C0w;->e:I

    .line 1841644
    iput p5, p0, LX/C0w;->f:I

    .line 1841645
    new-instance v0, LX/C0x;

    invoke-direct {v0, p0}, LX/C0x;-><init>(LX/C0w;)V

    iput-object v0, p0, LX/C0w;->c:LX/C0x;

    .line 1841646
    return-void
.end method

.method public static a(LX/C0w;Landroid/view/View;I)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1841647
    const/4 v2, 0x2

    new-array v2, v2, [I

    fill-array-data v2, :array_0

    .line 1841648
    invoke-virtual {p1, v2}, Landroid/view/View;->getLocationInWindow([I)V

    .line 1841649
    iget v3, p0, LX/C0w;->j:I

    aget v4, v2, v0

    sub-int/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    if-ge v3, p2, :cond_0

    iget v3, p0, LX/C0w;->k:I

    aget v4, v2, v1

    sub-int/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    if-lt v3, p2, :cond_1

    .line 1841650
    :cond_0
    aget v0, v2, v0

    iput v0, p0, LX/C0w;->j:I

    .line 1841651
    aget v0, v2, v1

    iput v0, p0, LX/C0w;->k:I

    move v0, v1

    .line 1841652
    :cond_1
    return v0

    nop

    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method

.method public static d(LX/C0w;Lcom/facebook/components/feed/FeedComponentView;)V
    .locals 4

    .prologue
    const/4 v2, 0x2

    .line 1841653
    iget-object v0, p0, LX/C0w;->a:LX/1w9;

    invoke-virtual {v0}, LX/1w9;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1841654
    invoke-virtual {p1}, Lcom/facebook/components/feed/FeedComponentView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1841655
    new-instance v1, LX/0hs;

    const/4 v3, 0x2

    invoke-direct {v1, v0, v3}, LX/0hs;-><init>(Landroid/content/Context;I)V

    .line 1841656
    const/4 v3, -0x1

    .line 1841657
    iput v3, v1, LX/0hs;->t:I

    .line 1841658
    const v3, 0x7f0828a7

    invoke-virtual {v1, v3}, LX/0hs;->b(I)V

    .line 1841659
    sget-object v3, LX/3AV;->ABOVE:LX/3AV;

    invoke-virtual {v1, v3}, LX/0ht;->a(LX/3AV;)V

    .line 1841660
    move-object v0, v1

    .line 1841661
    iput-object v0, p0, LX/C0w;->g:LX/0hs;

    .line 1841662
    iget-object v0, p0, LX/C0w;->g:LX/0hs;

    invoke-virtual {p1, v0}, Lcom/facebook/components/feed/FeedComponentView;->a(LX/0hs;)V

    .line 1841663
    new-array v0, v2, [I

    fill-array-data v0, :array_0

    .line 1841664
    new-array v1, v2, [I

    fill-array-data v1, :array_1

    .line 1841665
    invoke-virtual {p1, v0}, Lcom/facebook/components/feed/FeedComponentView;->getLocationInWindow([I)V

    .line 1841666
    invoke-virtual {p1, v1}, Lcom/facebook/components/feed/FeedComponentView;->getLocationOnScreen([I)V

    .line 1841667
    iget-object v0, p0, LX/C0w;->c:LX/C0x;

    invoke-virtual {v0, v2}, LX/C0x;->sendEmptyMessage(I)Z

    .line 1841668
    :goto_0
    return-void

    .line 1841669
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/C0w;->g:LX/0hs;

    goto :goto_0

    nop

    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data

    .line 1841670
    :array_1
    .array-data 4
        0x0
        0x0
    .end array-data
.end method
