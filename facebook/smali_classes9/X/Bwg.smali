.class public final LX/Bwg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0ib;


# instance fields
.field public final synthetic a:Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;)V
    .locals 0

    .prologue
    .line 1834180
    iput-object p1, p0, LX/Bwg;->a:Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/04g;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1834182
    iget-object v0, p0, LX/Bwg;->a:Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;

    iget-object v0, v0, LX/3Gb;->n:LX/7Lf;

    instance-of v0, v0, LX/BwV;

    if-nez v0, :cond_0

    iget-object v0, p0, LX/Bwg;->a:Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;

    iget-boolean v0, v0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->H:Z

    if-nez v0, :cond_1

    .line 1834183
    :cond_0
    :goto_0
    return-void

    .line 1834184
    :cond_1
    iget-object v0, p0, LX/Bwg;->a:Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;

    iget-object v0, v0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->v:LX/0iX;

    .line 1834185
    iget-boolean p1, v0, LX/0iX;->i:Z

    move v0, p1

    .line 1834186
    if-eqz v0, :cond_2

    iget-object v0, p0, LX/Bwg;->a:Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;

    iget-boolean v0, v0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->I:Z

    if-eqz v0, :cond_2

    .line 1834187
    iget-object v0, p0, LX/Bwg;->a:Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;

    .line 1834188
    iput-boolean v1, v0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->I:Z

    .line 1834189
    iget-object v0, p0, LX/Bwg;->a:Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;

    iget-object v0, v0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->e:LX/3Hc;

    invoke-virtual {v0}, LX/3Hc;->a()V

    .line 1834190
    iget-object v0, p0, LX/Bwg;->a:Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;

    iget-object v0, v0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->e:LX/3Hc;

    .line 1834191
    iput-boolean v1, v0, LX/3Hc;->r:Z

    .line 1834192
    iget-object v0, p0, LX/Bwg;->a:Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;

    iget-object v0, v0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->e:LX/3Hc;

    iget-object v1, p0, LX/Bwg;->a:Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;

    iget-object v1, v1, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->x:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/3Hc;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 1834193
    :cond_2
    iget-object v0, p0, LX/Bwg;->a:Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;

    iget-boolean v0, v0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->I:Z

    if-nez v0, :cond_0

    .line 1834194
    iget-object v0, p0, LX/Bwg;->a:Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;

    .line 1834195
    iput-boolean v2, v0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->I:Z

    .line 1834196
    iget-object v0, p0, LX/Bwg;->a:Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;

    iget-object v0, v0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->e:LX/3Hc;

    .line 1834197
    iput-boolean v2, v0, LX/3Hc;->r:Z

    .line 1834198
    iget-object v0, p0, LX/Bwg;->a:Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;

    iget-object v0, v0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->e:LX/3Hc;

    iget-object v1, p0, LX/Bwg;->a:Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;

    iget-object v1, v1, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->x:Ljava/lang/String;

    .line 1834199
    new-instance v2, LX/4Gm;

    invoke-direct {v2}, LX/4Gm;-><init>()V

    .line 1834200
    const-string p0, "video_id"

    invoke-virtual {v2, p0, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1834201
    move-object v2, v2

    .line 1834202
    new-instance p0, LX/6TU;

    invoke-direct {p0}, LX/6TU;-><init>()V

    move-object p0, p0

    .line 1834203
    const-string p1, "input"

    invoke-virtual {p0, p1, v2}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1834204
    iget-object v2, v0, LX/3Hc;->e:LX/0tX;

    invoke-static {p0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object p0

    invoke-virtual {v2, p0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1834205
    goto :goto_0
.end method

.method public final g()V
    .locals 0

    .prologue
    .line 1834181
    return-void
.end method
