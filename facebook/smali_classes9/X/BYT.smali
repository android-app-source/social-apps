.class public final LX/BYT;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15i;ILX/0nX;)V
    .locals 2

    .prologue
    .line 1796055
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1796056
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1796057
    if-eqz v0, :cond_0

    .line 1796058
    const-string v1, "timezone"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1796059
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1796060
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1796061
    return-void
.end method

.method public static a$redex0(LX/15w;LX/186;)I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1796173
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_3

    .line 1796174
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1796175
    :goto_0
    return v1

    .line 1796176
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1796177
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v3, :cond_2

    .line 1796178
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 1796179
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1796180
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v3, v4, :cond_1

    if-eqz v2, :cond_1

    .line 1796181
    const-string v3, "feed_unit_preview"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1796182
    invoke-static {p0, p1}, LX/2aD;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 1796183
    :cond_2
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1796184
    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1796185
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public static a$redex0(LX/15w;)LX/15i;
    .locals 15

    .prologue
    .line 1796134
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1796135
    const/4 v5, 0x0

    .line 1796136
    const-wide/16 v10, 0x0

    .line 1796137
    const-wide/16 v8, 0x0

    .line 1796138
    const-wide/16 v6, 0x0

    .line 1796139
    const/4 v4, 0x0

    .line 1796140
    const/4 v3, 0x0

    .line 1796141
    const/4 v2, 0x0

    .line 1796142
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->START_OBJECT:LX/15z;

    if-eq v12, v13, :cond_9

    .line 1796143
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1796144
    const/4 v2, 0x0

    .line 1796145
    :goto_0
    move v1, v2

    .line 1796146
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1796147
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    return-object v0

    .line 1796148
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v6, :cond_5

    .line 1796149
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 1796150
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1796151
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_0

    if-eqz v2, :cond_0

    .line 1796152
    const-string v6, "distance_unit"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1796153
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v14, v2

    goto :goto_1

    .line 1796154
    :cond_1
    const-string v6, "latitude"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1796155
    const/4 v2, 0x1

    .line 1796156
    invoke-virtual {p0}, LX/15w;->G()D

    move-result-wide v4

    move v3, v2

    goto :goto_1

    .line 1796157
    :cond_2
    const-string v6, "longitude"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1796158
    const/4 v2, 0x1

    .line 1796159
    invoke-virtual {p0}, LX/15w;->G()D

    move-result-wide v6

    move v9, v2

    move-wide v12, v6

    goto :goto_1

    .line 1796160
    :cond_3
    const-string v6, "radius"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1796161
    const/4 v2, 0x1

    .line 1796162
    invoke-virtual {p0}, LX/15w;->G()D

    move-result-wide v6

    move v8, v2

    move-wide v10, v6

    goto :goto_1

    .line 1796163
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1796164
    :cond_5
    const/4 v2, 0x4

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 1796165
    const/4 v2, 0x0

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 1796166
    if-eqz v3, :cond_6

    .line 1796167
    const/4 v3, 0x1

    const-wide/16 v6, 0x0

    move-object v2, v0

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1796168
    :cond_6
    if-eqz v9, :cond_7

    .line 1796169
    const/4 v3, 0x2

    const-wide/16 v6, 0x0

    move-object v2, v0

    move-wide v4, v12

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1796170
    :cond_7
    if-eqz v8, :cond_8

    .line 1796171
    const/4 v3, 0x3

    const-wide/16 v6, 0x0

    move-object v2, v0

    move-wide v4, v10

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1796172
    :cond_8
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_9
    move-wide v12, v8

    move v14, v5

    move v8, v2

    move v9, v3

    move v3, v4

    move-wide v4, v10

    move-wide v10, v6

    goto/16 :goto_1
.end method

.method public static a$redex0(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1796127
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1796128
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1796129
    if-eqz v0, :cond_0

    .line 1796130
    const-string v1, "feed_unit_preview"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1796131
    invoke-static {p0, v0, p2, p3}, LX/2aD;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1796132
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1796133
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 14

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1796081
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_5

    .line 1796082
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1796083
    :goto_0
    return v1

    .line 1796084
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_3

    .line 1796085
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 1796086
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1796087
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_0

    if-eqz v5, :cond_0

    .line 1796088
    const-string v6, "geo_score"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1796089
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v4, v0

    move v0, v2

    goto :goto_1

    .line 1796090
    :cond_1
    const-string v6, "node"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1796091
    const/4 v5, 0x0

    .line 1796092
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v6, :cond_e

    .line 1796093
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1796094
    :goto_2
    move v3, v5

    .line 1796095
    goto :goto_1

    .line 1796096
    :cond_2
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1796097
    :cond_3
    const/4 v5, 0x2

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 1796098
    if-eqz v0, :cond_4

    .line 1796099
    invoke-virtual {p1, v1, v4, v1}, LX/186;->a(III)V

    .line 1796100
    :cond_4
    invoke-virtual {p1, v2, v3}, LX/186;->b(II)V

    .line 1796101
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v3, v1

    move v4, v1

    goto :goto_1

    .line 1796102
    :cond_6
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1796103
    :cond_7
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_d

    .line 1796104
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 1796105
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1796106
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_7

    if-eqz v11, :cond_7

    .line 1796107
    const-string v12, "address"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_8

    .line 1796108
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p1, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto :goto_3

    .line 1796109
    :cond_8
    const-string v12, "city"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_9

    .line 1796110
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_3

    .line 1796111
    :cond_9
    const-string v12, "country"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_a

    .line 1796112
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_3

    .line 1796113
    :cond_a
    const-string v12, "latitude"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_b

    .line 1796114
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_3

    .line 1796115
    :cond_b
    const-string v12, "longitude"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_c

    .line 1796116
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_3

    .line 1796117
    :cond_c
    const-string v12, "postal_code"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 1796118
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_3

    .line 1796119
    :cond_d
    const/4 v11, 0x6

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 1796120
    invoke-virtual {p1, v5, v10}, LX/186;->b(II)V

    .line 1796121
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v9}, LX/186;->b(II)V

    .line 1796122
    const/4 v5, 0x2

    invoke-virtual {p1, v5, v8}, LX/186;->b(II)V

    .line 1796123
    const/4 v5, 0x3

    invoke-virtual {p1, v5, v7}, LX/186;->b(II)V

    .line 1796124
    const/4 v5, 0x4

    invoke-virtual {p1, v5, v6}, LX/186;->b(II)V

    .line 1796125
    const/4 v5, 0x5

    invoke-virtual {p1, v5, v3}, LX/186;->b(II)V

    .line 1796126
    invoke-virtual {p1}, LX/186;->d()I

    move-result v5

    goto/16 :goto_2

    :cond_e
    move v3, v5

    move v6, v5

    move v7, v5

    move v8, v5

    move v9, v5

    move v10, v5

    goto/16 :goto_3
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1796062
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1796063
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 1796064
    if-eqz v0, :cond_0

    .line 1796065
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1796066
    invoke-static {p0, p1, v1, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1796067
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1796068
    if-eqz v0, :cond_1

    .line 1796069
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1796070
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1796071
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1796072
    if-eqz v0, :cond_2

    .line 1796073
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1796074
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1796075
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1796076
    if-eqz v0, :cond_3

    .line 1796077
    const-string v1, "targeting_sentences"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1796078
    invoke-static {p0, v0, p2, p3}, LX/AAX;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1796079
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1796080
    return-void
.end method
