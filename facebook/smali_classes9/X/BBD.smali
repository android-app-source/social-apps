.class public final LX/BBD;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/BB7;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1756554
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1756555
    new-instance v0, LX/BB7;

    invoke-direct {v0}, LX/BB7;-><init>()V

    iput-object v0, p0, LX/BBD;->a:LX/BB7;

    .line 1756556
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;)LX/BBD;
    .locals 2

    .prologue
    .line 1756527
    iget-object v1, p0, LX/BBD;->a:LX/BB7;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    .line 1756528
    :goto_0
    iput-object v0, v1, LX/BB7;->a:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;

    .line 1756529
    return-object p0

    .line 1756530
    :cond_0
    iget-object v0, p1, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;->b:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)LX/BBD;
    .locals 1

    .prologue
    .line 1756531
    iget-object v0, p0, LX/BBD;->a:LX/BB7;

    .line 1756532
    iput-object p1, v0, LX/BB7;->b:Ljava/lang/String;

    .line 1756533
    return-object p0
.end method

.method public final a()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsDeltaFieldsModel;
    .locals 10

    .prologue
    .line 1756534
    new-instance v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsDeltaFieldsModel;

    iget-object v1, p0, LX/BBD;->a:LX/BB7;

    const/4 v6, 0x1

    const/4 v9, 0x0

    const/4 v4, 0x0

    .line 1756535
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1756536
    iget-object v3, v1, LX/BB7;->a:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;

    invoke-static {v2, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1756537
    iget-object v5, v1, LX/BB7;->b:Ljava/lang/String;

    invoke-virtual {v2, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1756538
    iget-object v7, v1, LX/BB7;->c:Ljava/lang/String;

    invoke-virtual {v2, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1756539
    const/4 v8, 0x3

    invoke-virtual {v2, v8}, LX/186;->c(I)V

    .line 1756540
    invoke-virtual {v2, v9, v3}, LX/186;->b(II)V

    .line 1756541
    invoke-virtual {v2, v6, v5}, LX/186;->b(II)V

    .line 1756542
    const/4 v3, 0x2

    invoke-virtual {v2, v3, v7}, LX/186;->b(II)V

    .line 1756543
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1756544
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1756545
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1756546
    invoke-virtual {v3, v9}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1756547
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1756548
    new-instance v3, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsDeltaFieldsModel;

    invoke-direct {v3, v2}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsDeltaFieldsModel;-><init>(LX/15i;)V

    .line 1756549
    move-object v1, v3

    .line 1756550
    invoke-direct {v0, v1}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsDeltaFieldsModel;-><init>(Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsDeltaFieldsModel;)V

    return-object v0
.end method

.method public final b(Ljava/lang/String;)LX/BBD;
    .locals 1

    .prologue
    .line 1756551
    iget-object v0, p0, LX/BBD;->a:LX/BB7;

    .line 1756552
    iput-object p1, v0, LX/BB7;->c:Ljava/lang/String;

    .line 1756553
    return-object p0
.end method
