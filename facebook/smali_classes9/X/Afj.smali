.class public final LX/Afj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/Ag2;

.field public final synthetic b:LX/Afk;


# direct methods
.method public constructor <init>(LX/Afk;LX/Ag2;)V
    .locals 0

    .prologue
    .line 1699626
    iput-object p1, p0, LX/Afj;->b:LX/Afk;

    iput-object p2, p0, LX/Afj;->a:LX/Ag2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 11

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x2

    const v2, -0x20981f72

    invoke-static {v1, v0, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1699608
    iget-object v2, p0, LX/Afj;->a:LX/Ag2;

    iget-object v3, p0, LX/Afj;->a:LX/Ag2;

    iget-boolean v3, v3, LX/Ag2;->d:Z

    if-nez v3, :cond_1

    :goto_0
    iput-boolean v0, v2, LX/Ag2;->d:Z

    .line 1699609
    iget-object v0, p0, LX/Afj;->b:LX/Afk;

    iget-object v2, p0, LX/Afj;->a:LX/Ag2;

    iget-boolean v2, v2, LX/Ag2;->d:Z

    const/4 p1, 0x1

    .line 1699610
    if-eqz v2, :cond_2

    .line 1699611
    iget-object v3, v0, LX/Afk;->w:Lcom/facebook/widget/springbutton/SpringScaleButton;

    sget-object v4, LX/Afk;->n:[I

    invoke-virtual {v3, v4, p1}, Lcom/facebook/widget/springbutton/SpringScaleButton;->setImageState([IZ)V

    .line 1699612
    iget-object v3, v0, LX/Afk;->w:Lcom/facebook/widget/springbutton/SpringScaleButton;

    iget-object v4, v0, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const p1, 0x7f080c2d

    invoke-virtual {v4, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/widget/springbutton/SpringScaleButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1699613
    :goto_1
    iget-object v0, p0, LX/Afj;->b:LX/Afk;

    iget-object v0, v0, LX/Afk;->t:LX/Acf;

    iget-object v2, p0, LX/Afj;->a:LX/Ag2;

    iget-object v2, v2, LX/Ag2;->e:Ljava/lang/String;

    iget-object v3, p0, LX/Afj;->a:LX/Ag2;

    iget-object v3, v3, LX/AeP;->a:LX/AcC;

    iget-object v3, v3, LX/AcC;->b:Ljava/lang/String;

    iget-object v4, p0, LX/Afj;->a:LX/Ag2;

    iget-boolean v4, v4, LX/Ag2;->d:Z

    .line 1699614
    if-eqz v4, :cond_0

    .line 1699615
    new-instance v5, Lcom/facebook/facecastdisplay/protocol/MutateLiveWatchLikeMethod$Params;

    invoke-direct {v5, v2, v3}, Lcom/facebook/facecastdisplay/protocol/MutateLiveWatchLikeMethod$Params;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1699616
    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    .line 1699617
    const-string v6, "LiveVideoWatchLikeParamKey"

    invoke-virtual {v7, v6, v5}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1699618
    iget-object v5, v0, LX/Acf;->a:LX/0aG;

    const-string v6, "live_video_watch_like"

    sget-object v8, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    const/4 v9, 0x0

    const v10, -0xa4208b6

    invoke-static/range {v5 .. v10}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v5

    invoke-interface {v5}, LX/1MF;->start()LX/1ML;

    .line 1699619
    :cond_0
    iget-object v5, v0, LX/Acf;->b:LX/AVU;

    invoke-static {v4}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v6

    .line 1699620
    const-string v7, "facecast_watch_like_impression"

    invoke-static {v5, v7}, LX/AVU;->d(LX/AVU;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string v8, "watcher_id"

    invoke-virtual {v7, v8, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string v8, "watched_video_id"

    invoke-virtual {v7, v8, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string v8, "is_liked"

    invoke-virtual {v7, v8, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    .line 1699621
    iget-object v8, v5, LX/AVU;->b:LX/0Zb;

    invoke-interface {v8, v7}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1699622
    const v0, -0x1118df0

    invoke-static {v0, v1}, LX/02F;->a(II)V

    return-void

    .line 1699623
    :cond_1
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 1699624
    :cond_2
    iget-object v3, v0, LX/Afk;->w:Lcom/facebook/widget/springbutton/SpringScaleButton;

    sget-object v4, LX/Afk;->o:[I

    invoke-virtual {v3, v4, p1}, Lcom/facebook/widget/springbutton/SpringScaleButton;->setImageState([IZ)V

    .line 1699625
    iget-object v3, v0, LX/Afk;->w:Lcom/facebook/widget/springbutton/SpringScaleButton;

    iget-object v4, v0, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const p1, 0x7f080c2e

    invoke-virtual {v4, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/widget/springbutton/SpringScaleButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method
