.class public LX/CbN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7Gn;


# instance fields
.field public final a:Z

.field public final b:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Landroid/graphics/PointF;

.field private final d:Landroid/graphics/PointF;

.field public e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/tagging/model/TaggingProfile;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/util/List;Landroid/graphics/PointF;Landroid/graphics/PointF;Z)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/tagging/model/TaggingProfile;",
            ">;",
            "Landroid/graphics/PointF;",
            "Landroid/graphics/PointF;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 1919488
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1919489
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1919490
    invoke-static {p4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1919491
    invoke-static {p1}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/CbN;->b:LX/0am;

    .line 1919492
    iput-object p3, p0, LX/CbN;->c:Landroid/graphics/PointF;

    .line 1919493
    iput-object p4, p0, LX/CbN;->d:Landroid/graphics/PointF;

    .line 1919494
    iput-boolean p5, p0, LX/CbN;->a:Z

    .line 1919495
    invoke-static {p2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/CbN;->e:LX/0Px;

    .line 1919496
    return-void
.end method


# virtual methods
.method public final c()Landroid/graphics/PointF;
    .locals 3

    .prologue
    .line 1919487
    new-instance v0, Landroid/graphics/PointF;

    iget-object v1, p0, LX/CbN;->d:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    iget-object v2, p0, LX/CbN;->d:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    return-object v0
.end method

.method public final setTagSuggestions(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/tagging/model/TaggingProfile;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1919485
    invoke-static {p1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/CbN;->e:LX/0Px;

    .line 1919486
    return-void
.end method
