.class public final LX/C13;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/1xG;

.field public final synthetic b:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic c:LX/7Dj;

.field public final synthetic d:LX/0Ot;

.field public final synthetic e:LX/1bf;


# direct methods
.method public constructor <init>(LX/1xG;Lcom/facebook/feed/rows/core/props/FeedProps;LX/7Dj;LX/0Ot;LX/1bf;)V
    .locals 0

    .prologue
    .line 1841865
    iput-object p1, p0, LX/C13;->a:LX/1xG;

    iput-object p2, p0, LX/C13;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iput-object p3, p0, LX/C13;->c:LX/7Dj;

    iput-object p4, p0, LX/C13;->d:LX/0Ot;

    iput-object p5, p0, LX/C13;->e:LX/1bf;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v4, 0x1

    const/4 v8, 0x2

    const v0, 0x1849687d

    invoke-static {v8, v4, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v7

    move-object v2, p1

    .line 1841866
    check-cast v2, LX/8xF;

    .line 1841867
    iget-object v1, p0, LX/C13;->a:LX/1xG;

    iget-object v0, p0, LX/C13;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1841868
    iget-object v3, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v3

    .line 1841869
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, LX/C13;->c:LX/7Dj;

    .line 1841870
    new-instance v5, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v6, "spherical_photo_tap_feed_to_fullscreen"

    invoke-direct {v5, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1841871
    invoke-static {v1, v5, v0, v3}, LX/1xG;->a(LX/1xG;Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;LX/7Dj;)V

    .line 1841872
    const/4 v0, 0x0

    .line 1841873
    iput-boolean v0, v2, LX/8wv;->w:Z

    .line 1841874
    iput-boolean v0, v2, LX/8wv;->v:Z

    .line 1841875
    invoke-static {v2}, LX/8wv;->r(LX/8wv;)V

    .line 1841876
    invoke-virtual {v2}, LX/8wv;->m()V

    .line 1841877
    iget-object v0, v2, LX/8wv;->n:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    invoke-virtual {v0}, LX/2qW;->e()V

    .line 1841878
    iput-boolean v4, v2, LX/8xF;->A:Z

    .line 1841879
    iget-object v0, v2, LX/8wv;->o:Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;

    invoke-virtual {v0}, Lcom/facebook/spherical/photo/ui/SphericalPhotoIndicatorPlugin;->m()V

    .line 1841880
    iget-object v0, p0, LX/C13;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BaD;

    iget-object v1, p0, LX/C13;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p0, LX/C13;->e:LX/1bf;

    const/4 v4, 0x0

    iget-object v5, p0, LX/C13;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1841881
    iget-object v6, v5, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v5, v6

    .line 1841882
    check-cast v5, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->hashCode()I

    move-result v5

    new-instance v6, LX/C12;

    invoke-direct {v6, p0, v2}, LX/C12;-><init>(LX/C13;LX/8xF;)V

    invoke-virtual/range {v0 .. v6}, LX/BaD;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;LX/1bf;ZILandroid/content/DialogInterface$OnDismissListener;)V

    .line 1841883
    const v0, -0x53c5bfbf

    invoke-static {v8, v8, v0, v7}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
