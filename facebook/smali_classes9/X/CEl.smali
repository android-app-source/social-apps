.class public interface abstract LX/CEl;
.super Ljava/lang/Object;
.source ""


# virtual methods
.method public abstract a()LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Enum;",
            ":",
            "LX/CEj;",
            ">()",
            "LX/0Rf",
            "<TT;>;"
        }
    .end annotation
.end method

.method public abstract a(Landroid/content/Context;Lcom/facebook/greetingcards/verve/model/VMView;)Landroid/widget/ImageView;
.end method

.method public abstract a(Lcom/facebook/greetingcards/verve/model/VMView;)Ljava/lang/Enum;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Enum;",
            ":",
            "LX/CEj;",
            ">(",
            "Lcom/facebook/greetingcards/verve/model/VMView;",
            ")TT;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract a(Lcom/facebook/greetingcards/verve/model/VMView;LX/CEe;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/greetingcards/verve/model/VMView;",
            "LX/CEe;",
            ")V"
        }
    .end annotation
.end method

.method public abstract a(Lcom/facebook/greetingcards/verve/model/VMView;Lcom/facebook/greetingcards/verve/model/VMSlide;LX/0P1;Landroid/graphics/drawable/Drawable;Landroid/widget/ImageView;)V
    .param p4    # Landroid/graphics/drawable/Drawable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/greetingcards/verve/model/VMView;",
            "Lcom/facebook/greetingcards/verve/model/VMSlide;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "Landroid/graphics/drawable/Drawable;",
            "Landroid/widget/ImageView;",
            ")V"
        }
    .end annotation
.end method

.method public abstract b(Landroid/content/Context;Lcom/facebook/greetingcards/verve/model/VMView;)Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ":",
            "LX/CEe;",
            ">(",
            "Landroid/content/Context;",
            "Lcom/facebook/greetingcards/verve/model/VMView;",
            ")TT;"
        }
    .end annotation
.end method

.method public abstract c(Landroid/content/Context;Lcom/facebook/greetingcards/verve/model/VMView;)Landroid/widget/Button;
.end method
