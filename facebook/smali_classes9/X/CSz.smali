.class public LX/CSz;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0tX;

.field public final b:LX/1Ck;

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CXd;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0tX;LX/1Ck;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0tX;",
            "LX/1Ck;",
            "LX/0Ot",
            "<",
            "LX/CXd;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1895191
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1895192
    iput-object p1, p0, LX/CSz;->a:LX/0tX;

    .line 1895193
    iput-object p2, p0, LX/CSz;->b:LX/1Ck;

    .line 1895194
    iput-object p3, p0, LX/CSz;->c:LX/0Ot;

    .line 1895195
    return-void
.end method

.method public static a(LX/CSz;Ljava/lang/String;LX/CT8;)LX/0Vd;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/facebook/pages/common/platform/interfaces/PlatformInterfaces$DataFetch$ScreenModelListener;",
            ")",
            "LX/0Vd",
            "<",
            "Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1895190
    new-instance v0, LX/CSw;

    invoke-direct {v0, p0, p2, p1}, LX/CSw;-><init>(LX/CSz;LX/CT8;Ljava/lang/String;)V

    return-object v0
.end method

.method public static a(LX/0QB;)LX/CSz;
    .locals 4

    .prologue
    .line 1895177
    new-instance v2, LX/CSz;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v1

    check-cast v1, LX/1Ck;

    const/16 v3, 0x2b8d

    invoke-static {p0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-direct {v2, v0, v1, v3}, LX/CSz;-><init>(LX/0tX;LX/1Ck;LX/0Ot;)V

    .line 1895178
    move-object v0, v2

    .line 1895179
    return-object v0
.end method

.method public static a(LX/CSz;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 1895196
    const-string v3, "GET"

    const-string v6, "{}"

    const-string v7, "{}"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v5, v4

    move-object v8, v4

    move-object v9, v4

    move-object v10, p3

    invoke-static/range {v0 .. v10}, LX/CSz;->a(LX/CSz;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method private static a(LX/CSz;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .param p2    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/PagesPlatformScreenRequestType;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1895184
    iget-object v1, p0, LX/CSz;->a:LX/0tX;

    .line 1895185
    new-instance v0, LX/CUf;

    invoke-direct {v0}, LX/CUf;-><init>()V

    move-object v2, v0

    .line 1895186
    const-string v3, "query_params"

    invoke-static {p10}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, LX/4I5;

    invoke-direct {v0}, LX/4I5;-><init>()V

    invoke-virtual {v0, p1}, LX/4I5;->a(Ljava/lang/String;)LX/4I5;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/4I5;->b(Ljava/lang/String;)LX/4I5;

    move-result-object v0

    :goto_0
    invoke-virtual {v2, v3, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v0

    const-string v2, "request_type"

    invoke-virtual {v0, v2, p3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v2, "id"

    invoke-virtual {v0, v2, p4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v2, "component_flow_id"

    invoke-virtual {v0, v2, p5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v2, "form_fields"

    invoke-virtual {v0, v2, p6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v2, "optional_data"

    invoke-virtual {v0, v2, p7}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v2, "original_screen_id"

    invoke-virtual {v0, v2, p9}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v2, "screen_element_id"

    invoke-virtual {v0, v2, p8}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/CUf;

    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    sget-object v2, LX/0zS;->c:LX/0zS;

    invoke-virtual {v0, v2}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    invoke-static {v0}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0

    :cond_0
    new-instance v0, LX/4I5;

    invoke-direct {v0}, LX/4I5;-><init>()V

    invoke-virtual {v0, p1}, LX/4I5;->a(Ljava/lang/String;)LX/4I5;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/4I5;->b(Ljava/lang/String;)LX/4I5;

    move-result-object v0

    .line 1895187
    const-string p0, "referrer"

    invoke-virtual {v0, p0, p10}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1895188
    move-object v0, v0

    .line 1895189
    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1895182
    iget-object v0, p0, LX/CSz;->b:LX/1Ck;

    invoke-virtual {v0}, LX/1Ck;->c()V

    .line 1895183
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/CT8;Ljava/lang/String;)V
    .locals 14
    .param p5    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/PagesPlatformScreenRequestType;
        .end annotation
    .end param

    .prologue
    .line 1895180
    iget-object v12, p0, LX/CSz;->b:LX/1Ck;

    sget-object v1, LX/CSy;->FETCH_FIRST_SCREEN:LX/CSy;

    invoke-virtual {v1}, LX/CSy;->name()Ljava/lang/String;

    move-result-object v13

    const/4 v3, 0x0

    const-string v8, "{}"

    move-object v1, p0

    move-object v2, p1

    move-object/from16 v4, p5

    move-object/from16 v5, p2

    move-object/from16 v6, p4

    move-object/from16 v7, p6

    move-object/from16 v9, p7

    move-object/from16 v10, p3

    move-object/from16 v11, p9

    invoke-static/range {v1 .. v11}, LX/CSz;->a(LX/CSz;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    move-object/from16 v0, p8

    invoke-static {p0, p1, v0}, LX/CSz;->a(LX/CSz;Ljava/lang/String;LX/CT8;)LX/0Vd;

    move-result-object v2

    invoke-virtual {v12, v13, v1, v2}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1895181
    return-void
.end method
