.class public LX/Bra;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final g:LX/BrJ;

.field private static final h:LX/BrJ;

.field private static final i:LX/BrL;

.field private static final j:LX/BrL;

.field private static final k:LX/BrJ;

.field private static final l:LX/BrJ;

.field private static final m:LX/BrL;

.field private static final n:LX/BrL;

.field private static final o:LX/BrJ;

.field private static final p:LX/BrJ;

.field private static final q:LX/BrL;

.field private static final r:LX/BrL;

.field private static final s:LX/BrJ;

.field private static final t:LX/BrJ;

.field private static final u:LX/BrL;


# instance fields
.field public a:LX/BrJ;

.field public b:LX/BrJ;

.field public c:LX/BrL;

.field public d:LX/BrL;

.field public e:Z

.field public f:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1826346
    new-instance v0, LX/BrR;

    invoke-direct {v0}, LX/BrR;-><init>()V

    sput-object v0, LX/Bra;->g:LX/BrJ;

    .line 1826347
    new-instance v0, LX/BrS;

    invoke-direct {v0}, LX/BrS;-><init>()V

    sput-object v0, LX/Bra;->h:LX/BrJ;

    .line 1826348
    new-instance v0, LX/BrT;

    invoke-direct {v0}, LX/BrT;-><init>()V

    sput-object v0, LX/Bra;->i:LX/BrL;

    .line 1826349
    new-instance v0, LX/BrU;

    invoke-direct {v0}, LX/BrU;-><init>()V

    sput-object v0, LX/Bra;->j:LX/BrL;

    .line 1826350
    new-instance v0, LX/BrV;

    invoke-direct {v0}, LX/BrV;-><init>()V

    sput-object v0, LX/Bra;->k:LX/BrJ;

    .line 1826351
    new-instance v0, LX/BrW;

    invoke-direct {v0}, LX/BrW;-><init>()V

    sput-object v0, LX/Bra;->l:LX/BrJ;

    .line 1826352
    new-instance v0, LX/BrX;

    invoke-direct {v0}, LX/BrX;-><init>()V

    sput-object v0, LX/Bra;->m:LX/BrL;

    .line 1826353
    new-instance v0, LX/BrY;

    invoke-direct {v0}, LX/BrY;-><init>()V

    sput-object v0, LX/Bra;->n:LX/BrL;

    .line 1826354
    new-instance v0, LX/BrZ;

    invoke-direct {v0}, LX/BrZ;-><init>()V

    sput-object v0, LX/Bra;->o:LX/BrJ;

    .line 1826355
    new-instance v0, LX/BrK;

    invoke-direct {v0}, LX/BrK;-><init>()V

    sput-object v0, LX/Bra;->p:LX/BrJ;

    .line 1826356
    new-instance v0, LX/BrM;

    invoke-direct {v0}, LX/BrM;-><init>()V

    sput-object v0, LX/Bra;->q:LX/BrL;

    .line 1826357
    new-instance v0, LX/BrN;

    invoke-direct {v0}, LX/BrN;-><init>()V

    sput-object v0, LX/Bra;->r:LX/BrL;

    .line 1826358
    new-instance v0, LX/BrO;

    invoke-direct {v0}, LX/BrO;-><init>()V

    sput-object v0, LX/Bra;->s:LX/BrJ;

    .line 1826359
    new-instance v0, LX/BrP;

    invoke-direct {v0}, LX/BrP;-><init>()V

    sput-object v0, LX/Bra;->t:LX/BrJ;

    .line 1826360
    new-instance v0, LX/BrQ;

    invoke-direct {v0}, LX/BrQ;-><init>()V

    sput-object v0, LX/Bra;->u:LX/BrL;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/Boolean;)V
    .locals 3
    .param p1    # Ljava/lang/Integer;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1826361
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1826362
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/Bra;->e:Z

    .line 1826363
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, LX/Bra;->f:I

    .line 1826364
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1826365
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported number of photos: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1826366
    :pswitch_0
    sget-object v0, LX/Bra;->g:LX/BrJ;

    iput-object v0, p0, LX/Bra;->a:LX/BrJ;

    .line 1826367
    sget-object v0, LX/Bra;->h:LX/BrJ;

    iput-object v0, p0, LX/Bra;->b:LX/BrJ;

    .line 1826368
    sget-object v0, LX/Bra;->i:LX/BrL;

    iput-object v0, p0, LX/Bra;->c:LX/BrL;

    .line 1826369
    sget-object v0, LX/Bra;->j:LX/BrL;

    iput-object v0, p0, LX/Bra;->d:LX/BrL;

    .line 1826370
    :goto_0
    return-void

    .line 1826371
    :pswitch_1
    sget-object v0, LX/Bra;->k:LX/BrJ;

    iput-object v0, p0, LX/Bra;->a:LX/BrJ;

    .line 1826372
    sget-object v0, LX/Bra;->l:LX/BrJ;

    iput-object v0, p0, LX/Bra;->b:LX/BrJ;

    .line 1826373
    sget-object v0, LX/Bra;->m:LX/BrL;

    iput-object v0, p0, LX/Bra;->c:LX/BrL;

    .line 1826374
    sget-object v0, LX/Bra;->n:LX/BrL;

    iput-object v0, p0, LX/Bra;->d:LX/BrL;

    goto :goto_0

    .line 1826375
    :pswitch_2
    sget-object v0, LX/Bra;->o:LX/BrJ;

    iput-object v0, p0, LX/Bra;->a:LX/BrJ;

    .line 1826376
    sget-object v0, LX/Bra;->p:LX/BrJ;

    iput-object v0, p0, LX/Bra;->b:LX/BrJ;

    .line 1826377
    sget-object v0, LX/Bra;->q:LX/BrL;

    iput-object v0, p0, LX/Bra;->c:LX/BrL;

    .line 1826378
    sget-object v0, LX/Bra;->r:LX/BrL;

    iput-object v0, p0, LX/Bra;->d:LX/BrL;

    goto :goto_0

    .line 1826379
    :pswitch_3
    sget-object v0, LX/Bra;->s:LX/BrJ;

    iput-object v0, p0, LX/Bra;->a:LX/BrJ;

    .line 1826380
    sget-object v0, LX/Bra;->t:LX/BrJ;

    iput-object v0, p0, LX/Bra;->b:LX/BrJ;

    .line 1826381
    sget-object v0, LX/Bra;->u:LX/BrL;

    iput-object v0, p0, LX/Bra;->c:LX/BrL;

    .line 1826382
    sget-object v0, LX/Bra;->u:LX/BrL;

    iput-object v0, p0, LX/Bra;->d:LX/BrL;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
