.class public final LX/BE4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Rl;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Rl",
        "<",
        "Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsDeltaFieldsModel;

.field public final synthetic b:LX/1s3;


# direct methods
.method public constructor <init>(LX/1s3;Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsDeltaFieldsModel;)V
    .locals 0

    .prologue
    .line 1764063
    iput-object p1, p0, LX/BE4;->b:LX/1s3;

    iput-object p2, p0, LX/BE4;->a:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsDeltaFieldsModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1764064
    check-cast p1, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;

    .line 1764065
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/BE4;->a:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsDeltaFieldsModel;

    invoke-virtual {v1}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsDeltaFieldsModel;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
