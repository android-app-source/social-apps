.class public final enum LX/B1c;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/B1c;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/B1c;

.field public static final enum FETCH_GROUP_MEMBERS:LX/B1c;

.field public static final enum FETCH_MEMBERS_FOR_SECTION:LX/B1c;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1736109
    new-instance v0, LX/B1c;

    const-string v1, "FETCH_GROUP_MEMBERS"

    invoke-direct {v0, v1, v2}, LX/B1c;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/B1c;->FETCH_GROUP_MEMBERS:LX/B1c;

    .line 1736110
    new-instance v0, LX/B1c;

    const-string v1, "FETCH_MEMBERS_FOR_SECTION"

    invoke-direct {v0, v1, v3}, LX/B1c;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/B1c;->FETCH_MEMBERS_FOR_SECTION:LX/B1c;

    .line 1736111
    const/4 v0, 0x2

    new-array v0, v0, [LX/B1c;

    sget-object v1, LX/B1c;->FETCH_GROUP_MEMBERS:LX/B1c;

    aput-object v1, v0, v2

    sget-object v1, LX/B1c;->FETCH_MEMBERS_FOR_SECTION:LX/B1c;

    aput-object v1, v0, v3

    sput-object v0, LX/B1c;->$VALUES:[LX/B1c;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1736112
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/B1c;
    .locals 1

    .prologue
    .line 1736113
    const-class v0, LX/B1c;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/B1c;

    return-object v0
.end method

.method public static values()[LX/B1c;
    .locals 1

    .prologue
    .line 1736114
    sget-object v0, LX/B1c;->$VALUES:[LX/B1c;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/B1c;

    return-object v0
.end method
