.class public final LX/B5t;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/Alg;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/Alg;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 1746024
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1746025
    iput-object p1, p0, LX/B5t;->a:LX/0QB;

    .line 1746026
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1746043
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/B5t;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1746028
    packed-switch p2, :pswitch_data_0

    .line 1746029
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1746030
    :pswitch_0
    new-instance p2, LX/BMH;

    invoke-static {p1}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    const-class v1, LX/AzL;

    invoke-interface {p1, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/AzL;

    invoke-static {p1}, LX/AzQ;->b(LX/0QB;)LX/AzQ;

    move-result-object v2

    check-cast v2, LX/AzQ;

    invoke-static {p1}, LX/5oY;->a(LX/0QB;)LX/5oY;

    move-result-object p0

    check-cast p0, LX/5oY;

    invoke-direct {p2, v0, v1, v2, p0}, LX/BMH;-><init>(Lcom/facebook/content/SecureContextHelper;LX/AzL;LX/AzQ;LX/5oY;)V

    .line 1746031
    move-object v0, p2

    .line 1746032
    :goto_0
    return-object v0

    .line 1746033
    :pswitch_1
    invoke-static {p1}, LX/BMK;->b(LX/0QB;)LX/BMK;

    move-result-object v0

    goto :goto_0

    .line 1746034
    :pswitch_2
    new-instance v2, LX/BML;

    const-class v0, LX/1Ns;

    invoke-interface {p1, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/1Ns;

    invoke-static {p1}, LX/1Nq;->b(LX/0QB;)LX/1Nq;

    move-result-object v1

    check-cast v1, LX/1Nq;

    invoke-direct {v2, v0, v1}, LX/BML;-><init>(LX/1Ns;LX/1Nq;)V

    .line 1746035
    move-object v0, v2

    .line 1746036
    goto :goto_0

    .line 1746037
    :pswitch_3
    new-instance p0, LX/BMM;

    invoke-static {p1}, LX/1ay;->b(LX/0QB;)LX/1ay;

    move-result-object v0

    check-cast v0, LX/1ay;

    invoke-static {p1}, LX/1Nq;->b(LX/0QB;)LX/1Nq;

    move-result-object v1

    check-cast v1, LX/1Nq;

    invoke-static {p1}, LX/Awp;->b(LX/0QB;)LX/Awp;

    move-result-object v2

    check-cast v2, LX/Awp;

    invoke-direct {p0, v0, v1, v2}, LX/BMM;-><init>(LX/1ay;LX/1Nq;LX/Awp;)V

    .line 1746038
    move-object v0, p0

    .line 1746039
    goto :goto_0

    .line 1746040
    :pswitch_4
    new-instance v0, LX/BMO;

    const/16 v1, 0x24de

    invoke-static {p1, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    invoke-direct {v0, v1}, LX/BMO;-><init>(LX/0Or;)V

    .line 1746041
    move-object v0, v0

    .line 1746042
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 1746027
    const/4 v0, 0x5

    return v0
.end method
