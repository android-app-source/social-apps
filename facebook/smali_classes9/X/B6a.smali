.class public final LX/B6a;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;


# direct methods
.method public constructor <init>(Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;)V
    .locals 0

    .prologue
    .line 1746820
    iput-object p1, p0, LX/B6a;->a:Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()V
    .locals 5

    .prologue
    .line 1746825
    iget-object v0, p0, LX/B6a;->a:Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;

    invoke-virtual {v0}, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->a()V

    .line 1746826
    iget-object v0, p0, LX/B6a;->a:Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;

    iget-object v0, v0, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->b:LX/B7W;

    new-instance v1, LX/B7e;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, LX/B7e;-><init>(Z)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1746827
    iget-object v0, p0, LX/B6a;->a:Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;

    iget-object v0, v0, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->c:LX/0if;

    sget-object v1, LX/0ig;->v:LX/0ih;

    const-string v2, "click_submit_button"

    invoke-virtual {v0, v1, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1746828
    iget-object v0, p0, LX/B6a;->a:Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;

    iget-object v0, v0, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->j:LX/B6S;

    iget-object v1, p0, LX/B6a;->a:Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;

    iget-object v1, v1, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->k:LX/B7F;

    iget-object v2, p0, LX/B6a;->a:Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;

    iget-object v2, v2, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->h:LX/B6u;

    invoke-virtual {v2}, LX/B6u;->e()LX/0P1;

    move-result-object v2

    iget-object v3, p0, LX/B6a;->a:Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;

    iget-object v3, v3, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->h:LX/B6u;

    invoke-virtual {v3}, LX/B6u;->g()LX/0P1;

    move-result-object v3

    iget-object v4, p0, LX/B6a;->a:Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;

    iget-object v4, v4, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->h:LX/B6u;

    .line 1746829
    iget p0, v4, LX/B6u;->h:I

    move v4, p0

    .line 1746830
    invoke-virtual {v0, v1, v2, v3, v4}, LX/B6S;->a(LX/B7F;LX/0P1;LX/0P1;I)V

    .line 1746831
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, 0x34e96abc

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1746821
    iget-object v1, p0, LX/B6a;->a:Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;

    iget-object v1, v1, Lcom/facebook/leadgen/LeadGenLegacyActionButtonsView;->h:LX/B6u;

    invoke-virtual {v1}, LX/B6u;->k()LX/B77;

    move-result-object v1

    .line 1746822
    invoke-virtual {v1}, LX/B77;->isValid()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1746823
    invoke-direct {p0}, LX/B6a;->a()V

    .line 1746824
    :cond_0
    const v1, 0x19cd200f

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
