.class public LX/B6S;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/1Ck;

.field public final b:LX/B7U;

.field public final c:LX/B6l;

.field public final d:LX/0if;

.field public e:LX/B72;


# direct methods
.method public constructor <init>(LX/1Ck;LX/B7U;LX/B6l;LX/0if;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1746743
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1746744
    iput-object p1, p0, LX/B6S;->a:LX/1Ck;

    .line 1746745
    iput-object p2, p0, LX/B6S;->b:LX/B7U;

    .line 1746746
    iput-object p3, p0, LX/B6S;->c:LX/B6l;

    .line 1746747
    iput-object p4, p0, LX/B6S;->d:LX/0if;

    .line 1746748
    return-void
.end method

.method public static b(LX/0QB;)LX/B6S;
    .locals 5

    .prologue
    .line 1746738
    new-instance v4, LX/B6S;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v0

    check-cast v0, LX/1Ck;

    .line 1746739
    new-instance v3, LX/B7U;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v1

    check-cast v1, LX/0tX;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v2

    check-cast v2, LX/03V;

    invoke-direct {v3, v1, v2}, LX/B7U;-><init>(LX/0tX;LX/03V;)V

    .line 1746740
    move-object v1, v3

    .line 1746741
    check-cast v1, LX/B7U;

    invoke-static {p0}, LX/B6l;->a(LX/0QB;)LX/B6l;

    move-result-object v2

    check-cast v2, LX/B6l;

    invoke-static {p0}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v3

    check-cast v3, LX/0if;

    invoke-direct {v4, v0, v1, v2, v3}, LX/B6S;-><init>(LX/1Ck;LX/B7U;LX/B6l;LX/0if;)V

    .line 1746742
    return-object v4
.end method


# virtual methods
.method public final a(LX/B7F;LX/0P1;LX/0P1;I)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/B7F;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 1746731
    invoke-virtual {p1}, LX/B7F;->n()Ljava/lang/String;

    move-result-object v2

    .line 1746732
    iget-object v0, p1, LX/B7F;->d:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    if-nez v0, :cond_1

    .line 1746733
    const/4 v0, 0x0

    .line 1746734
    :goto_0
    move-object v3, v0

    .line 1746735
    if-nez v3, :cond_0

    .line 1746736
    :goto_1
    return-void

    .line 1746737
    :cond_0
    iget-object v6, p0, LX/B6S;->a:LX/1Ck;

    invoke-virtual {p1}, LX/B7F;->b()Ljava/lang/String;

    move-result-object v7

    new-instance v0, LX/B6Q;

    move-object v1, p0

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, LX/B6Q;-><init>(LX/B6S;Ljava/lang/String;Ljava/lang/String;LX/0P1;LX/0P1;)V

    new-instance v1, LX/B6R;

    invoke-direct {v1, p0, p4}, LX/B6R;-><init>(LX/B6S;I)V

    invoke-virtual {v6, v7, v0, v1}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    goto :goto_1

    :cond_1
    iget-object v0, p1, LX/B7F;->d:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->Y()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
