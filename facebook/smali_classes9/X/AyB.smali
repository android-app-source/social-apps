.class public LX/AyB;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/AyB;


# instance fields
.field public final a:LX/0lB;


# direct methods
.method public constructor <init>(LX/0lB;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1729745
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1729746
    iput-object p1, p0, LX/AyB;->a:LX/0lB;

    .line 1729747
    return-void
.end method

.method public static a(LX/0QB;)LX/AyB;
    .locals 4

    .prologue
    .line 1729748
    sget-object v0, LX/AyB;->b:LX/AyB;

    if-nez v0, :cond_1

    .line 1729749
    const-class v1, LX/AyB;

    monitor-enter v1

    .line 1729750
    :try_start_0
    sget-object v0, LX/AyB;->b:LX/AyB;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1729751
    if-eqz v2, :cond_0

    .line 1729752
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1729753
    new-instance p0, LX/AyB;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v3

    check-cast v3, LX/0lB;

    invoke-direct {p0, v3}, LX/AyB;-><init>(LX/0lB;)V

    .line 1729754
    move-object v0, p0

    .line 1729755
    sput-object v0, LX/AyB;->b:LX/AyB;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1729756
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1729757
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1729758
    :cond_1
    sget-object v0, LX/AyB;->b:LX/AyB;

    return-object v0

    .line 1729759
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1729760
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
