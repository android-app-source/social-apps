.class public final LX/BED;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/BEF;


# direct methods
.method public constructor <init>(LX/BEF;)V
    .locals 0

    .prologue
    .line 1764229
    iput-object p1, p0, LX/BED;->a:LX/BEF;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x3ea1c2f0

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1764230
    iget-object v0, p0, LX/BED;->a:LX/BEF;

    .line 1764231
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1764232
    iget-object v3, v0, LX/BEF;->a:LX/BEG;

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1764233
    iget-object v3, v0, LX/BEF;->c:LX/AEn;

    if-eqz v3, :cond_0

    .line 1764234
    iget-object v3, v0, LX/BEF;->c:LX/AEn;

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1764235
    :cond_0
    move-object v2, v2

    .line 1764236
    iget-object v0, p0, LX/BED;->a:LX/BEF;

    iget-object v0, v0, LX/BEF;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Af;

    .line 1764237
    iget-object v3, p0, LX/BED;->a:LX/BEF;

    .line 1764238
    new-instance v5, LX/7TY;

    invoke-virtual {v3}, LX/BEF;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v5, v4}, LX/7TY;-><init>(Landroid/content/Context;)V

    .line 1764239
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/AEn;

    .line 1764240
    invoke-interface {v4}, LX/AEn;->a()I

    move-result p0

    invoke-virtual {v5, p0}, LX/34c;->e(I)LX/3Ai;

    move-result-object p0

    invoke-interface {v4}, LX/AEn;->b()I

    move-result p1

    invoke-virtual {p0, p1}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    move-result-object p0

    new-instance p1, LX/BEE;

    invoke-direct {p1, v3, v4}, LX/BEE;-><init>(LX/BEF;LX/AEn;)V

    invoke-interface {p0, p1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto :goto_0

    .line 1764241
    :cond_1
    move-object v3, v5

    .line 1764242
    invoke-virtual {v0, v3}, LX/3Af;->a(LX/1OM;)V

    .line 1764243
    invoke-virtual {v0}, LX/3Af;->show()V

    .line 1764244
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AEn;

    .line 1764245
    invoke-interface {v0}, LX/AEn;->c()V

    goto :goto_1

    .line 1764246
    :cond_2
    const v0, -0x165cab89

    invoke-static {v0, v1}, LX/02F;->a(II)V

    return-void
.end method
