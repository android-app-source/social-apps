.class public final LX/Aih;
.super LX/1a1;
.source ""


# instance fields
.field public final synthetic l:LX/Aij;

.field public m:Landroid/view/View;


# direct methods
.method public constructor <init>(LX/Aij;Landroid/view/View;)V
    .locals 4

    .prologue
    .line 1706758
    iput-object p1, p0, LX/Aih;->l:LX/Aij;

    .line 1706759
    invoke-direct {p0, p2}, LX/1a1;-><init>(Landroid/view/View;)V

    .line 1706760
    iput-object p2, p0, LX/Aih;->m:Landroid/view/View;

    .line 1706761
    const/4 p1, 0x1

    .line 1706762
    iget-object v0, p0, LX/Aih;->m:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    .line 1706763
    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    .line 1706764
    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    .line 1706765
    const v3, 0x7f010779

    invoke-virtual {v0, v3, v1, p1}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 1706766
    const v3, 0x7f01077b

    invoke-virtual {v0, v3, v2, p1}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 1706767
    new-instance v3, Landroid/text/SpannableStringBuilder;

    iget-object v0, p0, LX/Aih;->m:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v1, v1, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 1706768
    iget-object v0, p0, LX/Aih;->m:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0823a2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1706769
    const-string v1, " "

    invoke-virtual {v3, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1706770
    invoke-virtual {v3, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1706771
    new-instance v1, LX/Aig;

    invoke-direct {v1, p0, v2}, LX/Aig;-><init>(LX/Aih;Landroid/util/TypedValue;)V

    invoke-virtual {v3}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result p1

    sub-int/2addr v2, p1

    invoke-virtual {v3}, Landroid/text/SpannableStringBuilder;->length()I

    move-result p1

    const/16 p2, 0x21

    invoke-virtual {v3, v1, v2, p1, p2}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1706772
    new-instance v1, Landroid/text/style/TextAppearanceSpan;

    iget-object v2, p0, LX/Aih;->m:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    const p1, 0x7f0e0987

    invoke-direct {v1, v2, p1}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    const/4 v2, 0x0

    invoke-virtual {v3}, Landroid/text/SpannableStringBuilder;->length()I

    move-result p1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    sub-int v0, p1, v0

    const/16 p1, 0x12

    invoke-virtual {v3, v1, v2, v0, p1}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1706773
    iget-object v0, p0, LX/Aih;->m:Landroid/view/View;

    const v1, 0x7f0d063a

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/awesomizer/ui/LearnMoreTextView;

    .line 1706774
    invoke-virtual {v0, v3}, Lcom/facebook/feed/awesomizer/ui/LearnMoreTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1706775
    iget-object v1, p0, LX/Aih;->m:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f082389

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/feed/awesomizer/ui/LearnMoreTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1706776
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/feed/awesomizer/ui/LearnMoreTextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 1706777
    return-void
.end method
