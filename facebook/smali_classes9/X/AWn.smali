.class public final LX/AWn;
.super Landroid/os/CountDownTimer;
.source ""


# instance fields
.field public final synthetic a:LX/AWb;


# direct methods
.method public constructor <init>(LX/AWb;)V
    .locals 4

    .prologue
    .line 1683390
    iput-object p1, p0, LX/AWn;->a:LX/AWb;

    .line 1683391
    const-wide/16 v0, 0xbb8

    const-wide/16 v2, 0x1f4

    invoke-direct {p0, v0, v1, v2, v3}, Landroid/os/CountDownTimer;-><init>(JJ)V

    .line 1683392
    return-void
.end method


# virtual methods
.method public final onFinish()V
    .locals 2

    .prologue
    .line 1683398
    iget-object v0, p0, LX/AWn;->a:LX/AWb;

    iget-object v0, v0, LX/AWT;->e:LX/AVF;

    if-eqz v0, :cond_0

    .line 1683399
    iget-object v0, p0, LX/AWn;->a:LX/AWb;

    iget-object v0, v0, LX/AWT;->e:LX/AVF;

    sget-object v1, LX/AVE;->RECORDING:LX/AVE;

    invoke-virtual {v0, v1}, LX/AVF;->b(LX/AVE;)V

    .line 1683400
    :cond_0
    return-void
.end method

.method public final onTick(J)V
    .locals 5

    .prologue
    .line 1683393
    const-wide/16 v0, 0x3e8

    div-long v0, p1, v0

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    .line 1683394
    iget-object v1, p0, LX/AWn;->a:LX/AWb;

    iget-object v1, v1, LX/AWb;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v1}, Lcom/facebook/widget/text/BetterTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1683395
    iget-object v1, p0, LX/AWn;->a:LX/AWb;

    iget-object v1, v1, LX/AWb;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1683396
    iget-object v0, p0, LX/AWn;->a:LX/AWb;

    iget-object v0, v0, LX/AWb;->a:LX/3RX;

    const v1, 0x7f07002c

    iget-object v2, p0, LX/AWn;->a:LX/AWb;

    iget-object v2, v2, LX/AWb;->b:LX/3RZ;

    invoke-virtual {v2}, LX/3RZ;->a()I

    move-result v2

    const v3, 0x3e19999a    # 0.15f

    invoke-virtual {v0, v1, v2, v3}, LX/3RX;->a(IIF)LX/7Cb;

    .line 1683397
    :cond_0
    return-void
.end method
