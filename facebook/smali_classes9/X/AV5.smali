.class public LX/AV5;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:LX/03V;

.field private final c:LX/AVT;

.field public final d:Ljava/util/concurrent/ScheduledExecutorService;

.field public final e:LX/0tX;

.field public final f:LX/0Sh;

.field public final g:Landroid/os/Handler;

.field public final h:LX/0SG;

.field public final i:Ljava/lang/String;

.field public final j:LX/1b7;

.field public final k:LX/AV4;

.field public final l:I

.field public m:I

.field public n:Ljava/util/concurrent/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Future",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1678851
    const-class v0, LX/AV5;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/AV5;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/03V;LX/AVT;Ljava/util/concurrent/ScheduledExecutorService;LX/0tX;LX/0Sh;Landroid/os/Handler;LX/0SG;Ljava/lang/String;LX/1b7;)V
    .locals 2
    .param p3    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/SingleThreadedExecutorService;
        .end annotation
    .end param
    .param p6    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p8    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p9    # LX/1b7;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1678837
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1678838
    iput-object p1, p0, LX/AV5;->b:LX/03V;

    .line 1678839
    iput-object p2, p0, LX/AV5;->c:LX/AVT;

    .line 1678840
    iput-object p3, p0, LX/AV5;->d:Ljava/util/concurrent/ScheduledExecutorService;

    .line 1678841
    iput-object p4, p0, LX/AV5;->e:LX/0tX;

    .line 1678842
    iput-object p5, p0, LX/AV5;->f:LX/0Sh;

    .line 1678843
    iput-object p6, p0, LX/AV5;->g:Landroid/os/Handler;

    .line 1678844
    iput-object p7, p0, LX/AV5;->h:LX/0SG;

    .line 1678845
    iput-object p8, p0, LX/AV5;->i:Ljava/lang/String;

    .line 1678846
    invoke-static {p9}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1b7;

    iput-object v0, p0, LX/AV5;->j:LX/1b7;

    .line 1678847
    const/16 v0, 0x1e

    iput v0, p0, LX/AV5;->m:I

    .line 1678848
    const/4 v0, 0x5

    iput v0, p0, LX/AV5;->l:I

    .line 1678849
    new-instance v0, LX/AV4;

    invoke-direct {v0, p0}, LX/AV4;-><init>(LX/AV5;)V

    iput-object v0, p0, LX/AV5;->k:LX/AV4;

    .line 1678850
    return-void
.end method

.method public static a$redex0(LX/AV5;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1678835
    iget-object v0, p0, LX/AV5;->c:LX/AVT;

    invoke-virtual {v0, p1}, LX/AVT;->c(Ljava/lang/String;)V

    .line 1678836
    return-void
.end method

.method public static e(LX/AV5;)Z
    .locals 1

    .prologue
    .line 1678822
    iget-object v0, p0, LX/AV5;->d:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ScheduledExecutorService;->isShutdown()Z

    move-result v0

    return v0
.end method

.method public static f(LX/AV5;)Z
    .locals 1

    .prologue
    .line 1678834
    iget-object v0, p0, LX/AV5;->n:Ljava/util/concurrent/Future;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/AV5;->n:Ljava/util/concurrent/Future;

    invoke-interface {v0}, Ljava/util/concurrent/Future;->isDone()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final b()V
    .locals 2

    .prologue
    .line 1678828
    iget-object v0, p0, LX/AV5;->f:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 1678829
    invoke-static {p0}, LX/AV5;->f(LX/AV5;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1678830
    :goto_0
    return-void

    .line 1678831
    :cond_0
    iget-object v0, p0, LX/AV5;->n:Ljava/util/concurrent/Future;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 1678832
    const/4 v0, 0x0

    iput-object v0, p0, LX/AV5;->n:Ljava/util/concurrent/Future;

    .line 1678833
    const-string v0, "copyright_monitor_suspend"

    invoke-static {p0, v0}, LX/AV5;->a$redex0(LX/AV5;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1678823
    invoke-static {p0}, LX/AV5;->e(LX/AV5;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1678824
    :goto_0
    return-void

    .line 1678825
    :cond_0
    invoke-virtual {p0}, LX/AV5;->b()V

    .line 1678826
    iget-object v0, p0, LX/AV5;->d:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ScheduledExecutorService;->shutdown()V

    .line 1678827
    const-string v0, "copyright_monitor_stop"

    invoke-static {p0, v0}, LX/AV5;->a$redex0(LX/AV5;Ljava/lang/String;)V

    goto :goto_0
.end method
