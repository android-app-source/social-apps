.class public LX/C67;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Vq;


# instance fields
.field private final a:LX/C5o;

.field public b:Z

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/C5p;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/C5o;)V
    .locals 1
    .param p1    # LX/C5o;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1849657
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1849658
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1849659
    iput-object v0, p0, LX/C67;->c:LX/0Ot;

    .line 1849660
    iput-object p1, p0, LX/C67;->a:LX/C5o;

    .line 1849661
    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 4

    .prologue
    .line 1849662
    iput-boolean p1, p0, LX/C67;->b:Z

    .line 1849663
    iget-boolean v0, p0, LX/C67;->b:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/C67;->a:LX/C5o;

    iget-object v0, v0, LX/C5o;->a:LX/1Wt;

    .line 1849664
    iget-object v1, v0, LX/1Wt;->a:LX/1Wu;

    move-object v0, v1

    .line 1849665
    sget-object v1, LX/1Wu;->WAITING_FOR_INTERACTION_STOP:LX/1Wu;

    if-ne v0, v1, :cond_0

    .line 1849666
    iget-object v0, p0, LX/C67;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/C5p;

    iget-object v1, p0, LX/C67;->a:LX/C5o;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, p0, v2, v3}, LX/C5p;->a(LX/C5o;LX/C67;J)V

    .line 1849667
    :cond_0
    return-void
.end method
