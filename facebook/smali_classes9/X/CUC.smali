.class public final LX/CUC;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:Z

.field public final b:Ljava/lang/String;

.field public final c:LX/CUB;

.field public final d:Ljava/lang/String;

.field public final e:LX/CTv;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;)V
    .locals 10

    .prologue
    const/4 v8, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1896293
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1896294
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1896295
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;->r()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-nez v0, :cond_2

    move v5, v1

    .line 1896296
    :goto_0
    if-eqz v5, :cond_3

    move v4, v1

    .line 1896297
    :goto_1
    if-eqz v5, :cond_4

    move v0, v1

    .line 1896298
    :goto_2
    const/4 v6, 0x1

    .line 1896299
    if-eqz v5, :cond_8

    .line 1896300
    :cond_0
    :goto_3
    move v6, v6

    .line 1896301
    iput-boolean v6, p0, LX/CUC;->a:Z

    .line 1896302
    if-nez v5, :cond_6

    .line 1896303
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;->r()LX/1vs;

    move-result-object v6

    iget-object v7, v6, LX/1vs;->a:LX/15i;

    iget v6, v6, LX/1vs;->b:I

    .line 1896304
    invoke-virtual {v7, v6, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/CXh;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1896305
    :goto_4
    iput-object v2, p0, LX/CUC;->b:Ljava/lang/String;

    .line 1896306
    if-eqz v5, :cond_a

    .line 1896307
    sget-object v2, LX/CUB;->DEFAULT_CLOSE:LX/CUB;

    .line 1896308
    :goto_5
    move-object v2, v2

    .line 1896309
    iput-object v2, p0, LX/CUC;->c:LX/CUB;

    .line 1896310
    if-nez v4, :cond_7

    .line 1896311
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;->r()LX/1vs;

    move-result-object v2

    iget-object v4, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    .line 1896312
    invoke-virtual {v4, v2, v8}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    .line 1896313
    :goto_6
    iput-object v2, p0, LX/CUC;->d:Ljava/lang/String;

    .line 1896314
    if-nez v0, :cond_1

    .line 1896315
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;->r()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 1896316
    const-class v3, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenDataFragmentModel$NavbarActionModel$ActionEventHandlerModel;

    invoke-virtual {v2, v0, v1, v3}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenDataFragmentModel$NavbarActionModel$ActionEventHandlerModel;

    invoke-static {v0}, LX/CVz;->a(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenDataFragmentModel$NavbarActionModel$ActionEventHandlerModel;)Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformEventListenersFragmentModel$EventHandlerModel;

    move-result-object v0

    invoke-static {v0}, LX/CTv;->a(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformEventListenersFragmentModel$EventHandlerModel;)LX/CTv;

    move-result-object v3

    .line 1896317
    :cond_1
    iput-object v3, p0, LX/CUC;->e:LX/CTv;

    .line 1896318
    return-void

    :cond_2
    move v5, v2

    .line 1896319
    goto :goto_0

    .line 1896320
    :cond_3
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;->r()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 1896321
    invoke-virtual {v4, v0, v8}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    move v4, v0

    goto :goto_1

    .line 1896322
    :cond_4
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;->r()LX/1vs;

    move-result-object v0

    iget-object v6, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 1896323
    const-class v7, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenDataFragmentModel$NavbarActionModel$ActionEventHandlerModel;

    invoke-virtual {v6, v0, v1, v7}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    if-nez v0, :cond_5

    move v0, v1

    goto :goto_2

    :cond_5
    move v0, v2

    goto :goto_2

    :cond_6
    move-object v2, v3

    .line 1896324
    goto :goto_4

    :cond_7
    move-object v2, v3

    .line 1896325
    goto :goto_6

    .line 1896326
    :cond_8
    if-nez v4, :cond_9

    if-nez v0, :cond_0

    .line 1896327
    :cond_9
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;->r()LX/1vs;

    move-result-object v6

    iget-object v7, v6, LX/1vs;->a:LX/15i;

    iget v6, v6, LX/1vs;->b:I

    const/4 v9, 0x3

    invoke-virtual {v7, v6, v9}, LX/15i;->h(II)Z

    move-result v6

    goto/16 :goto_3

    .line 1896328
    :cond_a
    if-eqz v4, :cond_b

    .line 1896329
    sget-object v2, LX/CUB;->NONE:LX/CUB;

    goto :goto_5

    .line 1896330
    :cond_b
    if-eqz v0, :cond_c

    .line 1896331
    sget-object v2, LX/CUB;->DEFAULT_CLOSE:LX/CUB;

    goto :goto_5

    .line 1896332
    :cond_c
    sget-object v2, LX/CUB;->EVENT_HANDLER:LX/CUB;

    goto :goto_5
.end method
