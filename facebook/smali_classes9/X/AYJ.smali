.class public final enum LX/AYJ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/AYJ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/AYJ;

.field public static final enum BOTTOM_LEFT:LX/AYJ;

.field public static final enum BOTTOM_RIGHT:LX/AYJ;

.field public static final enum TOP_LEFT:LX/AYJ;

.field public static final enum TOP_RIGHT:LX/AYJ;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1685160
    new-instance v0, LX/AYJ;

    const-string v1, "TOP_LEFT"

    invoke-direct {v0, v1, v2}, LX/AYJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AYJ;->TOP_LEFT:LX/AYJ;

    new-instance v0, LX/AYJ;

    const-string v1, "TOP_RIGHT"

    invoke-direct {v0, v1, v3}, LX/AYJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AYJ;->TOP_RIGHT:LX/AYJ;

    new-instance v0, LX/AYJ;

    const-string v1, "BOTTOM_LEFT"

    invoke-direct {v0, v1, v4}, LX/AYJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AYJ;->BOTTOM_LEFT:LX/AYJ;

    new-instance v0, LX/AYJ;

    const-string v1, "BOTTOM_RIGHT"

    invoke-direct {v0, v1, v5}, LX/AYJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AYJ;->BOTTOM_RIGHT:LX/AYJ;

    .line 1685161
    const/4 v0, 0x4

    new-array v0, v0, [LX/AYJ;

    sget-object v1, LX/AYJ;->TOP_LEFT:LX/AYJ;

    aput-object v1, v0, v2

    sget-object v1, LX/AYJ;->TOP_RIGHT:LX/AYJ;

    aput-object v1, v0, v3

    sget-object v1, LX/AYJ;->BOTTOM_LEFT:LX/AYJ;

    aput-object v1, v0, v4

    sget-object v1, LX/AYJ;->BOTTOM_RIGHT:LX/AYJ;

    aput-object v1, v0, v5

    sput-object v0, LX/AYJ;->$VALUES:[LX/AYJ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1685159
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/AYJ;
    .locals 1

    .prologue
    .line 1685158
    const-class v0, LX/AYJ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/AYJ;

    return-object v0
.end method

.method public static values()[LX/AYJ;
    .locals 1

    .prologue
    .line 1685157
    sget-object v0, LX/AYJ;->$VALUES:[LX/AYJ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/AYJ;

    return-object v0
.end method
