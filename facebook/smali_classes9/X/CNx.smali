.class public LX/CNx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/CNe;


# instance fields
.field private final a:LX/CNb;

.field private final b:LX/CNq;

.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/CNe;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/CNe;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/CNb;LX/CNq;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 1882322
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1882323
    iput-object p1, p0, LX/CNx;->a:LX/CNb;

    .line 1882324
    iput-object p2, p0, LX/CNx;->b:LX/CNq;

    .line 1882325
    const-string v0, "send-actions"

    invoke-virtual {p1, v0}, LX/CNb;->a(Ljava/lang/String;)LX/0Px;

    move-result-object v3

    .line 1882326
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/CNx;->c:Ljava/util/List;

    move v1, v2

    .line 1882327
    :goto_0
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 1882328
    iget-object v4, p0, LX/CNx;->c:Ljava/util/List;

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CNb;

    invoke-static {v0, p2}, LX/CNd;->a(LX/CNb;LX/CNc;)LX/CNe;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1882329
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1882330
    :cond_0
    const-string v0, "failure-actions"

    invoke-virtual {p1, v0}, LX/CNb;->a(Ljava/lang/String;)LX/0Px;

    move-result-object v1

    .line 1882331
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/CNx;->d:Ljava/util/List;

    .line 1882332
    :goto_1
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    .line 1882333
    iget-object v3, p0, LX/CNx;->d:Ljava/util/List;

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CNb;

    invoke-static {v0, p2}, LX/CNd;->a(LX/CNb;LX/CNc;)LX/CNe;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1882334
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1882335
    :cond_1
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 11

    .prologue
    .line 1882336
    iget-object v0, p0, LX/CNx;->a:LX/CNb;

    .line 1882337
    const-string v1, "action_controller"

    move-object v1, v1

    .line 1882338
    invoke-virtual {v0, v1}, LX/CNb;->d(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CNz;

    .line 1882339
    if-eqz v0, :cond_0

    .line 1882340
    iget-object v1, p0, LX/CNx;->a:LX/CNb;

    iget-object v2, p0, LX/CNx;->b:LX/CNq;

    iget-object v3, p0, LX/CNx;->c:Ljava/util/List;

    iget-object v4, p0, LX/CNx;->d:Ljava/util/List;

    .line 1882341
    iget v10, v0, LX/CNz;->e:I

    move-object v5, v0

    move-object v6, v1

    move-object v7, v2

    move-object v8, v3

    move-object v9, v4

    invoke-static/range {v5 .. v10}, LX/CNz;->a(LX/CNz;LX/CNb;LX/CNq;Ljava/util/List;Ljava/util/List;I)Z

    move-result v5

    .line 1882342
    if-eqz v5, :cond_0

    .line 1882343
    iget v5, v0, LX/CNz;->e:I

    add-int/lit8 v5, v5, 0x1

    iput v5, v0, LX/CNz;->e:I

    .line 1882344
    :cond_0
    return-void
.end method
