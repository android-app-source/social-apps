.class public LX/BmL;
.super LX/2EJ;
.source ""

# interfaces
.implements Landroid/widget/TimePicker$OnTimeChangedListener;


# instance fields
.field private final b:LX/11S;

.field public final c:Landroid/text/format/Time;
    .annotation build Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field public d:Landroid/text/format/Time;

.field public e:Landroid/widget/TimePicker;

.field public f:LX/BmY;

.field private final g:LX/1lB;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/text/format/Time;LX/BmY;LX/1lB;LX/11S;)V
    .locals 7
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Landroid/text/format/Time;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p3    # LX/BmY;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/1lB;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1818170
    invoke-direct {p0, p1, v1}, LX/2EJ;-><init>(Landroid/content/Context;I)V

    .line 1818171
    iput-object p2, p0, LX/BmL;->d:Landroid/text/format/Time;

    .line 1818172
    iput-object p4, p0, LX/BmL;->g:LX/1lB;

    .line 1818173
    iput-object p5, p0, LX/BmL;->b:LX/11S;

    .line 1818174
    iget-boolean v0, p2, Landroid/text/format/Time;->allDay:Z

    if-eqz v0, :cond_0

    .line 1818175
    new-instance v3, Landroid/text/format/Time;

    invoke-direct {v3}, Landroid/text/format/Time;-><init>()V

    .line 1818176
    invoke-virtual {v3}, Landroid/text/format/Time;->setToNow()V

    .line 1818177
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0, p2}, Landroid/text/format/Time;-><init>(Landroid/text/format/Time;)V

    iput-object v0, p0, LX/BmL;->c:Landroid/text/format/Time;

    .line 1818178
    iget-object v0, p0, LX/BmL;->c:Landroid/text/format/Time;

    iget v2, v3, Landroid/text/format/Time;->minute:I

    iget v3, v3, Landroid/text/format/Time;->hour:I

    iget v4, p2, Landroid/text/format/Time;->monthDay:I

    iget v5, p2, Landroid/text/format/Time;->month:I

    iget v6, p2, Landroid/text/format/Time;->year:I

    invoke-virtual/range {v0 .. v6}, Landroid/text/format/Time;->set(IIIIII)V

    .line 1818179
    :goto_0
    iput-object p3, p0, LX/BmL;->f:LX/BmY;

    .line 1818180
    const/4 v3, -0x2

    .line 1818181
    invoke-virtual {p0}, LX/BmL;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0314c0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TimePicker;

    iput-object v0, p0, LX/BmL;->e:Landroid/widget/TimePicker;

    .line 1818182
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v0, v3, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 1818183
    iget-object v1, p0, LX/BmL;->e:Landroid/widget/TimePicker;

    invoke-virtual {p0}, LX/BmL;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TimePicker;->setIs24HourView(Ljava/lang/Boolean;)V

    .line 1818184
    iget-object v1, p0, LX/BmL;->e:Landroid/widget/TimePicker;

    iget-object v2, p0, LX/BmL;->c:Landroid/text/format/Time;

    iget v2, v2, Landroid/text/format/Time;->hour:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TimePicker;->setCurrentHour(Ljava/lang/Integer;)V

    .line 1818185
    iget-object v1, p0, LX/BmL;->e:Landroid/widget/TimePicker;

    iget-object v2, p0, LX/BmL;->c:Landroid/text/format/Time;

    iget v2, v2, Landroid/text/format/Time;->minute:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TimePicker;->setCurrentMinute(Ljava/lang/Integer;)V

    .line 1818186
    iget-object v1, p0, LX/BmL;->e:Landroid/widget/TimePicker;

    invoke-virtual {v1, p0}, Landroid/widget/TimePicker;->setOnTimeChangedListener(Landroid/widget/TimePicker$OnTimeChangedListener;)V

    .line 1818187
    iget-object v1, p0, LX/BmL;->c:Landroid/text/format/Time;

    invoke-static {p0, v1}, LX/BmL;->a$redex0(LX/BmL;Landroid/text/format/Time;)V

    .line 1818188
    iget-object v1, p0, LX/BmL;->e:Landroid/widget/TimePicker;

    invoke-virtual {v1, v0}, Landroid/widget/TimePicker;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1818189
    iget-object v0, p0, LX/BmL;->e:Landroid/widget/TimePicker;

    invoke-virtual {p0, v0}, LX/2EJ;->a(Landroid/view/View;)V

    .line 1818190
    const/4 v0, -0x1

    invoke-virtual {p0}, LX/BmL;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f080016

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/BmJ;

    invoke-direct {v2, p0}, LX/BmJ;-><init>(LX/BmL;)V

    invoke-virtual {p0, v0, v1, v2}, LX/2EJ;->a(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 1818191
    const/4 v0, -0x2

    invoke-virtual {p0}, LX/BmL;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f082179

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/BmK;

    invoke-direct {v2, p0}, LX/BmK;-><init>(LX/BmL;)V

    invoke-virtual {p0, v0, v1, v2}, LX/2EJ;->a(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 1818192
    return-void

    .line 1818193
    :cond_0
    iput-object p2, p0, LX/BmL;->c:Landroid/text/format/Time;

    goto/16 :goto_0
.end method

.method public static a$redex0(LX/BmL;Landroid/text/format/Time;)V
    .locals 4

    .prologue
    .line 1818201
    iget-object v0, p0, LX/BmL;->b:LX/11S;

    iget-object v1, p0, LX/BmL;->g:LX/1lB;

    const/4 v2, 0x1

    invoke-virtual {p1, v2}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, LX/11S;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/BmL;->setTitle(Ljava/lang/CharSequence;)V

    .line 1818202
    return-void
.end method


# virtual methods
.method public final onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 1818203
    invoke-super {p0, p1}, LX/2EJ;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 1818204
    const-string v0, "is24Hour"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 1818205
    const-string v1, "hour"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 1818206
    const-string v2, "minute"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 1818207
    iget-object v3, p0, LX/BmL;->e:Landroid/widget/TimePicker;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/TimePicker;->setIs24HourView(Ljava/lang/Boolean;)V

    .line 1818208
    iget-object v0, p0, LX/BmL;->e:Landroid/widget/TimePicker;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TimePicker;->setCurrentHour(Ljava/lang/Integer;)V

    .line 1818209
    iget-object v0, p0, LX/BmL;->e:Landroid/widget/TimePicker;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TimePicker;->setCurrentMinute(Ljava/lang/Integer;)V

    .line 1818210
    return-void
.end method

.method public final onSaveInstanceState()Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 1818196
    invoke-super {p0}, LX/2EJ;->onSaveInstanceState()Landroid/os/Bundle;

    move-result-object v0

    .line 1818197
    const-string v1, "is24Hour"

    iget-object v2, p0, LX/BmL;->e:Landroid/widget/TimePicker;

    invoke-virtual {v2}, Landroid/widget/TimePicker;->is24HourView()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1818198
    const-string v1, "hour"

    iget-object v2, p0, LX/BmL;->e:Landroid/widget/TimePicker;

    invoke-virtual {v2}, Landroid/widget/TimePicker;->getCurrentHour()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1818199
    const-string v1, "minute"

    iget-object v2, p0, LX/BmL;->e:Landroid/widget/TimePicker;

    invoke-virtual {v2}, Landroid/widget/TimePicker;->getCurrentMinute()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1818200
    return-object v0
.end method

.method public final onTimeChanged(Landroid/widget/TimePicker;II)V
    .locals 1

    .prologue
    .line 1818194
    iget-object v0, p0, LX/BmL;->c:Landroid/text/format/Time;

    invoke-static {v0, p2, p3}, LX/Bmb;->a(Landroid/text/format/Time;II)Landroid/text/format/Time;

    move-result-object v0

    invoke-static {p0, v0}, LX/BmL;->a$redex0(LX/BmL;Landroid/text/format/Time;)V

    .line 1818195
    return-void
.end method
