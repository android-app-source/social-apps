.class public LX/ArZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/89d;


# instance fields
.field public final a:Landroid/view/View;

.field public final b:LX/9dI;

.field private final c:LX/8GN;

.field public final d:LX/9cu;

.field public e:LX/0Px;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/SwipeableParams;",
            ">;"
        }
    .end annotation
.end field

.field public f:Lcom/facebook/photos/creativeediting/model/SwipeableParams;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/photos/creativeediting/model/SwipeableParams;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/photos/creativeediting/model/SwipeableParams;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public i:I


# direct methods
.method public constructor <init>(LX/9dJ;LX/8GN;Landroid/view/View;)V
    .locals 2
    .param p3    # Landroid/view/View;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1719323
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1719324
    new-instance v0, LX/ArW;

    invoke-direct {v0, p0}, LX/ArW;-><init>(LX/ArZ;)V

    iput-object v0, p0, LX/ArZ;->d:LX/9cu;

    .line 1719325
    iput-object p3, p0, LX/ArZ;->a:Landroid/view/View;

    .line 1719326
    iput-object p2, p0, LX/ArZ;->c:LX/8GN;

    .line 1719327
    new-instance v0, LX/ArX;

    invoke-direct {v0, p0}, LX/ArX;-><init>(LX/ArZ;)V

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, LX/9dJ;->a(LX/9d1;Z)LX/9dI;

    move-result-object v0

    iput-object v0, p0, LX/ArZ;->b:LX/9dI;

    .line 1719328
    return-void
.end method

.method public static d$redex0(LX/ArZ;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1719299
    iget-object v1, p0, LX/ArZ;->e:LX/0Px;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/ArZ;->e:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-le v1, v0, :cond_0

    iget-object v1, p0, LX/ArZ;->b:LX/9dI;

    .line 1719300
    iget-boolean p0, v1, LX/9dI;->B:Z

    move v1, p0

    .line 1719301
    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static e(LX/ArZ;)V
    .locals 1

    .prologue
    .line 1719320
    invoke-static {p0}, LX/ArZ;->d$redex0(LX/ArZ;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1719321
    iget-object v0, p0, LX/ArZ;->b:LX/9dI;

    invoke-virtual {v0}, LX/9dI;->i()V

    .line 1719322
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1719317
    iget-object v0, p0, LX/ArZ;->g:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/ArZ;->g:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    .line 1719318
    iget-object p0, v0, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->b:Ljava/lang/String;

    move-object v0, p0

    .line 1719319
    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 1719302
    iget-object v0, p0, LX/ArZ;->e:LX/0Px;

    const/4 v3, 0x0

    .line 1719303
    if-eqz v0, :cond_3

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    move v2, v3

    .line 1719304
    :goto_0
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v1

    if-ge v2, v1, :cond_2

    .line 1719305
    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    .line 1719306
    iget-object v4, v1, Lcom/facebook/photos/creativeediting/model/SwipeableParams;->b:Ljava/lang/String;

    move-object v1, v4

    .line 1719307
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1719308
    :goto_1
    move v0, v2

    .line 1719309
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 1719310
    iget-object v1, p0, LX/ArZ;->e:LX/0Px;

    invoke-virtual {v1, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    iput-object v0, p0, LX/ArZ;->g:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    .line 1719311
    iget-object v0, p0, LX/ArZ;->e:LX/0Px;

    iget-object v1, p0, LX/ArZ;->g:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    invoke-static {v0, v1}, LX/8GN;->b(LX/0Px;Lcom/facebook/photos/creativeediting/model/SwipeableParams;)Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    move-result-object v0

    iput-object v0, p0, LX/ArZ;->f:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    .line 1719312
    iget-object v0, p0, LX/ArZ;->e:LX/0Px;

    iget-object v1, p0, LX/ArZ;->g:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    invoke-static {v0, v1}, LX/8GN;->a(LX/0Px;Lcom/facebook/photos/creativeediting/model/SwipeableParams;)Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    move-result-object v0

    iput-object v0, p0, LX/ArZ;->h:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    .line 1719313
    :cond_0
    return-void

    .line 1719314
    :cond_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    :cond_2
    move v2, v3

    .line 1719315
    goto :goto_1

    .line 1719316
    :cond_3
    const/4 v2, -0x1

    goto :goto_1
.end method
