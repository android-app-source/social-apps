.class public final LX/Aqr;
.super LX/6oO;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/friendsharing/gif/activity/GifTypeaheadText;


# direct methods
.method public constructor <init>(Lcom/facebook/friendsharing/gif/activity/GifTypeaheadText;)V
    .locals 0

    .prologue
    .line 1718380
    iput-object p1, p0, LX/Aqr;->a:Lcom/facebook/friendsharing/gif/activity/GifTypeaheadText;

    invoke-direct {p0}, LX/6oO;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 4

    .prologue
    .line 1718381
    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    if-nez v0, :cond_2

    .line 1718382
    :cond_0
    iget-object v0, p0, LX/Aqr;->a:Lcom/facebook/friendsharing/gif/activity/GifTypeaheadText;

    iget-object v0, v0, Lcom/facebook/friendsharing/gif/activity/GifTypeaheadText;->c:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    .line 1718383
    iget-object v0, p0, LX/Aqr;->a:Lcom/facebook/friendsharing/gif/activity/GifTypeaheadText;

    iget-object v0, v0, Lcom/facebook/friendsharing/gif/activity/GifTypeaheadText;->d:LX/0wd;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    .line 1718384
    :goto_0
    return-void

    .line 1718385
    :cond_1
    iget-object v0, p0, LX/Aqr;->a:Lcom/facebook/friendsharing/gif/activity/GifTypeaheadText;

    iget-object v0, v0, Lcom/facebook/friendsharing/gif/activity/GifTypeaheadText;->c:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 1718386
    :cond_2
    iget-object v0, p0, LX/Aqr;->a:Lcom/facebook/friendsharing/gif/activity/GifTypeaheadText;

    iget-object v0, v0, Lcom/facebook/friendsharing/gif/activity/GifTypeaheadText;->d:LX/0wd;

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    .line 1718387
    iget-object v0, p0, LX/Aqr;->a:Lcom/facebook/friendsharing/gif/activity/GifTypeaheadText;

    iget-object v0, v0, Lcom/facebook/friendsharing/gif/activity/GifTypeaheadText;->c:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method
