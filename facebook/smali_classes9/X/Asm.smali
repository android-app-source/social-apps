.class public final LX/Asm;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/Ass;


# direct methods
.method public constructor <init>(LX/Ass;)V
    .locals 0

    .prologue
    .line 1720854
    iput-object p1, p0, LX/Asm;->a:LX/Ass;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x64636928

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1720855
    sget-object v1, LX/Asr;->a:[I

    iget-object v2, p0, LX/Asm;->a:LX/Ass;

    iget-object v2, v2, LX/Ass;->A:LX/87O;

    invoke-virtual {v2}, LX/87O;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1720856
    :cond_0
    :goto_0
    const v1, -0x2e64cde4

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 1720857
    :pswitch_0
    iget-object v1, p0, LX/Asm;->a:LX/Ass;

    iget-boolean v1, v1, LX/Ass;->B:Z

    if-eqz v1, :cond_0

    .line 1720858
    iget-object v1, p0, LX/Asm;->a:LX/Ass;

    .line 1720859
    iget-object v2, v1, LX/Ass;->e:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0il;

    .line 1720860
    invoke-interface {v2}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v3}, Lcom/facebook/composer/system/model/ComposerModelImpl;->j()Lcom/facebook/friendsharing/inspiration/model/CameraState;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/friendsharing/inspiration/model/CameraState;->getCaptureState()LX/86q;

    move-result-object v3

    sget-object p1, LX/86q;->FLIPPING:LX/86q;

    if-ne v3, p1, :cond_1

    .line 1720861
    :goto_1
    iget-object v1, p0, LX/Asm;->a:LX/Ass;

    const/4 v2, 0x0

    .line 1720862
    iput-boolean v2, v1, LX/Ass;->B:Z

    .line 1720863
    goto :goto_0

    .line 1720864
    :pswitch_1
    iget-object v1, p0, LX/Asm;->a:LX/Ass;

    iget-object v1, v1, LX/Ass;->g:LX/03V;

    sget-object v2, LX/Ass;->b:Ljava/lang/String;

    const-string v3, "Shutter button clicked in PREVIEW mode"

    invoke-static {v2, v3}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v2

    invoke-virtual {v2}, LX/0VK;->g()LX/0VG;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/03V;->a(LX/0VG;)V

    goto :goto_0

    .line 1720865
    :cond_1
    invoke-interface {v2}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0io;

    invoke-static {v3}, LX/2rf;->b(LX/0io;)Lcom/facebook/composer/attachments/ComposerAttachment;

    move-result-object v3

    if-nez v3, :cond_2

    const/4 v3, 0x1

    :goto_2
    invoke-static {v3}, LX/0PB;->checkState(Z)V

    move-object v3, v2

    .line 1720866
    check-cast v3, LX/0im;

    invoke-interface {v3}, LX/0im;->c()LX/0jJ;

    move-result-object v3

    sget-object p1, LX/Ass;->a:LX/0jK;

    invoke-virtual {v3, p1}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v3

    check-cast v3, LX/0jL;

    invoke-interface {v2}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v2}, Lcom/facebook/composer/system/model/ComposerModelImpl;->j()Lcom/facebook/friendsharing/inspiration/model/CameraState;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/friendsharing/inspiration/model/CameraState;->a(Lcom/facebook/friendsharing/inspiration/model/CameraState;)Lcom/facebook/friendsharing/inspiration/model/CameraState$Builder;

    move-result-object v2

    sget-object p1, LX/86q;->CAPTURE_PHOTO_REQUESTED:LX/86q;

    invoke-virtual {v2, p1}, Lcom/facebook/friendsharing/inspiration/model/CameraState$Builder;->setCaptureState(LX/86q;)Lcom/facebook/friendsharing/inspiration/model/CameraState$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/friendsharing/inspiration/model/CameraState$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/CameraState;

    move-result-object v2

    invoke-virtual {v3, v2}, LX/0jL;->a(Lcom/facebook/friendsharing/inspiration/model/CameraState;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0jL;

    invoke-virtual {v2}, LX/0jL;->a()V

    goto :goto_1

    .line 1720867
    :cond_2
    const/4 v3, 0x0

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
