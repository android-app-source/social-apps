.class public final LX/C8Y;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/0Px;

.field public final synthetic b:LX/1Vl;


# direct methods
.method public constructor <init>(LX/1Vl;LX/0Px;)V
    .locals 0

    .prologue
    .line 1853076
    iput-object p1, p0, LX/C8Y;->b:LX/1Vl;

    iput-object p2, p0, LX/C8Y;->a:LX/0Px;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x31c0cac4

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1853077
    iget-object v1, p0, LX/C8Y;->b:LX/1Vl;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v1, v2}, LX/1Vl;->a$redex0(LX/1Vl;Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1853078
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 1853079
    iget-object v2, p0, LX/C8Y;->a:LX/0Px;

    const/4 p1, 0x0

    .line 1853080
    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v2, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/model/GraphQLOpenGraphAction;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLOpenGraphAction;->j()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_3

    :cond_0
    const/4 v3, 0x0

    :goto_0
    move-object v2, v3

    .line 1853081
    if-eqz v2, :cond_2

    invoke-static {v2}, LX/17d;->a(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1853082
    invoke-static {}, LX/47I;->e()LX/47H;

    move-result-object v3

    .line 1853083
    iput-object v2, v3, LX/47H;->a:Ljava/lang/String;

    .line 1853084
    move-object v2, v3

    .line 1853085
    invoke-virtual {v2}, LX/47H;->a()LX/47I;

    move-result-object v2

    .line 1853086
    iget-object v3, p0, LX/C8Y;->b:LX/1Vl;

    iget-object v3, v3, LX/1Vl;->c:LX/17W;

    invoke-virtual {v3, v1, v2}, LX/17W;->a(Landroid/content/Context;LX/47I;)Z

    .line 1853087
    iget-object v2, p0, LX/C8Y;->b:LX/1Vl;

    iget-object v2, v2, LX/1Vl;->e:LX/1Vn;

    .line 1853088
    iget-object v3, v2, LX/1Vn;->a:LX/0Zb;

    new-instance p1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p0, "ig_cta_in_fb4a_launch_ig_with_deep_linking"

    invoke-direct {p1, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    invoke-interface {v3, p1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1853089
    :goto_1
    const v1, -0x1f773eae

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 1853090
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 1853091
    iget-object v2, p0, LX/C8Y;->b:LX/1Vl;

    iget-object v2, v2, LX/1Vl;->g:LX/1Vo;

    .line 1853092
    iget-object v3, v2, LX/1Vo;->a:LX/0Uh;

    const/16 v4, 0xe1

    const/4 p1, 0x1

    invoke-virtual {v3, v4, p1}, LX/0Uh;->a(IZ)Z

    move-result v3

    move v2, v3

    .line 1853093
    if-eqz v2, :cond_4

    const-string v2, "market://details?id=com.instagram.android&referrer=utm_campaign%%3D%s%%26utm_source%%3D%s"

    :goto_2
    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string p1, "bar_cta"

    aput-object p1, v3, v4

    const/4 v4, 0x1

    const-string p1, "text_link"

    aput-object p1, v3, v4

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1853094
    iget-object v3, p0, LX/C8Y;->b:LX/1Vl;

    iget-object v3, v3, LX/1Vl;->b:LX/1Vr;

    invoke-virtual {v3, v2, v1}, LX/1Vr;->a(Ljava/lang/String;Landroid/content/Context;)V

    .line 1853095
    iget-object v2, p0, LX/C8Y;->b:LX/1Vl;

    iget-object v2, v2, LX/1Vl;->e:LX/1Vn;

    .line 1853096
    iget-object v3, v2, LX/1Vn;->a:LX/0Zb;

    new-instance v4, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p1, "ig_cta_in_fb4a_launch_play_store"

    invoke-direct {v4, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    invoke-interface {v3, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1853097
    goto :goto_1

    .line 1853098
    :cond_2
    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 1853099
    iget-object v3, p0, LX/C8Y;->b:LX/1Vl;

    iget-object v3, v3, LX/1Vl;->d:LX/17U;

    const-string p1, "com.instagram.android"

    invoke-virtual {v2, p1}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v3, v2, v1}, LX/17U;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1853100
    iget-object v2, p0, LX/C8Y;->b:LX/1Vl;

    iget-object v2, v2, LX/1Vl;->e:LX/1Vn;

    .line 1853101
    iget-object v3, v2, LX/1Vn;->a:LX/0Zb;

    new-instance p1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p0, "ig_cta_in_fb4a_launch_ig_without_deep_linking"

    invoke-direct {p1, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    invoke-interface {v3, p1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1853102
    goto :goto_1

    :cond_3
    invoke-virtual {v2, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/model/GraphQLOpenGraphAction;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLOpenGraphAction;->j()LX/0Px;

    move-result-object v3

    invoke-virtual {v3, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    goto/16 :goto_0

    .line 1853103
    :cond_4
    const-string v2, "http://instagram.com/download?campaign=%s&source=%s"

    goto :goto_2
.end method
