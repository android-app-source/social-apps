.class public LX/Avf;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/89q;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/89q",
        "<",
        "Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/Avf;


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            ">;"
        }
    .end annotation
.end field

.field public volatile b:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0fO;",
            ">;"
        }
    .end annotation
.end field

.field public volatile c:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1724511
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1724512
    return-void
.end method

.method public static a(LX/0QB;)LX/Avf;
    .locals 6

    .prologue
    .line 1724470
    sget-object v0, LX/Avf;->d:LX/Avf;

    if-nez v0, :cond_1

    .line 1724471
    const-class v1, LX/Avf;

    monitor-enter v1

    .line 1724472
    :try_start_0
    sget-object v0, LX/Avf;->d:LX/Avf;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1724473
    if-eqz v2, :cond_0

    .line 1724474
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1724475
    new-instance v3, LX/Avf;

    invoke-direct {v3}, LX/Avf;-><init>()V

    .line 1724476
    const/16 v4, 0x271

    invoke-static {v0, v4}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 v5, 0xa86

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 p0, 0x2e26

    invoke-static {v0, p0}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    .line 1724477
    iput-object v4, v3, LX/Avf;->a:LX/0Or;

    iput-object v5, v3, LX/Avf;->b:LX/0Or;

    iput-object p0, v3, LX/Avf;->c:LX/0Or;

    .line 1724478
    move-object v0, v3

    .line 1724479
    sput-object v0, LX/Avf;->d:LX/Avf;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1724480
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1724481
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1724482
    :cond_1
    sget-object v0, LX/Avf;->d:LX/Avf;

    return-object v0

    .line 1724483
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1724484
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;LX/89p;)V
    .locals 5
    .param p2    # LX/89p;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1724485
    iget-object v0, p0, LX/Avf;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader;

    .line 1724486
    iget-object v1, p0, LX/Avf;->b:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    .line 1724487
    iget-object v1, p0, LX/Avf;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Sh;

    .line 1724488
    new-instance v2, LX/Ave;

    invoke-direct {v2, p0, v1, p2}, LX/Ave;-><init>(LX/Avf;LX/0Sh;LX/89p;)V

    new-instance v1, LX/BGK;

    invoke-direct {v1, p1}, LX/BGK;-><init>(Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;)V

    .line 1724489
    new-instance v3, LX/BGL;

    iget-object v4, v1, LX/BGK;->a:Ljava/lang/String;

    iget-object p0, v1, LX/BGK;->b:LX/0Pz;

    invoke-virtual {p0}, LX/0Pz;->b()LX/0Px;

    move-result-object p0

    iget-object p1, v1, LX/BGK;->c:Ljava/lang/String;

    invoke-direct {v3, v4, p0, p1}, LX/BGL;-><init>(Ljava/lang/String;LX/0Px;Ljava/lang/String;)V

    move-object v1, v3

    .line 1724490
    const/4 v4, 0x1

    const/4 p0, 0x0

    .line 1724491
    iget-object v3, v1, LX/BGL;->b:LX/0Px;

    move-object v3, v3

    .line 1724492
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    const/4 p1, 0x3

    if-ne v3, p1, :cond_1

    move v3, v4

    :goto_0
    invoke-static {v3}, LX/0Tp;->b(Z)V

    .line 1724493
    new-instance p1, LX/BGE;

    .line 1724494
    iget-object v3, v1, LX/BGL;->a:Ljava/lang/String;

    move-object v3, v3

    .line 1724495
    invoke-direct {p1, v3}, LX/BGE;-><init>(Ljava/lang/String;)V

    const-string p2, "face_align_model.bin"

    .line 1724496
    iget-object v3, v1, LX/BGL;->b:LX/0Px;

    move-object v3, v3

    .line 1724497
    invoke-virtual {v3, p0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {p1, p2, v3}, LX/BGE;->a(LX/BGE;Ljava/lang/String;Ljava/lang/String;)LX/BGE;

    move-result-object p0

    const-string p1, "face_detect_model.bin"

    .line 1724498
    iget-object v3, v1, LX/BGL;->b:LX/0Px;

    move-object v3, v3

    .line 1724499
    invoke-virtual {v3, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {p0, p1, v3}, LX/BGE;->a(LX/BGE;Ljava/lang/String;Ljava/lang/String;)LX/BGE;

    move-result-object v4

    const-string p0, "pdm_multires.bin"

    .line 1724500
    iget-object v3, v1, LX/BGL;->b:LX/0Px;

    move-object v3, v3

    .line 1724501
    const/4 p1, 0x2

    invoke-virtual {v3, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v4, p0, v3}, LX/BGE;->a(LX/BGE;Ljava/lang/String;Ljava/lang/String;)LX/BGE;

    move-result-object v3

    .line 1724502
    iget-object v4, v1, LX/BGL;->c:Ljava/lang/String;

    move-object v4, v4

    .line 1724503
    if-eqz v4, :cond_0

    .line 1724504
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object p0, v1, LX/BGL;->a:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string p0, ".msqrd"

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v4, v4

    .line 1724505
    iget-object p0, v1, LX/BGL;->c:Ljava/lang/String;

    move-object p0, p0

    .line 1724506
    invoke-static {v3, v4, p0}, LX/BGE;->a(LX/BGE;Ljava/lang/String;Ljava/lang/String;)LX/BGE;

    .line 1724507
    :cond_0
    move-object v1, v3

    .line 1724508
    iget-object v3, v0, Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader;->e:Ljava/util/concurrent/ExecutorService;

    new-instance v4, Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader$1;

    invoke-direct {v4, v0, v2, v1}, Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader$1;-><init>(Lcom/facebook/photos/creativeediting/msqrd/fetch/MsqrdAssetDownloader;LX/Ave;LX/BGE;)V

    const p0, -0x2b6dac7a

    invoke-static {v3, v4, p0}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1724509
    return-void

    :cond_1
    move v3, p0

    .line 1724510
    goto :goto_0
.end method
