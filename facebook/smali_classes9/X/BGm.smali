.class public final LX/BGm;
.super LX/0Vd;
.source ""


# instance fields
.field public final synthetic a:LX/0Px;

.field public final synthetic b:Lcom/facebook/photos/simplepicker/SimplePickerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/simplepicker/SimplePickerFragment;LX/0Px;)V
    .locals 0

    .prologue
    .line 1767460
    iput-object p1, p0, LX/BGm;->b:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    iput-object p2, p0, LX/BGm;->a:LX/0Px;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1767461
    iget-object v0, p0, LX/BGm;->b:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    iget-object v0, v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->aq:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v1, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->s:Ljava/lang/String;

    const-string v2, "Unable to fetch tagging data"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1767462
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 1767463
    iget-object v0, p0, LX/BGm;->b:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    iget-object v1, p0, LX/BGm;->a:LX/0Px;

    .line 1767464
    iget-object v2, v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->b:Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    .line 1767465
    iget-object v3, v2, Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;->d:Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;

    move-object v2, v3

    .line 1767466
    iget-boolean v3, v2, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->t:Z

    move v2, v3

    .line 1767467
    iget-boolean v3, v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->o:Z

    if-nez v3, :cond_2

    if-eqz v2, :cond_2

    const/4 v3, 0x0

    .line 1767468
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result p1

    move v4, v3

    :goto_0
    if-ge v4, p1, :cond_5

    invoke-virtual {v1, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/ipc/media/MediaItem;

    .line 1767469
    instance-of v0, v2, Lcom/facebook/photos/base/media/PhotoItem;

    if-eqz v0, :cond_4

    .line 1767470
    invoke-virtual {v2}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2}, LX/BGa;->a(Landroid/net/Uri;)I

    move-result v2

    const/4 v4, -0x1

    if-eq v2, v4, :cond_3

    const/4 v2, 0x1

    .line 1767471
    :goto_1
    move v2, v2

    .line 1767472
    if-eqz v2, :cond_2

    const/4 v2, 0x1

    :goto_2
    move v0, v2

    .line 1767473
    if-eqz v0, :cond_1

    .line 1767474
    new-instance v0, LX/BGl;

    invoke-direct {v0, p0}, LX/BGl;-><init>(LX/BGm;)V

    .line 1767475
    iget-object v1, p0, LX/BGm;->b:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    .line 1767476
    new-instance v2, LX/0ju;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, LX/0ju;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f08138d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v2

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f08138e

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v2

    const v3, 0x7f081390

    new-instance v4, LX/BGu;

    invoke-direct {v4, v1, v0}, LX/BGu;-><init>(Lcom/facebook/photos/simplepicker/SimplePickerFragment;LX/BGl;)V

    invoke-virtual {v2, v3, v4}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v2

    const v3, 0x7f08138f

    new-instance v4, LX/BGt;

    invoke-direct {v4, v1, v0}, LX/BGt;-><init>(Lcom/facebook/photos/simplepicker/SimplePickerFragment;LX/BGl;)V

    invoke-virtual {v2, v3, v4}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v2

    const v3, 0x1080027

    invoke-virtual {v2, v3}, LX/0ju;->c(I)LX/0ju;

    move-result-object v2

    invoke-virtual {v2}, LX/0ju;->b()LX/2EJ;

    .line 1767477
    :cond_0
    :goto_3
    return-void

    .line 1767478
    :cond_1
    iget-object v0, p0, LX/BGm;->b:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    iget-object v0, v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->x:LX/BHA;

    if-eqz v0, :cond_0

    .line 1767479
    iget-object v0, p0, LX/BGm;->b:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    iget-object v0, v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->x:LX/BHA;

    iget-object v1, p0, LX/BGm;->a:LX/0Px;

    iget-object v2, p0, LX/BGm;->b:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    iget-boolean v2, v2, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->V:Z

    invoke-virtual {v0, v1, v2}, LX/BHA;->b(LX/0Px;Z)V

    goto :goto_3

    :cond_2
    const/4 v2, 0x0

    goto :goto_2

    :cond_3
    move v2, v3

    .line 1767480
    goto :goto_1

    .line 1767481
    :cond_4
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto/16 :goto_0

    :cond_5
    move v2, v3

    .line 1767482
    goto :goto_1
.end method
