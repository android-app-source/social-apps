.class public LX/BIH;
.super LX/BIG;
.source ""

# interfaces
.implements LX/BIE;


# instance fields
.field public d:Landroid/widget/ImageView;

.field public e:Z

.field private f:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1770185
    invoke-direct {p0, p1}, LX/BIG;-><init>(Landroid/content/Context;)V

    .line 1770186
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/BIH;->e:Z

    .line 1770187
    invoke-direct {p0}, LX/BIH;->i()V

    .line 1770188
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;B)V
    .locals 1
    .param p2    # B
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1770181
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/BIG;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1770182
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/BIH;->e:Z

    .line 1770183
    invoke-direct {p0}, LX/BIH;->i()V

    .line 1770184
    return-void
.end method

.method private i()V
    .locals 1

    .prologue
    .line 1770178
    iget-object v0, p0, LX/BIH;->f:Landroid/view/View;

    if-nez v0, :cond_0

    .line 1770179
    const v0, 0x7f0d2515

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/BIH;->f:Landroid/view/View;

    .line 1770180
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1770176
    iget-object v0, p0, LX/BIH;->f:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1770177
    return-void
.end method

.method public final a(F)V
    .locals 2

    .prologue
    .line 1770189
    iget-object v0, p0, LX/BIH;->f:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setAlpha(F)V

    .line 1770190
    iget-object v0, p0, LX/BIH;->f:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1770191
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 1770163
    invoke-super {p0}, LX/BIG;->b()V

    .line 1770164
    iget-object v0, p0, LX/BIG;->c:Lcom/facebook/ipc/media/MediaItem;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/BIG;->c:Lcom/facebook/ipc/media/MediaItem;

    instance-of v0, v0, Lcom/facebook/photos/base/media/PhotoItem;

    if-eqz v0, :cond_2

    .line 1770165
    iget-object v0, p0, LX/BIH;->d:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/BIG;->c:Lcom/facebook/ipc/media/MediaItem;

    check-cast v0, Lcom/facebook/photos/base/media/PhotoItem;

    .line 1770166
    iget-boolean v1, v0, Lcom/facebook/photos/base/media/PhotoItem;->d:Z

    move v0, v1

    .line 1770167
    if-nez v0, :cond_0

    .line 1770168
    iget-object v0, p0, LX/BIH;->d:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1770169
    :cond_0
    iget-object v0, p0, LX/BIG;->c:Lcom/facebook/ipc/media/MediaItem;

    check-cast v0, Lcom/facebook/photos/base/media/PhotoItem;

    .line 1770170
    iget-boolean v1, v0, Lcom/facebook/photos/base/media/PhotoItem;->d:Z

    move v0, v1

    .line 1770171
    if-eqz v0, :cond_2

    iget-boolean v0, p0, LX/BIH;->e:Z

    if-eqz v0, :cond_2

    .line 1770172
    iget-object v0, p0, LX/BIH;->d:Landroid/widget/ImageView;

    if-nez v0, :cond_1

    .line 1770173
    const v0, 0x7f0d2518

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/BIH;->d:Landroid/widget/ImageView;

    .line 1770174
    :cond_1
    iget-object v0, p0, LX/BIH;->d:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1770175
    :cond_2
    return-void
.end method

.method public getHighlightLayerView()Landroid/view/View;
    .locals 1

    .prologue
    .line 1770162
    iget-object v0, p0, LX/BIH;->f:Landroid/view/View;

    return-object v0
.end method

.method public getItemType()LX/BHH;
    .locals 1

    .prologue
    .line 1770161
    sget-object v0, LX/BHH;->PHOTO:LX/BHH;

    return-object v0
.end method

.method public getLayoutResourceId()I
    .locals 1

    .prologue
    .line 1770160
    const v0, 0x7f030f5a

    return v0
.end method
