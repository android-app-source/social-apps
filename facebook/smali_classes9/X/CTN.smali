.class public final LX/CTN;
.super LX/CTK;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:J

.field public final b:J

.field public final c:J

.field public final d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;LX/CSS;Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1895716
    invoke-direct {p0, p1, p2, p3}, LX/CTK;-><init>(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;LX/CSS;Ljava/lang/String;)V

    .line 1895717
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;->ab()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v4, 0x1b46b811

    invoke-static {v1, v0, v4}, LX/3Sm;->a(LX/15i;II)LX/1vs;

    .line 1895718
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;->Z()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v4, -0x7969ad84

    invoke-static {v1, v0, v4}, LX/3Sm;->a(LX/15i;II)LX/1vs;

    .line 1895719
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;->H()LX/2uF;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1895720
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;->ab()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v1, v0, v3}, LX/15i;->k(II)J

    move-result-wide v0

    iput-wide v0, p0, LX/CTN;->a:J

    .line 1895721
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;->Z()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v1, v0, v3}, LX/15i;->k(II)J

    move-result-wide v0

    iput-wide v0, p0, LX/CTN;->b:J

    .line 1895722
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;->ai()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DatePickerFormFieldFragmentModel$TimeSelectedModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;->ai()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DatePickerFormFieldFragmentModel$TimeSelectedModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DatePickerFormFieldFragmentModel$TimeSelectedModel;->a()J

    move-result-wide v0

    :goto_0
    iput-wide v0, p0, LX/CTN;->c:J

    .line 1895723
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/CTN;->d:Ljava/util/ArrayList;

    .line 1895724
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;->H()LX/2uF;

    move-result-object v0

    invoke-virtual {v0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 1895725
    invoke-virtual {v4, v0, v3}, LX/15i;->k(II)J

    move-result-wide v4

    .line 1895726
    iget-wide v6, p0, LX/CTN;->a:J

    cmp-long v0, v6, v4

    if-gtz v0, :cond_1

    move v0, v2

    :goto_2
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1895727
    iget-wide v6, p0, LX/CTN;->b:J

    cmp-long v0, v4, v6

    if-gtz v0, :cond_2

    move v0, v2

    :goto_3
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1895728
    iget-object v0, p0, LX/CTN;->d:Ljava/util/ArrayList;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1895729
    :cond_0
    iget-wide v0, p0, LX/CTN;->a:J

    goto :goto_0

    :cond_1
    move v0, v3

    .line 1895730
    goto :goto_2

    :cond_2
    move v0, v3

    .line 1895731
    goto :goto_3

    .line 1895732
    :cond_3
    iget-wide v0, p0, LX/CTN;->a:J

    iget-wide v4, p0, LX/CTN;->b:J

    cmp-long v0, v0, v4

    if-gtz v0, :cond_4

    move v0, v2

    :goto_4
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1895733
    iget-wide v0, p0, LX/CTN;->a:J

    iget-wide v4, p0, LX/CTN;->c:J

    cmp-long v0, v0, v4

    if-gtz v0, :cond_5

    move v0, v2

    :goto_5
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1895734
    iget-wide v0, p0, LX/CTN;->c:J

    iget-wide v4, p0, LX/CTN;->b:J

    cmp-long v0, v0, v4

    if-gtz v0, :cond_6

    :goto_6
    invoke-static {v2}, LX/0PB;->checkState(Z)V

    .line 1895735
    return-void

    :cond_4
    move v0, v3

    .line 1895736
    goto :goto_4

    :cond_5
    move v0, v3

    .line 1895737
    goto :goto_5

    :cond_6
    move v2, v3

    .line 1895738
    goto :goto_6
.end method
