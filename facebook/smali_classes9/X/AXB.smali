.class public LX/AXB;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1683781
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1683782
    return-void
.end method

.method public static a(Landroid/content/res/Resources;JLcom/facebook/ipc/composer/intent/ComposerTargetData;Ljava/lang/String;)Ljava/lang/CharSequence;
    .locals 5
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1683783
    invoke-static {p1, p2}, LX/3Hk;->a(J)Ljava/lang/String;

    move-result-object v0

    .line 1683784
    sget-object v1, LX/AXA;->a:[I

    iget-object v2, p3, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    invoke-virtual {v2}, LX/2rw;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1683785
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1683786
    :pswitch_0
    if-nez p4, :cond_0

    .line 1683787
    const v1, 0x7f080c95

    new-array v2, v4, [Ljava/lang/Object;

    aput-object v0, v2, v3

    invoke-virtual {p0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1683788
    :cond_0
    invoke-static {p4, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1683789
    :pswitch_1
    const v1, 0x7f080c97

    new-array v2, v4, [Ljava/lang/Object;

    aput-object v0, v2, v3

    invoke-virtual {p0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1683790
    :pswitch_2
    const v1, 0x7f080c98

    new-array v2, v4, [Ljava/lang/Object;

    aput-object v0, v2, v3

    invoke-virtual {p0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1683791
    :pswitch_3
    const v1, 0x7f080c96

    new-array v2, v4, [Ljava/lang/Object;

    aput-object v0, v2, v3

    invoke-virtual {p0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
