.class public LX/COk;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/COi;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/COm;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1883553
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/COk;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/COm;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1883467
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1883468
    iput-object p1, p0, LX/COk;->b:LX/0Ot;

    .line 1883469
    return-void
.end method

.method public static a(LX/0QB;)LX/COk;
    .locals 4

    .prologue
    .line 1883470
    const-class v1, LX/COk;

    monitor-enter v1

    .line 1883471
    :try_start_0
    sget-object v0, LX/COk;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1883472
    sput-object v2, LX/COk;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1883473
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1883474
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1883475
    new-instance v3, LX/COk;

    const/16 p0, 0x2a90

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/COk;-><init>(LX/0Ot;)V

    .line 1883476
    move-object v0, v3

    .line 1883477
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1883478
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/COk;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1883479
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1883480
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1883481
    invoke-static {}, LX/1dS;->b()V

    .line 1883482
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/1De;LX/1Dg;IILX/1no;LX/1X1;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x46fb0f97

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1883483
    check-cast p6, LX/COj;

    .line 1883484
    iget-object v1, p0, LX/COk;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v1, p6, LX/COj;->a:Lcom/facebook/graphql/model/GraphQLVideo;

    invoke-static {p3, p4, p5, v1}, LX/COm;->a(IILX/1no;Lcom/facebook/graphql/model/GraphQLVideo;)V

    .line 1883485
    const/16 v1, 0x1f

    const v2, -0x1e142a87

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(LX/1De;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1883486
    iget-object v0, p0, LX/COk;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    .line 1883487
    new-instance v0, LX/7LN;

    invoke-direct {v0}, LX/7LN;-><init>()V

    invoke-virtual {v0, p1}, LX/7LN;->a(Landroid/content/Context;)Lcom/facebook/video/player/RichVideoPlayer;

    move-result-object v0

    move-object v0, v0

    .line 1883488
    return-object v0
.end method

.method public final b(LX/1De;LX/1X1;)V
    .locals 9

    .prologue
    .line 1883489
    check-cast p2, LX/COj;

    .line 1883490
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v1

    .line 1883491
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v2

    .line 1883492
    iget-object v0, p0, LX/COk;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/COm;

    iget-object v3, p2, LX/COj;->a:Lcom/facebook/graphql/model/GraphQLVideo;

    .line 1883493
    new-instance v4, LX/2oE;

    invoke-direct {v4}, LX/2oE;-><init>()V

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLVideo;->az()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 1883494
    iput-object v5, v4, LX/2oE;->a:Landroid/net/Uri;

    .line 1883495
    move-object v4, v4

    .line 1883496
    sget-object v5, LX/097;->FROM_STREAM:LX/097;

    .line 1883497
    iput-object v5, v4, LX/2oE;->e:LX/097;

    .line 1883498
    move-object v4, v4

    .line 1883499
    invoke-virtual {v4}, LX/2oE;->h()Lcom/facebook/video/engine/VideoDataSource;

    move-result-object v4

    .line 1883500
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLVideo;->N()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    invoke-static {v5}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v5

    invoke-static {v5}, LX/1bf;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v5

    .line 1883501
    new-instance v6, LX/2oH;

    invoke-direct {v6}, LX/2oH;-><init>()V

    invoke-virtual {v6, v4}, LX/2oH;->a(Lcom/facebook/video/engine/VideoDataSource;)LX/2oH;

    move-result-object v4

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLVideo;->aa()Z

    move-result v6

    .line 1883502
    iput-boolean v6, v4, LX/2oH;->h:Z

    .line 1883503
    move-object v4, v4

    .line 1883504
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLVideo;->ay()I

    move-result v6

    .line 1883505
    iput v6, v4, LX/2oH;->c:I

    .line 1883506
    move-object v4, v4

    .line 1883507
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLVideo;->q()I

    move-result v6

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLVideo;->D()I

    move-result v7

    invoke-virtual {v4, v6, v7}, LX/2oH;->a(II)LX/2oH;

    move-result-object v4

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLVideo;->m()I

    move-result v6

    .line 1883508
    iput v6, v4, LX/2oH;->l:I

    .line 1883509
    move-object v4, v4

    .line 1883510
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLVideo;->ag()Z

    move-result v6

    .line 1883511
    iput-boolean v6, v4, LX/2oH;->u:Z

    .line 1883512
    move-object v4, v4

    .line 1883513
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLVideo;->aa()Z

    move-result v6

    .line 1883514
    iput-boolean v6, v4, LX/2oH;->h:Z

    .line 1883515
    move-object v4, v4

    .line 1883516
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLVideo;->r()Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-result-object v6

    .line 1883517
    iput-object v6, v4, LX/2oH;->i:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    .line 1883518
    move-object v4, v4

    .line 1883519
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLVideo;->am()I

    move-result v6

    .line 1883520
    iput v6, v4, LX/2oH;->r:I

    .line 1883521
    move-object v4, v4

    .line 1883522
    invoke-virtual {v4}, LX/2oH;->n()Lcom/facebook/video/engine/VideoPlayerParams;

    move-result-object v4

    .line 1883523
    new-instance v6, LX/2pZ;

    invoke-direct {v6}, LX/2pZ;-><init>()V

    .line 1883524
    iput-object v4, v6, LX/2pZ;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 1883525
    move-object v4, v6

    .line 1883526
    const-string v6, "CoverImageParamsKey"

    invoke-virtual {v4, v6, v5}, LX/2pZ;->a(Ljava/lang/String;Ljava/lang/Object;)LX/2pZ;

    move-result-object v4

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLVideo;->bh()I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLVideo;->F()I

    move-result v6

    int-to-float v6, v6

    div-float/2addr v5, v6

    float-to-double v6, v5

    .line 1883527
    iput-wide v6, v4, LX/2pZ;->e:D

    .line 1883528
    move-object v4, v4

    .line 1883529
    invoke-virtual {v4}, LX/2pZ;->b()LX/2pa;

    move-result-object v4

    .line 1883530
    iput-object v4, v1, LX/1np;->a:Ljava/lang/Object;

    .line 1883531
    new-instance v4, LX/COl;

    invoke-direct {v4, v0, v3, p1}, LX/COl;-><init>(LX/COm;Lcom/facebook/graphql/model/GraphQLVideo;LX/1De;)V

    .line 1883532
    iput-object v4, v2, LX/1np;->a:Ljava/lang/Object;

    .line 1883533
    iget-object v0, v1, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 1883534
    check-cast v0, LX/2pa;

    iput-object v0, p2, LX/COj;->b:LX/2pa;

    .line 1883535
    invoke-static {v1}, LX/1cy;->a(LX/1np;)V

    .line 1883536
    iget-object v0, v2, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 1883537
    check-cast v0, Landroid/view/View$OnClickListener;

    iput-object v0, p2, LX/COj;->c:Landroid/view/View$OnClickListener;

    .line 1883538
    invoke-static {v2}, LX/1cy;->a(LX/1np;)V

    .line 1883539
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1883540
    const/4 v0, 0x1

    return v0
.end method

.method public final e(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 3

    .prologue
    .line 1883541
    check-cast p3, LX/COj;

    .line 1883542
    iget-object v0, p0, LX/COk;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/COm;

    check-cast p2, Lcom/facebook/video/player/RichVideoPlayer;

    iget-object v1, p3, LX/COj;->b:LX/2pa;

    iget-object v2, p3, LX/COj;->c:Landroid/view/View$OnClickListener;

    .line 1883543
    invoke-virtual {p2, v2}, Lcom/facebook/video/player/RichVideoPlayer;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1883544
    iget-object p0, v0, LX/COm;->a:LX/2mu;

    const/4 p1, 0x1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {p0, p1}, LX/2mu;->a(Ljava/lang/Boolean;)Lcom/facebook/feedplugins/video/richvideoplayer/InlineRichVideoPlayerPluginSelector;

    move-result-object p0

    const/4 p1, 0x0

    invoke-virtual {p0, p2, v1, p1}, LX/3Ge;->a(Lcom/facebook/video/player/RichVideoPlayer;LX/2pa;LX/7Lf;)Lcom/facebook/video/player/RichVideoPlayer;

    .line 1883545
    invoke-virtual {p2, v1}, Lcom/facebook/video/player/RichVideoPlayer;->c(LX/2pa;)V

    .line 1883546
    return-void
.end method

.method public final f()LX/1mv;
    .locals 1

    .prologue
    .line 1883547
    sget-object v0, LX/1mv;->VIEW:LX/1mv;

    return-object v0
.end method

.method public final f(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 1

    .prologue
    .line 1883548
    iget-object v0, p0, LX/COk;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    check-cast p2, Lcom/facebook/video/player/RichVideoPlayer;

    .line 1883549
    invoke-virtual {p2}, Lcom/facebook/video/player/RichVideoPlayer;->i()V

    .line 1883550
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Lcom/facebook/video/player/RichVideoPlayer;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1883551
    return-void
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 1883552
    const/16 v0, 0xf

    return v0
.end method
