.class public final LX/C16;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLQuestionOption;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/C17;


# direct methods
.method public constructor <init>(LX/C17;Lcom/facebook/graphql/model/GraphQLQuestionOption;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1841908
    iput-object p1, p0, LX/C16;->c:LX/C17;

    iput-object p2, p0, LX/C16;->a:Lcom/facebook/graphql/model/GraphQLQuestionOption;

    iput-object p3, p0, LX/C16;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x5f23cb84

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1841909
    iget-object v1, p0, LX/C16;->c:LX/C17;

    iget-object v1, v1, LX/C17;->b:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    iget-object v2, p0, LX/C16;->a:Lcom/facebook/graphql/model/GraphQLQuestionOption;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLQuestionOption;->j()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->b(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 1841910
    iget-object v2, p0, LX/C16;->b:Ljava/lang/String;

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1841911
    const-string v2, "fragment_title"

    iget-object v3, p0, LX/C16;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1841912
    :cond_0
    iget-object v2, p0, LX/C16;->c:LX/C17;

    iget-object v2, v2, LX/C17;->c:Lcom/facebook/content/SecureContextHelper;

    iget-object v3, p0, LX/C16;->c:LX/C17;

    iget-object v3, v3, LX/C17;->a:Landroid/content/Context;

    invoke-interface {v2, v1, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1841913
    const v1, -0x391604ab

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
