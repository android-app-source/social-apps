.class public LX/CIw;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/CIu;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/maps/rows/FbStaticMapComponentSpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1874936
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/CIw;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/maps/rows/FbStaticMapComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1874937
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1874938
    iput-object p1, p0, LX/CIw;->b:LX/0Ot;

    .line 1874939
    return-void
.end method

.method public static a(LX/0QB;)LX/CIw;
    .locals 4

    .prologue
    .line 1874940
    const-class v1, LX/CIw;

    monitor-enter v1

    .line 1874941
    :try_start_0
    sget-object v0, LX/CIw;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1874942
    sput-object v2, LX/CIw;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1874943
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1874944
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1874945
    new-instance v3, LX/CIw;

    const/16 p0, 0x263c

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/CIw;-><init>(LX/0Ot;)V

    .line 1874946
    move-object v0, v3

    .line 1874947
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1874948
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/CIw;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1874949
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1874950
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1874956
    invoke-static {}, LX/1dS;->b()V

    .line 1874957
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/1De;LX/1Dg;IILX/1no;LX/1X1;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x21e2b4bb

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1874951
    check-cast p6, LX/CIv;

    .line 1874952
    iget-object v1, p0, LX/CIw;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    iget v1, p6, LX/CIv;->a:I

    iget v2, p6, LX/CIv;->b:I

    .line 1874953
    iput v1, p5, LX/1no;->a:I

    .line 1874954
    iput v2, p5, LX/1no;->b:I

    .line 1874955
    const/16 v1, 0x1f

    const v2, 0x5a8ee4b4

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(LX/1De;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1874933
    iget-object v0, p0, LX/CIw;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    .line 1874934
    new-instance v0, Lcom/facebook/maps/FbStaticMapView;

    invoke-direct {v0, p1}, Lcom/facebook/maps/FbStaticMapView;-><init>(Landroid/content/Context;)V

    move-object v0, v0

    .line 1874935
    return-object v0
.end method

.method public final b(LX/1De;LX/1X1;)V
    .locals 13

    .prologue
    .line 1874916
    check-cast p2, LX/CIv;

    .line 1874917
    iget-object v0, p0, LX/CIw;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/maps/rows/FbStaticMapComponentSpec;

    iget v2, p2, LX/CIv;->a:I

    iget v3, p2, LX/CIv;->b:I

    iget-object v4, p2, LX/CIv;->c:Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    iget-object v5, p2, LX/CIv;->d:LX/1Pt;

    iget-boolean v6, p2, LX/CIv;->e:Z

    move-object v1, p1

    .line 1874918
    if-nez v6, :cond_0

    .line 1874919
    :goto_0
    return-void

    .line 1874920
    :cond_0
    const/4 v9, 0x2

    invoke-virtual {v1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    iget-object v7, v0, Lcom/facebook/maps/rows/FbStaticMapComponentSpec;->b:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/0W9;

    invoke-virtual {v7}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v7

    invoke-static {v7}, LX/0sI;->a(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v11

    move v7, v2

    move v8, v3

    move-object v12, v4

    invoke-static/range {v7 .. v12}, LX/3BP;->a(IIILandroid/content/res/Resources;Ljava/lang/String;Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;)Landroid/net/Uri;

    move-result-object v7

    .line 1874921
    invoke-static {v7}, LX/1bf;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v7

    sget-object v8, Lcom/facebook/maps/rows/FbStaticMapComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-interface {v5, v7, v8}, LX/1Pt;->a(LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1874932
    const/4 v0, 0x1

    return v0
.end method

.method public final e(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 6

    .prologue
    .line 1874924
    check-cast p3, LX/CIv;

    .line 1874925
    iget-object v0, p0, LX/CIw;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-object v1, p2

    check-cast v1, Lcom/facebook/maps/FbStaticMapView;

    iget-object v2, p3, LX/CIv;->c:Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    iget-boolean v3, p3, LX/CIv;->f:Z

    iget v4, p3, LX/CIv;->g:F

    iget v5, p3, LX/CIv;->h:F

    move-object v0, p1

    .line 1874926
    invoke-virtual {v1, v2}, LX/3BP;->setMapOptions(Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;)V

    .line 1874927
    const/16 p0, 0x8

    invoke-virtual {v1, p0}, LX/3BP;->setReportButtonVisibility(I)V

    .line 1874928
    if-eqz v3, :cond_0

    invoke-virtual {v0}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    const p1, 0x7f020e90

    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    .line 1874929
    :goto_0
    invoke-virtual {v1, p0, v4, v5}, LX/3BP;->a(Landroid/graphics/drawable/Drawable;FF)V

    .line 1874930
    return-void

    .line 1874931
    :cond_0
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public final f()LX/1mv;
    .locals 1

    .prologue
    .line 1874923
    sget-object v0, LX/1mv;->VIEW:LX/1mv;

    return-object v0
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 1874922
    const/16 v0, 0xf

    return v0
.end method
