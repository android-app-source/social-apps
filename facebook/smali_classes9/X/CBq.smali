.class public final enum LX/CBq;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/CBq;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/CBq;

.field public static final enum DISCLOSURE:LX/CBq;

.field public static final enum SHARE:LX/CBq;

.field public static final enum UNKNOWN:LX/CBq;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1856861
    new-instance v0, LX/CBq;

    const-string v1, "DISCLOSURE"

    invoke-direct {v0, v1, v2}, LX/CBq;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CBq;->DISCLOSURE:LX/CBq;

    .line 1856862
    new-instance v0, LX/CBq;

    const-string v1, "SHARE"

    invoke-direct {v0, v1, v3}, LX/CBq;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CBq;->SHARE:LX/CBq;

    .line 1856863
    new-instance v0, LX/CBq;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v4}, LX/CBq;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CBq;->UNKNOWN:LX/CBq;

    .line 1856864
    const/4 v0, 0x3

    new-array v0, v0, [LX/CBq;

    sget-object v1, LX/CBq;->DISCLOSURE:LX/CBq;

    aput-object v1, v0, v2

    sget-object v1, LX/CBq;->SHARE:LX/CBq;

    aput-object v1, v0, v3

    sget-object v1, LX/CBq;->UNKNOWN:LX/CBq;

    aput-object v1, v0, v4

    sput-object v0, LX/CBq;->$VALUES:[LX/CBq;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1856860
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)LX/CBq;
    .locals 1

    .prologue
    .line 1856855
    if-nez p0, :cond_0

    .line 1856856
    :try_start_0
    sget-object v0, LX/CBq;->UNKNOWN:LX/CBq;

    .line 1856857
    :goto_0
    return-object v0

    .line 1856858
    :cond_0
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/CBq;->valueOf(Ljava/lang/String;)LX/CBq;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 1856859
    :catch_0
    sget-object v0, LX/CBq;->UNKNOWN:LX/CBq;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/CBq;
    .locals 1

    .prologue
    .line 1856854
    const-class v0, LX/CBq;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/CBq;

    return-object v0
.end method

.method public static values()[LX/CBq;
    .locals 1

    .prologue
    .line 1856853
    sget-object v0, LX/CBq;->$VALUES:[LX/CBq;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/CBq;

    return-object v0
.end method
