.class public final LX/C2I;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/C2K;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/CharSequence;

.field public b:Landroid/view/View$OnClickListener;

.field public final synthetic c:LX/C2K;


# direct methods
.method public constructor <init>(LX/C2K;)V
    .locals 1

    .prologue
    .line 1844118
    iput-object p1, p0, LX/C2I;->c:LX/C2K;

    .line 1844119
    move-object v0, p1

    .line 1844120
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1844121
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1844122
    const-string v0, "ActionLinkCallToActionButtonComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1844123
    if-ne p0, p1, :cond_1

    .line 1844124
    :cond_0
    :goto_0
    return v0

    .line 1844125
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1844126
    goto :goto_0

    .line 1844127
    :cond_3
    check-cast p1, LX/C2I;

    .line 1844128
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1844129
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1844130
    if-eq v2, v3, :cond_0

    .line 1844131
    iget-object v2, p0, LX/C2I;->a:Ljava/lang/CharSequence;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/C2I;->a:Ljava/lang/CharSequence;

    iget-object v3, p1, LX/C2I;->a:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1844132
    goto :goto_0

    .line 1844133
    :cond_5
    iget-object v2, p1, LX/C2I;->a:Ljava/lang/CharSequence;

    if-nez v2, :cond_4

    .line 1844134
    :cond_6
    iget-object v2, p0, LX/C2I;->b:Landroid/view/View$OnClickListener;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/C2I;->b:Landroid/view/View$OnClickListener;

    iget-object v3, p1, LX/C2I;->b:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1844135
    goto :goto_0

    .line 1844136
    :cond_7
    iget-object v2, p1, LX/C2I;->b:Landroid/view/View$OnClickListener;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
