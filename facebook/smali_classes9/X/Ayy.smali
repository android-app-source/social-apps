.class public final LX/Ayy;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 27

    .prologue
    .line 1731598
    const/16 v23, 0x0

    .line 1731599
    const/16 v22, 0x0

    .line 1731600
    const/16 v21, 0x0

    .line 1731601
    const/16 v20, 0x0

    .line 1731602
    const/16 v19, 0x0

    .line 1731603
    const/16 v18, 0x0

    .line 1731604
    const/16 v17, 0x0

    .line 1731605
    const/16 v16, 0x0

    .line 1731606
    const/4 v15, 0x0

    .line 1731607
    const/4 v14, 0x0

    .line 1731608
    const/4 v13, 0x0

    .line 1731609
    const/4 v12, 0x0

    .line 1731610
    const/4 v11, 0x0

    .line 1731611
    const/4 v10, 0x0

    .line 1731612
    const/4 v9, 0x0

    .line 1731613
    const/4 v8, 0x0

    .line 1731614
    const/4 v7, 0x0

    .line 1731615
    const/4 v6, 0x0

    .line 1731616
    const/4 v5, 0x0

    .line 1731617
    const/4 v4, 0x0

    .line 1731618
    const/4 v3, 0x0

    .line 1731619
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v24

    sget-object v25, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    if-eq v0, v1, :cond_1

    .line 1731620
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1731621
    const/4 v3, 0x0

    .line 1731622
    :goto_0
    return v3

    .line 1731623
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1731624
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v24

    sget-object v25, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    if-eq v0, v1, :cond_11

    .line 1731625
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v24

    .line 1731626
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1731627
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v25

    sget-object v26, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    if-eq v0, v1, :cond_1

    if-eqz v24, :cond_1

    .line 1731628
    const-string v25, "__type__"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-nez v25, :cond_2

    const-string v25, "__typename"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_3

    .line 1731629
    :cond_2
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v23

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v23

    goto :goto_1

    .line 1731630
    :cond_3
    const-string v25, "can_viewer_delete"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_4

    .line 1731631
    const/4 v8, 0x1

    .line 1731632
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v22

    goto :goto_1

    .line 1731633
    :cond_4
    const-string v25, "can_viewer_report"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_5

    .line 1731634
    const/4 v7, 0x1

    .line 1731635
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v21

    goto :goto_1

    .line 1731636
    :cond_5
    const-string v25, "creation_story"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_6

    .line 1731637
    invoke-static/range {p0 .. p1}, LX/Ayx;->a(LX/15w;LX/186;)I

    move-result v20

    goto :goto_1

    .line 1731638
    :cond_6
    const-string v25, "focus"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_7

    .line 1731639
    invoke-static/range {p0 .. p1}, LX/4aC;->a(LX/15w;LX/186;)I

    move-result v19

    goto :goto_1

    .line 1731640
    :cond_7
    const-string v25, "height"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_8

    .line 1731641
    const/4 v6, 0x1

    .line 1731642
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v18

    goto/16 :goto_1

    .line 1731643
    :cond_8
    const-string v25, "id"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_9

    .line 1731644
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v17

    goto/16 :goto_1

    .line 1731645
    :cond_9
    const-string v25, "image"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_a

    .line 1731646
    invoke-static/range {p0 .. p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v16

    goto/16 :goto_1

    .line 1731647
    :cond_a
    const-string v25, "imageHigh"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_b

    .line 1731648
    invoke-static/range {p0 .. p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v15

    goto/16 :goto_1

    .line 1731649
    :cond_b
    const-string v25, "imageLow"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_c

    .line 1731650
    invoke-static/range {p0 .. p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v14

    goto/16 :goto_1

    .line 1731651
    :cond_c
    const-string v25, "imageMedium"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_d

    .line 1731652
    invoke-static/range {p0 .. p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v13

    goto/16 :goto_1

    .line 1731653
    :cond_d
    const-string v25, "is_playable"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_e

    .line 1731654
    const/4 v5, 0x1

    .line 1731655
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v12

    goto/16 :goto_1

    .line 1731656
    :cond_e
    const-string v25, "playable_duration_in_ms"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_f

    .line 1731657
    const/4 v4, 0x1

    .line 1731658
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v11

    goto/16 :goto_1

    .line 1731659
    :cond_f
    const-string v25, "playable_url"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_10

    .line 1731660
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto/16 :goto_1

    .line 1731661
    :cond_10
    const-string v25, "width"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_0

    .line 1731662
    const/4 v3, 0x1

    .line 1731663
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v9

    goto/16 :goto_1

    .line 1731664
    :cond_11
    const/16 v24, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 1731665
    const/16 v24, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v24

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1731666
    if-eqz v8, :cond_12

    .line 1731667
    const/4 v8, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v8, v1}, LX/186;->a(IZ)V

    .line 1731668
    :cond_12
    if-eqz v7, :cond_13

    .line 1731669
    const/4 v7, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v7, v1}, LX/186;->a(IZ)V

    .line 1731670
    :cond_13
    const/4 v7, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v7, v1}, LX/186;->b(II)V

    .line 1731671
    const/4 v7, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v7, v1}, LX/186;->b(II)V

    .line 1731672
    if-eqz v6, :cond_14

    .line 1731673
    const/4 v6, 0x5

    const/4 v7, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v6, v1, v7}, LX/186;->a(III)V

    .line 1731674
    :cond_14
    const/4 v6, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v6, v1}, LX/186;->b(II)V

    .line 1731675
    const/4 v6, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v6, v1}, LX/186;->b(II)V

    .line 1731676
    const/16 v6, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v15}, LX/186;->b(II)V

    .line 1731677
    const/16 v6, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v14}, LX/186;->b(II)V

    .line 1731678
    const/16 v6, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v13}, LX/186;->b(II)V

    .line 1731679
    if-eqz v5, :cond_15

    .line 1731680
    const/16 v5, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v12}, LX/186;->a(IZ)V

    .line 1731681
    :cond_15
    if-eqz v4, :cond_16

    .line 1731682
    const/16 v4, 0xc

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v11, v5}, LX/186;->a(III)V

    .line 1731683
    :cond_16
    const/16 v4, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v10}, LX/186;->b(II)V

    .line 1731684
    if-eqz v3, :cond_17

    .line 1731685
    const/16 v3, 0xe

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9, v4}, LX/186;->a(III)V

    .line 1731686
    :cond_17
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1731687
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1731688
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1731689
    if-eqz v0, :cond_0

    .line 1731690
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1731691
    invoke-static {p0, p1, v2, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1731692
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1731693
    if-eqz v0, :cond_1

    .line 1731694
    const-string v1, "can_viewer_delete"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1731695
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1731696
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1731697
    if-eqz v0, :cond_2

    .line 1731698
    const-string v1, "can_viewer_report"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1731699
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1731700
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1731701
    if-eqz v0, :cond_3

    .line 1731702
    const-string v1, "creation_story"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1731703
    invoke-static {p0, v0, p2, p3}, LX/Ayx;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1731704
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1731705
    if-eqz v0, :cond_4

    .line 1731706
    const-string v1, "focus"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1731707
    invoke-static {p0, v0, p2}, LX/4aC;->a(LX/15i;ILX/0nX;)V

    .line 1731708
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1731709
    if-eqz v0, :cond_5

    .line 1731710
    const-string v1, "height"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1731711
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1731712
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1731713
    if-eqz v0, :cond_6

    .line 1731714
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1731715
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1731716
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1731717
    if-eqz v0, :cond_7

    .line 1731718
    const-string v1, "image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1731719
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 1731720
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1731721
    if-eqz v0, :cond_8

    .line 1731722
    const-string v1, "imageHigh"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1731723
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 1731724
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1731725
    if-eqz v0, :cond_9

    .line 1731726
    const-string v1, "imageLow"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1731727
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 1731728
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1731729
    if-eqz v0, :cond_a

    .line 1731730
    const-string v1, "imageMedium"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1731731
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 1731732
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1731733
    if-eqz v0, :cond_b

    .line 1731734
    const-string v1, "is_playable"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1731735
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1731736
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1731737
    if-eqz v0, :cond_c

    .line 1731738
    const-string v1, "playable_duration_in_ms"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1731739
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1731740
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1731741
    if-eqz v0, :cond_d

    .line 1731742
    const-string v1, "playable_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1731743
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1731744
    :cond_d
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1731745
    if-eqz v0, :cond_e

    .line 1731746
    const-string v1, "width"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1731747
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1731748
    :cond_e
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1731749
    return-void
.end method
