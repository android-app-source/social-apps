.class public final LX/C2p;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/C2p;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/C2n;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/C2q;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1844629
    const/4 v0, 0x0

    sput-object v0, LX/C2p;->a:LX/C2p;

    .line 1844630
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/C2p;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1844650
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1844651
    new-instance v0, LX/C2q;

    invoke-direct {v0}, LX/C2q;-><init>()V

    iput-object v0, p0, LX/C2p;->c:LX/C2q;

    .line 1844652
    return-void
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1844649
    const v0, -0x16026955

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method

.method public static declared-synchronized q()LX/C2p;
    .locals 2

    .prologue
    .line 1844653
    const-class v1, LX/C2p;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/C2p;->a:LX/C2p;

    if-nez v0, :cond_0

    .line 1844654
    new-instance v0, LX/C2p;

    invoke-direct {v0}, LX/C2p;-><init>()V

    sput-object v0, LX/C2p;->a:LX/C2p;

    .line 1844655
    :cond_0
    sget-object v0, LX/C2p;->a:LX/C2p;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 1844656
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 9

    .prologue
    .line 1844642
    check-cast p2, LX/C2o;

    .line 1844643
    iget-object v0, p2, LX/C2o;->a:Ljava/lang/CharSequence;

    iget-object v1, p2, LX/C2o;->b:Ljava/lang/CharSequence;

    iget-object v2, p2, LX/C2o;->c:Ljava/lang/CharSequence;

    iget-object v3, p2, LX/C2o;->d:Ljava/lang/CharSequence;

    const/4 p2, 0x2

    const/4 p0, 0x0

    const/4 v8, 0x1

    .line 1844644
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v8}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v4

    const v5, 0x7f0e0131

    invoke-static {p1, p0, v5}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, v0}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, v8}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v5

    sget-object v6, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v5, v6}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v4

    const v5, 0x7f0e0132

    invoke-static {p1, p0, v5}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, v1}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, v8}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v5

    sget-object v6, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v5, v6}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v4

    const v5, 0x7f0e0133

    invoke-static {p1, p0, v5}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, v2}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, v8}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v5

    sget-object v6, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v5, v6}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v4

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-interface {v4, v5}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v4

    .line 1844645
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, p2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, p2}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, p2}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v5

    .line 1844646
    const v6, -0x16026955

    const/4 v7, 0x0

    invoke-static {p1, v6, v7}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v6

    move-object v6, v6

    .line 1844647
    invoke-interface {v5, v6}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v5

    const v6, 0x7f0e014f

    invoke-static {p1, p0, v6}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v6

    invoke-virtual {v6, v3}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v6

    const v7, 0x7f0b004e

    invoke-virtual {v6, v7}, LX/1ne;->q(I)LX/1ne;

    move-result-object v6

    invoke-virtual {v6}, LX/1X5;->c()LX/1Di;

    move-result-object v6

    invoke-interface {v6, v8}, LX/1Di;->a(Z)LX/1Di;

    move-result-object v6

    const v7, 0x7f02079d

    invoke-interface {v6, v7}, LX/1Di;->x(I)LX/1Di;

    move-result-object v6

    const v7, 0x7f0b006f

    invoke-interface {v6, p0, v7}, LX/1Di;->g(II)LX/1Di;

    move-result-object v6

    const v7, 0x7f0b0072

    invoke-interface {v6, p2, v7}, LX/1Di;->g(II)LX/1Di;

    move-result-object v6

    const v7, 0x7f0b0069

    invoke-interface {v6, v8, v7}, LX/1Di;->g(II)LX/1Di;

    move-result-object v6

    const/4 v7, 0x3

    const v8, 0x7f0b006c

    invoke-interface {v6, v7, v8}, LX/1Di;->g(II)LX/1Di;

    move-result-object v6

    invoke-interface {v5, v6}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    const/16 v5, 0x8

    const v6, 0x7f0b010f

    invoke-interface {v4, v5, v6}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v4

    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    move-object v0, v4

    .line 1844648
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1844631
    invoke-static {}, LX/1dS;->b()V

    .line 1844632
    iget v0, p1, LX/1dQ;->b:I

    .line 1844633
    packed-switch v0, :pswitch_data_0

    .line 1844634
    :goto_0
    return-object v2

    .line 1844635
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 1844636
    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1844637
    check-cast v1, LX/C2o;

    .line 1844638
    iget-object p0, v1, LX/C2o;->e:LX/C2l;

    .line 1844639
    if-eqz p0, :cond_0

    .line 1844640
    invoke-virtual {p0}, LX/C2l;->onClick()V

    .line 1844641
    :cond_0
    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x16026955
        :pswitch_0
    .end packed-switch
.end method
