.class public final LX/Bdz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:Lcom/facebook/composer/ui/text/ComposerEditText;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/ui/text/ComposerEditText;)V
    .locals 0

    .prologue
    .line 1804401
    iput-object p1, p0, LX/Bdz;->a:Lcom/facebook/composer/ui/text/ComposerEditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 5

    .prologue
    const/4 v4, -0x1

    .line 1804402
    iget-object v0, p0, LX/Bdz;->a:Lcom/facebook/composer/ui/text/ComposerEditText;

    invoke-static {v0}, Lcom/facebook/composer/ui/text/ComposerEditText;->getTextWithEntities(Lcom/facebook/composer/ui/text/ComposerEditText;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    .line 1804403
    iget-object v0, p0, LX/Bdz;->a:Lcom/facebook/composer/ui/text/ComposerEditText;

    iget-object v0, v0, Lcom/facebook/composer/ui/text/ComposerEditText;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Be0;

    .line 1804404
    iget-object v3, p0, LX/Bdz;->a:Lcom/facebook/composer/ui/text/ComposerEditText;

    iget-boolean v3, v3, Lcom/facebook/composer/ui/text/ComposerEditText;->b:Z

    invoke-interface {v0, v1, v3}, LX/Be0;->a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;Z)V

    goto :goto_0

    .line 1804405
    :cond_0
    iget-object v0, p0, LX/Bdz;->a:Lcom/facebook/composer/ui/text/ComposerEditText;

    const/4 v1, 0x0

    .line 1804406
    iput-boolean v1, v0, Lcom/facebook/composer/ui/text/ComposerEditText;->b:Z

    .line 1804407
    iget-object v0, p0, LX/Bdz;->a:Lcom/facebook/composer/ui/text/ComposerEditText;

    iget-boolean v0, v0, Lcom/facebook/composer/ui/text/ComposerEditText;->f:Z

    if-eqz v0, :cond_1

    .line 1804408
    iget-object v0, p0, LX/Bdz;->a:Lcom/facebook/composer/ui/text/ComposerEditText;

    invoke-virtual {v0}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->getUserText()Ljava/lang/CharSequence;

    move-result-object v0

    .line 1804409
    iget-object v1, p0, LX/Bdz;->a:Lcom/facebook/composer/ui/text/ComposerEditText;

    invoke-virtual {v1}, Lcom/facebook/composer/ui/text/ComposerEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, LX/Bdz;->a:Lcom/facebook/composer/ui/text/ComposerEditText;

    invoke-static {v1}, Lcom/facebook/composer/ui/text/ComposerEditText;->getMetaHintIndex(Lcom/facebook/composer/ui/text/ComposerEditText;)I

    move-result v1

    if-ne v1, v4, :cond_2

    .line 1804410
    iget-object v0, p0, LX/Bdz;->a:Lcom/facebook/composer/ui/text/ComposerEditText;

    invoke-static {v0}, Lcom/facebook/composer/ui/text/ComposerEditText;->d(Lcom/facebook/composer/ui/text/ComposerEditText;)V

    .line 1804411
    :cond_1
    :goto_1
    return-void

    .line 1804412
    :cond_2
    iget-object v1, p0, LX/Bdz;->a:Lcom/facebook/composer/ui/text/ComposerEditText;

    invoke-static {v1}, Lcom/facebook/composer/ui/text/ComposerEditText;->getMetaHintIndex(Lcom/facebook/composer/ui/text/ComposerEditText;)I

    move-result v1

    if-eq v1, v4, :cond_1

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-eqz v0, :cond_1

    .line 1804413
    iget-object v0, p0, LX/Bdz;->a:Lcom/facebook/composer/ui/text/ComposerEditText;

    invoke-static {v0}, Lcom/facebook/composer/ui/text/ComposerEditText;->e(Lcom/facebook/composer/ui/text/ComposerEditText;)V

    goto :goto_1
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1804414
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1804415
    return-void
.end method
