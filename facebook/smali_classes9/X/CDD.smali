.class public LX/CDD;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/CDA;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CDF;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1858883
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/CDD;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/CDF;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1858880
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1858881
    iput-object p1, p0, LX/CDD;->b:LX/0Ot;

    .line 1858882
    return-void
.end method

.method public static a(LX/0QB;)LX/CDD;
    .locals 4

    .prologue
    .line 1858869
    const-class v1, LX/CDD;

    monitor-enter v1

    .line 1858870
    :try_start_0
    sget-object v0, LX/CDD;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1858871
    sput-object v2, LX/CDD;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1858872
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1858873
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1858874
    new-instance v3, LX/CDD;

    const/16 p0, 0x21e9

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/CDD;-><init>(LX/0Ot;)V

    .line 1858875
    move-object v0, v3

    .line 1858876
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1858877
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/CDD;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1858878
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1858879
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/1De;Z)V
    .locals 3

    .prologue
    .line 1858838
    iget-object v0, p0, LX/1De;->g:LX/1X1;

    move-object v0, v0

    .line 1858839
    if-nez v0, :cond_0

    .line 1858840
    :goto_0
    return-void

    .line 1858841
    :cond_0
    check-cast v0, LX/CDB;

    .line 1858842
    new-instance v1, LX/CDC;

    iget-object v2, v0, LX/CDB;->c:LX/CDD;

    invoke-direct {v1, v2, p1}, LX/CDC;-><init>(LX/CDD;Z)V

    move-object v0, v1

    .line 1858843
    invoke-virtual {p0, v0}, LX/1De;->a(LX/48B;)V

    goto :goto_0
.end method

.method public static onClick(LX/1X1;LX/1De;Z)LX/1dQ;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            "LX/1De;",
            "Z)",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1858884
    const v0, 0x2301e146

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v2, 0x1

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 7

    .prologue
    .line 1858858
    check-cast p2, LX/CDB;

    .line 1858859
    iget-object v0, p0, LX/CDD;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CDF;

    iget-object v1, p2, LX/CDB;->a:Ljava/lang/Boolean;

    iget-object v2, p2, LX/CDB;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 p2, 0x7

    const/4 v5, 0x2

    const/high16 p0, 0x3f800000    # 1.0f

    const/4 v6, 0x6

    .line 1858860
    if-nez v1, :cond_0

    .line 1858861
    iget-object v3, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 1858862
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->aD()Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;->m()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v3

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLSavedState;->SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-ne v3, v4, :cond_1

    const/4 v3, 0x1

    :goto_0
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 1858863
    :cond_0
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v5}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v5}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x3

    invoke-interface {v3, v4}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, p0}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v3

    const v4, 0x7f0203fe

    invoke-interface {v3, v4}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v5

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, v0, LX/CDF;->b:LX/15X;

    invoke-virtual {v3, p1}, LX/15X;->i(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    :goto_1
    invoke-virtual {v5, v3}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v3

    const v5, 0x7f0a00a4

    invoke-virtual {v3, v5}, LX/1ne;->n(I)LX/1ne;

    move-result-object v3

    const v5, 0x7f0b004e

    invoke-virtual {v3, v5}, LX/1ne;->q(I)LX/1ne;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const v5, 0x7f0b068a

    invoke-interface {v3, v6, v5}, LX/1Di;->c(II)LX/1Di;

    move-result-object v3

    invoke-interface {v3, p0}, LX/1Di;->a(F)LX/1Di;

    move-result-object v3

    invoke-interface {v4, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    iget-object v3, v0, LX/CDF;->d:LX/5JV;

    invoke-virtual {v3, p1}, LX/5JV;->c(LX/1De;)LX/5JT;

    move-result-object v3

    const v5, 0x7f0a00a6

    invoke-virtual {v3, v5}, LX/5JT;->j(I)LX/5JT;

    move-result-object v5

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_3

    const v3, 0x7f0207d6

    :goto_2
    invoke-virtual {v5, v3}, LX/5JT;->h(I)LX/5JT;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const v5, 0x7f0214b5

    invoke-interface {v3, v5}, LX/1Di;->x(I)LX/1Di;

    move-result-object v3

    const v5, 0x7f0b0687

    invoke-interface {v3, p2, v5}, LX/1Di;->g(II)LX/1Di;

    move-result-object v3

    const v5, 0x7f0b0688

    invoke-interface {v3, v6, v5}, LX/1Di;->g(II)LX/1Di;

    move-result-object v3

    const v5, 0x7f0b068c

    invoke-interface {v3, p2, v5}, LX/1Di;->c(II)LX/1Di;

    move-result-object v3

    const v5, 0x7f0b068a

    invoke-interface {v3, v6, v5}, LX/1Di;->c(II)LX/1Di;

    move-result-object v3

    const v5, 0x7f0b068d

    invoke-interface {v3, v5}, LX/1Di;->i(I)LX/1Di;

    move-result-object v3

    const v5, 0x7f0b068d

    invoke-interface {v3, v5}, LX/1Di;->q(I)LX/1Di;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    .line 1858864
    const v6, 0x2301e146

    const/4 p0, 0x2

    new-array p0, p0, [Ljava/lang/Object;

    const/4 p2, 0x0

    aput-object p1, p0, p2

    const/4 p2, 0x1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, p0, p2

    invoke-static {p1, v6, p0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v6

    move-object v5, v6

    .line 1858865
    invoke-interface {v3, v5}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v3

    invoke-interface {v4, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 1858866
    return-object v0

    .line 1858867
    :cond_1
    const/4 v3, 0x0

    goto/16 :goto_0

    .line 1858868
    :cond_2
    iget-object v3, v0, LX/CDF;->b:LX/15X;

    invoke-virtual {v3, p1}, LX/15X;->h(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_1

    :cond_3
    const v3, 0x7f0207fd

    goto :goto_2
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1858849
    invoke-static {}, LX/1dS;->b()V

    .line 1858850
    iget v0, p1, LX/1dQ;->b:I

    .line 1858851
    packed-switch v0, :pswitch_data_0

    .line 1858852
    :goto_0
    return-object v4

    .line 1858853
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 1858854
    iget-object v2, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v0, p1, LX/1dQ;->c:[Ljava/lang/Object;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    check-cast v0, LX/1De;

    iget-object v1, p1, LX/1dQ;->c:[Ljava/lang/Object;

    const/4 v3, 0x1

    aget-object v1, v1, v3

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iget-object v3, p1, LX/1dQ;->a:LX/1X1;

    .line 1858855
    check-cast v3, LX/CDB;

    .line 1858856
    iget-object p1, p0, LX/CDD;->b:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/CDF;

    iget-object p2, v3, LX/CDB;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {p1, v2, v0, v1, p2}, LX/CDF;->onClick(Landroid/view/View;LX/1De;ZLcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1858857
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x2301e146
        :pswitch_0
    .end packed-switch
.end method

.method public final a(LX/1X1;LX/1X1;)V
    .locals 1

    .prologue
    .line 1858845
    check-cast p1, LX/CDB;

    .line 1858846
    check-cast p2, LX/CDB;

    .line 1858847
    iget-object v0, p1, LX/CDB;->a:Ljava/lang/Boolean;

    iput-object v0, p2, LX/CDB;->a:Ljava/lang/Boolean;

    .line 1858848
    return-void
.end method

.method public final p()Z
    .locals 1

    .prologue
    .line 1858844
    const/4 v0, 0x1

    return v0
.end method
