.class public final LX/C35;
.super Ljava/util/HashSet;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/HashSet",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1845064
    invoke-direct {p0}, Ljava/util/HashSet;-><init>()V

    .line 1845065
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->EVENT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-virtual {p0, v0}, LX/C35;->add(Ljava/lang/Object;)Z

    .line 1845066
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->GROUP:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-virtual {p0, v0}, LX/C35;->add(Ljava/lang/Object;)Z

    .line 1845067
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->SHARE:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-virtual {p0, v0}, LX/C35;->add(Ljava/lang/Object;)Z

    .line 1845068
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->CANCELED_EVENT:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-virtual {p0, v0}, LX/C35;->add(Ljava/lang/Object;)Z

    .line 1845069
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->MAP:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-virtual {p0, v0}, LX/C35;->add(Ljava/lang/Object;)Z

    .line 1845070
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->AVATAR:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-virtual {p0, v0}, LX/C35;->add(Ljava/lang/Object;)Z

    .line 1845071
    return-void
.end method
