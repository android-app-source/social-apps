.class public final LX/Aco;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field public final synthetic a:LX/Acp;

.field private b:F

.field private c:F

.field private d:I

.field private e:I


# direct methods
.method public constructor <init>(LX/Acp;)V
    .locals 0

    .prologue
    .line 1693151
    iput-object p1, p0, LX/Aco;->a:LX/Acp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1693152
    iget-object v1, p0, LX/Aco;->a:LX/Acp;

    iget-object v1, v1, LX/Acp;->d:Landroid/view/WindowManager;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/Aco;->a:LX/Acp;

    iget-object v1, v1, LX/Acp;->e:Landroid/view/WindowManager$LayoutParams;

    if-nez v1, :cond_1

    .line 1693153
    :cond_0
    :goto_0
    return v0

    .line 1693154
    :cond_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 1693155
    :pswitch_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    iput v0, p0, LX/Aco;->b:F

    .line 1693156
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    iput v0, p0, LX/Aco;->c:F

    .line 1693157
    iget-object v0, p0, LX/Aco;->a:LX/Acp;

    iget-object v0, v0, LX/Acp;->e:Landroid/view/WindowManager$LayoutParams;

    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->x:I

    iput v0, p0, LX/Aco;->d:I

    .line 1693158
    iget-object v0, p0, LX/Aco;->a:LX/Acp;

    iget-object v0, v0, LX/Acp;->e:Landroid/view/WindowManager$LayoutParams;

    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    iput v0, p0, LX/Aco;->e:I

    .line 1693159
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 1693160
    :pswitch_2
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    iget v1, p0, LX/Aco;->b:F

    sub-float/2addr v0, v1

    .line 1693161
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v1

    iget v2, p0, LX/Aco;->c:F

    sub-float/2addr v1, v2

    .line 1693162
    iget-object v2, p0, LX/Aco;->a:LX/Acp;

    iget-object v2, v2, LX/Acp;->e:Landroid/view/WindowManager$LayoutParams;

    iget v3, p0, LX/Aco;->d:I

    int-to-float v3, v3

    add-float/2addr v0, v3

    float-to-int v0, v0

    iput v0, v2, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 1693163
    iget-object v0, p0, LX/Aco;->a:LX/Acp;

    iget-object v0, v0, LX/Acp;->e:Landroid/view/WindowManager$LayoutParams;

    iget v2, p0, LX/Aco;->e:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 1693164
    iget-object v0, p0, LX/Aco;->a:LX/Acp;

    iget-object v0, v0, LX/Acp;->d:Landroid/view/WindowManager;

    iget-object v1, p0, LX/Aco;->a:LX/Acp;

    iget-object v1, v1, LX/Acp;->e:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v0, p1, v1}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
