.class public final LX/By3;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/events/graphql/EventsMutationsModels$WatchEventMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

.field public final synthetic b:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

.field public final synthetic c:LX/By4;


# direct methods
.method public constructor <init>(LX/By4;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)V
    .locals 0

    .prologue
    .line 1836615
    iput-object p1, p0, LX/By3;->c:LX/By4;

    iput-object p2, p0, LX/By3;->a:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    iput-object p3, p0, LX/By3;->b:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 1836616
    iget-object v0, p0, LX/By3;->c:LX/By4;

    iget-object v0, v0, LX/By4;->j:LX/1Sl;

    invoke-virtual {v0}, LX/1Sl;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1836617
    iget-object v0, p0, LX/By3;->c:LX/By4;

    iget-object v0, v0, LX/By4;->g:LX/189;

    iget-object v1, p0, LX/By3;->c:LX/By4;

    iget-object v1, v1, LX/By4;->b:Lcom/facebook/graphql/model/GraphQLEvent;

    iget-object v2, p0, LX/By3;->c:LX/By4;

    iget-object v2, v2, LX/By4;->d:Lcom/facebook/graphql/model/GraphQLStory;

    iget-object v3, p0, LX/By3;->c:LX/By4;

    iget-object v3, v3, LX/By4;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    iget-object v4, p0, LX/By3;->a:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    invoke-virtual {v0, v1, v2, v3, v4}, LX/189;->a(Lcom/facebook/graphql/model/GraphQLEvent;Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 1836618
    iget-object v1, p0, LX/By3;->c:LX/By4;

    iget-object v1, v1, LX/By4;->f:LX/0bH;

    new-instance v2, LX/1Ne;

    invoke-direct {v2, v0}, LX/1Ne;-><init>(Lcom/facebook/graphql/model/FeedUnit;)V

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b7;)V

    .line 1836619
    :cond_0
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 1836620
    iget-object v0, p0, LX/By3;->a:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/By3;->a:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->WATCHED:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    if-ne v0, v1, :cond_1

    .line 1836621
    :cond_0
    :goto_0
    return-void

    .line 1836622
    :cond_1
    iget-object v0, p0, LX/By3;->b:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    if-eq v0, v1, :cond_2

    iget-object v0, p0, LX/By3;->b:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->WATCHED:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    if-ne v0, v1, :cond_0

    .line 1836623
    :cond_2
    iget-object v0, p0, LX/By3;->c:LX/By4;

    invoke-static {v0}, LX/By4;->a$redex0(LX/By4;)V

    goto :goto_0
.end method
