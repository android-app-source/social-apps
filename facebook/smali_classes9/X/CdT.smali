.class public final enum LX/CdT;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/CdT;",
        ">;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/CdT;

.field public static final enum COMPOSER_EDIT:LX/CdT;

.field public static final enum FINCH_EDIT:LX/CdT;

.field public static final enum FINCH_PROFILE:LX/CdT;

.field public static final enum PLACE_CREATION:LX/CdT;

.field public static final enum POST_CHECKIN:LX/CdT;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1922679
    new-instance v0, LX/CdT;

    const-string v1, "COMPOSER_EDIT"

    invoke-direct {v0, v1, v2}, LX/CdT;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CdT;->COMPOSER_EDIT:LX/CdT;

    .line 1922680
    new-instance v0, LX/CdT;

    const-string v1, "PLACE_CREATION"

    invoke-direct {v0, v1, v3}, LX/CdT;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CdT;->PLACE_CREATION:LX/CdT;

    .line 1922681
    new-instance v0, LX/CdT;

    const-string v1, "FINCH_EDIT"

    invoke-direct {v0, v1, v4}, LX/CdT;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CdT;->FINCH_EDIT:LX/CdT;

    .line 1922682
    new-instance v0, LX/CdT;

    const-string v1, "FINCH_PROFILE"

    invoke-direct {v0, v1, v5}, LX/CdT;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CdT;->FINCH_PROFILE:LX/CdT;

    .line 1922683
    new-instance v0, LX/CdT;

    const-string v1, "POST_CHECKIN"

    invoke-direct {v0, v1, v6}, LX/CdT;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CdT;->POST_CHECKIN:LX/CdT;

    .line 1922684
    const/4 v0, 0x5

    new-array v0, v0, [LX/CdT;

    sget-object v1, LX/CdT;->COMPOSER_EDIT:LX/CdT;

    aput-object v1, v0, v2

    sget-object v1, LX/CdT;->PLACE_CREATION:LX/CdT;

    aput-object v1, v0, v3

    sget-object v1, LX/CdT;->FINCH_EDIT:LX/CdT;

    aput-object v1, v0, v4

    sget-object v1, LX/CdT;->FINCH_PROFILE:LX/CdT;

    aput-object v1, v0, v5

    sget-object v1, LX/CdT;->POST_CHECKIN:LX/CdT;

    aput-object v1, v0, v6

    sput-object v0, LX/CdT;->$VALUES:[LX/CdT;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1922685
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/CdT;
    .locals 1

    .prologue
    .line 1922686
    const-class v0, LX/CdT;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/CdT;

    return-object v0
.end method

.method public static values()[LX/CdT;
    .locals 1

    .prologue
    .line 1922687
    sget-object v0, LX/CdT;->$VALUES:[LX/CdT;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/CdT;

    return-object v0
.end method
