.class public final LX/CQ4;
.super LX/1dc;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1dc",
        "<",
        "Landroid/graphics/drawable/Drawable;",
        ">;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:I

.field public c:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1885367
    invoke-static {}, LX/CQ5;->a()LX/CQ5;

    move-result-object v0

    invoke-direct {p0, v0}, LX/1dc;-><init>(LX/1n4;)V

    .line 1885368
    iput v1, p0, LX/CQ4;->a:I

    .line 1885369
    iput v1, p0, LX/CQ4;->b:I

    .line 1885370
    iput v1, p0, LX/CQ4;->c:I

    .line 1885371
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1885372
    const-string v0, "NTForegroundReference"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1885373
    if-ne p0, p1, :cond_1

    .line 1885374
    :cond_0
    :goto_0
    return v0

    .line 1885375
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1885376
    goto :goto_0

    .line 1885377
    :cond_3
    check-cast p1, LX/CQ4;

    .line 1885378
    iget v2, p0, LX/CQ4;->a:I

    iget v3, p1, LX/CQ4;->a:I

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 1885379
    goto :goto_0

    .line 1885380
    :cond_4
    iget v2, p0, LX/CQ4;->b:I

    iget v3, p1, LX/CQ4;->b:I

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 1885381
    goto :goto_0

    .line 1885382
    :cond_5
    iget v2, p0, LX/CQ4;->c:I

    iget v3, p1, LX/CQ4;->c:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 1885383
    goto :goto_0
.end method
