.class public final LX/CcQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySeekBar;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySeekBar;)V
    .locals 0

    .prologue
    .line 1921098
    iput-object p1, p0, LX/CcQ;->a:Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySeekBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySeekBar;B)V
    .locals 0

    .prologue
    .line 1921085
    invoke-direct {p0, p1}, LX/CcQ;-><init>(Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySeekBar;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x2

    const v2, 0x1dd44064

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 1921086
    iget-object v3, p0, LX/CcQ;->a:Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySeekBar;

    iget-object v0, p0, LX/CcQ;->a:Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySeekBar;

    iget-boolean v0, v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySeekBar;->i:Z

    if-nez v0, :cond_3

    move v0, v1

    .line 1921087
    :goto_0
    iput-boolean v0, v3, Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySeekBar;->i:Z

    .line 1921088
    iget-object v0, p0, LX/CcQ;->a:Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySeekBar;

    iget-object v3, p0, LX/CcQ;->a:Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySeekBar;

    iget-boolean v3, v3, Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySeekBar;->i:Z

    invoke-virtual {v0, v1, v3}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySeekBar;->a(ZZ)V

    .line 1921089
    iget-object v0, p0, LX/CcQ;->a:Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySeekBar;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySeekBar;->a:LX/CcX;

    iget-object v1, p0, LX/CcQ;->a:Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySeekBar;

    iget-object v1, v1, Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySeekBar;->h:Ljava/lang/String;

    iget-object v3, p0, LX/CcQ;->a:Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySeekBar;

    iget-boolean v3, v3, Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySeekBar;->i:Z

    .line 1921090
    iget-object v4, v0, LX/CcX;->b:Ljava/util/WeakHashMap;

    invoke-virtual {v4}, Ljava/util/WeakHashMap;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/Cae;

    .line 1921091
    if-eqz v4, :cond_0

    .line 1921092
    iget-object p0, v4, LX/Cae;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;

    iget-object p0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->i:Lcom/facebook/video/player/RichVideoPlayer;

    if-eqz p0, :cond_1

    iget-object p0, v4, LX/Cae;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;

    invoke-static {p0, v1}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->a$redex0(Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;Ljava/lang/String;)Z

    move-result p0

    if-nez p0, :cond_4

    .line 1921093
    :cond_1
    :goto_2
    goto :goto_1

    .line 1921094
    :cond_2
    const v0, 0x53e16433

    invoke-static {v0, v2}, LX/02F;->a(II)V

    return-void

    .line 1921095
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 1921096
    :cond_4
    iget-object p0, v4, LX/Cae;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;

    iget-boolean p0, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->l:Z

    if-eqz p0, :cond_1

    .line 1921097
    iget-object p0, v4, LX/Cae;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;

    iget-object p1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->i:Lcom/facebook/video/player/RichVideoPlayer;

    if-eqz v3, :cond_5

    sget-object p0, LX/2oi;->HIGH_DEFINITION:LX/2oi;

    :goto_3
    sget-object v0, LX/04g;->BY_USER:LX/04g;

    invoke-virtual {p1, p0, v0}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/2oi;LX/04g;)V

    goto :goto_2

    :cond_5
    sget-object p0, LX/2oi;->STANDARD_DEFINITION:LX/2oi;

    goto :goto_3
.end method
