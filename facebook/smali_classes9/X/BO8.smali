.class public final LX/BO8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/AU9;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/AU9",
        "<",
        "Lcom/facebook/saved2/model/Saved2ItemTable_Queries$TimeSortedQueryBySectionDAO;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:I

.field private final b:I

.field private final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(IILjava/lang/String;)V
    .locals 0

    .prologue
    .line 1779600
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1779601
    iput p1, p0, LX/BO8;->a:I

    .line 1779602
    iput p2, p0, LX/BO8;->b:I

    .line 1779603
    iput-object p3, p0, LX/BO8;->c:Ljava/lang/String;

    .line 1779604
    return-void
.end method


# virtual methods
.method public final a(Landroid/database/Cursor;)LX/AU0;
    .locals 2

    .prologue
    .line 1779605
    new-instance v0, LX/BO7;

    invoke-direct {v0, p1}, LX/BO7;-><init>(Landroid/database/Cursor;)V

    return-object v0
.end method

.method public final a()[Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1779599
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-class v2, LX/BNx;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final b()[Ljava/lang/Object;
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1779598
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "item "

    aput-object v1, v0, v4

    const/16 v1, 0x35

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "_id"

    aput-object v2, v1, v4

    const-string v2, "node_id"

    aput-object v2, v1, v5

    const-string v2, "time_saved_ms"

    aput-object v2, v1, v6

    const-string v2, "graphql_cursor"

    aput-object v2, v1, v7

    const-string v2, "group_title"

    aput-object v2, v1, v8

    const/4 v2, 0x5

    const-string v3, "picture_uri"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "title"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "subtitle"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "type"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "type_display_extras"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "saved_state"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "is_item_viewed"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string v3, "url"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string v3, "permalink_id"

    aput-object v3, v1, v2

    const/16 v2, 0xe

    const-string v3, "permalink_url"

    aput-object v3, v1, v2

    const/16 v2, 0xf

    const-string v3, "source_story_id"

    aput-object v3, v1, v2

    const/16 v2, 0x10

    const-string v3, "source_message_other_user_id"

    aput-object v3, v1, v2

    const/16 v2, 0x11

    const-string v3, "source_message_thread_fbid"

    aput-object v3, v1, v2

    const/16 v2, 0x12

    const-string v3, "source_container_category"

    aput-object v3, v1, v2

    const/16 v2, 0x13

    const-string v3, "source_actor_name"

    aput-object v3, v1, v2

    const/16 v2, 0x14

    const-string v3, "source_actor_short_name"

    aput-object v3, v1, v2

    const/16 v2, 0x15

    const-string v3, "source_actor_picture_uri"

    aput-object v3, v1, v2

    const/16 v2, 0x16

    const-string v3, "fb_link_url"

    aput-object v3, v1, v2

    const/16 v2, 0x17

    const-string v3, "is_playable"

    aput-object v3, v1, v2

    const/16 v2, 0x18

    const-string v3, "playlist"

    aput-object v3, v1, v2

    const/16 v2, 0x19

    const-string v3, "preferredPlayableUrlString"

    aput-object v3, v1, v2

    const/16 v2, 0x1a

    const-string v3, "playable_url"

    aput-object v3, v1, v2

    const/16 v2, 0x1b

    const-string v3, "playable_duration_ms"

    aput-object v3, v1, v2

    const/16 v2, 0x1c

    const-string v3, "last_played_position_ms"

    aput-object v3, v1, v2

    const/16 v2, 0x1d

    const-string v3, "target_graphql_object_type"

    aput-object v3, v1, v2

    const/16 v2, 0x1e

    const-string v3, "global_share_id"

    aput-object v3, v1, v2

    const/16 v2, 0x1f

    const-string v3, "global_share_graphql_object_type"

    aput-object v3, v1, v2

    const/16 v2, 0x20

    const-string v3, "instant_article_id"

    aput-object v3, v1, v2

    const/16 v2, 0x21

    const-string v3, "instant_article_canonical_url"

    aput-object v3, v1, v2

    const/16 v2, 0x22

    const-string v3, "instant_article_last_read_block_id"

    aput-object v3, v1, v2

    const/16 v2, 0x23

    const-string v3, "can_viewer_share_event"

    aput-object v3, v1, v2

    const/16 v2, 0x24

    const-string v3, "can_viewer_share_product"

    aput-object v3, v1, v2

    const/16 v2, 0x25

    const-string v3, "is_spherical"

    aput-object v3, v1, v2

    const/16 v2, 0x26

    const-string v3, "spherical_preferred_fov"

    aput-object v3, v1, v2

    const/16 v2, 0x27

    const-string v3, "spherical_fullscreen_aspect_ratio"

    aput-object v3, v1, v2

    const/16 v2, 0x28

    const-string v3, "spherical_inline_aspect_ratio"

    aput-object v3, v1, v2

    const/16 v2, 0x29

    const-string v3, "spherical_playable_url_sd_string"

    aput-object v3, v1, v2

    const/16 v2, 0x2a

    const-string v3, "spherical_playable_url_hd_string"

    aput-object v3, v1, v2

    const/16 v2, 0x2b

    const-string v3, "initial_view_heading_degrees"

    aput-object v3, v1, v2

    const/16 v2, 0x2c

    const-string v3, "initial_view_pitch_degrees"

    aput-object v3, v1, v2

    const/16 v2, 0x2d

    const-string v3, "initial_view_roll_degrees"

    aput-object v3, v1, v2

    const/16 v2, 0x2e

    const-string v3, "media_content_size"

    aput-object v3, v1, v2

    const/16 v2, 0x2f

    const-string v3, "section_name_server"

    aput-object v3, v1, v2

    const/16 v2, 0x30

    const-string v3, "is_deleted_client"

    aput-object v3, v1, v2

    const/16 v2, 0x31

    const-string v3, "is_unarchived_client"

    aput-object v3, v1, v2

    const/16 v2, 0x32

    const-string v3, "is_download_client"

    aput-object v3, v1, v2

    const/16 v2, 0x33

    const-string v3, "saved_video_channel_id "

    aput-object v3, v1, v2

    const/16 v2, 0x34

    const-string v3, "item_attribution"

    aput-object v3, v1, v2

    aput-object v1, v0, v5

    const-string v1, "is_deleted_client = ? AND is_unarchived_client = ? AND section_name = ?"

    aput-object v1, v0, v6

    new-array v1, v7, [Ljava/lang/String;

    iget v2, p0, LX/BO8;->a:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    iget v2, p0, LX/BO8;->b:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    iget-object v2, p0, LX/BO8;->c:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    aput-object v1, v0, v7

    const-string v1, "is_download_client DESC, time_saved_ms DESC"

    aput-object v1, v0, v8

    return-object v0
.end method
