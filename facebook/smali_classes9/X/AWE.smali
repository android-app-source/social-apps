.class public final LX/AWE;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15i;ILX/0nX;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1682337
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1682338
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1682339
    if-eqz v0, :cond_0

    .line 1682340
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1682341
    invoke-static {p0, p1, v2, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1682342
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1682343
    if-eqz v0, :cond_1

    .line 1682344
    const-string v1, "countdown_time_offset_ms"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1682345
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1682346
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1682347
    if-eqz v0, :cond_2

    .line 1682348
    const-string v1, "text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1682349
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1682350
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1682351
    if-eqz v0, :cond_3

    .line 1682352
    const-string v1, "text_time_duration_ms"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1682353
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1682354
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1682355
    if-eqz v0, :cond_4

    .line 1682356
    const-string v1, "text_time_offset_ms"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1682357
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1682358
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1682359
    if-eqz v0, :cond_5

    .line 1682360
    const-string v1, "time_duration_ms"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1682361
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1682362
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1682363
    if-eqz v0, :cond_6

    .line 1682364
    const-string v1, "time_offset_ms"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1682365
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1682366
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1682367
    if-eqz v0, :cond_7

    .line 1682368
    const-string v1, "vertical_position_percent_thousand"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1682369
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1682370
    :cond_7
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1682371
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 19

    .prologue
    .line 1682372
    const/4 v15, 0x0

    .line 1682373
    const/4 v14, 0x0

    .line 1682374
    const/4 v13, 0x0

    .line 1682375
    const/4 v12, 0x0

    .line 1682376
    const/4 v11, 0x0

    .line 1682377
    const/4 v10, 0x0

    .line 1682378
    const/4 v9, 0x0

    .line 1682379
    const/4 v8, 0x0

    .line 1682380
    const/4 v7, 0x0

    .line 1682381
    const/4 v6, 0x0

    .line 1682382
    const/4 v5, 0x0

    .line 1682383
    const/4 v4, 0x0

    .line 1682384
    const/4 v3, 0x0

    .line 1682385
    const/4 v2, 0x0

    .line 1682386
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v16

    sget-object v17, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    if-eq v0, v1, :cond_1

    .line 1682387
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1682388
    const/4 v2, 0x0

    .line 1682389
    :goto_0
    return v2

    .line 1682390
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1682391
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v16

    sget-object v17, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    if-eq v0, v1, :cond_a

    .line 1682392
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v16

    .line 1682393
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1682394
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v17

    sget-object v18, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    if-eq v0, v1, :cond_1

    if-eqz v16, :cond_1

    .line 1682395
    const-string v17, "__type__"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-nez v17, :cond_2

    const-string v17, "__typename"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_3

    .line 1682396
    :cond_2
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v15

    goto :goto_1

    .line 1682397
    :cond_3
    const-string v17, "countdown_time_offset_ms"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_4

    .line 1682398
    const/4 v7, 0x1

    .line 1682399
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v14

    goto :goto_1

    .line 1682400
    :cond_4
    const-string v17, "text"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_5

    .line 1682401
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    goto :goto_1

    .line 1682402
    :cond_5
    const-string v17, "text_time_duration_ms"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_6

    .line 1682403
    const/4 v6, 0x1

    .line 1682404
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v12

    goto :goto_1

    .line 1682405
    :cond_6
    const-string v17, "text_time_offset_ms"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_7

    .line 1682406
    const/4 v5, 0x1

    .line 1682407
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v11

    goto :goto_1

    .line 1682408
    :cond_7
    const-string v17, "time_duration_ms"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_8

    .line 1682409
    const/4 v4, 0x1

    .line 1682410
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v10

    goto/16 :goto_1

    .line 1682411
    :cond_8
    const-string v17, "time_offset_ms"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_9

    .line 1682412
    const/4 v3, 0x1

    .line 1682413
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v9

    goto/16 :goto_1

    .line 1682414
    :cond_9
    const-string v17, "vertical_position_percent_thousand"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_0

    .line 1682415
    const/4 v2, 0x1

    .line 1682416
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v8

    goto/16 :goto_1

    .line 1682417
    :cond_a
    const/16 v16, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 1682418
    const/16 v16, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v1, v15}, LX/186;->b(II)V

    .line 1682419
    if-eqz v7, :cond_b

    .line 1682420
    const/4 v7, 0x1

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v14, v15}, LX/186;->a(III)V

    .line 1682421
    :cond_b
    const/4 v7, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v13}, LX/186;->b(II)V

    .line 1682422
    if-eqz v6, :cond_c

    .line 1682423
    const/4 v6, 0x3

    const/4 v7, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v12, v7}, LX/186;->a(III)V

    .line 1682424
    :cond_c
    if-eqz v5, :cond_d

    .line 1682425
    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v11, v6}, LX/186;->a(III)V

    .line 1682426
    :cond_d
    if-eqz v4, :cond_e

    .line 1682427
    const/4 v4, 0x5

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v10, v5}, LX/186;->a(III)V

    .line 1682428
    :cond_e
    if-eqz v3, :cond_f

    .line 1682429
    const/4 v3, 0x6

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9, v4}, LX/186;->a(III)V

    .line 1682430
    :cond_f
    if-eqz v2, :cond_10

    .line 1682431
    const/4 v2, 0x7

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8, v3}, LX/186;->a(III)V

    .line 1682432
    :cond_10
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0
.end method
