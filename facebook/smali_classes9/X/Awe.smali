.class public LX/Awe;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1726904
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 1726905
    return-void
.end method

.method public static a(LX/AwS;)LX/AwR;
    .locals 5
    .annotation runtime Lcom/facebook/friendsharing/inspiration/styletransfer/StyleTransferCache;
    .end annotation

    .annotation runtime Lcom/facebook/inject/ContextScoped;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .prologue
    .line 1726906
    invoke-static {}, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig;->newBuilder()Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig$Builder;

    move-result-object v1

    const-string v2, "style_transfer_disk_cache"

    invoke-virtual {v1, v2}, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig$Builder;->setName(Ljava/lang/String;)Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig$Builder;

    move-result-object v1

    const-wide/16 v3, 0x1

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig$Builder;->setVersion(Ljava/lang/Long;)Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig$Builder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig$Builder;->setCapacity(Ljava/lang/Long;)Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig$Builder;->a()Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig;

    move-result-object v1

    move-object v0, v1

    .line 1726907
    invoke-virtual {p0, v0}, LX/AwS;->a(Lcom/facebook/friendsharing/inspiration/store/InspirationCacheConfig;)LX/AwR;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 1726908
    return-void
.end method
