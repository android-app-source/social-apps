.class public LX/CAn;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/CAl;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CAo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1855728
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/CAn;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/CAo;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1855725
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1855726
    iput-object p1, p0, LX/CAn;->b:LX/0Ot;

    .line 1855727
    return-void
.end method

.method public static a(LX/0QB;)LX/CAn;
    .locals 4

    .prologue
    .line 1855714
    const-class v1, LX/CAn;

    monitor-enter v1

    .line 1855715
    :try_start_0
    sget-object v0, LX/CAn;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1855716
    sput-object v2, LX/CAn;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1855717
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1855718
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1855719
    new-instance v3, LX/CAn;

    const/16 p0, 0x20af

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/CAn;-><init>(LX/0Ot;)V

    .line 1855720
    move-object v0, v3

    .line 1855721
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1855722
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/CAn;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1855723
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1855724
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1855703
    invoke-static {}, LX/1dS;->b()V

    .line 1855704
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b(LX/1De;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1855711
    iget-object v0, p0, LX/CAn;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    .line 1855712
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/ColorDrawable;-><init>()V

    move-object v0, v0

    .line 1855713
    return-object v0
.end method

.method public final e(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 2

    .prologue
    .line 1855707
    check-cast p3, LX/CAm;

    .line 1855708
    iget-object v0, p0, LX/CAn;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CAo;

    iget-object v1, p3, LX/CAm;->a:Ljava/lang/String;

    .line 1855709
    iget-object p0, v0, LX/CAo;->a:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/D2D;

    sget-object p1, LX/D2C;->CALL_TO_ACTION_BUTTON_IMPRESSION:LX/D2C;

    invoke-virtual {p0, p1, v1}, LX/D2D;->b(LX/D2C;Ljava/lang/String;)V

    .line 1855710
    return-void
.end method

.method public final f()LX/1mv;
    .locals 1

    .prologue
    .line 1855706
    sget-object v0, LX/1mv;->DRAWABLE:LX/1mv;

    return-object v0
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 1855705
    const/16 v0, 0xf

    return v0
.end method
