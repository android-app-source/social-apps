.class public final LX/CX1;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextCityTypeaheadView;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextCityTypeaheadView;)V
    .locals 0

    .prologue
    .line 1909255
    iput-object p1, p0, LX/CX1;->a:Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextCityTypeaheadView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 1909256
    iget-object v0, p0, LX/CX1;->a:Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextCityTypeaheadView;

    iget-object v0, v0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextCityTypeaheadView;->f:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1909257
    iget-object v0, p0, LX/CX1;->a:Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextCityTypeaheadView;

    iget-object v0, v0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextCityTypeaheadView;->b:LX/CXd;

    sget-object v1, LX/CXc;->DATA_FETCH:LX/CXc;

    const-string v2, "location_typeahead_fetch_error"

    const-string v3, "Failed to fetch location typeahead suggestions"

    invoke-virtual {v0, v1, v2, v3}, LX/CXd;->a(LX/CXc;Ljava/lang/String;Ljava/lang/String;)V

    .line 1909258
    return-void
.end method

.method public final a(LX/0Px;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/pages/common/platform/protocol/FetchPageTargetingTypeaheadModels$InstantWorkflowPageTargetingTypeaheadLocationQueryModel$AdsTargetingModel$LocationsModel$NodesModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1909259
    iget-object v0, p0, LX/CX1;->a:Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextCityTypeaheadView;

    iget-object v0, v0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextCityTypeaheadView;->f:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1909260
    iget-object v0, p0, LX/CX1;->a:Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextCityTypeaheadView;

    .line 1909261
    invoke-static {v0, p1}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextCityTypeaheadView;->a$redex0(Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextCityTypeaheadView;LX/0Px;)V

    .line 1909262
    return-void
.end method
