.class public final LX/CIM;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 7

    .prologue
    .line 1873574
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1873575
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 1873576
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 1873577
    const/4 v2, 0x0

    .line 1873578
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_5

    .line 1873579
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1873580
    :goto_1
    move v1, v2

    .line 1873581
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1873582
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0

    .line 1873583
    :cond_1
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1873584
    :cond_2
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_4

    .line 1873585
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1873586
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1873587
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_2

    if-eqz v4, :cond_2

    .line 1873588
    const-string v5, "action"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1873589
    invoke-static {p0, p1}, LX/CHy;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_2

    .line 1873590
    :cond_3
    const-string v5, "bounds"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1873591
    invoke-static {p0, p1}, LX/CIV;->a(LX/15w;LX/186;)I

    move-result v1

    goto :goto_2

    .line 1873592
    :cond_4
    const/4 v4, 0x2

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1873593
    invoke-virtual {p1, v2, v3}, LX/186;->b(II)V

    .line 1873594
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v1}, LX/186;->b(II)V

    .line 1873595
    invoke-virtual {p1}, LX/186;->d()I

    move-result v2

    goto :goto_1

    :cond_5
    move v1, v2

    move v3, v2

    goto :goto_2
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 10

    .prologue
    .line 1873596
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1873597
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_6

    .line 1873598
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    .line 1873599
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1873600
    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1873601
    if-eqz v2, :cond_0

    .line 1873602
    const-string v3, "action"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1873603
    invoke-static {p0, v2, p2, p3}, LX/CHy;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1873604
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {p0, v1, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1873605
    if-eqz v2, :cond_5

    .line 1873606
    const-string v3, "bounds"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1873607
    const-wide/16 v8, 0x0

    .line 1873608
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1873609
    const/4 v4, 0x0

    invoke-virtual {p0, v2, v4, v8, v9}, LX/15i;->a(IID)D

    move-result-wide v4

    .line 1873610
    cmpl-double v6, v4, v8

    if-eqz v6, :cond_1

    .line 1873611
    const-string v6, "height"

    invoke-virtual {p2, v6}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1873612
    invoke-virtual {p2, v4, v5}, LX/0nX;->a(D)V

    .line 1873613
    :cond_1
    const/4 v4, 0x1

    invoke-virtual {p0, v2, v4, v8, v9}, LX/15i;->a(IID)D

    move-result-wide v4

    .line 1873614
    cmpl-double v6, v4, v8

    if-eqz v6, :cond_2

    .line 1873615
    const-string v6, "width"

    invoke-virtual {p2, v6}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1873616
    invoke-virtual {p2, v4, v5}, LX/0nX;->a(D)V

    .line 1873617
    :cond_2
    const/4 v4, 0x2

    invoke-virtual {p0, v2, v4, v8, v9}, LX/15i;->a(IID)D

    move-result-wide v4

    .line 1873618
    cmpl-double v6, v4, v8

    if-eqz v6, :cond_3

    .line 1873619
    const-string v6, "x"

    invoke-virtual {p2, v6}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1873620
    invoke-virtual {p2, v4, v5}, LX/0nX;->a(D)V

    .line 1873621
    :cond_3
    const/4 v4, 0x3

    invoke-virtual {p0, v2, v4, v8, v9}, LX/15i;->a(IID)D

    move-result-wide v4

    .line 1873622
    cmpl-double v6, v4, v8

    if-eqz v6, :cond_4

    .line 1873623
    const-string v6, "y"

    invoke-virtual {p2, v6}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1873624
    invoke-virtual {p2, v4, v5}, LX/0nX;->a(D)V

    .line 1873625
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1873626
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1873627
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1873628
    :cond_6
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1873629
    return-void
.end method
