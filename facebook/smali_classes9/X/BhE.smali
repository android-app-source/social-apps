.class public LX/BhE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Bgq;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/Bgq",
        "<",
        "Landroid/widget/TextView;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1808933
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1808934
    return-void
.end method


# virtual methods
.method public final a()LX/BeE;
    .locals 1

    .prologue
    .line 1808935
    sget-object v0, LX/BeE;->SECTION_TITLE:LX/BeE;

    return-object v0
.end method

.method public final a(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 1808936
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1808937
    const v1, 0x7f031425

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1808938
    return-object v0
.end method

.method public final a(Landroid/view/View;Ljava/lang/Object;LX/BgS;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1808939
    check-cast p2, Ljava/lang/String;

    .line 1808940
    return-object p2
.end method

.method public final a(Landroid/view/View;Ljava/lang/Object;Ljava/lang/Object;LX/BgS;LX/BgK;LX/BeD;Landroid/support/v4/app/Fragment;Ljava/lang/String;)V
    .locals 0
    .param p3    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # LX/BeD;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1808941
    check-cast p1, Landroid/widget/TextView;

    check-cast p2, Ljava/lang/String;

    .line 1808942
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1808943
    return-void
.end method
