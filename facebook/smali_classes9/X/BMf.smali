.class public LX/BMf;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pp;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/BMg;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/BMf",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/BMg;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1777772
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1777773
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/BMf;->b:LX/0Zi;

    .line 1777774
    iput-object p1, p0, LX/BMf;->a:LX/0Ot;

    .line 1777775
    return-void
.end method

.method public static a(LX/0QB;)LX/BMf;
    .locals 4

    .prologue
    .line 1777761
    const-class v1, LX/BMf;

    monitor-enter v1

    .line 1777762
    :try_start_0
    sget-object v0, LX/BMf;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1777763
    sput-object v2, LX/BMf;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1777764
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1777765
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1777766
    new-instance v3, LX/BMf;

    const/16 p0, 0x2ffe

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/BMf;-><init>(LX/0Ot;)V

    .line 1777767
    move-object v0, v3

    .line 1777768
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1777769
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/BMf;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1777770
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1777771
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 10

    .prologue
    .line 1777702
    check-cast p2, LX/BMe;

    .line 1777703
    iget-object v0, p0, LX/BMf;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BMg;

    iget-object v1, p2, LX/BMe;->a:LX/1Ri;

    iget-object v2, p2, LX/BMe;->b:LX/1Pp;

    .line 1777704
    iget-object v3, v1, LX/1Ri;->a:LX/1RN;

    iget-object v3, v3, LX/1RN;->a:LX/1kK;

    check-cast v3, LX/1kW;

    .line 1777705
    iget-object v4, v3, LX/1kW;->a:Lcom/facebook/productionprompts/model/ProductionPrompt;

    move-object v5, v4

    .line 1777706
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x2

    invoke-interface {v3, v4}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x1

    invoke-interface {v3, v4}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v6

    .line 1777707
    iget-object v3, v0, LX/BMg;->c:LX/BMc;

    const/4 v4, 0x0

    .line 1777708
    new-instance v7, LX/BMb;

    invoke-direct {v7, v3}, LX/BMb;-><init>(LX/BMc;)V

    .line 1777709
    sget-object v8, LX/BMc;->a:LX/0Zi;

    invoke-virtual {v8}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/BMa;

    .line 1777710
    if-nez v8, :cond_0

    .line 1777711
    new-instance v8, LX/BMa;

    invoke-direct {v8}, LX/BMa;-><init>()V

    .line 1777712
    :cond_0
    invoke-static {v8, p1, v4, v4, v7}, LX/BMa;->a$redex0(LX/BMa;LX/1De;IILX/BMb;)V

    .line 1777713
    move-object v7, v8

    .line 1777714
    move-object v4, v7

    .line 1777715
    move-object v3, v4

    .line 1777716
    invoke-virtual {v5}, Lcom/facebook/productionprompts/model/ProductionPrompt;->b()Ljava/lang/String;

    move-result-object v4

    .line 1777717
    iget-object v7, v3, LX/BMa;->a:LX/BMb;

    iput-object v4, v7, LX/BMb;->a:Ljava/lang/String;

    .line 1777718
    iget-object v7, v3, LX/BMa;->d:Ljava/util/BitSet;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Ljava/util/BitSet;->set(I)V

    .line 1777719
    move-object v3, v3

    .line 1777720
    invoke-virtual {v5}, Lcom/facebook/productionprompts/model/ProductionPrompt;->c()Ljava/lang/String;

    move-result-object v4

    .line 1777721
    iget-object v7, v3, LX/BMa;->a:LX/BMb;

    iput-object v4, v7, LX/BMb;->b:Ljava/lang/String;

    .line 1777722
    move-object v3, v3

    .line 1777723
    invoke-virtual {v5}, Lcom/facebook/productionprompts/model/ProductionPrompt;->x()Ljava/lang/String;

    move-result-object v4

    .line 1777724
    iget-object v7, v3, LX/BMa;->a:LX/BMb;

    iput-object v4, v7, LX/BMb;->c:Ljava/lang/String;

    .line 1777725
    move-object v3, v3

    .line 1777726
    invoke-virtual {v5}, Lcom/facebook/productionprompts/model/ProductionPrompt;->C()LX/0Px;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {v5}, Lcom/facebook/productionprompts/model/ProductionPrompt;->C()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, v0, LX/BMg;->f:LX/1E1;

    .line 1777727
    iget-object v7, v4, LX/1E1;->a:LX/0ad;

    sget-short v8, LX/1Nu;->l:S

    const/4 v9, 0x0

    invoke-interface {v7, v8, v9}, LX/0ad;->a(SZ)Z

    move-result v7

    move v4, v7

    .line 1777728
    if-nez v4, :cond_5

    .line 1777729
    :cond_1
    const/4 v4, 0x0

    .line 1777730
    :goto_0
    move-object v4, v4

    .line 1777731
    iget-object v7, v3, LX/BMa;->a:LX/BMb;

    iput-object v4, v7, LX/BMb;->d:Ljava/util/List;

    .line 1777732
    move-object v3, v3

    .line 1777733
    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const/4 v4, 0x7

    const v7, 0x7f0b1171

    invoke-interface {v3, v4, v7}, LX/1Di;->g(II)LX/1Di;

    move-result-object v3

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-interface {v3, v4}, LX/1Di;->a(F)LX/1Di;

    move-result-object v4

    .line 1777734
    invoke-virtual {v5}, Lcom/facebook/productionprompts/model/ProductionPrompt;->e()Landroid/net/Uri;

    move-result-object v3

    if-nez v3, :cond_2

    const/4 v3, 0x0

    :goto_1
    invoke-interface {v6, v3}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v7

    invoke-virtual {v5}, Lcom/facebook/productionprompts/model/ProductionPrompt;->e()Landroid/net/Uri;

    move-result-object v3

    if-nez v3, :cond_4

    const/4 v3, 0x0

    const v5, 0x7f0b0060

    invoke-interface {v4, v3, v5}, LX/1Di;->c(II)LX/1Di;

    move-result-object v3

    :goto_2
    invoke-interface {v7, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    iget-object v4, v0, LX/BMg;->b:LX/BMm;

    invoke-virtual {v4, p1}, LX/BMm;->c(LX/1De;)LX/BMk;

    move-result-object v4

    invoke-virtual {v4, v1}, LX/BMk;->a(LX/1Ri;)LX/BMk;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    .line 1777735
    const v3, -0x20f39abb

    const/4 v4, 0x0

    invoke-static {p1, v3, v4}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v3

    move-object v3, v3

    .line 1777736
    invoke-interface {v6, v3}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 1777737
    return-object v0

    .line 1777738
    :cond_2
    iget-object v3, v0, LX/BMg;->a:LX/BMj;

    const/4 v7, 0x0

    .line 1777739
    new-instance v8, LX/BMi;

    invoke-direct {v8, v3}, LX/BMi;-><init>(LX/BMj;)V

    .line 1777740
    iget-object v9, v3, LX/BMj;->b:LX/0Zi;

    invoke-virtual {v9}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, LX/BMh;

    .line 1777741
    if-nez v9, :cond_3

    .line 1777742
    new-instance v9, LX/BMh;

    invoke-direct {v9, v3}, LX/BMh;-><init>(LX/BMj;)V

    .line 1777743
    :cond_3
    invoke-static {v9, p1, v7, v7, v8}, LX/BMh;->a$redex0(LX/BMh;LX/1De;IILX/BMi;)V

    .line 1777744
    move-object v8, v9

    .line 1777745
    move-object v7, v8

    .line 1777746
    move-object v3, v7

    .line 1777747
    invoke-virtual {v5}, Lcom/facebook/productionprompts/model/ProductionPrompt;->e()Landroid/net/Uri;

    move-result-object v7

    .line 1777748
    iget-object v8, v3, LX/BMh;->a:LX/BMi;

    iput-object v7, v8, LX/BMi;->a:Landroid/net/Uri;

    .line 1777749
    iget-object v8, v3, LX/BMh;->e:Ljava/util/BitSet;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Ljava/util/BitSet;->set(I)V

    .line 1777750
    move-object v3, v3

    .line 1777751
    iget-object v7, v3, LX/BMh;->a:LX/BMi;

    iput-object v2, v7, LX/BMi;->b:LX/1Pp;

    .line 1777752
    iget-object v7, v3, LX/BMh;->e:Ljava/util/BitSet;

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Ljava/util/BitSet;->set(I)V

    .line 1777753
    move-object v3, v3

    .line 1777754
    goto :goto_1

    :cond_4
    move-object v3, v4

    goto :goto_2

    .line 1777755
    :cond_5
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 1777756
    invoke-virtual {v5}, Lcom/facebook/productionprompts/model/ProductionPrompt;->C()LX/0Px;

    move-result-object v9

    invoke-virtual {v9}, LX/0Px;->size()I

    move-result p0

    const/4 v4, 0x0

    move v8, v4

    :goto_3
    if-ge v8, p0, :cond_7

    invoke-virtual {v9, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;

    .line 1777757
    invoke-virtual {v4}, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;->c()Ljava/lang/String;

    move-result-object p2

    invoke-static {p2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result p2

    if-nez p2, :cond_6

    .line 1777758
    invoke-virtual {v4}, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;->c()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-interface {v7, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1777759
    :cond_6
    add-int/lit8 v4, v8, 0x1

    move v8, v4

    goto :goto_3

    :cond_7
    move-object v4, v7

    .line 1777760
    goto/16 :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 1777692
    invoke-static {}, LX/1dS;->b()V

    .line 1777693
    iget v0, p1, LX/1dQ;->b:I

    .line 1777694
    packed-switch v0, :pswitch_data_0

    .line 1777695
    :goto_0
    return-object v2

    .line 1777696
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 1777697
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1777698
    check-cast v1, LX/BMe;

    .line 1777699
    iget-object v3, p0, LX/BMf;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/BMg;

    iget-object v4, v1, LX/BMe;->a:LX/1Ri;

    .line 1777700
    iget-object p1, v3, LX/BMg;->d:LX/BMP;

    iget-object p2, v4, LX/1Ri;->a:LX/1RN;

    iget-object p0, v3, LX/BMg;->e:LX/B5l;

    iget-object v1, v4, LX/1Ri;->a:LX/1RN;

    invoke-virtual {p0, v1}, LX/B5l;->a(LX/1RN;)LX/B5n;

    move-result-object p0

    invoke-virtual {p0}, LX/B5n;->a()LX/B5p;

    move-result-object p0

    invoke-virtual {p1, v0, p2, p0}, LX/B5s;->a(Landroid/view/View;LX/1RN;LX/B5o;)V

    .line 1777701
    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x20f39abb
        :pswitch_0
    .end packed-switch
.end method
