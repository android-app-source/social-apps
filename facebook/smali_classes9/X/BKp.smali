.class public final LX/BKp;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:J

.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lcom/facebook/graphql/model/GraphQLAlbum;

.field public d:Z

.field public e:I

.field public f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

.field public g:Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

.field public h:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

.field public i:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

.field public j:J

.field public k:Z

.field public l:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/model/FacebookProfile;",
            ">;"
        }
    .end annotation
.end field

.field public m:LX/5Rn;

.field public n:Ljava/lang/Long;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public p:Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;

.field public q:Lcom/facebook/ipc/composer/model/ComposerDateInfo;

.field public r:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;",
            ">;"
        }
    .end annotation
.end field

.field public s:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;",
            ">;"
        }
    .end annotation
.end field

.field public t:Lcom/facebook/ipc/composer/model/ComposerStickerData;

.field public u:Lcom/facebook/ipc/composer/intent/ComposerShareParams;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:Z

.field public w:Lcom/facebook/share/model/ComposerAppAttribution;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:Ljava/lang/String;

.field public y:Z

.field public z:Z


# direct methods
.method public constructor <init>(J)V
    .locals 1

    .prologue
    .line 1774277
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1774278
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1774279
    iput-object v0, p0, LX/BKp;->b:LX/0Px;

    .line 1774280
    const/4 v0, 0x0

    iput-object v0, p0, LX/BKp;->c:Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 1774281
    sget-object v0, LX/16z;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, LX/BKp;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1774282
    invoke-static {}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->newBuilder()LX/5RO;

    move-result-object v0

    invoke-virtual {v0}, LX/5RO;->b()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v0

    iput-object v0, p0, LX/BKp;->g:Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    .line 1774283
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1774284
    iput-object v0, p0, LX/BKp;->l:LX/0Px;

    .line 1774285
    sget-object v0, LX/5Rn;->NORMAL:LX/5Rn;

    iput-object v0, p0, LX/BKp;->m:LX/5Rn;

    .line 1774286
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1774287
    iput-object v0, p0, LX/BKp;->o:LX/0Px;

    .line 1774288
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1774289
    iput-object v0, p0, LX/BKp;->r:LX/0Px;

    .line 1774290
    sget-object v0, LX/0Rg;->a:LX/0Rg;

    move-object v0, v0

    .line 1774291
    iput-object v0, p0, LX/BKp;->s:LX/0P1;

    .line 1774292
    iput-wide p1, p0, LX/BKp;->a:J

    .line 1774293
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1774294
    iput-object v0, p0, LX/BKp;->l:LX/0Px;

    .line 1774295
    return-void
.end method

.method public constructor <init>(Lcom/facebook/platform/composer/model/PlatformComposition;)V
    .locals 2

    .prologue
    .line 1774296
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1774297
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1774298
    iput-object v0, p0, LX/BKp;->b:LX/0Px;

    .line 1774299
    const/4 v0, 0x0

    iput-object v0, p0, LX/BKp;->c:Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 1774300
    sget-object v0, LX/16z;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, LX/BKp;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1774301
    invoke-static {}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->newBuilder()LX/5RO;

    move-result-object v0

    invoke-virtual {v0}, LX/5RO;->b()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v0

    iput-object v0, p0, LX/BKp;->g:Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    .line 1774302
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1774303
    iput-object v0, p0, LX/BKp;->l:LX/0Px;

    .line 1774304
    sget-object v0, LX/5Rn;->NORMAL:LX/5Rn;

    iput-object v0, p0, LX/BKp;->m:LX/5Rn;

    .line 1774305
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1774306
    iput-object v0, p0, LX/BKp;->o:LX/0Px;

    .line 1774307
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1774308
    iput-object v0, p0, LX/BKp;->r:LX/0Px;

    .line 1774309
    sget-object v0, LX/0Rg;->a:LX/0Rg;

    move-object v0, v0

    .line 1774310
    iput-object v0, p0, LX/BKp;->s:LX/0P1;

    .line 1774311
    iget-wide v0, p1, Lcom/facebook/platform/composer/model/PlatformComposition;->mUserId:J

    iput-wide v0, p0, LX/BKp;->a:J

    .line 1774312
    iget-object v0, p1, Lcom/facebook/platform/composer/model/PlatformComposition;->mAttachments:LX/0Px;

    iput-object v0, p0, LX/BKp;->b:LX/0Px;

    .line 1774313
    iget-object v0, p1, Lcom/facebook/platform/composer/model/PlatformComposition;->mTargetAlbum:Lcom/facebook/graphql/model/GraphQLAlbum;

    iput-object v0, p0, LX/BKp;->c:Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 1774314
    iget-boolean v0, p1, Lcom/facebook/platform/composer/model/PlatformComposition;->mHasUserInteracted:Z

    iput-boolean v0, p0, LX/BKp;->d:Z

    .line 1774315
    iget v0, p1, Lcom/facebook/platform/composer/model/PlatformComposition;->mRating:I

    iput v0, p0, LX/BKp;->e:I

    .line 1774316
    iget-object v0, p1, Lcom/facebook/platform/composer/model/PlatformComposition;->mTextWithEntities:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, LX/BKp;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1774317
    iget-object v0, p1, Lcom/facebook/platform/composer/model/PlatformComposition;->mLocationInfo:Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    iput-object v0, p0, LX/BKp;->g:Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    .line 1774318
    iget-object v0, p1, Lcom/facebook/platform/composer/model/PlatformComposition;->mMinutiaeObject:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    iput-object v0, p0, LX/BKp;->h:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    .line 1774319
    iget-object v0, p1, Lcom/facebook/platform/composer/model/PlatformComposition;->mProductItemAttachment:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    iput-object v0, p0, LX/BKp;->i:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    .line 1774320
    iget-wide v0, p1, Lcom/facebook/platform/composer/model/PlatformComposition;->mMarketplaceId:J

    iput-wide v0, p0, LX/BKp;->j:J

    .line 1774321
    iget-boolean v0, p1, Lcom/facebook/platform/composer/model/PlatformComposition;->mUserSelectedTags:Z

    iput-boolean v0, p0, LX/BKp;->k:Z

    .line 1774322
    iget-object v0, p1, Lcom/facebook/platform/composer/model/PlatformComposition;->mTaggedProfiles:LX/0Px;

    iput-object v0, p0, LX/BKp;->l:LX/0Px;

    .line 1774323
    iget-object v0, p1, Lcom/facebook/platform/composer/model/PlatformComposition;->mPublishMode:LX/5Rn;

    iput-object v0, p0, LX/BKp;->m:LX/5Rn;

    .line 1774324
    iget-object v0, p1, Lcom/facebook/platform/composer/model/PlatformComposition;->mScheduleTime:Ljava/lang/Long;

    iput-object v0, p0, LX/BKp;->n:Ljava/lang/Long;

    .line 1774325
    iget-object v0, p1, Lcom/facebook/platform/composer/model/PlatformComposition;->mRemovedURLs:LX/0Px;

    iput-object v0, p0, LX/BKp;->o:LX/0Px;

    .line 1774326
    iget-object v0, p1, Lcom/facebook/platform/composer/model/PlatformComposition;->a:Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;

    iput-object v0, p0, LX/BKp;->p:Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;

    .line 1774327
    iget-object v0, p1, Lcom/facebook/platform/composer/model/PlatformComposition;->mComposerDateInfo:Lcom/facebook/ipc/composer/model/ComposerDateInfo;

    iput-object v0, p0, LX/BKp;->q:Lcom/facebook/ipc/composer/model/ComposerDateInfo;

    .line 1774328
    iget-object v0, p1, Lcom/facebook/platform/composer/model/PlatformComposition;->b:LX/0Px;

    iput-object v0, p0, LX/BKp;->r:LX/0Px;

    .line 1774329
    iget-object v0, p1, Lcom/facebook/platform/composer/model/PlatformComposition;->c:LX/0P1;

    iput-object v0, p0, LX/BKp;->s:LX/0P1;

    .line 1774330
    iget-object v0, p1, Lcom/facebook/platform/composer/model/PlatformComposition;->mReferencedStickerData:Lcom/facebook/ipc/composer/model/ComposerStickerData;

    iput-object v0, p0, LX/BKp;->t:Lcom/facebook/ipc/composer/model/ComposerStickerData;

    .line 1774331
    iget-object v0, p1, Lcom/facebook/platform/composer/model/PlatformComposition;->mShareParams:Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    iput-object v0, p0, LX/BKp;->u:Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    .line 1774332
    iget-boolean v0, p1, Lcom/facebook/platform/composer/model/PlatformComposition;->mIsBackoutDraft:Z

    iput-boolean v0, p0, LX/BKp;->v:Z

    .line 1774333
    iget-object v0, p1, Lcom/facebook/platform/composer/model/PlatformComposition;->mAppAttribution:Lcom/facebook/share/model/ComposerAppAttribution;

    iput-object v0, p0, LX/BKp;->w:Lcom/facebook/share/model/ComposerAppAttribution;

    .line 1774334
    iget-object v0, p1, Lcom/facebook/platform/composer/model/PlatformComposition;->mAppProvidedHashtag:Ljava/lang/String;

    iput-object v0, p0, LX/BKp;->x:Ljava/lang/String;

    .line 1774335
    iget-boolean v0, p1, Lcom/facebook/platform/composer/model/PlatformComposition;->mUserDeletedAppProvidedHashtag:Z

    iput-boolean v0, p0, LX/BKp;->y:Z

    .line 1774336
    invoke-virtual {p1}, Lcom/facebook/platform/composer/model/PlatformComposition;->z()Z

    move-result v0

    iput-boolean v0, p0, LX/BKp;->z:Z

    .line 1774337
    return-void
.end method


# virtual methods
.method public final a(LX/0Px;)LX/BKp;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;)",
            "LX/BKp;"
        }
    .end annotation

    .prologue
    .line 1774338
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    iput-object v0, p0, LX/BKp;->b:LX/0Px;

    .line 1774339
    return-object p0
.end method

.method public final b(LX/0Px;)LX/BKp;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/model/FacebookProfile;",
            ">;)",
            "LX/BKp;"
        }
    .end annotation

    .prologue
    .line 1774340
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    iput-object v0, p0, LX/BKp;->l:LX/0Px;

    .line 1774341
    return-object p0
.end method

.method public final b()Lcom/facebook/platform/composer/model/PlatformComposition;
    .locals 2

    .prologue
    .line 1774342
    new-instance v0, Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-direct {v0, p0}, Lcom/facebook/platform/composer/model/PlatformComposition;-><init>(LX/BKp;)V

    return-object v0
.end method
