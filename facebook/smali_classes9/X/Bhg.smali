.class public final LX/Bhg;
.super LX/3nE;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/3nE",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "LX/3K3;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/20s;

.field private b:Landroid/content/res/Resources;

.field private c:LX/1zm;


# direct methods
.method public constructor <init>(LX/20s;)V
    .locals 0

    .prologue
    .line 1809587
    iput-object p1, p0, LX/Bhg;->a:LX/20s;

    invoke-direct {p0}, LX/3nE;-><init>()V

    return-void
.end method


# virtual methods
.method public final a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1809578
    check-cast p1, [Ljava/lang/String;

    const/4 v3, 0x0

    .line 1809579
    :try_start_0
    iget-object v0, p0, LX/Bhg;->b:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v1, p1, v1

    invoke-virtual {v0, v1}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1809580
    :try_start_1
    invoke-static {v1}, LX/3JK;->a(Ljava/io/InputStream;)LX/3K3;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 1809581
    :try_start_2
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 1809582
    :goto_0
    return-object v0

    .line 1809583
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    throw v0
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 1809584
    :catch_0
    move-exception v0

    .line 1809585
    sget-object v1, LX/20s;->a:Ljava/lang/Class;

    const-string v2, "Was trying to load the Choose Love Keyframes animation JSON"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1809586
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onPostExecute(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 1809570
    check-cast p1, LX/3K3;

    .line 1809571
    iget-object v0, p0, LX/Bhg;->a:LX/20s;

    .line 1809572
    iput-object p1, v0, LX/20s;->i:LX/3K3;

    .line 1809573
    iget-object v0, p0, LX/Bhg;->a:LX/20s;

    invoke-static {v0}, LX/20s;->e(LX/20s;)V

    .line 1809574
    return-void
.end method

.method public final onPreExecute()V
    .locals 1

    .prologue
    .line 1809575
    iget-object v0, p0, LX/Bhg;->a:LX/20s;

    iget-object v0, v0, LX/20s;->e:Landroid/content/res/Resources;

    iput-object v0, p0, LX/Bhg;->b:Landroid/content/res/Resources;

    .line 1809576
    iget-object v0, p0, LX/Bhg;->a:LX/20s;

    iget-object v0, v0, LX/20s;->b:LX/1zm;

    iput-object v0, p0, LX/Bhg;->c:LX/1zm;

    .line 1809577
    return-void
.end method
