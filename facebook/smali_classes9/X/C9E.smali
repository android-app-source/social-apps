.class public LX/C9E;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0ad;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0qn;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1853855
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1853856
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1853857
    iput-object v0, p0, LX/C9E;->a:LX/0Ot;

    .line 1853858
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1853859
    iput-object v0, p0, LX/C9E;->b:LX/0Ot;

    .line 1853860
    return-void
.end method

.method public static b(LX/0QB;)LX/C9E;
    .locals 3

    .prologue
    .line 1853861
    new-instance v0, LX/C9E;

    invoke-direct {v0}, LX/C9E;-><init>()V

    .line 1853862
    const/16 v1, 0x1032

    invoke-static {p0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0x110

    invoke-static {p0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    .line 1853863
    iput-object v1, v0, LX/C9E;->a:LX/0Ot;

    iput-object v2, v0, LX/C9E;->b:LX/0Ot;

    .line 1853864
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1853865
    invoke-static {p1}, LX/17E;->m(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1}, LX/17E;->q(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 1853866
    :goto_0
    return v0

    .line 1853867
    :cond_0
    iget-object v0, p0, LX/C9E;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    invoke-static {v0}, LX/8Nv;->a(LX/0ad;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p1}, LX/17E;->p(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 1853868
    goto :goto_0

    .line 1853869
    :cond_1
    iget-object v0, p0, LX/C9E;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0qn;

    invoke-virtual {v0, p1}, LX/0qn;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    move-result-object v0

    .line 1853870
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->SUCCESS:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    if-ne v0, v2, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method
