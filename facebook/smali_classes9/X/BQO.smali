.class public LX/BQO;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0Tn;

.field public static final b:LX/0Tn;

.field public static final c:LX/0Tn;

.field public static final d:LX/0Tn;

.field private static final e:LX/0Tn;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1782058
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "timeline/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 1782059
    sput-object v0, LX/BQO;->e:LX/0Tn;

    const-string v1, "pause_updates"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/BQO;->a:LX/0Tn;

    .line 1782060
    sget-object v0, LX/BQO;->e:LX/0Tn;

    const-string v1, "show_tooltips"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/BQO;->b:LX/0Tn;

    .line 1782061
    sget-object v0, LX/BQO;->e:LX/0Tn;

    const-string v1, "show_debug_overlay"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/BQO;->c:LX/0Tn;

    .line 1782062
    sget-object v0, LX/BQO;->e:LX/0Tn;

    const-string v1, "is_flat_buffer_corrupt"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/BQO;->d:LX/0Tn;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1782063
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
