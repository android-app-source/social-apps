.class public final LX/Br4;
.super LX/2eI;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2eI",
        "<TE;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic b:LX/0Px;

.field public final synthetic c:Lcom/facebook/graphql/model/GraphQLStory;

.field public final synthetic d:Lcom/facebook/feed/rows/sections/SubStoriesHScrollPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/sections/SubStoriesHScrollPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;LX/0Px;Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 0

    .prologue
    .line 1825790
    iput-object p1, p0, LX/Br4;->d:Lcom/facebook/feed/rows/sections/SubStoriesHScrollPartDefinition;

    iput-object p2, p0, LX/Br4;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iput-object p3, p0, LX/Br4;->b:LX/0Px;

    iput-object p4, p0, LX/Br4;->c:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-direct {p0}, LX/2eI;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/2eI;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PageSubParts",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    .line 1825791
    iget-object v0, p0, LX/Br4;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1825792
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 1825793
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/1VF;->e(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v2

    .line 1825794
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/Br4;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1825795
    if-eqz v2, :cond_0

    .line 1825796
    iget-object v0, p0, LX/Br4;->d:Lcom/facebook/feed/rows/sections/SubStoriesHScrollPartDefinition;

    iget-object v0, v0, Lcom/facebook/feed/rows/sections/SubStoriesHScrollPartDefinition;->c:Lcom/facebook/feed/rows/sections/EventSubStoriesPagePartDefinition;

    sget-object v3, Lcom/facebook/feed/rows/sections/EventSubStoriesPagePartDefinition;->a:LX/1Cz;

    iget-object v4, p0, LX/Br4;->d:Lcom/facebook/feed/rows/sections/SubStoriesHScrollPartDefinition;

    iget-object v4, v4, Lcom/facebook/feed/rows/sections/SubStoriesHScrollPartDefinition;->a:Landroid/content/Context;

    iget-object v5, p0, LX/Br4;->d:Lcom/facebook/feed/rows/sections/SubStoriesHScrollPartDefinition;

    iget-object v5, v5, Lcom/facebook/feed/rows/sections/SubStoriesHScrollPartDefinition;->i:LX/1Qx;

    invoke-static {v0, v3, v4, v5}, LX/6Vo;->a(Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;LX/1Cz;Landroid/content/Context;LX/1Qx;)LX/1RC;

    move-result-object v3

    iget-object v4, p0, LX/Br4;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v0, p0, LX/Br4;->b:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;

    invoke-virtual {v4, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, LX/2eI;->a(LX/1RC;Ljava/lang/Object;)V

    .line 1825797
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1825798
    :cond_0
    iget-object v0, p0, LX/Br4;->d:Lcom/facebook/feed/rows/sections/SubStoriesHScrollPartDefinition;

    iget-object v3, v0, Lcom/facebook/feed/rows/sections/SubStoriesHScrollPartDefinition;->b:Lcom/facebook/feed/rows/sections/SubStoriesPagePartDefinition;

    iget-object v4, p0, LX/Br4;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v0, p0, LX/Br4;->b:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;

    invoke-virtual {v4, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, LX/2eI;->a(LX/1RC;Ljava/lang/Object;)V

    goto :goto_1

    .line 1825799
    :cond_1
    return-void
.end method

.method public final c(I)V
    .locals 1

    .prologue
    .line 1825800
    iget-object v0, p0, LX/Br4;->c:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0, p1}, LX/1mc;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;I)V

    .line 1825801
    return-void
.end method
