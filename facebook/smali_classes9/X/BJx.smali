.class public final LX/BJx;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V
    .locals 0

    .prologue
    .line 1772507
    iput-object p1, p0, LX/BJx;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1772508
    iget-object v0, p0, LX/BJx;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v0, v0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v0, v0, LX/BKm;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Lcom/facebook/ipc/composer/intent/PlatformConfiguration;
    .locals 1

    .prologue
    .line 1772509
    iget-object v0, p0, LX/BJx;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v0, v0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v0, v0, LX/BKm;->b:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getPlatformConfiguration()Lcom/facebook/ipc/composer/intent/PlatformConfiguration;

    move-result-object v0

    return-object v0
.end method

.method public final c()Lcom/facebook/ipc/composer/intent/ComposerShareParams;
    .locals 1

    .prologue
    .line 1772510
    iget-object v0, p0, LX/BJx;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v0, v0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v0, v0, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v0}, Lcom/facebook/platform/composer/model/PlatformComposition;->p()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v0

    return-object v0
.end method

.method public final d()Lcom/facebook/share/model/ComposerAppAttribution;
    .locals 1

    .prologue
    .line 1772511
    iget-object v0, p0, LX/BJx;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v0, v0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v0, v0, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v0}, Lcom/facebook/platform/composer/model/PlatformComposition;->w()Lcom/facebook/share/model/ComposerAppAttribution;

    move-result-object v0

    return-object v0
.end method
