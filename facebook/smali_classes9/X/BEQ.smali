.class public final enum LX/BEQ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/BEQ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/BEQ;

.field public static final enum ACTIVITY:LX/BEQ;

.field public static final enum INSIGHTS:LX/BEQ;

.field public static final enum PUBLIC:LX/BEQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1764461
    new-instance v0, LX/BEQ;

    const-string v1, "PUBLIC"

    invoke-direct {v0, v1, v2}, LX/BEQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BEQ;->PUBLIC:LX/BEQ;

    .line 1764462
    new-instance v0, LX/BEQ;

    const-string v1, "ACTIVITY"

    invoke-direct {v0, v1, v3}, LX/BEQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BEQ;->ACTIVITY:LX/BEQ;

    .line 1764463
    new-instance v0, LX/BEQ;

    const-string v1, "INSIGHTS"

    invoke-direct {v0, v1, v4}, LX/BEQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BEQ;->INSIGHTS:LX/BEQ;

    .line 1764464
    const/4 v0, 0x3

    new-array v0, v0, [LX/BEQ;

    sget-object v1, LX/BEQ;->PUBLIC:LX/BEQ;

    aput-object v1, v0, v2

    sget-object v1, LX/BEQ;->ACTIVITY:LX/BEQ;

    aput-object v1, v0, v3

    sget-object v1, LX/BEQ;->INSIGHTS:LX/BEQ;

    aput-object v1, v0, v4

    sput-object v0, LX/BEQ;->$VALUES:[LX/BEQ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1764465
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/BEQ;
    .locals 1

    .prologue
    .line 1764466
    const-class v0, LX/BEQ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/BEQ;

    return-object v0
.end method

.method public static values()[LX/BEQ;
    .locals 1

    .prologue
    .line 1764467
    sget-object v0, LX/BEQ;->$VALUES:[LX/BEQ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/BEQ;

    return-object v0
.end method
