.class public final LX/AqQ;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/AqR;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/1X1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1X1",
            "<*>;"
        }
    .end annotation
.end field

.field public b:LX/1X1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1X1",
            "<*>;"
        }
    .end annotation
.end field

.field public c:LX/1X1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1X1",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1717883
    invoke-static {}, LX/AqR;->q()LX/AqR;

    move-result-object v0

    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1717884
    return-void
.end method

.method public synthetic constructor <init>(B)V
    .locals 0

    .prologue
    .line 1717885
    invoke-direct {p0}, LX/AqQ;-><init>()V

    return-void
.end method

.method private o()LX/AqQ;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1717886
    invoke-super {p0}, LX/1X1;->g()LX/1X1;

    move-result-object v0

    check-cast v0, LX/AqQ;

    .line 1717887
    iget-object v1, v0, LX/AqQ;->a:LX/1X1;

    if-eqz v1, :cond_1

    iget-object v1, v0, LX/AqQ;->a:LX/1X1;

    invoke-virtual {v1}, LX/1X1;->g()LX/1X1;

    move-result-object v1

    :goto_0
    iput-object v1, v0, LX/AqQ;->a:LX/1X1;

    .line 1717888
    iget-object v1, v0, LX/AqQ;->b:LX/1X1;

    if-eqz v1, :cond_2

    iget-object v1, v0, LX/AqQ;->b:LX/1X1;

    invoke-virtual {v1}, LX/1X1;->g()LX/1X1;

    move-result-object v1

    :goto_1
    iput-object v1, v0, LX/AqQ;->b:LX/1X1;

    .line 1717889
    iget-object v1, v0, LX/AqQ;->c:LX/1X1;

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/AqQ;->c:LX/1X1;

    invoke-virtual {v1}, LX/1X1;->g()LX/1X1;

    move-result-object v2

    :cond_0
    iput-object v2, v0, LX/AqQ;->c:LX/1X1;

    .line 1717890
    return-object v0

    :cond_1
    move-object v1, v2

    .line 1717891
    goto :goto_0

    :cond_2
    move-object v1, v2

    .line 1717892
    goto :goto_1
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1717893
    const-string v0, "FigHeaderComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1717894
    if-ne p0, p1, :cond_1

    .line 1717895
    :cond_0
    :goto_0
    return v0

    .line 1717896
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1717897
    goto :goto_0

    .line 1717898
    :cond_3
    check-cast p1, LX/AqQ;

    .line 1717899
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1717900
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1717901
    if-eq v2, v3, :cond_0

    .line 1717902
    iget-object v2, p0, LX/AqQ;->a:LX/1X1;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/AqQ;->a:LX/1X1;

    iget-object v3, p1, LX/AqQ;->a:LX/1X1;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1717903
    goto :goto_0

    .line 1717904
    :cond_5
    iget-object v2, p1, LX/AqQ;->a:LX/1X1;

    if-nez v2, :cond_4

    .line 1717905
    :cond_6
    iget-object v2, p0, LX/AqQ;->b:LX/1X1;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/AqQ;->b:LX/1X1;

    iget-object v3, p1, LX/AqQ;->b:LX/1X1;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1717906
    goto :goto_0

    .line 1717907
    :cond_8
    iget-object v2, p1, LX/AqQ;->b:LX/1X1;

    if-nez v2, :cond_7

    .line 1717908
    :cond_9
    iget-object v2, p0, LX/AqQ;->c:LX/1X1;

    if-eqz v2, :cond_a

    iget-object v2, p0, LX/AqQ;->c:LX/1X1;

    iget-object v3, p1, LX/AqQ;->c:LX/1X1;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1717909
    goto :goto_0

    .line 1717910
    :cond_a
    iget-object v2, p1, LX/AqQ;->c:LX/1X1;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public final synthetic g()LX/1X1;
    .locals 1

    .prologue
    .line 1717911
    invoke-direct {p0}, LX/AqQ;->o()LX/AqQ;

    move-result-object v0

    return-object v0
.end method
