.class public final LX/Cgg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:I

.field public final synthetic b:Lcom/facebook/reviews/composer/ComposerRatingView;


# direct methods
.method public constructor <init>(Lcom/facebook/reviews/composer/ComposerRatingView;I)V
    .locals 0

    .prologue
    .line 1927337
    iput-object p1, p0, LX/Cgg;->b:Lcom/facebook/reviews/composer/ComposerRatingView;

    iput p2, p0, LX/Cgg;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 1927338
    iget-object v0, p0, LX/Cgg;->b:Lcom/facebook/reviews/composer/ComposerRatingView;

    iget v1, p0, LX/Cgg;->a:I

    invoke-static {v0, v1}, Lcom/facebook/reviews/composer/ComposerRatingView;->setRatingSelector(Lcom/facebook/reviews/composer/ComposerRatingView;I)V

    .line 1927339
    iget-object v0, p0, LX/Cgg;->b:Lcom/facebook/reviews/composer/ComposerRatingView;

    iget-object v0, v0, Lcom/facebook/reviews/composer/ComposerRatingView;->g:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1927340
    iget-object v0, p0, LX/Cgg;->b:Lcom/facebook/reviews/composer/ComposerRatingView;

    iget-object v0, v0, Lcom/facebook/reviews/composer/ComposerRatingView;->g:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cgh;

    iget v1, p0, LX/Cgg;->a:I

    invoke-interface {v0, v1}, LX/Cgh;->c(I)V

    .line 1927341
    :cond_0
    const/4 v0, 0x1

    return v0
.end method
