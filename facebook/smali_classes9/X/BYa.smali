.class public LX/BYa;
.super LX/45y;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:LX/0Xp;

.field private final c:LX/BYc;

.field private final d:LX/0yI;

.field private final e:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final f:LX/0Zb;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1796272
    const-class v0, LX/BYa;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/BYa;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Xp;LX/BYc;LX/0yI;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1796273
    invoke-direct {p0}, LX/45y;-><init>()V

    .line 1796274
    iput-object p1, p0, LX/BYa;->b:LX/0Xp;

    .line 1796275
    iput-object p2, p0, LX/BYa;->c:LX/BYc;

    .line 1796276
    iput-object p3, p0, LX/BYa;->d:LX/0yI;

    .line 1796277
    iput-object p4, p0, LX/BYa;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1796278
    iput-object p5, p0, LX/BYa;->f:LX/0Zb;

    .line 1796279
    return-void
.end method

.method private a(ZZ)V
    .locals 5

    .prologue
    .line 1796280
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "offpeak_download_job_started"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "zero_module"

    .line 1796281
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1796282
    move-object v1, v0

    .line 1796283
    const-string v0, "carrier_id"

    iget-object v2, p0, LX/BYa;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/0yh;->NORMAL:LX/0yh;

    invoke-virtual {v3}, LX/0yh;->getCarrierIdKey()LX/0Tn;

    move-result-object v3

    const-string v4, ""

    invoke-interface {v2, v3, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1796284
    const-string v0, "service"

    const-string v2, "gcm"

    invoke-virtual {v1, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1796285
    const-string v2, "action"

    if-eqz p1, :cond_0

    const-string v0, "start"

    :goto_0
    invoke-virtual {v1, v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1796286
    const-string v0, "refresh"

    invoke-virtual {v1, v0, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1796287
    iget-object v0, p0, LX/BYa;->f:LX/0Zb;

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1796288
    return-void

    .line 1796289
    :cond_0
    const-string v0, "stop"

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/BYa;
    .locals 6

    .prologue
    .line 1796290
    new-instance v0, LX/BYa;

    invoke-static {p0}, LX/0Xo;->a(LX/0QB;)LX/0Xp;

    move-result-object v1

    check-cast v1, LX/0Xp;

    invoke-static {p0}, LX/BYc;->a(LX/0QB;)LX/BYc;

    move-result-object v2

    check-cast v2, LX/BYc;

    invoke-static {p0}, LX/0yI;->b(LX/0QB;)LX/0yI;

    move-result-object v3

    check-cast v3, LX/0yI;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v4

    check-cast v4, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v5

    check-cast v5, LX/0Zb;

    invoke-direct/range {v0 .. v5}, LX/BYa;-><init>(LX/0Xp;LX/BYc;LX/0yI;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Zb;)V

    .line 1796291
    return-object v0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 1796292
    const/4 v0, 0x0

    return v0
.end method

.method public final a(ILandroid/os/Bundle;LX/45o;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1796293
    const v2, 0x7f0d00f9

    if-ne p1, v2, :cond_1

    .line 1796294
    iget-object v2, p0, LX/BYa;->b:LX/0Xp;

    new-instance v3, Landroid/content/Intent;

    const-string v4, "com.facebook.zero.offpeakdownload.START_OFFPEAK_DOWNLOAD_ACTION"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, LX/0Xp;->a(Landroid/content/Intent;)Z

    .line 1796295
    iget-object v2, p0, LX/BYa;->c:LX/BYc;

    .line 1796296
    iput-boolean v1, v2, LX/BYc;->i:Z

    .line 1796297
    iget-object v2, p0, LX/BYa;->d:LX/0yI;

    sget-object v3, LX/0yY;->OFF_PEAK_VIDEO_DOWNLOAD:LX/0yY;

    invoke-virtual {v2, v3}, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->a(LX/0yY;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1796298
    iget-object v1, p0, LX/BYa;->c:LX/BYc;

    invoke-virtual {v1}, LX/BYc;->c()V

    .line 1796299
    invoke-direct {p0, v0, v0}, LX/BYa;->a(ZZ)V

    .line 1796300
    :goto_0
    return v0

    .line 1796301
    :cond_0
    invoke-direct {p0, v0, v1}, LX/BYa;->a(ZZ)V

    goto :goto_0

    .line 1796302
    :cond_1
    const v2, 0x7f0d00fa

    if-ne p1, v2, :cond_3

    .line 1796303
    iget-object v2, p0, LX/BYa;->b:LX/0Xp;

    new-instance v3, Landroid/content/Intent;

    const-string v4, "com.facebook.zero.offpeakdownload.STOP_OFFPEAK_DOWNLOAD_ACTION"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, LX/0Xp;->a(Landroid/content/Intent;)Z

    .line 1796304
    iget-object v2, p0, LX/BYa;->c:LX/BYc;

    .line 1796305
    iput-boolean v1, v2, LX/BYc;->j:Z

    .line 1796306
    iget-object v2, p0, LX/BYa;->d:LX/0yI;

    sget-object v3, LX/0yY;->OFF_PEAK_VIDEO_DOWNLOAD:LX/0yY;

    invoke-virtual {v2, v3}, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->a(LX/0yY;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1796307
    iget-object v2, p0, LX/BYa;->c:LX/BYc;

    invoke-virtual {v2}, LX/BYc;->c()V

    .line 1796308
    invoke-direct {p0, v1, v0}, LX/BYa;->a(ZZ)V

    goto :goto_0

    .line 1796309
    :cond_2
    invoke-direct {p0, v1, v1}, LX/BYa;->a(ZZ)V

    goto :goto_0

    :cond_3
    move v0, v1

    .line 1796310
    goto :goto_0
.end method
