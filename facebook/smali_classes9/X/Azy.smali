.class public LX/Azy;
.super Landroid/widget/FrameLayout;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public b:LX/Azu;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0oJ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/BA0;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/87h;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/23P;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThreadImmediate;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final h:Landroid/widget/FrameLayout;

.field private final i:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

.field private final j:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

.field private final k:LX/Azz;

.field public final l:LX/Azp;

.field private final m:LX/Azk;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1733164
    const-class v0, LX/Azy;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Azy;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1733162
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/Azy;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1733163
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1733165
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/Azy;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1733166
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 7

    .prologue
    .line 1733091
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1733092
    const-class v0, LX/Azy;

    invoke-static {v0, p0}, LX/Azy;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1733093
    const v0, 0x7f0307b7

    invoke-static {p1, v0, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1733094
    invoke-virtual {p0}, LX/Azy;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a004b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, LX/Azy;->setBackgroundColor(I)V

    .line 1733095
    const v0, 0x7f0d1483

    invoke-virtual {p0, v0}, LX/Azy;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 1733096
    const v1, 0x7f0d1486

    invoke-virtual {p0, v1}, LX/Azy;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbTextView;

    .line 1733097
    iget-object v2, p0, LX/Azy;->f:LX/23P;

    invoke-virtual {p0}, LX/Azy;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f082401

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, LX/23P;->getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1733098
    iget-object v0, p0, LX/Azy;->f:LX/23P;

    invoke-virtual {p0}, LX/Azy;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f082402

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, LX/23P;->getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1733099
    new-instance v0, LX/Azp;

    invoke-direct {v0, p1}, LX/Azp;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/Azy;->l:LX/Azp;

    .line 1733100
    new-instance v0, LX/Azz;

    invoke-direct {v0, p1}, LX/Azz;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/Azy;->k:LX/Azz;

    .line 1733101
    new-instance v0, LX/Azk;

    invoke-direct {v0, p1}, LX/Azk;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/Azy;->m:LX/Azk;

    .line 1733102
    const v0, 0x7f0d1485

    invoke-virtual {p0, v0}, LX/Azy;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, LX/Azy;->h:Landroid/widget/FrameLayout;

    .line 1733103
    const v0, 0x7f0d1484

    invoke-virtual {p0, v0}, LX/Azy;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    iput-object v0, p0, LX/Azy;->j:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    .line 1733104
    const v0, 0x7f0d1482

    invoke-virtual {p0, v0}, LX/Azy;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    iput-object v0, p0, LX/Azy;->i:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    .line 1733105
    iget-object v0, p0, LX/Azy;->i:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setShowSegmentedDividers(I)V

    .line 1733106
    iget-object v0, p0, LX/Azy;->i:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setSegmentedDividerThickness(I)V

    .line 1733107
    iget-object v0, p0, LX/Azy;->i:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00e8

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setSegmentedDivider(Landroid/graphics/drawable/Drawable;)V

    .line 1733108
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 1733109
    const-string v1, "camera"

    move-object v0, v1

    .line 1733110
    iget-object v1, p0, LX/Azy;->l:LX/Azp;

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1733111
    const-string v1, "composer"

    move-object v0, v1

    .line 1733112
    iget-object v1, p0, LX/Azy;->m:LX/Azk;

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1733113
    const-string v0, "gallery"

    move-object v0, v0

    .line 1733114
    iget-object v1, p0, LX/Azy;->k:LX/Azz;

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1733115
    iget-object v0, p0, LX/Azy;->c:LX/0oJ;

    const/4 v5, 0x0

    .line 1733116
    iget-object v1, v0, LX/0oJ;->a:LX/0ad;

    sget-char v3, LX/0ob;->o:C

    invoke-interface {v1, v3, v5}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1733117
    iget-object v3, v0, LX/0oJ;->a:LX/0ad;

    sget-char v4, LX/0ob;->A:C

    invoke-interface {v3, v4, v5}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1733118
    if-eqz v1, :cond_4

    .line 1733119
    invoke-static {v1}, LX/0oJ;->a(Ljava/lang/String;)LX/0Px;

    move-result-object v1

    .line 1733120
    :goto_0
    move-object v3, v1

    .line 1733121
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_0

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1733122
    iget-object v5, p0, LX/Azy;->b:LX/Azu;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Azj;

    .line 1733123
    iget-object p1, v5, LX/Azu;->b:Ljava/util/List;

    new-instance p2, LX/Azt;

    invoke-direct {p2, v0}, LX/Azt;-><init>(LX/Azj;)V

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1733124
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1733125
    :cond_0
    iget-object v0, p0, LX/Azy;->b:LX/Azu;

    iget-object v1, p0, LX/Azy;->j:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    iget-object v2, p0, LX/Azy;->h:Landroid/widget/FrameLayout;

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1733126
    invoke-virtual {v1, v6}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setSegmentedDividerThickness(I)V

    .line 1733127
    const/4 v3, 0x2

    invoke-virtual {v1, v3}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setShowSegmentedDividers(I)V

    .line 1733128
    invoke-virtual {v1}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a00e8

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setSegmentedDivider(Landroid/graphics/drawable/Drawable;)V

    .line 1733129
    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setSegmentedDividerPadding(I)V

    .line 1733130
    iget-object v3, v0, LX/Azu;->b:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Azt;

    .line 1733131
    invoke-virtual {v3}, LX/Azt;->b()Landroid/view/View;

    move-result-object p2

    .line 1733132
    iget-object p0, v0, LX/Azu;->a:LX/0hB;

    invoke-virtual {p0}, LX/0hB;->c()I

    move-result p1

    .line 1733133
    iget-object p0, v0, LX/Azu;->a:LX/0hB;

    invoke-virtual {p0}, LX/0hB;->d()I

    move-result p0

    .line 1733134
    if-le p1, p0, :cond_6

    .line 1733135
    int-to-float p1, p0

    invoke-virtual {v3}, LX/Azt;->a()F

    move-result p3

    div-float/2addr p1, p3

    float-to-int p1, p1

    .line 1733136
    :cond_1
    :goto_3
    new-instance p3, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {p3, p1, p0}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 1733137
    invoke-virtual {p2, p3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1733138
    invoke-virtual {v2, p2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 1733139
    const/4 p2, -0x2

    .line 1733140
    new-instance p0, LX/Azv;

    invoke-virtual {v1}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-direct {p0, p1}, LX/Azv;-><init>(Landroid/content/Context;)V

    .line 1733141
    iget-object p1, v3, LX/Azt;->b:LX/Azj;

    invoke-interface {p1}, LX/Azj;->getGlyphLabel()I

    move-result p1

    move p1, p1

    .line 1733142
    iget-object p3, p0, LX/Azv;->a:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {p3, p1}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 1733143
    new-instance p1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {p1, p2, p2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1733144
    const/high16 p2, 0x3f800000    # 1.0f

    iput p2, p1, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 1733145
    const/16 p2, 0x11

    iput p2, p1, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 1733146
    invoke-virtual {p0, p1}, LX/Azv;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1733147
    iget-object p1, v0, LX/Azu;->c:Ljava/util/List;

    invoke-interface {p1, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1733148
    new-instance p1, LX/Azq;

    invoke-direct {p1, v0, v3, p0}, LX/Azq;-><init>(LX/Azu;LX/Azt;LX/Azv;)V

    invoke-virtual {p0, p1}, LX/Azv;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1733149
    invoke-virtual {v1, p0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->addView(Landroid/view/View;)V

    .line 1733150
    invoke-virtual {v3}, LX/Azt;->b()Landroid/view/View;

    move-result-object p0

    const/16 p1, 0x8

    invoke-virtual {p0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 1733151
    goto :goto_2

    .line 1733152
    :cond_2
    iget-object v3, v0, LX/Azu;->c:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Azv;

    .line 1733153
    invoke-virtual {v3, v5}, LX/Azv;->setSelected(Z)V

    goto :goto_4

    .line 1733154
    :cond_3
    iget-object v3, v0, LX/Azu;->b:Ljava/util/List;

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Azt;

    invoke-static {v0, v3}, LX/Azu;->a$redex0(LX/Azu;LX/Azt;)V

    .line 1733155
    iget-object v3, v0, LX/Azu;->c:Ljava/util/List;

    invoke-interface {v3, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Azv;

    invoke-virtual {v3, v6}, LX/Azv;->setSelected(Z)V

    .line 1733156
    return-void

    .line 1733157
    :cond_4
    if-eqz v3, :cond_5

    .line 1733158
    invoke-static {v3}, LX/0oJ;->a(Ljava/lang/String;)LX/0Px;

    move-result-object v1

    goto/16 :goto_0

    .line 1733159
    :cond_5
    const-string v1, "camera,gallery,composer"

    invoke-static {v1}, LX/0oJ;->a(Ljava/lang/String;)LX/0Px;

    move-result-object v1

    goto/16 :goto_0

    .line 1733160
    :cond_6
    if-gt p1, p0, :cond_1

    .line 1733161
    int-to-float p0, p1

    invoke-virtual {v3}, LX/Azt;->a()F

    move-result p3

    div-float/2addr p0, p3

    float-to-int p0, p0

    goto/16 :goto_3
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    move-object v1, p1

    check-cast v1, LX/Azy;

    new-instance v4, LX/Azu;

    invoke-static {p0}, LX/9J2;->a(LX/0QB;)LX/9J2;

    move-result-object v2

    check-cast v2, LX/9J2;

    invoke-static {p0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v3

    check-cast v3, LX/0hB;

    invoke-direct {v4, v2, v3}, LX/Azu;-><init>(LX/9J2;LX/0hB;)V

    move-object v2, v4

    check-cast v2, LX/Azu;

    invoke-static {p0}, LX/0oJ;->b(LX/0QB;)LX/0oJ;

    move-result-object v3

    check-cast v3, LX/0oJ;

    const/16 v4, 0x2a6d

    invoke-static {p0, v4}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    new-instance p1, LX/87h;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v5

    check-cast v5, LX/0tX;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/Executor;

    invoke-direct {p1, v5, v6}, LX/87h;-><init>(LX/0tX;Ljava/util/concurrent/Executor;)V

    move-object v5, p1

    check-cast v5, LX/87h;

    invoke-static {p0}, LX/23P;->b(LX/0QB;)LX/23P;

    move-result-object v6

    check-cast v6, LX/23P;

    invoke-static {p0}, LX/0W1;->a(LX/0QB;)LX/0Tf;

    move-result-object p0

    check-cast p0, Ljava/util/concurrent/Executor;

    iput-object v2, v1, LX/Azy;->b:LX/Azu;

    iput-object v3, v1, LX/Azy;->c:LX/0oJ;

    iput-object v4, v1, LX/Azy;->d:LX/0Or;

    iput-object v5, v1, LX/Azy;->e:LX/87h;

    iput-object v6, v1, LX/Azy;->f:LX/23P;

    iput-object p0, v1, LX/Azy;->g:Ljava/util/concurrent/Executor;

    return-void
.end method
