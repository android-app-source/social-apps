.class public LX/CAP;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/CAP;


# instance fields
.field public a:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1855387
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1855388
    iput-object p1, p0, LX/CAP;->a:LX/0Zb;

    .line 1855389
    return-void
.end method

.method public static a(LX/0QB;)LX/CAP;
    .locals 4

    .prologue
    .line 1855374
    sget-object v0, LX/CAP;->b:LX/CAP;

    if-nez v0, :cond_1

    .line 1855375
    const-class v1, LX/CAP;

    monitor-enter v1

    .line 1855376
    :try_start_0
    sget-object v0, LX/CAP;->b:LX/CAP;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1855377
    if-eqz v2, :cond_0

    .line 1855378
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1855379
    new-instance p0, LX/CAP;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-direct {p0, v3}, LX/CAP;-><init>(LX/0Zb;)V

    .line 1855380
    move-object v0, p0

    .line 1855381
    sput-object v0, LX/CAP;->b:LX/CAP;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1855382
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1855383
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1855384
    :cond_1
    sget-object v0, LX/CAP;->b:LX/CAP;

    return-object v0

    .line 1855385
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1855386
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static c(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 1

    .prologue
    .line 1855373
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v0, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    return-object v0
.end method
