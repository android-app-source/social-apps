.class public final enum LX/BI3;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/BI3;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/BI3;

.field public static final enum HD_UPLOAD_NUX:LX/BI3;

.field public static final enum HIGHLIGHTS_NUX:LX/BI3;

.field public static final enum MULTIMEDIA_NUX:LX/BI3;

.field public static final enum SLIDESHOW_NUX:LX/BI3;


# instance fields
.field public final controllerClass:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "LX/3ko;",
            ">;"
        }
    .end annotation
.end field

.field public final description:Ljava/lang/String;

.field public final interstitialId:Ljava/lang/String;

.field public final prefKey:LX/0Tn;


# direct methods
.method public static constructor <clinit>()V
    .locals 13

    .prologue
    const/4 v12, 0x3

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v2, 0x0

    .line 1769931
    new-instance v0, LX/BI3;

    const-string v1, "MULTIMEDIA_NUX"

    const-class v3, LX/3kn;

    const-string v4, "3883"

    sget-object v5, LX/1Ip;->e:LX/0Tn;

    const-string v6, "Multi media post NUX"

    invoke-direct/range {v0 .. v6}, LX/BI3;-><init>(Ljava/lang/String;ILjava/lang/Class;Ljava/lang/String;LX/0Tn;Ljava/lang/String;)V

    sput-object v0, LX/BI3;->MULTIMEDIA_NUX:LX/BI3;

    .line 1769932
    new-instance v3, LX/BI3;

    const-string v4, "SLIDESHOW_NUX"

    const-class v6, LX/3l0;

    const-string v7, "4194"

    sget-object v8, LX/1Ip;->f:LX/0Tn;

    const-string v9, "Slidshow post NUX"

    move v5, v10

    invoke-direct/range {v3 .. v9}, LX/BI3;-><init>(Ljava/lang/String;ILjava/lang/Class;Ljava/lang/String;LX/0Tn;Ljava/lang/String;)V

    sput-object v3, LX/BI3;->SLIDESHOW_NUX:LX/BI3;

    .line 1769933
    new-instance v3, LX/BI3;

    const-string v4, "HD_UPLOAD_NUX"

    const-class v6, LX/3kr;

    const-string v7, "4169"

    sget-object v8, LX/1Ip;->g:LX/0Tn;

    const-string v9, "HD photo upload NUX"

    move v5, v11

    invoke-direct/range {v3 .. v9}, LX/BI3;-><init>(Ljava/lang/String;ILjava/lang/Class;Ljava/lang/String;LX/0Tn;Ljava/lang/String;)V

    sput-object v3, LX/BI3;->HD_UPLOAD_NUX:LX/BI3;

    .line 1769934
    new-instance v3, LX/BI3;

    const-string v4, "HIGHLIGHTS_NUX"

    const-class v6, LX/3lG;

    const-string v7, "4369"

    sget-object v8, LX/1Ip;->h:LX/0Tn;

    const-string v9, "Picker highlights NUX"

    move v5, v12

    invoke-direct/range {v3 .. v9}, LX/BI3;-><init>(Ljava/lang/String;ILjava/lang/Class;Ljava/lang/String;LX/0Tn;Ljava/lang/String;)V

    sput-object v3, LX/BI3;->HIGHLIGHTS_NUX:LX/BI3;

    .line 1769935
    const/4 v0, 0x4

    new-array v0, v0, [LX/BI3;

    sget-object v1, LX/BI3;->MULTIMEDIA_NUX:LX/BI3;

    aput-object v1, v0, v2

    sget-object v1, LX/BI3;->SLIDESHOW_NUX:LX/BI3;

    aput-object v1, v0, v10

    sget-object v1, LX/BI3;->HD_UPLOAD_NUX:LX/BI3;

    aput-object v1, v0, v11

    sget-object v1, LX/BI3;->HIGHLIGHTS_NUX:LX/BI3;

    aput-object v1, v0, v12

    sput-object v0, LX/BI3;->$VALUES:[LX/BI3;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/Class;Ljava/lang/String;LX/0Tn;Ljava/lang/String;)V
    .locals 0
    .param p5    # LX/0Tn;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "LX/3ko;",
            ">;",
            "Ljava/lang/String;",
            "LX/0Tn;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1769938
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1769939
    iput-object p3, p0, LX/BI3;->controllerClass:Ljava/lang/Class;

    .line 1769940
    iput-object p4, p0, LX/BI3;->interstitialId:Ljava/lang/String;

    .line 1769941
    iput-object p5, p0, LX/BI3;->prefKey:LX/0Tn;

    .line 1769942
    iput-object p6, p0, LX/BI3;->description:Ljava/lang/String;

    .line 1769943
    return-void
.end method

.method public static forControllerClass(Ljava/lang/Class;)LX/BI3;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "LX/3ko;",
            ">;)",
            "LX/BI3;"
        }
    .end annotation

    .prologue
    .line 1769944
    invoke-static {}, LX/BI3;->values()[LX/BI3;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 1769945
    iget-object v4, v3, LX/BI3;->controllerClass:Ljava/lang/Class;

    if-ne v4, p0, :cond_0

    .line 1769946
    return-object v3

    .line 1769947
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1769948
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown controller class: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/BI3;
    .locals 1

    .prologue
    .line 1769937
    const-class v0, LX/BI3;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/BI3;

    return-object v0
.end method

.method public static values()[LX/BI3;
    .locals 1

    .prologue
    .line 1769936
    sget-object v0, LX/BI3;->$VALUES:[LX/BI3;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/BI3;

    return-object v0
.end method
