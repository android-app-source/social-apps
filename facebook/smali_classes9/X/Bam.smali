.class public final enum LX/Bam;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Bam;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Bam;

.field public static final enum CUSTOM:LX/Bam;

.field public static final enum NARROW:LX/Bam;

.field public static final enum STANDARD:LX/Bam;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1799557
    new-instance v0, LX/Bam;

    const-string v1, "STANDARD"

    invoke-direct {v0, v1, v2}, LX/Bam;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Bam;->STANDARD:LX/Bam;

    .line 1799558
    new-instance v0, LX/Bam;

    const-string v1, "NARROW"

    invoke-direct {v0, v1, v3}, LX/Bam;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Bam;->NARROW:LX/Bam;

    .line 1799559
    new-instance v0, LX/Bam;

    const-string v1, "CUSTOM"

    invoke-direct {v0, v1, v4}, LX/Bam;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Bam;->CUSTOM:LX/Bam;

    .line 1799560
    const/4 v0, 0x3

    new-array v0, v0, [LX/Bam;

    sget-object v1, LX/Bam;->STANDARD:LX/Bam;

    aput-object v1, v0, v2

    sget-object v1, LX/Bam;->NARROW:LX/Bam;

    aput-object v1, v0, v3

    sget-object v1, LX/Bam;->CUSTOM:LX/Bam;

    aput-object v1, v0, v4

    sput-object v0, LX/Bam;->$VALUES:[LX/Bam;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1799561
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Bam;
    .locals 1

    .prologue
    .line 1799562
    const-class v0, LX/Bam;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Bam;

    return-object v0
.end method

.method public static values()[LX/Bam;
    .locals 1

    .prologue
    .line 1799563
    sget-object v0, LX/Bam;->$VALUES:[LX/Bam;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Bam;

    return-object v0
.end method
