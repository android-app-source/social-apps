.class public final LX/C4D;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/C4D;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/C4B;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/C4E;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1846992
    const/4 v0, 0x0

    sput-object v0, LX/C4D;->a:LX/C4D;

    .line 1846993
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/C4D;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1846994
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1846995
    new-instance v0, LX/C4E;

    invoke-direct {v0}, LX/C4E;-><init>()V

    iput-object v0, p0, LX/C4D;->c:LX/C4E;

    .line 1846996
    return-void
.end method

.method public static c(LX/1De;)LX/C4B;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1846997
    new-instance v1, LX/C4C;

    invoke-direct {v1}, LX/C4C;-><init>()V

    .line 1846998
    sget-object v2, LX/C4D;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/C4B;

    .line 1846999
    if-nez v2, :cond_0

    .line 1847000
    new-instance v2, LX/C4B;

    invoke-direct {v2}, LX/C4B;-><init>()V

    .line 1847001
    :cond_0
    invoke-static {v2, p0, v0, v0, v1}, LX/C4B;->a$redex0(LX/C4B;LX/1De;IILX/C4C;)V

    .line 1847002
    move-object v1, v2

    .line 1847003
    move-object v0, v1

    .line 1847004
    return-object v0
.end method

.method public static declared-synchronized q()LX/C4D;
    .locals 2

    .prologue
    .line 1847005
    const-class v1, LX/C4D;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/C4D;->a:LX/C4D;

    if-nez v0, :cond_0

    .line 1847006
    new-instance v0, LX/C4D;

    invoke-direct {v0}, LX/C4D;-><init>()V

    sput-object v0, LX/C4D;->a:LX/C4D;

    .line 1847007
    :cond_0
    sget-object v0, LX/C4D;->a:LX/C4D;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 1847008
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 1

    .prologue
    .line 1847009
    const/4 p2, 0x1

    .line 1847010
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    const/4 p0, 0x2

    invoke-interface {v0, p0}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, p2}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, p2}, LX/1Dh;->C(I)LX/1Dh;

    move-result-object v0

    const/16 p0, 0x8

    const p2, 0x7f0b0115

    invoke-interface {v0, p0, p2}, LX/1Dh;->x(II)LX/1Dh;

    move-result-object v0

    const/4 p0, -0x1

    invoke-interface {v0, p0}, LX/1Dh;->W(I)LX/1Dh;

    move-result-object v0

    const/4 p0, 0x0

    const p2, 0x7f0e071f

    invoke-static {p1, p0, p2}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object p0

    const p2, 0x7f08198c

    invoke-virtual {p0, p2}, LX/1ne;->h(I)LX/1ne;

    move-result-object p0

    invoke-interface {v0, p0}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    move-object v0, v0

    .line 1847011
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1847012
    invoke-static {}, LX/1dS;->b()V

    .line 1847013
    const/4 v0, 0x0

    return-object v0
.end method
