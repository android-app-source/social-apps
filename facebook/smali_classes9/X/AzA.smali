.class public LX/AzA;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1732223
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel;)Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel;
    .locals 4
    .param p0    # Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 1732213
    if-nez p0, :cond_0

    move-object v0, v2

    .line 1732214
    :goto_0
    return-object v0

    .line 1732215
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel;->d()Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;

    move-result-object v0

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;->PHOTO:Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;

    if-eq v0, v3, :cond_1

    invoke-virtual {p0}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel;->d()Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;

    move-result-object v0

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;->VIDEO:Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;

    if-ne v0, v3, :cond_3

    .line 1732216
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel;->c()Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel$SouvenirMediaModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel$SouvenirMediaModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel$SouvenirMediaModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel$SouvenirMediaModel$EdgesModel;->a()Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel;

    move-result-object v0

    .line 1732217
    if-nez v0, :cond_3

    move-object v0, v2

    .line 1732218
    goto :goto_0

    .line 1732219
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    :cond_3
    invoke-virtual {p0}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel;->c()Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel$SouvenirMediaModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel$SouvenirMediaModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 1732220
    invoke-virtual {p0}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel;->c()Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel$SouvenirMediaModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel$SouvenirMediaModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel$SouvenirMediaModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel$SouvenirMediaModel$EdgesModel;->a()Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel;

    move-result-object v0

    .line 1732221
    if-eqz v0, :cond_2

    goto :goto_0

    :cond_4
    move-object v0, v2

    .line 1732222
    goto :goto_0
.end method

.method public static b(Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel;Z)I
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 1732198
    invoke-virtual {p0}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel;->b()Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$MediaElementsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$MediaElementsModel;->a()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v4, v2

    move v1, v2

    :goto_0
    if-ge v4, v6, :cond_4

    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$MediaElementsModel$EdgesModel;

    .line 1732199
    invoke-virtual {v0}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$MediaElementsModel$EdgesModel;->a()Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel;

    move-result-object v7

    .line 1732200
    invoke-virtual {v7}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel;->c()Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel$SouvenirMediaModel;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v7}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel;->c()Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel$SouvenirMediaModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel$SouvenirMediaModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v7}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel;->c()Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel$SouvenirMediaModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel$SouvenirMediaModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1732201
    invoke-virtual {v7}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel;->d()Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;

    move-result-object v0

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;->BURST:Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;

    if-ne v0, v3, :cond_3

    move v3, v1

    move v1, v2

    .line 1732202
    :goto_1
    invoke-virtual {v7}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel;->c()Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel$SouvenirMediaModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel$SouvenirMediaModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1732203
    invoke-virtual {v7}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel;->c()Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel$SouvenirMediaModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel$SouvenirMediaModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel$SouvenirMediaModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel$SouvenirMediaModel$EdgesModel;->a()Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel;

    move-result-object v0

    .line 1732204
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v8

    if-eqz v8, :cond_0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v8, 0x4984e12

    if-ne v0, v8, :cond_0

    .line 1732205
    add-int/lit8 v3, v3, 0x1

    .line 1732206
    if-eqz p1, :cond_1

    .line 1732207
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    move v1, v3

    .line 1732208
    :cond_2
    :goto_2
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    .line 1732209
    :cond_3
    invoke-virtual {v7}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel;->c()Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel$SouvenirMediaModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel$SouvenirMediaModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel$SouvenirMediaModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaElementFieldsModel$SouvenirMediaModel$EdgesModel;->a()Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel;

    move-result-object v0

    .line 1732210
    if-eqz v0, :cond_2

    .line 1732211
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1732212
    :cond_4
    return v1
.end method
