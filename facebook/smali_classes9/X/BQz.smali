.class public LX/BQz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5SF;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/BQz;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1783091
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1783092
    return-void
.end method

.method public static a(LX/0QB;)LX/BQz;
    .locals 3

    .prologue
    .line 1783101
    sget-object v0, LX/BQz;->a:LX/BQz;

    if-nez v0, :cond_1

    .line 1783102
    const-class v1, LX/BQz;

    monitor-enter v1

    .line 1783103
    :try_start_0
    sget-object v0, LX/BQz;->a:LX/BQz;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1783104
    if-eqz v2, :cond_0

    .line 1783105
    :try_start_1
    new-instance v0, LX/BQz;

    invoke-direct {v0}, LX/BQz;-><init>()V

    .line 1783106
    move-object v0, v0

    .line 1783107
    sput-object v0, LX/BQz;->a:LX/BQz;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1783108
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1783109
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1783110
    :cond_1
    sget-object v0, LX/BQz;->a:LX/BQz;

    return-object v0

    .line 1783111
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1783112
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;)Landroid/content/Intent;
    .locals 3
    .param p3    # Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1783097
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/timeline/stagingground/StagingGroundActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1783098
    const-string v1, "key_staging_ground_launch_config"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1783099
    const-string v1, "extra_edit_gallery_launch_settings"

    if-nez p3, :cond_0

    new-instance v2, LX/5Rw;

    invoke-direct {v2}, LX/5Rw;-><init>()V

    invoke-virtual {v2}, LX/5Rw;->a()Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;

    move-result-object p3

    :cond_0
    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1783100
    return-object v0
.end method

.method public final a(Landroid/content/Context;Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 1783093
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/timeline/stagingground/StagingGroundActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1783094
    const-string v1, "key_staging_ground_launch_config"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1783095
    const-string v1, "extra_video_edit_gallery_launch_settings"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1783096
    return-object v0
.end method
