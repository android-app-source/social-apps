.class public final LX/AV4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/facecast/protocol/FetchBroadcastCopyrightsStateModels$FetchBroadcastCopyrightsStateQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/AV5;


# direct methods
.method public constructor <init>(LX/AV5;)V
    .locals 0

    .prologue
    .line 1678803
    iput-object p1, p0, LX/AV4;->a:LX/AV5;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 1678804
    iget-object v0, p0, LX/AV4;->a:LX/AV5;

    iget-object v0, v0, LX/AV5;->b:LX/03V;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, LX/AV5;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_graphFailure"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to get copyright state for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/AV4;->a:LX/AV5;

    iget-object v3, v3, LX/AV5;->i:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1678805
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1678806
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1678807
    if-eqz p1, :cond_0

    .line 1678808
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1678809
    if-nez v0, :cond_1

    .line 1678810
    :cond_0
    :goto_0
    return-void

    .line 1678811
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1678812
    check-cast v0, Lcom/facebook/facecast/protocol/FetchBroadcastCopyrightsStateModels$FetchBroadcastCopyrightsStateQueryModel;

    .line 1678813
    invoke-virtual {v0}, Lcom/facebook/facecast/protocol/FetchBroadcastCopyrightsStateModels$FetchBroadcastCopyrightsStateQueryModel;->a()Ljava/lang/String;

    move-result-object v1

    .line 1678814
    const-string v2, "private_broadcast_msg_sent"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "public_broadcast_msg_sent"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1678815
    :cond_2
    iget-object v1, p0, LX/AV4;->a:LX/AV5;

    const-string v2, "copyright_monitor_violated"

    invoke-static {v1, v2}, LX/AV5;->a$redex0(LX/AV5;Ljava/lang/String;)V

    .line 1678816
    invoke-virtual {v0}, Lcom/facebook/facecast/protocol/FetchBroadcastCopyrightsStateModels$FetchBroadcastCopyrightsStateQueryModel;->j()LX/0Px;

    move-result-object v0

    .line 1678817
    if-eqz v0, :cond_3

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x4

    if-ge v1, v2, :cond_4

    .line 1678818
    :cond_3
    iget-object v1, p0, LX/AV4;->a:LX/AV5;

    iget-object v1, v1, LX/AV5;->b:LX/03V;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, LX/AV5;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_graphCallback"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Copyright violation texts is not sufficient: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1678819
    :cond_4
    iget-object v1, p0, LX/AV4;->a:LX/AV5;

    .line 1678820
    iget-object v2, v1, LX/AV5;->g:Landroid/os/Handler;

    new-instance v3, Lcom/facebook/facecast/FacecastCopyrightMonitor$2;

    invoke-direct {v3, v1, v0}, Lcom/facebook/facecast/FacecastCopyrightMonitor$2;-><init>(LX/AV5;Ljava/util/List;)V

    const v4, -0x2f04d5b6

    invoke-static {v2, v3, v4}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1678821
    goto :goto_0
.end method
