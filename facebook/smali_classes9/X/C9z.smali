.class public LX/C9z;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/C9x;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CA0;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1854785
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/C9z;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/CA0;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1854786
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1854787
    iput-object p1, p0, LX/C9z;->b:LX/0Ot;

    .line 1854788
    return-void
.end method

.method public static a(LX/0QB;)LX/C9z;
    .locals 4

    .prologue
    .line 1854789
    const-class v1, LX/C9z;

    monitor-enter v1

    .line 1854790
    :try_start_0
    sget-object v0, LX/C9z;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1854791
    sput-object v2, LX/C9z;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1854792
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1854793
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1854794
    new-instance v3, LX/C9z;

    const/16 p0, 0x2074

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/C9z;-><init>(LX/0Ot;)V

    .line 1854795
    move-object v0, v3

    .line 1854796
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1854797
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C9z;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1854798
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1854799
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 3

    .prologue
    .line 1854800
    iget-object v0, p0, LX/C9z;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CA0;

    const/4 p0, 0x2

    .line 1854801
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1, p0}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v1

    const v2, 0x7f0a07c8

    invoke-interface {v1, v2}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v1

    invoke-interface {v1, p0}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v1

    .line 1854802
    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v2

    const p0, 0x7f0219ed

    invoke-virtual {v2, p0}, LX/1o5;->h(I)LX/1o5;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    const/4 p0, 0x7

    const p2, 0x7f0b1cc3

    invoke-interface {v2, p0, p2}, LX/1Di;->c(II)LX/1Di;

    move-result-object v2

    const/4 p0, 0x4

    const p2, 0x7f0b1cc5

    invoke-interface {v2, p0, p2}, LX/1Di;->c(II)LX/1Di;

    move-result-object v2

    const/4 p0, 0x5

    const p2, 0x7f0b1cc6

    invoke-interface {v2, p0, p2}, LX/1Di;->c(II)LX/1Di;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v2, v2

    .line 1854803
    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v1

    .line 1854804
    const/4 v2, 0x0

    const p0, 0x7f0e0a31

    invoke-static {p1, v2, p0}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v2

    const p0, 0x7f082985

    invoke-virtual {v2, p0}, LX/1ne;->h(I)LX/1ne;

    move-result-object v2

    const p0, 0x7f0a07c9

    invoke-virtual {v2, p0}, LX/1ne;->n(I)LX/1ne;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v2, v2

    .line 1854805
    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v1

    .line 1854806
    iget-object v2, v0, LX/CA0;->a:LX/CA2;

    invoke-virtual {v2}, LX/CA2;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1854807
    const v2, 0x14a23990

    const/4 p0, 0x0

    invoke-static {p1, v2, p0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v2

    move-object v2, v2

    .line 1854808
    invoke-interface {v1, v2}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    .line 1854809
    :cond_0
    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    move-object v0, v1

    .line 1854810
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1854811
    invoke-static {}, LX/1dS;->b()V

    .line 1854812
    iget v0, p1, LX/1dQ;->b:I

    .line 1854813
    packed-switch v0, :pswitch_data_0

    .line 1854814
    :goto_0
    return-object v2

    .line 1854815
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 1854816
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    .line 1854817
    iget-object p1, p0, LX/C9z;->b:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/CA0;

    .line 1854818
    iget-object p0, p1, LX/CA0;->a:LX/CA2;

    invoke-virtual {p0, v0}, LX/CA2;->a(Landroid/view/View;)V

    .line 1854819
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x14a23990
        :pswitch_0
    .end packed-switch
.end method
