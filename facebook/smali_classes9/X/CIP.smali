.class public final LX/CIP;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 50

    .prologue
    .line 1873796
    const/16 v46, 0x0

    .line 1873797
    const/16 v45, 0x0

    .line 1873798
    const/16 v44, 0x0

    .line 1873799
    const/16 v43, 0x0

    .line 1873800
    const/16 v42, 0x0

    .line 1873801
    const/16 v41, 0x0

    .line 1873802
    const/16 v40, 0x0

    .line 1873803
    const/16 v39, 0x0

    .line 1873804
    const/16 v38, 0x0

    .line 1873805
    const/16 v37, 0x0

    .line 1873806
    const/16 v36, 0x0

    .line 1873807
    const/16 v35, 0x0

    .line 1873808
    const/16 v34, 0x0

    .line 1873809
    const/16 v33, 0x0

    .line 1873810
    const/16 v32, 0x0

    .line 1873811
    const/16 v31, 0x0

    .line 1873812
    const/16 v30, 0x0

    .line 1873813
    const/16 v29, 0x0

    .line 1873814
    const/16 v28, 0x0

    .line 1873815
    const/16 v27, 0x0

    .line 1873816
    const/16 v26, 0x0

    .line 1873817
    const/16 v25, 0x0

    .line 1873818
    const/16 v24, 0x0

    .line 1873819
    const/16 v23, 0x0

    .line 1873820
    const/16 v22, 0x0

    .line 1873821
    const/16 v21, 0x0

    .line 1873822
    const/16 v20, 0x0

    .line 1873823
    const/16 v19, 0x0

    .line 1873824
    const/16 v18, 0x0

    .line 1873825
    const/16 v17, 0x0

    .line 1873826
    const/16 v16, 0x0

    .line 1873827
    const/4 v15, 0x0

    .line 1873828
    const/4 v14, 0x0

    .line 1873829
    const/4 v13, 0x0

    .line 1873830
    const/4 v12, 0x0

    .line 1873831
    const/4 v11, 0x0

    .line 1873832
    const/4 v10, 0x0

    .line 1873833
    const/4 v9, 0x0

    .line 1873834
    const/4 v8, 0x0

    .line 1873835
    const/4 v7, 0x0

    .line 1873836
    const/4 v6, 0x0

    .line 1873837
    const/4 v5, 0x0

    .line 1873838
    const/4 v4, 0x0

    .line 1873839
    const/4 v3, 0x0

    .line 1873840
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v47

    sget-object v48, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v47

    move-object/from16 v1, v48

    if-eq v0, v1, :cond_1

    .line 1873841
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1873842
    const/4 v3, 0x0

    .line 1873843
    :goto_0
    return v3

    .line 1873844
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1873845
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v47

    sget-object v48, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v47

    move-object/from16 v1, v48

    if-eq v0, v1, :cond_2b

    .line 1873846
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v47

    .line 1873847
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1873848
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v48

    sget-object v49, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v48

    move-object/from16 v1, v49

    if-eq v0, v1, :cond_1

    if-eqz v47, :cond_1

    .line 1873849
    const-string v48, "__type__"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-nez v48, :cond_2

    const-string v48, "__typename"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_3

    .line 1873850
    :cond_2
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v46

    move-object/from16 v0, p1

    move-object/from16 v1, v46

    invoke-virtual {v0, v1}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v46

    goto :goto_1

    .line 1873851
    :cond_3
    const-string v48, "action"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_4

    .line 1873852
    invoke-static/range {p0 .. p1}, LX/CIN;->a(LX/15w;LX/186;)I

    move-result v45

    goto :goto_1

    .line 1873853
    :cond_4
    const-string v48, "action_title"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_5

    .line 1873854
    invoke-static/range {p0 .. p1}, LX/8al;->a(LX/15w;LX/186;)I

    move-result v44

    goto :goto_1

    .line 1873855
    :cond_5
    const-string v48, "annotations"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_6

    .line 1873856
    invoke-static/range {p0 .. p1}, LX/CI0;->a(LX/15w;LX/186;)I

    move-result v43

    goto :goto_1

    .line 1873857
    :cond_6
    const-string v48, "autoplay_style"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_7

    .line 1873858
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v42

    invoke-static/range {v42 .. v42}, Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;

    move-result-object v42

    move-object/from16 v0, p1

    move-object/from16 v1, v42

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v42

    goto :goto_1

    .line 1873859
    :cond_7
    const-string v48, "block_elements"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_8

    .line 1873860
    invoke-static/range {p0 .. p1}, LX/CI2;->a(LX/15w;LX/186;)I

    move-result v41

    goto/16 :goto_1

    .line 1873861
    :cond_8
    const-string v48, "bounding_box"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_9

    .line 1873862
    invoke-static/range {p0 .. p1}, LX/CII;->a(LX/15w;LX/186;)I

    move-result v40

    goto/16 :goto_1

    .line 1873863
    :cond_9
    const-string v48, "child_elements"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_a

    .line 1873864
    invoke-static/range {p0 .. p1}, LX/CIG;->a(LX/15w;LX/186;)I

    move-result v39

    goto/16 :goto_1

    .line 1873865
    :cond_a
    const-string v48, "color"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_b

    .line 1873866
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v38

    move-object/from16 v0, p1

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v38

    goto/16 :goto_1

    .line 1873867
    :cond_b
    const-string v48, "color_types"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_c

    .line 1873868
    invoke-static/range {p0 .. p1}, LX/CI1;->a(LX/15w;LX/186;)I

    move-result v37

    goto/16 :goto_1

    .line 1873869
    :cond_c
    const-string v48, "content_element"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_d

    .line 1873870
    invoke-static/range {p0 .. p1}, LX/CIK;->a(LX/15w;LX/186;)I

    move-result v36

    goto/16 :goto_1

    .line 1873871
    :cond_d
    const-string v48, "control_style"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_e

    .line 1873872
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v35

    invoke-static/range {v35 .. v35}, Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;

    move-result-object v35

    move-object/from16 v0, p1

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v35

    goto/16 :goto_1

    .line 1873873
    :cond_e
    const-string v48, "do_action"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_f

    .line 1873874
    invoke-static/range {p0 .. p1}, LX/CHy;->a(LX/15w;LX/186;)I

    move-result v34

    goto/16 :goto_1

    .line 1873875
    :cond_f
    const-string v48, "document_element_type"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_10

    .line 1873876
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v33

    invoke-static/range {v33 .. v33}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    move-result-object v33

    move-object/from16 v0, p1

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v33

    goto/16 :goto_1

    .line 1873877
    :cond_10
    const-string v48, "element_descriptor"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_11

    .line 1873878
    invoke-static/range {p0 .. p1}, LX/CIO;->a(LX/15w;LX/186;)I

    move-result v32

    goto/16 :goto_1

    .line 1873879
    :cond_11
    const-string v48, "element_photo"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_12

    .line 1873880
    invoke-static/range {p0 .. p1}, LX/CIF;->a(LX/15w;LX/186;)I

    move-result v31

    goto/16 :goto_1

    .line 1873881
    :cond_12
    const-string v48, "element_text"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_13

    .line 1873882
    invoke-static/range {p0 .. p1}, LX/8al;->a(LX/15w;LX/186;)I

    move-result v30

    goto/16 :goto_1

    .line 1873883
    :cond_13
    const-string v48, "element_video"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_14

    .line 1873884
    invoke-static/range {p0 .. p1}, LX/8ZV;->a(LX/15w;LX/186;)I

    move-result v29

    goto/16 :goto_1

    .line 1873885
    :cond_14
    const-string v48, "end_date"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_15

    .line 1873886
    invoke-static/range {p0 .. p1}, LX/CI3;->a(LX/15w;LX/186;)I

    move-result v28

    goto/16 :goto_1

    .line 1873887
    :cond_15
    const-string v48, "footer_elements"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_16

    .line 1873888
    invoke-static/range {p0 .. p1}, LX/CIC;->a(LX/15w;LX/186;)I

    move-result v27

    goto/16 :goto_1

    .line 1873889
    :cond_16
    const-string v48, "grid_width_percent"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_17

    .line 1873890
    const/4 v5, 0x1

    .line 1873891
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v26

    goto/16 :goto_1

    .line 1873892
    :cond_17
    const-string v48, "header"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_18

    .line 1873893
    invoke-static/range {p0 .. p1}, LX/CIT;->a(LX/15w;LX/186;)I

    move-result v25

    goto/16 :goto_1

    .line 1873894
    :cond_18
    const-string v48, "header_elements"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_19

    .line 1873895
    invoke-static/range {p0 .. p1}, LX/CID;->a(LX/15w;LX/186;)I

    move-result v24

    goto/16 :goto_1

    .line 1873896
    :cond_19
    const-string v48, "image"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_1a

    .line 1873897
    invoke-static/range {p0 .. p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v23

    goto/16 :goto_1

    .line 1873898
    :cond_1a
    const-string v48, "is_on"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_1b

    .line 1873899
    const/4 v4, 0x1

    .line 1873900
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v22

    goto/16 :goto_1

    .line 1873901
    :cond_1b
    const-string v48, "locations"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_1c

    .line 1873902
    invoke-static/range {p0 .. p1}, LX/D1c;->a(LX/15w;LX/186;)I

    move-result v21

    goto/16 :goto_1

    .line 1873903
    :cond_1c
    const-string v48, "logging_token"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_1d

    .line 1873904
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v20

    goto/16 :goto_1

    .line 1873905
    :cond_1d
    const-string v48, "looping_style"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_1e

    .line 1873906
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v19

    goto/16 :goto_1

    .line 1873907
    :cond_1e
    const-string v48, "media_elements"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_1f

    .line 1873908
    invoke-static/range {p0 .. p1}, LX/CIG;->a(LX/15w;LX/186;)I

    move-result v18

    goto/16 :goto_1

    .line 1873909
    :cond_1f
    const-string v48, "off_text"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_20

    .line 1873910
    invoke-static/range {p0 .. p1}, LX/8al;->a(LX/15w;LX/186;)I

    move-result v17

    goto/16 :goto_1

    .line 1873911
    :cond_20
    const-string v48, "on_text"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_21

    .line 1873912
    invoke-static/range {p0 .. p1}, LX/8al;->a(LX/15w;LX/186;)I

    move-result v16

    goto/16 :goto_1

    .line 1873913
    :cond_21
    const-string v48, "page_set"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_22

    .line 1873914
    invoke-static/range {p0 .. p1}, LX/CIJ;->a(LX/15w;LX/186;)I

    move-result v15

    goto/16 :goto_1

    .line 1873915
    :cond_22
    const-string v48, "section_header"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_23

    .line 1873916
    invoke-static/range {p0 .. p1}, LX/CIK;->a(LX/15w;LX/186;)I

    move-result v14

    goto/16 :goto_1

    .line 1873917
    :cond_23
    const-string v48, "selected_index"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_24

    .line 1873918
    const/4 v3, 0x1

    .line 1873919
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v13

    goto/16 :goto_1

    .line 1873920
    :cond_24
    const-string v48, "start_date"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_25

    .line 1873921
    invoke-static/range {p0 .. p1}, LX/CI3;->a(LX/15w;LX/186;)I

    move-result v12

    goto/16 :goto_1

    .line 1873922
    :cond_25
    const-string v48, "style"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_26

    .line 1873923
    invoke-static/range {p0 .. p1}, LX/CIU;->a(LX/15w;LX/186;)I

    move-result v11

    goto/16 :goto_1

    .line 1873924
    :cond_26
    const-string v48, "style_list"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_27

    .line 1873925
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v10

    goto/16 :goto_1

    .line 1873926
    :cond_27
    const-string v48, "target_uri"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_28

    .line 1873927
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto/16 :goto_1

    .line 1873928
    :cond_28
    const-string v48, "title"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_29

    .line 1873929
    invoke-static/range {p0 .. p1}, LX/8al;->a(LX/15w;LX/186;)I

    move-result v8

    goto/16 :goto_1

    .line 1873930
    :cond_29
    const-string v48, "touch_targets"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v48

    if-eqz v48, :cond_2a

    .line 1873931
    invoke-static/range {p0 .. p1}, LX/CIM;->a(LX/15w;LX/186;)I

    move-result v7

    goto/16 :goto_1

    .line 1873932
    :cond_2a
    const-string v48, "undo_action"

    invoke-virtual/range {v47 .. v48}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v47

    if-eqz v47, :cond_0

    .line 1873933
    invoke-static/range {p0 .. p1}, LX/CHy;->a(LX/15w;LX/186;)I

    move-result v6

    goto/16 :goto_1

    .line 1873934
    :cond_2b
    const/16 v47, 0x29

    move-object/from16 v0, p1

    move/from16 v1, v47

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 1873935
    const/16 v47, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v47

    move/from16 v2, v46

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1873936
    const/16 v46, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v46

    move/from16 v2, v45

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1873937
    const/16 v45, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v45

    move/from16 v2, v44

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1873938
    const/16 v44, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v44

    move/from16 v2, v43

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1873939
    const/16 v43, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v43

    move/from16 v2, v42

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1873940
    const/16 v42, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v42

    move/from16 v2, v41

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1873941
    const/16 v41, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v41

    move/from16 v2, v40

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1873942
    const/16 v40, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v40

    move/from16 v2, v39

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1873943
    const/16 v39, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v39

    move/from16 v2, v38

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1873944
    const/16 v38, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v38

    move/from16 v2, v37

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1873945
    const/16 v37, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v37

    move/from16 v2, v36

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1873946
    const/16 v36, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v36

    move/from16 v2, v35

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1873947
    const/16 v35, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v35

    move/from16 v2, v34

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1873948
    const/16 v34, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v34

    move/from16 v2, v33

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1873949
    const/16 v33, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v33

    move/from16 v2, v32

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1873950
    const/16 v32, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v32

    move/from16 v2, v31

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1873951
    const/16 v31, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v31

    move/from16 v2, v30

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1873952
    const/16 v30, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v30

    move/from16 v2, v29

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1873953
    const/16 v29, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v29

    move/from16 v2, v28

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1873954
    const/16 v28, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v28

    move/from16 v2, v27

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1873955
    if-eqz v5, :cond_2c

    .line 1873956
    const/16 v5, 0x14

    const/16 v27, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v26

    move/from16 v2, v27

    invoke-virtual {v0, v5, v1, v2}, LX/186;->a(III)V

    .line 1873957
    :cond_2c
    const/16 v5, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 1873958
    const/16 v5, 0x16

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 1873959
    const/16 v5, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 1873960
    if-eqz v4, :cond_2d

    .line 1873961
    const/16 v4, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v4, v1}, LX/186;->a(IZ)V

    .line 1873962
    :cond_2d
    const/16 v4, 0x19

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1873963
    const/16 v4, 0x1a

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1873964
    const/16 v4, 0x1b

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1873965
    const/16 v4, 0x1c

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1873966
    const/16 v4, 0x1d

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1873967
    const/16 v4, 0x1e

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1873968
    const/16 v4, 0x1f

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v15}, LX/186;->b(II)V

    .line 1873969
    const/16 v4, 0x20

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v14}, LX/186;->b(II)V

    .line 1873970
    if-eqz v3, :cond_2e

    .line 1873971
    const/16 v3, 0x21

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v13, v4}, LX/186;->a(III)V

    .line 1873972
    :cond_2e
    const/16 v3, 0x22

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v12}, LX/186;->b(II)V

    .line 1873973
    const/16 v3, 0x23

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v11}, LX/186;->b(II)V

    .line 1873974
    const/16 v3, 0x24

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v10}, LX/186;->b(II)V

    .line 1873975
    const/16 v3, 0x25

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9}, LX/186;->b(II)V

    .line 1873976
    const/16 v3, 0x26

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v8}, LX/186;->b(II)V

    .line 1873977
    const/16 v3, 0x27

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 1873978
    const/16 v3, 0x28

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 1873979
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 7

    .prologue
    const/16 v6, 0x1b

    const/16 v5, 0xd

    const/16 v4, 0xb

    const/4 v3, 0x4

    const/4 v2, 0x0

    .line 1873980
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1873981
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1873982
    if-eqz v0, :cond_0

    .line 1873983
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1873984
    invoke-static {p0, p1, v2, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1873985
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1873986
    if-eqz v0, :cond_1

    .line 1873987
    const-string v1, "action"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1873988
    invoke-static {p0, v0, p2, p3}, LX/CIN;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1873989
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1873990
    if-eqz v0, :cond_2

    .line 1873991
    const-string v1, "action_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1873992
    invoke-static {p0, v0, p2, p3}, LX/8al;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1873993
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1873994
    if-eqz v0, :cond_3

    .line 1873995
    const-string v1, "annotations"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1873996
    invoke-static {p0, v0, p2, p3}, LX/CI0;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1873997
    :cond_3
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 1873998
    if-eqz v0, :cond_4

    .line 1873999
    const-string v0, "autoplay_style"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1874000
    invoke-virtual {p0, p1, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1874001
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1874002
    if-eqz v0, :cond_5

    .line 1874003
    const-string v1, "block_elements"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1874004
    invoke-static {p0, v0, p2, p3}, LX/CI2;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1874005
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1874006
    if-eqz v0, :cond_6

    .line 1874007
    const-string v1, "bounding_box"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1874008
    invoke-static {p0, v0, p2}, LX/CII;->a(LX/15i;ILX/0nX;)V

    .line 1874009
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1874010
    if-eqz v0, :cond_7

    .line 1874011
    const-string v1, "child_elements"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1874012
    invoke-static {p0, v0, p2, p3}, LX/CIG;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1874013
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1874014
    if-eqz v0, :cond_8

    .line 1874015
    const-string v1, "color"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1874016
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1874017
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1874018
    if-eqz v0, :cond_9

    .line 1874019
    const-string v1, "color_types"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1874020
    invoke-static {p0, v0, p2, p3}, LX/CI1;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1874021
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1874022
    if-eqz v0, :cond_a

    .line 1874023
    const-string v1, "content_element"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1874024
    invoke-static {p0, v0, p2, p3}, LX/CIK;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1874025
    :cond_a
    invoke-virtual {p0, p1, v4}, LX/15i;->g(II)I

    move-result v0

    .line 1874026
    if-eqz v0, :cond_b

    .line 1874027
    const-string v0, "control_style"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1874028
    invoke-virtual {p0, p1, v4}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1874029
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1874030
    if-eqz v0, :cond_c

    .line 1874031
    const-string v1, "do_action"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1874032
    invoke-static {p0, v0, p2, p3}, LX/CHy;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1874033
    :cond_c
    invoke-virtual {p0, p1, v5}, LX/15i;->g(II)I

    move-result v0

    .line 1874034
    if-eqz v0, :cond_d

    .line 1874035
    const-string v0, "document_element_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1874036
    invoke-virtual {p0, p1, v5}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1874037
    :cond_d
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1874038
    if-eqz v0, :cond_e

    .line 1874039
    const-string v1, "element_descriptor"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1874040
    invoke-static {p0, v0, p2, p3}, LX/CIO;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1874041
    :cond_e
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1874042
    if-eqz v0, :cond_f

    .line 1874043
    const-string v1, "element_photo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1874044
    invoke-static {p0, v0, p2}, LX/CIF;->a(LX/15i;ILX/0nX;)V

    .line 1874045
    :cond_f
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1874046
    if-eqz v0, :cond_10

    .line 1874047
    const-string v1, "element_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1874048
    invoke-static {p0, v0, p2, p3}, LX/8al;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1874049
    :cond_10
    const/16 v0, 0x11

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1874050
    if-eqz v0, :cond_11

    .line 1874051
    const-string v1, "element_video"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1874052
    invoke-static {p0, v0, p2, p3}, LX/8ZV;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1874053
    :cond_11
    const/16 v0, 0x12

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1874054
    if-eqz v0, :cond_12

    .line 1874055
    const-string v1, "end_date"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1874056
    invoke-static {p0, v0, p2}, LX/CI3;->a(LX/15i;ILX/0nX;)V

    .line 1874057
    :cond_12
    const/16 v0, 0x13

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1874058
    if-eqz v0, :cond_13

    .line 1874059
    const-string v1, "footer_elements"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1874060
    invoke-static {p0, v0, p2, p3}, LX/CIC;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1874061
    :cond_13
    const/16 v0, 0x14

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1874062
    if-eqz v0, :cond_14

    .line 1874063
    const-string v1, "grid_width_percent"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1874064
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1874065
    :cond_14
    const/16 v0, 0x15

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1874066
    if-eqz v0, :cond_15

    .line 1874067
    const-string v1, "header"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1874068
    invoke-static {p0, v0, p2, p3}, LX/CIT;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1874069
    :cond_15
    const/16 v0, 0x16

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1874070
    if-eqz v0, :cond_16

    .line 1874071
    const-string v1, "header_elements"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1874072
    invoke-static {p0, v0, p2, p3}, LX/CID;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1874073
    :cond_16
    const/16 v0, 0x17

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1874074
    if-eqz v0, :cond_17

    .line 1874075
    const-string v1, "image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1874076
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 1874077
    :cond_17
    const/16 v0, 0x18

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1874078
    if-eqz v0, :cond_18

    .line 1874079
    const-string v1, "is_on"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1874080
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1874081
    :cond_18
    const/16 v0, 0x19

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1874082
    if-eqz v0, :cond_19

    .line 1874083
    const-string v1, "locations"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1874084
    invoke-static {p0, v0, p2, p3}, LX/D1c;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1874085
    :cond_19
    const/16 v0, 0x1a

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1874086
    if-eqz v0, :cond_1a

    .line 1874087
    const-string v1, "logging_token"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1874088
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1874089
    :cond_1a
    invoke-virtual {p0, p1, v6}, LX/15i;->g(II)I

    move-result v0

    .line 1874090
    if-eqz v0, :cond_1b

    .line 1874091
    const-string v0, "looping_style"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1874092
    invoke-virtual {p0, p1, v6}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1874093
    :cond_1b
    const/16 v0, 0x1c

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1874094
    if-eqz v0, :cond_1c

    .line 1874095
    const-string v1, "media_elements"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1874096
    invoke-static {p0, v0, p2, p3}, LX/CIG;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1874097
    :cond_1c
    const/16 v0, 0x1d

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1874098
    if-eqz v0, :cond_1d

    .line 1874099
    const-string v1, "off_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1874100
    invoke-static {p0, v0, p2, p3}, LX/8al;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1874101
    :cond_1d
    const/16 v0, 0x1e

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1874102
    if-eqz v0, :cond_1e

    .line 1874103
    const-string v1, "on_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1874104
    invoke-static {p0, v0, p2, p3}, LX/8al;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1874105
    :cond_1e
    const/16 v0, 0x1f

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1874106
    if-eqz v0, :cond_1f

    .line 1874107
    const-string v1, "page_set"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1874108
    invoke-static {p0, v0, p2}, LX/CIJ;->a(LX/15i;ILX/0nX;)V

    .line 1874109
    :cond_1f
    const/16 v0, 0x20

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1874110
    if-eqz v0, :cond_20

    .line 1874111
    const-string v1, "section_header"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1874112
    invoke-static {p0, v0, p2, p3}, LX/CIK;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1874113
    :cond_20
    const/16 v0, 0x21

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1874114
    if-eqz v0, :cond_21

    .line 1874115
    const-string v1, "selected_index"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1874116
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1874117
    :cond_21
    const/16 v0, 0x22

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1874118
    if-eqz v0, :cond_22

    .line 1874119
    const-string v1, "start_date"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1874120
    invoke-static {p0, v0, p2}, LX/CI3;->a(LX/15i;ILX/0nX;)V

    .line 1874121
    :cond_22
    const/16 v0, 0x23

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1874122
    if-eqz v0, :cond_23

    .line 1874123
    const-string v1, "style"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1874124
    invoke-static {p0, v0, p2}, LX/CIU;->a(LX/15i;ILX/0nX;)V

    .line 1874125
    :cond_23
    const/16 v0, 0x24

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1874126
    if-eqz v0, :cond_24

    .line 1874127
    const-string v0, "style_list"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1874128
    const/16 v0, 0x24

    invoke-virtual {p0, p1, v0}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 1874129
    :cond_24
    const/16 v0, 0x25

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1874130
    if-eqz v0, :cond_25

    .line 1874131
    const-string v1, "target_uri"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1874132
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1874133
    :cond_25
    const/16 v0, 0x26

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1874134
    if-eqz v0, :cond_26

    .line 1874135
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1874136
    invoke-static {p0, v0, p2, p3}, LX/8al;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1874137
    :cond_26
    const/16 v0, 0x27

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1874138
    if-eqz v0, :cond_27

    .line 1874139
    const-string v1, "touch_targets"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1874140
    invoke-static {p0, v0, p2, p3}, LX/CIM;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1874141
    :cond_27
    const/16 v0, 0x28

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1874142
    if-eqz v0, :cond_28

    .line 1874143
    const-string v1, "undo_action"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1874144
    invoke-static {p0, v0, p2, p3}, LX/CHy;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1874145
    :cond_28
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1874146
    return-void
.end method
