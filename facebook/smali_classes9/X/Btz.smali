.class public final LX/Btz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7Lg;
.implements LX/7Li;
.implements LX/7Lk;


# instance fields
.field public final synthetic a:Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;)V
    .locals 0

    .prologue
    .line 1829755
    iput-object p1, p0, LX/Btz;->a:Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;B)V
    .locals 0

    .prologue
    .line 1829672
    invoke-direct {p0, p1}, LX/Btz;-><init>(Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;)V

    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/1bf;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1829752
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {p0}, LX/1VO;->v(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1829753
    :cond_0
    const/4 v0, 0x0

    .line 1829754
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/1bf;->a(Ljava/lang/String;)LX/1bf;

    move-result-object v0

    goto :goto_0
.end method

.method private a(LX/04g;LX/0QK;)Z
    .locals 10
    .param p2    # LX/0QK;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/04g;",
            "LX/0QK",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;>;)Z"
        }
    .end annotation

    .prologue
    const/4 v8, 0x1

    const/4 v1, 0x0

    .line 1829699
    const/4 v0, 0x0

    .line 1829700
    if-eqz p2, :cond_7

    .line 1829701
    iget-object v0, p0, LX/Btz;->a:Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

    iget-object v0, v0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->aa:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-interface {p2, v0}, LX/0QK;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v2, v0

    .line 1829702
    :goto_0
    if-eqz v2, :cond_0

    .line 1829703
    iget-object v0, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1829704
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    .line 1829705
    :goto_1
    return v0

    .line 1829706
    :cond_1
    iget-object v0, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1829707
    move-object v7, v0

    check-cast v7, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1829708
    invoke-static {v7}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 1829709
    invoke-virtual {v2, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v3

    .line 1829710
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v4

    invoke-static {v4}, LX/36q;->b(Lcom/facebook/graphql/model/GraphQLMedia;)Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v4

    .line 1829711
    iget-object v5, p0, LX/Btz;->a:Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

    iget-object v5, v5, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->k:LX/2mZ;

    invoke-virtual {v5, v3, v4}, LX/2mZ;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLVideo;)LX/3Im;

    move-result-object v3

    iget-object v4, p0, LX/Btz;->a:Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

    iget-boolean v4, v4, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->W:Z

    invoke-virtual {v3, v8, v1, v4}, LX/3Im;->a(ZZZ)Lcom/facebook/video/engine/VideoPlayerParams;

    move-result-object v6

    .line 1829712
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v3

    .line 1829713
    if-nez v3, :cond_2

    move v0, v1

    .line 1829714
    goto :goto_1

    .line 1829715
    :cond_2
    new-instance v1, LX/0P2;

    invoke-direct {v1}, LX/0P2;-><init>()V

    const-string v4, "GraphQLStoryProps"

    invoke-virtual {v1, v4, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v1

    const-string v4, "SubtitlesLocalesKey"

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLMedia;->bv()LX/0Px;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v1

    const-string v4, "ShowDeleteOptionKey"

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLMedia;->u()Z

    move-result v5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v1

    const-string v4, "ShowReportOptionKey"

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLMedia;->z()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v4, v3}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v1

    .line 1829716
    invoke-static {v0}, LX/Btz;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/1bf;

    move-result-object v0

    .line 1829717
    if-eqz v0, :cond_3

    .line 1829718
    const-string v3, "CoverImageParamsKey"

    invoke-virtual {v1, v3, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 1829719
    :cond_3
    sget-object v0, LX/04g;->BY_USER_SWIPE:LX/04g;

    if-ne p1, v0, :cond_4

    .line 1829720
    sget-object p1, LX/04g;->BY_USER:LX/04g;

    .line 1829721
    :cond_4
    new-instance v0, LX/2pZ;

    invoke-direct {v0}, LX/2pZ;-><init>()V

    .line 1829722
    iput-object v6, v0, LX/2pZ;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 1829723
    move-object v0, v0

    .line 1829724
    iget-object v3, p0, LX/Btz;->a:Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

    invoke-static {v3, v2}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->a(Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;Lcom/facebook/feed/rows/core/props/FeedProps;)D

    move-result-wide v2

    .line 1829725
    iput-wide v2, v0, LX/2pZ;->e:D

    .line 1829726
    move-object v0, v0

    .line 1829727
    invoke-virtual {v1}, LX/0P2;->b()LX/0P1;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2pZ;->a(LX/0P1;)LX/2pZ;

    move-result-object v0

    invoke-virtual {v0}, LX/2pZ;->b()LX/2pa;

    move-result-object v1

    .line 1829728
    iget-object v0, p0, LX/Btz;->a:Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

    iget-object v0, v0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->I:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->i()V

    .line 1829729
    iget-object v0, p0, LX/Btz;->a:Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

    .line 1829730
    invoke-static {v0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->o$redex0(Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;)V

    .line 1829731
    iget-object v0, p0, LX/Btz;->a:Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

    iget-object v0, v0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->I:Lcom/facebook/video/player/RichVideoPlayer;

    .line 1829732
    iget-object v2, v0, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    move-object v0, v2

    .line 1829733
    if-eqz v0, :cond_6

    iget-object v0, p0, LX/Btz;->a:Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

    iget-object v0, v0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->I:Lcom/facebook/video/player/RichVideoPlayer;

    .line 1829734
    iget-object v2, v0, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    move-object v0, v2

    .line 1829735
    invoke-virtual {v0}, LX/2pb;->f()LX/2oi;

    move-result-object v0

    .line 1829736
    :goto_2
    iget-object v2, p0, LX/Btz;->a:Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

    .line 1829737
    invoke-static {v2, v1}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->a$redex0(Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;LX/2pa;)V

    .line 1829738
    iget-object v2, p0, LX/Btz;->a:Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

    .line 1829739
    iput-object v1, v2, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->J:LX/2pa;

    .line 1829740
    iget-object v2, p0, LX/Btz;->a:Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

    invoke-static {v2}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->n(Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;)V

    .line 1829741
    iget-object v2, p0, LX/Btz;->a:Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

    iget-object v2, v2, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->I:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v2, v1}, Lcom/facebook/video/player/RichVideoPlayer;->c(LX/2pa;)V

    .line 1829742
    iget-object v1, p0, LX/Btz;->a:Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

    iget-object v1, v1, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->I:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v2, LX/04g;->BY_AUTOPLAY:LX/04g;

    invoke-virtual {v1, v0, v2}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/2oi;LX/04g;)V

    .line 1829743
    invoke-direct {p0, p1}, LX/Btz;->d(LX/04g;)V

    .line 1829744
    sget-object v1, LX/2oi;->HIGH_DEFINITION:LX/2oi;

    if-ne v0, v1, :cond_5

    .line 1829745
    iget-object v0, p0, LX/Btz;->a:Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

    iget-object v0, v0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->o:LX/1C2;

    iget-object v1, v6, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iget-object v2, v6, Lcom/facebook/video/engine/VideoPlayerParams;->e:LX/162;

    iget-object v3, p0, LX/Btz;->a:Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

    iget-object v3, v3, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->K:LX/395;

    invoke-virtual {v3}, LX/395;->q()LX/04D;

    move-result-object v3

    iget-object v4, p0, LX/Btz;->a:Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

    invoke-virtual {v4}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->getPlayerType()LX/04G;

    move-result-object v4

    iget-object v5, p0, LX/Btz;->a:Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

    iget-object v5, v5, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->I:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v5}, Lcom/facebook/video/player/RichVideoPlayer;->getCurrentPositionMs()I

    move-result v5

    iget-boolean v6, v6, Lcom/facebook/video/engine/VideoPlayerParams;->f:Z

    invoke-virtual/range {v0 .. v6}, LX/1C2;->a(Ljava/lang/String;LX/0lF;LX/04D;LX/04G;IZ)LX/1C2;

    .line 1829746
    :cond_5
    iget-object v0, p0, LX/Btz;->a:Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

    iget-object v0, v0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->I:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0, p1}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/04g;)V

    .line 1829747
    iget-object v0, p0, LX/Btz;->a:Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

    .line 1829748
    iput-object v7, v0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->aa:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1829749
    move v0, v8

    .line 1829750
    goto/16 :goto_1

    .line 1829751
    :cond_6
    sget-object v0, LX/2oi;->STANDARD_DEFINITION:LX/2oi;

    goto :goto_2

    :cond_7
    move-object v2, v0

    goto/16 :goto_0
.end method

.method private d(LX/04g;)V
    .locals 8

    .prologue
    .line 1829696
    iget-object v0, p0, LX/Btz;->a:Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

    iget-object v0, v0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->I:Lcom/facebook/video/player/RichVideoPlayer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Lcom/facebook/video/player/RichVideoPlayer;->a(ZLX/04g;)V

    .line 1829697
    iget-object v0, p0, LX/Btz;->a:Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

    iget-object v0, v0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->o:LX/1C2;

    iget-object v1, p0, LX/Btz;->a:Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

    iget-object v1, v1, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->J:LX/2pa;

    iget-object v1, v1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v1, v1, Lcom/facebook/video/engine/VideoPlayerParams;->e:LX/162;

    iget-object v2, p0, LX/Btz;->a:Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

    invoke-virtual {v2}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->getPlayerType()LX/04G;

    move-result-object v2

    iget-object v3, p1, LX/04g;->value:Ljava/lang/String;

    iget-object v4, p0, LX/Btz;->a:Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

    iget-object v4, v4, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->I:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v4}, Lcom/facebook/video/player/RichVideoPlayer;->getCurrentPositionMs()I

    move-result v4

    iget-object v5, p0, LX/Btz;->a:Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

    iget-object v5, v5, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->J:LX/2pa;

    iget-object v5, v5, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v5, v5, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iget-object v6, p0, LX/Btz;->a:Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

    iget-object v6, v6, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->K:LX/395;

    invoke-virtual {v6}, LX/395;->q()LX/04D;

    move-result-object v6

    iget-object v7, p0, LX/Btz;->a:Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

    iget-object v7, v7, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->J:LX/2pa;

    iget-object v7, v7, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    invoke-virtual/range {v0 .. v7}, LX/1C2;->b(LX/0lF;LX/04G;Ljava/lang/String;ILjava/lang/String;LX/04D;LX/098;)LX/1C2;

    .line 1829698
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 1829684
    iget-object v0, p0, LX/Btz;->a:Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

    iget-object v0, v0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->K:LX/395;

    .line 1829685
    iget-object v1, v0, LX/395;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v1

    .line 1829686
    if-eqz v0, :cond_2

    iget-object v0, p0, LX/Btz;->a:Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

    iget-object v0, v0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->K:LX/395;

    .line 1829687
    iget-object v1, v0, LX/395;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v1

    .line 1829688
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v1

    .line 1829689
    :goto_0
    if-eqz v0, :cond_0

    .line 1829690
    iget-object v1, p0, LX/Btz;->a:Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

    iget-object v1, v1, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->u:LX/0bH;

    new-instance v2, LX/1ZX;

    invoke-direct {v2, v0}, LX/1ZX;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b7;)V

    .line 1829691
    :cond_0
    iget-object v0, p0, LX/Btz;->a:Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1829692
    iget-object v0, p0, LX/Btz;->a:Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

    const/4 v1, 0x1

    sget-object v2, LX/04g;->BY_USER:LX/04g;

    sget-object v3, LX/04G;->INLINE_PLAYER:LX/04G;

    .line 1829693
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->a$redex0(Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;ZLX/04g;LX/04G;)V

    .line 1829694
    :cond_1
    return-void

    .line 1829695
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/04g;)V
    .locals 0

    .prologue
    .line 1829756
    return-void
.end method

.method public final b(LX/04g;)V
    .locals 1

    .prologue
    .line 1829681
    iget-object v0, p0, LX/Btz;->a:Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

    iget-object v0, v0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->af:LX/0QK;

    invoke-direct {p0, p1, v0}, LX/Btz;->a(LX/04g;LX/0QK;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Btz;->a:Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

    iget-object v0, v0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->ah:Ljava/util/Map;

    if-eqz v0, :cond_0

    sget-object v0, LX/04g;->BY_USER_SWIPE:LX/04g;

    if-ne p1, v0, :cond_0

    .line 1829682
    iget-object v0, p0, LX/Btz;->a:Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

    invoke-static {v0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->m(Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;)I

    .line 1829683
    :cond_0
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1829680
    const/4 v0, 0x0

    return v0
.end method

.method public final c(LX/04g;)V
    .locals 1

    .prologue
    .line 1829677
    iget-object v0, p0, LX/Btz;->a:Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

    iget-object v0, v0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->ag:LX/0QK;

    invoke-direct {p0, p1, v0}, LX/Btz;->a(LX/04g;LX/0QK;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Btz;->a:Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

    iget-object v0, v0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->ah:Ljava/util/Map;

    if-eqz v0, :cond_0

    sget-object v0, LX/04g;->BY_USER_SWIPE:LX/04g;

    if-ne p1, v0, :cond_0

    .line 1829678
    iget-object v0, p0, LX/Btz;->a:Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

    invoke-static {v0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->m(Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;)I

    .line 1829679
    :cond_0
    return-void
.end method

.method public final c()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1829675
    iget-object v1, p0, LX/Btz;->a:Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

    iget-object v1, v1, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->af:LX/0QK;

    if-nez v1, :cond_1

    .line 1829676
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, LX/Btz;->a:Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

    iget-object v1, v1, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->af:LX/0QK;

    iget-object v2, p0, LX/Btz;->a:Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

    iget-object v2, v2, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->aa:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-interface {v1, v2}, LX/0QK;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final d()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1829673
    iget-object v1, p0, LX/Btz;->a:Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

    iget-object v1, v1, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->ag:LX/0QK;

    if-nez v1, :cond_1

    .line 1829674
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, LX/Btz;->a:Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

    iget-object v1, v1, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->ag:LX/0QK;

    iget-object v2, p0, LX/Btz;->a:Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

    iget-object v2, v2, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->aa:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-interface {v1, v2}, LX/0QK;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final n()LX/1SX;
    .locals 1

    .prologue
    .line 1829671
    iget-object v0, p0, LX/Btz;->a:Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

    iget-object v0, v0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->ak:LX/Bur;

    return-object v0
.end method
