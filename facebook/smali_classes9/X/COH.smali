.class public final LX/COH;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/COH;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/COF;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/COI;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1882825
    const/4 v0, 0x0

    sput-object v0, LX/COH;->a:LX/COH;

    .line 1882826
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/COH;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1882827
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1882828
    new-instance v0, LX/COI;

    invoke-direct {v0}, LX/COI;-><init>()V

    iput-object v0, p0, LX/COH;->c:LX/COI;

    .line 1882829
    return-void
.end method

.method public static declared-synchronized q()LX/COH;
    .locals 2

    .prologue
    .line 1882830
    const-class v1, LX/COH;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/COH;->a:LX/COH;

    if-nez v0, :cond_0

    .line 1882831
    new-instance v0, LX/COH;

    invoke-direct {v0}, LX/COH;-><init>()V

    sput-object v0, LX/COH;->a:LX/COH;

    .line 1882832
    :cond_0
    sget-object v0, LX/COH;->a:LX/COH;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 1882833
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 3

    .prologue
    .line 1882834
    check-cast p2, LX/COG;

    .line 1882835
    iget-object v0, p2, LX/COG;->a:LX/CNb;

    iget-object v1, p2, LX/COG;->b:LX/CNr;

    .line 1882836
    iget-object v2, v1, LX/CNr;->f:LX/1vb;

    move-object v2, v2

    .line 1882837
    invoke-virtual {v2, p1}, LX/1vb;->c(LX/1De;)LX/1vd;

    move-result-object v2

    .line 1882838
    iget-object p0, v1, LX/CNr;->d:Lcom/facebook/graphql/model/FeedUnit;

    move-object p0, p0

    .line 1882839
    invoke-static {p0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object p0

    invoke-virtual {v2, p0}, LX/1vd;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1vd;

    move-result-object v2

    sget-object p0, LX/1dl;->CLICKABLE:LX/1dl;

    invoke-virtual {v2, p0}, LX/1vd;->a(LX/1dl;)LX/1vd;

    move-result-object v2

    .line 1882840
    iget-object p0, v1, LX/CNr;->e:LX/1Pf;

    move-object p0, p0

    .line 1882841
    invoke-interface {p0}, LX/1Pk;->e()LX/1SX;

    move-result-object p0

    invoke-virtual {v2, p0}, LX/1vd;->a(LX/1SX;)LX/1vd;

    move-result-object v2

    .line 1882842
    const-string p0, "color"

    invoke-virtual {v0, p0}, LX/CNb;->c(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_0

    .line 1882843
    const-string p0, "color"

    const/4 p2, 0x0

    invoke-virtual {v0, p0, p2}, LX/CNb;->b(Ljava/lang/String;I)I

    move-result p0

    .line 1882844
    iget-object p2, v2, LX/1vd;->a:LX/1vc;

    iput p0, p2, LX/1vc;->b:I

    .line 1882845
    :cond_0
    invoke-virtual {v2}, LX/1X5;->b()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 1882846
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1882847
    invoke-static {}, LX/1dS;->b()V

    .line 1882848
    const/4 v0, 0x0

    return-object v0
.end method
