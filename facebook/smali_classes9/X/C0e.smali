.class public final LX/C0e;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic b:LX/2yV;

.field public final synthetic c:LX/1Pf;

.field public final synthetic d:Lcom/facebook/feedplugins/attachments/linkshare/follow/FollowUserShareAttachmentPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/attachments/linkshare/follow/FollowUserShareAttachmentPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;LX/2yV;LX/1Pf;)V
    .locals 0

    .prologue
    .line 1841141
    iput-object p1, p0, LX/C0e;->d:Lcom/facebook/feedplugins/attachments/linkshare/follow/FollowUserShareAttachmentPartDefinition;

    iput-object p2, p0, LX/C0e;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iput-object p3, p0, LX/C0e;->b:LX/2yV;

    iput-object p4, p0, LX/C0e;->c:LX/1Pf;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v0, 0x2

    const v1, 0xca2cae5

    invoke-static {v0, v4, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1841142
    iget-object v0, p0, LX/C0e;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1841143
    iget-object v2, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v2

    .line 1841144
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v0}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->az()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v0

    .line 1841145
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProfile;->K()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->IS_SUBSCRIBED:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    if-eq v2, v3, :cond_0

    iget-object v2, p0, LX/C0e;->b:LX/2yV;

    .line 1841146
    iget-object v3, v2, LX/2yV;->a:LX/C0a;

    move-object v2, v3

    .line 1841147
    sget-object v3, LX/C0a;->FOLLOWING_USER:LX/C0a;

    if-ne v2, v3, :cond_1

    .line 1841148
    :cond_0
    iget-object v2, p0, LX/C0e;->c:LX/1Pf;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object v0

    const-string v3, "FEED_SHARE_LINK"

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v0, v3}, LX/1Pb;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1841149
    iget-object v0, p0, LX/C0e;->b:LX/2yV;

    sget-object v2, LX/C0a;->FOLLOW_USER:LX/C0a;

    .line 1841150
    iput-object v2, v0, LX/2yV;->a:LX/C0a;

    .line 1841151
    iget-object v0, p0, LX/C0e;->c:LX/1Pf;

    new-array v2, v4, [Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p0, LX/C0e;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    aput-object v3, v2, v5

    invoke-interface {v0, v2}, LX/1Pq;->a([Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1841152
    :goto_0
    const v0, 0x1a1e8930

    invoke-static {v0, v1}, LX/02F;->a(II)V

    return-void

    .line 1841153
    :cond_1
    iget-object v2, p0, LX/C0e;->c:LX/1Pf;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object v0

    const-string v3, "FEED_SHARE_LINK"

    invoke-interface {v2, v0, v3}, LX/1Pb;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1841154
    iget-object v0, p0, LX/C0e;->b:LX/2yV;

    sget-object v2, LX/C0a;->FOLLOWING_USER:LX/C0a;

    .line 1841155
    iput-object v2, v0, LX/2yV;->a:LX/C0a;

    .line 1841156
    iget-object v0, p0, LX/C0e;->c:LX/1Pf;

    new-array v2, v4, [Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p0, LX/C0e;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    aput-object v3, v2, v5

    invoke-interface {v0, v2}, LX/1Pq;->a([Lcom/facebook/feed/rows/core/props/FeedProps;)V

    goto :goto_0
.end method
