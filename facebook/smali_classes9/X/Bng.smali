.class public LX/Bng;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1821127
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1821128
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnitItem;)LX/Bnf;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1821129
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEventsSuggestionFeedUnitItem;->a()Lcom/facebook/graphql/model/GraphQLEvent;

    move-result-object v1

    .line 1821130
    if-nez v1, :cond_0

    .line 1821131
    :goto_0
    return-object v0

    .line 1821132
    :cond_0
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEvent;->aI()Ljava/lang/String;

    move-result-object v2

    .line 1821133
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEvent;->bn()J

    move-result-wide v4

    invoke-static {v4, v5}, LX/5O7;->c(J)Ljava/util/Date;

    move-result-object v3

    .line 1821134
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEvent;->Z()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v4

    .line 1821135
    invoke-static {v4}, LX/Bnh;->a(Lcom/facebook/graphql/model/GraphQLPlace;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4}, LX/Bnh;->b(Lcom/facebook/graphql/model/GraphQLPlace;)Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, v5, v4}, Lcom/facebook/events/widget/eventcard/EventsCardView;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v4

    .line 1821136
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEvent;->bl()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    if-eqz v5, :cond_1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEvent;->bl()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    .line 1821137
    :cond_1
    new-instance v1, LX/Bnf;

    invoke-direct {v1, v2, v4, v0, v3}, LX/Bnf;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/util/Date;)V

    move-object v0, v1

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/Bnf;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 1821138
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->a()LX/0Px;

    move-result-object v0

    .line 1821139
    invoke-static {v0}, LX/Bnh;->a(Ljava/util/List;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v4

    .line 1821140
    if-eqz v4, :cond_7

    .line 1821141
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->E()Lcom/facebook/graphql/model/GraphQLEvent;

    move-result-object v5

    .line 1821142
    if-eqz v5, :cond_6

    .line 1821143
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLEvent;->bn()J

    move-result-wide v2

    invoke-static {v2, v3}, LX/5O7;->c(J)Ljava/util/Date;

    move-result-object v0

    .line 1821144
    :goto_0
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ah()Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->EVENT_RSVP:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    if-ne v2, v3, :cond_0

    .line 1821145
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ag()Ljava/lang/String;

    move-result-object v3

    .line 1821146
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ab()Ljava/lang/String;

    move-result-object v2

    .line 1821147
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ac()Ljava/lang/String;

    move-result-object v1

    :goto_1
    move-object v7, v2

    move-object v2, v3

    move-object v3, v7

    .line 1821148
    :goto_2
    new-instance v4, LX/Bnf;

    invoke-direct {v4, v2, v3, v1, v0}, LX/Bnf;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/util/Date;)V

    return-object v4

    .line 1821149
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->A()Ljava/lang/String;

    move-result-object v2

    .line 1821150
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v4

    .line 1821151
    :goto_3
    if-eqz v5, :cond_5

    .line 1821152
    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1821153
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLEvent;->aI()Ljava/lang/String;

    move-result-object v2

    .line 1821154
    :cond_1
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLEvent;->Z()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v3

    .line 1821155
    invoke-static {v3}, LX/Bnh;->a(Lcom/facebook/graphql/model/GraphQLPlace;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v3}, LX/Bnh;->b(Lcom/facebook/graphql/model/GraphQLPlace;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v6, v3}, Lcom/facebook/events/widget/eventcard/EventsCardView;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v3

    .line 1821156
    invoke-static {v4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1821157
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLEvent;->bl()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    if-eqz v4, :cond_3

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLEvent;->bl()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    move-object v7, v3

    move-object v3, v2

    move-object v2, v7

    goto :goto_1

    :cond_2
    move-object v4, v1

    .line 1821158
    goto :goto_3

    :cond_3
    move-object v7, v3

    move-object v3, v2

    move-object v2, v7

    .line 1821159
    goto :goto_1

    :cond_4
    move-object v1, v4

    goto :goto_2

    :cond_5
    move-object v3, v1

    move-object v1, v4

    goto :goto_2

    :cond_6
    move-object v0, v1

    goto :goto_0

    :cond_7
    move-object v0, v1

    move-object v3, v1

    move-object v2, v1

    goto :goto_2
.end method
