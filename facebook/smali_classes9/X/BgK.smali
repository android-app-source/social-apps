.class public LX/BgK;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public A:LX/Bg9;

.field public B:Z

.field public final b:LX/BfJ;

.field public final c:LX/31f;

.field public final d:Landroid/content/Context;

.field public final e:LX/03V;

.field public final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0hB;

.field public final i:Lcom/facebook/content/SecureContextHelper;

.field private final j:LX/Bf7;

.field public final k:LX/CdY;

.field public final l:LX/BhM;

.field public final m:LX/0kL;

.field public final n:LX/17Y;

.field private final o:LX/0W3;

.field public p:Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;

.field public q:LX/1ZF;

.field public r:Ljava/lang/String;

.field public s:Ljava/lang/String;

.field public t:Lcom/facebook/crowdsourcing/suggestedits/fragment/SuggestEditsFragment;

.field public u:Landroid/widget/LinearLayout;

.field public v:Landroid/widget/ProgressBar;

.field public w:Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionsModel;

.field public x:LX/BgW;

.field public y:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/BgT;",
            ">;"
        }
    .end annotation
.end field

.field public z:LX/Bg9;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1807391
    const-class v0, LX/BgK;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/BgK;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0hB;LX/31f;LX/03V;LX/0Or;LX/0Or;Lcom/facebook/content/SecureContextHelper;LX/Bf7;LX/BfJ;LX/CdY;LX/BhM;LX/0kL;LX/17Y;LX/0W3;)V
    .locals 0
    .param p5    # LX/0Or;
        .annotation runtime Lcom/facebook/crowdsourcing/friendvote/gk/IsFriendVoteInviteEnabled;
        .end annotation
    .end param
    .param p6    # LX/0Or;
        .annotation runtime Lcom/facebook/crowdsourcing/suggestedits/gk/IsPostSuggestEditsUpsellEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0hB;",
            "LX/31f;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/Bf7;",
            "LX/BfJ;",
            "LX/CdY;",
            "LX/BhM;",
            "LX/0kL;",
            "LX/17Y;",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1807375
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1807376
    iput-object p3, p0, LX/BgK;->c:LX/31f;

    .line 1807377
    iput-object p1, p0, LX/BgK;->d:Landroid/content/Context;

    .line 1807378
    iput-object p2, p0, LX/BgK;->h:LX/0hB;

    .line 1807379
    iput-object p4, p0, LX/BgK;->e:LX/03V;

    .line 1807380
    iput-object p5, p0, LX/BgK;->f:LX/0Or;

    .line 1807381
    iput-object p6, p0, LX/BgK;->g:LX/0Or;

    .line 1807382
    iput-object p7, p0, LX/BgK;->i:Lcom/facebook/content/SecureContextHelper;

    .line 1807383
    iput-object p8, p0, LX/BgK;->j:LX/Bf7;

    .line 1807384
    iput-object p9, p0, LX/BgK;->b:LX/BfJ;

    .line 1807385
    iput-object p10, p0, LX/BgK;->k:LX/CdY;

    .line 1807386
    iput-object p11, p0, LX/BgK;->l:LX/BhM;

    .line 1807387
    iput-object p12, p0, LX/BgK;->m:LX/0kL;

    .line 1807388
    iput-object p13, p0, LX/BgK;->n:LX/17Y;

    .line 1807389
    iput-object p14, p0, LX/BgK;->o:LX/0W3;

    .line 1807390
    return-void
.end method

.method public static a$redex0(LX/BgK;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1807354
    iget-object v0, p0, LX/BgK;->o:LX/0W3;

    sget-wide v2, LX/0X5;->hv:J

    const/4 v1, 0x0

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JI)I

    move-result v0

    .line 1807355
    if-nez v0, :cond_0

    .line 1807356
    iget-object v0, p0, LX/BgK;->t:Lcom/facebook/crowdsourcing/suggestedits/fragment/SuggestEditsFragment;

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/suggestedits/fragment/SuggestEditsFragment;->b()V

    .line 1807357
    :goto_0
    return-void

    .line 1807358
    :cond_0
    new-instance v1, LX/0ju;

    iget-object v2, p0, LX/BgK;->d:Landroid/content/Context;

    invoke-direct {v1, v2}, LX/0ju;-><init>(Landroid/content/Context;)V

    .line 1807359
    new-instance v2, LX/Bgo;

    iget-object v3, p0, LX/BgK;->d:Landroid/content/Context;

    invoke-direct {v2, v3}, LX/Bgo;-><init>(Landroid/content/Context;)V

    .line 1807360
    iget-object v3, v2, LX/Bgo;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v3, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1807361
    iget-object v3, v2, LX/Bgo;->b:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v3, p2}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 1807362
    new-instance v3, LX/BgE;

    invoke-direct {v3, p0, p3}, LX/BgE;-><init>(LX/BgK;Landroid/content/Intent;)V

    .line 1807363
    iget-object p1, v2, LX/Bgo;->b:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {p1, v3}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1807364
    new-instance v3, LX/BgF;

    invoke-direct {v3, p0}, LX/BgF;-><init>(LX/BgK;)V

    .line 1807365
    iget-object p1, v2, LX/Bgo;->c:Lcom/facebook/resources/ui/FbButton;

    if-nez p1, :cond_2

    .line 1807366
    :goto_1
    invoke-virtual {v1, v2}, LX/0ju;->b(Landroid/view/View;)LX/0ju;

    move-result-object v2

    invoke-virtual {v2, v4}, LX/0ju;->a(Z)LX/0ju;

    .line 1807367
    if-ne v0, v4, :cond_1

    .line 1807368
    const v0, 0x7f082929

    new-instance v2, LX/BgG;

    invoke-direct {v2, p0}, LX/BgG;-><init>(LX/BgK;)V

    invoke-virtual {v1, v0, v2}, LX/0ju;->c(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 1807369
    :cond_1
    invoke-virtual {v1}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 1807370
    iget-object v0, p0, LX/BgK;->c:LX/31f;

    const-string v1, "android_post_suggest_edits_upsell"

    iget-object v2, p0, LX/BgK;->r:Ljava/lang/String;

    invoke-static {v2}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/31f;->a(Ljava/lang/String;LX/0am;)V

    goto :goto_0

    .line 1807371
    :cond_2
    iget-object p1, v2, LX/Bgo;->c:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {p1, v3}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1
.end method

.method public static b(LX/0QB;)LX/BgK;
    .locals 15

    .prologue
    .line 1807373
    new-instance v0, LX/BgK;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v2

    check-cast v2, LX/0hB;

    invoke-static {p0}, LX/31f;->a(LX/0QB;)LX/31f;

    move-result-object v3

    check-cast v3, LX/31f;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    const/16 v5, 0x147e

    invoke-static {p0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 v6, 0x1481

    invoke-static {p0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v7

    check-cast v7, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/Bf7;->a(LX/0QB;)LX/Bf7;

    move-result-object v8

    check-cast v8, LX/Bf7;

    invoke-static {p0}, LX/BfJ;->a(LX/0QB;)LX/BfJ;

    move-result-object v9

    check-cast v9, LX/BfJ;

    invoke-static {p0}, LX/CdY;->b(LX/0QB;)LX/CdY;

    move-result-object v10

    check-cast v10, LX/CdY;

    invoke-static {p0}, LX/BhM;->a(LX/0QB;)LX/BhM;

    move-result-object v11

    check-cast v11, LX/BhM;

    invoke-static {p0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v12

    check-cast v12, LX/0kL;

    invoke-static {p0}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v13

    check-cast v13, LX/17Y;

    invoke-static {p0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v14

    check-cast v14, LX/0W3;

    invoke-direct/range {v0 .. v14}, LX/BgK;-><init>(Landroid/content/Context;LX/0hB;LX/31f;LX/03V;LX/0Or;LX/0Or;Lcom/facebook/content/SecureContextHelper;LX/Bf7;LX/BfJ;LX/CdY;LX/BhM;LX/0kL;LX/17Y;LX/0W3;)V

    .line 1807374
    return-object v0
.end method

.method public static b(Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;)Z
    .locals 1

    .prologue
    .line 1807372
    if-eqz p0, :cond_2

    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;->b()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;->b()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;->a()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;->c()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;->c()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(LX/BgK;Landroid/os/Bundle;)V
    .locals 9

    .prologue
    .line 1807392
    iget-object v0, p0, LX/BgK;->h:LX/0hB;

    invoke-virtual {v0}, LX/0hB;->c()I

    move-result v0

    .line 1807393
    iget-object v1, p0, LX/BgK;->u:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b1c70

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 1807394
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v6

    .line 1807395
    if-nez p1, :cond_1

    const/4 v4, 0x0

    .line 1807396
    :goto_0
    if-eqz v4, :cond_2

    .line 1807397
    iget-boolean v0, v4, Lcom/facebook/crowdsourcing/suggestedits/data/SuggestEditsHeaderState;->a:Z

    move v0, v0

    .line 1807398
    if-eqz v0, :cond_2

    .line 1807399
    iget-object v0, p0, LX/BgK;->l:LX/BhM;

    iget-object v1, p0, LX/BgK;->t:Lcom/facebook/crowdsourcing/suggestedits/fragment/SuggestEditsFragment;

    iget-object v3, p0, LX/BgK;->u:Landroid/widget/LinearLayout;

    iget-object v5, p0, LX/BgK;->r:Ljava/lang/String;

    move-object v2, p0

    invoke-virtual/range {v0 .. v5}, LX/BhM;->a(Landroid/support/v4/app/Fragment;LX/BgK;Landroid/widget/LinearLayout;Lcom/facebook/crowdsourcing/suggestedits/data/SuggestEditsHeaderState;Ljava/lang/String;)LX/BgW;

    move-result-object v0

    iput-object v0, p0, LX/BgK;->x:LX/BgW;

    .line 1807400
    iget-object v0, p0, LX/BgK;->x:LX/BgW;

    invoke-virtual {v0}, LX/BgW;->c()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;

    move-result-object v0

    invoke-static {v0}, LX/BgK;->b(Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1807401
    iget-object v0, p0, LX/BgK;->z:LX/Bg9;

    sget-object v1, LX/BgJ;->HEADER:LX/BgJ;

    invoke-virtual {v0, v1}, LX/Bg9;->a(LX/BgJ;)V

    .line 1807402
    :cond_0
    iget-object v0, p0, LX/BgK;->A:LX/Bg9;

    sget-object v1, LX/BgJ;->HEADER:LX/BgJ;

    invoke-virtual {v0, v1}, LX/Bg9;->a(LX/BgJ;)V

    .line 1807403
    :goto_1
    return-void

    .line 1807404
    :cond_1
    const-string v0, "state_header"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/suggestedits/data/SuggestEditsHeaderState;

    move-object v4, v0

    goto :goto_0

    .line 1807405
    :cond_2
    iget-object v0, p0, LX/BgK;->l:LX/BhM;

    iget-object v1, p0, LX/BgK;->t:Lcom/facebook/crowdsourcing/suggestedits/fragment/SuggestEditsFragment;

    iget-object v3, p0, LX/BgK;->u:Landroid/widget/LinearLayout;

    iget-object v2, p0, LX/BgK;->s:Ljava/lang/String;

    .line 1807406
    new-instance v4, Lcom/facebook/crowdsourcing/suggestedits/data/SuggestEditsHeaderState;

    .line 1807407
    new-instance v5, LX/97m;

    invoke-direct {v5}, LX/97m;-><init>()V

    .line 1807408
    iput-object v2, v5, LX/97m;->m:Ljava/lang/String;

    .line 1807409
    move-object v5, v5

    .line 1807410
    invoke-virtual {v5}, LX/97m;->a()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;

    move-result-object v5

    .line 1807411
    new-instance p1, LX/97l;

    invoke-direct {p1}, LX/97l;-><init>()V

    .line 1807412
    iput-object v5, p1, LX/97l;->a:Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;

    .line 1807413
    move-object v5, p1

    .line 1807414
    invoke-virtual {v5}, LX/97l;->a()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel;

    move-result-object v5

    .line 1807415
    new-instance p1, LX/97k;

    invoke-direct {p1}, LX/97k;-><init>()V

    invoke-static {v5}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v5

    .line 1807416
    iput-object v5, p1, LX/97k;->a:LX/0Px;

    .line 1807417
    move-object v5, p1

    .line 1807418
    invoke-virtual {v5}, LX/97k;->a()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel;

    move-result-object v5

    .line 1807419
    new-instance p1, LX/97j;

    invoke-direct {p1}, LX/97j;-><init>()V

    .line 1807420
    iput-object v5, p1, LX/97j;->b:Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel;

    .line 1807421
    move-object v5, p1

    .line 1807422
    const/4 p1, 0x0

    .line 1807423
    iput-boolean p1, v5, LX/97j;->a:Z

    .line 1807424
    move-object v5, v5

    .line 1807425
    invoke-virtual {v5}, LX/97j;->a()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;

    move-result-object v5

    .line 1807426
    new-instance p1, LX/97z;

    invoke-direct {p1}, LX/97z;-><init>()V

    .line 1807427
    iput-object v5, p1, LX/97z;->a:Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;

    .line 1807428
    move-object v5, p1

    .line 1807429
    invoke-virtual {v5}, LX/97z;->a()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;

    move-result-object v5

    move-object v5, v5

    .line 1807430
    const/4 p1, 0x0

    invoke-direct {v4, v5, p1}, Lcom/facebook/crowdsourcing/suggestedits/data/SuggestEditsHeaderState;-><init>(Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;Z)V

    move-object v4, v4

    .line 1807431
    iget-object v5, p0, LX/BgK;->r:Ljava/lang/String;

    move-object v2, p0

    invoke-virtual/range {v0 .. v5}, LX/BhM;->a(Landroid/support/v4/app/Fragment;LX/BgK;Landroid/widget/LinearLayout;Lcom/facebook/crowdsourcing/suggestedits/data/SuggestEditsHeaderState;Ljava/lang/String;)LX/BgW;

    move-result-object v0

    iput-object v0, p0, LX/BgK;->x:LX/BgW;

    .line 1807432
    iget-object v0, p0, LX/BgK;->j:LX/Bf7;

    iget-object v1, p0, LX/BgK;->r:Ljava/lang/String;

    .line 1807433
    iget-object v2, v0, LX/Bf7;->b:LX/1Ck;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "key_load_suggest_edits_header"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v0, LX/Bf7;->a:LX/Bf4;

    .line 1807434
    new-instance v5, LX/97X;

    invoke-direct {v5}, LX/97X;-><init>()V

    move-object v5, v5

    .line 1807435
    const-string v7, "page_id"

    invoke-virtual {v5, v7, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v7

    const-string v8, "image_size"

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v7, v8, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1807436
    iget-object v7, v4, LX/Bf4;->a:LX/0tX;

    invoke-static {v5}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v5

    invoke-virtual {v7, v5}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v5

    .line 1807437
    new-instance v7, LX/Bf3;

    invoke-direct {v7, v4}, LX/Bf3;-><init>(LX/Bf4;)V

    iget-object v8, v4, LX/Bf4;->b:LX/0TD;

    invoke-static {v5, v7, v8}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    move-object v4, v5

    .line 1807438
    new-instance v5, LX/Bf5;

    invoke-direct {v5, v0, p0}, LX/Bf5;-><init>(LX/Bf7;LX/BgK;)V

    invoke-virtual {v2, v3, v4, v5}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1807439
    goto/16 :goto_1
.end method

.method public static d(LX/BgK;Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 1807321
    if-nez p1, :cond_2

    move-object v4, v1

    .line 1807322
    :goto_0
    if-nez p1, :cond_3

    move-object v6, v1

    .line 1807323
    :goto_1
    if-eqz v4, :cond_5

    if-eqz v6, :cond_5

    .line 1807324
    iput-object v4, p0, LX/BgK;->w:Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionsModel;

    .line 1807325
    iget-object v0, p0, LX/BgK;->l:LX/BhM;

    iget-object v1, p0, LX/BgK;->t:Lcom/facebook/crowdsourcing/suggestedits/fragment/SuggestEditsFragment;

    iget-object v3, p0, LX/BgK;->u:Landroid/widget/LinearLayout;

    iget-object v5, p0, LX/BgK;->r:Ljava/lang/String;

    move-object v2, p0

    invoke-virtual/range {v0 .. v5}, LX/BhM;->a(Landroid/support/v4/app/Fragment;LX/BgK;Landroid/widget/LinearLayout;Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionsModel;Ljava/lang/String;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/BgK;->y:LX/0Px;

    .line 1807326
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget-object v1, p0, LX/BgK;->y:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 1807327
    iget-object v0, p0, LX/BgK;->e:LX/03V;

    sget-object v1, LX/BgK;->a:Ljava/lang/String;

    const-string v2, "The number of re-created SuggestEditsFieldHolders didn\'t match the number of savedSuggestEditFields"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1807328
    :cond_0
    iget-object v0, p0, LX/BgK;->y:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1807329
    iget-object v0, p0, LX/BgK;->z:LX/Bg9;

    sget-object v1, LX/BgJ;->SECTIONS:LX/BgJ;

    invoke-virtual {v0, v1}, LX/Bg9;->a(LX/BgJ;)V

    .line 1807330
    :cond_1
    const/4 v0, 0x0

    move v2, v0

    :goto_2
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_4

    .line 1807331
    iget-object v0, p0, LX/BgK;->y:LX/0Px;

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BgT;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/97f;

    .line 1807332
    invoke-static {v0, v1}, LX/BgT;->c(LX/BgT;LX/97f;)V

    .line 1807333
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 1807334
    :cond_2
    const-string v0, "state_original_sections"

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionsModel;

    move-object v4, v0

    goto :goto_0

    .line 1807335
    :cond_3
    const-string v0, "state_sections"

    invoke-static {p1, v0}, LX/4By;->b(Landroid/os/Bundle;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    move-object v6, v0

    goto :goto_1

    .line 1807336
    :cond_4
    iget-object v0, p0, LX/BgK;->v:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1807337
    iget-object v0, p0, LX/BgK;->A:LX/Bg9;

    sget-object v1, LX/BgJ;->SECTIONS:LX/BgJ;

    invoke-virtual {v0, v1}, LX/Bg9;->a(LX/BgJ;)V

    .line 1807338
    :goto_3
    return-void

    .line 1807339
    :cond_5
    iget-object v0, p0, LX/BgK;->j:LX/Bf7;

    iget-object v1, p0, LX/BgK;->r:Ljava/lang/String;

    .line 1807340
    iget-object v2, v0, LX/Bf7;->b:LX/1Ck;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "key_load_suggest_edits_sections"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v0, LX/Bf7;->a:LX/Bf4;

    .line 1807341
    new-instance v5, LX/97Y;

    invoke-direct {v5}, LX/97Y;-><init>()V

    move-object v5, v5

    .line 1807342
    const-string v6, "page_id"

    invoke-virtual {v5, v6, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v6

    const-string v7, "scale"

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object p1

    invoke-virtual {v6, v7, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    .line 1807343
    iget-object v6, v4, LX/Bf4;->a:LX/0tX;

    invoke-static {v5}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v5

    invoke-virtual {v6, v5}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v5

    .line 1807344
    new-instance v6, LX/Bf2;

    invoke-direct {v6, v4}, LX/Bf2;-><init>(LX/Bf4;)V

    iget-object v7, v4, LX/Bf4;->b:LX/0TD;

    invoke-static {v5, v6, v7}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    move-object v4, v5

    .line 1807345
    new-instance v5, LX/Bf6;

    invoke-direct {v5, v0, p0}, LX/Bf6;-><init>(LX/Bf7;LX/BgK;)V

    invoke-virtual {v2, v3, v4, v5}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1807346
    goto :goto_3
.end method

.method public static e(LX/BgK;)V
    .locals 4

    .prologue
    .line 1807306
    iget-object v0, p0, LX/BgK;->q:LX/1ZF;

    if-nez v0, :cond_0

    .line 1807307
    :goto_0
    return-void

    .line 1807308
    :cond_0
    iget-object v0, p0, LX/BgK;->q:LX/1ZF;

    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v1

    iget-object v2, p0, LX/BgK;->t:Lcom/facebook/crowdsourcing/suggestedits/fragment/SuggestEditsFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f082912

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1807309
    iput-object v2, v1, LX/108;->g:Ljava/lang/String;

    .line 1807310
    move-object v1, v1

    .line 1807311
    iget-boolean v2, p0, LX/BgK;->B:Z

    .line 1807312
    iput-boolean v2, v1, LX/108;->d:Z

    .line 1807313
    move-object v1, v1

    .line 1807314
    invoke-virtual {v1}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1ZF;->a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 1807315
    iget-boolean v0, p0, LX/BgK;->B:Z

    if-nez v0, :cond_0

    .line 1807316
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/BgK;->B:Z

    .line 1807317
    iget-object v0, p0, LX/BgK;->c:LX/31f;

    iget-object v1, p0, LX/BgK;->p:Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;

    iget-object v2, p0, LX/BgK;->r:Ljava/lang/String;

    .line 1807318
    const-string v3, "suggestion_edited"

    invoke-static {v0, v1, v3, v2}, LX/31f;->b(LX/31f;Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;Ljava/lang/String;Ljava/lang/String;)V

    .line 1807319
    invoke-static {p0}, LX/BgK;->e(LX/BgK;)V

    .line 1807320
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 1807347
    iget-object v0, p0, LX/BgK;->e:LX/03V;

    sget-object v1, LX/BgK;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Loading Suggest Edits Header failed: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1807348
    return-void
.end method

.method public final b(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 1807349
    iget-object v0, p0, LX/BgK;->v:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1807350
    iget-object v0, p0, LX/BgK;->m:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f08290b

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 1807351
    iget-object v0, p0, LX/BgK;->t:Lcom/facebook/crowdsourcing/suggestedits/fragment/SuggestEditsFragment;

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/suggestedits/fragment/SuggestEditsFragment;->b()V

    .line 1807352
    iget-object v0, p0, LX/BgK;->e:LX/03V;

    sget-object v1, LX/BgK;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Loading Suggest Edits Sections failed: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1807353
    return-void
.end method
