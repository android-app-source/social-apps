.class public LX/BGF;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "LX/AwR;",
        ">;"
    }
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1766838
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/AwR;
    .locals 3

    .prologue
    .line 1766839
    const-class v1, LX/BGF;

    monitor-enter v1

    .line 1766840
    :try_start_0
    sget-object v0, LX/BGF;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1766841
    sput-object v2, LX/BGF;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1766842
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1766843
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1766844
    const-class p0, LX/AwS;

    invoke-interface {v0, p0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object p0

    check-cast p0, LX/AwS;

    invoke-static {p0}, LX/BGH;->a(LX/AwS;)LX/AwR;

    move-result-object p0

    move-object v0, p0

    .line 1766845
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1766846
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/AwR;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1766847
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1766848
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1766849
    const-class v0, LX/AwS;

    invoke-interface {p0, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/AwS;

    invoke-static {v0}, LX/BGH;->a(LX/AwS;)LX/AwR;

    move-result-object v0

    return-object v0
.end method
