.class public LX/Aby;
.super LX/AVi;
.source ""

# interfaces
.implements LX/Abx;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/AVi",
        "<",
        "Lcom/facebook/facecastdisplay/FacecastInteractionView;",
        ">;",
        "LX/Abx;"
    }
.end annotation


# instance fields
.field public A:I

.field public B:I

.field public C:I

.field public D:I

.field public E:I

.field public F:Z

.field public G:Z

.field public H:Z

.field public I:Z

.field private J:I

.field private K:I

.field public L:Z

.field public M:Z

.field public N:Z

.field public O:I

.field public P:Z

.field public Q:I

.field public R:Z

.field public final a:LX/AcX;

.field public final b:LX/1b4;

.field public final c:LX/AgK;

.field public final d:LX/AVU;

.field public final e:LX/3HT;

.field private final f:LX/Abs;

.field private final g:LX/Abu;

.field private final h:LX/Abv;

.field private final i:LX/Abt;

.field private final j:LX/0hc;

.field private final k:Landroid/view/View$OnTouchListener;

.field private final l:Landroid/view/View$OnTouchListener;

.field private final m:LX/Abw;

.field public n:LX/Add;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:LX/3Gs;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private p:Landroid/graphics/drawable/Drawable;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:LX/Ac1;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Landroid/view/GestureDetector;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Landroid/view/View;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:LX/Ade;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private u:D

.field public v:Z

.field public w:Z

.field private x:Z

.field public y:Z

.field public z:I


# direct methods
.method public constructor <init>(LX/AcX;LX/1b4;LX/AgK;LX/AVU;LX/3HT;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1691860
    invoke-direct {p0}, LX/AVi;-><init>()V

    .line 1691861
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    iput-wide v0, p0, LX/Aby;->u:D

    .line 1691862
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Aby;->R:Z

    .line 1691863
    iput-object p1, p0, LX/Aby;->a:LX/AcX;

    .line 1691864
    iput-object p2, p0, LX/Aby;->b:LX/1b4;

    .line 1691865
    iput-object p3, p0, LX/Aby;->c:LX/AgK;

    .line 1691866
    iput-object p4, p0, LX/Aby;->d:LX/AVU;

    .line 1691867
    iput-object p5, p0, LX/Aby;->e:LX/3HT;

    .line 1691868
    new-instance v0, LX/Abs;

    invoke-direct {v0, p0}, LX/Abs;-><init>(LX/Aby;)V

    iput-object v0, p0, LX/Aby;->f:LX/Abs;

    .line 1691869
    new-instance v0, LX/Abu;

    invoke-direct {v0, p0}, LX/Abu;-><init>(LX/Aby;)V

    iput-object v0, p0, LX/Aby;->g:LX/Abu;

    .line 1691870
    new-instance v0, LX/Abt;

    invoke-direct {v0, p0}, LX/Abt;-><init>(LX/Aby;)V

    iput-object v0, p0, LX/Aby;->i:LX/Abt;

    .line 1691871
    new-instance v0, LX/Abv;

    invoke-direct {v0, p0}, LX/Abv;-><init>(LX/Aby;)V

    iput-object v0, p0, LX/Aby;->h:LX/Abv;

    .line 1691872
    new-instance v0, LX/Abp;

    invoke-direct {v0, p0}, LX/Abp;-><init>(LX/Aby;)V

    iput-object v0, p0, LX/Aby;->j:LX/0hc;

    .line 1691873
    new-instance v0, LX/Abq;

    invoke-direct {v0, p0}, LX/Abq;-><init>(LX/Aby;)V

    iput-object v0, p0, LX/Aby;->l:Landroid/view/View$OnTouchListener;

    .line 1691874
    new-instance v0, LX/Abr;

    invoke-direct {v0, p0}, LX/Abr;-><init>(LX/Aby;)V

    iput-object v0, p0, LX/Aby;->k:Landroid/view/View$OnTouchListener;

    .line 1691875
    iget-object v0, p0, LX/Aby;->a:LX/AcX;

    iget-object v1, p0, LX/Aby;->k:Landroid/view/View$OnTouchListener;

    .line 1691876
    iput-object v1, v0, LX/AcX;->y:Landroid/view/View$OnTouchListener;

    .line 1691877
    new-instance v0, LX/Abw;

    invoke-direct {v0, p0}, LX/Abw;-><init>(LX/Aby;)V

    iput-object v0, p0, LX/Aby;->m:LX/Abw;

    .line 1691878
    return-void
.end method

.method public static b(LX/0QB;)LX/Aby;
    .locals 6

    .prologue
    .line 1691879
    new-instance v0, LX/Aby;

    invoke-static {p0}, LX/AcX;->b(LX/0QB;)LX/AcX;

    move-result-object v1

    check-cast v1, LX/AcX;

    invoke-static {p0}, LX/1b4;->b(LX/0QB;)LX/1b4;

    move-result-object v2

    check-cast v2, LX/1b4;

    invoke-static {p0}, LX/AgK;->a(LX/0QB;)LX/AgK;

    move-result-object v3

    check-cast v3, LX/AgK;

    invoke-static {p0}, LX/AVU;->a(LX/0QB;)LX/AVU;

    move-result-object v4

    check-cast v4, LX/AVU;

    invoke-static {p0}, LX/3HT;->a(LX/0QB;)LX/3HT;

    move-result-object v5

    check-cast v5, LX/3HT;

    invoke-direct/range {v0 .. v5}, LX/Aby;-><init>(LX/AcX;LX/1b4;LX/AgK;LX/AVU;LX/3HT;)V

    .line 1691880
    return-object v0
.end method

.method public static b(LX/Aby;F)V
    .locals 2

    .prologue
    .line 1691881
    iget-object v0, p0, LX/Aby;->p:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 1691882
    iget-object v0, p0, LX/Aby;->p:Landroid/graphics/drawable/Drawable;

    const/high16 v1, 0x437f0000    # 255.0f

    mul-float/2addr v1, p1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 1691883
    :cond_0
    return-void
.end method

.method private b(Lcom/facebook/facecastdisplay/FacecastInteractionView;)V
    .locals 2

    .prologue
    .line 1691884
    iput-object p0, p1, Lcom/facebook/facecastdisplay/FacecastInteractionView;->l:LX/Abx;

    .line 1691885
    iget-object v0, p0, LX/Aby;->l:Landroid/view/View$OnTouchListener;

    invoke-virtual {p1, v0}, Lcom/facebook/facecastdisplay/FacecastInteractionView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1691886
    iget-object v0, p1, Lcom/facebook/facecastdisplay/FacecastInteractionView;->b:Lcom/facebook/widget/CustomViewPager;

    iget-object v1, p0, LX/Aby;->j:LX/0hc;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(LX/0hc;)V

    .line 1691887
    iget-object v0, p1, Lcom/facebook/facecastdisplay/FacecastInteractionView;->b:Lcom/facebook/widget/CustomViewPager;

    iget-object v1, p0, LX/Aby;->k:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomViewPager;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1691888
    iget-object v0, p1, Lcom/facebook/facecastdisplay/FacecastInteractionView;->f:Landroid/graphics/drawable/Drawable;

    iput-object v0, p0, LX/Aby;->p:Landroid/graphics/drawable/Drawable;

    .line 1691889
    return-void
.end method

.method private static c(Lcom/facebook/facecastdisplay/FacecastInteractionView;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1691890
    iput-object v1, p0, Lcom/facebook/facecastdisplay/FacecastInteractionView;->l:LX/Abx;

    .line 1691891
    invoke-virtual {p0, v1}, Lcom/facebook/facecastdisplay/FacecastInteractionView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1691892
    iget-object v0, p0, Lcom/facebook/facecastdisplay/FacecastInteractionView;->b:Lcom/facebook/widget/CustomViewPager;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(LX/0hc;)V

    .line 1691893
    iget-object v0, p0, Lcom/facebook/facecastdisplay/FacecastInteractionView;->b:Lcom/facebook/widget/CustomViewPager;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomViewPager;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1691894
    return-void
.end method

.method public static d(LX/Aby;I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1691771
    iget v0, p0, LX/Aby;->z:I

    if-ne p1, v0, :cond_0

    .line 1691772
    const-string v0, "quiet_mode"

    .line 1691773
    :goto_0
    return-object v0

    .line 1691774
    :cond_0
    iget v0, p0, LX/Aby;->A:I

    if-ne p1, v0, :cond_1

    .line 1691775
    const-string v0, "ticker_view"

    goto :goto_0

    .line 1691776
    :cond_1
    iget v0, p0, LX/Aby;->B:I

    if-ne p1, v0, :cond_2

    .line 1691777
    const-string v0, "whos_watching"

    goto :goto_0

    .line 1691778
    :cond_2
    const-string v0, "none"

    goto :goto_0
.end method

.method public static j(LX/Aby;Z)V
    .locals 0

    .prologue
    .line 1691895
    iput-boolean p1, p0, LX/Aby;->N:Z

    .line 1691896
    invoke-direct {p0}, LX/Aby;->n()V

    .line 1691897
    return-void
.end method

.method public static k(LX/Aby;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 1691930
    iput v2, p0, LX/Aby;->z:I

    .line 1691931
    iput v4, p0, LX/Aby;->A:I

    .line 1691932
    const/4 v0, -0x1

    iput v0, p0, LX/Aby;->B:I

    .line 1691933
    iget-object v0, p0, LX/Aby;->n:LX/Add;

    if-nez v0, :cond_0

    .line 1691934
    new-instance v0, LX/Add;

    invoke-direct {v0, v3}, LX/Add;-><init>(F)V

    iput-object v0, p0, LX/Aby;->n:LX/Add;

    .line 1691935
    :cond_0
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1691936
    check-cast v0, Lcom/facebook/facecastdisplay/FacecastInteractionView;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/FacecastInteractionView;->a()V

    .line 1691937
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1691938
    check-cast v0, Lcom/facebook/facecastdisplay/FacecastInteractionView;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/FacecastInteractionView;->b()V

    .line 1691939
    iget-boolean v0, p0, LX/Aby;->y:Z

    if-eqz v0, :cond_1

    .line 1691940
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1691941
    check-cast v0, Lcom/facebook/facecastdisplay/FacecastInteractionView;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/FacecastInteractionView;->e()V

    .line 1691942
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1691943
    check-cast v0, Lcom/facebook/facecastdisplay/FacecastInteractionView;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/FacecastInteractionView;->h:Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingContainer;

    invoke-static {v0}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingContainer;

    iget-boolean v1, p0, LX/Aby;->v:Z

    invoke-virtual {v0, v1}, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingContainer;->setFullScreenMode(Z)V

    .line 1691944
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1691945
    check-cast v0, Lcom/facebook/facecastdisplay/FacecastInteractionView;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/FacecastInteractionView;->i:Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;

    invoke-static {v0}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;

    invoke-virtual {v0, v2}, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;->setIsShowing(Z)V

    .line 1691946
    iget v0, p0, LX/Aby;->A:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/Aby;->B:I

    .line 1691947
    iget-boolean v0, p0, LX/Aby;->v:Z

    if-nez v0, :cond_1

    iget-object v0, p0, LX/Aby;->q:LX/Ac1;

    if-nez v0, :cond_1

    .line 1691948
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1691949
    check-cast v0, Lcom/facebook/facecastdisplay/FacecastInteractionView;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/FacecastInteractionView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, LX/Ac1;->a(Landroid/content/res/Resources;)LX/Ac1;

    move-result-object v0

    iput-object v0, p0, LX/Aby;->q:LX/Ac1;

    .line 1691950
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1691951
    check-cast v0, Lcom/facebook/facecastdisplay/FacecastInteractionView;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/FacecastInteractionView;->c:Landroid/view/View;

    iget-object v1, p0, LX/Aby;->q:LX/Ac1;

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1691952
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1691953
    check-cast v0, Lcom/facebook/facecastdisplay/FacecastInteractionView;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/FacecastInteractionView;->c:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1691954
    iget-object v0, p0, LX/Aby;->q:LX/Ac1;

    invoke-virtual {v0, v3}, LX/Ac1;->a(F)V

    .line 1691955
    :cond_1
    iput-boolean v4, p0, LX/Aby;->I:Z

    .line 1691956
    iget-object v0, p0, LX/Aby;->c:LX/AgK;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, LX/AgK;->a(F)V

    .line 1691957
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1691958
    check-cast v0, Lcom/facebook/facecastdisplay/FacecastInteractionView;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/FacecastInteractionView;->b:Lcom/facebook/widget/CustomViewPager;

    iget v1, p0, LX/Aby;->A:I

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 1691959
    iget v0, p0, LX/Aby;->A:I

    iput v0, p0, LX/Aby;->C:I

    .line 1691960
    return-void
.end method

.method public static l(LX/Aby;)V
    .locals 3

    .prologue
    .line 1691898
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1691899
    check-cast v0, Lcom/facebook/facecastdisplay/FacecastInteractionView;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/FacecastInteractionView;->b:Lcom/facebook/widget/CustomViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    iget v1, p0, LX/Aby;->A:I

    if-eq v0, v1, :cond_0

    .line 1691900
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1691901
    check-cast v0, Lcom/facebook/facecastdisplay/FacecastInteractionView;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/FacecastInteractionView;->b:Lcom/facebook/widget/CustomViewPager;

    iget v1, p0, LX/Aby;->A:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/view/ViewPager;->a(IZ)V

    .line 1691902
    :cond_0
    return-void
.end method

.method private n()V
    .locals 3

    .prologue
    .line 1691903
    iget v0, p0, LX/Aby;->B:I

    if-ltz v0, :cond_0

    .line 1691904
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1691905
    check-cast v0, Lcom/facebook/facecastdisplay/FacecastInteractionView;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/FacecastInteractionView;->h:Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingContainer;

    if-eqz v0, :cond_0

    .line 1691906
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1691907
    check-cast v0, Lcom/facebook/facecastdisplay/FacecastInteractionView;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/FacecastInteractionView;->h:Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingContainer;

    iget-boolean v1, p0, LX/Aby;->N:Z

    invoke-virtual {v0, v1}, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingContainer;->setIsAudioLive(Z)V

    .line 1691908
    :cond_0
    iget-boolean v0, p0, LX/Aby;->P:Z

    if-nez v0, :cond_1

    iget-object v0, p0, LX/Aby;->b:LX/1b4;

    invoke-virtual {v0}, LX/1b4;->w()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1691909
    :goto_0
    return-void

    .line 1691910
    :cond_1
    iget-object v1, p0, LX/Aby;->a:LX/AcX;

    iget-boolean v0, p0, LX/Aby;->N:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, LX/Aby;->x:Z

    if-nez v0, :cond_2

    const/4 v0, 0x1

    .line 1691911
    :goto_1
    iget-object v2, v1, LX/AVi;->a:Landroid/view/View;

    move-object v2, v2

    .line 1691912
    check-cast v2, Lcom/facebook/facecastdisplay/LiveEventsTickerView;

    iget-object p0, v2, Lcom/facebook/facecastdisplay/LiveEventsTickerView;->h:Landroid/view/View;

    if-eqz v0, :cond_3

    const/4 v2, 0x0

    :goto_2
    invoke-virtual {p0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1691913
    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 1691914
    :cond_3
    const/16 v2, 0x8

    goto :goto_2
.end method


# virtual methods
.method public final a(II)I
    .locals 4

    .prologue
    .line 1691915
    iget-boolean v0, p0, LX/Aby;->v:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/Aby;->x:Z

    if-eqz v0, :cond_0

    .line 1691916
    iget v0, p0, LX/Aby;->K:I

    .line 1691917
    :goto_0
    return v0

    :cond_0
    int-to-double v0, p1

    iget-wide v2, p0, LX/Aby;->u:D

    div-double/2addr v0, v2

    double-to-int v0, v0

    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    sub-int v0, p2, v0

    goto :goto_0
.end method

.method public final a(D)V
    .locals 3

    .prologue
    .line 1691918
    iget-wide v0, p0, LX/Aby;->u:D

    cmpl-double v0, v0, p1

    if-nez v0, :cond_0

    .line 1691919
    :goto_0
    return-void

    .line 1691920
    :cond_0
    iput-wide p1, p0, LX/Aby;->u:D

    .line 1691921
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1691922
    check-cast v0, Lcom/facebook/facecastdisplay/FacecastInteractionView;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/FacecastInteractionView;->requestLayout()V

    goto :goto_0
.end method

.method public final a(F)V
    .locals 1

    .prologue
    .line 1691923
    iget-object v0, p0, LX/Aby;->a:LX/AcX;

    .line 1691924
    iput p1, v0, LX/AcX;->B:F

    .line 1691925
    iget-object p0, v0, LX/AcX;->r:LX/AeJ;

    if-eqz p0, :cond_0

    .line 1691926
    iget-object p0, v0, LX/AcX;->r:LX/AeJ;

    invoke-interface {p0, p1}, LX/AeJ;->a(F)V

    .line 1691927
    :cond_0
    iget-object p0, v0, LX/AcX;->l:LX/AfJ;

    .line 1691928
    iput p1, p0, LX/AfJ;->h:F

    .line 1691929
    return-void
.end method

.method public final a(I)V
    .locals 0

    .prologue
    .line 1691844
    iput p1, p0, LX/Aby;->Q:I

    .line 1691845
    return-void
.end method

.method public final a(Landroid/view/View;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1691846
    check-cast p1, Lcom/facebook/facecastdisplay/FacecastInteractionView;

    check-cast p2, Lcom/facebook/facecastdisplay/FacecastInteractionView;

    .line 1691847
    invoke-static {p2}, LX/Aby;->c(Lcom/facebook/facecastdisplay/FacecastInteractionView;)V

    .line 1691848
    invoke-direct {p0, p1}, LX/Aby;->b(Lcom/facebook/facecastdisplay/FacecastInteractionView;)V

    .line 1691849
    invoke-virtual {p2}, Lcom/facebook/facecastdisplay/FacecastInteractionView;->getVisibility()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/facebook/facecastdisplay/FacecastInteractionView;->setVisibility(I)V

    .line 1691850
    iget v0, p0, LX/Aby;->z:I

    if-ltz v0, :cond_0

    .line 1691851
    invoke-virtual {p1}, Lcom/facebook/facecastdisplay/FacecastInteractionView;->a()V

    .line 1691852
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/facecastdisplay/FacecastInteractionView;->b()V

    .line 1691853
    iget v0, p0, LX/Aby;->B:I

    if-ltz v0, :cond_1

    .line 1691854
    invoke-virtual {p1}, Lcom/facebook/facecastdisplay/FacecastInteractionView;->e()V

    .line 1691855
    :cond_1
    iget-object v0, p1, Lcom/facebook/facecastdisplay/FacecastInteractionView;->b:Lcom/facebook/widget/CustomViewPager;

    iget v1, p0, LX/Aby;->C:I

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 1691856
    iget v0, p0, LX/Aby;->C:I

    iget v1, p0, LX/Aby;->z:I

    if-ne v0, v1, :cond_2

    const/4 v0, 0x0

    :goto_0
    invoke-static {p0, v0}, LX/Aby;->b(LX/Aby;F)V

    .line 1691857
    iget-object v0, p0, LX/Aby;->a:LX/AcX;

    iget-object v1, p1, Lcom/facebook/facecastdisplay/FacecastInteractionView;->e:Lcom/facebook/facecastdisplay/LiveEventsTickerView;

    invoke-virtual {v0, v1}, LX/AVi;->a(Landroid/view/View;)V

    .line 1691858
    return-void

    .line 1691859
    :cond_2
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0
.end method

.method public final a(Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;)V
    .locals 2
    .param p1    # Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1691625
    iget-object v0, p0, LX/Aby;->a:LX/AcX;

    .line 1691626
    iget-object v1, v0, LX/AcX;->m:Lcom/facebook/facecastdisplay/donation/LiveDonationController;

    .line 1691627
    const/4 p0, 0x1

    iput-boolean p0, v1, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->g:Z

    .line 1691628
    if-nez p1, :cond_0

    .line 1691629
    const/4 p0, 0x0

    iput-object p0, v1, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->l:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    .line 1691630
    iget-object p0, v1, LX/AVi;->a:Landroid/view/View;

    move-object p0, p0

    .line 1691631
    check-cast p0, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;

    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;->setVisibility(I)V

    .line 1691632
    :goto_0
    return-void

    .line 1691633
    :cond_0
    iput-object p1, v1, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->l:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    .line 1691634
    invoke-static {v1}, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->j(Lcom/facebook/facecastdisplay/donation/LiveDonationController;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;ZZ)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Ljava/lang/String;",
            "ZZ)V"
        }
    .end annotation

    .prologue
    .line 1691635
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Aby;->P:Z

    .line 1691636
    invoke-static {p0}, LX/Aby;->k(LX/Aby;)V

    .line 1691637
    iget-object v0, p0, LX/Aby;->a:LX/AcX;

    .line 1691638
    iget-object v1, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 1691639
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    iput-object v1, v0, LX/AcX;->s:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1691640
    iget-object v1, v0, LX/AcX;->s:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v1}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    .line 1691641
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->ao()Z

    move-result v1

    if-eqz v1, :cond_5

    sget-object v1, LX/Abz;->WATCHING_LIVE:LX/Abz;

    :goto_0
    iput-object v1, v0, LX/AcX;->x:LX/Abz;

    .line 1691642
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/AcX;->t:Ljava/lang/String;

    .line 1691643
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->bQ()Z

    move-result v3

    .line 1691644
    iget-object v1, v0, LX/AcX;->s:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    if-eqz v1, :cond_6

    iget-object v1, v0, LX/AcX;->s:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedback;->R()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v1

    :goto_1
    iput-object v1, v0, LX/AcX;->u:Lcom/facebook/graphql/model/GraphQLPage;

    .line 1691645
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->ao()Z

    move-result v1

    invoke-static {v0, v1}, LX/AcX;->e(LX/AcX;Z)V

    .line 1691646
    iget-object v1, v0, LX/AcX;->r:LX/AeJ;

    .line 1691647
    iget-object v4, v0, LX/AcX;->u:Lcom/facebook/graphql/model/GraphQLPage;

    if-eqz v4, :cond_7

    .line 1691648
    iget-object v4, v0, LX/AcX;->u:Lcom/facebook/graphql/model/GraphQLPage;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v4

    .line 1691649
    :goto_2
    move-object v4, v4

    .line 1691650
    invoke-interface {v1, v4}, LX/AeJ;->c(Ljava/lang/String;)V

    .line 1691651
    iget-object v1, v0, LX/AcX;->r:LX/AeJ;

    const/4 v4, 0x0

    invoke-interface {v1, v4}, LX/AeJ;->a(Z)V

    .line 1691652
    iget-object v1, v0, LX/AcX;->p:Ljava/lang/String;

    invoke-static {v1}, LX/AeT;->a(Ljava/lang/String;)LX/AeT;

    move-result-object v1

    iget-object v4, v0, LX/AcX;->t:Ljava/lang/String;

    .line 1691653
    iput-object v4, v1, LX/AeT;->e:Ljava/lang/String;

    .line 1691654
    move-object v1, v1

    .line 1691655
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->ao()Z

    move-result v4

    .line 1691656
    iput-boolean v4, v1, LX/AeT;->a:Z

    .line 1691657
    if-eqz v4, :cond_9

    iget-boolean v5, v1, LX/AeT;->b:Z

    if-eqz v5, :cond_9

    const/4 v5, 0x1

    :goto_3
    iput-boolean v5, v1, LX/AeT;->b:Z

    .line 1691658
    move-object v1, v1

    .line 1691659
    iput-boolean v3, v1, LX/AeT;->c:Z

    .line 1691660
    move-object v1, v1

    .line 1691661
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->aK()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    .line 1691662
    if-eqz v2, :cond_0

    .line 1691663
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, LX/AcX;->C:Ljava/lang/String;

    .line 1691664
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v3

    .line 1691665
    iput-object v3, v1, LX/AeT;->f:Ljava/lang/String;

    .line 1691666
    move-object v3, v1

    .line 1691667
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v4

    .line 1691668
    iput-object v4, v3, LX/AeT;->g:Ljava/lang/String;

    .line 1691669
    move-object v3, v3

    .line 1691670
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLActor;->aB()Z

    move-result v2

    .line 1691671
    iput-boolean v2, v3, LX/AeT;->h:Z

    .line 1691672
    :cond_0
    invoke-virtual {v1}, LX/AeT;->a()LX/AeU;

    move-result-object v1

    .line 1691673
    iget-object v2, v0, LX/AcX;->n:LX/AeS;

    .line 1691674
    iput-object v1, v2, LX/AeS;->h:LX/AeU;

    .line 1691675
    iget-object v2, v0, LX/AcX;->s:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 1691676
    iget-object v2, v0, LX/AcX;->r:LX/AeJ;

    iget-object v3, v0, LX/AcX;->s:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, LX/AeJ;->e(Ljava/lang/String;)V

    .line 1691677
    :cond_1
    iget-object v2, v0, LX/AcX;->k:LX/Aev;

    .line 1691678
    iget-object v3, v2, LX/Aev;->d:LX/Af2;

    .line 1691679
    new-instance v4, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    invoke-static {p1}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v5

    const-string p3, "newsfeed_ufi"

    const-string v2, "video_fullscreen_player"

    invoke-direct {v4, v5, p3, v2}, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;-><init>(LX/162;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v4, v3, LX/Af2;->c:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 1691680
    iget-object v2, v0, LX/AcX;->k:LX/Aev;

    invoke-virtual {v1}, LX/AeU;->a()Z

    move-result v3

    .line 1691681
    iput-boolean v3, v2, LX/Aev;->h:Z

    .line 1691682
    iget-object v2, v0, LX/AcX;->j:LX/Aem;

    .line 1691683
    iput-object p1, v2, LX/Aem;->g:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1691684
    iget-object v2, v0, LX/AcX;->d:LX/1b4;

    invoke-virtual {v2}, LX/1b4;->F()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, v0, LX/AcX;->x:LX/Abz;

    sget-object v3, LX/Abz;->BROADCASTING:LX/Abz;

    if-eq v2, v3, :cond_2

    .line 1691685
    iget-object v2, v0, LX/AcX;->j:LX/Aem;

    new-instance v3, LX/AcT;

    invoke-direct {v3, v0}, LX/AcT;-><init>(LX/AcX;)V

    .line 1691686
    iput-object v3, v2, LX/Aem;->f:LX/AcT;

    .line 1691687
    :cond_2
    iget-object v2, v0, LX/AcX;->m:Lcom/facebook/facecastdisplay/donation/LiveDonationController;

    const/4 v4, 0x0

    const/16 p3, 0x8

    .line 1691688
    iget-object v3, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 1691689
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStory;

    iput-object v3, v2, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->k:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1691690
    iget-object v3, v2, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->k:Lcom/facebook/graphql/model/GraphQLStory;

    if-nez v3, :cond_a

    .line 1691691
    iget-object v3, v2, LX/AVi;->a:Landroid/view/View;

    move-object v3, v3

    .line 1691692
    check-cast v3, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;

    invoke-virtual {v3, p3}, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;->setVisibility(I)V

    .line 1691693
    :goto_4
    iget-object v2, v0, LX/AcX;->d:LX/1b4;

    invoke-virtual {v2}, LX/1b4;->C()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1691694
    invoke-static {v0, v1}, LX/AcX;->a(LX/AcX;LX/AeU;)V

    .line 1691695
    :cond_3
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1691696
    check-cast v0, Lcom/facebook/facecastdisplay/FacecastInteractionView;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/FacecastInteractionView;->i:Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;

    if-eqz v0, :cond_4

    if-eqz p2, :cond_4

    .line 1691697
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1691698
    check-cast v0, Lcom/facebook/facecastdisplay/FacecastInteractionView;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/FacecastInteractionView;->i:Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;

    invoke-virtual {v0, p2}, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;->setVideoId(Ljava/lang/String;)V

    .line 1691699
    :cond_4
    invoke-static {p0, p4}, LX/Aby;->j(LX/Aby;Z)V

    .line 1691700
    return-void

    .line 1691701
    :cond_5
    sget-object v1, LX/Abz;->WATCHING_VOD:LX/Abz;

    goto/16 :goto_0

    .line 1691702
    :cond_6
    const/4 v1, 0x0

    goto/16 :goto_1

    .line 1691703
    :cond_7
    iget-object v4, v0, LX/AcX;->g:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/20i;

    invoke-virtual {v4}, LX/20i;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v4

    .line 1691704
    if-nez v4, :cond_8

    const/4 v4, 0x0

    goto/16 :goto_2

    :cond_8
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_2

    .line 1691705
    :cond_9
    const/4 v5, 0x0

    goto/16 :goto_3

    .line 1691706
    :cond_a
    iget-object v3, v2, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->k:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v3}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v3

    .line 1691707
    if-eqz v3, :cond_b

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v5

    if-nez v5, :cond_c

    .line 1691708
    :cond_b
    iget-object v3, v2, LX/AVi;->a:Landroid/view/View;

    move-object v3, v3

    .line 1691709
    check-cast v3, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;

    invoke-virtual {v3, p3}, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;->setVisibility(I)V

    goto :goto_4

    .line 1691710
    :cond_c
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v5

    .line 1691711
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLMedia;->ao()Z

    move-result v3

    if-nez v3, :cond_d

    const/4 v3, 0x1

    :goto_5
    iput-boolean v3, v2, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->h:Z

    .line 1691712
    iput-boolean v4, v2, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->g:Z

    .line 1691713
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->n:Ljava/lang/String;

    .line 1691714
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLMedia;->aK()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v3

    if-nez v3, :cond_e

    const/4 v3, 0x0

    :goto_6
    iput-object v3, v2, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->o:Ljava/lang/String;

    .line 1691715
    iget-object v3, v2, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->b:LX/1b4;

    invoke-virtual {v3}, LX/1b4;->j()Z

    move-result v3

    if-eqz v3, :cond_f

    .line 1691716
    iget-object v3, v2, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->c:LX/Acy;

    iget-object v4, v2, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->n:Ljava/lang/String;

    invoke-virtual {v3, v2, v4}, LX/Acy;->a(LX/Acx;Ljava/lang/String;)V

    goto/16 :goto_4

    :cond_d
    move v3, v4

    .line 1691717
    goto :goto_5

    .line 1691718
    :cond_e
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLMedia;->aK()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v3

    goto :goto_6

    .line 1691719
    :cond_f
    iget-object v3, v2, LX/AVi;->a:Landroid/view/View;

    move-object v3, v3

    .line 1691720
    check-cast v3, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;

    invoke-virtual {v3, p3}, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;->setVisibility(I)V

    goto/16 :goto_4
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 1691721
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1691722
    check-cast v0, Lcom/facebook/facecastdisplay/FacecastInteractionView;

    invoke-virtual {v0, p1}, Lcom/facebook/facecastdisplay/FacecastInteractionView;->setVisibility(I)V

    .line 1691723
    return-void
.end method

.method public final b(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 1691724
    check-cast p1, Lcom/facebook/facecastdisplay/FacecastInteractionView;

    .line 1691725
    invoke-direct {p0, p1}, LX/Aby;->b(Lcom/facebook/facecastdisplay/FacecastInteractionView;)V

    .line 1691726
    iget-object v0, p0, LX/Aby;->r:Landroid/view/GestureDetector;

    if-nez v0, :cond_0

    .line 1691727
    new-instance v0, Landroid/view/GestureDetector;

    invoke-virtual {p1}, Lcom/facebook/facecastdisplay/FacecastInteractionView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, LX/Aby;->m:LX/Abw;

    invoke-direct {v0, v1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, LX/Aby;->r:Landroid/view/GestureDetector;

    .line 1691728
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/facecastdisplay/FacecastInteractionView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b04c7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/Aby;->J:I

    .line 1691729
    invoke-virtual {p1}, Lcom/facebook/facecastdisplay/FacecastInteractionView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b04c8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/Aby;->K:I

    .line 1691730
    iget-object v0, p0, LX/Aby;->a:LX/AcX;

    iget-object v1, p1, Lcom/facebook/facecastdisplay/FacecastInteractionView;->e:Lcom/facebook/facecastdisplay/LiveEventsTickerView;

    invoke-virtual {v0, v1}, LX/AVi;->a(Landroid/view/View;)V

    .line 1691731
    return-void
.end method

.method public final b(Z)V
    .locals 2

    .prologue
    .line 1691732
    iput-boolean p1, p0, LX/Aby;->v:Z

    .line 1691733
    iget-object v0, p0, LX/Aby;->a:LX/AcX;

    const/16 p0, 0x8

    .line 1691734
    iput-boolean p1, v0, LX/AcX;->z:Z

    .line 1691735
    iget-object v1, v0, LX/AVi;->a:Landroid/view/View;

    move-object v1, v1

    .line 1691736
    check-cast v1, Lcom/facebook/facecastdisplay/LiveEventsTickerView;

    iget-object v1, v1, Lcom/facebook/facecastdisplay/LiveEventsTickerView;->a:Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;

    invoke-virtual {v1, p1}, Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;->setFullScreen(Z)V

    .line 1691737
    if-eqz p1, :cond_0

    .line 1691738
    iget-object v1, v0, LX/AVi;->a:Landroid/view/View;

    move-object v1, v1

    .line 1691739
    check-cast v1, Lcom/facebook/facecastdisplay/LiveEventsTickerView;

    iget-object v1, v1, Lcom/facebook/facecastdisplay/LiveEventsTickerView;->c:Landroid/view/View;

    invoke-virtual {v1, p0}, Landroid/view/View;->setVisibility(I)V

    .line 1691740
    iget-object v1, v0, LX/AVi;->a:Landroid/view/View;

    move-object v1, v1

    .line 1691741
    check-cast v1, Lcom/facebook/facecastdisplay/LiveEventsTickerView;

    iget-object v1, v1, Lcom/facebook/facecastdisplay/LiveEventsTickerView;->b:Landroid/view/View;

    invoke-virtual {v1, p0}, Landroid/view/View;->setVisibility(I)V

    .line 1691742
    :goto_0
    return-void

    .line 1691743
    :cond_0
    iget-object v1, v0, LX/AVi;->a:Landroid/view/View;

    move-object v1, v1

    .line 1691744
    check-cast v1, Lcom/facebook/facecastdisplay/LiveEventsTickerView;

    iget-object v1, v1, Lcom/facebook/facecastdisplay/LiveEventsTickerView;->c:Landroid/view/View;

    const/4 p0, 0x0

    invoke-virtual {v1, p0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1691745
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1691746
    check-cast v0, Lcom/facebook/facecastdisplay/FacecastInteractionView;

    invoke-static {v0}, LX/Aby;->c(Lcom/facebook/facecastdisplay/FacecastInteractionView;)V

    .line 1691747
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-static {p0, v0}, LX/Aby;->b(LX/Aby;F)V

    .line 1691748
    iget-object v0, p0, LX/Aby;->a:LX/AcX;

    invoke-virtual {v0}, LX/AVi;->b()V

    .line 1691749
    return-void
.end method

.method public final c(I)V
    .locals 2

    .prologue
    .line 1691750
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1691751
    check-cast v0, Lcom/facebook/facecastdisplay/FacecastInteractionView;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/FacecastInteractionView;->b:Lcom/facebook/widget/CustomViewPager;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Landroid/support/v4/view/ViewPager;->a(IZ)V

    .line 1691752
    return-void
.end method

.method public final c(Z)V
    .locals 2

    .prologue
    .line 1691753
    iput-boolean p1, p0, LX/Aby;->x:Z

    .line 1691754
    iget-boolean v0, p0, LX/Aby;->x:Z

    if-eqz v0, :cond_1

    .line 1691755
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1691756
    check-cast v0, Lcom/facebook/facecastdisplay/FacecastInteractionView;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/FacecastInteractionView;->d:Lcom/facebook/facecastdisplay/FacecastPercentFrameLayout;

    const/high16 v1, 0x3f000000    # 0.5f

    invoke-virtual {v0, v1}, Lcom/facebook/facecastdisplay/FacecastPercentFrameLayout;->setPercent(F)V

    .line 1691757
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1691758
    check-cast v0, Lcom/facebook/facecastdisplay/FacecastInteractionView;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/FacecastInteractionView;->d:Lcom/facebook/facecastdisplay/FacecastPercentFrameLayout;

    iget v1, p0, LX/Aby;->J:I

    invoke-virtual {v0, v1}, Lcom/facebook/facecastdisplay/FacecastPercentFrameLayout;->setExtraWidth(I)V

    .line 1691759
    :goto_0
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1691760
    check-cast v0, Lcom/facebook/facecastdisplay/FacecastInteractionView;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/FacecastInteractionView;->i:Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;

    if-eqz v0, :cond_0

    .line 1691761
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1691762
    check-cast v0, Lcom/facebook/facecastdisplay/FacecastInteractionView;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/FacecastInteractionView;->i:Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;

    invoke-virtual {v0, p1}, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;->setLandscape(Z)V

    .line 1691763
    :cond_0
    invoke-direct {p0}, LX/Aby;->n()V

    .line 1691764
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1691765
    check-cast v0, Lcom/facebook/facecastdisplay/FacecastInteractionView;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/FacecastInteractionView;->requestLayout()V

    .line 1691766
    return-void

    .line 1691767
    :cond_1
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1691768
    check-cast v0, Lcom/facebook/facecastdisplay/FacecastInteractionView;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/FacecastInteractionView;->d:Lcom/facebook/facecastdisplay/FacecastPercentFrameLayout;

    const/high16 v1, -0x40800000    # -1.0f

    invoke-virtual {v0, v1}, Lcom/facebook/facecastdisplay/FacecastPercentFrameLayout;->setPercent(F)V

    .line 1691769
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1691770
    check-cast v0, Lcom/facebook/facecastdisplay/FacecastInteractionView;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/FacecastInteractionView;->d:Lcom/facebook/facecastdisplay/FacecastPercentFrameLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/facecastdisplay/FacecastPercentFrameLayout;->setExtraWidth(I)V

    goto :goto_0
.end method

.method public final d()V
    .locals 4

    .prologue
    .line 1691779
    iget-object v0, p0, LX/Aby;->b:LX/1b4;

    .line 1691780
    iget-object v1, v0, LX/1b4;->b:LX/0Uh;

    const/16 v2, 0x66c

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    move v0, v1

    .line 1691781
    if-eqz v0, :cond_0

    .line 1691782
    iget-object v0, p0, LX/Aby;->a:LX/AcX;

    .line 1691783
    iget-object v1, v0, LX/AcX;->n:LX/AeS;

    new-instance v2, LX/AfR;

    sget-object v3, LX/AfQ;->LIVE_COMMERCIAL_BREAK_VIOLATION:LX/AfQ;

    invoke-direct {v2, v3}, LX/AfR;-><init>(LX/AfQ;)V

    invoke-virtual {v1, v2}, LX/AeS;->a(LX/AeO;)V

    .line 1691784
    :cond_0
    return-void
.end method

.method public final d(Z)V
    .locals 2

    .prologue
    .line 1691785
    iget-boolean v0, p0, LX/Aby;->M:Z

    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    iget-object v0, p0, LX/Aby;->n:LX/Add;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/Aby;->R:Z

    if-nez v0, :cond_0

    .line 1691786
    iget-object v0, p0, LX/Aby;->e:LX/3HT;

    iget-object v1, p0, LX/Aby;->n:LX/Add;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1691787
    :cond_0
    iput-boolean p1, p0, LX/Aby;->M:Z

    .line 1691788
    return-void
.end method

.method public final e()V
    .locals 3

    .prologue
    .line 1691789
    iget-object v0, p0, LX/Aby;->a:LX/AcX;

    .line 1691790
    iget-object v1, v0, LX/AcX;->r:LX/AeJ;

    invoke-static {v1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1691791
    iget-object v1, v0, LX/AcX;->r:LX/AeJ;

    iget-object v2, v0, LX/AcX;->t:Ljava/lang/String;

    invoke-interface {v1, v2}, LX/AeJ;->a(Ljava/lang/String;)V

    .line 1691792
    iget-object v1, v0, LX/AcX;->d:LX/1b4;

    invoke-virtual {v1}, LX/1b4;->F()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/AcX;->x:LX/Abz;

    sget-object v2, LX/Abz;->BROADCASTING:LX/Abz;

    if-eq v1, v2, :cond_0

    .line 1691793
    iget-object v1, v0, LX/AcX;->c:LX/3HT;

    iget-object v2, v0, LX/AcX;->a:LX/AcV;

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b2;)Z

    .line 1691794
    :cond_0
    iget-object v1, v0, LX/AcX;->l:LX/AfJ;

    invoke-virtual {v1}, LX/AfJ;->d()V

    .line 1691795
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1691796
    check-cast v0, Lcom/facebook/facecastdisplay/FacecastInteractionView;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/FacecastInteractionView;->i:Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;

    if-eqz v0, :cond_1

    .line 1691797
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1691798
    check-cast v0, Lcom/facebook/facecastdisplay/FacecastInteractionView;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/FacecastInteractionView;->i:Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;->l()V

    .line 1691799
    iget-object v0, p0, LX/Aby;->e:LX/3HT;

    iget-object v1, p0, LX/Aby;->f:LX/Abs;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 1691800
    :cond_1
    iget-object v0, p0, LX/Aby;->e:LX/3HT;

    iget-object v1, p0, LX/Aby;->g:LX/Abu;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 1691801
    iget-object v0, p0, LX/Aby;->e:LX/3HT;

    iget-object v1, p0, LX/Aby;->i:LX/Abt;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 1691802
    iget-object v0, p0, LX/Aby;->e:LX/3HT;

    iget-object v1, p0, LX/Aby;->h:LX/Abv;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 1691803
    return-void
.end method

.method public final f()V
    .locals 3

    .prologue
    .line 1691804
    iget-object v0, p0, LX/Aby;->a:LX/AcX;

    .line 1691805
    iget-object v1, v0, LX/AcX;->r:LX/AeJ;

    invoke-static {v1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1691806
    iget-object v1, v0, LX/AcX;->r:LX/AeJ;

    invoke-interface {v1}, LX/AeJ;->a()V

    .line 1691807
    iget-object v1, v0, LX/AcX;->d:LX/1b4;

    invoke-virtual {v1}, LX/1b4;->F()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/AcX;->x:LX/Abz;

    sget-object v2, LX/Abz;->BROADCASTING:LX/Abz;

    if-eq v1, v2, :cond_0

    .line 1691808
    iget-object v1, v0, LX/AcX;->c:LX/3HT;

    iget-object v2, v0, LX/AcX;->a:LX/AcV;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 1691809
    :cond_0
    iget-object v1, v0, LX/AcX;->n:LX/AeS;

    invoke-virtual {v1}, LX/AeS;->d()V

    .line 1691810
    iget-object v1, v0, LX/AVi;->a:Landroid/view/View;

    move-object v1, v1

    .line 1691811
    check-cast v1, Lcom/facebook/facecastdisplay/LiveEventsTickerView;

    iget-object v1, v1, Lcom/facebook/facecastdisplay/LiveEventsTickerView;->d:Landroid/view/ViewStub;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/ViewStub;->setVisibility(I)V

    .line 1691812
    const/4 v1, 0x0

    iput-object v1, v0, LX/AcX;->s:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1691813
    iget-object v1, v0, LX/AcX;->l:LX/AfJ;

    .line 1691814
    iget-object v2, v1, LX/AfJ;->g:LX/AfO;

    if-eqz v2, :cond_1

    .line 1691815
    iget-object v2, v1, LX/AfJ;->g:LX/AfO;

    .line 1691816
    iget-object v0, v2, LX/AfO;->c:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 1691817
    iget-boolean v0, v2, LX/AfO;->j:Z

    if-nez v0, :cond_3

    .line 1691818
    :cond_1
    :goto_0
    iget-object v0, p0, LX/Aby;->a:LX/AcX;

    .line 1691819
    iget-object v1, v0, LX/AVi;->a:Landroid/view/View;

    move-object v1, v1

    .line 1691820
    check-cast v1, Lcom/facebook/facecastdisplay/LiveEventsTickerView;

    iget-object v1, v1, Lcom/facebook/facecastdisplay/LiveEventsTickerView;->a:Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;

    invoke-virtual {v1}, Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 1691821
    iget-object v1, v0, LX/AVi;->a:Landroid/view/View;

    move-object v1, v1

    .line 1691822
    check-cast v1, Lcom/facebook/facecastdisplay/LiveEventsTickerView;

    iget-object v1, v1, Lcom/facebook/facecastdisplay/LiveEventsTickerView;->a:Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;->setVisibility(I)V

    .line 1691823
    iget-object v1, v0, LX/AVi;->a:Landroid/view/View;

    move-object v1, v1

    .line 1691824
    check-cast v1, Lcom/facebook/facecastdisplay/LiveEventsTickerView;

    iget-object v1, v1, Lcom/facebook/facecastdisplay/LiveEventsTickerView;->a:Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v1, v2}, Lcom/facebook/facecastdisplay/LiveEventsRecyclerView;->setAlpha(F)V

    .line 1691825
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1691826
    check-cast v0, Lcom/facebook/facecastdisplay/FacecastInteractionView;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/FacecastInteractionView;->i:Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;

    if-eqz v0, :cond_2

    .line 1691827
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1691828
    check-cast v0, Lcom/facebook/facecastdisplay/FacecastInteractionView;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/FacecastInteractionView;->i:Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/whoswatching/LiveWhosWatchingView;->m()V

    .line 1691829
    iget-object v0, p0, LX/Aby;->e:LX/3HT;

    iget-object v1, p0, LX/Aby;->f:LX/Abs;

    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 1691830
    :cond_2
    iget-object v0, p0, LX/Aby;->e:LX/3HT;

    iget-object v1, p0, LX/Aby;->g:LX/Abu;

    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 1691831
    iget-object v0, p0, LX/Aby;->e:LX/3HT;

    iget-object v1, p0, LX/Aby;->i:LX/Abt;

    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 1691832
    iget-object v0, p0, LX/Aby;->e:LX/3HT;

    iget-object v1, p0, LX/Aby;->h:LX/Abv;

    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 1691833
    return-void

    .line 1691834
    :cond_3
    iget-object v0, v2, LX/AfO;->g:LX/0gM;

    if-eqz v0, :cond_4

    .line 1691835
    iget-object v0, v2, LX/AfO;->b:LX/0gX;

    iget-object v1, v2, LX/AfO;->g:LX/0gM;

    invoke-static {v1}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0gX;->a(Ljava/util/Set;)V

    .line 1691836
    const/4 v0, 0x0

    iput-object v0, v2, LX/AfO;->g:LX/0gM;

    .line 1691837
    :cond_4
    const/4 v0, 0x0

    iput-boolean v0, v2, LX/AfO;->j:Z

    goto :goto_0
.end method

.method public final h(Z)V
    .locals 2

    .prologue
    .line 1691838
    iput-boolean p1, p0, LX/Aby;->w:Z

    .line 1691839
    iget-object v0, p0, LX/AVi;->a:Landroid/view/View;

    move-object v0, v0

    .line 1691840
    check-cast v0, Lcom/facebook/facecastdisplay/FacecastInteractionView;

    iget-object v1, v0, Lcom/facebook/facecastdisplay/FacecastInteractionView;->b:Lcom/facebook/widget/CustomViewPager;

    if-nez p1, :cond_0

    const/4 v0, 0x1

    .line 1691841
    :goto_0
    iput-boolean v0, v1, Lcom/facebook/widget/CustomViewPager;->a:Z

    .line 1691842
    return-void

    .line 1691843
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
