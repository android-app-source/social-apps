.class public final LX/CPF;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/CPF;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/CPD;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/CPH;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1884170
    const/4 v0, 0x0

    sput-object v0, LX/CPF;->a:LX/CPF;

    .line 1884171
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/CPF;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1884172
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1884173
    new-instance v0, LX/CPH;

    invoke-direct {v0}, LX/CPH;-><init>()V

    iput-object v0, p0, LX/CPF;->c:LX/CPH;

    .line 1884174
    return-void
.end method

.method public static declared-synchronized q()LX/CPF;
    .locals 2

    .prologue
    .line 1884175
    const-class v1, LX/CPF;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/CPF;->a:LX/CPF;

    if-nez v0, :cond_0

    .line 1884176
    new-instance v0, LX/CPF;

    invoke-direct {v0}, LX/CPF;-><init>()V

    sput-object v0, LX/CPF;->a:LX/CPF;

    .line 1884177
    :cond_0
    sget-object v0, LX/CPF;->a:LX/CPF;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 1884178
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 3

    .prologue
    .line 1884179
    check-cast p2, LX/CPE;

    .line 1884180
    iget-object v0, p2, LX/CPE;->a:LX/CNb;

    iget-object v1, p2, LX/CPE;->b:LX/CNc;

    iget-object v2, p2, LX/CPE;->c:Ljava/util/List;

    invoke-static {p1, v0, v1, v2}, LX/CPH;->a(LX/1De;LX/CNb;LX/CNc;Ljava/util/List;)LX/1Dg;

    move-result-object v0

    .line 1884181
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 1884182
    invoke-static {}, LX/1dS;->b()V

    .line 1884183
    iget v0, p1, LX/1dQ;->b:I

    .line 1884184
    sparse-switch v0, :sswitch_data_0

    move-object v0, v1

    .line 1884185
    :goto_0
    return-object v0

    .line 1884186
    :sswitch_0
    iget-object v0, p1, LX/1dQ;->c:[Ljava/lang/Object;

    aget-object v0, v0, v2

    check-cast v0, LX/CNe;

    .line 1884187
    invoke-interface {v0}, LX/CNe;->a()V

    .line 1884188
    move-object v0, v1

    .line 1884189
    goto :goto_0

    .line 1884190
    :sswitch_1
    iget-object v0, p1, LX/1dQ;->c:[Ljava/lang/Object;

    aget-object v0, v0, v2

    check-cast v0, LX/CNe;

    .line 1884191
    invoke-interface {v0}, LX/CNe;->a()V

    .line 1884192
    const/4 v2, 0x1

    move v2, v2

    .line 1884193
    move v0, v2

    .line 1884194
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x3eccc4be -> :sswitch_1
        0x59c53a21 -> :sswitch_0
    .end sparse-switch
.end method
