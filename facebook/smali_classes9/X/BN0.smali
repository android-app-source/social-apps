.class public LX/BN0;
.super Landroid/widget/BaseAdapter;
.source ""


# instance fields
.field public final a:LX/2Rd;

.field public final b:Landroid/view/LayoutInflater;

.field public final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/BMx;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Landroid/content/Context;

.field public e:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/ipc/model/FacebookProfile;",
            ">;"
        }
    .end annotation
.end field

.field public f:Z


# direct methods
.method public constructor <init>(Landroid/view/LayoutInflater;LX/2Rd;Landroid/content/Context;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1778199
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 1778200
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/BN0;->c:Ljava/util/List;

    .line 1778201
    iput-object p1, p0, LX/BN0;->b:Landroid/view/LayoutInflater;

    .line 1778202
    iput-object p2, p0, LX/BN0;->a:LX/2Rd;

    .line 1778203
    iput-object p3, p0, LX/BN0;->d:Landroid/content/Context;

    .line 1778204
    return-void
.end method

.method public static b(Lcom/facebook/ipc/model/FacebookProfile;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1778205
    iget-object v0, p0, Lcom/facebook/ipc/model/FacebookProfile;->mDisplayName:Ljava/lang/String;

    .line 1778206
    const/4 v1, 0x1

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->offsetByCodePoints(II)I

    move-result v1

    .line 1778207
    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final getCount()I
    .locals 1

    .prologue
    .line 1778197
    iget-object v0, p0, LX/BN0;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1778198
    iget-object v0, p0, LX/BN0;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 1778190
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 1778191
    iget-object v0, p0, LX/BN0;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, LX/BMy;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 1778192
    iget-object v0, p0, LX/BN0;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BMx;

    .line 1778193
    if-nez p2, :cond_0

    invoke-interface {v0}, LX/BMx;->a()Landroid/view/View;

    move-result-object p2

    .line 1778194
    :cond_0
    invoke-interface {v0, p2}, LX/BMx;->a(Landroid/view/View;)V

    .line 1778195
    return-object p2
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 1778196
    const/4 v0, 0x2

    return v0
.end method
