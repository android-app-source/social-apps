.class public LX/Cev;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/49e;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile i:LX/Cev;


# instance fields
.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Gh;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Gh;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Gh;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Gh;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Gh;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/push/registration/FacebookPushServerRegistrar;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Gc;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1925284
    const-class v0, LX/Cev;

    sput-object v0, LX/Cev;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .param p1    # LX/0Ot;
        .annotation runtime Lcom/facebook/push/registration/C2DMService;
        .end annotation
    .end param
    .param p2    # LX/0Ot;
        .annotation runtime Lcom/facebook/push/registration/ADMService;
        .end annotation
    .end param
    .param p3    # LX/0Ot;
        .annotation runtime Lcom/facebook/push/registration/NNAService;
        .end annotation
    .end param
    .param p4    # LX/0Ot;
        .annotation runtime Lcom/facebook/push/registration/FbnsService;
        .end annotation
    .end param
    .param p5    # LX/0Ot;
        .annotation runtime Lcom/facebook/push/registration/FbnsLiteService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/2Gh;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2Gh;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2Gh;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2Gh;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2Gh;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/push/registration/FacebookPushServerRegistrar;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2Gc;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1925309
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1925310
    iput-object p1, p0, LX/Cev;->b:LX/0Ot;

    .line 1925311
    iput-object p2, p0, LX/Cev;->c:LX/0Ot;

    .line 1925312
    iput-object p3, p0, LX/Cev;->d:LX/0Ot;

    .line 1925313
    iput-object p4, p0, LX/Cev;->e:LX/0Ot;

    .line 1925314
    iput-object p5, p0, LX/Cev;->f:LX/0Ot;

    .line 1925315
    iput-object p6, p0, LX/Cev;->g:LX/0Ot;

    .line 1925316
    iput-object p7, p0, LX/Cev;->h:LX/0Ot;

    .line 1925317
    return-void
.end method

.method public static a(LX/0QB;)LX/Cev;
    .locals 11

    .prologue
    .line 1925296
    sget-object v0, LX/Cev;->i:LX/Cev;

    if-nez v0, :cond_1

    .line 1925297
    const-class v1, LX/Cev;

    monitor-enter v1

    .line 1925298
    :try_start_0
    sget-object v0, LX/Cev;->i:LX/Cev;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1925299
    if-eqz v2, :cond_0

    .line 1925300
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1925301
    new-instance v3, LX/Cev;

    const/16 v4, 0xfec

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0xfe8

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x3025

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0xff9

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0xffc

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x1027

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0xff5

    invoke-static {v0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-direct/range {v3 .. v10}, LX/Cev;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 1925302
    move-object v0, v3

    .line 1925303
    sput-object v0, LX/Cev;->i:LX/Cev;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1925304
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1925305
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1925306
    :cond_1
    sget-object v0, LX/Cev;->i:LX/Cev;

    return-object v0

    .line 1925307
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1925308
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/0dI;LX/0dI;LX/49d;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1925285
    iget-object v0, p0, LX/Cev;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Gc;

    sget-object v1, LX/2Ge;->GCM:LX/2Ge;

    invoke-virtual {v0, v1}, LX/2Gc;->a(LX/2Ge;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1925286
    iget-object v0, p0, LX/Cev;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;

    sget-object v2, LX/2Ge;->GCM:LX/2Ge;

    iget-object v1, p0, LX/Cev;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2Gh;

    invoke-interface {v1}, LX/2Gh;->c()LX/2H7;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->a(LX/2Ge;LX/2H7;)V

    .line 1925287
    :cond_0
    iget-object v0, p0, LX/Cev;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Gc;

    sget-object v1, LX/2Ge;->FBNS:LX/2Ge;

    invoke-virtual {v0, v1}, LX/2Gc;->a(LX/2Ge;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1925288
    iget-object v0, p0, LX/Cev;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;

    sget-object v2, LX/2Ge;->FBNS:LX/2Ge;

    iget-object v1, p0, LX/Cev;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2Gh;

    invoke-interface {v1}, LX/2Gh;->c()LX/2H7;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->a(LX/2Ge;LX/2H7;)V

    .line 1925289
    :cond_1
    iget-object v0, p0, LX/Cev;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Gc;

    sget-object v1, LX/2Ge;->ADM:LX/2Ge;

    invoke-virtual {v0, v1}, LX/2Gc;->a(LX/2Ge;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1925290
    iget-object v0, p0, LX/Cev;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;

    sget-object v2, LX/2Ge;->ADM:LX/2Ge;

    iget-object v1, p0, LX/Cev;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2Gh;

    invoke-interface {v1}, LX/2Gh;->c()LX/2H7;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->a(LX/2Ge;LX/2H7;)V

    .line 1925291
    :cond_2
    iget-object v0, p0, LX/Cev;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Gc;

    sget-object v1, LX/2Ge;->NNA:LX/2Ge;

    invoke-virtual {v0, v1}, LX/2Gc;->a(LX/2Ge;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1925292
    iget-object v0, p0, LX/Cev;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;

    sget-object v2, LX/2Ge;->NNA:LX/2Ge;

    iget-object v1, p0, LX/Cev;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2Gh;

    invoke-interface {v1}, LX/2Gh;->c()LX/2H7;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->a(LX/2Ge;LX/2H7;)V

    .line 1925293
    :cond_3
    iget-object v0, p0, LX/Cev;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Gc;

    sget-object v1, LX/2Ge;->FBNS_LITE:LX/2Ge;

    invoke-virtual {v0, v1}, LX/2Gc;->a(LX/2Ge;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1925294
    iget-object v0, p0, LX/Cev;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/push/registration/FacebookPushServerRegistrar;

    sget-object v2, LX/2Ge;->FBNS_LITE:LX/2Ge;

    iget-object v1, p0, LX/Cev;->f:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2Gh;

    invoke-interface {v1}, LX/2Gh;->c()LX/2H7;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/facebook/push/registration/FacebookPushServerRegistrar;->a(LX/2Ge;LX/2H7;)V

    .line 1925295
    :cond_4
    return-void
.end method
