.class public final LX/CKO;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/messaging/business/subscription/common/graphql/BusinessSubscriptionMutationsModels$ContentPageUnsubscribeMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/CKS;

.field public final synthetic b:LX/CKT;


# direct methods
.method public constructor <init>(LX/CKT;LX/CKS;)V
    .locals 0

    .prologue
    .line 1877149
    iput-object p1, p0, LX/CKO;->b:LX/CKT;

    iput-object p2, p0, LX/CKO;->a:LX/CKS;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 1877150
    iget-object v0, p0, LX/CKO;->b:LX/CKT;

    iget-object v0, v0, LX/CKT;->a:LX/03V;

    const-string v1, "BusinessSubscriptionMutationHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unsubscribe page mutation request fails "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1877151
    iget-object v0, p0, LX/CKO;->a:LX/CKS;

    if-eqz v0, :cond_0

    .line 1877152
    iget-object v0, p0, LX/CKO;->a:LX/CKS;

    invoke-interface {v0}, LX/CKS;->b()V

    .line 1877153
    :cond_0
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1877154
    iget-object v0, p0, LX/CKO;->a:LX/CKS;

    if-eqz v0, :cond_0

    .line 1877155
    iget-object v0, p0, LX/CKO;->a:LX/CKS;

    invoke-interface {v0}, LX/CKS;->a()V

    .line 1877156
    :cond_0
    return-void
.end method
