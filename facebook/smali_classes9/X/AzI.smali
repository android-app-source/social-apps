.class public final LX/AzI;
.super LX/1ci;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1ci",
        "<",
        "LX/1FJ",
        "<",
        "Lcom/facebook/imagepipeline/memory/PooledByteBuffer;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotoEditHelper;


# direct methods
.method public constructor <init>(Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotoEditHelper;)V
    .locals 0

    .prologue
    .line 1732368
    iput-object p1, p0, LX/AzI;->a:Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotoEditHelper;

    invoke-direct {p0}, LX/1ci;-><init>()V

    return-void
.end method


# virtual methods
.method public final e(LX/1ca;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "Lcom/facebook/imagepipeline/memory/PooledByteBuffer;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1732369
    invoke-interface {p1}, LX/1ca;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1732370
    :goto_0
    return-void

    .line 1732371
    :cond_0
    iget-object v0, p0, LX/AzI;->a:Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotoEditHelper;

    iget-object v0, v0, Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotoEditHelper;->l:LX/AzQ;

    iget-object v1, p0, LX/AzI;->a:Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotoEditHelper;

    iget-object v1, v1, Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotoEditHelper;->c:Ljava/lang/String;

    .line 1732372
    iget-object v2, v0, LX/AzQ;->a:LX/0Zb;

    sget-object v3, LX/AzP;->DOWNLOAD_SUCCESS:LX/AzP;

    invoke-virtual {v3}, LX/AzP;->name()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, LX/AzQ;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    invoke-interface {v2, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1732373
    iget-object v1, p0, LX/AzI;->a:Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotoEditHelper;

    invoke-interface {p1}, LX/1ca;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1FJ;

    .line 1732374
    new-instance v3, LX/1lZ;

    invoke-virtual {v0}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1FK;

    invoke-direct {v3, v2}, LX/1lZ;-><init>(LX/1FK;)V

    .line 1732375
    invoke-static {v3}, LX/1la;->b(Ljava/io/InputStream;)LX/1lW;

    move-result-object v2

    .line 1732376
    sget-object p0, LX/1ld;->a:LX/1lW;

    if-ne v2, p0, :cond_1

    .line 1732377
    invoke-virtual {v3}, LX/1lZ;->reset()V

    .line 1732378
    iget-object v2, v1, Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotoEditHelper;->f:LX/0TD;

    new-instance p0, LX/AzK;

    invoke-direct {p0, v1, v3, v0}, LX/AzK;-><init>(Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotoEditHelper;LX/1lZ;LX/1FJ;)V

    invoke-interface {v2, p0}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    move-object v2, v2

    .line 1732379
    new-instance p0, LX/AzJ;

    invoke-direct {p0, v1, v3, v0}, LX/AzJ;-><init>(Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotoEditHelper;LX/1lZ;LX/1FJ;)V

    iget-object v3, v1, Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotoEditHelper;->g:Ljava/util/concurrent/ExecutorService;

    invoke-static {v2, p0, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1732380
    :goto_1
    goto :goto_0

    .line 1732381
    :cond_1
    invoke-static {v3}, LX/1md;->a(Ljava/io/InputStream;)V

    .line 1732382
    invoke-static {v0}, LX/1FJ;->c(LX/1FJ;)V

    goto :goto_1
.end method

.method public final f(LX/1ca;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "Lcom/facebook/imagepipeline/memory/PooledByteBuffer;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1732383
    iget-object v0, p0, LX/AzI;->a:Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotoEditHelper;

    iget-object v0, v0, Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotoEditHelper;->l:LX/AzQ;

    iget-object v1, p0, LX/AzI;->a:Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotoEditHelper;

    iget-object v1, v1, Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotoEditHelper;->c:Ljava/lang/String;

    .line 1732384
    iget-object v2, v0, LX/AzQ;->a:LX/0Zb;

    sget-object p1, LX/AzP;->ERROR_FAILED_DOWNLOAD:LX/AzP;

    invoke-virtual {p1}, LX/AzP;->name()Ljava/lang/String;

    move-result-object p1

    invoke-static {v1, p1}, LX/AzQ;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    invoke-interface {v2, p1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1732385
    iget-object v0, p0, LX/AzI;->a:Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotoEditHelper;

    invoke-static {v0}, Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotoEditHelper;->b(Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotoEditHelper;)V

    .line 1732386
    return-void
.end method
