.class public LX/C1v;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/C1t;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/C1x;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1843628
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/C1v;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/C1x;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1843625
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1843626
    iput-object p1, p0, LX/C1v;->b:LX/0Ot;

    .line 1843627
    return-void
.end method

.method public static a(LX/0QB;)LX/C1v;
    .locals 4

    .prologue
    .line 1843614
    const-class v1, LX/C1v;

    monitor-enter v1

    .line 1843615
    :try_start_0
    sget-object v0, LX/C1v;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1843616
    sput-object v2, LX/C1v;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1843617
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1843618
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1843619
    new-instance v3, LX/C1v;

    const/16 p0, 0x1e99

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/C1v;-><init>(LX/0Ot;)V

    .line 1843620
    move-object v0, v3

    .line 1843621
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1843622
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C1v;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1843623
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1843624
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1843612
    invoke-static {}, LX/1dS;->b()V

    .line 1843613
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/1De;LX/1Dg;IILX/1no;LX/1X1;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x2ce0d1bc

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1843605
    iget-object v1, p0, LX/C1v;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    .line 1843606
    new-instance v1, Lcom/facebook/facecastdisplay/LiveVideoStatusView;

    invoke-direct {v1, p1}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;-><init>(Landroid/content/Context;)V

    .line 1843607
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->setIsLiveNow(Z)V

    .line 1843608
    invoke-static {p3}, LX/1mh;->b(I)I

    move-result v2

    const/high16 p0, 0x40000000    # 2.0f

    invoke-static {v2, p0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-static {p4}, LX/1oC;->a(I)I

    move-result p0

    invoke-virtual {v1, v2, p0}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->measure(II)V

    .line 1843609
    invoke-virtual {v1}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->getMeasuredWidth()I

    move-result v2

    iput v2, p5, LX/1no;->a:I

    .line 1843610
    invoke-virtual {v1}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->getMeasuredHeight()I

    move-result v1

    iput v1, p5, LX/1no;->b:I

    .line 1843611
    const/16 v1, 0x1f

    const v2, 0x10b4e4bb

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(LX/1De;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1843629
    iget-object v0, p0, LX/C1v;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    .line 1843630
    new-instance v0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;

    invoke-direct {v0, p1}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;-><init>(Landroid/content/Context;)V

    move-object v0, v0

    .line 1843631
    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1843604
    const/4 v0, 0x1

    return v0
.end method

.method public final e(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 1

    .prologue
    .line 1843598
    check-cast p3, LX/C1u;

    .line 1843599
    iget-object v0, p0, LX/C1v;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    check-cast p2, Lcom/facebook/facecastdisplay/LiveVideoStatusView;

    iget-object v0, p3, LX/C1u;->b:LX/3Hc;

    .line 1843600
    const/4 p0, 0x1

    invoke-virtual {p2, p0}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->setIsLiveNow(Z)V

    .line 1843601
    new-instance p0, LX/C1w;

    invoke-direct {p0, p2}, LX/C1w;-><init>(Lcom/facebook/facecastdisplay/LiveVideoStatusView;)V

    .line 1843602
    iput-object p0, v0, LX/3Hc;->l:LX/3Ha;

    .line 1843603
    return-void
.end method

.method public final f()LX/1mv;
    .locals 1

    .prologue
    .line 1843597
    sget-object v0, LX/1mv;->VIEW:LX/1mv;

    return-object v0
.end method

.method public final g(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 8

    .prologue
    .line 1843593
    check-cast p3, LX/C1u;

    .line 1843594
    iget-object v0, p0, LX/C1v;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/C1x;

    iget-object v1, p3, LX/C1u;->a:Ljava/lang/String;

    iget-object v2, p3, LX/C1u;->b:LX/3Hc;

    .line 1843595
    iget-object v3, v0, LX/C1x;->a:Landroid/os/Handler;

    new-instance v4, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveStatusViewComponentSpec$2;

    invoke-direct {v4, v0, v2, v1}, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveStatusViewComponentSpec$2;-><init>(LX/C1x;LX/3Hc;Ljava/lang/String;)V

    const-wide/16 v5, 0x32

    const v7, -0x2c405f74

    invoke-static {v3, v4, v5, v6, v7}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 1843596
    return-void
.end method

.method public final h(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 2

    .prologue
    .line 1843588
    check-cast p3, LX/C1u;

    .line 1843589
    iget-object v0, p0, LX/C1v;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/C1x;

    iget-object v1, p3, LX/C1u;->b:LX/3Hc;

    .line 1843590
    iget-object p0, v0, LX/C1x;->a:Landroid/os/Handler;

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 1843591
    invoke-virtual {v1}, LX/3Hc;->a()V

    .line 1843592
    return-void
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 1843587
    const/16 v0, 0xf

    return v0
.end method
