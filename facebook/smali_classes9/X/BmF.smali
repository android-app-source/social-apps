.class public final LX/BmF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/BmE;


# instance fields
.field public final synthetic a:Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;)V
    .locals 0

    .prologue
    .line 1818007
    iput-object p1, p0, LX/BmF;->a:Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/Calendar;Ljava/util/Calendar;)V
    .locals 4

    .prologue
    const/16 v3, 0xc

    const/16 v2, 0xb

    .line 1818008
    if-eqz p2, :cond_0

    .line 1818009
    iget-object v0, p0, LX/BmF;->a:Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;

    iput-object p2, v0, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->y:Ljava/util/Calendar;

    .line 1818010
    iget-object v0, p0, LX/BmF;->a:Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;

    iget-object v1, p0, LX/BmF;->a:Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;

    iget-object v1, v1, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->t:Lcom/facebook/resources/ui/FbTextView;

    .line 1818011
    invoke-static {v0, v1, p2}, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->a$redex0(Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;Lcom/facebook/resources/ui/FbTextView;Ljava/util/Calendar;)V

    .line 1818012
    :cond_0
    iget-object v0, p0, LX/BmF;->a:Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;

    iput-object p1, v0, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->x:Ljava/util/Calendar;

    .line 1818013
    iget-object v0, p0, LX/BmF;->a:Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;

    iget-object v1, p0, LX/BmF;->a:Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;

    iget-object v1, v1, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->s:Lcom/facebook/resources/ui/FbTextView;

    .line 1818014
    invoke-static {v0, v1, p1}, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->a$redex0(Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;Lcom/facebook/resources/ui/FbTextView;Ljava/util/Calendar;)V

    .line 1818015
    iget-object v0, p0, LX/BmF;->a:Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;

    iget-boolean v0, v0, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->A:Z

    if-eqz v0, :cond_2

    .line 1818016
    iget-object v0, p0, LX/BmF;->a:Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;

    iget-object v0, v0, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->y:Ljava/util/Calendar;

    if-eqz v0, :cond_1

    .line 1818017
    iget-object v0, p0, LX/BmF;->a:Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;

    iget-object v1, p0, LX/BmF;->a:Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;

    iget-object v1, v1, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->y:Ljava/util/Calendar;

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v1

    iget-object v2, p0, LX/BmF;->a:Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;

    iget-object v2, v2, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->y:Ljava/util/Calendar;

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->get(I)I

    move-result v2

    .line 1818018
    invoke-static {v0, v1, v2}, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->a$redex0(Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;II)V

    .line 1818019
    :cond_1
    :goto_0
    return-void

    .line 1818020
    :cond_2
    iget-object v0, p0, LX/BmF;->a:Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;

    iget-object v1, p0, LX/BmF;->a:Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;

    iget-object v1, v1, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->x:Ljava/util/Calendar;

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v1

    iget-object v2, p0, LX/BmF;->a:Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;

    iget-object v2, v2, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->x:Ljava/util/Calendar;

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->get(I)I

    move-result v2

    .line 1818021
    invoke-static {v0, v1, v2}, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->a$redex0(Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;II)V

    .line 1818022
    goto :goto_0
.end method
