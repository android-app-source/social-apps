.class public final LX/B1Z;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<TMODE",
        "L;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/Enum;

.field public final synthetic b:LX/3rL;

.field public final synthetic c:LX/B1b;

.field public final synthetic d:LX/B1d;


# direct methods
.method public constructor <init>(LX/B1d;Ljava/lang/Enum;LX/3rL;LX/B1b;)V
    .locals 0

    .prologue
    .line 1736094
    iput-object p1, p0, LX/B1Z;->d:LX/B1d;

    iput-object p2, p0, LX/B1Z;->a:Ljava/lang/Enum;

    iput-object p3, p0, LX/B1Z;->b:LX/3rL;

    iput-object p4, p0, LX/B1Z;->c:LX/B1b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1736095
    iget-object v0, p0, LX/B1Z;->d:LX/B1d;

    iget-object v1, p0, LX/B1Z;->a:Ljava/lang/Enum;

    invoke-virtual {v0, v1}, LX/B1d;->a(Ljava/lang/Enum;)LX/0gW;

    move-result-object v0

    .line 1736096
    if-eqz v0, :cond_0

    .line 1736097
    iget-object v1, p0, LX/B1Z;->c:LX/B1b;

    const/4 v2, 0x1

    invoke-interface {v1, v2}, LX/B1b;->a(Z)V

    .line 1736098
    iget-object v1, p0, LX/B1Z;->d:LX/B1d;

    iget-object v1, v1, LX/B1d;->g:LX/0tX;

    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    sget-object v2, LX/0zS;->c:LX/0zS;

    invoke-virtual {v0, v2}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 1736099
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
