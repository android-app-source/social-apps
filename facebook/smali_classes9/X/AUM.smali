.class public final LX/AUM;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# static fields
.field public static final a:LX/AUM;


# instance fields
.field private final b:LX/AUg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/AUg",
            "<",
            "LX/AUJ;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/AUL;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1677957
    new-instance v0, LX/AUM;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, LX/AUM;-><init>(Landroid/os/Looper;)V

    sput-object v0, LX/AUM;->a:LX/AUM;

    return-void
.end method

.method private constructor <init>(Landroid/os/Looper;)V
    .locals 1
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .prologue
    .line 1677953
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1677954
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/AUM;->c:Ljava/util/ArrayList;

    .line 1677955
    new-instance v0, LX/AUg;

    invoke-direct {v0, p1}, LX/AUg;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, LX/AUM;->b:LX/AUg;

    .line 1677956
    return-void
.end method

.method public static a()LX/AUM;
    .locals 1

    .prologue
    .line 1677952
    sget-object v0, LX/AUM;->a:LX/AUM;

    return-object v0
.end method

.method private static c(LX/AUM;Ljava/lang/Object;LX/AUF;)I
    .locals 4

    .prologue
    .line 1677945
    const/4 v1, 0x0

    iget-object v0, p0, LX/AUM;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    :goto_0
    if-ge v1, v2, :cond_1

    .line 1677946
    iget-object v0, p0, LX/AUM;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AUL;

    .line 1677947
    iget-object v3, v0, LX/AUL;->a:Ljava/lang/Object;

    invoke-virtual {v3, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, v0, LX/AUL;->b:LX/AUF;

    invoke-virtual {v3, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x1

    :goto_1
    move v0, v3

    .line 1677948
    if-eqz v0, :cond_0

    move v0, v1

    .line 1677949
    :goto_2
    return v0

    .line 1677950
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1677951
    :cond_1
    const/4 v0, -0x1

    goto :goto_2

    :cond_2
    const/4 v3, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 1677943
    iget-object v0, p0, LX/AUM;->b:LX/AUg;

    new-instance v1, LX/AUJ;

    invoke-direct {v1, p1}, LX/AUJ;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, LX/AUg;->b(LX/AUI;)Z

    .line 1677944
    return-void
.end method

.method public final declared-synchronized a(Ljava/lang/Object;LX/AUF;)V
    .locals 3

    .prologue
    .line 1677930
    monitor-enter p0

    :try_start_0
    invoke-static {p0, p1, p2}, LX/AUM;->c(LX/AUM;Ljava/lang/Object;LX/AUF;)I

    move-result v0

    if-ltz v0, :cond_0

    .line 1677931
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "This observer is already registered: key="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; observer="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1677932
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1677933
    :cond_0
    :try_start_1
    new-instance v0, LX/AUL;

    invoke-direct {v0, p1, p2}, LX/AUL;-><init>(Ljava/lang/Object;LX/AUF;)V

    .line 1677934
    iget-object v1, p0, LX/AUM;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1677935
    iget-object v1, p0, LX/AUM;->b:LX/AUg;

    invoke-virtual {v1, v0}, LX/AUg;->a(LX/AUK;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1677936
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized b(Ljava/lang/Object;LX/AUF;)V
    .locals 2

    .prologue
    .line 1677937
    monitor-enter p0

    :try_start_0
    invoke-static {p0, p1, p2}, LX/AUM;->c(LX/AUM;Ljava/lang/Object;LX/AUF;)I

    move-result v0

    .line 1677938
    if-ltz v0, :cond_0

    .line 1677939
    iget-object v1, p0, LX/AUM;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AUL;

    .line 1677940
    iget-object v1, p0, LX/AUM;->b:LX/AUg;

    invoke-virtual {v1, v0}, LX/AUg;->b(LX/AUK;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1677941
    :cond_0
    monitor-exit p0

    return-void

    .line 1677942
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
