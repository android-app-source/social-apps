.class public LX/AYG;
.super LX/AWT;
.source ""


# instance fields
.field public a:LX/3HT;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final b:Lcom/facebook/facecastdisplay/FacecastPercentView;

.field private final c:LX/AYF;

.field public f:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1685149
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/AYG;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1685150
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1685147
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/AYG;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1685148
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 1685141
    invoke-direct {p0, p1, p2, p3}, LX/AWT;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1685142
    const-class v0, LX/AYG;

    invoke-static {v0, p0}, LX/AYG;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1685143
    const v0, 0x7f0305e7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1685144
    const v0, 0x7f0d1044

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/FacecastPercentView;

    iput-object v0, p0, LX/AYG;->b:Lcom/facebook/facecastdisplay/FacecastPercentView;

    .line 1685145
    new-instance v0, LX/AYF;

    invoke-direct {v0, p0}, LX/AYF;-><init>(LX/AYG;)V

    iput-object v0, p0, LX/AYG;->c:LX/AYF;

    .line 1685146
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/AYG;

    invoke-static {p0}, LX/3HT;->a(LX/0QB;)LX/3HT;

    move-result-object p0

    check-cast p0, LX/3HT;

    iput-object p0, p1, LX/AYG;->a:LX/3HT;

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;LX/AVF;)V
    .locals 2

    .prologue
    .line 1685138
    invoke-super {p0, p1, p2}, LX/AWT;->a(Landroid/view/ViewGroup;LX/AVF;)V

    .line 1685139
    iget-object v0, p0, LX/AYG;->a:LX/3HT;

    iget-object v1, p0, LX/AYG;->c:LX/AYF;

    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 1685140
    return-void
.end method

.method public final a(ZJ)V
    .locals 6

    .prologue
    .line 1685136
    iget-object v0, p0, LX/AYG;->b:Lcom/facebook/facecastdisplay/FacecastPercentView;

    const-wide/16 v4, 0xfa

    move v1, p1

    move-wide v2, p2

    invoke-static/range {v0 .. v5}, LX/3Hk;->a(Landroid/view/View;ZJJ)V

    .line 1685137
    return-void
.end method

.method public final iF_()V
    .locals 2

    .prologue
    .line 1685133
    invoke-super {p0}, LX/AWT;->iF_()V

    .line 1685134
    iget-object v0, p0, LX/AYG;->a:LX/3HT;

    iget-object v1, p0, LX/AYG;->c:LX/AYF;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 1685135
    return-void
.end method

.method public setIsLandscape(Z)V
    .locals 2

    .prologue
    .line 1685130
    iget-object v1, p0, LX/AYG;->b:Lcom/facebook/facecastdisplay/FacecastPercentView;

    if-eqz p1, :cond_0

    const v0, 0x3ecccccd    # 0.4f

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/facecastdisplay/FacecastPercentView;->setPercent(F)V

    .line 1685131
    return-void

    .line 1685132
    :cond_0
    const/high16 v0, 0x3f000000    # 0.5f

    goto :goto_0
.end method
