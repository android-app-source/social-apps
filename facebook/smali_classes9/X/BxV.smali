.class public LX/BxV;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/Boolean;

.field public b:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1835588
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1835589
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/BxV;->b:Z

    .line 1835590
    const/4 v0, 0x0

    iput-object v0, p0, LX/BxV;->a:Ljava/lang/Boolean;

    .line 1835591
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;LX/BxV;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1835576
    iget-object v0, p1, LX/BxV;->a:Ljava/lang/Boolean;

    move-object v0, v0

    .line 1835577
    if-eqz v0, :cond_0

    .line 1835578
    :goto_0
    return-void

    .line 1835579
    :cond_0
    const v0, -0x3625f733

    invoke-static {p0, v0}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v3

    .line 1835580
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSavedState;->NOT_SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 1835581
    if-eqz v3, :cond_1

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->U()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->U()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLNode;->kT()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 1835582
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->U()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->kT()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v3

    .line 1835583
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSavedState;->NOT_SAVABLE:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-eq v3, v0, :cond_4

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSavedState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-eq v3, v0, :cond_4

    move v0, v1

    .line 1835584
    :goto_1
    iput-boolean v0, p1, LX/BxV;->b:Z

    .line 1835585
    move-object v0, v3

    .line 1835586
    :cond_1
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSavedState;->SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-eq v0, v3, :cond_2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSavedState;->ARCHIVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-ne v0, v3, :cond_3

    :cond_2
    move v2, v1

    :cond_3
    invoke-virtual {p1, v2}, LX/BxV;->a(Z)V

    goto :goto_0

    :cond_4
    move v0, v2

    .line 1835587
    goto :goto_1
.end method


# virtual methods
.method public final a(Z)V
    .locals 1

    .prologue
    .line 1835592
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LX/BxV;->a:Ljava/lang/Boolean;

    .line 1835593
    return-void
.end method
