.class public LX/CbF;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final e:Ljava/lang/Object;


# instance fields
.field public final a:LX/8Gm;

.field public final b:LX/Cao;

.field public final c:LX/03V;

.field public final d:LX/0Sh;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1919337
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/CbF;->e:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/8Gm;LX/Cao;LX/0Sh;LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1919331
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1919332
    iput-object p1, p0, LX/CbF;->a:LX/8Gm;

    .line 1919333
    iput-object p2, p0, LX/CbF;->b:LX/Cao;

    .line 1919334
    iput-object p3, p0, LX/CbF;->d:LX/0Sh;

    .line 1919335
    iput-object p4, p0, LX/CbF;->c:LX/03V;

    .line 1919336
    return-void
.end method

.method public static a(LX/0QB;)LX/CbF;
    .locals 10

    .prologue
    .line 1919302
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 1919303
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 1919304
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 1919305
    if-nez v1, :cond_0

    .line 1919306
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1919307
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 1919308
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 1919309
    sget-object v1, LX/CbF;->e:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 1919310
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 1919311
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1919312
    :cond_1
    if-nez v1, :cond_4

    .line 1919313
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 1919314
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 1919315
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 1919316
    new-instance p0, LX/CbF;

    invoke-static {v0}, LX/8Gm;->b(LX/0QB;)LX/8Gm;

    move-result-object v1

    check-cast v1, LX/8Gm;

    invoke-static {v0}, LX/Cao;->a(LX/0QB;)LX/Cao;

    move-result-object v7

    check-cast v7, LX/Cao;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v8

    check-cast v8, LX/0Sh;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v9

    check-cast v9, LX/03V;

    invoke-direct {p0, v1, v7, v8, v9}, LX/CbF;-><init>(LX/8Gm;LX/Cao;LX/0Sh;LX/03V;)V

    .line 1919317
    move-object v1, p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1919318
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 1919319
    if-nez v1, :cond_2

    .line 1919320
    sget-object v0, LX/CbF;->e:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CbF;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1919321
    :goto_1
    if-eqz v0, :cond_3

    .line 1919322
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 1919323
    :goto_3
    check-cast v0, LX/CbF;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 1919324
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 1919325
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 1919326
    :catchall_1
    move-exception v0

    .line 1919327
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 1919328
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 1919329
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 1919330
    :cond_2
    :try_start_8
    sget-object v0, LX/CbF;->e:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CbF;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method
