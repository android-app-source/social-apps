.class public LX/CK7;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1876421
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1876422
    iput-object p1, p0, LX/CK7;->a:LX/0Zb;

    .line 1876423
    return-void
.end method

.method public static a(LX/0QB;)LX/CK7;
    .locals 1

    .prologue
    .line 1876424
    invoke-static {p0}, LX/CK7;->b(LX/0QB;)LX/CK7;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/CK7;
    .locals 2

    .prologue
    .line 1876425
    new-instance v1, LX/CK7;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-direct {v1, v0}, LX/CK7;-><init>(LX/0Zb;)V

    .line 1876426
    return-object v1
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1876427
    iget-object v0, p0, LX/CK7;->a:LX/0Zb;

    const-string v1, "messenger_content_subscription_banner_impression"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 1876428
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1876429
    :goto_0
    return-void

    .line 1876430
    :cond_0
    const-string v1, "source"

    invoke-virtual {v0, v1, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v0

    const-string v1, "page_id"

    invoke-virtual {v0, v1, p2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v0

    const-string v1, "article_info"

    invoke-virtual {v0, v1, p3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v0

    invoke-virtual {v0}, LX/0oG;->d()V

    goto :goto_0
.end method
