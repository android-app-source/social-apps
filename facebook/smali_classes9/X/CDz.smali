.class public final LX/CDz;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/CE0;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/CharSequence;

.field public b:LX/1X1;

.field public c:I

.field public d:Ljava/lang/CharSequence;

.field public e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation
.end field

.field public f:Z

.field public g:Z

.field public h:LX/1X5;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1X5",
            "<*>;"
        }
    .end annotation
.end field

.field public i:LX/1X1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1X1",
            "<*>;"
        }
    .end annotation
.end field

.field public j:LX/1dQ;

.field public k:Z

.field public l:Z

.field public final synthetic m:LX/CE0;


# direct methods
.method public constructor <init>(LX/CE0;)V
    .locals 1

    .prologue
    .line 1860412
    iput-object p1, p0, LX/CDz;->m:LX/CE0;

    .line 1860413
    move-object v0, p1

    .line 1860414
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1860415
    const/4 v0, 0x3

    iput v0, p0, LX/CDz;->c:I

    .line 1860416
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/CDz;->f:Z

    .line 1860417
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1860418
    const-string v0, "FigStoryHeaderComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1860419
    if-ne p0, p1, :cond_1

    .line 1860420
    :cond_0
    :goto_0
    return v0

    .line 1860421
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1860422
    goto :goto_0

    .line 1860423
    :cond_3
    check-cast p1, LX/CDz;

    .line 1860424
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1860425
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1860426
    if-eq v2, v3, :cond_0

    .line 1860427
    iget-object v2, p0, LX/CDz;->a:Ljava/lang/CharSequence;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/CDz;->a:Ljava/lang/CharSequence;

    iget-object v3, p1, LX/CDz;->a:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1860428
    goto :goto_0

    .line 1860429
    :cond_5
    iget-object v2, p1, LX/CDz;->a:Ljava/lang/CharSequence;

    if-nez v2, :cond_4

    .line 1860430
    :cond_6
    iget-object v2, p0, LX/CDz;->b:LX/1X1;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/CDz;->b:LX/1X1;

    iget-object v3, p1, LX/CDz;->b:LX/1X1;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1860431
    goto :goto_0

    .line 1860432
    :cond_8
    iget-object v2, p1, LX/CDz;->b:LX/1X1;

    if-nez v2, :cond_7

    .line 1860433
    :cond_9
    iget v2, p0, LX/CDz;->c:I

    iget v3, p1, LX/CDz;->c:I

    if-eq v2, v3, :cond_a

    move v0, v1

    .line 1860434
    goto :goto_0

    .line 1860435
    :cond_a
    iget-object v2, p0, LX/CDz;->d:Ljava/lang/CharSequence;

    if-eqz v2, :cond_c

    iget-object v2, p0, LX/CDz;->d:Ljava/lang/CharSequence;

    iget-object v3, p1, LX/CDz;->d:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    :cond_b
    move v0, v1

    .line 1860436
    goto :goto_0

    .line 1860437
    :cond_c
    iget-object v2, p1, LX/CDz;->d:Ljava/lang/CharSequence;

    if-nez v2, :cond_b

    .line 1860438
    :cond_d
    iget-object v2, p0, LX/CDz;->e:LX/0Or;

    if-eqz v2, :cond_f

    iget-object v2, p0, LX/CDz;->e:LX/0Or;

    iget-object v3, p1, LX/CDz;->e:LX/0Or;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_10

    :cond_e
    move v0, v1

    .line 1860439
    goto :goto_0

    .line 1860440
    :cond_f
    iget-object v2, p1, LX/CDz;->e:LX/0Or;

    if-nez v2, :cond_e

    .line 1860441
    :cond_10
    iget-boolean v2, p0, LX/CDz;->f:Z

    iget-boolean v3, p1, LX/CDz;->f:Z

    if-eq v2, v3, :cond_11

    move v0, v1

    .line 1860442
    goto :goto_0

    .line 1860443
    :cond_11
    iget-boolean v2, p0, LX/CDz;->g:Z

    iget-boolean v3, p1, LX/CDz;->g:Z

    if-eq v2, v3, :cond_12

    move v0, v1

    .line 1860444
    goto :goto_0

    .line 1860445
    :cond_12
    iget-object v2, p0, LX/CDz;->h:LX/1X5;

    if-eqz v2, :cond_14

    iget-object v2, p0, LX/CDz;->h:LX/1X5;

    iget-object v3, p1, LX/CDz;->h:LX/1X5;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_15

    :cond_13
    move v0, v1

    .line 1860446
    goto/16 :goto_0

    .line 1860447
    :cond_14
    iget-object v2, p1, LX/CDz;->h:LX/1X5;

    if-nez v2, :cond_13

    .line 1860448
    :cond_15
    iget-object v2, p0, LX/CDz;->i:LX/1X1;

    if-eqz v2, :cond_17

    iget-object v2, p0, LX/CDz;->i:LX/1X1;

    iget-object v3, p1, LX/CDz;->i:LX/1X1;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_18

    :cond_16
    move v0, v1

    .line 1860449
    goto/16 :goto_0

    .line 1860450
    :cond_17
    iget-object v2, p1, LX/CDz;->i:LX/1X1;

    if-nez v2, :cond_16

    .line 1860451
    :cond_18
    iget-object v2, p0, LX/CDz;->j:LX/1dQ;

    if-eqz v2, :cond_1a

    iget-object v2, p0, LX/CDz;->j:LX/1dQ;

    iget-object v3, p1, LX/CDz;->j:LX/1dQ;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1b

    :cond_19
    move v0, v1

    .line 1860452
    goto/16 :goto_0

    .line 1860453
    :cond_1a
    iget-object v2, p1, LX/CDz;->j:LX/1dQ;

    if-nez v2, :cond_19

    .line 1860454
    :cond_1b
    iget-boolean v2, p0, LX/CDz;->k:Z

    iget-boolean v3, p1, LX/CDz;->k:Z

    if-eq v2, v3, :cond_1c

    move v0, v1

    .line 1860455
    goto/16 :goto_0

    .line 1860456
    :cond_1c
    iget-boolean v2, p0, LX/CDz;->l:Z

    iget-boolean v3, p1, LX/CDz;->l:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 1860457
    goto/16 :goto_0
.end method

.method public final g()LX/1X1;
    .locals 3

    .prologue
    .line 1860458
    const/4 v2, 0x0

    .line 1860459
    invoke-super {p0}, LX/1X1;->g()LX/1X1;

    move-result-object v0

    check-cast v0, LX/CDz;

    .line 1860460
    iget-object v1, v0, LX/CDz;->b:LX/1X1;

    if-eqz v1, :cond_1

    iget-object v1, v0, LX/CDz;->b:LX/1X1;

    invoke-virtual {v1}, LX/1X1;->g()LX/1X1;

    move-result-object v1

    :goto_0
    iput-object v1, v0, LX/CDz;->b:LX/1X1;

    .line 1860461
    iget-object v1, v0, LX/CDz;->i:LX/1X1;

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/CDz;->i:LX/1X1;

    invoke-virtual {v1}, LX/1X1;->g()LX/1X1;

    move-result-object v2

    :cond_0
    iput-object v2, v0, LX/CDz;->i:LX/1X1;

    .line 1860462
    return-object v0

    :cond_1
    move-object v1, v2

    .line 1860463
    goto :goto_0
.end method
