.class public final LX/Agk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6qn;


# instance fields
.field public final synthetic a:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;)V
    .locals 0

    .prologue
    .line 1701502
    iput-object p1, p0, LX/Agk;->a:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/checkout/CheckoutCommonParams;)V
    .locals 2

    .prologue
    .line 1701500
    iget-object v0, p0, LX/Agk;->a:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->w:LX/6qd;

    iget-object v1, p0, LX/Agk;->a:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;

    iget-object v1, v1, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->v:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v0, v1, p1}, LX/6qd;->a(Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/payments/checkout/CheckoutCommonParams;)V

    .line 1701501
    return-void
.end method

.method public final a(Lcom/facebook/payments/contactinfo/model/NameContactInfo;)V
    .locals 2

    .prologue
    .line 1701499
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Not needed in tip jar checkout!"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Lcom/facebook/payments/paymentmethods/model/PaymentMethod;)V
    .locals 2

    .prologue
    .line 1701503
    iget-object v0, p0, LX/Agk;->a:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->w:LX/6qd;

    iget-object v1, p0, LX/Agk;->a:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;

    iget-object v1, v1, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->v:Lcom/facebook/payments/checkout/model/CheckoutData;

    invoke-interface {v0, v1, p1}, LX/6qd;->a(Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/payments/paymentmethods/model/PaymentMethod;)V

    .line 1701504
    return-void
.end method

.method public final a(Lcom/facebook/payments/shipping/model/MailingAddress;)V
    .locals 2

    .prologue
    .line 1701498
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Not needed in tip jar checkout!"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Lcom/facebook/payments/shipping/model/ShippingOption;)V
    .locals 2

    .prologue
    .line 1701497
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Not needed in tip jar checkout!"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Ljava/lang/Integer;Lcom/facebook/payments/currency/CurrencyAmount;)V
    .locals 2

    .prologue
    .line 1701496
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Not needed in tip jar checkout!"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Ljava/lang/String;LX/0Px;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/checkout/configuration/model/CheckoutOption;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1701495
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Not needed in tip jar checkout!"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/payments/contactinfo/model/ContactInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1701494
    return-void
.end method
