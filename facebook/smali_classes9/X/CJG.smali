.class public LX/CJG;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:Landroid/view/View;

.field public b:Landroid/view/View;

.field public c:Lcom/facebook/resources/ui/FbTextView;

.field public d:Lcom/facebook/messaging/attribution/MediaResourceView;

.field public e:Lcom/facebook/resources/ui/FbTextView;

.field public f:Lcom/facebook/resources/ui/FbTextView;

.field public g:LX/CJ7;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1875272
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 1875273
    const v0, 0x7f030918

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1875274
    const v0, 0x7f0d1756

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/attribution/MediaResourceView;

    iput-object v0, p0, LX/CJG;->d:Lcom/facebook/messaging/attribution/MediaResourceView;

    .line 1875275
    const v0, 0x7f0d1755

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/CJG;->a:Landroid/view/View;

    .line 1875276
    const v0, 0x7f0d118a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/CJG;->b:Landroid/view/View;

    .line 1875277
    const v0, 0x7f0d09a9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/CJG;->c:Lcom/facebook/resources/ui/FbTextView;

    .line 1875278
    const v0, 0x7f0d02c4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/CJG;->e:Lcom/facebook/resources/ui/FbTextView;

    .line 1875279
    const v0, 0x7f0d0550

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/CJG;->f:Lcom/facebook/resources/ui/FbTextView;

    .line 1875280
    iget-object v0, p0, LX/CJG;->a:Landroid/view/View;

    new-instance p1, LX/CJD;

    invoke-direct {p1, p0}, LX/CJD;-><init>(LX/CJG;)V

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1875281
    iget-object v0, p0, LX/CJG;->b:Landroid/view/View;

    new-instance p1, LX/CJE;

    invoke-direct {p1, p0}, LX/CJE;-><init>(LX/CJG;)V

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1875282
    iget-object v0, p0, LX/CJG;->c:Lcom/facebook/resources/ui/FbTextView;

    new-instance p1, LX/CJF;

    invoke-direct {p1, p0}, LX/CJF;-><init>(LX/CJG;)V

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1875283
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 1875284
    iget-object v0, p0, LX/CJG;->b:Landroid/view/View;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 1875285
    iget-object v0, p0, LX/CJG;->b:Landroid/view/View;

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0}, LX/CJG;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a019a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1875286
    return-void
.end method

.method public setMediaResource(Lcom/facebook/ui/media/attachments/MediaResource;)V
    .locals 1

    .prologue
    .line 1875287
    iget-object v0, p0, LX/CJG;->d:Lcom/facebook/messaging/attribution/MediaResourceView;

    invoke-virtual {v0, p1}, Lcom/facebook/messaging/attribution/MediaResourceView;->setMediaResource(Lcom/facebook/ui/media/attachments/MediaResource;)V

    .line 1875288
    return-void
.end method
