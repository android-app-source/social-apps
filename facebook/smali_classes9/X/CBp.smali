.class public final enum LX/CBp;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/CBp;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/CBp;

.field public static final enum PRIMARY:LX/CBp;

.field public static final enum UNKNOWN:LX/CBp;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1856842
    new-instance v0, LX/CBp;

    const-string v1, "PRIMARY"

    invoke-direct {v0, v1, v2}, LX/CBp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CBp;->PRIMARY:LX/CBp;

    .line 1856843
    new-instance v0, LX/CBp;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v3}, LX/CBp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CBp;->UNKNOWN:LX/CBp;

    .line 1856844
    const/4 v0, 0x2

    new-array v0, v0, [LX/CBp;

    sget-object v1, LX/CBp;->PRIMARY:LX/CBp;

    aput-object v1, v0, v2

    sget-object v1, LX/CBp;->UNKNOWN:LX/CBp;

    aput-object v1, v0, v3

    sput-object v0, LX/CBp;->$VALUES:[LX/CBp;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1856845
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)LX/CBp;
    .locals 1

    .prologue
    .line 1856846
    if-nez p0, :cond_0

    .line 1856847
    :try_start_0
    sget-object v0, LX/CBp;->UNKNOWN:LX/CBp;

    .line 1856848
    :goto_0
    return-object v0

    .line 1856849
    :cond_0
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/CBp;->valueOf(Ljava/lang/String;)LX/CBp;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 1856850
    :catch_0
    sget-object v0, LX/CBp;->UNKNOWN:LX/CBp;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/CBp;
    .locals 1

    .prologue
    .line 1856851
    const-class v0, LX/CBp;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/CBp;

    return-object v0
.end method

.method public static values()[LX/CBp;
    .locals 1

    .prologue
    .line 1856852
    sget-object v0, LX/CBp;->$VALUES:[LX/CBp;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/CBp;

    return-object v0
.end method
