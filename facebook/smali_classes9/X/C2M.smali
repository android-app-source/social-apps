.class public final LX/C2M;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/1Vm;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/1Pp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public b:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

.field public c:Landroid/view/View$OnClickListener;

.field public d:LX/1dc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dc",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field public e:Landroid/net/Uri;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public j:Landroid/view/View$OnClickListener;

.field public k:Landroid/view/View$OnClickListener;

.field public final synthetic l:LX/1Vm;


# direct methods
.method public constructor <init>(LX/1Vm;)V
    .locals 1

    .prologue
    .line 1844206
    iput-object p1, p0, LX/C2M;->l:LX/1Vm;

    .line 1844207
    move-object v0, p1

    .line 1844208
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1844209
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1844210
    const-string v0, "ActionLinkCallToActionComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1844211
    if-ne p0, p1, :cond_1

    .line 1844212
    :cond_0
    :goto_0
    return v0

    .line 1844213
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1844214
    goto :goto_0

    .line 1844215
    :cond_3
    check-cast p1, LX/C2M;

    .line 1844216
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1844217
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1844218
    if-eq v2, v3, :cond_0

    .line 1844219
    iget-object v2, p0, LX/C2M;->a:LX/1Pp;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/C2M;->a:LX/1Pp;

    iget-object v3, p1, LX/C2M;->a:LX/1Pp;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1844220
    goto :goto_0

    .line 1844221
    :cond_5
    iget-object v2, p1, LX/C2M;->a:LX/1Pp;

    if-nez v2, :cond_4

    .line 1844222
    :cond_6
    iget-object v2, p0, LX/C2M;->b:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/C2M;->b:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    iget-object v3, p1, LX/C2M;->b:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1844223
    goto :goto_0

    .line 1844224
    :cond_8
    iget-object v2, p1, LX/C2M;->b:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    if-nez v2, :cond_7

    .line 1844225
    :cond_9
    iget-object v2, p0, LX/C2M;->c:Landroid/view/View$OnClickListener;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/C2M;->c:Landroid/view/View$OnClickListener;

    iget-object v3, p1, LX/C2M;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 1844226
    goto :goto_0

    .line 1844227
    :cond_b
    iget-object v2, p1, LX/C2M;->c:Landroid/view/View$OnClickListener;

    if-nez v2, :cond_a

    .line 1844228
    :cond_c
    iget-object v2, p0, LX/C2M;->d:LX/1dc;

    if-eqz v2, :cond_e

    iget-object v2, p0, LX/C2M;->d:LX/1dc;

    iget-object v3, p1, LX/C2M;->d:LX/1dc;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    :cond_d
    move v0, v1

    .line 1844229
    goto :goto_0

    .line 1844230
    :cond_e
    iget-object v2, p1, LX/C2M;->d:LX/1dc;

    if-nez v2, :cond_d

    .line 1844231
    :cond_f
    iget-object v2, p0, LX/C2M;->e:Landroid/net/Uri;

    if-eqz v2, :cond_11

    iget-object v2, p0, LX/C2M;->e:Landroid/net/Uri;

    iget-object v3, p1, LX/C2M;->e:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_12

    :cond_10
    move v0, v1

    .line 1844232
    goto :goto_0

    .line 1844233
    :cond_11
    iget-object v2, p1, LX/C2M;->e:Landroid/net/Uri;

    if-nez v2, :cond_10

    .line 1844234
    :cond_12
    iget-object v2, p0, LX/C2M;->f:Ljava/lang/String;

    if-eqz v2, :cond_14

    iget-object v2, p0, LX/C2M;->f:Ljava/lang/String;

    iget-object v3, p1, LX/C2M;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_15

    :cond_13
    move v0, v1

    .line 1844235
    goto/16 :goto_0

    .line 1844236
    :cond_14
    iget-object v2, p1, LX/C2M;->f:Ljava/lang/String;

    if-nez v2, :cond_13

    .line 1844237
    :cond_15
    iget-object v2, p0, LX/C2M;->g:Ljava/lang/String;

    if-eqz v2, :cond_17

    iget-object v2, p0, LX/C2M;->g:Ljava/lang/String;

    iget-object v3, p1, LX/C2M;->g:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_18

    :cond_16
    move v0, v1

    .line 1844238
    goto/16 :goto_0

    .line 1844239
    :cond_17
    iget-object v2, p1, LX/C2M;->g:Ljava/lang/String;

    if-nez v2, :cond_16

    .line 1844240
    :cond_18
    iget-object v2, p0, LX/C2M;->h:Ljava/lang/String;

    if-eqz v2, :cond_1a

    iget-object v2, p0, LX/C2M;->h:Ljava/lang/String;

    iget-object v3, p1, LX/C2M;->h:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1b

    :cond_19
    move v0, v1

    .line 1844241
    goto/16 :goto_0

    .line 1844242
    :cond_1a
    iget-object v2, p1, LX/C2M;->h:Ljava/lang/String;

    if-nez v2, :cond_19

    .line 1844243
    :cond_1b
    iget-object v2, p0, LX/C2M;->i:Ljava/lang/String;

    if-eqz v2, :cond_1d

    iget-object v2, p0, LX/C2M;->i:Ljava/lang/String;

    iget-object v3, p1, LX/C2M;->i:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1e

    :cond_1c
    move v0, v1

    .line 1844244
    goto/16 :goto_0

    .line 1844245
    :cond_1d
    iget-object v2, p1, LX/C2M;->i:Ljava/lang/String;

    if-nez v2, :cond_1c

    .line 1844246
    :cond_1e
    iget-object v2, p0, LX/C2M;->j:Landroid/view/View$OnClickListener;

    if-eqz v2, :cond_20

    iget-object v2, p0, LX/C2M;->j:Landroid/view/View$OnClickListener;

    iget-object v3, p1, LX/C2M;->j:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_21

    :cond_1f
    move v0, v1

    .line 1844247
    goto/16 :goto_0

    .line 1844248
    :cond_20
    iget-object v2, p1, LX/C2M;->j:Landroid/view/View$OnClickListener;

    if-nez v2, :cond_1f

    .line 1844249
    :cond_21
    iget-object v2, p0, LX/C2M;->k:Landroid/view/View$OnClickListener;

    if-eqz v2, :cond_22

    iget-object v2, p0, LX/C2M;->k:Landroid/view/View$OnClickListener;

    iget-object v3, p1, LX/C2M;->k:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1844250
    goto/16 :goto_0

    .line 1844251
    :cond_22
    iget-object v2, p1, LX/C2M;->k:Landroid/view/View$OnClickListener;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
