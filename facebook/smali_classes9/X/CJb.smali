.class public final LX/CJb;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Uh;

.field public final b:Lcom/facebook/content/SecureContextHelper;

.field public final c:LX/1Bg;

.field public final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/01T;

.field public final f:LX/0WV;

.field public g:Landroid/content/Intent;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Uh;Lcom/facebook/content/SecureContextHelper;LX/1Bg;LX/01T;LX/0WV;LX/0Or;)V
    .locals 0
    .param p6    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/IsMeUserAnEmployee;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/1Bg;",
            "LX/01T;",
            "LX/0WV;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1875573
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1875574
    iput-object p1, p0, LX/CJb;->a:LX/0Uh;

    .line 1875575
    iput-object p2, p0, LX/CJb;->b:Lcom/facebook/content/SecureContextHelper;

    .line 1875576
    iput-object p3, p0, LX/CJb;->c:LX/1Bg;

    .line 1875577
    iput-object p6, p0, LX/CJb;->d:LX/0Or;

    .line 1875578
    iput-object p5, p0, LX/CJb;->f:LX/0WV;

    .line 1875579
    iput-object p4, p0, LX/CJb;->e:LX/01T;

    .line 1875580
    return-void
.end method

.method public static a(LX/CJb;LX/0DW;Lcom/facebook/messaging/browser/model/MessengerInAppBrowserLaunchParam;)V
    .locals 3

    .prologue
    .line 1875581
    sget-object v0, Lcom/facebook/messaging/browser/model/MessengerInAppBrowserLaunchParam;->a:Lcom/facebook/messaging/browser/model/MessengerInAppBrowserLaunchParam;

    invoke-virtual {v0, p2}, Lcom/facebook/messaging/browser/model/MessengerInAppBrowserLaunchParam;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1875582
    :cond_0
    :goto_0
    return-void

    .line 1875583
    :cond_1
    iget-wide v0, p2, Lcom/facebook/messaging/browser/model/MessengerInAppBrowserLaunchParam;->b:D

    invoke-virtual {p1, v0, v1}, LX/0DW;->a(D)LX/0DW;

    .line 1875584
    iget-object v0, p2, Lcom/facebook/messaging/browser/model/MessengerInAppBrowserLaunchParam;->g:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p2, Lcom/facebook/messaging/browser/model/MessengerInAppBrowserLaunchParam;->f:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/CJb;->a:LX/0Uh;

    const/16 v1, 0x1f7

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1875585
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1875586
    const-string v1, "extra_page_id"

    iget-object v2, p2, Lcom/facebook/messaging/browser/model/MessengerInAppBrowserLaunchParam;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1875587
    const-string v1, "extra_source_type"

    iget-object v2, p2, Lcom/facebook/messaging/browser/model/MessengerInAppBrowserLaunchParam;->e:LX/CJa;

    invoke-virtual {v2}, LX/CJa;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1875588
    const-string v1, "extra_message_id"

    iget-object v2, p2, Lcom/facebook/messaging/browser/model/MessengerInAppBrowserLaunchParam;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1875589
    invoke-virtual {p1, v0}, LX/0DW;->a(Landroid/os/Bundle;)LX/0DW;

    .line 1875590
    iget-object v0, p2, Lcom/facebook/messaging/browser/model/MessengerInAppBrowserLaunchParam;->g:Ljava/lang/String;

    const-string v1, "ACTION_MESSENGER_OPEN_THREAD"

    .line 1875591
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 1875592
    const-string p0, "KEY_ICON_URL"

    invoke-virtual {v2, p0, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1875593
    const-string p0, "action"

    invoke-virtual {v2, p0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1875594
    iget-object p0, p1, LX/0DW;->a:Landroid/content/Intent;

    const-string p2, "BrowserLiteIntent.EXTRA_PROFILE_ICON"

    invoke-virtual {p0, p2, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 1875595
    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/CJb;
    .locals 7

    .prologue
    .line 1875596
    new-instance v0, LX/CJb;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v1

    check-cast v1, LX/0Uh;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v2

    check-cast v2, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/1Bg;->b(LX/0QB;)LX/1Bg;

    move-result-object v3

    check-cast v3, LX/1Bg;

    invoke-static {p0}, LX/15N;->b(LX/0QB;)LX/01T;

    move-result-object v4

    check-cast v4, LX/01T;

    invoke-static {p0}, LX/0WD;->a(LX/0QB;)LX/0WV;

    move-result-object v5

    check-cast v5, LX/0WV;

    const/16 v6, 0x2fd

    invoke-static {p0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, LX/CJb;-><init>(LX/0Uh;Lcom/facebook/content/SecureContextHelper;LX/1Bg;LX/01T;LX/0WV;LX/0Or;)V

    .line 1875597
    return-object v0
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1875598
    sget-boolean v1, LX/007;->j:Z

    move v1, v1

    .line 1875599
    if-nez v1, :cond_0

    iget-object v1, p0, LX/CJb;->a:LX/0Uh;

    const/16 v2, 0x16d

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method
