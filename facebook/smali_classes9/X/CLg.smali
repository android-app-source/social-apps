.class public LX/CLg;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/CLf;",
        ">;"
    }
.end annotation


# instance fields
.field private a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/customthreads/CustomThreadTheme;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private b:Lcom/facebook/messaging/model/threads/ThreadCustomization;

.field public c:I

.field public d:I


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1879250
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 1879251
    const v0, 0x7f0b024a

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/CLg;->c:I

    .line 1879252
    iget v0, p0, LX/CLg;->c:I

    iput v0, p0, LX/CLg;->d:I

    .line 1879253
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/1OM;->a(Z)V

    .line 1879254
    return-void
.end method


# virtual methods
.method public final C_(I)J
    .locals 2

    .prologue
    .line 1879255
    int-to-long v0, p1

    return-wide v0
.end method

.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 1879256
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1879257
    const v1, 0x7f0303c5

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 1879258
    check-cast p1, Landroid/support/v7/widget/RecyclerView;

    .line 1879259
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()LX/1OR;

    move-result-object v2

    .line 1879260
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, LX/1a3;

    .line 1879261
    if-nez v1, :cond_0

    .line 1879262
    invoke-virtual {v2}, LX/1OR;->b()LX/1a3;

    move-result-object v1

    .line 1879263
    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1879264
    :cond_0
    iget v2, p0, LX/CLg;->c:I

    iput v2, v1, LX/1a3;->width:I

    .line 1879265
    iget v2, p0, LX/CLg;->d:I

    iput v2, v1, LX/1a3;->height:I

    .line 1879266
    new-instance v1, LX/CLf;

    invoke-direct {v1, v0}, LX/CLf;-><init>(Landroid/view/View;)V

    return-object v1
.end method

.method public final a(LX/1a1;I)V
    .locals 3

    .prologue
    .line 1879267
    check-cast p1, LX/CLf;

    .line 1879268
    iget-object v0, p0, LX/CLg;->a:LX/0Px;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1879269
    iget-object v0, p0, LX/CLg;->a:LX/0Px;

    invoke-virtual {v0, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/customthreads/CustomThreadTheme;

    .line 1879270
    iget-object v1, p0, LX/CLg;->b:Lcom/facebook/messaging/model/threads/ThreadCustomization;

    .line 1879271
    if-eqz v1, :cond_0

    .line 1879272
    iget v2, v1, Lcom/facebook/messaging/model/threads/ThreadCustomization;->c:I

    move v2, v2

    .line 1879273
    iget p2, v0, Lcom/facebook/messaging/customthreads/CustomThreadTheme;->b:I

    move p2, p2

    .line 1879274
    if-ne v2, p2, :cond_0

    const/4 v2, 0x1

    :goto_0
    move v1, v2

    .line 1879275
    iget-object p2, p1, LX/CLf;->l:Landroid/widget/ImageView;

    if-eqz v1, :cond_1

    const/4 v2, 0x0

    :goto_1
    invoke-virtual {p2, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1879276
    iget-object v1, p1, LX/CLf;->m:Landroid/widget/ImageView;

    .line 1879277
    iget v2, v0, Lcom/facebook/messaging/customthreads/CustomThreadTheme;->e:I

    if-eqz v2, :cond_2

    .line 1879278
    iget v2, v0, Lcom/facebook/messaging/customthreads/CustomThreadTheme;->e:I

    .line 1879279
    :goto_2
    move v2, v2

    .line 1879280
    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setColorFilter(I)V

    .line 1879281
    iget-object v1, p1, LX/CLf;->m:Landroid/widget/ImageView;

    new-instance v2, LX/CLe;

    invoke-direct {v2, p0, v0}, LX/CLe;-><init>(LX/CLg;Lcom/facebook/messaging/customthreads/CustomThreadTheme;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1879282
    return-void

    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 1879283
    :cond_1
    const/16 v2, 0x8

    goto :goto_1

    :cond_2
    iget v2, v0, Lcom/facebook/messaging/customthreads/CustomThreadTheme;->d:I

    goto :goto_2
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 1879284
    iget-object v0, p0, LX/CLg;->a:LX/0Px;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/CLg;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    goto :goto_0
.end method
