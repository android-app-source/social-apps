.class public final LX/CUI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6E0;


# instance fields
.field private final a:LX/6tM;

.field private final b:LX/0lB;


# direct methods
.method public constructor <init>(LX/6tM;LX/0lB;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1896452
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1896453
    iput-object p1, p0, LX/CUI;->a:LX/6tM;

    .line 1896454
    iput-object p2, p0, LX/CUI;->b:LX/0lB;

    .line 1896455
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/payments/checkout/model/SendPaymentCheckoutResult;)Lcom/facebook/payments/confirmation/ConfirmationParams;
    .locals 5

    .prologue
    .line 1896449
    new-instance v1, Lcom/facebook/pages/common/platform/payments/InstantWorkflowsConfirmationParams;

    iget-object v2, p0, LX/CUI;->b:LX/0lB;

    sget-object v0, LX/6uW;->INSTANT_WORKFLOWS:LX/6uW;

    invoke-static {p1, p2, v0}, LX/6tM;->a(Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/payments/checkout/model/SendPaymentCheckoutResult;LX/6uW;)Lcom/facebook/payments/confirmation/ConfirmationCommonParams;

    move-result-object v3

    check-cast p1, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;

    invoke-virtual {p1}, Lcom/facebook/payments/checkout/model/SimpleCheckoutData;->b()Lcom/facebook/payments/checkout/CheckoutParams;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/payments/InstantWorkflowsCheckoutParams;

    iget-object v0, v0, Lcom/facebook/pages/common/platform/payments/InstantWorkflowsCheckoutParams;->b:Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformCheckOutProviderInfoFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformCheckOutProviderInfoFragmentModel;->c()Ljava/lang/String;

    move-result-object v4

    check-cast p2, Lcom/facebook/payments/checkout/model/SimpleSendPaymentCheckoutResult;

    .line 1896450
    iget-object v0, p2, Lcom/facebook/payments/checkout/model/SimpleSendPaymentCheckoutResult;->b:LX/0lF;

    move-object v0, v0

    .line 1896451
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    invoke-direct {v1, v2, v3, v4, v0}, Lcom/facebook/pages/common/platform/payments/InstantWorkflowsConfirmationParams;-><init>(LX/0lB;Lcom/facebook/payments/confirmation/ConfirmationCommonParams;Ljava/lang/String;LX/0lF;)V

    return-object v1
.end method

.method public final a(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;
    .locals 1

    .prologue
    .line 1896448
    iget-object v0, p0, LX/CUI;->a:LX/6tM;

    invoke-virtual {v0, p1}, LX/6tM;->a(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;)Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;
    .locals 1

    .prologue
    .line 1896447
    iget-object v0, p0, LX/CUI;->a:LX/6tM;

    invoke-virtual {v0, p1, p2}, LX/6tM;->a(Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/payments/paymentmethods/model/FbPaymentCard;)Lcom/facebook/payments/paymentmethods/cardform/CardFormParams;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;)Lcom/facebook/payments/picker/option/PaymentsPickerOptionPickerScreenConfig;
    .locals 1

    .prologue
    .line 1896446
    iget-object v0, p0, LX/CUI;->a:LX/6tM;

    invoke-virtual {v0, p1, p2}, LX/6tM;->a(Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;)Lcom/facebook/payments/picker/option/PaymentsPickerOptionPickerScreenConfig;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/facebook/payments/contactinfo/picker/ContactInfoPickerScreenConfig;
    .locals 1

    .prologue
    .line 1896445
    iget-object v0, p0, LX/CUI;->a:LX/6tM;

    invoke-virtual {v0, p1}, LX/6tM;->b(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/facebook/payments/contactinfo/picker/ContactInfoPickerScreenConfig;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;)Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;
    .locals 1

    .prologue
    .line 1896444
    iget-object v0, p0, LX/CUI;->a:LX/6tM;

    invoke-virtual {v0, p1, p2}, LX/6tM;->b(Lcom/facebook/payments/checkout/model/CheckoutData;Lcom/facebook/payments/checkout/configuration/model/CheckoutOptionsPurchaseInfoExtension;)Lcom/facebook/payments/selector/model/PaymentsSelectorScreenParams;

    move-result-object v0

    return-object v0
.end method

.method public final c(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/facebook/payments/contactinfo/picker/ContactInfoPickerScreenConfig;
    .locals 1

    .prologue
    .line 1896438
    iget-object v0, p0, LX/CUI;->a:LX/6tM;

    invoke-virtual {v0, p1}, LX/6tM;->c(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/facebook/payments/contactinfo/picker/ContactInfoPickerScreenConfig;

    move-result-object v0

    return-object v0
.end method

.method public final d(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;
    .locals 1

    .prologue
    .line 1896443
    iget-object v0, p0, LX/CUI;->a:LX/6tM;

    invoke-virtual {v0, p1}, LX/6tM;->d(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/facebook/payments/contactinfo/form/ContactInfoCommonFormParams;

    move-result-object v0

    return-object v0
.end method

.method public final e(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/facebook/payments/shipping/model/ShippingParams;
    .locals 1

    .prologue
    .line 1896442
    iget-object v0, p0, LX/CUI;->a:LX/6tM;

    invoke-virtual {v0, p1}, LX/6tM;->e(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/facebook/payments/shipping/model/ShippingParams;

    move-result-object v0

    return-object v0
.end method

.method public final f(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/facebook/payments/shipping/addresspicker/ShippingPickerScreenConfig;
    .locals 1

    .prologue
    .line 1896441
    iget-object v0, p0, LX/CUI;->a:LX/6tM;

    invoke-virtual {v0, p1}, LX/6tM;->f(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/facebook/payments/shipping/addresspicker/ShippingPickerScreenConfig;

    move-result-object v0

    return-object v0
.end method

.method public final g(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/facebook/payments/shipping/optionpicker/ShippingOptionPickerScreenConfig;
    .locals 1

    .prologue
    .line 1896440
    iget-object v0, p0, LX/CUI;->a:LX/6tM;

    invoke-virtual {v0, p1}, LX/6tM;->g(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/facebook/payments/shipping/optionpicker/ShippingOptionPickerScreenConfig;

    move-result-object v0

    return-object v0
.end method

.method public final h(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/facebook/payments/paymentmethods/picker/PaymentMethodsPickerScreenConfig;
    .locals 1

    .prologue
    .line 1896439
    iget-object v0, p0, LX/CUI;->a:LX/6tM;

    invoke-virtual {v0, p1}, LX/6tM;->h(Lcom/facebook/payments/checkout/model/CheckoutData;)Lcom/facebook/payments/paymentmethods/picker/PaymentMethodsPickerScreenConfig;

    move-result-object v0

    return-object v0
.end method
