.class public LX/C4p;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pe;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pt;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/C4q;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/C4p",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/C4q;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1847967
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1847968
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/C4p;->b:LX/0Zi;

    .line 1847969
    iput-object p1, p0, LX/C4p;->a:LX/0Ot;

    .line 1847970
    return-void
.end method

.method public static a(LX/0QB;)LX/C4p;
    .locals 4

    .prologue
    .line 1847971
    const-class v1, LX/C4p;

    monitor-enter v1

    .line 1847972
    :try_start_0
    sget-object v0, LX/C4p;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1847973
    sput-object v2, LX/C4p;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1847974
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1847975
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1847976
    new-instance v3, LX/C4p;

    const/16 p0, 0x1f4d

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/C4p;-><init>(LX/0Ot;)V

    .line 1847977
    move-object v0, v3

    .line 1847978
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1847979
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C4p;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1847980
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1847981
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 3

    .prologue
    .line 1847956
    check-cast p2, LX/C4o;

    .line 1847957
    iget-object v0, p0, LX/C4p;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/C4q;

    iget-object v1, p2, LX/C4o;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/C4o;->b:LX/1Pe;

    const/4 p2, 0x1

    .line 1847958
    iget-object p0, v0, LX/C4q;->a:LX/CDQ;

    invoke-virtual {p0, p1}, LX/CDQ;->c(LX/1De;)LX/CDO;

    move-result-object p0

    invoke-virtual {p0, v1}, LX/CDO;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/CDO;

    move-result-object p0

    invoke-virtual {p0, v2}, LX/CDO;->a(LX/1Pe;)LX/CDO;

    move-result-object p0

    .line 1847959
    iget-object v0, p0, LX/CDO;->a:LX/CDP;

    iput-boolean p2, v0, LX/CDP;->e:Z

    .line 1847960
    move-object p0, p0

    .line 1847961
    iget-object v0, p0, LX/CDO;->a:LX/CDP;

    iput-boolean p2, v0, LX/CDP;->f:Z

    .line 1847962
    move-object p0, p0

    .line 1847963
    iget-object v0, p0, LX/CDO;->a:LX/CDP;

    iput-boolean p2, v0, LX/CDP;->d:Z

    .line 1847964
    move-object p0, p0

    .line 1847965
    sget-object p2, LX/04D;->UNKNOWN:LX/04D;

    invoke-virtual {p0, p2}, LX/CDO;->a(LX/04D;)LX/CDO;

    move-result-object p0

    invoke-virtual {p0}, LX/1X5;->b()LX/1Dg;

    move-result-object p0

    move-object v0, p0

    .line 1847966
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1847954
    invoke-static {}, LX/1dS;->b()V

    .line 1847955
    const/4 v0, 0x0

    return-object v0
.end method
