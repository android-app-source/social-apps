.class public LX/C01;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/C01;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1839782
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1839783
    return-void
.end method

.method public static a(LX/0QB;)LX/C01;
    .locals 3

    .prologue
    .line 1839795
    sget-object v0, LX/C01;->a:LX/C01;

    if-nez v0, :cond_1

    .line 1839796
    const-class v1, LX/C01;

    monitor-enter v1

    .line 1839797
    :try_start_0
    sget-object v0, LX/C01;->a:LX/C01;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1839798
    if-eqz v2, :cond_0

    .line 1839799
    :try_start_1
    new-instance v0, LX/C01;

    invoke-direct {v0}, LX/C01;-><init>()V

    .line 1839800
    move-object v0, v0

    .line 1839801
    sput-object v0, LX/C01;->a:LX/C01;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1839802
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1839803
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1839804
    :cond_1
    sget-object v0, LX/C01;->a:LX/C01;

    return-object v0

    .line 1839805
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1839806
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static final a(ILcom/facebook/resources/ui/EllipsizingTextView;Lcom/facebook/resources/ui/EllipsizingTextView;)V
    .locals 5

    .prologue
    .line 1839784
    invoke-virtual {p1}, Lcom/facebook/resources/ui/EllipsizingTextView;->getLineHeight()I

    move-result v2

    .line 1839785
    invoke-virtual {p2}, Lcom/facebook/resources/ui/EllipsizingTextView;->getLineHeight()I

    move-result v3

    .line 1839786
    invoke-virtual {p1}, Lcom/facebook/resources/ui/EllipsizingTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1839787
    invoke-virtual {p2}, Lcom/facebook/resources/ui/EllipsizingTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1839788
    iget v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v0, v4

    .line 1839789
    iget v4, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget v1, v1, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v1, v4

    .line 1839790
    sub-int v0, p0, v0

    sub-int/2addr v0, v1

    sub-int/2addr v0, v3

    .line 1839791
    div-int/2addr v0, v2

    move v0, v0

    .line 1839792
    invoke-virtual {p1, v0}, Lcom/facebook/resources/ui/EllipsizingTextView;->setMaxLines(I)V

    .line 1839793
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Lcom/facebook/resources/ui/EllipsizingTextView;->setMaxLines(I)V

    .line 1839794
    return-void
.end method
