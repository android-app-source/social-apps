.class public LX/AYh;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private final b:LX/0SG;

.field private final c:LX/0tX;

.field private final d:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation
.end field

.field public final e:LX/03V;

.field private final f:Landroid/content/Context;

.field public final g:Ljava/lang/String;

.field private final h:J

.field private i:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveVideoViewersQueryModel;",
            ">;>;"
        }
    .end annotation
.end field

.field private final j:LX/AX5;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1686027
    const-class v0, LX/AYh;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/AYh;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0SG;LX/0tX;LX/03V;Ljava/util/concurrent/ExecutorService;Ljava/lang/String;JLX/AX5;Landroid/content/Context;)V
    .locals 0
    .param p4    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # J
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p8    # LX/AX5;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p9    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1686017
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1686018
    iput-object p1, p0, LX/AYh;->b:LX/0SG;

    .line 1686019
    iput-object p2, p0, LX/AYh;->c:LX/0tX;

    .line 1686020
    iput-object p4, p0, LX/AYh;->d:Ljava/util/concurrent/ExecutorService;

    .line 1686021
    iput-object p3, p0, LX/AYh;->e:LX/03V;

    .line 1686022
    iput-object p9, p0, LX/AYh;->f:Landroid/content/Context;

    .line 1686023
    iput-object p5, p0, LX/AYh;->g:Ljava/lang/String;

    .line 1686024
    iput-wide p6, p0, LX/AYh;->h:J

    .line 1686025
    iput-object p8, p0, LX/AYh;->j:LX/AX5;

    .line 1686026
    return-void
.end method

.method public static a$redex0(LX/AYh;Ljava/util/List;ILcom/facebook/graphql/model/GraphQLFeedback;)V
    .locals 1
    .param p0    # LX/AYh;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/AcC;",
            ">;I",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1685981
    iget-object v0, p0, LX/AYh;->j:LX/AX5;

    invoke-interface {v0, p1, p2, p3}, LX/AX5;->a(Ljava/util/List;ILcom/facebook/graphql/model/GraphQLFeedback;)V

    .line 1685982
    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;I)Landroid/text/SpannableString;
    .locals 8
    .param p1    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/AcC;",
            ">;I)",
            "Landroid/text/SpannableString;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v7, 0x2

    const/4 v5, 0x0

    const/4 v6, 0x1

    .line 1685995
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move-object v0, v1

    .line 1685996
    :goto_0
    return-object v0

    .line 1685997
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge p2, v0, :cond_2

    .line 1685998
    iget-object v0, p0, LX/AYh;->e:LX/03V;

    sget-object v2, LX/AYh;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "verifiedViewCount("

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") < viewers.size("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1685999
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p2

    .line 1686000
    :cond_2
    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AcC;

    .line 1686001
    iget-object v2, p0, LX/AYh;->f:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    sget-object v3, LX/03r;->FacecastFacepile:[I

    const v4, 0x7f010466

    invoke-virtual {v2, v1, v3, v4, v5}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 1686002
    const/16 v2, 0x0

    invoke-virtual {v1, v2, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    .line 1686003
    const/16 v3, 0x1

    invoke-virtual {v1, v3, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    .line 1686004
    const/16 v4, 0x2

    invoke-virtual {v1, v4, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v4

    .line 1686005
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 1686006
    new-instance v1, LX/47x;

    iget-object v5, p0, LX/AYh;->f:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-direct {v1, v5}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    .line 1686007
    if-ne p2, v6, :cond_3

    .line 1686008
    invoke-virtual {v1, v2}, LX/47x;->a(I)LX/47x;

    .line 1686009
    iget-object v0, v0, LX/AcC;->a:Ljava/lang/String;

    iget-object v2, p0, LX/AYh;->f:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v2}, LX/3Hk;->a(Landroid/content/res/Resources;)Ljava/util/List;

    move-result-object v2

    invoke-static {v6, v0, v2, v1}, LX/3Hk;->a(ILjava/lang/String;Ljava/util/List;LX/47x;)V

    .line 1686010
    :goto_1
    invoke-virtual {v1}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v0

    goto/16 :goto_0

    .line 1686011
    :cond_3
    if-ne p2, v7, :cond_4

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-le v2, v6, :cond_4

    .line 1686012
    invoke-virtual {v1, v3}, LX/47x;->a(I)LX/47x;

    .line 1686013
    iget-object v0, v0, LX/AcC;->a:Ljava/lang/String;

    iget-object v2, p0, LX/AYh;->f:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v2}, LX/3Hk;->a(Landroid/content/res/Resources;)Ljava/util/List;

    move-result-object v2

    invoke-static {v6, v0, v2, v1}, LX/3Hk;->a(ILjava/lang/String;Ljava/util/List;LX/47x;)V

    .line 1686014
    invoke-interface {p1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AcC;

    iget-object v0, v0, LX/AcC;->a:Ljava/lang/String;

    iget-object v2, p0, LX/AYh;->f:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v2}, LX/3Hk;->a(Landroid/content/res/Resources;)Ljava/util/List;

    move-result-object v2

    invoke-static {v7, v0, v2, v1}, LX/3Hk;->a(ILjava/lang/String;Ljava/util/List;LX/47x;)V

    goto :goto_1

    .line 1686015
    :cond_4
    invoke-virtual {v1, v4}, LX/47x;->a(I)LX/47x;

    .line 1686016
    iget-object v0, v0, LX/AcC;->a:Ljava/lang/String;

    iget-object v2, p0, LX/AYh;->f:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v2}, LX/3Hk;->a(Landroid/content/res/Resources;)Ljava/util/List;

    move-result-object v2

    invoke-static {v6, v0, v2, v1}, LX/3Hk;->a(ILjava/lang/String;Ljava/util/List;LX/47x;)V

    goto :goto_1
.end method

.method public final a()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x3e8

    .line 1685986
    iget-object v0, p0, LX/AYh;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    div-long/2addr v0, v6

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 1685987
    invoke-static {}, LX/6SN;->d()LX/6SL;

    move-result-object v1

    .line 1685988
    const-string v2, "targetID"

    iget-object v3, p0, LX/AYh;->g:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1685989
    const-string v2, "after_timestamp"

    iget-wide v4, p0, LX/AYh;->h:J

    div-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1685990
    const-string v2, "before_timestamp"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1685991
    const-string v0, "limit"

    const/16 v2, 0x20

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1685992
    iget-object v0, p0, LX/AYh;->c:LX/0tX;

    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    iput-object v0, p0, LX/AYh;->i:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1685993
    iget-object v0, p0, LX/AYh;->i:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v1, LX/AYg;

    invoke-direct {v1, p0}, LX/AYg;-><init>(LX/AYh;)V

    iget-object v2, p0, LX/AYh;->d:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1685994
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1685983
    iget-object v0, p0, LX/AYh;->i:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    .line 1685984
    iget-object v0, p0, LX/AYh;->i:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 1685985
    :cond_0
    return-void
.end method
