.class public LX/BQD;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/BQD;


# instance fields
.field public final a:LX/11i;

.field public final b:LX/0id;

.field public final c:LX/0So;


# direct methods
.method public constructor <init>(LX/11i;LX/0id;LX/0So;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1781963
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1781964
    iput-object p1, p0, LX/BQD;->a:LX/11i;

    .line 1781965
    iput-object p2, p0, LX/BQD;->b:LX/0id;

    .line 1781966
    iput-object p3, p0, LX/BQD;->c:LX/0So;

    .line 1781967
    return-void
.end method

.method public static a(LX/0QB;)LX/BQD;
    .locals 6

    .prologue
    .line 1781950
    sget-object v0, LX/BQD;->d:LX/BQD;

    if-nez v0, :cond_1

    .line 1781951
    const-class v1, LX/BQD;

    monitor-enter v1

    .line 1781952
    :try_start_0
    sget-object v0, LX/BQD;->d:LX/BQD;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1781953
    if-eqz v2, :cond_0

    .line 1781954
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1781955
    new-instance p0, LX/BQD;

    invoke-static {v0}, LX/11h;->a(LX/0QB;)LX/11h;

    move-result-object v3

    check-cast v3, LX/11i;

    invoke-static {v0}, LX/0id;->a(LX/0QB;)LX/0id;

    move-result-object v4

    check-cast v4, LX/0id;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v5

    check-cast v5, LX/0So;

    invoke-direct {p0, v3, v4, v5}, LX/BQD;-><init>(LX/11i;LX/0id;LX/0So;)V

    .line 1781956
    move-object v0, p0

    .line 1781957
    sput-object v0, LX/BQD;->d:LX/BQD;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1781958
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1781959
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1781960
    :cond_1
    sget-object v0, LX/BQD;->d:LX/BQD;

    return-object v0

    .line 1781961
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1781962
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/BQD;ZZ)V
    .locals 4

    .prologue
    .line 1781945
    iget-object v0, p0, LX/BQD;->a:LX/11i;

    sget-object v1, LX/BQF;->a:LX/BQE;

    invoke-interface {v0, v1}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v0

    .line 1781946
    if-eqz v0, :cond_0

    .line 1781947
    const-string v0, "is_failed"

    invoke-static {p1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v1

    const-string v2, "is_cancelled"

    invoke-static {p2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v0

    .line 1781948
    iget-object v1, p0, LX/BQD;->a:LX/11i;

    sget-object v2, LX/BQF;->a:LX/BQE;

    invoke-interface {v1, v2, v0}, LX/11i;->b(LX/0Pq;LX/0P1;)V

    .line 1781949
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1781941
    iget-object v0, p0, LX/BQD;->a:LX/11i;

    sget-object v1, LX/BQF;->a:LX/BQE;

    invoke-interface {v0, v1}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v0

    .line 1781942
    if-eqz v0, :cond_0

    .line 1781943
    const v1, 0x497eaa32

    invoke-static {v0, p1, v1}, LX/096;->a(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 1781944
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;LX/0P1;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1781968
    iget-object v0, p0, LX/BQD;->a:LX/11i;

    sget-object v1, LX/BQF;->a:LX/BQE;

    invoke-interface {v0, v1}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v0

    .line 1781969
    if-eqz v0, :cond_0

    .line 1781970
    const/4 v2, 0x0

    iget-object v1, p0, LX/BQD;->c:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v4

    const v6, 0x26ad33c4

    move-object v1, p1

    move-object v3, p2

    invoke-static/range {v0 .. v6}, LX/096;->a(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;JI)LX/11o;

    .line 1781971
    :cond_0
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1781937
    iget-object v0, p0, LX/BQD;->a:LX/11i;

    sget-object v1, LX/BQF;->a:LX/BQE;

    invoke-interface {v0, v1}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v0

    .line 1781938
    if-eqz v0, :cond_0

    .line 1781939
    const v1, -0x17a5a8a0

    invoke-static {v0, p1, v1}, LX/096;->b(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 1781940
    :cond_0
    return-void
.end method

.method public final b(Ljava/lang/String;LX/0P1;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1781933
    iget-object v0, p0, LX/BQD;->a:LX/11i;

    sget-object v1, LX/BQF;->a:LX/BQE;

    invoke-interface {v0, v1}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v0

    .line 1781934
    if-eqz v0, :cond_0

    .line 1781935
    const/4 v2, 0x0

    iget-object v1, p0, LX/BQD;->c:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v4

    const v6, 0x68f095f

    move-object v1, p1

    move-object v3, p2

    invoke-static/range {v0 .. v6}, LX/096;->b(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;JI)LX/11o;

    .line 1781936
    :cond_0
    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1781921
    iget-object v0, p0, LX/BQD;->a:LX/11i;

    sget-object v1, LX/BQF;->a:LX/BQE;

    invoke-interface {v0, v1}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v0

    .line 1781922
    if-eqz v0, :cond_0

    .line 1781923
    const v1, 0xbe6ae90

    invoke-static {v0, p1, v1}, LX/096;->d(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 1781924
    :cond_0
    return-void
.end method

.method public final d(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1781929
    iget-object v0, p0, LX/BQD;->a:LX/11i;

    sget-object v1, LX/BQF;->a:LX/BQE;

    invoke-interface {v0, v1}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v0

    .line 1781930
    if-eqz v0, :cond_0

    .line 1781931
    const v1, 0x13fdb2ed

    invoke-static {v0, p1, v1}, LX/096;->c(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 1781932
    :cond_0
    return-void
.end method

.method public final f(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1781925
    iget-object v0, p0, LX/BQD;->a:LX/11i;

    sget-object v1, LX/BQF;->a:LX/BQE;

    invoke-interface {v0, v1}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v0

    .line 1781926
    if-eqz v0, :cond_0

    .line 1781927
    const v1, -0x53da4762

    invoke-static {v0, p1, v1}, LX/096;->e(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 1781928
    :cond_0
    return-void
.end method
