.class public final LX/BE1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3Cf;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/3Cf",
        "<",
        "Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;",
        ">;"
    }
.end annotation


# instance fields
.field public a:I

.field private b:Ljava/lang/String;

.field public c:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1764031
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1764032
    iput v0, p0, LX/BE1;->a:I

    .line 1764033
    iput-object p1, p0, LX/BE1;->b:Ljava/lang/String;

    .line 1764034
    iput-boolean v0, p0, LX/BE1;->c:Z

    .line 1764035
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1764029
    check-cast p1, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;

    .line 1764030
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 1764023
    check-cast p1, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;

    .line 1764024
    iget-boolean v0, p0, LX/BE1;->c:Z

    if-eqz v0, :cond_0

    .line 1764025
    :goto_0
    return-void

    .line 1764026
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/BE1;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1764027
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/BE1;->c:Z

    goto :goto_0

    .line 1764028
    :cond_1
    iget v0, p0, LX/BE1;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/BE1;->a:I

    goto :goto_0
.end method
