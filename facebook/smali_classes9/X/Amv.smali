.class public final enum LX/Amv;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Amv;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Amv;

.field public static final enum COMMENT:LX/Amv;

.field public static final enum LIKE:LX/Amv;

.field public static final enum SHARE:LX/Amv;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1711666
    new-instance v0, LX/Amv;

    const-string v1, "LIKE"

    invoke-direct {v0, v1, v2}, LX/Amv;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Amv;->LIKE:LX/Amv;

    .line 1711667
    new-instance v0, LX/Amv;

    const-string v1, "COMMENT"

    invoke-direct {v0, v1, v3}, LX/Amv;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Amv;->COMMENT:LX/Amv;

    .line 1711668
    new-instance v0, LX/Amv;

    const-string v1, "SHARE"

    invoke-direct {v0, v1, v4}, LX/Amv;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Amv;->SHARE:LX/Amv;

    .line 1711669
    const/4 v0, 0x3

    new-array v0, v0, [LX/Amv;

    sget-object v1, LX/Amv;->LIKE:LX/Amv;

    aput-object v1, v0, v2

    sget-object v1, LX/Amv;->COMMENT:LX/Amv;

    aput-object v1, v0, v3

    sget-object v1, LX/Amv;->SHARE:LX/Amv;

    aput-object v1, v0, v4

    sput-object v0, LX/Amv;->$VALUES:[LX/Amv;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1711670
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Amv;
    .locals 1

    .prologue
    .line 1711665
    const-class v0, LX/Amv;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Amv;

    return-object v0
.end method

.method public static values()[LX/Amv;
    .locals 1

    .prologue
    .line 1711664
    sget-object v0, LX/Amv;->$VALUES:[LX/Amv;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Amv;

    return-object v0
.end method
