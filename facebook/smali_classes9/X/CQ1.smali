.class public final LX/CQ1;
.super LX/1n4;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1n4",
        "<",
        "Landroid/graphics/drawable/Drawable;",
        ">;"
    }
.end annotation


# static fields
.field private static a:LX/CQ1;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/CPz;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/CQ2;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1885352
    const/4 v0, 0x0

    sput-object v0, LX/CQ1;->a:LX/CQ1;

    .line 1885353
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/CQ1;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1885349
    invoke-direct {p0}, LX/1n4;-><init>()V

    .line 1885350
    new-instance v0, LX/CQ2;

    invoke-direct {v0}, LX/CQ2;-><init>()V

    iput-object v0, p0, LX/CQ1;->c:LX/CQ2;

    .line 1885351
    return-void
.end method

.method public static declared-synchronized a()LX/CQ1;
    .locals 2

    .prologue
    .line 1885345
    const-class v1, LX/CQ1;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/CQ1;->a:LX/CQ1;

    if-nez v0, :cond_0

    .line 1885346
    new-instance v0, LX/CQ1;

    invoke-direct {v0}, LX/CQ1;-><init>()V

    sput-object v0, LX/CQ1;->a:LX/CQ1;

    .line 1885347
    :cond_0
    sget-object v0, LX/CQ1;->a:LX/CQ1;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 1885348
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1dc;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1885332
    check-cast p2, LX/CQ0;

    .line 1885333
    iget v0, p2, LX/CQ0;->a:I

    iget v1, p2, LX/CQ0;->b:I

    iget v2, p2, LX/CQ0;->c:I

    iget v3, p2, LX/CQ0;->d:I

    .line 1885334
    sget-object p0, LX/CQ2;->a:LX/0Zk;

    invoke-interface {p0}, LX/0Zk;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/graphics/drawable/GradientDrawable;

    .line 1885335
    if-nez p0, :cond_0

    .line 1885336
    new-instance p0, Landroid/graphics/drawable/GradientDrawable;

    invoke-direct {p0}, Landroid/graphics/drawable/GradientDrawable;-><init>()V

    .line 1885337
    :cond_0
    invoke-virtual {p0, v2, v1}, Landroid/graphics/drawable/GradientDrawable;->setStroke(II)V

    .line 1885338
    int-to-float p1, v3

    invoke-virtual {p0, p1}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    .line 1885339
    invoke-virtual {p0, v0}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 1885340
    move-object v0, p0

    .line 1885341
    return-object v0
.end method

.method public final a(LX/1De;Ljava/lang/Object;LX/1dc;)V
    .locals 0

    .prologue
    .line 1885342
    check-cast p2, Landroid/graphics/drawable/Drawable;

    .line 1885343
    sget-object p0, LX/CQ2;->a:LX/0Zk;

    check-cast p2, Landroid/graphics/drawable/GradientDrawable;

    invoke-interface {p0, p2}, LX/0Zk;->a(Ljava/lang/Object;)Z

    .line 1885344
    return-void
.end method
