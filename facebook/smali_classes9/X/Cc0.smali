.class public final LX/Cc0;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "LX/175;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/5kD;

.field public final synthetic b:LX/CcO;


# direct methods
.method public constructor <init>(LX/CcO;LX/5kD;)V
    .locals 0

    .prologue
    .line 1920585
    iput-object p1, p0, LX/Cc0;->b:LX/CcO;

    iput-object p2, p0, LX/Cc0;->a:LX/5kD;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1920586
    iget-object v0, p0, LX/Cc0;->b:LX/CcO;

    iget-object v0, v0, LX/CcO;->a:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->k:LX/03V;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_dialog_failed"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, LX/03V;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1920587
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 1920588
    check-cast p1, LX/175;

    .line 1920589
    iget-object v0, p0, LX/Cc0;->b:LX/CcO;

    iget-object v0, v0, LX/CcO;->a:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->i:LX/9hh;

    iget-object v1, p0, LX/Cc0;->a:LX/5kD;

    invoke-interface {v1}, LX/5kD;->d()Ljava/lang/String;

    move-result-object v1

    const/4 p0, 0x0

    .line 1920590
    new-instance v2, LX/9hY;

    invoke-direct {v2, v0, v1, p1}, LX/9hY;-><init>(LX/9hh;Ljava/lang/String;LX/175;)V

    .line 1920591
    iget-object v3, v0, LX/9hh;->f:LX/0Uh;

    const/16 v4, 0x405

    invoke-virtual {v3, v4, p0}, LX/0Uh;->a(IZ)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1920592
    const/4 v3, 0x2

    new-array v3, v3, [LX/4VT;

    new-instance v4, LX/9i3;

    invoke-static {p1}, LX/9JZ;->a(LX/175;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    invoke-direct {v4, v1, v5}, LX/9i3;-><init>(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLTextWithEntities;)V

    aput-object v4, v3, p0

    const/4 v4, 0x1

    new-instance v5, LX/9i2;

    invoke-static {p1}, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;->a(LX/175;)Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object p0

    invoke-direct {v5, v1, p0}, LX/9i2;-><init>(Ljava/lang/String;Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;)V

    aput-object v5, v3, v4

    invoke-static {v0, v2, v3}, LX/9hh;->a(LX/9hh;Ljava/util/concurrent/Callable;[LX/4VT;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1920593
    :goto_0
    return-void

    :cond_0
    iget-object v3, v0, LX/9hh;->d:LX/9hs;

    .line 1920594
    new-instance v4, LX/9hk;

    new-instance v5, LX/9hm;

    invoke-direct {v5, v3, v1, p1}, LX/9hm;-><init>(LX/9hs;Ljava/lang/String;LX/175;)V

    invoke-direct {v4, v1, v5}, LX/9hk;-><init>(Ljava/lang/String;LX/9hl;)V

    move-object v3, v4

    .line 1920595
    invoke-static {v0, v2, v3}, LX/9hh;->a(LX/9hh;Ljava/util/concurrent/Callable;LX/9hk;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0
.end method
