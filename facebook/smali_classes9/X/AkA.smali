.class public LX/AkA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/24J;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ":",
        "LX/1aQ;",
        ">",
        "Ljava/lang/Object;",
        "LX/24J",
        "<",
        "LX/24P;",
        "TV;>;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field public final b:LX/1Rg;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1Rg;)V
    .locals 0

    .prologue
    .line 1708806
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1708807
    iput-object p1, p0, LX/AkA;->a:Landroid/content/Context;

    .line 1708808
    iput-object p2, p0, LX/AkA;->b:LX/1Rg;

    .line 1708809
    return-void
.end method

.method public static a(LX/AkA;Landroid/view/View;Z)Landroid/animation/Animator$AnimatorListener;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;Z)",
            "Landroid/animation/Animator$AnimatorListener;"
        }
    .end annotation

    .prologue
    .line 1708810
    new-instance v0, LX/Ak9;

    invoke-direct {v0, p0, p2, p1}, LX/Ak9;-><init>(LX/AkA;ZLandroid/view/View;)V

    return-object v0
.end method

.method public static b(LX/AkA;)I
    .locals 3

    .prologue
    .line 1708811
    iget-object v0, p0, LX/AkA;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b11aa

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iget-object v1, p0, LX/AkA;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b11a9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Ljava/lang/Object;Landroid/view/View;)Ljava/lang/Runnable;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 1708812
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/util/List;Ljava/lang/Object;Ljava/lang/Object;Landroid/view/View;)V
    .locals 11

    .prologue
    .line 1708813
    check-cast p2, LX/24P;

    check-cast p3, LX/24P;

    .line 1708814
    if-ne p2, p3, :cond_1

    .line 1708815
    :cond_0
    :goto_0
    return-void

    .line 1708816
    :cond_1
    sget-object v0, LX/24P;->MAXIMIZED:LX/24P;

    if-ne p2, v0, :cond_2

    .line 1708817
    iget-object v0, p0, LX/AkA;->b:LX/1Rg;

    const-wide/16 v2, 0xc8

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v3, v1}, LX/1Rg;->a(JLandroid/animation/Animator$AnimatorListener;)LX/8sj;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1708818
    const/4 v9, 0x0

    .line 1708819
    iget-object v4, p0, LX/AkA;->b:LX/1Rg;

    const-wide/16 v6, 0xc8

    move-object v5, p4

    check-cast v5, LX/1aQ;

    invoke-interface {v5}, LX/1aQ;->getExpandedFlyoutHeight()I

    move-result v5

    invoke-static {p0}, LX/AkA;->b(LX/AkA;)I

    move-result v8

    add-int/2addr v8, v5

    invoke-static {p0, p4, v9}, LX/AkA;->a(LX/AkA;Landroid/view/View;Z)Landroid/animation/Animator$AnimatorListener;

    move-result-object v10

    move-object v5, p4

    invoke-virtual/range {v4 .. v10}, LX/1Rg;->a(Landroid/view/View;JIILandroid/animation/Animator$AnimatorListener;)LX/8sj;

    move-result-object v4

    move-object v0, v4

    .line 1708820
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1708821
    :cond_2
    sget-object v0, LX/24P;->MAXIMIZED:LX/24P;

    if-ne p3, v0, :cond_0

    .line 1708822
    iget-object v4, p0, LX/AkA;->b:LX/1Rg;

    const-wide/16 v6, 0xc8

    const/4 v8, 0x0

    move-object v5, p4

    check-cast v5, LX/1aQ;

    invoke-interface {v5}, LX/1aQ;->getExpandedFlyoutHeight()I

    move-result v5

    invoke-static {p0}, LX/AkA;->b(LX/AkA;)I

    move-result v9

    add-int/2addr v9, v5

    const/4 v5, 0x1

    invoke-static {p0, p4, v5}, LX/AkA;->a(LX/AkA;Landroid/view/View;Z)Landroid/animation/Animator$AnimatorListener;

    move-result-object v10

    move-object v5, p4

    invoke-virtual/range {v4 .. v10}, LX/1Rg;->a(Landroid/view/View;JIILandroid/animation/Animator$AnimatorListener;)LX/8sj;

    move-result-object v4

    move-object v0, v4

    .line 1708823
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method
