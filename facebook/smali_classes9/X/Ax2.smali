.class public LX/Ax2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/AlW;


# instance fields
.field private a:Landroid/content/Context;

.field private b:Lcom/facebook/productionprompts/model/ProductionPrompt;

.field private c:Lcom/facebook/friendsharing/meme/prompt/MemePromptView;

.field private d:LX/1RN;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1RN;)V
    .locals 2
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1727464
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1727465
    invoke-static {p2}, LX/1RN;->a(LX/1RN;)LX/1kK;

    move-result-object v0

    .line 1727466
    instance-of v1, v0, LX/1kW;

    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 1727467
    iput-object p1, p0, LX/Ax2;->a:Landroid/content/Context;

    .line 1727468
    iput-object p2, p0, LX/Ax2;->d:LX/1RN;

    .line 1727469
    check-cast v0, LX/1kW;

    .line 1727470
    iget-object v1, v0, LX/1kW;->a:Lcom/facebook/productionprompts/model/ProductionPrompt;

    move-object v0, v1

    .line 1727471
    iput-object v0, p0, LX/Ax2;->b:Lcom/facebook/productionprompts/model/ProductionPrompt;

    .line 1727472
    new-instance v0, Lcom/facebook/friendsharing/meme/prompt/MemePromptView;

    iget-object v1, p0, LX/Ax2;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/facebook/friendsharing/meme/prompt/MemePromptView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/Ax2;->c:Lcom/facebook/friendsharing/meme/prompt/MemePromptView;

    .line 1727473
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1727474
    iget-object v0, p0, LX/Ax2;->b:Lcom/facebook/productionprompts/model/ProductionPrompt;

    invoke-virtual {v0}, Lcom/facebook/productionprompts/model/ProductionPrompt;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1727475
    iget-object v0, p0, LX/Ax2;->b:Lcom/facebook/productionprompts/model/ProductionPrompt;

    invoke-virtual {v0}, Lcom/facebook/productionprompts/model/ProductionPrompt;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/Integer;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1727476
    const/4 v0, 0x0

    return-object v0
.end method

.method public final d()Landroid/graphics/drawable/Drawable;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1727477
    const/4 v0, 0x0

    return-object v0
.end method

.method public final e()Landroid/graphics/drawable/Drawable;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1727478
    const/4 v0, 0x0

    return-object v0
.end method

.method public final f()Landroid/net/Uri;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1727479
    iget-object v0, p0, LX/Ax2;->b:Lcom/facebook/productionprompts/model/ProductionPrompt;

    invoke-virtual {v0}, Lcom/facebook/productionprompts/model/ProductionPrompt;->e()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public final g()LX/AkM;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1727480
    iget-object v0, p0, LX/Ax2;->c:Lcom/facebook/friendsharing/meme/prompt/MemePromptView;

    if-eqz v0, :cond_0

    .line 1727481
    iget-object v0, p0, LX/Ax2;->c:Lcom/facebook/friendsharing/meme/prompt/MemePromptView;

    iget-object v1, p0, LX/Ax2;->d:LX/1RN;

    invoke-virtual {v0, v1}, Lcom/facebook/friendsharing/meme/prompt/MemePromptView;->a(LX/1RN;)V

    .line 1727482
    :cond_0
    iget-object v0, p0, LX/Ax2;->c:Lcom/facebook/friendsharing/meme/prompt/MemePromptView;

    return-object v0
.end method

.method public final h()Lcom/facebook/productionprompts/model/PromptDisplayReason;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1727483
    const/4 v0, 0x0

    return-object v0
.end method
