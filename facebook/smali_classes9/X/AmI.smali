.class public final LX/AmI;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/AmH;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1710688
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 1710689
    iput-object p1, p0, LX/AmI;->a:Ljava/util/List;

    .line 1710690
    return-void
.end method

.method private static a(Landroid/view/ViewGroup;)LX/AmH;
    .locals 3

    .prologue
    .line 1710683
    new-instance v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 1710684
    const/4 v1, 0x2

    const/high16 v2, 0x41100000    # 9.0f

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1710685
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    const/high16 v2, 0x40800000    # 4.0f

    invoke-static {v1, v2}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v1

    .line 1710686
    invoke-virtual {v0, v1, v1, v1, v1}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 1710687
    new-instance v1, LX/AmH;

    invoke-direct {v1, v0}, LX/AmH;-><init>(Landroid/view/View;)V

    return-object v1
.end method

.method private a(LX/AmH;I)V
    .locals 8

    .prologue
    .line 1710691
    iget-object v1, p1, LX/AmH;->l:Landroid/widget/TextView;

    const-string v2, "[%d] %s"

    iget-object v0, p0, LX/AmI;->a:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iget-object v0, p0, LX/AmI;->a:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-static {v2, v3, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1710692
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 1

    .prologue
    .line 1710682
    invoke-static {p1}, LX/AmI;->a(Landroid/view/ViewGroup;)LX/AmH;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1a1;I)V
    .locals 0

    .prologue
    .line 1710681
    check-cast p1, LX/AmH;

    invoke-direct {p0, p1, p2}, LX/AmI;->a(LX/AmH;I)V

    return-void
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 1710680
    iget-object v0, p0, LX/AmI;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
