.class public final LX/BdM;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/BdN;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/CharSequence;

.field public b:Ljava/lang/CharSequence;

.field public c:Z

.field public d:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1803677
    invoke-static {}, LX/BdN;->q()LX/BdN;

    move-result-object v0

    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1803678
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/BdM;->c:Z

    .line 1803679
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1803699
    const-string v0, "DefaultEmptyComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1803680
    if-ne p0, p1, :cond_1

    .line 1803681
    :cond_0
    :goto_0
    return v0

    .line 1803682
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1803683
    goto :goto_0

    .line 1803684
    :cond_3
    check-cast p1, LX/BdM;

    .line 1803685
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1803686
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1803687
    if-eq v2, v3, :cond_0

    .line 1803688
    iget-object v2, p0, LX/BdM;->a:Ljava/lang/CharSequence;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/BdM;->a:Ljava/lang/CharSequence;

    iget-object v3, p1, LX/BdM;->a:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1803689
    goto :goto_0

    .line 1803690
    :cond_5
    iget-object v2, p1, LX/BdM;->a:Ljava/lang/CharSequence;

    if-nez v2, :cond_4

    .line 1803691
    :cond_6
    iget-object v2, p0, LX/BdM;->b:Ljava/lang/CharSequence;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/BdM;->b:Ljava/lang/CharSequence;

    iget-object v3, p1, LX/BdM;->b:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1803692
    goto :goto_0

    .line 1803693
    :cond_8
    iget-object v2, p1, LX/BdM;->b:Ljava/lang/CharSequence;

    if-nez v2, :cond_7

    .line 1803694
    :cond_9
    iget-boolean v2, p0, LX/BdM;->c:Z

    iget-boolean v3, p1, LX/BdM;->c:Z

    if-eq v2, v3, :cond_a

    move v0, v1

    .line 1803695
    goto :goto_0

    .line 1803696
    :cond_a
    iget-object v2, p0, LX/BdM;->d:Ljava/lang/Runnable;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/BdM;->d:Ljava/lang/Runnable;

    iget-object v3, p1, LX/BdM;->d:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1803697
    goto :goto_0

    .line 1803698
    :cond_b
    iget-object v2, p1, LX/BdM;->d:Ljava/lang/Runnable;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
