.class public LX/CEA;
.super Lcom/facebook/fbui/widget/layout/ImageBlockLayout;
.source ""


# instance fields
.field public j:LX/0wM;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public k:LX/6WW;

.field public l:LX/6WW;

.field public m:I

.field private n:Z

.field private o:Lcom/facebook/fig/facepile/FigFacepileView;

.field public p:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    .prologue
    .line 1860727
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;)V

    .line 1860728
    const-class v2, LX/CEA;

    invoke-static {v2, p0}, LX/CEA;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1860729
    new-instance v2, LX/6WW;

    invoke-direct {v2}, LX/6WW;-><init>()V

    iput-object v2, p0, LX/CEA;->k:LX/6WW;

    .line 1860730
    new-instance v2, LX/6WW;

    invoke-direct {v2}, LX/6WW;-><init>()V

    iput-object v2, p0, LX/CEA;->l:LX/6WW;

    .line 1860731
    invoke-virtual {p0}, LX/CEA;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 1860732
    iget-object v3, p0, LX/CEA;->k:LX/6WW;

    const v4, 0x7f0e0122

    invoke-virtual {v3, v2, v4}, LX/6WW;->a(Landroid/content/Context;I)V

    .line 1860733
    iget-object v3, p0, LX/CEA;->l:LX/6WW;

    const v4, 0x7f0e012d

    invoke-virtual {v3, v2, v4}, LX/6WW;->a(Landroid/content/Context;I)V

    .line 1860734
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 1860735
    const v4, 0x7f0b1cae

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 1860736
    const v5, 0x7f0b1caf

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    iput v5, p0, LX/CEA;->p:I

    .line 1860737
    const v5, 0x7f0b1cb0

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, p0, LX/CEA;->m:I

    .line 1860738
    iget v3, p0, LX/CEA;->p:I

    iget v5, p0, LX/CEA;->p:I

    invoke-super {p0, v3, v4, v5, v4}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setPadding(IIII)V

    .line 1860739
    iget v3, p0, LX/CEA;->p:I

    invoke-super {p0, v3}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailPadding(I)V

    .line 1860740
    iget v3, p0, LX/CEA;->m:I

    invoke-super {p0, v3}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailSize(I)V

    .line 1860741
    new-instance v3, Landroid/graphics/drawable/ColorDrawable;

    const v4, 0x7f0a0097

    invoke-static {v2, v4}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v4

    invoke-direct {v3, v4}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p0, v3}, LX/CEA;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1860742
    iget-object v3, p0, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->e:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v3}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1860743
    goto :goto_0

    .line 1860744
    :goto_0
    invoke-static {p0}, LX/CEA;->e(LX/CEA;)V

    .line 1860745
    return-void
    .line 1860746
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/CEA;

    invoke-static {p0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object p0

    check-cast p0, LX/0wM;

    iput-object p0, p1, LX/CEA;->j:LX/0wM;

    return-void
.end method

.method public static e(LX/CEA;)V
    .locals 2

    .prologue
    .line 1860803
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1860804
    iget-object v1, p0, LX/CEA;->k:LX/6WW;

    invoke-virtual {v1}, LX/6WW;->a()Ljava/lang/CharSequence;

    move-result-object v1

    move-object v1, v1

    .line 1860805
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1860806
    iget-object v1, p0, LX/CEA;->l:LX/6WW;

    invoke-virtual {v1}, LX/6WW;->a()Ljava/lang/CharSequence;

    move-result-object v1

    move-object v1, v1

    .line 1860807
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/CEA;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1860808
    return-void
.end method


# virtual methods
.method public final a(II)V
    .locals 3

    .prologue
    .line 1860790
    iget-object v0, p0, LX/CEA;->k:LX/6WW;

    invoke-virtual {v0, p1}, LX/6WW;->c(I)V

    .line 1860791
    const/4 v0, 0x0

    iget-object v1, p0, LX/CEA;->k:LX/6WW;

    invoke-virtual {v1}, LX/6WW;->b()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1860792
    iget-object v1, p0, LX/CEA;->k:LX/6WW;

    invoke-virtual {v1}, LX/6WW;->c()I

    move-result v1

    add-int/lit8 v1, v1, 0x0

    .line 1860793
    iget-object v2, p0, LX/CEA;->l:LX/6WW;

    invoke-virtual {v2, p1}, LX/6WW;->c(I)V

    .line 1860794
    iget-object v2, p0, LX/CEA;->l:LX/6WW;

    invoke-virtual {v2}, LX/6WW;->b()I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1860795
    iget-object v2, p0, LX/CEA;->l:LX/6WW;

    invoke-virtual {v2}, LX/6WW;->c()I

    move-result v2

    add-int/2addr v1, v2

    .line 1860796
    iget-boolean v2, p0, LX/CEA;->n:Z

    if-eqz v2, :cond_0

    .line 1860797
    iget-object v2, p0, LX/CEA;->o:Lcom/facebook/fig/facepile/FigFacepileView;

    invoke-virtual {v2, p1, p2}, Lcom/facebook/fig/facepile/FigFacepileView;->measure(II)V

    .line 1860798
    iget-object v2, p0, LX/CEA;->o:Lcom/facebook/fig/facepile/FigFacepileView;

    invoke-virtual {v2}, Lcom/facebook/fig/facepile/FigFacepileView;->getMeasuredWidth()I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1860799
    iget v2, p0, LX/CEA;->p:I

    add-int/2addr v1, v2

    .line 1860800
    iget-object v2, p0, LX/CEA;->o:Lcom/facebook/fig/facepile/FigFacepileView;

    invoke-virtual {v2}, Lcom/facebook/fig/facepile/FigFacepileView;->getMeasuredHeight()I

    move-result v2

    add-int/2addr v1, v2

    .line 1860801
    :cond_0
    invoke-virtual {p0, v0, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->b(II)V

    .line 1860802
    return-void
.end method

.method public final a(IIII)V
    .locals 3

    .prologue
    .line 1860776
    iget-object v0, p0, LX/CEA;->k:LX/6WW;

    invoke-virtual {v0}, LX/6WW;->c()I

    move-result v0

    iget-object v1, p0, LX/CEA;->l:LX/6WW;

    invoke-virtual {v1}, LX/6WW;->c()I

    move-result v1

    add-int/2addr v0, v1

    .line 1860777
    iget v1, p0, LX/CEA;->m:I

    if-ge v0, v1, :cond_2

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 1860778
    if-eqz v0, :cond_0

    .line 1860779
    iget-object v0, p0, LX/CEA;->k:LX/6WW;

    invoke-virtual {v0}, LX/6WW;->c()I

    move-result v0

    iget-object v1, p0, LX/CEA;->l:LX/6WW;

    invoke-virtual {v1}, LX/6WW;->c()I

    move-result v1

    add-int/2addr v0, v1

    .line 1860780
    iget v1, p0, LX/CEA;->m:I

    sub-int v0, v1, v0

    div-int/lit8 v0, v0, 0x2

    move v0, v0

    .line 1860781
    add-int/2addr p2, v0

    .line 1860782
    :cond_0
    iget-object v0, p0, LX/CEA;->k:LX/6WW;

    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->jx_()Z

    move-result v1

    invoke-virtual {v0, v1, p1, p2, p3}, LX/6WW;->a(ZIII)V

    .line 1860783
    iget-object v0, p0, LX/CEA;->k:LX/6WW;

    invoke-virtual {v0}, LX/6WW;->c()I

    move-result v0

    add-int/2addr v0, p2

    .line 1860784
    iget-object v1, p0, LX/CEA;->l:LX/6WW;

    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->jx_()Z

    move-result v2

    invoke-virtual {v1, v2, p1, v0, p3}, LX/6WW;->a(ZIII)V

    .line 1860785
    iget-object v1, p0, LX/CEA;->l:LX/6WW;

    invoke-virtual {v1}, LX/6WW;->c()I

    move-result v1

    add-int/2addr v0, v1

    .line 1860786
    iget-boolean v1, p0, LX/CEA;->n:Z

    if-eqz v1, :cond_1

    .line 1860787
    iget v1, p0, LX/CEA;->p:I

    add-int/2addr v0, v1

    .line 1860788
    iget-object v1, p0, LX/CEA;->o:Lcom/facebook/fig/facepile/FigFacepileView;

    invoke-virtual {v1, p1, v0, p3, p4}, Lcom/facebook/fig/facepile/FigFacepileView;->layout(IIII)V

    .line 1860789
    :cond_1
    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 1860772
    invoke-super {p0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 1860773
    iget-object v0, p0, LX/CEA;->k:LX/6WW;

    invoke-virtual {v0, p1}, LX/6WW;->a(Landroid/graphics/Canvas;)V

    .line 1860774
    iget-object v0, p0, LX/CEA;->l:LX/6WW;

    invoke-virtual {v0, p1}, LX/6WW;->a(Landroid/graphics/Canvas;)V

    .line 1860775
    return-void
.end method

.method public final dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 1

    .prologue
    .line 1860769
    iget-object v0, p0, LX/CEA;->k:LX/6WW;

    invoke-virtual {v0, p1}, LX/6WW;->a(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 1860770
    iget-object v0, p0, LX/CEA;->l:LX/6WW;

    invoke-virtual {v0, p1}, LX/6WW;->a(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 1860771
    invoke-super {p0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v0

    return v0
.end method

.method public setBodyText(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1860809
    iget-object v0, p0, LX/CEA;->k:LX/6WW;

    invoke-virtual {v0, p1}, LX/6WW;->a(Ljava/lang/CharSequence;)V

    .line 1860810
    invoke-static {p0}, LX/CEA;->e(LX/CEA;)V

    .line 1860811
    invoke-virtual {p0}, LX/CEA;->requestLayout()V

    .line 1860812
    invoke-virtual {p0}, LX/CEA;->invalidate()V

    .line 1860813
    return-void
.end method

.method public setFacepile(Lcom/facebook/fig/facepile/FigFacepileView;)V
    .locals 3

    .prologue
    const/4 v1, -0x2

    .line 1860759
    if-nez p1, :cond_0

    .line 1860760
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/CEA;->n:Z

    .line 1860761
    :goto_0
    invoke-virtual {p0}, LX/CEA;->requestLayout()V

    .line 1860762
    invoke-virtual {p0}, LX/CEA;->invalidate()V

    .line 1860763
    return-void

    .line 1860764
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/CEA;->n:Z

    .line 1860765
    iput-object p1, p0, LX/CEA;->o:Lcom/facebook/fig/facepile/FigFacepileView;

    .line 1860766
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 1860767
    iget-object v1, p0, LX/CEA;->o:Lcom/facebook/fig/facepile/FigFacepileView;

    invoke-virtual {v1, v0}, Lcom/facebook/fig/facepile/FigFacepileView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1860768
    iget-object v1, p0, LX/CEA;->o:Lcom/facebook/fig/facepile/FigFacepileView;

    invoke-virtual {p0}, LX/CEA;->getChildCount()I

    move-result v2

    invoke-super {p0, v1, v2, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public setMetaText(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1860754
    iget-object v0, p0, LX/CEA;->l:LX/6WW;

    invoke-virtual {v0, p1}, LX/6WW;->a(Ljava/lang/CharSequence;)V

    .line 1860755
    invoke-static {p0}, LX/CEA;->e(LX/CEA;)V

    .line 1860756
    invoke-virtual {p0}, LX/CEA;->requestLayout()V

    .line 1860757
    invoke-virtual {p0}, LX/CEA;->invalidate()V

    .line 1860758
    return-void
.end method

.method public final setPadding(IIII)V
    .locals 0

    .prologue
    .line 1860753
    return-void
.end method

.method public setThumbnailDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 3

    .prologue
    .line 1860749
    iget-object v0, p0, LX/CEA;->j:LX/0wM;

    if-eqz v0, :cond_0

    .line 1860750
    iget-object v0, p0, LX/CEA;->j:LX/0wM;

    invoke-virtual {p0}, LX/CEA;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0a00a3

    invoke-static {v1, v2}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, p1, v1}, LX/0wM;->a(Landroid/graphics/drawable/Drawable;I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    .line 1860751
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1860752
    return-void
.end method

.method public setThumbnailPadding(I)V
    .locals 0

    .prologue
    .line 1860748
    return-void
.end method

.method public setThumbnailSize(I)V
    .locals 0

    .prologue
    .line 1860747
    return-void
.end method
