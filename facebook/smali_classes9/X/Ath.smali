.class public final LX/Ath;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0iK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "Lcom/facebook/friendsharing/inspiration/model/CameraStateSpec$ProvidesCameraState;",
        ":",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationStateSpec$ProvidesInspirationState;",
        "DerivedData:",
        "Ljava/lang/Object;",
        "Mutation::",
        "Lcom/facebook/ipc/composer/dataaccessor/ComposerCanSave;",
        ":",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationStateSpec$SetsInspirationState",
        "<TMutation;>;Services::",
        "LX/0il",
        "<TModelData;>;:",
        "LX/0ik",
        "<TDerivedData;>;:",
        "LX/0im",
        "<TMutation;>;>",
        "Ljava/lang/Object;",
        "LX/0iK",
        "<TModelData;TDerivedData;>;"
    }
.end annotation


# instance fields
.field public final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TServices;>;"
        }
    .end annotation
.end field

.field public final b:LX/0zw;

.field private final c:Landroid/content/Context;

.field private final d:LX/0wW;

.field public final e:Landroid/os/Handler;

.field public final f:LX/Atg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/Ath",
            "<TModelData;TDerivedData;TMutation;TServices;>.ProgressBarAnimation;"
        }
    .end annotation
.end field

.field public final g:Ljava/lang/Runnable;

.field private h:Lcom/facebook/facecast/view/CircularProgressView;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:LX/0wd;

.field public j:I


# direct methods
.method public constructor <init>(LX/0il;ILandroid/view/ViewStub;Landroid/content/Context;LX/0Sh;LX/0wY;LX/0wW;)V
    .locals 4
    .param p1    # LX/0il;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # I
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Landroid/view/ViewStub;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TServices;I",
            "Landroid/view/ViewStub;",
            "Landroid/content/Context;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/0wY;",
            "LX/0wW;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1721689
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1721690
    new-instance v0, Lcom/facebook/friendsharing/inspiration/controller/InspirationProgressController$1;

    invoke-direct {v0, p0}, Lcom/facebook/friendsharing/inspiration/controller/InspirationProgressController$1;-><init>(LX/Ath;)V

    iput-object v0, p0, LX/Ath;->g:Ljava/lang/Runnable;

    .line 1721691
    iput-object p4, p0, LX/Ath;->c:Landroid/content/Context;

    .line 1721692
    iput-object p7, p0, LX/Ath;->d:LX/0wW;

    .line 1721693
    const-string v0, "This controller will animate a progress bar, hence implementation relies on UI thread."

    invoke-virtual {p5, v0}, LX/0Sh;->a(Ljava/lang/String;)V

    .line 1721694
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/Ath;->a:Ljava/lang/ref/WeakReference;

    .line 1721695
    if-lez p2, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1721696
    iput p2, p0, LX/Ath;->j:I

    .line 1721697
    const-string v0, "Please pass a non-null viewstub"

    invoke-static {p3, v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1721698
    invoke-virtual {p3}, Landroid/view/ViewStub;->getLayoutResource()I

    move-result v0

    if-nez v0, :cond_2

    :goto_1
    const-string v0, "A layout was declared on this view stub. Please remove it because this controller will inflate its own layout on this stub."

    invoke-static {v1, v0}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1721699
    new-instance v0, LX/0zw;

    invoke-direct {v0, p3}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    iput-object v0, p0, LX/Ath;->b:LX/0zw;

    .line 1721700
    iget-object v0, p0, LX/Ath;->b:LX/0zw;

    const v1, 0x7f03093d

    .line 1721701
    iget-object v2, v0, LX/0zw;->a:Landroid/view/ViewStub;

    if-eqz v2, :cond_0

    .line 1721702
    iget-object v2, v0, LX/0zw;->a:Landroid/view/ViewStub;

    invoke-virtual {v2, v1}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 1721703
    :cond_0
    new-instance v0, LX/Atg;

    invoke-direct {v0, p0, p6}, LX/Atg;-><init>(LX/Ath;LX/0wY;)V

    iput-object v0, p0, LX/Ath;->f:LX/Atg;

    .line 1721704
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, LX/Ath;->e:Landroid/os/Handler;

    .line 1721705
    invoke-direct {p0}, LX/Ath;->d()V

    .line 1721706
    return-void

    :cond_1
    move v0, v2

    .line 1721707
    goto :goto_0

    :cond_2
    move v1, v2

    .line 1721708
    goto :goto_1
.end method

.method private a()V
    .locals 11

    .prologue
    .line 1721709
    iget-object v0, p0, LX/Ath;->b:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1721710
    invoke-static {p0}, LX/Ath;->c$redex0(LX/Ath;)Lcom/facebook/facecast/view/CircularProgressView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/facecast/view/CircularProgressView;->setProgress(F)V

    .line 1721711
    iget-object v0, p0, LX/Ath;->e:Landroid/os/Handler;

    iget-object v1, p0, LX/Ath;->g:Ljava/lang/Runnable;

    iget v2, p0, LX/Ath;->j:I

    int-to-long v2, v2

    const v4, -0x52f792de

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1721712
    iget-object v0, p0, LX/Ath;->f:LX/Atg;

    .line 1721713
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    long-to-int v5, v5

    iput v5, v0, LX/Atg;->c:I

    .line 1721714
    iget-object v5, v0, LX/Atg;->a:LX/0wY;

    invoke-interface {v5, v0}, LX/0wY;->a(LX/0wa;)V

    .line 1721715
    iget-object v7, v0, LX/Atg;->b:Landroid/view/View;

    if-nez v7, :cond_0

    .line 1721716
    iget-object v7, v0, LX/Atg;->d:LX/Ath;

    iget-object v7, v7, LX/Ath;->b:LX/0zw;

    invoke-virtual {v7}, LX/0zw;->a()Landroid/view/View;

    move-result-object v7

    const v8, 0x7f0d17b5

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    iput-object v7, v0, LX/Atg;->b:Landroid/view/View;

    .line 1721717
    :cond_0
    new-instance v7, Landroid/view/animation/AlphaAnimation;

    const/high16 v8, 0x3f800000    # 1.0f

    const/4 v9, 0x0

    invoke-direct {v7, v8, v9}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 1721718
    const-wide/16 v9, 0x3e8

    invoke-virtual {v7, v9, v10}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 1721719
    new-instance v8, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v8}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v7, v8}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 1721720
    const/4 v8, -0x1

    invoke-virtual {v7, v8}, Landroid/view/animation/Animation;->setRepeatCount(I)V

    .line 1721721
    const/4 v8, 0x2

    invoke-virtual {v7, v8}, Landroid/view/animation/Animation;->setRepeatMode(I)V

    .line 1721722
    iget-object v8, v0, LX/Atg;->b:Landroid/view/View;

    invoke-virtual {v8, v7}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1721723
    iget-object v0, p0, LX/Ath;->i:LX/0wd;

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    .line 1721724
    return-void
.end method

.method public static c$redex0(LX/Ath;)Lcom/facebook/facecast/view/CircularProgressView;
    .locals 2

    .prologue
    .line 1721725
    iget-object v0, p0, LX/Ath;->h:Lcom/facebook/facecast/view/CircularProgressView;

    if-nez v0, :cond_0

    .line 1721726
    iget-object v0, p0, LX/Ath;->b:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0d17b6

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecast/view/CircularProgressView;

    iput-object v0, p0, LX/Ath;->h:Lcom/facebook/facecast/view/CircularProgressView;

    .line 1721727
    iget-object v0, p0, LX/Ath;->h:Lcom/facebook/facecast/view/CircularProgressView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/facecast/view/CircularProgressView;->setProgress(F)V

    .line 1721728
    :cond_0
    iget-object v0, p0, LX/Ath;->h:Lcom/facebook/facecast/view/CircularProgressView;

    return-object v0
.end method

.method private d()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 1721729
    iget-object v0, p0, LX/Ath;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1a6d    # 1.848999E38f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 1721730
    iget-object v1, p0, LX/Ath;->d:LX/0wW;

    invoke-virtual {v1}, LX/0wW;->a()LX/0wd;

    move-result-object v1

    invoke-static {}, LX/87U;->a()LX/0wT;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v1

    invoke-virtual {v1, v4, v5}, LX/0wd;->a(D)LX/0wd;

    move-result-object v1

    invoke-virtual {v1, v4, v5}, LX/0wd;->b(D)LX/0wd;

    move-result-object v1

    invoke-virtual {v1}, LX/0wd;->j()LX/0wd;

    move-result-object v1

    new-instance v2, LX/Atf;

    invoke-direct {v2, p0, v0}, LX/Atf;-><init>(LX/Ath;I)V

    invoke-virtual {v1, v2}, LX/0wd;->a(LX/0xi;)LX/0wd;

    move-result-object v0

    iput-object v0, p0, LX/Ath;->i:LX/0wd;

    .line 1721731
    return-void
.end method


# virtual methods
.method public final a(LX/5L2;)V
    .locals 0

    .prologue
    .line 1721732
    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 7

    .prologue
    .line 1721733
    check-cast p1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    .line 1721734
    iget-object v0, p0, LX/Ath;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 1721735
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    .line 1721736
    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->j()Lcom/facebook/friendsharing/inspiration/model/CameraState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/model/CameraState;->getCaptureState()LX/86q;

    move-result-object v1

    .line 1721737
    invoke-virtual {p1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->j()Lcom/facebook/friendsharing/inspiration/model/CameraState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/friendsharing/inspiration/model/CameraState;->getCaptureState()LX/86q;

    move-result-object v2

    .line 1721738
    if-eq v1, v2, :cond_1

    sget-object v2, LX/86q;->STOP_RECORD_VIDEO_REQUESTED:LX/86q;

    if-ne v1, v2, :cond_1

    .line 1721739
    iget-object v3, p0, LX/Ath;->e:Landroid/os/Handler;

    iget-object v4, p0, LX/Ath;->g:Ljava/lang/Runnable;

    invoke-static {v3, v4}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1721740
    iget-object v3, p0, LX/Ath;->f:LX/Atg;

    .line 1721741
    iget-object v4, v3, LX/Atg;->a:LX/0wY;

    invoke-interface {v4, v3}, LX/0wY;->b(LX/0wa;)V

    .line 1721742
    iget-object v4, v3, LX/Atg;->b:Landroid/view/View;

    if-eqz v4, :cond_0

    .line 1721743
    iget-object v4, v3, LX/Atg;->b:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->clearAnimation()V

    .line 1721744
    :cond_0
    iget-object v3, p0, LX/Ath;->i:LX/0wd;

    const-wide/16 v5, 0x0

    invoke-virtual {v3, v5, v6}, LX/0wd;->b(D)LX/0wd;

    .line 1721745
    :cond_1
    invoke-static {p1, v0}, LX/Arx;->a(Lcom/facebook/composer/system/model/ComposerModelImpl;Lcom/facebook/composer/system/model/ComposerModelImpl;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1721746
    invoke-direct {p0}, LX/Ath;->a()V

    .line 1721747
    :cond_2
    return-void
.end method
