.class public final LX/AiL;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1706048
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_7

    .line 1706049
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1706050
    :goto_0
    return v1

    .line 1706051
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_5

    .line 1706052
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 1706053
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1706054
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_0

    if-eqz v6, :cond_0

    .line 1706055
    const-string v7, "count"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 1706056
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v5, v0

    move v0, v2

    goto :goto_1

    .line 1706057
    :cond_1
    const-string v7, "edges"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 1706058
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1706059
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->START_ARRAY:LX/15z;

    if-ne v6, v7, :cond_2

    .line 1706060
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_ARRAY:LX/15z;

    if-eq v6, v7, :cond_2

    .line 1706061
    invoke-static {p0, p1}, LX/AiK;->b(LX/15w;LX/186;)I

    move-result v6

    .line 1706062
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1706063
    :cond_2
    invoke-static {v4, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v4

    move v4, v4

    .line 1706064
    goto :goto_1

    .line 1706065
    :cond_3
    const-string v7, "page_info"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1706066
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1706067
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v8, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v8, :cond_d

    .line 1706068
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1706069
    :goto_3
    move v3, v6

    .line 1706070
    goto :goto_1

    .line 1706071
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1706072
    :cond_5
    const/4 v6, 0x3

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 1706073
    if-eqz v0, :cond_6

    .line 1706074
    invoke-virtual {p1, v1, v5, v1}, LX/186;->a(III)V

    .line 1706075
    :cond_6
    invoke-virtual {p1, v2, v4}, LX/186;->b(II)V

    .line 1706076
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1706077
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_7
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    goto/16 :goto_1

    .line 1706078
    :cond_8
    const-string v11, "has_next_page"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_a

    .line 1706079
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v3

    move v8, v3

    move v3, v7

    .line 1706080
    :cond_9
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_b

    .line 1706081
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 1706082
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1706083
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_9

    if-eqz v10, :cond_9

    .line 1706084
    const-string v11, "end_cursor"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_8

    .line 1706085
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_4

    .line 1706086
    :cond_a
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_4

    .line 1706087
    :cond_b
    const/4 v10, 0x2

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 1706088
    invoke-virtual {p1, v6, v9}, LX/186;->b(II)V

    .line 1706089
    if-eqz v3, :cond_c

    .line 1706090
    invoke-virtual {p1, v7, v8}, LX/186;->a(IZ)V

    .line 1706091
    :cond_c
    invoke-virtual {p1}, LX/186;->d()I

    move-result v6

    goto :goto_3

    :cond_d
    move v3, v6

    move v8, v6

    move v9, v6

    goto :goto_4
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1706092
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1706093
    invoke-virtual {p0, p1, v0, v0}, LX/15i;->a(III)I

    move-result v0

    .line 1706094
    if-eqz v0, :cond_0

    .line 1706095
    const-string v1, "count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1706096
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1706097
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1706098
    if-eqz v0, :cond_2

    .line 1706099
    const-string v1, "edges"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1706100
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1706101
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 1706102
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2, p3}, LX/AiK;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1706103
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1706104
    :cond_1
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1706105
    :cond_2
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1706106
    if-eqz v0, :cond_5

    .line 1706107
    const-string v1, "page_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1706108
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1706109
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1706110
    if-eqz v1, :cond_3

    .line 1706111
    const-string v2, "end_cursor"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1706112
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1706113
    :cond_3
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/15i;->b(II)Z

    move-result v1

    .line 1706114
    if-eqz v1, :cond_4

    .line 1706115
    const-string v2, "has_next_page"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1706116
    invoke-virtual {p2, v1}, LX/0nX;->a(Z)V

    .line 1706117
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1706118
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1706119
    return-void
.end method
