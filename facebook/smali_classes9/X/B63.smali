.class public final enum LX/B63;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/B63;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/B63;

.field public static final enum LAUNCH_COMPOSER:LX/B63;

.field public static final enum NONE:LX/B63;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1746245
    new-instance v0, LX/B63;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, LX/B63;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/B63;->NONE:LX/B63;

    .line 1746246
    new-instance v0, LX/B63;

    const-string v1, "LAUNCH_COMPOSER"

    invoke-direct {v0, v1, v3}, LX/B63;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/B63;->LAUNCH_COMPOSER:LX/B63;

    .line 1746247
    const/4 v0, 0x2

    new-array v0, v0, [LX/B63;

    sget-object v1, LX/B63;->NONE:LX/B63;

    aput-object v1, v0, v2

    sget-object v1, LX/B63;->LAUNCH_COMPOSER:LX/B63;

    aput-object v1, v0, v3

    sput-object v0, LX/B63;->$VALUES:[LX/B63;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1746248
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/B63;
    .locals 1

    .prologue
    .line 1746249
    const-class v0, LX/B63;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/B63;

    return-object v0
.end method

.method public static values()[LX/B63;
    .locals 1

    .prologue
    .line 1746250
    sget-object v0, LX/B63;->$VALUES:[LX/B63;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/B63;

    return-object v0
.end method
