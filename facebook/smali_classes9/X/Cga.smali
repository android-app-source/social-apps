.class public LX/Cga;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile h:LX/Cga;


# instance fields
.field private a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1B1;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lcom/facebook/reaction/ReactionUtil;

.field private c:Landroid/content/res/Resources;

.field private d:LX/BNP;

.field public e:LX/Ch5;

.field public f:LX/1B1;

.field private g:LX/0kL;


# direct methods
.method public constructor <init>(LX/0Or;Lcom/facebook/reaction/ReactionUtil;Landroid/content/res/Resources;LX/BNP;LX/Ch5;LX/0kL;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/1B1;",
            ">;",
            "Lcom/facebook/reaction/ReactionUtil;",
            "Landroid/content/res/Resources;",
            "LX/BNP;",
            "LX/Ch5;",
            "LX/0kL;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1927286
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1927287
    iput-object p1, p0, LX/Cga;->a:LX/0Or;

    .line 1927288
    iput-object p2, p0, LX/Cga;->b:Lcom/facebook/reaction/ReactionUtil;

    .line 1927289
    iput-object p3, p0, LX/Cga;->c:Landroid/content/res/Resources;

    .line 1927290
    iput-object p4, p0, LX/Cga;->d:LX/BNP;

    .line 1927291
    iput-object p5, p0, LX/Cga;->e:LX/Ch5;

    .line 1927292
    iget-object v0, p0, LX/Cga;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1B1;

    iput-object v0, p0, LX/Cga;->f:LX/1B1;

    .line 1927293
    iput-object p6, p0, LX/Cga;->g:LX/0kL;

    .line 1927294
    return-void
.end method

.method public static a(LX/0QB;)LX/Cga;
    .locals 10

    .prologue
    .line 1927295
    sget-object v0, LX/Cga;->h:LX/Cga;

    if-nez v0, :cond_1

    .line 1927296
    const-class v1, LX/Cga;

    monitor-enter v1

    .line 1927297
    :try_start_0
    sget-object v0, LX/Cga;->h:LX/Cga;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1927298
    if-eqz v2, :cond_0

    .line 1927299
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1927300
    new-instance v3, LX/Cga;

    const/16 v4, 0x45a

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {v0}, Lcom/facebook/reaction/ReactionUtil;->b(LX/0QB;)Lcom/facebook/reaction/ReactionUtil;

    move-result-object v5

    check-cast v5, Lcom/facebook/reaction/ReactionUtil;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v6

    check-cast v6, Landroid/content/res/Resources;

    invoke-static {v0}, LX/BNP;->a(LX/0QB;)LX/BNP;

    move-result-object v7

    check-cast v7, LX/BNP;

    invoke-static {v0}, LX/Ch5;->a(LX/0QB;)LX/Ch5;

    move-result-object v8

    check-cast v8, LX/Ch5;

    invoke-static {v0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v9

    check-cast v9, LX/0kL;

    invoke-direct/range {v3 .. v9}, LX/Cga;-><init>(LX/0Or;Lcom/facebook/reaction/ReactionUtil;Landroid/content/res/Resources;LX/BNP;LX/Ch5;LX/0kL;)V

    .line 1927301
    move-object v0, v3

    .line 1927302
    sput-object v0, LX/Cga;->h:LX/Cga;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1927303
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1927304
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1927305
    :cond_1
    sget-object v0, LX/Cga;->h:LX/Cga;

    return-object v0

    .line 1927306
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1927307
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(ILandroid/content/Intent;Ljava/lang/String;Ljava/lang/String;LX/0o8;Ljava/lang/Long;Ljava/lang/String;)V
    .locals 7
    .param p6    # Ljava/lang/Long;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1927308
    iget-object v6, p0, LX/Cga;->d:LX/BNP;

    new-instance v0, LX/CgZ;

    move-object v1, p0

    move-object v2, p6

    move-object v3, p7

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, LX/CgZ;-><init>(LX/Cga;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;LX/0o8;)V

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v4

    invoke-interface {p5}, LX/0o8;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v5

    move-object v0, v6

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-virtual/range {v0 .. v5}, LX/BNP;->a(ILandroid/content/Intent;Ljava/lang/String;LX/0am;LX/0am;)V

    .line 1927309
    return-void
.end method
