.class public final LX/Ail;
.super LX/1OX;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;)V
    .locals 0

    .prologue
    .line 1706826
    iput-object p1, p0, LX/Ail;->a:Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;

    invoke-direct {p0}, LX/1OX;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v7/widget/RecyclerView;II)V
    .locals 8

    .prologue
    .line 1706827
    invoke-super {p0, p1, p2, p3}, LX/1OX;->a(Landroid/support/v7/widget/RecyclerView;II)V

    .line 1706828
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->computeVerticalScrollOffset()I

    move-result v0

    .line 1706829
    iget-object v1, p0, LX/Ail;->a:Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;

    iget-object v1, v1, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->d:LX/3wu;

    invoke-virtual {v1}, LX/1P1;->n()I

    move-result v1

    .line 1706830
    iget-object v2, p0, LX/Ail;->a:Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;

    iget-object v3, p0, LX/Ail;->a:Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;

    iget-wide v4, v3, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->f:D

    int-to-double v6, v0

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->max(DD)D

    move-result-wide v4

    iput-wide v4, v2, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->f:D

    .line 1706831
    iget-object v0, p0, LX/Ail;->a:Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;

    iget-object v2, p0, LX/Ail;->a:Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;

    iget v2, v2, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->g:I

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, v0, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->g:I

    .line 1706832
    iget-object v0, p0, LX/Ail;->a:Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;

    iget v0, v0, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->g:I

    iget-object v1, p0, LX/Ail;->a:Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;

    iget-object v1, v1, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->h:LX/Aij;

    invoke-virtual {v1}, LX/1OM;->ij_()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, LX/Ail;->a:Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;

    iget-object v0, v0, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->h:LX/Aij;

    .line 1706833
    iget-boolean v1, v0, LX/Aij;->h:Z

    move v0, v1

    .line 1706834
    if-eqz v0, :cond_0

    .line 1706835
    iget-object v0, p0, LX/Ail;->a:Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;

    iget-object v1, p0, LX/Ail;->a:Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;

    iget-object v1, v1, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->b:LX/0Vd;

    invoke-virtual {v0, v1}, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->a(LX/0Vd;)V

    .line 1706836
    :cond_0
    return-void
.end method
