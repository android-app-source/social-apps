.class public final synthetic LX/BVu;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final synthetic a:[I

.field public static final synthetic b:[I

.field public static final synthetic c:[I

.field public static final synthetic d:[I

.field public static final synthetic e:[I


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1791501
    invoke-static {}, LX/BVz;->values()[LX/BVz;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/BVu;->e:[I

    :try_start_0
    sget-object v0, LX/BVu;->e:[I

    sget-object v1, LX/BVz;->PADDING_TOP:LX/BVz;

    invoke-virtual {v1}, LX/BVz;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_1c

    :goto_0
    :try_start_1
    sget-object v0, LX/BVu;->e:[I

    sget-object v1, LX/BVz;->PADDING_RIGHT:LX/BVz;

    invoke-virtual {v1}, LX/BVz;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1b

    :goto_1
    :try_start_2
    sget-object v0, LX/BVu;->e:[I

    sget-object v1, LX/BVz;->PADDING_LEFT:LX/BVz;

    invoke-virtual {v1}, LX/BVz;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_1a

    :goto_2
    :try_start_3
    sget-object v0, LX/BVu;->e:[I

    sget-object v1, LX/BVz;->PADDING_BOTTOM:LX/BVz;

    invoke-virtual {v1}, LX/BVz;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_19

    :goto_3
    :try_start_4
    sget-object v0, LX/BVu;->e:[I

    sget-object v1, LX/BVz;->PADDING:LX/BVz;

    invoke-virtual {v1}, LX/BVz;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_18

    :goto_4
    :try_start_5
    sget-object v0, LX/BVu;->e:[I

    sget-object v1, LX/BVz;->ID:LX/BVz;

    invoke-virtual {v1}, LX/BVz;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_17

    :goto_5
    :try_start_6
    sget-object v0, LX/BVu;->e:[I

    sget-object v1, LX/BVz;->FOCUSABLE:LX/BVz;

    invoke-virtual {v1}, LX/BVz;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_16

    :goto_6
    :try_start_7
    sget-object v0, LX/BVu;->e:[I

    sget-object v1, LX/BVz;->VISIBILITY:LX/BVz;

    invoke-virtual {v1}, LX/BVz;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_15

    :goto_7
    :try_start_8
    sget-object v0, LX/BVu;->e:[I

    sget-object v1, LX/BVz;->BACKGROUND:LX/BVz;

    invoke-virtual {v1}, LX/BVz;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_14

    :goto_8
    :try_start_9
    sget-object v0, LX/BVu;->e:[I

    sget-object v1, LX/BVz;->MIN_WIDTH:LX/BVz;

    invoke-virtual {v1}, LX/BVz;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_13

    .line 1791502
    :goto_9
    invoke-static {}, LX/BVw;->values()[LX/BVw;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/BVu;->d:[I

    :try_start_a
    sget-object v0, LX/BVu;->d:[I

    sget-object v1, LX/BVw;->GRAVITY:LX/BVw;

    invoke-virtual {v1}, LX/BVw;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_12

    :goto_a
    :try_start_b
    sget-object v0, LX/BVu;->d:[I

    sget-object v1, LX/BVw;->WEIGHT:LX/BVw;

    invoke-virtual {v1}, LX/BVw;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_11

    .line 1791503
    :goto_b
    invoke-static {}, LX/BVy;->values()[LX/BVy;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/BVu;->c:[I

    :try_start_c
    sget-object v0, LX/BVu;->c:[I

    sget-object v1, LX/BVy;->ABOVE:LX/BVy;

    invoke-virtual {v1}, LX/BVy;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_10

    :goto_c
    :try_start_d
    sget-object v0, LX/BVu;->c:[I

    sget-object v1, LX/BVy;->BELOW:LX/BVy;

    invoke-virtual {v1}, LX/BVy;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_d
    .catch Ljava/lang/NoSuchFieldError; {:try_start_d .. :try_end_d} :catch_f

    :goto_d
    :try_start_e
    sget-object v0, LX/BVu;->c:[I

    sget-object v1, LX/BVy;->TO_LEFT_OF:LX/BVy;

    invoke-virtual {v1}, LX/BVy;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_e .. :try_end_e} :catch_e

    :goto_e
    :try_start_f
    sget-object v0, LX/BVu;->c:[I

    sget-object v1, LX/BVy;->TO_RIGHT_OF:LX/BVy;

    invoke-virtual {v1}, LX/BVy;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_f .. :try_end_f} :catch_d

    :goto_f
    :try_start_10
    sget-object v0, LX/BVu;->c:[I

    sget-object v1, LX/BVy;->CENTER_VERTICAL:LX/BVy;

    invoke-virtual {v1}, LX/BVy;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_10
    .catch Ljava/lang/NoSuchFieldError; {:try_start_10 .. :try_end_10} :catch_c

    :goto_10
    :try_start_11
    sget-object v0, LX/BVu;->c:[I

    sget-object v1, LX/BVy;->CENTER_HORIZONTAL:LX/BVy;

    invoke-virtual {v1}, LX/BVy;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_11
    .catch Ljava/lang/NoSuchFieldError; {:try_start_11 .. :try_end_11} :catch_b

    :goto_11
    :try_start_12
    sget-object v0, LX/BVu;->c:[I

    sget-object v1, LX/BVy;->ALIGN_PARENT_RIGHT:LX/BVy;

    invoke-virtual {v1}, LX/BVy;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_12
    .catch Ljava/lang/NoSuchFieldError; {:try_start_12 .. :try_end_12} :catch_a

    :goto_12
    :try_start_13
    sget-object v0, LX/BVu;->c:[I

    sget-object v1, LX/BVy;->ALIGN_PARENT_TOP:LX/BVy;

    invoke-virtual {v1}, LX/BVy;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_13
    .catch Ljava/lang/NoSuchFieldError; {:try_start_13 .. :try_end_13} :catch_9

    :goto_13
    :try_start_14
    sget-object v0, LX/BVu;->c:[I

    sget-object v1, LX/BVy;->ALIGN_PARENT_LEFT:LX/BVy;

    invoke-virtual {v1}, LX/BVy;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_14
    .catch Ljava/lang/NoSuchFieldError; {:try_start_14 .. :try_end_14} :catch_8

    :goto_14
    :try_start_15
    sget-object v0, LX/BVu;->c:[I

    sget-object v1, LX/BVy;->ALIGN_PARENT_BOTTOM:LX/BVy;

    invoke-virtual {v1}, LX/BVy;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_15
    .catch Ljava/lang/NoSuchFieldError; {:try_start_15 .. :try_end_15} :catch_7

    :goto_15
    :try_start_16
    sget-object v0, LX/BVu;->c:[I

    sget-object v1, LX/BVy;->CENTER_IN_PARENT:LX/BVy;

    invoke-virtual {v1}, LX/BVy;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1
    :try_end_16
    .catch Ljava/lang/NoSuchFieldError; {:try_start_16 .. :try_end_16} :catch_6

    .line 1791504
    :goto_16
    invoke-static {}, LX/BVv;->values()[LX/BVv;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/BVu;->b:[I

    :try_start_17
    sget-object v0, LX/BVu;->b:[I

    sget-object v1, LX/BVv;->WIDTH:LX/BVv;

    invoke-virtual {v1}, LX/BVv;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_17
    .catch Ljava/lang/NoSuchFieldError; {:try_start_17 .. :try_end_17} :catch_5

    :goto_17
    :try_start_18
    sget-object v0, LX/BVu;->b:[I

    sget-object v1, LX/BVv;->HEIGHT:LX/BVv;

    invoke-virtual {v1}, LX/BVv;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_18
    .catch Ljava/lang/NoSuchFieldError; {:try_start_18 .. :try_end_18} :catch_4

    .line 1791505
    :goto_18
    invoke-static {}, LX/BVx;->values()[LX/BVx;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/BVu;->a:[I

    :try_start_19
    sget-object v0, LX/BVu;->a:[I

    sget-object v1, LX/BVx;->MARGIN_TOP:LX/BVx;

    invoke-virtual {v1}, LX/BVx;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_19
    .catch Ljava/lang/NoSuchFieldError; {:try_start_19 .. :try_end_19} :catch_3

    :goto_19
    :try_start_1a
    sget-object v0, LX/BVu;->a:[I

    sget-object v1, LX/BVx;->MARGIN_RIGHT:LX/BVx;

    invoke-virtual {v1}, LX/BVx;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1a .. :try_end_1a} :catch_2

    :goto_1a
    :try_start_1b
    sget-object v0, LX/BVu;->a:[I

    sget-object v1, LX/BVx;->MARGIN_LEFT:LX/BVx;

    invoke-virtual {v1}, LX/BVx;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_1b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1b .. :try_end_1b} :catch_1

    :goto_1b
    :try_start_1c
    sget-object v0, LX/BVu;->a:[I

    sget-object v1, LX/BVx;->MARGIN_BOTTOM:LX/BVx;

    invoke-virtual {v1}, LX/BVx;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_1c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1c .. :try_end_1c} :catch_0

    :goto_1c
    return-void

    :catch_0
    goto :goto_1c

    :catch_1
    goto :goto_1b

    :catch_2
    goto :goto_1a

    :catch_3
    goto :goto_19

    :catch_4
    goto :goto_18

    :catch_5
    goto :goto_17

    :catch_6
    goto :goto_16

    :catch_7
    goto :goto_15

    :catch_8
    goto :goto_14

    :catch_9
    goto/16 :goto_13

    :catch_a
    goto/16 :goto_12

    :catch_b
    goto/16 :goto_11

    :catch_c
    goto/16 :goto_10

    :catch_d
    goto/16 :goto_f

    :catch_e
    goto/16 :goto_e

    :catch_f
    goto/16 :goto_d

    :catch_10
    goto/16 :goto_c

    :catch_11
    goto/16 :goto_b

    :catch_12
    goto/16 :goto_a

    :catch_13
    goto/16 :goto_9

    :catch_14
    goto/16 :goto_8

    :catch_15
    goto/16 :goto_7

    :catch_16
    goto/16 :goto_6

    :catch_17
    goto/16 :goto_5

    :catch_18
    goto/16 :goto_4

    :catch_19
    goto/16 :goto_3

    :catch_1a
    goto/16 :goto_2

    :catch_1b
    goto/16 :goto_1

    :catch_1c
    goto/16 :goto_0
.end method
