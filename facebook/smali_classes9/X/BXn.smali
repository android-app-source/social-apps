.class public LX/BXn;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/BXn;


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1793773
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1793774
    iput-object p1, p0, LX/BXn;->a:Landroid/content/Context;

    .line 1793775
    return-void
.end method

.method public static a(LX/0QB;)LX/BXn;
    .locals 4

    .prologue
    .line 1793776
    sget-object v0, LX/BXn;->b:LX/BXn;

    if-nez v0, :cond_1

    .line 1793777
    const-class v1, LX/BXn;

    monitor-enter v1

    .line 1793778
    :try_start_0
    sget-object v0, LX/BXn;->b:LX/BXn;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1793779
    if-eqz v2, :cond_0

    .line 1793780
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1793781
    new-instance p0, LX/BXn;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-direct {p0, v3}, LX/BXn;-><init>(Landroid/content/Context;)V

    .line 1793782
    move-object v0, p0

    .line 1793783
    sput-object v0, LX/BXn;->b:LX/BXn;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1793784
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1793785
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1793786
    :cond_1
    sget-object v0, LX/BXn;->b:LX/BXn;

    return-object v0

    .line 1793787
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1793788
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 1793789
    iget-object v0, p0, LX/BXn;->a:Landroid/content/Context;

    invoke-static {v0}, LX/1sT;->a(Landroid/content/Context;)I

    move-result v0

    const/16 v1, 0x7db

    if-gt v0, v1, :cond_0

    .line 1793790
    const/16 v0, 0xb4

    .line 1793791
    :goto_0
    return v0

    :cond_0
    const/16 v0, 0xdc

    goto :goto_0
.end method
