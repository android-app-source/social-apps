.class public LX/COM;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1882971
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/CNb;Landroid/content/Context;)I
    .locals 2

    .prologue
    .line 1882968
    const-string v0, "shadow-blur-radius"

    invoke-virtual {p0, v0}, LX/CNb;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "shadow-offset-x"

    invoke-virtual {p0, v0}, LX/CNb;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "shadow-offset-y"

    invoke-virtual {p0, v0}, LX/CNb;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "shadow-color"

    invoke-virtual {p0, v0}, LX/CNb;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1882969
    const/4 v0, 0x0

    .line 1882970
    :goto_0
    return v0

    :cond_0
    const-string v0, "shadow-blur-radius"

    invoke-virtual {p0, v0, p1}, LX/CNb;->a(Ljava/lang/String;Landroid/content/Context;)I

    move-result v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_0
.end method
