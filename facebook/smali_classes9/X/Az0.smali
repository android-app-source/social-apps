.class public final LX/Az0;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1731752
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/186;Lcom/facebook/graphql/model/GraphQLPhoto;)I
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 1731753
    if-nez p1, :cond_0

    .line 1731754
    :goto_0
    return v0

    .line 1731755
    :cond_0
    new-instance v1, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v2, 0x4984e12

    invoke-direct {v1, v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    invoke-virtual {p0, v1}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v1

    .line 1731756
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPhoto;->G()Lcom/facebook/graphql/model/GraphQLVect2;

    move-result-object v2

    invoke-static {p0, v2}, LX/Az0;->a(LX/186;Lcom/facebook/graphql/model/GraphQLVect2;)I

    move-result v2

    .line 1731757
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPhoto;->K()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1731758
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPhoto;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    invoke-static {p0, v4}, LX/Az0;->b(LX/186;Lcom/facebook/graphql/model/GraphQLImage;)I

    move-result v4

    .line 1731759
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPhoto;->M()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    invoke-static {p0, v5}, LX/Az0;->b(LX/186;Lcom/facebook/graphql/model/GraphQLImage;)I

    move-result v5

    .line 1731760
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPhoto;->S()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v6

    invoke-static {p0, v6}, LX/Az0;->b(LX/186;Lcom/facebook/graphql/model/GraphQLImage;)I

    move-result v6

    .line 1731761
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPhoto;->T()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    invoke-static {p0, v7}, LX/Az0;->b(LX/186;Lcom/facebook/graphql/model/GraphQLImage;)I

    move-result v7

    .line 1731762
    const/4 v8, 0x7

    invoke-virtual {p0, v8}, LX/186;->c(I)V

    .line 1731763
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 1731764
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v2}, LX/186;->b(II)V

    .line 1731765
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v3}, LX/186;->b(II)V

    .line 1731766
    const/4 v0, 0x3

    invoke-virtual {p0, v0, v4}, LX/186;->b(II)V

    .line 1731767
    const/4 v0, 0x4

    invoke-virtual {p0, v0, v5}, LX/186;->b(II)V

    .line 1731768
    const/4 v0, 0x5

    invoke-virtual {p0, v0, v6}, LX/186;->b(II)V

    .line 1731769
    const/4 v0, 0x6

    invoke-virtual {p0, v0, v7}, LX/186;->b(II)V

    .line 1731770
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 1731771
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0
.end method

.method public static a(LX/186;Lcom/facebook/graphql/model/GraphQLSouvenirMediaEdge;)I
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 1731772
    if-nez p1, :cond_0

    .line 1731773
    :goto_0
    return v0

    .line 1731774
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSouvenirMediaEdge;->a()Lcom/facebook/graphql/model/GraphQLSouvenirMediaElement;

    move-result-object v1

    const/4 v2, 0x0

    .line 1731775
    if-nez v1, :cond_1

    .line 1731776
    :goto_1
    move v1, v2

    .line 1731777
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, LX/186;->c(I)V

    .line 1731778
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 1731779
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 1731780
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0

    .line 1731781
    :cond_1
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLSouvenirMediaElement;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1731782
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLSouvenirMediaElement;->l()Lcom/facebook/graphql/model/GraphQLSouvenirMediaElementMediaConnection;

    move-result-object v4

    const/4 p1, 0x1

    const/4 v7, 0x0

    .line 1731783
    if-nez v4, :cond_2

    .line 1731784
    :goto_2
    move v4, v7

    .line 1731785
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLSouvenirMediaElement;->m()Lcom/facebook/graphql/enums/GraphQLSouvenirMediaFieldType;

    move-result-object v5

    invoke-virtual {p0, v5}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v5

    .line 1731786
    const/4 v6, 0x4

    invoke-virtual {p0, v6}, LX/186;->c(I)V

    .line 1731787
    invoke-virtual {p0, v2, v3}, LX/186;->b(II)V

    .line 1731788
    const/4 v2, 0x1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLSouvenirMediaElement;->k()Z

    move-result v3

    invoke-virtual {p0, v2, v3}, LX/186;->a(IZ)V

    .line 1731789
    const/4 v2, 0x2

    invoke-virtual {p0, v2, v4}, LX/186;->b(II)V

    .line 1731790
    const/4 v2, 0x3

    invoke-virtual {p0, v2, v5}, LX/186;->b(II)V

    .line 1731791
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 1731792
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto :goto_1

    .line 1731793
    :cond_2
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLSouvenirMediaElementMediaConnection;->a()LX/0Px;

    move-result-object v8

    .line 1731794
    if-eqz v8, :cond_4

    .line 1731795
    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v5

    new-array v9, v5, [I

    move v6, v7

    .line 1731796
    :goto_3
    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v5

    if-ge v6, v5, :cond_3

    .line 1731797
    invoke-virtual {v8, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/graphql/model/GraphQLSouvenirMediaElementMediaEdge;

    invoke-static {p0, v5}, LX/Az0;->a(LX/186;Lcom/facebook/graphql/model/GraphQLSouvenirMediaElementMediaEdge;)I

    move-result v5

    aput v5, v9, v6

    .line 1731798
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    goto :goto_3

    .line 1731799
    :cond_3
    invoke-virtual {p0, v9, p1}, LX/186;->a([IZ)I

    move-result v5

    .line 1731800
    :goto_4
    invoke-virtual {p0, p1}, LX/186;->c(I)V

    .line 1731801
    invoke-virtual {p0, v7, v5}, LX/186;->b(II)V

    .line 1731802
    invoke-virtual {p0}, LX/186;->d()I

    move-result v7

    .line 1731803
    invoke-virtual {p0, v7}, LX/186;->d(I)V

    goto :goto_2

    :cond_4
    move v5, v7

    goto :goto_4
.end method

.method public static a(LX/186;Lcom/facebook/graphql/model/GraphQLSouvenirMediaElementMediaEdge;)I
    .locals 12

    .prologue
    const/4 v0, 0x0

    .line 1731804
    if-nez p1, :cond_0

    .line 1731805
    :goto_0
    return v0

    .line 1731806
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSouvenirMediaElementMediaEdge;->a()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    const/4 v2, 0x0

    .line 1731807
    if-nez v1, :cond_1

    .line 1731808
    :goto_1
    move v1, v2

    .line 1731809
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, LX/186;->c(I)V

    .line 1731810
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 1731811
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 1731812
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0

    .line 1731813
    :cond_1
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    .line 1731814
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->G()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v4

    const/4 v5, 0x0

    .line 1731815
    if-nez v4, :cond_2

    .line 1731816
    :goto_2
    move v4, v5

    .line 1731817
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->K()Lcom/facebook/graphql/model/GraphQLVect2;

    move-result-object v5

    invoke-static {p0, v5}, LX/Az0;->a(LX/186;Lcom/facebook/graphql/model/GraphQLVect2;)I

    move-result v5

    .line 1731818
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1731819
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    invoke-static {p0, v7}, LX/Az0;->b(LX/186;Lcom/facebook/graphql/model/GraphQLImage;)I

    move-result v7

    .line 1731820
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->V()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v8

    invoke-static {p0, v8}, LX/Az0;->b(LX/186;Lcom/facebook/graphql/model/GraphQLImage;)I

    move-result v8

    .line 1731821
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->Z()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v9

    invoke-static {p0, v9}, LX/Az0;->b(LX/186;Lcom/facebook/graphql/model/GraphQLImage;)I

    move-result v9

    .line 1731822
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->aa()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v10

    invoke-static {p0, v10}, LX/Az0;->b(LX/186;Lcom/facebook/graphql/model/GraphQLImage;)I

    move-result v10

    .line 1731823
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->aS()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {p0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 1731824
    const/16 p1, 0xf

    invoke-virtual {p0, p1}, LX/186;->c(I)V

    .line 1731825
    invoke-virtual {p0, v2, v3}, LX/186;->b(II)V

    .line 1731826
    const/4 v3, 0x1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->u()Z

    move-result p1

    invoke-virtual {p0, v3, p1}, LX/186;->a(IZ)V

    .line 1731827
    const/4 v3, 0x2

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->z()Z

    move-result p1

    invoke-virtual {p0, v3, p1}, LX/186;->a(IZ)V

    .line 1731828
    const/4 v3, 0x3

    invoke-virtual {p0, v3, v4}, LX/186;->b(II)V

    .line 1731829
    const/4 v3, 0x4

    invoke-virtual {p0, v3, v5}, LX/186;->b(II)V

    .line 1731830
    const/4 v3, 0x5

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->S()I

    move-result v4

    invoke-virtual {p0, v3, v4, v2}, LX/186;->a(III)V

    .line 1731831
    const/4 v3, 0x6

    invoke-virtual {p0, v3, v6}, LX/186;->b(II)V

    .line 1731832
    const/4 v3, 0x7

    invoke-virtual {p0, v3, v7}, LX/186;->b(II)V

    .line 1731833
    const/16 v3, 0x8

    invoke-virtual {p0, v3, v8}, LX/186;->b(II)V

    .line 1731834
    const/16 v3, 0x9

    invoke-virtual {p0, v3, v9}, LX/186;->b(II)V

    .line 1731835
    const/16 v3, 0xa

    invoke-virtual {p0, v3, v10}, LX/186;->b(II)V

    .line 1731836
    const/16 v3, 0xb

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->aq()Z

    move-result v4

    invoke-virtual {p0, v3, v4}, LX/186;->a(IZ)V

    .line 1731837
    const/16 v3, 0xc

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->aR()I

    move-result v4

    invoke-virtual {p0, v3, v4, v2}, LX/186;->a(III)V

    .line 1731838
    const/16 v3, 0xd

    invoke-virtual {p0, v3, v11}, LX/186;->b(II)V

    .line 1731839
    const/16 v3, 0xe

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->bB()I

    move-result v4

    invoke-virtual {p0, v3, v4, v2}, LX/186;->a(III)V

    .line 1731840
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 1731841
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto/16 :goto_1

    .line 1731842
    :cond_2
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v6

    const/4 v7, 0x0

    .line 1731843
    if-nez v6, :cond_3

    .line 1731844
    :goto_3
    move v6, v7

    .line 1731845
    const/4 v7, 0x1

    invoke-virtual {p0, v7}, LX/186;->c(I)V

    .line 1731846
    invoke-virtual {p0, v5, v6}, LX/186;->b(II)V

    .line 1731847
    invoke-virtual {p0}, LX/186;->d()I

    move-result v5

    .line 1731848
    invoke-virtual {p0, v5}, LX/186;->d(I)V

    goto/16 :goto_2

    .line 1731849
    :cond_3
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLFeedback;->r()Lcom/facebook/graphql/model/GraphQLCommentsConnection;

    move-result-object v8

    const/4 v9, 0x0

    .line 1731850
    if-nez v8, :cond_4

    .line 1731851
    :goto_4
    move v8, v9

    .line 1731852
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 1731853
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 1731854
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLFeedback;->F()Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    move-result-object v11

    const/4 p1, 0x0

    .line 1731855
    if-nez v11, :cond_5

    .line 1731856
    :goto_5
    move v11, p1

    .line 1731857
    const/16 p1, 0x8

    invoke-virtual {p0, p1}, LX/186;->c(I)V

    .line 1731858
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLFeedback;->b()Z

    move-result p1

    invoke-virtual {p0, v7, p1}, LX/186;->a(IZ)V

    .line 1731859
    const/4 v7, 0x1

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLFeedback;->c()Z

    move-result p1

    invoke-virtual {p0, v7, p1}, LX/186;->a(IZ)V

    .line 1731860
    const/4 v7, 0x2

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLFeedback;->r_()Z

    move-result p1

    invoke-virtual {p0, v7, p1}, LX/186;->a(IZ)V

    .line 1731861
    const/4 v7, 0x3

    invoke-virtual {p0, v7, v8}, LX/186;->b(II)V

    .line 1731862
    const/4 v7, 0x4

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLFeedback;->s_()Z

    move-result v8

    invoke-virtual {p0, v7, v8}, LX/186;->a(IZ)V

    .line 1731863
    const/4 v7, 0x5

    invoke-virtual {p0, v7, v9}, LX/186;->b(II)V

    .line 1731864
    const/4 v7, 0x6

    invoke-virtual {p0, v7, v10}, LX/186;->b(II)V

    .line 1731865
    const/4 v7, 0x7

    invoke-virtual {p0, v7, v11}, LX/186;->b(II)V

    .line 1731866
    invoke-virtual {p0}, LX/186;->d()I

    move-result v7

    .line 1731867
    invoke-virtual {p0, v7}, LX/186;->d(I)V

    goto :goto_3

    .line 1731868
    :cond_4
    const/4 v10, 0x1

    invoke-virtual {p0, v10}, LX/186;->c(I)V

    .line 1731869
    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLCommentsConnection;->a()I

    move-result v10

    invoke-virtual {p0, v9, v10, v9}, LX/186;->a(III)V

    .line 1731870
    invoke-virtual {p0}, LX/186;->d()I

    move-result v9

    .line 1731871
    invoke-virtual {p0, v9}, LX/186;->d(I)V

    goto :goto_4

    .line 1731872
    :cond_5
    const/4 v4, 0x1

    invoke-virtual {p0, v4}, LX/186;->c(I)V

    .line 1731873
    invoke-virtual {v11}, Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;->a()I

    move-result v4

    invoke-virtual {p0, p1, v4, p1}, LX/186;->a(III)V

    .line 1731874
    invoke-virtual {p0}, LX/186;->d()I

    move-result p1

    .line 1731875
    invoke-virtual {p0, p1}, LX/186;->d(I)V

    goto :goto_5
.end method

.method public static a(LX/186;Lcom/facebook/graphql/model/GraphQLVect2;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    .line 1731876
    if-nez p1, :cond_0

    .line 1731877
    :goto_0
    return v1

    .line 1731878
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, LX/186;->c(I)V

    .line 1731879
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLVect2;->a()D

    move-result-wide v2

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1731880
    const/4 v1, 0x1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLVect2;->b()D

    move-result-wide v2

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1731881
    invoke-virtual {p0}, LX/186;->d()I

    move-result v1

    .line 1731882
    invoke-virtual {p0, v1}, LX/186;->d(I)V

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLNode;)Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel;
    .locals 15
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1731883
    if-nez p0, :cond_1

    .line 1731884
    :cond_0
    :goto_0
    return-object v2

    .line 1731885
    :cond_1
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1731886
    const/4 v1, 0x0

    .line 1731887
    if-nez p0, :cond_4

    .line 1731888
    :cond_2
    :goto_1
    move v1, v1

    .line 1731889
    if-eqz v1, :cond_0

    .line 1731890
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1731891
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1731892
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1731893
    new-instance v0, LX/15i;

    const/4 v4, 0x1

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1731894
    instance-of v1, p0, Lcom/facebook/flatbuffers/Flattenable;

    if-eqz v1, :cond_3

    .line 1731895
    const-string v1, "SouvenirsConversionHelper.getSouvenirsDetailsFields"

    invoke-virtual {v0, v1, p0}, LX/15i;->a(Ljava/lang/String;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1731896
    :cond_3
    new-instance v2, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel;

    invoke-direct {v2, v0}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel;-><init>(LX/15i;)V

    goto :goto_0

    .line 1731897
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    .line 1731898
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v3

    const v4, 0x6c2aa72f

    if-ne v3, v4, :cond_2

    .line 1731899
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->nM()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v3

    const/4 v9, 0x1

    const/4 v6, 0x0

    .line 1731900
    if-nez v3, :cond_5

    .line 1731901
    :goto_2
    move v3, v6

    .line 1731902
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->nO()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1731903
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1731904
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->fp()Lcom/facebook/graphql/model/GraphQLSouvenirMediaConnection;

    move-result-object v6

    const/4 v12, 0x1

    const/4 v9, 0x0

    .line 1731905
    if-nez v6, :cond_a

    .line 1731906
    :goto_3
    move v6, v9

    .line 1731907
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->iM()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v7

    invoke-static {v0, v7}, LX/Az0;->a(LX/186;Lcom/facebook/graphql/model/GraphQLPhoto;)I

    move-result v7

    .line 1731908
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->jL()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v8

    const/4 v9, 0x0

    .line 1731909
    if-nez v8, :cond_d

    .line 1731910
    :goto_4
    move v8, v9

    .line 1731911
    const/4 v9, 0x6

    invoke-virtual {v0, v9}, LX/186;->c(I)V

    .line 1731912
    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 1731913
    const/4 v1, 0x1

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 1731914
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 1731915
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 1731916
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 1731917
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v8}, LX/186;->b(II)V

    .line 1731918
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 1731919
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    goto/16 :goto_1

    .line 1731920
    :cond_5
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v7

    .line 1731921
    if-eqz v7, :cond_7

    .line 1731922
    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v4

    new-array v8, v4, [I

    move v5, v6

    .line 1731923
    :goto_5
    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v4

    if-ge v5, v4, :cond_6

    .line 1731924
    invoke-virtual {v7, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLActor;

    const/4 v10, 0x0

    .line 1731925
    if-nez v4, :cond_8

    .line 1731926
    :goto_6
    move v4, v10

    .line 1731927
    aput v4, v8, v5

    .line 1731928
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_5

    .line 1731929
    :cond_6
    invoke-virtual {v0, v8, v9}, LX/186;->a([IZ)I

    move-result v4

    .line 1731930
    :goto_7
    invoke-virtual {v0, v9}, LX/186;->c(I)V

    .line 1731931
    invoke-virtual {v0, v6, v4}, LX/186;->b(II)V

    .line 1731932
    invoke-virtual {v0}, LX/186;->d()I

    move-result v6

    .line 1731933
    invoke-virtual {v0, v6}, LX/186;->d(I)V

    goto/16 :goto_2

    :cond_7
    move v4, v6

    goto :goto_7

    .line 1731934
    :cond_8
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLActor;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v11

    invoke-virtual {v0, v11}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v11

    .line 1731935
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v0, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 1731936
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v13

    const/4 v14, 0x0

    .line 1731937
    if-nez v13, :cond_9

    .line 1731938
    :goto_8
    move v13, v14

    .line 1731939
    const/4 v14, 0x3

    invoke-virtual {v0, v14}, LX/186;->c(I)V

    .line 1731940
    invoke-virtual {v0, v10, v11}, LX/186;->b(II)V

    .line 1731941
    const/4 v10, 0x1

    invoke-virtual {v0, v10, v12}, LX/186;->b(II)V

    .line 1731942
    const/4 v10, 0x2

    invoke-virtual {v0, v10, v13}, LX/186;->b(II)V

    .line 1731943
    invoke-virtual {v0}, LX/186;->d()I

    move-result v10

    .line 1731944
    invoke-virtual {v0, v10}, LX/186;->d(I)V

    goto :goto_6

    .line 1731945
    :cond_9
    invoke-virtual {v13}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1731946
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 1731947
    invoke-virtual {v0, v14, v3}, LX/186;->b(II)V

    .line 1731948
    invoke-virtual {v0}, LX/186;->d()I

    move-result v14

    .line 1731949
    invoke-virtual {v0, v14}, LX/186;->d(I)V

    goto :goto_8

    .line 1731950
    :cond_a
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLSouvenirMediaConnection;->a()LX/0Px;

    move-result-object v10

    .line 1731951
    if-eqz v10, :cond_c

    .line 1731952
    invoke-virtual {v10}, LX/0Px;->size()I

    move-result v7

    new-array v11, v7, [I

    move v8, v9

    .line 1731953
    :goto_9
    invoke-virtual {v10}, LX/0Px;->size()I

    move-result v7

    if-ge v8, v7, :cond_b

    .line 1731954
    invoke-virtual {v10, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/facebook/graphql/model/GraphQLSouvenirMediaEdge;

    invoke-static {v0, v7}, LX/Az0;->a(LX/186;Lcom/facebook/graphql/model/GraphQLSouvenirMediaEdge;)I

    move-result v7

    aput v7, v11, v8

    .line 1731955
    add-int/lit8 v7, v8, 0x1

    move v8, v7

    goto :goto_9

    .line 1731956
    :cond_b
    invoke-virtual {v0, v11, v12}, LX/186;->a([IZ)I

    move-result v7

    .line 1731957
    :goto_a
    invoke-virtual {v0, v12}, LX/186;->c(I)V

    .line 1731958
    invoke-virtual {v0, v9, v7}, LX/186;->b(II)V

    .line 1731959
    invoke-virtual {v0}, LX/186;->d()I

    move-result v9

    .line 1731960
    invoke-virtual {v0, v9}, LX/186;->d(I)V

    goto/16 :goto_3

    :cond_c
    move v7, v9

    goto :goto_a

    .line 1731961
    :cond_d
    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 1731962
    const/4 v11, 0x1

    invoke-virtual {v0, v11}, LX/186;->c(I)V

    .line 1731963
    invoke-virtual {v0, v9, v10}, LX/186;->b(II)V

    .line 1731964
    invoke-virtual {v0}, LX/186;->d()I

    move-result v9

    .line 1731965
    invoke-virtual {v0, v9}, LX/186;->d(I)V

    goto/16 :goto_4
.end method

.method public static a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;
    .locals 2

    .prologue
    .line 1731966
    if-nez p0, :cond_0

    .line 1731967
    const/4 v0, 0x0

    .line 1731968
    :goto_0
    return-object v0

    .line 1731969
    :cond_0
    new-instance v0, LX/2dc;

    invoke-direct {v0}, LX/2dc;-><init>()V

    .line 1731970
    invoke-interface {p0}, LX/1Fb;->a()I

    move-result v1

    .line 1731971
    iput v1, v0, LX/2dc;->c:I

    .line 1731972
    invoke-interface {p0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v1

    .line 1731973
    iput-object v1, v0, LX/2dc;->h:Ljava/lang/String;

    .line 1731974
    invoke-interface {p0}, LX/1Fb;->c()I

    move-result v1

    .line 1731975
    iput v1, v0, LX/2dc;->i:I

    .line 1731976
    invoke-virtual {v0}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel;)Lcom/facebook/graphql/model/GraphQLPhoto;
    .locals 8
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1731977
    if-nez p0, :cond_1

    .line 1731978
    :cond_0
    :goto_0
    return-object v0

    .line 1731979
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    .line 1731980
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v1

    const v2, 0x4984e12

    if-ne v1, v2, :cond_0

    .line 1731981
    new-instance v0, LX/4Xy;

    invoke-direct {v0}, LX/4Xy;-><init>()V

    .line 1731982
    invoke-virtual {p0}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel;->k()Z

    move-result v1

    .line 1731983
    iput-boolean v1, v0, LX/4Xy;->l:Z

    .line 1731984
    invoke-virtual {p0}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel;->l()Z

    move-result v1

    .line 1731985
    iput-boolean v1, v0, LX/4Xy;->r:Z

    .line 1731986
    invoke-virtual {p0}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel;->m()Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel$CreationStoryModel;

    move-result-object v1

    invoke-static {v1}, LX/Az0;->a(Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel$CreationStoryModel;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 1731987
    iput-object v1, v0, LX/4Xy;->z:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1731988
    invoke-virtual {p0}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel;->c()LX/1f8;

    move-result-object v1

    .line 1731989
    if-nez v1, :cond_2

    .line 1731990
    const/4 v3, 0x0

    .line 1731991
    :goto_1
    move-object v1, v3

    .line 1731992
    iput-object v1, v0, LX/4Xy;->E:Lcom/facebook/graphql/model/GraphQLVect2;

    .line 1731993
    invoke-virtual {p0}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel;->d()Ljava/lang/String;

    move-result-object v1

    .line 1731994
    iput-object v1, v0, LX/4Xy;->I:Ljava/lang/String;

    .line 1731995
    invoke-virtual {p0}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel;->e()LX/1Fb;

    move-result-object v1

    invoke-static {v1}, LX/Az0;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 1731996
    iput-object v1, v0, LX/4Xy;->J:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1731997
    invoke-virtual {p0}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel;->aj_()LX/1Fb;

    move-result-object v1

    invoke-static {v1}, LX/Az0;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 1731998
    iput-object v1, v0, LX/4Xy;->M:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1731999
    invoke-virtual {p0}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel;->ai_()LX/1Fb;

    move-result-object v1

    invoke-static {v1}, LX/Az0;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 1732000
    iput-object v1, v0, LX/4Xy;->S:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1732001
    invoke-virtual {p0}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel;->j()LX/1Fb;

    move-result-object v1

    invoke-static {v1}, LX/Az0;->a(LX/1Fb;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    .line 1732002
    iput-object v1, v0, LX/4Xy;->V:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1732003
    invoke-virtual {p0}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel;->o()Z

    move-result v1

    .line 1732004
    iput-boolean v1, v0, LX/4Xy;->ai:Z

    .line 1732005
    invoke-virtual {p0}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel;->p()I

    move-result v1

    .line 1732006
    iput v1, v0, LX/4Xy;->aI:I

    .line 1732007
    invoke-virtual {p0}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel;->q()Ljava/lang/String;

    move-result-object v1

    .line 1732008
    iput-object v1, v0, LX/4Xy;->aJ:Ljava/lang/String;

    .line 1732009
    invoke-virtual {v0}, LX/4Xy;->a()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    goto :goto_0

    .line 1732010
    :cond_2
    new-instance v3, LX/4ZN;

    invoke-direct {v3}, LX/4ZN;-><init>()V

    .line 1732011
    invoke-interface {v1}, LX/1f8;->a()D

    move-result-wide v5

    .line 1732012
    iput-wide v5, v3, LX/4ZN;->b:D

    .line 1732013
    invoke-interface {v1}, LX/1f8;->b()D

    move-result-wide v5

    .line 1732014
    iput-wide v5, v3, LX/4ZN;->c:D

    .line 1732015
    invoke-virtual {v3}, LX/4ZN;->a()Lcom/facebook/graphql/model/GraphQLVect2;

    move-result-object v3

    goto :goto_1
.end method

.method public static a(Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel$CreationStoryModel;)Lcom/facebook/graphql/model/GraphQLStory;
    .locals 5

    .prologue
    .line 1732016
    if-nez p0, :cond_0

    .line 1732017
    const/4 v0, 0x0

    .line 1732018
    :goto_0
    return-object v0

    .line 1732019
    :cond_0
    new-instance v0, LX/23u;

    invoke-direct {v0}, LX/23u;-><init>()V

    .line 1732020
    invoke-virtual {p0}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel$CreationStoryModel;->a()Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel$CreationStoryModel$FeedbackModel;

    move-result-object v1

    .line 1732021
    if-nez v1, :cond_1

    .line 1732022
    const/4 v2, 0x0

    .line 1732023
    :goto_1
    move-object v1, v2

    .line 1732024
    iput-object v1, v0, LX/23u;->D:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1732025
    invoke-virtual {v0}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    goto :goto_0

    .line 1732026
    :cond_1
    new-instance v2, LX/3dM;

    invoke-direct {v2}, LX/3dM;-><init>()V

    .line 1732027
    invoke-virtual {v1}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel$CreationStoryModel$FeedbackModel;->b()Z

    move-result v3

    .line 1732028
    iput-boolean v3, v2, LX/3dM;->e:Z

    .line 1732029
    invoke-virtual {v1}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel$CreationStoryModel$FeedbackModel;->c()Z

    move-result v3

    invoke-virtual {v2, v3}, LX/3dM;->c(Z)LX/3dM;

    .line 1732030
    invoke-virtual {v1}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel$CreationStoryModel$FeedbackModel;->d()Z

    move-result v3

    invoke-virtual {v2, v3}, LX/3dM;->g(Z)LX/3dM;

    .line 1732031
    invoke-virtual {v1}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel$CreationStoryModel$FeedbackModel;->e()Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel$CreationStoryModel$FeedbackModel$CommentsModel;

    move-result-object v3

    .line 1732032
    if-nez v3, :cond_2

    .line 1732033
    const/4 v4, 0x0

    .line 1732034
    :goto_2
    move-object v3, v4

    .line 1732035
    iput-object v3, v2, LX/3dM;->n:Lcom/facebook/graphql/model/GraphQLCommentsConnection;

    .line 1732036
    invoke-virtual {v1}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel$CreationStoryModel$FeedbackModel;->io_()Z

    move-result v3

    invoke-virtual {v2, v3}, LX/3dM;->j(Z)LX/3dM;

    .line 1732037
    invoke-virtual {v1}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel$CreationStoryModel$FeedbackModel;->ip_()Ljava/lang/String;

    move-result-object v3

    .line 1732038
    iput-object v3, v2, LX/3dM;->y:Ljava/lang/String;

    .line 1732039
    invoke-virtual {v1}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel$CreationStoryModel$FeedbackModel;->j()Ljava/lang/String;

    move-result-object v3

    .line 1732040
    iput-object v3, v2, LX/3dM;->D:Ljava/lang/String;

    .line 1732041
    invoke-virtual {v1}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel$CreationStoryModel$FeedbackModel;->k()Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel$CreationStoryModel$FeedbackModel$LikersModel;

    move-result-object v3

    .line 1732042
    if-nez v3, :cond_3

    .line 1732043
    const/4 v4, 0x0

    .line 1732044
    :goto_3
    move-object v3, v4

    .line 1732045
    invoke-virtual {v2, v3}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;)LX/3dM;

    .line 1732046
    invoke-virtual {v2}, LX/3dM;->a()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    goto :goto_1

    .line 1732047
    :cond_2
    new-instance v4, LX/4Vv;

    invoke-direct {v4}, LX/4Vv;-><init>()V

    .line 1732048
    invoke-virtual {v3}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel$CreationStoryModel$FeedbackModel$CommentsModel;->a()I

    move-result p0

    .line 1732049
    iput p0, v4, LX/4Vv;->b:I

    .line 1732050
    invoke-virtual {v4}, LX/4Vv;->a()Lcom/facebook/graphql/model/GraphQLCommentsConnection;

    move-result-object v4

    goto :goto_2

    .line 1732051
    :cond_3
    new-instance v4, LX/3dN;

    invoke-direct {v4}, LX/3dN;-><init>()V

    .line 1732052
    invoke-virtual {v3}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsMediaFieldsModel$CreationStoryModel$FeedbackModel$LikersModel;->a()I

    move-result p0

    .line 1732053
    iput p0, v4, LX/3dN;->b:I

    .line 1732054
    invoke-virtual {v4}, LX/3dN;->a()Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    move-result-object v4

    goto :goto_3
.end method

.method public static b(LX/186;Lcom/facebook/graphql/model/GraphQLImage;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1732055
    if-nez p1, :cond_0

    .line 1732056
    :goto_0
    return v0

    .line 1732057
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1732058
    const/4 v2, 0x3

    invoke-virtual {p0, v2}, LX/186;->c(I)V

    .line 1732059
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v2

    invoke-virtual {p0, v0, v2, v0}, LX/186;->a(III)V

    .line 1732060
    const/4 v2, 0x1

    invoke-virtual {p0, v2, v1}, LX/186;->b(II)V

    .line 1732061
    const/4 v1, 0x2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v2

    invoke-virtual {p0, v1, v2, v0}, LX/186;->a(III)V

    .line 1732062
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 1732063
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0
.end method
