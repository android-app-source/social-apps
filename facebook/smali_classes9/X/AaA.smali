.class public LX/AaA;
.super LX/AWT;
.source ""

# interfaces
.implements LX/AZR;
.implements LX/AZq;
.implements LX/Aa3;


# static fields
.field private static final h:Ljava/lang/String;


# instance fields
.field public a:LX/AZW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/AZj;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/AZu;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/AVT;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/1b4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final i:Lcom/facebook/facecast/plugin/creativetools/CreativeToolsPackSelectorTray;

.field public final j:Landroid/widget/FrameLayout;

.field private final k:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Lcom/facebook/widget/text/BetterTextView;",
            ">;"
        }
    .end annotation
.end field

.field public final l:Landroid/widget/LinearLayout;

.field private m:LX/AZZ;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:LX/AZU;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:LX/AZV;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private p:LX/AZi;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:LX/AZt;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:LX/AVP;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Lcom/facebook/facecast/FacecastPreviewView;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:LX/AV1;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:LX/AYC;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1688054
    const-class v0, LX/AaA;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/AaA;->h:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1688057
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/AaA;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1688058
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1688055
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/AaA;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1688056
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    .line 1687925
    invoke-direct {p0, p1, p2, p3}, LX/AWT;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1687926
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, LX/AaA;

    const-class v3, LX/AZW;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/AZW;

    const-class p1, LX/AZj;

    invoke-interface {v0, p1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object p1

    check-cast p1, LX/AZj;

    const-class p2, LX/AZu;

    invoke-interface {v0, p2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object p2

    check-cast p2, LX/AZu;

    invoke-static {v0}, LX/AVT;->a(LX/0QB;)LX/AVT;

    move-result-object p3

    check-cast p3, LX/AVT;

    invoke-static {v0}, LX/1b4;->b(LX/0QB;)LX/1b4;

    move-result-object v0

    check-cast v0, LX/1b4;

    iput-object v3, v2, LX/AaA;->a:LX/AZW;

    iput-object p1, v2, LX/AaA;->b:LX/AZj;

    iput-object p2, v2, LX/AaA;->c:LX/AZu;

    iput-object p3, v2, LX/AaA;->f:LX/AVT;

    iput-object v0, v2, LX/AaA;->g:LX/1b4;

    .line 1687927
    invoke-virtual {p0}, LX/AaA;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, LX/03r;->CreativeTools:[I

    invoke-virtual {v0, v1}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1687928
    const/16 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 1687929
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1687930
    invoke-virtual {p0, v1}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1687931
    const v0, 0x7f0d0fd8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsPackSelectorTray;

    iput-object v0, p0, LX/AaA;->i:Lcom/facebook/facecast/plugin/creativetools/CreativeToolsPackSelectorTray;

    .line 1687932
    const v0, 0x7f0d0fd6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, LX/AaA;->j:Landroid/widget/FrameLayout;

    .line 1687933
    const v0, 0x7f0d0fd7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/AaA;->l:Landroid/widget/LinearLayout;

    .line 1687934
    const v0, 0x7f0d0fd9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->b(I)LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/AaA;->k:LX/0am;

    .line 1687935
    iget-object v0, p0, LX/AaA;->i:Lcom/facebook/facecast/plugin/creativetools/CreativeToolsPackSelectorTray;

    .line 1687936
    iput-object p0, v0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsPackSelectorTray;->g:LX/Aa3;

    .line 1687937
    return-void
.end method


# virtual methods
.method public final a(LX/AZZ;)V
    .locals 13

    .prologue
    const/4 v1, 0x0

    const/4 v6, -0x1

    .line 1687983
    iget-object v0, p0, LX/AaA;->m:LX/AZZ;

    if-ne v0, p1, :cond_1

    .line 1687984
    :cond_0
    :goto_0
    return-void

    .line 1687985
    :cond_1
    iget-object v2, p0, LX/AaA;->m:LX/AZZ;

    .line 1687986
    iput-object p1, p0, LX/AaA;->m:LX/AZZ;

    .line 1687987
    iget-object v0, p0, LX/AaA;->k:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1687988
    iget-object v0, p0, LX/AaA;->k:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p1}, LX/AZZ;->a()I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/facebook/widget/text/BetterTextView;->setText(I)V

    .line 1687989
    :cond_2
    sget-object v0, LX/Aa9;->a:[I

    invoke-virtual {p1}, LX/AZZ;->c()LX/Aa1;

    move-result-object v3

    invoke-virtual {v3}, LX/Aa1;->ordinal()I

    move-result v3

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_0

    move-object v0, v1

    .line 1687990
    :goto_1
    iget-object v3, p0, LX/AaA;->n:LX/AZU;

    if-eq v0, v3, :cond_0

    .line 1687991
    iget-object v3, p0, LX/AaA;->n:LX/AZU;

    if-eqz v3, :cond_3

    .line 1687992
    iget-object v3, p0, LX/AaA;->n:LX/AZU;

    iget-object v4, p0, LX/AaA;->s:Lcom/facebook/facecast/FacecastPreviewView;

    invoke-interface {v3, v4}, LX/AZU;->b(Lcom/facebook/facecast/FacecastPreviewView;)V

    .line 1687993
    iget-object v3, p0, LX/AaA;->l:Landroid/widget/LinearLayout;

    iget-object v4, p0, LX/AaA;->n:LX/AZU;

    invoke-interface {v4}, LX/AZU;->c()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->removeView(Landroid/view/View;)V

    .line 1687994
    iget-object v3, p0, LX/AaA;->j:Landroid/widget/FrameLayout;

    iget-object v4, p0, LX/AaA;->n:LX/AZU;

    invoke-interface {v4}, LX/AZU;->b()Landroid/view/View;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    .line 1687995
    :cond_3
    iput-object v0, p0, LX/AaA;->n:LX/AZU;

    .line 1687996
    if-nez v0, :cond_8

    .line 1687997
    invoke-virtual {p0}, LX/AaA;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {p1}, LX/AZZ;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1687998
    sget-object v1, LX/AaA;->h:Ljava/lang/String;

    const-string v2, "Cannot show %s pack"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 1687999
    :pswitch_0
    iget-object v0, p0, LX/AaA;->o:LX/AZV;

    if-nez v0, :cond_4

    .line 1688000
    iget-object v0, p0, LX/AaA;->r:LX/AVP;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1688001
    iget-object v0, p0, LX/AaA;->a:LX/AZW;

    invoke-virtual {p0}, LX/AaA;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, LX/AaA;->r:LX/AVP;

    .line 1688002
    new-instance v7, LX/AZV;

    invoke-static {v0}, LX/AVT;->a(LX/0QB;)LX/AVT;

    move-result-object v5

    check-cast v5, LX/AVT;

    invoke-direct {v7, v3, v4, v5}, LX/AZV;-><init>(Landroid/content/Context;LX/AVP;LX/AVT;)V

    .line 1688003
    move-object v0, v7

    .line 1688004
    iput-object v0, p0, LX/AaA;->o:LX/AZV;

    .line 1688005
    iget-object v0, p0, LX/AaA;->o:LX/AZV;

    .line 1688006
    iput-object p0, v0, LX/AZV;->g:LX/AZR;

    .line 1688007
    :cond_4
    iget-object v3, p0, LX/AaA;->o:LX/AZV;

    move-object v0, p1

    check-cast v0, LX/AZa;

    const/4 v11, 0x0

    const/4 v10, 0x1

    .line 1688008
    iget-object v4, v3, LX/AZV;->h:LX/AZa;

    if-ne v0, v4, :cond_b

    .line 1688009
    :cond_5
    iget-object v0, p0, LX/AaA;->o:LX/AZV;

    goto :goto_1

    .line 1688010
    :pswitch_1
    iget-object v0, p0, LX/AaA;->p:LX/AZi;

    if-nez v0, :cond_6

    .line 1688011
    iget-object v0, p0, LX/AaA;->r:LX/AVP;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1688012
    iget-object v0, p0, LX/AaA;->b:LX/AZj;

    invoke-virtual {p0}, LX/AaA;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, LX/AaA;->r:LX/AVP;

    .line 1688013
    new-instance v7, LX/AZi;

    invoke-static {v0}, LX/AVT;->a(LX/0QB;)LX/AVT;

    move-result-object v5

    check-cast v5, LX/AVT;

    invoke-direct {v7, v3, v4, v5}, LX/AZi;-><init>(Landroid/content/Context;LX/AVP;LX/AVT;)V

    .line 1688014
    move-object v0, v7

    .line 1688015
    iput-object v0, p0, LX/AaA;->p:LX/AZi;

    .line 1688016
    :cond_6
    iget-object v3, p0, LX/AaA;->p:LX/AZi;

    move-object v0, p1

    check-cast v0, LX/AZe;

    .line 1688017
    iput-object v0, v3, LX/AZi;->h:LX/AZe;

    .line 1688018
    iget-object v4, v3, LX/AZi;->c:Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;

    invoke-virtual {v4, v0}, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;->setColorDoodlePack(LX/AZe;)V

    .line 1688019
    iget-object v0, p0, LX/AaA;->p:LX/AZi;

    iget-object v3, p0, LX/AaA;->u:LX/AYC;

    .line 1688020
    iput-object v3, v0, LX/AZi;->i:LX/AYB;

    .line 1688021
    iget-object v0, p0, LX/AaA;->p:LX/AZi;

    goto/16 :goto_1

    .line 1688022
    :pswitch_2
    iget-object v0, p0, LX/AaA;->q:LX/AZt;

    if-nez v0, :cond_7

    .line 1688023
    iget-object v0, p0, LX/AaA;->r:LX/AVP;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1688024
    iget-object v0, p0, LX/AaA;->t:LX/AV1;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1688025
    iget-object v0, p0, LX/AaA;->c:LX/AZu;

    invoke-virtual {p0}, LX/AaA;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, LX/AaA;->r:LX/AVP;

    iget-object v5, p0, LX/AaA;->t:LX/AV1;

    invoke-virtual {v0, v3, v4, v5}, LX/AZu;->a(Landroid/content/Context;LX/AVP;LX/AV1;)LX/AZt;

    move-result-object v0

    iput-object v0, p0, LX/AaA;->q:LX/AZt;

    .line 1688026
    iget-object v0, p0, LX/AaA;->q:LX/AZt;

    .line 1688027
    iput-object p0, v0, LX/AZt;->l:LX/AZq;

    .line 1688028
    :cond_7
    iget-object v3, p0, LX/AaA;->q:LX/AZt;

    move-object v0, p1

    check-cast v0, LX/AZm;

    invoke-virtual {v3, v0}, LX/AZt;->a(LX/AZm;)V

    .line 1688029
    iget-object v0, p0, LX/AaA;->q:LX/AZt;

    goto/16 :goto_1

    .line 1688030
    :cond_8
    invoke-interface {v0}, LX/AZU;->c()Landroid/view/View;

    move-result-object v3

    if-eqz v3, :cond_9

    .line 1688031
    iget-object v3, p0, LX/AaA;->l:Landroid/widget/LinearLayout;

    invoke-interface {v0}, LX/AZU;->c()Landroid/view/View;

    move-result-object v4

    const/4 v5, -0x2

    invoke-virtual {v3, v4, v5, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;II)V

    .line 1688032
    :cond_9
    iget-object v3, p0, LX/AaA;->j:Landroid/widget/FrameLayout;

    invoke-interface {v0}, LX/AZU;->b()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v3, v0, v6, v6}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;II)V

    .line 1688033
    iget-object v0, p0, LX/AaA;->n:LX/AZU;

    iget-object v3, p0, LX/AaA;->s:Lcom/facebook/facecast/FacecastPreviewView;

    invoke-interface {v0, v3}, LX/AZU;->a(Lcom/facebook/facecast/FacecastPreviewView;)V

    .line 1688034
    iget-object v0, p0, LX/AaA;->f:LX/AVT;

    iget-object v3, p0, LX/AaA;->m:LX/AZZ;

    invoke-virtual {v3}, LX/AZZ;->d()Ljava/lang/String;

    move-result-object v3

    if-nez v2, :cond_a

    :goto_2
    const/4 v11, 0x0

    .line 1688035
    const-string v10, "creative_tool_did_show"

    move-object v7, v0

    move-object v8, v1

    move-object v9, v3

    move-object v12, v11

    invoke-virtual/range {v7 .. v12}, LX/AVT;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/video/videostreaming/LiveStreamingError;Ljava/util/Map;)V

    .line 1688036
    goto/16 :goto_0

    :cond_a
    invoke-virtual {v2}, LX/AZZ;->d()Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    .line 1688037
    :cond_b
    iput-object v0, v3, LX/AZV;->h:LX/AZa;

    .line 1688038
    iget-object v4, v3, LX/AZV;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 1688039
    iget-object v4, v3, LX/AZV;->e:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_c

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsBasicAdjustmentFilterView;

    .line 1688040
    invoke-virtual {v4}, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsBasicAdjustmentFilterView;->a()V

    goto :goto_3

    .line 1688041
    :cond_c
    iput v11, v3, LX/AZV;->i:I

    .line 1688042
    iput-boolean v10, v3, LX/AZV;->j:Z

    .line 1688043
    iget-object v4, v3, LX/AZV;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v7

    .line 1688044
    iget-object v4, v3, LX/AZV;->h:LX/AZa;

    invoke-virtual {v4}, LX/AZZ;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_d
    :goto_4
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/AZO;

    .line 1688045
    const v5, 0x7f0305b9

    iget-object v9, v3, LX/AZV;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v7, v5, v9, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsBasicAdjustmentFilterView;

    .line 1688046
    iput-object v3, v5, LX/AZX;->e:LX/AZT;

    .line 1688047
    iget-object v9, v3, LX/AZV;->b:LX/AVP;

    .line 1688048
    iput-object v9, v5, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsBasicAdjustmentFilterView;->b:LX/AVP;

    .line 1688049
    invoke-virtual {v5, v4}, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsBasicAdjustmentFilterView;->a(LX/AZO;)V

    .line 1688050
    iget-object v4, v3, LX/AZV;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1688051
    iget-object v4, v3, LX/AZV;->e:Ljava/util/List;

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1688052
    iget-object v4, v3, LX/AZV;->e:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ne v4, v10, :cond_d

    .line 1688053
    invoke-virtual {v5, v10}, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsBasicAdjustmentFilterView;->setSelected(Z)V

    goto :goto_4

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Landroid/view/ViewGroup;LX/AVF;)V
    .locals 1

    .prologue
    .line 1687978
    invoke-super {p0, p1, p2}, LX/AWT;->a(Landroid/view/ViewGroup;LX/AVF;)V

    .line 1687979
    iget-object v0, p0, LX/AaA;->q:LX/AZt;

    if-eqz v0, :cond_0

    .line 1687980
    iget-object v0, p0, LX/AaA;->q:LX/AZt;

    .line 1687981
    iget-object p0, v0, LX/AZt;->c:LX/AMO;

    invoke-virtual {p0}, LX/AMO;->a()V

    .line 1687982
    :cond_0
    return-void
.end method

.method public final a(ZJ)V
    .locals 6

    .prologue
    .line 1688059
    iget-object v0, p0, LX/AaA;->i:Lcom/facebook/facecast/plugin/creativetools/CreativeToolsPackSelectorTray;

    const-wide/16 v4, 0xfa

    move v1, p1

    move-wide v2, p2

    invoke-static/range {v0 .. v5}, LX/3Hk;->a(Landroid/view/View;ZJJ)V

    .line 1688060
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1687974
    iget-object v0, p0, LX/AaA;->o:LX/AZV;

    if-eqz v0, :cond_0

    .line 1687975
    iget-object v0, p0, LX/AaA;->o:LX/AZV;

    .line 1687976
    const/4 p0, 0x0

    invoke-static {v0, p0}, LX/AZV;->a$redex0(LX/AZV;I)V

    .line 1687977
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 1687965
    iget-object v0, p0, LX/AaA;->i:Lcom/facebook/facecast/plugin/creativetools/CreativeToolsPackSelectorTray;

    invoke-virtual {v0, v1}, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsPackSelectorTray;->setVisibility(I)V

    .line 1687966
    iget-object v0, p0, LX/AaA;->j:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1687967
    iget-object v0, p0, LX/AaA;->l:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1687968
    iget-object v0, p0, LX/AaA;->r:LX/AVP;

    if-eqz v0, :cond_0

    .line 1687969
    iget-object v0, p0, LX/AaA;->r:LX/AVP;

    const/4 v1, 0x0

    .line 1687970
    iput-boolean v1, v0, LX/AVP;->z:Z

    .line 1687971
    :cond_0
    iget-object v0, p0, LX/AaA;->n:LX/AZU;

    if-eqz v0, :cond_1

    .line 1687972
    iget-object v0, p0, LX/AaA;->n:LX/AZU;

    iget-object v1, p0, LX/AaA;->s:Lcom/facebook/facecast/FacecastPreviewView;

    invoke-interface {v0, v1}, LX/AZU;->b(Lcom/facebook/facecast/FacecastPreviewView;)V

    .line 1687973
    :cond_1
    return-void
.end method

.method public final g()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1687955
    iget-object v0, p0, LX/AaA;->i:Lcom/facebook/facecast/plugin/creativetools/CreativeToolsPackSelectorTray;

    invoke-virtual {v0, v1}, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsPackSelectorTray;->setVisibility(I)V

    .line 1687956
    iget-object v0, p0, LX/AaA;->j:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1687957
    iget-object v0, p0, LX/AaA;->l:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1687958
    iget-object v0, p0, LX/AaA;->r:LX/AVP;

    if-eqz v0, :cond_0

    .line 1687959
    iget-object v0, p0, LX/AaA;->r:LX/AVP;

    const/4 v1, 0x1

    .line 1687960
    iput-boolean v1, v0, LX/AVP;->z:Z

    .line 1687961
    :cond_0
    iget-object v0, p0, LX/AaA;->n:LX/AZU;

    if-eqz v0, :cond_1

    .line 1687962
    iget-object v0, p0, LX/AaA;->n:LX/AZU;

    iget-object v1, p0, LX/AaA;->s:Lcom/facebook/facecast/FacecastPreviewView;

    invoke-interface {v0, v1}, LX/AZU;->a(Lcom/facebook/facecast/FacecastPreviewView;)V

    .line 1687963
    :cond_1
    iget-object v0, p0, LX/AaA;->i:Lcom/facebook/facecast/plugin/creativetools/CreativeToolsPackSelectorTray;

    invoke-virtual {v0}, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsPackSelectorTray;->a()V

    .line 1687964
    return-void
.end method

.method public getCreativeToolsTrayHeight()I
    .locals 3

    .prologue
    .line 1687951
    invoke-virtual {p0}, LX/AaA;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b05e1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 1687952
    iget-boolean v1, p0, LX/AaA;->v:Z

    if-eqz v1, :cond_0

    .line 1687953
    invoke-virtual {p0}, LX/AaA;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b05e2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sub-int/2addr v0, v1

    .line 1687954
    :cond_0
    return v0
.end method

.method public final iI_()V
    .locals 3

    .prologue
    .line 1687938
    iget-object v0, p0, LX/AaA;->q:LX/AZt;

    if-eqz v0, :cond_0

    .line 1687939
    iget-object v0, p0, LX/AaA;->q:LX/AZt;

    const/4 p0, 0x0

    .line 1687940
    iget v1, v0, LX/AZt;->n:I

    if-eqz v1, :cond_0

    .line 1687941
    iget-object v1, v0, LX/AZt;->m:LX/AZm;

    iget v2, v0, LX/AZt;->n:I

    invoke-virtual {v1, v2}, LX/AZZ;->a(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/AZl;

    .line 1687942
    if-eqz v1, :cond_0

    .line 1687943
    iput-boolean p0, v1, LX/AZl;->i:Z

    .line 1687944
    iget-object v1, v0, LX/AZt;->j:LX/AZr;

    iget v2, v0, LX/AZt;->n:I

    invoke-virtual {v1, v2}, LX/1OM;->i_(I)V

    .line 1687945
    iput p0, v0, LX/AZt;->n:I

    .line 1687946
    iget-object v1, v0, LX/AZt;->m:LX/AZm;

    iget v2, v0, LX/AZt;->n:I

    invoke-virtual {v1, v2}, LX/AZZ;->a(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/AZl;

    const/4 v2, 0x1

    .line 1687947
    iput-boolean v2, v1, LX/AZl;->i:Z

    .line 1687948
    iget-object v1, v0, LX/AZt;->j:LX/AZr;

    iget v2, v0, LX/AZt;->n:I

    invoke-virtual {v1, v2}, LX/1OM;->i_(I)V

    .line 1687949
    invoke-static {v0}, LX/AZt;->f(LX/AZt;)V

    .line 1687950
    :cond_0
    return-void
.end method
