.class public LX/CNn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/CNe;


# instance fields
.field private final a:LX/CNb;

.field private final b:LX/CNc;


# direct methods
.method public constructor <init>(LX/CNb;LX/CNc;)V
    .locals 0

    .prologue
    .line 1882108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1882109
    iput-object p1, p0, LX/CNn;->a:LX/CNb;

    .line 1882110
    iput-object p2, p0, LX/CNn;->b:LX/CNc;

    .line 1882111
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 11

    .prologue
    .line 1882112
    iget-object v0, p0, LX/CNn;->a:LX/CNb;

    const-string v1, "target-id"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/CNb;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1882113
    iget-object v0, p0, LX/CNn;->a:LX/CNb;

    const-string v2, "nil-attributes"

    invoke-virtual {v0, v2}, LX/CNb;->d(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    .line 1882114
    if-eqz v0, :cond_0

    .line 1882115
    iget-object v2, p0, LX/CNn;->b:LX/CNc;

    iget-object v2, v2, LX/CNc;->b:LX/CNS;

    invoke-virtual {v2}, LX/CNS;->a()V

    .line 1882116
    iget-object v2, p0, LX/CNn;->b:LX/CNc;

    iget-object v2, v2, LX/CNc;->b:LX/CNS;

    const/4 v4, 0x0

    .line 1882117
    iget-object v3, v2, LX/CNS;->d:LX/CNL;

    invoke-virtual {v3, v1}, LX/CNL;->a(Ljava/lang/String;)LX/CNb;

    move-result-object v5

    .line 1882118
    if-nez v5, :cond_1

    .line 1882119
    :goto_0
    iget-object v0, p0, LX/CNn;->b:LX/CNc;

    iget-object v0, v0, LX/CNc;->b:LX/CNS;

    invoke-virtual {v0}, LX/CNS;->b()V

    .line 1882120
    :cond_0
    return-void

    .line 1882121
    :cond_1
    new-instance v6, LX/CNa;

    invoke-direct {v6}, LX/CNa;-><init>()V

    .line 1882122
    new-instance v7, Ljava/util/HashSet;

    invoke-direct {v7, v0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    move v3, v4

    .line 1882123
    :goto_1
    invoke-virtual {v5}, LX/CNb;->b()I

    move-result v8

    if-ge v3, v8, :cond_3

    .line 1882124
    invoke-virtual {v5, v3}, LX/CNb;->a(I)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 1882125
    invoke-virtual {v5, v3}, LX/CNb;->a(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v3}, LX/CNb;->b(I)Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v6, v8, v9}, LX/CNa;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1882126
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 1882127
    :cond_3
    invoke-static {v5}, LX/3jU;->a(LX/CNb;)LX/3j9;

    move-result-object v3

    .line 1882128
    invoke-virtual {v3, v5}, LX/3j9;->a(LX/CNb;)Ljava/util/Set;

    move-result-object v3

    .line 1882129
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_4
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 1882130
    invoke-interface {v7, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_4

    .line 1882131
    invoke-virtual {v5, v3}, LX/CNb;->a(Ljava/lang/String;)LX/0Px;

    move-result-object v9

    .line 1882132
    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v10

    move v3, v4

    :goto_2
    if-ge v3, v10, :cond_4

    invoke-virtual {v9, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 1882133
    :cond_5
    invoke-virtual {v6}, LX/CNa;->a()LX/CNb;

    move-result-object v3

    invoke-static {v3}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    invoke-static {v2, v5, v3}, LX/CNS;->a(LX/CNS;LX/CNb;LX/0Px;)V

    goto :goto_0
.end method
