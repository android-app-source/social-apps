.class public final LX/CU2;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:I

.field public final b:I

.field public final c:Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;

.field public final d:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/CU2;I)V
    .locals 1

    .prologue
    .line 1896207
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1896208
    iget v0, p1, LX/CU2;->a:I

    add-int/2addr v0, p2

    iput v0, p0, LX/CU2;->a:I

    .line 1896209
    iget v0, p1, LX/CU2;->b:I

    iput v0, p0, LX/CU2;->b:I

    .line 1896210
    iget-object v0, p1, LX/CU2;->c:Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;

    iput-object v0, p0, LX/CU2;->c:Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;

    .line 1896211
    iget-object v0, p1, LX/CU2;->d:Ljava/lang/String;

    iput-object v0, p0, LX/CU2;->d:Ljava/lang/String;

    .line 1896212
    return-void
.end method

.method public constructor <init>(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel$EntitiesModel;)V
    .locals 1

    .prologue
    .line 1896213
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1896214
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1896215
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel$EntitiesModel;->a()Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1896216
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel$EntitiesModel;->c()I

    move-result v0

    iput v0, p0, LX/CU2;->a:I

    .line 1896217
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel$EntitiesModel;->b()I

    move-result v0

    iput v0, p0, LX/CU2;->b:I

    .line 1896218
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel$EntitiesModel;->a()Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;

    move-result-object v0

    iput-object v0, p0, LX/CU2;->c:Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;

    .line 1896219
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel$EntitiesModel;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/CU2;->d:Ljava/lang/String;

    .line 1896220
    return-void
.end method
