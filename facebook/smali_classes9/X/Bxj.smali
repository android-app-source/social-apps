.class public LX/Bxj;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pp;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/AlbumAttachmentPageComponentSpec;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Bxj",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/attachments/AlbumAttachmentPageComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1835903
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1835904
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/Bxj;->b:LX/0Zi;

    .line 1835905
    iput-object p1, p0, LX/Bxj;->a:LX/0Ot;

    .line 1835906
    return-void
.end method

.method public static a(LX/0QB;)LX/Bxj;
    .locals 4

    .prologue
    .line 1835923
    const-class v1, LX/Bxj;

    monitor-enter v1

    .line 1835924
    :try_start_0
    sget-object v0, LX/Bxj;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1835925
    sput-object v2, LX/Bxj;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1835926
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1835927
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1835928
    new-instance v3, LX/Bxj;

    const/16 p0, 0x1e0f

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/Bxj;-><init>(LX/0Ot;)V

    .line 1835929
    move-object v0, v3

    .line 1835930
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1835931
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Bxj;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1835932
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1835933
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(Landroid/view/View;LX/1X1;)V
    .locals 11

    .prologue
    .line 1835934
    check-cast p2, LX/Bxh;

    .line 1835935
    iget-object v0, p0, LX/Bxj;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/attachments/AlbumAttachmentPageComponentSpec;

    iget-object v1, p2, LX/Bxh;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v6, 0x0

    .line 1835936
    invoke-static {v1}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v3

    .line 1835937
    iget-object v2, v3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 1835938
    check-cast v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v2}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v2

    .line 1835939
    invoke-virtual {v3, v2}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v3

    .line 1835940
    iget-object v4, v0, Lcom/facebook/feedplugins/attachments/AlbumAttachmentPageComponentSpec;->c:LX/1qa;

    .line 1835941
    iget-object v2, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 1835942
    check-cast v2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    sget-object v5, LX/26P;->Album:LX/26P;

    invoke-virtual {v4, v2, v5}, LX/1qa;->a(Lcom/facebook/graphql/model/GraphQLMedia;LX/26P;)LX/1bf;

    move-result-object v7

    .line 1835943
    iget-object v2, v0, Lcom/facebook/feedplugins/attachments/AlbumAttachmentPageComponentSpec;->d:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/BaD;

    const/4 v8, 0x0

    const/4 v9, -0x1

    move-object v4, v1

    move-object v5, p1

    move-object v10, v6

    invoke-virtual/range {v2 .. v10}, LX/BaD;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;LX/1aZ;LX/1bf;ZILandroid/content/DialogInterface$OnDismissListener;)V

    .line 1835944
    return-void
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 7

    .prologue
    .line 1835913
    check-cast p2, LX/Bxh;

    .line 1835914
    iget-object v0, p0, LX/Bxj;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/attachments/AlbumAttachmentPageComponentSpec;

    iget-object v1, p2, LX/Bxh;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/Bxh;->b:LX/1Pp;

    const/4 p2, 0x6

    const/4 p0, 0x4

    .line 1835915
    iget-object v3, v0, Lcom/facebook/feedplugins/attachments/AlbumAttachmentPageComponentSpec;->c:LX/1qa;

    sget-object v4, LX/26P;->Album:LX/26P;

    invoke-virtual {v3, v4}, LX/1qa;->a(LX/26P;)I

    move-result v4

    .line 1835916
    iget-object v5, v0, Lcom/facebook/feedplugins/attachments/AlbumAttachmentPageComponentSpec;->c:LX/1qa;

    .line 1835917
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 1835918
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v3

    sget-object v6, LX/26P;->Album:LX/26P;

    invoke-virtual {v5, v3, v6}, LX/1qa;->b(Lcom/facebook/graphql/model/GraphQLMedia;LX/26P;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    .line 1835919
    iget-object v5, v0, Lcom/facebook/feedplugins/attachments/AlbumAttachmentPageComponentSpec;->b:LX/1nu;

    invoke-virtual {v5, p1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object v5

    invoke-virtual {v5, v2}, LX/1nw;->a(LX/1Pp;)LX/1nw;

    move-result-object v5

    sget-object v6, Lcom/facebook/feedplugins/attachments/AlbumAttachmentPageComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v5, v6}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object v5

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v5, v3}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object v3

    const v5, 0x7f0a045d

    invoke-virtual {v3, v5}, LX/1nw;->h(I)LX/1nw;

    move-result-object v3

    const v5, 0x7f021af6

    invoke-virtual {v3, v5}, LX/1nw;->i(I)LX/1nw;

    move-result-object v3

    const/16 v5, 0x3e8

    invoke-virtual {v3, v5}, LX/1nw;->j(I)LX/1nw;

    move-result-object v3

    const v5, 0x7f020aa6

    invoke-virtual {v3, v5}, LX/1nw;->k(I)LX/1nw;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const v5, 0x7f080ff9

    invoke-interface {v3, v5}, LX/1Di;->A(I)LX/1Di;

    move-result-object v3

    const v5, 0x7f020aa7

    invoke-interface {v3, v5}, LX/1Di;->x(I)LX/1Di;

    move-result-object v3

    invoke-interface {v3, p2, p0}, LX/1Di;->h(II)LX/1Di;

    move-result-object v3

    const/4 v5, 0x1

    invoke-interface {v3, v5, p0}, LX/1Di;->h(II)LX/1Di;

    move-result-object v3

    const/4 v5, 0x3

    invoke-interface {v3, v5, p2}, LX/1Di;->h(II)LX/1Di;

    move-result-object v3

    invoke-interface {v3, v4}, LX/1Di;->g(I)LX/1Di;

    move-result-object v3

    invoke-interface {v3, v4}, LX/1Di;->o(I)LX/1Di;

    move-result-object v3

    .line 1835920
    const v4, -0x14471f5a

    const/4 v5, 0x0

    invoke-static {p1, v4, v5}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v4

    move-object v4, v4

    .line 1835921
    invoke-interface {v3, v4}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 1835922
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1835907
    invoke-static {}, LX/1dS;->b()V

    .line 1835908
    iget v0, p1, LX/1dQ;->b:I

    .line 1835909
    packed-switch v0, :pswitch_data_0

    .line 1835910
    :goto_0
    return-object v2

    .line 1835911
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 1835912
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    invoke-direct {p0, v0, v1}, LX/Bxj;->a(Landroid/view/View;LX/1X1;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x14471f5a
        :pswitch_0
    .end packed-switch
.end method
