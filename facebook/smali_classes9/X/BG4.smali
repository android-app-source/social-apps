.class public final LX/BG4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5f5;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/5f5",
        "<",
        "LX/5fl;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/BG5;


# direct methods
.method public constructor <init>(LX/BG5;)V
    .locals 0

    .prologue
    .line 1766523
    iput-object p1, p0, LX/BG4;->a:LX/BG5;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 1766524
    sget-object v0, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->b:Ljava/lang/String;

    const-string v1, "Failed to start recording a video"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1766525
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 12

    .prologue
    .line 1766526
    iget-object v0, p0, LX/BG4;->a:LX/BG5;

    iget-object v0, v0, LX/BG5;->a:Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;

    iget-object v0, v0, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->d:LX/Jvd;

    const/16 v2, 0x8

    .line 1766527
    iget-object v1, v0, LX/Jvd;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    iget-object v1, v1, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->E:LX/BGC;

    const/4 v7, 0x0

    .line 1766528
    iget-object v3, v1, LX/BGC;->d:Landroid/widget/ProgressBar;

    if-eqz v3, :cond_0

    .line 1766529
    iget-object v3, v1, LX/BGC;->d:Landroid/widget/ProgressBar;

    invoke-virtual {v3, v7}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 1766530
    iget-object v3, v1, LX/BGC;->d:Landroid/widget/ProgressBar;

    invoke-virtual {v3, v7}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1766531
    :cond_0
    iget-object v3, v1, LX/BGC;->b:Lcom/facebook/resources/ui/FbTextView;

    if-nez v3, :cond_2

    .line 1766532
    iget-object v3, v1, LX/BGC;->b:Lcom/facebook/resources/ui/FbTextView;

    if-eqz v3, :cond_1

    .line 1766533
    iget-object v3, v1, LX/BGC;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v3}, Lcom/facebook/resources/ui/FbTextView;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    .line 1766534
    iget-object v4, v1, LX/BGC;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->removeViewAt(I)V

    .line 1766535
    :cond_1
    iget-object v3, v1, LX/BGC;->e:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0303b2

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/facebook/resources/ui/FbTextView;

    iput-object v3, v1, LX/BGC;->b:Lcom/facebook/resources/ui/FbTextView;

    .line 1766536
    iget-object v3, v1, LX/BGC;->e:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    iget-object v4, v1, LX/BGC;->b:Lcom/facebook/resources/ui/FbTextView;

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 1766537
    :cond_2
    iget-object v3, v1, LX/BGC;->b:Lcom/facebook/resources/ui/FbTextView;

    const-wide/16 v5, 0x0

    invoke-static {v5, v6}, Landroid/text/format/DateUtils;->formatElapsedTime(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1766538
    iget-boolean v3, v1, LX/BGC;->i:Z

    if-eqz v3, :cond_4

    .line 1766539
    iget-object v8, v1, LX/BGC;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v8}, Lcom/facebook/resources/ui/FbTextView;->getVisibility()I

    move-result v8

    if-eqz v8, :cond_3

    .line 1766540
    iget-object v8, v1, LX/BGC;->b:Lcom/facebook/resources/ui/FbTextView;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/facebook/resources/ui/FbTextView;->setAlpha(F)V

    .line 1766541
    iget-object v8, v1, LX/BGC;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v8}, Lcom/facebook/resources/ui/FbTextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v8

    const/high16 v9, 0x3f800000    # 1.0f

    invoke-virtual {v8, v9}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v8

    const-wide/16 v10, 0xc8

    invoke-virtual {v8, v10, v11}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 1766542
    iget-object v8, v1, LX/BGC;->b:Lcom/facebook/resources/ui/FbTextView;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1766543
    :cond_3
    const/4 v5, -0x2

    const/high16 v9, 0x3f800000    # 1.0f

    .line 1766544
    iget-object v3, v1, LX/BGC;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v3}, Lcom/facebook/resources/ui/FbTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1766545
    new-instance v4, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v4, v5, v5}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 1766546
    iget-object v5, v1, LX/BGC;->b:Lcom/facebook/resources/ui/FbTextView;

    const/4 v6, -0x2

    const/4 v8, -0x1

    invoke-virtual {v5, v6, v8}, Lcom/facebook/resources/ui/FbTextView;->measure(II)V

    .line 1766547
    iget v5, v1, LX/BGC;->k:I

    div-int/lit8 v5, v5, 0x2

    iget-object v6, v1, LX/BGC;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v6}, Lcom/facebook/resources/ui/FbTextView;->getMeasuredWidth()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    sub-int/2addr v5, v6

    move v5, v5

    .line 1766548
    invoke-static {v1}, LX/BGC;->e(LX/BGC;)I

    move-result v6

    iget v8, v3, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    iget v3, v3, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    invoke-virtual {v4, v5, v6, v8, v3}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    .line 1766549
    iget-object v3, v1, LX/BGC;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v3, v4}, Lcom/facebook/resources/ui/FbTextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1766550
    iget-object v3, v1, LX/BGC;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v3, v9}, Lcom/facebook/resources/ui/FbTextView;->setScaleX(F)V

    .line 1766551
    iget-object v3, v1, LX/BGC;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v3, v9}, Lcom/facebook/resources/ui/FbTextView;->setScaleY(F)V

    .line 1766552
    :cond_4
    iget-object v3, v1, LX/BGC;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v3, v7}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1766553
    iget-object v3, v1, LX/BGC;->e:Landroid/view/ViewGroup;

    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1766554
    iget-object v3, v1, LX/BGC;->c:LX/0So;

    invoke-interface {v3}, LX/0So;->now()J

    move-result-wide v3

    iput-wide v3, v1, LX/BGC;->f:J

    .line 1766555
    const/4 v3, 0x1

    iput-boolean v3, v1, LX/BGC;->g:Z

    .line 1766556
    iget-object v3, v1, LX/BGC;->h:LX/0wY;

    iget-object v4, v1, LX/BGC;->n:LX/0wa;

    invoke-interface {v3, v4}, LX/0wY;->a(LX/0wa;)V

    .line 1766557
    iget-object v1, v0, LX/Jvd;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    iget-object v1, v1, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->N:LX/4gI;

    invoke-virtual {v1}, LX/4gI;->supportsPhotos()Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, v0, LX/Jvd;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    iget-object v1, v1, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->N:LX/4gI;

    invoke-virtual {v1}, LX/4gI;->supportsVideos()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1766558
    iget-object v1, v0, LX/Jvd;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    iget-object v1, v1, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->l:LX/0am;

    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1766559
    iget-object v1, v0, LX/Jvd;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    iget-object v1, v1, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->l:LX/0am;

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/creativecam/ui/CreativeCamPhotoVideoSwitch;

    invoke-virtual {v1, v2}, Lcom/facebook/photos/creativecam/ui/CreativeCamPhotoVideoSwitch;->setVisibility(I)V

    .line 1766560
    :cond_5
    iget-object v1, v0, LX/Jvd;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    iget-object v1, v1, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    if-eqz v1, :cond_6

    .line 1766561
    iget-object v1, v0, LX/Jvd;->a:Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    iget-object v1, v1, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1766562
    :cond_6
    return-void
.end method
