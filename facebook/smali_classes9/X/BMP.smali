.class public LX/BMP;
.super LX/B5s;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<CONTEXT::",
        "LX/B5o;",
        ":",
        "Lcom/facebook/ipc/productionprompts/actioncontext/PromptActionContextInterfaces$HasPromptSessionId;",
        ":",
        "Lcom/facebook/ipc/productionprompts/actioncontext/PromptActionContextInterfaces$SetsComposerSessionId;",
        ">",
        "LX/B5s",
        "<TCONTEXT;>;"
    }
.end annotation


# instance fields
.field private final b:LX/5oW;


# direct methods
.method public constructor <init>(Ljava/util/Set;LX/5oW;)V
    .locals 0
    .param p1    # Ljava/util/Set;
        .annotation runtime Lcom/facebook/ipc/productionprompts/annotations/TopLevel;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "LX/Alg;",
            ">;",
            "LX/5oW;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1777160
    invoke-direct {p0, p1}, LX/B5s;-><init>(Ljava/util/Set;)V

    .line 1777161
    iput-object p2, p0, LX/BMP;->b:LX/5oW;

    .line 1777162
    return-void
.end method

.method private a(LX/1RN;LX/B5o;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1RN;",
            "TCONTEXT;)V"
        }
    .end annotation

    .prologue
    .line 1777150
    invoke-static {p1}, LX/1RN;->a(LX/1RN;)LX/1kK;

    move-result-object v0

    .line 1777151
    instance-of v0, v0, LX/1kJ;

    if-nez v0, :cond_0

    .line 1777152
    iget-object v1, p0, LX/BMP;->b:LX/5oW;

    move-object v0, p2

    check-cast v0, LX/B5p;

    .line 1777153
    iget-object p0, v0, LX/B5p;->a:Ljava/lang/String;

    move-object v0, p0

    .line 1777154
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1777155
    sget-object v8, LX/5oV;->OPEN_SELECTING_CONTENT:LX/5oV;

    iget-object v2, p1, LX/1RN;->a:LX/1kK;

    invoke-interface {v2}, LX/1kK;->b()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p1, LX/1RN;->b:LX/1lP;

    iget-object v3, v3, LX/1lP;->c:Ljava/lang/String;

    iget-object v4, p1, LX/1RN;->b:LX/1lP;

    iget-object v6, v4, LX/1lP;->b:Ljava/lang/String;

    iget-object v4, p1, LX/1RN;->c:LX/32e;

    iget-object v4, v4, LX/32e;->a:LX/24P;

    invoke-virtual {v4}, LX/24P;->getName()Ljava/lang/String;

    move-result-object v7

    move-object v4, v0

    invoke-static/range {v2 .. v7}, Lcom/facebook/productionprompts/logging/PromptAnalytics;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/productionprompts/logging/PromptAnalytics;

    move-result-object v2

    invoke-static {v1, v8, v2}, LX/5oW;->a(LX/5oW;LX/5oV;Lcom/facebook/productionprompts/logging/PromptAnalytics;)V

    .line 1777156
    move-object v0, v5

    .line 1777157
    check-cast p2, LX/B5p;

    .line 1777158
    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v1

    iput-object v1, p2, LX/B5p;->j:LX/0am;

    .line 1777159
    :cond_0
    return-void
.end method

.method public static b(LX/0QB;)LX/BMP;
    .locals 4

    .prologue
    .line 1777142
    new-instance v1, LX/BMP;

    .line 1777143
    new-instance v0, LX/0U8;

    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v2

    new-instance v3, LX/B5u;

    invoke-direct {v3, p0}, LX/B5u;-><init>(LX/0QB;)V

    invoke-direct {v0, v2, v3}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v2, v0

    .line 1777144
    invoke-static {p0}, LX/5oW;->a(LX/0QB;)LX/5oW;

    move-result-object v0

    check-cast v0, LX/5oW;

    invoke-direct {v1, v2, v0}, LX/BMP;-><init>(Ljava/util/Set;LX/5oW;)V

    .line 1777145
    return-object v1
.end method


# virtual methods
.method public final a(Landroid/view/View;LX/1RN;LX/B5o;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "LX/1RN;",
            "TCONTEXT;)V"
        }
    .end annotation

    .prologue
    .line 1777147
    invoke-direct {p0, p2, p3}, LX/BMP;->a(LX/1RN;LX/B5o;)V

    .line 1777148
    invoke-super {p0, p1, p2, p3}, LX/B5s;->a(Landroid/view/View;LX/1RN;LX/B5o;)V

    .line 1777149
    return-void
.end method

.method public final b(LX/1RN;)Z
    .locals 1

    .prologue
    .line 1777146
    const/4 v0, 0x1

    return v0
.end method
