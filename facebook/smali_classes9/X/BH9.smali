.class public final LX/BH9;
.super LX/0xh;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/photos/simplepicker/SimplePickerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/simplepicker/SimplePickerFragment;)V
    .locals 0

    .prologue
    .line 1767652
    iput-object p1, p0, LX/BH9;->a:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    invoke-direct {p0}, LX/0xh;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0wd;)V
    .locals 5

    .prologue
    .line 1767653
    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v0

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    .line 1767654
    iget-object v0, p0, LX/BH9;->a:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    iget-object v0, v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->i:Lcom/facebook/photos/simplepicker/view/PickerLongPressProgressBar;

    .line 1767655
    iget-object v1, v0, Lcom/facebook/photos/simplepicker/view/PickerLongPressProgressBar;->e:Lcom/facebook/ipc/media/MediaItem;

    move-object v1, v1

    .line 1767656
    sget-object v0, LX/BGv;->a:[I

    invoke-virtual {v1}, Lcom/facebook/ipc/media/MediaItem;->m()LX/4gF;

    move-result-object v2

    invoke-virtual {v2}, LX/4gF;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 1767657
    :cond_0
    :goto_0
    return-void

    :pswitch_0
    move-object v0, v1

    .line 1767658
    check-cast v0, Lcom/facebook/photos/base/media/PhotoItem;

    .line 1767659
    iget-boolean v2, v0, Lcom/facebook/photos/base/media/PhotoItem;->d:Z

    move v0, v2

    .line 1767660
    if-nez v0, :cond_0

    .line 1767661
    iget-object v0, p0, LX/BH9;->a:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    invoke-virtual {v1}, Lcom/facebook/ipc/media/MediaItem;->d()Lcom/facebook/ipc/media/MediaIdKey;

    move-result-object v1

    .line 1767662
    iget-boolean v2, v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->h:Z

    if-nez v2, :cond_1

    .line 1767663
    iget-object v2, v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->m:LX/11o;

    const-string v3, "GalleryLaunched"

    const v4, -0x30d5df3d

    invoke-static {v2, v3, v4}, LX/096;->e(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 1767664
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->c()V

    .line 1767665
    iget-object v2, v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->M:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1Ck;

    sget-object v3, LX/BHC;->FETCH_TAGGING_DATA:LX/BHC;

    invoke-static {v0}, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->A(Lcom/facebook/photos/simplepicker/SimplePickerFragment;)Ljava/util/concurrent/Callable;

    move-result-object v4

    new-instance p0, LX/BGn;

    invoke-direct {p0, v0, v1}, LX/BGn;-><init>(Lcom/facebook/photos/simplepicker/SimplePickerFragment;Lcom/facebook/ipc/media/MediaIdKey;)V

    invoke-virtual {v2, v3, v4, p0}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 1767666
    goto :goto_0

    .line 1767667
    :pswitch_1
    new-instance v0, Ljava/io/File;

    invoke-virtual {v1}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1767668
    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, LX/BH9;->a:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    invoke-virtual {v3}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v3

    const-class v4, Lcom/facebook/photos/taggablegallery/ProductionVideoGalleryActivity;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v3, "extra_session_id"

    iget-object v4, p0, LX/BH9;->a:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    iget-object v4, v4, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->W:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const-string v3, "extra_source"

    sget-object v4, LX/BIf;->SIMPLEPICKER:LX/BIf;

    invoke-virtual {v4}, LX/BIf;->ordinal()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v2

    const-string v3, "extra_video_item"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "extra_video_uri"

    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v1

    .line 1767669
    iget-object v0, p0, LX/BH9;->a:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    iget-object v0, v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->H:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    const/16 v2, 0x3e8

    iget-object v3, p0, LX/BH9;->a:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
