.class public LX/BOl;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1780235
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1780236
    return-void
.end method

.method public static a(LX/0eX;Lcom/facebook/ipc/media/MediaItem;)I
    .locals 8

    .prologue
    .line 1780123
    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v0

    .line 1780124
    iget-object v1, v0, Lcom/facebook/ipc/media/data/MediaData;->mId:Ljava/lang/String;

    move-object v0, v1

    .line 1780125
    invoke-virtual {p0, v0}, LX/0eX;->a(Ljava/lang/String;)I

    move-result v2

    .line 1780126
    sget-object v0, LX/BOk;->a:[I

    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v1

    .line 1780127
    iget-object v3, v1, Lcom/facebook/ipc/media/data/MediaData;->mType:LX/4gQ;

    move-object v1, v3

    .line 1780128
    invoke-virtual {v1}, LX/4gQ;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1780129
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown media: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1780130
    :pswitch_0
    invoke-static {p1, p0}, LX/BOl;->a(Lcom/facebook/ipc/media/MediaItem;LX/0eX;)I

    move-result v1

    .line 1780131
    const/4 v0, 0x1

    .line 1780132
    :goto_0
    invoke-static {p0}, LX/BOy;->a(LX/0eX;)V

    .line 1780133
    const/4 v3, 0x0

    .line 1780134
    invoke-virtual {p0, v3, v2, v3}, LX/0eX;->c(III)V

    .line 1780135
    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->j()J

    move-result-wide v2

    long-to-double v2, v2

    const-wide v4, 0x408f400000000000L    # 1000.0

    div-double/2addr v2, v4

    .line 1780136
    invoke-static {p0, v2, v3}, LX/BOy;->a(LX/0eX;D)V

    .line 1780137
    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v2

    .line 1780138
    iget-wide v6, v2, Lcom/facebook/ipc/media/data/MediaData;->mLatitude:D

    move-wide v2, v6

    .line 1780139
    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v4

    .line 1780140
    iget-wide v6, v4, Lcom/facebook/ipc/media/data/MediaData;->mLongitude:D

    move-wide v4, v6

    .line 1780141
    const/16 v6, 0x8

    const/16 v7, 0x10

    invoke-virtual {p0, v6, v7}, LX/0eX;->a(II)V

    .line 1780142
    invoke-virtual {p0, v4, v5}, LX/0eX;->a(D)V

    .line 1780143
    invoke-virtual {p0, v2, v3}, LX/0eX;->a(D)V

    .line 1780144
    invoke-virtual {p0}, LX/0eX;->a()I

    move-result v6

    move v2, v6

    .line 1780145
    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-virtual {p0, v3, v2, v4}, LX/0eX;->d(III)V

    .line 1780146
    invoke-static {p0, v0}, LX/BOy;->a(LX/0eX;B)V

    .line 1780147
    invoke-static {p0, v1}, LX/BOy;->c(LX/0eX;I)V

    .line 1780148
    invoke-static {p0}, LX/BOy;->b(LX/0eX;)I

    move-result v0

    return v0

    .line 1780149
    :pswitch_1
    invoke-static {p1, p0}, LX/BOl;->b(Lcom/facebook/ipc/media/MediaItem;LX/0eX;)I

    move-result v1

    .line 1780150
    const/4 v0, 0x2

    .line 1780151
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static a(Lcom/facebook/ipc/media/MediaItem;LX/0eX;)I
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 1780152
    sget-object v0, Lcom/facebook/ipc/media/data/MimeType;->a:Lcom/facebook/ipc/media/data/MimeType;

    invoke-virtual {p0}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v1

    .line 1780153
    iget-object v2, v1, Lcom/facebook/ipc/media/data/MediaData;->mMimeType:Lcom/facebook/ipc/media/data/MimeType;

    move-object v1, v2

    .line 1780154
    invoke-virtual {v0, v1}, Lcom/facebook/ipc/media/data/MimeType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v4, v6

    .line 1780155
    :goto_0
    const-string v0, ""

    invoke-virtual {p1, v0}, LX/0eX;->a(Ljava/lang/String;)I

    move-result v5

    .line 1780156
    invoke-virtual {p0}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v0

    .line 1780157
    iget-object v1, v0, Lcom/facebook/ipc/media/data/MediaData;->mUri:Landroid/net/Uri;

    move-object v0, v1

    .line 1780158
    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/0eX;->a(Ljava/lang/String;)I

    move-result v7

    .line 1780159
    invoke-virtual {p0}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v0

    .line 1780160
    iget v1, v0, Lcom/facebook/ipc/media/data/MediaData;->mWidth:I

    move v1, v1

    .line 1780161
    invoke-virtual {p0}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v0

    .line 1780162
    iget v2, v0, Lcom/facebook/ipc/media/data/MediaData;->mHeight:I

    move v2, v2

    .line 1780163
    invoke-virtual {p0}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v0

    .line 1780164
    iget v3, v0, Lcom/facebook/ipc/media/data/MediaData;->mOrientation:I

    move v3, v3

    .line 1780165
    move-object v0, p1

    .line 1780166
    const/4 p0, 0x7

    invoke-virtual {v0, p0}, LX/0eX;->b(I)V

    .line 1780167
    const/4 p0, 0x6

    const/4 p1, 0x0

    invoke-virtual {v0, p0, v7, p1}, LX/0eX;->c(III)V

    .line 1780168
    const/4 p0, 0x4

    const/4 p1, 0x0

    invoke-virtual {v0, p0, v5, p1}, LX/0eX;->c(III)V

    .line 1780169
    const/4 p0, 0x2

    const/4 p1, 0x1

    invoke-virtual {v0, p0, v3, p1}, LX/0eX;->b(III)V

    .line 1780170
    const/4 p0, 0x1

    const/4 p1, 0x0

    invoke-virtual {v0, p0, v2, p1}, LX/0eX;->b(III)V

    .line 1780171
    const/4 p0, 0x0

    .line 1780172
    invoke-virtual {v0, p0, v1, p0}, LX/0eX;->b(III)V

    .line 1780173
    const/4 p0, 0x5

    const/4 p1, 0x0

    invoke-virtual {v0, p0, v6, p1}, LX/0eX;->a(IZZ)V

    .line 1780174
    const/4 p0, 0x3

    const/4 p1, 0x2

    invoke-virtual {v0, p0, v4, p1}, LX/0eX;->a(IBI)V

    .line 1780175
    invoke-virtual {v0}, LX/0eX;->c()I

    move-result p0

    .line 1780176
    move p0, p0

    .line 1780177
    move v0, p0

    .line 1780178
    return v0

    .line 1780179
    :cond_0
    sget-object v0, Lcom/facebook/ipc/media/data/MimeType;->c:Lcom/facebook/ipc/media/data/MimeType;

    invoke-virtual {p0}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v1

    .line 1780180
    iget-object v2, v1, Lcom/facebook/ipc/media/data/MediaData;->mMimeType:Lcom/facebook/ipc/media/data/MimeType;

    move-object v1, v2

    .line 1780181
    invoke-virtual {v0, v1}, Lcom/facebook/ipc/media/data/MimeType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1780182
    const/4 v4, 0x1

    goto :goto_0

    .line 1780183
    :cond_1
    const/4 v4, 0x2

    goto :goto_0
.end method

.method public static a(LX/8I2;JI)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/photos/local/LocalMediaCursor;",
            "JI)",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1780184
    sget-object v1, LX/4gI;->ALL:LX/4gI;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "datetaken > "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "datetaken"

    invoke-virtual {p0, v1, v0, v2, v3}, LX/8I2;->a(LX/4gI;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 1780185
    if-nez v1, :cond_0

    .line 1780186
    :goto_0
    return-object v0

    .line 1780187
    :cond_0
    :try_start_0
    invoke-virtual {p0, v1, p3}, LX/8I2;->a(Landroid/database/Cursor;I)Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1780188
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public static a(Ljava/util/List;)[B
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;)[B"
        }
    .end annotation

    .prologue
    const-wide/16 v2, 0x0

    .line 1780189
    new-instance v1, LX/0eX;

    const/16 v0, 0x80

    invoke-direct {v1, v0}, LX/0eX;-><init>(I)V

    .line 1780190
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v5

    .line 1780191
    new-array v6, v5, [I

    .line 1780192
    const/4 v0, 0x0

    move v4, v0

    :goto_0
    if-ge v4, v5, :cond_0

    .line 1780193
    invoke-interface {p0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    invoke-static {v1, v0}, LX/BOl;->a(LX/0eX;Lcom/facebook/ipc/media/MediaItem;)I

    move-result v0

    aput v0, v6, v4

    .line 1780194
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    .line 1780195
    :cond_0
    const/4 v4, 0x4

    .line 1780196
    array-length v0, v6

    invoke-virtual {v1, v4, v0, v4}, LX/0eX;->a(III)V

    array-length v0, v6

    add-int/lit8 v0, v0, -0x1

    :goto_1
    if-ltz v0, :cond_1

    aget v4, v6, v0

    invoke-virtual {v1, v4}, LX/0eX;->a(I)V

    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    :cond_1
    invoke-virtual {v1}, LX/0eX;->b()I

    move-result v0

    move v6, v0

    .line 1780197
    move-wide v4, v2

    .line 1780198
    const/4 v7, 0x3

    invoke-virtual {v1, v7}, LX/0eX;->b(I)V

    .line 1780199
    const/4 v9, 0x1

    const-wide/16 v12, 0x0

    move-object v8, v1

    move-wide v10, v4

    invoke-virtual/range {v8 .. v13}, LX/0eX;->a(IJJ)V

    .line 1780200
    const/4 v9, 0x0

    const-wide/16 v12, 0x0

    move-object v8, v1

    move-wide v10, v2

    invoke-virtual/range {v8 .. v13}, LX/0eX;->a(IJJ)V

    .line 1780201
    const/4 v7, 0x2

    const/4 v8, 0x0

    invoke-virtual {v1, v7, v6, v8}, LX/0eX;->c(III)V

    .line 1780202
    invoke-virtual {v1}, LX/0eX;->c()I

    move-result v7

    .line 1780203
    move v7, v7

    .line 1780204
    move v0, v7

    .line 1780205
    invoke-static {v1}, LX/BOy;->a(LX/0eX;)V

    .line 1780206
    const/4 v2, 0x4

    invoke-static {v1, v2}, LX/BOy;->a(LX/0eX;B)V

    .line 1780207
    invoke-static {v1, v0}, LX/BOy;->c(LX/0eX;I)V

    .line 1780208
    invoke-static {v1}, LX/BOy;->b(LX/0eX;)I

    move-result v0

    .line 1780209
    invoke-virtual {v1, v0}, LX/0eX;->c(I)V

    .line 1780210
    invoke-virtual {v1}, LX/0eX;->e()[B

    move-result-object v0

    return-object v0
.end method

.method private static b(Lcom/facebook/ipc/media/MediaItem;LX/0eX;)I
    .locals 12

    .prologue
    .line 1780211
    move-object v0, p0

    check-cast v0, Lcom/facebook/photos/base/media/VideoItem;

    .line 1780212
    iget-wide v7, v0, Lcom/facebook/photos/base/media/VideoItem;->d:J

    move-wide v0, v7

    .line 1780213
    long-to-float v0, v0

    const/high16 v1, 0x447a0000    # 1000.0f

    div-float v3, v0, v1

    .line 1780214
    const-string v0, ""

    invoke-virtual {p1, v0}, LX/0eX;->a(Ljava/lang/String;)I

    move-result v4

    .line 1780215
    invoke-virtual {p0}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v0

    .line 1780216
    iget-object v1, v0, Lcom/facebook/ipc/media/data/MediaData;->mUri:Landroid/net/Uri;

    move-object v0, v1

    .line 1780217
    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/0eX;->a(Ljava/lang/String;)I

    move-result v6

    .line 1780218
    invoke-virtual {p0}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v0

    .line 1780219
    iget v1, v0, Lcom/facebook/ipc/media/data/MediaData;->mWidth:I

    move v1, v1

    .line 1780220
    invoke-virtual {p0}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v0

    .line 1780221
    iget v2, v0, Lcom/facebook/ipc/media/data/MediaData;->mHeight:I

    move v2, v2

    .line 1780222
    const/4 v5, 0x0

    move-object v0, p1

    .line 1780223
    const/4 v7, 0x6

    invoke-virtual {v0, v7}, LX/0eX;->b(I)V

    .line 1780224
    const/4 v7, 0x5

    const/4 v8, 0x0

    invoke-virtual {v0, v7, v6, v8}, LX/0eX;->c(III)V

    .line 1780225
    const/4 v7, 0x3

    const/4 v8, 0x0

    invoke-virtual {v0, v7, v4, v8}, LX/0eX;->c(III)V

    .line 1780226
    const/4 v8, 0x2

    const-wide/16 v10, 0x0

    invoke-virtual {v0, v8, v3, v10, v11}, LX/0eX;->a(IFD)V

    .line 1780227
    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-virtual {v0, v7, v2, v8}, LX/0eX;->b(III)V

    .line 1780228
    const/4 v7, 0x0

    .line 1780229
    invoke-virtual {v0, v7, v1, v7}, LX/0eX;->b(III)V

    .line 1780230
    const/4 v7, 0x4

    const/4 v8, 0x0

    invoke-virtual {v0, v7, v5, v8}, LX/0eX;->a(IZZ)V

    .line 1780231
    invoke-virtual {v0}, LX/0eX;->c()I

    move-result v7

    .line 1780232
    move v7, v7

    .line 1780233
    move v0, v7

    .line 1780234
    return v0
.end method
