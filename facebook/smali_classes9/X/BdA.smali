.class public LX/BdA;
.super LX/BcS;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<TModel:",
        "Ljava/lang/Object;",
        ">",
        "LX/BcS;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/BdD;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/BdA",
            "<TTModel;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/BdD;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1803453
    invoke-direct {p0}, LX/BcS;-><init>()V

    .line 1803454
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/BdA;->b:LX/0Zi;

    .line 1803455
    iput-object p1, p0, LX/BdA;->a:LX/0Ot;

    .line 1803456
    return-void
.end method

.method public static a(LX/0QB;)LX/BdA;
    .locals 4

    .prologue
    .line 1803442
    const-class v1, LX/BdA;

    monitor-enter v1

    .line 1803443
    :try_start_0
    sget-object v0, LX/BdA;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1803444
    sput-object v2, LX/BdA;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1803445
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1803446
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1803447
    new-instance v3, LX/BdA;

    const/16 p0, 0x1946

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/BdA;-><init>(LX/0Ot;)V

    .line 1803448
    move-object v0, v3

    .line 1803449
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1803450
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/BdA;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1803451
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1803452
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/BcP;LX/BdC;Ljava/lang/Object;Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1803436
    invoke-virtual {p0}, LX/BcP;->i()LX/BcO;

    move-result-object v0

    .line 1803437
    if-nez v0, :cond_0

    .line 1803438
    :goto_0
    return-void

    .line 1803439
    :cond_0
    check-cast v0, LX/Bd7;

    .line 1803440
    new-instance v1, LX/Bd9;

    iget-object v2, v0, LX/Bd7;->h:LX/BdA;

    invoke-direct {v1, v2, p1, p2, p3}, LX/Bd9;-><init>(LX/BdA;LX/BdC;Ljava/lang/Object;Ljava/lang/Throwable;)V

    move-object v0, v1

    .line 1803441
    invoke-virtual {p0, v0}, LX/BcP;->a(LX/BcR;)V

    goto :goto_0
.end method

.method public static a(LX/BcP;Ljava/lang/Boolean;)V
    .locals 3

    .prologue
    .line 1803430
    invoke-virtual {p0}, LX/BcP;->i()LX/BcO;

    move-result-object v0

    .line 1803431
    if-nez v0, :cond_0

    .line 1803432
    :goto_0
    return-void

    .line 1803433
    :cond_0
    check-cast v0, LX/Bd7;

    .line 1803434
    new-instance v1, LX/Bd8;

    iget-object v2, v0, LX/Bd7;->h:LX/BdA;

    invoke-direct {v1, v2, p1}, LX/Bd8;-><init>(LX/BdA;Ljava/lang/Boolean;)V

    move-object v0, v1

    .line 1803435
    invoke-virtual {p0, v0}, LX/BcP;->a(LX/BcR;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/BcO;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1803342
    check-cast p1, LX/Bd7;

    iget-object v0, p1, LX/Bd7;->g:LX/BdF;

    return-object v0
.end method

.method public final a(LX/BcQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1803428
    invoke-static {}, LX/1dS;->b()V

    .line 1803429
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/BcO;LX/BcO;)V
    .locals 1

    .prologue
    .line 1803421
    check-cast p1, LX/Bd7;

    .line 1803422
    check-cast p2, LX/Bd7;

    .line 1803423
    iget-boolean v0, p1, LX/Bd7;->b:Z

    iput-boolean v0, p2, LX/Bd7;->b:Z

    .line 1803424
    iget-object v0, p1, LX/Bd7;->c:LX/BdC;

    iput-object v0, p2, LX/Bd7;->c:LX/BdC;

    .line 1803425
    iget-object v0, p1, LX/Bd7;->d:Ljava/lang/Object;

    iput-object v0, p2, LX/Bd7;->d:Ljava/lang/Object;

    .line 1803426
    iget-object v0, p1, LX/Bd7;->e:Ljava/lang/Throwable;

    iput-object v0, p2, LX/Bd7;->e:Ljava/lang/Throwable;

    .line 1803427
    return-void
.end method

.method public final a(LX/BcP;LX/BcO;)V
    .locals 3

    .prologue
    .line 1803457
    check-cast p2, LX/Bd7;

    .line 1803458
    iget-object v0, p0, LX/BdA;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BdD;

    iget-object v1, p2, LX/Bd7;->g:LX/BdF;

    iget-boolean v2, p2, LX/Bd7;->b:Z

    .line 1803459
    new-instance p0, LX/BdB;

    invoke-direct {p0, v0, p1}, LX/BdB;-><init>(LX/BdD;LX/BcP;)V

    move-object p0, p0

    .line 1803460
    invoke-virtual {v1, p0, v2}, LX/BdF;->a(LX/BdB;Z)V

    .line 1803461
    if-eqz v2, :cond_0

    .line 1803462
    const/4 p0, 0x0

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    invoke-static {p1, p0}, LX/BdA;->a(LX/BcP;Ljava/lang/Boolean;)V

    .line 1803463
    :cond_0
    return-void
.end method

.method public final a(LX/BcP;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 1803414
    iget-object v0, p0, LX/BdA;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    check-cast p2, LX/BdF;

    .line 1803415
    const/4 p1, 0x0

    .line 1803416
    iput-object p1, p2, LX/BdF;->c:LX/BdB;

    .line 1803417
    iget-object v0, p2, LX/BdF;->d:LX/1Zp;

    if-eqz v0, :cond_0

    .line 1803418
    iget-object v0, p2, LX/BdF;->d:LX/1Zp;

    const/4 p0, 0x0

    invoke-virtual {v0, p0}, LX/1Zp;->cancel(Z)Z

    .line 1803419
    iput-object p1, p2, LX/BdF;->d:LX/1Zp;

    .line 1803420
    :cond_0
    return-void
.end method

.method public final a(LX/BcP;Ljava/util/List;LX/BcO;)V
    .locals 5

    .prologue
    .line 1803386
    check-cast p3, LX/Bd7;

    .line 1803387
    iget-object v0, p0, LX/BdA;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v0, p3, LX/Bd7;->c:LX/BdC;

    iget-object v1, p3, LX/Bd7;->d:Ljava/lang/Object;

    iget-object v2, p3, LX/Bd7;->e:Ljava/lang/Throwable;

    iget-object v3, p3, LX/Bd7;->f:LX/Egm;

    const/4 p1, 0x1

    const/4 p0, 0x0

    .line 1803388
    sget-object v4, LX/BdC;->DATA_AVAILABLE:LX/BdC;

    if-ne v0, v4, :cond_0

    if-eqz v1, :cond_3

    if-nez v2, :cond_3

    :cond_0
    move v4, p1

    :goto_0
    const-string p3, "Fetch state is DATA_AVAILABLE but response model/fetch error are not properly set."

    invoke-static {v4, p3}, LX/0Tp;->b(ZLjava/lang/String;)V

    .line 1803389
    sget-object v4, LX/BdC;->DOWNLOAD_ERROR:LX/BdC;

    if-ne v0, v4, :cond_1

    if-nez v1, :cond_2

    if-eqz v2, :cond_2

    :cond_1
    move p0, p1

    :cond_2
    const-string v4, "Fetch state is DOWNLOAD_ERROR but response model/fetch error are not properly set."

    invoke-static {p0, v4}, LX/0Tp;->b(ZLjava/lang/String;)V

    .line 1803390
    check-cast v1, Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;

    .line 1803391
    sget-object v4, LX/Egn;->a:[I

    invoke-virtual {v0}, LX/BdC;->ordinal()I

    move-result p0

    aget v4, v4, p0

    packed-switch v4, :pswitch_data_0

    .line 1803392
    iget-object v4, v3, LX/Egm;->a:LX/BcP;

    invoke-static {v4}, LX/Bce;->b(LX/BcP;)LX/Bcc;

    move-result-object v4

    iget-object p0, v3, LX/Egm;->a:LX/BcP;

    invoke-static {p0}, LX/BdR;->c(LX/1De;)LX/BdP;

    move-result-object p0

    invoke-virtual {v4, p0}, LX/Bcc;->a(LX/1X5;)LX/Bcc;

    move-result-object v4

    invoke-virtual {v4}, LX/Bcc;->b()LX/BcO;

    move-result-object v4

    :goto_1
    move-object v4, v4

    .line 1803393
    invoke-interface {p2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1803394
    return-void

    :cond_3
    move v4, p0

    .line 1803395
    goto :goto_0

    .line 1803396
    :pswitch_0
    iget-object v4, v3, LX/Egm;->a:LX/BcP;

    invoke-static {v4}, LX/Bce;->b(LX/BcP;)LX/Bcc;

    move-result-object v4

    iget-object p0, v3, LX/Egm;->b:Lcom/facebook/bookmark/components/sections/BookmarksRootSectionSpec;

    iget-object p0, p0, Lcom/facebook/bookmark/components/sections/BookmarksRootSectionSpec;->d:LX/EtW;

    iget-object p1, v3, LX/Egm;->a:LX/BcP;

    invoke-virtual {p0, p1}, LX/EtW;->c(LX/1De;)LX/EtV;

    move-result-object p0

    invoke-virtual {v0}, LX/BdC;->name()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, LX/EtV;->b(Ljava/lang/String;)LX/EtV;

    move-result-object p0

    invoke-virtual {v2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object p1

    .line 1803397
    iget-object v3, p0, LX/EtV;->a:Lcom/facebook/fig/components/listitem/FigListItemComponent$FigListItemComponentImpl;

    iput-object p1, v3, Lcom/facebook/fig/components/listitem/FigListItemComponent$FigListItemComponentImpl;->b:Ljava/lang/String;

    .line 1803398
    move-object p0, p0

    .line 1803399
    invoke-virtual {v4, p0}, LX/Bcc;->a(LX/1X5;)LX/Bcc;

    move-result-object v4

    invoke-virtual {v4}, LX/Bcc;->b()LX/BcO;

    move-result-object v4

    goto :goto_1

    .line 1803400
    :pswitch_1
    iget-object v4, v3, LX/Egm;->b:Lcom/facebook/bookmark/components/sections/BookmarksRootSectionSpec;

    iget-object v4, v4, Lcom/facebook/bookmark/components/sections/BookmarksRootSectionSpec;->c:LX/EgS;

    .line 1803401
    new-instance p1, LX/EgQ;

    invoke-direct {p1, v4}, LX/EgQ;-><init>(LX/EgS;)V

    .line 1803402
    sget-object v3, LX/EgS;->a:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/EgR;

    .line 1803403
    if-nez v3, :cond_4

    .line 1803404
    new-instance v3, LX/EgR;

    invoke-direct {v3}, LX/EgR;-><init>()V

    .line 1803405
    :cond_4
    iput-object p1, v3, LX/BcN;->a:LX/BcO;

    .line 1803406
    iput-object p1, v3, LX/EgR;->a:LX/EgQ;

    .line 1803407
    iget-object v4, v3, LX/EgR;->d:Ljava/util/BitSet;

    invoke-virtual {v4}, Ljava/util/BitSet;->clear()V

    .line 1803408
    move-object p1, v3

    .line 1803409
    move-object v4, p1

    .line 1803410
    iget-object p0, v4, LX/EgR;->a:LX/EgQ;

    iput-object v1, p0, LX/EgQ;->b:Lcom/facebook/bookmark/components/sections/BookmarksQueryModels$BookmarksQueryModel;

    .line 1803411
    iget-object p0, v4, LX/EgR;->d:Ljava/util/BitSet;

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Ljava/util/BitSet;->set(I)V

    .line 1803412
    move-object v4, v4

    .line 1803413
    invoke-virtual {v4}, LX/EgR;->b()LX/BcO;

    move-result-object v4

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final b(LX/BcO;LX/BcO;)V
    .locals 1

    .prologue
    .line 1803382
    check-cast p1, LX/Bd7;

    .line 1803383
    check-cast p2, LX/Bd7;

    .line 1803384
    iget-object v0, p1, LX/Bd7;->g:LX/BdF;

    iput-object v0, p2, LX/Bd7;->g:LX/BdF;

    .line 1803385
    return-void
.end method

.method public final b(LX/BcP;LX/BcO;)V
    .locals 1

    .prologue
    .line 1803378
    check-cast p2, LX/Bd7;

    .line 1803379
    iget-object v0, p0, LX/BdA;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v0, p2, LX/Bd7;->g:LX/BdF;

    .line 1803380
    const/4 p0, 0x0

    const/4 p1, 0x0

    invoke-virtual {v0, p0, p1}, LX/BdF;->a(LX/BdB;Z)V

    .line 1803381
    return-void
.end method

.method public final c(LX/BcP;LX/BcO;)V
    .locals 5

    .prologue
    .line 1803357
    check-cast p2, LX/Bd7;

    .line 1803358
    invoke-static {}, LX/BcS;->a()LX/1np;

    move-result-object v0

    .line 1803359
    invoke-static {}, LX/BcS;->a()LX/1np;

    move-result-object v1

    .line 1803360
    invoke-static {}, LX/BcS;->a()LX/1np;

    move-result-object v2

    .line 1803361
    invoke-static {}, LX/BcS;->a()LX/1np;

    move-result-object v3

    .line 1803362
    iget-object v4, p0, LX/BdA;->a:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    const/4 p0, 0x0

    .line 1803363
    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    .line 1803364
    iput-object v4, v0, LX/1np;->a:Ljava/lang/Object;

    .line 1803365
    sget-object v4, LX/BdC;->INITIAL_STATE:LX/BdC;

    .line 1803366
    iput-object v4, v1, LX/1np;->a:Ljava/lang/Object;

    .line 1803367
    iput-object p0, v2, LX/1np;->a:Ljava/lang/Object;

    .line 1803368
    iput-object p0, v3, LX/1np;->a:Ljava/lang/Object;

    .line 1803369
    iget-object v4, v0, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v4

    .line 1803370
    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p2, LX/Bd7;->b:Z

    .line 1803371
    iget-object v0, v1, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 1803372
    check-cast v0, LX/BdC;

    iput-object v0, p2, LX/Bd7;->c:LX/BdC;

    .line 1803373
    iget-object v0, v2, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 1803374
    iput-object v0, p2, LX/Bd7;->d:Ljava/lang/Object;

    .line 1803375
    iget-object v0, v3, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 1803376
    check-cast v0, Ljava/lang/Throwable;

    iput-object v0, p2, LX/Bd7;->e:Ljava/lang/Throwable;

    .line 1803377
    return-void
.end method

.method public final d(LX/BcP;LX/BcO;)V
    .locals 3

    .prologue
    .line 1803347
    move-object v0, p2

    check-cast v0, LX/Bd7;

    .line 1803348
    check-cast p2, LX/Bd7;

    .line 1803349
    iget-object v1, p0, LX/BdA;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/BdD;

    iget-object v2, p2, LX/Bd7;->f:LX/Egm;

    .line 1803350
    new-instance p0, LX/BdF;

    iget-object p2, v1, LX/BdD;->a:LX/0tX;

    invoke-direct {p0, v2, p2}, LX/BdF;-><init>(LX/Egm;LX/0tX;)V

    .line 1803351
    invoke-virtual {p0}, LX/BdF;->b()V

    .line 1803352
    const/4 p2, 0x1

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-static {p1, p2}, LX/BdA;->a(LX/BcP;Ljava/lang/Boolean;)V

    .line 1803353
    move-object v1, p0

    .line 1803354
    move-object v1, v1

    .line 1803355
    iput-object v1, v0, LX/Bd7;->g:LX/BdF;

    .line 1803356
    return-void
.end method

.method public final e(LX/BcP;LX/BcO;)V
    .locals 1

    .prologue
    .line 1803343
    check-cast p2, LX/Bd7;

    .line 1803344
    iget-object v0, p0, LX/BdA;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v0, p2, LX/Bd7;->g:LX/BdF;

    .line 1803345
    invoke-virtual {v0}, LX/BdF;->b()V

    .line 1803346
    return-void
.end method
