.class public final LX/CgY;
.super LX/0Vd;
.source ""


# instance fields
.field public final synthetic a:LX/CgZ;


# direct methods
.method public constructor <init>(LX/CgZ;)V
    .locals 0

    .prologue
    .line 1927242
    iput-object p1, p0, LX/CgY;->a:LX/CgZ;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 1927243
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 1927244
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1927245
    iget-object v1, p0, LX/CgY;->a:LX/CgZ;

    iget-object v1, v1, LX/CgZ;->c:Ljava/lang/String;

    iget-object v2, p0, LX/CgY;->a:LX/CgZ;

    iget-object v2, v2, LX/CgZ;->d:LX/0o8;

    .line 1927246
    if-eqz p1, :cond_3

    .line 1927247
    iget-object v3, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v3, v3

    .line 1927248
    if-eqz v3, :cond_3

    .line 1927249
    iget-object v3, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v3, v3

    .line 1927250
    check-cast v3, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel;

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel;->a()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel$ReactionUnitsModel;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 1927251
    iget-object v3, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v3, v3

    .line 1927252
    check-cast v3, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel;

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel;->a()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel$ReactionUnitsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel$ReactionUnitsModel;->a()LX/0Px;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 1927253
    iget-object v3, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v3, v3

    .line 1927254
    check-cast v3, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel;

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel;->a()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel$ReactionUnitsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel$ReactionUnitsModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_3

    const/4 v3, 0x1

    :goto_0
    move v3, v3

    .line 1927255
    if-eqz v3, :cond_0

    .line 1927256
    iget-object v3, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v3, v3

    .line 1927257
    check-cast v3, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel;

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel;->j()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel$ReactionUnitsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel$ReactionUnitsModel;->a()LX/0Px;

    move-result-object v3

    const/4 p0, 0x0

    invoke-virtual {v3, p0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoriesModel$EdgesModel;

    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoriesModel$EdgesModel;->j()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v3

    .line 1927258
    invoke-virtual {v3}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->d()Ljava/lang/String;

    move-result-object p0

    .line 1927259
    if-nez p0, :cond_1

    .line 1927260
    :cond_0
    :goto_1
    return-void

    .line 1927261
    :cond_1
    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1927262
    invoke-interface {v2, v3, p0}, LX/0o8;->a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;Ljava/lang/String;)Z

    goto :goto_1

    .line 1927263
    :cond_2
    invoke-interface {v2, v1}, LX/0o8;->a(Ljava/lang/String;)Z

    .line 1927264
    invoke-interface {v2, v3, p0}, LX/0o8;->a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;Ljava/lang/String;)Z

    goto :goto_1

    :cond_3
    const/4 v3, 0x0

    goto :goto_0
.end method
