.class public final LX/CVM;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 28

    .prologue
    .line 1903040
    const/16 v24, 0x0

    .line 1903041
    const/16 v23, 0x0

    .line 1903042
    const/16 v22, 0x0

    .line 1903043
    const/16 v21, 0x0

    .line 1903044
    const/16 v20, 0x0

    .line 1903045
    const/16 v19, 0x0

    .line 1903046
    const/16 v18, 0x0

    .line 1903047
    const/16 v17, 0x0

    .line 1903048
    const/16 v16, 0x0

    .line 1903049
    const/4 v15, 0x0

    .line 1903050
    const/4 v14, 0x0

    .line 1903051
    const/4 v13, 0x0

    .line 1903052
    const/4 v12, 0x0

    .line 1903053
    const/4 v11, 0x0

    .line 1903054
    const/4 v10, 0x0

    .line 1903055
    const/4 v9, 0x0

    .line 1903056
    const/4 v8, 0x0

    .line 1903057
    const/4 v7, 0x0

    .line 1903058
    const/4 v6, 0x0

    .line 1903059
    const/4 v5, 0x0

    .line 1903060
    const/4 v4, 0x0

    .line 1903061
    const/4 v3, 0x0

    .line 1903062
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v25

    sget-object v26, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    if-eq v0, v1, :cond_1

    .line 1903063
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1903064
    const/4 v3, 0x0

    .line 1903065
    :goto_0
    return v3

    .line 1903066
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1903067
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v25

    sget-object v26, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    if-eq v0, v1, :cond_15

    .line 1903068
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v25

    .line 1903069
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1903070
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v26

    sget-object v27, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    if-eq v0, v1, :cond_1

    if-eqz v25, :cond_1

    .line 1903071
    const-string v26, "__type__"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-nez v26, :cond_2

    const-string v26, "__typename"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_3

    .line 1903072
    :cond_2
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v24

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v24

    goto :goto_1

    .line 1903073
    :cond_3
    const-string v26, "additional_info"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_4

    .line 1903074
    invoke-static/range {p0 .. p1}, LX/CW8;->a(LX/15w;LX/186;)I

    move-result v23

    goto :goto_1

    .line 1903075
    :cond_4
    const-string v26, "anchor_id"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_5

    .line 1903076
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v22

    goto :goto_1

    .line 1903077
    :cond_5
    const-string v26, "body"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_6

    .line 1903078
    invoke-static/range {p0 .. p1}, LX/CVr;->b(LX/15w;LX/186;)I

    move-result v21

    goto :goto_1

    .line 1903079
    :cond_6
    const-string v26, "component_flow_id"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_7

    .line 1903080
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v20

    goto/16 :goto_1

    .line 1903081
    :cond_7
    const-string v26, "disclaimer"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_8

    .line 1903082
    invoke-static/range {p0 .. p1}, LX/CVl;->a(LX/15w;LX/186;)I

    move-result v19

    goto/16 :goto_1

    .line 1903083
    :cond_8
    const-string v26, "event_listeners"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_9

    .line 1903084
    invoke-static/range {p0 .. p1}, LX/CVU;->a(LX/15w;LX/186;)I

    move-result v18

    goto/16 :goto_1

    .line 1903085
    :cond_9
    const-string v26, "footer"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_a

    .line 1903086
    invoke-static/range {p0 .. p1}, LX/CVs;->a(LX/15w;LX/186;)I

    move-result v17

    goto/16 :goto_1

    .line 1903087
    :cond_a
    const-string v26, "header"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_b

    .line 1903088
    invoke-static/range {p0 .. p1}, LX/CVr;->b(LX/15w;LX/186;)I

    move-result v16

    goto/16 :goto_1

    .line 1903089
    :cond_b
    const-string v26, "navbar_action"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_c

    .line 1903090
    invoke-static/range {p0 .. p1}, LX/CVi;->a(LX/15w;LX/186;)I

    move-result v15

    goto/16 :goto_1

    .line 1903091
    :cond_c
    const-string v26, "need_delivery_address"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_d

    .line 1903092
    const/4 v5, 0x1

    .line 1903093
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v14

    goto/16 :goto_1

    .line 1903094
    :cond_d
    const-string v26, "need_phone_number"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_e

    .line 1903095
    const/4 v4, 0x1

    .line 1903096
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v13

    goto/16 :goto_1

    .line 1903097
    :cond_e
    const-string v26, "need_verified_email"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_f

    .line 1903098
    const/4 v3, 0x1

    .line 1903099
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v12

    goto/16 :goto_1

    .line 1903100
    :cond_f
    const-string v26, "next_button"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_10

    .line 1903101
    invoke-static/range {p0 .. p1}, LX/CVg;->a(LX/15w;LX/186;)I

    move-result v11

    goto/16 :goto_1

    .line 1903102
    :cond_10
    const-string v26, "order_info"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_11

    .line 1903103
    invoke-static/range {p0 .. p1}, LX/CW6;->a(LX/15w;LX/186;)I

    move-result v10

    goto/16 :goto_1

    .line 1903104
    :cond_11
    const-string v26, "provider_info"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_12

    .line 1903105
    invoke-static/range {p0 .. p1}, LX/CW5;->a(LX/15w;LX/186;)I

    move-result v9

    goto/16 :goto_1

    .line 1903106
    :cond_12
    const-string v26, "screen_id"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_13

    .line 1903107
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto/16 :goto_1

    .line 1903108
    :cond_13
    const-string v26, "screen_type"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v26

    if-eqz v26, :cond_14

    .line 1903109
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v7

    goto/16 :goto_1

    .line 1903110
    :cond_14
    const-string v26, "title"

    invoke-virtual/range {v25 .. v26}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_0

    .line 1903111
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_1

    .line 1903112
    :cond_15
    const/16 v25, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 1903113
    const/16 v25, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v25

    move/from16 v2, v24

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1903114
    const/16 v24, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v24

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1903115
    const/16 v23, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v23

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1903116
    const/16 v22, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v22

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1903117
    const/16 v21, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v21

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1903118
    const/16 v20, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1903119
    const/16 v19, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v19

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1903120
    const/16 v18, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1903121
    const/16 v17, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v16

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1903122
    const/16 v16, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v1, v15}, LX/186;->b(II)V

    .line 1903123
    if-eqz v5, :cond_16

    .line 1903124
    const/16 v5, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v14}, LX/186;->a(IZ)V

    .line 1903125
    :cond_16
    if-eqz v4, :cond_17

    .line 1903126
    const/16 v4, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v13}, LX/186;->a(IZ)V

    .line 1903127
    :cond_17
    if-eqz v3, :cond_18

    .line 1903128
    const/16 v3, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v12}, LX/186;->a(IZ)V

    .line 1903129
    :cond_18
    const/16 v3, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v11}, LX/186;->b(II)V

    .line 1903130
    const/16 v3, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v10}, LX/186;->b(II)V

    .line 1903131
    const/16 v3, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9}, LX/186;->b(II)V

    .line 1903132
    const/16 v3, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v8}, LX/186;->b(II)V

    .line 1903133
    const/16 v3, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 1903134
    const/16 v3, 0x12

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 1903135
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/16 v2, 0x11

    const/4 v1, 0x0

    .line 1903136
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1903137
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 1903138
    if-eqz v0, :cond_0

    .line 1903139
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1903140
    invoke-static {p0, p1, v1, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1903141
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1903142
    if-eqz v0, :cond_1

    .line 1903143
    const-string v1, "additional_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1903144
    invoke-static {p0, v0, p2}, LX/CW8;->a(LX/15i;ILX/0nX;)V

    .line 1903145
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1903146
    if-eqz v0, :cond_2

    .line 1903147
    const-string v1, "anchor_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1903148
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1903149
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1903150
    if-eqz v0, :cond_3

    .line 1903151
    const-string v1, "body"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1903152
    invoke-static {p0, v0, p2, p3}, LX/CVr;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1903153
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1903154
    if-eqz v0, :cond_4

    .line 1903155
    const-string v1, "component_flow_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1903156
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1903157
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1903158
    if-eqz v0, :cond_5

    .line 1903159
    const-string v1, "disclaimer"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1903160
    invoke-static {p0, v0, p2, p3}, LX/CVl;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1903161
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1903162
    if-eqz v0, :cond_6

    .line 1903163
    const-string v1, "event_listeners"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1903164
    invoke-static {p0, v0, p2, p3}, LX/CVU;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1903165
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1903166
    if-eqz v0, :cond_7

    .line 1903167
    const-string v1, "footer"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1903168
    invoke-static {p0, v0, p2, p3}, LX/CVs;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1903169
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1903170
    if-eqz v0, :cond_8

    .line 1903171
    const-string v1, "header"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1903172
    invoke-static {p0, v0, p2, p3}, LX/CVr;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1903173
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1903174
    if-eqz v0, :cond_9

    .line 1903175
    const-string v1, "navbar_action"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1903176
    invoke-static {p0, v0, p2, p3}, LX/CVi;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1903177
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1903178
    if-eqz v0, :cond_a

    .line 1903179
    const-string v1, "need_delivery_address"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1903180
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1903181
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1903182
    if-eqz v0, :cond_b

    .line 1903183
    const-string v1, "need_phone_number"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1903184
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1903185
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1903186
    if-eqz v0, :cond_c

    .line 1903187
    const-string v1, "need_verified_email"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1903188
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1903189
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1903190
    if-eqz v0, :cond_d

    .line 1903191
    const-string v1, "next_button"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1903192
    invoke-static {p0, v0, p2, p3}, LX/CVg;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1903193
    :cond_d
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1903194
    if-eqz v0, :cond_e

    .line 1903195
    const-string v1, "order_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1903196
    invoke-static {p0, v0, p2, p3}, LX/CW6;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1903197
    :cond_e
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1903198
    if-eqz v0, :cond_f

    .line 1903199
    const-string v1, "provider_info"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1903200
    invoke-static {p0, v0, p2, p3}, LX/CW5;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1903201
    :cond_f
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1903202
    if-eqz v0, :cond_10

    .line 1903203
    const-string v1, "screen_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1903204
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1903205
    :cond_10
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1903206
    if-eqz v0, :cond_11

    .line 1903207
    const-string v0, "screen_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1903208
    invoke-virtual {p0, p1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1903209
    :cond_11
    const/16 v0, 0x12

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1903210
    if-eqz v0, :cond_12

    .line 1903211
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1903212
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1903213
    :cond_12
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1903214
    return-void
.end method
