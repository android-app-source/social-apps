.class public final enum LX/CFs;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/CFs;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/CFs;

.field public static final enum IN:LX/CFs;

.field public static final enum MOVE:LX/CFs;

.field public static final enum OUT:LX/CFs;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1864572
    new-instance v0, LX/CFs;

    const-string v1, "IN"

    invoke-direct {v0, v1, v2}, LX/CFs;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CFs;->IN:LX/CFs;

    new-instance v0, LX/CFs;

    const-string v1, "OUT"

    invoke-direct {v0, v1, v3}, LX/CFs;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CFs;->OUT:LX/CFs;

    new-instance v0, LX/CFs;

    const-string v1, "MOVE"

    invoke-direct {v0, v1, v4}, LX/CFs;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CFs;->MOVE:LX/CFs;

    .line 1864573
    const/4 v0, 0x3

    new-array v0, v0, [LX/CFs;

    sget-object v1, LX/CFs;->IN:LX/CFs;

    aput-object v1, v0, v2

    sget-object v1, LX/CFs;->OUT:LX/CFs;

    aput-object v1, v0, v3

    sget-object v1, LX/CFs;->MOVE:LX/CFs;

    aput-object v1, v0, v4

    sput-object v0, LX/CFs;->$VALUES:[LX/CFs;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1864574
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/CFs;
    .locals 1

    .prologue
    .line 1864575
    const-class v0, LX/CFs;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/CFs;

    return-object v0
.end method

.method public static values()[LX/CFs;
    .locals 1

    .prologue
    .line 1864576
    sget-object v0, LX/CFs;->$VALUES:[LX/CFs;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/CFs;

    return-object v0
.end method
