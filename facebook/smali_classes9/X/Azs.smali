.class public final enum LX/Azs;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Azs;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Azs;

.field public static final enum DESTROYED:LX/Azs;

.field public static final enum PAUSED:LX/Azs;

.field public static final enum RESUMED:LX/Azs;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1733008
    new-instance v0, LX/Azs;

    const-string v1, "RESUMED"

    invoke-direct {v0, v1, v2}, LX/Azs;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Azs;->RESUMED:LX/Azs;

    .line 1733009
    new-instance v0, LX/Azs;

    const-string v1, "PAUSED"

    invoke-direct {v0, v1, v3}, LX/Azs;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Azs;->PAUSED:LX/Azs;

    .line 1733010
    new-instance v0, LX/Azs;

    const-string v1, "DESTROYED"

    invoke-direct {v0, v1, v4}, LX/Azs;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Azs;->DESTROYED:LX/Azs;

    .line 1733011
    const/4 v0, 0x3

    new-array v0, v0, [LX/Azs;

    sget-object v1, LX/Azs;->RESUMED:LX/Azs;

    aput-object v1, v0, v2

    sget-object v1, LX/Azs;->PAUSED:LX/Azs;

    aput-object v1, v0, v3

    sget-object v1, LX/Azs;->DESTROYED:LX/Azs;

    aput-object v1, v0, v4

    sput-object v0, LX/Azs;->$VALUES:[LX/Azs;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1733013
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Azs;
    .locals 1

    .prologue
    .line 1733014
    const-class v0, LX/Azs;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Azs;

    return-object v0
.end method

.method public static values()[LX/Azs;
    .locals 1

    .prologue
    .line 1733012
    sget-object v0, LX/Azs;->$VALUES:[LX/Azs;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Azs;

    return-object v0
.end method
