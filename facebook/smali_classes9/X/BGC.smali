.class public LX/BGC;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/Jvi;

.field public b:Lcom/facebook/resources/ui/FbTextView;

.field public final c:LX/0So;

.field public final d:Landroid/widget/ProgressBar;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:Landroid/view/ViewGroup;

.field public f:J

.field public g:Z

.field public h:LX/0wY;

.field public i:Z

.field private j:Z

.field public k:I

.field private l:I

.field private final m:Landroid/os/Handler;

.field public final n:LX/0wa;


# direct methods
.method public constructor <init>(LX/Jvi;Landroid/widget/ProgressBar;Lcom/facebook/resources/ui/FbTextView;Landroid/view/ViewGroup;ZLX/0wY;LX/0So;)V
    .locals 2
    .param p1    # LX/Jvi;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Landroid/widget/ProgressBar;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/resources/ui/FbTextView;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Landroid/view/ViewGroup;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 1766791
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1766792
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/BGC;->j:Z

    .line 1766793
    iput v1, p0, LX/BGC;->k:I

    .line 1766794
    iput v1, p0, LX/BGC;->l:I

    .line 1766795
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, LX/BGC;->m:Landroid/os/Handler;

    .line 1766796
    new-instance v0, LX/BGB;

    invoke-direct {v0, p0}, LX/BGB;-><init>(LX/BGC;)V

    iput-object v0, p0, LX/BGC;->n:LX/0wa;

    .line 1766797
    iput-object p1, p0, LX/BGC;->a:LX/Jvi;

    .line 1766798
    iput-object p2, p0, LX/BGC;->d:Landroid/widget/ProgressBar;

    .line 1766799
    iput-object p3, p0, LX/BGC;->b:Lcom/facebook/resources/ui/FbTextView;

    .line 1766800
    iput-object p4, p0, LX/BGC;->e:Landroid/view/ViewGroup;

    .line 1766801
    iput-object p6, p0, LX/BGC;->h:LX/0wY;

    .line 1766802
    iput-object p7, p0, LX/BGC;->c:LX/0So;

    .line 1766803
    iput-boolean p5, p0, LX/BGC;->i:Z

    .line 1766804
    return-void
.end method

.method public static a$redex0(LX/BGC;I)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x3e8

    const v3, 0x3ecccccd    # 0.4f

    const/4 v4, 0x0

    .line 1766805
    int-to-long v0, p1

    div-long/2addr v0, v6

    .line 1766806
    invoke-static {v0, v1}, Landroid/text/format/DateUtils;->formatElapsedTime(J)Ljava/lang/String;

    move-result-object v0

    .line 1766807
    iget-boolean v1, p0, LX/BGC;->i:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, LX/BGC;->j:Z

    if-nez v1, :cond_0

    .line 1766808
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/BGC;->j:Z

    .line 1766809
    iget-object v1, p0, LX/BGC;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1}, Lcom/facebook/resources/ui/FbTextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-static {p0}, LX/BGC;->e(LX/BGC;)I

    move-result v2

    neg-int v2, v2

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, v6, v7}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    const-wide/16 v2, 0x1f4

    invoke-virtual {v1, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 1766810
    :cond_0
    iget-boolean v1, p0, LX/BGC;->i:Z

    if-eqz v1, :cond_1

    .line 1766811
    rem-int/lit16 v1, p1, 0x578

    const/16 v2, 0x2bc

    if-le v1, v2, :cond_2

    .line 1766812
    iget-object v1, p0, LX/BGC;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1}, Lcom/facebook/resources/ui/FbTextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v1

    aget-object v1, v1, v4

    invoke-virtual {v1, v4}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 1766813
    :cond_1
    :goto_0
    iget-object v1, p0, LX/BGC;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1766814
    return-void

    .line 1766815
    :cond_2
    iget-object v1, p0, LX/BGC;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1}, Lcom/facebook/resources/ui/FbTextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v1

    aget-object v1, v1, v4

    const/16 v2, 0xff

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    goto :goto_0
.end method

.method public static e(LX/BGC;)I
    .locals 1

    .prologue
    .line 1766816
    iget v0, p0, LX/BGC;->l:I

    div-int/lit8 v0, v0, 0x3

    return v0
.end method


# virtual methods
.method public final a(Z)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x0

    .line 1766817
    iget-object v0, p0, LX/BGC;->d:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    .line 1766818
    iget-object v0, p0, LX/BGC;->d:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1766819
    :cond_0
    iget-boolean v0, p0, LX/BGC;->i:Z

    if-eqz v0, :cond_1

    .line 1766820
    iget-object v0, p0, LX/BGC;->m:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 1766821
    :cond_1
    iget-object v0, p0, LX/BGC;->b:Lcom/facebook/resources/ui/FbTextView;

    if-eqz v0, :cond_2

    .line 1766822
    iget-object v0, p0, LX/BGC;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1766823
    :cond_2
    iget-object v0, p0, LX/BGC;->h:LX/0wY;

    iget-object v1, p0, LX/BGC;->n:LX/0wa;

    invoke-interface {v0, v1}, LX/0wY;->b(LX/0wa;)V

    .line 1766824
    if-nez p1, :cond_3

    iget-boolean v0, p0, LX/BGC;->g:Z

    if-eqz v0, :cond_3

    .line 1766825
    iput-boolean v2, p0, LX/BGC;->g:Z

    .line 1766826
    iget-object v0, p0, LX/BGC;->e:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1766827
    :cond_3
    return-void
.end method
