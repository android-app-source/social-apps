.class public LX/AVW;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/AVW;


# instance fields
.field public final a:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1679914
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1679915
    iput-object p1, p0, LX/AVW;->a:LX/0Zb;

    .line 1679916
    return-void
.end method

.method public static a(LX/0QB;)LX/AVW;
    .locals 4

    .prologue
    .line 1679901
    sget-object v0, LX/AVW;->b:LX/AVW;

    if-nez v0, :cond_1

    .line 1679902
    const-class v1, LX/AVW;

    monitor-enter v1

    .line 1679903
    :try_start_0
    sget-object v0, LX/AVW;->b:LX/AVW;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1679904
    if-eqz v2, :cond_0

    .line 1679905
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1679906
    new-instance p0, LX/AVW;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-direct {p0, v3}, LX/AVW;-><init>(LX/0Zb;)V

    .line 1679907
    move-object v0, p0

    .line 1679908
    sput-object v0, LX/AVW;->b:LX/AVW;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1679909
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1679910
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1679911
    :cond_1
    sget-object v0, LX/AVW;->b:LX/AVW;

    return-object v0

    .line 1679912
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1679913
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
