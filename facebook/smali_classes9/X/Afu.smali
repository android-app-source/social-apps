.class public LX/Afu;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:LX/0gX;

.field public final c:LX/0Sh;

.field public final d:LX/03V;

.field public final e:LX/0tX;

.field public final f:Ljava/util/concurrent/ExecutorService;

.field public g:LX/AeV;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:LX/0gM;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1699849
    const-class v0, LX/Afu;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Afu;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/03V;LX/0gX;LX/0Sh;Ljava/util/concurrent/ExecutorService;LX/0tX;)V
    .locals 0
    .param p4    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/SingleThreadedExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1699850
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1699851
    iput-object p2, p0, LX/Afu;->b:LX/0gX;

    .line 1699852
    iput-object p3, p0, LX/Afu;->c:LX/0Sh;

    .line 1699853
    iput-object p1, p0, LX/Afu;->d:LX/03V;

    .line 1699854
    iput-object p5, p0, LX/Afu;->e:LX/0tX;

    .line 1699855
    iput-object p4, p0, LX/Afu;->f:Ljava/util/concurrent/ExecutorService;

    .line 1699856
    return-void
.end method


# virtual methods
.method public final a(LX/AeV;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1699857
    iget-boolean v0, p0, LX/Afu;->j:Z

    if-eqz v0, :cond_1

    .line 1699858
    :cond_0
    :goto_0
    return-void

    .line 1699859
    :cond_1
    iget-object v0, p0, LX/Afu;->c:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 1699860
    iput-object p2, p0, LX/Afu;->i:Ljava/lang/String;

    .line 1699861
    iput-object p1, p0, LX/Afu;->g:LX/AeV;

    .line 1699862
    iget-object v0, p0, LX/Afu;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Afu;->g:LX/AeV;

    if-eqz v0, :cond_0

    .line 1699863
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Afu;->j:Z

    .line 1699864
    new-instance v0, LX/6U5;

    invoke-direct {v0}, LX/6U5;-><init>()V

    move-object v0, v0

    .line 1699865
    const-string p1, "targetID"

    iget-object p2, p0, LX/Afu;->i:Ljava/lang/String;

    invoke-virtual {v0, p1, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1699866
    const-string p1, "first"

    const/16 p2, 0xa

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {v0, p1, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1699867
    iget-object p1, p0, LX/Afu;->e:LX/0tX;

    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 1699868
    new-instance p1, LX/Aft;

    invoke-direct {p1, p0}, LX/Aft;-><init>(LX/Afu;)V

    iget-object p2, p0, LX/Afu;->f:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, p1, p2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1699869
    new-instance v0, LX/6U4;

    invoke-direct {v0}, LX/6U4;-><init>()V

    move-object v0, v0

    .line 1699870
    new-instance v1, LX/4Jr;

    invoke-direct {v1}, LX/4Jr;-><init>()V

    iget-object p1, p0, LX/Afu;->i:Ljava/lang/String;

    .line 1699871
    const-string p2, "video_id"

    invoke-virtual {v1, p2, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1699872
    move-object v1, v1

    .line 1699873
    const-string p1, "input"

    invoke-virtual {v0, p1, v1}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1699874
    const-string v1, "scale"

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    .line 1699875
    :try_start_0
    iget-object v1, p0, LX/Afu;->b:LX/0gX;

    new-instance p1, LX/Afs;

    invoke-direct {p1, p0}, LX/Afs;-><init>(LX/Afu;)V

    invoke-virtual {v1, v0, p1}, LX/0gX;->a(LX/0gV;LX/0TF;)LX/0gM;

    move-result-object v0

    iput-object v0, p0, LX/Afu;->h:LX/0gM;
    :try_end_0
    .catch LX/31B; {:try_start_0 .. :try_end_0} :catch_0

    .line 1699876
    :goto_1
    goto :goto_0

    .line 1699877
    :catch_0
    goto :goto_1
.end method
