.class public final LX/BXa;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field public final synthetic a:Lcom/facebook/widget/mediareorderview/ImagesReorderView;


# direct methods
.method public constructor <init>(Lcom/facebook/widget/mediareorderview/ImagesReorderView;)V
    .locals 0

    .prologue
    .line 1793318
    iput-object p1, p0, LX/BXa;->a:Lcom/facebook/widget/mediareorderview/ImagesReorderView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onGlobalLayout()V
    .locals 4

    .prologue
    .line 1793314
    iget-object v0, p0, LX/BXa;->a:Lcom/facebook/widget/mediareorderview/ImagesReorderView;

    iget-object v0, v0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->j:Lcom/facebook/widget/ScrollingAwareScrollView;

    const/4 v1, 0x0

    iget-object v2, p0, LX/BXa;->a:Lcom/facebook/widget/mediareorderview/ImagesReorderView;

    iget-object v2, v2, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->p:LX/BXg;

    iget v2, v2, LX/BXg;->i:I

    invoke-virtual {v0, v1, v2}, Lcom/facebook/widget/ScrollingAwareScrollView;->scrollTo(II)V

    .line 1793315
    iget-object v0, p0, LX/BXa;->a:Lcom/facebook/widget/mediareorderview/ImagesReorderView;

    iget-object v0, v0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->r:LX/0wd;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, LX/0wd;->a(D)LX/0wd;

    move-result-object v0

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    .line 1793316
    iget-object v0, p0, LX/BXa;->a:Lcom/facebook/widget/mediareorderview/ImagesReorderView;

    iget-object v0, v0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->k:Lcom/facebook/widget/CustomViewGroup;

    invoke-static {v0, p0}, LX/1r0;->a(Landroid/view/View;Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 1793317
    return-void
.end method
