.class public final LX/AYg;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveVideoViewersQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/AYh;


# direct methods
.method public constructor <init>(LX/AYh;)V
    .locals 0

    .prologue
    .line 1685955
    iput-object p1, p0, LX/AYg;->a:LX/AYh;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 1685956
    iget-object v0, p0, LX/AYg;->a:LX/AYh;

    iget-object v1, v0, LX/AYh;->e:LX/03V;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, LX/AYh;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "_graphFailure"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "Failed to get live watch events for "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/AYg;->a:LX/AYh;

    iget-object v3, v3, LX/AYh;->g:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/AYg;->a:LX/AYh;

    iget-object v0, v0, LX/AYh;->g:Ljava/lang/String;

    :goto_0
    invoke-virtual {v1, v2, v0, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1685957
    return-void

    .line 1685958
    :cond_0
    const-string v0, "no story id"

    goto :goto_0
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 7

    .prologue
    .line 1685959
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v1, 0x0

    const/4 v5, 0x0

    .line 1685960
    if-nez p1, :cond_0

    .line 1685961
    iget-object v0, p0, LX/AYg;->a:LX/AYh;

    invoke-static {v0, v5, v1, v5}, LX/AYh;->a$redex0(LX/AYh;Ljava/util/List;ILcom/facebook/graphql/model/GraphQLFeedback;)V

    .line 1685962
    :goto_0
    return-void

    .line 1685963
    :cond_0
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1685964
    check-cast v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveVideoViewersQueryModel;

    .line 1685965
    if-nez v0, :cond_1

    .line 1685966
    iget-object v0, p0, LX/AYg;->a:LX/AYh;

    invoke-static {v0, v5, v1, v5}, LX/AYh;->a$redex0(LX/AYh;Ljava/util/List;ILcom/facebook/graphql/model/GraphQLFeedback;)V

    goto :goto_0

    .line 1685967
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveVideoViewersQueryModel;->k()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveVideoViewersCollectionFragmentModel;

    move-result-object v2

    .line 1685968
    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveVideoViewersQueryModel;->j()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;

    move-result-object v0

    invoke-static {v0}, LX/6Tu;->a(Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v3

    .line 1685969
    if-nez v2, :cond_2

    .line 1685970
    iget-object v0, p0, LX/AYg;->a:LX/AYh;

    invoke-static {v0, v5, v1, v3}, LX/AYh;->a$redex0(LX/AYh;Ljava/util/List;ILcom/facebook/graphql/model/GraphQLFeedback;)V

    goto :goto_0

    .line 1685971
    :cond_2
    invoke-virtual {v2}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveVideoViewersCollectionFragmentModel;->j()LX/0Px;

    move-result-object v4

    .line 1685972
    if-nez v4, :cond_3

    .line 1685973
    iget-object v0, p0, LX/AYg;->a:LX/AYh;

    invoke-static {v0, v5, v1, v3}, LX/AYh;->a$redex0(LX/AYh;Ljava/util/List;ILcom/facebook/graphql/model/GraphQLFeedback;)V

    goto :goto_0

    .line 1685974
    :cond_3
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1685975
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v6

    :goto_1
    if-ge v1, v6, :cond_5

    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveVideoViewersCollectionFragmentModel$EdgesModel;

    .line 1685976
    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveVideoViewersCollectionFragmentModel$EdgesModel;->a()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveEventAuthorModel;

    move-result-object v0

    invoke-static {v0}, LX/AcC;->a(Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveEventAuthorModel;)LX/AcC;

    move-result-object v0

    .line 1685977
    if-eqz v0, :cond_4

    .line 1685978
    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1685979
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1685980
    :cond_5
    iget-object v0, p0, LX/AYg;->a:LX/AYh;

    invoke-virtual {v2}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveVideoViewersCollectionFragmentModel;->a()I

    move-result v1

    invoke-static {v0, v5, v1, v3}, LX/AYh;->a$redex0(LX/AYh;Ljava/util/List;ILcom/facebook/graphql/model/GraphQLFeedback;)V

    goto :goto_0
.end method
