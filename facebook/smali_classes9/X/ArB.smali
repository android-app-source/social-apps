.class public final LX/ArB;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/ArJ;

.field public final synthetic b:Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;LX/ArJ;)V
    .locals 0

    .prologue
    .line 1718659
    iput-object p1, p0, LX/ArB;->b:Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;

    iput-object p2, p0, LX/ArB;->a:LX/ArJ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;)V
    .locals 4

    .prologue
    .line 1718660
    iget-object v0, p0, LX/ArB;->b:Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;

    invoke-static {v0}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->X(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, LX/ArB;->b:Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;

    iget-object v0, v0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ai:LX/0ij;

    invoke-virtual {v0}, LX/0ij;->e()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v0

    invoke-static {v0}, LX/87R;->a(LX/0io;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1718661
    iget-object v0, p0, LX/ArB;->b:Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;

    iget-object v0, v0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ai:LX/0ij;

    invoke-virtual {v0}, LX/0ij;->c()LX/0jJ;

    move-result-object v0

    sget-object v1, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->G:LX/0jK;

    invoke-virtual {v0, v1}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v0

    check-cast v0, LX/0jL;

    .line 1718662
    iget-object v1, p0, LX/ArB;->b:Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;

    invoke-static {v1, v0, p1}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->a$redex0(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;LX/0jL;Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;)V

    .line 1718663
    invoke-virtual {v0}, LX/0jL;->a()V

    .line 1718664
    iget-object v0, p0, LX/ArB;->b:Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;

    iget-boolean v0, v0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->aw:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/ArB;->a:LX/ArJ;

    if-eqz v0, :cond_0

    .line 1718665
    iget-object v0, p0, LX/ArB;->b:Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;

    iget-object v0, v0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ac:LX/ArT;

    iget-object v1, p0, LX/ArB;->a:LX/ArJ;

    iget-object v2, p0, LX/ArB;->b:Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;

    iget-object v2, v2, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->E:Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;

    invoke-virtual {v0, v1, v2}, LX/ArT;->c(LX/ArJ;Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;)V

    .line 1718666
    :cond_0
    iget-object v0, p0, LX/ArB;->b:Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;

    const/4 p1, 0x1

    .line 1718667
    iget-object v1, v0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->E:Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;

    if-eqz v1, :cond_1

    iget-boolean v1, v0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ar:Z

    if-nez v1, :cond_1

    iget-object v1, v0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ai:LX/0ij;

    invoke-virtual {v1}, LX/0ij;->e()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->isInNuxMode()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1718668
    iget-object v1, v0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->E:Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;

    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/feedcta/InspirationNewsFeedCTAConfiguration;->getAppliedInspirations()LX/0Px;

    move-result-object v2

    .line 1718669
    if-eqz v2, :cond_1

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1718670
    :cond_1
    :goto_0
    iget-object v0, p0, LX/ArB;->b:Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;

    .line 1718671
    iget-object v2, v0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ah:LX/HvN;

    invoke-interface {v2}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v2}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->isLocationFetchAndRequeryingEffectsInProgress()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1718672
    iget-object v2, v0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ah:LX/HvN;

    invoke-interface {v2}, LX/0im;->c()LX/0jJ;

    move-result-object v2

    sget-object v3, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->G:LX/0jK;

    invoke-virtual {v2, v3}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v2

    check-cast v2, LX/0jL;

    iget-object v3, v0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ah:LX/HvN;

    invoke-interface {v3}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v3}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationState;)Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;

    move-result-object v3

    const/4 v1, 0x0

    invoke-virtual {v3, v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->setIsLocationFetchAndRequeryingEffectsInProgress(Z)Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;

    move-result-object v3

    const/4 v1, 0x0

    invoke-virtual {v3, v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->setLocation(Lcom/facebook/ipc/composer/model/ComposerLocation;)Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0jL;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationState;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0jL;

    invoke-virtual {v2}, LX/0jL;->a()V

    .line 1718673
    :cond_2
    return-void

    .line 1718674
    :cond_3
    iput-boolean p1, v0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ar:Z

    .line 1718675
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v1

    const/4 v3, 0x2

    if-ne v1, v3, :cond_4

    .line 1718676
    iget-object v1, v0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->Y:LX/Ass;

    const v2, 0x7f0827a7

    invoke-virtual {v1, v2}, LX/Ass;->a(I)V

    goto :goto_0

    .line 1718677
    :cond_4
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v1

    if-ne v1, p1, :cond_1

    .line 1718678
    iget-object v1, v0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->q:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/87V;

    .line 1718679
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;

    .line 1718680
    iget-object v3, v0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ai:LX/0ij;

    invoke-virtual {v3}, LX/0ij;->e()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v3

    invoke-virtual {v1, v3}, LX/87V;->d(LX/0is;)LX/0Px;

    move-result-object v1

    .line 1718681
    invoke-virtual {v2}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/87Q;->c(LX/0Px;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1718682
    iget-object v1, v0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->Y:LX/Ass;

    const v2, 0x7f0827a6

    invoke-virtual {v1, v2}, LX/Ass;->a(I)V

    goto/16 :goto_0
.end method
