.class public final LX/CRj;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 1892437
    const-class v1, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel;

    const v0, 0xb84e1c9

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x3

    const-string v5, "FBNearbyPlacesHereCardHugeResultCellQuery"

    const-string v6, "5b83be6958d42a93d0b6793e4b14bbde"

    const-string v7, "checkin_search_query"

    const-string v8, "10155156451501729"

    const/4 v9, 0x0

    .line 1892438
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 1892439
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 1892440
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1892445
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1892446
    sparse-switch v0, :sswitch_data_0

    .line 1892447
    :goto_0
    return-object p1

    .line 1892448
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 1892449
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 1892450
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 1892451
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 1892452
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 1892453
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    .line 1892454
    :sswitch_6
    const-string p1, "6"

    goto :goto_0

    .line 1892455
    :sswitch_7
    const-string p1, "7"

    goto :goto_0

    .line 1892456
    :sswitch_8
    const-string p1, "8"

    goto :goto_0

    .line 1892457
    :sswitch_9
    const-string p1, "9"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x5864c243 -> :sswitch_0
        -0x54eebaed -> :sswitch_2
        -0x295d97e2 -> :sswitch_1
        -0xa0d333f -> :sswitch_5
        0x683094a -> :sswitch_8
        0x13f7647d -> :sswitch_7
        0x32fb70cf -> :sswitch_3
        0x3cbbc973 -> :sswitch_6
        0x4a526274 -> :sswitch_9
        0x73a026b5 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1892441
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_1

    .line 1892442
    :goto_1
    return v0

    .line 1892443
    :pswitch_0
    const-string v2, "1"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    .line 1892444
    :pswitch_1
    invoke-static {p2}, LX/0wE;->b(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x31
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method
