.class public final LX/BTx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/BUL;

.field public final synthetic b:LX/BUA;


# direct methods
.method public constructor <init>(LX/BUA;LX/BUL;)V
    .locals 0

    .prologue
    .line 1788082
    iput-object p1, p0, LX/BTx;->b:LX/BUA;

    iput-object p2, p0, LX/BTx;->a:LX/BUL;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 11

    .prologue
    .line 1788083
    const/4 v9, 0x0

    .line 1788084
    iget-object v10, p0, LX/BTx;->b:LX/BUA;

    monitor-enter v10

    .line 1788085
    :try_start_0
    iget-object v2, p0, LX/BTx;->b:LX/BUA;

    iget-object v2, v2, LX/BUA;->e:LX/19w;

    iget-object v3, p0, LX/BTx;->a:LX/BUL;

    iget-object v3, v3, LX/BUL;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/19w;->i(Ljava/lang/String;)LX/7Jg;

    move-result-object v2

    .line 1788086
    if-eqz v2, :cond_3

    new-instance v3, Ljava/io/File;

    iget-object v4, v2, LX/7Jg;->h:Ljava/lang/String;

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1788087
    iget-object v3, v2, LX/7Jg;->l:LX/1A0;

    sget-object v4, LX/1A0;->DOWNLOAD_IN_PROGRESS:LX/1A0;

    if-eq v3, v4, :cond_2

    iget-object v3, v2, LX/7Jg;->l:LX/1A0;

    sget-object v4, LX/1A0;->DOWNLOAD_COMPLETED:LX/1A0;

    if-eq v3, v4, :cond_2

    .line 1788088
    iget-object v3, p0, LX/BTx;->b:LX/BUA;

    iget-object v4, v2, LX/7Jg;->a:Ljava/lang/String;

    sget-object v5, LX/1A0;->DOWNLOAD_NOT_STARTED:LX/1A0;

    invoke-static {v3, v4, v5}, LX/BUA;->a$redex0(LX/BUA;Ljava/lang/String;LX/1A0;)V

    .line 1788089
    iget-object v3, p0, LX/BTx;->b:LX/BUA;

    iget-object v3, v3, LX/BUA;->k:LX/19v;

    iget-object v4, v2, LX/7Jg;->a:Ljava/lang/String;

    sget-object v5, LX/7Jd;->DOWNLOAD_REQUESTED:LX/7Jd;

    invoke-virtual {v3, v4, v5}, LX/19v;->a(Ljava/lang/String;LX/7Jd;)V

    .line 1788090
    :goto_0
    iget-object v3, p0, LX/BTx;->a:LX/BUL;

    iget-boolean v3, v3, LX/BUL;->n:Z

    if-nez v3, :cond_a

    .line 1788091
    iget-object v3, p0, LX/BTx;->b:LX/BUA;

    iget-object v4, p0, LX/BTx;->a:LX/BUL;

    invoke-static {v3, v4}, LX/BUA;->b(LX/BUA;LX/BUL;)V

    .line 1788092
    :goto_1
    iget-object v3, v2, LX/7Jg;->n:LX/2ft;

    sget-object v4, LX/2ft;->WAIT_FOR_OFF_PEAK_DATA_HAPPY_HOUR:LX/2ft;

    if-ne v3, v4, :cond_b

    .line 1788093
    iget-object v3, p0, LX/BTx;->b:LX/BUA;

    .line 1788094
    iget-object v0, v2, LX/7Jg;->n:LX/2ft;

    sget-object v1, LX/2ft;->WAIT_FOR_OFF_PEAK_DATA_HAPPY_HOUR:LX/2ft;

    if-ne v0, v1, :cond_0

    .line 1788095
    iget-object v0, v3, LX/BUA;->u:LX/BYc;

    .line 1788096
    iget-boolean v1, v0, LX/BYc;->i:Z

    if-eqz v1, :cond_c

    iget-boolean v1, v0, LX/BYc;->j:Z

    if-eqz v1, :cond_c

    .line 1788097
    :cond_0
    :goto_2
    iget-object v2, p0, LX/BTx;->a:LX/BUL;

    iget-boolean v2, v2, LX/BUL;->n:Z

    if-nez v2, :cond_1

    .line 1788098
    iget-object v2, p0, LX/BTx;->b:LX/BUA;

    iget-object v2, v2, LX/BUA;->o:LX/15V;

    iget-object v3, p0, LX/BTx;->a:LX/BUL;

    iget-object v3, v3, LX/BUL;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/15V;->a(Ljava/lang/String;)V

    .line 1788099
    :cond_1
    iget-object v2, p0, LX/BTx;->b:LX/BUA;

    iget-object v3, p0, LX/BTx;->a:LX/BUL;

    iget-object v3, v3, LX/BUL;->b:Ljava/lang/String;

    invoke-static {v2, v3}, LX/BUA;->g(LX/BUA;Ljava/lang/String;)V

    .line 1788100
    monitor-exit v10
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1788101
    :goto_3
    return-object v9

    .line 1788102
    :cond_2
    :try_start_1
    monitor-exit v10

    goto :goto_3

    .line 1788103
    :catchall_0
    move-exception v2

    monitor-exit v10
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    .line 1788104
    :cond_3
    if-eqz v2, :cond_4

    .line 1788105
    :try_start_2
    iget-object v2, p0, LX/BTx;->b:LX/BUA;

    iget-object v2, v2, LX/BUA;->e:LX/19w;

    iget-object v3, p0, LX/BTx;->a:LX/BUL;

    iget-object v3, v3, LX/BUL;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/19w;->j(Ljava/lang/String;)Z

    .line 1788106
    :cond_4
    new-instance v2, LX/7Jg;

    iget-object v3, p0, LX/BTx;->a:LX/BUL;

    iget-object v3, v3, LX/BUL;->b:Ljava/lang/String;

    iget-object v4, p0, LX/BTx;->a:LX/BUL;

    iget-wide v4, v4, LX/BUL;->e:J

    iget-object v6, p0, LX/BTx;->a:LX/BUL;

    iget-object v6, v6, LX/BUL;->f:LX/2ft;

    iget-object v7, p0, LX/BTx;->a:LX/BUL;

    iget-object v7, v7, LX/BUL;->a:Landroid/net/Uri;

    iget-object v8, p0, LX/BTx;->a:LX/BUL;

    iget-boolean v8, v8, LX/BUL;->n:Z

    invoke-direct/range {v2 .. v8}, LX/7Jg;-><init>(Ljava/lang/String;JLX/2ft;Landroid/net/Uri;Z)V

    .line 1788107
    iget-object v3, p0, LX/BTx;->b:LX/BUA;

    iget-object v4, p0, LX/BTx;->a:LX/BUL;

    invoke-static {v3, v4, v2}, LX/BUA;->a$redex0(LX/BUA;LX/BUL;LX/7Jg;)V

    .line 1788108
    iget-object v3, p0, LX/BTx;->a:LX/BUL;

    iget-object v3, v3, LX/BUL;->o:Ljava/lang/String;

    if-eqz v3, :cond_5

    iget-object v3, v2, LX/7Jg;->i:Ljava/lang/String;

    if-nez v3, :cond_5

    .line 1788109
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Invalid format id specified for download"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1788110
    :cond_5
    iget-object v3, p0, LX/BTx;->b:LX/BUA;

    iget-object v3, v3, LX/BUA;->e:LX/19w;

    iget-object v4, p0, LX/BTx;->a:LX/BUL;

    iget-object v4, v4, LX/BUL;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, LX/19w;->h(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, LX/7Jg;->h:Ljava/lang/String;

    .line 1788111
    iget-object v3, v2, LX/7Jg;->c:Landroid/net/Uri;

    if-eqz v3, :cond_6

    .line 1788112
    iget-object v3, p0, LX/BTx;->b:LX/BUA;

    iget-object v3, v3, LX/BUA;->e:LX/19w;

    iget-object v4, p0, LX/BTx;->a:LX/BUL;

    iget-object v4, v4, LX/BUL;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, LX/19w;->h(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, LX/7Jg;->k:Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1788113
    :cond_6
    :try_start_3
    new-instance v3, Ljava/io/File;

    iget-object v4, v2, LX/7Jg;->h:Ljava/lang/String;

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1788114
    invoke-virtual {v3}, Ljava/io/File;->createNewFile()Z

    .line 1788115
    iget-object v3, v2, LX/7Jg;->k:Ljava/lang/String;

    if-eqz v3, :cond_7

    .line 1788116
    new-instance v3, Ljava/io/File;

    iget-object v4, v2, LX/7Jg;->k:Ljava/lang/String;

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->createNewFile()Z
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1788117
    :cond_7
    :goto_4
    :try_start_4
    iget-object v3, p0, LX/BTx;->b:LX/BUA;

    .line 1788118
    iget-object v0, v3, LX/BUA;->e:LX/19w;

    invoke-virtual {v0}, LX/19w;->r()Ljava/util/List;

    move-result-object v0

    .line 1788119
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Jg;

    .line 1788120
    sget-object v4, LX/7Jb;->CACHE_EVICTION:LX/7Jb;

    invoke-static {v3, v0, v4}, LX/BUA;->a(LX/BUA;LX/7Jg;LX/7Jb;)V

    goto :goto_5

    .line 1788121
    :cond_8
    iget-object v3, p0, LX/BTx;->b:LX/BUA;

    iget-object v4, p0, LX/BTx;->a:LX/BUL;

    iget-wide v4, v4, LX/BUL;->e:J

    iget-object v6, p0, LX/BTx;->a:LX/BUL;

    iget-object v6, v6, LX/BUL;->d:Ljava/lang/String;

    invoke-static {v3, v4, v5, v6}, LX/BUA;->a$redex0(LX/BUA;JLjava/lang/String;)V

    .line 1788122
    iget-object v3, p0, LX/BTx;->b:LX/BUA;

    iget-object v3, v3, LX/BUA;->e:LX/19w;

    iget-object v4, p0, LX/BTx;->a:LX/BUL;

    iget-object v4, v4, LX/BUL;->d:Ljava/lang/String;

    invoke-virtual {v3, v2, v4}, LX/19w;->a(LX/7Jg;Ljava/lang/String;)V

    .line 1788123
    iget-object v3, p0, LX/BTx;->b:LX/BUA;

    iget-object v3, v3, LX/BUA;->k:LX/19v;

    iget-object v4, v2, LX/7Jg;->a:Ljava/lang/String;

    sget-object v5, LX/7Jd;->DOWNLOAD_REQUESTED:LX/7Jd;

    invoke-virtual {v3, v4, v5}, LX/19v;->a(Ljava/lang/String;LX/7Jd;)V

    .line 1788124
    iget-object v3, p0, LX/BTx;->b:LX/BUA;

    iget-object v3, v3, LX/BUA;->k:LX/19v;

    iget-object v4, v2, LX/7Jg;->a:Ljava/lang/String;

    sget-object v5, LX/7Jd;->DOWNLOAD_QUEUED:LX/7Jd;

    invoke-virtual {v3, v4, v5}, LX/19v;->a(Ljava/lang/String;LX/7Jd;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 1788125
    :catch_0
    move-exception v3

    .line 1788126
    :try_start_5
    sget-object v4, LX/BUA;->d:Ljava/lang/String;

    const-string v5, "Exception adding download record %s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v8, v2, LX/7Jg;->a:Ljava/lang/String;

    aput-object v8, v6, v7

    invoke-static {v4, v3, v5, v6}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1788127
    iget-object v4, p0, LX/BTx;->b:LX/BUA;

    iget-object v4, v4, LX/BUA;->k:LX/19v;

    iget-object v5, v2, LX/7Jg;->a:Ljava/lang/String;

    instance-of v2, v3, LX/BTp;

    if-eqz v2, :cond_9

    move-object v0, v3

    check-cast v0, LX/BTp;

    move-object v2, v0

    iget-object v2, v2, LX/BTp;->mExceptionCode:LX/BTo;

    invoke-virtual {v2}, LX/BTo;->toString()Ljava/lang/String;

    move-result-object v2

    :goto_6
    invoke-virtual {v4, v5, v3, v2}, LX/19v;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)V

    .line 1788128
    iget-object v2, p0, LX/BTx;->b:LX/BUA;

    iget-object v2, v2, LX/BUA;->o:LX/15V;

    invoke-virtual {v2, v3}, LX/15V;->b(Ljava/lang/Throwable;)V

    .line 1788129
    iget-object v2, p0, LX/BTx;->b:LX/BUA;

    iget-object v3, p0, LX/BTx;->a:LX/BUL;

    iget-object v3, v3, LX/BUL;->b:Ljava/lang/String;

    invoke-static {v2, v3}, LX/BUA;->g(LX/BUA;Ljava/lang/String;)V

    .line 1788130
    monitor-exit v10

    goto/16 :goto_3

    .line 1788131
    :catch_1
    goto/16 :goto_4

    :cond_9
    move-object v2, v9

    .line 1788132
    goto :goto_6

    .line 1788133
    :cond_a
    iget-object v3, p0, LX/BTx;->b:LX/BUA;

    iget-object v3, v3, LX/BUA;->p:LX/BUi;

    iget-object v4, p0, LX/BTx;->a:LX/BUL;

    iget-object v4, v4, LX/BUL;->b:Ljava/lang/String;

    iget-object v5, p0, LX/BTx;->a:LX/BUL;

    iget-object v5, v5, LX/BUL;->k:Ljava/lang/String;

    iget-object v6, p0, LX/BTx;->a:LX/BUL;

    iget-object v6, v6, LX/BUL;->j:Ljava/lang/String;

    invoke-virtual {v3, v4, v5, v6}, LX/BUi;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1788134
    :cond_b
    iget-object v3, p0, LX/BTx;->b:LX/BUA;

    .line 1788135
    invoke-static {v3, v2}, LX/BUA;->a$redex0(LX/BUA;LX/7Jg;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 1788136
    goto/16 :goto_2

    .line 1788137
    :cond_c
    iget-object v1, v0, LX/BYc;->f:LX/7WS;

    new-instance v3, LX/BYb;

    iget-object v2, v0, LX/BYc;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {v3, v0, v2}, LX/BYb;-><init>(LX/BYc;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    invoke-virtual {v1, v3}, LX/7WS;->a(LX/0TF;)V

    goto/16 :goto_2
.end method
