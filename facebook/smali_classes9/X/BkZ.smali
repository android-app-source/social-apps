.class public final LX/BkZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Ljava/lang/Boolean;

.field public final synthetic b:LX/Bka;


# direct methods
.method public constructor <init>(LX/Bka;Ljava/lang/Boolean;)V
    .locals 0

    .prologue
    .line 1814522
    iput-object p1, p0, LX/BkZ;->b:LX/Bka;

    iput-object p2, p0, LX/BkZ;->a:Ljava/lang/Boolean;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x58cc045d

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1814523
    iget-object v1, p0, LX/BkZ;->a:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1814524
    iget-object v1, p0, LX/BkZ;->b:LX/Bka;

    iget-object v1, v1, LX/Bka;->s:LX/Bim;

    iget-object v2, p0, LX/BkZ;->b:LX/Bka;

    iget-object v2, v2, LX/Bka;->r:Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageEventCategoriesQueryModel$EventCategoryGroupsModel$NodesModel$CategoriesModel;

    invoke-virtual {v1, v2}, LX/Bim;->a(Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageEventCategoriesQueryModel$EventCategoryGroupsModel$NodesModel$CategoriesModel;)V

    .line 1814525
    :goto_0
    const v1, 0x35242017

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 1814526
    :cond_0
    iget-object v1, p0, LX/BkZ;->b:LX/Bka;

    const/4 p0, 0x1

    .line 1814527
    iget-object v2, v1, LX/Bka;->q:Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageEventCategoriesQueryModel$EventCategoryGroupsModel$NodesModel;

    invoke-virtual {v2}, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageEventCategoriesQueryModel$EventCategoryGroupsModel$NodesModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-ne v2, p0, :cond_1

    .line 1814528
    iget-object v2, v1, LX/Bka;->q:Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageEventCategoriesQueryModel$EventCategoryGroupsModel$NodesModel;

    invoke-virtual {v2}, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageEventCategoriesQueryModel$EventCategoryGroupsModel$NodesModel;->a()LX/0Px;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageEventCategoriesQueryModel$EventCategoryGroupsModel$NodesModel$CategoriesModel;

    iput-object v2, v1, LX/Bka;->r:Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageEventCategoriesQueryModel$EventCategoryGroupsModel$NodesModel$CategoriesModel;

    .line 1814529
    iget-object v2, v1, LX/Bka;->s:LX/Bim;

    iget-object v3, v1, LX/Bka;->r:Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageEventCategoriesQueryModel$EventCategoryGroupsModel$NodesModel$CategoriesModel;

    invoke-virtual {v2, v3}, LX/Bim;->a(Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageEventCategoriesQueryModel$EventCategoryGroupsModel$NodesModel$CategoriesModel;)V

    .line 1814530
    :goto_1
    goto :goto_0

    .line 1814531
    :cond_1
    iget-object v2, v1, LX/Bka;->t:LX/17Y;

    iget-object v3, v1, LX/Bka;->m:Landroid/content/Context;

    sget-object v4, LX/0ax;->cw:Ljava/lang/String;

    invoke-interface {v2, v3, v4}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    .line 1814532
    const-string v2, "extra_title_bar_content"

    iget-object v4, v1, LX/Bka;->q:Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageEventCategoriesQueryModel$EventCategoryGroupsModel$NodesModel;

    invoke-virtual {v4}, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageEventCategoriesQueryModel$EventCategoryGroupsModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1814533
    const-string v2, "extra_is_subcateory"

    invoke-virtual {v3, v2, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1814534
    const-string v2, "extra_category_group"

    iget-object v4, v1, LX/Bka;->q:Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageEventCategoriesQueryModel$EventCategoryGroupsModel$NodesModel;

    invoke-static {v3, v2, v4}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1814535
    iget-object v4, v1, LX/Bka;->u:Lcom/facebook/content/SecureContextHelper;

    const/16 p0, 0x6e

    iget-object v2, v1, LX/Bka;->m:Landroid/content/Context;

    const-class p1, Landroid/app/Activity;

    invoke-static {v2, p1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/Activity;

    invoke-interface {v4, v3, p0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    goto :goto_1
.end method
