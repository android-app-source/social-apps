.class public LX/AY1;
.super LX/AWT;
.source ""

# interfaces
.implements LX/1bG;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x13
.end annotation


# instance fields
.field public a:LX/AVT;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/AVR;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0So;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/Ac6;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/3HT;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final i:Lcom/facebook/facecast/FacecastPreviewView;

.field private final j:Landroid/view/View;

.field public final k:LX/AXy;

.field public l:LX/AVP;

.field public m:LX/AV1;

.field public n:Landroid/view/ScaleGestureDetector;

.field public o:LX/AY0;

.field private p:LX/AXz;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1684707
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/AY1;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1684708
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1684742
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/AY1;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1684743
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1684735
    invoke-direct {p0, p1, p2, p3}, LX/AWT;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1684736
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, LX/AY1;

    invoke-static {v0}, LX/AVT;->a(LX/0QB;)LX/AVT;

    move-result-object v3

    check-cast v3, LX/AVT;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    new-instance p2, LX/AVR;

    invoke-static {v0}, LX/0Zo;->a(LX/0QB;)LX/0Tf;

    move-result-object p1

    check-cast p1, Ljava/util/concurrent/ExecutorService;

    invoke-direct {p2, p1}, LX/AVR;-><init>(Ljava/util/concurrent/ExecutorService;)V

    move-object p1, p2

    check-cast p1, LX/AVR;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object p2

    check-cast p2, LX/0So;

    invoke-static {v0}, LX/Ac6;->b(LX/0QB;)LX/Ac6;

    move-result-object p3

    check-cast p3, LX/Ac6;

    invoke-static {v0}, LX/3HT;->a(LX/0QB;)LX/3HT;

    move-result-object v0

    check-cast v0, LX/3HT;

    iput-object v3, v2, LX/AY1;->a:LX/AVT;

    iput-object v4, v2, LX/AY1;->b:LX/03V;

    iput-object p1, v2, LX/AY1;->c:LX/AVR;

    iput-object p2, v2, LX/AY1;->f:LX/0So;

    iput-object p3, v2, LX/AY1;->g:LX/Ac6;

    iput-object v0, v2, LX/AY1;->h:LX/3HT;

    .line 1684737
    const v0, 0x7f0305e6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1684738
    const v0, 0x7f0d0f72

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecast/FacecastPreviewView;

    iput-object v0, p0, LX/AY1;->i:Lcom/facebook/facecast/FacecastPreviewView;

    .line 1684739
    const v0, 0x7f0d1043

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/AY1;->j:Landroid/view/View;

    .line 1684740
    new-instance v0, LX/AXy;

    invoke-direct {v0, p0}, LX/AXy;-><init>(LX/AY1;)V

    iput-object v0, p0, LX/AY1;->k:LX/AXy;

    .line 1684741
    return-void
.end method


# virtual methods
.method public final a(LX/AYp;LX/AYp;)V
    .locals 2

    .prologue
    .line 1684731
    sget-object v0, LX/AXv;->a:[I

    invoke-virtual {p1}, LX/AYp;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1684732
    :goto_0
    return-void

    .line 1684733
    :pswitch_0
    iget-object v0, p0, LX/AY1;->j:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 1684734
    :pswitch_1
    iget-object v0, p0, LX/AY1;->j:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getLoggingInfo()Ljava/util/Map;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1684716
    iget-object v0, p0, LX/AY1;->l:LX/AVP;

    if-eqz v0, :cond_1

    .line 1684717
    iget-object v0, p0, LX/AY1;->l:LX/AVP;

    .line 1684718
    iget-object v1, v0, LX/AVP;->K:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 1684719
    iget-object v1, v0, LX/AVP;->s:Ljava/util/List;

    if-eqz v1, :cond_2

    iget-object v1, v0, LX/AVP;->s:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_2

    .line 1684720
    iget-object v1, v0, LX/AVP;->K:Ljava/util/Map;

    const-string v2, "rendering_state"

    const-string v3, "preview"

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1684721
    :goto_0
    iget-object v1, v0, LX/AVP;->K:Ljava/util/Map;

    const-string v2, "rendering_fps"

    .line 1684722
    const/high16 v4, -0x40800000    # -1.0f

    .line 1684723
    iget-object v5, v0, LX/AVP;->y:LX/0So;

    invoke-interface {v5}, LX/0So;->now()J

    move-result-wide v6

    iget-wide v8, v0, LX/AVP;->G:J

    sub-long/2addr v6, v8

    .line 1684724
    iget v5, v0, LX/AVP;->F:I

    if-lez v5, :cond_0

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-lez v5, :cond_0

    .line 1684725
    iget v4, v0, LX/AVP;->F:I

    int-to-float v4, v4

    const/high16 v5, 0x447a0000    # 1000.0f

    mul-float/2addr v4, v5

    long-to-float v5, v6

    div-float/2addr v4, v5

    .line 1684726
    :cond_0
    move v3, v4

    .line 1684727
    invoke-static {v3}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1684728
    iget-object v1, v0, LX/AVP;->K:Ljava/util/Map;

    move-object v0, v1

    .line 1684729
    :goto_1
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 1684730
    :cond_2
    iget-object v1, v0, LX/AVP;->K:Ljava/util/Map;

    const-string v2, "rendering_state"

    const-string v3, "streaming"

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public setCameraZoomLevel(I)V
    .locals 3

    .prologue
    .line 1684711
    iget-object v0, p0, LX/AY1;->m:LX/AV1;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/AY1;->m:LX/AV1;

    .line 1684712
    iget-object v1, v0, LX/AV1;->r:Landroid/hardware/Camera;

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 1684713
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/AY1;->m:LX/AV1;

    invoke-virtual {v0}, LX/AV1;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1684714
    iget-object v0, p0, LX/AY1;->m:LX/AV1;

    const/4 v1, 0x4

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/AV1;->a(ILjava/lang/Object;)V

    .line 1684715
    :cond_0
    return-void

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setListener(LX/AXz;)V
    .locals 0

    .prologue
    .line 1684709
    iput-object p1, p0, LX/AY1;->p:LX/AXz;

    .line 1684710
    return-void
.end method
