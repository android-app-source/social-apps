.class public final LX/CBh;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/2mN;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLQuickPromotionFeedUnit;",
            ">;"
        }
    .end annotation
.end field

.field public b:Landroid/view/View$OnClickListener;

.field public c:LX/1Pn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public final synthetic d:LX/2mN;


# direct methods
.method public constructor <init>(LX/2mN;)V
    .locals 1

    .prologue
    .line 1856748
    iput-object p1, p0, LX/CBh;->d:LX/2mN;

    .line 1856749
    move-object v0, p1

    .line 1856750
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1856751
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1856730
    const-string v0, "QuickPromotionSocialContextLayoutComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1856731
    if-ne p0, p1, :cond_1

    .line 1856732
    :cond_0
    :goto_0
    return v0

    .line 1856733
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1856734
    goto :goto_0

    .line 1856735
    :cond_3
    check-cast p1, LX/CBh;

    .line 1856736
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1856737
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1856738
    if-eq v2, v3, :cond_0

    .line 1856739
    iget-object v2, p0, LX/CBh;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/CBh;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/CBh;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1856740
    goto :goto_0

    .line 1856741
    :cond_5
    iget-object v2, p1, LX/CBh;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 1856742
    :cond_6
    iget-object v2, p0, LX/CBh;->b:Landroid/view/View$OnClickListener;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/CBh;->b:Landroid/view/View$OnClickListener;

    iget-object v3, p1, LX/CBh;->b:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1856743
    goto :goto_0

    .line 1856744
    :cond_8
    iget-object v2, p1, LX/CBh;->b:Landroid/view/View$OnClickListener;

    if-nez v2, :cond_7

    .line 1856745
    :cond_9
    iget-object v2, p0, LX/CBh;->c:LX/1Pn;

    if-eqz v2, :cond_a

    iget-object v2, p0, LX/CBh;->c:LX/1Pn;

    iget-object v3, p1, LX/CBh;->c:LX/1Pn;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1856746
    goto :goto_0

    .line 1856747
    :cond_a
    iget-object v2, p1, LX/CBh;->c:LX/1Pn;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
