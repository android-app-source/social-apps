.class public LX/BHF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1AX;


# instance fields
.field private a:LX/1AY;


# direct methods
.method public constructor <init>(LX/1AY;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1768625
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1768626
    iput-object p1, p0, LX/BHF;->a:LX/1AY;

    .line 1768627
    return-void
.end method


# virtual methods
.method public final a(Ljava/util/Set;Z)Landroid/view/View;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V:",
            "Landroid/view/View;",
            ">(",
            "Ljava/util/Set",
            "<TV;>;Z)TV;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1768628
    invoke-interface {p1}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1768629
    :goto_0
    return-object v1

    .line 1768630
    :cond_0
    const/high16 v0, -0x80000000

    .line 1768631
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v2, v0

    move-object v3, v1

    :cond_1
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1768632
    iget-object v1, p0, LX/BHF;->a:LX/1AY;

    invoke-virtual {v1, v0}, LX/1AY;->a(Landroid/view/View;)LX/7zf;

    move-result-object v1

    .line 1768633
    sget-object v5, LX/7zf;->VISIBLE_FOR_AUTOPLAY:LX/7zf;

    if-ne v1, v5, :cond_1

    .line 1768634
    instance-of v1, v0, LX/BIO;

    if-eqz v1, :cond_4

    move-object v1, v0

    .line 1768635
    check-cast v1, LX/BIO;

    .line 1768636
    if-nez v3, :cond_2

    .line 1768637
    invoke-interface {v1}, LX/BIO;->getPlayPriority()I

    move-result v1

    move v2, v1

    move-object v3, v0

    goto :goto_1

    .line 1768638
    :cond_2
    invoke-interface {v1}, LX/BIO;->getPlayPriority()I

    move-result v5

    if-le v5, v2, :cond_4

    .line 1768639
    invoke-interface {v1}, LX/BIO;->getPlayPriority()I

    move-result v2

    move-object v1, v0

    move v0, v2

    :goto_2
    move v2, v0

    move-object v3, v1

    .line 1768640
    goto :goto_1

    :cond_3
    move-object v1, v3

    .line 1768641
    goto :goto_0

    :cond_4
    move v0, v2

    move-object v1, v3

    goto :goto_2
.end method
