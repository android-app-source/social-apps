.class public LX/AjI;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/contextual/ContextualResolver;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1707754
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1707755
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1707756
    iput-object v0, p0, LX/AjI;->a:LX/0Ot;

    .line 1707757
    return-void
.end method

.method public static b(LX/0QB;)LX/AjI;
    .locals 2

    .prologue
    .line 1707758
    new-instance v0, LX/AjI;

    invoke-direct {v0}, LX/AjI;-><init>()V

    .line 1707759
    const/16 v1, 0x45e

    invoke-static {p0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    .line 1707760
    iput-object v1, v0, LX/AjI;->a:LX/0Ot;

    .line 1707761
    return-object v0
.end method


# virtual methods
.method public final a()Z
    .locals 4

    .prologue
    .line 1707762
    iget-object v0, p0, LX/AjI;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0uf;

    sget-wide v2, LX/0X5;->jD:J

    invoke-virtual {v0, v2, v3}, LX/0uf;->a(J)LX/1jr;

    move-result-object v0

    .line 1707763
    const-string v1, "frequently_offline"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/1jr;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method
