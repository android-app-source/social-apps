.class public final enum LX/BVX;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/BVX;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/BVX;

.field public static final enum RATING:LX/BVX;


# instance fields
.field private final mValue:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1791215
    new-instance v0, LX/BVX;

    const-string v1, "RATING"

    const-string v2, "rating"

    invoke-direct {v0, v1, v3, v2}, LX/BVX;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BVX;->RATING:LX/BVX;

    .line 1791216
    const/4 v0, 0x1

    new-array v0, v0, [LX/BVX;

    sget-object v1, LX/BVX;->RATING:LX/BVX;

    aput-object v1, v0, v3

    sput-object v0, LX/BVX;->$VALUES:[LX/BVX;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1791217
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1791218
    iput-object p3, p0, LX/BVX;->mValue:Ljava/lang/String;

    .line 1791219
    return-void
.end method

.method public static from(Ljava/lang/String;)LX/BVX;
    .locals 4

    .prologue
    .line 1791220
    invoke-static {}, LX/BVX;->values()[LX/BVX;

    move-result-object v1

    .line 1791221
    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_1

    .line 1791222
    aget-object v2, v1, v0

    .line 1791223
    iget-object v3, v2, LX/BVX;->mValue:Ljava/lang/String;

    invoke-virtual {v3, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1791224
    return-object v2

    .line 1791225
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1791226
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unknown rating bar attribute = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/BVX;
    .locals 1

    .prologue
    .line 1791227
    const-class v0, LX/BVX;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/BVX;

    return-object v0
.end method

.method public static values()[LX/BVX;
    .locals 1

    .prologue
    .line 1791228
    sget-object v0, LX/BVX;->$VALUES:[LX/BVX;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/BVX;

    return-object v0
.end method
