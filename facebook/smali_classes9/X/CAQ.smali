.class public final LX/CAQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

.field public final synthetic b:LX/1Vt;


# direct methods
.method public constructor <init>(LX/1Vt;Lcom/facebook/graphql/model/GraphQLStoryActionLink;)V
    .locals 0

    .prologue
    .line 1855390
    iput-object p1, p0, LX/CAQ;->b:LX/1Vt;

    iput-object p2, p0, LX/CAQ;->a:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0x786ef909

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v3

    .line 1855391
    iget-object v0, p0, LX/CAQ;->a:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    if-nez v0, :cond_0

    .line 1855392
    iget-object v0, p0, LX/CAQ;->b:LX/1Vt;

    iget-object v0, v0, LX/1Vt;->a:LX/03V;

    const-string v1, "platform_cta_on_click_null_action_link"

    const-string v2, "actionLink was null in OnClickListener for Platform CTA"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1855393
    const v0, 0x67ab8fb4

    invoke-static {v4, v4, v0, v3}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1855394
    :goto_0
    return-void

    .line 1855395
    :cond_0
    iget-object v0, p0, LX/CAQ;->a:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->r()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1855396
    iget-object v0, p0, LX/CAQ;->a:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->r()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLApplication;->k()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 1855397
    :goto_1
    iget-object v0, p0, LX/CAQ;->b:LX/1Vt;

    iget-object v0, v0, LX/1Vt;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CAP;

    .line 1855398
    iget-object v4, v0, LX/CAP;->a:LX/0Zb;

    const-string v5, "platform_cta_click"

    invoke-static {v5}, LX/CAP;->c(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string v6, "app_id"

    invoke-virtual {v5, v6, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    invoke-interface {v4, v5}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1855399
    iget-object v0, p0, LX/CAQ;->b:LX/1Vt;

    iget-object v0, v0, LX/1Vt;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/82I;

    iget-object v1, p0, LX/CAQ;->a:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bc()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1, v2}, LX/82I;->a(Ljava/lang/String;Landroid/view/View;LX/0lF;)V

    .line 1855400
    const v0, 0x41d60064

    invoke-static {v0, v3}, LX/02F;->a(II)V

    goto :goto_0

    .line 1855401
    :cond_1
    iget-object v0, p0, LX/CAQ;->b:LX/1Vt;

    iget-object v0, v0, LX/1Vt;->a:LX/03V;

    const-string v1, "platform_cta_on_click_null_application"

    const-string v4, "application was null in OnClickListener for Platform CTA"

    invoke-virtual {v0, v1, v4}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v2

    goto :goto_1
.end method
