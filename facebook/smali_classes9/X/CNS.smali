.class public LX/CNS;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final c:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/CNb;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/CNT;

.field public d:LX/CNL;

.field public e:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "LX/CNb;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "LX/CNb;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "LX/CNb;",
            ">;"
        }
    .end annotation
.end field

.field private h:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1881757
    const-class v0, LX/CNS;

    sput-object v0, LX/CNS;->c:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/CNL;)V
    .locals 1

    .prologue
    .line 1881758
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1881759
    iput-object p1, p0, LX/CNS;->d:LX/CNL;

    .line 1881760
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1881761
    iput-object v0, p0, LX/CNS;->a:LX/0Px;

    .line 1881762
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/CNS;->e:Ljava/util/HashSet;

    .line 1881763
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/CNS;->f:Ljava/util/HashSet;

    .line 1881764
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/CNS;->g:Ljava/util/HashSet;

    .line 1881765
    return-void
.end method

.method public static a(LX/CNb;)LX/CNb;
    .locals 1

    .prologue
    .line 1881774
    const-string v0, "client_pp"

    invoke-virtual {p0, v0}, LX/CNb;->d(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CNV;

    .line 1881775
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, LX/CNV;->a()LX/CNb;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/CNS;LX/0Px;I)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/CNb;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 1881766
    new-instance v2, Ljava/util/ArrayList;

    iget-object v0, p0, LX/CNS;->a:LX/0Px;

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1881767
    invoke-virtual {v2, p2, p1}, Ljava/util/ArrayList;->addAll(ILjava/util/Collection;)Z

    .line 1881768
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CNb;

    .line 1881769
    iget-object v4, p0, LX/CNS;->d:LX/CNL;

    invoke-static {v0, v4}, LX/CNU;->a(LX/CNb;LX/CNL;)V

    .line 1881770
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1881771
    :cond_0
    iget-object v0, p0, LX/CNS;->e:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 1881772
    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/CNS;->a:LX/0Px;

    .line 1881773
    return-void
.end method

.method public static a(LX/CNS;LX/CNM;LX/CNb;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1881740
    invoke-virtual {p2, p3}, LX/CNb;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1881741
    :goto_0
    return-void

    .line 1881742
    :cond_0
    new-instance v1, LX/CNa;

    invoke-direct {v1}, LX/CNa;-><init>()V

    .line 1881743
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {p2}, LX/CNb;->b()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 1881744
    invoke-virtual {p2, v0}, LX/CNb;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1881745
    invoke-virtual {p2, v0}, LX/CNb;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v0}, LX/CNb;->b(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/CNa;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1881746
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1881747
    :cond_2
    invoke-virtual {p2, p3}, LX/CNb;->d(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    invoke-interface {p1, v0}, LX/CNM;->a(LX/0Px;)LX/0Px;

    move-result-object v0

    invoke-virtual {v1, p3, v0}, LX/CNa;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1881748
    invoke-static {p2}, LX/CNS;->a(LX/CNb;)LX/CNb;

    move-result-object v0

    .line 1881749
    if-eqz v0, :cond_3

    .line 1881750
    invoke-virtual {v1}, LX/CNa;->a()LX/CNb;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-static {p0, p2, v0}, LX/CNS;->a(LX/CNS;LX/CNb;LX/0Px;)V

    goto :goto_0

    .line 1881751
    :cond_3
    invoke-virtual {v1}, LX/CNa;->a()LX/CNb;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-direct {p0, p2, v0}, LX/CNS;->b(LX/CNb;LX/0Px;)V

    goto :goto_0
.end method

.method public static a(LX/CNS;LX/CNb;LX/0Px;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CNb;",
            "LX/0Px",
            "<",
            "LX/CNb;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1881776
    invoke-static {p1}, LX/CNS;->a(LX/CNb;)LX/CNb;

    move-result-object v0

    .line 1881777
    if-eqz v0, :cond_0

    .line 1881778
    invoke-static {p1}, LX/CNS;->b(LX/CNb;)Ljava/lang/String;

    move-result-object v1

    .line 1881779
    new-instance v2, LX/CNR;

    invoke-direct {v2, p0, p1, p2}, LX/CNR;-><init>(LX/CNS;LX/CNb;LX/0Px;)V

    invoke-static {p0, v2, v0, v1}, LX/CNS;->a(LX/CNS;LX/CNM;LX/CNb;Ljava/lang/String;)V

    .line 1881780
    :goto_0
    return-void

    .line 1881781
    :cond_0
    invoke-direct {p0, p1, p2}, LX/CNS;->b(LX/CNb;LX/0Px;)V

    goto :goto_0
.end method

.method public static a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1881752
    instance-of v0, p0, LX/0Px;

    if-eqz v0, :cond_0

    move-object v0, p0

    check-cast v0, LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    check-cast p0, LX/0Px;

    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, LX/CNb;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public static b(LX/CNb;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1881753
    const-string v0, "client_pp"

    invoke-virtual {p0, v0}, LX/CNb;->d(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CNV;

    .line 1881754
    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    .line 1881755
    :cond_0
    iget-object p0, v0, LX/CNV;->b:Ljava/lang/String;

    move-object v0, p0

    .line 1881756
    goto :goto_0
.end method

.method private b(LX/CNb;LX/0Px;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CNb;",
            "LX/0Px",
            "<",
            "LX/CNb;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1881642
    iget-object v0, p0, LX/CNS;->a:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 1881643
    const/4 v1, -0x1

    if-eq v0, v1, :cond_4

    .line 1881644
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1881645
    if-lez v0, :cond_0

    .line 1881646
    iget-object v1, p0, LX/CNS;->a:LX/0Px;

    invoke-virtual {v1, v2, v0}, LX/0Px;->subList(II)LX/0Px;

    move-result-object v1

    invoke-virtual {v3, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 1881647
    :cond_0
    invoke-virtual {v3, p2}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 1881648
    iget-object v1, p0, LX/CNS;->a:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_1

    .line 1881649
    iget-object v1, p0, LX/CNS;->a:LX/0Px;

    add-int/lit8 v0, v0, 0x1

    iget-object v4, p0, LX/CNS;->a:LX/0Px;

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v4

    invoke-virtual {v1, v0, v4}, LX/0Px;->subList(II)LX/0Px;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 1881650
    :cond_1
    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v4

    move v1, v2

    :goto_0
    if-ge v1, v4, :cond_3

    invoke-virtual {p2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CNb;

    .line 1881651
    iget-object v5, p0, LX/CNS;->d:LX/CNL;

    invoke-static {v0, v5}, LX/CNU;->a(LX/CNb;LX/CNL;)V

    .line 1881652
    invoke-virtual {p2, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    if-ne v0, v5, :cond_2

    .line 1881653
    iget-object v5, p0, LX/CNS;->f:Ljava/util/HashSet;

    invoke-virtual {v5, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1881654
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1881655
    :cond_2
    iget-object v5, p0, LX/CNS;->e:Ljava/util/HashSet;

    invoke-virtual {v5, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1881656
    :cond_3
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/CNS;->a:LX/0Px;

    .line 1881657
    :cond_4
    return-void
.end method

.method private c()V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 1881626
    const-string v1, "%s.removeAllTemplates"

    sget-object v2, LX/CNS;->c:Ljava/lang/Class;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    const v3, -0x5c04ab07

    invoke-static {v1, v2, v3}, LX/02m;->a(Ljava/lang/String;Ljava/lang/Object;I)V

    .line 1881627
    :try_start_0
    iget-object v1, p0, LX/CNS;->a:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v3, p0, LX/CNS;->a:LX/0Px;

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1881628
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1881629
    iget-object v1, p0, LX/CNS;->a:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v3

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_2

    iget-object v0, p0, LX/CNS;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CNb;

    .line 1881630
    iget-object v4, p0, LX/CNS;->e:Ljava/util/HashSet;

    invoke-virtual {v4, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 1881631
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1881632
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1881633
    :cond_2
    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/CNS;->a:LX/0Px;

    .line 1881634
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/CNS;->e:Ljava/util/HashSet;

    .line 1881635
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/CNS;->f:Ljava/util/HashSet;

    .line 1881636
    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p0, LX/CNS;->a:LX/0Px;

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, LX/CNS;->g:Ljava/util/HashSet;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1881637
    const v0, 0x3dbf99fc

    invoke-static {v0}, LX/02m;->a(I)V

    .line 1881638
    return-void

    .line 1881639
    :catchall_0
    move-exception v0

    const v1, 0x3b9addcf

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1881640
    iget v0, p0, LX/CNS;->h:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/CNS;->h:I

    .line 1881641
    return-void
.end method

.method public final a(LX/0Px;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/CNb;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1881658
    const-string v0, "%s.initializeWithTemplates"

    sget-object v1, LX/CNS;->c:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const v2, 0x5cb79f51

    invoke-static {v0, v1, v2}, LX/02m;->a(Ljava/lang/String;Ljava/lang/Object;I)V

    .line 1881659
    :try_start_0
    invoke-virtual {p0}, LX/CNS;->a()V

    .line 1881660
    invoke-direct {p0}, LX/CNS;->c()V

    .line 1881661
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, LX/CNS;->a:LX/0Px;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1881662
    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Ljava/util/ArrayList;->addAll(ILjava/util/Collection;)Z

    .line 1881663
    iget-object v1, p0, LX/CNS;->e:Ljava/util/HashSet;

    invoke-virtual {v1, p1}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 1881664
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/CNS;->a:LX/0Px;

    .line 1881665
    invoke-virtual {p0}, LX/CNS;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1881666
    const v0, 0x3073f1ef

    invoke-static {v0}, LX/02m;->a(I)V

    .line 1881667
    return-void

    .line 1881668
    :catchall_0
    move-exception v0

    const v1, 0x29e3c5f2

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final a(LX/0Px;LX/0Px;LX/CNb;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/Object;",
            ">;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/CNb;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1881669
    if-eqz p3, :cond_0

    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v1

    if-eq v0, v1, :cond_1

    .line 1881670
    :cond_0
    :goto_0
    return-void

    .line 1881671
    :cond_1
    new-instance v4, LX/CNa;

    invoke-direct {v4}, LX/CNa;-><init>()V

    .line 1881672
    invoke-static {p3}, LX/3jU;->a(LX/CNb;)LX/3j9;

    move-result-object v0

    .line 1881673
    invoke-virtual {v0, p3}, LX/3j9;->a(LX/CNb;)Ljava/util/Set;

    move-result-object v5

    move v1, v2

    .line 1881674
    :goto_1
    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 1881675
    invoke-virtual {p2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1881676
    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v4, v0, v3}, LX/CNa;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1881677
    invoke-virtual {p3, v0}, LX/CNb;->c(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v5, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 1881678
    invoke-virtual {p3, v0}, LX/CNb;->d(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 1881679
    invoke-static {v0}, LX/CNS;->a(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1881680
    check-cast v0, LX/0Px;

    move v3, v2

    .line 1881681
    :goto_2
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v6

    if-ge v3, v6, :cond_2

    .line 1881682
    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    .line 1881683
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 1881684
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1881685
    :cond_3
    :goto_3
    invoke-virtual {p3}, LX/CNb;->b()I

    move-result v0

    if-ge v2, v0, :cond_5

    .line 1881686
    invoke-virtual {p3, v2}, LX/CNb;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1881687
    invoke-virtual {p3, v2}, LX/CNb;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v2}, LX/CNb;->b(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, LX/CNa;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1881688
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 1881689
    :cond_5
    invoke-virtual {v4}, LX/CNa;->a()LX/CNb;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-static {p3}, LX/CNS;->a(LX/CNb;)LX/CNb;

    move-result-object v1

    invoke-static {p3}, LX/CNS;->b(LX/CNb;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/CNU;->a(LX/0Px;LX/CNb;Ljava/lang/String;)LX/0Px;

    move-result-object v0

    .line 1881690
    invoke-static {p0, p3, v0}, LX/CNS;->a(LX/CNS;LX/CNb;LX/0Px;)V

    goto :goto_0
.end method

.method public final b()V
    .locals 11

    .prologue
    const/4 v2, 0x0

    .line 1881691
    const-string v0, "%s.endUpdates"

    sget-object v1, LX/CNS;->c:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const v3, 0x780e2712

    invoke-static {v0, v1, v3}, LX/02m;->a(Ljava/lang/String;Ljava/lang/Object;I)V

    .line 1881692
    :try_start_0
    iget v0, p0, LX/CNS;->h:I

    if-lez v0, :cond_0

    .line 1881693
    iget v0, p0, LX/CNS;->h:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/CNS;->h:I

    .line 1881694
    iget v0, p0, LX/CNS;->h:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-lez v0, :cond_0

    .line 1881695
    const v0, -0x186c7b76

    invoke-static {v0}, LX/02m;->a(I)V

    .line 1881696
    :goto_0
    return-void

    .line 1881697
    :cond_0
    :try_start_1
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 1881698
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 1881699
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v6

    .line 1881700
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v7

    .line 1881701
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v8

    .line 1881702
    iget-object v0, p0, LX/CNS;->g:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1881703
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v9

    move v3, v2

    move v1, v2

    .line 1881704
    :goto_1
    iget-object v0, p0, LX/CNS;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v3, v0, :cond_2

    .line 1881705
    iget-object v0, p0, LX/CNS;->a:LX/0Px;

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CNb;

    .line 1881706
    iget-object v10, p0, LX/CNS;->g:Ljava/util/HashSet;

    invoke-virtual {v10, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 1881707
    sub-int v0, v3, v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move v0, v1

    .line 1881708
    :goto_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_1

    .line 1881709
    :cond_1
    invoke-virtual {v9, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1881710
    iget-object v10, p0, LX/CNS;->e:Ljava/util/HashSet;

    invoke-virtual {v10, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1881711
    add-int/lit8 v0, v1, 0x1

    goto :goto_2

    .line 1881712
    :cond_2
    invoke-virtual {v9}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/CNS;->a:LX/0Px;

    :cond_3
    move v1, v2

    .line 1881713
    :goto_3
    iget-object v0, p0, LX/CNS;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_6

    .line 1881714
    iget-object v0, p0, LX/CNS;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CNb;

    .line 1881715
    iget-object v2, p0, LX/CNS;->e:Ljava/util/HashSet;

    invoke-virtual {v2, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1881716
    invoke-virtual {v7, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1881717
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v8, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1881718
    :cond_4
    :goto_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 1881719
    :cond_5
    iget-object v2, p0, LX/CNS;->f:Ljava/util/HashSet;

    invoke-virtual {v2, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1881720
    invoke-virtual {v5, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1881721
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v6, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_4

    .line 1881722
    :catchall_0
    move-exception v0

    const v1, 0x76a25502

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 1881723
    :cond_6
    :try_start_2
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 1881724
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    .line 1881725
    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v5

    .line 1881726
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    .line 1881727
    invoke-virtual {v8}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    .line 1881728
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v0

    if-nez v0, :cond_7

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v0

    if-nez v0, :cond_7

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v0

    if-eqz v0, :cond_8

    .line 1881729
    :cond_7
    iget-object v0, p0, LX/CNS;->b:LX/CNT;

    if-eqz v0, :cond_8

    .line 1881730
    iget-object v0, p0, LX/CNS;->b:LX/CNT;

    invoke-interface/range {v0 .. v5}, LX/CNT;->a(LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;)V

    .line 1881731
    :cond_8
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/CNS;->e:Ljava/util/HashSet;

    .line 1881732
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/CNS;->f:Ljava/util/HashSet;

    .line 1881733
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/CNS;->g:Ljava/util/HashSet;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1881734
    const v0, 0x35e9262f

    invoke-static {v0}, LX/02m;->a(I)V

    goto/16 :goto_0

    :cond_9
    move v0, v1

    goto/16 :goto_2
.end method

.method public final e(LX/0Px;Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/CNb;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1881735
    iget-object v0, p0, LX/CNS;->d:LX/CNL;

    invoke-virtual {v0, p2}, LX/CNL;->a(Ljava/lang/String;)LX/CNb;

    move-result-object v0

    .line 1881736
    if-nez v0, :cond_0

    .line 1881737
    :goto_0
    return-void

    .line 1881738
    :cond_0
    invoke-static {v0}, LX/CNS;->a(LX/CNb;)LX/CNb;

    move-result-object v1

    invoke-static {v0}, LX/CNS;->b(LX/CNb;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v1, v2}, LX/CNU;->a(LX/0Px;LX/CNb;Ljava/lang/String;)LX/0Px;

    move-result-object v1

    .line 1881739
    invoke-static {p0, v0, v1}, LX/CNS;->a(LX/CNS;LX/CNb;LX/0Px;)V

    goto :goto_0
.end method
