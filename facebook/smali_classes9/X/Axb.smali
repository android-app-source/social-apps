.class public LX/Axb;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/BOq;

.field private final b:LX/BOy;

.field private final c:LX/BOs;

.field private final d:LX/BOy;

.field private final e:LX/0ad;

.field private final f:LX/Axy;

.field private final g:LX/0SG;

.field private final h:Lcom/facebook/storyteller/ImageSimilarity4a;


# direct methods
.method public constructor <init>(LX/0ad;LX/Axy;LX/0SG;Lcom/facebook/storyteller/ImageSimilarity4a;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1728626
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1728627
    new-instance v0, LX/BOq;

    invoke-direct {v0}, LX/BOq;-><init>()V

    iput-object v0, p0, LX/Axb;->a:LX/BOq;

    .line 1728628
    new-instance v0, LX/BOy;

    invoke-direct {v0}, LX/BOy;-><init>()V

    iput-object v0, p0, LX/Axb;->b:LX/BOy;

    .line 1728629
    new-instance v0, LX/BOs;

    invoke-direct {v0}, LX/BOs;-><init>()V

    iput-object v0, p0, LX/Axb;->c:LX/BOs;

    .line 1728630
    new-instance v0, LX/BOy;

    invoke-direct {v0}, LX/BOy;-><init>()V

    iput-object v0, p0, LX/Axb;->d:LX/BOy;

    .line 1728631
    iput-object p1, p0, LX/Axb;->e:LX/0ad;

    .line 1728632
    iput-object p2, p0, LX/Axb;->f:LX/Axy;

    .line 1728633
    iput-object p3, p0, LX/Axb;->g:LX/0SG;

    .line 1728634
    iput-object p4, p0, LX/Axb;->h:Lcom/facebook/storyteller/ImageSimilarity4a;

    .line 1728635
    return-void
.end method

.method public static a(LX/Axb;LX/BOy;LX/Ay4;)LX/Ay6;
    .locals 11

    .prologue
    const-wide v8, 0x408f400000000000L    # 1000.0

    const/4 v1, 0x0

    .line 1728636
    new-instance v3, LX/Ay6;

    invoke-direct {v3}, LX/Ay6;-><init>()V

    .line 1728637
    iget-object v0, p0, LX/Axb;->c:LX/BOs;

    invoke-virtual {p1, v0}, LX/BOy;->a(LX/0eW;)LX/0eW;

    .line 1728638
    iget-object v0, p0, LX/Axb;->c:LX/BOs;

    invoke-virtual {v0}, LX/BOs;->a()I

    move-result v4

    .line 1728639
    if-lez v4, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    move v2, v1

    .line 1728640
    :goto_1
    if-ge v2, v4, :cond_3

    .line 1728641
    iget-object v0, p0, LX/Axb;->c:LX/BOs;

    iget-object v5, p0, LX/Axb;->d:LX/BOy;

    invoke-virtual {v0, v5, v2}, LX/BOs;->a(LX/BOy;I)LX/BOy;

    .line 1728642
    iget-object v0, p0, LX/Axb;->d:LX/BOy;

    invoke-virtual {v0}, LX/BOy;->d()B

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1728643
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const/4 v1, 0x4

    iget-object v2, p0, LX/Axb;->d:LX/BOy;

    invoke-virtual {v2}, LX/BOy;->d()B

    move-result v2

    invoke-static {v1, v2}, LX/Axb;->a(BB)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v0, v1

    .line 1728644
    goto :goto_0

    .line 1728645
    :pswitch_0
    iget-object v0, p0, LX/Axb;->e:LX/0ad;

    sget-short v5, LX/1kO;->x:S

    invoke-interface {v0, v5, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1728646
    iget-object v0, p0, LX/Axb;->d:LX/BOy;

    invoke-direct {p0, v0}, LX/Axb;->e(LX/BOy;)Lcom/facebook/friendsharing/souvenirs/models/SouvenirPhotoItem;

    move-result-object v0

    .line 1728647
    :goto_2
    if-nez v2, :cond_1

    .line 1728648
    iget-object v5, p0, LX/Axb;->d:LX/BOy;

    invoke-virtual {v5}, LX/BOy;->b()D

    move-result-wide v6

    mul-double/2addr v6, v8

    double-to-long v6, v6

    .line 1728649
    iput-wide v6, p2, LX/Ay4;->c:J

    .line 1728650
    :cond_1
    invoke-virtual {v3, v0}, LX/Ay6;->a(Lcom/facebook/friendsharing/souvenirs/models/SouvenirItem;)LX/Ay6;

    .line 1728651
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 1728652
    :cond_2
    iget-object v0, p0, LX/Axb;->d:LX/BOy;

    invoke-direct {p0, v0}, LX/Axb;->d(LX/BOy;)Lcom/facebook/friendsharing/souvenirs/models/SouvenirBurstItem;

    move-result-object v0

    goto :goto_2

    .line 1728653
    :pswitch_1
    iget-object v0, p0, LX/Axb;->d:LX/BOy;

    invoke-direct {p0, v0}, LX/Axb;->f(LX/BOy;)Lcom/facebook/friendsharing/souvenirs/models/SouvenirPhotoItem;

    move-result-object v0

    goto :goto_2

    .line 1728654
    :pswitch_2
    iget-object v0, p0, LX/Axb;->d:LX/BOy;

    invoke-direct {p0, v0}, LX/Axb;->g(LX/BOy;)Lcom/facebook/friendsharing/souvenirs/models/SouvenirVideoItem;

    move-result-object v0

    goto :goto_2

    .line 1728655
    :cond_3
    iget-object v0, p0, LX/Axb;->d:LX/BOy;

    invoke-virtual {v0}, LX/BOy;->b()D

    move-result-wide v0

    mul-double/2addr v0, v8

    double-to-long v0, v0

    .line 1728656
    iput-wide v0, p2, LX/Ay4;->d:J

    .line 1728657
    invoke-virtual {p2}, LX/Ay4;->a()Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;

    move-result-object v0

    .line 1728658
    iput-object v0, v3, LX/Ay6;->b:Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;

    .line 1728659
    return-object v3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method private static a(Lcom/facebook/friendsharing/souvenirs/util/SouvenirLoggingParameters;)Lcom/facebook/friendsharing/souvenirclassifier/models/AssetCounts;
    .locals 11

    .prologue
    const/4 v5, 0x0

    .line 1728660
    new-instance v0, Lcom/facebook/friendsharing/souvenirclassifier/models/AssetCounts;

    iget v1, p0, Lcom/facebook/friendsharing/souvenirs/util/SouvenirLoggingParameters;->d:I

    iget v2, p0, Lcom/facebook/friendsharing/souvenirs/util/SouvenirLoggingParameters;->e:I

    iget v3, p0, Lcom/facebook/friendsharing/souvenirs/util/SouvenirLoggingParameters;->a:I

    iget v4, p0, Lcom/facebook/friendsharing/souvenirs/util/SouvenirLoggingParameters;->c:I

    iget v6, p0, Lcom/facebook/friendsharing/souvenirs/util/SouvenirLoggingParameters;->b:I

    iget v7, p0, Lcom/facebook/friendsharing/souvenirs/util/SouvenirLoggingParameters;->f:I

    move v8, v5

    move v9, v5

    move v10, v5

    invoke-direct/range {v0 .. v10}, Lcom/facebook/friendsharing/souvenirclassifier/models/AssetCounts;-><init>(IIIIIIIIII)V

    return-object v0
.end method

.method private static a(Lcom/facebook/friendsharing/souvenirs/models/SouvenirPhotoItem;)Lcom/facebook/friendsharing/souvenirclassifier/models/AssetDetail;
    .locals 28
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1728665
    invoke-static/range {p0 .. p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1728666
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirPhotoItem;->a()Lcom/facebook/ipc/media/data/LocalMediaData;

    move-result-object v8

    .line 1728667
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirPhotoItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v14

    .line 1728668
    new-instance v4, Lcom/facebook/friendsharing/souvenirclassifier/models/AssetDetail;

    invoke-virtual {v14}, Lcom/facebook/ipc/media/data/MediaData;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirPhotoItem;->iq_()LX/Ay0;

    move-result-object v6

    invoke-virtual {v6}, LX/Ay0;->toString()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/util/Date;

    invoke-virtual {v8}, Lcom/facebook/ipc/media/data/LocalMediaData;->a()J

    move-result-wide v8

    invoke-direct {v7, v8, v9}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v14}, Lcom/facebook/ipc/media/data/MediaData;->f()I

    move-result v8

    int-to-double v8, v8

    invoke-virtual {v14}, Lcom/facebook/ipc/media/data/MediaData;->g()I

    move-result v10

    int-to-double v10, v10

    invoke-virtual {v14}, Lcom/facebook/ipc/media/data/MediaData;->i()D

    move-result-wide v12

    invoke-virtual {v14}, Lcom/facebook/ipc/media/data/MediaData;->j()D

    move-result-wide v14

    const-wide/16 v16, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    new-instance v23, Lcom/facebook/friendsharing/souvenirclassifier/models/OptionalImageMetricValue;

    const/16 v24, 0x0

    const-wide/16 v26, 0x0

    move-object/from16 v0, v23

    move/from16 v1, v24

    move-wide/from16 v2, v26

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/friendsharing/souvenirclassifier/models/OptionalImageMetricValue;-><init>(ZD)V

    new-instance v24, Lcom/facebook/friendsharing/souvenirclassifier/models/OptionalImageMetricValue;

    const/16 v25, 0x0

    const-wide/16 v26, 0x0

    invoke-direct/range {v24 .. v27}, Lcom/facebook/friendsharing/souvenirclassifier/models/OptionalImageMetricValue;-><init>(ZD)V

    invoke-direct/range {v4 .. v24}, Lcom/facebook/friendsharing/souvenirclassifier/models/AssetDetail;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;DDDDDLjava/util/Date;IIIILcom/facebook/friendsharing/souvenirclassifier/models/OptionalImageMetricValue;Lcom/facebook/friendsharing/souvenirclassifier/models/OptionalImageMetricValue;)V

    return-object v4
.end method

.method private static a(Lcom/facebook/friendsharing/souvenirs/models/SouvenirVideoItem;)Lcom/facebook/friendsharing/souvenirclassifier/models/AssetDetail;
    .locals 28
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1728661
    invoke-static/range {p0 .. p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1728662
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirVideoItem;->a()Lcom/facebook/ipc/media/data/LocalMediaData;

    move-result-object v8

    .line 1728663
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirVideoItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v14

    .line 1728664
    new-instance v4, Lcom/facebook/friendsharing/souvenirclassifier/models/AssetDetail;

    invoke-virtual {v14}, Lcom/facebook/ipc/media/data/MediaData;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirVideoItem;->iq_()LX/Ay0;

    move-result-object v6

    invoke-virtual {v6}, LX/Ay0;->toString()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/util/Date;

    invoke-virtual {v8}, Lcom/facebook/ipc/media/data/LocalMediaData;->a()J

    move-result-wide v8

    invoke-direct {v7, v8, v9}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v14}, Lcom/facebook/ipc/media/data/MediaData;->f()I

    move-result v8

    int-to-double v8, v8

    invoke-virtual {v14}, Lcom/facebook/ipc/media/data/MediaData;->g()I

    move-result v10

    int-to-double v10, v10

    invoke-virtual {v14}, Lcom/facebook/ipc/media/data/MediaData;->i()D

    move-result-wide v12

    invoke-virtual {v14}, Lcom/facebook/ipc/media/data/MediaData;->j()D

    move-result-wide v14

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirVideoItem;->d()J

    move-result-wide v16

    move-wide/from16 v0, v16

    long-to-double v0, v0

    move-wide/from16 v16, v0

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    new-instance v23, Lcom/facebook/friendsharing/souvenirclassifier/models/OptionalImageMetricValue;

    const/16 v24, 0x0

    const-wide/16 v26, 0x0

    move-object/from16 v0, v23

    move/from16 v1, v24

    move-wide/from16 v2, v26

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/friendsharing/souvenirclassifier/models/OptionalImageMetricValue;-><init>(ZD)V

    new-instance v24, Lcom/facebook/friendsharing/souvenirclassifier/models/OptionalImageMetricValue;

    const/16 v25, 0x0

    const-wide/16 v26, 0x0

    invoke-direct/range {v24 .. v27}, Lcom/facebook/friendsharing/souvenirclassifier/models/OptionalImageMetricValue;-><init>(ZD)V

    invoke-direct/range {v4 .. v24}, Lcom/facebook/friendsharing/souvenirclassifier/models/AssetDetail;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;DDDDDLjava/util/Date;IIIILcom/facebook/friendsharing/souvenirclassifier/models/OptionalImageMetricValue;Lcom/facebook/friendsharing/souvenirclassifier/models/OptionalImageMetricValue;)V

    return-object v4
.end method

.method private static a(BB)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1728676
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 1728677
    invoke-static {p0}, LX/BOp;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1728678
    const-string v1, " should not contain a "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1728679
    invoke-static {p1}, LX/BOp;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1728680
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/String;B)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1728669
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 1728670
    const-string v1, "Attempted to construct a "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1728671
    invoke-virtual {v0, p0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1728672
    const-string v1, " from a StoryTellerItem with a "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1728673
    invoke-static {p1}, LX/BOp;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1728674
    const-string v1, " asset"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1728675
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(LX/Axb;LX/BOs;)[LX/BOy;
    .locals 4
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1728611
    invoke-virtual {p1}, LX/BOs;->a()I

    move-result v1

    .line 1728612
    new-array v2, v1, [LX/BOy;

    .line 1728613
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 1728614
    new-instance v3, LX/BOy;

    invoke-direct {v3}, LX/BOy;-><init>()V

    invoke-virtual {p1, v3, v0}, LX/BOs;->a(LX/BOy;I)LX/BOy;

    move-result-object v3

    move-object v3, v3

    .line 1728615
    aput-object v3, v2, v0

    .line 1728616
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1728617
    :cond_0
    new-instance v0, LX/Axa;

    invoke-direct {v0, p0}, LX/Axa;-><init>(LX/Axb;)V

    invoke-static {v2, v0}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 1728618
    return-object v2
.end method

.method public static c(LX/BOy;)LX/Ay4;
    .locals 3

    .prologue
    .line 1728619
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1728620
    invoke-virtual {p0}, LX/BOy;->d()B

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-class v1, Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, LX/BOy;->d()B

    move-result v2

    invoke-static {v1, v2}, LX/Axb;->a(Ljava/lang/String;B)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1728621
    new-instance v0, LX/Ay4;

    invoke-direct {v0}, LX/Ay4;-><init>()V

    .line 1728622
    invoke-virtual {p0}, LX/BOy;->a()Ljava/lang/String;

    move-result-object v1

    .line 1728623
    iput-object v1, v0, LX/Ay4;->a:Ljava/lang/String;

    .line 1728624
    return-object v0

    .line 1728625
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d(LX/BOy;)Lcom/facebook/friendsharing/souvenirs/models/SouvenirBurstItem;
    .locals 8
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v7, 0x3

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1728596
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1728597
    invoke-virtual {p1}, LX/BOy;->d()B

    move-result v0

    if-ne v0, v7, :cond_0

    move v0, v1

    :goto_0
    const-class v3, Lcom/facebook/friendsharing/souvenirs/models/SouvenirBurstItem;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, LX/BOy;->d()B

    move-result v4

    invoke-static {v3, v4}, LX/Axb;->a(Ljava/lang/String;B)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1728598
    iget-object v0, p0, LX/Axb;->a:LX/BOq;

    invoke-virtual {p1, v0}, LX/BOy;->a(LX/0eW;)LX/0eW;

    .line 1728599
    iget-object v0, p0, LX/Axb;->a:LX/BOq;

    invoke-virtual {v0}, LX/BOq;->b()I

    move-result v4

    .line 1728600
    if-le v4, v1, :cond_1

    move v0, v1

    :goto_1
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "No assets contained in burst "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, LX/BOy;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1728601
    new-instance v5, LX/0Pz;

    invoke-direct {v5}, LX/0Pz;-><init>()V

    move v3, v2

    .line 1728602
    :goto_2
    if-ge v3, v4, :cond_3

    .line 1728603
    iget-object v0, p0, LX/Axb;->a:LX/BOq;

    iget-object v6, p0, LX/Axb;->b:LX/BOy;

    invoke-virtual {v0, v6, v3}, LX/BOq;->a(LX/BOy;I)LX/BOy;

    .line 1728604
    iget-object v0, p0, LX/Axb;->b:LX/BOy;

    invoke-virtual {v0}, LX/BOy;->d()B

    move-result v0

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_3
    iget-object v6, p0, LX/Axb;->b:LX/BOy;

    invoke-virtual {v6}, LX/BOy;->d()B

    move-result v6

    invoke-static {v7, v6}, LX/Axb;->a(BB)Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1728605
    iget-object v0, p0, LX/Axb;->b:LX/BOy;

    invoke-direct {p0, v0}, LX/Axb;->f(LX/BOy;)Lcom/facebook/friendsharing/souvenirs/models/SouvenirPhotoItem;

    move-result-object v0

    invoke-virtual {v5, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1728606
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    :cond_0
    move v0, v2

    .line 1728607
    goto :goto_0

    :cond_1
    move v0, v2

    .line 1728608
    goto :goto_1

    :cond_2
    move v0, v2

    .line 1728609
    goto :goto_3

    .line 1728610
    :cond_3
    new-instance v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirBurstItem;

    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iget-object v2, p0, LX/Axb;->a:LX/BOq;

    invoke-virtual {v2}, LX/BOq;->a()I

    move-result v2

    invoke-direct {v0, v1, v2}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirBurstItem;-><init>(LX/0Px;I)V

    return-object v0
.end method

.method private e(LX/BOy;)Lcom/facebook/friendsharing/souvenirs/models/SouvenirPhotoItem;
    .locals 5
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1728586
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1728587
    invoke-virtual {p1}, LX/BOy;->d()B

    move-result v0

    const/4 v3, 0x3

    if-ne v0, v3, :cond_0

    move v0, v1

    :goto_0
    const-class v3, Lcom/facebook/friendsharing/souvenirs/models/SouvenirBurstItem;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, LX/BOy;->d()B

    move-result v4

    invoke-static {v3, v4}, LX/Axb;->a(Ljava/lang/String;B)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1728588
    iget-object v0, p0, LX/Axb;->a:LX/BOq;

    invoke-virtual {p1, v0}, LX/BOy;->a(LX/0eW;)LX/0eW;

    .line 1728589
    iget-object v0, p0, LX/Axb;->a:LX/BOq;

    invoke-virtual {v0}, LX/BOq;->b()I

    move-result v0

    .line 1728590
    if-le v0, v1, :cond_1

    :goto_1
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "No assets contained in burst "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, LX/BOy;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1728591
    iget-object v1, p0, LX/Axb;->a:LX/BOq;

    invoke-virtual {v1}, LX/BOq;->a()I

    move-result v1

    invoke-static {v1, v0}, LX/0PB;->checkElementIndex(II)I

    .line 1728592
    iget-object v0, p0, LX/Axb;->a:LX/BOq;

    iget-object v1, p0, LX/Axb;->b:LX/BOy;

    iget-object v2, p0, LX/Axb;->a:LX/BOq;

    invoke-virtual {v2}, LX/BOq;->a()I

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/BOq;->a(LX/BOy;I)LX/BOy;

    .line 1728593
    iget-object v0, p0, LX/Axb;->b:LX/BOy;

    invoke-direct {p0, v0}, LX/Axb;->f(LX/BOy;)Lcom/facebook/friendsharing/souvenirs/models/SouvenirPhotoItem;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v2

    .line 1728594
    goto :goto_0

    :cond_1
    move v1, v2

    .line 1728595
    goto :goto_1
.end method

.method private f(LX/BOy;)Lcom/facebook/friendsharing/souvenirs/models/SouvenirPhotoItem;
    .locals 6
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 1728579
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1728580
    invoke-virtual {p1}, LX/BOy;->d()B

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    const-class v1, Lcom/facebook/friendsharing/souvenirs/models/SouvenirPhotoItem;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, LX/BOy;->d()B

    move-result v2

    invoke-static {v1, v2}, LX/Axb;->a(Ljava/lang/String;B)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1728581
    invoke-virtual {p1}, LX/BOy;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/ipc/media/MediaIdKey;->a(Ljava/lang/String;)Lcom/facebook/ipc/media/MediaIdKey;

    move-result-object v0

    .line 1728582
    iget-object v1, p0, LX/Axb;->f:LX/Axy;

    .line 1728583
    iget-wide v4, v0, Lcom/facebook/ipc/media/MediaIdKey;->b:J

    move-wide v2, v4

    .line 1728584
    invoke-virtual {v1, v2, v3}, LX/Axy;->a(J)Lcom/facebook/friendsharing/souvenirs/models/SouvenirUriItem;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirPhotoItem;

    return-object v0

    .line 1728585
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private g(LX/BOy;)Lcom/facebook/friendsharing/souvenirs/models/SouvenirVideoItem;
    .locals 6
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1728572
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1728573
    invoke-virtual {p1}, LX/BOy;->d()B

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-class v1, Lcom/facebook/friendsharing/souvenirs/models/SouvenirVideoItem;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, LX/BOy;->d()B

    move-result v2

    invoke-static {v1, v2}, LX/Axb;->a(Ljava/lang/String;B)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1728574
    invoke-virtual {p1}, LX/BOy;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/ipc/media/MediaIdKey;->a(Ljava/lang/String;)Lcom/facebook/ipc/media/MediaIdKey;

    move-result-object v0

    .line 1728575
    iget-object v1, p0, LX/Axb;->f:LX/Axy;

    .line 1728576
    iget-wide v4, v0, Lcom/facebook/ipc/media/MediaIdKey;->b:J

    move-wide v2, v4

    .line 1728577
    invoke-virtual {v1, v2, v3}, LX/Axy;->a(J)Lcom/facebook/friendsharing/souvenirs/models/SouvenirUriItem;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirVideoItem;

    return-object v0

    .line 1728578
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static h(LX/Axb;LX/BOy;)Lcom/facebook/friendsharing/souvenirclassifier/models/AssetDetail;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1728566
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1728567
    invoke-virtual {p1}, LX/BOy;->d()B

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1728568
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1728569
    :pswitch_0
    invoke-direct {p0, p1}, LX/Axb;->e(LX/BOy;)Lcom/facebook/friendsharing/souvenirs/models/SouvenirPhotoItem;

    move-result-object v0

    invoke-static {v0}, LX/Axb;->a(Lcom/facebook/friendsharing/souvenirs/models/SouvenirPhotoItem;)Lcom/facebook/friendsharing/souvenirclassifier/models/AssetDetail;

    move-result-object v0

    goto :goto_0

    .line 1728570
    :pswitch_1
    invoke-direct {p0, p1}, LX/Axb;->f(LX/BOy;)Lcom/facebook/friendsharing/souvenirs/models/SouvenirPhotoItem;

    move-result-object v0

    invoke-static {v0}, LX/Axb;->a(Lcom/facebook/friendsharing/souvenirs/models/SouvenirPhotoItem;)Lcom/facebook/friendsharing/souvenirclassifier/models/AssetDetail;

    move-result-object v0

    goto :goto_0

    .line 1728571
    :pswitch_2
    invoke-direct {p0, p1}, LX/Axb;->g(LX/BOy;)Lcom/facebook/friendsharing/souvenirs/models/SouvenirVideoItem;

    move-result-object v0

    invoke-static {v0}, LX/Axb;->a(Lcom/facebook/friendsharing/souvenirs/models/SouvenirVideoItem;)Lcom/facebook/friendsharing/souvenirclassifier/models/AssetDetail;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method private j(LX/BOy;)[Lcom/facebook/friendsharing/souvenirclassifier/models/DuplicateInfo;
    .locals 24

    .prologue
    .line 1728548
    invoke-static/range {p1 .. p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1728549
    new-instance v4, LX/BOs;

    invoke-direct {v4}, LX/BOs;-><init>()V

    .line 1728550
    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/BOy;->a(LX/0eW;)LX/0eW;

    .line 1728551
    invoke-virtual {v4}, LX/BOs;->a()I

    move-result v16

    .line 1728552
    move-object/from16 v0, p0

    invoke-static {v0, v4}, LX/Axb;->a(LX/Axb;LX/BOs;)[LX/BOy;

    move-result-object v17

    .line 1728553
    add-int/lit8 v4, v16, -0x1

    new-array v0, v4, [Lcom/facebook/friendsharing/souvenirclassifier/models/DuplicateInfo;

    move-object/from16 v18, v0

    .line 1728554
    const/4 v4, 0x0

    move v15, v4

    :goto_0
    add-int/lit8 v4, v15, 0x1

    move/from16 v0, v16

    if-ge v4, v0, :cond_1

    .line 1728555
    aget-object v4, v17, v15

    .line 1728556
    add-int/lit8 v5, v15, 0x1

    aget-object v5, v17, v5

    .line 1728557
    invoke-virtual {v4}, LX/BOy;->a()Ljava/lang/String;

    move-result-object v19

    .line 1728558
    invoke-virtual {v5}, LX/BOy;->a()Ljava/lang/String;

    move-result-object v20

    .line 1728559
    move-object/from16 v0, p0

    invoke-static {v0, v4}, LX/Axb;->k(LX/Axb;LX/BOy;)Lcom/facebook/ipc/media/data/LocalMediaData;

    move-result-object v8

    .line 1728560
    move-object/from16 v0, p0

    invoke-static {v0, v5}, LX/Axb;->k(LX/Axb;LX/BOy;)Lcom/facebook/ipc/media/data/LocalMediaData;

    move-result-object v13

    .line 1728561
    if-eqz v8, :cond_0

    if-eqz v13, :cond_0

    .line 1728562
    new-instance v21, Lcom/facebook/friendsharing/souvenirclassifier/models/DuplicateInfo;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/Axb;->h:Lcom/facebook/storyteller/ImageSimilarity4a;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8}, Lcom/facebook/ipc/media/data/LocalMediaData;->d()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v8}, Lcom/facebook/ipc/media/data/LocalMediaData;->a()J

    move-result-wide v6

    long-to-double v6, v6

    invoke-virtual {v8}, Lcom/facebook/ipc/media/data/LocalMediaData;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/ipc/media/data/MediaData;->e()I

    move-result v8

    const/4 v9, 0x1

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13}, Lcom/facebook/ipc/media/data/LocalMediaData;->d()J

    move-result-wide v22

    move-wide/from16 v0, v22

    invoke-virtual {v10, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v13}, Lcom/facebook/ipc/media/data/LocalMediaData;->a()J

    move-result-wide v22

    move-wide/from16 v0, v22

    long-to-double v11, v0

    invoke-virtual {v13}, Lcom/facebook/ipc/media/data/LocalMediaData;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v13

    invoke-virtual {v13}, Lcom/facebook/ipc/media/data/MediaData;->e()I

    move-result v13

    const/4 v14, 0x1

    invoke-virtual/range {v4 .. v14}, Lcom/facebook/storyteller/ImageSimilarity4a;->similarityScore(Ljava/lang/String;DIZLjava/lang/String;DIZ)F

    move-result v4

    float-to-double v4, v4

    move-object/from16 v0, v21

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    invoke-direct {v0, v1, v2, v4, v5}, Lcom/facebook/friendsharing/souvenirclassifier/models/DuplicateInfo;-><init>(Ljava/lang/String;Ljava/lang/String;D)V

    aput-object v21, v18, v15

    .line 1728563
    :goto_1
    add-int/lit8 v4, v15, 0x1

    move v15, v4

    goto :goto_0

    .line 1728564
    :cond_0
    new-instance v4, Lcom/facebook/friendsharing/souvenirclassifier/models/DuplicateInfo;

    const-wide/16 v6, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-direct {v4, v0, v1, v6, v7}, Lcom/facebook/friendsharing/souvenirclassifier/models/DuplicateInfo;-><init>(Ljava/lang/String;Ljava/lang/String;D)V

    aput-object v4, v18, v15

    goto :goto_1

    .line 1728565
    :cond_1
    return-object v18
.end method

.method public static k(LX/Axb;LX/BOy;)Lcom/facebook/ipc/media/data/LocalMediaData;
    .locals 1

    .prologue
    .line 1728542
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1728543
    invoke-virtual {p1}, LX/BOy;->d()B

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1728544
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1728545
    :pswitch_0
    invoke-direct {p0, p1}, LX/Axb;->e(LX/BOy;)Lcom/facebook/friendsharing/souvenirs/models/SouvenirPhotoItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirPhotoItem;->a()Lcom/facebook/ipc/media/data/LocalMediaData;

    move-result-object v0

    goto :goto_0

    .line 1728546
    :pswitch_1
    invoke-direct {p0, p1}, LX/Axb;->f(LX/BOy;)Lcom/facebook/friendsharing/souvenirs/models/SouvenirPhotoItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirPhotoItem;->a()Lcom/facebook/ipc/media/data/LocalMediaData;

    move-result-object v0

    goto :goto_0

    .line 1728547
    :pswitch_2
    invoke-direct {p0, p1}, LX/Axb;->g(LX/BOy;)Lcom/facebook/friendsharing/souvenirs/models/SouvenirVideoItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirVideoItem;->a()Lcom/facebook/ipc/media/data/LocalMediaData;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/BOy;)Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;
    .locals 5

    .prologue
    .line 1728535
    invoke-static {p1}, LX/Axb;->c(LX/BOy;)LX/Ay4;

    move-result-object v0

    const-wide/16 v2, 0x0

    .line 1728536
    iput-wide v2, v0, LX/Ay4;->f:D

    .line 1728537
    move-object v0, v0

    .line 1728538
    const/4 v1, 0x0

    .line 1728539
    iput-object v1, v0, LX/Ay4;->g:Ljava/util/Map;

    .line 1728540
    move-object v0, v0

    .line 1728541
    invoke-static {p0, p1, v0}, LX/Axb;->a(LX/Axb;LX/BOy;LX/Ay4;)LX/Ay6;

    move-result-object v0

    invoke-virtual {v0}, LX/Ay6;->a()Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/BOy;)Lcom/facebook/friendsharing/souvenirclassifier/models/SouvenirClassifierInput;
    .locals 12
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const-wide/16 v4, 0x0

    .line 1728509
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1728510
    invoke-virtual {p0, p1}, LX/Axb;->a(LX/BOy;)Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;

    move-result-object v0

    iget-object v1, p0, LX/Axb;->g:LX/0SG;

    invoke-static {v0, v1}, Lcom/facebook/friendsharing/souvenirs/util/SouvenirLoggingParameters;->a(Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;LX/0SG;)Lcom/facebook/friendsharing/souvenirs/util/SouvenirLoggingParameters;

    move-result-object v8

    .line 1728511
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1728512
    new-instance v0, LX/BOv;

    invoke-direct {v0}, LX/BOv;-><init>()V

    .line 1728513
    const/16 v1, 0x8

    invoke-virtual {p1, v1}, LX/0eW;->a(I)I

    move-result v1

    if-eqz v1, :cond_3

    iget v2, p1, LX/0eW;->a:I

    add-int/2addr v1, v2

    iget-object v2, p1, LX/0eW;->b:Ljava/nio/ByteBuffer;

    .line 1728514
    iput v1, v0, LX/BOv;->a:I

    iput-object v2, v0, LX/BOv;->b:Ljava/nio/ByteBuffer;

    move-object v1, v0

    .line 1728515
    :goto_0
    move-object v0, v1

    .line 1728516
    move-object v0, v0

    .line 1728517
    if-nez v0, :cond_2

    .line 1728518
    new-instance v1, Lcom/facebook/friendsharing/souvenirclassifier/models/LocationInfo;

    const-string v2, ""

    const/4 v3, 0x0

    move-wide v6, v4

    invoke-direct/range {v1 .. v7}, Lcom/facebook/friendsharing/souvenirclassifier/models/LocationInfo;-><init>(Ljava/lang/String;ZDD)V

    move-object v4, v1

    .line 1728519
    :goto_1
    new-instance v0, Lcom/facebook/friendsharing/souvenirclassifier/models/SouvenirClassifierInput;

    invoke-virtual {p1}, LX/BOy;->a()Ljava/lang/String;

    move-result-object v1

    iget-wide v2, v8, Lcom/facebook/friendsharing/souvenirs/util/SouvenirLoggingParameters;->g:J

    long-to-double v2, v2

    invoke-static {v8}, LX/Axb;->a(Lcom/facebook/friendsharing/souvenirs/util/SouvenirLoggingParameters;)Lcom/facebook/friendsharing/souvenirclassifier/models/AssetCounts;

    move-result-object v5

    const/4 v7, 0x0

    .line 1728520
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1728521
    new-instance v8, LX/BOs;

    invoke-direct {v8}, LX/BOs;-><init>()V

    .line 1728522
    invoke-virtual {p1, v8}, LX/BOy;->a(LX/0eW;)LX/0eW;

    .line 1728523
    new-instance v9, LX/BOy;

    invoke-direct {v9}, LX/BOy;-><init>()V

    .line 1728524
    invoke-virtual {v8}, LX/BOs;->a()I

    move-result v10

    .line 1728525
    if-lez v10, :cond_0

    const/4 v6, 0x1

    :goto_2
    invoke-static {v6}, LX/0PB;->checkArgument(Z)V

    .line 1728526
    new-array v6, v10, [Lcom/facebook/friendsharing/souvenirclassifier/models/AssetDetail;

    .line 1728527
    :goto_3
    if-ge v7, v10, :cond_1

    .line 1728528
    invoke-virtual {v8, v9, v7}, LX/BOs;->a(LX/BOy;I)LX/BOy;

    .line 1728529
    invoke-static {p0, v9}, LX/Axb;->h(LX/Axb;LX/BOy;)Lcom/facebook/friendsharing/souvenirclassifier/models/AssetDetail;

    move-result-object v11

    aput-object v11, v6, v7

    .line 1728530
    add-int/lit8 v7, v7, 0x1

    goto :goto_3

    :cond_0
    move v6, v7

    .line 1728531
    goto :goto_2

    .line 1728532
    :cond_1
    move-object v6, v6

    .line 1728533
    invoke-direct {p0, p1}, LX/Axb;->j(LX/BOy;)[Lcom/facebook/friendsharing/souvenirclassifier/models/DuplicateInfo;

    move-result-object v7

    invoke-direct/range {v0 .. v7}, Lcom/facebook/friendsharing/souvenirclassifier/models/SouvenirClassifierInput;-><init>(Ljava/lang/String;DLcom/facebook/friendsharing/souvenirclassifier/models/LocationInfo;Lcom/facebook/friendsharing/souvenirclassifier/models/AssetCounts;[Lcom/facebook/friendsharing/souvenirclassifier/models/AssetDetail;[Lcom/facebook/friendsharing/souvenirclassifier/models/DuplicateInfo;)V

    return-object v0

    .line 1728534
    :cond_2
    const/4 v4, 0x0

    goto :goto_1

    :cond_3
    const/4 v1, 0x0

    goto :goto_0
.end method
