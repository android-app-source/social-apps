.class public final enum LX/BeB;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/BeB;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/BeB;

.field public static final enum PAGE_PHONE:LX/BeB;

.field public static final enum PLACE_ADDRESS:LX/BeB;

.field public static final enum PLACE_TOPIC:LX/BeB;


# instance fields
.field private value:J


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1804651
    new-instance v0, LX/BeB;

    const-string v1, "PAGE_PHONE"

    const-wide v2, 0x7cab7c4a1ed3L

    invoke-direct {v0, v1, v4, v2, v3}, LX/BeB;-><init>(Ljava/lang/String;IJ)V

    sput-object v0, LX/BeB;->PAGE_PHONE:LX/BeB;

    .line 1804652
    new-instance v0, LX/BeB;

    const-string v1, "PLACE_ADDRESS"

    const-wide v2, 0x1328abb20a37dL

    invoke-direct {v0, v1, v5, v2, v3}, LX/BeB;-><init>(Ljava/lang/String;IJ)V

    sput-object v0, LX/BeB;->PLACE_ADDRESS:LX/BeB;

    .line 1804653
    new-instance v0, LX/BeB;

    const-string v1, "PLACE_TOPIC"

    const-wide v2, 0x12f563c2ece97L    # 1.647819600099484E-309

    invoke-direct {v0, v1, v6, v2, v3}, LX/BeB;-><init>(Ljava/lang/String;IJ)V

    sput-object v0, LX/BeB;->PLACE_TOPIC:LX/BeB;

    .line 1804654
    const/4 v0, 0x3

    new-array v0, v0, [LX/BeB;

    sget-object v1, LX/BeB;->PAGE_PHONE:LX/BeB;

    aput-object v1, v0, v4

    sget-object v1, LX/BeB;->PLACE_ADDRESS:LX/BeB;

    aput-object v1, v0, v5

    sget-object v1, LX/BeB;->PLACE_TOPIC:LX/BeB;

    aput-object v1, v0, v6

    sput-object v0, LX/BeB;->$VALUES:[LX/BeB;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IJ)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)V"
        }
    .end annotation

    .prologue
    .line 1804655
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1804656
    iput-wide p3, p0, LX/BeB;->value:J

    .line 1804657
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/BeB;
    .locals 1

    .prologue
    .line 1804658
    const-class v0, LX/BeB;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/BeB;

    return-object v0
.end method

.method public static values()[LX/BeB;
    .locals 1

    .prologue
    .line 1804659
    sget-object v0, LX/BeB;->$VALUES:[LX/BeB;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/BeB;

    return-object v0
.end method


# virtual methods
.method public final getValue()J
    .locals 2

    .prologue
    .line 1804660
    iget-wide v0, p0, LX/BeB;->value:J

    return-wide v0
.end method
