.class public LX/Ak7;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field public final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1k4;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/3Af;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/5oY;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;LX/0Or;LX/0Or;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/1k4;",
            ">;",
            "LX/0Or",
            "<",
            "LX/3Af;",
            ">;",
            "LX/0Or",
            "<",
            "LX/5oY;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1708728
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1708729
    iput-object p1, p0, LX/Ak7;->a:LX/0Or;

    .line 1708730
    iput-object p2, p0, LX/Ak7;->b:LX/0Or;

    .line 1708731
    iput-object p3, p0, LX/Ak7;->c:LX/0Or;

    .line 1708732
    iput-object p4, p0, LX/Ak7;->d:LX/0Ot;

    .line 1708733
    return-void
.end method

.method public static a(LX/0QB;)LX/Ak7;
    .locals 7

    .prologue
    .line 1708734
    const-class v1, LX/Ak7;

    monitor-enter v1

    .line 1708735
    :try_start_0
    sget-object v0, LX/Ak7;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1708736
    sput-object v2, LX/Ak7;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1708737
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1708738
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1708739
    new-instance v3, LX/Ak7;

    const/16 v4, 0xfe0

    invoke-static {v0, v4}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 v5, 0x1399

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 v6, 0x2ffa

    invoke-static {v0, v6}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const/16 p0, 0x455

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, v4, v5, v6, p0}, LX/Ak7;-><init>(LX/0Or;LX/0Or;LX/0Or;LX/0Ot;)V

    .line 1708740
    move-object v0, v3

    .line 1708741
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1708742
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Ak7;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1708743
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1708744
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1Ri;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 1708745
    new-instance v0, LX/Ak4;

    invoke-direct {v0, p0, p1}, LX/Ak4;-><init>(LX/Ak7;LX/1Ri;)V

    return-object v0
.end method

.method public final b(LX/1Ri;)V
    .locals 7

    .prologue
    .line 1708746
    iget-object v0, p0, LX/Ak7;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Af;

    .line 1708747
    invoke-virtual {v0}, LX/3Af;->getContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Landroid/app/Activity;

    invoke-static {v1, v2}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    .line 1708748
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/app/Activity;->isFinishing()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1708749
    :cond_0
    :goto_0
    return-void

    .line 1708750
    :cond_1
    new-instance v2, LX/7TY;

    invoke-direct {v2, v1}, LX/7TY;-><init>(Landroid/content/Context;)V

    .line 1708751
    if-eqz p1, :cond_2

    iget-object v3, p1, LX/1Ri;->c:LX/AkL;

    if-nez v3, :cond_3

    .line 1708752
    :cond_2
    const/4 v3, 0x0

    .line 1708753
    :goto_1
    move v1, v3

    .line 1708754
    if-eqz v1, :cond_0

    .line 1708755
    invoke-virtual {v0, v2}, LX/3Af;->a(LX/1OM;)V

    .line 1708756
    invoke-static {v0}, LX/4ml;->a(Landroid/app/Dialog;)V

    goto :goto_0

    .line 1708757
    :cond_3
    const v3, 0x7f081b45

    invoke-interface {v2, v3}, Landroid/view/Menu;->add(I)Landroid/view/MenuItem;

    move-result-object v3

    .line 1708758
    const v4, 0x7f0208cf

    invoke-interface {v3, v4}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 1708759
    new-instance v4, LX/Ak5;

    invoke-direct {v4, p0, p1}, LX/Ak5;-><init>(LX/Ak7;LX/1Ri;)V

    invoke-interface {v3, v4}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1708760
    iget-object v3, p1, LX/1Ri;->c:LX/AkL;

    if-nez v3, :cond_5

    const/4 v3, 0x0

    move-object v5, v3

    .line 1708761
    :goto_2
    if-eqz v5, :cond_4

    .line 1708762
    iget-object v3, v5, Lcom/facebook/productionprompts/model/PromptDisplayReason;->textWithEntities:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-object v3, v3

    .line 1708763
    if-eqz v3, :cond_4

    .line 1708764
    iget-object v3, v5, Lcom/facebook/productionprompts/model/PromptDisplayReason;->textWithEntities:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-object v3, v3

    .line 1708765
    invoke-virtual {v3}, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;->a()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_6

    .line 1708766
    :cond_4
    :goto_3
    const/4 v3, 0x1

    goto :goto_1

    .line 1708767
    :cond_5
    iget-object v3, p1, LX/1Ri;->c:LX/AkL;

    invoke-interface {v3}, LX/AkL;->h()Lcom/facebook/productionprompts/model/PromptDisplayReason;

    move-result-object v3

    move-object v5, v3

    goto :goto_2

    .line 1708768
    :cond_6
    const v3, 0x7f081b46

    invoke-interface {v2, v3}, Landroid/view/Menu;->add(I)Landroid/view/MenuItem;

    move-result-object v4

    .line 1708769
    instance-of v3, v4, LX/3Ai;

    if-eqz v3, :cond_7

    move-object v3, v4

    .line 1708770
    check-cast v3, LX/3Ai;

    invoke-virtual {v5}, Lcom/facebook/productionprompts/model/PromptDisplayReason;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, LX/3Ai;->a(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 1708771
    :cond_7
    const v3, 0x7f0208ed

    invoke-interface {v4, v3}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 1708772
    new-instance v3, LX/Ak6;

    invoke-direct {v3, p0, v5, v1}, LX/Ak6;-><init>(LX/Ak7;Lcom/facebook/productionprompts/model/PromptDisplayReason;Landroid/content/Context;)V

    invoke-interface {v4, v3}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto :goto_3
.end method
