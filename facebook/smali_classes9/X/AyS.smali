.class public LX/AyS;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/content/res/Resources;

.field private final b:LX/AxY;

.field private final c:LX/1bQ;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/AxY;LX/1bQ;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1730049
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1730050
    iput-object p1, p0, LX/AyS;->a:Landroid/content/res/Resources;

    .line 1730051
    iput-object p2, p0, LX/AyS;->b:LX/AxY;

    .line 1730052
    iput-object p3, p0, LX/AyS;->c:LX/1bQ;

    .line 1730053
    return-void
.end method

.method private static a(Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;)Ljava/util/BitSet;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1730054
    new-instance v1, Ljava/util/BitSet;

    const/4 v0, 0x2

    invoke-direct {v1, v0}, Ljava/util/BitSet;-><init>(I)V

    .line 1730055
    invoke-static {p0}, LX/Az7;->a(Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;)LX/0Rc;

    move-result-object v2

    .line 1730056
    :goto_0
    invoke-virtual {v1, v5}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v1, v6}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    if-nez v0, :cond_3

    :cond_0
    invoke-virtual {v2}, LX/0Rc;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1730057
    invoke-virtual {v2}, LX/0Rc;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirUriItem;

    .line 1730058
    invoke-virtual {v0}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirUriItem;->iq_()LX/Ay0;

    move-result-object v3

    sget-object v4, LX/Ay0;->Photo:LX/Ay0;

    if-ne v3, v4, :cond_1

    .line 1730059
    invoke-virtual {v1, v5}, Ljava/util/BitSet;->set(I)V

    goto :goto_0

    .line 1730060
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirUriItem;->iq_()LX/Ay0;

    move-result-object v3

    sget-object v4, LX/Ay0;->Video:LX/Ay0;

    if-ne v3, v4, :cond_2

    .line 1730061
    invoke-virtual {v1, v6}, Ljava/util/BitSet;->set(I)V

    goto :goto_0

    .line 1730062
    :cond_2
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown Uri item type "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirUriItem;->iq_()LX/Ay0;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1730063
    :cond_3
    return-object v1
.end method

.method public static b(LX/0QB;)LX/AyS;
    .locals 6

    .prologue
    .line 1730064
    new-instance v3, LX/AyS;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    .line 1730065
    new-instance v4, LX/AxY;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/0eD;->b(LX/0QB;)Ljava/util/Locale;

    move-result-object v2

    check-cast v2, Ljava/util/Locale;

    const/16 v5, 0x1616

    invoke-static {p0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-direct {v4, v1, v2, v5}, LX/AxY;-><init>(Landroid/content/Context;Ljava/util/Locale;LX/0Or;)V

    .line 1730066
    move-object v1, v4

    .line 1730067
    check-cast v1, LX/AxY;

    invoke-static {p0}, LX/1bQ;->b(LX/0QB;)LX/1bQ;

    move-result-object v2

    check-cast v2, LX/1bQ;

    invoke-direct {v3, v0, v1, v2}, LX/AyS;-><init>(Landroid/content/res/Resources;LX/AxY;LX/1bQ;)V

    .line 1730068
    return-object v3
.end method


# virtual methods
.method public final a(Landroid/content/res/Resources;Ljava/lang/String;)Landroid/text/SpannableStringBuilder;
    .locals 6

    .prologue
    .line 1730069
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 1730070
    invoke-virtual {v0, p2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1730071
    new-instance v1, Landroid/graphics/Rect;

    const/4 v2, 0x0

    const v3, 0x7f0b11c3

    invoke-virtual {p1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    const v4, 0x7f0b11c2

    invoke-virtual {p1, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    const v5, 0x7f0b11c1

    invoke-virtual {p1, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    invoke-direct {v1, v2, v3, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 1730072
    iget-object v2, p0, LX/AyS;->c:LX/1bQ;

    invoke-virtual {v2}, LX/1bQ;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 1730073
    invoke-virtual {v2, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 1730074
    new-instance v1, Landroid/text/style/ImageSpan;

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Landroid/text/style/ImageSpan;-><init>(Landroid/graphics/drawable/Drawable;I)V

    .line 1730075
    const-string v2, " "

    invoke-virtual {v0, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1730076
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v3

    const/16 v4, 0x11

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1730077
    return-object v0
.end method

.method public final a(LX/Ayb;)Ljava/lang/String;
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1730078
    if-eqz p1, :cond_1

    iget-object v0, p1, LX/Ayb;->a:Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1730079
    iget-object v0, p1, LX/Ayb;->a:Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;

    invoke-static {v0}, LX/AyS;->a(Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;)Ljava/util/BitSet;

    move-result-object v3

    .line 1730080
    invoke-virtual {v3, v2}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {v3, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    move v0, v1

    :goto_1
    const-string v4, "How can you have a collage without photos or videos?"

    invoke-static {v0, v4}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1730081
    invoke-virtual {v3, v2}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {v3, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1730082
    const v0, 0x7f082762

    .line 1730083
    :goto_2
    iget-object v3, p1, LX/Ayb;->a:Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;

    invoke-virtual {v3}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;->a()Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;

    move-result-object v3

    .line 1730084
    iget-object v4, p0, LX/AyS;->a:Landroid/content/res/Resources;

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v5, p0, LX/AyS;->b:LX/AxY;

    invoke-virtual {v3}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;->b()J

    move-result-wide v6

    invoke-virtual {v3}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;->c()J

    move-result-wide v8

    invoke-virtual {v5, v6, v7, v8, v9}, LX/AxY;->a(JJ)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v4, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    move v0, v2

    .line 1730085
    goto :goto_0

    :cond_2
    move v0, v2

    .line 1730086
    goto :goto_1

    .line 1730087
    :cond_3
    invoke-virtual {v3, v2}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {v3, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1730088
    const v0, 0x7f082763

    goto :goto_2

    .line 1730089
    :cond_4
    const v0, 0x7f082764

    goto :goto_2
.end method
