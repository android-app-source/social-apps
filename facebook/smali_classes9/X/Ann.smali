.class public LX/Ann;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/Ann;


# instance fields
.field public final a:I

.field public final b:I


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1712863
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1712864
    const v0, 0x7f0b0977

    .line 1712865
    invoke-static {p1, v0}, LX/0tP;->a(Landroid/content/res/Resources;I)I

    move-result v0

    .line 1712866
    const v1, 0x7f0b010a

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 1712867
    const v2, 0x7f0b010b

    invoke-virtual {p1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    .line 1712868
    add-int/2addr v0, v1

    add-int/2addr v0, v2

    iput v0, p0, LX/Ann;->a:I

    .line 1712869
    const/4 v0, 0x0

    iput v0, p0, LX/Ann;->b:I

    .line 1712870
    return-void
.end method

.method public static a(LX/0QB;)LX/Ann;
    .locals 4

    .prologue
    .line 1712871
    sget-object v0, LX/Ann;->c:LX/Ann;

    if-nez v0, :cond_1

    .line 1712872
    const-class v1, LX/Ann;

    monitor-enter v1

    .line 1712873
    :try_start_0
    sget-object v0, LX/Ann;->c:LX/Ann;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1712874
    if-eqz v2, :cond_0

    .line 1712875
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1712876
    new-instance p0, LX/Ann;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-direct {p0, v3}, LX/Ann;-><init>(Landroid/content/res/Resources;)V

    .line 1712877
    move-object v0, p0

    .line 1712878
    sput-object v0, LX/Ann;->c:LX/Ann;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1712879
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1712880
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1712881
    :cond_1
    sget-object v0, LX/Ann;->c:LX/Ann;

    return-object v0

    .line 1712882
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1712883
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
