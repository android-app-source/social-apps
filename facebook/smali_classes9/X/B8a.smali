.class public final LX/B8a;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 10

    .prologue
    .line 1749877
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1749878
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 1749879
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 1749880
    const/4 v2, 0x0

    .line 1749881
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_6

    .line 1749882
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1749883
    :goto_1
    move v1, v2

    .line 1749884
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1749885
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0

    .line 1749886
    :cond_1
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1749887
    :cond_2
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_5

    .line 1749888
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1749889
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1749890
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_2

    if-eqz v4, :cond_2

    .line 1749891
    const-string v5, "info_fields"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1749892
    invoke-static {p0, p1}, LX/B8g;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_2

    .line 1749893
    :cond_3
    const-string v5, "privacy_data"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1749894
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1749895
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->START_ARRAY:LX/15z;

    if-ne v4, v5, :cond_4

    .line 1749896
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_ARRAY:LX/15z;

    if-eq v4, v5, :cond_4

    .line 1749897
    const/4 v5, 0x0

    .line 1749898
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v4, v6, :cond_b

    .line 1749899
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1749900
    :goto_4
    move v4, v5

    .line 1749901
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 1749902
    :cond_4
    invoke-static {v1, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v1

    move v1, v1

    .line 1749903
    goto :goto_2

    .line 1749904
    :cond_5
    const/4 v4, 0x2

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1749905
    invoke-virtual {p1, v2, v3}, LX/186;->b(II)V

    .line 1749906
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v1}, LX/186;->b(II)V

    .line 1749907
    invoke-virtual {p1}, LX/186;->d()I

    move-result v2

    goto :goto_1

    :cond_6
    move v1, v2

    move v3, v2

    goto :goto_2

    .line 1749908
    :cond_7
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1749909
    :cond_8
    :goto_5
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_a

    .line 1749910
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 1749911
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1749912
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_8

    if-eqz v7, :cond_8

    .line 1749913
    const-string v8, "privacy_text"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_9

    .line 1749914
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_5

    .line 1749915
    :cond_9
    const-string v8, "privacy_type"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 1749916
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLLeadGenPrivacyType;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v4

    goto :goto_5

    .line 1749917
    :cond_a
    const/4 v7, 0x2

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 1749918
    invoke-virtual {p1, v5, v6}, LX/186;->b(II)V

    .line 1749919
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v4}, LX/186;->b(II)V

    .line 1749920
    invoke-virtual {p1}, LX/186;->d()I

    move-result v5

    goto :goto_4

    :cond_b
    move v4, v5

    move v6, v5

    goto :goto_5
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 1749921
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1749922
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1749923
    if-eqz v0, :cond_0

    .line 1749924
    const-string v1, "info_fields"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1749925
    invoke-static {p0, v0, p2, p3}, LX/B8g;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1749926
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1749927
    if-eqz v0, :cond_4

    .line 1749928
    const-string v1, "privacy_data"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1749929
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1749930
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_3

    .line 1749931
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    const/4 p3, 0x1

    .line 1749932
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1749933
    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 1749934
    if-eqz v3, :cond_1

    .line 1749935
    const-string p1, "privacy_text"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1749936
    invoke-virtual {p2, v3}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1749937
    :cond_1
    invoke-virtual {p0, v2, p3}, LX/15i;->g(II)I

    move-result v3

    .line 1749938
    if-eqz v3, :cond_2

    .line 1749939
    const-string v3, "privacy_type"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1749940
    invoke-virtual {p0, v2, p3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v3}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1749941
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1749942
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1749943
    :cond_3
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1749944
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1749945
    return-void
.end method
