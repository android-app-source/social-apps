.class public LX/BJ3;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:LX/1WM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:Lcom/facebook/photos/warning/ObjectionableContentWarningView;

.field public c:Lcom/facebook/feed/collage/ui/CollageAttachmentView;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/collage/ui/CollageAttachmentView",
            "<",
            "LX/26N;",
            ">;"
        }
    .end annotation
.end field

.field private d:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1771468
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 1771469
    const v0, 0x7f030c4a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1771470
    const-class v0, LX/BJ3;

    invoke-static {v0, p0}, LX/BJ3;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1771471
    const v0, 0x7f0d1e06

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/warning/ObjectionableContentWarningView;

    iput-object v0, p0, LX/BJ3;->b:Lcom/facebook/photos/warning/ObjectionableContentWarningView;

    .line 1771472
    const v0, 0x7f0d1cc1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    iput-object v0, p0, LX/BJ3;->c:Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    .line 1771473
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/BJ3;

    invoke-static {p0}, LX/1WM;->a(LX/0QB;)LX/1WM;

    move-result-object p0

    check-cast p0, LX/1WM;

    iput-object p0, p1, LX/BJ3;->a:LX/1WM;

    return-void
.end method


# virtual methods
.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 10

    .prologue
    .line 1771474
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 1771475
    iget-object v0, p0, LX/BJ3;->c:Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    invoke-virtual {v0}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->getVisibleAttachments()LX/0Px;

    move-result-object v2

    .line 1771476
    iget-boolean v0, p0, LX/BJ3;->d:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    move v1, v0

    :goto_1
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 1771477
    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/26N;

    .line 1771478
    iget-object v3, p0, LX/BJ3;->c:Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    invoke-virtual {v3, v1}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->c(I)Landroid/graphics/Rect;

    move-result-object v3

    .line 1771479
    iget-object v4, p0, LX/BJ3;->a:LX/1WM;

    invoke-interface {v0}, LX/26N;->c()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/1WM;->a(Lcom/facebook/graphql/model/GraphQLMedia;)Z

    move-result v4

    if-eqz v4, :cond_3

    iget-object v4, p0, LX/BJ3;->c:Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    invoke-virtual {v4, v1}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->f(I)Z

    move-result v4

    if-eqz v4, :cond_3

    iget-object v4, p0, LX/BJ3;->c:Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    invoke-virtual {v4, v1}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->b(I)Z

    move-result v4

    if-nez v4, :cond_3

    invoke-interface {v0}, LX/26N;->a()Z

    move-result v4

    if-nez v4, :cond_3

    const/4 v4, 0x1

    :goto_2
    move v0, v4

    .line 1771480
    if-eqz v0, :cond_0

    .line 1771481
    const-wide/high16 v4, 0x3fd0000000000000L    # 0.25

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v6

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v7

    invoke-static {v6, v7}, Ljava/lang/Math;->min(II)I

    move-result v6

    int-to-double v6, v6

    mul-double/2addr v4, v6

    double-to-int v4, v4

    .line 1771482
    invoke-virtual {p0}, LX/BJ3;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f021ae3

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    .line 1771483
    const/16 v6, 0x80

    invoke-virtual {v5, v6}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 1771484
    invoke-virtual {v3}, Landroid/graphics/Rect;->centerX()I

    move-result v6

    div-int/lit8 v7, v4, 0x2

    sub-int/2addr v6, v7

    invoke-virtual {v3}, Landroid/graphics/Rect;->centerY()I

    move-result v7

    div-int/lit8 v8, v4, 0x2

    sub-int/2addr v7, v8

    invoke-virtual {v3}, Landroid/graphics/Rect;->centerX()I

    move-result v8

    div-int/lit8 v9, v4, 0x2

    add-int/2addr v8, v9

    invoke-virtual {v3}, Landroid/graphics/Rect;->centerY()I

    move-result v9

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v4, v9

    invoke-virtual {v5, v6, v7, v8, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1771485
    invoke-virtual {v5, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1771486
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 1771487
    :cond_2
    return-void

    :cond_3
    const/4 v4, 0x0

    goto :goto_2
.end method

.method public final onLayout(ZIIII)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1771488
    iget-object v0, p0, LX/BJ3;->c:Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    invoke-virtual {p0}, LX/BJ3;->getMeasuredWidth()I

    move-result v3

    invoke-virtual {p0}, LX/BJ3;->getMeasuredHeight()I

    move-result v4

    invoke-virtual {v0, v2, v2, v3, v4}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->layout(IIII)V

    .line 1771489
    iget-object v0, p0, LX/BJ3;->c:Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    invoke-virtual {v0}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->getVisibleAttachments()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/26N;

    .line 1771490
    const/4 v3, 0x0

    .line 1771491
    move v4, v3

    .line 1771492
    :goto_0
    iget-object v5, p0, LX/BJ3;->c:Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    invoke-virtual {v5}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->getVisibleAttachmentsCount()I

    move-result v5

    if-ge v3, v5, :cond_1

    .line 1771493
    iget-object v5, p0, LX/BJ3;->c:Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    invoke-virtual {v5, v3}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->c(I)Landroid/graphics/Rect;

    move-result-object v5

    .line 1771494
    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v6

    invoke-virtual {p0}, LX/BJ3;->getMeasuredWidth()I

    move-result p1

    if-ne v6, p1, :cond_4

    const/4 v6, 0x1

    :goto_1
    move v5, v6

    .line 1771495
    if-eqz v5, :cond_0

    .line 1771496
    add-int/lit8 v4, v4, 0x1

    .line 1771497
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1771498
    :cond_1
    move v3, v4

    .line 1771499
    if-ne v3, v1, :cond_2

    iget-object v3, p0, LX/BJ3;->a:LX/1WM;

    invoke-interface {v0}, LX/26N;->c()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/1WM;->a(Lcom/facebook/graphql/model/GraphQLMedia;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, LX/BJ3;->d:Z

    .line 1771500
    iget-boolean v0, p0, LX/BJ3;->d:Z

    if-eqz v0, :cond_3

    .line 1771501
    iget-object v0, p0, LX/BJ3;->c:Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    invoke-virtual {v0, v2}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->c(I)Landroid/graphics/Rect;

    move-result-object v0

    .line 1771502
    iget-object v1, p0, LX/BJ3;->b:Lcom/facebook/photos/warning/ObjectionableContentWarningView;

    invoke-virtual {p0}, LX/BJ3;->getPaddingLeft()I

    move-result v3

    iget v4, v0, Landroid/graphics/Rect;->left:I

    add-int/2addr v3, v4

    invoke-virtual {p0}, LX/BJ3;->getPaddingTop()I

    move-result v4

    iget v5, v0, Landroid/graphics/Rect;->top:I

    add-int/2addr v4, v5

    invoke-virtual {p0}, LX/BJ3;->getPaddingLeft()I

    move-result v5

    iget v6, v0, Landroid/graphics/Rect;->right:I

    add-int/2addr v5, v6

    invoke-virtual {p0}, LX/BJ3;->getPaddingTop()I

    move-result v6

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v0, v6

    invoke-virtual {v1, v3, v4, v5, v0}, Lcom/facebook/photos/warning/ObjectionableContentWarningView;->layout(IIII)V

    .line 1771503
    iget-object v0, p0, LX/BJ3;->b:Lcom/facebook/photos/warning/ObjectionableContentWarningView;

    invoke-virtual {v0, v2}, Lcom/facebook/photos/warning/ObjectionableContentWarningView;->setVisibility(I)V

    .line 1771504
    :goto_3
    return-void

    :cond_2
    move v0, v2

    .line 1771505
    goto :goto_2

    .line 1771506
    :cond_3
    iget-object v0, p0, LX/BJ3;->b:Lcom/facebook/photos/warning/ObjectionableContentWarningView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/photos/warning/ObjectionableContentWarningView;->setVisibility(I)V

    goto :goto_3

    :cond_4
    const/4 v6, 0x0

    goto :goto_1
.end method

.method public final onMeasure(II)V
    .locals 3

    .prologue
    const/high16 v2, 0x40000000    # 2.0f

    .line 1771507
    iget-object v0, p0, LX/BJ3;->c:Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->measure(II)V

    .line 1771508
    iget-object v0, p0, LX/BJ3;->c:Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->c(I)Landroid/graphics/Rect;

    move-result-object v0

    .line 1771509
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v1

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 1771510
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 1771511
    iget-object v2, p0, LX/BJ3;->b:Lcom/facebook/photos/warning/ObjectionableContentWarningView;

    invoke-virtual {v2, v1, v0}, Lcom/facebook/photos/warning/ObjectionableContentWarningView;->measure(II)V

    .line 1771512
    iget-object v0, p0, LX/BJ3;->c:Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    invoke-virtual {v0}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->getMeasuredWidth()I

    move-result v0

    iget-object v1, p0, LX/BJ3;->c:Lcom/facebook/feed/collage/ui/CollageAttachmentView;

    invoke-virtual {v1}, Lcom/facebook/feed/collage/ui/CollageAttachmentView;->getMeasuredHeight()I

    move-result v1

    invoke-virtual {p0, v0, v1}, LX/BJ3;->setMeasuredDimension(II)V

    .line 1771513
    return-void
.end method
