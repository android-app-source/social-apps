.class public final LX/AxE;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/AxF;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Ljava/lang/String;

.field public final synthetic c:LX/AxF;


# direct methods
.method public constructor <init>(LX/AxF;)V
    .locals 1

    .prologue
    .line 1727714
    iput-object p1, p0, LX/AxE;->c:LX/AxF;

    .line 1727715
    move-object v0, p1

    .line 1727716
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1727717
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1727718
    const-string v0, "MemePromptHScrollItemComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1727719
    if-ne p0, p1, :cond_1

    .line 1727720
    :cond_0
    :goto_0
    return v0

    .line 1727721
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1727722
    goto :goto_0

    .line 1727723
    :cond_3
    check-cast p1, LX/AxE;

    .line 1727724
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1727725
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1727726
    if-eq v2, v3, :cond_0

    .line 1727727
    iget v2, p0, LX/AxE;->a:I

    iget v3, p1, LX/AxE;->a:I

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 1727728
    goto :goto_0

    .line 1727729
    :cond_4
    iget-object v2, p0, LX/AxE;->b:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/AxE;->b:Ljava/lang/String;

    iget-object v3, p1, LX/AxE;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1727730
    goto :goto_0

    .line 1727731
    :cond_5
    iget-object v2, p1, LX/AxE;->b:Ljava/lang/String;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
