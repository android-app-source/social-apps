.class public final enum LX/CZc;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/CZc;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/CZc;

.field public static final enum FINAL_DATA:LX/CZc;

.field public static final enum PRELIMINARY_DATA:LX/CZc;

.field public static final enum UNINITIALIZED:LX/CZc;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1916264
    new-instance v0, LX/CZc;

    const-string v1, "UNINITIALIZED"

    invoke-direct {v0, v1, v2}, LX/CZc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CZc;->UNINITIALIZED:LX/CZc;

    .line 1916265
    new-instance v0, LX/CZc;

    const-string v1, "PRELIMINARY_DATA"

    invoke-direct {v0, v1, v3}, LX/CZc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CZc;->PRELIMINARY_DATA:LX/CZc;

    .line 1916266
    new-instance v0, LX/CZc;

    const-string v1, "FINAL_DATA"

    invoke-direct {v0, v1, v4}, LX/CZc;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CZc;->FINAL_DATA:LX/CZc;

    .line 1916267
    const/4 v0, 0x3

    new-array v0, v0, [LX/CZc;

    sget-object v1, LX/CZc;->UNINITIALIZED:LX/CZc;

    aput-object v1, v0, v2

    sget-object v1, LX/CZc;->PRELIMINARY_DATA:LX/CZc;

    aput-object v1, v0, v3

    sget-object v1, LX/CZc;->FINAL_DATA:LX/CZc;

    aput-object v1, v0, v4

    sput-object v0, LX/CZc;->$VALUES:[LX/CZc;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1916261
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/CZc;
    .locals 1

    .prologue
    .line 1916262
    const-class v0, LX/CZc;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/CZc;

    return-object v0
.end method

.method public static values()[LX/CZc;
    .locals 1

    .prologue
    .line 1916263
    sget-object v0, LX/CZc;->$VALUES:[LX/CZc;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/CZc;

    return-object v0
.end method
