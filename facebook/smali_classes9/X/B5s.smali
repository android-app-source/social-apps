.class public abstract LX/B5s;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Alg;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<CONTEXT::",
        "LX/B5o;",
        ">",
        "Ljava/lang/Object;",
        "LX/Alg",
        "<TCONTEXT;>;"
    }
.end annotation


# instance fields
.field public a:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "LX/Alg;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "LX/Alg;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1746005
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1746006
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1746007
    invoke-static {p1}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v0

    iput-object v0, p0, LX/B5s;->a:LX/0Rf;

    .line 1746008
    return-void
.end method


# virtual methods
.method public a(LX/1RN;)LX/Alg;
    .locals 4
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1746009
    const/4 v1, 0x0

    .line 1746010
    iget-object v0, p0, LX/B5s;->a:LX/0Rf;

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Alg;

    .line 1746011
    invoke-interface {v0, p1}, LX/Alg;->b(LX/1RN;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1746012
    if-eqz v1, :cond_1

    .line 1746013
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Two prompt action handlers should not be enabled for the same prompt object"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move-object v0, v1

    :cond_1
    move-object v1, v0

    .line 1746014
    goto :goto_0

    .line 1746015
    :cond_2
    return-object v1
.end method

.method public final a(LX/88f;LX/1RN;)V
    .locals 1
    .param p1    # LX/88f;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1746016
    invoke-virtual {p0, p2}, LX/B5s;->a(LX/1RN;)LX/Alg;

    move-result-object v0

    .line 1746017
    if-eqz v0, :cond_0

    .line 1746018
    invoke-interface {v0, p1, p2}, LX/Alg;->a(LX/88f;LX/1RN;)V

    .line 1746019
    :cond_0
    return-void
.end method

.method public a(Landroid/view/View;LX/1RN;LX/B5o;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "LX/1RN;",
            "TCONTEXT;)V"
        }
    .end annotation

    .prologue
    .line 1746020
    invoke-virtual {p0, p2}, LX/B5s;->a(LX/1RN;)LX/Alg;

    move-result-object v0

    .line 1746021
    if-eqz v0, :cond_0

    .line 1746022
    invoke-interface {v0, p1, p2, p3}, LX/Alg;->a(Landroid/view/View;LX/1RN;LX/B5o;)V

    .line 1746023
    :cond_0
    return-void
.end method
