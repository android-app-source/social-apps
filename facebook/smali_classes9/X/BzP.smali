.class public LX/BzP;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public final a:LX/1qb;

.field public final b:LX/2y3;

.field public final c:LX/Ap5;


# direct methods
.method public constructor <init>(LX/1qb;LX/2y3;LX/Ap5;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1838692
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1838693
    iput-object p1, p0, LX/BzP;->a:LX/1qb;

    .line 1838694
    iput-object p2, p0, LX/BzP;->b:LX/2y3;

    .line 1838695
    iput-object p3, p0, LX/BzP;->c:LX/Ap5;

    .line 1838696
    return-void
.end method

.method public static a(LX/0QB;)LX/BzP;
    .locals 6

    .prologue
    .line 1838697
    const-class v1, LX/BzP;

    monitor-enter v1

    .line 1838698
    :try_start_0
    sget-object v0, LX/BzP;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1838699
    sput-object v2, LX/BzP;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1838700
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1838701
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1838702
    new-instance p0, LX/BzP;

    invoke-static {v0}, LX/1qb;->a(LX/0QB;)LX/1qb;

    move-result-object v3

    check-cast v3, LX/1qb;

    invoke-static {v0}, LX/2y3;->a(LX/0QB;)LX/2y3;

    move-result-object v4

    check-cast v4, LX/2y3;

    invoke-static {v0}, LX/Ap5;->a(LX/0QB;)LX/Ap5;

    move-result-object v5

    check-cast v5, LX/Ap5;

    invoke-direct {p0, v3, v4, v5}, LX/BzP;-><init>(LX/1qb;LX/2y3;LX/Ap5;)V

    .line 1838703
    move-object v0, p0

    .line 1838704
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1838705
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/BzP;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1838706
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1838707
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
