.class public final enum LX/BHC;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/BHC;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/BHC;

.field public static final enum FETCH_TAGGING_DATA:LX/BHC;

.field public static final enum MEDIA_STORE_QUERY:LX/BHC;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1768598
    new-instance v0, LX/BHC;

    const-string v1, "FETCH_TAGGING_DATA"

    invoke-direct {v0, v1, v2}, LX/BHC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BHC;->FETCH_TAGGING_DATA:LX/BHC;

    .line 1768599
    new-instance v0, LX/BHC;

    const-string v1, "MEDIA_STORE_QUERY"

    invoke-direct {v0, v1, v3}, LX/BHC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BHC;->MEDIA_STORE_QUERY:LX/BHC;

    .line 1768600
    const/4 v0, 0x2

    new-array v0, v0, [LX/BHC;

    sget-object v1, LX/BHC;->FETCH_TAGGING_DATA:LX/BHC;

    aput-object v1, v0, v2

    sget-object v1, LX/BHC;->MEDIA_STORE_QUERY:LX/BHC;

    aput-object v1, v0, v3

    sput-object v0, LX/BHC;->$VALUES:[LX/BHC;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1768601
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/BHC;
    .locals 1

    .prologue
    .line 1768602
    const-class v0, LX/BHC;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/BHC;

    return-object v0
.end method

.method public static values()[LX/BHC;
    .locals 1

    .prologue
    .line 1768603
    sget-object v0, LX/BHC;->$VALUES:[LX/BHC;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/BHC;

    return-object v0
.end method
