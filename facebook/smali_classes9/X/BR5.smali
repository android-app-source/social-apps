.class public final LX/BR5;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Landroid/net/Uri;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;)V
    .locals 0

    .prologue
    .line 1783167
    iput-object p1, p0, LX/BR5;->a:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1783197
    sget-object v0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->e:Ljava/lang/String;

    const-string v1, "Failed to convert Uri to file scheme"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1783198
    iget-object v0, p0, LX/BR5;->a:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    iget-object v0, v0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->u:Lcom/facebook/base/fragment/FbFragment;

    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 1783199
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 14

    .prologue
    .line 1783168
    check-cast p1, Landroid/net/Uri;

    const/high16 v3, 0x40000000    # 2.0f

    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    .line 1783169
    iget-object v0, p0, LX/BR5;->a:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    iget-object v0, v0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->k:LX/BR1;

    iput-object p1, v0, LX/BR1;->a:Landroid/net/Uri;

    .line 1783170
    iget-object v0, p0, LX/BR5;->a:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    iget-object v0, v0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->k:LX/BR1;

    iget-object v0, v0, LX/BR1;->b:Landroid/net/Uri;

    if-nez v0, :cond_0

    .line 1783171
    iget-object v0, p0, LX/BR5;->a:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    iget-object v0, v0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->k:LX/BR1;

    iput-object p1, v0, LX/BR1;->b:Landroid/net/Uri;

    .line 1783172
    :cond_0
    iget-object v0, p0, LX/BR5;->a:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    iget-object v0, v0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->k:LX/BR1;

    iget-object v0, v0, LX/BR1;->d:LX/434;

    if-nez v0, :cond_1

    .line 1783173
    iget-object v0, p0, LX/BR5;->a:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    iget-object v0, v0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->k:LX/BR1;

    iget-object v1, p0, LX/BR5;->a:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    iget-object v1, v1, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->k:LX/BR1;

    iget-object v1, v1, LX/BR1;->a:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/2Qx;->a(Ljava/lang/String;)LX/434;

    move-result-object v1

    iput-object v1, v0, LX/BR1;->d:LX/434;

    .line 1783174
    :cond_1
    iget-object v0, p0, LX/BR5;->a:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    iget-object v0, v0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->k:LX/BR1;

    iget-object v0, v0, LX/BR1;->c:Landroid/graphics/RectF;

    if-nez v0, :cond_2

    .line 1783175
    iget-object v0, p0, LX/BR5;->a:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    iget-object v0, v0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->k:LX/BR1;

    iget-object v0, v0, LX/BR1;->d:LX/434;

    iget v0, v0, LX/434;->b:I

    .line 1783176
    iget-object v1, p0, LX/BR5;->a:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    iget-object v1, v1, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->k:LX/BR1;

    iget-object v1, v1, LX/BR1;->d:LX/434;

    iget v1, v1, LX/434;->a:I

    .line 1783177
    if-le v0, v1, :cond_3

    .line 1783178
    sub-int v2, v0, v1

    int-to-float v2, v2

    div-float/2addr v2, v3

    .line 1783179
    iget-object v3, p0, LX/BR5;->a:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    iget-object v3, v3, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->k:LX/BR1;

    new-instance v4, Landroid/graphics/RectF;

    int-to-float v5, v0

    div-float v5, v2, v5

    int-to-float v1, v1

    add-float/2addr v1, v2

    int-to-float v0, v0

    div-float v0, v1, v0

    invoke-direct {v4, v5, v6, v0, v7}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v4, v3, LX/BR1;->c:Landroid/graphics/RectF;

    .line 1783180
    iget-object v0, p0, LX/BR5;->a:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    const-string v1, "auto_crop"

    .line 1783181
    iput-object v1, v0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->E:Ljava/lang/String;

    .line 1783182
    :cond_2
    :goto_0
    iget-object v0, p0, LX/BR5;->a:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    iget-boolean v0, v0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->j:Z

    if-nez v0, :cond_5

    .line 1783183
    iget-object v0, p0, LX/BR5;->a:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    iget-object v1, p0, LX/BR5;->a:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    iget-object v1, v1, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->k:LX/BR1;

    iget-object v1, v1, LX/BR1;->c:Landroid/graphics/RectF;

    new-instance v2, LX/BR4;

    invoke-direct {v2, p0}, LX/BR4;-><init>(LX/BR5;)V

    .line 1783184
    iget-object v8, v0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->o:LX/9fn;

    iget-object v9, v0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->h:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    iget-object v10, v0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->k:LX/BR1;

    iget-object v10, v10, LX/BR1;->b:Landroid/net/Uri;

    iget-object v11, v0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->k:LX/BR1;

    iget-object v11, v11, LX/BR1;->d:LX/434;

    move-object v12, v1

    move-object v13, v2

    invoke-virtual/range {v8 .. v13}, LX/9fn;->a(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;Landroid/net/Uri;LX/434;Landroid/graphics/RectF;LX/9fc;)V

    .line 1783185
    :goto_1
    return-void

    .line 1783186
    :cond_3
    if-ge v0, v1, :cond_4

    .line 1783187
    sub-int v2, v1, v0

    int-to-float v2, v2

    div-float/2addr v2, v3

    .line 1783188
    iget-object v3, p0, LX/BR5;->a:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    iget-object v3, v3, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->k:LX/BR1;

    new-instance v4, Landroid/graphics/RectF;

    int-to-float v5, v1

    div-float v5, v2, v5

    int-to-float v0, v0

    add-float/2addr v0, v2

    int-to-float v1, v1

    div-float/2addr v0, v1

    invoke-direct {v4, v6, v5, v7, v0}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v4, v3, LX/BR1;->c:Landroid/graphics/RectF;

    .line 1783189
    iget-object v0, p0, LX/BR5;->a:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    const-string v1, "auto_crop"

    .line 1783190
    iput-object v1, v0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->E:Ljava/lang/String;

    .line 1783191
    goto :goto_0

    .line 1783192
    :cond_4
    iget-object v0, p0, LX/BR5;->a:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    iget-object v0, v0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->k:LX/BR1;

    sget-object v1, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->f:Landroid/graphics/RectF;

    iput-object v1, v0, LX/BR1;->c:Landroid/graphics/RectF;

    .line 1783193
    iget-object v0, p0, LX/BR5;->a:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    const-string v1, "no_crop"

    .line 1783194
    iput-object v1, v0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->E:Ljava/lang/String;

    .line 1783195
    goto :goto_0

    .line 1783196
    :cond_5
    iget-object v0, p0, LX/BR5;->a:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    invoke-virtual {v0}, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->h()V

    goto :goto_1
.end method
