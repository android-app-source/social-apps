.class public final enum LX/BoO;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/BoO;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/BoO;

.field public static final enum HIDE_TOPIC_FROM_USER:LX/BoO;

.field public static final enum NEGATIVE_FEEDBACK_ACTION:LX/BoO;

.field public static final enum NOTIFY:LX/BoO;

.field public static final enum SAVE:LX/BoO;

.field public static final enum SAVE_OFFLINE:LX/BoO;

.field public static final enum UN_SEE_FIRST:LX/BoO;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1821704
    new-instance v0, LX/BoO;

    const-string v1, "NEGATIVE_FEEDBACK_ACTION"

    invoke-direct {v0, v1, v3}, LX/BoO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BoO;->NEGATIVE_FEEDBACK_ACTION:LX/BoO;

    .line 1821705
    new-instance v0, LX/BoO;

    const-string v1, "NOTIFY"

    invoke-direct {v0, v1, v4}, LX/BoO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BoO;->NOTIFY:LX/BoO;

    .line 1821706
    new-instance v0, LX/BoO;

    const-string v1, "SAVE"

    invoke-direct {v0, v1, v5}, LX/BoO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BoO;->SAVE:LX/BoO;

    .line 1821707
    new-instance v0, LX/BoO;

    const-string v1, "SAVE_OFFLINE"

    invoke-direct {v0, v1, v6}, LX/BoO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BoO;->SAVE_OFFLINE:LX/BoO;

    .line 1821708
    new-instance v0, LX/BoO;

    const-string v1, "UN_SEE_FIRST"

    invoke-direct {v0, v1, v7}, LX/BoO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BoO;->UN_SEE_FIRST:LX/BoO;

    .line 1821709
    new-instance v0, LX/BoO;

    const-string v1, "HIDE_TOPIC_FROM_USER"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/BoO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BoO;->HIDE_TOPIC_FROM_USER:LX/BoO;

    .line 1821710
    const/4 v0, 0x6

    new-array v0, v0, [LX/BoO;

    sget-object v1, LX/BoO;->NEGATIVE_FEEDBACK_ACTION:LX/BoO;

    aput-object v1, v0, v3

    sget-object v1, LX/BoO;->NOTIFY:LX/BoO;

    aput-object v1, v0, v4

    sget-object v1, LX/BoO;->SAVE:LX/BoO;

    aput-object v1, v0, v5

    sget-object v1, LX/BoO;->SAVE_OFFLINE:LX/BoO;

    aput-object v1, v0, v6

    sget-object v1, LX/BoO;->UN_SEE_FIRST:LX/BoO;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/BoO;->HIDE_TOPIC_FROM_USER:LX/BoO;

    aput-object v2, v0, v1

    sput-object v0, LX/BoO;->$VALUES:[LX/BoO;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1821711
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/BoO;
    .locals 1

    .prologue
    .line 1821712
    const-class v0, LX/BoO;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/BoO;

    return-object v0
.end method

.method public static values()[LX/BoO;
    .locals 1

    .prologue
    .line 1821713
    sget-object v0, LX/BoO;->$VALUES:[LX/BoO;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/BoO;

    return-object v0
.end method
