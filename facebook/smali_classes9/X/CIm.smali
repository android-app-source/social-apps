.class public LX/CIm;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:LX/0Zb;

.field private b:Landroid/view/ViewGroup;

.field public c:LX/0h5;

.field public d:Landroid/widget/TextView;

.field public e:Lcom/facebook/widget/SwitchCompat;

.field private f:Landroid/widget/ListView;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1874791
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1874792
    iput-object p1, p0, LX/CIm;->a:LX/0Zb;

    .line 1874793
    return-void
.end method

.method public static a(LX/0QB;)LX/CIm;
    .locals 1

    .prologue
    .line 1874790
    invoke-static {p0}, LX/CIm;->b(LX/0QB;)LX/CIm;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 1874788
    const v0, 0x7f0400da

    const v1, 0x7f040031

    invoke-virtual {p0, v0, v1}, Landroid/app/Activity;->overridePendingTransition(II)V

    .line 1874789
    return-void
.end method

.method public static b(LX/0QB;)LX/CIm;
    .locals 2

    .prologue
    .line 1874786
    new-instance v1, LX/CIm;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-direct {v1, v0}, LX/CIm;-><init>(LX/0Zb;)V

    .line 1874787
    return-object v1
.end method

.method public static b(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 1874784
    const v0, 0x7f0400b7

    const v1, 0x7f0400ed

    invoke-virtual {p0, v0, v1}, Landroid/app/Activity;->overridePendingTransition(II)V

    .line 1874785
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/base/activity/FbPreferenceActivity;LX/0Tn;IIZ)LX/4oi;
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 1874770
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v6, v5

    move v7, p5

    .line 1874771
    new-instance p0, LX/4ok;

    invoke-direct {p0, v1}, LX/4ok;-><init>(Landroid/content/Context;)V

    .line 1874772
    invoke-virtual {p0, v2}, LX/4oi;->a(LX/0Tn;)V

    .line 1874773
    invoke-virtual {p0, v3}, LX/4oi;->setTitle(I)V

    .line 1874774
    if-lez v4, :cond_0

    .line 1874775
    invoke-virtual {p0, v4}, LX/4oi;->setSummary(I)V

    .line 1874776
    :cond_0
    if-lez v5, :cond_1

    .line 1874777
    invoke-virtual {p0, v5}, LX/4oi;->setSummaryOff(I)V

    .line 1874778
    :cond_1
    if-lez v6, :cond_2

    .line 1874779
    invoke-virtual {p0, v6}, LX/4oi;->setSummaryOn(I)V

    .line 1874780
    :cond_2
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {p0, p1}, LX/4oi;->setDefaultValue(Ljava/lang/Object;)V

    .line 1874781
    invoke-virtual {v0, p0}, LX/CIm;->a(Landroid/preference/Preference;)V

    .line 1874782
    move-object v0, p0

    .line 1874783
    return-object v0
.end method

.method public final a(Lcom/facebook/base/activity/FbPreferenceActivity;LX/0Tn;IZ)LX/4oi;
    .locals 6

    .prologue
    .line 1874769
    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, LX/CIm;->a(Lcom/facebook/base/activity/FbPreferenceActivity;LX/0Tn;IIZ)LX/4oi;

    move-result-object v0

    return-object v0
.end method

.method public final a(F)V
    .locals 1

    .prologue
    .line 1874767
    iget-object v0, p0, LX/CIm;->f:Landroid/widget/ListView;

    invoke-virtual {v0, p1}, Landroid/widget/ListView;->setAlpha(F)V

    .line 1874768
    return-void
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 1874765
    iget-object v0, p0, LX/CIm;->d:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 1874766
    return-void
.end method

.method public final a(Landroid/preference/Preference;)V
    .locals 1

    .prologue
    .line 1874726
    new-instance v0, LX/CIl;

    invoke-direct {v0, p0}, LX/CIl;-><init>(LX/CIm;)V

    invoke-virtual {p1, v0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 1874727
    return-void
.end method

.method public final a(Landroid/preference/Preference;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 1874763
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, LX/CIm;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1874764
    return-void
.end method

.method public final a(Landroid/preference/PreferenceGroup;)V
    .locals 4

    .prologue
    .line 1874755
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p1}, Landroid/preference/PreferenceGroup;->getPreferenceCount()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 1874756
    invoke-virtual {p1, v1}, Landroid/preference/PreferenceGroup;->getPreference(I)Landroid/preference/Preference;

    move-result-object v0

    .line 1874757
    invoke-virtual {v0}, Landroid/preference/Preference;->getSummary()Ljava/lang/CharSequence;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Landroid/preference/Preference;->getLayoutResource()I

    move-result v2

    const v3, 0x7f031000

    if-ne v2, v3, :cond_0

    .line 1874758
    const v2, 0x7f031002

    invoke-virtual {v0, v2}, Landroid/preference/Preference;->setLayoutResource(I)V

    .line 1874759
    :cond_0
    instance-of v2, v0, Landroid/preference/PreferenceGroup;

    if-eqz v2, :cond_1

    .line 1874760
    check-cast v0, Landroid/preference/PreferenceGroup;

    invoke-virtual {p0, v0}, LX/CIm;->a(Landroid/preference/PreferenceGroup;)V

    .line 1874761
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1874762
    :cond_2
    return-void
.end method

.method public final a(Lcom/facebook/base/activity/FbPreferenceActivity;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v1, -0x1

    const/4 v3, 0x0

    .line 1874744
    invoke-virtual {p1}, Lcom/facebook/base/activity/FbPreferenceActivity;->getListView()Landroid/widget/ListView;

    move-result-object v0

    iput-object v0, p0, LX/CIm;->f:Landroid/widget/ListView;

    .line 1874745
    iget-object v0, p0, LX/CIm;->f:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setBackgroundColor(I)V

    .line 1874746
    iget-object v0, p0, LX/CIm;->f:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setCacheColorHint(I)V

    .line 1874747
    iget-object v0, p0, LX/CIm;->f:Landroid/widget/ListView;

    invoke-virtual {v0, v4}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 1874748
    iget-object v0, p0, LX/CIm;->f:Landroid/widget/ListView;

    iget-object v1, p0, LX/CIm;->f:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getPaddingTop()I

    move-result v1

    iget-object v2, p0, LX/CIm;->f:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getPaddingBottom()I

    move-result v2

    invoke-virtual {v0, v3, v1, v3, v2}, Landroid/widget/ListView;->setPadding(IIII)V

    .line 1874749
    iget-object v0, p0, LX/CIm;->f:Landroid/widget/ListView;

    const v1, 0x10100fb

    invoke-static {p1, v1, v3}, LX/0WH;->e(Landroid/content/Context;II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setSelector(Landroid/graphics/drawable/Drawable;)V

    .line 1874750
    iget-object v0, p0, LX/CIm;->f:Landroid/widget/ListView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOverScrollMode(I)V

    .line 1874751
    iget-object v0, p0, LX/CIm;->f:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/CIm;->f:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v0, v0, Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1874752
    iget-object v0, p0, LX/CIm;->f:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0, v3, v3, v3, v3}, Landroid/view/View;->setPadding(IIII)V

    .line 1874753
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/base/activity/FbPreferenceActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f031308

    invoke-virtual {v0, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, LX/CIm;->b:Landroid/view/ViewGroup;

    .line 1874754
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 1874742
    iget-object v0, p0, LX/CIm;->a:LX/0Zb;

    new-instance v1, Lcom/facebook/katana/settings/SettingsHelper$SettingChangedEvent;

    invoke-direct {v1, p0, p1, p2}, Lcom/facebook/katana/settings/SettingsHelper$SettingChangedEvent;-><init>(LX/CIm;Ljava/lang/String;Ljava/lang/Object;)V

    invoke-interface {v0, v1}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1874743
    return-void
.end method

.method public final b(Lcom/facebook/base/activity/FbPreferenceActivity;)V
    .locals 3

    .prologue
    .line 1874728
    const v0, 0x1020002

    invoke-virtual {p1, v0}, Lcom/facebook/base/activity/FbPreferenceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 1874729
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 1874730
    iget-object v2, p0, LX/CIm;->b:Landroid/view/ViewGroup;

    if-eq v1, v2, :cond_0

    .line 1874731
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 1874732
    iget-object v2, p0, LX/CIm;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1874733
    iget-object v0, p0, LX/CIm;->b:Landroid/view/ViewGroup;

    const v2, 0x7f0d16fb

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1874734
    invoke-static {p1}, LX/63Z;->b(Landroid/app/Activity;)Z

    .line 1874735
    const v0, 0x7f0d00bc

    invoke-virtual {p1, v0}, Lcom/facebook/base/activity/FbPreferenceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    iput-object v0, p0, LX/CIm;->c:LX/0h5;

    .line 1874736
    iget-object v0, p0, LX/CIm;->c:LX/0h5;

    new-instance v1, LX/CIk;

    invoke-direct {v1, p0, p1}, LX/CIk;-><init>(LX/CIm;Lcom/facebook/base/activity/FbPreferenceActivity;)V

    invoke-interface {v0, v1}, LX/0h5;->a(Landroid/view/View$OnClickListener;)V

    .line 1874737
    new-instance v0, Landroid/view/ContextThemeWrapper;

    const v1, 0x7f0e05cf

    invoke-direct {v0, p1, v1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f031309

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 1874738
    const v1, 0x7f0d0a0a

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, LX/CIm;->d:Landroid/widget/TextView;

    .line 1874739
    iget-object v1, p0, LX/CIm;->c:LX/0h5;

    invoke-interface {v1, v0}, LX/0h5;->setCustomTitleView(Landroid/view/View;)V

    .line 1874740
    const v1, 0x7f0d2c3d

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/SwitchCompat;

    iput-object v0, p0, LX/CIm;->e:Lcom/facebook/widget/SwitchCompat;

    .line 1874741
    :cond_0
    return-void
.end method
