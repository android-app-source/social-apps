.class public LX/Bnz;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field public final b:LX/1JH;

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Zb;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lcom/facebook/user/model/User;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:LX/0pG;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public f:LX/0Ym;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public g:LX/0pn;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public h:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public i:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Bo0;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1821393
    const-class v0, LX/Bnz;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Bnz;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/1JH;)V
    .locals 1
    .param p1    # LX/1JH;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1821394
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1821395
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1821396
    iput-object v0, p0, LX/Bnz;->c:LX/0Ot;

    .line 1821397
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1821398
    iput-object v0, p0, LX/Bnz;->i:LX/0Ot;

    .line 1821399
    iput-object p1, p0, LX/Bnz;->b:LX/1JH;

    .line 1821400
    return-void
.end method

.method public static a(LX/Bnz;Lcom/facebook/graphql/model/GraphQLComment;ZZZ)V
    .locals 3

    .prologue
    .line 1821401
    iget-object v0, p0, LX/Bnz;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Zb;

    const-string v1, "ff_live_comment_received"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 1821402
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1821403
    :goto_0
    return-void

    .line 1821404
    :cond_0
    const-string v1, "id"

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->z()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1821405
    const-string v1, "isSelf"

    invoke-virtual {v0, v1, p2}, LX/0oG;->a(Ljava/lang/String;Z)LX/0oG;

    .line 1821406
    const-string v1, "isViewerFriend"

    invoke-virtual {v0, v1, p3}, LX/0oG;->a(Ljava/lang/String;Z)LX/0oG;

    .line 1821407
    const-string v1, "isDedupKeyFound"

    invoke-virtual {v0, v1, p4}, LX/0oG;->a(Ljava/lang/String;Z)LX/0oG;

    .line 1821408
    const-string v1, "candidates"

    iget-object v2, p0, LX/Bnz;->b:LX/1JH;

    .line 1821409
    iget-object p0, v2, LX/1JH;->i:Ljava/util/LinkedHashMap;

    invoke-virtual {p0}, Ljava/util/LinkedHashMap;->size()I

    move-result p0

    move v2, p0

    .line 1821410
    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 1821411
    invoke-virtual {v0}, LX/0oG;->d()V

    goto :goto_0
.end method
