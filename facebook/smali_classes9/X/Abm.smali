.class public final enum LX/Abm;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Abm;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Abm;

.field public static final enum IDLE:LX/Abm;

.field public static final enum SAVED:LX/Abm;

.field public static final enum SAVING_COMPLETED:LX/Abm;

.field public static final enum SAVING_IN_PROGRESS:LX/Abm;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1691354
    new-instance v0, LX/Abm;

    const-string v1, "IDLE"

    invoke-direct {v0, v1, v2}, LX/Abm;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Abm;->IDLE:LX/Abm;

    .line 1691355
    new-instance v0, LX/Abm;

    const-string v1, "SAVING_IN_PROGRESS"

    invoke-direct {v0, v1, v3}, LX/Abm;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Abm;->SAVING_IN_PROGRESS:LX/Abm;

    .line 1691356
    new-instance v0, LX/Abm;

    const-string v1, "SAVING_COMPLETED"

    invoke-direct {v0, v1, v4}, LX/Abm;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Abm;->SAVING_COMPLETED:LX/Abm;

    .line 1691357
    new-instance v0, LX/Abm;

    const-string v1, "SAVED"

    invoke-direct {v0, v1, v5}, LX/Abm;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Abm;->SAVED:LX/Abm;

    .line 1691358
    const/4 v0, 0x4

    new-array v0, v0, [LX/Abm;

    sget-object v1, LX/Abm;->IDLE:LX/Abm;

    aput-object v1, v0, v2

    sget-object v1, LX/Abm;->SAVING_IN_PROGRESS:LX/Abm;

    aput-object v1, v0, v3

    sget-object v1, LX/Abm;->SAVING_COMPLETED:LX/Abm;

    aput-object v1, v0, v4

    sget-object v1, LX/Abm;->SAVED:LX/Abm;

    aput-object v1, v0, v5

    sput-object v0, LX/Abm;->$VALUES:[LX/Abm;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1691351
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Abm;
    .locals 1

    .prologue
    .line 1691352
    const-class v0, LX/Abm;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Abm;

    return-object v0
.end method

.method public static values()[LX/Abm;
    .locals 1

    .prologue
    .line 1691353
    sget-object v0, LX/Abm;->$VALUES:[LX/Abm;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Abm;

    return-object v0
.end method
