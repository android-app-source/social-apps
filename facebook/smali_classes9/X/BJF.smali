.class public final LX/BJF;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/widget/ListView;

.field private final b:LX/0Sh;

.field public final c:LX/9bq;

.field public final d:Ljava/lang/Long;

.field private final e:LX/9b2;

.field public final f:LX/BJB;

.field public final g:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

.field public h:LX/9b1;

.field public final i:I


# direct methods
.method public constructor <init>(Lcom/facebook/ipc/composer/intent/ComposerTargetData;Ljava/lang/Long;Landroid/content/Context;Landroid/widget/ListView;LX/0Sh;LX/9bq;LX/9b2;LX/BJB;)V
    .locals 2

    .prologue
    .line 1771707
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1771708
    iput-object p2, p0, LX/BJF;->d:Ljava/lang/Long;

    .line 1771709
    iput-object p4, p0, LX/BJF;->a:Landroid/widget/ListView;

    .line 1771710
    iput-object p5, p0, LX/BJF;->b:LX/0Sh;

    .line 1771711
    iput-object p6, p0, LX/BJF;->c:LX/9bq;

    .line 1771712
    invoke-virtual {p3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b196f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/BJF;->i:I

    .line 1771713
    iput-object p7, p0, LX/BJF;->e:LX/9b2;

    .line 1771714
    iput-object p8, p0, LX/BJF;->f:LX/BJB;

    .line 1771715
    iput-object p1, p0, LX/BJF;->g:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    .line 1771716
    invoke-direct {p0}, LX/BJF;->a()V

    .line 1771717
    return-void
.end method

.method private a()V
    .locals 7

    .prologue
    .line 1771718
    iget-object v3, p0, LX/BJF;->g:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    iget-object v3, v3, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    sget-object v4, LX/2rw;->GROUP:LX/2rw;

    if-ne v3, v4, :cond_0

    .line 1771719
    iget-object v3, p0, LX/BJF;->c:LX/9bq;

    iget-object v4, p0, LX/BJF;->g:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    iget-wide v5, v4, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    invoke-static {v5, v6}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    iget v5, p0, LX/BJF;->i:I

    invoke-virtual {v3, v4, v5}, LX/9bq;->b(Ljava/lang/String;I)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    .line 1771720
    :goto_0
    move-object v0, v3

    .line 1771721
    new-instance v1, LX/BJE;

    invoke-direct {v1, p0}, LX/BJE;-><init>(LX/BJF;)V

    .line 1771722
    iget-object v2, p0, LX/BJF;->b:LX/0Sh;

    invoke-virtual {v2, v0, v1}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 1771723
    return-void

    :cond_0
    iget-object v3, p0, LX/BJF;->c:LX/9bq;

    iget-object v4, p0, LX/BJF;->d:Ljava/lang/Long;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    iget v5, p0, LX/BJF;->i:I

    invoke-virtual {v3, v4, v5}, LX/9bq;->a(Ljava/lang/String;I)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    goto :goto_0
.end method
