.class public abstract LX/CH6;
.super LX/CH5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "LX/CH5",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0t2;

.field public final b:LX/0So;

.field public final c:LX/0QI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QI",
            "<",
            "Ljava/lang/String;",
            "LX/CH6",
            "<TT;>.CacheUnit;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0tX;LX/0t2;LX/0So;LX/0Uh;)V
    .locals 9

    .prologue
    .line 1866198
    const-wide/16 v6, 0x1e

    sget-object v8, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p4

    move-object v5, p3

    invoke-direct/range {v1 .. v8}, LX/CH6;-><init>(LX/0tX;LX/0t2;LX/0Uh;LX/0So;JLjava/util/concurrent/TimeUnit;)V

    .line 1866199
    return-void
.end method

.method public constructor <init>(LX/0tX;LX/0t2;LX/0Uh;LX/0So;JLjava/util/concurrent/TimeUnit;)V
    .locals 5

    .prologue
    .line 1866200
    invoke-direct {p0, p1, p2, p3}, LX/CH5;-><init>(LX/0tX;LX/0t2;LX/0Uh;)V

    .line 1866201
    iput-object p2, p0, LX/CH6;->a:LX/0t2;

    .line 1866202
    iput-object p4, p0, LX/CH6;->b:LX/0So;

    .line 1866203
    invoke-static {}, LX/0QN;->newBuilder()LX/0QN;

    move-result-object v0

    const-wide/16 v2, 0x5

    invoke-virtual {v0, v2, v3}, LX/0QN;->a(J)LX/0QN;

    move-result-object v0

    invoke-virtual {v0, p5, p6, p7}, LX/0QN;->b(JLjava/util/concurrent/TimeUnit;)LX/0QN;

    move-result-object v0

    invoke-virtual {v0}, LX/0QN;->q()LX/0QI;

    move-result-object v0

    iput-object v0, p0, LX/CH6;->c:LX/0QI;

    .line 1866204
    return-void
.end method


# virtual methods
.method public final a(LX/CGs;LX/0Ve;)V
    .locals 12
    .param p1    # LX/CGs;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # LX/0Ve;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CGs",
            "<",
            "LX/0zO",
            "<TT;>;>;",
            "LX/0Ve",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;>;)V"
        }
    .end annotation

    .prologue
    .line 1866205
    invoke-interface {p1}, LX/CGs;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0zO;

    .line 1866206
    iget-object v1, p0, LX/CH6;->a:LX/0t2;

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0t2;)Ljava/lang/String;

    move-result-object v2

    .line 1866207
    iget-object v1, p0, LX/CH6;->c:LX/0QI;

    invoke-interface {v1, v2}, LX/0QI;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Ciz;

    .line 1866208
    if-eqz v1, :cond_1

    iget-wide v4, v1, LX/Ciz;->b:J

    .line 1866209
    iget-wide v7, v0, LX/0zO;->c:J

    .line 1866210
    iget-object v9, p0, LX/CH6;->b:LX/0So;

    invoke-interface {v9}, LX/0So;->now()J

    move-result-wide v9

    sub-long/2addr v9, v4

    cmp-long v7, v9, v7

    if-gez v7, :cond_2

    const/4 v7, 0x1

    :goto_0
    move v0, v7

    .line 1866211
    if-eqz v0, :cond_1

    .line 1866212
    iget-object v2, v1, LX/Ciz;->a:Ljava/lang/Object;

    .line 1866213
    new-instance v1, Lcom/facebook/graphql/executor/GraphQLResult;

    sget-object v3, LX/0ta;->FROM_CACHE_UP_TO_DATE:LX/0ta;

    iget-object v0, p0, LX/CH6;->b:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v4

    const-string v0, "FROM_MEMORY_CACHE"

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v6

    invoke-direct/range {v1 .. v6}, Lcom/facebook/graphql/executor/GraphQLResult;-><init>(Ljava/lang/Object;LX/0ta;JLjava/util/Set;)V

    .line 1866214
    if-eqz p2, :cond_0

    .line 1866215
    invoke-interface {p2, v1}, LX/0TF;->onSuccess(Ljava/lang/Object;)V

    .line 1866216
    :cond_0
    :goto_1
    return-void

    .line 1866217
    :cond_1
    new-instance v0, LX/Cj0;

    invoke-direct {v0, p0, p2, v2}, LX/Cj0;-><init>(LX/CH6;LX/0Ve;Ljava/lang/String;)V

    invoke-super {p0, p1, v0}, LX/CH5;->a(LX/CGs;LX/0Ve;)V

    goto :goto_1

    :cond_2
    const/4 v7, 0x0

    goto :goto_0
.end method
