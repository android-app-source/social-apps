.class public final enum LX/B9l;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/B9l;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/B9l;

.field public static final enum GRAPH:LX/B9l;

.field public static final enum MQTT:LX/B9l;

.field public static final enum UNKNOWN:LX/B9l;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1752228
    new-instance v0, LX/B9l;

    const-string v1, "MQTT"

    invoke-direct {v0, v1, v2}, LX/B9l;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/B9l;->MQTT:LX/B9l;

    .line 1752229
    new-instance v0, LX/B9l;

    const-string v1, "GRAPH"

    invoke-direct {v0, v1, v3}, LX/B9l;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/B9l;->GRAPH:LX/B9l;

    .line 1752230
    new-instance v0, LX/B9l;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v4}, LX/B9l;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/B9l;->UNKNOWN:LX/B9l;

    .line 1752231
    const/4 v0, 0x3

    new-array v0, v0, [LX/B9l;

    sget-object v1, LX/B9l;->MQTT:LX/B9l;

    aput-object v1, v0, v2

    sget-object v1, LX/B9l;->GRAPH:LX/B9l;

    aput-object v1, v0, v3

    sget-object v1, LX/B9l;->UNKNOWN:LX/B9l;

    aput-object v1, v0, v4

    sput-object v0, LX/B9l;->$VALUES:[LX/B9l;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1752233
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/B9l;
    .locals 1

    .prologue
    .line 1752234
    const-class v0, LX/B9l;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/B9l;

    return-object v0
.end method

.method public static values()[LX/B9l;
    .locals 1

    .prologue
    .line 1752232
    sget-object v0, LX/B9l;->$VALUES:[LX/B9l;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/B9l;

    return-object v0
.end method
