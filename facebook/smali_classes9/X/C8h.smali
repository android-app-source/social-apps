.class public LX/C8h;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/79m;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Uh;

.field private final d:LX/1Sj;

.field private e:LX/1Sa;

.field private f:LX/03V;


# direct methods
.method public constructor <init>(LX/0Ot;LX/1Sj;LX/1Sa;LX/03V;LX/0Uh;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/79m;",
            ">;",
            "LX/1Sj;",
            "LX/1Sa;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1853218
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1853219
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/C8h;->a:Ljava/lang/String;

    .line 1853220
    iput-object p1, p0, LX/C8h;->b:LX/0Ot;

    .line 1853221
    iput-object p2, p0, LX/C8h;->d:LX/1Sj;

    .line 1853222
    iput-object p3, p0, LX/C8h;->e:LX/1Sa;

    .line 1853223
    iput-object p4, p0, LX/C8h;->f:LX/03V;

    .line 1853224
    iput-object p5, p0, LX/C8h;->c:LX/0Uh;

    .line 1853225
    return-void
.end method

.method public static a(LX/0QB;)LX/C8h;
    .locals 9

    .prologue
    .line 1853207
    const-class v1, LX/C8h;

    monitor-enter v1

    .line 1853208
    :try_start_0
    sget-object v0, LX/C8h;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1853209
    sput-object v2, LX/C8h;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1853210
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1853211
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1853212
    new-instance v3, LX/C8h;

    const/16 v4, 0x327a

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-static {v0}, LX/1Sj;->a(LX/0QB;)LX/1Sj;

    move-result-object v5

    check-cast v5, LX/1Sj;

    invoke-static {v0}, LX/1Sa;->a(LX/0QB;)LX/1Sa;

    move-result-object v6

    check-cast v6, LX/1Sa;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v7

    check-cast v7, LX/03V;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v8

    check-cast v8, LX/0Uh;

    invoke-direct/range {v3 .. v8}, LX/C8h;-><init>(LX/0Ot;LX/1Sj;LX/1Sa;LX/03V;LX/0Uh;)V

    .line 1853213
    move-object v0, v3

    .line 1853214
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1853215
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C8h;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1853216
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1853217
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(Landroid/view/View;Lcom/facebook/feed/rows/core/props/FeedProps;LX/5OG;)V
    .locals 2
    .param p2    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "LX/5OG;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1853199
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1853200
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aD()Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;->m()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v0

    .line 1853201
    sget-object v1, LX/C8g;->a:[I

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLSavedState;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 1853202
    const v0, 0x7f081082

    invoke-virtual {p3, v0}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v0

    .line 1853203
    new-instance v1, LX/C8f;

    invoke-direct {v1, p0, p1, p2}, LX/C8f;-><init>(LX/C8h;Landroid/view/View;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-virtual {v0, v1}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1853204
    :goto_0
    return-void

    .line 1853205
    :pswitch_0
    const v0, 0x7f081083

    invoke-virtual {p3, v0}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v0

    .line 1853206
    new-instance v1, LX/C8e;

    invoke-direct {v1, p0, p2}, LX/C8e;-><init>(LX/C8h;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-virtual {v0, v1}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method private static b(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/enums/GraphQLSavedState;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)",
            "Lcom/facebook/graphql/enums/GraphQLSavedState;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1853191
    invoke-static {p0}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 1853192
    if-nez v0, :cond_0

    move-object v0, v1

    .line 1853193
    :goto_0
    return-object v0

    .line 1853194
    :cond_0
    iget-object p0, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, p0

    .line 1853195
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aD()Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    move-result-object v0

    .line 1853196
    if-nez v0, :cond_1

    move-object v0, v1

    .line 1853197
    goto :goto_0

    .line 1853198
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;->m()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/view/View;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 4
    .param p2    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1853181
    iget-object v0, p0, LX/C8h;->c:LX/0Uh;

    const/16 v1, 0x48b

    invoke-virtual {v0, v1}, LX/0Uh;->a(I)LX/03R;

    move-result-object v0

    sget-object v1, LX/03R;->NO:LX/03R;

    if-ne v0, v1, :cond_1

    .line 1853182
    :cond_0
    :goto_0
    return-void

    .line 1853183
    :cond_1
    invoke-static {p2}, LX/C8h;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v0

    .line 1853184
    if-eqz v0, :cond_0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSavedState;->NOT_SAVABLE:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-eq v0, v1, :cond_0

    .line 1853185
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1853186
    new-instance v1, LX/5OG;

    invoke-direct {v1, v0}, LX/5OG;-><init>(Landroid/content/Context;)V

    .line 1853187
    invoke-static {p2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    invoke-direct {p0, p1, v2, v1}, LX/C8h;->a(Landroid/view/View;Lcom/facebook/feed/rows/core/props/FeedProps;LX/5OG;)V

    .line 1853188
    new-instance v2, LX/6WS;

    const/4 v3, 0x1

    invoke-direct {v2, v0, v3}, LX/6WS;-><init>(Landroid/content/Context;I)V

    .line 1853189
    invoke-virtual {v2, v1}, LX/5OM;->a(LX/5OG;)V

    .line 1853190
    invoke-virtual {v2, p1}, LX/0ht;->f(Landroid/view/View;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 3
    .param p1    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1853168
    iget-object v0, p0, LX/C8h;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/79m;

    invoke-virtual {v0}, LX/79m;->a()V

    .line 1853169
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1853170
    instance-of v0, v0, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_0

    .line 1853171
    iget-object v0, p0, LX/C8h;->d:LX/1Sj;

    const-string v1, "long_press"

    const-string v2, "native_story"

    invoke-virtual {v0, p1, v1, v2}, LX/1Sj;->b(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Ljava/lang/String;)V

    .line 1853172
    :goto_0
    return-void

    .line 1853173
    :cond_0
    iget-object v0, p0, LX/C8h;->f:LX/03V;

    iget-object v1, p0, LX/C8h;->a:Ljava/lang/String;

    const-string v2, "Unsaving something that isn\'t a Story. Item not unsaved."

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b(Landroid/view/View;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 3
    .param p2    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1853174
    iget-object v0, p0, LX/C8h;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/79m;

    invoke-virtual {v0, p1}, LX/79m;->a(Landroid/view/View;)V

    .line 1853175
    iget-object v0, p0, LX/C8h;->e:LX/1Sa;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1Sa;->a(Landroid/content/Context;)V

    .line 1853176
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1853177
    instance-of v0, v0, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_0

    .line 1853178
    iget-object v0, p0, LX/C8h;->d:LX/1Sj;

    const-string v1, "long_press"

    const-string v2, "native_story"

    invoke-virtual {v0, p2, v1, v2}, LX/1Sj;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Ljava/lang/String;)V

    .line 1853179
    :goto_0
    return-void

    .line 1853180
    :cond_0
    iget-object v0, p0, LX/C8h;->f:LX/03V;

    iget-object v1, p0, LX/C8h;->a:Ljava/lang/String;

    const-string v2, "Saving something that isn\'t a Story. Item was not saved."

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
