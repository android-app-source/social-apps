.class public final enum LX/BKh;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/BKh;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/BKh;

.field public static final enum BIG_BOTTOM:LX/BKh;

.field public static final enum BOTTOM:LX/BKh;

.field public static final enum TOP:LX/BKh;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1774109
    new-instance v0, LX/BKh;

    const-string v1, "TOP"

    invoke-direct {v0, v1, v2}, LX/BKh;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BKh;->TOP:LX/BKh;

    .line 1774110
    new-instance v0, LX/BKh;

    const-string v1, "BOTTOM"

    invoke-direct {v0, v1, v3}, LX/BKh;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BKh;->BOTTOM:LX/BKh;

    .line 1774111
    new-instance v0, LX/BKh;

    const-string v1, "BIG_BOTTOM"

    invoke-direct {v0, v1, v4}, LX/BKh;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BKh;->BIG_BOTTOM:LX/BKh;

    .line 1774112
    const/4 v0, 0x3

    new-array v0, v0, [LX/BKh;

    sget-object v1, LX/BKh;->TOP:LX/BKh;

    aput-object v1, v0, v2

    sget-object v1, LX/BKh;->BOTTOM:LX/BKh;

    aput-object v1, v0, v3

    sget-object v1, LX/BKh;->BIG_BOTTOM:LX/BKh;

    aput-object v1, v0, v4

    sput-object v0, LX/BKh;->$VALUES:[LX/BKh;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1774108
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/BKh;
    .locals 1

    .prologue
    .line 1774107
    const-class v0, LX/BKh;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/BKh;

    return-object v0
.end method

.method public static values()[LX/BKh;
    .locals 1

    .prologue
    .line 1774106
    sget-object v0, LX/BKh;->$VALUES:[LX/BKh;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/BKh;

    return-object v0
.end method
