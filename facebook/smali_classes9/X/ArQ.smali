.class public final enum LX/ArQ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/ArQ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/ArQ;

.field public static final enum CAMERA:LX/ArQ;

.field public static final enum NEWSFEED:LX/ArQ;


# instance fields
.field public final index:I

.field private final tag:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1719099
    new-instance v0, LX/ArQ;

    const-string v1, "CAMERA"

    const-string v2, "camera"

    invoke-direct {v0, v1, v3, v2, v3}, LX/ArQ;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, LX/ArQ;->CAMERA:LX/ArQ;

    .line 1719100
    new-instance v0, LX/ArQ;

    const-string v1, "NEWSFEED"

    const-string v2, "newsfeed"

    invoke-direct {v0, v1, v4, v2, v4}, LX/ArQ;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, LX/ArQ;->NEWSFEED:LX/ArQ;

    .line 1719101
    const/4 v0, 0x2

    new-array v0, v0, [LX/ArQ;

    sget-object v1, LX/ArQ;->CAMERA:LX/ArQ;

    aput-object v1, v0, v3

    sget-object v1, LX/ArQ;->NEWSFEED:LX/ArQ;

    aput-object v1, v0, v4

    sput-object v0, LX/ArQ;->$VALUES:[LX/ArQ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 1719102
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1719103
    iput-object p3, p0, LX/ArQ;->tag:Ljava/lang/String;

    .line 1719104
    iput p4, p0, LX/ArQ;->index:I

    .line 1719105
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/ArQ;
    .locals 1

    .prologue
    .line 1719106
    const-class v0, LX/ArQ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/ArQ;

    return-object v0
.end method

.method public static values()[LX/ArQ;
    .locals 1

    .prologue
    .line 1719107
    sget-object v0, LX/ArQ;->$VALUES:[LX/ArQ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/ArQ;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1719108
    iget-object v0, p0, LX/ArQ;->tag:Ljava/lang/String;

    return-object v0
.end method
