.class public final LX/B8O;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:LX/B8R;


# direct methods
.method public constructor <init>(LX/B8R;)V
    .locals 0

    .prologue
    .line 1748780
    iput-object p1, p0, LX/B8O;->a:LX/B8R;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 1

    .prologue
    .line 1748770
    iget-object v0, p0, LX/B8O;->a:LX/B8R;

    iget-object v0, v0, LX/B8R;->g:LX/B7w;

    if-eqz v0, :cond_0

    .line 1748771
    iget-object v0, p0, LX/B8O;->a:LX/B8R;

    iget-object v0, v0, LX/B8R;->f:Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->j()Ljava/lang/String;

    iget-object v0, p0, LX/B8O;->a:LX/B8R;

    invoke-virtual {v0}, LX/B8R;->getInputValue()Ljava/lang/String;

    .line 1748772
    :cond_0
    iget-object v0, p0, LX/B8O;->a:LX/B8R;

    .line 1748773
    iget-object p1, v0, LX/B8R;->i:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p1}, Lcom/facebook/widget/text/BetterTextView;->getVisibility()I

    move-result p1

    if-nez p1, :cond_2

    const/4 p1, 0x1

    :goto_0
    move v0, p1

    .line 1748774
    if-eqz v0, :cond_1

    .line 1748775
    iget-object v0, p0, LX/B8O;->a:LX/B8R;

    invoke-static {v0}, LX/B8R;->f(LX/B8R;)V

    .line 1748776
    iget-object v0, p0, LX/B8O;->a:LX/B8R;

    iget-object v0, v0, LX/B8R;->i:Lcom/facebook/widget/text/BetterTextView;

    invoke-static {v0}, LX/B8v;->a(Landroid/widget/TextView;)V

    .line 1748777
    :cond_1
    return-void

    :cond_2
    const/4 p1, 0x0

    goto :goto_0
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1748779
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1748778
    return-void
.end method
