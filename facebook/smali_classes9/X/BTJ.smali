.class public LX/BTJ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:I

.field private final b:I

.field public c:Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;

.field public d:Landroid/view/View;

.field public e:Landroid/view/View;

.field public f:Landroid/view/View;

.field public g:Landroid/view/View;

.field public h:Landroid/view/View;


# direct methods
.method public constructor <init>(II)V
    .locals 0

    .prologue
    .line 1787097
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1787098
    iput p1, p0, LX/BTJ;->a:I

    .line 1787099
    iput p2, p0, LX/BTJ;->b:I

    .line 1787100
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 1787081
    iget-object v0, p0, LX/BTJ;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v0

    iget v1, p0, LX/BTJ;->a:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public final a(LX/BTI;)I
    .locals 1

    .prologue
    .line 1787094
    sget-object v0, LX/BTI;->LEFT:LX/BTI;

    if-ne p1, v0, :cond_0

    .line 1787095
    invoke-virtual {p0}, LX/BTJ;->a()I

    move-result v0

    .line 1787096
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, LX/BTJ;->b()I

    move-result v0

    goto :goto_0
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 1787092
    invoke-virtual {p0}, LX/BTJ;->a()I

    move-result v0

    add-int/2addr v0, p1

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/BTJ;->a(IZ)V

    .line 1787093
    return-void
.end method

.method public final a(IZ)V
    .locals 3

    .prologue
    .line 1787101
    iget-object v0, p0, LX/BTJ;->d:Landroid/view/View;

    invoke-virtual {p0}, LX/BTJ;->a()I

    move-result v1

    sub-int v1, p1, v1

    invoke-virtual {v0, v1}, Landroid/view/View;->offsetLeftAndRight(I)V

    .line 1787102
    iget-object v0, p0, LX/BTJ;->f:Landroid/view/View;

    iget-object v1, p0, LX/BTJ;->d:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v1

    iget-object v2, p0, LX/BTJ;->d:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/view/View;->setRight(I)V

    .line 1787103
    iget-object v0, p0, LX/BTJ;->h:Landroid/view/View;

    iget-object v1, p0, LX/BTJ;->d:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v1

    iget v2, p0, LX/BTJ;->a:I

    add-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/view/View;->setLeft(I)V

    .line 1787104
    if-eqz p2, :cond_0

    .line 1787105
    iget-object v0, p0, LX/BTJ;->f:Landroid/view/View;

    iget-object v1, p0, LX/BTJ;->c:Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;

    invoke-virtual {v1}, Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;->getZoomedInLeftOffset()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setLeft(I)V

    .line 1787106
    :goto_0
    return-void

    .line 1787107
    :cond_0
    iget-object v0, p0, LX/BTJ;->f:Landroid/view/View;

    iget v1, p0, LX/BTJ;->b:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setLeft(I)V

    goto :goto_0
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 1787091
    iget-object v0, p0, LX/BTJ;->e:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v0

    iget v1, p0, LX/BTJ;->a:I

    add-int/2addr v0, v1

    return v0
.end method

.method public final b(I)V
    .locals 2

    .prologue
    .line 1787089
    invoke-virtual {p0}, LX/BTJ;->b()I

    move-result v0

    add-int/2addr v0, p1

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/BTJ;->b(IZ)V

    .line 1787090
    return-void
.end method

.method public final b(IZ)V
    .locals 3

    .prologue
    .line 1787082
    iget-object v0, p0, LX/BTJ;->e:Landroid/view/View;

    invoke-virtual {p0}, LX/BTJ;->b()I

    move-result v1

    sub-int v1, p1, v1

    invoke-virtual {v0, v1}, Landroid/view/View;->offsetLeftAndRight(I)V

    .line 1787083
    iget-object v0, p0, LX/BTJ;->g:Landroid/view/View;

    iget-object v1, p0, LX/BTJ;->e:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    move-result v1

    iget-object v2, p0, LX/BTJ;->e:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/view/View;->setLeft(I)V

    .line 1787084
    iget-object v0, p0, LX/BTJ;->h:Landroid/view/View;

    iget-object v1, p0, LX/BTJ;->e:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    move-result v1

    iget v2, p0, LX/BTJ;->a:I

    sub-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/view/View;->setRight(I)V

    .line 1787085
    if-eqz p2, :cond_0

    .line 1787086
    iget-object v0, p0, LX/BTJ;->g:Landroid/view/View;

    iget-object v1, p0, LX/BTJ;->c:Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;

    invoke-virtual {v1}, Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;->getZoomedInWidth()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setRight(I)V

    .line 1787087
    :goto_0
    return-void

    .line 1787088
    :cond_0
    iget-object v0, p0, LX/BTJ;->g:Landroid/view/View;

    iget-object v1, p0, LX/BTJ;->c:Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;

    invoke-virtual {v1}, Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;->getZoomedOutWidth()I

    move-result v1

    iget v2, p0, LX/BTJ;->b:I

    sub-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/view/View;->setRight(I)V

    goto :goto_0
.end method
