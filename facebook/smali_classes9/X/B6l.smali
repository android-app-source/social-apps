.class public LX/B6l;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static volatile g:LX/B6l;


# instance fields
.field private b:LX/0Zb;

.field private c:LX/0lF;

.field private d:Z

.field private e:I

.field private f:LX/03V;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1747316
    const-class v0, LX/B6l;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/B6l;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Zb;LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1747312
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1747313
    iput-object p1, p0, LX/B6l;->b:LX/0Zb;

    .line 1747314
    iput-object p2, p0, LX/B6l;->f:LX/03V;

    .line 1747315
    return-void
.end method

.method public static a(LX/0QB;)LX/B6l;
    .locals 5

    .prologue
    .line 1747299
    sget-object v0, LX/B6l;->g:LX/B6l;

    if-nez v0, :cond_1

    .line 1747300
    const-class v1, LX/B6l;

    monitor-enter v1

    .line 1747301
    :try_start_0
    sget-object v0, LX/B6l;->g:LX/B6l;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1747302
    if-eqz v2, :cond_0

    .line 1747303
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1747304
    new-instance p0, LX/B6l;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-direct {p0, v3, v4}, LX/B6l;-><init>(LX/0Zb;LX/03V;)V

    .line 1747305
    move-object v0, p0

    .line 1747306
    sput-object v0, LX/B6l;->g:LX/B6l;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1747307
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1747308
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1747309
    :cond_1
    sget-object v0, LX/B6l;->g:LX/B6l;

    return-object v0

    .line 1747310
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1747311
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private b(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 1747286
    iget-object v0, p0, LX/B6l;->c:LX/0lF;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/B6l;->c:LX/0lF;

    invoke-virtual {v0}, LX/0lF;->e()I

    move-result v0

    if-nez v0, :cond_2

    .line 1747287
    :cond_0
    const/4 v0, 0x0

    .line 1747288
    :cond_1
    :goto_0
    return-object v0

    .line 1747289
    :cond_2
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "lead_gen"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "tracking"

    iget-object v2, p0, LX/B6l;->c:LX/0lF;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    iget-boolean v1, p0, LX/B6l;->d:Z

    invoke-virtual {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "native_newsfeed"

    .line 1747290
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1747291
    move-object v0, v0

    .line 1747292
    if-eqz p1, :cond_4

    .line 1747293
    const-string v1, "1"

    invoke-virtual {v0, p1, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1747294
    :goto_1
    if-eq p2, v3, :cond_3

    .line 1747295
    const-string v1, "page_index"

    invoke-virtual {v0, v1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1747296
    :cond_3
    iget v1, p0, LX/B6l;->e:I

    if-eq v1, v3, :cond_1

    .line 1747297
    const-string v1, "item_index"

    iget v2, p0, LX/B6l;->e:I

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_0

    .line 1747298
    :cond_4
    const-string v1, "cta_click"

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_1
.end method


# virtual methods
.method public final a(LX/0lF;ZI)V
    .locals 0

    .prologue
    .line 1747282
    iput-object p1, p0, LX/B6l;->c:LX/0lF;

    .line 1747283
    iput-boolean p2, p0, LX/B6l;->d:Z

    .line 1747284
    iput p3, p0, LX/B6l;->e:I

    .line 1747285
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1747277
    const/4 v0, -0x1

    invoke-direct {p0, p1, v0}, LX/B6l;->b(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 1747278
    if-nez v0, :cond_0

    .line 1747279
    iget-object v0, p0, LX/B6l;->f:LX/03V;

    sget-object v1, LX/B6l;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Honey Client Event is null as there are no Tracking codes in source: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1747280
    :goto_0
    return-void

    .line 1747281
    :cond_0
    iget-object v1, p0, LX/B6l;->b:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 4

    .prologue
    .line 1747272
    invoke-direct {p0, p1, p2}, LX/B6l;->b(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 1747273
    if-nez v0, :cond_0

    .line 1747274
    iget-object v0, p0, LX/B6l;->f:LX/03V;

    sget-object v1, LX/B6l;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Honey Client Event is null as there are no Tracking codes in source: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1747275
    :goto_0
    return-void

    .line 1747276
    :cond_0
    iget-object v1, p0, LX/B6l;->b:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 4

    .prologue
    .line 1747266
    invoke-direct {p0, p1, p3}, LX/B6l;->b(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 1747267
    if-nez v0, :cond_0

    .line 1747268
    iget-object v0, p0, LX/B6l;->f:LX/03V;

    sget-object v1, LX/B6l;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Honey Client Event is null as there are no Tracking codes in source: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1747269
    :goto_0
    return-void

    .line 1747270
    :cond_0
    const-string v1, "field_key"

    invoke-virtual {v0, v1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1747271
    iget-object v1, p0, LX/B6l;->b:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    goto :goto_0
.end method
