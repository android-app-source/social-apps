.class public final LX/C1H;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/17z;
.implements LX/0jQ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/17z",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "LX/0jQ;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:Z

.field public final b:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Lcom/facebook/graphql/model/GraphQLQuestionOption;

.field public final d:I

.field public final e:Landroid/view/View$OnClickListener;

.field public final f:Landroid/view/View$OnClickListener;

.field public final g:LX/C1E;

.field public final h:Z


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLQuestionOption;IZLX/C1E;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLQuestionOption;",
            "IZ",
            "LX/C1E;",
            "Landroid/view/View$OnClickListener;",
            "Landroid/view/View$OnClickListener;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 1842236
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1842237
    iput-object p1, p0, LX/C1H;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1842238
    iput p3, p0, LX/C1H;->d:I

    .line 1842239
    iput-object p2, p0, LX/C1H;->c:Lcom/facebook/graphql/model/GraphQLQuestionOption;

    .line 1842240
    iput-boolean p4, p0, LX/C1H;->a:Z

    .line 1842241
    iput-object p6, p0, LX/C1H;->e:Landroid/view/View$OnClickListener;

    .line 1842242
    iput-object p7, p0, LX/C1H;->f:Landroid/view/View$OnClickListener;

    .line 1842243
    iput-object p5, p0, LX/C1H;->g:LX/C1E;

    .line 1842244
    iput-boolean p8, p0, LX/C1H;->h:Z

    .line 1842245
    return-void
.end method


# virtual methods
.method public final c()Lcom/facebook/graphql/model/FeedUnit;
    .locals 1

    .prologue
    .line 1842234
    iget-object v0, p0, LX/C1H;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    return-object v0
.end method

.method public final g()Lcom/facebook/feed/rows/core/props/FeedProps;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1842235
    iget-object v0, p0, LX/C1H;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    return-object v0
.end method
