.class public final LX/ATT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/BTC;

.field public final synthetic b:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;LX/BTC;)V
    .locals 0

    .prologue
    .line 1676041
    iput-object p1, p0, LX/ATT;->b:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iput-object p2, p0, LX/ATT;->a:LX/BTC;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v0, 0x1

    const v1, -0x5c3ef251

    invoke-static {v7, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v6

    .line 1676042
    iget-object v0, p0, LX/ATT;->b:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v0, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->f()Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    move-result-object v0

    .line 1676043
    if-nez v0, :cond_0

    .line 1676044
    invoke-static {}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->newBuilder()Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData$Builder;->a()Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    move-result-object v0

    move-object v1, v0

    .line 1676045
    :goto_0
    new-instance v2, LX/5SM;

    iget-object v0, p0, LX/ATT;->b:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v0, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aK:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;

    invoke-direct {v2, v0}, LX/5SM;-><init>(Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;)V

    .line 1676046
    iput-object v1, v2, LX/5SM;->c:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    .line 1676047
    move-object v0, v2

    .line 1676048
    iget-object v1, p0, LX/ATT;->b:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v1, v1, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v1}, Lcom/facebook/composer/attachments/ComposerAttachment;->h()Ljava/lang/String;

    move-result-object v1

    .line 1676049
    iput-object v1, v0, LX/5SM;->q:Ljava/lang/String;

    .line 1676050
    move-object v0, v0

    .line 1676051
    invoke-virtual {v0}, LX/5SM;->a()Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;

    move-result-object v1

    .line 1676052
    iget-object v0, p0, LX/ATT;->b:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v0, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->y:LX/BTG;

    iget-object v2, p0, LX/ATT;->b:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v2, v2, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v2}, Lcom/facebook/composer/attachments/ComposerAttachment;->c()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/ATT;->a:LX/BTC;

    iget-object v4, p0, LX/ATT;->b:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v4, v4, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->al:Ljava/lang/String;

    iget-object v5, p0, LX/ATT;->b:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v5, v5, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v5}, Lcom/facebook/composer/attachments/ComposerAttachment;->h()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v2, v3, v4, v5}, LX/BTG;->a(Ljava/lang/String;LX/BTC;Ljava/lang/String;Ljava/lang/String;)V

    .line 1676053
    iget-object v0, p0, LX/ATT;->b:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v0, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->z:LX/BSu;

    invoke-virtual {v0}, LX/BSu;->a()V

    .line 1676054
    iget-object v0, p0, LX/ATT;->b:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v0, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ay:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BSt;

    iget-object v2, p0, LX/ATT;->b:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v2, v2, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v2}, Lcom/facebook/composer/attachments/ComposerAttachment;->c()Landroid/net/Uri;

    move-result-object v2

    iget-object v3, p0, LX/ATT;->b:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v3, v3, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->N:LX/ATX;

    const-string v4, "composer"

    iget-object v5, p0, LX/ATT;->b:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v5, v5, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->S:Landroid/widget/FrameLayout;

    invoke-static {v5}, LX/9fh;->a(Landroid/view/View;)LX/9fh;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, LX/BSt;->a(Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;Landroid/net/Uri;LX/ATX;Ljava/lang/String;LX/9fh;)V

    .line 1676055
    const v0, -0x1888a53a

    invoke-static {v7, v7, v0, v6}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    :cond_0
    move-object v1, v0

    goto :goto_0
.end method
