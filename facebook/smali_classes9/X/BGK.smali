.class public final LX/BGK;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public b:LX/0Pz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Pz",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public c:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1766979
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1766980
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    iput-object v0, p0, LX/BGK;->b:LX/0Pz;

    .line 1766981
    return-void
.end method

.method public constructor <init>(Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;)V
    .locals 4

    .prologue
    .line 1766967
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1766968
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    iput-object v0, p0, LX/BGK;->b:LX/0Pz;

    .line 1766969
    invoke-virtual {p1}, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;->getMaskModel()Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$MaskModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$MaskModel;->k()Ljava/lang/String;

    move-result-object v0

    .line 1766970
    iput-object v0, p0, LX/BGK;->a:Ljava/lang/String;

    .line 1766971
    invoke-virtual {p1}, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;->getMaskModel()Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$MaskModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$MaskModel;->j()LX/2uF;

    move-result-object v0

    invoke-virtual {v0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, LX/2sN;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/2sN;->b()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 1766972
    const/4 v3, 0x1

    invoke-virtual {v2, v1, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1766973
    iget-object v2, p0, LX/BGK;->b:LX/0Pz;

    invoke-virtual {v2, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1766974
    goto :goto_0

    .line 1766975
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;->getMaskModel()Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$MaskModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$MaskModel;->m()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 1766976
    invoke-virtual {p1}, Lcom/facebook/videocodec/effects/model/MsqrdGLConfig;->getMaskModel()Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$MaskModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/graphql/MaskGraphQLModels$MaskModel;->m()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 1766977
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, LX/BGK;->c:Ljava/lang/String;

    .line 1766978
    :cond_1
    return-void
.end method
