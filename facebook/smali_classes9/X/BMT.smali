.class public LX/BMT;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1777343
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/1Ri;)Z
    .locals 2

    .prologue
    .line 1777344
    iget-object v0, p0, LX/1Ri;->a:LX/1RN;

    iget-object v0, v0, LX/1RN;->b:LX/1lP;

    iget-object v0, v0, LX/1lP;->e:Ljava/lang/String;

    .line 1777345
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPromptConfidence;->VERY_HIGH:Lcom/facebook/graphql/enums/GraphQLPromptConfidence;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLPromptConfidence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPromptConfidence;->HIGH:Lcom/facebook/graphql/enums/GraphQLPromptConfidence;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLPromptConfidence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/1Ri;)Z
    .locals 1

    .prologue
    .line 1777346
    invoke-static {p0}, LX/BMT;->a(LX/1Ri;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
