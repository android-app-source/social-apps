.class public LX/ByS;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pt;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/ByT;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/ByS",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/ByT;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1837359
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1837360
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/ByS;->b:LX/0Zi;

    .line 1837361
    iput-object p1, p0, LX/ByS;->a:LX/0Ot;

    .line 1837362
    return-void
.end method

.method public static a(LX/0QB;)LX/ByS;
    .locals 4

    .prologue
    .line 1837363
    const-class v1, LX/ByS;

    monitor-enter v1

    .line 1837364
    :try_start_0
    sget-object v0, LX/ByS;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1837365
    sput-object v2, LX/ByS;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1837366
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1837367
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1837368
    new-instance v3, LX/ByS;

    const/16 p0, 0x1e28

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/ByS;-><init>(LX/0Ot;)V

    .line 1837369
    move-object v0, v3

    .line 1837370
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1837371
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/ByS;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1837372
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1837373
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 2

    .prologue
    .line 1837374
    check-cast p2, LX/ByR;

    .line 1837375
    iget-object v0, p0, LX/ByS;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v0, p2, LX/ByR;->a:LX/1aZ;

    iget v1, p2, LX/ByR;->b:F

    .line 1837376
    invoke-static {p1}, LX/1um;->c(LX/1De;)LX/1up;

    move-result-object p0

    invoke-virtual {p0, v0}, LX/1up;->a(LX/1aZ;)LX/1up;

    move-result-object p0

    invoke-virtual {p0, v1}, LX/1up;->c(F)LX/1up;

    move-result-object p0

    .line 1837377
    invoke-virtual {p0}, LX/1X5;->c()LX/1Di;

    move-result-object p0

    const/high16 p2, 0x3f800000    # 1.0f

    invoke-interface {p0, p2}, LX/1Di;->a(F)LX/1Di;

    move-result-object p0

    invoke-interface {p0}, LX/1Di;->k()LX/1Dg;

    move-result-object p0

    move-object v0, p0

    .line 1837378
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1837379
    invoke-static {}, LX/1dS;->b()V

    .line 1837380
    const/4 v0, 0x0

    return-object v0
.end method
