.class public final LX/BKM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0io;
.implements LX/0iv;
.implements LX/5Qw;
.implements LX/5Qz;
.implements LX/5R0;
.implements LX/0j0;
.implements LX/0ik;
.implements LX/0il;
.implements LX/0j3;
.implements LX/0j4;
.implements LX/0j6;
.implements LX/5RE;
.implements LX/0jA;


# instance fields
.field public final synthetic a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V
    .locals 0

    .prologue
    .line 1772836
    iput-object p1, p0, LX/BKM;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final C()Z
    .locals 1

    .prologue
    .line 1772835
    const/4 v0, 0x1

    return v0
.end method

.method public final D()Z
    .locals 1

    .prologue
    .line 1772834
    const/4 v0, 0x0

    return v0
.end method

.method public final I()LX/5RF;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1772816
    iget-object v0, p0, LX/BKM;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v0, v0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v0, v0, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v0}, Lcom/facebook/platform/composer/model/PlatformComposition;->p()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v0

    .line 1772817
    invoke-virtual {p0}, LX/BKM;->getAttachments()LX/0Px;

    move-result-object v1

    .line 1772818
    iget-object v2, p0, LX/BKM;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v2, v2, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v2, v2, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v2}, Lcom/facebook/platform/composer/model/PlatformComposition;->o()Lcom/facebook/ipc/composer/model/ComposerStickerData;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1772819
    sget-object v0, LX/5RF;->STICKER:LX/5RF;

    .line 1772820
    :goto_0
    return-object v0

    .line 1772821
    :cond_0
    invoke-static {v0}, LX/9J0;->a(Lcom/facebook/ipc/composer/intent/ComposerShareParams;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1772822
    sget-object v0, LX/5RF;->GIF_VIDEO:LX/5RF;

    goto :goto_0

    .line 1772823
    :cond_1
    if-eqz v0, :cond_2

    .line 1772824
    sget-object v0, LX/5RF;->SHARE_ATTACHMENT:LX/5RF;

    goto :goto_0

    .line 1772825
    :cond_2
    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1772826
    sget-object v0, LX/5RF;->NO_ATTACHMENTS:LX/5RF;

    goto :goto_0

    .line 1772827
    :cond_3
    invoke-static {v1}, LX/7kq;->k(LX/0Px;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1772828
    sget-object v0, LX/5RF;->GIF_VIDEO:LX/5RF;

    goto :goto_0

    .line 1772829
    :cond_4
    invoke-static {v1}, LX/7kq;->o(LX/0Px;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1772830
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v0

    if-ne v0, v3, :cond_5

    sget-object v0, LX/5RF;->SINGLE_VIDEO:LX/5RF;

    goto :goto_0

    :cond_5
    sget-object v0, LX/5RF;->MULTIPLE_VIDEOS:LX/5RF;

    goto :goto_0

    .line 1772831
    :cond_6
    invoke-static {v1}, LX/7kq;->m(LX/0Px;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1772832
    sget-object v0, LX/5RF;->MULTIMEDIA:LX/5RF;

    goto :goto_0

    .line 1772833
    :cond_7
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v0

    if-ne v0, v3, :cond_8

    sget-object v0, LX/5RF;->SINGLE_PHOTO:LX/5RF;

    goto :goto_0

    :cond_8
    sget-object v0, LX/5RF;->MULTIPLE_PHOTOS:LX/5RF;

    goto :goto_0
.end method

.method public final a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1772815
    return-object p0
.end method

.method public final b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1772814
    return-object p0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1772813
    const/4 v0, 0x1

    return v0
.end method

.method public final getAttachments()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1772812
    iget-object v0, p0, LX/BKM;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v0, v0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v0, v0, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v0}, Lcom/facebook/platform/composer/model/PlatformComposition;->e()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;
    .locals 1

    .prologue
    .line 1772806
    iget-object v0, p0, LX/BKM;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v0, v0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v0, v0, LX/BKm;->b:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    return-object v0
.end method

.method public final getPageData()Lcom/facebook/ipc/composer/intent/ComposerPageData;
    .locals 1

    .prologue
    .line 1772811
    iget-object v0, p0, LX/BKM;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v0, v0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v0, v0, LX/BKm;->i:Lcom/facebook/ipc/composer/intent/ComposerPageData;

    return-object v0
.end method

.method public final getSessionId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1772810
    iget-object v0, p0, LX/BKM;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v0, v0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v0, v0, LX/BKm;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final getSlideshowData()Lcom/facebook/ipc/composer/model/ComposerSlideshowData;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 1772809
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;
    .locals 1

    .prologue
    .line 1772808
    iget-object v0, p0, LX/BKM;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v0, v0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v0, v0, LX/BKm;->e:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    return-object v0
.end method

.method public final s()Z
    .locals 1

    .prologue
    .line 1772807
    const/4 v0, 0x0

    return v0
.end method
