.class public final LX/CeP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Vj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Vj",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/placetips/settings/graphql/GravitySettingsQueryModels$GravitySettingsQueryModel;",
        ">;",
        "Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentInterfaces$GravitySettingsGraphQlFragment;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/CeT;


# direct methods
.method public constructor <init>(LX/CeT;)V
    .locals 0

    .prologue
    .line 1924622
    iput-object p1, p0, LX/CeP;->a:LX/CeT;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1924623
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1924624
    if-nez p1, :cond_0

    .line 1924625
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "result is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1924626
    :cond_0
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1924627
    check-cast v0, Lcom/facebook/placetips/settings/graphql/GravitySettingsQueryModels$GravitySettingsQueryModel;

    .line 1924628
    if-nez v0, :cond_1

    .line 1924629
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "result.getResult is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1924630
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/placetips/settings/graphql/GravitySettingsQueryModels$GravitySettingsQueryModel;->a()Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;

    move-result-object v0

    .line 1924631
    if-nez v0, :cond_2

    .line 1924632
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "result\'s gravity_settings is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1924633
    :cond_2
    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
