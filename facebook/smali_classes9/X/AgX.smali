.class public LX/AgX;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1700990
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/util/Queue;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Queue",
            "<",
            "Ljava/lang/ref/SoftReference",
            "<TE;>;>;)TE;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1700991
    invoke-interface {p0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/SoftReference;

    .line 1700992
    :goto_0
    if-eqz v0, :cond_1

    .line 1700993
    invoke-virtual {v0}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1700994
    invoke-virtual {v0}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v0

    .line 1700995
    :goto_1
    return-object v0

    .line 1700996
    :cond_0
    invoke-interface {p0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/SoftReference;

    goto :goto_0

    .line 1700997
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static a(Landroid/graphics/drawable/Drawable;III)V
    .locals 4

    .prologue
    .line 1700998
    div-int/lit8 v0, p3, 0x2

    sub-int v0, p1, v0

    div-int/lit8 v1, p3, 0x2

    sub-int v1, p2, v1

    div-int/lit8 v2, p3, 0x2

    add-int/2addr v2, p1

    div-int/lit8 v3, p3, 0x2

    add-int/2addr v3, p2

    invoke-virtual {p0, v0, v1, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1700999
    return-void
.end method
