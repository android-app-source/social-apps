.class public final LX/BwI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:Lcom/facebook/feed/video/fullscreen/WatchAndMoreFullscreenVideoControlsPlugin;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/video/fullscreen/WatchAndMoreFullscreenVideoControlsPlugin;Z)V
    .locals 0

    .prologue
    .line 1833476
    iput-object p1, p0, LX/BwI;->b:Lcom/facebook/feed/video/fullscreen/WatchAndMoreFullscreenVideoControlsPlugin;

    iput-boolean p2, p0, LX/BwI;->a:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v0, 0x1

    const v1, -0x53b26bf9

    invoke-static {v9, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v8

    .line 1833477
    iget-object v0, p0, LX/BwI;->b:Lcom/facebook/feed/video/fullscreen/WatchAndMoreFullscreenVideoControlsPlugin;

    iget-object v0, v0, LX/2oy;->j:LX/2pb;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/BwI;->a:Z

    if-eqz v0, :cond_0

    .line 1833478
    iget-object v0, p0, LX/BwI;->b:Lcom/facebook/feed/video/fullscreen/WatchAndMoreFullscreenVideoControlsPlugin;

    iget-object v0, v0, LX/2oy;->j:LX/2pb;

    sget-object v1, LX/04g;->BY_USER:LX/04g;

    invoke-virtual {v0, v1}, LX/2pb;->b(LX/04g;)V

    .line 1833479
    iget-object v0, p0, LX/BwI;->b:Lcom/facebook/feed/video/fullscreen/WatchAndMoreFullscreenVideoControlsPlugin;

    iget-object v0, v0, Lcom/facebook/feed/video/fullscreen/WatchAndMoreFullscreenVideoControlsPlugin;->o:LX/1C2;

    iget-object v1, p0, LX/BwI;->b:Lcom/facebook/feed/video/fullscreen/WatchAndMoreFullscreenVideoControlsPlugin;

    iget-object v1, v1, Lcom/facebook/feed/video/fullscreen/WatchAndMoreFullscreenVideoControlsPlugin;->s:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v1, v1, Lcom/facebook/video/engine/VideoPlayerParams;->e:LX/162;

    iget-object v2, p0, LX/BwI;->b:Lcom/facebook/feed/video/fullscreen/WatchAndMoreFullscreenVideoControlsPlugin;

    iget-object v2, v2, Lcom/facebook/feed/video/fullscreen/WatchAndMoreFullscreenVideoControlsPlugin;->s:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v2, v2, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iget-object v3, p0, LX/BwI;->b:Lcom/facebook/feed/video/fullscreen/WatchAndMoreFullscreenVideoControlsPlugin;

    iget-object v3, v3, LX/2oy;->j:LX/2pb;

    invoke-virtual {v3}, LX/2pb;->h()I

    move-result v3

    iget-object v4, p0, LX/BwI;->b:Lcom/facebook/feed/video/fullscreen/WatchAndMoreFullscreenVideoControlsPlugin;

    iget-object v4, v4, LX/2oy;->j:LX/2pb;

    invoke-virtual {v4}, LX/2pb;->s()LX/04D;

    move-result-object v4

    iget-object v5, p0, LX/BwI;->b:Lcom/facebook/feed/video/fullscreen/WatchAndMoreFullscreenVideoControlsPlugin;

    iget-object v5, v5, LX/2oy;->j:LX/2pb;

    .line 1833480
    iget-object v6, v5, LX/2pb;->D:LX/04G;

    move-object v5, v6

    .line 1833481
    iget-object v6, p0, LX/BwI;->b:Lcom/facebook/feed/video/fullscreen/WatchAndMoreFullscreenVideoControlsPlugin;

    iget-object v6, v6, LX/2oy;->j:LX/2pb;

    invoke-virtual {v6}, LX/2pb;->l()Z

    move-result v6

    iget-object v7, p0, LX/BwI;->b:Lcom/facebook/feed/video/fullscreen/WatchAndMoreFullscreenVideoControlsPlugin;

    iget-object v7, v7, LX/2oy;->j:LX/2pb;

    invoke-virtual {v7}, LX/2pb;->p()Z

    move-result v7

    invoke-virtual/range {v0 .. v7}, LX/1C2;->a(LX/0lF;Ljava/lang/String;ILX/04D;LX/04G;ZZ)LX/1C2;

    .line 1833482
    :cond_0
    iget-object v0, p0, LX/BwI;->b:Lcom/facebook/feed/video/fullscreen/WatchAndMoreFullscreenVideoControlsPlugin;

    iget-object v0, v0, Lcom/facebook/feed/video/fullscreen/WatchAndMoreFullscreenVideoControlsPlugin;->t:LX/D8U;

    if-eqz v0, :cond_1

    .line 1833483
    iget-object v0, p0, LX/BwI;->b:Lcom/facebook/feed/video/fullscreen/WatchAndMoreFullscreenVideoControlsPlugin;

    iget-object v0, v0, Lcom/facebook/feed/video/fullscreen/WatchAndMoreFullscreenVideoControlsPlugin;->t:LX/D8U;

    .line 1833484
    iget-object v1, v0, LX/D8U;->a:LX/D8g;

    sget-object v2, LX/D8g;->WATCH_AND_BROWSE:LX/D8g;

    if-ne v1, v2, :cond_2

    .line 1833485
    iget-object v1, v0, LX/D8U;->b:LX/D8W;

    iget-object v1, v1, LX/D8W;->h:Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/facebook/video/watchandmore/WatchAndMoreVideoPlayer;->setVisibility(I)V

    .line 1833486
    iget-object v1, v0, LX/D8U;->b:LX/D8W;

    iget-object v1, v1, LX/D8W;->l:LX/2mz;

    invoke-interface {v1}, LX/2mz;->b()V

    .line 1833487
    :cond_1
    :goto_0
    const v0, 0x31e47dd9

    invoke-static {v9, v9, v0, v8}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1833488
    :cond_2
    iget-object v1, v0, LX/D8U;->a:LX/D8g;

    sget-object v2, LX/D8g;->WATCH_AND_INSTALL:LX/D8g;

    if-eq v1, v2, :cond_3

    iget-object v1, v0, LX/D8U;->a:LX/D8g;

    sget-object v2, LX/D8g;->WATCH_AND_LEADGEN:LX/D8g;

    if-ne v1, v2, :cond_1

    .line 1833489
    :cond_3
    iget-object v1, v0, LX/D8U;->b:LX/D8W;

    invoke-static {v1}, LX/D8W;->d(LX/D8W;)V

    goto :goto_0
.end method
