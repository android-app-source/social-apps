.class public final LX/CKf;
.super LX/0xh;
.source ""


# instance fields
.field public final synthetic a:LX/CKg;


# direct methods
.method public constructor <init>(LX/CKg;)V
    .locals 0

    .prologue
    .line 1877419
    iput-object p1, p0, LX/CKf;->a:LX/CKg;

    invoke-direct {p0}, LX/0xh;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0wd;)V
    .locals 2

    .prologue
    .line 1877405
    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v0

    double-to-float v0, v0

    .line 1877406
    iget-object v1, p0, LX/CKf;->a:LX/CKg;

    iget-object v1, v1, LX/0Dp;->c:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setTranslationY(F)V

    .line 1877407
    return-void
.end method

.method public final b(LX/0wd;)V
    .locals 4

    .prologue
    .line 1877410
    iget-object v0, p0, LX/CKf;->a:LX/CKg;

    iget-object v1, v0, LX/0Dp;->c:Landroid/view/View;

    iget-object v0, p0, LX/CKf;->a:LX/CKg;

    iget-boolean v0, v0, LX/0Dp;->a:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1877411
    iget-object v0, p0, LX/CKf;->a:LX/CKg;

    iget-boolean v0, v0, LX/0Dp;->a:Z

    if-eqz v0, :cond_0

    .line 1877412
    iget-object v0, p0, LX/CKf;->a:LX/CKg;

    iget-object v0, v0, LX/CKg;->k:LX/CK7;

    const-string v1, "instant_article"

    iget-object v2, p0, LX/CKf;->a:LX/CKg;

    iget-object v2, v2, LX/CKg;->i:Lcom/facebook/user/model/User;

    .line 1877413
    iget-object v3, v2, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v2, v3

    .line 1877414
    iget-object v3, p0, LX/CKf;->a:LX/CKg;

    iget-object v3, v3, LX/CKg;->j:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, LX/CK7;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1877415
    :cond_0
    iget-object v0, p0, LX/CKf;->a:LX/CKg;

    iget-boolean v0, v0, LX/0Dp;->b:Z

    if-nez v0, :cond_1

    .line 1877416
    iget-object v0, p0, LX/CKf;->a:LX/CKg;

    invoke-virtual {v0}, LX/0Dp;->b()V

    .line 1877417
    :cond_1
    return-void

    .line 1877418
    :cond_2
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public final c(LX/0wd;)V
    .locals 2

    .prologue
    .line 1877408
    iget-object v0, p0, LX/CKf;->a:LX/CKg;

    iget-object v0, v0, LX/0Dp;->c:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1877409
    return-void
.end method
