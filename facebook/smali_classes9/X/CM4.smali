.class public LX/CM4;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/CM3;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1880106
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 1880107
    return-void
.end method


# virtual methods
.method public final a(LX/7kc;)LX/CM3;
    .locals 9

    .prologue
    .line 1880108
    new-instance v0, LX/CM3;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    const/16 v2, 0x3767

    invoke-static {p0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    invoke-static {p0}, Lcom/facebook/messaging/emoji/DownloadableEmojiButtonBuilder;->a(LX/0QB;)Lcom/facebook/messaging/emoji/DownloadableEmojiButtonBuilder;

    move-result-object v3

    check-cast v3, Lcom/facebook/messaging/emoji/DownloadableEmojiButtonBuilder;

    invoke-static {p0}, LX/1zU;->a(LX/0QB;)LX/1zU;

    move-result-object v4

    check-cast v4, LX/1zU;

    const/16 v5, 0xf9a

    invoke-static {p0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x275a

    invoke-static {p0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {p0}, LX/CMN;->b(LX/0QB;)LX/CMN;

    move-result-object v8

    check-cast v8, LX/CMN;

    move-object v7, p1

    invoke-direct/range {v0 .. v8}, LX/CM3;-><init>(Landroid/content/Context;LX/0Ot;Lcom/facebook/messaging/emoji/DownloadableEmojiButtonBuilder;LX/1zU;LX/0Ot;LX/0Ot;LX/7kc;LX/CMN;)V

    .line 1880109
    return-object v0
.end method
