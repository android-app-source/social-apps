.class public final LX/BXr;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "LX/0P1",
        "<",
        "Ljava/lang/String;",
        "LX/0Px",
        "<",
        "Lcom/facebook/user/model/User;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;)V
    .locals 0

    .prologue
    .line 1793897
    iput-object p1, p0, LX/BXr;->a:Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1793913
    iget-object v0, p0, LX/BXr;->a:Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;->a(Ljava/lang/Throwable;)V

    .line 1793914
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 1793898
    check-cast p1, LX/0P1;

    .line 1793899
    iget-object v0, p0, LX/BXr;->a:Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;

    .line 1793900
    iget-object v1, v0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;->g:Lcom/facebook/resources/ui/FbTextView;

    const v2, 0x7f08271c

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 1793901
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1793902
    invoke-virtual {v0}, Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;->c()LX/0Px;

    move-result-object v4

    .line 1793903
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result p0

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, p0, :cond_1

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1793904
    invoke-static {v0, v1, p1}, Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;->a(Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;Ljava/lang/String;Ljava/util/Map;)LX/44w;

    move-result-object v1

    .line 1793905
    if-eqz v1, :cond_0

    .line 1793906
    iget-object v1, v1, LX/44w;->b:Ljava/lang/Object;

    invoke-virtual {v3, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1793907
    :cond_0
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 1793908
    :cond_1
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 1793909
    iget-object v2, v0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;->r:LX/BY0;

    invoke-virtual {v2, v1}, LX/8tB;->a(Ljava/util/List;)V

    .line 1793910
    iget-object v1, v0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;->r:LX/BY0;

    const v2, -0x367c18d2

    invoke-static {v1, v2}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1793911
    iget-object v1, v0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;->i:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1793912
    return-void
.end method
