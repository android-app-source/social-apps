.class public LX/CBP;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/MountSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public final a:LX/CNw;

.field public final b:LX/2mJ;

.field public final c:LX/13P;


# direct methods
.method public constructor <init>(LX/CNw;LX/2mJ;LX/13P;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1856401
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1856402
    iput-object p1, p0, LX/CBP;->a:LX/CNw;

    .line 1856403
    iput-object p2, p0, LX/CBP;->b:LX/2mJ;

    .line 1856404
    iput-object p3, p0, LX/CBP;->c:LX/13P;

    .line 1856405
    return-void
.end method

.method public static a(LX/0QB;)LX/CBP;
    .locals 6

    .prologue
    .line 1856406
    const-class v1, LX/CBP;

    monitor-enter v1

    .line 1856407
    :try_start_0
    sget-object v0, LX/CBP;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1856408
    sput-object v2, LX/CBP;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1856409
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1856410
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1856411
    new-instance p0, LX/CBP;

    invoke-static {v0}, LX/CNw;->a(LX/0QB;)LX/CNw;

    move-result-object v3

    check-cast v3, LX/CNw;

    invoke-static {v0}, LX/2mJ;->a(LX/0QB;)LX/2mJ;

    move-result-object v4

    check-cast v4, LX/2mJ;

    invoke-static {v0}, LX/13P;->b(LX/0QB;)LX/13P;

    move-result-object v5

    check-cast v5, LX/13P;

    invoke-direct {p0, v3, v4, v5}, LX/CBP;-><init>(LX/CNw;LX/2mJ;LX/13P;)V

    .line 1856412
    move-object v0, p0

    .line 1856413
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1856414
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/CBP;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1856415
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1856416
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
