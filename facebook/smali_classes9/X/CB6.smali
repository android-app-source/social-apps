.class public final LX/CB6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLStory;

.field public final synthetic b:LX/CB7;

.field public final synthetic c:Lcom/facebook/feedplugins/prompts/storycta/PromptCallToActionPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/prompts/storycta/PromptCallToActionPartDefinition;Lcom/facebook/graphql/model/GraphQLStory;LX/CB7;)V
    .locals 0

    .prologue
    .line 1856165
    iput-object p1, p0, LX/CB6;->c:Lcom/facebook/feedplugins/prompts/storycta/PromptCallToActionPartDefinition;

    iput-object p2, p0, LX/CB6;->a:Lcom/facebook/graphql/model/GraphQLStory;

    iput-object p3, p0, LX/CB6;->b:LX/CB7;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 11

    .prologue
    const/4 v5, 0x1

    const/4 v8, 0x2

    const v0, 0x33f66223

    invoke-static {v8, v5, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v6

    .line 1856166
    iget-object v0, p0, LX/CB6;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, Lcom/facebook/feedplugins/prompts/storycta/PromptCallToActionPartDefinition;->f(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/productionprompts/model/ProductionPrompt;

    move-result-object v4

    .line 1856167
    new-instance v2, LX/CB5;

    invoke-direct {v2, p0, v4}, LX/CB5;-><init>(LX/CB6;Lcom/facebook/productionprompts/model/ProductionPrompt;)V

    .line 1856168
    iget-object v0, p0, LX/CB6;->c:Lcom/facebook/feedplugins/prompts/storycta/PromptCallToActionPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/prompts/storycta/PromptCallToActionPartDefinition;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5oW;

    iget-object v1, p0, LX/CB6;->b:LX/CB7;

    iget-object v1, v1, LX/CB7;->a:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    invoke-virtual {v0, v1}, LX/5oW;->b(Lcom/facebook/productionprompts/logging/PromptAnalytics;)V

    .line 1856169
    iget-object v0, p0, LX/CB6;->c:Lcom/facebook/feedplugins/prompts/storycta/PromptCallToActionPartDefinition;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v3, p0, LX/CB6;->c:Lcom/facebook/feedplugins/prompts/storycta/PromptCallToActionPartDefinition;

    iget-object v3, v3, Lcom/facebook/feedplugins/prompts/storycta/PromptCallToActionPartDefinition;->d:LX/1Ns;

    sget-object v7, Lcom/facebook/feedplugins/prompts/storycta/PromptCallToActionPartDefinition;->c:LX/1Qh;

    invoke-virtual {v3, v7, v2}, LX/1Ns;->a(LX/1Qh;LX/1DQ;)LX/1aw;

    move-result-object v2

    iget-object v3, p0, LX/CB6;->b:LX/CB7;

    iget-object v3, v3, LX/CB7;->a:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    .line 1856170
    const-class v7, Landroid/app/Activity;

    invoke-static {v1, v7}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/app/Activity;

    .line 1856171
    iget-object v10, v3, Lcom/facebook/productionprompts/logging/PromptAnalytics;->composerSessionId:Ljava/lang/String;

    const-string p0, "promptsCallToAction"

    iget-object v9, v0, Lcom/facebook/feedplugins/prompts/storycta/PromptCallToActionPartDefinition;->f:LX/0Ot;

    invoke-interface {v9}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, LX/1Nq;

    invoke-static {v4, v3, v5}, Lcom/facebook/productionprompts/composer/ProductionPromptsPluginConfig;->a(Lcom/facebook/productionprompts/model/ProductionPrompt;Lcom/facebook/productionprompts/logging/PromptAnalytics;Z)Lcom/facebook/productionprompts/composer/ProductionPromptsPluginConfig;

    move-result-object p1

    invoke-virtual {v9, p1}, LX/1Nq;->a(LX/88f;)Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;

    move-result-object v9

    invoke-virtual {v2, v10, p0, v9, v7}, LX/1aw;->a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;Landroid/app/Activity;)V

    .line 1856172
    const v0, 0x789632f3

    invoke-static {v8, v8, v0, v6}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
