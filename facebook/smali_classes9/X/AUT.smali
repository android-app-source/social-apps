.class public LX/AUT;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "PublicMethodReturnMutableCollection"
    }
.end annotation


# instance fields
.field public final a:LX/1Se;

.field public final b:LX/1Sf;

.field private final c:LX/AUA;

.field private final d:I

.field private final e:Z


# direct methods
.method public constructor <init>(LX/1Se;LX/1Sf;IZ)V
    .locals 2

    .prologue
    .line 1678213
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1678214
    iput-object p1, p0, LX/AUT;->a:LX/1Se;

    .line 1678215
    iput-object p2, p0, LX/AUT;->b:LX/1Sf;

    .line 1678216
    new-instance v0, LX/AUA;

    iget-object v1, p0, LX/AUT;->a:LX/1Se;

    invoke-direct {v0, v1}, LX/AUA;-><init>(LX/1Se;)V

    iput-object v0, p0, LX/AUT;->c:LX/AUA;

    .line 1678217
    iput p3, p0, LX/AUT;->d:I

    .line 1678218
    iput-boolean p4, p0, LX/AUT;->e:Z

    .line 1678219
    return-void
.end method

.method public static a(LX/AUT;Landroid/database/sqlite/SQLiteDatabase;LX/1Sh;[LX/AUP;I)I
    .locals 7
    .annotation build Lcom/facebook/crudolib/dbschema/direct/SchemaMigrator$MigrateResult;
    .end annotation

    .prologue
    .line 1678205
    iget-object v0, p0, LX/AUT;->c:LX/AUA;

    iget-object v1, p2, LX/1Sh;->a:Ljava/lang/String;

    invoke-static {v0, v1}, LX/AUR;->a(LX/AUA;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1678206
    if-nez v0, :cond_0

    .line 1678207
    iget-object v0, p0, LX/AUT;->b:LX/1Sf;

    invoke-virtual {v0, p4}, LX/1Sf;->b(I)[LX/AUQ;

    move-result-object v0

    invoke-static {p1, p2, p3, v0}, LX/AUT;->a(Landroid/database/sqlite/SQLiteDatabase;LX/1Sh;[LX/AUP;[LX/AUQ;)V

    .line 1678208
    const/4 v0, 0x4

    .line 1678209
    :goto_0
    return v0

    .line 1678210
    :cond_0
    iget-object v1, p2, LX/1Sh;->b:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1678211
    iget-object v1, p0, LX/AUT;->c:LX/AUA;

    iget-object v4, p0, LX/AUT;->b:LX/1Sf;

    iget v6, p0, LX/AUT;->d:I

    move-object v0, p1

    move-object v2, p2

    move-object v3, p3

    move v5, p4

    invoke-static/range {v0 .. v6}, LX/AUT;->a(Landroid/database/sqlite/SQLiteDatabase;LX/AUA;LX/1Sh;[LX/AUP;LX/1Sf;II)I

    move-result v0

    goto :goto_0

    .line 1678212
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;LX/AUA;LX/1Sh;[LX/AUP;LX/1Sf;II)I
    .locals 2
    .annotation build Lcom/facebook/crudolib/dbschema/direct/SchemaMigrator$MigrateResult;
    .end annotation

    .prologue
    .line 1678086
    const-string v0, "migrateTable"

    const v1, 0x3ca0ce33

    invoke-static {v0, v1}, LX/03q;->a(Ljava/lang/String;I)V

    .line 1678087
    :try_start_0
    invoke-static/range {p0 .. p6}, LX/AUT;->b(Landroid/database/sqlite/SQLiteDatabase;LX/AUA;LX/1Sh;[LX/AUP;LX/1Sf;II)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 1678088
    const v1, -0x5e40014b

    invoke-static {v1}, LX/03q;->a(I)V

    return v0

    :catchall_0
    move-exception v0

    const v1, -0x24752f17

    invoke-static {v1}, LX/03q;->a(I)V

    throw v0
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;LX/1Sh;LX/1Sf;ILjava/util/ArrayList;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "LX/1Sh;",
            "Lcom/facebook/crudolib/dbschema/SchemaProvider;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "LX/AUP;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1678192
    invoke-virtual {p4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1678193
    invoke-virtual {p4}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v2, v0

    move v1, v0

    :goto_0
    if-ge v2, v3, :cond_0

    .line 1678194
    invoke-virtual {p4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AUP;

    .line 1678195
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 1678196
    const-string v5, "ALTER TABLE "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p1, LX/1Sh;->a:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/16 v6, 0x20

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1678197
    const-string v5, "ADD COLUMN "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1678198
    invoke-static {v4, v0}, LX/AUT;->a(Ljava/lang/StringBuilder;LX/AUP;)V

    .line 1678199
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const v5, -0x62ae6797

    invoke-static {v5}, LX/03h;->a(I)V

    invoke-virtual {p0, v4}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v4, -0x25a68212

    invoke-static {v4}, LX/03h;->a(I)V

    .line 1678200
    iget-boolean v0, v0, LX/AUP;->h:Z

    or-int/2addr v1, v0

    .line 1678201
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_0
    move v0, v1

    .line 1678202
    :cond_1
    if-eqz v0, :cond_2

    .line 1678203
    invoke-virtual {p2, p3}, LX/1Sf;->b(I)[LX/AUQ;

    move-result-object v0

    invoke-static {p0, p1, v0}, LX/AUT;->a(Landroid/database/sqlite/SQLiteDatabase;LX/1Sh;[LX/AUQ;)V

    .line 1678204
    :cond_2
    return-void
.end method

.method public static a(Landroid/database/sqlite/SQLiteDatabase;LX/1Sh;[LX/AUP;[LX/AUQ;)V
    .locals 4

    .prologue
    .line 1678177
    const-string v0, "createTableWithIndices"

    const v1, -0x5e2cd847

    invoke-static {v0, v1}, LX/03q;->a(Ljava/lang/String;I)V

    .line 1678178
    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1678179
    const-string v0, "CREATE TABLE "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p1, LX/1Sh;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v2, 0x20

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1678180
    const/16 v0, 0x28

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1678181
    const/4 v0, 0x0

    aget-object v0, p2, v0

    invoke-static {v1, v0}, LX/AUT;->a(Ljava/lang/StringBuilder;LX/AUP;)V

    .line 1678182
    const/4 v0, 0x1

    array-length v2, p2

    :goto_0
    if-ge v0, v2, :cond_0

    .line 1678183
    const-string v3, ", "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1678184
    aget-object v3, p2, v0

    invoke-static {v1, v3}, LX/AUT;->a(Ljava/lang/StringBuilder;LX/AUP;)V

    .line 1678185
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1678186
    :cond_0
    const/16 v0, 0x29

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1678187
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const v1, 0x8dbf50

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x576a8350

    invoke-static {v0}, LX/03h;->a(I)V

    .line 1678188
    iget-object v0, p1, LX/1Sh;->a:Ljava/lang/String;

    invoke-static {p0, v0, p3}, LX/AUT;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[LX/AUQ;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1678189
    const v0, -0x5f3ac0ee

    invoke-static {v0}, LX/03q;->a(I)V

    .line 1678190
    return-void

    .line 1678191
    :catchall_0
    move-exception v0

    const v1, -0x3165e02a

    invoke-static {v1}, LX/03q;->a(I)V

    throw v0
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;LX/1Sh;[LX/AUQ;)V
    .locals 2

    .prologue
    .line 1678171
    const-string v0, "recreateIndices"

    const v1, 0x36b997fd

    invoke-static {v0, v1}, LX/03q;->a(Ljava/lang/String;I)V

    .line 1678172
    :try_start_0
    iget-object v0, p1, LX/1Sh;->a:Ljava/lang/String;

    invoke-static {p0, v0}, LX/AUT;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 1678173
    iget-object v0, p1, LX/1Sh;->a:Ljava/lang/String;

    invoke-static {p0, v0, p2}, LX/AUT;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[LX/AUQ;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1678174
    const v0, 0x775bce80

    invoke-static {v0}, LX/03q;->a(I)V

    .line 1678175
    return-void

    .line 1678176
    :catchall_0
    move-exception v0

    const v1, 0x658d9c8

    invoke-static {v1}, LX/03q;->a(I)V

    throw v0
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1678165
    const-string v0, "SELECT name FROM sqlite_master WHERE type == \'index\' AND tbl_name == ?"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    aput-object p1, v1, v2

    invoke-virtual {p0, v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 1678166
    :goto_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1678167
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1678168
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "DROP INDEX "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const v2, -0x387cf92e

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x4fd281aa

    invoke-static {v0}, LX/03h;->a(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1678169
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1678170
    return-void
.end method

.method public static a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[LX/AUQ;)V
    .locals 10

    .prologue
    .line 1678141
    array-length v1, p2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_3

    aget-object v2, p2, v0

    .line 1678142
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 1678143
    const/4 v5, 0x0

    .line 1678144
    const-string v4, "CREATE "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1678145
    iget-boolean v4, v2, LX/AUQ;->a:Z

    if-eqz v4, :cond_0

    .line 1678146
    const-string v4, "UNIQUE "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1678147
    :cond_0
    const-string v4, "INDEX "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1678148
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1678149
    iget-object v6, v2, LX/AUQ;->b:[Ljava/lang/String;

    array-length v7, v6

    move v4, v5

    :goto_1
    if-ge v4, v7, :cond_1

    aget-object v8, v6, v4

    .line 1678150
    const-string v9, "_"

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1678151
    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1678152
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 1678153
    :cond_1
    const-string v4, " ON "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1678154
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1678155
    const-string v4, "("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1678156
    iget-object v4, v2, LX/AUQ;->b:[Ljava/lang/String;

    aget-object v4, v4, v5

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1678157
    const/4 v4, 0x1

    iget-object v5, v2, LX/AUQ;->b:[Ljava/lang/String;

    array-length v5, v5

    :goto_2
    if-ge v4, v5, :cond_2

    .line 1678158
    const/16 v6, 0x2c

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1678159
    iget-object v6, v2, LX/AUQ;->b:[Ljava/lang/String;

    aget-object v6, v6, v4

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1678160
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 1678161
    :cond_2
    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1678162
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const v3, -0x49a40882

    invoke-static {v3}, LX/03h;->a(I)V

    invoke-virtual {p0, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v2, 0x2ec26c2e

    invoke-static {v2}, LX/03h;->a(I)V

    .line 1678163
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1678164
    :cond_3
    return-void
.end method

.method private static a(Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 1678137
    if-eqz p1, :cond_0

    .line 1678138
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0, p0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1678139
    :cond_0
    const-string v0, "SchemaMigrator"

    invoke-static {v0, p0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1678140
    return-void
.end method

.method public static a(Ljava/lang/StringBuilder;LX/AUP;)V
    .locals 2

    .prologue
    .line 1678122
    iget-object v0, p1, LX/AUP;->a:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1678123
    iget-object v0, p1, LX/AUP;->b:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1678124
    iget-object v0, p1, LX/AUP;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1678125
    const-string v0, "DEFAULT "

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, LX/AUP;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1678126
    :cond_0
    iget-boolean v0, p1, LX/AUP;->d:Z

    if-nez v0, :cond_1

    .line 1678127
    const-string v0, "NOT NULL "

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1678128
    :cond_1
    iget-boolean v0, p1, LX/AUP;->e:Z

    if-eqz v0, :cond_2

    .line 1678129
    const-string v0, "PRIMARY KEY "

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1678130
    :cond_2
    iget-boolean v0, p1, LX/AUP;->f:Z

    if-eqz v0, :cond_3

    .line 1678131
    const-string v0, "AUTOINCREMENT "

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1678132
    :cond_3
    iget-object v0, p1, LX/AUP;->j:Ljava/lang/String;

    if-nez v0, :cond_4

    iget-object v0, p1, LX/AUP;->k:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 1678133
    :cond_4
    const-string v0, "REFERENCES "

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1678134
    iget-object v0, p1, LX/AUP;->j:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1678135
    const-string v0, "("

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, LX/AUP;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1678136
    :cond_5
    return-void
.end method

.method private static b(Landroid/database/sqlite/SQLiteDatabase;LX/AUA;LX/1Sh;[LX/AUP;LX/1Sf;II)I
    .locals 13
    .annotation build Lcom/facebook/crudolib/dbschema/direct/SchemaMigrator$MigrateResult;
    .end annotation

    .prologue
    .line 1678089
    new-instance v2, LX/AUS;

    iget-object v3, p2, LX/1Sh;->a:Ljava/lang/String;

    invoke-static {p1, v3}, LX/AUR;->b(LX/AUA;Ljava/lang/String;)[LX/AUP;

    move-result-object v3

    move-object/from16 v0, p3

    invoke-direct {v2, v3, v0}, LX/AUS;-><init>([LX/AUP;[LX/AUP;)V

    .line 1678090
    invoke-virtual {v2}, LX/AUS;->a()V

    .line 1678091
    invoke-virtual {v2}, LX/AUS;->c()Ljava/util/ArrayList;

    move-result-object v5

    .line 1678092
    invoke-virtual {v2}, LX/AUS;->b()Ljava/util/ArrayList;

    move-result-object v6

    .line 1678093
    invoke-virtual {v2}, LX/AUS;->d()Ljava/util/ArrayList;

    move-result-object v7

    .line 1678094
    invoke-virtual {v2}, LX/AUS;->e()[Z

    move-result-object v8

    .line 1678095
    invoke-virtual {v2}, LX/AUS;->f()Ljava/util/ArrayList;

    move-result-object v9

    .line 1678096
    sget-object v2, LX/AUj;->DROP_TABLE:LX/AUj;

    invoke-virtual {v2}, LX/AUj;->ordinal()I

    move-result v2

    aget-boolean v10, v8, v2

    .line 1678097
    sget-object v2, LX/AUj;->DROP_ALL_TABLES:LX/AUj;

    invoke-virtual {v2}, LX/AUj;->ordinal()I

    move-result v2

    aget-boolean v3, v8, v2

    .line 1678098
    const/4 v2, 0x2

    move/from16 v0, p6

    if-ne v0, v2, :cond_4

    const/4 v2, 0x1

    move v4, v2

    .line 1678099
    :goto_0
    const/4 v2, 0x1

    move/from16 v0, p6

    if-ne v0, v2, :cond_5

    const/4 v2, 0x1

    .line 1678100
    :goto_1
    invoke-virtual {v6}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v11

    if-nez v11, :cond_0

    .line 1678101
    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "["

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v12, p2, LX/1Sh;->a:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "]: You must use @Deleted to remove columns: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6, v2}, LX/AUT;->a(Ljava/lang/String;Z)V

    .line 1678102
    or-int/2addr v3, v4

    .line 1678103
    :cond_0
    invoke-virtual {v9}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_1

    .line 1678104
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v11, "["

    invoke-direct {v6, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v11, p2, LX/1Sh;->a:Ljava/lang/String;

    invoke-virtual {v6, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v11, "]: Modification of columns is not permitted, use @Deleted and a new column instead: "

    invoke-virtual {v6, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6, v2}, LX/AUT;->a(Ljava/lang/String;Z)V

    .line 1678105
    or-int/2addr v3, v4

    .line 1678106
    :cond_1
    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 1678107
    const-string v2, "SchemaMigrator"

    const-string v4, "[%s] Ignoring deleted columns: %s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget-object v11, p2, LX/1Sh;->a:Ljava/lang/String;

    aput-object v11, v6, v9

    const/4 v9, 0x1

    aput-object v5, v6, v9

    invoke-static {v2, v4, v6}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1678108
    :cond_2
    if-nez v10, :cond_3

    if-eqz v3, :cond_8

    .line 1678109
    :cond_3
    if-eqz v3, :cond_6

    .line 1678110
    :goto_2
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "DROP TABLE IF EXISTS "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p2, LX/1Sh;->a:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const v4, -0x76a33a31

    invoke-static {v4}, LX/03h;->a(I)V

    invoke-virtual {p0, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v2, -0x171c2185

    invoke-static {v2}, LX/03h;->a(I)V

    .line 1678111
    invoke-virtual/range {p4 .. p5}, LX/1Sf;->b(I)[LX/AUQ;

    move-result-object v2

    move-object/from16 v0, p3

    invoke-static {p0, p2, v0, v2}, LX/AUT;->a(Landroid/database/sqlite/SQLiteDatabase;LX/1Sh;[LX/AUP;[LX/AUQ;)V

    .line 1678112
    if-eqz v3, :cond_7

    const/4 v2, 0x5

    .line 1678113
    :goto_3
    return v2

    .line 1678114
    :cond_4
    const/4 v2, 0x0

    move v4, v2

    goto/16 :goto_0

    .line 1678115
    :cond_5
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 1678116
    :cond_6
    goto :goto_2

    .line 1678117
    :cond_7
    const/4 v2, 0x3

    goto :goto_3

    .line 1678118
    :cond_8
    sget-object v2, LX/AUj;->ASSIGN_DEFAULT:LX/AUj;

    invoke-virtual {v2}, LX/AUj;->ordinal()I

    move-result v2

    aget-boolean v2, v8, v2

    if-nez v2, :cond_9

    .line 1678119
    const-string v2, "SchemaMigrator"

    const-string v3, "[%s] Assuming auto-upgrade policy of ASSIGN_DEFAULT"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p2, LX/1Sh;->a:Ljava/lang/String;

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1678120
    :cond_9
    move-object/from16 v0, p4

    move/from16 v1, p5

    invoke-static {p0, p2, v0, v1, v7}, LX/AUT;->a(Landroid/database/sqlite/SQLiteDatabase;LX/1Sh;LX/1Sf;ILjava/util/ArrayList;)V

    .line 1678121
    const/4 v2, 0x2

    goto :goto_3
.end method
