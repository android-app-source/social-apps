.class public final LX/BRJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8G6;


# instance fields
.field public final synthetic a:I

.field public final synthetic b:I

.field public final synthetic c:LX/BRL;


# direct methods
.method public constructor <init>(LX/BRL;II)V
    .locals 0

    .prologue
    .line 1783857
    iput-object p1, p0, LX/BRJ;->c:LX/BRL;

    iput p2, p0, LX/BRJ;->a:I

    iput p3, p0, LX/BRJ;->b:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1783858
    iget-object v0, p0, LX/BRJ;->c:LX/BRL;

    iget-boolean v0, v0, LX/BRL;->o:Z

    if-eqz v0, :cond_0

    .line 1783859
    :goto_0
    return-void

    .line 1783860
    :cond_0
    iget-object v0, p0, LX/BRJ;->c:LX/BRL;

    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    iget-object v2, p0, LX/BRJ;->c:LX/BRL;

    iget-object v2, v2, LX/BRL;->m:LX/0Px;

    invoke-virtual {v1, v2}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v1

    invoke-virtual {v1, p1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 1783861
    iput-object v1, v0, LX/BRL;->m:LX/0Px;

    .line 1783862
    iget-object v0, p0, LX/BRJ;->c:LX/BRL;

    iget-object v0, v0, LX/BRL;->f:LX/8GP;

    iget v1, p0, LX/BRJ;->a:I

    iget v2, p0, LX/BRJ;->b:I

    invoke-virtual {v0, v1, v2}, LX/8GP;->a(II)LX/8GO;

    move-result-object v0

    iget-object v1, p0, LX/BRJ;->c:LX/BRL;

    iget-object v1, v1, LX/BRL;->m:LX/0Px;

    invoke-virtual {v0, v1}, LX/8GO;->c(LX/0Px;)LX/8GO;

    move-result-object v0

    invoke-virtual {v0}, LX/8GO;->b()LX/0Px;

    move-result-object v0

    .line 1783863
    iget-object v1, p0, LX/BRJ;->c:LX/BRL;

    iget-object v1, v1, LX/BRL;->j:LX/8GL;

    iget-object v2, p0, LX/BRJ;->c:LX/BRL;

    iget-object v2, v2, LX/BRL;->c:LX/BRP;

    .line 1783864
    iget-object p0, v2, LX/BRP;->a:Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;

    move-object v2, p0

    .line 1783865
    invoke-virtual {v2}, Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;->c()LX/5QV;

    move-result-object v2

    invoke-interface {v2}, LX/5QV;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v3, v0, v2, v3}, LX/8GL;->a(ZLX/0Px;Ljava/lang/String;Z)V

    goto :goto_0
.end method
