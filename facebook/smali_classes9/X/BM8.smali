.class public LX/BM8;
.super LX/BM7;
.source ""


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/BM5;

.field private final c:LX/1E1;


# direct methods
.method public constructor <init>(Lcom/facebook/productionprompts/model/ProductionPrompt;Landroid/content/Context;LX/1bQ;LX/1E1;)V
    .locals 2
    .param p1    # Lcom/facebook/productionprompts/model/ProductionPrompt;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1776888
    invoke-direct {p0, p1, p3, p4}, LX/BM7;-><init>(Lcom/facebook/productionprompts/model/ProductionPrompt;LX/1bQ;LX/1E1;)V

    .line 1776889
    iput-object p2, p0, LX/BM8;->a:Landroid/content/Context;

    .line 1776890
    iput-object p4, p0, LX/BM8;->c:LX/1E1;

    .line 1776891
    new-instance v0, LX/BM5;

    invoke-direct {v0, p2}, LX/BM5;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/BM8;->b:LX/BM5;

    .line 1776892
    iget-object v0, p0, LX/BM8;->b:LX/BM5;

    invoke-virtual {p1}, Lcom/facebook/productionprompts/model/ProductionPrompt;->f()Ljava/lang/String;

    move-result-object v1

    .line 1776893
    iget-object p0, v0, LX/BM5;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1776894
    return-void
.end method


# virtual methods
.method public final c()Ljava/lang/Integer;
    .locals 2

    .prologue
    .line 1776895
    iget-object v0, p0, LX/BM8;->c:LX/1E1;

    invoke-virtual {v0}, LX/1E1;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/BM8;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00e5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method

.method public final e()Landroid/graphics/drawable/Drawable;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1776896
    const/4 v0, 0x0

    return-object v0
.end method

.method public final g()LX/AkM;
    .locals 1

    .prologue
    .line 1776897
    iget-object v0, p0, LX/BM8;->b:LX/BM5;

    return-object v0
.end method
