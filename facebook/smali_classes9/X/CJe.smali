.class public final LX/CJe;
.super Landroid/webkit/WebChromeClient;
.source ""


# instance fields
.field public final synthetic a:LX/CJg;


# direct methods
.method public constructor <init>(LX/CJg;)V
    .locals 0

    .prologue
    .line 1875617
    iput-object p1, p0, LX/CJe;->a:LX/CJg;

    invoke-direct {p0}, Landroid/webkit/WebChromeClient;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCloseWindow(Landroid/webkit/WebView;)V
    .locals 1

    .prologue
    .line 1875618
    iget-object v0, p0, LX/CJe;->a:LX/CJg;

    .line 1875619
    iget-object p0, v0, LX/CJg;->c:Ljava/util/Stack;

    invoke-virtual {p0}, Ljava/util/Stack;->isEmpty()Z

    move-result p0

    if-eqz p0, :cond_1

    const/4 p0, 0x0

    :goto_0
    move-object p0, p0

    .line 1875620
    if-eqz p0, :cond_0

    .line 1875621
    iget-object p1, v0, LX/CJg;->c:Ljava/util/Stack;

    invoke-virtual {p1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    .line 1875622
    invoke-static {p0}, LX/CJg;->c(Landroid/webkit/WebView;)V

    .line 1875623
    :cond_0
    return-void

    :cond_1
    iget-object p0, v0, LX/CJg;->c:Ljava/util/Stack;

    invoke-virtual {p0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/webkit/WebView;

    goto :goto_0
.end method

.method public final onCreateWindow(Landroid/webkit/WebView;ZZLandroid/os/Message;)Z
    .locals 2

    .prologue
    .line 1875624
    iget-object v0, p4, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/webkit/WebView$WebViewTransport;

    .line 1875625
    iget-object v1, p0, LX/CJe;->a:LX/CJg;

    invoke-virtual {v1}, LX/CJg;->a()Landroid/webkit/WebView;

    move-result-object v1

    .line 1875626
    invoke-virtual {v0, v1}, Landroid/webkit/WebView$WebViewTransport;->setWebView(Landroid/webkit/WebView;)V

    .line 1875627
    invoke-virtual {p4}, Landroid/os/Message;->sendToTarget()V

    .line 1875628
    const/4 v0, 0x1

    return v0
.end method

.method public final onProgressChanged(Landroid/webkit/WebView;I)V
    .locals 2

    .prologue
    .line 1875629
    iget-object v0, p0, LX/CJe;->a:LX/CJg;

    iget-object v0, v0, LX/CJg;->e:Landroid/widget/ProgressBar;

    if-nez v0, :cond_0

    .line 1875630
    :goto_0
    return-void

    .line 1875631
    :cond_0
    iget-object v0, p0, LX/CJe;->a:LX/CJg;

    iget-object v0, v0, LX/CJg;->e:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p2}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 1875632
    iget-object v0, p0, LX/CJe;->a:LX/CJg;

    iget-object v1, v0, LX/CJg;->e:Landroid/widget/ProgressBar;

    const/16 v0, 0x64

    if-ne p2, v0, :cond_1

    const/16 v0, 0x8

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
