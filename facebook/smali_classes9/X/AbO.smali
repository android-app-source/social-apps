.class public LX/AbO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/facecast/protocol/VideoBroadcastUpdateCommercialBreakTimeOffsetsRequest;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1690648
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1690649
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 4

    .prologue
    .line 1690629
    check-cast p1, Lcom/facebook/facecast/protocol/VideoBroadcastUpdateCommercialBreakTimeOffsetsRequest;

    .line 1690630
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 1690631
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "commercial_break_time_offsets"

    iget-object v3, p1, Lcom/facebook/facecast/protocol/VideoBroadcastUpdateCommercialBreakTimeOffsetsRequest;->b:[F

    invoke-static {v3}, Ljava/util/Arrays;->toString([F)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1690632
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "video_broadcast_update"

    .line 1690633
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 1690634
    move-object v1, v1

    .line 1690635
    const-string v2, "POST"

    .line 1690636
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 1690637
    move-object v1, v1

    .line 1690638
    iget-object v2, p1, Lcom/facebook/facecast/protocol/VideoBroadcastUpdateCommercialBreakTimeOffsetsRequest;->a:Ljava/lang/String;

    .line 1690639
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 1690640
    move-object v1, v1

    .line 1690641
    sget-object v2, LX/14S;->JSON:LX/14S;

    .line 1690642
    iput-object v2, v1, LX/14O;->k:LX/14S;

    .line 1690643
    move-object v1, v1

    .line 1690644
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 1690645
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 1690646
    move-object v0, v1

    .line 1690647
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1690627
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 1690628
    const/4 v0, 0x0

    return-object v0
.end method
