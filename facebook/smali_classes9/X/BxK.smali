.class public LX/BxK;
.super LX/An9;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pm;",
        ">",
        "LX/An9",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Bx5;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/Bx5;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1835307
    invoke-direct {p0}, LX/An9;-><init>()V

    .line 1835308
    iput-object p1, p0, LX/BxK;->a:LX/0Ot;

    .line 1835309
    return-void
.end method

.method public static a(LX/0QB;)LX/BxK;
    .locals 4

    .prologue
    .line 1835274
    const-class v1, LX/BxK;

    monitor-enter v1

    .line 1835275
    :try_start_0
    sget-object v0, LX/BxK;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1835276
    sput-object v2, LX/BxK;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1835277
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1835278
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1835279
    new-instance v3, LX/BxK;

    const/16 p0, 0x1e00

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/BxK;-><init>(LX/0Ot;)V

    .line 1835280
    move-object v0, v3

    .line 1835281
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1835282
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/BxK;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1835283
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1835284
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/3mj;Ljava/lang/Object;Ljava/lang/Object;)LX/1X1;
    .locals 2

    .prologue
    .line 1835287
    check-cast p4, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    check-cast p5, LX/1Pm;

    .line 1835288
    iget-object v0, p0, LX/BxK;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Bx5;

    const/4 v1, 0x0

    .line 1835289
    new-instance p0, LX/Bx3;

    invoke-direct {p0, v0}, LX/Bx3;-><init>(LX/Bx5;)V

    .line 1835290
    iget-object p3, v0, LX/Bx5;->b:LX/0Zi;

    invoke-virtual {p3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, LX/Bx4;

    .line 1835291
    if-nez p3, :cond_0

    .line 1835292
    new-instance p3, LX/Bx4;

    invoke-direct {p3, v0}, LX/Bx4;-><init>(LX/Bx5;)V

    .line 1835293
    :cond_0
    invoke-static {p3, p1, v1, v1, p0}, LX/Bx4;->a$redex0(LX/Bx4;LX/1De;IILX/Bx3;)V

    .line 1835294
    move-object p0, p3

    .line 1835295
    move-object v1, p0

    .line 1835296
    move-object v0, v1

    .line 1835297
    iget-object v1, v0, LX/Bx4;->a:LX/Bx3;

    iput-object p2, v1, LX/Bx3;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1835298
    iget-object v1, v0, LX/Bx4;->e:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v1, p0}, Ljava/util/BitSet;->set(I)V

    .line 1835299
    move-object v0, v0

    .line 1835300
    iget-object v1, v0, LX/Bx4;->a:LX/Bx3;

    iput-object p4, v1, LX/Bx3;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1835301
    iget-object v1, v0, LX/Bx4;->e:Ljava/util/BitSet;

    const/4 p0, 0x1

    invoke-virtual {v1, p0}, Ljava/util/BitSet;->set(I)V

    .line 1835302
    move-object v0, v0

    .line 1835303
    iget-object v1, v0, LX/Bx4;->a:LX/Bx3;

    iput-object p5, v1, LX/Bx3;->c:LX/1Pm;

    .line 1835304
    iget-object v1, v0, LX/Bx4;->e:Ljava/util/BitSet;

    const/4 p0, 0x2

    invoke-virtual {v1, p0}, Ljava/util/BitSet;->set(I)V

    .line 1835305
    move-object v0, v0

    .line 1835306
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1835286
    const-class v0, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;

    return-object v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 1835285
    const/16 v0, 0x8

    return v0
.end method
