.class public LX/B35;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:[Ljava/lang/String;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public static final b:[Ljava/lang/String;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field private static volatile h:LX/B35;


# instance fields
.field public final c:Lcom/facebook/content/SecureContextHelper;

.field private final d:LX/0i4;

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0ad;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2ua;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1739512
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "android.permission.CAMERA"

    aput-object v1, v0, v2

    const-string v1, "android.permission.READ_EXTERNAL_STORAGE"

    aput-object v1, v0, v3

    const-string v1, "android.permission.WRITE_EXTERNAL_STORAGE"

    aput-object v1, v0, v4

    sput-object v0, LX/B35;->a:[Ljava/lang/String;

    .line 1739513
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "android.permission.CAMERA"

    aput-object v1, v0, v2

    const-string v1, "android.permission.RECORD_AUDIO"

    aput-object v1, v0, v3

    const-string v1, "android.permission.READ_EXTERNAL_STORAGE"

    aput-object v1, v0, v4

    const-string v1, "android.permission.WRITE_EXTERNAL_STORAGE"

    aput-object v1, v0, v5

    sput-object v0, LX/B35;->b:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/content/SecureContextHelper;LX/0i4;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/0i4;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0ad;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2ua;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1739514
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1739515
    iput-object p1, p0, LX/B35;->c:Lcom/facebook/content/SecureContextHelper;

    .line 1739516
    iput-object p2, p0, LX/B35;->d:LX/0i4;

    .line 1739517
    iput-object p3, p0, LX/B35;->e:LX/0Ot;

    .line 1739518
    iput-object p4, p0, LX/B35;->f:LX/0Ot;

    .line 1739519
    iput-object p5, p0, LX/B35;->g:LX/0Ot;

    .line 1739520
    return-void
.end method

.method public static a(LX/0QB;)LX/B35;
    .locals 9

    .prologue
    .line 1739521
    sget-object v0, LX/B35;->h:LX/B35;

    if-nez v0, :cond_1

    .line 1739522
    const-class v1, LX/B35;

    monitor-enter v1

    .line 1739523
    :try_start_0
    sget-object v0, LX/B35;->h:LX/B35;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1739524
    if-eqz v2, :cond_0

    .line 1739525
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1739526
    new-instance v3, LX/B35;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v4

    check-cast v4, Lcom/facebook/content/SecureContextHelper;

    const-class v5, LX/0i4;

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/0i4;

    const/16 v6, 0x259

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x1032

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x125b

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-direct/range {v3 .. v8}, LX/B35;-><init>(Lcom/facebook/content/SecureContextHelper;LX/0i4;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 1739527
    move-object v0, v3

    .line 1739528
    sput-object v0, LX/B35;->h:LX/B35;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1739529
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1739530
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1739531
    :cond_1
    sget-object v0, LX/B35;->h:LX/B35;

    return-object v0

    .line 1739532
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1739533
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/app/Activity;ILcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;)V
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1739534
    iget-object v0, p0, LX/B35;->d:LX/0i4;

    invoke-virtual {v0, p1}, LX/0i4;->a(Landroid/app/Activity;)LX/0i5;

    move-result-object v1

    .line 1739535
    invoke-virtual {p3}, Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;->h()I

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, LX/B35;->a:[Ljava/lang/String;

    :goto_0
    new-instance v2, LX/B34;

    invoke-direct {v2, p0, p1, p3, p2}, LX/B34;-><init>(LX/B35;Landroid/app/Activity;Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;I)V

    invoke-virtual {v1, v0, v2}, LX/0i5;->a([Ljava/lang/String;LX/6Zx;)V

    .line 1739536
    return-void

    .line 1739537
    :cond_0
    sget-object v0, LX/B35;->b:[Ljava/lang/String;

    goto :goto_0
.end method

.method public final a(Landroid/app/Activity;Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;Ljava/lang/String;Ljava/lang/String;LX/5QV;JLcom/facebook/productionprompts/logging/PromptAnalytics;I)V
    .locals 4
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # Lcom/facebook/productionprompts/logging/PromptAnalytics;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 1739538
    if-ne p9, v3, :cond_1

    .line 1739539
    iget-object v0, p0, LX/B35;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2ua;

    invoke-virtual {v0}, LX/2ua;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/B35;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-short v2, LX/B3X;->e:S

    invoke-interface {v0, v2, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move p9, v1

    .line 1739540
    :cond_1
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1739541
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;->SUGGESTED_OVERLAYS:Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;

    if-ne p2, v2, :cond_2

    .line 1739542
    new-instance v1, LX/B4N;

    invoke-direct {v1, v0, p4}, LX/B4N;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p5}, LX/5QV;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/B4N;->b(Ljava/lang/String;)LX/B4N;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/B4J;->a(Ljava/lang/String;)LX/B4J;

    move-result-object v0

    check-cast v0, LX/B4N;

    invoke-virtual {v0, p6, p7}, LX/B4J;->a(J)LX/B4J;

    move-result-object v0

    check-cast v0, LX/B4N;

    invoke-virtual {v0, p9}, LX/B4J;->a(I)LX/B4J;

    move-result-object v0

    check-cast v0, LX/B4N;

    invoke-virtual {v0}, LX/B4N;->a()Lcom/facebook/heisman/intent/ProfilePictureOverlayPivotIntentData;

    move-result-object v0

    .line 1739543
    invoke-virtual {p0, p1, v0}, LX/B35;->a(Landroid/content/Context;Lcom/facebook/heisman/intent/ProfilePictureOverlayPivotIntentData;)V

    .line 1739544
    :goto_0
    return-void

    .line 1739545
    :cond_2
    invoke-static {p5}, LX/5Qm;->a(LX/5QV;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1739546
    new-instance v1, LX/B4K;

    invoke-direct {v1, p5, v0, p4}, LX/B4K;-><init>(LX/5QV;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, p3}, LX/B4J;->a(Ljava/lang/String;)LX/B4J;

    move-result-object v0

    check-cast v0, LX/B4K;

    invoke-virtual {v0, p6, p7}, LX/B4J;->a(J)LX/B4J;

    move-result-object v0

    check-cast v0, LX/B4K;

    invoke-virtual {v0, p8}, LX/B4J;->a(Lcom/facebook/productionprompts/logging/PromptAnalytics;)LX/B4J;

    move-result-object v0

    check-cast v0, LX/B4K;

    invoke-virtual {v0, p9}, LX/B4J;->a(I)LX/B4J;

    move-result-object v0

    check-cast v0, LX/B4K;

    invoke-virtual {v0}, LX/B4K;->a()Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;

    move-result-object v0

    .line 1739547
    const/16 v1, 0x20b3

    invoke-virtual {p0, p1, v1, v0}, LX/B35;->a(Landroid/app/Activity;ILcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;)V

    goto :goto_0

    .line 1739548
    :cond_3
    invoke-static {p5}, LX/5Qm;->c(LX/5QV;)Ljava/lang/String;

    move-result-object v0

    .line 1739549
    const-string v2, "Insufficient information to launch profile picture overlay flow; imageOverlay: %s"

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v1

    invoke-static {v2, v3}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1739550
    iget-object v0, p0, LX/B35;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v2, "profile_picture_overlay_launcher"

    invoke-virtual {v0, v2, v1}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;Lcom/facebook/heisman/intent/ProfilePictureOverlayPivotIntentData;)V
    .locals 2

    .prologue
    .line 1739551
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/heisman/ProfilePictureOverlayPivotActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1739552
    const-string v1, "heisman_pivot_intent_data"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1739553
    iget-object v1, p0, LX/B35;->c:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1739554
    return-void
.end method
