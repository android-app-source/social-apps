.class public LX/BRy;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1784625
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0ad;LX/5wO;LX/03R;LX/BS1;)V
    .locals 7

    .prologue
    .line 1784605
    const/4 v1, 0x2

    .line 1784606
    const v0, 0x7f081541

    move v2, v0

    .line 1784607
    invoke-static {}, LX/10A;->a()I

    move-result v3

    .line 1784608
    invoke-interface {p1}, LX/5wO;->d()Z

    move-result v0

    move v5, v0

    .line 1784609
    const/4 v6, 0x1

    move-object v0, p3

    move v4, v1

    invoke-interface/range {v0 .. v6}, LX/BS1;->a(IIIIZZ)V

    .line 1784610
    invoke-interface {p1}, LX/5wO;->n()Z

    move-result v6

    .line 1784611
    const/16 v1, 0x8

    const v2, 0x7f08154c

    const v3, 0x7f0209ae

    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object v0, p3

    invoke-interface/range {v0 .. v6}, LX/BS1;->a(IIIIZZ)V

    .line 1784612
    invoke-interface {p1}, LX/5wO;->m()Z

    move-result v6

    .line 1784613
    const/4 v1, 0x7

    const v2, 0x7f08154b

    const v3, 0x7f020888

    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object v0, p3

    invoke-interface/range {v0 .. v6}, LX/BS1;->a(IIIIZZ)V

    .line 1784614
    const/4 v1, 0x3

    const v2, 0x7f081543

    const v3, 0x7f0208ac

    const/4 v4, 0x2

    invoke-interface {p1}, LX/5wO;->l()Z

    move-result v5

    invoke-interface {p1}, LX/5wO;->l()Z

    move-result v6

    move-object v0, p3

    invoke-interface/range {v0 .. v6}, LX/BS1;->a(IIIIZZ)V

    .line 1784615
    invoke-static {p0, p1, p3}, LX/BRy;->a(LX/0ad;LX/5wO;LX/BS1;)V

    .line 1784616
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1784617
    invoke-interface {p1}, LX/5wO;->cd_()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ARE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    .line 1784618
    if-eqz v0, :cond_0

    invoke-virtual {p2, v4}, LX/03R;->asBoolean(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    move v6, v5

    .line 1784619
    :goto_0
    const/16 v1, 0xe

    const v2, 0x7f082829

    const v3, 0x7f02073d

    move-object v0, p3

    invoke-interface/range {v0 .. v6}, LX/BS1;->a(IIIIZZ)V

    .line 1784620
    const/4 v1, 0x4

    const v2, 0x7f081547

    const v3, 0x7f020975

    const/4 v4, 0x0

    invoke-interface {p1}, LX/5wO;->e()Z

    move-result v5

    invoke-interface {p1}, LX/5wO;->e()Z

    move-result v6

    move-object v0, p3

    invoke-interface/range {v0 .. v6}, LX/BS1;->a(IIIIZZ)V

    .line 1784621
    invoke-interface {p1}, LX/5wO;->cd_()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ARE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v6

    .line 1784622
    const/4 v1, 0x5

    const v2, 0x7f081549

    const v3, 0x7f02088f

    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object v0, p3

    invoke-interface/range {v0 .. v6}, LX/BS1;->a(IIIIZZ)V

    .line 1784623
    return-void

    :cond_0
    move v6, v4

    .line 1784624
    goto :goto_0
.end method

.method private static a(LX/0ad;LX/5wO;LX/BS1;)V
    .locals 7

    .prologue
    const/4 v1, 0x6

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1784591
    invoke-interface {p1}, LX/5wO;->cd_()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ARE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne v0, v2, :cond_2

    .line 1784592
    const/4 v0, 0x0

    .line 1784593
    invoke-interface {p1}, LX/5wO;->o()Lcom/facebook/timeline/widget/actionbar/protocol/TimelineHeaderActionFieldsGraphQLModels$NonSelfActionFieldsModel$FriendsModel;

    move-result-object v2

    if-nez v2, :cond_3

    .line 1784594
    :cond_0
    :goto_0
    move v0, v0

    .line 1784595
    if-eqz v0, :cond_1

    .line 1784596
    const v2, 0x7f080fad

    const v3, 0x7f020886

    const/4 v4, 0x2

    move-object v0, p2

    move v6, v5

    invoke-interface/range {v0 .. v6}, LX/BS1;->a(IIIIZZ)V

    .line 1784597
    :goto_1
    return-void

    .line 1784598
    :cond_1
    sget-short v0, LX/AqT;->e:S

    invoke-interface {p0, v0, v4}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1784599
    const v2, 0x7f080fac

    const v3, 0x7f020886

    move-object v0, p2

    move v6, v5

    invoke-interface/range {v0 .. v6}, LX/BS1;->a(IIIIZZ)V

    goto :goto_1

    .line 1784600
    :cond_2
    const v2, 0x7f080fac

    const v3, 0x7f020886

    move-object v0, p2

    move v5, v4

    move v6, v4

    invoke-interface/range {v0 .. v6}, LX/BS1;->a(IIIIZZ)V

    goto :goto_1

    .line 1784601
    :cond_3
    invoke-interface {p1}, LX/5wO;->o()Lcom/facebook/timeline/widget/actionbar/protocol/TimelineHeaderActionFieldsGraphQLModels$NonSelfActionFieldsModel$FriendsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/timeline/widget/actionbar/protocol/TimelineHeaderActionFieldsGraphQLModels$NonSelfActionFieldsModel$FriendsModel;->a()I

    move-result v2

    .line 1784602
    sget-short v3, LX/AqT;->a:S

    invoke-interface {p0, v3, v0}, LX/0ad;->a(SZ)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1784603
    sget v3, LX/AqT;->b:I

    const/16 p1, 0x1e

    invoke-interface {p0, v3, p1}, LX/0ad;->a(II)I

    move-result v3

    move v3, v3

    .line 1784604
    if-gt v2, v3, :cond_0

    if-lez v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method
