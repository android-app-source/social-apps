.class public final LX/Ajh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/AQ7;


# instance fields
.field private final a:LX/Ajj;

.field private final b:LX/1Nq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Nq",
            "<",
            "Lcom/facebook/feed/goodfriends/composer/GoodFriendsComposerPluginConfig;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/Ajj;LX/1Nq;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1708200
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1708201
    iput-object p1, p0, LX/Ajh;->a:LX/Ajj;

    .line 1708202
    iput-object p2, p0, LX/Ajh;->b:LX/1Nq;

    .line 1708203
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;LX/B5j;LX/B5f;)LX/AQ9;
    .locals 3
    .param p3    # LX/B5f;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ModelData::",
            "Lcom/facebook/ipc/composer/plugin/ComposerPluginDataProvider;",
            "DerivedData::",
            "Lcom/facebook/ipc/composer/plugin/ComposerPluginDerivedDataProvider;",
            "Mutation::",
            "Lcom/facebook/ipc/composer/plugin/ComposerPluginMutation",
            "<TMutation;>;>(",
            "Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;",
            "LX/B5j",
            "<TModelData;TDerivedData;TMutation;>;",
            "LX/B5f;",
            ")",
            "Lcom/facebook/ipc/composer/plugin/ComposerPlugin",
            "<TModelData;TDerivedData;TMutation;>;"
        }
    .end annotation

    .prologue
    .line 1708204
    iget-object v1, p0, LX/Ajh;->a:LX/Ajj;

    iget-object v0, p0, LX/Ajh;->b:LX/1Nq;

    const-class v2, Lcom/facebook/feed/goodfriends/composer/GoodFriendsComposerPluginConfig;

    invoke-virtual {v0, p1, v2}, LX/1Nq;->a(Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;Ljava/lang/Class;)LX/88f;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/goodfriends/composer/GoodFriendsComposerPluginConfig;

    .line 1708205
    new-instance p1, LX/Aji;

    const-class v2, Landroid/content/Context;

    invoke-interface {v1, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    const-class p0, LX/Ajc;

    invoke-interface {v1, p0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object p0

    check-cast p0, LX/Ajc;

    invoke-direct {p1, v0, p2, v2, p0}, LX/Aji;-><init>(Lcom/facebook/feed/goodfriends/composer/GoodFriendsComposerPluginConfig;LX/B5j;Landroid/content/Context;LX/Ajc;)V

    .line 1708206
    move-object v0, p1

    .line 1708207
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1708208
    sget-object v0, Lcom/facebook/feed/goodfriends/composer/GoodFriendsComposerPluginConfig;->a:Ljava/lang/String;

    return-object v0
.end method
