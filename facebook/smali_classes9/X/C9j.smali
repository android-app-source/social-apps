.class public final LX/C9j;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/C9m;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/C9k;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/C9m",
            "<TE;>.OfflineRetryComponentImpl;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/C9m;

.field private c:[Ljava/lang/String;

.field private d:I

.field private e:Ljava/util/BitSet;


# direct methods
.method public constructor <init>(LX/C9m;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 1854355
    iput-object p1, p0, LX/C9j;->b:LX/C9m;

    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 1854356
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "storyProps"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "environment"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/C9j;->c:[Ljava/lang/String;

    .line 1854357
    iput v3, p0, LX/C9j;->d:I

    .line 1854358
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/C9j;->d:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/C9j;->e:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/C9j;LX/1De;IILX/C9k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "II",
            "LX/C9m",
            "<TE;>.OfflineRetryComponentImpl;)V"
        }
    .end annotation

    .prologue
    .line 1854359
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 1854360
    iput-object p4, p0, LX/C9j;->a:LX/C9k;

    .line 1854361
    iget-object v0, p0, LX/C9j;->e:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 1854362
    return-void
.end method


# virtual methods
.method public final a(LX/1Pn;)LX/C9j;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)",
            "LX/C9m",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 1854363
    iget-object v0, p0, LX/C9j;->a:LX/C9k;

    iput-object p1, v0, LX/C9k;->b:LX/1Pn;

    .line 1854364
    iget-object v0, p0, LX/C9j;->e:Ljava/util/BitSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1854365
    return-object p0
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/C9j;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "LX/C9m",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 1854366
    iget-object v0, p0, LX/C9j;->a:LX/C9k;

    iput-object p1, v0, LX/C9k;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1854367
    iget-object v0, p0, LX/C9j;->e:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1854368
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 1854369
    invoke-super {p0}, LX/1X5;->a()V

    .line 1854370
    const/4 v0, 0x0

    iput-object v0, p0, LX/C9j;->a:LX/C9k;

    .line 1854371
    iget-object v0, p0, LX/C9j;->b:LX/C9m;

    iget-object v0, v0, LX/C9m;->b:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 1854372
    return-void
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/C9m;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1854373
    iget-object v1, p0, LX/C9j;->e:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/C9j;->e:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/C9j;->d:I

    if-ge v1, v2, :cond_2

    .line 1854374
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1854375
    :goto_0
    iget v2, p0, LX/C9j;->d:I

    if-ge v0, v2, :cond_1

    .line 1854376
    iget-object v2, p0, LX/C9j;->e:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1854377
    iget-object v2, p0, LX/C9j;->c:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1854378
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1854379
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1854380
    :cond_2
    iget-object v0, p0, LX/C9j;->a:LX/C9k;

    .line 1854381
    invoke-virtual {p0}, LX/C9j;->a()V

    .line 1854382
    return-object v0
.end method
