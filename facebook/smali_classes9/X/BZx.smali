.class public final LX/BZx;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;

.field private final b:LX/BZw;

.field public c:LX/BZy;

.field private d:Landroid/view/View;


# direct methods
.method public constructor <init>(Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;LX/BZw;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1798274
    iput-object p1, p0, LX/BZx;->a:Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1798275
    iput-object v0, p0, LX/BZx;->c:LX/BZy;

    .line 1798276
    iput-object v0, p0, LX/BZx;->d:Landroid/view/View;

    .line 1798277
    iput-object p2, p0, LX/BZx;->b:LX/BZw;

    .line 1798278
    return-void
.end method

.method private e()LX/BZy;
    .locals 2

    .prologue
    .line 1798255
    iget-object v0, p0, LX/BZx;->c:LX/BZy;

    if-nez v0, :cond_0

    .line 1798256
    invoke-direct {p0}, LX/BZx;->f()LX/BZy;

    move-result-object v0

    iput-object v0, p0, LX/BZx;->c:LX/BZy;

    .line 1798257
    iget-object v0, p0, LX/BZx;->c:LX/BZy;

    iget-object v1, p0, LX/BZx;->a:Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;

    .line 1798258
    iput-object v1, v0, LX/BZy;->a:Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;

    .line 1798259
    :cond_0
    iget-object v0, p0, LX/BZx;->c:LX/BZy;

    return-object v0
.end method

.method private f()LX/BZy;
    .locals 2

    .prologue
    .line 1798264
    sget-object v0, LX/BZu;->a:[I

    iget-object v1, p0, LX/BZx;->b:LX/BZw;

    invoke-virtual {v1}, LX/BZw;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1798265
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1798266
    :pswitch_0
    iget-object v0, p0, LX/BZx;->a:Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;

    iget-object v0, v0, Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;->q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BZy;

    .line 1798267
    :goto_0
    return-object v0

    .line 1798268
    :pswitch_1
    iget-object v0, p0, LX/BZx;->a:Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;

    iget-object v0, v0, Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BZy;

    goto :goto_0

    .line 1798269
    :pswitch_2
    iget-object v0, p0, LX/BZx;->a:Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;

    iget-object v0, v0, Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;->s:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BZy;

    goto :goto_0

    .line 1798270
    :pswitch_3
    iget-object v0, p0, LX/BZx;->a:Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;

    iget-object v0, v0, Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;->t:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BZy;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public final a()Landroid/view/View;
    .locals 2

    .prologue
    .line 1798271
    iget-object v0, p0, LX/BZx;->d:Landroid/view/View;

    if-nez v0, :cond_0

    .line 1798272
    invoke-direct {p0}, LX/BZx;->e()LX/BZy;

    move-result-object v0

    iget-object v1, p0, LX/BZx;->a:Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/BZy;->a(Landroid/view/LayoutInflater;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/BZx;->d:Landroid/view/View;

    .line 1798273
    :cond_0
    iget-object v0, p0, LX/BZx;->d:Landroid/view/View;

    return-object v0
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 1798260
    invoke-direct {p0}, LX/BZx;->e()LX/BZy;

    move-result-object v1

    iget-object v0, p0, LX/BZx;->a:Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v0, p0, LX/BZx;->a:Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;

    .line 1798261
    iget-object p0, v0, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    move-object v0, p0

    .line 1798262
    check-cast v0, LX/2EJ;

    invoke-virtual {v1, v2, v0}, LX/BZy;->a(Landroid/content/Context;LX/2EJ;)V

    .line 1798263
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1798251
    iget-object v0, p0, LX/BZx;->c:LX/BZy;

    if-eqz v0, :cond_0

    .line 1798252
    iget-object v0, p0, LX/BZx;->c:LX/BZy;

    invoke-virtual {v0}, LX/BZy;->a()V

    .line 1798253
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/BZx;->d:Landroid/view/View;

    .line 1798254
    return-void
.end method
