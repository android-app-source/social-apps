.class public final LX/BE9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/1rn;


# direct methods
.method public constructor <init>(LX/1rn;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1764177
    iput-object p1, p0, LX/BE9;->c:LX/1rn;

    iput-object p2, p0, LX/BE9;->a:Ljava/lang/String;

    iput-object p3, p0, LX/BE9;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1764170
    iget-object v0, p0, LX/BE9;->c:LX/1rn;

    iget-object v0, v0, LX/1rn;->f:LX/1rU;

    invoke-virtual {v0}, LX/1rU;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1764171
    iget-object v0, p0, LX/BE9;->c:LX/1rn;

    iget-object v0, v0, LX/1rn;->i:LX/1rp;

    invoke-virtual {v0}, LX/1rp;->a()LX/2kW;

    move-result-object v0

    iget-object v1, p0, LX/BE9;->a:Ljava/lang/String;

    invoke-static {v0, v1}, LX/3Cu;->b(LX/2kW;Ljava/lang/String;)Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;

    move-result-object v0

    .line 1764172
    if-nez v0, :cond_0

    .line 1764173
    const/4 v0, 0x0

    .line 1764174
    :goto_0
    return-object v0

    .line 1764175
    :cond_0
    invoke-interface {v0}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    goto :goto_0

    .line 1764176
    :cond_1
    iget-object v0, p0, LX/BE9;->c:LX/1rn;

    iget-object v0, v0, LX/1rn;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    iget-object v1, p0, LX/BE9;->a:Ljava/lang/String;

    iget-object v2, p0, LX/BE9;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    goto :goto_0
.end method
