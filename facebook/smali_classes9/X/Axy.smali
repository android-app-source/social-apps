.class public LX/Axy;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/photos/local/LocalMediaCursor;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/photos/local/LocalMediaCursor;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1729173
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1729174
    iput-object p1, p0, LX/Axy;->a:LX/0Ot;

    .line 1729175
    return-void
.end method

.method private static a(Lcom/facebook/ipc/media/MediaItem;)Lcom/facebook/friendsharing/souvenirs/models/SouvenirUriItem;
    .locals 9

    .prologue
    .line 1729122
    sget-object v0, LX/Axx;->a:[I

    invoke-virtual {p0}, Lcom/facebook/ipc/media/MediaItem;->m()LX/4gF;

    move-result-object v1

    invoke-virtual {v1}, LX/4gF;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1729123
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SouvenirsMediaItemsHelper.createUriItem: unknown type <"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/facebook/ipc/media/MediaItem;->m()LX/4gF;

    move-result-object v2

    invoke-virtual {v2}, LX/4gF;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ">.  MimeType = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/ipc/media/MediaItem;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1729124
    :pswitch_0
    check-cast p0, Lcom/facebook/photos/base/media/PhotoItem;

    .line 1729125
    new-instance v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirPhotoItem;

    .line 1729126
    iget-object v1, p0, Lcom/facebook/ipc/media/MediaItem;->c:Lcom/facebook/ipc/media/data/LocalMediaData;

    move-object v1, v1

    .line 1729127
    invoke-direct {v0, v1}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirPhotoItem;-><init>(Lcom/facebook/ipc/media/data/LocalMediaData;)V

    move-object v0, v0

    .line 1729128
    :goto_0
    return-object v0

    :pswitch_1
    check-cast p0, Lcom/facebook/photos/base/media/VideoItem;

    .line 1729129
    new-instance v3, Lcom/facebook/friendsharing/souvenirs/models/SouvenirVideoItem;

    .line 1729130
    iget-object v4, p0, Lcom/facebook/ipc/media/MediaItem;->c:Lcom/facebook/ipc/media/data/LocalMediaData;

    move-object v4, v4

    .line 1729131
    iget-wide v7, p0, Lcom/facebook/photos/base/media/VideoItem;->d:J

    move-wide v5, v7

    .line 1729132
    invoke-direct {v3, v4, v5, v6}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirVideoItem;-><init>(Lcom/facebook/ipc/media/data/LocalMediaData;J)V

    move-object v0, v3

    .line 1729133
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(LX/Axy;Lcom/facebook/friendsharing/souvenirs/models/SouvenirUriItem;)Lcom/facebook/ipc/media/MediaItem;
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1729144
    instance-of v0, p1, LX/4gL;

    if-eqz v0, :cond_1

    move-object v0, p1

    .line 1729145
    check-cast v0, LX/4gL;

    invoke-interface {v0}, LX/4gL;->a()Lcom/facebook/ipc/media/data/LocalMediaData;

    move-result-object v1

    .line 1729146
    iget-object v0, p0, LX/Axy;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8I2;

    .line 1729147
    iget-wide v6, v1, Lcom/facebook/ipc/media/data/LocalMediaData;->mMediaStoreId:J

    move-wide v4, v6

    .line 1729148
    invoke-virtual {v0, v4, v5}, LX/8I2;->a(J)Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    .line 1729149
    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirUriItem;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->d()Lcom/facebook/ipc/media/MediaIdKey;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/ipc/media/MediaIdKey;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1729150
    :goto_0
    return-object v0

    :cond_0
    move-object v0, v1

    .line 1729151
    :goto_1
    invoke-virtual {p1}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirUriItem;->iq_()LX/Ay0;

    move-result-object v1

    sget-object v4, LX/Ay0;->Photo:LX/Ay0;

    if-ne v1, v4, :cond_2

    .line 1729152
    new-instance v1, LX/74k;

    invoke-direct {v1}, LX/74k;-><init>()V

    .line 1729153
    iput-object v0, v1, LX/74k;->f:Lcom/facebook/ipc/media/data/LocalMediaData;

    .line 1729154
    move-object v0, v1

    .line 1729155
    invoke-virtual {v0}, LX/74k;->a()Lcom/facebook/photos/base/media/PhotoItem;

    move-result-object v0

    goto :goto_0

    .line 1729156
    :cond_1
    new-instance v0, LX/4gN;

    invoke-direct {v0}, LX/4gN;-><init>()V

    invoke-virtual {p1}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirUriItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4gN;->a(Lcom/facebook/ipc/media/data/MediaData;)LX/4gN;

    move-result-object v0

    invoke-virtual {v0}, LX/4gN;->a()Lcom/facebook/ipc/media/data/LocalMediaData;

    move-result-object v0

    goto :goto_1

    .line 1729157
    :cond_2
    instance-of v1, p1, Lcom/facebook/friendsharing/souvenirs/models/SouvenirVideoItem;

    if-eqz v1, :cond_4

    .line 1729158
    invoke-virtual {p1}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirUriItem;->iq_()LX/Ay0;

    move-result-object v1

    sget-object v4, LX/Ay0;->Video:LX/Ay0;

    if-ne v1, v4, :cond_3

    move v1, v2

    :goto_2
    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 1729159
    new-instance v1, LX/74m;

    invoke-direct {v1}, LX/74m;-><init>()V

    .line 1729160
    iput-object v0, v1, LX/74m;->e:Lcom/facebook/ipc/media/data/LocalMediaData;

    .line 1729161
    move-object v0, v1

    .line 1729162
    check-cast p1, Lcom/facebook/friendsharing/souvenirs/models/SouvenirVideoItem;

    invoke-virtual {p1}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirVideoItem;->d()J

    move-result-wide v2

    .line 1729163
    iput-wide v2, v0, LX/74m;->d:J

    .line 1729164
    move-object v0, v0

    .line 1729165
    invoke-virtual {v0}, LX/74m;->a()Lcom/facebook/photos/base/media/VideoItem;

    move-result-object v0

    goto :goto_0

    :cond_3
    move v1, v3

    .line 1729166
    goto :goto_2

    .line 1729167
    :cond_4
    invoke-virtual {p1}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirUriItem;->iq_()LX/Ay0;

    move-result-object v1

    sget-object v4, LX/Ay0;->Video:LX/Ay0;

    if-ne v1, v4, :cond_5

    :goto_3
    invoke-static {v2}, LX/0PB;->checkArgument(Z)V

    .line 1729168
    new-instance v1, LX/74m;

    invoke-direct {v1}, LX/74m;-><init>()V

    .line 1729169
    iput-object v0, v1, LX/74m;->e:Lcom/facebook/ipc/media/data/LocalMediaData;

    .line 1729170
    move-object v0, v1

    .line 1729171
    invoke-virtual {v0}, LX/74m;->a()Lcom/facebook/photos/base/media/VideoItem;

    move-result-object v0

    goto :goto_0

    :cond_5
    move v2, v3

    .line 1729172
    goto :goto_3
.end method

.method public static b(LX/0QB;)LX/Axy;
    .locals 2

    .prologue
    .line 1729142
    new-instance v0, LX/Axy;

    const/16 v1, 0x2e67

    invoke-static {p0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-direct {v0, v1}, LX/Axy;-><init>(LX/0Ot;)V

    .line 1729143
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;)LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1729136
    invoke-static {p1}, LX/Az7;->a(Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;)LX/0Rc;

    move-result-object v0

    .line 1729137
    new-instance p1, LX/0Pz;

    invoke-direct {p1}, LX/0Pz;-><init>()V

    .line 1729138
    :goto_0
    invoke-virtual {v0}, LX/0Rc;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1729139
    invoke-virtual {v0}, LX/0Rc;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/friendsharing/souvenirs/models/SouvenirUriItem;

    invoke-static {p0, v1}, LX/Axy;->a(LX/Axy;Lcom/facebook/friendsharing/souvenirs/models/SouvenirUriItem;)Lcom/facebook/ipc/media/MediaItem;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 1729140
    :cond_0
    invoke-virtual {p1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    move-object v0, v1

    .line 1729141
    return-object v0
.end method

.method public final a(J)Lcom/facebook/friendsharing/souvenirs/models/SouvenirUriItem;
    .locals 1

    .prologue
    .line 1729134
    iget-object v0, p0, LX/Axy;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8I2;

    invoke-virtual {v0, p1, p2}, LX/8I2;->a(J)Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    .line 1729135
    invoke-static {v0}, LX/Axy;->a(Lcom/facebook/ipc/media/MediaItem;)Lcom/facebook/friendsharing/souvenirs/models/SouvenirUriItem;

    move-result-object v0

    return-object v0
.end method
