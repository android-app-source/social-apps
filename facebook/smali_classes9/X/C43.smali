.class public LX/C43;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pf;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static h:LX/0Xm;


# instance fields
.field public final a:Landroid/app/Activity;

.field public final b:Landroid/content/res/Resources;

.field public final c:LX/1aw;

.field public final d:LX/0wM;

.field public final e:LX/1Vm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Vm",
            "<TE;>;"
        }
    .end annotation
.end field

.field public final f:Landroid/view/View$OnClickListener;

.field public final g:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/content/res/Resources;LX/1Ns;LX/0wM;LX/1Vm;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1846735
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1846736
    iput-object p1, p0, LX/C43;->a:Landroid/app/Activity;

    .line 1846737
    iput-object p2, p0, LX/C43;->b:Landroid/content/res/Resources;

    .line 1846738
    new-instance v0, LX/1Qg;

    invoke-direct {v0}, LX/1Qg;-><init>()V

    sget-object v1, LX/1aw;->a:LX/1DQ;

    invoke-virtual {p3, v0, v1}, LX/1Ns;->a(LX/1Qh;LX/1DQ;)LX/1aw;

    move-result-object v0

    iput-object v0, p0, LX/C43;->c:LX/1aw;

    .line 1846739
    iput-object p4, p0, LX/C43;->d:LX/0wM;

    .line 1846740
    iput-object p5, p0, LX/C43;->e:LX/1Vm;

    .line 1846741
    new-instance v0, LX/C42;

    invoke-direct {v0, p0}, LX/C42;-><init>(LX/C43;)V

    move-object v0, v0

    .line 1846742
    iput-object v0, p0, LX/C43;->f:Landroid/view/View$OnClickListener;

    .line 1846743
    iget-object v0, p0, LX/C43;->b:Landroid/content/res/Resources;

    const v1, 0x7f020e22

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1846744
    iget-object v1, p0, LX/C43;->d:LX/0wM;

    const p1, 0x7f02090f

    const/4 p2, -0x1

    invoke-virtual {v1, p1, p2}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 1846745
    iget-object p1, p0, LX/C43;->b:Landroid/content/res/Resources;

    const p2, 0x7f0b1cbe

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result p1

    .line 1846746
    new-instance p2, Landroid/graphics/drawable/InsetDrawable;

    invoke-direct {p2, v1, p1}, Landroid/graphics/drawable/InsetDrawable;-><init>(Landroid/graphics/drawable/Drawable;I)V

    .line 1846747
    new-instance v1, Landroid/graphics/drawable/LayerDrawable;

    const/4 p1, 0x2

    new-array p1, p1, [Landroid/graphics/drawable/Drawable;

    const/4 p3, 0x0

    aput-object v0, p1, p3

    const/4 v0, 0x1

    aput-object p2, p1, v0

    invoke-direct {v1, p1}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    move-object v0, v1

    .line 1846748
    iput-object v0, p0, LX/C43;->g:Landroid/graphics/drawable/Drawable;

    .line 1846749
    return-void
.end method

.method public static a(LX/0QB;)LX/C43;
    .locals 9

    .prologue
    .line 1846750
    const-class v1, LX/C43;

    monitor-enter v1

    .line 1846751
    :try_start_0
    sget-object v0, LX/C43;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1846752
    sput-object v2, LX/C43;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1846753
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1846754
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1846755
    new-instance v3, LX/C43;

    invoke-static {v0}, LX/0kU;->b(LX/0QB;)Landroid/app/Activity;

    move-result-object v4

    check-cast v4, Landroid/app/Activity;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v5

    check-cast v5, Landroid/content/res/Resources;

    const-class v6, LX/1Ns;

    invoke-interface {v0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/1Ns;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v7

    check-cast v7, LX/0wM;

    invoke-static {v0}, LX/1Vm;->a(LX/0QB;)LX/1Vm;

    move-result-object v8

    check-cast v8, LX/1Vm;

    invoke-direct/range {v3 .. v8}, LX/C43;-><init>(Landroid/app/Activity;Landroid/content/res/Resources;LX/1Ns;LX/0wM;LX/1Vm;)V

    .line 1846756
    move-object v0, v3

    .line 1846757
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1846758
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C43;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1846759
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1846760
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
