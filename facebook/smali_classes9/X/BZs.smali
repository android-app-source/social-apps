.class public LX/BZs;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1qM;


# instance fields
.field public final a:LX/18V;

.field public final b:LX/3g8;

.field public final c:LX/BZo;


# direct methods
.method public constructor <init>(LX/18V;LX/3g8;LX/BZo;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1798120
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1798121
    iput-object p1, p0, LX/BZs;->a:LX/18V;

    .line 1798122
    iput-object p2, p0, LX/BZs;->b:LX/3g8;

    .line 1798123
    iput-object p3, p0, LX/BZs;->c:LX/BZo;

    .line 1798124
    return-void
.end method


# virtual methods
.method public final handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 1798102
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v0, v0

    .line 1798103
    const-string v1, "appirater_should_show_dialog"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1798104
    :try_start_0
    iget-object v0, p0, LX/BZs;->a:LX/18V;

    iget-object v1, p0, LX/BZs;->b:LX/3g8;

    const/4 p1, 0x0

    invoke-virtual {v0, v1, p1}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/appirater/api/FetchISRConfigResult;

    .line 1798105
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_0
    .catch LX/2Oo; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1798106
    :goto_0
    move-object v0, v0

    .line 1798107
    :goto_1
    return-object v0

    .line 1798108
    :cond_0
    const-string v1, "appirater_create_report"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1798109
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 1798110
    const-string v1, "app_rater_report"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/appirater/api/AppRaterReport;

    .line 1798111
    :try_start_1
    iget-object v1, p0, LX/BZs;->a:LX/18V;

    iget-object v2, p0, LX/BZs;->c:LX/BZo;

    invoke-virtual {v1, v2, v0}, LX/18V;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1798112
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0
    :try_end_1
    .catch LX/2Oo; {:try_start_1 .. :try_end_1} :catch_1

    .line 1798113
    :goto_2
    move-object v0, v0

    .line 1798114
    goto :goto_1

    .line 1798115
    :cond_1
    new-instance v1, LX/4B3;

    invoke-direct {v1, v0}, LX/4B3;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1798116
    :catch_0
    move-exception v0

    .line 1798117
    sget-object v1, LX/1nY;->API_ERROR:LX/1nY;

    invoke-static {v1, v0}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;Ljava/lang/Throwable;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0

    .line 1798118
    :catch_1
    move-exception v0

    .line 1798119
    sget-object v1, LX/1nY;->API_ERROR:LX/1nY;

    invoke-static {v1, v0}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;Ljava/lang/Throwable;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_2
.end method
