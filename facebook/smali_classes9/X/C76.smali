.class public final LX/C76;
.super Landroid/text/style/ClickableSpan;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLStory;

.field public final synthetic b:LX/1W2;

.field public final synthetic c:LX/1Pq;

.field public final synthetic d:Lcom/facebook/feedplugins/graphqlstory/seefirsttombstone/SeeFirstTombstonePartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/graphqlstory/seefirsttombstone/SeeFirstTombstonePartDefinition;Lcom/facebook/graphql/model/GraphQLStory;LX/1W2;LX/1Pq;)V
    .locals 0

    .prologue
    .line 1850887
    iput-object p1, p0, LX/C76;->d:Lcom/facebook/feedplugins/graphqlstory/seefirsttombstone/SeeFirstTombstonePartDefinition;

    iput-object p2, p0, LX/C76;->a:Lcom/facebook/graphql/model/GraphQLStory;

    iput-object p3, p0, LX/C76;->b:LX/1W2;

    iput-object p4, p0, LX/C76;->c:LX/1Pq;

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    .line 1850888
    iget-object v0, p0, LX/C76;->d:Lcom/facebook/feedplugins/graphqlstory/seefirsttombstone/SeeFirstTombstonePartDefinition;

    iget-object v1, p0, LX/C76;->a:Lcom/facebook/graphql/model/GraphQLStory;

    iget-object v2, p0, LX/C76;->b:LX/1W2;

    .line 1850889
    iget-object v3, v2, LX/1W2;->b:Ljava/lang/String;

    move-object v2, v3

    .line 1850890
    iget-object v3, p0, LX/C76;->c:LX/1Pq;

    .line 1850891
    iget-object v4, v0, Lcom/facebook/feedplugins/graphqlstory/seefirsttombstone/SeeFirstTombstonePartDefinition;->c:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/2dj;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    const-string p0, "SEE_FIRST"

    const-string p1, "FEED_X_UNDO"

    invoke-virtual {v4, v5, p0, p1}, LX/2dj;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-object v4, v3

    .line 1850892
    check-cast v4, LX/1Pr;

    new-instance v5, LX/1W1;

    invoke-direct {v5, v1}, LX/1W1;-><init>(Lcom/facebook/graphql/model/GraphQLStory;)V

    invoke-interface {v4, v5, v1}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/1W2;

    .line 1850893
    const/4 v5, 0x0

    invoke-virtual {v4, v5}, LX/1W2;->a(Lcom/facebook/graphql/model/GraphQLActor;)V

    .line 1850894
    const/4 v5, 0x0

    .line 1850895
    iput-boolean v5, v4, LX/1W2;->d:Z

    .line 1850896
    invoke-interface {v3}, LX/1Pq;->iN_()V

    .line 1850897
    return-void
.end method

.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 1

    .prologue
    .line 1850898
    iget v0, p1, Landroid/text/TextPaint;->linkColor:I

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 1850899
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    .line 1850900
    return-void
.end method
