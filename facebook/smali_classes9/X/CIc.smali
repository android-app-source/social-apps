.class public LX/CIc;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "LX/0jT;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1874401
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Object;)Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingDocumentFragmentModel;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;)",
            "Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingDocumentFragmentModel;"
        }
    .end annotation

    .prologue
    .line 1874402
    instance-of v0, p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingCatalogQueryModel;

    if-eqz v0, :cond_0

    .line 1874403
    check-cast p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingCatalogQueryModel;

    invoke-virtual {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingCatalogQueryModel;->a()Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingDocumentFragmentModel;

    move-result-object v0

    .line 1874404
    :goto_0
    return-object v0

    .line 1874405
    :cond_0
    instance-of v0, p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingProductQueryModel;

    if-eqz v0, :cond_1

    .line 1874406
    check-cast p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingProductQueryModel;

    invoke-virtual {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingProductQueryModel;->a()Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingDocumentFragmentModel;

    move-result-object v0

    goto :goto_0

    .line 1874407
    :cond_1
    instance-of v0, p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$RichMediaDocumentQueryModel;

    if-eqz v0, :cond_2

    .line 1874408
    check-cast p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$RichMediaDocumentQueryModel;

    invoke-virtual {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$RichMediaDocumentQueryModel;->j()Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingDocumentFragmentModel;

    move-result-object v0

    goto :goto_0

    .line 1874409
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
