.class public final LX/Bwp;
.super LX/2oa;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2oa",
        "<",
        "LX/2ou;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Bwq;


# direct methods
.method public constructor <init>(LX/Bwq;LX/2oy;)V
    .locals 0

    .prologue
    .line 1834390
    iput-object p1, p0, LX/Bwp;->a:LX/Bwq;

    .line 1834391
    invoke-direct {p0, p2}, LX/2oa;-><init>(LX/2oy;)V

    .line 1834392
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/2ou;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1834378
    const-class v0, LX/2ou;

    return-object v0
.end method

.method public final b(LX/0b7;)V
    .locals 2

    .prologue
    .line 1834379
    check-cast p1, LX/2ou;

    .line 1834380
    iget-object v0, p0, LX/Bwp;->a:LX/Bwq;

    .line 1834381
    iget-boolean v1, v0, LX/3Ga;->c:Z

    move v1, v1

    .line 1834382
    move v0, v1

    .line 1834383
    if-nez v0, :cond_0

    .line 1834384
    :goto_0
    return-void

    .line 1834385
    :cond_0
    iget-object v0, p1, LX/2ou;->b:LX/2qV;

    invoke-virtual {v0}, LX/2qV;->isPlayingState()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1834386
    iget-object v0, p0, LX/Bwp;->a:LX/Bwq;

    invoke-static {v0}, LX/Bwq;->v(LX/Bwq;)V

    goto :goto_0

    .line 1834387
    :cond_1
    iget-object v0, p0, LX/Bwp;->a:LX/Bwq;

    iget-object v0, v0, LX/Bwq;->p:Lcom/facebook/widget/FbImageView;

    iget-object v1, p0, LX/Bwp;->a:LX/Bwq;

    iget-object v1, v1, LX/Bwq;->v:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/FbImageView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 1834388
    iget-object v0, p0, LX/Bwp;->a:LX/Bwq;

    invoke-static {v0}, LX/Bwq;->x(LX/Bwq;)V

    .line 1834389
    iget-object v0, p0, LX/Bwp;->a:LX/Bwq;

    iget-object v0, v0, LX/Bwq;->p:Lcom/facebook/widget/FbImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/facebook/widget/FbImageView;->setVisibility(I)V

    goto :goto_0
.end method
