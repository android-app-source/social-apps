.class public LX/AaZ;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0Tn;

.field public static final b:LX/0Tn;


# instance fields
.field private final c:LX/1b4;

.field public final d:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1688703
    sget-object v0, LX/0Tm;->g:LX/0Tn;

    const-string v1, "user_enabled_live_comment_translation"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/AaZ;->a:LX/0Tn;

    .line 1688704
    sget-object v0, LX/0Tm;->g:LX/0Tn;

    const-string v1, "when_live_nux_assets_last_downloaded"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/AaZ;->b:LX/0Tn;

    return-void
.end method

.method public constructor <init>(LX/1b4;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1688705
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1688706
    iput-object p1, p0, LX/AaZ;->c:LX/1b4;

    .line 1688707
    iput-object p2, p0, LX/AaZ;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1688708
    return-void
.end method

.method public static a(LX/0QB;)LX/AaZ;
    .locals 1

    .prologue
    .line 1688709
    invoke-static {p0}, LX/AaZ;->b(LX/0QB;)LX/AaZ;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/AaZ;
    .locals 3

    .prologue
    .line 1688710
    new-instance v2, LX/AaZ;

    invoke-static {p0}, LX/1b4;->b(LX/0QB;)LX/1b4;

    move-result-object v0

    check-cast v0, LX/1b4;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v1

    check-cast v1, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {v2, v0, v1}, LX/AaZ;-><init>(LX/1b4;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 1688711
    return-object v2
.end method


# virtual methods
.method public final a(Z)V
    .locals 2

    .prologue
    .line 1688712
    iget-object v0, p0, LX/AaZ;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/AaZ;->a:LX/0Tn;

    invoke-interface {v0, v1, p1}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 1688713
    return-void
.end method

.method public final a()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1688714
    iget-object v1, p0, LX/AaZ;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/AaZ;->a:LX/0Tn;

    invoke-interface {v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->b(LX/0Tn;)LX/03R;

    move-result-object v1

    sget-object v2, LX/03R;->UNSET:LX/03R;

    if-ne v1, v2, :cond_0

    .line 1688715
    iget-object v1, p0, LX/AaZ;->c:LX/1b4;

    .line 1688716
    invoke-static {v1}, LX/1b4;->ag(LX/1b4;)Ljava/lang/String;

    move-result-object v2

    .line 1688717
    if-nez v2, :cond_2

    .line 1688718
    const/4 v2, 0x0

    .line 1688719
    :goto_0
    move v1, v2

    .line 1688720
    invoke-virtual {p0, v1}, LX/AaZ;->a(Z)V

    .line 1688721
    :cond_0
    iget-object v1, p0, LX/AaZ;->c:LX/1b4;

    invoke-virtual {v1}, LX/1b4;->F()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/AaZ;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/AaZ;->a:LX/0Tn;

    invoke-interface {v1, v2, v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    return v0

    :cond_2
    const-string v3, "auto_enable"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    goto :goto_0
.end method
