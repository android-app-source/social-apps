.class public LX/CAE;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/0ad;

.field public b:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>(LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1855048
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1855049
    iput-object p1, p0, LX/CAE;->a:LX/0ad;

    .line 1855050
    return-void
.end method

.method public static a(LX/0QB;)LX/CAE;
    .locals 4

    .prologue
    .line 1855051
    const-class v1, LX/CAE;

    monitor-enter v1

    .line 1855052
    :try_start_0
    sget-object v0, LX/CAE;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1855053
    sput-object v2, LX/CAE;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1855054
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1855055
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1855056
    new-instance p0, LX/CAE;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-direct {p0, v3}, LX/CAE;-><init>(LX/0ad;)V

    .line 1855057
    move-object v0, p0

    .line 1855058
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1855059
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/CAE;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1855060
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1855061
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
