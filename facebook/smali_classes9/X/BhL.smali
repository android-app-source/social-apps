.class public final LX/BhL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Landroid/widget/LinearLayout;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/BhM;


# direct methods
.method public constructor <init>(LX/BhM;Landroid/widget/LinearLayout;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1809009
    iput-object p1, p0, LX/BhL;->c:LX/BhM;

    iput-object p2, p0, LX/BhL;->a:Landroid/widget/LinearLayout;

    iput-object p3, p0, LX/BhL;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v0, 0x2

    const v1, -0x634d069

    invoke-static {v0, v4, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1809010
    iget-object v1, p0, LX/BhL;->c:LX/BhM;

    iget-object v1, v1, LX/BhM;->d:LX/17Y;

    iget-object v2, p0, LX/BhL;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    sget-object v3, LX/0ax;->aK:Ljava/lang/String;

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, LX/BhL;->b:Ljava/lang/String;

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 1809011
    if-eqz v1, :cond_0

    .line 1809012
    iget-object v2, p0, LX/BhL;->c:LX/BhM;

    iget-object v2, v2, LX/BhM;->e:Lcom/facebook/content/SecureContextHelper;

    iget-object v3, p0, LX/BhL;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v2, v1, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1809013
    :goto_0
    const v1, -0x547ea39e

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 1809014
    :cond_0
    iget-object v1, p0, LX/BhL;->c:LX/BhM;

    iget-object v1, v1, LX/BhM;->f:LX/03V;

    sget-object v2, LX/BhM;->a:Ljava/lang/Class;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Could not open the recent edits view because the intent was null"

    invoke-virtual {v1, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
