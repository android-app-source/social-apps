.class public final enum LX/BeS;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/BeS;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/BeS;

.field public static final enum INTRO:LX/BeS;

.field public static final enum QUESTION:LX/BeS;

.field public static final enum THANK_YOU:LX/BeS;

.field public static final enum UNDEFINED:LX/BeS;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1804910
    new-instance v0, LX/BeS;

    const-string v1, "INTRO"

    invoke-direct {v0, v1, v2}, LX/BeS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BeS;->INTRO:LX/BeS;

    .line 1804911
    new-instance v0, LX/BeS;

    const-string v1, "QUESTION"

    invoke-direct {v0, v1, v3}, LX/BeS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BeS;->QUESTION:LX/BeS;

    .line 1804912
    new-instance v0, LX/BeS;

    const-string v1, "THANK_YOU"

    invoke-direct {v0, v1, v4}, LX/BeS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BeS;->THANK_YOU:LX/BeS;

    .line 1804913
    new-instance v0, LX/BeS;

    const-string v1, "UNDEFINED"

    invoke-direct {v0, v1, v5}, LX/BeS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BeS;->UNDEFINED:LX/BeS;

    .line 1804914
    const/4 v0, 0x4

    new-array v0, v0, [LX/BeS;

    sget-object v1, LX/BeS;->INTRO:LX/BeS;

    aput-object v1, v0, v2

    sget-object v1, LX/BeS;->QUESTION:LX/BeS;

    aput-object v1, v0, v3

    sget-object v1, LX/BeS;->THANK_YOU:LX/BeS;

    aput-object v1, v0, v4

    sget-object v1, LX/BeS;->UNDEFINED:LX/BeS;

    aput-object v1, v0, v5

    sput-object v0, LX/BeS;->$VALUES:[LX/BeS;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1804915
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/BeS;
    .locals 1

    .prologue
    .line 1804916
    const-class v0, LX/BeS;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/BeS;

    return-object v0
.end method

.method public static values()[LX/BeS;
    .locals 1

    .prologue
    .line 1804917
    sget-object v0, LX/BeS;->$VALUES:[LX/BeS;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/BeS;

    return-object v0
.end method
