.class public final LX/Cab;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3IA;
.implements LX/3IB;


# instance fields
.field public final synthetic a:LX/CaV;


# direct methods
.method public constructor <init>(LX/CaV;)V
    .locals 0

    .prologue
    .line 1918215
    iput-object p1, p0, LX/Cab;->a:LX/CaV;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(LX/CaV;B)V
    .locals 0

    .prologue
    .line 1918214
    invoke-direct {p0, p1}, LX/Cab;-><init>(LX/CaV;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 1918200
    return-void
.end method

.method public final a(FF)V
    .locals 1

    .prologue
    .line 1918212
    iget-object v0, p0, LX/Cab;->a:LX/CaV;

    iget-object v0, v0, LX/8wv;->n:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    invoke-virtual {v0, p1, p2}, LX/2qW;->c(FF)V

    .line 1918213
    return-void
.end method

.method public final a(F)Z
    .locals 1

    .prologue
    .line 1918210
    iget-object v0, p0, LX/Cab;->a:LX/CaV;

    iget-object v0, v0, LX/8wv;->n:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    invoke-virtual {v0, p1}, LX/2qW;->a(F)V

    .line 1918211
    const/4 v0, 0x1

    return v0
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 1918207
    iget-object v0, p0, LX/Cab;->a:LX/CaV;

    iget-object v0, v0, LX/8wv;->a:LX/1xG;

    iget-object v1, p0, LX/Cab;->a:LX/CaV;

    iget-object v1, v1, LX/8wv;->r:Ljava/lang/String;

    iget-object v2, p0, LX/Cab;->a:LX/CaV;

    iget-object v2, v2, LX/8wv;->p:LX/7Dj;

    invoke-virtual {v0, v1, v2}, LX/1xG;->h(Ljava/lang/String;LX/7Dj;)V

    .line 1918208
    iget-object v0, p0, LX/Cab;->a:LX/CaV;

    iget-object v0, v0, LX/8wv;->n:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    invoke-virtual {v0}, LX/2qW;->f()V

    .line 1918209
    return-void
.end method

.method public final b(FF)V
    .locals 2

    .prologue
    .line 1918205
    iget-object v0, p0, LX/Cab;->a:LX/CaV;

    iget-object v0, v0, LX/8wv;->n:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, LX/2qW;->d(FF)V

    .line 1918206
    return-void
.end method

.method public final c()Z
    .locals 3

    .prologue
    .line 1918203
    iget-object v0, p0, LX/Cab;->a:LX/CaV;

    iget-object v0, v0, LX/8wv;->a:LX/1xG;

    iget-object v1, p0, LX/Cab;->a:LX/CaV;

    iget-object v1, v1, LX/8wv;->r:Ljava/lang/String;

    sget-object v2, LX/7Dj;->OTHER:LX/7Dj;

    invoke-virtual {v0, v1, v2}, LX/1xG;->g(Ljava/lang/String;LX/7Dj;)V

    .line 1918204
    const/4 v0, 0x1

    return v0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 1918201
    iget-object v0, p0, LX/Cab;->a:LX/CaV;

    iget-object v0, v0, LX/8wv;->n:Lcom/facebook/spherical/photo/SphericalPhotoTextureView;

    invoke-virtual {v0}, LX/2qW;->g()V

    .line 1918202
    return-void
.end method
