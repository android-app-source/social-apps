.class public LX/Awh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1uy;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1uy",
        "<",
        "Ljava/io/File;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/AwR;

.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/AwR;Ljava/lang/String;)V
    .locals 0
    .param p1    # LX/AwR;
        .annotation runtime Lcom/facebook/friendsharing/inspiration/styletransfer/StyleTransferCache;
        .end annotation
    .end param

    .prologue
    .line 1726937
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1726938
    iput-object p1, p0, LX/Awh;->a:LX/AwR;

    .line 1726939
    iput-object p2, p0, LX/Awh;->b:Ljava/lang/String;

    .line 1726940
    return-void
.end method


# virtual methods
.method public final a(Ljava/io/InputStream;JLX/2ur;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1726941
    const/4 v0, 0x0

    .line 1726942
    :try_start_0
    iget-object v1, p0, LX/Awh;->a:LX/AwR;

    iget-object v2, p0, LX/Awh;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, p1}, LX/AwR;->a(Ljava/lang/String;Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v2

    .line 1726943
    if-nez v2, :cond_0

    .line 1726944
    :goto_0
    return-object v0

    .line 1726945
    :cond_0
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    goto :goto_0

    .line 1726946
    :catch_0
    goto :goto_0

    :catch_1
    goto :goto_0

    :catch_2
    goto :goto_0
.end method
