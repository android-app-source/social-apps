.class public LX/Ccv;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/Ccv;


# instance fields
.field public a:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Lcom/facebook/places/create/citypicker/FetchCityParam;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "LX/0Px",
            "<",
            "Lcom/facebook/places/graphql/PlacesGraphQLInterfaces$CheckinPlace;",
            ">;>;"
        }
    .end annotation
.end field

.field public final c:Lcom/facebook/places/create/citypicker/FetchCityRunner;

.field public final d:LX/0Sh;

.field public final e:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/places/create/citypicker/CitySearchResultsManager$Listener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/places/create/citypicker/FetchCityRunner;LX/0Sh;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1921794
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1921795
    iput-object p1, p0, LX/Ccv;->c:Lcom/facebook/places/create/citypicker/FetchCityRunner;

    .line 1921796
    iput-object p2, p0, LX/Ccv;->d:LX/0Sh;

    .line 1921797
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/Ccv;->a:LX/0am;

    .line 1921798
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/Ccv;->b:LX/0am;

    .line 1921799
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LX/Ccv;->e:Ljava/util/Set;

    .line 1921800
    return-void
.end method

.method public static a(LX/0QB;)LX/Ccv;
    .locals 5

    .prologue
    .line 1921801
    sget-object v0, LX/Ccv;->f:LX/Ccv;

    if-nez v0, :cond_1

    .line 1921802
    const-class v1, LX/Ccv;

    monitor-enter v1

    .line 1921803
    :try_start_0
    sget-object v0, LX/Ccv;->f:LX/Ccv;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1921804
    if-eqz v2, :cond_0

    .line 1921805
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1921806
    new-instance p0, LX/Ccv;

    invoke-static {v0}, Lcom/facebook/places/create/citypicker/FetchCityRunner;->b(LX/0QB;)Lcom/facebook/places/create/citypicker/FetchCityRunner;

    move-result-object v3

    check-cast v3, Lcom/facebook/places/create/citypicker/FetchCityRunner;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v4

    check-cast v4, LX/0Sh;

    invoke-direct {p0, v3, v4}, LX/Ccv;-><init>(Lcom/facebook/places/create/citypicker/FetchCityRunner;LX/0Sh;)V

    .line 1921807
    move-object v0, p0

    .line 1921808
    sput-object v0, LX/Ccv;->f:LX/Ccv;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1921809
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1921810
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1921811
    :cond_1
    sget-object v0, LX/Ccv;->f:LX/Ccv;

    return-object v0

    .line 1921812
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1921813
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
