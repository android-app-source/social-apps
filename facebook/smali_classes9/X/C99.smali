.class public LX/C99;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static h:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1EZ;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kb;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0ad;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0p9;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9A1;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1853775
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1853776
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1853777
    iput-object v0, p0, LX/C99;->a:LX/0Ot;

    .line 1853778
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1853779
    iput-object v0, p0, LX/C99;->b:LX/0Ot;

    .line 1853780
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1853781
    iput-object v0, p0, LX/C99;->c:LX/0Ot;

    .line 1853782
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1853783
    iput-object v0, p0, LX/C99;->d:LX/0Ot;

    .line 1853784
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1853785
    iput-object v0, p0, LX/C99;->e:LX/0Ot;

    .line 1853786
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1853787
    iput-object v0, p0, LX/C99;->f:LX/0Ot;

    .line 1853788
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1853789
    iput-object v0, p0, LX/C99;->g:LX/0Ot;

    .line 1853790
    return-void
.end method

.method public static a(LX/0QB;)LX/C99;
    .locals 10

    .prologue
    .line 1853762
    const-class v1, LX/C99;

    monitor-enter v1

    .line 1853763
    :try_start_0
    sget-object v0, LX/C99;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1853764
    sput-object v2, LX/C99;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1853765
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1853766
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1853767
    new-instance v3, LX/C99;

    invoke-direct {v3}, LX/C99;-><init>()V

    .line 1853768
    const/16 v4, 0xf39

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x2ca

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x1032

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x2cb

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const-class v8, Landroid/content/Context;

    invoke-interface {v0, v8}, LX/0QC;->getLazy(Ljava/lang/Class;)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x1d69

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 p0, 0x3cf

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 1853769
    iput-object v4, v3, LX/C99;->a:LX/0Ot;

    iput-object v5, v3, LX/C99;->b:LX/0Ot;

    iput-object v6, v3, LX/C99;->c:LX/0Ot;

    iput-object v7, v3, LX/C99;->d:LX/0Ot;

    iput-object v8, v3, LX/C99;->e:LX/0Ot;

    iput-object v9, v3, LX/C99;->f:LX/0Ot;

    iput-object p0, v3, LX/C99;->g:LX/0Ot;

    .line 1853770
    move-object v0, v3

    .line 1853771
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1853772
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C99;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1853773
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1853774
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/5ON;
    .locals 2

    .prologue
    .line 1853697
    new-instance v1, LX/5ON;

    iget-object v0, p0, LX/C99;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-direct {v1, v0}, LX/5ON;-><init>(Landroid/content/Context;)V

    .line 1853698
    const v0, 0x7f110010

    invoke-virtual {v1, v0}, LX/5OM;->b(I)V

    .line 1853699
    return-object v1
.end method

.method public final a(LX/5ON;Lcom/facebook/graphql/model/GraphQLStory;Landroid/view/View;)V
    .locals 7

    .prologue
    .line 1853708
    invoke-virtual {p1}, LX/5OM;->c()LX/5OG;

    move-result-object v1

    .line 1853709
    const v0, 0x7f0d3217

    invoke-virtual {v1, v0}, LX/5OG;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 1853710
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1853711
    if-nez p2, :cond_6

    .line 1853712
    :cond_0
    :goto_0
    move v2, v4

    .line 1853713
    if-eqz v0, :cond_1

    .line 1853714
    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1853715
    :cond_1
    const v0, 0x7f0d3216

    invoke-virtual {v1, v0}, LX/5OG;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    .line 1853716
    if-eqz v3, :cond_2

    .line 1853717
    if-nez v2, :cond_4

    const/4 v2, 0x0

    .line 1853718
    if-nez p2, :cond_a

    move v0, v2

    .line 1853719
    :goto_1
    move v0, v0

    .line 1853720
    if-eqz v0, :cond_4

    const/4 v0, 0x1

    :goto_2
    invoke-interface {v3, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1853721
    :cond_2
    const v0, 0x7f0d3218

    invoke-virtual {v1, v0}, LX/5OG;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 1853722
    if-eqz v0, :cond_3

    .line 1853723
    const/4 v2, 0x0

    .line 1853724
    if-nez p2, :cond_d

    move v1, v2

    .line 1853725
    :goto_3
    move v1, v1

    .line 1853726
    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1853727
    :cond_3
    iget-boolean v0, p1, LX/0ht;->r:Z

    move v0, v0

    .line 1853728
    if-nez v0, :cond_5

    .line 1853729
    invoke-virtual {p1, p3}, LX/0ht;->f(Landroid/view/View;)V

    .line 1853730
    :goto_4
    return-void

    .line 1853731
    :cond_4
    const/4 v0, 0x0

    goto :goto_2

    .line 1853732
    :cond_5
    invoke-virtual {p1}, LX/0ht;->l()V

    goto :goto_4

    .line 1853733
    :cond_6
    iget-object v2, p0, LX/C99;->a:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1EZ;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStory;->S()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/1EZ;->e(Ljava/lang/String;)Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v6

    .line 1853734
    if-eqz v6, :cond_0

    .line 1853735
    iget-boolean v2, v6, Lcom/facebook/photos/upload/operation/UploadOperation;->au:Z

    move v2, v2

    .line 1853736
    if-nez v2, :cond_0

    .line 1853737
    iget-object v2, p0, LX/C99;->d:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0p9;

    const-string v3, "WIFI"

    invoke-virtual {v2, v3}, LX/0p9;->a(Ljava/lang/String;)LX/0p3;

    move-result-object v2

    .line 1853738
    sget-object v3, LX/0p3;->EXCELLENT:LX/0p3;

    invoke-virtual {v2, v3}, LX/0p3;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_7

    sget-object v3, LX/0p3;->GOOD:LX/0p3;

    invoke-virtual {v2, v3}, LX/0p3;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    :cond_7
    move v3, v5

    .line 1853739
    :goto_5
    iget-object v2, p0, LX/C99;->b:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_9

    iget-object v2, p0, LX/C99;->b:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0kb;

    invoke-virtual {v2}, LX/0kb;->d()Z

    move-result v2

    if-eqz v2, :cond_9

    iget-object v2, p0, LX/C99;->b:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0kb;

    invoke-virtual {v2}, LX/0kb;->b()Landroid/net/NetworkInfo;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getType()I

    move-result v2

    if-ne v2, v5, :cond_9

    move v2, v5

    .line 1853740
    :goto_6
    if-nez v2, :cond_0

    if-eqz v3, :cond_0

    .line 1853741
    iget v2, v6, Lcom/facebook/photos/upload/operation/UploadOperation;->T:I

    move v2, v2

    .line 1853742
    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    iget-object v2, p0, LX/C99;->c:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0ad;

    sget-short v3, LX/0ws;->fQ:S

    invoke-interface {v2, v3, v4}, LX/0ad;->a(SZ)Z

    move-result v2

    if-eqz v2, :cond_0

    move v4, v5

    goto/16 :goto_0

    :cond_8
    move v3, v4

    .line 1853743
    goto :goto_5

    :cond_9
    move v2, v4

    .line 1853744
    goto :goto_6

    .line 1853745
    :cond_a
    iget-object v0, p0, LX/C99;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1EZ;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStory;->S()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/1EZ;->e(Ljava/lang/String;)Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v0

    .line 1853746
    if-eqz v0, :cond_c

    .line 1853747
    iget-boolean v4, v0, Lcom/facebook/photos/upload/operation/UploadOperation;->au:Z

    move v4, v4

    .line 1853748
    if-nez v4, :cond_c

    .line 1853749
    iget v4, v0, Lcom/facebook/photos/upload/operation/UploadOperation;->T:I

    move v0, v4

    .line 1853750
    const/4 v4, -0x1

    if-eq v0, v4, :cond_b

    const/4 v0, 0x1

    goto/16 :goto_1

    :cond_b
    move v0, v2

    goto/16 :goto_1

    :cond_c
    move v0, v2

    .line 1853751
    goto/16 :goto_1

    .line 1853752
    :cond_d
    iget-object v1, p0, LX/C99;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1EZ;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStory;->S()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, LX/1EZ;->e(Ljava/lang/String;)Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v1

    .line 1853753
    if-nez v1, :cond_e

    move v1, v2

    .line 1853754
    goto/16 :goto_3

    .line 1853755
    :cond_e
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 1853756
    iget-object v2, p0, LX/C99;->b:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_f

    iget-object v2, p0, LX/C99;->b:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0kb;

    invoke-virtual {v2}, LX/0kb;->d()Z

    move-result v2

    if-eqz v2, :cond_f

    iget-object v2, p0, LX/C99;->b:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0kb;

    invoke-virtual {v2}, LX/0kb;->b()Landroid/net/NetworkInfo;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getType()I

    move-result v2

    if-ne v2, v3, :cond_f

    move v2, v3

    .line 1853757
    :goto_7
    iget-boolean p2, v1, Lcom/facebook/photos/upload/operation/UploadOperation;->au:Z

    move p2, p2

    .line 1853758
    if-eqz p2, :cond_10

    if-nez v2, :cond_10

    :goto_8
    move v1, v3

    .line 1853759
    goto/16 :goto_3

    :cond_f
    move v2, v4

    .line 1853760
    goto :goto_7

    :cond_10
    move v3, v4

    .line 1853761
    goto :goto_8
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStory;Landroid/view/View;)V
    .locals 5

    .prologue
    .line 1853700
    iget-object v0, p0, LX/C99;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->S()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->d(Ljava/lang/String;)Lcom/facebook/composer/publish/common/PendingStory;

    move-result-object v1

    .line 1853701
    if-nez v1, :cond_0

    .line 1853702
    :goto_0
    return-void

    .line 1853703
    :cond_0
    invoke-virtual {p0}, LX/C99;->a()LX/5ON;

    move-result-object v2

    .line 1853704
    new-instance v3, LX/C98;

    iget-object v0, p0, LX/C99;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9A1;

    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v0, v4}, LX/C98;-><init>(LX/9A1;Landroid/content/Context;)V

    .line 1853705
    iput-object v1, v3, LX/C98;->c:Lcom/facebook/composer/publish/common/PendingStory;

    .line 1853706
    iput-object v3, v2, LX/5OM;->p:LX/5OO;

    .line 1853707
    invoke-virtual {p0, v2, p1, p2}, LX/C99;->a(LX/5ON;Lcom/facebook/graphql/model/GraphQLStory;Landroid/view/View;)V

    goto :goto_0
.end method
