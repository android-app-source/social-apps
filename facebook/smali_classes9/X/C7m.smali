.class public LX/C7m;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pb;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/C7r;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/C7m",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/C7r;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1851802
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1851803
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/C7m;->b:LX/0Zi;

    .line 1851804
    iput-object p1, p0, LX/C7m;->a:LX/0Ot;

    .line 1851805
    return-void
.end method

.method public static a(LX/0QB;)LX/C7m;
    .locals 4

    .prologue
    .line 1851806
    const-class v1, LX/C7m;

    monitor-enter v1

    .line 1851807
    :try_start_0
    sget-object v0, LX/C7m;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1851808
    sput-object v2, LX/C7m;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1851809
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1851810
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1851811
    new-instance v3, LX/C7m;

    const/16 p0, 0x1f9f

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/C7m;-><init>(LX/0Ot;)V

    .line 1851812
    move-object v0, v3

    .line 1851813
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1851814
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C7m;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1851815
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1851816
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 3

    .prologue
    .line 1851817
    check-cast p2, LX/C7l;

    .line 1851818
    iget-object v0, p0, LX/C7m;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/C7r;

    iget-object v1, p2, LX/C7l;->a:LX/1Pn;

    iget-object v2, p2, LX/C7l;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v0, p1, v1, v2}, LX/C7r;->a(LX/1De;LX/1Pn;Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1Dg;

    move-result-object v0

    .line 1851819
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1851820
    invoke-static {}, LX/1dS;->b()V

    .line 1851821
    const/4 v0, 0x0

    return-object v0
.end method
