.class public LX/CGN;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1865319
    const-class v0, LX/CGN;

    sput-object v0, LX/CGN;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1865320
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1865321
    return-void
.end method

.method public static a(LX/0Px;ILX/CFp;Landroid/content/Context;LX/0YU;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/greetingcards/verve/model/VMView;",
            ">;I",
            "LX/CFp;",
            "Landroid/content/Context;",
            "LX/0YU",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1865322
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_3

    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/greetingcards/verve/model/VMView;

    .line 1865323
    iget v3, v0, Lcom/facebook/greetingcards/verve/model/VMView;->a:I

    invoke-virtual {p4, v3}, LX/0YU;->a(I)Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_2

    .line 1865324
    sget-object v3, LX/CFl;->UNKNOWN:LX/CFl;

    .line 1865325
    invoke-virtual {v0}, Lcom/facebook/greetingcards/verve/model/VMView;->o()LX/CFo;

    move-result-object v4

    sget-object v5, LX/CFo;->MEDIA:LX/CFo;

    if-ne v4, v5, :cond_0

    .line 1865326
    iget-object v3, p2, LX/CFp;->d:LX/CEU;

    move-object v3, v3

    .line 1865327
    iget-object v4, v0, Lcom/facebook/greetingcards/verve/model/VMView;->src:Ljava/lang/String;

    invoke-interface {v3, v4}, LX/CEU;->a(Ljava/lang/String;)LX/CFl;

    move-result-object v3

    .line 1865328
    :cond_0
    iget-object v4, p2, LX/CFp;->e:LX/CGR;

    move-object v4, v4

    .line 1865329
    iget-object v5, v4, LX/CGR;->b:LX/CEl;

    invoke-interface {v5, v0}, LX/CEl;->a(Lcom/facebook/greetingcards/verve/model/VMView;)Ljava/lang/Enum;

    move-result-object v5

    check-cast v5, LX/CEj;

    .line 1865330
    if-nez v5, :cond_1

    .line 1865331
    sget-object v5, LX/CGP;->b:[I

    invoke-virtual {v0}, Lcom/facebook/greetingcards/verve/model/VMView;->o()LX/CFo;

    move-result-object v6

    invoke-virtual {v6}, LX/CFo;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 1865332
    sget-object v5, LX/CGQ;->RECT:LX/CGQ;

    :goto_1
    move-object v5, v5

    .line 1865333
    :cond_1
    iget-object v6, v4, LX/CGR;->a:LX/0Px;

    invoke-virtual {v6, v5}, LX/0Px;->indexOf(Ljava/lang/Object;)I

    move-result v5

    move v5, v5

    .line 1865334
    iget-object v6, v4, LX/CGR;->c:LX/CGO;

    .line 1865335
    iget-object v7, v6, LX/CGO;->a:Landroid/util/SparseArray;

    invoke-virtual {v7, v5}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/ArrayList;

    .line 1865336
    if-eqz v7, :cond_6

    invoke-virtual {v7}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_6

    .line 1865337
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v8

    add-int/lit8 v4, v8, -0x1

    .line 1865338
    invoke-virtual {v7, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/view/View;

    .line 1865339
    invoke-virtual {v7, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 1865340
    :goto_2
    move-object v6, v8

    .line 1865341
    move-object v4, v6

    .line 1865342
    if-eqz v4, :cond_4

    .line 1865343
    :goto_3
    move-object v3, v4

    .line 1865344
    invoke-static {v0, p1, p2, v3}, LX/CGN;->a(Lcom/facebook/greetingcards/verve/model/VMView;ILX/CFp;Landroid/view/View;)V

    .line 1865345
    move-object v3, v3

    .line 1865346
    iget v0, v0, Lcom/facebook/greetingcards/verve/model/VMView;->a:I

    invoke-virtual {p4, v0, v3}, LX/0YU;->a(ILjava/lang/Object;)V

    .line 1865347
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1865348
    :cond_3
    return-void

    .line 1865349
    :cond_4
    sget-object v4, LX/CGM;->a:[I

    invoke-virtual {v0}, Lcom/facebook/greetingcards/verve/model/VMView;->o()LX/CFo;

    move-result-object v6

    invoke-virtual {v6}, LX/CFo;->ordinal()I

    move-result v6

    aget v4, v4, v6

    packed-switch v4, :pswitch_data_1

    .line 1865350
    sget-object v4, LX/CGN;->a:Ljava/lang/Class;

    const-string v6, "Unrecognized verve view type: %s. Display default view."

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-virtual {v0}, Lcom/facebook/greetingcards/verve/model/VMView;->o()LX/CFo;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v4, v6, v7}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1865351
    new-instance v4, Landroid/view/View;

    invoke-direct {v4, p3}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 1865352
    :goto_4
    const v6, 0x7f0d01d3

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v6, v5}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 1865353
    const v5, 0x7f0d01d5

    invoke-virtual {v4, v5, v3}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    goto :goto_3

    .line 1865354
    :pswitch_0
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 1865355
    sget-object v4, LX/CGM;->b:[I

    invoke-virtual {v3}, LX/CFl;->ordinal()I

    move-result v6

    aget v4, v4, v6

    packed-switch v4, :pswitch_data_2

    .line 1865356
    new-instance v4, Landroid/view/View;

    invoke-direct {v4, p3}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 1865357
    sget-object v6, LX/CGN;->a:Ljava/lang/Class;

    const-string v7, "Unrecognized verve media type: %s. Display default view."

    new-array v8, v10, [Ljava/lang/Object;

    aput-object v3, v8, v9

    invoke-static {v6, v7, v8}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1865358
    :goto_5
    if-nez v4, :cond_5

    .line 1865359
    sget-object v4, LX/CGN;->a:Ljava/lang/Class;

    const-string v6, "Client does not return views for media type: %s. Display default view."

    new-array v7, v10, [Ljava/lang/Object;

    aput-object v3, v7, v9

    invoke-static {v4, v6, v7}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1865360
    new-instance v4, Landroid/view/View;

    invoke-direct {v4, p3}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 1865361
    :cond_5
    move-object v4, v4

    .line 1865362
    goto :goto_4

    .line 1865363
    :pswitch_1
    new-instance v4, Landroid/view/View;

    invoke-direct {v4, p3}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    goto :goto_4

    .line 1865364
    :pswitch_2
    new-instance v4, Landroid/widget/TextView;

    invoke-direct {v4, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    goto :goto_4

    .line 1865365
    :pswitch_3
    iget-object v4, p2, LX/CFp;->b:LX/CEl;

    move-object v4, v4

    .line 1865366
    invoke-interface {v4, p3, v0}, LX/CEl;->c(Landroid/content/Context;Lcom/facebook/greetingcards/verve/model/VMView;)Landroid/widget/Button;

    move-result-object v4

    goto :goto_4

    .line 1865367
    :pswitch_4
    new-instance v4, LX/CGJ;

    invoke-direct {v4, p3, p2}, LX/CGJ;-><init>(Landroid/content/Context;LX/CFp;)V

    goto :goto_4

    .line 1865368
    :pswitch_5
    new-instance v4, LX/CGH;

    invoke-direct {v4, p3, p2}, LX/CGH;-><init>(Landroid/content/Context;LX/CFp;)V

    goto :goto_4

    .line 1865369
    :pswitch_6
    sget-object v5, LX/CGP;->a:[I

    invoke-virtual {v3}, LX/CFl;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_3

    .line 1865370
    sget-object v5, LX/CGQ;->RECT:LX/CGQ;

    goto/16 :goto_1

    .line 1865371
    :pswitch_7
    sget-object v5, LX/CGQ;->MEDIA_WITH_IMAGE:LX/CGQ;

    goto/16 :goto_1

    .line 1865372
    :pswitch_8
    sget-object v5, LX/CGQ;->MEDIA_WITH_VIDEO:LX/CGQ;

    goto/16 :goto_1

    .line 1865373
    :pswitch_9
    sget-object v5, LX/CGQ;->LABEL:LX/CGQ;

    goto/16 :goto_1

    .line 1865374
    :pswitch_a
    sget-object v5, LX/CGQ;->BUTTON:LX/CGQ;

    goto/16 :goto_1

    .line 1865375
    :pswitch_b
    sget-object v5, LX/CGQ;->SEQUENCE:LX/CGQ;

    goto/16 :goto_1

    .line 1865376
    :pswitch_c
    sget-object v5, LX/CGQ;->GROUP:LX/CGQ;

    goto/16 :goto_1

    :cond_6
    const/4 v8, 0x0

    goto/16 :goto_2

    .line 1865377
    :pswitch_d
    iget-object v4, p2, LX/CFp;->b:LX/CEl;

    move-object v4, v4

    .line 1865378
    invoke-interface {v4, p3, v0}, LX/CEl;->a(Landroid/content/Context;Lcom/facebook/greetingcards/verve/model/VMView;)Landroid/widget/ImageView;

    move-result-object v4

    goto :goto_5

    .line 1865379
    :pswitch_e
    iget-object v4, p2, LX/CFp;->b:LX/CEl;

    move-object v4, v4

    .line 1865380
    invoke-interface {v4, p3, v0}, LX/CEl;->b(Landroid/content/Context;Lcom/facebook/greetingcards/verve/model/VMView;)Landroid/view/View;

    move-result-object v4

    goto :goto_5

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_d
        :pswitch_e
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x1
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public static a(LX/CFp;Landroid/view/ViewGroup;)V
    .locals 7

    .prologue
    .line 1865381
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1865382
    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 1865383
    const v0, 0x7f0d01d3

    invoke-virtual {v2, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1865384
    iget-object v3, p0, LX/CFp;->e:LX/CGR;

    move-object v3, v3

    .line 1865385
    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1865386
    iget-object v4, v3, LX/CGR;->c:LX/CGO;

    .line 1865387
    iget-object v5, v4, LX/CGO;->a:Landroid/util/SparseArray;

    invoke-virtual {v5, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/ArrayList;

    .line 1865388
    if-nez v5, :cond_0

    .line 1865389
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1865390
    iget-object v6, v4, LX/CGO;->a:Landroid/util/SparseArray;

    invoke-virtual {v6, v0, v5}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1865391
    iget-object v6, v4, LX/CGO;->b:Landroid/util/SparseIntArray;

    invoke-virtual {v6, v0}, Landroid/util/SparseIntArray;->indexOfKey(I)I

    move-result v6

    if-gez v6, :cond_0

    .line 1865392
    iget-object v6, v4, LX/CGO;->b:Landroid/util/SparseIntArray;

    const/4 v3, 0x5

    invoke-virtual {v6, v0, v3}, Landroid/util/SparseIntArray;->put(II)V

    .line 1865393
    :cond_0
    move-object v5, v5

    .line 1865394
    iget-object v6, v4, LX/CGO;->b:Landroid/util/SparseIntArray;

    invoke-virtual {v6, v0}, Landroid/util/SparseIntArray;->get(I)I

    move-result v6

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-gt v6, v3, :cond_2

    .line 1865395
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1865396
    :cond_1
    invoke-virtual {p1}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 1865397
    return-void

    .line 1865398
    :cond_2
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public static a(Lcom/facebook/greetingcards/verve/model/VMView;ILX/CFp;Landroid/view/View;)V
    .locals 12

    .prologue
    const/4 v6, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1865399
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1865400
    sget-object v0, LX/CGM;->a:[I

    invoke-virtual {p0}, Lcom/facebook/greetingcards/verve/model/VMView;->o()LX/CFo;

    move-result-object v1

    invoke-virtual {v1}, LX/CFo;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1865401
    sget-object v0, LX/CGN;->a:Ljava/lang/Class;

    const-string v1, "Unrecognized verve view type: %s. Display default view."

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/facebook/greetingcards/verve/model/VMView;->o()LX/CFo;

    move-result-object v4

    aput-object v4, v3, v2

    invoke-static {v0, v1, v3}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1865402
    :cond_0
    :goto_0
    invoke-static {p3, p0}, LX/CGS;->a(Landroid/view/View;Lcom/facebook/greetingcards/verve/model/VMView;)V

    .line 1865403
    const v0, 0x7f0d01d4

    invoke-virtual {p3, v0, p0}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 1865404
    return-void

    .line 1865405
    :pswitch_0
    const v0, 0x7f0d01d5

    invoke-virtual {p3, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CFl;

    .line 1865406
    iget-object v1, p2, LX/CFp;->a:Lcom/facebook/greetingcards/verve/model/VMDeck;

    move-object v1, v1

    .line 1865407
    iget-object v1, v1, Lcom/facebook/greetingcards/verve/model/VMDeck;->slides:LX/0Px;

    invoke-virtual {v1, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/greetingcards/verve/model/VMSlide;

    invoke-static {p0, v1, p2, p3, v0}, LX/CGN;->a(Lcom/facebook/greetingcards/verve/model/VMView;Lcom/facebook/greetingcards/verve/model/VMSlide;LX/CFp;Landroid/view/View;LX/CFl;)V

    goto :goto_0

    .line 1865408
    :pswitch_1
    invoke-virtual {p0}, Lcom/facebook/greetingcards/verve/model/VMView;->d()Lcom/facebook/greetingcards/verve/model/VMColor;

    move-result-object v0

    invoke-static {v0}, LX/CFw;->a(Lcom/facebook/greetingcards/verve/model/VMColor;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p3, v0}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :pswitch_2
    move-object v0, p3

    .line 1865409
    check-cast v0, Landroid/widget/TextView;

    .line 1865410
    iget-object v1, p0, Lcom/facebook/greetingcards/verve/model/VMView;->text:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1865411
    invoke-virtual {p0}, Lcom/facebook/greetingcards/verve/model/VMView;->d()Lcom/facebook/greetingcards/verve/model/VMColor;

    move-result-object v1

    invoke-static {v1}, LX/CFw;->b(Lcom/facebook/greetingcards/verve/model/VMColor;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1865412
    invoke-virtual {p0}, Lcom/facebook/greetingcards/verve/model/VMView;->c()F

    move-result v1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1865413
    iget-object v1, p0, Lcom/facebook/greetingcards/verve/model/VMView;->shadowColor:Lcom/facebook/greetingcards/verve/model/VMColor;

    if-eqz v1, :cond_1

    .line 1865414
    invoke-virtual {p0}, Lcom/facebook/greetingcards/verve/model/VMView;->e()I

    move-result v1

    int-to-float v4, v1

    invoke-virtual {p0}, Lcom/facebook/greetingcards/verve/model/VMView;->f()LX/0Px;

    move-result-object v1

    const/4 v5, 0x0

    invoke-virtual {v1, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v5

    invoke-virtual {p0}, Lcom/facebook/greetingcards/verve/model/VMView;->f()LX/0Px;

    move-result-object v1

    const/4 p1, 0x1

    invoke-virtual {v1, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    iget-object p1, p0, Lcom/facebook/greetingcards/verve/model/VMView;->shadowColor:Lcom/facebook/greetingcards/verve/model/VMColor;

    invoke-static {p1}, LX/CFw;->b(Lcom/facebook/greetingcards/verve/model/VMColor;)I

    move-result p1

    invoke-virtual {v0, v4, v5, v1, p1}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    .line 1865415
    :cond_1
    const-string v1, "center"

    iget-object v4, p0, Lcom/facebook/greetingcards/verve/model/VMView;->alignment:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1865416
    const/4 v1, 0x1

    .line 1865417
    :goto_1
    const-string v4, "bottom"

    iget-object v5, p0, Lcom/facebook/greetingcards/verve/model/VMView;->vAlignment:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 1865418
    or-int/lit8 v1, v1, 0x50

    .line 1865419
    :goto_2
    move v1, v1

    .line 1865420
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 1865421
    const-string v1, "System-Italic"

    iget-object v4, p0, Lcom/facebook/greetingcards/verve/model/VMView;->font:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1865422
    const/4 v1, 0x2

    invoke-virtual {v0, v6, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 1865423
    :cond_2
    :goto_3
    invoke-virtual {p0}, Lcom/facebook/greetingcards/verve/model/VMView;->n()Ljava/lang/String;

    move-result-object v4

    const/4 v1, -0x1

    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    :cond_3
    :goto_4
    packed-switch v1, :pswitch_data_1

    .line 1865424
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 1865425
    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 1865426
    :goto_5
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setHorizontallyScrolling(Z)V

    goto/16 :goto_0

    .line 1865427
    :cond_4
    const-string v1, "System-Bold"

    iget-object v4, p0, Lcom/facebook/greetingcards/verve/model/VMView;->font:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1865428
    invoke-virtual {v0, v6, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    goto :goto_3

    .line 1865429
    :cond_5
    const-string v1, "System-Light"

    iget-object v4, p0, Lcom/facebook/greetingcards/verve/model/VMView;->font:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x10

    if-lt v1, v4, :cond_2

    .line 1865430
    const-string v1, "sans-serif-light"

    invoke-static {v1, v2}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    goto :goto_3

    .line 1865431
    :sswitch_0
    const-string v5, "clip"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    move v1, v2

    goto :goto_4

    :sswitch_1
    const-string v5, "truncate"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    move v1, v3

    goto :goto_4

    .line 1865432
    :pswitch_3
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 1865433
    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    goto :goto_5

    .line 1865434
    :pswitch_4
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 1865435
    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    goto :goto_5

    .line 1865436
    :pswitch_5
    iget-object v0, p2, LX/CFp;->c:LX/CEO;

    move-object v1, v0

    .line 1865437
    iget-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMView;->actionsMap:LX/0P1;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMView;->actionsMap:LX/0P1;

    const-string v2, "button"

    invoke-virtual {v0, v2}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 1865438
    iget-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMView;->actionsMap:LX/0P1;

    const-string v2, "button"

    invoke-virtual {v0, v2}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/greetingcards/verve/model/VMAction;

    .line 1865439
    new-instance v2, LX/CGL;

    invoke-direct {v2, v1, v0, p0}, LX/CGL;-><init>(LX/CEO;Lcom/facebook/greetingcards/verve/model/VMAction;Lcom/facebook/greetingcards/verve/model/VMView;)V

    invoke-virtual {p3, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    :pswitch_6
    move-object v0, p3

    .line 1865440
    check-cast v0, LX/CGJ;

    .line 1865441
    iput-object p0, v0, LX/CGJ;->d:Lcom/facebook/greetingcards/verve/model/VMView;

    .line 1865442
    const/4 v7, 0x0

    iput v7, v0, LX/CGJ;->g:I

    .line 1865443
    iget-object v7, v0, LX/CGJ;->b:LX/0YU;

    invoke-virtual {v7}, LX/0YU;->b()V

    .line 1865444
    new-instance v7, LX/CG5;

    invoke-direct {v7}, LX/CG5;-><init>()V

    iput-object v7, v0, LX/CGJ;->f:LX/CG5;

    .line 1865445
    iget-object v7, v0, LX/CGJ;->d:Lcom/facebook/greetingcards/verve/model/VMView;

    invoke-virtual {v7}, Lcom/facebook/greetingcards/verve/model/VMView;->g()F

    move-result v7

    const/high16 v8, 0x447a0000    # 1000.0f

    mul-float/2addr v7, v8

    float-to-int v7, v7

    iput v7, v0, LX/CGJ;->e:I

    .line 1865446
    iget-object v7, p0, Lcom/facebook/greetingcards/verve/model/VMView;->subviews:LX/0Px;

    iget-object v8, v0, LX/CGJ;->a:LX/CFp;

    invoke-virtual {v0}, LX/CGJ;->getContext()Landroid/content/Context;

    move-result-object v9

    iget-object v10, v0, LX/CGJ;->b:LX/0YU;

    invoke-static {v7, p1, v8, v9, v10}, LX/CGN;->a(LX/0Px;ILX/CFp;Landroid/content/Context;LX/0YU;)V

    .line 1865447
    iget v7, v0, LX/CGJ;->g:I

    invoke-static {v0, v7}, LX/CGJ;->a(LX/CGJ;I)Landroid/view/View;

    move-result-object v7

    .line 1865448
    invoke-virtual {v0, v7}, LX/CGJ;->addView(Landroid/view/View;)V

    .line 1865449
    iget-object v7, v0, LX/CGJ;->c:Landroid/os/Handler;

    iget-object v8, v0, LX/CGJ;->h:Ljava/lang/Runnable;

    const-wide/16 v9, 0x3e8

    const v11, -0xda9e277

    invoke-static {v7, v8, v9, v10, v11}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 1865450
    goto/16 :goto_0

    :pswitch_7
    move-object v0, p3

    .line 1865451
    check-cast v0, LX/CGH;

    .line 1865452
    invoke-virtual {v0, p0, p1}, LX/CGH;->a(Lcom/facebook/greetingcards/verve/model/VMView;I)V

    goto/16 :goto_0

    .line 1865453
    :cond_6
    const-string v1, "right"

    iget-object v4, p0, Lcom/facebook/greetingcards/verve/model/VMView;->alignment:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1865454
    const/4 v1, 0x5

    goto/16 :goto_1

    .line 1865455
    :cond_7
    const/4 v1, 0x3

    goto/16 :goto_1

    .line 1865456
    :cond_8
    const-string v4, "center"

    iget-object v5, p0, Lcom/facebook/greetingcards/verve/model/VMView;->vAlignment:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 1865457
    or-int/lit8 v1, v1, 0x10

    goto/16 :goto_2

    .line 1865458
    :cond_9
    or-int/lit8 v1, v1, 0x30

    goto/16 :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch

    :sswitch_data_0
    .sparse-switch
        0x2ea350 -> :sswitch_0
        0x6e724d66 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private static a(Lcom/facebook/greetingcards/verve/model/VMView;Lcom/facebook/greetingcards/verve/model/VMSlide;LX/CFp;Landroid/view/View;LX/CFl;)V
    .locals 6

    .prologue
    .line 1865459
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1865460
    sget-object v0, LX/CGM;->b:[I

    invoke-virtual {p4}, LX/CFl;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1865461
    :cond_0
    :goto_0
    return-void

    .line 1865462
    :pswitch_0
    instance-of v0, p3, Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    move-object v5, p3

    .line 1865463
    check-cast v5, Landroid/widget/ImageView;

    .line 1865464
    const/4 v4, 0x0

    .line 1865465
    iget-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMView;->placeholderColor:Lcom/facebook/greetingcards/verve/model/VMColor;

    if-eqz v0, :cond_1

    .line 1865466
    iget-object v0, p0, Lcom/facebook/greetingcards/verve/model/VMView;->placeholderColor:Lcom/facebook/greetingcards/verve/model/VMColor;

    invoke-static {v0}, LX/CFw;->a(Lcom/facebook/greetingcards/verve/model/VMColor;)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    .line 1865467
    :cond_1
    iget-object v0, p2, LX/CFp;->b:LX/CEl;

    move-object v0, v0

    .line 1865468
    iget-object v1, p2, LX/CFp;->a:Lcom/facebook/greetingcards/verve/model/VMDeck;

    move-object v1, v1

    .line 1865469
    iget-object v3, v1, Lcom/facebook/greetingcards/verve/model/VMDeck;->resources:LX/0P1;

    move-object v1, p0

    move-object v2, p1

    invoke-interface/range {v0 .. v5}, LX/CEl;->a(Lcom/facebook/greetingcards/verve/model/VMView;Lcom/facebook/greetingcards/verve/model/VMSlide;LX/0P1;Landroid/graphics/drawable/Drawable;Landroid/widget/ImageView;)V

    .line 1865470
    const-string v0, "fit"

    iget-object v1, p0, Lcom/facebook/greetingcards/verve/model/VMView;->resize:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1865471
    sget-object v0, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v5, v0}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    goto :goto_0

    .line 1865472
    :cond_2
    sget-object v0, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v5, v0}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    goto :goto_0

    .line 1865473
    :pswitch_1
    instance-of v0, p3, LX/CEe;

    if-eqz v0, :cond_0

    .line 1865474
    iget-object v0, p2, LX/CFp;->b:LX/CEl;

    move-object v0, v0

    .line 1865475
    check-cast p3, LX/CEe;

    invoke-interface {v0, p0, p3}, LX/CEl;->a(Lcom/facebook/greetingcards/verve/model/VMView;LX/CEe;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
