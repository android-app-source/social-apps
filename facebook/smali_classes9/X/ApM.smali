.class public LX/ApM;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:LX/Apq;


# direct methods
.method public constructor <init>(LX/Apq;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1715905
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1715906
    iput-object p1, p0, LX/ApM;->a:LX/Apq;

    .line 1715907
    return-void
.end method

.method public static a(LX/0QB;)LX/ApM;
    .locals 4

    .prologue
    .line 1715894
    const-class v1, LX/ApM;

    monitor-enter v1

    .line 1715895
    :try_start_0
    sget-object v0, LX/ApM;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1715896
    sput-object v2, LX/ApM;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1715897
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1715898
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1715899
    new-instance p0, LX/ApM;

    invoke-static {v0}, LX/Apq;->a(LX/0QB;)LX/Apq;

    move-result-object v3

    check-cast v3, LX/Apq;

    invoke-direct {p0, v3}, LX/ApM;-><init>(LX/Apq;)V

    .line 1715900
    move-object v0, p0

    .line 1715901
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1715902
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/ApM;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1715903
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1715904
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;ILandroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;LX/1X1;)LX/1Dg;
    .locals 3
    .param p2    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation

        .annotation build Lcom/facebook/fig/components/attachment/annotations/FigAttachmentFooterMediaType;
        .end annotation
    .end param
    .param p3    # Landroid/net/Uri;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p5    # LX/1X1;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation

        .annotation runtime Ljava/lang/Deprecated;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "I",
            "Landroid/net/Uri;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "LX/1X1",
            "<*>;)",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    .line 1715891
    packed-switch p2, :pswitch_data_0

    .line 1715892
    :pswitch_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported media type = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1715893
    :pswitch_1
    iget-object v0, p0, LX/ApM;->a:LX/Apq;

    invoke-virtual {v0, p1}, LX/Apq;->c(LX/1De;)LX/App;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/App;->h(I)LX/App;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/App;->a(Landroid/net/Uri;)LX/App;

    move-result-object v0

    invoke-virtual {v0, p4}, LX/App;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/App;

    move-result-object v0

    invoke-virtual {v0, p5}, LX/App;->a(LX/1X1;)LX/App;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->b()LX/1Dg;

    move-result-object v0

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
