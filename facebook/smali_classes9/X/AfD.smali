.class public LX/AfD;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:LX/0aG;

.field public final c:Ljava/lang/String;

.field public final d:LX/0kL;

.field public final e:Ljava/util/concurrent/Executor;

.field private final f:Ljava/lang/Boolean;

.field public final g:LX/1b4;

.field public h:LX/5OM;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:LX/3Af;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:LX/Aev;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:LX/AfJ;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1698734
    const-class v0, LX/AfD;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/AfD;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0aG;Ljava/lang/String;LX/0kL;Ljava/util/concurrent/Executor;Ljava/lang/Boolean;LX/1b4;)V
    .locals 0
    .param p2    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUserId;
        .end annotation
    .end param
    .param p4    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p5    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1698735
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1698736
    iput-object p1, p0, LX/AfD;->b:LX/0aG;

    .line 1698737
    iput-object p2, p0, LX/AfD;->c:Ljava/lang/String;

    .line 1698738
    iput-object p3, p0, LX/AfD;->d:LX/0kL;

    .line 1698739
    iput-object p4, p0, LX/AfD;->e:Ljava/util/concurrent/Executor;

    .line 1698740
    iput-object p5, p0, LX/AfD;->f:Ljava/lang/Boolean;

    .line 1698741
    iput-object p6, p0, LX/AfD;->g:LX/1b4;

    .line 1698742
    return-void
.end method

.method public static c(LX/AfD;)Z
    .locals 1

    .prologue
    .line 1698743
    iget-object v0, p0, LX/AfD;->f:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/Aeu;Landroid/view/View;LX/Aev;LX/AfJ;)V
    .locals 8

    .prologue
    .line 1698744
    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1698745
    iput-object p3, p0, LX/AfD;->j:LX/Aev;

    .line 1698746
    iput-object p4, p0, LX/AfD;->k:LX/AfJ;

    .line 1698747
    iget-object v1, p0, LX/AfD;->g:LX/1b4;

    invoke-virtual {v1}, LX/1b4;->D()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1698748
    const/4 p4, 0x1

    const/4 p3, 0x0

    .line 1698749
    new-instance v2, LX/34b;

    invoke-direct {v2, v0}, LX/34b;-><init>(Landroid/content/Context;)V

    .line 1698750
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v3, 0x7f03059e

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4, p3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 1698751
    iget-object v1, p1, LX/AeP;->a:LX/AcC;

    iget-object v1, v1, LX/AcC;->b:Ljava/lang/String;

    invoke-static {v1}, Lcom/facebook/user/model/UserKey;->b(Ljava/lang/String;)Lcom/facebook/user/model/UserKey;

    move-result-object v4

    .line 1698752
    const v1, 0x7f0d0f67

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/user/tiles/UserTileView;

    .line 1698753
    invoke-static {v4}, LX/8t9;->a(Lcom/facebook/user/model/UserKey;)LX/8t9;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/facebook/user/tiles/UserTileView;->setParams(LX/8t9;)V

    .line 1698754
    const v1, 0x7f0d058b

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/text/BetterTextView;

    .line 1698755
    iget-object v4, p1, LX/AeP;->a:LX/AcC;

    iget-object v4, v4, LX/AcC;->a:Ljava/lang/String;

    .line 1698756
    invoke-static {v4}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object p2

    invoke-virtual {v1, p2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1698757
    invoke-virtual {v1, v4}, Lcom/facebook/widget/text/BetterTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1698758
    const/high16 v1, -0x40800000    # -1.0f

    invoke-virtual {v2, v3, v1}, LX/34b;->a(Landroid/view/View;F)V

    .line 1698759
    invoke-static {p0}, LX/AfD;->c(LX/AfD;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1698760
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f080c0f

    new-array v4, p4, [Ljava/lang/Object;

    iget-object p2, p1, LX/AeP;->a:LX/AcC;

    iget-object p2, p2, LX/AcC;->a:Ljava/lang/String;

    aput-object p2, v4, p3

    invoke-virtual {v1, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/34c;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v1

    .line 1698761
    const v3, 0x7f020888

    invoke-virtual {v1, v3}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    .line 1698762
    iget-object v3, p0, LX/AfD;->j:LX/Aev;

    if-eqz v3, :cond_0

    .line 1698763
    new-instance v3, LX/Af6;

    invoke-direct {v3, p0, p1}, LX/Af6;-><init>(LX/AfD;LX/Aeu;)V

    invoke-virtual {v1, v3}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1698764
    :cond_0
    const v1, 0x7f080c0e

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/34c;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v1

    .line 1698765
    const v3, 0x7f020a0b

    invoke-virtual {v1, v3}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    .line 1698766
    iget-object v3, p0, LX/AfD;->j:LX/Aev;

    if-eqz v3, :cond_1

    .line 1698767
    new-instance v3, LX/Af7;

    invoke-direct {v3, p0, p1}, LX/Af7;-><init>(LX/AfD;LX/Aeu;)V

    invoke-virtual {v1, v3}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1698768
    :cond_1
    iget-object v1, p0, LX/AfD;->k:LX/AfJ;

    if-eqz v1, :cond_3

    .line 1698769
    iget-object v1, p0, LX/AfD;->k:LX/AfJ;

    const/4 v3, 0x1

    .line 1698770
    invoke-static {v1}, LX/AfJ;->j(LX/AfJ;)Z

    move-result v4

    if-nez v4, :cond_8

    .line 1698771
    const/4 v3, 0x0

    .line 1698772
    :cond_2
    :goto_0
    move v1, v3

    .line 1698773
    if-ne v1, p4, :cond_7

    .line 1698774
    const v1, 0x7f080c10

    invoke-virtual {v2, v1}, LX/34c;->e(I)LX/3Ai;

    move-result-object v1

    .line 1698775
    const v3, 0x7f020995

    invoke-virtual {v1, v3}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    .line 1698776
    new-instance v3, LX/Af8;

    invoke-direct {v3, p0, p1}, LX/Af8;-><init>(LX/AfD;LX/Aeu;)V

    invoke-virtual {v1, v3}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1698777
    :cond_3
    :goto_1
    new-instance v1, LX/3Af;

    invoke-direct {v1, v0}, LX/3Af;-><init>(Landroid/content/Context;)V

    .line 1698778
    invoke-virtual {v1, v2}, LX/3Af;->a(LX/1OM;)V

    .line 1698779
    invoke-virtual {v1}, LX/3Af;->show()V

    .line 1698780
    iput-object v1, p0, LX/AfD;->i:LX/3Af;

    .line 1698781
    :goto_2
    return-void

    .line 1698782
    :cond_4
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1698783
    new-instance v2, LX/6WS;

    invoke-direct {v2, v0, v6}, LX/6WS;-><init>(Landroid/content/Context;I)V

    iput-object v2, p0, LX/AfD;->h:LX/5OM;

    .line 1698784
    iget-object v2, p0, LX/AfD;->h:LX/5OM;

    invoke-virtual {v2}, LX/5OM;->c()LX/5OG;

    move-result-object v2

    .line 1698785
    const v3, 0x7f080c0e

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/5OG;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v3

    .line 1698786
    iget-object v4, p0, LX/AfD;->j:LX/Aev;

    if-eqz v4, :cond_5

    .line 1698787
    new-instance v4, LX/AfA;

    invoke-direct {v4, p0, p1}, LX/AfA;-><init>(LX/AfD;LX/Aeu;)V

    invoke-virtual {v3, v4}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1698788
    :cond_5
    invoke-static {p0}, LX/AfD;->c(LX/AfD;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 1698789
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080c0f

    new-array v6, v6, [Ljava/lang/Object;

    iget-object v7, p1, LX/AeP;->a:LX/AcC;

    iget-object v7, v7, LX/AcC;->a:Ljava/lang/String;

    aput-object v7, v6, v5

    invoke-virtual {v3, v4, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/5OG;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v3

    .line 1698790
    iget-object v4, p0, LX/AfD;->j:LX/Aev;

    if-eqz v4, :cond_6

    .line 1698791
    new-instance v4, LX/AfB;

    invoke-direct {v4, p0, p1}, LX/AfB;-><init>(LX/AfD;LX/Aeu;)V

    invoke-virtual {v3, v4}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1698792
    :cond_6
    const v3, 0x7f080017

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/5OG;->a(Ljava/lang/CharSequence;)LX/3Ai;

    .line 1698793
    iget-object v2, p0, LX/AfD;->h:LX/5OM;

    invoke-virtual {p2}, Landroid/view/View;->getWidth()I

    move-result v4

    invoke-virtual {p2}, Landroid/view/View;->getWidth()I

    move-result v6

    invoke-virtual {p2}, Landroid/view/View;->getHeight()I

    move-result v7

    move-object v3, p2

    invoke-virtual/range {v2 .. v7}, LX/0ht;->a(Landroid/view/View;IIII)V

    .line 1698794
    iget-object v2, p0, LX/AfD;->h:LX/5OM;

    invoke-virtual {v2}, LX/0ht;->d()V

    .line 1698795
    goto :goto_2

    .line 1698796
    :cond_7
    const/4 v3, -0x1

    if-ne v1, v3, :cond_3

    .line 1698797
    const v1, 0x7f080c11

    invoke-virtual {v2, v1}, LX/34c;->e(I)LX/3Ai;

    move-result-object v1

    .line 1698798
    const v3, 0x7f020995

    invoke-virtual {v1, v3}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    .line 1698799
    new-instance v3, LX/Af9;

    invoke-direct {v3, p0}, LX/Af9;-><init>(LX/AfD;)V

    invoke-virtual {v1, v3}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1698800
    goto/16 :goto_1

    .line 1698801
    :cond_8
    iget-object v4, v1, LX/AfJ;->i:LX/Aeu;

    if-eqz v4, :cond_2

    .line 1698802
    iget-object v4, v1, LX/AfJ;->i:LX/Aeu;

    iget-object v4, v4, LX/Aeu;->f:Ljava/lang/String;

    iget-object p2, p1, LX/Aeu;->f:Ljava/lang/String;

    invoke-static {v4, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v3, -0x1

    goto/16 :goto_0
.end method
