.class public LX/Bsr;
.super Lcom/facebook/fbui/widget/layout/ImageBlockLayout;
.source ""

# interfaces
.implements LX/3V1;


# instance fields
.field public j:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private k:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private l:Landroid/view/View;

.field private m:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1828363
    const/4 v0, 0x0

    const v1, 0x7f0308aa

    invoke-direct {p0, p1, v0, v1}, LX/Bsr;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1828364
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1828360
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1828361
    const v0, 0x7f0308aa

    invoke-direct {p0, v0}, LX/Bsr;->a(I)V

    .line 1828362
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1828365
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1828366
    invoke-direct {p0, p3}, LX/Bsr;->a(I)V

    .line 1828367
    return-void
.end method

.method private a(I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1828340
    const-class v0, LX/Bsr;

    invoke-static {v0, p0}, LX/Bsr;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1828341
    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 1828342
    invoke-virtual {p0, v1}, LX/Bsr;->setClipChildren(Z)V

    .line 1828343
    invoke-virtual {p0, v1}, LX/Bsr;->setClipToPadding(Z)V

    .line 1828344
    const v0, 0x7f0d160d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, LX/Bsr;->k:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1828345
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    .line 1828346
    iget-object v0, p0, LX/Bsr;->k:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setImportantForAccessibility(I)V

    .line 1828347
    :cond_0
    const v0, 0x7f0d1616

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/Bsr;->l:Landroid/view/View;

    .line 1828348
    const v0, 0x7f0d1615

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/Bsr;->m:Landroid/view/View;

    .line 1828349
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, LX/Bsr;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, LX/Bsr;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v0

    check-cast v0, LX/0wM;

    iput-object v0, p0, LX/Bsr;->j:LX/0wM;

    return-void
.end method


# virtual methods
.method public setProfileImageOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1    # Landroid/view/View$OnClickListener;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1828358
    iget-object v0, p0, LX/Bsr;->k:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1828359
    return-void
.end method

.method public setSingleLineTitle(Z)V
    .locals 2

    .prologue
    .line 1828354
    iget-object v0, p0, LX/Bsr;->l:Landroid/view/View;

    check-cast v0, Landroid/widget/TextView;

    .line 1828355
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 1828356
    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 1828357
    return-void
.end method

.method public setSubtitleIcon(I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1828351
    iget-object v0, p0, LX/Bsr;->m:Landroid/view/View;

    check-cast v0, Lcom/facebook/widget/text/TextViewWithFallback;

    .line 1828352
    iget-object v1, p0, LX/Bsr;->j:LX/0wM;

    const v2, -0x6e685d

    invoke-virtual {v1, p1, v2}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v3, v3, v1, v3}, Lcom/facebook/widget/text/TextViewWithFallback;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 1828353
    return-void
.end method

.method public setSubtitleWithLayout(Landroid/text/Layout;)V
    .locals 2

    .prologue
    .line 1828350
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Not a text layout header t6009510"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
