.class public LX/Arh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0iK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "Lcom/facebook/friendsharing/inspiration/model/CameraStateSpec$ProvidesCameraState;",
        ":",
        "LX/0io;",
        ":",
        "LX/0iq;",
        ":",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingDataSpec$ProvidesInspirationLoggingData;",
        ":",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationStateSpec$ProvidesInspirationState;",
        ":",
        "LX/0is;",
        "DerivedData:",
        "Ljava/lang/Object;",
        "Mutation::",
        "Lcom/facebook/ipc/composer/dataaccessor/ComposerCanSave;",
        ":",
        "Lcom/facebook/composer/attachments/ComposerAttachment$SetsAttachments",
        "<TMutation;>;:",
        "Lcom/facebook/composer/privacy/model/ComposerPrivacyData$SetsPrivacyData",
        "<TMutation;>;:",
        "Lcom/facebook/friendsharing/inspiration/model/CameraStateSpec$SetsCameraState",
        "<TMutation;>;:",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingDataSpec$SetsInspirationLoggingData",
        "<TMutation;>;:",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBoundsSpec$SetsInspirationPreviewBounds",
        "<TMutation;>;Services::",
        "LX/0il",
        "<TModelData;>;:",
        "LX/0ik",
        "<TDerivedData;>;:",
        "LX/0im",
        "<TMutation;>;>",
        "Ljava/lang/Object;",
        "LX/0iK",
        "<TModelData;TDerivedData;>;",
        "Lcom/facebook/friendsharing/inspiration/capture/InspirationCameraEffectsApplier;",
        "Lcom/facebook/friendsharing/inspiration/capture/InspirationCaptureHelper$Delegate;"
    }
.end annotation


# static fields
.field public static final d:LX/0jK;


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0kL;",
            ">;"
        }
    .end annotation
.end field

.field public volatile b:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/192;",
            ">;"
        }
    .end annotation
.end field

.field public volatile c:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0Yi;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;

.field public final f:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TServices;>;"
        }
    .end annotation
.end field

.field private final g:Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;

.field private final h:LX/Aqx;

.field public final i:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field public final j:LX/Arq;

.field public final k:LX/195;

.field public final l:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final m:Lcom/facebook/friendsharing/inspiration/capture/cache/InspirationPhotoCaptureCache;

.field public final n:LX/ArL;

.field public final o:LX/ArT;

.field public final p:LX/74n;

.field public final q:LX/0SG;

.field private final r:Ljava/lang/Runnable;

.field private final s:LX/Are;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/Arh",
            "<TModelData;TDerivedData;TMutation;TServices;>.TapCamera",
            "Listener;"
        }
    .end annotation
.end field

.field private final t:LX/Arg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/Arh",
            "<TModelData;TDerivedData;TMutation;TServices;>.ZoomScale",
            "Listener;"
        }
    .end annotation
.end field

.field public final u:LX/Art;

.field private v:Z

.field public w:Z

.field private x:Z

.field private y:Z

.field public z:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1719487
    const-class v0, LX/Arh;

    invoke-static {v0}, LX/0jK;->a(Ljava/lang/Class;)LX/0jK;

    move-result-object v0

    sput-object v0, LX/Arh;->d:LX/0jK;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;LX/0il;Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;LX/Aqx;LX/ArL;LX/ArT;LX/Arr;LX/Aru;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/193;Lcom/facebook/prefs/shared/FbSharedPreferences;Lcom/facebook/friendsharing/inspiration/capture/cache/InspirationPhotoCaptureCache;LX/74n;LX/0SG;)V
    .locals 3
    .param p1    # Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/0il;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/Aqx;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/ArL;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/ArT;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;",
            "TServices;",
            "Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;",
            "Lcom/facebook/friendsharing/inspiration/capture/InspirationCameraPreviewController$Delegate;",
            "LX/ArL;",
            "LX/ArT;",
            "LX/Arr;",
            "LX/Aru;",
            "Lcom/facebook/quicklog/QuickPerformanceLogger;",
            "LX/193;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "Lcom/facebook/friendsharing/inspiration/capture/cache/InspirationPhotoCaptureCache;",
            "LX/74n;",
            "LX/0SG;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1719488
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1719489
    iput-object p1, p0, LX/Arh;->e:Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;

    .line 1719490
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v1, p0, LX/Arh;->f:Ljava/lang/ref/WeakReference;

    .line 1719491
    iput-object p3, p0, LX/Arh;->g:Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;

    .line 1719492
    iput-object p4, p0, LX/Arh;->h:LX/Aqx;

    .line 1719493
    iget-object v2, p0, LX/Arh;->g:Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;

    invoke-interface {p2}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->j()Lcom/facebook/friendsharing/inspiration/model/CameraState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/model/CameraState;->isCameraFrontFacing()Z

    move-result v1

    invoke-virtual {p7, p0, v2, v1}, LX/Arr;->a(LX/Arh;Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;Z)LX/Arq;

    move-result-object v1

    iput-object v1, p0, LX/Arh;->j:LX/Arq;

    .line 1719494
    iput-object p5, p0, LX/Arh;->n:LX/ArL;

    .line 1719495
    iput-object p6, p0, LX/Arh;->o:LX/ArT;

    .line 1719496
    iput-object p9, p0, LX/Arh;->i:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 1719497
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const-string v2, "inspiration_camera_preview"

    invoke-virtual {p10, v1, v2}, LX/193;->a(Ljava/lang/Boolean;Ljava/lang/String;)LX/195;

    move-result-object v1

    iput-object v1, p0, LX/Arh;->k:LX/195;

    .line 1719498
    iget-object v1, p0, LX/Arh;->k:LX/195;

    new-instance v2, LX/2rW;

    invoke-direct {v2, p2}, LX/2rW;-><init>(LX/0il;)V

    invoke-virtual {v1, v2}, LX/195;->a(LX/1KG;)V

    .line 1719499
    invoke-direct {p0}, LX/Arh;->k()Ljava/lang/Runnable;

    move-result-object v1

    iput-object v1, p0, LX/Arh;->r:Ljava/lang/Runnable;

    .line 1719500
    iput-object p11, p0, LX/Arh;->l:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1719501
    iput-object p12, p0, LX/Arh;->m:Lcom/facebook/friendsharing/inspiration/capture/cache/InspirationPhotoCaptureCache;

    .line 1719502
    move-object/from16 v0, p13

    iput-object v0, p0, LX/Arh;->p:LX/74n;

    .line 1719503
    move-object/from16 v0, p14

    iput-object v0, p0, LX/Arh;->q:LX/0SG;

    .line 1719504
    new-instance v1, LX/Are;

    invoke-direct {v1, p0}, LX/Are;-><init>(LX/Arh;)V

    iput-object v1, p0, LX/Arh;->s:LX/Are;

    .line 1719505
    new-instance v1, LX/Arg;

    invoke-direct {v1, p0}, LX/Arg;-><init>(LX/Arh;)V

    iput-object v1, p0, LX/Arh;->t:LX/Arg;

    .line 1719506
    iget-object v1, p0, LX/Arh;->e:Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;

    invoke-virtual {p8, v1}, LX/Aru;->a(Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;)LX/Art;

    move-result-object v1

    iput-object v1, p0, LX/Arh;->u:LX/Art;

    .line 1719507
    return-void
.end method

.method private a(LX/0jL;)LX/0jL;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TMutation;)TMutation;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 1719508
    iget-boolean v0, p0, LX/Arh;->x:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/Arh;->y:Z

    if-eqz v0, :cond_1

    .line 1719509
    :cond_0
    :goto_0
    return-object p1

    .line 1719510
    :cond_1
    iget-object v0, p0, LX/Arh;->g:Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;->setVisibility(I)V

    .line 1719511
    iput-boolean v2, p0, LX/Arh;->x:Z

    .line 1719512
    iget-boolean v0, p0, LX/Arh;->v:Z

    if-nez v0, :cond_2

    .line 1719513
    iget-object v0, p0, LX/Arh;->j:LX/Arq;

    invoke-virtual {v0}, LX/Arq;->a()V

    .line 1719514
    iput-boolean v2, p0, LX/Arh;->v:Z

    .line 1719515
    :cond_2
    iget-object v0, p0, LX/Arh;->j:LX/Arq;

    const/4 v5, 0x0

    .line 1719516
    iget-object v1, v0, LX/Arq;->J:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v2, 0x1

    invoke-static {v1, v2}, LX/3CW;->c(II)Z

    move-result v1

    if-nez v1, :cond_4

    .line 1719517
    :goto_1
    iget-object v0, p0, LX/Arh;->g:Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;

    invoke-static {v0}, LX/Awl;->a(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_3

    move-object v0, p1

    .line 1719518
    check-cast v0, LX/0jL;

    invoke-static {p0}, LX/Arh;->l(LX/Arh;)Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0jL;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;)Ljava/lang/Object;

    goto :goto_0

    .line 1719519
    :cond_3
    iget-object v0, p0, LX/Arh;->g:Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;

    iget-object v1, p0, LX/Arh;->r:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/Awl;->a(Landroid/view/View;Ljava/lang/Runnable;)V

    goto :goto_0

    .line 1719520
    :cond_4
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, LX/Arq;->J:Ljava/lang/Integer;

    .line 1719521
    invoke-static {v0}, LX/Arq;->p(LX/Arq;)V

    .line 1719522
    iget-object v1, v0, LX/Arq;->r:LX/6Jt;

    invoke-virtual {v1}, LX/6Jt;->b()V

    .line 1719523
    iget-object v1, v0, LX/Arq;->r:LX/6Jt;

    new-instance v2, LX/7Sh;

    iget-object v3, v0, LX/Arq;->z:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, LX/0eJ;->q:LX/0Tn;

    invoke-interface {v3, v4, v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v3

    invoke-direct {v2, v3}, LX/7Sh;-><init>(Z)V

    invoke-virtual {v1, v2}, LX/6Jt;->a(LX/7Sb;)V

    goto :goto_1
.end method

.method public static a(LX/Arh;LX/0jL;LX/86q;)LX/0jL;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TMutation;",
            "LX/86q;",
            ")TMutation;"
        }
    .end annotation

    .prologue
    .line 1719524
    iget-object v0, p0, LX/Arh;->f:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 1719525
    check-cast p1, LX/0jL;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->j()Lcom/facebook/friendsharing/inspiration/model/CameraState;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/friendsharing/inspiration/model/CameraState;->a(Lcom/facebook/friendsharing/inspiration/model/CameraState;)Lcom/facebook/friendsharing/inspiration/model/CameraState$Builder;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/facebook/friendsharing/inspiration/model/CameraState$Builder;->setCaptureState(LX/86q;)Lcom/facebook/friendsharing/inspiration/model/CameraState$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/CameraState$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/CameraState;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/0jL;->a(Lcom/facebook/friendsharing/inspiration/model/CameraState;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jL;

    return-object v0
.end method

.method public static a(LX/Arh;LX/0Or;LX/0Or;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Arh;",
            "LX/0Or",
            "<",
            "LX/0kL;",
            ">;",
            "LX/0Or",
            "<",
            "LX/192;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0Yi;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1719526
    iput-object p1, p0, LX/Arh;->a:LX/0Or;

    iput-object p2, p0, LX/Arh;->b:LX/0Or;

    iput-object p3, p0, LX/Arh;->c:LX/0Or;

    return-void
.end method

.method public static b(LX/Arh;LX/0jL;)LX/0jL;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TMutation;)TMutation;"
        }
    .end annotation

    .prologue
    .line 1719527
    iget-boolean v0, p0, LX/Arh;->x:Z

    if-nez v0, :cond_0

    .line 1719528
    sget-object v0, LX/86q;->UNINITIALIZED:LX/86q;

    invoke-static {p0, p1, v0}, LX/Arh;->a(LX/Arh;LX/0jL;LX/86q;)LX/0jL;

    .line 1719529
    :goto_0
    return-object p1

    .line 1719530
    :cond_0
    iget-object v0, p0, LX/Arh;->g:Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;->setVisibility(I)V

    .line 1719531
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Arh;->x:Z

    .line 1719532
    iget-object v0, p0, LX/Arh;->j:LX/Arq;

    .line 1719533
    iget-object v1, v0, LX/Arq;->J:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v2, 0x0

    invoke-static {v1, v2}, LX/3CW;->c(II)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1719534
    :goto_1
    iget-object v0, p0, LX/Arh;->k:LX/195;

    invoke-virtual {v0}, LX/195;->b()V

    .line 1719535
    sget-object v0, LX/86q;->UNINITIALIZED:LX/86q;

    invoke-static {p0, p1, v0}, LX/Arh;->a(LX/Arh;LX/0jL;LX/86q;)LX/0jL;

    goto :goto_0

    .line 1719536
    :cond_1
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, LX/Arq;->J:Ljava/lang/Integer;

    .line 1719537
    iget-object v1, v0, LX/Arq;->D:LX/6Ia;

    invoke-interface {v1}, LX/6Ia;->b()V

    .line 1719538
    iget-object v1, v0, LX/Arq;->r:LX/6Jt;

    invoke-virtual {v1}, LX/6Jt;->c()V

    goto :goto_1
.end method

.method public static j(LX/Arh;)LX/0jL;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TMutation;"
        }
    .end annotation

    .prologue
    .line 1719539
    iget-object v0, p0, LX/Arh;->f:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    check-cast v0, LX/0im;

    invoke-interface {v0}, LX/0im;->c()LX/0jJ;

    move-result-object v0

    sget-object v1, LX/Arh;->d:LX/0jK;

    invoke-virtual {v0, v1}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v0

    return-object v0
.end method

.method private k()Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 1719540
    new-instance v0, Lcom/facebook/friendsharing/inspiration/capture/InspirationCameraPreviewController$1;

    invoke-direct {v0, p0}, Lcom/facebook/friendsharing/inspiration/capture/InspirationCameraPreviewController$1;-><init>(LX/Arh;)V

    return-object v0
.end method

.method public static l(LX/Arh;)Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1719374
    invoke-static {}, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;->newBuilder()Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds$Builder;->setLeft(I)Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds$Builder;->setTop(I)Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds$Builder;

    move-result-object v0

    iget-object v1, p0, LX/Arh;->g:Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;

    invoke-virtual {v1}, Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds$Builder;->setRight(I)Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds$Builder;

    move-result-object v0

    iget-object v1, p0, LX/Arh;->g:Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;

    invoke-virtual {v1}, Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;->getMeasuredHeight()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds$Builder;->setBottom(I)Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBounds;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/5L2;)V
    .locals 2

    .prologue
    .line 1719541
    iget-object v0, p0, LX/Arh;->f:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 1719542
    sget-object v1, LX/5L2;->ON_PAUSE:LX/5L2;

    if-ne p1, v1, :cond_1

    .line 1719543
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/Arh;->y:Z

    .line 1719544
    :cond_0
    :goto_0
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    invoke-static {v0}, LX/87R;->b(LX/0io;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1719545
    :goto_1
    return-void

    .line 1719546
    :cond_1
    sget-object v1, LX/5L2;->ON_RESUME:LX/5L2;

    if-ne p1, v1, :cond_0

    .line 1719547
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/Arh;->y:Z

    goto :goto_0

    .line 1719548
    :cond_2
    sget-object v0, LX/Arc;->b:[I

    invoke-virtual {p1}, LX/5L2;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_1

    .line 1719549
    :pswitch_0
    invoke-static {p0}, LX/Arh;->j(LX/Arh;)LX/0jL;

    move-result-object v0

    invoke-direct {p0, v0}, LX/Arh;->a(LX/0jL;)LX/0jL;

    move-result-object v0

    invoke-virtual {v0}, LX/0jL;->a()V

    goto :goto_1

    .line 1719550
    :pswitch_1
    invoke-static {p0}, LX/Arh;->j(LX/Arh;)LX/0jL;

    move-result-object v0

    invoke-static {p0, v0}, LX/Arh;->b(LX/Arh;LX/0jL;)LX/0jL;

    move-result-object v0

    invoke-virtual {v0}, LX/0jL;->a()V

    goto :goto_1

    .line 1719551
    :pswitch_2
    iget-object v0, p0, LX/Arh;->j:LX/Arq;

    .line 1719552
    iget-object v1, v0, LX/Arq;->D:LX/6Ia;

    if-eqz v1, :cond_3

    .line 1719553
    iget-object v1, v0, LX/Arq;->D:LX/6Ia;

    invoke-interface {v1}, LX/6Ia;->b()V

    .line 1719554
    :cond_3
    iget-object v1, v0, LX/Arq;->r:LX/6Jt;

    invoke-virtual {v1}, LX/6Jt;->d()V

    .line 1719555
    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(LX/Arb;)V
    .locals 1

    .prologue
    .line 1719484
    iget-object v0, p0, LX/Arh;->j:LX/Arq;

    .line 1719485
    iget-object p0, v0, LX/Arq;->v:LX/0UE;

    invoke-virtual {p0, p1}, LX/0UE;->add(Ljava/lang/Object;)Z

    .line 1719486
    return-void
.end method

.method public final a(LX/BA6;)V
    .locals 3

    .prologue
    .line 1719556
    iget-object v0, p0, LX/Arh;->j:LX/Arq;

    .line 1719557
    new-instance v1, LX/BAE;

    invoke-direct {v1, p1}, LX/BAE;-><init>(LX/BA6;)V

    .line 1719558
    iget-object v2, v0, LX/Arq;->r:LX/6Jt;

    iget-object p0, v0, LX/Arq;->m:LX/BAC;

    invoke-virtual {v2, v1, p0}, LX/6Jt;->a(LX/7Sb;LX/6Jv;)V

    .line 1719559
    return-void
.end method

.method public final a(LX/BVA;)V
    .locals 3

    .prologue
    .line 1719480
    iget-object v0, p0, LX/Arh;->j:LX/Arq;

    .line 1719481
    new-instance v1, LX/BVJ;

    invoke-direct {v1, p1}, LX/BVJ;-><init>(LX/BVA;)V

    .line 1719482
    iget-object v2, v0, LX/Arq;->r:LX/6Jt;

    iget-object p0, v0, LX/Arq;->l:Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;

    invoke-virtual {v2, v1, p0}, LX/6Jt;->a(LX/7Sb;LX/6Jv;)V

    .line 1719483
    return-void
.end method

.method public final a(Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;)V
    .locals 1

    .prologue
    .line 1719475
    iget-object v0, p0, LX/Arh;->j:LX/Arq;

    .line 1719476
    iget-object p0, v0, LX/Arq;->n:LX/8Fw;

    .line 1719477
    iput-object p1, p0, LX/8Fw;->c:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;

    .line 1719478
    invoke-static {p0}, LX/8Fw;->a(LX/8Fw;)V

    .line 1719479
    return-void
.end method

.method public final a(Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;)V
    .locals 3

    .prologue
    .line 1719471
    iget-object v0, p0, LX/Arh;->j:LX/Arq;

    .line 1719472
    new-instance v1, LX/7Sn;

    invoke-direct {v1, p1}, LX/7Sn;-><init>(Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;)V

    .line 1719473
    iget-object v2, v0, LX/Arq;->r:LX/6Jt;

    iget-object p0, v0, LX/Arq;->o:Lcom/facebook/videocodec/effects/renderers/ShaderRenderer;

    invoke-virtual {v2, v1, p0}, LX/6Jt;->a(LX/7Sb;LX/6Jv;)V

    .line 1719474
    return-void
.end method

.method public final a(Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;)V
    .locals 3

    .prologue
    .line 1719463
    iget-object v0, p0, LX/Arh;->j:LX/Arq;

    .line 1719464
    new-instance v1, LX/7So;

    invoke-direct {v1}, LX/7So;-><init>()V

    .line 1719465
    if-eqz p1, :cond_0

    .line 1719466
    invoke-virtual {p1}, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;->initResPath()Ljava/lang/String;

    move-result-object v2

    .line 1719467
    invoke-virtual {p1}, Lcom/facebook/videocodec/effects/model/StyleTransferGLConfig;->initPredictPath()Ljava/lang/String;

    move-result-object p0

    .line 1719468
    new-instance v1, LX/7So;

    invoke-direct {v1, v2, p0}, LX/7So;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1719469
    :cond_0
    iget-object v2, v0, LX/Arq;->r:LX/6Jt;

    iget-object p0, v0, LX/Arq;->q:LX/BVQ;

    invoke-virtual {v2, v1, p0}, LX/6Jt;->a(LX/7Sb;LX/6Jv;)V

    .line 1719470
    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 10

    .prologue
    .line 1719394
    check-cast p1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1719395
    iget-object v0, p0, LX/Arh;->f:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 1719396
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    .line 1719397
    invoke-virtual {v1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->j()Lcom/facebook/friendsharing/inspiration/model/CameraState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/friendsharing/inspiration/model/CameraState;->getCaptureState()LX/86q;

    move-result-object v6

    .line 1719398
    invoke-virtual {p1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->j()Lcom/facebook/friendsharing/inspiration/model/CameraState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/friendsharing/inspiration/model/CameraState;->getCaptureState()LX/86q;

    move-result-object v7

    .line 1719399
    sget-object v2, LX/86q;->READY:LX/86q;

    if-eq v6, v2, :cond_0

    sget-object v2, LX/86q;->RECORDING_VIDEO:LX/86q;

    if-ne v6, v2, :cond_5

    :cond_0
    move v3, v5

    :goto_0
    move-object v2, v1

    .line 1719400
    check-cast v2, LX/0io;

    invoke-static {v2}, LX/87R;->a(LX/0io;)Z

    move-result v8

    .line 1719401
    if-eq v6, v7, :cond_1

    .line 1719402
    sget-object v2, LX/Arc;->a:[I

    invoke-virtual {v1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->j()Lcom/facebook/friendsharing/inspiration/model/CameraState;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/friendsharing/inspiration/model/CameraState;->getCaptureState()LX/86q;

    move-result-object v6

    invoke-virtual {v6}, LX/86q;->ordinal()I

    move-result v6

    aget v2, v2, v6

    packed-switch v2, :pswitch_data_0

    .line 1719403
    :goto_1
    if-eqz v3, :cond_6

    if-nez v8, :cond_6

    iget-object v2, p0, LX/Arh;->j:LX/Arq;

    invoke-virtual {v2}, LX/Arq;->o()Z

    move-result v2

    if-eqz v2, :cond_6

    move v2, v5

    .line 1719404
    :goto_2
    if-eqz v2, :cond_7

    iget-object v6, p0, LX/Arh;->e:Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;

    iget-object v7, p0, LX/Arh;->t:LX/Arg;

    invoke-virtual {v6, v7}, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->c(LX/Arf;)Z

    move-result v6

    if-nez v6, :cond_7

    .line 1719405
    iget-object v2, p0, LX/Arh;->e:Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;

    iget-object v6, p0, LX/Arh;->t:LX/Arg;

    invoke-virtual {v2, v6}, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->a(LX/Arf;)V

    .line 1719406
    :cond_1
    :goto_3
    invoke-static {v0}, LX/87N;->a(LX/0il;)LX/86o;

    move-result-object v2

    sget-object v6, LX/86o;->NONE:LX/86o;

    if-ne v2, v6, :cond_8

    .line 1719407
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v2}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->isBottomTrayTransitioning()Z

    move-result v2

    move v0, v2

    .line 1719408
    if-nez v0, :cond_8

    if-eqz v3, :cond_8

    if-nez v8, :cond_8

    .line 1719409
    :goto_4
    if-eqz v5, :cond_9

    iget-object v0, p0, LX/Arh;->e:Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;

    iget-object v2, p0, LX/Arh;->s:LX/Are;

    invoke-virtual {v0, v2}, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->c(LX/Ard;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 1719410
    iget-object v0, p0, LX/Arh;->e:Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;

    iget-object v2, p0, LX/Arh;->s:LX/Are;

    invoke-virtual {v0, v2}, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->a(LX/Ard;)V

    .line 1719411
    :cond_2
    :goto_5
    invoke-static {p0}, LX/Arh;->j(LX/Arh;)LX/0jL;

    move-result-object v3

    move-object v0, p1

    .line 1719412
    check-cast v0, LX/0io;

    move-object v2, v1

    check-cast v2, LX/0io;

    invoke-static {v0, v2}, LX/87R;->a(LX/0io;LX/0io;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1719413
    invoke-static {p0, v3}, LX/Arh;->b(LX/Arh;LX/0jL;)LX/0jL;

    .line 1719414
    :cond_3
    :goto_6
    invoke-virtual {v1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->j()Lcom/facebook/friendsharing/inspiration/model/CameraState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/CameraState;->isCameraFrontFacing()Z

    move-result v0

    invoke-virtual {p1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->j()Lcom/facebook/friendsharing/inspiration/model/CameraState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/model/CameraState;->isCameraFrontFacing()Z

    move-result v1

    if-eq v0, v1, :cond_4

    .line 1719415
    iget-object v0, p0, LX/Arh;->f:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    .line 1719416
    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->j()Lcom/facebook/friendsharing/inspiration/model/CameraState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/model/CameraState;->isCameraFrontFacing()Z

    move-result v1

    .line 1719417
    iget-object v2, p0, LX/Arh;->j:LX/Arq;

    invoke-virtual {v2}, LX/Arq;->c()Z

    move-result v2

    if-eq v1, v2, :cond_4

    .line 1719418
    iget-object v2, p0, LX/Arh;->j:LX/Arq;

    const/4 v4, 0x1

    .line 1719419
    iget-object v5, v2, LX/Arq;->w:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v5}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v5

    if-nez v5, :cond_d

    .line 1719420
    const/4 v4, 0x0

    .line 1719421
    :goto_7
    move v2, v4

    .line 1719422
    if-eqz v2, :cond_4

    .line 1719423
    iget-object v2, p0, LX/Arh;->l:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    sget-object v4, LX/AwE;->b:LX/0Tn;

    invoke-interface {v2, v4, v1}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 1719424
    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->j()Lcom/facebook/friendsharing/inspiration/model/CameraState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/CameraState;->getCaptureState()LX/86q;

    move-result-object v0

    sget-object v1, LX/86q;->RECORDING_VIDEO:LX/86q;

    if-ne v0, v1, :cond_c

    const/4 v0, 0x1

    :goto_8
    iput-boolean v0, p0, LX/Arh;->w:Z

    .line 1719425
    sget-object v0, LX/86q;->FLIPPING:LX/86q;

    invoke-static {p0, v3, v0}, LX/Arh;->a(LX/Arh;LX/0jL;LX/86q;)LX/0jL;

    .line 1719426
    :cond_4
    invoke-virtual {v3}, LX/0jL;->a()V

    .line 1719427
    return-void

    :cond_5
    move v3, v4

    .line 1719428
    goto/16 :goto_0

    .line 1719429
    :pswitch_0
    iget-object v2, p0, LX/Arh;->j:LX/Arq;

    .line 1719430
    iget-object v6, v2, LX/Arq;->x:LX/1Er;

    const-string v7, "FB_PHOTO_FOR_UPLOAD_"

    const-string v9, ".jpeg"

    sget-object p2, LX/46h;->REQUIRE_PRIVATE:LX/46h;

    invoke-virtual {v6, v7, v9, p2}, LX/1Er;->a(Ljava/lang/String;Ljava/lang/String;LX/46h;)Ljava/io/File;

    move-result-object v6

    .line 1719431
    :try_start_0
    invoke-static {v6}, LX/6JM;->a(Ljava/io/File;)V

    .line 1719432
    iget-object v7, v2, LX/Arq;->r:LX/6Jt;

    iget-object v9, v2, LX/Arq;->y:Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;

    .line 1719433
    new-instance p2, LX/Arm;

    invoke-direct {p2, v2, v6}, LX/Arm;-><init>(LX/Arq;Ljava/io/File;)V

    move-object p2, p2

    .line 1719434
    invoke-virtual {v7, v6, v9, p2}, LX/6Jt;->a(Ljava/io/File;Landroid/view/View;LX/6JI;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1719435
    :goto_9
    goto/16 :goto_1

    .line 1719436
    :pswitch_1
    iget-object v2, p0, LX/Arh;->j:LX/Arq;

    .line 1719437
    invoke-static {v2}, LX/Arq;->s(LX/Arq;)Ljava/io/File;

    move-result-object v6

    .line 1719438
    if-nez v6, :cond_e

    .line 1719439
    :goto_a
    goto/16 :goto_1

    .line 1719440
    :pswitch_2
    iget-object v2, p0, LX/Arh;->j:LX/Arq;

    .line 1719441
    iget-object v6, v2, LX/Arq;->r:LX/6Jt;

    invoke-virtual {v6}, LX/6Jt;->e()V

    .line 1719442
    goto/16 :goto_1

    :cond_6
    move v2, v4

    .line 1719443
    goto/16 :goto_2

    .line 1719444
    :cond_7
    if-nez v2, :cond_1

    iget-object v2, p0, LX/Arh;->e:Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;

    iget-object v6, p0, LX/Arh;->t:LX/Arg;

    invoke-virtual {v2, v6}, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->c(LX/Arf;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1719445
    iget-object v2, p0, LX/Arh;->e:Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;

    iget-object v6, p0, LX/Arh;->t:LX/Arg;

    invoke-virtual {v2, v6}, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->b(LX/Arf;)Z

    goto/16 :goto_3

    :cond_8
    move v5, v4

    .line 1719446
    goto/16 :goto_4

    .line 1719447
    :cond_9
    if-nez v5, :cond_2

    iget-object v0, p0, LX/Arh;->e:Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;

    iget-object v2, p0, LX/Arh;->s:LX/Are;

    invoke-virtual {v0, v2}, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->c(LX/Ard;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1719448
    iget-object v0, p0, LX/Arh;->e:Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;

    iget-object v2, p0, LX/Arh;->s:LX/Are;

    invoke-virtual {v0, v2}, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->b(LX/Ard;)Z

    goto/16 :goto_5

    :cond_a
    move-object v0, p1

    .line 1719449
    check-cast v0, LX/0io;

    move-object v2, v1

    check-cast v2, LX/0io;

    invoke-static {v0, v2}, LX/87R;->b(LX/0io;LX/0io;)Z

    move-result v0

    if-nez v0, :cond_b

    move-object v0, p1

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-object v2, v1

    check-cast v2, Lcom/facebook/composer/system/model/ComposerModelImpl;

    .line 1719450
    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->isInNuxMode()Z

    move-result v4

    if-eqz v4, :cond_11

    invoke-virtual {v2}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->isInNuxMode()Z

    move-result v4

    if-nez v4, :cond_11

    const/4 v4, 0x1

    :goto_b
    move v0, v4

    .line 1719451
    if-eqz v0, :cond_3

    .line 1719452
    :cond_b
    invoke-direct {p0, v3}, LX/Arh;->a(LX/0jL;)LX/0jL;

    goto/16 :goto_6

    .line 1719453
    :cond_c
    const/4 v0, 0x0

    goto/16 :goto_8

    .line 1719454
    :cond_d
    iput-boolean v4, v2, LX/Arq;->C:Z

    .line 1719455
    iget-object v5, v2, LX/Arq;->D:LX/6Ia;

    new-instance v6, LX/Arj;

    invoke-direct {v6, v2}, LX/Arj;-><init>(LX/Arq;)V

    invoke-interface {v5, v6}, LX/6Ia;->b(LX/6Ik;)V

    goto/16 :goto_7

    .line 1719456
    :catch_0
    move-exception v6

    .line 1719457
    const-string v7, "pr_camera_take_photo_file_error"

    invoke-static {v2, v7, v6}, LX/Arq;->a$redex0(LX/Arq;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_9

    .line 1719458
    :cond_e
    iget-object v7, v2, LX/Arq;->r:LX/6Jt;

    invoke-virtual {v7}, LX/6Jt;->a()LX/6K9;

    move-result-object v7

    sget-object v9, LX/6K9;->PREPARED:LX/6K9;

    if-ne v7, v9, :cond_f

    .line 1719459
    iget-object v7, v2, LX/Arq;->r:LX/6Jt;

    invoke-static {v2, v6}, LX/Arq;->d(LX/Arq;Ljava/io/File;)LX/6JG;

    move-result-object v9

    invoke-virtual {v7, v6, v9}, LX/6Jt;->a(Ljava/io/File;LX/6JG;)V

    goto/16 :goto_a

    .line 1719460
    :cond_f
    iget-object v7, v2, LX/Arq;->r:LX/6Jt;

    invoke-virtual {v7}, LX/6Jt;->a()LX/6K9;

    move-result-object v7

    sget-object v9, LX/6K9;->STOPPED:LX/6K9;

    if-ne v7, v9, :cond_10

    .line 1719461
    invoke-static {v2, v6}, LX/Arq;->a$redex0(LX/Arq;Ljava/io/File;)V

    .line 1719462
    :cond_10
    const/4 v6, 0x1

    iput-boolean v6, v2, LX/Arq;->H:Z

    goto/16 :goto_a

    :cond_11
    const/4 v4, 0x0

    goto :goto_b

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1719387
    iget-object v0, p0, LX/Arh;->m:Lcom/facebook/friendsharing/inspiration/capture/cache/InspirationPhotoCaptureCache;

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/capture/cache/InspirationPhotoCaptureCache;->a()V

    .line 1719388
    iget-object v0, p0, LX/Arh;->h:LX/Aqx;

    .line 1719389
    iget-object v1, v0, LX/Aqx;->a:Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;

    iget-object v1, v1, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->S:LX/0Sh;

    invoke-virtual {v1}, LX/0Sh;->a()V

    .line 1719390
    iget-object v1, v0, LX/Aqx;->a:Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object p0, v0, LX/Aqx;->a:Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    const p1, 0x7f082792

    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object p0

    const/4 p1, 0x0

    invoke-static {v1, p0, p1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 1719391
    iget-object v1, v0, LX/Aqx;->a:Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;

    iget-object v1, v1, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->am:LX/0iO;

    if-eqz v1, :cond_0

    .line 1719392
    iget-object v1, v0, LX/Aqx;->a:Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;

    iget-object v1, v1, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->am:LX/0iO;

    invoke-interface {v1}, LX/0iO;->b()Z

    .line 1719393
    :cond_0
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 1719384
    iget-object v0, p0, LX/Arh;->j:LX/Arq;

    .line 1719385
    iget-object p0, v0, LX/Arq;->B:LX/6JR;

    iget p0, p0, LX/6JR;->a:I

    move v0, p0

    .line 1719386
    return v0
.end method

.method public final b(LX/Arb;)V
    .locals 1

    .prologue
    .line 1719381
    iget-object v0, p0, LX/Arh;->j:LX/Arq;

    .line 1719382
    iget-object p0, v0, LX/Arq;->v:LX/0UE;

    invoke-virtual {p0, p1}, LX/0UE;->remove(Ljava/lang/Object;)Z

    .line 1719383
    return-void
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 1719378
    iget-object v0, p0, LX/Arh;->j:LX/Arq;

    .line 1719379
    iget-object p0, v0, LX/Arq;->B:LX/6JR;

    iget p0, p0, LX/6JR;->b:I

    move v0, p0

    .line 1719380
    return v0
.end method

.method public final h()V
    .locals 2

    .prologue
    .line 1719375
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/Arh;->z:J

    .line 1719376
    invoke-static {p0}, LX/Arh;->j(LX/Arh;)LX/0jL;

    move-result-object v0

    sget-object v1, LX/86q;->READY:LX/86q;

    invoke-static {p0, v0, v1}, LX/Arh;->a(LX/Arh;LX/0jL;LX/86q;)LX/0jL;

    move-result-object v0

    invoke-virtual {v0}, LX/0jL;->a()V

    .line 1719377
    return-void
.end method
