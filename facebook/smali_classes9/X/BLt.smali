.class public LX/BLt;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "LX/2rw;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "LX/2rw;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final c:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 1776491
    sget-object v0, LX/2rw;->UNDIRECTED:LX/2rw;

    sget-object v1, LX/2rw;->USER:LX/2rw;

    sget-object v2, LX/2rw;->GROUP:LX/2rw;

    sget-object v3, LX/2rw;->PAGE:LX/2rw;

    invoke-static {v0, v1, v2, v3}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/BLt;->a:LX/0Rf;

    .line 1776492
    sget-object v0, LX/2rw;->UNDIRECTED:LX/2rw;

    sget-object v1, LX/2rw;->GROUP:LX/2rw;

    invoke-static {v0, v1}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/BLt;->b:LX/0Rf;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;)V
    .locals 1
    .param p1    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1776488
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1776489
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/BLt;->c:Z

    .line 1776490
    return-void
.end method
