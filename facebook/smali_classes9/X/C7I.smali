.class public LX/C7I;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ":",
        "LX/1Pq;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static h:LX/0Xm;


# instance fields
.field private final a:Z

.field public final b:LX/3AF;

.field private final c:LX/3AE;

.field public final d:LX/3AD;

.field private final e:LX/17W;

.field private final f:LX/1nM;

.field private final g:LX/1VI;


# direct methods
.method public constructor <init>(LX/0Or;LX/3AF;LX/3AE;LX/3AD;LX/1nM;LX/17W;LX/1VI;)V
    .locals 1
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/feed/annotations/IsAutoTranslateEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/3AF;",
            "LX/3AE;",
            "LX/3AD;",
            "LX/1nM;",
            "LX/17W;",
            "LX/1VI;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1851135
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1851136
    invoke-interface {p1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/C7I;->a:Z

    .line 1851137
    iput-object p2, p0, LX/C7I;->b:LX/3AF;

    .line 1851138
    iput-object p3, p0, LX/C7I;->c:LX/3AE;

    .line 1851139
    iput-object p4, p0, LX/C7I;->d:LX/3AD;

    .line 1851140
    iput-object p5, p0, LX/C7I;->f:LX/1nM;

    .line 1851141
    iput-object p6, p0, LX/C7I;->e:LX/17W;

    .line 1851142
    iput-object p7, p0, LX/C7I;->g:LX/1VI;

    .line 1851143
    return-void
.end method

.method public static a(LX/1VI;)I
    .locals 1
    .annotation build Landroid/support/annotation/ColorRes;
    .end annotation

    .prologue
    .line 1851132
    invoke-virtual {p0}, LX/1VI;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1851133
    const v0, 0x7f0a00aa

    .line 1851134
    :goto_0
    return v0

    :cond_0
    const v0, 0x7f0a0158

    goto :goto_0
.end method

.method private static a(LX/C7I;Landroid/content/Context;LX/C7H;ZLX/1Pr;Lcom/facebook/feed/rows/core/props/FeedProps;)LX/8pR;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/C7H;",
            "ZTE;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "LX/8pR;"
        }
    .end annotation

    .prologue
    .line 1851144
    iget-object v0, p5, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1851145
    move-object v6, v0

    check-cast v6, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1851146
    new-instance v0, LX/8pR;

    invoke-static {v6}, LX/8pK;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/translation/TranslatabilityForViewerGraphQLModels$TranslatabilityGraphQLModel;

    move-result-object v1

    iget-object v3, p0, LX/C7I;->c:LX/3AE;

    iget-boolean v2, p0, LX/C7I;->a:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    iget-object v5, p0, LX/C7I;->e:LX/17W;

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, LX/8pR;-><init>(Lcom/facebook/translation/TranslatabilityForViewerGraphQLModels$TranslatabilityGraphQLModel;Landroid/content/Context;LX/3AE;Ljava/lang/Boolean;LX/17W;)V

    .line 1851147
    if-nez p3, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v0, v1}, LX/8pR;->a(Z)V

    .line 1851148
    new-instance v1, LX/C7F;

    invoke-direct {v1, p0, p4, p2, p5}, LX/C7F;-><init>(LX/C7I;LX/1Pr;LX/C7H;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1851149
    iput-object v1, v0, LX/8pR;->g:LX/8pQ;

    .line 1851150
    iget-object v1, v0, LX/8pR;->b:LX/8pV;

    move-object v1, v1

    .line 1851151
    new-instance v2, LX/C7G;

    invoke-direct {v2, p0, v6}, LX/C7G;-><init>(LX/C7I;Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 1851152
    iput-object v2, v1, LX/8pV;->e:LX/8pU;

    .line 1851153
    return-object v0

    .line 1851154
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/C7I;
    .locals 11

    .prologue
    .line 1851121
    const-class v1, LX/C7I;

    monitor-enter v1

    .line 1851122
    :try_start_0
    sget-object v0, LX/C7I;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1851123
    sput-object v2, LX/C7I;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1851124
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1851125
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1851126
    new-instance v3, LX/C7I;

    const/16 v4, 0x1496

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {v0}, LX/3AF;->a(LX/0QB;)LX/3AF;

    move-result-object v5

    check-cast v5, LX/3AF;

    invoke-static {v0}, LX/3AE;->b(LX/0QB;)LX/3AE;

    move-result-object v6

    check-cast v6, LX/3AE;

    invoke-static {v0}, LX/3AD;->b(LX/0QB;)LX/3AD;

    move-result-object v7

    check-cast v7, LX/3AD;

    invoke-static {v0}, LX/1nM;->a(LX/0QB;)LX/1nM;

    move-result-object v8

    check-cast v8, LX/1nM;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v9

    check-cast v9, LX/17W;

    invoke-static {v0}, LX/1VI;->a(LX/0QB;)LX/1VI;

    move-result-object v10

    check-cast v10, LX/1VI;

    invoke-direct/range {v3 .. v10}, LX/C7I;-><init>(LX/0Or;LX/3AF;LX/3AE;LX/3AD;LX/1nM;LX/17W;LX/1VI;)V

    .line 1851127
    move-object v0, v3

    .line 1851128
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1851129
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C7I;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1851130
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1851131
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pr;LX/1np;)LX/1Dg;
    .locals 11
    .param p2    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # LX/1Pr;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;",
            "LX/1np",
            "<",
            "LX/8pR;",
            ">;)",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    const/4 v10, 0x5

    const/4 v9, 0x4

    const/4 v8, 0x2

    const/4 v7, 0x0

    const/4 v6, 0x3

    .line 1851098
    new-instance v2, LX/C7H;

    .line 1851099
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1851100
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-direct {v2, v0}, LX/C7H;-><init>(Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 1851101
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1851102
    check-cast v0, LX/0jW;

    invoke-interface {p3, v2, v0}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    move-object v0, p0

    move-object v1, p1

    move-object v4, p3

    move-object v5, p2

    .line 1851103
    invoke-static/range {v0 .. v5}, LX/C7I;->a(LX/C7I;Landroid/content/Context;LX/C7H;ZLX/1Pr;Lcom/facebook/feed/rows/core/props/FeedProps;)LX/8pR;

    move-result-object v0

    .line 1851104
    iput-object v0, p4, LX/1np;->a:Ljava/lang/Object;

    .line 1851105
    iget-object v0, p0, LX/C7I;->f:LX/1nM;

    invoke-virtual {v0, p2}, LX/1nM;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Landroid/text/Spannable;

    move-result-object v1

    .line 1851106
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1851107
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1851108
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aY()Lcom/facebook/graphql/model/GraphQLPostTranslatability;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPostTranslatability;->m()Lcom/facebook/graphql/model/GraphQLTranslation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTranslation;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    .line 1851109
    iget-object v4, p0, LX/C7I;->b:LX/3AF;

    invoke-virtual {v4, v2}, LX/3AF;->a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object v2, v2

    .line 1851110
    iget-object v4, p0, LX/C7I;->b:LX/3AF;

    .line 1851111
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1851112
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v4, v0}, LX/3AF;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v4

    .line 1851113
    if-eqz v3, :cond_0

    .line 1851114
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v0

    const v1, 0x7f0b0050

    invoke-virtual {v0, v1}, LX/1ne;->q(I)LX/1ne;

    move-result-object v0

    const/high16 v1, 0x3fc00000    # 1.5f

    invoke-virtual {v0, v1}, LX/1ne;->i(F)LX/1ne;

    move-result-object v0

    invoke-virtual {v0, v7}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v0

    iget-object v1, p0, LX/C7I;->g:LX/1VI;

    invoke-static {v1}, LX/C7I;->a(LX/1VI;)I

    move-result v1

    invoke-virtual {v0, v1}, LX/1ne;->n(I)LX/1ne;

    move-result-object v0

    const v1, 0x7f0a0159

    invoke-virtual {v0, v1}, LX/1ne;->v(I)LX/1ne;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-interface {v0, v1}, LX/1Di;->a(F)LX/1Di;

    move-result-object v0

    const/4 v1, 0x7

    invoke-interface {v0, v1, v6}, LX/1Di;->d(II)LX/1Di;

    move-result-object v0

    const v1, 0x7f0b0917

    invoke-interface {v0, v10, v1}, LX/1Di;->c(II)LX/1Di;

    move-result-object v0

    invoke-interface {v0, v6, v8}, LX/1Di;->h(II)LX/1Di;

    move-result-object v0

    const v1, 0x7f020afa

    invoke-interface {v0, v1}, LX/1Di;->x(I)LX/1Di;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    .line 1851115
    :goto_0
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1, v8}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v1

    invoke-static {p1}, LX/25N;->c(LX/1De;)LX/25Q;

    move-result-object v3

    const v5, 0x7f0a0042

    invoke-virtual {v3, v5}, LX/25Q;->i(I)LX/25Q;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const/4 v5, 0x1

    invoke-interface {v3, v5}, LX/1Di;->j(I)LX/1Di;

    move-result-object v3

    const/4 v5, 0x6

    invoke-interface {v3, v10, v5}, LX/1Di;->d(II)LX/1Di;

    move-result-object v3

    invoke-interface {v3, v9}, LX/1Di;->b(I)LX/1Di;

    move-result-object v3

    invoke-interface {v1, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v2

    const v3, 0x7f0b0050

    invoke-virtual {v2, v3}, LX/1ne;->q(I)LX/1ne;

    move-result-object v2

    const/high16 v3, 0x3fc00000    # 1.5f

    invoke-virtual {v2, v3}, LX/1ne;->i(F)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, v7}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v2

    iget-object v3, p0, LX/C7I;->g:LX/1VI;

    invoke-static {v3}, LX/C7I;->a(LX/1VI;)I

    move-result v3

    invoke-virtual {v2, v3}, LX/1ne;->n(I)LX/1ne;

    move-result-object v2

    const v3, 0x7f0a0159

    invoke-virtual {v2, v3}, LX/1ne;->v(I)LX/1ne;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-interface {v2, v3}, LX/1Di;->a(F)LX/1Di;

    move-result-object v2

    const/4 v3, 0x7

    invoke-interface {v2, v3, v6}, LX/1Di;->d(II)LX/1Di;

    move-result-object v2

    const/4 v3, 0x6

    invoke-interface {v2, v9, v3}, LX/1Di;->d(II)LX/1Di;

    move-result-object v2

    const v3, 0x7f0b0917

    invoke-interface {v2, v10, v3}, LX/1Di;->c(II)LX/1Di;

    move-result-object v2

    invoke-interface {v2, v6, v8}, LX/1Di;->h(II)LX/1Di;

    move-result-object v2

    const v3, 0x7f020afa

    invoke-interface {v2, v3}, LX/1Di;->x(I)LX/1Di;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    .line 1851116
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v8}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/25N;->c(LX/1De;)LX/25Q;

    move-result-object v3

    const v5, 0x7f0a0042

    invoke-virtual {v3, v5}, LX/25Q;->i(I)LX/25Q;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const/4 v5, 0x1

    invoke-interface {v3, v5}, LX/1Di;->j(I)LX/1Di;

    move-result-object v3

    const/4 v5, 0x6

    invoke-interface {v3, v10, v5}, LX/1Di;->d(II)LX/1Di;

    move-result-object v3

    invoke-interface {v3, v9}, LX/1Di;->b(I)LX/1Di;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, v4}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v3

    const v4, 0x7f0b0050

    invoke-virtual {v3, v4}, LX/1ne;->q(I)LX/1ne;

    move-result-object v3

    const/high16 v4, 0x3fc00000    # 1.5f

    invoke-virtual {v3, v4}, LX/1ne;->i(F)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, v7}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v3

    const v4, 0x7f0a0427

    invoke-virtual {v3, v4}, LX/1ne;->n(I)LX/1ne;

    move-result-object v3

    const v4, 0x7f0a0159

    invoke-virtual {v3, v4}, LX/1ne;->v(I)LX/1ne;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-interface {v3, v4}, LX/1Di;->a(F)LX/1Di;

    move-result-object v3

    const/4 v4, 0x7

    invoke-interface {v3, v4, v6}, LX/1Di;->d(II)LX/1Di;

    move-result-object v3

    const/4 v4, 0x6

    invoke-interface {v3, v9, v4}, LX/1Di;->d(II)LX/1Di;

    move-result-object v3

    const v4, 0x7f020afa

    invoke-interface {v3, v4}, LX/1Di;->x(I)LX/1Di;

    move-result-object v3

    .line 1851117
    const v4, 0xa4f2351

    const/4 v5, 0x0

    invoke-static {p1, v4, v5}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v4

    move-object v4, v4

    .line 1851118
    invoke-interface {v3, v4}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    .line 1851119
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v7}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v0}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, v2}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    return-object v0

    .line 1851120
    :cond_0
    const/4 v0, 0x0

    goto/16 :goto_0
.end method
