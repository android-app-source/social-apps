.class public final LX/ATm;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;)V
    .locals 0

    .prologue
    .line 1676881
    iput-object p1, p0, LX/ATm;->a:Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x2

    const v1, -0x4e609bc5

    invoke-static {v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1676882
    iget-object v1, p0, LX/ATm;->a:Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;

    iget-object v1, v1, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->d:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v1}, Lcom/facebook/video/player/RichVideoPlayer;->q()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1676883
    iget-object v1, p0, LX/ATm;->a:Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;

    invoke-virtual {v1, v2}, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->b(Z)V

    .line 1676884
    :goto_0
    iget-object v1, p0, LX/ATm;->a:Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;

    iget-object v1, v1, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->n:LX/ATs;

    if-eqz v1, :cond_0

    .line 1676885
    iget-object v1, p0, LX/ATm;->a:Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;

    iget-object v1, v1, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->n:LX/ATs;

    .line 1676886
    invoke-static {v1}, LX/ATs;->o(LX/ATs;)V

    .line 1676887
    :cond_0
    const v1, -0x7198d8ed

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 1676888
    :cond_1
    iget-object v1, p0, LX/ATm;->a:Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;

    invoke-virtual {v1, v2}, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->a(Z)V

    goto :goto_0
.end method
