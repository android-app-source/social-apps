.class public LX/BVt;
.super LX/BVT;
.source ""


# direct methods
.method public constructor <init>(LX/BW4;LX/BW5;)V
    .locals 0

    .prologue
    .line 1791488
    invoke-direct {p0, p1, p2}, LX/BVT;-><init>(LX/BW4;LX/BW5;)V

    .line 1791489
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;)Landroid/view/View;
    .locals 2

    .prologue
    .line 1791490
    new-instance v0, Lcom/facebook/widget/images/UrlImage;

    invoke-direct {v0, p1}, Lcom/facebook/widget/images/UrlImage;-><init>(Landroid/content/Context;)V

    .line 1791491
    const v1, 0x7f02013f

    invoke-virtual {v0, v1}, Lcom/facebook/widget/images/UrlImage;->setPlaceHolderResourceId(I)V

    .line 1791492
    sget-object v1, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/images/UrlImage;->setPlaceHolderScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 1791493
    return-object v0
.end method

.method public final a(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 1791494
    check-cast p1, Lcom/facebook/widget/images/UrlImage;

    .line 1791495
    sget-object v0, LX/BVr;->a:[I

    invoke-static {p2}, LX/BVs;->from(Ljava/lang/String;)LX/BVs;

    move-result-object v1

    invoke-virtual {v1}, LX/BVs;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1791496
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unhandled url image attribute = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1791497
    :pswitch_0
    iget-object v0, p0, LX/BVT;->a:LX/BW4;

    invoke-virtual {v0, p3}, LX/BW4;->l(Ljava/lang/String;)Landroid/widget/ImageView$ScaleType;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/widget/images/UrlImage;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 1791498
    :goto_0
    return-void

    .line 1791499
    :pswitch_1
    invoke-static {p3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/widget/images/UrlImage;->setImageParams(Landroid/net/Uri;)V

    goto :goto_0

    .line 1791500
    :pswitch_2
    invoke-virtual {p4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {p3}, LX/BW4;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "drawable"

    invoke-virtual {p4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/facebook/widget/images/UrlImage;->setPlaceHolderResourceId(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
