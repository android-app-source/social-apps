.class public final LX/AcL;
.super LX/2oa;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2oa",
        "<",
        "LX/7Ly;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/facecastdisplay/LiveEventsPlugin;


# direct methods
.method public constructor <init>(Lcom/facebook/facecastdisplay/LiveEventsPlugin;)V
    .locals 0

    .prologue
    .line 1692442
    iput-object p1, p0, LX/AcL;->a:Lcom/facebook/facecastdisplay/LiveEventsPlugin;

    invoke-direct {p0}, LX/2oa;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/7Ly;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1692443
    const-class v0, LX/7Ly;

    return-object v0
.end method

.method public final b(LX/0b7;)V
    .locals 2

    .prologue
    .line 1692444
    check-cast p1, LX/7Ly;

    .line 1692445
    iget-object v0, p0, LX/AcL;->a:Lcom/facebook/facecastdisplay/LiveEventsPlugin;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->c:LX/Aby;

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/7Ly;->a:LX/7Lx;

    sget-object v1, LX/7Lx;->HIDE:LX/7Lx;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, LX/AcL;->a:Lcom/facebook/facecastdisplay/LiveEventsPlugin;

    iget-boolean v0, v0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->E:Z

    if-eqz v0, :cond_0

    .line 1692446
    iget-object v0, p0, LX/AcL;->a:Lcom/facebook/facecastdisplay/LiveEventsPlugin;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->c:LX/Aby;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/Aby;->h(Z)V

    .line 1692447
    iget-object v0, p0, LX/AcL;->a:Lcom/facebook/facecastdisplay/LiveEventsPlugin;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->c:LX/Aby;

    iget-object v1, p0, LX/AcL;->a:Lcom/facebook/facecastdisplay/LiveEventsPlugin;

    iget-object v1, v1, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->c:LX/Aby;

    .line 1692448
    iget p0, v1, LX/Aby;->A:I

    move v1, p0

    .line 1692449
    invoke-virtual {v0, v1}, LX/Aby;->c(I)V

    .line 1692450
    :cond_0
    return-void
.end method
