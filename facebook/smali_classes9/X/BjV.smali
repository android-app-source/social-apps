.class public final LX/BjV;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/events/create/protocol/EventCreationMutationsModels$EditEventCoreMutationFieldsModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/events/create/EventEditNikumanActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/events/create/EventEditNikumanActivity;)V
    .locals 0

    .prologue
    .line 1812083
    iput-object p1, p0, LX/BjV;->a:Lcom/facebook/events/create/EventEditNikumanActivity;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method

.method private a()V
    .locals 4

    .prologue
    .line 1812081
    iget-object v0, p0, LX/BjV;->a:Lcom/facebook/events/create/EventEditNikumanActivity;

    invoke-virtual {v0}, Lcom/facebook/events/create/EventEditNikumanActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, LX/BjV;->a:Lcom/facebook/events/create/EventEditNikumanActivity;

    iget-object v1, v1, Lcom/facebook/events/create/EventEditNikumanActivity;->q:LX/Bky;

    iget-object v2, p0, LX/BjV;->a:Lcom/facebook/events/create/EventEditNikumanActivity;

    iget-object v2, v2, Lcom/facebook/events/create/EventEditNikumanActivity;->J:Lcom/facebook/events/model/Event;

    iget-object v3, p0, LX/BjV;->a:Lcom/facebook/events/create/EventEditNikumanActivity;

    iget-object v3, v3, Lcom/facebook/events/create/EventEditNikumanActivity;->p:LX/0TD;

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/events/data/EventsProvider;->a(Landroid/content/ContentResolver;LX/Bky;Lcom/facebook/events/model/Event;LX/0TD;)V

    .line 1812082
    return-void
.end method

.method private a(Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/events/create/protocol/EventCreationMutationsModels$EditEventCoreMutationFieldsModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1812084
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1812085
    check-cast v0, Lcom/facebook/events/create/protocol/EventCreationMutationsModels$EditEventCoreMutationFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/events/create/protocol/EventCreationMutationsModels$EditEventCoreMutationFieldsModel;->a()Lcom/facebook/events/create/protocol/EventCreationMutationsModels$EditEventCoreMutationFieldsModel$EventModel;

    move-result-object v0

    .line 1812086
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/events/create/protocol/EventCreationMutationsModels$EditEventCoreMutationFieldsModel$EventModel;->j()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/events/create/protocol/EventCreationMutationsModels$EditEventCoreMutationFieldsModel$EventModel;->j()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/BjV;->a:Lcom/facebook/events/create/EventEditNikumanActivity;

    iget-object v1, v1, Lcom/facebook/events/create/EventEditNikumanActivity;->K:Lcom/facebook/events/model/Event;

    .line 1812087
    iget-object v2, v1, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v1, v2

    .line 1812088
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1812089
    :cond_0
    invoke-direct {p0}, LX/BjV;->a()V

    .line 1812090
    :cond_1
    iget-object v0, p0, LX/BjV;->a:Lcom/facebook/events/create/EventEditNikumanActivity;

    invoke-virtual {v0}, Lcom/facebook/events/create/EventEditNikumanActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0f74

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 1812091
    iget-object v1, p0, LX/BjV;->a:Lcom/facebook/events/create/EventEditNikumanActivity;

    iget-object v1, v1, Lcom/facebook/events/create/EventEditNikumanActivity;->u:LX/Bm7;

    iget-object v2, p0, LX/BjV;->a:Lcom/facebook/events/create/EventEditNikumanActivity;

    iget-object v2, v2, Lcom/facebook/events/create/EventEditNikumanActivity;->K:Lcom/facebook/events/model/Event;

    .line 1812092
    iget-object p1, v2, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v2, p1

    .line 1812093
    invoke-virtual {v1, v0, v2}, LX/Bm7;->a(ILjava/lang/String;)V

    .line 1812094
    iget-object v0, p0, LX/BjV;->a:Lcom/facebook/events/create/EventEditNikumanActivity;

    invoke-static {v0}, Lcom/facebook/events/create/EventEditNikumanActivity;->F(Lcom/facebook/events/create/EventEditNikumanActivity;)V

    .line 1812095
    iget-object v0, p0, LX/BjV;->a:Lcom/facebook/events/create/EventEditNikumanActivity;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/facebook/events/create/EventEditNikumanActivity;->setResult(I)V

    .line 1812096
    iget-object v0, p0, LX/BjV;->a:Lcom/facebook/events/create/EventEditNikumanActivity;

    iget-object v0, v0, Lcom/facebook/events/create/EventEditNikumanActivity;->t:LX/Bl6;

    new-instance v1, LX/BlL;

    iget-object v2, p0, LX/BjV;->a:Lcom/facebook/events/create/EventEditNikumanActivity;

    iget-object v2, v2, Lcom/facebook/events/create/EventEditNikumanActivity;->K:Lcom/facebook/events/model/Event;

    .line 1812097
    iget-object p1, v2, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v2, p1

    .line 1812098
    invoke-direct {v1, v2}, LX/BlL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1812099
    iget-object v0, p0, LX/BjV;->a:Lcom/facebook/events/create/EventEditNikumanActivity;

    invoke-virtual {v0}, Lcom/facebook/events/create/EventEditNikumanActivity;->finish()V

    .line 1812100
    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1812075
    const-class v0, Lcom/facebook/events/create/EventEditNikumanActivity;

    const-string v1, "Error editing event: "

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1812076
    invoke-direct {p0}, LX/BjV;->a()V

    .line 1812077
    iget-object v0, p0, LX/BjV;->a:Lcom/facebook/events/create/EventEditNikumanActivity;

    invoke-static {v0}, Lcom/facebook/events/create/EventEditNikumanActivity;->F(Lcom/facebook/events/create/EventEditNikumanActivity;)V

    .line 1812078
    iget-object v0, p0, LX/BjV;->a:Lcom/facebook/events/create/EventEditNikumanActivity;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/facebook/events/create/EventEditNikumanActivity;->setResult(I)V

    .line 1812079
    iget-object v0, p0, LX/BjV;->a:Lcom/facebook/events/create/EventEditNikumanActivity;

    invoke-virtual {v0}, Lcom/facebook/events/create/EventEditNikumanActivity;->finish()V

    .line 1812080
    return-void
.end method

.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1812074
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    invoke-direct {p0, p1}, LX/BjV;->a(Lcom/facebook/graphql/executor/GraphQLResult;)V

    return-void
.end method
