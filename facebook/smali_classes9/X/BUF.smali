.class public LX/BUF;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "BadMethodUse-com.google.common.util.concurrent.Futures.addCallback"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static volatile j:LX/BUF;


# instance fields
.field private final b:LX/0tX;

.field private final c:LX/0sa;

.field private final d:LX/0rq;

.field private final e:LX/0se;

.field public f:I

.field public g:I

.field public h:I

.field public i:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1788761
    const-class v0, LX/BUF;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/BUF;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0tX;LX/0sa;LX/0rq;LX/0se;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1788719
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1788720
    const/16 v0, 0x1f4

    iput v0, p0, LX/BUF;->f:I

    .line 1788721
    iput-object p1, p0, LX/BUF;->b:LX/0tX;

    .line 1788722
    iput-object p2, p0, LX/BUF;->c:LX/0sa;

    .line 1788723
    iput-object p3, p0, LX/BUF;->d:LX/0rq;

    .line 1788724
    iput-object p4, p0, LX/BUF;->e:LX/0se;

    .line 1788725
    return-void
.end method

.method public static a(LX/0QB;)LX/BUF;
    .locals 7

    .prologue
    .line 1788748
    sget-object v0, LX/BUF;->j:LX/BUF;

    if-nez v0, :cond_1

    .line 1788749
    const-class v1, LX/BUF;

    monitor-enter v1

    .line 1788750
    :try_start_0
    sget-object v0, LX/BUF;->j:LX/BUF;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1788751
    if-eqz v2, :cond_0

    .line 1788752
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1788753
    new-instance p0, LX/BUF;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {v0}, LX/0sa;->a(LX/0QB;)LX/0sa;

    move-result-object v4

    check-cast v4, LX/0sa;

    invoke-static {v0}, LX/0rq;->a(LX/0QB;)LX/0rq;

    move-result-object v5

    check-cast v5, LX/0rq;

    invoke-static {v0}, LX/0se;->a(LX/0QB;)LX/0se;

    move-result-object v6

    check-cast v6, LX/0se;

    invoke-direct {p0, v3, v4, v5, v6}, LX/BUF;-><init>(LX/0tX;LX/0sa;LX/0rq;LX/0se;)V

    .line 1788754
    move-object v0, p0

    .line 1788755
    sput-object v0, LX/BUF;->j:LX/BUF;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1788756
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1788757
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1788758
    :cond_1
    sget-object v0, LX/BUF;->j:LX/BUF;

    return-object v0

    .line 1788759
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1788760
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static synthetic d(LX/BUF;I)I
    .locals 1

    .prologue
    .line 1788747
    iget v0, p0, LX/BUF;->f:I

    mul-int/2addr v0, p1

    iput v0, p0, LX/BUF;->f:I

    return v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/BTr;)V
    .locals 2

    .prologue
    .line 1788739
    new-instance v0, LX/BUb;

    invoke-direct {v0}, LX/BUb;-><init>()V

    move-object v0, v0

    .line 1788740
    const-string v1, "video_id"

    invoke-virtual {v0, v1, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1788741
    sget-object v1, LX/0zS;->c:LX/0zS;

    .line 1788742
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    const/4 v1, 0x1

    .line 1788743
    iput-boolean v1, v0, LX/0zO;->p:Z

    .line 1788744
    move-object v0, v0

    .line 1788745
    iget-object v1, p0, LX/BUF;->b:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    invoke-static {v0}, LX/0tX;->b(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, LX/BUE;

    invoke-direct {v1, p0, p2, p1}, LX/BUE;-><init>(LX/BUF;LX/BTr;Ljava/lang/String;)V

    invoke-static {v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 1788746
    return-void
.end method

.method public final declared-synchronized a(Ljava/util/List;LX/BTq;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/video/downloadmanager/OfflineVideoInfoFetcher$StoryFromVideoIdsQueryListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1788726
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/BUF;->i:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 1788727
    :goto_0
    monitor-exit p0

    return-void

    .line 1788728
    :cond_0
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, LX/BUF;->i:Z

    .line 1788729
    new-instance v0, LX/BUS;

    invoke-direct {v0}, LX/BUS;-><init>()V

    move-object v0, v0

    .line 1788730
    const-string v1, "profile_image_size"

    invoke-static {}, LX/0sa;->a()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1788731
    iget-object v1, p0, LX/BUF;->e:LX/0se;

    iget-object v2, p0, LX/BUF;->d:LX/0rq;

    invoke-virtual {v2}, LX/0rq;->c()LX/0wF;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/0se;->a(LX/0gW;LX/0wF;)LX/0gW;

    .line 1788732
    const-string v1, "video_ids"

    invoke-virtual {v0, v1, p1}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0gW;

    .line 1788733
    sget-object v1, LX/0zS;->c:LX/0zS;

    .line 1788734
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    const/4 v1, 0x1

    .line 1788735
    iput-boolean v1, v0, LX/0zO;->p:Z

    .line 1788736
    move-object v0, v0

    .line 1788737
    iget-object v1, p0, LX/BUF;->b:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    invoke-static {v0}, LX/0tX;->d(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, LX/BUD;

    invoke-direct {v1, p0, p2, p1}, LX/BUD;-><init>(LX/BUF;LX/BTq;Ljava/util/List;)V

    invoke-static {v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1788738
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
