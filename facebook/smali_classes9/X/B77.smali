.class public final enum LX/B77;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/B77;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/B77;

.field public static final enum INLINE_SELECT_FIELD_ERROR:LX/B77;

.field public static final enum NO_ERROR:LX/B77;

.field public static final enum PRIVACY_CHECKBOX_ERROR:LX/B77;

.field public static final enum REQUIRED_CHECKBOX_ERROR:LX/B77;

.field public static final enum SELECT_FIELD_ERROR:LX/B77;

.field public static final enum TEXT_FIELD_ERROR:LX/B77;

.field public static final enum UNKNOWN_ERROR:LX/B77;


# instance fields
.field public final fieldInputType:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 1747709
    new-instance v0, LX/B77;

    const-string v1, "SELECT_FIELD_ERROR"

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;->SELECT:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

    invoke-direct {v0, v1, v4, v2}, LX/B77;-><init>(Ljava/lang/String;ILcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;)V

    sput-object v0, LX/B77;->SELECT_FIELD_ERROR:LX/B77;

    .line 1747710
    new-instance v0, LX/B77;

    const-string v1, "INLINE_SELECT_FIELD_ERROR"

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;->INLINE_SELECT:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

    invoke-direct {v0, v1, v5, v2}, LX/B77;-><init>(Ljava/lang/String;ILcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;)V

    sput-object v0, LX/B77;->INLINE_SELECT_FIELD_ERROR:LX/B77;

    .line 1747711
    new-instance v0, LX/B77;

    const-string v1, "TEXT_FIELD_ERROR"

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;->TEXT:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

    invoke-direct {v0, v1, v6, v2}, LX/B77;-><init>(Ljava/lang/String;ILcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;)V

    sput-object v0, LX/B77;->TEXT_FIELD_ERROR:LX/B77;

    .line 1747712
    new-instance v0, LX/B77;

    const-string v1, "PRIVACY_CHECKBOX_ERROR"

    invoke-direct {v0, v1, v7, v3}, LX/B77;-><init>(Ljava/lang/String;ILcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;)V

    sput-object v0, LX/B77;->PRIVACY_CHECKBOX_ERROR:LX/B77;

    .line 1747713
    new-instance v0, LX/B77;

    const-string v1, "REQUIRED_CHECKBOX_ERROR"

    const/4 v2, 0x4

    invoke-direct {v0, v1, v2, v3}, LX/B77;-><init>(Ljava/lang/String;ILcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;)V

    sput-object v0, LX/B77;->REQUIRED_CHECKBOX_ERROR:LX/B77;

    .line 1747714
    new-instance v0, LX/B77;

    const-string v1, "NO_ERROR"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2, v3}, LX/B77;-><init>(Ljava/lang/String;ILcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;)V

    sput-object v0, LX/B77;->NO_ERROR:LX/B77;

    .line 1747715
    new-instance v0, LX/B77;

    const-string v1, "UNKNOWN_ERROR"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2, v3}, LX/B77;-><init>(Ljava/lang/String;ILcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;)V

    sput-object v0, LX/B77;->UNKNOWN_ERROR:LX/B77;

    .line 1747716
    const/4 v0, 0x7

    new-array v0, v0, [LX/B77;

    sget-object v1, LX/B77;->SELECT_FIELD_ERROR:LX/B77;

    aput-object v1, v0, v4

    sget-object v1, LX/B77;->INLINE_SELECT_FIELD_ERROR:LX/B77;

    aput-object v1, v0, v5

    sget-object v1, LX/B77;->TEXT_FIELD_ERROR:LX/B77;

    aput-object v1, v0, v6

    sget-object v1, LX/B77;->PRIVACY_CHECKBOX_ERROR:LX/B77;

    aput-object v1, v0, v7

    const/4 v1, 0x4

    sget-object v2, LX/B77;->REQUIRED_CHECKBOX_ERROR:LX/B77;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, LX/B77;->NO_ERROR:LX/B77;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/B77;->UNKNOWN_ERROR:LX/B77;

    aput-object v2, v0, v1

    sput-object v0, LX/B77;->$VALUES:[LX/B77;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1747717
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1747718
    iput-object p3, p0, LX/B77;->fieldInputType:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

    .line 1747719
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/B77;
    .locals 1

    .prologue
    .line 1747720
    const-class v0, LX/B77;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/B77;

    return-object v0
.end method

.method public static values()[LX/B77;
    .locals 1

    .prologue
    .line 1747721
    sget-object v0, LX/B77;->$VALUES:[LX/B77;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/B77;

    return-object v0
.end method


# virtual methods
.method public final isValid()Z
    .locals 1

    .prologue
    .line 1747722
    sget-object v0, LX/B77;->NO_ERROR:LX/B77;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
