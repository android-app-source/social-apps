.class public LX/BUp;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1789606
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/facebook/graphql/model/GraphQLActor;Lcom/facebook/graphql/model/GraphQLMedia;LX/Aek;)V
    .locals 5
    .param p2    # Lcom/facebook/graphql/model/GraphQLMedia;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1789607
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLActor;->aC()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLActor;->aE()Z

    move-result v0

    if-nez v0, :cond_2

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLMedia;->bh()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 1789608
    if-nez v0, :cond_1

    .line 1789609
    :goto_1
    return-void

    .line 1789610
    :cond_1
    new-instance v0, LX/BUo;

    invoke-direct {v0}, LX/BUo;-><init>()V

    .line 1789611
    iput-object p0, v0, LX/BUo;->a:Landroid/content/Context;

    .line 1789612
    move-object v0, v0

    .line 1789613
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v1

    .line 1789614
    iput-object v1, v0, LX/BUo;->b:Ljava/lang/String;

    .line 1789615
    move-object v0, v0

    .line 1789616
    const-wide/16 v2, 0x1388

    .line 1789617
    iput-wide v2, v0, LX/BUo;->c:J

    .line 1789618
    move-object v0, v0

    .line 1789619
    iput-object p3, v0, LX/BUo;->d:LX/Aek;

    .line 1789620
    move-object v0, v0

    .line 1789621
    invoke-virtual {v0}, LX/BUo;->a()LX/3Ag;

    move-result-object v0

    invoke-virtual {v0}, LX/3Ag;->show()V

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
