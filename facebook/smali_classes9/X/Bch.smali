.class public final LX/Bch;
.super LX/BcO;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/BcO",
        "<",
        "LX/Bck;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public b:Landroid/os/Handler;

.field public c:LX/2kW;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/controller/connectioncontroller/common/ConnectionController",
            "<TTEdge;TTUserInfo;>;"
        }
    .end annotation
.end field

.field public d:I

.field public e:I

.field public f:LX/Bcm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/Bcm",
            "<TTEdge;>;"
        }
    .end annotation
.end field

.field public g:LX/Exd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/components/list/fb/datasources/ImpressionLogger",
            "<TTEdge;>;"
        }
    .end annotation
.end field

.field public h:I

.field public i:LX/0YT;

.field public j:Ljava/lang/String;

.field public k:LX/Bcp;

.field public l:LX/BcQ;

.field public final synthetic m:LX/Bck;


# direct methods
.method public constructor <init>(LX/Bck;)V
    .locals 1

    .prologue
    .line 1802488
    iput-object p1, p0, LX/Bch;->m:LX/Bck;

    .line 1802489
    invoke-static {}, LX/Bck;->d()LX/Bck;

    move-result-object v0

    invoke-direct {p0, v0}, LX/BcO;-><init>(LX/BcS;)V

    .line 1802490
    const/16 v0, 0xa

    iput v0, p0, LX/Bch;->d:I

    .line 1802491
    const/4 v0, 0x0

    iput v0, p0, LX/Bch;->e:I

    .line 1802492
    const/4 v0, 0x5

    iput v0, p0, LX/Bch;->h:I

    .line 1802493
    return-void
.end method


# virtual methods
.method public final a(Z)LX/BcO;
    .locals 2

    .prologue
    .line 1802494
    const/4 v1, 0x0

    .line 1802495
    invoke-super {p0, p1}, LX/BcO;->a(Z)LX/BcO;

    move-result-object v0

    check-cast v0, LX/Bch;

    .line 1802496
    if-nez p1, :cond_0

    .line 1802497
    iput-object v1, v0, LX/Bch;->b:Landroid/os/Handler;

    .line 1802498
    iput-object v1, v0, LX/Bch;->f:LX/Bcm;

    .line 1802499
    iput-object v1, v0, LX/Bch;->i:LX/0YT;

    .line 1802500
    iput-object v1, v0, LX/Bch;->j:Ljava/lang/String;

    .line 1802501
    :cond_0
    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1802502
    if-ne p0, p1, :cond_1

    .line 1802503
    :cond_0
    :goto_0
    return v0

    .line 1802504
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1802505
    goto :goto_0

    .line 1802506
    :cond_3
    check-cast p1, LX/Bch;

    .line 1802507
    iget-object v2, p0, LX/Bch;->b:Landroid/os/Handler;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/Bch;->b:Landroid/os/Handler;

    iget-object v3, p1, LX/Bch;->b:Landroid/os/Handler;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1802508
    goto :goto_0

    .line 1802509
    :cond_5
    iget-object v2, p1, LX/Bch;->b:Landroid/os/Handler;

    if-nez v2, :cond_4

    .line 1802510
    :cond_6
    iget-object v2, p0, LX/Bch;->c:LX/2kW;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/Bch;->c:LX/2kW;

    iget-object v3, p1, LX/Bch;->c:LX/2kW;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1802511
    goto :goto_0

    .line 1802512
    :cond_8
    iget-object v2, p1, LX/Bch;->c:LX/2kW;

    if-nez v2, :cond_7

    .line 1802513
    :cond_9
    iget v2, p0, LX/Bch;->d:I

    iget v3, p1, LX/Bch;->d:I

    if-eq v2, v3, :cond_a

    move v0, v1

    .line 1802514
    goto :goto_0

    .line 1802515
    :cond_a
    iget v2, p0, LX/Bch;->e:I

    iget v3, p1, LX/Bch;->e:I

    if-eq v2, v3, :cond_b

    move v0, v1

    .line 1802516
    goto :goto_0

    .line 1802517
    :cond_b
    iget-object v2, p0, LX/Bch;->f:LX/Bcm;

    if-eqz v2, :cond_d

    iget-object v2, p0, LX/Bch;->f:LX/Bcm;

    iget-object v3, p1, LX/Bch;->f:LX/Bcm;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_e

    :cond_c
    move v0, v1

    .line 1802518
    goto :goto_0

    .line 1802519
    :cond_d
    iget-object v2, p1, LX/Bch;->f:LX/Bcm;

    if-nez v2, :cond_c

    .line 1802520
    :cond_e
    iget-object v2, p0, LX/Bch;->g:LX/Exd;

    if-eqz v2, :cond_10

    iget-object v2, p0, LX/Bch;->g:LX/Exd;

    iget-object v3, p1, LX/Bch;->g:LX/Exd;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_11

    :cond_f
    move v0, v1

    .line 1802521
    goto :goto_0

    .line 1802522
    :cond_10
    iget-object v2, p1, LX/Bch;->g:LX/Exd;

    if-nez v2, :cond_f

    .line 1802523
    :cond_11
    iget v2, p0, LX/Bch;->h:I

    iget v3, p1, LX/Bch;->h:I

    if-eq v2, v3, :cond_12

    move v0, v1

    .line 1802524
    goto :goto_0

    .line 1802525
    :cond_12
    iget-object v2, p0, LX/Bch;->i:LX/0YT;

    if-eqz v2, :cond_14

    iget-object v2, p0, LX/Bch;->i:LX/0YT;

    iget-object v3, p1, LX/Bch;->i:LX/0YT;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_15

    :cond_13
    move v0, v1

    .line 1802526
    goto/16 :goto_0

    .line 1802527
    :cond_14
    iget-object v2, p1, LX/Bch;->i:LX/0YT;

    if-nez v2, :cond_13

    .line 1802528
    :cond_15
    iget-object v2, p0, LX/Bch;->j:Ljava/lang/String;

    if-eqz v2, :cond_16

    iget-object v2, p0, LX/Bch;->j:Ljava/lang/String;

    iget-object v3, p1, LX/Bch;->j:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1802529
    goto/16 :goto_0

    .line 1802530
    :cond_16
    iget-object v2, p1, LX/Bch;->j:Ljava/lang/String;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
