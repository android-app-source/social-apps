.class public final LX/CDm;
.super LX/451;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/451",
        "<",
        "Lcom/facebook/graphql/model/GraphQLFeedback;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/auth/viewercontext/ViewerContext;

.field public final synthetic b:LX/3iF;


# direct methods
.method public constructor <init>(LX/3iF;Lcom/facebook/auth/viewercontext/ViewerContext;)V
    .locals 0

    .prologue
    .line 1860112
    iput-object p1, p0, LX/CDm;->b:LX/3iF;

    iput-object p2, p0, LX/CDm;->a:Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-direct {p0}, LX/451;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 1860097
    check-cast p1, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1860098
    if-nez p1, :cond_0

    .line 1860099
    iget-object v0, p0, LX/CDm;->b:LX/3iF;

    iget-object v0, v0, LX/3iF;->i:LX/03V;

    sget-object v1, LX/3iF;->a:Ljava/lang/String;

    const-string v2, "Fetched feedback is null!"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1860100
    iget-object v0, p0, LX/CDm;->b:LX/3iF;

    .line 1860101
    invoke-static {v0}, LX/3iF;->e$redex0(LX/3iF;)V

    .line 1860102
    :goto_0
    return-void

    .line 1860103
    :cond_0
    iget-object v0, p0, LX/CDm;->b:LX/3iF;

    iget-object v0, v0, LX/3iF;->o:LX/1Ps;

    check-cast v0, LX/1Pq;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v2, 0x0

    iget-object v3, p0, LX/CDm;->b:LX/3iF;

    iget-object v3, v3, LX/3iF;->m:Lcom/facebook/feed/rows/core/props/FeedProps;

    aput-object v3, v1, v2

    invoke-interface {v0, v1}, LX/1Pq;->a([Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1860104
    iget-object v0, p0, LX/CDm;->b:LX/3iF;

    iget-object v0, v0, LX/3iF;->m:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1860105
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 1860106
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0, p1}, LX/189;->a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 1860107
    iget-object v1, p0, LX/CDm;->b:LX/3iF;

    iget-object v1, v1, LX/3iF;->n:LX/189;

    iget-object v2, p0, LX/CDm;->b:LX/3iF;

    iget-object v2, v2, LX/3iF;->m:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v1, v0, v2}, LX/189;->a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1860108
    iget-object v1, p0, LX/CDm;->b:LX/3iF;

    iget-object v2, p0, LX/CDm;->a:Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-static {v1, v0, v2}, LX/3iF;->a$redex0(LX/3iF;Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/auth/viewercontext/ViewerContext;)V

    .line 1860109
    iget-object v0, p0, LX/CDm;->b:LX/3iF;

    .line 1860110
    invoke-static {v0}, LX/3iF;->e$redex0(LX/3iF;)V

    .line 1860111
    goto :goto_0
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1860092
    iget-object v0, p0, LX/CDm;->b:LX/3iF;

    invoke-static {v0, p1}, LX/3iF;->a$redex0(LX/3iF;Ljava/lang/Throwable;)V

    .line 1860093
    return-void
.end method

.method public final a(Ljava/util/concurrent/CancellationException;)V
    .locals 1

    .prologue
    .line 1860094
    invoke-super {p0, p1}, LX/451;->a(Ljava/util/concurrent/CancellationException;)V

    .line 1860095
    iget-object v0, p0, LX/CDm;->b:LX/3iF;

    invoke-static {v0, p1}, LX/3iF;->a$redex0(LX/3iF;Ljava/lang/Throwable;)V

    .line 1860096
    return-void
.end method
