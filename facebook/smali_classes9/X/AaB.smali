.class public LX/AaB;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/AaB;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public final d:LX/0if;


# direct methods
.method public constructor <init>(LX/0if;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1688061
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1688062
    iput-object p1, p0, LX/AaB;->d:LX/0if;

    .line 1688063
    return-void
.end method

.method public static a(LX/0QB;)LX/AaB;
    .locals 4

    .prologue
    .line 1688064
    sget-object v0, LX/AaB;->e:LX/AaB;

    if-nez v0, :cond_1

    .line 1688065
    const-class v1, LX/AaB;

    monitor-enter v1

    .line 1688066
    :try_start_0
    sget-object v0, LX/AaB;->e:LX/AaB;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1688067
    if-eqz v2, :cond_0

    .line 1688068
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1688069
    new-instance p0, LX/AaB;

    invoke-static {v0}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v3

    check-cast v3, LX/0if;

    invoke-direct {p0, v3}, LX/AaB;-><init>(LX/0if;)V

    .line 1688070
    move-object v0, p0

    .line 1688071
    sput-object v0, LX/AaB;->e:LX/AaB;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1688072
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1688073
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1688074
    :cond_1
    sget-object v0, LX/AaB;->e:LX/AaB;

    return-object v0

    .line 1688075
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1688076
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static i(LX/AaB;)V
    .locals 2

    .prologue
    .line 1688077
    iget-object v0, p0, LX/AaB;->d:LX/0if;

    sget-object v1, LX/0ig;->E:LX/0ih;

    invoke-virtual {v0, v1}, LX/0if;->c(LX/0ih;)V

    .line 1688078
    return-void
.end method
