.class public LX/BSt;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private c:LX/0gc;

.field private d:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1786230
    const-class v0, LX/BSt;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/BSt;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0gc;)V
    .locals 2

    .prologue
    .line 1786202
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1786203
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, LX/BSt;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 1786204
    new-instance v0, Ljava/lang/ref/WeakReference;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/BSt;->d:Ljava/lang/ref/WeakReference;

    .line 1786205
    iput-object p1, p0, LX/BSt;->c:LX/0gc;

    .line 1786206
    return-void
.end method


# virtual methods
.method public final a(LX/ATX;)V
    .locals 3

    .prologue
    .line 1786225
    new-instance v0, Ljava/lang/ref/WeakReference;

    iget-object v1, p0, LX/BSt;->c:LX/0gc;

    sget-object v2, LX/BSt;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/BSt;->d:Ljava/lang/ref/WeakReference;

    .line 1786226
    iget-object v0, p0, LX/BSt;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1786227
    iget-object v0, p0, LX/BSt;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;

    .line 1786228
    iput-object p1, v0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->s:LX/ATX;

    .line 1786229
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;Landroid/net/Uri;LX/ATX;Ljava/lang/String;LX/9fh;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1786207
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1786208
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1786209
    iget-object v0, p0, LX/BSt;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1786210
    iget-object v0, p0, LX/BSt;->c:LX/0gc;

    sget-object v1, LX/BSt;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1786211
    iget-object v0, p0, LX/BSt;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v6}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 1786212
    :cond_0
    :goto_0
    return-void

    .line 1786213
    :cond_1
    new-instance v0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;

    invoke-direct {v0}, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;-><init>()V

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p5

    move-object v5, p4

    .line 1786214
    iput-object v1, v0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->o:Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;

    .line 1786215
    iput-object v2, v0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->n:Landroid/net/Uri;

    .line 1786216
    iput-object v3, v0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->s:LX/ATX;

    .line 1786217
    iput-object v4, v0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->y:LX/9fh;

    .line 1786218
    iput-object v5, v0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->t:Ljava/lang/String;

    .line 1786219
    iget-object p1, v1, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;->q:Ljava/lang/String;

    move-object p1, p1

    .line 1786220
    iput-object p1, v0, Lcom/facebook/video/creativeediting/VideoEditGalleryFragment;->u:Ljava/lang/String;

    .line 1786221
    iget-object v1, p0, LX/BSt;->c:LX/0gc;

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    sget-object v2, LX/BSt;->a:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, LX/0hH;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v1

    invoke-virtual {v1}, LX/0hH;->b()I

    .line 1786222
    iget-object v1, p0, LX/BSt;->c:LX/0gc;

    invoke-virtual {v1}, LX/0gc;->b()Z

    .line 1786223
    iget-object v1, p0, LX/BSt;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1, v6}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 1786224
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v1, p0, LX/BSt;->d:Ljava/lang/ref/WeakReference;

    goto :goto_0
.end method
