.class public LX/B8r;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private a:LX/3Lz;


# direct methods
.method public constructor <init>(LX/3Lz;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1750650
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1750651
    iput-object p1, p0, LX/B8r;->a:LX/3Lz;

    .line 1750652
    return-void
.end method

.method public static a(LX/0QB;)LX/B8r;
    .locals 4

    .prologue
    .line 1750653
    const-class v1, LX/B8r;

    monitor-enter v1

    .line 1750654
    :try_start_0
    sget-object v0, LX/B8r;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1750655
    sput-object v2, LX/B8r;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1750656
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1750657
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1750658
    new-instance p0, LX/B8r;

    invoke-static {v0}, LX/3Ly;->a(LX/0QB;)LX/3Lz;

    move-result-object v3

    check-cast v3, LX/3Lz;

    invoke-direct {p0, v3}, LX/B8r;-><init>(LX/3Lz;)V

    .line 1750659
    move-object v0, p0

    .line 1750660
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1750661
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/B8r;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1750662
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1750663
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/B8s;Lcom/facebook/common/locale/Country;)Z
    .locals 3
    .param p1    # Lcom/facebook/common/locale/Country;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 1750664
    iget-object v1, p0, LX/B8s;->b:Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;

    move-object v1, v1

    .line 1750665
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->k()Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;->ZIPCODE:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;

    if-ne v1, v2, :cond_2

    if-eqz p1, :cond_2

    .line 1750666
    iget-object v1, p0, LX/B8s;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1750667
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 1750668
    invoke-static {v1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1750669
    :cond_0
    :goto_0
    return v0

    .line 1750670
    :cond_1
    invoke-static {v1, p1}, LX/46H;->a(Ljava/lang/CharSequence;Lcom/facebook/common/locale/Country;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1750671
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/B8s;)Z
    .locals 2

    .prologue
    .line 1750672
    iget-object v0, p1, LX/B8s;->a:Ljava/lang/String;

    move-object v0, v0

    .line 1750673
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 1750674
    iget-object v1, p1, LX/B8s;->b:Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;

    move-object v1, v1

    .line 1750675
    invoke-virtual {p0, v0, v1}, LX/B8r;->a(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;)Z

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1750676
    invoke-static {p1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Landroid/util/Patterns;->PHONE:Ljava/util/regex/Pattern;

    invoke-virtual {v1, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->matches()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1750677
    :cond_0
    :goto_0
    return v0

    .line 1750678
    :cond_1
    :try_start_0
    iget-object v1, p0, LX/B8r;->a:LX/3Lz;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, LX/3Lz;->parse(Ljava/lang/String;Ljava/lang/String;)LX/4hT;

    move-result-object v1

    .line 1750679
    iget-object v2, p0, LX/B8r;->a:LX/3Lz;

    invoke-virtual {v2, v1}, LX/3Lz;->isValidNumber(LX/4hT;)Z
    :try_end_0
    .catch LX/4hE; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-eqz v1, :cond_0

    .line 1750680
    const/4 v0, 0x1

    goto :goto_0

    .line 1750681
    :catch_0
    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1750682
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->n()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1750683
    :cond_0
    :goto_0
    return v0

    .line 1750684
    :cond_1
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->o()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    move v0, v1

    .line 1750685
    goto :goto_0

    .line 1750686
    :cond_2
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->k()Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;->EMAIL:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;

    if-ne v2, v3, :cond_4

    const/4 v5, 0x2

    const/4 v2, 0x0

    .line 1750687
    sget-object v3, Landroid/util/Patterns;->EMAIL_ADDRESS:Ljava/util/regex/Pattern;

    invoke-virtual {v3, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/regex/Matcher;->matches()Z

    move-result v3

    if-nez v3, :cond_5

    .line 1750688
    :cond_3
    :goto_1
    move v2, v2

    .line 1750689
    if-nez v2, :cond_4

    move v0, v1

    .line 1750690
    goto :goto_0

    .line 1750691
    :cond_4
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->k()Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;->PHONE:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputDomain;

    if-ne v2, v3, :cond_0

    invoke-virtual {p0, p1}, LX/B8r;->a(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1750692
    goto :goto_0

    .line 1750693
    :cond_5
    const-string v3, "@"

    invoke-virtual {p1, v3, v5}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v3

    .line 1750694
    array-length v4, v3

    if-ne v4, v5, :cond_3

    aget-object v3, v3, v2

    const-string v4, "."

    invoke-virtual {v3, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 1750695
    const/4 v2, 0x1

    goto :goto_1
.end method
