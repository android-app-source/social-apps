.class public final enum LX/AXx;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/AXx;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/AXx;

.field public static final enum LIVE_STREAM:LX/AXx;

.field public static final enum LOCAL_VIDEO:LX/AXx;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1684677
    new-instance v0, LX/AXx;

    const-string v1, "LIVE_STREAM"

    invoke-direct {v0, v1, v2}, LX/AXx;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AXx;->LIVE_STREAM:LX/AXx;

    .line 1684678
    new-instance v0, LX/AXx;

    const-string v1, "LOCAL_VIDEO"

    invoke-direct {v0, v1, v3}, LX/AXx;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AXx;->LOCAL_VIDEO:LX/AXx;

    .line 1684679
    const/4 v0, 0x2

    new-array v0, v0, [LX/AXx;

    sget-object v1, LX/AXx;->LIVE_STREAM:LX/AXx;

    aput-object v1, v0, v2

    sget-object v1, LX/AXx;->LOCAL_VIDEO:LX/AXx;

    aput-object v1, v0, v3

    sput-object v0, LX/AXx;->$VALUES:[LX/AXx;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1684680
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static getEncoderSurfaceType(I)LX/AXx;
    .locals 1

    .prologue
    .line 1684681
    invoke-static {}, LX/AXx;->values()[LX/AXx;

    move-result-object v0

    aget-object v0, v0, p0

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/AXx;
    .locals 1

    .prologue
    .line 1684682
    const-class v0, LX/AXx;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/AXx;

    return-object v0
.end method

.method public static values()[LX/AXx;
    .locals 1

    .prologue
    .line 1684683
    sget-object v0, LX/AXx;->$VALUES:[LX/AXx;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/AXx;

    return-object v0
.end method


# virtual methods
.method public final toInt()I
    .locals 1

    .prologue
    .line 1684684
    invoke-virtual {p0}, LX/AXx;->ordinal()I

    move-result v0

    return v0
.end method
