.class public LX/BYp;
.super LX/BYo;
.source ""


# instance fields
.field private a:F

.field private b:Z

.field private c:Z

.field private d:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1796610
    invoke-direct {p0}, LX/BYo;-><init>()V

    return-void
.end method


# virtual methods
.method public getDoubleSided()Z
    .locals 1

    .prologue
    .line 1796601
    iget-boolean v0, p0, LX/BYp;->d:Z

    return v0
.end method

.method public getReadDepth()Z
    .locals 1

    .prologue
    .line 1796609
    iget-boolean v0, p0, LX/BYp;->b:Z

    return v0
.end method

.method public getTransparency()F
    .locals 1

    .prologue
    .line 1796608
    iget v0, p0, LX/BYp;->a:F

    return v0
.end method

.method public getWriteDepth()Z
    .locals 1

    .prologue
    .line 1796611
    iget-boolean v0, p0, LX/BYp;->c:Z

    return v0
.end method

.method public setDoubleSided(Z)V
    .locals 0

    .prologue
    .line 1796606
    iput-boolean p1, p0, LX/BYp;->d:Z

    .line 1796607
    return-void
.end method

.method public setReadDepth(Z)V
    .locals 0

    .prologue
    .line 1796604
    iput-boolean p1, p0, LX/BYp;->b:Z

    .line 1796605
    return-void
.end method

.method public setTransparency(F)V
    .locals 0

    .prologue
    .line 1796602
    iput p1, p0, LX/BYp;->a:F

    .line 1796603
    return-void
.end method

.method public setWriteDepth(Z)V
    .locals 0

    .prologue
    .line 1796599
    iput-boolean p1, p0, LX/BYp;->c:Z

    .line 1796600
    return-void
.end method
