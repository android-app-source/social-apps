.class public final LX/C6u;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/C6v;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;

.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public c:Ljava/lang/String;

.field public d:Z

.field public e:LX/C6a;

.field public f:LX/C6H;

.field public g:Ljava/lang/String;

.field public h:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;"
        }
    .end annotation
.end field

.field public i:LX/1Pq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public final synthetic j:LX/C6v;


# direct methods
.method public constructor <init>(LX/C6v;)V
    .locals 1

    .prologue
    .line 1850544
    iput-object p1, p0, LX/C6u;->j:LX/C6v;

    .line 1850545
    move-object v0, p1

    .line 1850546
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1850547
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1850548
    const-string v0, "InlineSurveyQuestionComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1850549
    if-ne p0, p1, :cond_1

    .line 1850550
    :cond_0
    :goto_0
    return v0

    .line 1850551
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1850552
    goto :goto_0

    .line 1850553
    :cond_3
    check-cast p1, LX/C6u;

    .line 1850554
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1850555
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1850556
    if-eq v2, v3, :cond_0

    .line 1850557
    iget-object v2, p0, LX/C6u;->a:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/C6u;->a:Ljava/lang/String;

    iget-object v3, p1, LX/C6u;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1850558
    goto :goto_0

    .line 1850559
    :cond_5
    iget-object v2, p1, LX/C6u;->a:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 1850560
    :cond_6
    iget-object v2, p0, LX/C6u;->b:LX/0Px;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/C6u;->b:LX/0Px;

    iget-object v3, p1, LX/C6u;->b:LX/0Px;

    invoke-virtual {v2, v3}, LX/0Px;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1850561
    goto :goto_0

    .line 1850562
    :cond_8
    iget-object v2, p1, LX/C6u;->b:LX/0Px;

    if-nez v2, :cond_7

    .line 1850563
    :cond_9
    iget-object v2, p0, LX/C6u;->c:Ljava/lang/String;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/C6u;->c:Ljava/lang/String;

    iget-object v3, p1, LX/C6u;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 1850564
    goto :goto_0

    .line 1850565
    :cond_b
    iget-object v2, p1, LX/C6u;->c:Ljava/lang/String;

    if-nez v2, :cond_a

    .line 1850566
    :cond_c
    iget-boolean v2, p0, LX/C6u;->d:Z

    iget-boolean v3, p1, LX/C6u;->d:Z

    if-eq v2, v3, :cond_d

    move v0, v1

    .line 1850567
    goto :goto_0

    .line 1850568
    :cond_d
    iget-object v2, p0, LX/C6u;->e:LX/C6a;

    if-eqz v2, :cond_f

    iget-object v2, p0, LX/C6u;->e:LX/C6a;

    iget-object v3, p1, LX/C6u;->e:LX/C6a;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_10

    :cond_e
    move v0, v1

    .line 1850569
    goto :goto_0

    .line 1850570
    :cond_f
    iget-object v2, p1, LX/C6u;->e:LX/C6a;

    if-nez v2, :cond_e

    .line 1850571
    :cond_10
    iget-object v2, p0, LX/C6u;->f:LX/C6H;

    if-eqz v2, :cond_12

    iget-object v2, p0, LX/C6u;->f:LX/C6H;

    iget-object v3, p1, LX/C6u;->f:LX/C6H;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_13

    :cond_11
    move v0, v1

    .line 1850572
    goto :goto_0

    .line 1850573
    :cond_12
    iget-object v2, p1, LX/C6u;->f:LX/C6H;

    if-nez v2, :cond_11

    .line 1850574
    :cond_13
    iget-object v2, p0, LX/C6u;->g:Ljava/lang/String;

    if-eqz v2, :cond_15

    iget-object v2, p0, LX/C6u;->g:Ljava/lang/String;

    iget-object v3, p1, LX/C6u;->g:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_16

    :cond_14
    move v0, v1

    .line 1850575
    goto/16 :goto_0

    .line 1850576
    :cond_15
    iget-object v2, p1, LX/C6u;->g:Ljava/lang/String;

    if-nez v2, :cond_14

    .line 1850577
    :cond_16
    iget-object v2, p0, LX/C6u;->h:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_18

    iget-object v2, p0, LX/C6u;->h:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/C6u;->h:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_19

    :cond_17
    move v0, v1

    .line 1850578
    goto/16 :goto_0

    .line 1850579
    :cond_18
    iget-object v2, p1, LX/C6u;->h:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_17

    .line 1850580
    :cond_19
    iget-object v2, p0, LX/C6u;->i:LX/1Pq;

    if-eqz v2, :cond_1a

    iget-object v2, p0, LX/C6u;->i:LX/1Pq;

    iget-object v3, p1, LX/C6u;->i:LX/1Pq;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1850581
    goto/16 :goto_0

    .line 1850582
    :cond_1a
    iget-object v2, p1, LX/C6u;->i:LX/1Pq;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
