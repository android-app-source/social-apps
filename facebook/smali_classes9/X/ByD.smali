.class public LX/ByD;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pt;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pq;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/ByE;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/ByD",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/ByE;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1836930
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1836931
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/ByD;->b:LX/0Zi;

    .line 1836932
    iput-object p1, p0, LX/ByD;->a:LX/0Ot;

    .line 1836933
    return-void
.end method

.method public static a(LX/0QB;)LX/ByD;
    .locals 4

    .prologue
    .line 1836934
    const-class v1, LX/ByD;

    monitor-enter v1

    .line 1836935
    :try_start_0
    sget-object v0, LX/ByD;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1836936
    sput-object v2, LX/ByD;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1836937
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1836938
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1836939
    new-instance v3, LX/ByD;

    const/16 p0, 0x1e21

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/ByD;-><init>(LX/0Ot;)V

    .line 1836940
    move-object v0, v3

    .line 1836941
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1836942
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/ByD;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1836943
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1836944
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 5

    .prologue
    .line 1836945
    check-cast p2, LX/ByC;

    .line 1836946
    iget-object v0, p0, LX/ByD;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ByE;

    iget-object v1, p2, LX/ByC;->a:LX/1aZ;

    iget v2, p2, LX/ByC;->c:F

    const/4 v4, 0x2

    .line 1836947
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v4}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v4}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v3

    .line 1836948
    const v4, -0x36125a43

    const/4 p0, 0x0

    invoke-static {p1, v4, p0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v4

    move-object v4, v4

    .line 1836949
    invoke-interface {v3, v4}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v3

    .line 1836950
    if-eqz v1, :cond_1

    .line 1836951
    iget-object v4, v0, LX/ByE;->b:LX/ByS;

    const/4 p0, 0x0

    .line 1836952
    new-instance p2, LX/ByR;

    invoke-direct {p2, v4}, LX/ByR;-><init>(LX/ByS;)V

    .line 1836953
    iget-object v0, v4, LX/ByS;->b:LX/0Zi;

    invoke-virtual {v0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ByQ;

    .line 1836954
    if-nez v0, :cond_0

    .line 1836955
    new-instance v0, LX/ByQ;

    invoke-direct {v0, v4}, LX/ByQ;-><init>(LX/ByS;)V

    .line 1836956
    :cond_0
    invoke-static {v0, p1, p0, p0, p2}, LX/ByQ;->a$redex0(LX/ByQ;LX/1De;IILX/ByR;)V

    .line 1836957
    move-object p2, v0

    .line 1836958
    move-object p0, p2

    .line 1836959
    move-object v4, p0

    .line 1836960
    iget-object p0, v4, LX/ByQ;->a:LX/ByR;

    iput-object v1, p0, LX/ByR;->a:LX/1aZ;

    .line 1836961
    iget-object p0, v4, LX/ByQ;->e:Ljava/util/BitSet;

    const/4 p2, 0x0

    invoke-virtual {p0, p2}, Ljava/util/BitSet;->set(I)V

    .line 1836962
    move-object v4, v4

    .line 1836963
    iget-object p0, v4, LX/ByQ;->a:LX/ByR;

    iput v2, p0, LX/ByR;->b:F

    .line 1836964
    iget-object p0, v4, LX/ByQ;->e:Ljava/util/BitSet;

    const/4 p2, 0x1

    invoke-virtual {p0, p2}, Ljava/util/BitSet;->set(I)V

    .line 1836965
    move-object v4, v4

    .line 1836966
    invoke-virtual {v4}, LX/1X5;->d()LX/1X1;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1X1;)LX/1Dh;

    .line 1836967
    :goto_0
    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 1836968
    return-object v0

    .line 1836969
    :cond_1
    const/4 v4, 0x5

    invoke-interface {v3, v4}, LX/1Dh;->K(I)LX/1Dh;

    move-result-object v4

    const/high16 p0, 0x3f800000    # 1.0f

    invoke-interface {v4, p0}, LX/1Dh;->d(F)LX/1Dh;

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 1836970
    invoke-static {}, LX/1dS;->b()V

    .line 1836971
    iget v0, p1, LX/1dQ;->b:I

    .line 1836972
    packed-switch v0, :pswitch_data_0

    .line 1836973
    :goto_0
    return-object v2

    .line 1836974
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 1836975
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1836976
    check-cast v1, LX/ByC;

    .line 1836977
    iget-object v3, p0, LX/ByD;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/ByE;

    iget-object v4, v1, LX/ByC;->e:LX/1Pt;

    iget-object p1, v1, LX/ByC;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object p2, v1, LX/ByC;->d:Ljava/lang/String;

    .line 1836978
    iget-object p0, v3, LX/ByE;->a:LX/38w;

    check-cast v4, LX/1Pq;

    invoke-virtual {p0, v0, v4, p1, p2}, LX/38w;->a(Landroid/view/View;LX/1Pq;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;)V

    .line 1836979
    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x36125a43
        :pswitch_0
    .end packed-switch
.end method
