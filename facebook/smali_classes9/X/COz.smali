.class public final LX/COz;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/COz;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/COx;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/CP0;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1883852
    const/4 v0, 0x0

    sput-object v0, LX/COz;->a:LX/COz;

    .line 1883853
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/COz;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1883849
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1883850
    new-instance v0, LX/CP0;

    invoke-direct {v0}, LX/CP0;-><init>()V

    iput-object v0, p0, LX/COz;->c:LX/CP0;

    .line 1883851
    return-void
.end method

.method public static declared-synchronized q()LX/COz;
    .locals 2

    .prologue
    .line 1883845
    const-class v1, LX/COz;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/COz;->a:LX/COz;

    if-nez v0, :cond_0

    .line 1883846
    new-instance v0, LX/COz;

    invoke-direct {v0}, LX/COz;-><init>()V

    sput-object v0, LX/COz;->a:LX/COz;

    .line 1883847
    :cond_0
    sget-object v0, LX/COz;->a:LX/COz;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 1883848
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1883801
    invoke-static {}, LX/1dS;->b()V

    .line 1883802
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/1De;LX/1Dg;IILX/1no;LX/1X1;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x22a4c3bb

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1883837
    check-cast p6, LX/COy;

    .line 1883838
    iget-object v1, p6, LX/COy;->a:LX/COu;

    const/4 p0, 0x0

    .line 1883839
    invoke-virtual {v1}, LX/COu;->e()I

    move-result v2

    if-nez v2, :cond_0

    .line 1883840
    iput p0, p5, LX/1no;->b:I

    .line 1883841
    iput p0, p5, LX/1no;->a:I

    .line 1883842
    :goto_0
    const/16 v1, 0x1f

    const v2, 0x2fbc3146

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1883843
    :cond_0
    invoke-virtual {v1, p1, p0}, LX/3mY;->a(LX/1De;I)LX/1X1;

    move-result-object v2

    invoke-static {p1, v2}, LX/1cy;->a(LX/1De;LX/1X1;)LX/1me;

    move-result-object v2

    invoke-virtual {v2}, LX/1me;->b()LX/1dV;

    move-result-object v2

    .line 1883844
    invoke-virtual {v2, p3, p4, p5}, LX/1dV;->a(IILX/1no;)V

    goto :goto_0
.end method

.method public final a(LX/1De;LX/1Dg;LX/1X1;)V
    .locals 1

    .prologue
    .line 1883833
    check-cast p3, LX/COy;

    .line 1883834
    iget-object v0, p3, LX/COy;->a:LX/COu;

    .line 1883835
    invoke-virtual {p2}, LX/1Dg;->c()I

    move-result p0

    invoke-virtual {p2}, LX/1Dg;->d()I

    move-result p1

    invoke-virtual {v0, p0, p1}, LX/3mY;->b(II)V

    .line 1883836
    return-void
.end method

.method public final b(LX/1De;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1883831
    new-instance v0, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    invoke-direct {v0, p1}, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;-><init>(Landroid/content/Context;)V

    move-object v0, v0

    .line 1883832
    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1883854
    const/4 v0, 0x1

    return v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 1883830
    const/4 v0, 0x1

    return v0
.end method

.method public final e(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 6

    .prologue
    .line 1883821
    check-cast p3, LX/COy;

    move-object v0, p2

    .line 1883822
    check-cast v0, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    iget-object v1, p3, LX/COy;->a:LX/COu;

    iget v2, p3, LX/COy;->b:I

    iget-boolean v3, p3, LX/COy;->c:Z

    iget-object v4, p3, LX/COy;->d:LX/3x6;

    iget-boolean v5, p3, LX/COy;->e:Z

    .line 1883823
    invoke-virtual {v0, v3}, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;->setClipToPadding(Z)V

    .line 1883824
    invoke-static {v0, v2}, LX/0vv;->c(Landroid/view/View;I)V

    .line 1883825
    if-eqz v4, :cond_0

    .line 1883826
    invoke-virtual {v0, v4}, Landroid/support/v7/widget/RecyclerView;->a(LX/3x6;)V

    .line 1883827
    :cond_0
    invoke-virtual {v0, v5}, LX/25R;->setSnappingEnabled(Z)V

    .line 1883828
    invoke-virtual {v1, v0}, LX/3mY;->a(Landroid/view/ViewGroup;)V

    .line 1883829
    return-void
.end method

.method public final f()LX/1mv;
    .locals 1

    .prologue
    .line 1883820
    sget-object v0, LX/1mv;->VIEW:LX/1mv;

    return-object v0
.end method

.method public final f(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 2

    .prologue
    .line 1883812
    check-cast p3, LX/COy;

    .line 1883813
    check-cast p2, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    iget-object v0, p3, LX/COy;->a:LX/COu;

    iget-object v1, p3, LX/COy;->d:LX/3x6;

    const/4 p0, 0x1

    .line 1883814
    invoke-virtual {v0, p2}, LX/3mY;->d(Landroid/view/ViewGroup;)V

    .line 1883815
    if-eqz v1, :cond_0

    .line 1883816
    invoke-virtual {p2, v1}, Landroid/support/v7/widget/RecyclerView;->b(LX/3x6;)V

    .line 1883817
    :cond_0
    invoke-static {p2, p0}, LX/0vv;->c(Landroid/view/View;I)V

    .line 1883818
    invoke-virtual {p2, p0}, LX/25R;->setSnappingEnabled(Z)V

    .line 1883819
    return-void
.end method

.method public final g(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 1

    .prologue
    .line 1883808
    check-cast p3, LX/COy;

    .line 1883809
    check-cast p2, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    iget-object v0, p3, LX/COy;->a:LX/COu;

    .line 1883810
    invoke-virtual {v0, p2}, LX/3mY;->b(Landroid/view/ViewGroup;)V

    .line 1883811
    return-void
.end method

.method public final h(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 1

    .prologue
    .line 1883804
    check-cast p3, LX/COy;

    .line 1883805
    check-cast p2, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    iget-object v0, p3, LX/COy;->a:LX/COu;

    .line 1883806
    invoke-virtual {v0, p2}, LX/3mY;->c(Landroid/view/ViewGroup;)V

    .line 1883807
    return-void
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 1883803
    const/16 v0, 0xf

    return v0
.end method
