.class public final LX/Btu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3It;


# instance fields
.field public final synthetic a:Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;)V
    .locals 0

    .prologue
    .line 1829615
    iput-object p1, p0, LX/Btu;->a:Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 10

    .prologue
    .line 1829616
    iget-object v0, p0, LX/Btu;->a:Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

    iget-object v0, v0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->aq:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/D7p;

    iget-object v1, p0, LX/Btu;->a:Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

    iget-object v1, v1, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->I:Lcom/facebook/video/player/RichVideoPlayer;

    iget-object v2, p0, LX/Btu;->a:Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

    iget-object v2, v2, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->K:LX/395;

    .line 1829617
    iget-object v3, v2, LX/395;->a:Lcom/facebook/graphql/model/GraphQLVideo;

    move-object v2, v3

    .line 1829618
    iget-object v3, p0, LX/Btu;->a:Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

    iget-object v3, v3, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->K:LX/395;

    .line 1829619
    iget-object v4, v3, LX/395;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v3, v4

    .line 1829620
    sget-object v6, LX/D7o;->FULLSCREEN:LX/D7o;

    const/4 v7, 0x0

    invoke-static {v2}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v8

    move-object v4, v0

    move-object v5, v1

    move-object v9, v3

    invoke-static/range {v4 .. v9}, LX/D7p;->a(LX/D7p;Lcom/facebook/video/player/RichVideoPlayer;LX/D7o;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1829621
    iget-object v0, p0, LX/Btu;->a:Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

    const/4 v1, 0x0

    sget-object v2, LX/04g;->BY_USER:LX/04g;

    sget-object v3, LX/04G;->WATCH_AND_GO:LX/04G;

    .line 1829622
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->a$redex0(Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;ZLX/04g;LX/04G;)V

    .line 1829623
    return-void
.end method

.method public final a(LX/2op;)V
    .locals 4

    .prologue
    .line 1829624
    iget-object v0, p0, LX/Btu;->a:Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1829625
    iget-object v0, p0, LX/Btu;->a:Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

    const/4 v1, 0x0

    sget-object v2, LX/04g;->BY_INLINE_FULLSCREEN_TRANSITION:LX/04g;

    sget-object v3, LX/04G;->INLINE_PLAYER:LX/04G;

    .line 1829626
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->a$redex0(Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;ZLX/04g;LX/04G;)V

    .line 1829627
    :cond_0
    return-void
.end method

.method public final a(LX/2oq;)V
    .locals 2

    .prologue
    .line 1829628
    iget-object v0, p0, LX/Btu;->a:Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

    const/4 v1, 0x1

    .line 1829629
    invoke-static {v0, v1}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->a$redex0(Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;Z)V

    .line 1829630
    return-void
.end method

.method public final a(LX/2or;)V
    .locals 4

    .prologue
    .line 1829631
    iget-object v0, p0, LX/Btu;->a:Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

    iget-object v0, v0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->ao:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/16I;

    invoke-virtual {v0}, LX/16I;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f080d60

    .line 1829632
    :goto_0
    iget-object v1, p0, LX/Btu;->a:Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

    iget-object v1, v1, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->b:LX/1CX;

    iget-object v2, p0, LX/Btu;->a:Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

    invoke-virtual {v2}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v2}, LX/4mm;->a(Landroid/content/res/Resources;)LX/4mn;

    move-result-object v2

    const v3, 0x7f080d5f

    invoke-virtual {v2, v3}, LX/4mn;->a(I)LX/4mn;

    move-result-object v2

    invoke-virtual {v2, v0}, LX/4mn;->b(I)LX/4mn;

    move-result-object v0

    invoke-virtual {v0}, LX/4mn;->l()LX/4mm;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/1CX;->a(LX/4mm;)LX/2EJ;

    .line 1829633
    iget-object v0, p0, LX/Btu;->a:Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1829634
    iget-object v0, p0, LX/Btu;->a:Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

    const/4 v1, 0x0

    sget-object v2, LX/04g;->BY_INLINE_FULLSCREEN_TRANSITION:LX/04g;

    sget-object v3, LX/04G;->INLINE_PLAYER:LX/04G;

    .line 1829635
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->a$redex0(Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;ZLX/04g;LX/04G;)V

    .line 1829636
    :cond_0
    return-void

    .line 1829637
    :cond_1
    const v0, 0x7f080d61

    goto :goto_0
.end method
