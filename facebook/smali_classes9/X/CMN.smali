.class public LX/CMN;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:LX/1zH;

.field private b:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public c:LX/0Uh;

.field private d:LX/3QX;


# direct methods
.method public constructor <init>(LX/1zH;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Uh;LX/3QX;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1880559
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1880560
    iput-object p1, p0, LX/CMN;->a:LX/1zH;

    .line 1880561
    iput-object p2, p0, LX/CMN;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1880562
    iput-object p3, p0, LX/CMN;->c:LX/0Uh;

    .line 1880563
    iput-object p4, p0, LX/CMN;->d:LX/3QX;

    .line 1880564
    return-void
.end method

.method public static b(LX/0QB;)LX/CMN;
    .locals 5

    .prologue
    .line 1880557
    new-instance v4, LX/CMN;

    invoke-static {p0}, LX/1zF;->a(LX/0QB;)LX/1zH;

    move-result-object v0

    check-cast v0, LX/1zH;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v1

    check-cast v1, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v2

    check-cast v2, LX/0Uh;

    invoke-static {p0}, LX/3QX;->a(LX/0QB;)LX/3QX;

    move-result-object v3

    check-cast v3, LX/3QX;

    invoke-direct {v4, v0, v1, v2, v3}, LX/CMN;-><init>(LX/1zH;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Uh;LX/3QX;)V

    .line 1880558
    return-object v4
.end method

.method public static f(LX/CMN;)Z
    .locals 1

    .prologue
    .line 1880551
    goto :goto_1

    :goto_0
    return v0

    :goto_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1880565
    goto :goto_0

    :goto_0
    return v0
.end method

.method public final c()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1880556
    goto :goto_0

    :goto_0
    return v0
.end method

.method public final d()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1880553
    goto :goto_1

    .line 1880554
    :goto_0
    iget-object v3, p0, LX/CMN;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v4, LX/CMI;->f:LX/0Tn;

    invoke-interface {v3, v4, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v3

    if-nez v3, :cond_0

    if-eqz v0, :cond_1

    :cond_0
    move v2, v1

    :cond_1
    return v2

    :goto_1
    move v0, v2

    .line 1880555
    goto :goto_0
.end method

.method public final e()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1880552
    goto :goto_0

    :goto_0
    return v0
.end method
