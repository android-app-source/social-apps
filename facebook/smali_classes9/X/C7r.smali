.class public LX/C7r;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pb;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pu;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:LX/3mL;

.field private final b:LX/C7p;

.field public final c:LX/0W3;


# direct methods
.method public constructor <init>(LX/3mL;LX/C7p;LX/0W3;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1851913
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1851914
    iput-object p1, p0, LX/C7r;->a:LX/3mL;

    .line 1851915
    iput-object p2, p0, LX/C7r;->b:LX/C7p;

    .line 1851916
    iput-object p3, p0, LX/C7r;->c:LX/0W3;

    .line 1851917
    return-void
.end method

.method public static a(LX/0QB;)LX/C7r;
    .locals 6

    .prologue
    .line 1851955
    const-class v1, LX/C7r;

    monitor-enter v1

    .line 1851956
    :try_start_0
    sget-object v0, LX/C7r;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1851957
    sput-object v2, LX/C7r;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1851958
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1851959
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1851960
    new-instance p0, LX/C7r;

    invoke-static {v0}, LX/3mL;->a(LX/0QB;)LX/3mL;

    move-result-object v3

    check-cast v3, LX/3mL;

    const-class v4, LX/C7p;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/C7p;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v5

    check-cast v5, LX/0W3;

    invoke-direct {p0, v3, v4, v5}, LX/C7r;-><init>(LX/3mL;LX/C7p;LX/0W3;)V

    .line 1851961
    move-object v0, p0

    .line 1851962
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1851963
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C7r;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1851964
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1851965
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1Pn;Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1Dg;
    .locals 12
    .param p2    # LX/1Pn;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "TE;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    .line 1851918
    new-instance v1, LX/C7q;

    invoke-direct {v1, p0, p3}, LX/C7q;-><init>(LX/C7r;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1851919
    invoke-static {}, LX/3mP;->newBuilder()LX/3mP;

    move-result-object v2

    .line 1851920
    iget-object v0, p3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1851921
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/25L;->a(Ljava/lang/String;)LX/25L;

    move-result-object v0

    .line 1851922
    iput-object v0, v2, LX/3mP;->d:LX/25L;

    .line 1851923
    move-object v2, v2

    .line 1851924
    iget-object v0, p3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1851925
    check-cast v0, LX/0jW;

    .line 1851926
    iput-object v0, v2, LX/3mP;->e:LX/0jW;

    .line 1851927
    move-object v0, v2

    .line 1851928
    const/16 v2, 0x8

    .line 1851929
    iput v2, v0, LX/3mP;->b:I

    .line 1851930
    move-object v0, v0

    .line 1851931
    iput-object v1, v0, LX/3mP;->g:LX/25K;

    .line 1851932
    move-object v0, v0

    .line 1851933
    invoke-virtual {v0}, LX/3mP;->a()LX/25M;

    move-result-object v0

    .line 1851934
    iget-object v1, p0, LX/C7r;->b:LX/C7p;

    .line 1851935
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1851936
    new-instance v3, LX/C7n;

    const/4 v5, 0x0

    invoke-direct {v3, v5, p3}, LX/C7n;-><init>(ILcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1851937
    iget-object v3, p3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 1851938
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1851939
    iget-object v6, p0, LX/C7r;->c:LX/0W3;

    sget-wide v8, LX/0X5;->ff:J

    invoke-interface {v6, v8, v9}, LX/0W4;->a(J)Z

    move-result v6

    .line 1851940
    if-eqz v6, :cond_2

    invoke-static {v3}, LX/C84;->b(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v6

    invoke-static {v6}, Lcom/facebook/feedplugins/groupmemberwelcome/GroupsBasicStoryComponentSpec;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v6

    if-eqz v6, :cond_2

    const/4 v6, 0x1

    :goto_0
    move v3, v6

    .line 1851941
    if-eqz v3, :cond_0

    .line 1851942
    new-instance v3, LX/C7n;

    const/4 v5, 0x3

    invoke-direct {v3, v5, p3}, LX/C7n;-><init>(ILcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1851943
    :cond_0
    iget-object v3, p3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 1851944
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1851945
    iget-object v6, p0, LX/C7r;->c:LX/0W3;

    sget-wide v8, LX/0X5;->fg:J

    invoke-interface {v6, v8, v9}, LX/0W4;->a(J)Z

    move-result v6

    .line 1851946
    invoke-static {v3}, LX/C84;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v7

    .line 1851947
    if-eqz v6, :cond_3

    invoke-static {v7}, LX/C84;->b(Lcom/facebook/graphql/model/GraphQLGroup;)LX/0Px;

    move-result-object v6

    invoke-static {v6}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v6

    if-eqz v6, :cond_3

    const/4 v6, 0x1

    :goto_1
    move v3, v6

    .line 1851948
    if-eqz v3, :cond_1

    .line 1851949
    new-instance v3, LX/C7n;

    const/4 v5, 0x2

    invoke-direct {v3, v5, p3}, LX/C7n;-><init>(ILcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1851950
    :cond_1
    new-instance v3, LX/C7n;

    const/4 v5, 0x1

    invoke-direct {v3, v5, p3}, LX/C7n;-><init>(ILcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1851951
    invoke-static {v4}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v3

    move-object v2, v3

    .line 1851952
    new-instance v3, LX/C7o;

    move-object v6, p2

    check-cast v6, LX/1Pb;

    invoke-static {v1}, LX/C7g;->a(LX/0QB;)LX/C7g;

    move-result-object v8

    check-cast v8, LX/C7g;

    invoke-static {v1}, LX/C7j;->a(LX/0QB;)LX/C7j;

    move-result-object v9

    check-cast v9, LX/C7j;

    invoke-static {v1}, LX/C7u;->a(LX/0QB;)LX/C7u;

    move-result-object v10

    check-cast v10, LX/C7u;

    invoke-static {v1}, LX/C82;->a(LX/0QB;)LX/C82;

    move-result-object v11

    check-cast v11, LX/C82;

    move-object v4, p1

    move-object v5, v2

    move-object v7, v0

    invoke-direct/range {v3 .. v11}, LX/C7o;-><init>(Landroid/content/Context;LX/0Px;LX/1Pb;LX/25M;LX/C7g;LX/C7j;LX/C7u;LX/C82;)V

    .line 1851953
    move-object v0, v3

    .line 1851954
    iget-object v1, p0, LX/C7r;->a:LX/3mL;

    invoke-virtual {v1, p1}, LX/3mL;->c(LX/1De;)LX/3ml;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/3ml;->a(LX/3mX;)LX/3ml;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    const/4 v1, 0x7

    const/4 v2, 0x2

    invoke-interface {v0, v1, v2}, LX/1Di;->h(II)LX/1Di;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    return-object v0

    :cond_2
    const/4 v6, 0x0

    goto/16 :goto_0

    :cond_3
    const/4 v6, 0x0

    goto :goto_1
.end method
