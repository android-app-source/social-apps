.class public final LX/CKi;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/messaging/business/subscription/instantarticle/graphql/BusinessSubscriptionIAQueriesModels$BusinessSubscriptionMessengerContentSubscribedQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/CKd;

.field public final synthetic b:LX/CKk;


# direct methods
.method public constructor <init>(LX/CKk;LX/CKd;)V
    .locals 0

    .prologue
    .line 1877478
    iput-object p1, p0, LX/CKi;->b:LX/CKk;

    iput-object p2, p0, LX/CKi;->a:LX/CKd;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1877479
    iget-object v0, p0, LX/CKi;->b:LX/CKk;

    iget-object v0, v0, LX/CKk;->a:LX/03V;

    const-string v1, "BusinessIASubscriptionLoader"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1877480
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1877481
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1877482
    iget-object v0, p0, LX/CKi;->a:LX/CKd;

    if-eqz v0, :cond_1

    .line 1877483
    if-eqz p1, :cond_0

    .line 1877484
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1877485
    if-nez v0, :cond_2

    .line 1877486
    :cond_0
    iget-object v0, p0, LX/CKi;->b:LX/CKk;

    iget-object v0, v0, LX/CKk;->a:LX/03V;

    const-string v1, "BusinessIASubscriptionLoader"

    const-string v2, "GraphQL return invalid results"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1877487
    :cond_1
    :goto_0
    return-void

    .line 1877488
    :cond_2
    iget-object v1, p0, LX/CKi;->a:LX/CKd;

    .line 1877489
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1877490
    check-cast v0, Lcom/facebook/messaging/business/subscription/instantarticle/graphql/BusinessSubscriptionIAQueriesModels$BusinessSubscriptionMessengerContentSubscribedQueryModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/business/subscription/instantarticle/graphql/BusinessSubscriptionIAQueriesModels$BusinessSubscriptionMessengerContentSubscribedQueryModel;->a()Z

    move-result v0

    invoke-virtual {v1, v0}, LX/CKd;->a(Z)V

    goto :goto_0
.end method
