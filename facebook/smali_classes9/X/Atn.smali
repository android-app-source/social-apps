.class public final LX/Atn;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/ipc/media/MediaItem;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Atp;


# direct methods
.method public constructor <init>(LX/Atp;)V
    .locals 0

    .prologue
    .line 1721786
    iput-object p1, p0, LX/Atn;->a:LX/Atp;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 1721787
    iget-object v0, p0, LX/Atn;->a:LX/Atp;

    iget-object v0, v0, LX/Atp;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 1721788
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    invoke-interface {v1}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v1

    invoke-static {v1}, LX/7kq;->j(LX/0Px;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->isReleaseVideoPlayerRequested()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    instance-of v1, v1, LX/7T9;

    if-eqz v1, :cond_0

    move-object v1, v0

    .line 1721789
    check-cast v1, LX/0im;

    invoke-interface {v1}, LX/0im;->c()LX/0jJ;

    move-result-object v1

    sget-object v2, LX/Atp;->b:LX/0jK;

    invoke-virtual {v1, v2}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v1

    check-cast v1, LX/0jL;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationState;)Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->setIsReleaseVideoPlayerRequested(Z)Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0jL;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationState;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jL;

    invoke-virtual {v0}, LX/0jL;->a()V

    .line 1721790
    iget-object v0, p0, LX/Atn;->a:LX/Atp;

    invoke-static {v0}, LX/Atp;->e$redex0(LX/Atp;)V

    .line 1721791
    :goto_0
    return-void

    .line 1721792
    :cond_0
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->isReleaseVideoPlayerRequested()Z

    move-result v1

    if-eqz v1, :cond_1

    move-object v1, v0

    .line 1721793
    check-cast v1, LX/0im;

    invoke-interface {v1}, LX/0im;->c()LX/0jJ;

    move-result-object v1

    const-class v2, LX/Atp;

    invoke-static {v2}, LX/0jK;->a(Ljava/lang/Class;)LX/0jK;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v1

    check-cast v1, LX/0jL;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationState;)Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->setIsReleaseVideoPlayerRequested(Z)Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0jL;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationState;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jL;

    invoke-virtual {v0}, LX/0jL;->a()V

    .line 1721794
    :cond_1
    iget-object v0, p0, LX/Atn;->a:LX/Atp;

    iget-object v0, v0, LX/Atp;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v1, LX/Atp;->a:Ljava/lang/String;

    const-string v2, "Unable to save"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1721795
    iget-object v0, p0, LX/Atn;->a:LX/Atp;

    invoke-static {v0, v3}, LX/Atp;->a$redex0(LX/Atp;Z)V

    .line 1721796
    iget-object v0, p0, LX/Atn;->a:LX/Atp;

    .line 1721797
    iput-object v4, v0, LX/Atp;->j:Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 1721798
    iget-object v0, p0, LX/Atn;->a:LX/Atp;

    .line 1721799
    iput-object v4, v0, LX/Atp;->l:Ljava/lang/String;

    .line 1721800
    goto :goto_0
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 1721801
    check-cast p1, Lcom/facebook/ipc/media/MediaItem;

    const/4 v3, 0x1

    .line 1721802
    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v0

    .line 1721803
    iget-object v1, v0, Lcom/facebook/ipc/media/data/MediaData;->mType:LX/4gQ;

    move-object v0, v1

    .line 1721804
    sget-object v1, LX/4gQ;->Video:LX/4gQ;

    if-ne v0, v1, :cond_0

    .line 1721805
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/1t3;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".mp4"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1721806
    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, LX/Atn;->a:LX/Atp;

    iget-object v2, v2, LX/Atp;->i:Landroid/content/Context;

    invoke-static {v1, v0, v2}, LX/As0;->a(Landroid/net/Uri;Ljava/lang/String;Landroid/content/Context;)Landroid/net/Uri;

    .line 1721807
    :cond_0
    iget-object v0, p0, LX/Atn;->a:LX/Atp;

    invoke-static {v0, v3}, LX/Atp;->a$redex0(LX/Atp;Z)V

    .line 1721808
    iget-object v0, p0, LX/Atn;->a:LX/Atp;

    iget-object v0, v0, LX/Atp;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    move-object v1, v0

    .line 1721809
    check-cast v1, LX/0im;

    invoke-interface {v1}, LX/0im;->c()LX/0jJ;

    move-result-object v1

    const-class v2, LX/Atp;

    invoke-static {v2}, LX/0jK;->a(Ljava/lang/Class;)LX/0jK;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v1

    check-cast v1, LX/0jL;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationState;)Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->setShouldRefreshCameraRoll(Z)Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->setIsReleaseVideoPlayerRequested(Z)Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0jL;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationState;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jL;

    invoke-virtual {v0}, LX/0jL;->a()V

    .line 1721810
    return-void
.end method
