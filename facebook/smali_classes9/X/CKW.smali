.class public final enum LX/CKW;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/CKW;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/CKW;

.field public static final enum REQUEST_SENT:LX/CKW;

.field public static final enum SUBSCRIBE:LX/CKW;

.field public static final enum SUBSCRIBED:LX/CKW;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1877232
    new-instance v0, LX/CKW;

    const-string v1, "SUBSCRIBE"

    invoke-direct {v0, v1, v2}, LX/CKW;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CKW;->SUBSCRIBE:LX/CKW;

    .line 1877233
    new-instance v0, LX/CKW;

    const-string v1, "SUBSCRIBED"

    invoke-direct {v0, v1, v3}, LX/CKW;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CKW;->SUBSCRIBED:LX/CKW;

    .line 1877234
    new-instance v0, LX/CKW;

    const-string v1, "REQUEST_SENT"

    invoke-direct {v0, v1, v4}, LX/CKW;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CKW;->REQUEST_SENT:LX/CKW;

    .line 1877235
    const/4 v0, 0x3

    new-array v0, v0, [LX/CKW;

    sget-object v1, LX/CKW;->SUBSCRIBE:LX/CKW;

    aput-object v1, v0, v2

    sget-object v1, LX/CKW;->SUBSCRIBED:LX/CKW;

    aput-object v1, v0, v3

    sget-object v1, LX/CKW;->REQUEST_SENT:LX/CKW;

    aput-object v1, v0, v4

    sput-object v0, LX/CKW;->$VALUES:[LX/CKW;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1877236
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/CKW;
    .locals 1

    .prologue
    .line 1877237
    const-class v0, LX/CKW;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/CKW;

    return-object v0
.end method

.method public static values()[LX/CKW;
    .locals 1

    .prologue
    .line 1877238
    sget-object v0, LX/CKW;->$VALUES:[LX/CKW;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/CKW;

    return-object v0
.end method
