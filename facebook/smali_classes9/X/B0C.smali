.class public final LX/B0C;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/ALj;


# instance fields
.field public final synthetic a:Lcom/facebook/goodfriends/camera/GoodFriendsPreviewFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/goodfriends/camera/GoodFriendsPreviewFragment;)V
    .locals 0

    .prologue
    .line 1733419
    iput-object p1, p0, LX/B0C;->a:Lcom/facebook/goodfriends/camera/GoodFriendsPreviewFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/7gj;)V
    .locals 3

    .prologue
    .line 1733420
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "file://"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1733421
    iget-object v1, p1, LX/7gj;->e:Ljava/lang/String;

    move-object v1, v1

    .line 1733422
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1733423
    invoke-virtual {p1}, LX/7gj;->h()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1733424
    iget-object v1, p0, LX/B0C;->a:Lcom/facebook/goodfriends/camera/GoodFriendsPreviewFragment;

    iget-object v1, v1, Lcom/facebook/goodfriends/camera/GoodFriendsPreviewFragment;->e:LX/Azf;

    invoke-virtual {v1, v0}, LX/Azf;->a(Landroid/net/Uri;)V

    .line 1733425
    :cond_0
    :goto_0
    return-void

    .line 1733426
    :cond_1
    iget-object v1, p1, LX/7gj;->b:LX/7gi;

    sget-object v2, LX/7gi;->VIDEO:LX/7gi;

    if-ne v1, v2, :cond_2

    const/4 v1, 0x1

    :goto_1
    move v1, v1

    .line 1733427
    if-eqz v1, :cond_0

    .line 1733428
    iget-object v1, p0, LX/B0C;->a:Lcom/facebook/goodfriends/camera/GoodFriendsPreviewFragment;

    iget-object v1, v1, Lcom/facebook/goodfriends/camera/GoodFriendsPreviewFragment;->d:Lcom/facebook/backstage/camera/PreviewView;

    invoke-virtual {v1}, Lcom/facebook/backstage/camera/PreviewView;->d()V

    .line 1733429
    iget-object v1, p0, LX/B0C;->a:Lcom/facebook/goodfriends/camera/GoodFriendsPreviewFragment;

    iget-object v1, v1, Lcom/facebook/goodfriends/camera/GoodFriendsPreviewFragment;->e:LX/Azf;

    invoke-virtual {v1, v0}, LX/Azf;->b(Landroid/net/Uri;)V

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1733430
    const/4 v0, 0x1

    return v0
.end method
