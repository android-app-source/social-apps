.class public LX/AX9;
.super Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;
.source ""


# static fields
.field private static final h:Ljava/lang/String;


# instance fields
.field public g:LX/Ac6;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final i:Lcom/facebook/resources/ui/FbButton;

.field private final j:Landroid/widget/LinearLayout;

.field public k:LX/2EJ;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1683762
    const-class v0, LX/AX9;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/AX9;->h:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1683778
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/AX9;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1683779
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1683776
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/AX9;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1683777
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1683766
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1683767
    const-class v0, LX/AX9;

    invoke-static {v0, p0}, LX/AX9;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1683768
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f0305d3

    const v0, 0x7f0d0ff7

    invoke-virtual {p0, v0}, LX/AX9;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v1, v2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1683769
    const v0, 0x7f0d1004

    invoke-virtual {p0, v0}, LX/AX9;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, LX/AX9;->i:Lcom/facebook/resources/ui/FbButton;

    .line 1683770
    iget-object v0, p0, LX/AX9;->i:Lcom/facebook/resources/ui/FbButton;

    new-instance v1, LX/AX7;

    invoke-direct {v1, p0}, LX/AX7;-><init>(LX/AX9;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1683771
    const v0, 0x7f0d1003

    invoke-virtual {p0, v0}, LX/AX9;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/AX9;->j:Landroid/widget/LinearLayout;

    .line 1683772
    iget-object v0, p0, LX/AX9;->g:LX/Ac6;

    invoke-virtual {v0}, LX/Ac6;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1683773
    iget-object v0, p0, LX/AX9;->j:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1683774
    :goto_0
    return-void

    .line 1683775
    :cond_0
    iget-object v0, p0, LX/AX9;->j:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/AX9;

    invoke-static {p0}, LX/Ac6;->b(LX/0QB;)LX/Ac6;

    move-result-object p0

    check-cast p0, LX/Ac6;

    iput-object p0, p1, LX/AX9;->g:LX/Ac6;

    return-void
.end method


# virtual methods
.method public final c()V
    .locals 2

    .prologue
    .line 1683763
    invoke-super {p0}, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;->c()V

    .line 1683764
    iget-object v0, p0, LX/AX9;->j:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1683765
    return-void
.end method
