.class public final LX/CDP;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/CDQ;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/04D;

.field public c:Landroid/view/View$OnClickListener;

.field public d:Z

.field public e:Z

.field public f:Z

.field public g:LX/CDf;

.field public h:LX/3FV;

.field public i:LX/1Pe;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public j:LX/CDT;

.field public k:LX/121;

.field public l:Landroid/view/View$OnClickListener;

.field public final synthetic m:LX/CDQ;


# direct methods
.method public constructor <init>(LX/CDQ;)V
    .locals 1

    .prologue
    .line 1859156
    iput-object p1, p0, LX/CDP;->m:LX/CDQ;

    .line 1859157
    move-object v0, p1

    .line 1859158
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1859159
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1859160
    const-string v0, "RichVideoAttachmentComponent"

    return-object v0
.end method

.method public final a(LX/1X1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1",
            "<",
            "LX/CDQ;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1859161
    check-cast p1, LX/CDP;

    .line 1859162
    iget-object v0, p1, LX/CDP;->j:LX/CDT;

    iput-object v0, p0, LX/CDP;->j:LX/CDT;

    .line 1859163
    iget-object v0, p1, LX/CDP;->k:LX/121;

    iput-object v0, p0, LX/CDP;->k:LX/121;

    .line 1859164
    iget-object v0, p1, LX/CDP;->l:Landroid/view/View$OnClickListener;

    iput-object v0, p0, LX/CDP;->l:Landroid/view/View$OnClickListener;

    .line 1859165
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1859166
    if-ne p0, p1, :cond_1

    .line 1859167
    :cond_0
    :goto_0
    return v0

    .line 1859168
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1859169
    goto :goto_0

    .line 1859170
    :cond_3
    check-cast p1, LX/CDP;

    .line 1859171
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1859172
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1859173
    if-eq v2, v3, :cond_0

    .line 1859174
    iget-object v2, p0, LX/CDP;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/CDP;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/CDP;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1859175
    goto :goto_0

    .line 1859176
    :cond_5
    iget-object v2, p1, LX/CDP;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 1859177
    :cond_6
    iget-object v2, p0, LX/CDP;->b:LX/04D;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/CDP;->b:LX/04D;

    iget-object v3, p1, LX/CDP;->b:LX/04D;

    invoke-virtual {v2, v3}, LX/04D;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1859178
    goto :goto_0

    .line 1859179
    :cond_8
    iget-object v2, p1, LX/CDP;->b:LX/04D;

    if-nez v2, :cond_7

    .line 1859180
    :cond_9
    iget-object v2, p0, LX/CDP;->c:Landroid/view/View$OnClickListener;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/CDP;->c:Landroid/view/View$OnClickListener;

    iget-object v3, p1, LX/CDP;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 1859181
    goto :goto_0

    .line 1859182
    :cond_b
    iget-object v2, p1, LX/CDP;->c:Landroid/view/View$OnClickListener;

    if-nez v2, :cond_a

    .line 1859183
    :cond_c
    iget-boolean v2, p0, LX/CDP;->d:Z

    iget-boolean v3, p1, LX/CDP;->d:Z

    if-eq v2, v3, :cond_d

    move v0, v1

    .line 1859184
    goto :goto_0

    .line 1859185
    :cond_d
    iget-boolean v2, p0, LX/CDP;->e:Z

    iget-boolean v3, p1, LX/CDP;->e:Z

    if-eq v2, v3, :cond_e

    move v0, v1

    .line 1859186
    goto :goto_0

    .line 1859187
    :cond_e
    iget-boolean v2, p0, LX/CDP;->f:Z

    iget-boolean v3, p1, LX/CDP;->f:Z

    if-eq v2, v3, :cond_f

    move v0, v1

    .line 1859188
    goto :goto_0

    .line 1859189
    :cond_f
    iget-object v2, p0, LX/CDP;->g:LX/CDf;

    if-eqz v2, :cond_11

    iget-object v2, p0, LX/CDP;->g:LX/CDf;

    iget-object v3, p1, LX/CDP;->g:LX/CDf;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_12

    :cond_10
    move v0, v1

    .line 1859190
    goto :goto_0

    .line 1859191
    :cond_11
    iget-object v2, p1, LX/CDP;->g:LX/CDf;

    if-nez v2, :cond_10

    .line 1859192
    :cond_12
    iget-object v2, p0, LX/CDP;->h:LX/3FV;

    if-eqz v2, :cond_14

    iget-object v2, p0, LX/CDP;->h:LX/3FV;

    iget-object v3, p1, LX/CDP;->h:LX/3FV;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_15

    :cond_13
    move v0, v1

    .line 1859193
    goto/16 :goto_0

    .line 1859194
    :cond_14
    iget-object v2, p1, LX/CDP;->h:LX/3FV;

    if-nez v2, :cond_13

    .line 1859195
    :cond_15
    iget-object v2, p0, LX/CDP;->i:LX/1Pe;

    if-eqz v2, :cond_16

    iget-object v2, p0, LX/CDP;->i:LX/1Pe;

    iget-object v3, p1, LX/CDP;->i:LX/1Pe;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1859196
    goto/16 :goto_0

    .line 1859197
    :cond_16
    iget-object v2, p1, LX/CDP;->i:LX/1Pe;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public final g()LX/1X1;
    .locals 2

    .prologue
    .line 1859198
    const/4 v1, 0x0

    .line 1859199
    invoke-super {p0}, LX/1X1;->g()LX/1X1;

    move-result-object v0

    check-cast v0, LX/CDP;

    .line 1859200
    iput-object v1, v0, LX/CDP;->j:LX/CDT;

    .line 1859201
    iput-object v1, v0, LX/CDP;->k:LX/121;

    .line 1859202
    iput-object v1, v0, LX/CDP;->l:Landroid/view/View$OnClickListener;

    .line 1859203
    return-object v0
.end method
