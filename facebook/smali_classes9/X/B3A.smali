.class public final LX/B3A;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;)V
    .locals 0

    .prologue
    .line 1739589
    iput-object p1, p0, LX/B3A;->a:Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 1739586
    iget-object v0, p0, LX/B3A;->a:Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;

    iget-object v0, v0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->F:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->c()V

    .line 1739587
    iget-object v0, p0, LX/B3A;->a:Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;

    invoke-static {v0}, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->q(Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;)V

    .line 1739588
    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1739582
    iget-object v0, p0, LX/B3A;->a:Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;

    iget-object v0, v0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->F:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->c()V

    .line 1739583
    const-string v0, "ProfilePictureOverlayCameraActivity"

    const-string v1, "first image overlay load failed"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1739584
    return-void
.end method

.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1739585
    invoke-direct {p0}, LX/B3A;->a()V

    return-void
.end method
