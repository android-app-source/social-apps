.class public final LX/Aor;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2yQ;


# instance fields
.field public final synthetic a:Landroid/view/View;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic d:Ljava/lang/String;

.field public final synthetic e:LX/Amo;

.field public final synthetic f:LX/Aov;


# direct methods
.method public constructor <init>(LX/Aov;Landroid/view/View;Ljava/lang/String;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;LX/Amo;)V
    .locals 0

    .prologue
    .line 1714932
    iput-object p1, p0, LX/Aor;->f:LX/Aov;

    iput-object p2, p0, LX/Aor;->a:Landroid/view/View;

    iput-object p3, p0, LX/Aor;->b:Ljava/lang/String;

    iput-object p4, p0, LX/Aor;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    iput-object p5, p0, LX/Aor;->d:Ljava/lang/String;

    iput-object p6, p0, LX/Aor;->e:LX/Amo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 11

    .prologue
    .line 1714933
    iget-object v0, p0, LX/Aor;->f:LX/Aov;

    iget-object v0, v0, LX/Aov;->s:LX/0wL;

    iget-object v1, p0, LX/Aor;->a:Landroid/view/View;

    invoke-virtual {v0, v1}, LX/0wL;->b(Landroid/view/View;)V

    .line 1714934
    iget-object v0, p0, LX/Aor;->f:LX/Aov;

    iget-object v1, p0, LX/Aor;->b:Ljava/lang/String;

    iget-object v2, p0, LX/Aor;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p0, LX/Aor;->d:Ljava/lang/String;

    iget-object v4, p0, LX/Aor;->e:LX/Amo;

    .line 1714935
    iget-object v5, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v5, v5

    .line 1714936
    move-object v6, v5

    check-cast v6, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1714937
    iget-object v5, v0, LX/Aov;->b:LX/1EQ;

    invoke-static {v2}, LX/14w;->q(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v7

    .line 1714938
    iget-object v8, v7, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v7, v8

    .line 1714939
    check-cast v7, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v7

    iget-object v8, v0, LX/Aov;->c:LX/0Or;

    invoke-interface {v8}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1714940
    iget-object v9, v8, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v8, v9

    .line 1714941
    invoke-static {v6}, LX/214;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v2}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object p0

    move-object v6, v3

    move-object v10, v1

    .line 1714942
    iget-object v0, v5, LX/1EQ;->a:LX/0Zb;

    const-string v1, "feed_share_action"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 1714943
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1714944
    invoke-virtual {v0, v6}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object v1

    const-string v2, "share_type"

    const-string v3, "share_abandoned"

    invoke-virtual {v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v1

    const-string v2, "story_id"

    invoke-virtual {v1, v2, v7}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v1

    const-string v2, "user_id"

    invoke-virtual {v1, v2, v8}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v1

    const-string v2, "shareable_id"

    invoke-virtual {v1, v2, v9}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v1

    const-string v2, "tracking"

    invoke-virtual {v1, v2, p0}, LX/0oG;->a(Ljava/lang/String;LX/0lF;)LX/0oG;

    move-result-object v1

    invoke-virtual {v1, v10}, LX/0oG;->e(Ljava/lang/String;)LX/0oG;

    .line 1714945
    invoke-virtual {v0}, LX/0oG;->d()V

    .line 1714946
    :cond_0
    if-eqz v4, :cond_1

    .line 1714947
    invoke-interface {v4}, LX/Amo;->a()V

    .line 1714948
    :cond_1
    const/4 v5, 0x0

    move v0, v5

    .line 1714949
    return v0
.end method
