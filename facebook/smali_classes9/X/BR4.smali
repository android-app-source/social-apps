.class public final LX/BR4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/9fc;


# instance fields
.field public final synthetic a:LX/BR5;


# direct methods
.method public constructor <init>(LX/BR5;)V
    .locals 0

    .prologue
    .line 1783158
    iput-object p1, p0, LX/BR4;->a:LX/BR5;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)V
    .locals 2

    .prologue
    .line 1783159
    iget-object v0, p0, LX/BR4;->a:LX/BR5;

    iget-object v0, v0, LX/BR5;->a:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    .line 1783160
    iput-object p1, v0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->h:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 1783161
    iget-object v0, p0, LX/BR4;->a:LX/BR5;

    iget-object v0, v0, LX/BR5;->a:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    iget-object v0, v0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->k:LX/BR1;

    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getEditedUri()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/1be;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iput-object v1, v0, LX/BR1;->a:Landroid/net/Uri;

    .line 1783162
    iget-object v0, p0, LX/BR4;->a:LX/BR5;

    iget-object v0, v0, LX/BR5;->a:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    invoke-virtual {v0}, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->h()V

    .line 1783163
    return-void
.end method

.method public final a(Ljava/lang/Throwable;Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)V
    .locals 3

    .prologue
    .line 1783164
    iget-object v0, p0, LX/BR4;->a:LX/BR5;

    iget-object v0, v0, LX/BR5;->a:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    iget-object v0, v0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->v:LX/03V;

    const-string v1, "timeline_staging_ground"

    const-string v2, "Failed to crop profile picture preview"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1783165
    iget-object v0, p0, LX/BR4;->a:LX/BR5;

    iget-object v0, v0, LX/BR5;->a:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    invoke-virtual {v0}, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->h()V

    .line 1783166
    return-void
.end method
