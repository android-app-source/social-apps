.class public final LX/Bjq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/Bjs;


# direct methods
.method public constructor <init>(LX/Bjs;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1812849
    iput-object p1, p0, LX/Bjq;->b:LX/Bjs;

    iput-object p2, p0, LX/Bjq;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 6

    .prologue
    .line 1812850
    new-instance v0, LX/BkD;

    invoke-direct {v0}, LX/BkD;-><init>()V

    move-object v0, v0

    .line 1812851
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 1812852
    iget-object v1, p0, LX/Bjq;->b:LX/Bjs;

    iget-object v2, p0, LX/Bjq;->a:Ljava/lang/String;

    .line 1812853
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1812854
    const/4 v3, 0x0

    .line 1812855
    :goto_0
    move-object v1, v3

    .line 1812856
    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0w7;)LX/0zO;

    .line 1812857
    iget-object v1, p0, LX/Bjq;->b:LX/Bjs;

    iget-object v1, v1, LX/Bjs;->a:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    return-object v0

    .line 1812858
    :cond_0
    new-instance v3, LX/BkD;

    invoke-direct {v3}, LX/BkD;-><init>()V

    .line 1812859
    const-string v4, "page_id"

    invoke-virtual {v3, v4, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1812860
    iget-object v4, v1, LX/Bjs;->b:Landroid/content/res/Resources;

    const v5, 0x7f0b008b

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 1812861
    const-string v5, "profile_image_size"

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v5, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1812862
    const-string v4, "cover_image_portrait_size"

    iget-object v5, v1, LX/Bjs;->c:LX/0hB;

    invoke-virtual {v5}, LX/0hB;->f()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1812863
    iget-object v4, v3, LX/0gW;->e:LX/0w7;

    move-object v3, v4

    .line 1812864
    goto :goto_0
.end method
