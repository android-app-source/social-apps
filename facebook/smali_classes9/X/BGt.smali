.class public final LX/BGt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:LX/BGl;

.field public final synthetic b:Lcom/facebook/photos/simplepicker/SimplePickerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/simplepicker/SimplePickerFragment;LX/BGl;)V
    .locals 0

    .prologue
    .line 1767551
    iput-object p1, p0, LX/BGt;->b:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    iput-object p2, p0, LX/BGt;->a:LX/BGl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 1767552
    iget-object v0, p0, LX/BGt;->a:LX/BGl;

    .line 1767553
    iget-object v1, v0, LX/BGl;->a:LX/BGm;

    iget-object v1, v1, LX/BGm;->b:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    iget-object v1, v1, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->z:LX/BGe;

    iget-object v2, v0, LX/BGl;->a:LX/BGm;

    iget-object v2, v2, LX/BGm;->a:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    .line 1767554
    sget-object p0, LX/BGd;->PICKER_MOTION_PHOTOS_SHARE_AS_PHOTOS:LX/BGd;

    invoke-static {p0}, LX/BGe;->a(LX/BGd;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string p1, "photo_selected_count"

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    invoke-static {v1, p0}, LX/BGe;->a(LX/BGe;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1767555
    iget-object v1, v0, LX/BGl;->a:LX/BGm;

    iget-object v1, v1, LX/BGm;->b:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    iget-object v1, v1, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->x:LX/BHA;

    if-eqz v1, :cond_0

    .line 1767556
    iget-object v1, v0, LX/BGl;->a:LX/BGm;

    iget-object v1, v1, LX/BGm;->b:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    iget-object v1, v1, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->x:LX/BHA;

    iget-object v2, v0, LX/BGl;->a:LX/BGm;

    iget-object v2, v2, LX/BGm;->a:LX/0Px;

    iget-object p0, v0, LX/BGl;->a:LX/BGm;

    iget-object p0, p0, LX/BGm;->b:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    iget-boolean p0, p0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->V:Z

    invoke-virtual {v1, v2, p0}, LX/BHA;->b(LX/0Px;Z)V

    .line 1767557
    :cond_0
    return-void
.end method
