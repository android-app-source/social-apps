.class public final LX/BYd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0YZ;


# instance fields
.field private a:LX/0Xp;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:LX/BYc;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private c:LX/0yI;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private d:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private e:LX/0Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1796396
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(LX/BYd;LX/0Xp;LX/BYc;LX/0yI;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Zb;)V
    .locals 0

    .prologue
    .line 1796397
    iput-object p1, p0, LX/BYd;->a:LX/0Xp;

    iput-object p2, p0, LX/BYd;->b:LX/BYc;

    iput-object p3, p0, LX/BYd;->c:LX/0yI;

    iput-object p4, p0, LX/BYd;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object p5, p0, LX/BYd;->e:LX/0Zb;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 6

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v5

    move-object v0, p0

    check-cast v0, LX/BYd;

    invoke-static {v5}, LX/0Xo;->a(LX/0QB;)LX/0Xp;

    move-result-object v1

    check-cast v1, LX/0Xp;

    invoke-static {v5}, LX/BYc;->a(LX/0QB;)LX/BYc;

    move-result-object v2

    check-cast v2, LX/BYc;

    invoke-static {v5}, LX/0yI;->b(LX/0QB;)LX/0yI;

    move-result-object v3

    check-cast v3, LX/0yI;

    invoke-static {v5}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v4

    check-cast v4, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v5}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v5

    check-cast v5, LX/0Zb;

    invoke-static/range {v0 .. v5}, LX/BYd;->a(LX/BYd;LX/0Xp;LX/BYc;LX/0yI;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Zb;)V

    return-void
.end method

.method private a(ZZ)V
    .locals 5

    .prologue
    .line 1796398
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "offpeak_download_job_started"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "zero_module"

    .line 1796399
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1796400
    move-object v1, v0

    .line 1796401
    const-string v0, "carrier_id"

    iget-object v2, p0, LX/BYd;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/0yh;->NORMAL:LX/0yh;

    invoke-virtual {v3}, LX/0yh;->getCarrierIdKey()LX/0Tn;

    move-result-object v3

    const-string v4, ""

    invoke-interface {v2, v3, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1796402
    const-string v0, "service"

    const-string v2, "alarm"

    invoke-virtual {v1, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1796403
    const-string v2, "action"

    if-eqz p1, :cond_0

    const-string v0, "start"

    :goto_0
    invoke-virtual {v1, v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1796404
    const-string v0, "refresh"

    invoke-virtual {v1, v0, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1796405
    iget-object v0, p0, LX/BYd;->e:LX/0Zb;

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1796406
    return-void

    .line 1796407
    :cond_0
    const-string v0, "stop"

    goto :goto_0
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/16 v0, 0x26

    const v1, -0x1a43d957

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1796408
    invoke-static {p0, p1}, LX/BYd;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1796409
    const-string v1, "is_start_download_intent"

    invoke-virtual {p2, v1, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1796410
    iget-object v1, p0, LX/BYd;->a:LX/0Xp;

    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.facebook.zero.offpeakdownload.START_OFFPEAK_DOWNLOAD_ACTION"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, LX/0Xp;->a(Landroid/content/Intent;)Z

    .line 1796411
    iget-object v1, p0, LX/BYd;->b:LX/BYc;

    .line 1796412
    iput-boolean v4, v1, LX/BYc;->i:Z

    .line 1796413
    iget-object v1, p0, LX/BYd;->c:LX/0yI;

    sget-object v2, LX/0yY;->OFF_PEAK_VIDEO_DOWNLOAD:LX/0yY;

    invoke-virtual {v1, v2}, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->a(LX/0yY;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1796414
    iget-object v1, p0, LX/BYd;->b:LX/BYc;

    invoke-virtual {v1}, LX/BYc;->c()V

    .line 1796415
    invoke-direct {p0, v5, v5}, LX/BYd;->a(ZZ)V

    .line 1796416
    const/16 v1, 0x27

    const v2, -0x78336033

    invoke-static {v6, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1796417
    :goto_0
    return-void

    .line 1796418
    :cond_0
    invoke-direct {p0, v5, v4}, LX/BYd;->a(ZZ)V

    .line 1796419
    :goto_1
    const v1, 0x43420b93

    invoke-static {v1, v0}, LX/02F;->e(II)V

    goto :goto_0

    .line 1796420
    :cond_1
    iget-object v1, p0, LX/BYd;->a:LX/0Xp;

    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.facebook.zero.offpeakdownload.STOP_OFFPEAK_DOWNLOAD_ACTION"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, LX/0Xp;->a(Landroid/content/Intent;)Z

    .line 1796421
    iget-object v1, p0, LX/BYd;->b:LX/BYc;

    .line 1796422
    iput-boolean v4, v1, LX/BYc;->j:Z

    .line 1796423
    iget-object v1, p0, LX/BYd;->c:LX/0yI;

    sget-object v2, LX/0yY;->OFF_PEAK_VIDEO_DOWNLOAD:LX/0yY;

    invoke-virtual {v1, v2}, Lcom/facebook/zero/sdk/token/AbstractZeroTokenManager;->a(LX/0yY;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1796424
    iget-object v1, p0, LX/BYd;->b:LX/BYc;

    invoke-virtual {v1}, LX/BYc;->c()V

    .line 1796425
    invoke-direct {p0, v4, v5}, LX/BYd;->a(ZZ)V

    .line 1796426
    const v1, 0x7057a251

    invoke-static {v1, v0}, LX/02F;->e(II)V

    goto :goto_0

    .line 1796427
    :cond_2
    invoke-direct {p0, v4, v4}, LX/BYd;->a(ZZ)V

    goto :goto_1
.end method
