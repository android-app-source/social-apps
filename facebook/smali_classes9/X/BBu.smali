.class public final LX/BBu;
.super LX/0zP;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0zP",
        "<",
        "Lcom/facebook/notifications/protocol/MarkNotificationsSeenReadMutationGraphQLModels$MarkNotificationReadMutationModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 12

    .prologue
    .line 1758359
    const-class v1, Lcom/facebook/notifications/protocol/MarkNotificationsSeenReadMutationGraphQLModels$MarkNotificationReadMutationModel;

    const v0, 0x6d6066e1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "MarkNotificationReadMutation"

    const-string v6, "1d56f5dc29c339aaa8af873f7abc2caa"

    const-string v7, "notification_story_mark_read"

    const-string v8, "0"

    const-string v9, "10155069967481729"

    const/4 v10, 0x0

    .line 1758360
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v11, v0

    .line 1758361
    move-object v0, p0

    invoke-direct/range {v0 .. v11}, LX/0zP;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 1758362
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1758363
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1758364
    packed-switch v0, :pswitch_data_0

    .line 1758365
    :goto_0
    return-object p1

    .line 1758366
    :pswitch_0
    const-string p1, "0"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2eefaa
        :pswitch_0
    .end packed-switch
.end method
