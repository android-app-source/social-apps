.class public final LX/B98;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;


# direct methods
.method public constructor <init>(Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;)V
    .locals 0

    .prologue
    .line 1751065
    iput-object p1, p0, LX/B98;->a:Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x33d876d1

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1751066
    iget-object v1, p0, LX/B98;->a:Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;

    iget-boolean v1, v1, Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;->l:Z

    if-eqz v1, :cond_0

    .line 1751067
    iget-object v1, p0, LX/B98;->a:Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;

    iget-object v1, v1, Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;->g:LX/B7i;

    invoke-interface {v1}, LX/B7i;->b()V

    .line 1751068
    iget-object v1, p0, LX/B98;->a:Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;

    iget-object v1, v1, Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;->a:LX/B6l;

    const-string v2, "cta_lead_gen_xout_on_top"

    iget-object v3, p0, LX/B98;->a:Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;

    iget v3, v3, Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;->m:I

    invoke-virtual {v1, v2, v3}, LX/B6l;->a(Ljava/lang/String;I)V

    .line 1751069
    iget-object v1, p0, LX/B98;->a:Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;

    iget-object v1, v1, Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;->b:LX/0if;

    sget-object v2, LX/0ig;->v:LX/0ih;

    const-string v3, "click_xout_button_after_submit"

    invoke-virtual {v1, v2, v3}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1751070
    iget-object v1, p0, LX/B98;->a:Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;

    iget-object v1, v1, Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;->b:LX/0if;

    sget-object v2, LX/0ig;->v:LX/0ih;

    invoke-virtual {v1, v2}, LX/0if;->c(LX/0ih;)V

    .line 1751071
    const v1, -0x67d4c495

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1751072
    :goto_0
    return-void

    .line 1751073
    :cond_0
    iget-object v1, p0, LX/B98;->a:Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;

    iget-object v1, v1, Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;->g:LX/B7i;

    invoke-interface {v1}, LX/B7i;->b()V

    .line 1751074
    iget-object v1, p0, LX/B98;->a:Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;

    iget-object v1, v1, Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;->k:LX/B6u;

    .line 1751075
    iget v2, v1, LX/B6u;->h:I

    move v1, v2

    .line 1751076
    iget-object v2, p0, LX/B98;->a:Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;

    iget-object v2, v2, Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;->k:LX/B6u;

    invoke-virtual {v2}, LX/B6u;->o()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1751077
    iget-object v2, p0, LX/B98;->a:Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;

    iget-object v2, v2, Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;->a:LX/B6l;

    const-string v3, "lead_gen_close_context_card_click"

    invoke-virtual {v2, v3, v1}, LX/B6l;->a(Ljava/lang/String;I)V

    .line 1751078
    iget-object v1, p0, LX/B98;->a:Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;

    iget-object v1, v1, Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;->b:LX/0if;

    sget-object v2, LX/0ig;->v:LX/0ih;

    const-string v3, "lead_gen_close_context_card_click"

    invoke-virtual {v1, v2, v3}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1751079
    :goto_1
    iget-object v1, p0, LX/B98;->a:Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;

    iget-object v1, v1, Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;->b:LX/0if;

    sget-object v2, LX/0ig;->v:LX/0ih;

    invoke-virtual {v1, v2}, LX/0if;->c(LX/0ih;)V

    .line 1751080
    const v1, 0x14ea43d2

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto :goto_0

    .line 1751081
    :cond_1
    iget-object v2, p0, LX/B98;->a:Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;

    iget-object v2, v2, Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;->k:LX/B6u;

    invoke-virtual {v2}, LX/B6u;->n()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1751082
    iget-object v2, p0, LX/B98;->a:Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;

    iget-object v2, v2, Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;->a:LX/B6l;

    const-string v3, "cta_lead_gen_xout_on_top"

    invoke-virtual {v2, v3, v1}, LX/B6l;->a(Ljava/lang/String;I)V

    .line 1751083
    iget-object v1, p0, LX/B98;->a:Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;

    iget-object v1, v1, Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;->b:LX/0if;

    sget-object v2, LX/0ig;->v:LX/0ih;

    const-string v3, "click_xout_button_on_disclaimer"

    invoke-virtual {v1, v2, v3}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    goto :goto_1

    .line 1751084
    :cond_2
    iget-object v2, p0, LX/B98;->a:Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;

    iget-object v2, v2, Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;->a:LX/B6l;

    const-string v3, "cta_lead_gen_xout_on_top"

    invoke-virtual {v2, v3, v1}, LX/B6l;->a(Ljava/lang/String;I)V

    .line 1751085
    iget-object v2, p0, LX/B98;->a:Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;

    iget-object v2, v2, Lcom/facebook/leadgen/view/LeadGenFixedHeaderView;->b:LX/0if;

    sget-object v3, LX/0ig;->v:LX/0ih;

    const-string v4, "cta_lead_gen_xout_on_top"

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v4, v1}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method
