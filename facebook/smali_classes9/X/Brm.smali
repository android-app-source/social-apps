.class public LX/Brm;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/Brm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1826611
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1826612
    return-void
.end method

.method public static a(LX/0QB;)LX/Brm;
    .locals 3

    .prologue
    .line 1826613
    sget-object v0, LX/Brm;->a:LX/Brm;

    if-nez v0, :cond_1

    .line 1826614
    const-class v1, LX/Brm;

    monitor-enter v1

    .line 1826615
    :try_start_0
    sget-object v0, LX/Brm;->a:LX/Brm;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1826616
    if-eqz v2, :cond_0

    .line 1826617
    :try_start_1
    new-instance v0, LX/Brm;

    invoke-direct {v0}, LX/Brm;-><init>()V

    .line 1826618
    move-object v0, v0

    .line 1826619
    sput-object v0, LX/Brm;->a:LX/Brm;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1826620
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1826621
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1826622
    :cond_1
    sget-object v0, LX/Brm;->a:LX/Brm;

    return-object v0

    .line 1826623
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1826624
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
