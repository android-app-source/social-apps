.class public LX/BCw;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1761561
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel;)Lcom/facebook/graphql/model/GraphQLStory;
    .locals 8

    .prologue
    .line 1761562
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel;->b()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->j()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NavigationEndpointModel;

    move-result-object v1

    .line 1761563
    invoke-virtual {v1}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NavigationEndpointModel;->c()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    .line 1761564
    if-eqz v2, :cond_7

    invoke-static {v2}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object v0

    .line 1761565
    :goto_0
    new-instance v3, LX/2dc;

    invoke-direct {v3}, LX/2dc;-><init>()V

    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel;->b()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->b()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$AppIconModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$AppIconModel;->b()Ljava/lang/String;

    move-result-object v4

    .line 1761566
    iput-object v4, v3, LX/2dc;->h:Ljava/lang/String;

    .line 1761567
    move-object v3, v3

    .line 1761568
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel;->b()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->b()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$AppIconModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$AppIconModel;->a()I

    move-result v4

    .line 1761569
    iput v4, v3, LX/2dc;->c:I

    .line 1761570
    move-object v3, v3

    .line 1761571
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel;->b()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->b()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$AppIconModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$AppIconModel;->c()I

    move-result v4

    .line 1761572
    iput v4, v3, LX/2dc;->i:I

    .line 1761573
    move-object v3, v3

    .line 1761574
    invoke-virtual {v3}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    .line 1761575
    iput-object v3, v0, LX/23u;->g:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1761576
    new-instance v3, LX/4Wu;

    invoke-direct {v3}, LX/4Wu;-><init>()V

    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel;->b()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->b()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$AppIconModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$AppIconModel;->b()Ljava/lang/String;

    move-result-object v4

    .line 1761577
    iput-object v4, v3, LX/4Wu;->d:Ljava/lang/String;

    .line 1761578
    move-object v3, v3

    .line 1761579
    invoke-virtual {v3}, LX/4Wu;->a()Lcom/facebook/graphql/model/GraphQLIcon;

    move-result-object v3

    .line 1761580
    iput-object v3, v0, LX/23u;->M:Lcom/facebook/graphql/model/GraphQLIcon;

    .line 1761581
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel;->b()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->m()J

    move-result-wide v4

    .line 1761582
    iput-wide v4, v0, LX/23u;->v:J

    .line 1761583
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel;->b()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->o()Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v3

    .line 1761584
    iput-object v3, v0, LX/23u;->as:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    .line 1761585
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel;->b()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->q()Ljava/lang/String;

    move-result-object v3

    .line 1761586
    iput-object v3, v0, LX/23u;->aM:Ljava/lang/String;

    .line 1761587
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel;->b()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->e()Ljava/lang/String;

    move-result-object v3

    .line 1761588
    iput-object v3, v0, LX/23u;->w:Ljava/lang/String;

    .line 1761589
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_1

    .line 1761590
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel;->b()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->e()Ljava/lang/String;

    move-result-object v3

    .line 1761591
    iput-object v3, v0, LX/23u;->N:Ljava/lang/String;

    .line 1761592
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel;->b()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->c()Ljava/lang/String;

    move-result-object v3

    .line 1761593
    iput-object v3, v0, LX/23u;->m:Ljava/lang/String;

    .line 1761594
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel;->b()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->p()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$TitleModel;

    move-result-object v3

    const/4 v5, 0x0

    .line 1761595
    if-nez v3, :cond_8

    .line 1761596
    const/4 v4, 0x0

    .line 1761597
    :goto_1
    move-object v3, v4

    .line 1761598
    iput-object v3, v0, LX/23u;->aI:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1761599
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel;->b()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->k()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NotifImageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NotifImageModel;->a()Ljava/lang/String;

    move-result-object v3

    .line 1761600
    const/4 v4, 0x0

    .line 1761601
    if-eqz v2, :cond_b

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v5

    if-eqz v5, :cond_b

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_b

    .line 1761602
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLActor;

    move-object v5, v4

    .line 1761603
    :goto_2
    if-eqz v5, :cond_2

    invoke-static {v5}, LX/3dL;->a(Lcom/facebook/graphql/model/GraphQLActor;)LX/3dL;

    move-result-object v4

    .line 1761604
    :goto_3
    new-instance v6, LX/2dc;

    invoke-direct {v6}, LX/2dc;-><init>()V

    .line 1761605
    iput-object v3, v6, LX/2dc;->h:Ljava/lang/String;

    .line 1761606
    move-object v6, v6

    .line 1761607
    invoke-virtual {v6}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v6

    .line 1761608
    iput-object v6, v4, LX/3dL;->as:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1761609
    move-object v4, v4

    .line 1761610
    invoke-virtual {v4}, LX/3dL;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v4

    .line 1761611
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v6

    .line 1761612
    invoke-virtual {v6, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1761613
    if-eqz v5, :cond_3

    .line 1761614
    const/4 v4, 0x1

    :goto_4
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v5

    if-ge v4, v5, :cond_3

    .line 1761615
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v5

    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v6, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1761616
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    .line 1761617
    :cond_2
    new-instance v4, LX/3dL;

    invoke-direct {v4}, LX/3dL;-><init>()V

    goto :goto_3

    .line 1761618
    :cond_3
    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    move-object v2, v4

    .line 1761619
    iput-object v2, v0, LX/23u;->d:LX/0Px;

    .line 1761620
    invoke-virtual {v1}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NavigationEndpointModel;->b()Ljava/lang/String;

    move-result-object v2

    .line 1761621
    iput-object v2, v0, LX/23u;->aP:Ljava/lang/String;

    .line 1761622
    invoke-virtual {v1}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NavigationEndpointModel;->d()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NavigationEndpointModel$TargetIdModel;

    move-result-object v2

    .line 1761623
    invoke-virtual {v1}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NavigationEndpointModel;->c()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v3

    if-eqz v3, :cond_c

    if-eqz v2, :cond_c

    invoke-virtual {v2}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NavigationEndpointModel$TargetIdModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v2

    const v3, 0x4c808d5

    if-ne v2, v3, :cond_c

    const/4 v2, 0x1

    :goto_5
    move v2, v2

    .line 1761624
    if-nez v2, :cond_6

    .line 1761625
    invoke-virtual {v1}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NavigationEndpointModel;->a()LX/0Px;

    move-result-object v2

    .line 1761626
    invoke-virtual {v1}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NavigationEndpointModel;->d()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NavigationEndpointModel$TargetIdModel;

    move-result-object v3

    .line 1761627
    invoke-virtual {v1}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NavigationEndpointModel;->b()Ljava/lang/String;

    move-result-object v4

    .line 1761628
    if-eqz v2, :cond_4

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_5

    :cond_4
    if-nez v3, :cond_5

    if-eqz v4, :cond_d

    .line 1761629
    :cond_5
    new-instance v5, LX/39x;

    invoke-direct {v5}, LX/39x;-><init>()V

    .line 1761630
    iput-object v2, v5, LX/39x;->b:LX/0Px;

    .line 1761631
    move-object v2, v5

    .line 1761632
    sget-object v5, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->NOTIFICATION_TARGET:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-static {v5}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v5

    .line 1761633
    iput-object v5, v2, LX/39x;->p:LX/0Px;

    .line 1761634
    move-object v2, v2

    .line 1761635
    if-eqz v3, :cond_e

    new-instance v5, LX/4XR;

    invoke-direct {v5}, LX/4XR;-><init>()V

    invoke-virtual {v3}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NavigationEndpointModel$TargetIdModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    .line 1761636
    iput-object v1, v5, LX/4XR;->qc:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1761637
    move-object v5, v5

    .line 1761638
    invoke-virtual {v3}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NavigationEndpointModel$TargetIdModel;->c()Ljava/lang/String;

    move-result-object v1

    .line 1761639
    iput-object v1, v5, LX/4XR;->fO:Ljava/lang/String;

    .line 1761640
    move-object v5, v5

    .line 1761641
    invoke-virtual {v5}, LX/4XR;->a()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v5

    :goto_6
    move-object v3, v5

    .line 1761642
    iput-object v3, v2, LX/39x;->s:Lcom/facebook/graphql/model/GraphQLNode;

    .line 1761643
    move-object v2, v2

    .line 1761644
    iput-object v4, v2, LX/39x;->w:Ljava/lang/String;

    .line 1761645
    move-object v2, v2

    .line 1761646
    invoke-virtual {v2}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v2

    .line 1761647
    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    .line 1761648
    :goto_7
    move-object v1, v2

    .line 1761649
    iput-object v1, v0, LX/23u;->k:LX/0Px;

    .line 1761650
    :cond_6
    invoke-virtual {v0}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    return-object v0

    .line 1761651
    :cond_7
    new-instance v0, LX/23u;

    invoke-direct {v0}, LX/23u;-><init>()V

    goto/16 :goto_0

    :cond_8
    new-instance v4, LX/173;

    invoke-direct {v4}, LX/173;-><init>()V

    invoke-virtual {v3}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$TitleModel;->b()Ljava/lang/String;

    move-result-object v6

    .line 1761652
    iput-object v6, v4, LX/173;->f:Ljava/lang/String;

    .line 1761653
    move-object v6, v4

    .line 1761654
    new-instance v7, LX/4W6;

    invoke-direct {v7}, LX/4W6;-><init>()V

    invoke-virtual {v3}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$TitleModel;->a()LX/0Px;

    move-result-object v4

    invoke-static {v4}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v4

    if-eqz v4, :cond_9

    move v4, v5

    .line 1761655
    :goto_8
    iput v4, v7, LX/4W6;->c:I

    .line 1761656
    move-object v7, v7

    .line 1761657
    invoke-virtual {v3}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$TitleModel;->a()LX/0Px;

    move-result-object v4

    invoke-static {v4}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 1761658
    :goto_9
    iput v5, v7, LX/4W6;->d:I

    .line 1761659
    move-object v4, v7

    .line 1761660
    invoke-virtual {v4}, LX/4W6;->a()Lcom/facebook/graphql/model/GraphQLEntityAtRange;

    move-result-object v4

    invoke-static {v4}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v4

    .line 1761661
    iput-object v4, v6, LX/173;->e:LX/0Px;

    .line 1761662
    move-object v4, v6

    .line 1761663
    invoke-virtual {v4}, LX/173;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    goto/16 :goto_1

    :cond_9
    invoke-virtual {v3}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$TitleModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$TitleModel$RangesModel;

    invoke-virtual {v4}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$TitleModel$RangesModel;->a()I

    move-result v4

    goto :goto_8

    :cond_a
    invoke-virtual {v3}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$TitleModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$TitleModel$RangesModel;

    invoke-virtual {v4}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$TitleModel$RangesModel;->b()I

    move-result v5

    goto :goto_9

    :cond_b
    move-object v5, v4

    goto/16 :goto_2

    :cond_c
    const/4 v2, 0x0

    goto/16 :goto_5

    .line 1761664
    :cond_d
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 1761665
    goto :goto_7

    :cond_e
    const/4 v5, 0x0

    goto/16 :goto_6
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1761666
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->E_()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->E_()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
