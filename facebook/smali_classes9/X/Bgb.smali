.class public LX/Bgb;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1807988
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;)Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1807984
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;->b()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel;

    move-result-object v0

    .line 1807985
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1807986
    :cond_0
    const/4 v0, 0x0

    .line 1807987
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel;->a()LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel;->a()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/97f;)Ljava/lang/String;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1807978
    invoke-static {p0}, LX/Bgb;->i(LX/97f;)Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsOptionModel;

    move-result-object v2

    .line 1807979
    if-nez v2, :cond_1

    :cond_0
    :goto_0
    if-eqz v0, :cond_2

    .line 1807980
    const/4 v0, 0x0

    .line 1807981
    :goto_1
    return-object v0

    .line 1807982
    :cond_1
    invoke-virtual {v2}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsOptionModel;->d()LX/1vs;

    move-result-object v3

    iget v3, v3, LX/1vs;->b:I

    if-eqz v3, :cond_0

    move v0, v1

    goto :goto_0

    .line 1807983
    :cond_2
    invoke-virtual {v2}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsOptionModel;->d()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v2, v0, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public static a(Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsOptionModel;Z)Ljava/lang/String;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1807971
    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsOptionModel;->gx_()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_2

    .line 1807972
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsOptionModel;->gx_()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v2, v0, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    .line 1807973
    :goto_1
    return-object v0

    :cond_0
    move v0, v1

    .line 1807974
    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0

    .line 1807975
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsOptionModel;->e()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_3

    .line 1807976
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsOptionModel;->e()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v2, v0, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 1807977
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static b(LX/97f;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1807969
    invoke-static {p0}, LX/Bgb;->k(LX/97f;)Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    move-result-object v0

    .line 1807970
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;->DOESNT_HAVE_VALUE:Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    if-ne v0, v1, :cond_0

    const-string v0, "<<not-applicable>>"

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, LX/Bgb;->d(LX/97f;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static c(LX/97f;)LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/97f;",
            ")",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1807967
    invoke-static {p0}, LX/Bgb;->k(LX/97f;)Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    move-result-object v0

    .line 1807968
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;->DOESNT_HAVE_VALUE:Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    if-ne v0, v1, :cond_0

    const-string v0, "<<not-applicable>>"

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, LX/Bgb;->j(LX/97f;)LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public static c(Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;)Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1807964
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;->b()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;->b()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;->b()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;->b()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;->b()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;->b()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;->b()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel;->a()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1807965
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;->b()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;->b()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel;->a()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;->m()Ljava/lang/String;

    move-result-object v0

    .line 1807966
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(LX/97f;)Ljava/lang/String;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1807989
    invoke-static {p0}, LX/Bgb;->f(LX/97f;)Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;

    move-result-object v0

    .line 1807990
    if-eqz v0, :cond_0

    const-string v1, "<<not-applicable>>"

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;->m()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;->m()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static d(Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;)Ljava/lang/String;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1807955
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;->c()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;->c()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;->b()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;->c()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;->b()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;->c()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;->b()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel;->a()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1807956
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;->c()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;->b()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel;->a()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;->c()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 1807957
    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_3

    .line 1807958
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;->c()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;->b()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel;->a()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;->c()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 1807959
    invoke-virtual {v3, v0, v2}, LX/15i;->g(II)I

    move-result v0

    if-eqz v0, :cond_2

    :goto_1
    if-eqz v1, :cond_4

    .line 1807960
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;->c()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;->b()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel;->a()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;->c()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v0

    invoke-virtual {v1, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    .line 1807961
    :goto_2
    return-object v0

    :cond_0
    move v0, v2

    .line 1807962
    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1

    :cond_3
    move v1, v2

    goto :goto_1

    .line 1807963
    :cond_4
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public static e(LX/97f;)Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1807952
    invoke-interface {p0}, LX/97f;->j()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1807953
    const/4 v0, 0x0

    .line 1807954
    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p0}, LX/97f;->j()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;->b()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel;

    move-result-object v0

    goto :goto_0
.end method

.method public static f(LX/97f;)Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1807949
    invoke-interface {p0}, LX/97f;->j()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1807950
    const/4 v0, 0x0

    .line 1807951
    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p0}, LX/97f;->j()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;

    move-result-object v0

    invoke-static {v0}, LX/Bgb;->a(Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;)Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;

    move-result-object v0

    goto :goto_0
.end method

.method public static g(LX/97f;)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/97f;",
            ")",
            "LX/0Px",
            "<+",
            "Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsInterfaces$CrowdsourcedField$$UserValues$$Edges$;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1807946
    invoke-interface {p0}, LX/97f;->j()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, LX/97f;->j()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;->b()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, LX/97f;->j()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;->b()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1807947
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1807948
    :goto_0
    return-object v0

    :cond_1
    invoke-interface {p0}, LX/97f;->j()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel;->b()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel;->a()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public static h(LX/97f;)Landroid/location/Location;
    .locals 8
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1807934
    invoke-static {p0}, LX/Bgb;->f(LX/97f;)Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;

    move-result-object v3

    .line 1807935
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;->b()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedAddressModel$CityModel;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    if-eqz v0, :cond_3

    .line 1807936
    const/4 v0, 0x0

    .line 1807937
    :goto_1
    return-object v0

    .line 1807938
    :cond_1
    invoke-virtual {v3}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;->b()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedAddressModel$CityModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedAddressModel$CityModel;->b()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-nez v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_0

    .line 1807939
    :cond_3
    invoke-virtual {v3}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;->b()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedAddressModel$CityModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedAddressModel$CityModel;->b()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v4, v0, LX/1vs;->b:I

    .line 1807940
    sget-object v5, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v5

    :try_start_0
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1807941
    new-instance v0, Landroid/location/Location;

    const-string v5, ""

    invoke-direct {v0, v5}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    .line 1807942
    invoke-virtual {v0}, Landroid/location/Location;->reset()V

    .line 1807943
    invoke-virtual {v3, v4, v2}, LX/15i;->l(II)D

    move-result-wide v6

    invoke-virtual {v0, v6, v7}, Landroid/location/Location;->setLatitude(D)V

    .line 1807944
    invoke-virtual {v3, v4, v1}, LX/15i;->l(II)D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/location/Location;->setLongitude(D)V

    goto :goto_1

    .line 1807945
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static i(LX/97f;)Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsOptionModel;
    .locals 7
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1807927
    invoke-interface {p0}, LX/97e;->e()Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, LX/97f;->k()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel$OptionsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, LX/97f;->k()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel$OptionsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel$OptionsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move-object v0, v1

    .line 1807928
    :goto_0
    return-object v0

    .line 1807929
    :cond_1
    invoke-interface {p0}, LX/97f;->k()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel$OptionsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel$OptionsModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v4, :cond_3

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel$OptionsModel$EdgesModel;

    .line 1807930
    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel$OptionsModel$EdgesModel;->a()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsOptionModel;

    move-result-object v5

    if-eqz v5, :cond_2

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel$OptionsModel$EdgesModel;->a()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsOptionModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsOptionModel;->c()Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    move-result-object v5

    invoke-interface {p0}, LX/97e;->e()Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1807931
    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel$OptionsModel$EdgesModel;->a()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsOptionModel;

    move-result-object v0

    goto :goto_0

    .line 1807932
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_3
    move-object v0, v1

    .line 1807933
    goto :goto_0
.end method

.method public static j(LX/97f;)LX/0Px;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/97f;",
            ")",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1807919
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1807920
    invoke-static {p0}, LX/Bgb;->g(LX/97f;)LX/0Px;

    move-result-object v3

    .line 1807921
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_2

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel;

    .line 1807922
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel;->a()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel;->a()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;->m()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 1807923
    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel;->a()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;->m()Ljava/lang/String;

    move-result-object v5

    const-string v6, "<<not-applicable>>"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    const-string v0, ""

    :goto_1
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1807924
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1807925
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel;->a()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;->m()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 1807926
    :cond_2
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static k(LX/97f;)Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1807915
    invoke-static {p0}, LX/Bgb;->i(LX/97f;)Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsOptionModel;

    move-result-object v0

    .line 1807916
    if-nez v0, :cond_0

    .line 1807917
    const/4 v0, 0x0

    .line 1807918
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsOptionModel;->c()Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    move-result-object v0

    goto :goto_0
.end method
