.class public final LX/C2x;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/C2y;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/CharSequence;

.field public b:Ljava/lang/CharSequence;

.field public c:Ljava/lang/CharSequence;

.field public d:Ljava/lang/CharSequence;

.field public e:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public final synthetic f:LX/C2y;


# direct methods
.method public constructor <init>(LX/C2y;)V
    .locals 1

    .prologue
    .line 1844823
    iput-object p1, p0, LX/C2x;->f:LX/C2y;

    .line 1844824
    move-object v0, p1

    .line 1844825
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1844826
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1844850
    const-string v0, "SingleProductShareComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1844827
    if-ne p0, p1, :cond_1

    .line 1844828
    :cond_0
    :goto_0
    return v0

    .line 1844829
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1844830
    goto :goto_0

    .line 1844831
    :cond_3
    check-cast p1, LX/C2x;

    .line 1844832
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1844833
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1844834
    if-eq v2, v3, :cond_0

    .line 1844835
    iget-object v2, p0, LX/C2x;->a:Ljava/lang/CharSequence;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/C2x;->a:Ljava/lang/CharSequence;

    iget-object v3, p1, LX/C2x;->a:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1844836
    goto :goto_0

    .line 1844837
    :cond_5
    iget-object v2, p1, LX/C2x;->a:Ljava/lang/CharSequence;

    if-nez v2, :cond_4

    .line 1844838
    :cond_6
    iget-object v2, p0, LX/C2x;->b:Ljava/lang/CharSequence;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/C2x;->b:Ljava/lang/CharSequence;

    iget-object v3, p1, LX/C2x;->b:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1844839
    goto :goto_0

    .line 1844840
    :cond_8
    iget-object v2, p1, LX/C2x;->b:Ljava/lang/CharSequence;

    if-nez v2, :cond_7

    .line 1844841
    :cond_9
    iget-object v2, p0, LX/C2x;->c:Ljava/lang/CharSequence;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/C2x;->c:Ljava/lang/CharSequence;

    iget-object v3, p1, LX/C2x;->c:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 1844842
    goto :goto_0

    .line 1844843
    :cond_b
    iget-object v2, p1, LX/C2x;->c:Ljava/lang/CharSequence;

    if-nez v2, :cond_a

    .line 1844844
    :cond_c
    iget-object v2, p0, LX/C2x;->d:Ljava/lang/CharSequence;

    if-eqz v2, :cond_e

    iget-object v2, p0, LX/C2x;->d:Ljava/lang/CharSequence;

    iget-object v3, p1, LX/C2x;->d:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    :cond_d
    move v0, v1

    .line 1844845
    goto :goto_0

    .line 1844846
    :cond_e
    iget-object v2, p1, LX/C2x;->d:Ljava/lang/CharSequence;

    if-nez v2, :cond_d

    .line 1844847
    :cond_f
    iget-object v2, p0, LX/C2x;->e:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_10

    iget-object v2, p0, LX/C2x;->e:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/C2x;->e:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1844848
    goto :goto_0

    .line 1844849
    :cond_10
    iget-object v2, p1, LX/C2x;->e:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
