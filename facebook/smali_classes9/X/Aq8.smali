.class public final LX/Aq8;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/Aq8;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Aq6;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/AqE;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1717307
    const/4 v0, 0x0

    sput-object v0, LX/Aq8;->a:LX/Aq8;

    .line 1717308
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/Aq8;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1717304
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1717305
    new-instance v0, LX/AqE;

    invoke-direct {v0}, LX/AqE;-><init>()V

    iput-object v0, p0, LX/Aq8;->c:LX/AqE;

    .line 1717306
    return-void
.end method

.method public static declared-synchronized q()LX/Aq8;
    .locals 2

    .prologue
    .line 1717300
    const-class v1, LX/Aq8;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/Aq8;->a:LX/Aq8;

    if-nez v0, :cond_0

    .line 1717301
    new-instance v0, LX/Aq8;

    invoke-direct {v0}, LX/Aq8;-><init>()V

    sput-object v0, LX/Aq8;->a:LX/Aq8;

    .line 1717302
    :cond_0
    sget-object v0, LX/Aq8;->a:LX/Aq8;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 1717303
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1717298
    invoke-static {}, LX/1dS;->b()V

    .line 1717299
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/1De;LX/1Dg;IILX/1no;LX/1X1;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x2ca26196

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1717281
    check-cast p6, LX/Aq7;

    .line 1717282
    iget-boolean v1, p6, LX/Aq7;->a:Z

    const/4 p2, 0x0

    .line 1717283
    sget-object v2, LX/AqE;->a:Ljava/lang/ref/WeakReference;

    if-eqz v2, :cond_2

    sget-object v2, LX/AqE;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/RadioButton;

    .line 1717284
    :goto_0
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/widget/RadioButton;->getContext()Landroid/content/Context;

    move-result-object p0

    if-eq p0, p1, :cond_1

    .line 1717285
    :cond_0
    new-instance v2, Landroid/widget/RadioButton;

    invoke-direct {v2, p1}, Landroid/widget/RadioButton;-><init>(Landroid/content/Context;)V

    .line 1717286
    new-instance p0, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {p0, p2, p2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v2, p0}, Landroid/widget/RadioButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1717287
    new-instance p0, Ljava/lang/ref/WeakReference;

    invoke-direct {p0, v2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    sput-object p0, LX/AqE;->a:Ljava/lang/ref/WeakReference;

    .line 1717288
    :cond_1
    invoke-virtual {v2, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 1717289
    invoke-static {p3}, LX/1oC;->a(I)I

    move-result p0

    .line 1717290
    invoke-static {p4}, LX/1oC;->a(I)I

    move-result p2

    .line 1717291
    invoke-virtual {v2}, Landroid/widget/RadioButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p6

    iput p0, p6, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1717292
    invoke-virtual {v2}, Landroid/widget/RadioButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p0

    iput p2, p0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1717293
    invoke-static {p3}, LX/1oC;->a(I)I

    move-result p0

    invoke-static {p4}, LX/1oC;->a(I)I

    move-result p2

    invoke-virtual {v2, p0, p2}, Landroid/widget/RadioButton;->measure(II)V

    .line 1717294
    invoke-virtual {v2}, Landroid/widget/RadioButton;->getMeasuredWidth()I

    move-result p0

    iput p0, p5, LX/1no;->a:I

    .line 1717295
    invoke-virtual {v2}, Landroid/widget/RadioButton;->getMeasuredHeight()I

    move-result v2

    iput v2, p5, LX/1no;->b:I

    .line 1717296
    const/16 v1, 0x1f

    const v2, -0x5fb57f64

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1717297
    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public final b(LX/1De;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1717279
    new-instance v0, LX/AqD;

    invoke-direct {v0, p1}, LX/AqD;-><init>(Landroid/content/Context;)V

    move-object v0, v0

    .line 1717280
    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1717278
    const/4 v0, 0x1

    return v0
.end method

.method public final e(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 1

    .prologue
    .line 1717267
    check-cast p3, LX/Aq7;

    .line 1717268
    check-cast p2, LX/AqD;

    iget-boolean v0, p3, LX/Aq7;->a:Z

    .line 1717269
    iget-object p0, p1, LX/1De;->g:LX/1X1;

    move-object p0, p0

    .line 1717270
    if-nez p0, :cond_0

    .line 1717271
    const/4 p0, 0x0

    .line 1717272
    :goto_0
    move-object p0, p0

    .line 1717273
    iput-object p0, p2, LX/AqD;->a:LX/1dQ;

    .line 1717274
    invoke-virtual {p2, v0}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 1717275
    return-void

    .line 1717276
    :cond_0
    iget-object p0, p1, LX/1De;->g:LX/1X1;

    move-object p0, p0

    .line 1717277
    check-cast p0, LX/Aq7;

    iget-object p0, p0, LX/Aq7;->b:LX/1dQ;

    goto :goto_0
.end method

.method public final f()LX/1mv;
    .locals 1

    .prologue
    .line 1717266
    sget-object v0, LX/1mv;->VIEW:LX/1mv;

    return-object v0
.end method

.method public final f(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 0

    .prologue
    .line 1717254
    check-cast p2, LX/AqD;

    .line 1717255
    const/4 p0, 0x0

    .line 1717256
    iput-object p0, p2, LX/AqD;->a:LX/1dQ;

    .line 1717257
    return-void
.end method

.method public final g(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 0

    .prologue
    .line 1717263
    check-cast p2, LX/AqD;

    .line 1717264
    invoke-virtual {p2, p2}, LX/AqD;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 1717265
    return-void
.end method

.method public final h(LX/1De;Ljava/lang/Object;LX/1X1;)V
    .locals 0

    .prologue
    .line 1717260
    check-cast p2, LX/AqD;

    .line 1717261
    const/4 p0, 0x0

    invoke-virtual {p2, p0}, LX/AqD;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 1717262
    return-void
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 1717259
    const/4 v0, 0x1

    return v0
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 1717258
    const/16 v0, 0xf

    return v0
.end method
