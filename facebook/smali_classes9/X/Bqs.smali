.class public final LX/Bqs;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:LX/8wV;

.field public final synthetic b:Ljava/util/Map;

.field public final synthetic c:Landroid/content/Context;

.field public final synthetic d:Lcom/facebook/graphql/model/GraphQLAYMTNativeMobileAction;

.field public final synthetic e:LX/Bqw;


# direct methods
.method public constructor <init>(LX/Bqw;LX/8wV;Ljava/util/Map;Landroid/content/Context;Lcom/facebook/graphql/model/GraphQLAYMTNativeMobileAction;)V
    .locals 0

    .prologue
    .line 1825444
    iput-object p1, p0, LX/Bqs;->e:LX/Bqw;

    iput-object p2, p0, LX/Bqs;->a:LX/8wV;

    iput-object p3, p0, LX/Bqs;->b:Ljava/util/Map;

    iput-object p4, p0, LX/Bqs;->c:Landroid/content/Context;

    iput-object p5, p0, LX/Bqs;->d:Lcom/facebook/graphql/model/GraphQLAYMTNativeMobileAction;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5

    .prologue
    .line 1825445
    iget-object v0, p0, LX/Bqs;->e:LX/Bqw;

    iget-object v0, v0, LX/Bqw;->e:LX/2Pb;

    iget-object v1, p0, LX/Bqs;->a:LX/8wV;

    sget-object v2, LX/8wW;->EVENT_CLIENT_INELIGIBLE_FLOW_ACTION_CLICK:LX/8wW;

    iget-object v3, p0, LX/Bqs;->b:Ljava/util/Map;

    iget-object v4, p0, LX/Bqs;->e:LX/Bqw;

    iget-object v4, v4, LX/Bqw;->g:LX/03V;

    invoke-virtual {v0, v1, v2, v3, v4}, LX/2Pb;->a(LX/8wV;LX/8wW;Ljava/util/Map;LX/03V;)V

    .line 1825446
    iget-object v0, p0, LX/Bqs;->e:LX/Bqw;

    iget-object v1, p0, LX/Bqs;->c:Landroid/content/Context;

    iget-object v2, p0, LX/Bqs;->d:Lcom/facebook/graphql/model/GraphQLAYMTNativeMobileAction;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLAYMTNativeMobileAction;->j()Ljava/lang/String;

    move-result-object v2

    const/4 p0, 0x1

    .line 1825447
    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.intent.action.VIEW"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1825448
    const/high16 v4, 0x10000000

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 1825449
    const-string v4, "force_in_app_browser"

    invoke-virtual {v3, v4, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1825450
    const-string v4, "should_hide_menu"

    invoke-virtual {v3, v4, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1825451
    iget-object v4, v0, LX/Bqw;->h:LX/00H;

    .line 1825452
    iget-object p0, v4, LX/00H;->j:LX/01T;

    move-object v4, p0

    .line 1825453
    sget-object p0, LX/01T;->PAA:LX/01T;

    if-ne v4, p0, :cond_1

    .line 1825454
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1825455
    iget-object v4, v0, LX/Bqw;->d:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v4, v3, v1}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1825456
    :cond_0
    :goto_0
    return-void

    .line 1825457
    :cond_1
    iget-object v4, v0, LX/Bqw;->h:LX/00H;

    .line 1825458
    iget-object p0, v4, LX/00H;->j:LX/01T;

    move-object v4, p0

    .line 1825459
    sget-object p0, LX/01T;->FB4A:LX/01T;

    if-ne v4, p0, :cond_0

    .line 1825460
    sget-object v4, LX/0ax;->do:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-static {v4, p0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1825461
    iget-object v4, v0, LX/Bqw;->d:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v4, v3, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0
.end method
