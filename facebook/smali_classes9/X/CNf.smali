.class public LX/CNf;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/CNe;


# instance fields
.field private final a:LX/CNb;

.field private final b:LX/CNe;


# direct methods
.method public constructor <init>(LX/CNb;LX/CNc;)V
    .locals 1

    .prologue
    .line 1881993
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1881994
    iput-object p1, p0, LX/CNf;->a:LX/CNb;

    .line 1881995
    const-string v0, "children"

    invoke-virtual {p1, v0}, LX/CNb;->b(Ljava/lang/String;)LX/CNb;

    move-result-object v0

    .line 1881996
    if-eqz v0, :cond_0

    invoke-static {v0, p2}, LX/CNd;->a(LX/CNb;LX/CNc;)LX/CNe;

    move-result-object v0

    :goto_0
    iput-object v0, p0, LX/CNf;->b:LX/CNe;

    .line 1881997
    return-void

    .line 1881998
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 12

    .prologue
    .line 1881999
    iget-object v0, p0, LX/CNf;->b:LX/CNe;

    if-nez v0, :cond_0

    .line 1882000
    :goto_0
    return-void

    .line 1882001
    :cond_0
    iget-object v0, p0, LX/CNf;->a:LX/CNb;

    .line 1882002
    const-string v1, "action_controller"

    move-object v1, v1

    .line 1882003
    invoke-virtual {v0, v1}, LX/CNb;->d(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CNg;

    check-cast v0, LX/CNg;

    .line 1882004
    iget-object v1, p0, LX/CNf;->b:LX/CNe;

    iget-object v2, p0, LX/CNf;->a:LX/CNb;

    const-string v3, "time-ms"

    const-wide/16 v4, 0x0

    invoke-virtual {v2, v3, v4, v5}, LX/CNb;->a(Ljava/lang/String;J)J

    move-result-wide v2

    .line 1882005
    new-instance v6, Lcom/facebook/nativetemplates/action/NTDateActionController$1;

    invoke-direct {v6, v0, v1}, Lcom/facebook/nativetemplates/action/NTDateActionController$1;-><init>(LX/CNg;LX/CNe;)V

    iput-object v6, v0, LX/CNg;->b:Ljava/lang/Runnable;

    .line 1882006
    iget-object v6, v0, LX/CNg;->a:Landroid/os/Handler;

    iget-object v7, v0, LX/CNg;->b:Ljava/lang/Runnable;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    sub-long v8, v2, v8

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v10

    add-long/2addr v8, v10

    const v10, 0xe07649d

    invoke-static {v6, v7, v8, v9, v10}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 1882007
    goto :goto_0
.end method
