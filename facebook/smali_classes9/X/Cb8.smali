.class public final LX/Cb8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/CbA;


# direct methods
.method public constructor <init>(LX/CbA;)V
    .locals 0

    .prologue
    .line 1919172
    iput-object p1, p0, LX/Cb8;->a:LX/CbA;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x2

    const v0, -0x6daed4a

    invoke-static {v3, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1919173
    iget-object v1, p0, LX/Cb8;->a:LX/CbA;

    iget-object v1, v1, LX/CbA;->m:LX/8Hs;

    invoke-virtual {v1, v2}, LX/8Hs;->b(Z)V

    .line 1919174
    iget-object v1, p0, LX/Cb8;->a:LX/CbA;

    iget-object v1, v1, LX/CbA;->j:LX/Cau;

    iget-object v2, p0, LX/Cb8;->a:LX/CbA;

    .line 1919175
    iget-object v4, v1, LX/Cau;->a:LX/Caz;

    iget-object v4, v4, LX/Caz;->b:LX/CbI;

    invoke-virtual {v4, v2}, LX/CbI;->a(LX/CbA;)Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel;

    move-result-object v6

    .line 1919176
    invoke-static {v6}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1919177
    invoke-virtual {v6}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel;->b()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v6}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel;->b()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v4

    const v5, -0x2e4d6bd6

    if-ne v4, v5, :cond_0

    const/4 v4, 0x1

    move v5, v4

    .line 1919178
    :goto_0
    iget-object v4, v1, LX/Cau;->a:LX/Caz;

    iget-object v4, v4, LX/Caz;->h:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/745;

    sget-object v7, LX/74T;->CONSUMPTION:LX/74T;

    .line 1919179
    invoke-static {v7}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1919180
    invoke-static {v4}, LX/745;->d(LX/745;)Ljava/util/HashMap;

    move-result-object p0

    .line 1919181
    const-string p1, "ex_tag_screen"

    invoke-virtual {v7}, LX/74T;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, p1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1919182
    const-string p1, "is_text"

    invoke-static {v5}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, p1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1919183
    sget-object p1, LX/74R;->TAG_DELETED:LX/74R;

    const/4 v2, 0x0

    invoke-static {v4, p1, p0, v2}, LX/745;->a(LX/745;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 1919184
    iget-object v4, v1, LX/Cau;->a:LX/Caz;

    iget-object v4, v4, LX/Caz;->i:LX/1Ck;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v7, "MediaTaggingControllerRemoveTag"

    invoke-direct {v5, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel;->b()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;->c()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    new-instance v7, LX/Cas;

    invoke-direct {v7, v1, v6}, LX/Cas;-><init>(LX/Cau;Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel;)V

    new-instance v6, LX/Cat;

    invoke-direct {v6, v1}, LX/Cat;-><init>(LX/Cau;)V

    invoke-virtual {v4, v5, v7, v6}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 1919185
    const v1, -0x6dfa867c

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1919186
    :cond_0
    const/4 v4, 0x0

    move v5, v4

    goto :goto_0
.end method
