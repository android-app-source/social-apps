.class public LX/By7;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/By8;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/By7",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/By8;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1836797
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1836798
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/By7;->b:LX/0Zi;

    .line 1836799
    iput-object p1, p0, LX/By7;->a:LX/0Ot;

    .line 1836800
    return-void
.end method

.method private a(LX/1De;II)LX/By5;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "II)",
            "LX/By7",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 1836796
    new-instance v0, LX/By6;

    invoke-direct {v0, p0}, LX/By6;-><init>(LX/By7;)V

    invoke-direct {p0, p1, p2, p3, v0}, LX/By7;->a(LX/1De;IILX/By6;)LX/By5;

    move-result-object v0

    return-object v0
.end method

.method private a(LX/1De;IILX/By6;)LX/By5;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "II",
            "LX/By7",
            "<TE;>.EventAttachmentFooterViewComponentImpl;)",
            "LX/By7",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 1836791
    iget-object v0, p0, LX/By7;->b:LX/0Zi;

    invoke-virtual {v0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/By5;

    .line 1836792
    if-nez v0, :cond_0

    .line 1836793
    new-instance v0, LX/By5;

    invoke-direct {v0, p0}, LX/By5;-><init>(LX/By7;)V

    .line 1836794
    :cond_0
    invoke-static {v0, p1, p2, p3, p4}, LX/By5;->a$redex0(LX/By5;LX/1De;IILX/By6;)V

    .line 1836795
    return-object v0
.end method

.method public static a(LX/0QB;)LX/By7;
    .locals 3

    .prologue
    .line 1836783
    const-class v1, LX/By7;

    monitor-enter v1

    .line 1836784
    :try_start_0
    sget-object v0, LX/By7;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1836785
    sput-object v2, LX/By7;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1836786
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1836787
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/By7;->b(LX/0QB;)LX/By7;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1836788
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/By7;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1836789
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1836790
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(Landroid/view/View;LX/1X1;)V
    .locals 4

    .prologue
    .line 1836780
    check-cast p2, LX/By6;

    .line 1836781
    iget-object v0, p0, LX/By7;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/By8;

    iget-object v1, p2, LX/By6;->c:LX/1Pq;

    iget-object v2, p2, LX/By6;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p2, LX/By6;->d:Ljava/lang/String;

    invoke-virtual {v0, p1, v1, v2, v3}, LX/By8;->onClick(Landroid/view/View;LX/1Pq;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;)V

    .line 1836782
    return-void
.end method

.method private static b(LX/0QB;)LX/By7;
    .locals 2

    .prologue
    .line 1836778
    new-instance v0, LX/By7;

    const/16 v1, 0x1e1e

    invoke-static {p0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-direct {v0, v1}, LX/By7;-><init>(LX/0Ot;)V

    .line 1836779
    return-object v0
.end method

.method public static onClick(LX/1De;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1836801
    const v0, 0x560cc779

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1836777
    const v0, 0x560cc779

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1836771
    invoke-static {}, LX/1dS;->b()V

    .line 1836772
    iget v0, p1, LX/1dQ;->b:I

    .line 1836773
    packed-switch v0, :pswitch_data_0

    .line 1836774
    :goto_0
    return-object v2

    .line 1836775
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 1836776
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    invoke-direct {p0, v0, v1}, LX/By7;->a(Landroid/view/View;LX/1X1;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x560cc779
        :pswitch_0
    .end packed-switch
.end method

.method public final b(LX/1De;IILX/1X1;)LX/1Dg;
    .locals 3

    .prologue
    .line 1836768
    check-cast p4, LX/By6;

    .line 1836769
    iget-object v0, p0, LX/By7;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/By8;

    iget-object v1, p4, LX/By6;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget v2, p4, LX/By6;->b:I

    invoke-virtual {v0, p1, p2, v1, v2}, LX/By8;->a(LX/1De;ILcom/facebook/feed/rows/core/props/FeedProps;I)LX/1Dg;

    move-result-object v0

    .line 1836770
    return-object v0
.end method

.method public final c(LX/1De;)LX/By5;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/By7",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1836767
    invoke-direct {p0, p1, v0, v0}, LX/By7;->a(LX/1De;II)LX/By5;

    move-result-object v0

    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1836766
    const/4 v0, 0x1

    return v0
.end method
