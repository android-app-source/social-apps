.class public LX/CfJ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/CfW;

.field private final b:LX/CfG;

.field public final c:LX/CfI;

.field public d:LX/2jY;

.field public e:Z

.field private final f:LX/CfG;


# direct methods
.method public constructor <init>(LX/CfG;LX/CfI;LX/CfW;)V
    .locals 1
    .param p1    # LX/CfG;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/CfI;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1925735
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1925736
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/CfJ;->e:Z

    .line 1925737
    new-instance v0, LX/CfH;

    invoke-direct {v0, p0}, LX/CfH;-><init>(LX/CfJ;)V

    iput-object v0, p0, LX/CfJ;->f:LX/CfG;

    .line 1925738
    iput-object p1, p0, LX/CfJ;->b:LX/CfG;

    .line 1925739
    iput-object p2, p0, LX/CfJ;->c:LX/CfI;

    .line 1925740
    iput-object p3, p0, LX/CfJ;->a:LX/CfW;

    .line 1925741
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/reaction/ReactionQueryParams;Lcom/facebook/reaction/ReactionQueryParams;Ljava/lang/String;Ljava/lang/Long;Z)LX/2jY;
    .locals 3
    .param p3    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
        .end annotation
    .end param
    .param p4    # Ljava/lang/Long;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1925742
    iget-object v0, p0, LX/CfJ;->a:LX/CfW;

    invoke-virtual {v0, p3, p1, p4}, LX/CfW;->a(Ljava/lang/String;Lcom/facebook/reaction/ReactionQueryParams;Ljava/lang/Long;)LX/2jY;

    move-result-object v1

    .line 1925743
    iget-object v0, p0, LX/CfJ;->b:LX/CfG;

    invoke-virtual {v1, v0}, LX/2jY;->a(LX/CfG;)V

    .line 1925744
    if-eqz p5, :cond_0

    iget-object v0, p0, LX/CfJ;->a:LX/CfW;

    invoke-virtual {v0, p3, p2}, LX/CfW;->b(Ljava/lang/String;Lcom/facebook/reaction/ReactionQueryParams;)LX/2jY;

    move-result-object v0

    :goto_0
    iput-object v0, p0, LX/CfJ;->d:LX/2jY;

    .line 1925745
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/CfJ;->e:Z

    .line 1925746
    iget-object v0, p0, LX/CfJ;->d:LX/2jY;

    iget-object v2, p0, LX/CfJ;->f:LX/CfG;

    invoke-virtual {v0, v2}, LX/2jY;->a(LX/CfG;)V

    .line 1925747
    return-object v1

    .line 1925748
    :cond_0
    iget-object v0, p0, LX/CfJ;->a:LX/CfW;

    invoke-virtual {v0, p3, p2}, LX/CfW;->a(Ljava/lang/String;Lcom/facebook/reaction/ReactionQueryParams;)LX/2jY;

    move-result-object v0

    goto :goto_0
.end method
