.class public LX/Apq;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/App;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/fig/components/utils/FigMediaComponentSpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1716857
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/Apq;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/fig/components/utils/FigMediaComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1716854
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1716855
    iput-object p1, p0, LX/Apq;->b:LX/0Ot;

    .line 1716856
    return-void
.end method

.method public static a(LX/0QB;)LX/Apq;
    .locals 4

    .prologue
    .line 1716843
    const-class v1, LX/Apq;

    monitor-enter v1

    .line 1716844
    :try_start_0
    sget-object v0, LX/Apq;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1716845
    sput-object v2, LX/Apq;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1716846
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1716847
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1716848
    new-instance v3, LX/Apq;

    const/16 p0, 0x2213

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/Apq;-><init>(LX/0Ot;)V

    .line 1716849
    move-object v0, v3

    .line 1716850
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1716851
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Apq;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1716852
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1716853
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 1716840
    check-cast p2, Lcom/facebook/fig/components/utils/FigMediaComponent$FigMediaComponentImpl;

    .line 1716841
    iget-object v0, p0, LX/Apq;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/components/utils/FigMediaComponentSpec;

    iget v2, p2, Lcom/facebook/fig/components/utils/FigMediaComponent$FigMediaComponentImpl;->a:I

    iget-object v3, p2, Lcom/facebook/fig/components/utils/FigMediaComponent$FigMediaComponentImpl;->b:Landroid/net/Uri;

    iget-object v4, p2, Lcom/facebook/fig/components/utils/FigMediaComponent$FigMediaComponentImpl;->c:Lcom/facebook/common/callercontext/CallerContext;

    iget-object v5, p2, Lcom/facebook/fig/components/utils/FigMediaComponent$FigMediaComponentImpl;->d:LX/1X1;

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/facebook/fig/components/utils/FigMediaComponentSpec;->a(LX/1De;ILandroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;LX/1X1;)LX/1Dg;

    move-result-object v0

    .line 1716842
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1716830
    invoke-static {}, LX/1dS;->b()V

    .line 1716831
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c(LX/1De;)LX/App;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1716832
    new-instance v1, Lcom/facebook/fig/components/utils/FigMediaComponent$FigMediaComponentImpl;

    invoke-direct {v1, p0}, Lcom/facebook/fig/components/utils/FigMediaComponent$FigMediaComponentImpl;-><init>(LX/Apq;)V

    .line 1716833
    sget-object v2, LX/Apq;->a:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/App;

    .line 1716834
    if-nez v2, :cond_0

    .line 1716835
    new-instance v2, LX/App;

    invoke-direct {v2}, LX/App;-><init>()V

    .line 1716836
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/App;->a$redex0(LX/App;LX/1De;IILcom/facebook/fig/components/utils/FigMediaComponent$FigMediaComponentImpl;)V

    .line 1716837
    move-object v1, v2

    .line 1716838
    move-object v0, v1

    .line 1716839
    return-object v0
.end method
