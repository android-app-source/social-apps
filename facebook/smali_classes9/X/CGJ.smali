.class public LX/CGJ;
.super Landroid/widget/FrameLayout;
.source ""


# instance fields
.field public final a:LX/CFp;

.field public final b:LX/0YU;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0YU",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Landroid/os/Handler;

.field public d:Lcom/facebook/greetingcards/verve/model/VMView;

.field public e:I

.field public f:LX/CG5;

.field public g:I

.field public final h:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/CFp;)V
    .locals 1

    .prologue
    .line 1865305
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 1865306
    new-instance v0, Lcom/facebook/greetingcards/verve/render/VerveSequenceView$1;

    invoke-direct {v0, p0}, Lcom/facebook/greetingcards/verve/render/VerveSequenceView$1;-><init>(LX/CGJ;)V

    iput-object v0, p0, LX/CGJ;->h:Ljava/lang/Runnable;

    .line 1865307
    iput-object p2, p0, LX/CGJ;->a:LX/CFp;

    .line 1865308
    new-instance v0, LX/0YU;

    invoke-direct {v0}, LX/0YU;-><init>()V

    iput-object v0, p0, LX/CGJ;->b:LX/0YU;

    .line 1865309
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, LX/CGJ;->c:Landroid/os/Handler;

    .line 1865310
    return-void
.end method

.method public static a(LX/CGJ;I)Landroid/view/View;
    .locals 2

    .prologue
    .line 1865311
    iget-object v0, p0, LX/CGJ;->d:Lcom/facebook/greetingcards/verve/model/VMView;

    iget-object v0, v0, Lcom/facebook/greetingcards/verve/model/VMView;->subviews:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/greetingcards/verve/model/VMView;

    .line 1865312
    iget-object v1, p0, LX/CGJ;->b:LX/0YU;

    iget v0, v0, Lcom/facebook/greetingcards/verve/model/VMView;->a:I

    invoke-virtual {v1, v0}, LX/0YU;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method
