.class public LX/BS7;
.super Landroid/graphics/drawable/Drawable;
.source ""


# static fields
.field private static final c:LX/2iJ;

.field private static final d:I


# instance fields
.field public final a:Landroid/content/res/Resources;

.field public b:Landroid/graphics/drawable/Drawable;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final e:LX/0wM;

.field public f:LX/2iJ;

.field private g:Z

.field private h:Ljava/lang/Integer;

.field public i:I

.field public j:Landroid/graphics/drawable/Drawable;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final k:Landroid/graphics/Paint;

.field public l:I

.field private final m:Landroid/graphics/Rect;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1784834
    sget-object v0, LX/2iJ;->BIG:LX/2iJ;

    sput-object v0, LX/BS7;->c:LX/2iJ;

    .line 1784835
    const v0, 0x7f0a00d1

    sput v0, LX/BS7;->d:I

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;LX/0wM;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 1784823
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 1784824
    iput-boolean v1, p0, LX/BS7;->g:Z

    .line 1784825
    const/4 v0, -0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, LX/BS7;->h:Ljava/lang/Integer;

    .line 1784826
    const/4 v0, 0x0

    iput v0, p0, LX/BS7;->i:I

    .line 1784827
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, LX/BS7;->k:Landroid/graphics/Paint;

    .line 1784828
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/BS7;->m:Landroid/graphics/Rect;

    .line 1784829
    iput-object p1, p0, LX/BS7;->a:Landroid/content/res/Resources;

    .line 1784830
    iput-object p2, p0, LX/BS7;->e:LX/0wM;

    .line 1784831
    sget-object v0, LX/BS7;->c:LX/2iJ;

    invoke-virtual {p0, v0}, LX/BS7;->a(LX/2iJ;)V

    .line 1784832
    iget-object v0, p0, LX/BS7;->k:Landroid/graphics/Paint;

    iget-object v1, p0, LX/BS7;->a:Landroid/content/res/Resources;

    sget v2, LX/BS7;->d:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1784833
    return-void
.end method

.method private a(Landroid/graphics/Canvas;Z)V
    .locals 4

    .prologue
    .line 1784817
    invoke-virtual {p0}, LX/BS7;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    .line 1784818
    iget v1, v0, Landroid/graphics/Rect;->left:I

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v2

    invoke-virtual {p0}, LX/BS7;->getIntrinsicWidth()I

    move-result v3

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    .line 1784819
    iget v2, v0, Landroid/graphics/Rect;->top:I

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    invoke-virtual {p0}, LX/BS7;->getIntrinsicHeight()I

    move-result v3

    sub-int/2addr v0, v3

    div-int/lit8 v0, v0, 0x2

    add-int/2addr v2, v0

    .line 1784820
    if-eqz p2, :cond_0

    neg-int v0, v1

    int-to-float v0, v0

    move v1, v0

    :goto_0
    if-eqz p2, :cond_1

    neg-int v0, v2

    int-to-float v0, v0

    :goto_1
    invoke-virtual {p1, v1, v0}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1784821
    return-void

    .line 1784822
    :cond_0
    int-to-float v0, v1

    move v1, v0

    goto :goto_0

    :cond_1
    int-to-float v0, v2

    goto :goto_1
.end method

.method private b()V
    .locals 4

    .prologue
    .line 1784808
    iget-boolean v0, p0, LX/BS7;->g:Z

    if-nez v0, :cond_0

    .line 1784809
    const/4 v0, 0x0

    iput-object v0, p0, LX/BS7;->b:Landroid/graphics/drawable/Drawable;

    .line 1784810
    :goto_0
    return-void

    .line 1784811
    :cond_0
    iget-object v0, p0, LX/BS7;->a:Landroid/content/res/Resources;

    iget-object v1, p0, LX/BS7;->f:LX/2iJ;

    iget v1, v1, LX/2iJ;->shadowDrawableResource:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, LX/BS7;->b:Landroid/graphics/drawable/Drawable;

    .line 1784812
    iget-object v0, p0, LX/BS7;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    iget-object v1, p0, LX/BS7;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    if-eq v0, v1, :cond_1

    .line 1784813
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Shadow height is different than its width: width="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/BS7;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", height="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/BS7;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1784814
    :cond_1
    iget-object v0, p0, LX/BS7;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    iget-object v1, p0, LX/BS7;->f:LX/2iJ;

    iget-object v2, p0, LX/BS7;->a:Landroid/content/res/Resources;

    invoke-virtual {v1, v2}, LX/2iJ;->getFullSize(Landroid/content/res/Resources;)I

    move-result v1

    sub-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    const/4 v1, 0x2

    if-lt v0, v1, :cond_2

    .line 1784815
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected shadow width: Expected "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/BS7;->f:LX/2iJ;

    iget-object v3, p0, LX/BS7;->a:Landroid/content/res/Resources;

    invoke-virtual {v2, v3}, LX/2iJ;->getFullSize(Landroid/content/res/Resources;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " but is actually "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/BS7;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1784816
    :cond_2
    iget-object v0, p0, LX/BS7;->b:Landroid/graphics/drawable/Drawable;

    invoke-static {p0}, LX/BS7;->d(LX/BS7;)I

    move-result v1

    invoke-direct {p0, v1}, LX/BS7;->d(I)Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    goto/16 :goto_0
.end method

.method public static c(LX/BS7;)V
    .locals 2

    .prologue
    .line 1784805
    iget-object v0, p0, LX/BS7;->j:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 1784806
    iget-object v0, p0, LX/BS7;->j:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, LX/BS7;->j:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    invoke-direct {p0, v1}, LX/BS7;->d(I)Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 1784807
    :cond_0
    return-void
.end method

.method public static d(LX/BS7;)I
    .locals 2

    .prologue
    .line 1784804
    iget-boolean v0, p0, LX/BS7;->g:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/BS7;->f:LX/2iJ;

    iget-object v1, p0, LX/BS7;->a:Landroid/content/res/Resources;

    invoke-virtual {v0, v1}, LX/2iJ;->getFullSize(Landroid/content/res/Resources;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/BS7;->f:LX/2iJ;

    iget-object v1, p0, LX/BS7;->a:Landroid/content/res/Resources;

    invoke-virtual {v0, v1}, LX/2iJ;->getFillRadius(Landroid/content/res/Resources;)I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    goto :goto_0
.end method

.method private d(I)Landroid/graphics/Rect;
    .locals 5

    .prologue
    .line 1784801
    invoke-static {p0}, LX/BS7;->d(LX/BS7;)I

    move-result v0

    .line 1784802
    iget-object v1, p0, LX/BS7;->m:Landroid/graphics/Rect;

    sub-int v2, v0, p1

    div-int/lit8 v2, v2, 0x2

    sub-int v3, v0, p1

    div-int/lit8 v3, v3, 0x2

    add-int v4, v0, p1

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v0, p1

    div-int/lit8 v0, v0, 0x2

    invoke-virtual {v1, v2, v3, v4, v0}, Landroid/graphics/Rect;->set(IIII)V

    .line 1784803
    iget-object v0, p0, LX/BS7;->m:Landroid/graphics/Rect;

    return-object v0
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    .prologue
    .line 1784799
    iget-object v0, p0, LX/BS7;->k:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1784800
    return-void
.end method

.method public final a(LX/2iJ;)V
    .locals 2

    .prologue
    .line 1784836
    iput-object p1, p0, LX/BS7;->f:LX/2iJ;

    .line 1784837
    iget-object v0, p0, LX/BS7;->a:Landroid/content/res/Resources;

    iget-object v1, p0, LX/BS7;->f:LX/2iJ;

    iget v1, v1, LX/2iJ;->fillSizeDimen:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, LX/BS7;->l:I

    .line 1784838
    invoke-direct {p0}, LX/BS7;->b()V

    .line 1784839
    invoke-static {p0}, LX/BS7;->c(LX/BS7;)V

    .line 1784840
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 1784796
    iput-boolean p1, p0, LX/BS7;->g:Z

    .line 1784797
    invoke-direct {p0}, LX/BS7;->b()V

    .line 1784798
    return-void
.end method

.method public final b(I)V
    .locals 3

    .prologue
    .line 1784792
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, LX/BS7;->h:Ljava/lang/Integer;

    .line 1784793
    iget-object v0, p0, LX/BS7;->j:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 1784794
    iget-object v0, p0, LX/BS7;->j:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iget-object v1, p0, LX/BS7;->e:LX/0wM;

    iget-object v2, p0, LX/BS7;->h:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v1, v2}, LX/0wM;->a(I)Landroid/graphics/ColorFilter;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 1784795
    :cond_0
    return-void
.end method

.method public final c(I)V
    .locals 3

    .prologue
    .line 1784781
    iget v0, p0, LX/BS7;->i:I

    if-ne v0, p1, :cond_0

    .line 1784782
    :goto_0
    return-void

    .line 1784783
    :cond_0
    if-nez p1, :cond_1

    .line 1784784
    const/4 v0, 0x0

    iput-object v0, p0, LX/BS7;->j:Landroid/graphics/drawable/Drawable;

    .line 1784785
    :goto_1
    iput p1, p0, LX/BS7;->i:I

    goto :goto_0

    .line 1784786
    :cond_1
    iget-object v0, p0, LX/BS7;->a:Landroid/content/res/Resources;

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, LX/BS7;->j:Landroid/graphics/drawable/Drawable;

    .line 1784787
    iget-object v0, p0, LX/BS7;->j:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_2

    .line 1784788
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "mGlyphDrawable is null!"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1784789
    :cond_2
    iget-object v0, p0, LX/BS7;->j:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iget-object v2, p0, LX/BS7;->e:LX/0wM;

    iget-object v0, p0, LX/BS7;->h:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/BS7;->h:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_2
    invoke-virtual {v2, v0}, LX/0wM;->a(I)Landroid/graphics/ColorFilter;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 1784790
    invoke-static {p0}, LX/BS7;->c(LX/BS7;)V

    goto :goto_1

    .line 1784791
    :cond_3
    const/4 v0, -0x1

    goto :goto_2
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    .line 1784772
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/BS7;->a(Landroid/graphics/Canvas;Z)V

    .line 1784773
    iget-object v0, p0, LX/BS7;->b:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 1784774
    iget-object v0, p0, LX/BS7;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1784775
    :cond_0
    invoke-static {p0}, LX/BS7;->d(LX/BS7;)I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    .line 1784776
    int-to-float v1, v0

    int-to-float v0, v0

    iget v2, p0, LX/BS7;->l:I

    int-to-float v2, v2

    iget-object v3, p0, LX/BS7;->k:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v0, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1784777
    iget-object v0, p0, LX/BS7;->j:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    .line 1784778
    iget-object v0, p0, LX/BS7;->j:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1784779
    :cond_1
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, LX/BS7;->a(Landroid/graphics/Canvas;Z)V

    .line 1784780
    return-void
.end method

.method public final getIntrinsicHeight()I
    .locals 1

    .prologue
    .line 1784771
    invoke-static {p0}, LX/BS7;->d(LX/BS7;)I

    move-result v0

    return v0
.end method

.method public final getIntrinsicWidth()I
    .locals 1

    .prologue
    .line 1784770
    invoke-static {p0}, LX/BS7;->d(LX/BS7;)I

    move-result v0

    return v0
.end method

.method public final getOpacity()I
    .locals 1

    .prologue
    .line 1784769
    const/4 v0, 0x0

    return v0
.end method

.method public final setAlpha(I)V
    .locals 1

    .prologue
    .line 1784767
    iget-object v0, p0, LX/BS7;->k:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 1784768
    return-void
.end method

.method public final setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 0

    .prologue
    .line 1784766
    return-void
.end method
