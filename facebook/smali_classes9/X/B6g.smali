.class public LX/B6g;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements LX/B6N;


# instance fields
.field public a:LX/B7W;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/B6l;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/B6k;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Lcom/facebook/widget/FbScrollView;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/TextView;

.field public g:LX/B7F;

.field private h:Landroid/widget/TextView;

.field private i:Landroid/widget/LinearLayout;

.field private j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/B7n;",
            ">;"
        }
    .end annotation
.end field

.field private k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/B7n;",
            ">;"
        }
    .end annotation
.end field

.field private l:Landroid/view/View;

.field private m:LX/B6y;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 1747048
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1747049
    const-class v0, LX/B6g;

    invoke-static {v0, p0}, LX/B6g;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1747050
    const v0, 0x7f0309d0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1747051
    const v0, 0x7f0d18d5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/FbScrollView;

    iput-object v0, p0, LX/B6g;->d:Lcom/facebook/widget/FbScrollView;

    .line 1747052
    const v0, 0x7f0d18d6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/B6g;->e:Landroid/widget/TextView;

    .line 1747053
    const v0, 0x7f0d18d7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/B6g;->f:Landroid/widget/TextView;

    .line 1747054
    const v0, 0x7f0d18d9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/B6g;->h:Landroid/widget/TextView;

    .line 1747055
    const v0, 0x7f0d18d8

    invoke-virtual {p0, v0}, LX/B6g;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/B6g;->i:Landroid/widget/LinearLayout;

    .line 1747056
    const v0, 0x7f0d18da

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/B6g;->l:Landroid/view/View;

    .line 1747057
    new-instance v0, LX/B6y;

    invoke-virtual {p0}, LX/B6g;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, LX/B6g;->l:Landroid/view/View;

    iget-object v3, p0, LX/B6g;->d:Lcom/facebook/widget/FbScrollView;

    invoke-direct {v0, v1, v2, v3}, LX/B6y;-><init>(Landroid/content/Context;Landroid/view/View;Lcom/facebook/widget/FbScrollView;)V

    iput-object v0, p0, LX/B6g;->m:LX/B6y;

    .line 1747058
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/B6g;->j:Ljava/util/List;

    .line 1747059
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/B6g;->k:Ljava/util/List;

    .line 1747060
    iget-object v0, p0, LX/B6g;->m:LX/B6y;

    invoke-virtual {v0}, LX/B6y;->a()V

    .line 1747061
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p1, LX/B6g;

    invoke-static {v3}, LX/B7W;->a(LX/0QB;)LX/B7W;

    move-result-object v1

    check-cast v1, LX/B7W;

    invoke-static {v3}, LX/B6l;->a(LX/0QB;)LX/B6l;

    move-result-object v2

    check-cast v2, LX/B6l;

    const-class p0, LX/B6k;

    invoke-interface {v3, p0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/B6k;

    iput-object v1, p1, LX/B6g;->a:LX/B7W;

    iput-object v2, p1, LX/B6g;->b:LX/B6l;

    iput-object v3, p1, LX/B6g;->c:LX/B6k;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1747045
    iget-object v0, p0, LX/B6g;->m:LX/B6y;

    if-eqz v0, :cond_0

    .line 1747046
    iget-object v0, p0, LX/B6g;->m:LX/B6y;

    invoke-virtual {v0}, LX/B6y;->b()V

    .line 1747047
    :cond_0
    return-void
.end method

.method public final a(LX/B7B;ILX/B7F;ILX/B7w;)V
    .locals 8

    .prologue
    .line 1746968
    check-cast p1, LX/B7E;

    .line 1746969
    iput-object p3, p0, LX/B6g;->g:LX/B7F;

    .line 1746970
    iget-object v0, p0, LX/B6g;->e:Landroid/widget/TextView;

    .line 1746971
    iget-object v1, p1, LX/B7E;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1746972
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1746973
    iget-object v0, p1, LX/B7E;->c:Landroid/text/Spanned;

    move-object v0, v0

    .line 1746974
    if-eqz v0, :cond_2

    .line 1746975
    iget-object v0, p0, LX/B6g;->f:Landroid/widget/TextView;

    .line 1746976
    iget-object v1, p1, LX/B7E;->c:Landroid/text/Spanned;

    move-object v1, v1

    .line 1746977
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1746978
    :goto_0
    iget-object v0, p0, LX/B6g;->f:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 1746979
    iget-object v0, p0, LX/B6g;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 1746980
    iget-object v0, p1, LX/B7E;->d:LX/0Px;

    move-object v2, v0

    .line 1746981
    if-eqz v2, :cond_4

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 1746982
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_4

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLLeadGenLegalContentCheckbox;

    .line 1746983
    new-instance v4, LX/B7n;

    invoke-virtual {p0}, LX/B6g;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v4, v5}, LX/B7n;-><init>(Landroid/content/Context;)V

    .line 1746984
    const/16 p5, 0x12

    .line 1746985
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLLeadGenLegalContentCheckbox;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v6

    .line 1746986
    const-string v5, ""

    .line 1746987
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLLeadGenLegalContentCheckbox;->l()Z

    move-result v7

    if-nez v7, :cond_0

    .line 1746988
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v7, " ("

    invoke-direct {v5, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/B6g;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const p2, 0x7f0824e7

    invoke-virtual {v7, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, ")"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1746989
    :cond_0
    new-instance v7, Landroid/text/SpannableString;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p2

    invoke-virtual {p2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {v7, p2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 1746990
    new-instance p2, Landroid/text/style/ForegroundColorSpan;

    invoke-virtual {p0}, LX/B6g;->getResources()Landroid/content/res/Resources;

    move-result-object p3

    const p4, 0x7f0a00a3

    invoke-virtual {p3, p4}, Landroid/content/res/Resources;->getColor(I)I

    move-result p3

    invoke-direct {p2, p3}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/4 p3, 0x0

    invoke-static {v6}, LX/1z7;->a(Ljava/lang/String;)I

    move-result p4

    invoke-virtual {v7, p2, p3, p4, p5}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 1746991
    new-instance p2, Landroid/text/style/ForegroundColorSpan;

    invoke-virtual {p0}, LX/B6g;->getResources()Landroid/content/res/Resources;

    move-result-object p3

    const p4, 0x7f0a00a3

    invoke-virtual {p3, p4}, Landroid/content/res/Resources;->getColor(I)I

    move-result p3

    invoke-direct {p2, p3}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-static {v6}, LX/1z7;->a(Ljava/lang/String;)I

    move-result p3

    invoke-static {v6}, LX/1z7;->a(Ljava/lang/String;)I

    move-result v6

    invoke-static {v5}, LX/1z7;->a(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v5, v6

    invoke-virtual {v7, p2, p3, v5, p5}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 1746992
    move-object v5, v7

    .line 1746993
    invoke-virtual {v4, v5}, LX/B7n;->a(Ljava/lang/CharSequence;)V

    .line 1746994
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLLeadGenLegalContentCheckbox;->k()Z

    move-result v5

    invoke-virtual {v4, v5}, LX/B7n;->setChecked(Z)V

    .line 1746995
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLLeadGenLegalContentCheckbox;->l()Z

    move-result v5

    .line 1746996
    iput-boolean v5, v4, LX/B7n;->e:Z

    .line 1746997
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLLeadGenLegalContentCheckbox;->m()Ljava/lang/String;

    move-result-object v5

    .line 1746998
    iput-object v5, v4, LX/B7n;->f:Ljava/lang/String;

    .line 1746999
    iget-object v5, p0, LX/B6g;->k:Ljava/util/List;

    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1747000
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLLeadGenLegalContentCheckbox;->l()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1747001
    iget-object v0, p0, LX/B6g;->j:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1747002
    :cond_1
    iget-object v0, p0, LX/B6g;->i:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1747003
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_1

    .line 1747004
    :cond_2
    iget-object v0, p0, LX/B6g;->f:Landroid/widget/TextView;

    const/4 v3, 0x0

    .line 1747005
    iget-object v1, p1, LX/B7E;->b:LX/0Px;

    move-object v2, v1

    .line 1747006
    new-instance v4, Landroid/text/SpannableString;

    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 1747007
    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->b()LX/0Px;

    move-result-object v5

    .line 1747008
    if-eqz v5, :cond_3

    move v2, v3

    .line 1747009
    :goto_2
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v1

    if-ge v2, v1, :cond_3

    .line 1747010
    invoke-virtual {v5, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEntityAtRange;

    .line 1747011
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEntityAtRange;->j()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLEntity;->j()Ljava/lang/String;

    move-result-object v6

    .line 1747012
    new-instance v7, LX/B6d;

    invoke-direct {v7, p0, v6}, LX/B6d;-><init>(LX/B6g;Ljava/lang/String;)V

    .line 1747013
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEntityAtRange;->c()I

    move-result v6

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEntityAtRange;->c()I

    move-result p2

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEntityAtRange;->b()I

    move-result v1

    add-int/2addr v1, p2

    const/16 p2, 0x11

    invoke-virtual {v4, v7, v6, v1, p2}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 1747014
    new-instance v1, Landroid/text/style/ForegroundColorSpan;

    invoke-virtual {p0}, LX/B6g;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0a00a1

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    invoke-direct {v1, v6}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v4}, Landroid/text/SpannableString;->length()I

    move-result v6

    const/16 v7, 0x21

    invoke-virtual {v4, v1, v3, v6, v7}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 1747015
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_2

    .line 1747016
    :cond_3
    move-object v1, v4

    .line 1747017
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 1747018
    :cond_4
    iget-object v0, p0, LX/B6g;->h:Landroid/widget/TextView;

    const/4 v5, 0x0

    .line 1747019
    new-instance v1, Landroid/text/SpannableString;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 1747020
    iget-object v3, p1, LX/B7E;->i:Ljava/lang/String;

    move-object v3, v3

    .line 1747021
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 1747022
    iget-object v3, p1, LX/B7E;->e:Ljava/lang/String;

    move-object v3, v3

    .line 1747023
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 1747024
    iget-object v3, p1, LX/B7E;->h:Ljava/lang/String;

    move-object v3, v3

    .line 1747025
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 1747026
    new-instance v2, LX/B6e;

    invoke-direct {v2, p0, p1}, LX/B6e;-><init>(LX/B6g;LX/B7E;)V

    .line 1747027
    iget-object v3, p1, LX/B7E;->i:Ljava/lang/String;

    move-object v3, v3

    .line 1747028
    invoke-static {v3}, LX/1z7;->a(Ljava/lang/String;)I

    move-result v3

    .line 1747029
    iget-object v4, p1, LX/B7E;->e:Ljava/lang/String;

    move-object v4, v4

    .line 1747030
    invoke-static {v4}, LX/1z7;->a(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v4, v3

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 1747031
    new-instance v2, LX/B6f;

    invoke-direct {v2, p0, p1}, LX/B6f;-><init>(LX/B6g;LX/B7E;)V

    .line 1747032
    iget-object v3, p1, LX/B7E;->i:Ljava/lang/String;

    move-object v3, v3

    .line 1747033
    invoke-static {v3}, LX/1z7;->a(Ljava/lang/String;)I

    move-result v3

    .line 1747034
    iget-object v4, p1, LX/B7E;->e:Ljava/lang/String;

    move-object v4, v4

    .line 1747035
    invoke-static {v4}, LX/1z7;->a(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    add-int/lit8 v3, v3, 0x1

    .line 1747036
    iget-object v4, p1, LX/B7E;->h:Ljava/lang/String;

    move-object v4, v4

    .line 1747037
    invoke-static {v4}, LX/1z7;->a(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v4, v3

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 1747038
    new-instance v2, Landroid/text/style/ForegroundColorSpan;

    invoke-virtual {p0}, LX/B6g;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a00a1

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-direct {v2, v3}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v1}, Landroid/text/SpannableString;->length()I

    move-result v3

    const/16 v4, 0x21

    invoke-virtual {v1, v2, v5, v3, v4}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 1747039
    move-object v1, v1

    .line 1747040
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1747041
    iget-object v0, p0, LX/B6g;->h:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 1747042
    iget-object v0, p0, LX/B6g;->d:Lcom/facebook/widget/FbScrollView;

    invoke-virtual {v0}, Lcom/facebook/widget/FbScrollView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, LX/B6b;

    invoke-direct {v1, p0}, LX/B6b;-><init>(LX/B6g;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 1747043
    iget-object v0, p0, LX/B6g;->d:Lcom/facebook/widget/FbScrollView;

    invoke-virtual {v0}, Lcom/facebook/widget/FbScrollView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, LX/B6c;

    invoke-direct {v1, p0}, LX/B6c;-><init>(LX/B6g;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnScrollChangedListener(Landroid/view/ViewTreeObserver$OnScrollChangedListener;)V

    .line 1747044
    return-void
.end method

.method public final a(Lcom/facebook/leadgen/cache/LeadGenFormPendingInputEntry;)V
    .locals 0

    .prologue
    .line 1747062
    return-void
.end method

.method public final b()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/B8s;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1746967
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c()LX/0P1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1746961
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 1746962
    iget-object v0, p0, LX/B6g;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/B7n;

    .line 1746963
    iget-object v3, p0, LX/B6g;->j:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1746964
    iget-object v3, v0, LX/B7n;->f:Ljava/lang/String;

    move-object v3, v3

    .line 1746965
    invoke-virtual {v0}, LX/B7n;->getInputValue()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1746966
    :cond_1
    invoke-static {v1}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    return-object v0
.end method

.method public getContentScrollView()Lcom/facebook/widget/FbScrollView;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1746960
    iget-object v0, p0, LX/B6g;->d:Lcom/facebook/widget/FbScrollView;

    return-object v0
.end method

.method public final l_(I)LX/B77;
    .locals 4

    .prologue
    .line 1746951
    sget-object v1, LX/B77;->NO_ERROR:LX/B77;

    .line 1746952
    const/4 v0, 0x0

    move-object v2, v1

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/B6g;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1746953
    iget-object v0, p0, LX/B6g;->j:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/B7n;

    .line 1746954
    invoke-virtual {v0}, LX/B7n;->getInputValue()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1746955
    invoke-virtual {v0}, LX/B7n;->getErrorMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/B7n;->a(Ljava/lang/String;)V

    .line 1746956
    sget-object v2, LX/B77;->PRIVACY_CHECKBOX_ERROR:LX/B77;

    .line 1746957
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1746958
    :cond_0
    invoke-virtual {v0}, LX/B7n;->c()V

    goto :goto_1

    .line 1746959
    :cond_1
    return-object v2
.end method
