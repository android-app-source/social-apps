.class public final enum LX/An0;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/An0;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/An0;

.field public static final enum BLINGBAR:LX/An0;

.field public static final enum FOOTER:LX/An0;

.field public static final enum INLINE_COMMENT_COMPOSER:LX/An0;

.field public static final enum INLINE_COMMENT_PREVIEW:LX/An0;

.field public static final enum MESSAGE:LX/An0;

.field public static final enum PHOTOS_FEED_BLINGBAR:LX/An0;

.field public static final enum PHOTOS_FEED_FOOTER:LX/An0;

.field public static final enum SNACKBAR_VIEW:LX/An0;

.field public static final enum STICKER_KEYBOARD_SELECT:LX/An0;

.field public static final enum STICKER_SELECT:LX/An0;


# instance fields
.field public final navigationTapPoint:Ljava/lang/String;

.field public final nectarModule:Ljava/lang/String;

.field public final scrollToBottomOnFirstLoad:Z

.field public final showKeyboardOnFirstLoad:Z

.field public final showStickerKeyboardOnFirstLoad:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    .line 1711728
    new-instance v0, LX/An0;

    const-string v1, "SNACKBAR_VIEW"

    const/4 v2, 0x0

    const-string v3, "snackbar_comment_flyout"

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    const-string v7, "tap_snackbar_view"

    invoke-direct/range {v0 .. v7}, LX/An0;-><init>(Ljava/lang/String;ILjava/lang/String;ZZZLjava/lang/String;)V

    sput-object v0, LX/An0;->SNACKBAR_VIEW:LX/An0;

    .line 1711729
    new-instance v0, LX/An0;

    const-string v1, "STICKER_SELECT"

    const/4 v2, 0x1

    const-string v3, "newsfeed_ufi"

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    const-string v7, "tap_footer_comment"

    invoke-direct/range {v0 .. v7}, LX/An0;-><init>(Ljava/lang/String;ILjava/lang/String;ZZZLjava/lang/String;)V

    sput-object v0, LX/An0;->STICKER_SELECT:LX/An0;

    .line 1711730
    new-instance v0, LX/An0;

    const-string v1, "STICKER_KEYBOARD_SELECT"

    const/4 v2, 0x2

    const-string v3, "newsfeed_ufi"

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x1

    const-string v7, "tap_footer_comment"

    invoke-direct/range {v0 .. v7}, LX/An0;-><init>(Ljava/lang/String;ILjava/lang/String;ZZZLjava/lang/String;)V

    sput-object v0, LX/An0;->STICKER_KEYBOARD_SELECT:LX/An0;

    .line 1711731
    new-instance v0, LX/An0;

    const-string v1, "FOOTER"

    const/4 v2, 0x3

    const-string v3, "newsfeed_ufi"

    const/4 v4, 0x1

    const-string v5, "tap_footer_comment"

    invoke-direct/range {v0 .. v5}, LX/An0;-><init>(Ljava/lang/String;ILjava/lang/String;ZLjava/lang/String;)V

    sput-object v0, LX/An0;->FOOTER:LX/An0;

    .line 1711732
    new-instance v0, LX/An0;

    const-string v1, "BLINGBAR"

    const/4 v2, 0x4

    const-string v3, "newsfeed_blingbar"

    const/4 v4, 0x0

    const-string v5, "tap_bling_bar_comment"

    invoke-direct/range {v0 .. v5}, LX/An0;-><init>(Ljava/lang/String;ILjava/lang/String;ZLjava/lang/String;)V

    sput-object v0, LX/An0;->BLINGBAR:LX/An0;

    .line 1711733
    new-instance v0, LX/An0;

    const-string v1, "MESSAGE"

    const/4 v2, 0x5

    const-string v3, "story_message_flyout"

    const/4 v4, 0x0

    const-string v5, "tap_message_comment"

    invoke-direct/range {v0 .. v5}, LX/An0;-><init>(Ljava/lang/String;ILjava/lang/String;ZLjava/lang/String;)V

    sput-object v0, LX/An0;->MESSAGE:LX/An0;

    .line 1711734
    new-instance v0, LX/An0;

    const-string v1, "INLINE_COMMENT_PREVIEW"

    const/4 v2, 0x6

    const-string v3, "feed_inline_comments"

    const/4 v4, 0x0

    const-string v5, "tap_feed_inline_comment"

    invoke-direct/range {v0 .. v5}, LX/An0;-><init>(Ljava/lang/String;ILjava/lang/String;ZLjava/lang/String;)V

    sput-object v0, LX/An0;->INLINE_COMMENT_PREVIEW:LX/An0;

    .line 1711735
    new-instance v0, LX/An0;

    const-string v1, "INLINE_COMMENT_COMPOSER"

    const/4 v2, 0x7

    const-string v3, "feed_inline_comment_composer"

    const/4 v4, 0x1

    const-string v5, "tap_feed_inline_comment"

    invoke-direct/range {v0 .. v5}, LX/An0;-><init>(Ljava/lang/String;ILjava/lang/String;ZLjava/lang/String;)V

    sput-object v0, LX/An0;->INLINE_COMMENT_COMPOSER:LX/An0;

    .line 1711736
    new-instance v0, LX/An0;

    const-string v1, "PHOTOS_FEED_FOOTER"

    const/16 v2, 0x8

    const-string v3, "photos_feed_ufi"

    const/4 v4, 0x0

    const-string v5, "tap_photos_feed_footer_comment"

    invoke-direct/range {v0 .. v5}, LX/An0;-><init>(Ljava/lang/String;ILjava/lang/String;ZLjava/lang/String;)V

    sput-object v0, LX/An0;->PHOTOS_FEED_FOOTER:LX/An0;

    .line 1711737
    new-instance v0, LX/An0;

    const-string v1, "PHOTOS_FEED_BLINGBAR"

    const/16 v2, 0x9

    const-string v3, "photos_feed_blingbar"

    const/4 v4, 0x0

    const-string v5, "tap_photos_feed_bling_bar_comment"

    invoke-direct/range {v0 .. v5}, LX/An0;-><init>(Ljava/lang/String;ILjava/lang/String;ZLjava/lang/String;)V

    sput-object v0, LX/An0;->PHOTOS_FEED_BLINGBAR:LX/An0;

    .line 1711738
    const/16 v0, 0xa

    new-array v0, v0, [LX/An0;

    const/4 v1, 0x0

    sget-object v2, LX/An0;->SNACKBAR_VIEW:LX/An0;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, LX/An0;->STICKER_SELECT:LX/An0;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, LX/An0;->STICKER_KEYBOARD_SELECT:LX/An0;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, LX/An0;->FOOTER:LX/An0;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, LX/An0;->BLINGBAR:LX/An0;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, LX/An0;->MESSAGE:LX/An0;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/An0;->INLINE_COMMENT_PREVIEW:LX/An0;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/An0;->INLINE_COMMENT_COMPOSER:LX/An0;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/An0;->PHOTOS_FEED_FOOTER:LX/An0;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/An0;->PHOTOS_FEED_BLINGBAR:LX/An0;

    aput-object v2, v0, v1

    sput-object v0, LX/An0;->$VALUES:[LX/An0;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;ZLjava/lang/String;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Z",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 1711726
    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move v4, p4

    move v6, v5

    move-object v7, p5

    invoke-direct/range {v0 .. v7}, LX/An0;-><init>(Ljava/lang/String;ILjava/lang/String;ZZZLjava/lang/String;)V

    .line 1711727
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;ZZZLjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "ZZZ",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1711739
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1711740
    iput-object p3, p0, LX/An0;->nectarModule:Ljava/lang/String;

    .line 1711741
    iput-boolean p4, p0, LX/An0;->showKeyboardOnFirstLoad:Z

    .line 1711742
    iput-boolean p5, p0, LX/An0;->scrollToBottomOnFirstLoad:Z

    .line 1711743
    iput-boolean p6, p0, LX/An0;->showStickerKeyboardOnFirstLoad:Z

    .line 1711744
    iput-object p7, p0, LX/An0;->navigationTapPoint:Ljava/lang/String;

    .line 1711745
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/An0;
    .locals 1

    .prologue
    .line 1711725
    const-class v0, LX/An0;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/An0;

    return-object v0
.end method

.method public static values()[LX/An0;
    .locals 1

    .prologue
    .line 1711724
    sget-object v0, LX/An0;->$VALUES:[LX/An0;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/An0;

    return-object v0
.end method
