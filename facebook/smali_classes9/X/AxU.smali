.class public final LX/AxU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/friendsharing/souvenirclassifier/graphql/FBSouvenirClassifierFragmentsModels$FBSouvenirClassifierFeaturesQueryModel;",
        ">;",
        "Lcom/facebook/friendsharing/souvenirclassifier/models/SouvenirClassifierServerFeatures;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/AxW;


# direct methods
.method public constructor <init>(LX/AxW;)V
    .locals 0

    .prologue
    .line 1728319
    iput-object p1, p0, LX/AxU;->a:LX/AxW;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1728320
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v6, 0x0

    .line 1728321
    new-instance v1, Lcom/facebook/friendsharing/souvenirclassifier/models/SouvenirClassifierServerFeatures;

    invoke-direct {v1}, Lcom/facebook/friendsharing/souvenirclassifier/models/SouvenirClassifierServerFeatures;-><init>()V

    .line 1728322
    if-eqz p1, :cond_2

    .line 1728323
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1728324
    check-cast v0, Lcom/facebook/friendsharing/souvenirclassifier/graphql/FBSouvenirClassifierFragmentsModels$FBSouvenirClassifierFeaturesQueryModel;

    invoke-virtual {v0}, Lcom/facebook/friendsharing/souvenirclassifier/graphql/FBSouvenirClassifierFragmentsModels$FBSouvenirClassifierFeaturesQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x589340c2

    invoke-static {v2, v0, v6, v3}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_0
    invoke-virtual {v0, v6}, LX/2uF;->a(I)LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v2, v0, v6}, LX/15i;->g(II)I

    move-result v0

    const v3, 0x537097f7

    invoke-static {v2, v0, v6, v3}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    .line 1728325
    if-eqz v0, :cond_1

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    .line 1728326
    :goto_1
    invoke-virtual {v0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, LX/2sN;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, LX/2sN;->b()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    .line 1728327
    invoke-virtual {v3, v2, v6}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v3, v2, v5}, LX/15i;->l(II)D

    move-result-wide v2

    invoke-virtual {v1, v4, v2, v3}, Lcom/facebook/friendsharing/souvenirclassifier/models/SouvenirClassifierServerFeatures;->put(Ljava/lang/String;D)V

    goto :goto_2

    .line 1728328
    :cond_0
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_0

    .line 1728329
    :cond_1
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_1

    .line 1728330
    :cond_2
    return-object v1
.end method
