.class public final LX/B8I;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/TextView$OnEditorActionListener;


# instance fields
.field public final synthetic a:LX/B8K;


# direct methods
.method public constructor <init>(LX/B8K;)V
    .locals 0

    .prologue
    .line 1748667
    iput-object p1, p0, LX/B8I;->a:LX/B8K;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 1748653
    const/4 v1, 0x6

    if-ne p2, v1, :cond_0

    .line 1748654
    iget-object v1, p0, LX/B8I;->a:LX/B8K;

    iget-object v1, v1, LX/B8K;->c:LX/B8r;

    iget-object v2, p0, LX/B8I;->a:LX/B8K;

    iget-object v2, v2, LX/B8K;->e:Lcom/facebook/resources/ui/FbAutoCompleteTextView;

    invoke-virtual {v2}, Lcom/facebook/resources/ui/FbAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/B8I;->a:LX/B8K;

    iget-object v3, v3, LX/B8K;->f:Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;

    invoke-virtual {v1, v2, v3}, LX/B8r;->a(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1748655
    invoke-virtual {p1}, Landroid/widget/TextView;->clearFocus()V

    .line 1748656
    iget-object v1, p0, LX/B8I;->a:LX/B8K;

    iget-object v1, v1, LX/B8K;->j:LX/B7F;

    iget-object v2, p0, LX/B8I;->a:LX/B8K;

    iget-object v2, v2, LX/B8K;->f:Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;

    invoke-static {v2}, LX/2sb;->a(Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;)LX/B77;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/B7F;->a(LX/B77;)Ljava/lang/String;

    move-result-object v1

    .line 1748657
    iget-object v2, p0, LX/B8I;->a:LX/B8K;

    invoke-virtual {v2, v1}, LX/B8K;->a(Ljava/lang/String;)V

    .line 1748658
    iget-object v1, p0, LX/B8I;->a:LX/B8K;

    invoke-virtual {v1}, LX/B8K;->b()V

    .line 1748659
    :goto_0
    return v0

    .line 1748660
    :cond_0
    const/4 v1, 0x5

    if-ne p2, v1, :cond_1

    .line 1748661
    iget-object v1, p0, LX/B8I;->a:LX/B8K;

    iget-object v1, v1, LX/B8K;->c:LX/B8r;

    iget-object v2, p0, LX/B8I;->a:LX/B8K;

    iget-object v2, v2, LX/B8K;->e:Lcom/facebook/resources/ui/FbAutoCompleteTextView;

    invoke-virtual {v2}, Lcom/facebook/resources/ui/FbAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/B8I;->a:LX/B8K;

    iget-object v3, v3, LX/B8K;->f:Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;

    invoke-virtual {v1, v2, v3}, LX/B8r;->a(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1748662
    invoke-virtual {p1}, Landroid/widget/TextView;->clearFocus()V

    .line 1748663
    iget-object v1, p0, LX/B8I;->a:LX/B8K;

    iget-object v1, v1, LX/B8K;->j:LX/B7F;

    iget-object v2, p0, LX/B8I;->a:LX/B8K;

    iget-object v2, v2, LX/B8K;->f:Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;

    invoke-static {v2}, LX/2sb;->a(Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;)LX/B77;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/B7F;->a(LX/B77;)Ljava/lang/String;

    move-result-object v1

    .line 1748664
    iget-object v2, p0, LX/B8I;->a:LX/B8K;

    invoke-virtual {v2, v1}, LX/B8K;->a(Ljava/lang/String;)V

    .line 1748665
    iget-object v1, p0, LX/B8I;->a:LX/B8K;

    invoke-virtual {v1}, LX/B8K;->b()V

    goto :goto_0

    .line 1748666
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
