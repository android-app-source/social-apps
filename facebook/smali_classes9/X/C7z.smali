.class public final LX/C7z;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6WM;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLGroup;

.field public final synthetic b:Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;

.field public final synthetic c:LX/0Pz;

.field public final synthetic d:LX/0Px;

.field public final synthetic e:Lcom/facebook/fig/mediagrid/FigMediaGrid;

.field public final synthetic f:Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomePhotoGridSpec;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomePhotoGridSpec;Lcom/facebook/graphql/model/GraphQLGroup;Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;LX/0Pz;LX/0Px;Lcom/facebook/fig/mediagrid/FigMediaGrid;)V
    .locals 0

    .prologue
    .line 1852119
    iput-object p1, p0, LX/C7z;->f:Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomePhotoGridSpec;

    iput-object p2, p0, LX/C7z;->a:Lcom/facebook/graphql/model/GraphQLGroup;

    iput-object p3, p0, LX/C7z;->b:Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;

    iput-object p4, p0, LX/C7z;->c:LX/0Pz;

    iput-object p5, p0, LX/C7z;->d:LX/0Px;

    iput-object p6, p0, LX/C7z;->e:Lcom/facebook/fig/mediagrid/FigMediaGrid;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(ILX/1aZ;Landroid/graphics/Rect;)V
    .locals 4

    .prologue
    .line 1852120
    iget-object v0, p0, LX/C7z;->f:Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomePhotoGridSpec;

    iget-object v0, v0, Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomePhotoGridSpec;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1g8;

    iget-object v1, p0, LX/C7z;->a:Lcom/facebook/graphql/model/GraphQLGroup;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGroup;->t()Ljava/lang/String;

    move-result-object v1

    const-string v2, "gmw_unit_photo_click"

    invoke-virtual {v0, v1, v2}, LX/1g8;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1852121
    new-instance v0, LX/9hE;

    iget-object v1, p0, LX/C7z;->b:Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;

    invoke-direct {v0, v1}, LX/9hE;-><init>(Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;)V

    iget-object v1, p0, LX/C7z;->c:LX/0Pz;

    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/9hD;->a(LX/0Px;)LX/9hD;

    move-result-object v0

    sget-object v1, LX/74S;->NEWSFEED:LX/74S;

    invoke-virtual {v0, v1}, LX/9hD;->a(LX/74S;)LX/9hD;

    move-result-object v0

    const/4 v1, 0x0

    .line 1852122
    iput-boolean v1, v0, LX/9hD;->m:Z

    .line 1852123
    move-object v1, v0

    .line 1852124
    iget-object v0, p0, LX/C7z;->d:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMedia;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/9hD;->a(Ljava/lang/String;)LX/9hD;

    move-result-object v1

    iget-object v0, p0, LX/C7z;->d:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMedia;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->aa()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, LX/1bf;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/9hD;->a(LX/1bf;)LX/9hD;

    move-result-object v0

    iget-object v1, p0, LX/C7z;->a:Lcom/facebook/graphql/model/GraphQLGroup;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGroup;->t()Ljava/lang/String;

    move-result-object v1

    .line 1852125
    iput-object v1, v0, LX/9hD;->y:Ljava/lang/String;

    .line 1852126
    move-object v0, v0

    .line 1852127
    const v1, 0x41e065f

    .line 1852128
    iput v1, v0, LX/9hD;->x:I

    .line 1852129
    move-object v0, v0

    .line 1852130
    invoke-virtual {v0}, LX/9hD;->b()Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    move-result-object v1

    .line 1852131
    iget-object v0, p0, LX/C7z;->f:Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomePhotoGridSpec;

    iget-object v0, v0, Lcom/facebook/feedplugins/groupmemberwelcome/GroupMemberWelcomePhotoGridSpec;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/23R;

    iget-object v2, p0, LX/C7z;->e:Lcom/facebook/fig/mediagrid/FigMediaGrid;

    invoke-virtual {v2}, Lcom/facebook/fig/mediagrid/FigMediaGrid;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v0, v2, v1, v3}, LX/23R;->a(Landroid/content/Context;Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;LX/9hN;)V

    .line 1852132
    return-void
.end method
