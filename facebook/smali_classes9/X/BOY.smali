.class public final LX/BOY;
.super Ljava/util/HashMap;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/HashMap",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:LX/BOa;

.field public final synthetic val$charityChosenValue:LX/BOS;

.field public final synthetic val$charityId:Ljava/lang/String;

.field public final synthetic val$charityName:Ljava/lang/String;

.field public final synthetic val$isDAFCharity:Z


# direct methods
.method public constructor <init>(LX/BOa;Ljava/lang/String;Ljava/lang/String;LX/BOS;Z)V
    .locals 2

    .prologue
    .line 1779846
    iput-object p1, p0, LX/BOY;->this$0:LX/BOa;

    iput-object p2, p0, LX/BOY;->val$charityId:Ljava/lang/String;

    iput-object p3, p0, LX/BOY;->val$charityName:Ljava/lang/String;

    iput-object p4, p0, LX/BOY;->val$charityChosenValue:LX/BOS;

    iput-boolean p5, p0, LX/BOY;->val$isDAFCharity:Z

    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    .line 1779847
    const-string v0, "charity_id_selected"

    iget-object v1, p0, LX/BOY;->val$charityId:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, LX/BOY;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1779848
    const-string v0, "charity_name_selected"

    iget-object v1, p0, LX/BOY;->val$charityName:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, LX/BOY;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1779849
    const-string v0, "charity_chosen_from"

    iget-object v1, p0, LX/BOY;->val$charityChosenValue:LX/BOS;

    invoke-virtual {v1}, LX/BOS;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, LX/BOY;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1779850
    const-string v0, "is_daf_charity"

    iget-boolean v1, p0, LX/BOY;->val$isDAFCharity:Z

    invoke-static {v1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, LX/BOY;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1779851
    return-void
.end method
