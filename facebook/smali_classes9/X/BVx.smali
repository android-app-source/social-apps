.class public final enum LX/BVx;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/BVx;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/BVx;

.field public static final enum MARGIN_BOTTOM:LX/BVx;

.field public static final enum MARGIN_LEFT:LX/BVx;

.field public static final enum MARGIN_RIGHT:LX/BVx;

.field public static final enum MARGIN_TOP:LX/BVx;


# instance fields
.field private final mValue:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1791536
    new-instance v0, LX/BVx;

    const-string v1, "MARGIN_TOP"

    const-string v2, "marginTop"

    invoke-direct {v0, v1, v3, v2}, LX/BVx;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BVx;->MARGIN_TOP:LX/BVx;

    .line 1791537
    new-instance v0, LX/BVx;

    const-string v1, "MARGIN_RIGHT"

    const-string v2, "marginRight"

    invoke-direct {v0, v1, v4, v2}, LX/BVx;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BVx;->MARGIN_RIGHT:LX/BVx;

    .line 1791538
    new-instance v0, LX/BVx;

    const-string v1, "MARGIN_BOTTOM"

    const-string v2, "marginBottom"

    invoke-direct {v0, v1, v5, v2}, LX/BVx;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BVx;->MARGIN_BOTTOM:LX/BVx;

    .line 1791539
    new-instance v0, LX/BVx;

    const-string v1, "MARGIN_LEFT"

    const-string v2, "marginLeft"

    invoke-direct {v0, v1, v6, v2}, LX/BVx;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BVx;->MARGIN_LEFT:LX/BVx;

    .line 1791540
    const/4 v0, 0x4

    new-array v0, v0, [LX/BVx;

    sget-object v1, LX/BVx;->MARGIN_TOP:LX/BVx;

    aput-object v1, v0, v3

    sget-object v1, LX/BVx;->MARGIN_RIGHT:LX/BVx;

    aput-object v1, v0, v4

    sget-object v1, LX/BVx;->MARGIN_BOTTOM:LX/BVx;

    aput-object v1, v0, v5

    sget-object v1, LX/BVx;->MARGIN_LEFT:LX/BVx;

    aput-object v1, v0, v6

    sput-object v0, LX/BVx;->$VALUES:[LX/BVx;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1791541
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1791542
    iput-object p3, p0, LX/BVx;->mValue:Ljava/lang/String;

    .line 1791543
    return-void
.end method

.method public static from(Ljava/lang/String;)LX/BVx;
    .locals 4

    .prologue
    .line 1791544
    invoke-static {}, LX/BVx;->values()[LX/BVx;

    move-result-object v2

    .line 1791545
    const/4 v0, 0x0

    :goto_0
    array-length v1, v2

    if-ge v0, v1, :cond_1

    .line 1791546
    aget-object v1, v2, v0

    .line 1791547
    iget-object v3, v1, LX/BVx;->mValue:Ljava/lang/String;

    invoke-virtual {v3, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move-object v0, v1

    .line 1791548
    :goto_1
    return-object v0

    .line 1791549
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1791550
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)LX/BVx;
    .locals 1

    .prologue
    .line 1791551
    const-class v0, LX/BVx;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/BVx;

    return-object v0
.end method

.method public static values()[LX/BVx;
    .locals 1

    .prologue
    .line 1791552
    sget-object v0, LX/BVx;->$VALUES:[LX/BVx;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/BVx;

    return-object v0
.end method
