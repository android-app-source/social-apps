.class public final LX/BR8;
.super LX/63W;
.source ""


# instance fields
.field public final synthetic a:LX/BRg;

.field public final synthetic b:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;LX/BRg;)V
    .locals 0

    .prologue
    .line 1783223
    iput-object p1, p0, LX/BR8;->b:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    iput-object p2, p0, LX/BR8;->a:LX/BRg;

    invoke-direct {p0}, LX/63W;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V
    .locals 10

    .prologue
    .line 1783224
    iget-object v0, p0, LX/BR8;->b:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    iget-object v0, v0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->w:LX/BQo;

    iget-object v1, p0, LX/BR8;->b:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    iget-object v1, v1, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->k:LX/BR1;

    iget-object v1, v1, LX/BR1;->g:Ljava/lang/String;

    iget-object v2, p0, LX/BR8;->b:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    iget-object v2, v2, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->k:LX/BR1;

    iget-object v2, v2, LX/BR1;->f:Ljava/lang/String;

    iget-object v3, p0, LX/BR8;->b:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    iget-object v3, v3, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->n:LX/BRP;

    .line 1783225
    iget-object v4, v3, LX/BRP;->a:Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;

    move-object v3, v4

    .line 1783226
    invoke-virtual {v3}, Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;->f()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/BR8;->b:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    iget-object v4, v4, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->E:Ljava/lang/String;

    iget-object v5, p0, LX/BR8;->b:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    iget-object v5, v5, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->D:Ljava/lang/String;

    iget-object v6, p0, LX/BR8;->a:LX/BRg;

    invoke-virtual {v6}, LX/BRg;->a()J

    move-result-wide v6

    .line 1783227
    iget-object v8, v0, LX/BQo;->a:LX/0Zb;

    const-string v9, "staging_ground_tap_use"

    const/4 p1, 0x0

    invoke-interface {v8, v9, p1}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v8

    .line 1783228
    invoke-virtual {v8}, LX/0oG;->a()Z

    move-result v9

    if-eqz v9, :cond_0

    .line 1783229
    iget-object v9, v0, LX/BQo;->b:Ljava/lang/String;

    invoke-virtual {v8, v9}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 1783230
    const-string v9, "heisman_composer_session_id"

    invoke-virtual {v8, v9, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1783231
    const-string v9, "picture_id"

    invoke-virtual {v8, v9, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1783232
    const-string v9, "profile_pic_frame_id"

    invoke-virtual {v8, v9, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1783233
    const-string v9, "use_cropping"

    invoke-virtual {v8, v9, v4}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1783234
    const-string v9, "enter_cropping"

    invoke-virtual {v8, v9, v5}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1783235
    const-string v9, "expiration_time_duration"

    invoke-virtual {v8, v9, v6, v7}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 1783236
    invoke-virtual {v8}, LX/0oG;->d()V

    .line 1783237
    :cond_0
    iget-object v0, p0, LX/BR8;->b:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    iget-object v0, v0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->g:Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;

    if-nez v0, :cond_2

    .line 1783238
    iget-object v0, p0, LX/BR8;->b:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    new-instance v1, LX/5Ru;

    invoke-direct {v1}, LX/5Ru;-><init>()V

    iget-object v2, p0, LX/BR8;->b:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    iget-object v2, v2, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->h:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 1783239
    iput-object v2, v1, LX/5Ru;->d:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 1783240
    move-object v1, v1

    .line 1783241
    iget-object v2, p0, LX/BR8;->b:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    iget-object v2, v2, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->k:LX/BR1;

    iget-object v2, v2, LX/BR1;->a:Landroid/net/Uri;

    .line 1783242
    iput-object v2, v1, LX/5Ru;->a:Landroid/net/Uri;

    .line 1783243
    move-object v1, v1

    .line 1783244
    iget-object v2, p0, LX/BR8;->b:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    iget-object v2, v2, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->k:LX/BR1;

    iget-object v2, v2, LX/BR1;->f:Ljava/lang/String;

    .line 1783245
    iput-object v2, v1, LX/5Ru;->e:Ljava/lang/String;

    .line 1783246
    move-object v1, v1

    .line 1783247
    iget-object v2, p0, LX/BR8;->b:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    iget-object v2, v2, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->k:LX/BR1;

    iget-object v2, v2, LX/BR1;->c:Landroid/graphics/RectF;

    .line 1783248
    iput-object v2, v1, LX/5Ru;->f:Landroid/graphics/RectF;

    .line 1783249
    move-object v1, v1

    .line 1783250
    invoke-virtual {v1}, LX/5Ru;->a()Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;

    move-result-object v1

    .line 1783251
    iput-object v1, v0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->g:Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;

    .line 1783252
    :cond_1
    :goto_0
    iget-object v0, p0, LX/BR8;->b:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    iget-object v0, v0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->n:LX/BRP;

    .line 1783253
    iget-object v1, v0, LX/BRP;->a:Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;

    move-object v0, v1

    .line 1783254
    invoke-virtual {v0}, Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1783255
    iget-object v0, p0, LX/BR8;->b:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    iget-object v0, v0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->h:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/BR8;->b:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    iget-object v0, v0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->h:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    invoke-static {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->a(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;

    move-result-object v0

    .line 1783256
    :goto_1
    iget-object v1, p0, LX/BR8;->b:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    iget-object v1, v1, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->n:LX/BRP;

    .line 1783257
    iget-object v2, v1, LX/BRP;->a:Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;

    move-object v1, v2

    .line 1783258
    invoke-virtual {v1}, Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;->c()LX/5QV;

    move-result-object v1

    .line 1783259
    iget-object v2, p0, LX/BR8;->b:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    invoke-static {v1}, LX/B5P;->a(LX/5QV;)Lcom/facebook/photos/creativeediting/model/StickerParams;

    move-result-object v3

    invoke-static {v3}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->setFrameOverlayItems(LX/0Px;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->a()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v0

    .line 1783260
    iput-object v0, v2, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->h:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 1783261
    iget-object v0, p0, LX/BR8;->b:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    iget-object v2, p0, LX/BR8;->b:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    iget-object v2, v2, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->h:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    iget-object v3, p0, LX/BR8;->b:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    iget-object v3, v3, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->k:LX/BR1;

    iget-object v3, v3, LX/BR1;->a:Landroid/net/Uri;

    new-instance v4, LX/BR7;

    invoke-direct {v4, p0, v1}, LX/BR7;-><init>(LX/BR8;LX/5QV;)V

    invoke-static {v0, v2, v3, v4}, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->a$redex0(Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;Lcom/facebook/photos/creativeediting/model/CreativeEditingData;Landroid/net/Uri;LX/0Vd;)V

    .line 1783262
    :goto_2
    return-void

    .line 1783263
    :cond_2
    iget-object v0, p0, LX/BR8;->b:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    iget-object v0, v0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->k:LX/BR1;

    iget-object v0, v0, LX/BR1;->a:Landroid/net/Uri;

    iget-object v1, p0, LX/BR8;->b:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    iget-object v1, v1, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->g:Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;

    .line 1783264
    iget-object v2, v1, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->b:Landroid/net/Uri;

    move-object v1, v2

    .line 1783265
    invoke-virtual {v0, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1783266
    iget-object v0, p0, LX/BR8;->b:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    iget-object v1, p0, LX/BR8;->b:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    iget-object v1, v1, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->g:Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;

    invoke-static {v1}, LX/5Ru;->a(Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;)LX/5Ru;

    move-result-object v1

    iget-object v2, p0, LX/BR8;->b:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    iget-object v2, v2, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->k:LX/BR1;

    iget-object v2, v2, LX/BR1;->a:Landroid/net/Uri;

    .line 1783267
    iput-object v2, v1, LX/5Ru;->a:Landroid/net/Uri;

    .line 1783268
    move-object v1, v1

    .line 1783269
    invoke-virtual {v1}, LX/5Ru;->a()Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;

    move-result-object v1

    .line 1783270
    iput-object v1, v0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->g:Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;

    .line 1783271
    goto :goto_0

    .line 1783272
    :cond_3
    invoke-static {}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->newBuilder()Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;

    move-result-object v0

    goto :goto_1

    .line 1783273
    :cond_4
    iget-object v0, p0, LX/BR8;->b:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    iget-object v1, p0, LX/BR8;->a:LX/BRg;

    invoke-static {v0, v1}, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->a$redex0(Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;LX/BRg;)V

    goto :goto_2
.end method
