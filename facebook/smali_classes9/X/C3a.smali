.class public LX/C3a;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Zb;

.field private final b:LX/17Q;


# direct methods
.method public constructor <init>(LX/0Zb;LX/17Q;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1845957
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1845958
    iput-object p1, p0, LX/C3a;->a:LX/0Zb;

    .line 1845959
    iput-object p2, p0, LX/C3a;->b:LX/17Q;

    .line 1845960
    return-void
.end method

.method public static b(LX/0QB;)LX/C3a;
    .locals 3

    .prologue
    .line 1845955
    new-instance v2, LX/C3a;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-static {p0}, LX/17Q;->a(LX/0QB;)LX/17Q;

    move-result-object v1

    check-cast v1, LX/17Q;

    invoke-direct {v2, v0, v1}, LX/C3a;-><init>(LX/0Zb;LX/17Q;)V

    .line 1845956
    return-object v2
.end method

.method public static c(LX/C33;)LX/162;
    .locals 3

    .prologue
    .line 1845949
    new-instance v1, LX/162;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v1, v0}, LX/162;-><init>(LX/0mC;)V

    .line 1845950
    invoke-virtual {p0}, LX/C33;->g()Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 1845951
    iget-object v2, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v2

    .line 1845952
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/162;->g(Ljava/lang/String;)LX/162;

    .line 1845953
    iget-object v0, p0, LX/C33;->d:Ljava/lang/String;

    invoke-virtual {v1, v0}, LX/162;->g(Ljava/lang/String;)LX/162;

    .line 1845954
    return-object v1
.end method
