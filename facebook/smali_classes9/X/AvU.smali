.class public LX/AvU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/89q;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/89q",
        "<",
        "Lcom/facebook/photos/creativeediting/model/FrameGraphQLInterfaces$Frame;",
        ">;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1724120
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1724121
    return-void
.end method

.method public static a(LX/0QB;)LX/AvU;
    .locals 4

    .prologue
    .line 1724125
    const-class v1, LX/AvU;

    monitor-enter v1

    .line 1724126
    :try_start_0
    sget-object v0, LX/AvU;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1724127
    sput-object v2, LX/AvU;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1724128
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1724129
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1724130
    new-instance v3, LX/AvU;

    invoke-direct {v3}, LX/AvU;-><init>()V

    .line 1724131
    const/16 p0, 0x2e2d

    invoke-static {v0, p0}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    .line 1724132
    iput-object p0, v3, LX/AvU;->a:LX/0Or;

    .line 1724133
    move-object v0, v3

    .line 1724134
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1724135
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/AvU;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1724136
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1724137
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;LX/89p;)V
    .locals 3
    .param p2    # LX/89p;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1724122
    iget-object v0, p0, LX/AvU;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;

    .line 1724123
    invoke-static {p1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    new-instance v2, LX/AvT;

    invoke-direct {v2, p0, p2}, LX/AvT;-><init>(LX/AvU;LX/89p;)V

    invoke-virtual {v0, v1, v2}, Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;->a(LX/0Px;LX/8G6;)V

    .line 1724124
    return-void
.end method
