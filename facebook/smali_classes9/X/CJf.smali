.class public final LX/CJf;
.super Landroid/webkit/WebViewClient;
.source ""


# instance fields
.field public final synthetic a:LX/CJg;


# direct methods
.method public constructor <init>(LX/CJg;)V
    .locals 0

    .prologue
    .line 1875633
    iput-object p1, p0, LX/CJf;->a:LX/CJg;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1875634
    iget-object v0, p0, LX/CJf;->a:LX/CJg;

    iget-object v0, v0, LX/CJg;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CJd;

    .line 1875635
    iget-object v2, v0, LX/CJd;->b:LX/IZ6;

    if-eqz v2, :cond_0

    iget-object v2, v0, LX/CJd;->a:Ljava/lang/String;

    iget v3, v0, LX/CJd;->d:I

    invoke-static {p2, v2, v3}, LX/CJg;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1875636
    iget-object v0, v0, LX/CJd;->b:LX/IZ6;

    invoke-virtual {v0}, LX/IZ6;->a()V

    .line 1875637
    :cond_1
    return-void
.end method

.method public final onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 4

    .prologue
    .line 1875638
    iget-object v0, p0, LX/CJf;->a:LX/CJg;

    iget-object v0, v0, LX/CJg;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CJd;

    .line 1875639
    iget-object v2, v0, LX/CJd;->b:LX/IZ6;

    if-eqz v2, :cond_0

    iget-object v2, v0, LX/CJd;->a:Ljava/lang/String;

    iget v3, v0, LX/CJd;->d:I

    invoke-static {p2, v2, v3}, LX/CJg;->b(Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-boolean v2, v0, LX/CJd;->c:Z

    if-eqz v2, :cond_0

    .line 1875640
    iget-object v0, v0, LX/CJd;->b:LX/IZ6;

    invoke-virtual {v0}, LX/IZ6;->a()V

    goto :goto_0

    .line 1875641
    :cond_1
    return-void
.end method

.method public final shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1875642
    invoke-static {p2}, Landroid/webkit/URLUtil;->isNetworkUrl(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1875643
    const/4 v2, 0x1

    :try_start_0
    invoke-static {p2, v2}, Landroid/content/Intent;->parseUri(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v2

    .line 1875644
    iget-object v3, p0, LX/CJf;->a:LX/CJg;

    iget-object v3, v3, LX/CJg;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v2, v4}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v3

    if-nez v3, :cond_1

    .line 1875645
    :cond_0
    :goto_0
    return v0

    .line 1875646
    :cond_1
    iget-object v3, p0, LX/CJf;->a:LX/CJg;

    iget-object v3, v3, LX/CJg;->b:Lcom/facebook/content/SecureContextHelper;

    iget-object v4, p0, LX/CJf;->a:LX/CJg;

    iget-object v4, v4, LX/CJg;->a:Landroid/content/Context;

    invoke-interface {v3, v2, v4}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v1

    .line 1875647
    goto :goto_0

    :catch_0
    goto :goto_0
.end method
