.class public final LX/BoE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$FetchCurationFlowModel$FeedCurationFlowStepModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;

.field public final synthetic b:LX/1dt;


# direct methods
.method public constructor <init>(LX/1dt;Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;)V
    .locals 0

    .prologue
    .line 1821607
    iput-object p1, p0, LX/BoE;->b:LX/1dt;

    iput-object p2, p0, LX/BoE;->a:Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1821608
    iget-object v0, p0, LX/BoE;->b:LX/1dt;

    iget-object v0, v0, LX/1dt;->k:LX/03V;

    sget-object v1, LX/1dt;->a:Ljava/lang/String;

    const-string v3, "fetch curation flow failed"

    invoke-virtual {v0, v1, v3, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1821609
    iget-object v0, p0, LX/BoE;->b:LX/1dt;

    iget-object v6, v0, LX/1dt;->z:LX/0bH;

    new-instance v0, LX/1Nd;

    iget-object v1, p0, LX/BoE;->a:Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;

    invoke-interface {v1}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v1

    sget-object v4, Lcom/facebook/graphql/enums/StoryVisibility;->GONE:Lcom/facebook/graphql/enums/StoryVisibility;

    iget-object v3, p0, LX/BoE;->a:Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;

    invoke-interface {v3}, Lcom/facebook/graphql/model/HideableUnit;->H_()I

    move-result v5

    move-object v3, v2

    invoke-direct/range {v0 .. v5}, LX/1Nd;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/StoryVisibility;I)V

    invoke-virtual {v6, v0}, LX/0b4;->a(LX/0b7;)V

    .line 1821610
    iget-object v0, p0, LX/BoE;->b:LX/1dt;

    iget-object v0, v0, LX/1dt;->z:LX/0bH;

    new-instance v1, LX/1YM;

    invoke-direct {v1}, LX/1YM;-><init>()V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1821611
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 1821612
    iget-object v0, p0, LX/BoE;->b:LX/1dt;

    iget-object v0, v0, LX/1dt;->z:LX/0bH;

    new-instance v1, LX/1YM;

    invoke-direct {v1}, LX/1YM;-><init>()V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1821613
    return-void
.end method
