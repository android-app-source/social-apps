.class public LX/Bwf;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static volatile h:LX/Bwf;


# instance fields
.field public final b:LX/14v;

.field public final c:LX/0Sh;

.field public final d:LX/3Hf;

.field public final e:LX/157;

.field public final f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/03V;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1834144
    const-class v0, LX/Bwf;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Bwf;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/14v;LX/0Sh;LX/3Hf;LX/03V;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1834145
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1834146
    iput-object p1, p0, LX/Bwf;->b:LX/14v;

    .line 1834147
    iput-object p2, p0, LX/Bwf;->c:LX/0Sh;

    .line 1834148
    iput-object p3, p0, LX/Bwf;->d:LX/3Hf;

    .line 1834149
    iput-object p4, p0, LX/Bwf;->g:LX/03V;

    .line 1834150
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/Bwf;->f:Ljava/util/Map;

    .line 1834151
    new-instance v0, LX/Bwe;

    invoke-direct {v0, p0}, LX/Bwe;-><init>(LX/Bwf;)V

    iput-object v0, p0, LX/Bwf;->e:LX/157;

    .line 1834152
    return-void
.end method

.method public static a(LX/0QB;)LX/Bwf;
    .locals 7

    .prologue
    .line 1834153
    sget-object v0, LX/Bwf;->h:LX/Bwf;

    if-nez v0, :cond_1

    .line 1834154
    const-class v1, LX/Bwf;

    monitor-enter v1

    .line 1834155
    :try_start_0
    sget-object v0, LX/Bwf;->h:LX/Bwf;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1834156
    if-eqz v2, :cond_0

    .line 1834157
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1834158
    new-instance p0, LX/Bwf;

    invoke-static {v0}, LX/14v;->a(LX/0QB;)LX/14v;

    move-result-object v3

    check-cast v3, LX/14v;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v4

    check-cast v4, LX/0Sh;

    invoke-static {v0}, LX/3Hf;->a(LX/0QB;)LX/3Hf;

    move-result-object v5

    check-cast v5, LX/3Hf;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v6

    check-cast v6, LX/03V;

    invoke-direct {p0, v3, v4, v5, v6}, LX/Bwf;-><init>(LX/14v;LX/0Sh;LX/3Hf;LX/03V;)V

    .line 1834159
    move-object v0, p0

    .line 1834160
    sput-object v0, LX/Bwf;->h:LX/Bwf;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1834161
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1834162
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1834163
    :cond_1
    sget-object v0, LX/Bwf;->h:LX/Bwf;

    return-object v0

    .line 1834164
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1834165
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a$redex0(LX/Bwf;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 1

    .prologue
    .line 1834166
    iget-object v0, p0, LX/Bwf;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1834167
    iget-object v0, p0, LX/Bwf;->f:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1834168
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/2pa;)V
    .locals 3
    .param p1    # LX/2pa;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1834169
    if-nez p1, :cond_1

    .line 1834170
    :cond_0
    :goto_0
    return-void

    .line 1834171
    :cond_1
    invoke-static {p1}, LX/393;->b(LX/2pa;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 1834172
    if-eqz v0, :cond_0

    .line 1834173
    invoke-static {v0}, LX/17E;->v(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    .line 1834174
    if-eqz v1, :cond_0

    .line 1834175
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v1

    .line 1834176
    iget-object v2, p0, LX/Bwf;->f:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1834177
    iget-object v2, p0, LX/Bwf;->f:Ljava/util/Map;

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1834178
    iget-object v1, p0, LX/Bwf;->b:LX/14v;

    iget-object v2, p0, LX/Bwf;->e:LX/157;

    invoke-virtual {v1, v0, v2}, LX/14v;->a(Lcom/facebook/graphql/model/GraphQLStory;LX/157;)V

    goto :goto_0

    .line 1834179
    :cond_2
    invoke-static {p0, v1, v0}, LX/Bwf;->a$redex0(LX/Bwf;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)V

    goto :goto_0
.end method
