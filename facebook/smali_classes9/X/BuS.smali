.class public final LX/BuS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLPage;

.field public final synthetic b:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic c:Lcom/facebook/feed/video/fullscreen/FullscreenCallToActionButtonPlugin;

.field private d:Z


# direct methods
.method public constructor <init>(Lcom/facebook/feed/video/fullscreen/FullscreenCallToActionButtonPlugin;Lcom/facebook/graphql/model/GraphQLPage;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 1

    .prologue
    .line 1830988
    iput-object p1, p0, LX/BuS;->c:Lcom/facebook/feed/video/fullscreen/FullscreenCallToActionButtonPlugin;

    iput-object p2, p0, LX/BuS;->a:Lcom/facebook/graphql/model/GraphQLPage;

    iput-object p3, p0, LX/BuS;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1830989
    iget-object v0, p0, LX/BuS;->a:Lcom/facebook/graphql/model/GraphQLPage;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->w()Z

    move-result v0

    iput-boolean v0, p0, LX/BuS;->d:Z

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 12

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x2

    const v2, 0x400bbe12

    invoke-static {v1, v0, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1830990
    iget-object v2, p0, LX/BuS;->c:Lcom/facebook/feed/video/fullscreen/FullscreenCallToActionButtonPlugin;

    iget-object v3, p0, LX/BuS;->a:Lcom/facebook/graphql/model/GraphQLPage;

    iget-object v4, p0, LX/BuS;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v9, 0x0

    .line 1830991
    invoke-static {v4}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v5

    .line 1830992
    if-nez v5, :cond_1

    .line 1830993
    :goto_0
    iget-boolean v2, p0, LX/BuS;->d:Z

    if-nez v2, :cond_0

    :goto_1
    iput-boolean v0, p0, LX/BuS;->d:Z

    .line 1830994
    iget-object v0, p0, LX/BuS;->c:Lcom/facebook/feed/video/fullscreen/FullscreenCallToActionButtonPlugin;

    iget-boolean v2, p0, LX/BuS;->d:Z

    invoke-static {v0, v2}, Lcom/facebook/feed/video/fullscreen/FullscreenCallToActionButtonPlugin;->a$redex0(Lcom/facebook/feed/video/fullscreen/FullscreenCallToActionButtonPlugin;Z)V

    .line 1830995
    const v0, -0x1b5c2617

    invoke-static {v0, v1}, LX/02F;->a(II)V

    return-void

    .line 1830996
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 1830997
    :cond_1
    const/4 v6, 0x0

    move-object v7, v4

    .line 1830998
    :goto_2
    if-eqz v7, :cond_4

    .line 1830999
    iget-object v8, v7, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v8, v8

    .line 1831000
    instance-of v10, v8, Lcom/facebook/graphql/model/GraphQLStory;

    if-nez v10, :cond_2

    instance-of v8, v8, Lcom/facebook/graphql/model/GraphQLStorySet;

    if-eqz v8, :cond_3

    :cond_2
    move-object v6, v7

    .line 1831001
    :cond_3
    iget-object v8, v7, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v7, v8

    .line 1831002
    goto :goto_2

    .line 1831003
    :cond_4
    move-object v6, v6

    .line 1831004
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v7

    if-eqz v7, :cond_5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v8

    .line 1831005
    :goto_3
    iget-object v11, v2, Lcom/facebook/feed/video/fullscreen/FullscreenCallToActionButtonPlugin;->b:LX/0bH;

    new-instance v5, LX/1Za;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v7

    move-object v10, v9

    invoke-direct/range {v5 .. v10}, LX/1Za;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v11, v5}, LX/0b4;->a(LX/0b7;)V

    goto :goto_0

    :cond_5
    move-object v8, v9

    .line 1831006
    goto :goto_3
.end method
