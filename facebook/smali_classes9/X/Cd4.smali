.class public LX/Cd4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:LX/6Wc;

.field public static final b:LX/6Wc;

.field public static final c:LX/6Wc;

.field public static final d:LX/6Wc;

.field public static final e:LX/6Wc;


# instance fields
.field public final f:LX/6Wr;

.field private final g:LX/0TD;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1922078
    new-instance v0, LX/6Wc;

    const-string v1, "full_name"

    invoke-direct {v0, v1}, LX/6Wc;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/Cd4;->a:LX/6Wc;

    .line 1922079
    new-instance v0, LX/6Wc;

    const-string v1, "type"

    invoke-direct {v0, v1}, LX/6Wc;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/Cd4;->b:LX/6Wc;

    .line 1922080
    new-instance v0, LX/6Wc;

    const-string v1, "page_fbid"

    invoke-direct {v0, v1}, LX/6Wc;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/Cd4;->c:LX/6Wc;

    .line 1922081
    new-instance v0, LX/6Wc;

    const-string v1, "latitude"

    invoke-direct {v0, v1}, LX/6Wc;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/Cd4;->d:LX/6Wc;

    .line 1922082
    new-instance v0, LX/6Wc;

    const-string v1, "longitude"

    invoke-direct {v0, v1}, LX/6Wc;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/Cd4;->e:LX/6Wc;

    return-void
.end method

.method public constructor <init>(LX/6Wr;LX/0TD;)V
    .locals 0
    .param p2    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1922071
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1922072
    iput-object p1, p0, LX/Cd4;->f:LX/6Wr;

    .line 1922073
    iput-object p2, p0, LX/Cd4;->g:LX/0TD;

    .line 1922074
    return-void
.end method

.method public static b(LX/0QB;)LX/Cd4;
    .locals 3

    .prologue
    .line 1922076
    new-instance v2, LX/Cd4;

    invoke-static {p0}, LX/6Wr;->a(LX/0QB;)LX/6Wr;

    move-result-object v0

    check-cast v0, LX/6Wr;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v1

    check-cast v1, LX/0TD;

    invoke-direct {v2, v0, v1}, LX/Cd4;-><init>(LX/6Wr;LX/0TD;)V

    .line 1922077
    return-object v2
.end method


# virtual methods
.method public final a(Landroid/location/Location;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/location/Location;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/0am",
            "<+",
            "Lcom/facebook/places/graphql/PlacesGraphQLInterfaces$CheckinPlace;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1922075
    iget-object v0, p0, LX/Cd4;->g:LX/0TD;

    new-instance v1, Lcom/facebook/places/create/citypicker/PlaceCreationCityAtLocationQuery$1;

    invoke-direct {v1, p0, p1}, Lcom/facebook/places/create/citypicker/PlaceCreationCityAtLocationQuery$1;-><init>(LX/Cd4;Landroid/location/Location;)V

    invoke-interface {v0, v1}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
