.class public LX/Bep;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/Ber;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:Ljava/lang/String;

.field public h:Landroid/content/Context;

.field private i:I

.field public j:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;",
            ">;>;"
        }
    .end annotation
.end field

.field public k:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;",
            ">;"
        }
    .end annotation
.end field

.field public l:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public m:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 1
    .param p2    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # I
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1805415
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1805416
    iput-object p1, p0, LX/Bep;->h:Landroid/content/Context;

    .line 1805417
    iput-object p2, p0, LX/Bep;->g:Ljava/lang/String;

    .line 1805418
    iput p3, p0, LX/Bep;->i:I

    .line 1805419
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/Bep;->j:Ljava/util/LinkedList;

    .line 1805420
    const/4 v0, 0x0

    iput v0, p0, LX/Bep;->m:I

    .line 1805421
    return-void
.end method

.method public static a$redex0(LX/Bep;Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Integer;",
            ">;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1805422
    new-instance v0, LX/Ben;

    invoke-direct {v0, p0}, LX/Ben;-><init>(LX/Bep;)V

    iget-object v1, p0, LX/Bep;->d:Ljava/util/concurrent/Executor;

    invoke-static {p1, v0, v1}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public static e(LX/Bep;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1805423
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1805424
    iget-object v0, p0, LX/Bep;->f:LX/Ber;

    .line 1805425
    iget-boolean v3, v0, LX/Ber;->d:Z

    move v0, v3

    .line 1805426
    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1805427
    new-instance v0, LX/96n;

    invoke-direct {v0}, LX/96n;-><init>()V

    move-object v3, v0

    .line 1805428
    new-instance v0, LX/4Iz;

    invoke-direct {v0}, LX/4Iz;-><init>()V

    iget-object v4, p0, LX/Bep;->g:Ljava/lang/String;

    .line 1805429
    const-string v5, "entry_point"

    invoke-virtual {v0, v5, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1805430
    move-object v0, v0

    .line 1805431
    const-string v4, "ANDROID_GRAPH_EDITOR"

    .line 1805432
    const-string v5, "endpoint"

    invoke-virtual {v0, v5, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1805433
    move-object v4, v0

    .line 1805434
    iget-object v0, p0, LX/Bep;->h:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 1805435
    const-string v5, "place_question_photo_size"

    iget-object v6, p0, LX/Bep;->e:LX/0Uh;

    const/16 v7, 0x31c

    invoke-virtual {v6, v7, v2}, LX/0Uh;->a(IZ)Z

    move-result v2

    if-eqz v2, :cond_2

    :goto_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v3, v5, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1805436
    const-string v0, "profile_picture_size"

    iget-object v2, p0, LX/Bep;->h:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v5, 0x7f0b1c81

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v2, v2

    iget-object v5, p0, LX/Bep;->h:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    invoke-static {v1, v2, v5}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1805437
    const-string v0, "query_data"

    invoke-virtual {v3, v0, v4}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1805438
    const-string v0, "card_count"

    const/4 v1, 0x6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1805439
    iget-object v0, p0, LX/Bep;->f:LX/Ber;

    .line 1805440
    iget-object v1, v0, LX/Ber;->e:Ljava/lang/String;

    move-object v0, v1

    .line 1805441
    if-eqz v0, :cond_0

    .line 1805442
    const-string v0, "after_cursor"

    iget-object v1, p0, LX/Bep;->f:LX/Ber;

    .line 1805443
    iget-object v2, v1, LX/Ber;->e:Ljava/lang/String;

    move-object v1, v2

    .line 1805444
    invoke-virtual {v3, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1805445
    :cond_0
    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 1805446
    iget-object v1, p0, LX/Bep;->b:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 1805447
    iget-object v1, p0, LX/Bep;->a:LX/1Ck;

    const-string v2, "graph_editor_task_fetch_questions"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1805448
    move-object v0, v0

    .line 1805449
    new-instance v1, LX/Beo;

    invoke-direct {v1, p0}, LX/Beo;-><init>(LX/Bep;)V

    iget-object v2, p0, LX/Bep;->d:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, LX/Bep;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1805450
    iget-object v0, p0, LX/Bep;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    return-object v0

    :cond_1
    move v0, v2

    .line 1805451
    goto/16 :goto_0

    .line 1805452
    :cond_2
    div-int/lit8 v0, v0, 0x2

    goto :goto_1
.end method


# virtual methods
.method public final b()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1805453
    :goto_0
    iget-object v0, p0, LX/Bep;->j:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    iget v1, p0, LX/Bep;->i:I

    mul-int/lit8 v1, v1, 0x2

    if-ge v0, v1, :cond_0

    .line 1805454
    iget-object v0, p0, LX/Bep;->j:Ljava/util/LinkedList;

    .line 1805455
    iget-object v1, p0, LX/Bep;->k:Lcom/google/common/util/concurrent/ListenableFuture;

    if-nez v1, :cond_2

    .line 1805456
    iget-object v1, p0, LX/Bep;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/Bep;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {p0, v1}, LX/Bep;->a$redex0(LX/Bep;Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    :goto_1
    iput-object v1, p0, LX/Bep;->k:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1805457
    iget-object v1, p0, LX/Bep;->k:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1805458
    :goto_2
    move-object v1, v1

    .line 1805459
    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1805460
    :cond_0
    iget-object v0, p0, LX/Bep;->j:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->remove()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/util/concurrent/ListenableFuture;

    return-object v0

    .line 1805461
    :cond_1
    invoke-static {p0}, LX/Bep;->e(LX/Bep;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    invoke-static {p0, v1}, LX/Bep;->a$redex0(LX/Bep;Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    goto :goto_1

    .line 1805462
    :cond_2
    iget-object v1, p0, LX/Bep;->k:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v2, LX/Bem;

    invoke-direct {v2, p0}, LX/Bem;-><init>(LX/Bep;)V

    iget-object v3, p0, LX/Bep;->d:Ljava/util/concurrent/Executor;

    invoke-static {v1, v2, v3}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    iput-object v1, p0, LX/Bep;->k:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1805463
    iget-object v1, p0, LX/Bep;->k:Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_2
.end method
