.class public LX/BQG;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/BQG;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/common/perftest/PerfTestConfig;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Or;
    .annotation runtime Lcom/facebook/common/random/InsecureRandom;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/util/Random;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;LX/0Or;)V
    .locals 0
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/common/random/InsecureRandom;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/common/perftest/PerfTestConfig;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/util/Random;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1781978
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1781979
    iput-object p2, p0, LX/BQG;->b:LX/0Or;

    .line 1781980
    iput-object p1, p0, LX/BQG;->a:LX/0Or;

    .line 1781981
    return-void
.end method

.method public static a(LX/0QB;)LX/BQG;
    .locals 5

    .prologue
    .line 1781982
    sget-object v0, LX/BQG;->c:LX/BQG;

    if-nez v0, :cond_1

    .line 1781983
    const-class v1, LX/BQG;

    monitor-enter v1

    .line 1781984
    :try_start_0
    sget-object v0, LX/BQG;->c:LX/BQG;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1781985
    if-eqz v2, :cond_0

    .line 1781986
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1781987
    new-instance v3, LX/BQG;

    const/16 v4, 0x2d2

    invoke-static {v0, v4}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 p0, 0x1618

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v3, v4, p0}, LX/BQG;-><init>(LX/0Or;LX/0Or;)V

    .line 1781988
    move-object v0, v3

    .line 1781989
    sput-object v0, LX/BQG;->c:LX/BQG;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1781990
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1781991
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1781992
    :cond_1
    sget-object v0, LX/BQG;->c:LX/BQG;

    return-object v0

    .line 1781993
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1781994
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 2

    .prologue
    .line 1781995
    iget-object v0, p0, LX/BQG;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    invoke-static {}, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->a()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/BQG;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Random;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
