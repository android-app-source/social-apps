.class public LX/CKg;
.super LX/0Dp;
.source ""


# static fields
.field private static final d:LX/0wT;


# instance fields
.field private e:LX/0wW;

.field private f:LX/0wd;

.field public g:LX/3Rb;

.field private h:LX/3MV;

.field public i:Lcom/facebook/user/model/User;

.field public j:Ljava/lang/String;

.field public k:LX/CK7;

.field public l:LX/CKk;

.field public m:LX/0ad;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 1877420
    const-wide v0, 0x4062c00000000000L    # 150.0

    const-wide/high16 v2, 0x4028000000000000L    # 12.0

    invoke-static {v0, v1, v2, v3}, LX/0wT;->a(DD)LX/0wT;

    move-result-object v0

    sput-object v0, LX/CKg;->d:LX/0wT;

    return-void
.end method

.method public constructor <init>(LX/0wW;LX/3Rb;LX/3MV;LX/CK7;LX/CKk;LX/0ad;Lcom/facebook/messaging/business/subscription/instantarticle/view/BusinessIASubscribeBannerView;)V
    .locals 0
    .param p7    # Lcom/facebook/messaging/business/subscription/instantarticle/view/BusinessIASubscribeBannerView;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1877421
    invoke-direct {p0}, LX/0Dp;-><init>()V

    .line 1877422
    iput-object p7, p0, LX/0Dp;->c:Landroid/view/View;

    .line 1877423
    iput-object p1, p0, LX/CKg;->e:LX/0wW;

    .line 1877424
    iput-object p2, p0, LX/CKg;->g:LX/3Rb;

    .line 1877425
    iput-object p3, p0, LX/CKg;->h:LX/3MV;

    .line 1877426
    iput-object p4, p0, LX/CKg;->k:LX/CK7;

    .line 1877427
    iput-object p5, p0, LX/CKg;->l:LX/CKk;

    .line 1877428
    iput-object p6, p0, LX/CKg;->m:LX/0ad;

    .line 1877429
    return-void
.end method

.method public static a(LX/8Yp;)Lcom/facebook/user/model/User;
    .locals 10

    .prologue
    const/4 v3, 0x0

    .line 1877430
    new-instance v0, LX/0XI;

    invoke-direct {v0}, LX/0XI;-><init>()V

    sget-object v1, LX/0XG;->FACEBOOK:LX/0XG;

    invoke-interface {p0}, LX/8Yp;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0XI;->a(LX/0XG;Ljava/lang/String;)LX/0XI;

    move-result-object v0

    new-instance v1, Lcom/facebook/user/model/Name;

    invoke-interface {p0}, LX/8Yp;->e()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/facebook/user/model/Name;-><init>(Ljava/lang/String;)V

    .line 1877431
    iput-object v1, v0, LX/0XI;->g:Lcom/facebook/user/model/Name;

    .line 1877432
    move-object v0, v0

    .line 1877433
    new-instance v1, LX/5be;

    invoke-direct {v1}, LX/5be;-><init>()V

    invoke-interface {p0}, LX/8Yp;->eb_()LX/1Fb;

    move-result-object v2

    invoke-interface {v2}, LX/1Fb;->a()I

    move-result v2

    .line 1877434
    iput v2, v1, LX/5be;->a:I

    .line 1877435
    move-object v1, v1

    .line 1877436
    invoke-interface {p0}, LX/8Yp;->eb_()LX/1Fb;

    move-result-object v2

    invoke-interface {v2}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v2

    .line 1877437
    iput-object v2, v1, LX/5be;->b:Ljava/lang/String;

    .line 1877438
    move-object v1, v1

    .line 1877439
    const/4 v8, 0x1

    const/4 v6, 0x0

    const/4 v9, 0x0

    .line 1877440
    new-instance v4, LX/186;

    const/16 v5, 0x80

    invoke-direct {v4, v5}, LX/186;-><init>(I)V

    .line 1877441
    iget-object v5, v1, LX/5be;->b:Ljava/lang/String;

    invoke-virtual {v4, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1877442
    const/4 v7, 0x2

    invoke-virtual {v4, v7}, LX/186;->c(I)V

    .line 1877443
    iget v7, v1, LX/5be;->a:I

    invoke-virtual {v4, v9, v7, v9}, LX/186;->a(III)V

    .line 1877444
    invoke-virtual {v4, v8, v5}, LX/186;->b(II)V

    .line 1877445
    invoke-virtual {v4}, LX/186;->d()I

    move-result v5

    .line 1877446
    invoke-virtual {v4, v5}, LX/186;->d(I)V

    .line 1877447
    invoke-virtual {v4}, LX/186;->e()[B

    move-result-object v4

    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 1877448
    invoke-virtual {v5, v9}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1877449
    new-instance v4, LX/15i;

    move-object v7, v6

    move-object v9, v6

    invoke-direct/range {v4 .. v9}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1877450
    new-instance v5, Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;

    invoke-direct {v5, v4}, Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;-><init>(LX/15i;)V

    .line 1877451
    move-object v1, v5

    .line 1877452
    invoke-static {v1, v3, v3}, LX/3MV;->a(Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;Lcom/facebook/messaging/graphql/threads/UserInfoModels$ProfilePhotoInfoModel;)Lcom/facebook/user/model/PicSquare;

    move-result-object v1

    .line 1877453
    iput-object v1, v0, LX/0XI;->p:Lcom/facebook/user/model/PicSquare;

    .line 1877454
    move-object v0, v0

    .line 1877455
    const-string v1, "page"

    .line 1877456
    iput-object v1, v0, LX/0XI;->y:Ljava/lang/String;

    .line 1877457
    move-object v0, v0

    .line 1877458
    const/4 v1, 0x1

    .line 1877459
    iput-boolean v1, v0, LX/0XI;->z:Z

    .line 1877460
    move-object v0, v0

    .line 1877461
    invoke-virtual {v0}, LX/0XI;->aj()Lcom/facebook/user/model/User;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 1877462
    invoke-super {p0}, LX/0Dp;->a()V

    .line 1877463
    iget-object v0, p0, LX/CKg;->k:LX/CK7;

    const-string v1, "instant_article"

    iget-object v2, p0, LX/CKg;->i:Lcom/facebook/user/model/User;

    .line 1877464
    iget-object v3, v2, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v2, v3

    .line 1877465
    iget-object v3, p0, LX/CKg;->j:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, LX/CK7;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1877466
    iget-object v0, p0, LX/CKg;->e:LX/0wW;

    invoke-virtual {v0}, LX/0wW;->a()LX/0wd;

    move-result-object v0

    sget-object v1, LX/CKg;->d:LX/0wT;

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v0

    const/4 v1, 0x1

    .line 1877467
    iput-boolean v1, v0, LX/0wd;->c:Z

    .line 1877468
    move-object v0, v0

    .line 1877469
    invoke-virtual {v0}, LX/0wd;->j()LX/0wd;

    move-result-object v0

    new-instance v1, LX/CKf;

    invoke-direct {v1, p0}, LX/CKf;-><init>(LX/CKg;)V

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0xi;)LX/0wd;

    move-result-object v0

    iput-object v0, p0, LX/CKg;->f:LX/0wd;

    .line 1877470
    return-void
.end method

.method public final a(Z)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 1877471
    iget-object v0, p0, LX/0Dp;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    .line 1877472
    if-eqz p1, :cond_0

    .line 1877473
    iget-object v1, p0, LX/CKg;->f:LX/0wd;

    int-to-double v2, v0

    invoke-virtual {v1, v2, v3}, LX/0wd;->a(D)LX/0wd;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, LX/0wd;->b(D)LX/0wd;

    .line 1877474
    :goto_0
    return-void

    .line 1877475
    :cond_0
    iget-object v1, p0, LX/CKg;->f:LX/0wd;

    invoke-virtual {v1, v4, v5}, LX/0wd;->a(D)LX/0wd;

    move-result-object v1

    int-to-double v2, v0

    invoke-virtual {v1, v2, v3}, LX/0wd;->b(D)LX/0wd;

    goto :goto_0
.end method
