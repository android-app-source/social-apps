.class public LX/BA2;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/BA2;


# instance fields
.field private final a:LX/0TD;

.field public final b:LX/0Sh;

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(LX/0Sh;LX/0TD;LX/0Ot;Landroid/content/res/Resources;)V
    .locals 0
    .param p2    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/ForegroundExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/0TD;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "Landroid/content/res/Resources;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1752627
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1752628
    iput-object p1, p0, LX/BA2;->b:LX/0Sh;

    .line 1752629
    iput-object p2, p0, LX/BA2;->a:LX/0TD;

    .line 1752630
    iput-object p3, p0, LX/BA2;->c:LX/0Ot;

    .line 1752631
    iput-object p4, p0, LX/BA2;->d:Landroid/content/res/Resources;

    .line 1752632
    return-void
.end method

.method public static a(LX/0QB;)LX/BA2;
    .locals 7

    .prologue
    .line 1752633
    sget-object v0, LX/BA2;->e:LX/BA2;

    if-nez v0, :cond_1

    .line 1752634
    const-class v1, LX/BA2;

    monitor-enter v1

    .line 1752635
    :try_start_0
    sget-object v0, LX/BA2;->e:LX/BA2;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1752636
    if-eqz v2, :cond_0

    .line 1752637
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1752638
    new-instance v6, LX/BA2;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v3

    check-cast v3, LX/0Sh;

    invoke-static {v0}, LX/0tb;->a(LX/0QB;)LX/0TD;

    move-result-object v4

    check-cast v4, LX/0TD;

    const/16 v5, 0x259

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v5

    check-cast v5, Landroid/content/res/Resources;

    invoke-direct {v6, v3, v4, p0, v5}, LX/BA2;-><init>(LX/0Sh;LX/0TD;LX/0Ot;Landroid/content/res/Resources;)V

    .line 1752639
    move-object v0, v6

    .line 1752640
    sput-object v0, LX/BA2;->e:LX/BA2;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1752641
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1752642
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1752643
    :cond_1
    sget-object v0, LX/BA2;->e:LX/BA2;

    return-object v0

    .line 1752644
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1752645
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)[B
    .locals 6
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1752646
    const/4 v1, 0x0

    .line 1752647
    :try_start_0
    iget-object v0, p0, LX/BA2;->d:Landroid/content/res/Resources;

    const v2, 0x7f070034

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 1752648
    const/4 v0, 0x0

    :try_start_1
    invoke-static {p1, v0}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    .line 1752649
    array-length v2, v0

    add-int/lit8 v2, v2, -0x3

    add-int/lit16 v2, v2, 0x25f

    new-array v2, v2, [B

    .line 1752650
    const/4 v3, 0x0

    const/16 v4, 0x25f

    invoke-virtual {v1, v2, v3, v4}, Ljava/io/InputStream;->read([BII)I

    .line 1752651
    const/16 v3, 0xa2

    const/4 v4, 0x1

    aget-byte v4, v0, v4

    aput-byte v4, v2, v3

    .line 1752652
    const/16 v3, 0xa0

    const/4 v4, 0x2

    aget-byte v4, v0, v4

    aput-byte v4, v2, v3

    .line 1752653
    const/4 v3, 0x3

    const/16 v4, 0x25f

    array-length v5, v0

    add-int/lit8 v5, v5, -0x3

    invoke-static {v0, v3, v2, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1752654
    if-eqz v1, :cond_0

    .line 1752655
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    :cond_0
    return-object v2

    .line 1752656
    :catch_0
    move-exception v0

    move-object v2, v1

    move-object v1, v0

    .line 1752657
    :goto_0
    :try_start_2
    iget-object v0, p0, LX/BA2;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v3, "mini_preview_generator"

    invoke-virtual {v0, v3, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1752658
    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1752659
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_1
    if-eqz v1, :cond_1

    .line 1752660
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    :cond_1
    throw v0

    .line 1752661
    :catchall_1
    move-exception v0

    goto :goto_1

    .line 1752662
    :catch_1
    move-exception v0

    move-object v2, v1

    move-object v1, v0

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1752663
    iget-object v0, p0, LX/BA2;->a:LX/0TD;

    new-instance v1, LX/BA1;

    invoke-direct {v1, p0, p1}, LX/BA1;-><init>(LX/BA2;Ljava/lang/String;)V

    invoke-interface {v0, v1}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
