.class public final LX/AV0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;


# instance fields
.field public final synthetic a:LX/AV1;

.field private final b:I

.field private c:Z


# direct methods
.method public constructor <init>(LX/AV1;I)V
    .locals 1

    .prologue
    .line 1678572
    iput-object p1, p0, LX/AV0;->a:LX/AV1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1678573
    iput p2, p0, LX/AV0;->b:I

    .line 1678574
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/AV0;->c:Z

    .line 1678575
    return-void
.end method


# virtual methods
.method public final onFrameAvailable(Landroid/graphics/SurfaceTexture;)V
    .locals 9
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    const-wide/32 v6, 0xf4240

    const-wide/16 v2, 0x0

    .line 1678576
    iget v0, p0, LX/AV0;->b:I

    iget-object v1, p0, LX/AV0;->a:LX/AV1;

    iget v1, v1, LX/AV1;->e:I

    if-eq v0, v1, :cond_0

    .line 1678577
    :cond_0
    iget-boolean v0, p0, LX/AV0;->c:Z

    if-eqz v0, :cond_1

    .line 1678578
    iget-object v0, p0, LX/AV0;->a:LX/AV1;

    iget-object v0, v0, LX/AV1;->h:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0xac000f

    const/4 v4, 0x2

    invoke-interface {v0, v1, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 1678579
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/AV0;->c:Z

    .line 1678580
    :cond_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_3

    iget-object v0, p0, LX/AV0;->a:LX/AV1;

    iget-boolean v0, v0, LX/AV1;->s:Z

    if-eqz v0, :cond_3

    .line 1678581
    iget-object v0, p0, LX/AV0;->a:LX/AV1;

    iget-wide v0, v0, LX/AV1;->t:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_2

    .line 1678582
    iget-object v0, p0, LX/AV0;->a:LX/AV1;

    invoke-virtual {p1}, Landroid/graphics/SurfaceTexture;->getTimestamp()J

    move-result-wide v4

    div-long/2addr v4, v6

    .line 1678583
    iput-wide v4, v0, LX/AV1;->t:J

    .line 1678584
    :cond_2
    iget-object v0, p0, LX/AV0;->a:LX/AV1;

    invoke-virtual {p1}, Landroid/graphics/SurfaceTexture;->getTimestamp()J

    move-result-wide v4

    div-long/2addr v4, v6

    .line 1678585
    iput-wide v4, v0, LX/AV1;->u:J

    .line 1678586
    iget-object v0, p0, LX/AV0;->a:LX/AV1;

    iget-wide v0, v0, LX/AV1;->u:J

    iget-object v4, p0, LX/AV0;->a:LX/AV1;

    iget-wide v4, v4, LX/AV1;->t:J

    sub-long/2addr v0, v4

    .line 1678587
    cmp-long v2, v0, v2

    if-lez v2, :cond_5

    :goto_0
    iget-object v2, p0, LX/AV0;->a:LX/AV1;

    iget-wide v2, v2, LX/AV1;->v:J

    add-long/2addr v0, v2

    move-wide v2, v0

    .line 1678588
    :cond_3
    iget-object v0, p0, LX/AV0;->a:LX/AV1;

    iget-object v0, v0, LX/AV1;->n:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/AV0;->a:LX/AV1;

    iget-object v0, v0, LX/AV1;->n:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1678589
    iget-object v0, p0, LX/AV0;->a:LX/AV1;

    iget-object v0, v0, LX/AV1;->n:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1b6;

    iget v1, p0, LX/AV0;->b:I

    invoke-interface {v0, v1, v2, v3}, LX/1b6;->a(IJ)V

    .line 1678590
    :cond_4
    return-void

    .line 1678591
    :cond_5
    const-wide/16 v0, 0x1

    goto :goto_0
.end method
