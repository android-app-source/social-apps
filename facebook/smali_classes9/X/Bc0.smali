.class public LX/Bc0;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:Lcom/facebook/maps/FbStaticMapView;

.field public b:Landroid/view/View;

.field public c:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1801429
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1801430
    const p1, 0x7f03137b

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1801431
    const p1, 0x7f0d1cd0

    invoke-virtual {p0, p1}, LX/Bc0;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/maps/FbStaticMapView;

    iput-object p1, p0, LX/Bc0;->a:Lcom/facebook/maps/FbStaticMapView;

    .line 1801432
    const p1, 0x7f0d2d02

    invoke-virtual {p0, p1}, LX/Bc0;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, LX/Bc0;->b:Landroid/view/View;

    .line 1801433
    const p1, 0x7f0d2d01

    invoke-virtual {p0, p1}, LX/Bc0;->findViewById(I)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, LX/Bc0;->c:Landroid/view/View;

    .line 1801434
    return-void
.end method


# virtual methods
.method public getMapView()Lcom/facebook/maps/FbStaticMapView;
    .locals 1

    .prologue
    .line 1801435
    iget-object v0, p0, LX/Bc0;->a:Lcom/facebook/maps/FbStaticMapView;

    return-object v0
.end method

.method public setMapForeground(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 1801436
    iget-object v0, p0, LX/Bc0;->a:Lcom/facebook/maps/FbStaticMapView;

    invoke-virtual {v0, p1}, Lcom/facebook/maps/FbStaticMapView;->setForeground(Landroid/graphics/drawable/Drawable;)V

    .line 1801437
    return-void
.end method
