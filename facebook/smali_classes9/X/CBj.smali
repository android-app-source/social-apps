.class public final enum LX/CBj;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/CBj;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/CBj;

.field public static final enum BRANDING_BOTTOM_DIVIDER:LX/CBj;

.field public static final enum UNKNOWN:LX/CBj;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1856771
    new-instance v0, LX/CBj;

    const-string v1, "BRANDING_BOTTOM_DIVIDER"

    invoke-direct {v0, v1, v2}, LX/CBj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CBj;->BRANDING_BOTTOM_DIVIDER:LX/CBj;

    .line 1856772
    new-instance v0, LX/CBj;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v3}, LX/CBj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CBj;->UNKNOWN:LX/CBj;

    .line 1856773
    const/4 v0, 0x2

    new-array v0, v0, [LX/CBj;

    sget-object v1, LX/CBj;->BRANDING_BOTTOM_DIVIDER:LX/CBj;

    aput-object v1, v0, v2

    sget-object v1, LX/CBj;->UNKNOWN:LX/CBj;

    aput-object v1, v0, v3

    sput-object v0, LX/CBj;->$VALUES:[LX/CBj;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1856774
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)LX/CBj;
    .locals 1

    .prologue
    .line 1856775
    if-nez p0, :cond_0

    .line 1856776
    :try_start_0
    sget-object v0, LX/CBj;->UNKNOWN:LX/CBj;

    .line 1856777
    :goto_0
    return-object v0

    .line 1856778
    :cond_0
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0, v0}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/CBj;->valueOf(Ljava/lang/String;)LX/CBj;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 1856779
    :catch_0
    sget-object v0, LX/CBj;->UNKNOWN:LX/CBj;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/CBj;
    .locals 1

    .prologue
    .line 1856780
    const-class v0, LX/CBj;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/CBj;

    return-object v0
.end method

.method public static values()[LX/CBj;
    .locals 1

    .prologue
    .line 1856781
    sget-object v0, LX/CBj;->$VALUES:[LX/CBj;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/CBj;

    return-object v0
.end method
