.class public LX/BeH;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile h:LX/BeH;


# instance fields
.field public a:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/BeM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0if;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private g:LX/BeJ;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1804749
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1804750
    return-void
.end method

.method public static a(LX/0QB;)LX/BeH;
    .locals 9

    .prologue
    .line 1804751
    sget-object v0, LX/BeH;->h:LX/BeH;

    if-nez v0, :cond_1

    .line 1804752
    const-class v1, LX/BeH;

    monitor-enter v1

    .line 1804753
    :try_start_0
    sget-object v0, LX/BeH;->h:LX/BeH;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1804754
    if-eqz v2, :cond_0

    .line 1804755
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1804756
    new-instance v3, LX/BeH;

    invoke-direct {v3}, LX/BeH;-><init>()V

    .line 1804757
    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    .line 1804758
    new-instance v7, LX/BeM;

    invoke-direct {v7}, LX/BeM;-><init>()V

    .line 1804759
    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v5

    check-cast v5, LX/0tX;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v6

    check-cast v6, Landroid/content/res/Resources;

    .line 1804760
    iput-object v5, v7, LX/BeM;->a:LX/0tX;

    iput-object v6, v7, LX/BeM;->b:Landroid/content/res/Resources;

    .line 1804761
    move-object v5, v7

    .line 1804762
    check-cast v5, LX/BeM;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v6

    check-cast v6, LX/0Uh;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v7

    check-cast v7, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v8

    check-cast v8, LX/0if;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object p0

    check-cast p0, Ljava/util/concurrent/Executor;

    .line 1804763
    iput-object v4, v3, LX/BeH;->a:LX/03V;

    iput-object v5, v3, LX/BeH;->b:LX/BeM;

    iput-object v6, v3, LX/BeH;->c:LX/0Uh;

    iput-object v7, v3, LX/BeH;->d:Lcom/facebook/content/SecureContextHelper;

    iput-object v8, v3, LX/BeH;->e:LX/0if;

    iput-object p0, v3, LX/BeH;->f:Ljava/util/concurrent/Executor;

    .line 1804764
    move-object v0, v3

    .line 1804765
    sput-object v0, LX/BeH;->h:LX/BeH;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1804766
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1804767
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1804768
    :cond_1
    sget-object v0, LX/BeH;->h:LX/BeH;

    return-object v0

    .line 1804769
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1804770
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/BeH;LX/BeI;Z)Z
    .locals 7

    .prologue
    .line 1804771
    iget-object v0, p0, LX/BeH;->g:LX/BeJ;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/BeH;->g:LX/BeJ;

    iget-object v0, v0, LX/BeJ;->a:LX/BeI;

    invoke-virtual {p1, v0}, LX/BeI;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1804772
    const/4 v0, 0x0

    .line 1804773
    :goto_0
    return v0

    .line 1804774
    :cond_0
    new-instance v0, LX/BeJ;

    iget-object v1, p0, LX/BeH;->b:LX/BeM;

    .line 1804775
    new-instance v2, LX/96h;

    invoke-direct {v2}, LX/96h;-><init>()V

    move-object v2, v2

    .line 1804776
    const-string v3, "page_id"

    iget-object v4, p1, LX/BeI;->a:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1804777
    const-string v3, "place_question_photo_size"

    iget-object v4, v1, LX/BeM;->b:Landroid/content/res/Resources;

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v4, v4, Landroid/util/DisplayMetrics;->heightPixels:I

    div-int/lit8 v4, v4, 0x2

    iget-object v5, v1, LX/BeM;->b:Landroid/content/res/Resources;

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    iget v5, v5, Landroid/util/DisplayMetrics;->widthPixels:I

    div-int/lit8 v5, v5, 0x2

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1804778
    const-string v3, "profile_picture_size"

    const/4 v4, 0x1

    const/high16 v5, 0x42400000    # 48.0f

    iget-object v6, v1, LX/BeM;->b:Landroid/content/res/Resources;

    invoke-virtual {v6}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v6

    invoke-static {v4, v5, v6}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1804779
    const-string v3, "endpoint"

    const-string v4, "android_feather"

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1804780
    const-string v3, "entry_point"

    iget-object v4, p1, LX/BeI;->b:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1804781
    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v2

    .line 1804782
    iget-object v3, v1, LX/BeM;->a:LX/0tX;

    invoke-virtual {v3, v2}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v2

    .line 1804783
    move-object v1, v2

    .line 1804784
    invoke-direct {v0, v1, p1}, LX/BeJ;-><init>(LX/1Zp;LX/BeI;)V

    iput-object v0, p0, LX/BeH;->g:LX/BeJ;

    .line 1804785
    iget-object v1, p0, LX/BeH;->e:LX/0if;

    sget-object v2, LX/0ig;->au:LX/0ih;

    if-eqz p2, :cond_1

    const-string v0, "prefetch_start"

    :goto_1
    invoke-virtual {v1, v2, v0}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1804786
    const/4 v0, 0x1

    goto :goto_0

    .line 1804787
    :cond_1
    const-string v0, "fetch_start"

    goto :goto_1
.end method


# virtual methods
.method public final a(LX/BeI;Ljava/lang/String;Landroid/content/Context;)V
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 1804788
    iget-object v0, p0, LX/BeH;->c:LX/0Uh;

    const/16 v1, 0x31a

    invoke-virtual {v0, v1, v4}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1804789
    :goto_0
    return-void

    .line 1804790
    :cond_0
    iget-object v0, p0, LX/BeH;->e:LX/0if;

    sget-object v1, LX/0ig;->au:LX/0ih;

    invoke-virtual {v0, v1}, LX/0if;->a(LX/0ih;)V

    .line 1804791
    iget-object v0, p0, LX/BeH;->e:LX/0if;

    sget-object v1, LX/0ig;->au:LX/0ih;

    invoke-virtual {v0, v1, p2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1804792
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    .line 1804793
    invoke-static {p0, p1, v4}, LX/BeH;->a(LX/BeH;LX/BeI;Z)Z

    move-result v4

    .line 1804794
    iget-object v0, p0, LX/BeH;->g:LX/BeJ;

    iget-object v7, v0, LX/BeJ;->b:LX/1Zp;

    new-instance v0, LX/BeG;

    move-object v1, p0

    move-object v5, p1

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, LX/BeG;-><init>(LX/BeH;JZLX/BeI;Landroid/content/Context;)V

    iget-object v1, p0, LX/BeH;->f:Ljava/util/concurrent/Executor;

    invoke-static {v7, v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method
