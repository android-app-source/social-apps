.class public final LX/C1U;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/C1R;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic d:LX/C1W;


# direct methods
.method public constructor <init>(LX/C1W;LX/C1R;Ljava/lang/String;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 0

    .prologue
    .line 1842475
    iput-object p1, p0, LX/C1U;->d:LX/C1W;

    iput-object p2, p0, LX/C1U;->a:LX/C1R;

    iput-object p3, p0, LX/C1U;->b:Ljava/lang/String;

    iput-object p4, p0, LX/C1U;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1842476
    iget-object v0, p0, LX/C1U;->d:LX/C1W;

    iget-object v0, v0, LX/C1W;->c:LX/0bH;

    new-instance v1, LX/1Ne;

    iget-object v2, p0, LX/C1U;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v2}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    invoke-direct {v1, v2}, LX/1Ne;-><init>(Lcom/facebook/graphql/model/FeedUnit;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1842477
    iget-object v0, p0, LX/C1U;->a:LX/C1R;

    invoke-virtual {v0}, LX/C1R;->b()V

    .line 1842478
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 8

    .prologue
    .line 1842479
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v2, 0x0

    .line 1842480
    iget-object v0, p0, LX/C1U;->a:LX/C1R;

    invoke-virtual {v0}, LX/C1R;->b()V

    .line 1842481
    if-eqz p1, :cond_0

    .line 1842482
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1842483
    if-nez v0, :cond_1

    .line 1842484
    :cond_0
    :goto_0
    return-void

    .line 1842485
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1842486
    check-cast v0, Lcom/facebook/feed/protocol/QuestionAddPollOptionModels$QuestionAddResponseMutationModel;

    .line 1842487
    invoke-virtual {v0}, Lcom/facebook/feed/protocol/QuestionAddPollOptionModels$QuestionAddResponseMutationModel;->a()Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionMutationFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionMutationFragmentModel;->k()Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionMutationFragmentModel$OptionsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionMutationFragmentModel$OptionsModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v1, v2

    :goto_1
    if-ge v1, v4, :cond_0

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionMutationFragmentModel$OptionsModel$EdgesModel;

    .line 1842488
    invoke-virtual {v0}, Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionMutationFragmentModel$OptionsModel$EdgesModel;->a()Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionMutationFragmentModel$OptionsModel$EdgesModel$NodeModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionMutationFragmentModel$OptionsModel$EdgesModel$NodeModel;->k()LX/1vs;

    move-result-object v5

    iget-object v6, v5, LX/1vs;->a:LX/15i;

    iget v5, v5, LX/1vs;->b:I

    iget-object v7, p0, LX/C1U;->b:Ljava/lang/String;

    invoke-virtual {v6, v5, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v7, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1842489
    iget-object v1, p0, LX/C1U;->d:LX/C1W;

    iget-object v1, v1, LX/C1W;->a:LX/189;

    iget-object v2, p0, LX/C1U;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p0, LX/C1U;->b:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionMutationFragmentModel$OptionsModel$EdgesModel;->a()Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionMutationFragmentModel$OptionsModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/feed/protocol/QuestionPollUpdateVoteModels$QuestionMutationFragmentModel$OptionsModel$EdgesModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v3, v0}, LX/189;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 1842490
    invoke-static {v0}, LX/182;->f(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    .line 1842491
    iget-object v1, p0, LX/C1U;->d:LX/C1W;

    iget-object v1, v1, LX/C1W;->c:LX/0bH;

    new-instance v2, LX/1Ne;

    invoke-direct {v2, v0}, LX/1Ne;-><init>(Lcom/facebook/graphql/model/FeedUnit;)V

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b7;)V

    goto :goto_0

    .line 1842492
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1
.end method
