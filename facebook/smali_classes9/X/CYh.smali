.class public final LX/CYh;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 12

    .prologue
    const/4 v1, 0x0

    .line 1912044
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_9

    .line 1912045
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1912046
    :goto_0
    return v1

    .line 1912047
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1912048
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_8

    .line 1912049
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 1912050
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1912051
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_1

    if-eqz v8, :cond_1

    .line 1912052
    const-string v9, "byline"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 1912053
    const/4 v8, 0x0

    .line 1912054
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v9, LX/15z;->START_OBJECT:LX/15z;

    if-eq v7, v9, :cond_d

    .line 1912055
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1912056
    :goto_2
    move v7, v8

    .line 1912057
    goto :goto_1

    .line 1912058
    :cond_2
    const-string v9, "external_image"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 1912059
    const/4 v8, 0x0

    .line 1912060
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v9, LX/15z;->START_OBJECT:LX/15z;

    if-eq v6, v9, :cond_11

    .line 1912061
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1912062
    :goto_3
    move v6, v8

    .line 1912063
    goto :goto_1

    .line 1912064
    :cond_3
    const-string v9, "external_url"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 1912065
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_1

    .line 1912066
    :cond_4
    const-string v9, "published_on"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 1912067
    const/4 v8, 0x0

    .line 1912068
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v9, LX/15z;->START_OBJECT:LX/15z;

    if-eq v4, v9, :cond_15

    .line 1912069
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1912070
    :goto_4
    move v4, v8

    .line 1912071
    goto :goto_1

    .line 1912072
    :cond_5
    const-string v9, "reviewer"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 1912073
    invoke-static {p0, p1}, LX/CYg;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 1912074
    :cond_6
    const-string v9, "summary"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 1912075
    const/4 v8, 0x0

    .line 1912076
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v9, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v9, :cond_19

    .line 1912077
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1912078
    :goto_5
    move v2, v8

    .line 1912079
    goto/16 :goto_1

    .line 1912080
    :cond_7
    const-string v9, "title"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 1912081
    const/4 v8, 0x0

    .line 1912082
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v9, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v9, :cond_1d

    .line 1912083
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1912084
    :goto_6
    move v0, v8

    .line 1912085
    goto/16 :goto_1

    .line 1912086
    :cond_8
    const/4 v8, 0x7

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 1912087
    invoke-virtual {p1, v1, v7}, LX/186;->b(II)V

    .line 1912088
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 1912089
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 1912090
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 1912091
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 1912092
    const/4 v1, 0x5

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1912093
    const/4 v1, 0x6

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1912094
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_9
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    goto/16 :goto_1

    .line 1912095
    :cond_a
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1912096
    :cond_b
    :goto_7
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_c

    .line 1912097
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 1912098
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1912099
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_b

    if-eqz v9, :cond_b

    .line 1912100
    const-string v10, "text"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_a

    .line 1912101
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_7

    .line 1912102
    :cond_c
    const/4 v9, 0x1

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 1912103
    invoke-virtual {p1, v8, v7}, LX/186;->b(II)V

    .line 1912104
    invoke-virtual {p1}, LX/186;->d()I

    move-result v8

    goto/16 :goto_2

    :cond_d
    move v7, v8

    goto :goto_7

    .line 1912105
    :cond_e
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1912106
    :cond_f
    :goto_8
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_10

    .line 1912107
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 1912108
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1912109
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_f

    if-eqz v9, :cond_f

    .line 1912110
    const-string v10, "uri"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_e

    .line 1912111
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_8

    .line 1912112
    :cond_10
    const/4 v9, 0x1

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 1912113
    invoke-virtual {p1, v8, v6}, LX/186;->b(II)V

    .line 1912114
    invoke-virtual {p1}, LX/186;->d()I

    move-result v8

    goto/16 :goto_3

    :cond_11
    move v6, v8

    goto :goto_8

    .line 1912115
    :cond_12
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1912116
    :cond_13
    :goto_9
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_14

    .line 1912117
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 1912118
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1912119
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_13

    if-eqz v9, :cond_13

    .line 1912120
    const-string v10, "text"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_12

    .line 1912121
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_9

    .line 1912122
    :cond_14
    const/4 v9, 0x1

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 1912123
    invoke-virtual {p1, v8, v4}, LX/186;->b(II)V

    .line 1912124
    invoke-virtual {p1}, LX/186;->d()I

    move-result v8

    goto/16 :goto_4

    :cond_15
    move v4, v8

    goto :goto_9

    .line 1912125
    :cond_16
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1912126
    :cond_17
    :goto_a
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_18

    .line 1912127
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 1912128
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1912129
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_17

    if-eqz v9, :cond_17

    .line 1912130
    const-string v10, "text"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_16

    .line 1912131
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_a

    .line 1912132
    :cond_18
    const/4 v9, 0x1

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 1912133
    invoke-virtual {p1, v8, v2}, LX/186;->b(II)V

    .line 1912134
    invoke-virtual {p1}, LX/186;->d()I

    move-result v8

    goto/16 :goto_5

    :cond_19
    move v2, v8

    goto :goto_a

    .line 1912135
    :cond_1a
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1912136
    :cond_1b
    :goto_b
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_1c

    .line 1912137
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 1912138
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1912139
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_1b

    if-eqz v9, :cond_1b

    .line 1912140
    const-string v10, "text"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1a

    .line 1912141
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_b

    .line 1912142
    :cond_1c
    const/4 v9, 0x1

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 1912143
    invoke-virtual {p1, v8, v0}, LX/186;->b(II)V

    .line 1912144
    invoke-virtual {p1}, LX/186;->d()I

    move-result v8

    goto/16 :goto_6

    :cond_1d
    move v0, v8

    goto :goto_b
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1911988
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1911989
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1911990
    if-eqz v0, :cond_1

    .line 1911991
    const-string v1, "byline"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1911992
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1911993
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1911994
    if-eqz v1, :cond_0

    .line 1911995
    const-string v2, "text"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1911996
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1911997
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1911998
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1911999
    if-eqz v0, :cond_3

    .line 1912000
    const-string v1, "external_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1912001
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1912002
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1912003
    if-eqz v1, :cond_2

    .line 1912004
    const-string v2, "uri"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1912005
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1912006
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1912007
    :cond_3
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1912008
    if-eqz v0, :cond_4

    .line 1912009
    const-string v1, "external_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1912010
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1912011
    :cond_4
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1912012
    if-eqz v0, :cond_6

    .line 1912013
    const-string v1, "published_on"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1912014
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1912015
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1912016
    if-eqz v1, :cond_5

    .line 1912017
    const-string v2, "text"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1912018
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1912019
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1912020
    :cond_6
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1912021
    if-eqz v0, :cond_7

    .line 1912022
    const-string v1, "reviewer"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1912023
    invoke-static {p0, v0, p2, p3}, LX/CYg;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1912024
    :cond_7
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1912025
    if-eqz v0, :cond_9

    .line 1912026
    const-string v1, "summary"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1912027
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1912028
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1912029
    if-eqz v1, :cond_8

    .line 1912030
    const-string v2, "text"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1912031
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1912032
    :cond_8
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1912033
    :cond_9
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1912034
    if-eqz v0, :cond_b

    .line 1912035
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1912036
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1912037
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1912038
    if-eqz v1, :cond_a

    .line 1912039
    const-string v2, "text"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1912040
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1912041
    :cond_a
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1912042
    :cond_b
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1912043
    return-void
.end method
