.class public final LX/CgZ;
.super LX/BNO;
.source ""


# instance fields
.field public final synthetic a:Ljava/lang/Long;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:LX/0o8;

.field public final synthetic e:LX/Cga;


# direct methods
.method public constructor <init>(LX/Cga;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;LX/0o8;)V
    .locals 0

    .prologue
    .line 1927265
    iput-object p1, p0, LX/CgZ;->e:LX/Cga;

    iput-object p2, p0, LX/CgZ;->a:Ljava/lang/Long;

    iput-object p3, p0, LX/CgZ;->b:Ljava/lang/String;

    iput-object p4, p0, LX/CgZ;->c:Ljava/lang/String;

    iput-object p5, p0, LX/CgZ;->d:LX/0o8;

    invoke-direct {p0}, LX/BNO;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/fbservice/service/OperationResult;)V
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 1927266
    new-instance v5, Lcom/facebook/reaction/ReactionQueryParams;

    invoke-direct {v5}, Lcom/facebook/reaction/ReactionQueryParams;-><init>()V

    .line 1927267
    iget-object v0, p0, LX/CgZ;->a:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 1927268
    iget-object v0, p0, LX/CgZ;->a:Ljava/lang/Long;

    .line 1927269
    iput-object v0, v5, Lcom/facebook/reaction/ReactionQueryParams;->l:Ljava/lang/Long;

    .line 1927270
    :cond_0
    iget-object v0, p0, LX/CgZ;->b:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1927271
    iget-object v0, p0, LX/CgZ;->b:Ljava/lang/String;

    .line 1927272
    iput-object v0, v5, Lcom/facebook/reaction/ReactionQueryParams;->e:Ljava/lang/String;

    .line 1927273
    :cond_1
    iget-object v0, p0, LX/CgZ;->e:LX/Cga;

    iget-object v0, v0, LX/Cga;->b:Lcom/facebook/reaction/ReactionUtil;

    new-instance v1, LX/CgY;

    invoke-direct {v1, p0}, LX/CgY;-><init>(LX/CgZ;)V

    iget-object v2, p0, LX/CgZ;->d:LX/0o8;

    invoke-interface {v2}, LX/0o8;->n()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/CgZ;->d:LX/0o8;

    invoke-interface {v3}, LX/0o8;->o()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/CgZ;->c:Ljava/lang/String;

    .line 1927274
    if-nez v5, :cond_3

    new-instance v7, Lcom/facebook/reaction/ReactionQueryParams;

    invoke-direct {v7}, Lcom/facebook/reaction/ReactionQueryParams;-><init>()V

    .line 1927275
    iput-object v4, v7, Lcom/facebook/reaction/ReactionQueryParams;->u:Ljava/lang/String;

    .line 1927276
    move-object v7, v7

    .line 1927277
    :goto_0
    invoke-static {v0, v7, v2, v3}, Lcom/facebook/reaction/ReactionUtil;->c(Lcom/facebook/reaction/ReactionUtil;Lcom/facebook/reaction/ReactionQueryParams;Ljava/lang/String;Ljava/lang/String;)LX/0zO;

    move-result-object v7

    .line 1927278
    iget-object p1, v0, Lcom/facebook/reaction/ReactionUtil;->e:LX/0tX;

    invoke-virtual {p1, v7}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v7

    .line 1927279
    iget-object p1, v0, Lcom/facebook/reaction/ReactionUtil;->u:LX/1Ck;

    invoke-virtual {p1, v4, v7, v1}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1927280
    iget-object v0, p0, LX/CgZ;->d:LX/0o8;

    invoke-interface {v0}, LX/0o8;->m()LX/2ja;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1927281
    iget-object v0, p0, LX/CgZ;->d:LX/0o8;

    invoke-interface {v0}, LX/0o8;->m()LX/2ja;

    move-result-object v0

    iget-object v1, p0, LX/CgZ;->c:Ljava/lang/String;

    sget-object v2, LX/Cfc;->WRITE_REVIEW_PUBLISH:LX/Cfc;

    invoke-virtual {v0, v1, v6, v6, v2}, LX/2ja;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/Cfc;)V

    .line 1927282
    :cond_2
    return-void

    .line 1927283
    :cond_3
    iput-object v4, v5, Lcom/facebook/reaction/ReactionQueryParams;->u:Ljava/lang/String;

    .line 1927284
    move-object v7, v5

    .line 1927285
    goto :goto_0
.end method
