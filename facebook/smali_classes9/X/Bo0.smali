.class public LX/Bo0;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:J

.field private static volatile g:LX/Bo0;


# instance fields
.field private final b:LX/0SG;

.field public final c:LX/0y5;

.field private final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public f:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 1821412
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1e

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, LX/Bo0;->a:J

    return-void
.end method

.method public constructor <init>(LX/0SG;LX/0y5;)V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1821413
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1821414
    iput-object p1, p0, LX/Bo0;->b:LX/0SG;

    .line 1821415
    iput-object p2, p0, LX/Bo0;->c:LX/0y5;

    .line 1821416
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/Bo0;->d:Ljava/util/Map;

    .line 1821417
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/Bo0;->e:Ljava/util/Map;

    .line 1821418
    return-void
.end method

.method public static a(LX/0QB;)LX/Bo0;
    .locals 5

    .prologue
    .line 1821419
    sget-object v0, LX/Bo0;->g:LX/Bo0;

    if-nez v0, :cond_1

    .line 1821420
    const-class v1, LX/Bo0;

    monitor-enter v1

    .line 1821421
    :try_start_0
    sget-object v0, LX/Bo0;->g:LX/Bo0;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1821422
    if-eqz v2, :cond_0

    .line 1821423
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1821424
    new-instance p0, LX/Bo0;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v3

    check-cast v3, LX/0SG;

    invoke-static {v0}, LX/0y5;->a(LX/0QB;)LX/0y5;

    move-result-object v4

    check-cast v4, LX/0y5;

    invoke-direct {p0, v3, v4}, LX/Bo0;-><init>(LX/0SG;LX/0y5;)V

    .line 1821425
    move-object v0, p0

    .line 1821426
    sput-object v0, LX/Bo0;->g:LX/Bo0;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1821427
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1821428
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1821429
    :cond_1
    sget-object v0, LX/Bo0;->g:LX/Bo0;

    return-object v0

    .line 1821430
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1821431
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static declared-synchronized b(LX/Bo0;Ljava/lang/String;)J
    .locals 12

    .prologue
    const-wide v2, 0x7fffffffffffffffL

    .line 1821432
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Bo0;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    move-wide v0, v2

    .line 1821433
    :goto_0
    monitor-exit p0

    return-wide v0

    .line 1821434
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/Bo0;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1821435
    iget-object v1, p0, LX/Bo0;->e:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    move-wide v0, v2

    .line 1821436
    goto :goto_0

    .line 1821437
    :cond_1
    iget-object v1, p0, LX/Bo0;->b:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v4

    .line 1821438
    iget-object v8, p0, LX/Bo0;->e:Ljava/util/Map;

    invoke-interface {v8, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Long;

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    const-wide/16 v10, 0x3e8

    mul-long/2addr v8, v10

    move-wide v6, v8

    .line 1821439
    sub-long/2addr v4, v6

    .line 1821440
    sget-wide v6, LX/Bo0;->a:J

    cmp-long v1, v4, v6

    if-gez v1, :cond_2

    .line 1821441
    const-wide/16 v0, 0x0

    invoke-static {v0, v1, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    goto :goto_0

    .line 1821442
    :cond_2
    iget-object v1, p0, LX/Bo0;->d:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1821443
    iget-object v1, p0, LX/Bo0;->e:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-wide v0, v2

    .line 1821444
    goto :goto_0

    .line 1821445
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/lang/String;Ljava/lang/String;J)V
    .locals 3

    .prologue
    .line 1821446
    monitor-enter p0

    .line 1821447
    :try_start_0
    iget-boolean v0, p0, LX/Bo0;->f:Z

    if-eqz v0, :cond_0

    .line 1821448
    :goto_0
    iget-object v0, p0, LX/Bo0;->d:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1821449
    iget-object v0, p0, LX/Bo0;->e:Ljava/util/Map;

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, p2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1821450
    monitor-exit p0

    return-void

    .line 1821451
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1821452
    :cond_0
    iget-object v0, p0, LX/Bo0;->c:LX/0y5;

    .line 1821453
    iget-object v1, v0, LX/0y5;->f:Ljava/util/List;

    invoke-interface {v1, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1821454
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Bo0;->f:Z

    goto :goto_0
.end method
