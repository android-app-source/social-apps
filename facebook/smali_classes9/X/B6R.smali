.class public final LX/B6R;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/api/graphql/leadgen/LeadGenDeepLinkUserInfoCreateMutationModels$LeadGenDeepLinkUserInfoCoreMutationFieldsModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:I

.field public final synthetic b:LX/B6S;


# direct methods
.method public constructor <init>(LX/B6S;I)V
    .locals 0

    .prologue
    .line 1746709
    iput-object p1, p0, LX/B6R;->b:LX/B6S;

    iput p2, p0, LX/B6R;->a:I

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1746710
    iget-object v0, p0, LX/B6R;->b:LX/B6S;

    iget-object v0, v0, LX/B6S;->c:LX/B6l;

    const-string v1, "cta_lead_gen_error_confirmation_card_click"

    iget v2, p0, LX/B6R;->a:I

    invoke-virtual {v0, v1, v2}, LX/B6l;->a(Ljava/lang/String;I)V

    .line 1746711
    iget-object v0, p0, LX/B6R;->b:LX/B6S;

    iget-object v0, v0, LX/B6S;->d:LX/0if;

    if-eqz v0, :cond_0

    .line 1746712
    iget-object v0, p0, LX/B6R;->b:LX/B6S;

    iget-object v0, v0, LX/B6S;->d:LX/0if;

    sget-object v1, LX/0ig;->v:LX/0ih;

    const-string v2, "lead_submission_noncancellation_failure"

    invoke-virtual {v0, v1, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1746713
    :cond_0
    iget-object v0, p0, LX/B6R;->b:LX/B6S;

    iget-object v0, v0, LX/B6S;->e:LX/B72;

    if-eqz v0, :cond_1

    .line 1746714
    iget-object v0, p0, LX/B6R;->b:LX/B6S;

    iget-object v0, v0, LX/B6S;->e:LX/B72;

    const/4 v1, 0x0

    sget-object v2, LX/B76;->FAILURE:LX/B76;

    invoke-virtual {v0, v1, v2}, LX/B72;->a(Lcom/facebook/api/graphql/leadgen/LeadGenDeepLinkUserInfoCreateMutationModels$LeadGenDeepLinkUserInfoCoreMutationFieldsModel;LX/B76;)V

    .line 1746715
    :cond_1
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1746716
    check-cast p1, Lcom/facebook/api/graphql/leadgen/LeadGenDeepLinkUserInfoCreateMutationModels$LeadGenDeepLinkUserInfoCoreMutationFieldsModel;

    .line 1746717
    const/4 v1, 0x0

    .line 1746718
    if-nez p1, :cond_3

    .line 1746719
    :cond_0
    :goto_0
    move v1, v1

    .line 1746720
    move v0, v1

    .line 1746721
    if-eqz v0, :cond_1

    .line 1746722
    iget-object v0, p0, LX/B6R;->b:LX/B6S;

    iget-object v0, v0, LX/B6S;->c:LX/B6l;

    const-string v1, "cta_lead_gen_share_click"

    iget v2, p0, LX/B6R;->a:I

    invoke-virtual {v0, v1, v2}, LX/B6l;->a(Ljava/lang/String;I)V

    .line 1746723
    iget-object v0, p0, LX/B6R;->b:LX/B6S;

    iget-object v0, v0, LX/B6S;->d:LX/0if;

    if-eqz v0, :cond_1

    .line 1746724
    iget-object v0, p0, LX/B6R;->b:LX/B6S;

    iget-object v0, v0, LX/B6S;->d:LX/0if;

    sget-object v1, LX/0ig;->v:LX/0ih;

    const-string v2, "lead_submission_success"

    invoke-virtual {v0, v1, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1746725
    :cond_1
    iget-object v0, p0, LX/B6R;->b:LX/B6S;

    iget-object v0, v0, LX/B6S;->e:LX/B72;

    if-eqz v0, :cond_2

    .line 1746726
    iget-object v0, p0, LX/B6R;->b:LX/B6S;

    iget-object v0, v0, LX/B6S;->e:LX/B72;

    sget-object v1, LX/B76;->SUCCESS:LX/B76;

    invoke-virtual {v0, p1, v1}, LX/B72;->a(Lcom/facebook/api/graphql/leadgen/LeadGenDeepLinkUserInfoCreateMutationModels$LeadGenDeepLinkUserInfoCoreMutationFieldsModel;LX/B76;)V

    .line 1746727
    :cond_2
    return-void

    .line 1746728
    :cond_3
    invoke-virtual {p1}, Lcom/facebook/api/graphql/leadgen/LeadGenDeepLinkUserInfoCreateMutationModels$LeadGenDeepLinkUserInfoCoreMutationFieldsModel;->a()Lcom/facebook/api/graphql/leadgen/LeadGenDeepLinkUserInfoCreateMutationModels$LeadGenDeepLinkUserInfoCoreMutationFieldsModel$LeadGenDeepLinkUserStatusModel;

    move-result-object v0

    .line 1746729
    if-eqz v0, :cond_0

    .line 1746730
    invoke-virtual {v0}, Lcom/facebook/api/graphql/leadgen/LeadGenDeepLinkUserInfoCreateMutationModels$LeadGenDeepLinkUserInfoCoreMutationFieldsModel$LeadGenDeepLinkUserStatusModel;->b()Z

    move-result v1

    goto :goto_0
.end method
