.class public final LX/BJT;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1771972
    iput-object p1, p0, LX/BJT;->b:Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;

    iput-object p2, p0, LX/BJT;->a:Ljava/lang/String;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1771980
    iget-object v0, p0, LX/BJT;->b:Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;

    iget-object v0, v0, Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;->a:LX/BJR;

    invoke-virtual {v0, p1}, LX/BJR;->a(Ljava/lang/Throwable;)LX/0am;

    move-result-object v0

    .line 1771981
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1771982
    iget-object v1, p0, LX/BJT;->b:Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    iget-object v2, p0, LX/BJT;->b:Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;

    iget-object v2, v2, Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;->a:LX/BJR;

    invoke-static {v1, v0, v2}, Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;->a$redex0(Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;Lcom/facebook/graphql/model/GraphQLStoryAttachment;LX/BJR;)V

    .line 1771983
    :goto_0
    return-void

    .line 1771984
    :cond_0
    iget-object v0, p0, LX/BJT;->b:Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;

    invoke-static {v0}, Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;->d(Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;)V

    goto :goto_0
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 1771973
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1771974
    if-nez p1, :cond_0

    .line 1771975
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "story_attachment_null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, LX/BJT;->onNonCancellationFailure(Ljava/lang/Throwable;)V

    .line 1771976
    :goto_0
    return-void

    .line 1771977
    :cond_0
    iget-object v0, p0, LX/BJT;->b:Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;

    iget-object v1, p0, LX/BJT;->a:Ljava/lang/String;

    invoke-static {v1}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v1

    .line 1771978
    iput-object v1, v0, Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;->c:LX/0am;

    .line 1771979
    iget-object v0, p0, LX/BJT;->b:Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;

    iget-object v1, p0, LX/BJT;->b:Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;

    iget-object v1, v1, Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;->a:LX/BJR;

    invoke-static {v0, p1, v1}, Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;->a$redex0(Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentFragment;Lcom/facebook/graphql/model/GraphQLStoryAttachment;LX/BJR;)V

    goto :goto_0
.end method
