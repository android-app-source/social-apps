.class public final LX/CMu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/4ck;


# instance fields
.field public final synthetic a:LX/2Mo;

.field private b:J

.field private c:J

.field private d:Z


# direct methods
.method public constructor <init>(LX/2Mo;)V
    .locals 2

    .prologue
    const-wide/16 v0, 0x0

    .line 1881057
    iput-object p1, p0, LX/CMu;->a:LX/2Mo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1881058
    iput-wide v0, p0, LX/CMu;->b:J

    .line 1881059
    iput-wide v0, p0, LX/CMu;->c:J

    .line 1881060
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/CMu;->d:Z

    return-void
.end method


# virtual methods
.method public final a(JJ)V
    .locals 9

    .prologue
    .line 1881061
    iget-object v0, p0, LX/CMu;->a:LX/2Mo;

    iget-object v0, v0, LX/2Mo;->f:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    .line 1881062
    iget-wide v2, p0, LX/CMu;->b:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_1

    .line 1881063
    iput-wide v0, p0, LX/CMu;->b:J

    .line 1881064
    iput-wide p1, p0, LX/CMu;->c:J

    .line 1881065
    :cond_0
    :goto_0
    return-void

    .line 1881066
    :cond_1
    iget-boolean v2, p0, LX/CMu;->d:Z

    if-nez v2, :cond_0

    const-wide/16 v2, 0x64

    mul-long/2addr v2, p1

    div-long/2addr v2, p3

    long-to-double v2, v2

    const-wide v4, 0x4056800000000000L    # 90.0

    cmpl-double v2, v2, v4

    if-lez v2, :cond_0

    .line 1881067
    const/4 v2, 0x1

    iput-boolean v2, p0, LX/CMu;->d:Z

    .line 1881068
    iget-object v2, p0, LX/CMu;->a:LX/2Mo;

    iget-wide v4, p0, LX/CMu;->c:J

    sub-long v4, p1, v4

    iget-wide v6, p0, LX/CMu;->b:J

    sub-long/2addr v0, v6

    invoke-virtual {v2, v4, v5, v0, v1}, LX/2Mo;->a(JJ)V

    goto :goto_0
.end method
