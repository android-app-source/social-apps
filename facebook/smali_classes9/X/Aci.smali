.class public final LX/Aci;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/facecastdisplay/protocol/FetchMomentsOfInterestQueryModels$FetchMomentsOfInterestQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Ack;


# direct methods
.method public constructor <init>(LX/Ack;)V
    .locals 0

    .prologue
    .line 1693067
    iput-object p1, p0, LX/Aci;->a:LX/Ack;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 1693077
    iget-object v0, p0, LX/Aci;->a:LX/Ack;

    iget-object v0, v0, LX/Ack;->d:LX/03V;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, LX/Ack;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_graphFailure"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to fetch moments of interest GraphQL for video "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/Aci;->a:LX/Ack;

    iget-object v3, v3, LX/Ack;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1693078
    iget-object v0, p0, LX/Aci;->a:LX/Ack;

    iget-object v0, v0, LX/Ack;->e:LX/Acj;

    if-eqz v0, :cond_0

    .line 1693079
    :cond_0
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1693068
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1693069
    if-eqz p1, :cond_0

    .line 1693070
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1693071
    if-nez v0, :cond_1

    .line 1693072
    :cond_0
    :goto_0
    return-void

    .line 1693073
    :cond_1
    iget-object v0, p0, LX/Aci;->a:LX/Ack;

    iget-object v0, v0, LX/Ack;->e:LX/Acj;

    if-eqz v0, :cond_0

    .line 1693074
    iget-object v0, p0, LX/Aci;->a:LX/Ack;

    iget-object v1, v0, LX/Ack;->e:LX/Acj;

    .line 1693075
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1693076
    check-cast v0, Lcom/facebook/facecastdisplay/protocol/FetchMomentsOfInterestQueryModels$FetchMomentsOfInterestQueryModel;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchMomentsOfInterestQueryModels$FetchMomentsOfInterestQueryModel;->j()LX/0Px;

    move-result-object v0

    invoke-interface {v1, v0}, LX/Acj;->a(LX/0Px;)V

    goto :goto_0
.end method
