.class public LX/Bus;
.super LX/Bur;
.source ""


# instance fields
.field public final b:LX/3Gp;


# direct methods
.method public constructor <init>(LX/1Pf;LX/0wD;Ljava/lang/String;LX/0Or;LX/0Or;LX/1Kf;LX/1Ck;LX/0Ot;LX/0lC;LX/1Sa;LX/1Sj;LX/0Ot;LX/16H;LX/0Or;LX/0Sh;LX/0bH;LX/0Or;LX/0Zb;LX/17Q;LX/0Or;LX/0Or;LX/0SG;LX/0Or;LX/0Or;LX/0Or;LX/14w;LX/0ad;LX/0Or;LX/0qn;LX/0W3;LX/0Ot;LX/0Or;LX/3Gp;Lcom/facebook/prefs/shared/FbSharedPreferences;Landroid/support/v4/app/FragmentActivity;LX/0Or;LX/1Sl;LX/0tX;LX/1Sm;LX/0Ot;LX/3HS;LX/0Ot;LX/0Ot;LX/0Ot;LX/17Y;Ljava/lang/Boolean;LX/3HT;LX/AVU;LX/1b4;LX/AaZ;LX/0Ot;LX/0wL;)V
    .locals 1
    .param p1    # LX/1Pf;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/0wD;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p17    # LX/0Or;
        .annotation runtime Lcom/facebook/feed/annotations/IsNativeNewsfeedSpamReportingEnabled;
        .end annotation
    .end param
    .param p20    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/IsMeUserAnEmployee;
        .end annotation
    .end param
    .param p24    # LX/0Or;
        .annotation runtime Lcom/facebook/feed/annotations/IsNotifyMeSubscriptionEnabled;
        .end annotation
    .end param
    .param p36    # LX/0Or;
        .annotation runtime Lcom/facebook/feed/annotations/IsInNewPlayerOldUIClosedCaptioningGateKeeper;
        .end annotation
    .end param
    .param p46    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Pf;",
            "LX/0wD;",
            "Ljava/lang/String;",
            "LX/0Or",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;",
            "LX/1Kf;",
            "LX/1Ck;",
            "LX/0Ot",
            "<",
            "LX/9LP;",
            ">;",
            "LX/0lC;",
            "LX/1Sa;",
            "LX/1Sj;",
            "LX/0Ot",
            "<",
            "LX/79m;",
            ">;",
            "LX/16H;",
            "LX/0Or",
            "<",
            "LX/1dv;",
            ">;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/0bH;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Zb;",
            "LX/17Q;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0kL;",
            ">;",
            "LX/0SG;",
            "LX/0Or",
            "<",
            "LX/8Q3;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "LX/BNQ;",
            ">;",
            "LX/14w;",
            "LX/0ad;",
            "LX/0Or",
            "<",
            "LX/3Af;",
            ">;",
            "LX/0qn;",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            "LX/0Ot",
            "<",
            "LX/9yI;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0gt;",
            ">;",
            "LX/3Gp;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "Landroid/support/v4/app/FragmentActivity;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/1Sl;",
            "LX/0tX;",
            "LX/1Sm;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;",
            ">;",
            "LX/3HS;",
            "LX/0Ot",
            "<",
            "LX/BUA;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0tQ;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/video/downloadmanager/db/OfflineVideoCache;",
            ">;",
            "LX/17Y;",
            "Ljava/lang/Boolean;",
            "LX/3HT;",
            "LX/AVU;",
            "LX/1b4;",
            "LX/AaZ;",
            "LX/0Ot",
            "<",
            "LX/17Y;",
            ">;",
            "LX/0wL;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1831813
    invoke-direct/range {p0 .. p52}, LX/Bur;-><init>(LX/1Pf;LX/0wD;Ljava/lang/String;LX/0Or;LX/0Or;LX/1Kf;LX/1Ck;LX/0Ot;LX/0lC;LX/1Sa;LX/1Sj;LX/0Ot;LX/16H;LX/0Or;LX/0Sh;LX/0bH;LX/0Or;LX/0Zb;LX/17Q;LX/0Or;LX/0Or;LX/0SG;LX/0Or;LX/0Or;LX/0Or;LX/14w;LX/0ad;LX/0Or;LX/0qn;LX/0W3;LX/0Ot;LX/0Or;LX/3Gp;Lcom/facebook/prefs/shared/FbSharedPreferences;Landroid/support/v4/app/FragmentActivity;LX/0Or;LX/1Sl;LX/0tX;LX/1Sm;LX/0Ot;LX/3HS;LX/0Ot;LX/0Ot;LX/0Ot;LX/17Y;Ljava/lang/Boolean;LX/3HT;LX/AVU;LX/1b4;LX/AaZ;LX/0Ot;LX/0wL;)V

    .line 1831814
    move-object/from16 v0, p33

    iput-object v0, p0, LX/Bus;->b:LX/3Gp;

    .line 1831815
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/flatbuffers/Flattenable;",
            ">;",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1831816
    invoke-super {p0, p1, p2}, LX/Bur;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;)V

    .line 1831817
    return-void
.end method

.method public final d(Lcom/facebook/graphql/model/FeedUnit;)LX/1wH;
    .locals 1

    .prologue
    .line 1831818
    new-instance v0, LX/Buq;

    invoke-direct {v0, p0}, LX/Buq;-><init>(LX/Bus;)V

    return-object v0
.end method
