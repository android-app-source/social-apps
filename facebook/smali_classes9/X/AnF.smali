.class public abstract LX/AnF;
.super LX/2sw;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/0hB;

.field public final c:LX/1DR;

.field public d:LX/1PW;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:I

.field public f:I


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0hB;LX/1DR;)V
    .locals 0

    .prologue
    .line 1712244
    invoke-direct {p0}, LX/2sw;-><init>()V

    .line 1712245
    iput-object p1, p0, LX/AnF;->a:Landroid/content/Context;

    .line 1712246
    iput-object p2, p0, LX/AnF;->b:LX/0hB;

    .line 1712247
    iput-object p3, p0, LX/AnF;->c:LX/1DR;

    .line 1712248
    return-void
.end method

.method public static a(LX/AnE;Landroid/support/v4/view/ViewPager;)V
    .locals 2

    .prologue
    .line 1712249
    iget-boolean v0, p0, LX/99W;->b:Z

    move v0, v0

    .line 1712250
    if-eqz v0, :cond_0

    .line 1712251
    invoke-virtual {p1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 1712252
    invoke-virtual {p0}, LX/0gG;->b()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1712253
    invoke-virtual {p1, v0}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 1712254
    :cond_0
    return-void
.end method


# virtual methods
.method public a(LX/99X;)F
    .locals 2

    .prologue
    .line 1712255
    sget-object v0, LX/99X;->FIRST:LX/99X;

    if-ne p1, v0, :cond_0

    .line 1712256
    invoke-virtual {p0}, LX/AnF;->d()I

    move-result v0

    invoke-virtual {p0}, LX/AnF;->f()I

    move-result v1

    add-int/2addr v0, v1

    iget v1, p0, LX/AnF;->f:I

    add-int/2addr v0, v1

    .line 1712257
    int-to-float v0, v0

    iget v1, p0, LX/AnF;->e:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 1712258
    :goto_0
    return v0

    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0
.end method

.method public a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;",
            ">;)",
            "LX/162;"
        }
    .end annotation

    .prologue
    .line 1712259
    const/4 v0, 0x0

    return-object v0
.end method

.method public a(LX/1PW;)V
    .locals 0

    .prologue
    .line 1712262
    iput-object p1, p0, LX/AnF;->d:LX/1PW;

    .line 1712263
    return-void
.end method

.method public a(LX/AnE;Landroid/support/v4/view/ViewPager;Landroid/view/View;Ljava/lang/Object;Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)V
    .locals 0

    .prologue
    .line 1712260
    invoke-static {p1, p2}, LX/AnF;->a(LX/AnE;Landroid/support/v4/view/ViewPager;)V

    .line 1712261
    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 1712269
    return-void
.end method

.method public final a(Landroid/view/View;LX/99X;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1712264
    new-instance v0, LX/18u;

    invoke-direct {v0}, LX/18u;-><init>()V

    invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1712265
    sget-object v0, LX/99X;->FIRST:LX/99X;

    if-ne p2, v0, :cond_0

    .line 1712266
    invoke-virtual {p0}, LX/AnF;->d()I

    move-result v0

    iget v1, p0, LX/AnF;->f:I

    invoke-virtual {p1, v0, v2, v1, v2}, Landroid/view/View;->setPadding(IIII)V

    .line 1712267
    :goto_0
    return-void

    .line 1712268
    :cond_0
    iget v0, p0, LX/AnF;->f:I

    iget v1, p0, LX/AnF;->f:I

    invoke-virtual {p1, v0, v2, v1, v2}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_0
.end method

.method public abstract a(Landroid/view/View;Ljava/lang/Object;LX/99X;LX/AnC;Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)V
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/AnD;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;",
            ">;",
            "LX/AnD;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1712237
    const/4 v0, 0x1

    move v0, v0

    .line 1712238
    if-eqz v0, :cond_0

    .line 1712239
    iget-object v0, p2, LX/AnD;->b:Landroid/widget/TextView;

    move-object v0, v0

    .line 1712240
    invoke-virtual {p0, p1, v0}, LX/AnF;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/widget/TextView;)V

    .line 1712241
    :goto_0
    return-void

    .line 1712242
    :cond_0
    iget-object v0, p2, LX/AnD;->f:Landroid/view/View;

    const/16 p0, 0x8

    invoke-virtual {v0, p0}, Landroid/view/View;->setVisibility(I)V

    .line 1712243
    goto :goto_0
.end method

.method public a(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/widget/TextView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;",
            ">;",
            "Landroid/widget/TextView;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1712236
    return-void
.end method

.method public a(Lcom/facebook/widget/CustomViewPager;Landroid/content/res/Resources;)V
    .locals 1

    .prologue
    .line 1712231
    iget-object v0, p0, LX/AnF;->c:LX/1DR;

    invoke-virtual {v0}, LX/1DR;->a()I

    move-result v0

    iput v0, p0, LX/AnF;->e:I

    .line 1712232
    iget v0, p0, LX/AnF;->e:I

    invoke-virtual {p0}, LX/AnF;->f()I

    move-result p2

    sub-int/2addr v0, p2

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, LX/AnF;->f:I

    .line 1712233
    iget v0, p0, LX/AnF;->f:I

    mul-int/lit8 v0, v0, 0x2

    invoke-virtual {p0}, LX/AnF;->g()I

    move-result p2

    sub-int/2addr v0, p2

    .line 1712234
    mul-int/lit8 v0, v0, -0x1

    invoke-virtual {p1, v0}, Landroid/support/v4/view/ViewPager;->setPageMargin(I)V

    .line 1712235
    return-void
.end method

.method public a(Ljava/util/List;Lcom/facebook/widget/CustomViewPager;)V
    .locals 2

    .prologue
    .line 1712229
    invoke-virtual {p0}, LX/AnF;->e()I

    move-result v0

    const/4 v1, 0x1

    invoke-virtual {p2, v0, v1}, Lcom/facebook/widget/CustomViewPager;->b(IZ)V

    .line 1712230
    return-void
.end method

.method public b()I
    .locals 2

    .prologue
    .line 1712228
    iget v0, p0, LX/AnF;->e:I

    invoke-virtual {p0}, LX/AnF;->f()I

    move-result v1

    div-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public abstract c()Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;",
            ">;"
        }
    .end annotation
.end method

.method public d()I
    .locals 1

    .prologue
    .line 1712227
    const/4 v0, 0x0

    return v0
.end method

.method public e()I
    .locals 1

    .prologue
    .line 1712226
    const/4 v0, 0x0

    return v0
.end method

.method public f()I
    .locals 1

    .prologue
    .line 1712225
    const/4 v0, 0x0

    return v0
.end method

.method public g()I
    .locals 1

    .prologue
    .line 1712224
    const/4 v0, 0x0

    return v0
.end method
