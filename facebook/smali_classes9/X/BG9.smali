.class public final LX/BG9;
.super LX/1Ur;
.source ""


# instance fields
.field public i:Landroid/graphics/RectF;

.field public j:Landroid/graphics/RectF;

.field public k:I

.field public l:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1766743
    invoke-direct {p0}, LX/1Ur;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Matrix;Landroid/graphics/Rect;IIFFFF)V
    .locals 6

    .prologue
    .line 1766744
    if-ge p3, p4, :cond_0

    iget-object v0, p0, LX/BG9;->i:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    iget-object v1, p0, LX/BG9;->i:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v1

    cmpg-float v0, v0, v1

    if-ltz v0, :cond_1

    :cond_0
    move p7, p8

    move v5, p3

    move p3, p4

    move p4, v5

    .line 1766745
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Matrix;->reset()V

    .line 1766746
    iget-object v0, p0, LX/BG9;->i:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    iget-object v1, p0, LX/BG9;->i:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_4

    const/4 v0, 0x1

    .line 1766747
    :goto_0
    iget-object v1, p0, LX/BG9;->i:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v1

    iget-object v2, p0, LX/BG9;->j:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v2

    div-float/2addr v1, v2

    .line 1766748
    mul-float/2addr v1, p7

    .line 1766749
    invoke-virtual {p1, v1, v1}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 1766750
    invoke-static {p3, p4}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 1766751
    int-to-float v2, v2

    mul-float/2addr v2, v1

    if-eqz v0, :cond_5

    iget-object v1, p0, LX/BG9;->i:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v1

    :goto_1
    div-float v1, v2, v1

    .line 1766752
    iget-object v2, p0, LX/BG9;->i:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    iget-object v3, p0, LX/BG9;->j:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->left:F

    sub-float/2addr v2, v3

    mul-float/2addr v2, v1

    .line 1766753
    iget-object v3, p0, LX/BG9;->i:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    iget-object v4, p0, LX/BG9;->j:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    sub-float/2addr v3, v4

    mul-float/2addr v1, v3

    .line 1766754
    if-eqz v0, :cond_6

    .line 1766755
    invoke-virtual {p1, v2, v1}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 1766756
    :goto_2
    iget-boolean v1, p0, LX/BG9;->l:Z

    if-eqz v1, :cond_2

    .line 1766757
    const/high16 v1, -0x40800000    # -1.0f

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {p2}, Landroid/graphics/Rect;->centerX()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p2}, Landroid/graphics/Rect;->centerY()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {p1, v1, v2, v3, v4}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 1766758
    :cond_2
    iget v1, p0, LX/BG9;->k:I

    if-nez v1, :cond_3

    if-nez v0, :cond_3

    .line 1766759
    iget v0, p0, LX/BG9;->k:I

    int-to-float v0, v0

    invoke-virtual {p2}, Landroid/graphics/Rect;->centerX()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p2}, Landroid/graphics/Rect;->centerY()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Matrix;->setRotate(FFF)V

    .line 1766760
    :cond_3
    return-void

    .line 1766761
    :cond_4
    const/4 v0, 0x0

    goto :goto_0

    .line 1766762
    :cond_5
    iget-object v1, p0, LX/BG9;->i:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v1

    goto :goto_1

    .line 1766763
    :cond_6
    invoke-virtual {p1, v1, v2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    goto :goto_2
.end method
