.class public final LX/BJc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/BKm;

.field public final synthetic b:LX/BJp;

.field public final synthetic c:Lcom/facebook/platform/composer/draftpost/DraftPostComposerCancelDialogFragment;

.field public final synthetic d:LX/BJi;


# direct methods
.method public constructor <init>(LX/BJi;LX/BKm;LX/BJp;Lcom/facebook/platform/composer/draftpost/DraftPostComposerCancelDialogFragment;)V
    .locals 0

    .prologue
    .line 1772250
    iput-object p1, p0, LX/BJc;->d:LX/BJi;

    iput-object p2, p0, LX/BJc;->a:LX/BKm;

    iput-object p3, p0, LX/BJc;->b:LX/BJp;

    iput-object p4, p0, LX/BJc;->c:Lcom/facebook/platform/composer/draftpost/DraftPostComposerCancelDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x273cdb42

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1772251
    iget-object v1, p0, LX/BJc;->d:LX/BJi;

    iget-object v1, v1, LX/BJi;->a:LX/BJ7;

    iget-object v2, p0, LX/BJc;->a:LX/BKm;

    iget-object v2, v2, LX/BKm;->a:Ljava/lang/String;

    .line 1772252
    new-instance v4, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object p1, LX/BJ6;->DRAFT_DIALOG_DISCARD_CLICKED:LX/BJ6;

    iget-object p1, p1, LX/BJ6;->name:Ljava/lang/String;

    invoke-direct {v4, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p1, "composer_session_id"

    invoke-virtual {v4, p1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    invoke-static {v1, v4}, LX/BJ7;->a(LX/BJ7;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1772253
    iget-object v1, p0, LX/BJc;->b:LX/BJp;

    invoke-virtual {v1}, LX/BJp;->c()V

    .line 1772254
    iget-object v1, p0, LX/BJc;->c:Lcom/facebook/platform/composer/draftpost/DraftPostComposerCancelDialogFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 1772255
    const v1, -0x665535b6

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
