.class public final LX/C85;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/C87;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/C86;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/C87",
            "<TE;>.GroupsBasicStoryComponentImpl;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/C87;

.field private c:[Ljava/lang/String;

.field private d:I

.field private e:Ljava/util/BitSet;


# direct methods
.method public constructor <init>(LX/C87;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 1852320
    iput-object p1, p0, LX/C85;->b:LX/C87;

    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 1852321
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "storyProps"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "environment"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/C85;->c:[Ljava/lang/String;

    .line 1852322
    iput v3, p0, LX/C85;->d:I

    .line 1852323
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/C85;->d:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/C85;->e:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/C85;LX/1De;IILX/C86;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "II",
            "LX/C87",
            "<TE;>.GroupsBasicStoryComponentImpl;)V"
        }
    .end annotation

    .prologue
    .line 1852344
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 1852345
    iput-object p4, p0, LX/C85;->a:LX/C86;

    .line 1852346
    iget-object v0, p0, LX/C85;->e:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 1852347
    return-void
.end method


# virtual methods
.method public final a(LX/1Pn;)LX/C85;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)",
            "LX/C87",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 1852341
    iget-object v0, p0, LX/C85;->a:LX/C86;

    iput-object p1, v0, LX/C86;->b:LX/1Pn;

    .line 1852342
    iget-object v0, p0, LX/C85;->e:Ljava/util/BitSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1852343
    return-object p0
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/C85;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "LX/C87",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 1852338
    iget-object v0, p0, LX/C85;->a:LX/C86;

    iput-object p1, v0, LX/C86;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1852339
    iget-object v0, p0, LX/C85;->e:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1852340
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 1852334
    invoke-super {p0}, LX/1X5;->a()V

    .line 1852335
    const/4 v0, 0x0

    iput-object v0, p0, LX/C85;->a:LX/C86;

    .line 1852336
    iget-object v0, p0, LX/C85;->b:LX/C87;

    iget-object v0, v0, LX/C87;->b:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 1852337
    return-void
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/C87;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1852324
    iget-object v1, p0, LX/C85;->e:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/C85;->e:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/C85;->d:I

    if-ge v1, v2, :cond_2

    .line 1852325
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1852326
    :goto_0
    iget v2, p0, LX/C85;->d:I

    if-ge v0, v2, :cond_1

    .line 1852327
    iget-object v2, p0, LX/C85;->e:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1852328
    iget-object v2, p0, LX/C85;->c:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1852329
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1852330
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1852331
    :cond_2
    iget-object v0, p0, LX/C85;->a:LX/C86;

    .line 1852332
    invoke-virtual {p0}, LX/C85;->a()V

    .line 1852333
    return-object v0
.end method
