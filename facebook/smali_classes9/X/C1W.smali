.class public LX/C1W;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field public final a:LX/189;

.field public final b:LX/1Ck;

.field public final c:LX/0bH;

.field public final d:LX/0tX;

.field public final e:Ljava/lang/String;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;LX/189;LX/0tX;LX/1Ck;LX/0bH;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1842509
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1842510
    iput-object p1, p0, LX/C1W;->e:Ljava/lang/String;

    .line 1842511
    iput-object p2, p0, LX/C1W;->a:LX/189;

    .line 1842512
    iput-object p3, p0, LX/C1W;->d:LX/0tX;

    .line 1842513
    iput-object p4, p0, LX/C1W;->b:LX/1Ck;

    .line 1842514
    iput-object p5, p0, LX/C1W;->c:LX/0bH;

    .line 1842515
    return-void
.end method

.method public static a(LX/C1W;Lcom/facebook/graphql/model/GraphQLStory;)LX/0Vd;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ")",
            "LX/0Vd",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1842516
    new-instance v0, LX/C1V;

    invoke-direct {v0, p0, p1}, LX/C1V;-><init>(LX/C1W;Lcom/facebook/graphql/model/GraphQLStory;)V

    return-object v0
.end method

.method public static a(LX/0QB;)LX/C1W;
    .locals 9

    .prologue
    .line 1842498
    const-class v1, LX/C1W;

    monitor-enter v1

    .line 1842499
    :try_start_0
    sget-object v0, LX/C1W;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1842500
    sput-object v2, LX/C1W;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1842501
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1842502
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1842503
    new-instance v3, LX/C1W;

    invoke-static {v0}, LX/0dG;->b(LX/0QB;)Ljava/lang/String;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v0}, LX/189;->b(LX/0QB;)LX/189;

    move-result-object v5

    check-cast v5, LX/189;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v6

    check-cast v6, LX/0tX;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v7

    check-cast v7, LX/1Ck;

    invoke-static {v0}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v8

    check-cast v8, LX/0bH;

    invoke-direct/range {v3 .. v8}, LX/C1W;-><init>(Ljava/lang/String;LX/189;LX/0tX;LX/1Ck;LX/0bH;)V

    .line 1842504
    move-object v0, v3

    .line 1842505
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1842506
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C1W;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1842507
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1842508
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
