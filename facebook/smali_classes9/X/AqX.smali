.class public LX/AqX;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements LX/AkM;


# instance fields
.field public a:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Lcom/facebook/fbui/facepile/FacepileView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1718043
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1718044
    const v0, 0x7f0303b7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1718045
    new-instance p1, LX/0zw;

    const v0, 0x7f0d0bb7

    invoke-virtual {p0, v0}, LX/AqX;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-direct {p1, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    iput-object p1, p0, LX/AqX;->a:LX/0zw;

    .line 1718046
    return-void
.end method


# virtual methods
.method public final a()Landroid/view/View;
    .locals 0

    .prologue
    .line 1718047
    return-object p0
.end method

.method public final a(LX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/6UY;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1718048
    iget-object v0, p0, LX/AqX;->a:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/facepile/FacepileView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/facepile/FacepileView;->setFaces(Ljava/util/List;)V

    .line 1718049
    return-void
.end method

.method public getCollapseAnimator()Landroid/animation/Animator;
    .locals 1

    .prologue
    .line 1718050
    const/4 v0, 0x0

    return-object v0
.end method

.method public getExpandAnimator()Landroid/animation/Animator;
    .locals 1

    .prologue
    .line 1718051
    const/4 v0, 0x0

    return-object v0
.end method
