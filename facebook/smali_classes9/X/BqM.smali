.class public final LX/BqM;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/BqO;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/graphql/model/GraphQLStoryInsights;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1824608
    invoke-static {}, LX/BqO;->q()LX/BqO;

    move-result-object v0

    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1824609
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1824610
    const-string v0, "BoostBarComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1824611
    if-ne p0, p1, :cond_1

    .line 1824612
    :cond_0
    :goto_0
    return v0

    .line 1824613
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1824614
    goto :goto_0

    .line 1824615
    :cond_3
    check-cast p1, LX/BqM;

    .line 1824616
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1824617
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1824618
    if-eq v2, v3, :cond_0

    .line 1824619
    iget-object v2, p0, LX/BqM;->a:Lcom/facebook/graphql/model/GraphQLStoryInsights;

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/BqM;->a:Lcom/facebook/graphql/model/GraphQLStoryInsights;

    iget-object v3, p1, LX/BqM;->a:Lcom/facebook/graphql/model/GraphQLStoryInsights;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1824620
    goto :goto_0

    .line 1824621
    :cond_4
    iget-object v2, p1, LX/BqM;->a:Lcom/facebook/graphql/model/GraphQLStoryInsights;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
