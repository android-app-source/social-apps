.class public final LX/BzS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/BzZ;

.field public final synthetic b:LX/47G;

.field public final synthetic c:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic d:Landroid/view/View$OnClickListener;

.field public final synthetic e:Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoWithDirectInstallOverlayPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoWithDirectInstallOverlayPartDefinition;LX/BzZ;LX/47G;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View$OnClickListener;)V
    .locals 0

    .prologue
    .line 1838734
    iput-object p1, p0, LX/BzS;->e:Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoWithDirectInstallOverlayPartDefinition;

    iput-object p2, p0, LX/BzS;->a:LX/BzZ;

    iput-object p3, p0, LX/BzS;->b:LX/47G;

    iput-object p4, p0, LX/BzS;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    iput-object p5, p0, LX/BzS;->d:Landroid/view/View$OnClickListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x2

    const/4 v0, 0x1

    const v1, 0x7f9df6dc

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1838726
    iget-object v1, p0, LX/BzS;->a:LX/BzZ;

    .line 1838727
    iget-object v2, v1, LX/BzZ;->b:Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    move-object v1, v2

    .line 1838728
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;->DOWNLOADING:Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    if-eq v1, v2, :cond_0

    iget-object v1, p0, LX/BzS;->a:LX/BzZ;

    .line 1838729
    iget-object v2, v1, LX/BzZ;->b:Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    move-object v1, v2

    .line 1838730
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;->INSTALLED:Lcom/facebook/graphql/enums/GraphQLAppStoreApplicationInstallState;

    if-eq v1, v2, :cond_0

    .line 1838731
    iget-object v1, p0, LX/BzS;->e:Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoWithDirectInstallOverlayPartDefinition;

    const-string v2, "neko_di_click_overlay_button_to_start_install"

    iget-object v3, p0, LX/BzS;->b:LX/47G;

    iget-object v4, p0, LX/BzS;->e:Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoWithDirectInstallOverlayPartDefinition;

    iget-object v5, p0, LX/BzS;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v4, v5, v7}, Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoWithDirectInstallOverlayPartDefinition;->a(Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoWithDirectInstallOverlayPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;Z)Ljava/util/Map;

    move-result-object v4

    invoke-static {v1, v2, v3, v4, v7}, Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoWithDirectInstallOverlayPartDefinition;->a$redex0(Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoWithDirectInstallOverlayPartDefinition;Ljava/lang/String;LX/47G;Ljava/util/Map;Z)V

    .line 1838732
    :cond_0
    iget-object v1, p0, LX/BzS;->d:Landroid/view/View$OnClickListener;

    invoke-interface {v1, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 1838733
    const v1, 0x5bafd5e6

    invoke-static {v6, v6, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
