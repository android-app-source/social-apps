.class public final LX/Bth;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0fx;


# instance fields
.field public final synthetic a:LX/0fx;

.field public final synthetic b:Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;LX/0fx;)V
    .locals 0

    .prologue
    .line 1829393
    iput-object p1, p0, LX/Bth;->b:Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;

    iput-object p2, p0, LX/Bth;->a:LX/0fx;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0g8;I)V
    .locals 1

    .prologue
    .line 1829394
    iget-object v0, p0, LX/Bth;->a:LX/0fx;

    invoke-interface {v0, p1, p2}, LX/0fx;->a(LX/0g8;I)V

    .line 1829395
    return-void
.end method

.method public final a(LX/0g8;III)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1829396
    iget-object v1, p0, LX/Bth;->a:LX/0fx;

    invoke-interface {v1, p1, p2, p3, p4}, LX/0fx;->a(LX/0g8;III)V

    .line 1829397
    if-eqz p4, :cond_1

    if-nez p2, :cond_2

    invoke-interface {p1, v0}, LX/0g8;->e(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-interface {p1, v0}, LX/0g8;->e(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    invoke-interface {p1}, LX/0g8;->g()I

    move-result v2

    if-lt v1, v2, :cond_2

    iget-object v1, p0, LX/Bth;->b:Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;

    const/4 v2, 0x0

    .line 1829398
    iget-boolean p1, v1, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->aD:Z

    if-nez p1, :cond_3

    .line 1829399
    :cond_0
    :goto_0
    move v1, v2

    .line 1829400
    if-nez v1, :cond_2

    :cond_1
    const/4 v0, 0x1

    .line 1829401
    :cond_2
    iget-object v1, p0, LX/Bth;->b:Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;

    iget-object v1, v1, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->Z:Lcom/facebook/widget/FbSwipeRefreshLayout;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/FbSwipeRefreshLayout;->setEnabled(Z)V

    .line 1829402
    return-void

    :cond_3
    iget-object p1, v1, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->b:Landroid/support/design/widget/AppBarLayout;

    invoke-virtual {p1}, Landroid/support/design/widget/AppBarLayout;->getHeight()I

    move-result p1

    iget-object p2, v1, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->b:Landroid/support/design/widget/AppBarLayout;

    invoke-virtual {p2}, Landroid/support/design/widget/AppBarLayout;->getBottom()I

    move-result p2

    sub-int/2addr p1, p2

    if-eqz p1, :cond_0

    const/4 v2, 0x1

    goto :goto_0
.end method
