.class public LX/Bg2;
.super Landroid/view/animation/Animation;
.source ""


# instance fields
.field private final a:F

.field private final b:I

.field private final c:Landroid/view/View;

.field private final d:I


# direct methods
.method public constructor <init>(Landroid/view/View;F)V
    .locals 1

    .prologue
    .line 1806985
    invoke-direct {p0}, Landroid/view/animation/Animation;-><init>()V

    .line 1806986
    iput-object p1, p0, LX/Bg2;->c:Landroid/view/View;

    .line 1806987
    iput p2, p0, LX/Bg2;->a:F

    .line 1806988
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    iput v0, p0, LX/Bg2;->d:I

    .line 1806989
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v0

    iput v0, p0, LX/Bg2;->b:I

    .line 1806990
    return-void
.end method


# virtual methods
.method public final applyTransformation(FLandroid/view/animation/Transformation;)V
    .locals 6

    .prologue
    const/high16 v5, 0x40000000    # 2.0f

    const/high16 v4, 0x3f800000    # 1.0f

    .line 1806977
    iget v0, p0, LX/Bg2;->a:F

    sub-float/2addr v0, v4

    mul-float/2addr v0, p1

    .line 1806978
    iget v1, p0, LX/Bg2;->d:I

    int-to-float v1, v1

    mul-float/2addr v1, v0

    .line 1806979
    iget v2, p0, LX/Bg2;->b:I

    int-to-float v2, v2

    mul-float/2addr v2, v0

    .line 1806980
    invoke-virtual {p2}, Landroid/view/animation/Transformation;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v3

    neg-float v1, v1

    div-float/2addr v1, v5

    neg-float v2, v2

    div-float/2addr v2, v5

    invoke-virtual {v3, v1, v2}, Landroid/graphics/Matrix;->setTranslate(FF)V

    .line 1806981
    invoke-virtual {p2}, Landroid/view/animation/Transformation;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v1

    add-float v2, v4, v0

    add-float/2addr v0, v4

    invoke-virtual {v1, v2, v0}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 1806982
    iget-object v0, p0, LX/Bg2;->c:Landroid/view/View;

    sub-float v1, v4, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 1806983
    iget-object v0, p0, LX/Bg2;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    .line 1806984
    return-void
.end method
