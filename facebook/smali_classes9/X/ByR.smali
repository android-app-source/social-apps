.class public final LX/ByR;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/ByS;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/1aZ;

.field public b:F

.field public final synthetic c:LX/ByS;


# direct methods
.method public constructor <init>(LX/ByS;)V
    .locals 1

    .prologue
    .line 1837341
    iput-object p1, p0, LX/ByR;->c:LX/ByS;

    .line 1837342
    move-object v0, p1

    .line 1837343
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1837344
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1837345
    const-string v0, "LargeImageComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1837346
    if-ne p0, p1, :cond_1

    .line 1837347
    :cond_0
    :goto_0
    return v0

    .line 1837348
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1837349
    goto :goto_0

    .line 1837350
    :cond_3
    check-cast p1, LX/ByR;

    .line 1837351
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1837352
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1837353
    if-eq v2, v3, :cond_0

    .line 1837354
    iget-object v2, p0, LX/ByR;->a:LX/1aZ;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/ByR;->a:LX/1aZ;

    iget-object v3, p1, LX/ByR;->a:LX/1aZ;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1837355
    goto :goto_0

    .line 1837356
    :cond_5
    iget-object v2, p1, LX/ByR;->a:LX/1aZ;

    if-nez v2, :cond_4

    .line 1837357
    :cond_6
    iget v2, p0, LX/ByR;->b:F

    iget v3, p1, LX/ByR;->b:F

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    .line 1837358
    goto :goto_0
.end method
