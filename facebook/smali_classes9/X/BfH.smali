.class public final LX/BfH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/BfC;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/BfC;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 1805858
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1805859
    iput-object p1, p0, LX/BfH;->a:LX/0QB;

    .line 1805860
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1805844
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/BfH;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1805846
    packed-switch p2, :pswitch_data_0

    .line 1805847
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1805848
    :pswitch_0
    new-instance v0, LX/BfD;

    const/16 v1, 0xc

    invoke-static {p1, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    invoke-direct {v0, v1}, LX/BfD;-><init>(LX/0Or;)V

    .line 1805849
    move-object v0, v0

    .line 1805850
    :goto_0
    return-object v0

    .line 1805851
    :pswitch_1
    new-instance v0, LX/BfE;

    const/16 v1, 0xc

    invoke-static {p1, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    invoke-direct {v0, v1}, LX/BfE;-><init>(LX/0Or;)V

    .line 1805852
    move-object v0, v0

    .line 1805853
    goto :goto_0

    .line 1805854
    :pswitch_2
    new-instance v1, LX/BfF;

    const/16 v0, 0xc

    invoke-static {p1, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {p1}, LX/Bez;->b(LX/0QB;)LX/Bez;

    move-result-object v0

    check-cast v0, LX/Bez;

    invoke-direct {v1, p0, v0}, LX/BfF;-><init>(LX/0Or;LX/Bez;)V

    .line 1805855
    move-object v0, v1

    .line 1805856
    goto :goto_0

    .line 1805857
    :pswitch_3
    invoke-static {p1}, LX/BfG;->a(LX/0QB;)LX/BfG;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 1805845
    const/4 v0, 0x4

    return v0
.end method
