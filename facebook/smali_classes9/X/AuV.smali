.class public LX/AuV;
.super LX/9dK;
.source ""


# instance fields
.field private c:Landroid/widget/ImageView;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/8Gc;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1722663
    invoke-direct {p0, p1, p2}, LX/9dK;-><init>(Landroid/content/Context;LX/8Gc;)V

    .line 1722664
    return-void
.end method


# virtual methods
.method public final a(F)F
    .locals 1

    .prologue
    .line 1722661
    iget-object v0, p0, LX/AuV;->c:Landroid/widget/ImageView;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1722662
    iget-object v0, p0, LX/AuV;->c:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v0

    int-to-float v0, v0

    sub-float v0, p1, v0

    return v0
.end method

.method public final a()Landroid/widget/ImageView;
    .locals 3

    .prologue
    .line 1722649
    iget-object v0, p0, LX/AuV;->c:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    .line 1722650
    new-instance v0, Landroid/widget/ImageView;

    iget-object v1, p0, LX/9dK;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/AuV;->c:Landroid/widget/ImageView;

    .line 1722651
    iget-object v0, p0, LX/AuV;->c:Landroid/widget/ImageView;

    iget-object v1, p0, LX/9dK;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020da0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1722652
    iget-object v0, p0, LX/AuV;->c:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 1722653
    :cond_0
    iget-object v0, p0, LX/AuV;->c:Landroid/widget/ImageView;

    return-object v0
.end method

.method public final b()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 1722660
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c()F
    .locals 1

    .prologue
    .line 1722659
    iget-object v0, p0, LX/9dK;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getMeasuredWidth()I

    move-result v0

    int-to-float v0, v0

    return v0
.end method

.method public final d()F
    .locals 1

    .prologue
    .line 1722665
    const/4 v0, 0x0

    return v0
.end method

.method public final e()LX/9dT;
    .locals 1

    .prologue
    .line 1722658
    sget-object v0, LX/9dT;->NO_ROTATION:LX/9dT;

    return-object v0
.end method

.method public final f()F
    .locals 1

    .prologue
    .line 1722657
    iget-object v0, p0, LX/9dK;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getMeasuredWidth()I

    move-result v0

    int-to-float v0, v0

    return v0
.end method

.method public final g()F
    .locals 2

    .prologue
    .line 1722655
    iget-object v0, p0, LX/AuV;->c:Landroid/widget/ImageView;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1722656
    iget-object v0, p0, LX/AuV;->c:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    const/high16 v1, 0x42c80000    # 100.0f

    mul-float/2addr v0, v1

    return v0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 1722654
    const/4 v0, 0x0

    return v0
.end method
