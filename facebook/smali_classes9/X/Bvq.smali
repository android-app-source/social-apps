.class public final LX/Bvq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3IA;
.implements LX/3IB;


# instance fields
.field public final synthetic a:Lcom/facebook/feed/video/fullscreen/Video360ControlsPlugin;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/video/fullscreen/Video360ControlsPlugin;)V
    .locals 0

    .prologue
    .line 1832948
    iput-object p1, p0, LX/Bvq;->a:Lcom/facebook/feed/video/fullscreen/Video360ControlsPlugin;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1832918
    iget-object v0, p0, LX/Bvq;->a:Lcom/facebook/feed/video/fullscreen/Video360ControlsPlugin;

    .line 1832919
    invoke-virtual {v0}, LX/7N3;->g()V

    .line 1832920
    iget-object v0, p0, LX/Bvq;->a:Lcom/facebook/feed/video/fullscreen/Video360ControlsPlugin;

    iget-object v0, v0, LX/2oy;->i:LX/2oj;

    if-nez v0, :cond_0

    .line 1832921
    :goto_0
    return-void

    .line 1832922
    :cond_0
    new-instance v0, LX/7Lr;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, LX/7Lr;-><init>(I)V

    .line 1832923
    iget-object v1, p0, LX/Bvq;->a:Lcom/facebook/feed/video/fullscreen/Video360ControlsPlugin;

    iget-object v1, v1, LX/2oy;->i:LX/2oj;

    invoke-virtual {v1, v0}, LX/2oj;->a(LX/2ol;)V

    goto :goto_0
.end method

.method public final a(FF)V
    .locals 2

    .prologue
    .line 1832944
    iget-object v0, p0, LX/Bvq;->a:Lcom/facebook/feed/video/fullscreen/Video360ControlsPlugin;

    iget-object v0, v0, LX/2oy;->i:LX/2oj;

    if-nez v0, :cond_0

    .line 1832945
    :goto_0
    return-void

    .line 1832946
    :cond_0
    new-instance v0, LX/7Lr;

    const/4 v1, 0x1

    invoke-direct {v0, v1, p1, p2}, LX/7Lr;-><init>(IFF)V

    .line 1832947
    iget-object v1, p0, LX/Bvq;->a:Lcom/facebook/feed/video/fullscreen/Video360ControlsPlugin;

    iget-object v1, v1, LX/2oy;->i:LX/2oj;

    invoke-virtual {v1, v0}, LX/2oj;->a(LX/2ol;)V

    goto :goto_0
.end method

.method public final a(F)Z
    .locals 3

    .prologue
    .line 1832939
    iget-object v0, p0, LX/Bvq;->a:Lcom/facebook/feed/video/fullscreen/Video360ControlsPlugin;

    iget-object v0, v0, LX/2oy;->i:LX/2oj;

    if-nez v0, :cond_0

    .line 1832940
    const/4 v0, 0x0

    .line 1832941
    :goto_0
    return v0

    .line 1832942
    :cond_0
    iget-object v0, p0, LX/Bvq;->a:Lcom/facebook/feed/video/fullscreen/Video360ControlsPlugin;

    iget-object v0, v0, LX/2oy;->i:LX/2oj;

    new-instance v1, LX/7Lq;

    const/4 v2, 0x2

    invoke-direct {v1, v2, p1}, LX/7Lq;-><init>(IF)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    .line 1832943
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1832935
    iget-object v0, p0, LX/Bvq;->a:Lcom/facebook/feed/video/fullscreen/Video360ControlsPlugin;

    iget-object v0, v0, LX/2oy;->i:LX/2oj;

    if-nez v0, :cond_0

    .line 1832936
    :goto_0
    return-void

    .line 1832937
    :cond_0
    new-instance v0, LX/7Lr;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, LX/7Lr;-><init>(I)V

    .line 1832938
    iget-object v1, p0, LX/Bvq;->a:Lcom/facebook/feed/video/fullscreen/Video360ControlsPlugin;

    iget-object v1, v1, LX/2oy;->i:LX/2oj;

    invoke-virtual {v1, v0}, LX/2oj;->a(LX/2ol;)V

    goto :goto_0
.end method

.method public final b(FF)V
    .locals 2

    .prologue
    .line 1832931
    iget-object v0, p0, LX/Bvq;->a:Lcom/facebook/feed/video/fullscreen/Video360ControlsPlugin;

    iget-object v0, v0, LX/2oy;->i:LX/2oj;

    if-nez v0, :cond_0

    .line 1832932
    :goto_0
    return-void

    .line 1832933
    :cond_0
    new-instance v0, LX/7Lr;

    const/4 v1, 0x2

    invoke-direct {v0, v1, p1, p2}, LX/7Lr;-><init>(IFF)V

    .line 1832934
    iget-object v1, p0, LX/Bvq;->a:Lcom/facebook/feed/video/fullscreen/Video360ControlsPlugin;

    iget-object v1, v1, LX/2oy;->i:LX/2oj;

    invoke-virtual {v1, v0}, LX/2oj;->a(LX/2ol;)V

    goto :goto_0
.end method

.method public final c()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 1832927
    iget-object v1, p0, LX/Bvq;->a:Lcom/facebook/feed/video/fullscreen/Video360ControlsPlugin;

    iget-object v1, v1, LX/2oy;->i:LX/2oj;

    if-nez v1, :cond_0

    .line 1832928
    const/4 v0, 0x0

    .line 1832929
    :goto_0
    return v0

    .line 1832930
    :cond_0
    iget-object v1, p0, LX/Bvq;->a:Lcom/facebook/feed/video/fullscreen/Video360ControlsPlugin;

    iget-object v1, v1, LX/2oy;->i:LX/2oj;

    new-instance v2, LX/7Lq;

    invoke-direct {v2, v0}, LX/7Lq;-><init>(I)V

    invoke-virtual {v1, v2}, LX/2oj;->a(LX/2ol;)V

    goto :goto_0
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 1832924
    iget-object v0, p0, LX/Bvq;->a:Lcom/facebook/feed/video/fullscreen/Video360ControlsPlugin;

    iget-object v0, v0, LX/2oy;->i:LX/2oj;

    if-nez v0, :cond_0

    .line 1832925
    :goto_0
    return-void

    .line 1832926
    :cond_0
    iget-object v0, p0, LX/Bvq;->a:Lcom/facebook/feed/video/fullscreen/Video360ControlsPlugin;

    iget-object v0, v0, LX/2oy;->i:LX/2oj;

    new-instance v1, LX/7Lq;

    const/4 v2, 0x3

    invoke-direct {v1, v2}, LX/7Lq;-><init>(I)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    goto :goto_0
.end method
