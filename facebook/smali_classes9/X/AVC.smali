.class public LX/AVC;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:[J


# instance fields
.field public final b:Landroid/app/NotificationManager;

.field public final c:Landroid/content/Context;

.field public final d:Landroid/content/res/Resources;

.field public final e:Landroid/os/Handler;

.field public final f:LX/0SG;

.field public final g:Z

.field public final h:LX/AVU;

.field public final i:LX/1b2;

.field public j:Ljava/lang/Runnable;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1679000
    const/4 v0, 0x2

    new-array v0, v0, [J

    fill-array-data v0, :array_0

    sput-object v0, LX/AVC;->a:[J

    return-void

    nop

    :array_0
    .array-data 8
        0x0
        0x64
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/app/NotificationManager;Landroid/content/res/Resources;Landroid/os/Handler;LX/0SG;LX/AVU;LX/0Uh;LX/1b2;)V
    .locals 2
    .param p1    # Landroid/content/Context;
        .annotation build Lcom/facebook/inject/ForAppContext;
        .end annotation
    .end param
    .param p4    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1679001
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1679002
    iput-object p1, p0, LX/AVC;->c:Landroid/content/Context;

    .line 1679003
    iput-object p2, p0, LX/AVC;->b:Landroid/app/NotificationManager;

    .line 1679004
    iput-object p3, p0, LX/AVC;->d:Landroid/content/res/Resources;

    .line 1679005
    iput-object p4, p0, LX/AVC;->e:Landroid/os/Handler;

    .line 1679006
    iput-object p5, p0, LX/AVC;->f:LX/0SG;

    .line 1679007
    iput-object p6, p0, LX/AVC;->h:LX/AVU;

    .line 1679008
    const/16 v0, 0x345

    const/4 v1, 0x0

    invoke-virtual {p7, v0, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    iput-boolean v0, p0, LX/AVC;->g:Z

    .line 1679009
    iput-object p8, p0, LX/AVC;->i:LX/1b2;

    .line 1679010
    new-instance v0, Lcom/facebook/facecast/FacecastPausedNotificationManager$1;

    invoke-direct {v0, p0}, Lcom/facebook/facecast/FacecastPausedNotificationManager$1;-><init>(LX/AVC;)V

    iput-object v0, p0, LX/AVC;->j:Ljava/lang/Runnable;

    .line 1679011
    return-void
.end method

.method public static a(LX/0QB;)LX/AVC;
    .locals 10

    .prologue
    .line 1679012
    new-instance v1, LX/AVC;

    const-class v2, Landroid/content/Context;

    const-class v3, Lcom/facebook/inject/ForAppContext;

    invoke-interface {p0, v2, v3}, LX/0QC;->getInstance(Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-static {p0}, LX/1s4;->b(LX/0QB;)Landroid/app/NotificationManager;

    move-result-object v3

    check-cast v3, Landroid/app/NotificationManager;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v4

    check-cast v4, Landroid/content/res/Resources;

    invoke-static {p0}, LX/0kY;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v5

    check-cast v5, Landroid/os/Handler;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v6

    check-cast v6, LX/0SG;

    invoke-static {p0}, LX/AVU;->a(LX/0QB;)LX/AVU;

    move-result-object v7

    check-cast v7, LX/AVU;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v8

    check-cast v8, LX/0Uh;

    invoke-static {p0}, LX/1b2;->a(LX/0QB;)LX/1b2;

    move-result-object v9

    check-cast v9, LX/1b2;

    invoke-direct/range {v1 .. v9}, LX/AVC;-><init>(Landroid/content/Context;Landroid/app/NotificationManager;Landroid/content/res/Resources;Landroid/os/Handler;LX/0SG;LX/AVU;LX/0Uh;LX/1b2;)V

    .line 1679013
    move-object v0, v1

    .line 1679014
    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1679015
    iget-boolean v0, p0, LX/AVC;->g:Z

    if-nez v0, :cond_0

    .line 1679016
    :goto_0
    return-void

    .line 1679017
    :cond_0
    iget-object v0, p0, LX/AVC;->e:Landroid/os/Handler;

    iget-object v1, p0, LX/AVC;->j:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1679018
    iget-object v0, p0, LX/AVC;->b:Landroid/app/NotificationManager;

    const/16 v1, 0x2766

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    goto :goto_0
.end method
