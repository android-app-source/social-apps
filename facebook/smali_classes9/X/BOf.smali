.class public LX/BOf;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0SG;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/1kG;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/1kH;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/1kZ;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1779958
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1779959
    return-void
.end method

.method public static b(LX/BOf;)I
    .locals 14

    .prologue
    .line 1779960
    iget-object v0, p0, LX/BOf;->b:LX/1kG;

    .line 1779961
    iget-object v4, p0, LX/BOf;->a:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v4

    iget-object v6, p0, LX/BOf;->d:LX/1kZ;

    .line 1779962
    iget-object v8, v6, LX/1kZ;->q:Ljava/lang/Long;

    if-nez v8, :cond_0

    .line 1779963
    iget-object v8, v6, LX/1kZ;->a:LX/0ad;

    sget-wide v10, LX/1vU;->j:J

    const-wide/32 v12, 0x5265c00

    invoke-interface {v8, v10, v11, v12, v13}, LX/0ad;->a(JJ)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    iput-object v8, v6, LX/1kZ;->q:Ljava/lang/Long;

    .line 1779964
    :cond_0
    iget-object v8, v6, LX/1kZ;->q:Ljava/lang/Long;

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    move-wide v6, v8

    .line 1779965
    sub-long/2addr v4, v6

    move-wide v2, v4

    .line 1779966
    sget-object v1, LX/1po;->PHOTO:LX/1po;

    invoke-virtual {v0, v2, v3, v1}, LX/1kG;->a(JLX/1po;)I

    move-result v0

    return v0
.end method

.method public static b(LX/0QB;)LX/BOf;
    .locals 5

    .prologue
    .line 1779967
    new-instance v4, LX/BOf;

    invoke-direct {v4}, LX/BOf;-><init>()V

    .line 1779968
    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v0

    check-cast v0, LX/0SG;

    invoke-static {p0}, LX/1kG;->a(LX/0QB;)LX/1kG;

    move-result-object v1

    check-cast v1, LX/1kG;

    invoke-static {p0}, LX/1kH;->a(LX/0QB;)LX/1kH;

    move-result-object v2

    check-cast v2, LX/1kH;

    invoke-static {p0}, LX/1kZ;->a(LX/0QB;)LX/1kZ;

    move-result-object v3

    check-cast v3, LX/1kZ;

    .line 1779969
    iput-object v0, v4, LX/BOf;->a:LX/0SG;

    iput-object v1, v4, LX/BOf;->b:LX/1kG;

    iput-object v2, v4, LX/BOf;->c:LX/1kH;

    iput-object v3, v4, LX/BOf;->d:LX/1kZ;

    .line 1779970
    return-object v4
.end method

.method public static b(LX/BOf;I)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/media/util/model/MediaModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1779971
    iget-object v0, p0, LX/BOf;->c:LX/1kH;

    const/4 v1, 0x1

    .line 1779972
    iget-object v2, p0, LX/BOf;->d:LX/1kZ;

    .line 1779973
    iget-object v3, v2, LX/1kZ;->p:Ljava/lang/Integer;

    if-nez v3, :cond_0

    .line 1779974
    iget-object v3, v2, LX/1kZ;->a:LX/0ad;

    sget v4, LX/1vU;->i:I

    const/16 p0, 0x1e

    invoke-interface {v3, v4, p0}, LX/0ad;->a(II)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v2, LX/1kZ;->p:Ljava/lang/Integer;

    .line 1779975
    :cond_0
    iget-object v3, v2, LX/1kZ;->p:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    move v2, v3

    .line 1779976
    invoke-static {p1, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    move v2, v2

    .line 1779977
    sget-object v3, LX/1po;->PHOTO:LX/1po;

    invoke-virtual {v0, v1, v2, v3}, LX/1kH;->a(ZILX/1po;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
