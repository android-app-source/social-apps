.class public LX/BsK;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:LX/1nq;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0hL;LX/1W9;)V
    .locals 5
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 1827365
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1827366
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 1827367
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    const v2, 0x1010036

    invoke-virtual {v1, v2, v0, v4}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 1827368
    new-instance v1, LX/1nq;

    invoke-direct {v1}, LX/1nq;-><init>()V

    iput-object v1, p0, LX/BsK;->a:LX/1nq;

    .line 1827369
    iget-object v1, p0, LX/BsK;->a:LX/1nq;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b1064

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v1, v2}, LX/1nq;->b(I)LX/1nq;

    .line 1827370
    iget-object v1, p0, LX/BsK;->a:LX/1nq;

    iget v0, v0, Landroid/util/TypedValue;->data:I

    invoke-virtual {v1, v0}, LX/1nq;->c(I)LX/1nq;

    .line 1827371
    iget-object v0, p0, LX/BsK;->a:LX/1nq;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b1063

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    invoke-virtual {v0, v1}, LX/1nq;->a(F)LX/1nq;

    .line 1827372
    iget-object v0, p0, LX/BsK;->a:LX/1nq;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, LX/1nq;->b(F)LX/1nq;

    .line 1827373
    iget-object v0, p0, LX/BsK;->a:LX/1nq;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/1nq;->a(Z)LX/1nq;

    .line 1827374
    iget-object v1, p0, LX/BsK;->a:LX/1nq;

    invoke-virtual {p2}, LX/0hL;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_OPPOSITE:Landroid/text/Layout$Alignment;

    :goto_0
    invoke-virtual {v1, v0}, LX/1nq;->a(Landroid/text/Layout$Alignment;)LX/1nq;

    .line 1827375
    iget-object v0, p0, LX/BsK;->a:LX/1nq;

    .line 1827376
    iput-boolean v4, v0, LX/1nq;->f:Z

    .line 1827377
    iget-object v0, p0, LX/BsK;->a:LX/1nq;

    .line 1827378
    iput-object p3, v0, LX/1nq;->d:LX/1WA;

    .line 1827379
    return-void

    .line 1827380
    :cond_0
    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/BsK;
    .locals 6

    .prologue
    .line 1827381
    const-class v1, LX/BsK;

    monitor-enter v1

    .line 1827382
    :try_start_0
    sget-object v0, LX/BsK;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1827383
    sput-object v2, LX/BsK;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1827384
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1827385
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1827386
    new-instance p0, LX/BsK;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0hL;->a(LX/0QB;)LX/0hL;

    move-result-object v4

    check-cast v4, LX/0hL;

    invoke-static {v0}, LX/1W9;->a(LX/0QB;)LX/1W9;

    move-result-object v5

    check-cast v5, LX/1W9;

    invoke-direct {p0, v3, v4, v5}, LX/BsK;-><init>(Landroid/content/Context;LX/0hL;LX/1W9;)V

    .line 1827387
    move-object v0, p0

    .line 1827388
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1827389
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/BsK;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1827390
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1827391
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1nq;
    .locals 1

    .prologue
    .line 1827392
    iget-object v0, p0, LX/BsK;->a:LX/1nq;

    return-object v0
.end method
