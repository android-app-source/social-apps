.class public abstract LX/CA5;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements LX/9A2;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1854891
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 1854892
    return-void
.end method


# virtual methods
.method public a(Landroid/view/View$OnClickListener;Landroid/view/View$OnTouchListener;)V
    .locals 0
    .param p1    # Landroid/view/View$OnClickListener;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Landroid/view/View$OnTouchListener;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1854893
    return-void
.end method

.method public abstract setCallbackOnProgressComplete(LX/0TF;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0TF",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract setCallbackOnProgressStarted(LX/0TF;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0TF",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract setProgress(I)V
.end method
