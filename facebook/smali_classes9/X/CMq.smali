.class public LX/CMq;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/CMq;


# instance fields
.field private final a:Lcom/facebook/content/SecureContextHelper;

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/CJb;

.field private final e:LX/17d;


# direct methods
.method public constructor <init>(Lcom/facebook/content/SecureContextHelper;LX/0Ot;LX/0Ot;LX/CJb;LX/17d;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/CJb;",
            "LX/17d;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1880977
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1880978
    iput-object p1, p0, LX/CMq;->a:Lcom/facebook/content/SecureContextHelper;

    .line 1880979
    iput-object p2, p0, LX/CMq;->b:LX/0Ot;

    .line 1880980
    iput-object p3, p0, LX/CMq;->c:LX/0Ot;

    .line 1880981
    iput-object p4, p0, LX/CMq;->d:LX/CJb;

    .line 1880982
    iput-object p5, p0, LX/CMq;->e:LX/17d;

    .line 1880983
    return-void
.end method

.method public static a(LX/0QB;)LX/CMq;
    .locals 9

    .prologue
    .line 1880984
    sget-object v0, LX/CMq;->f:LX/CMq;

    if-nez v0, :cond_1

    .line 1880985
    const-class v1, LX/CMq;

    monitor-enter v1

    .line 1880986
    :try_start_0
    sget-object v0, LX/CMq;->f:LX/CMq;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1880987
    if-eqz v2, :cond_0

    .line 1880988
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1880989
    new-instance v3, LX/CMq;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v4

    check-cast v4, Lcom/facebook/content/SecureContextHelper;

    const/16 v5, 0x12c4

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x259

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {v0}, LX/CJb;->b(LX/0QB;)LX/CJb;

    move-result-object v7

    check-cast v7, LX/CJb;

    invoke-static {v0}, LX/17d;->a(LX/0QB;)LX/17d;

    move-result-object v8

    check-cast v8, LX/17d;

    invoke-direct/range {v3 .. v8}, LX/CMq;-><init>(Lcom/facebook/content/SecureContextHelper;LX/0Ot;LX/0Ot;LX/CJb;LX/17d;)V

    .line 1880990
    move-object v0, v3

    .line 1880991
    sput-object v0, LX/CMq;->f:LX/CMq;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1880992
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1880993
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1880994
    :cond_1
    sget-object v0, LX/CMq;->f:LX/CMq;

    return-object v0

    .line 1880995
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1880996
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 9

    .prologue
    .line 1880997
    invoke-static {p2}, LX/CMp;->from(Landroid/net/Uri;)LX/CMp;

    move-result-object v0

    sget-object v1, LX/CMp;->WEB:LX/CMp;

    if-ne v0, v1, :cond_6

    iget-object v0, p0, LX/CMq;->d:LX/CJb;

    .line 1880998
    invoke-static {p2}, LX/17d;->a(Landroid/net/Uri;)Z

    move-result v1

    if-nez v1, :cond_a

    invoke-virtual {v0}, LX/CJb;->a()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 1880999
    iget-object v1, v0, LX/CJb;->g:Landroid/content/Intent;

    if-nez v1, :cond_0

    .line 1881000
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iput-object v1, v0, LX/CJb;->g:Landroid/content/Intent;

    .line 1881001
    :cond_0
    iget-object v1, v0, LX/CJb;->g:Landroid/content/Intent;

    invoke-virtual {v1, p2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1881002
    iget-object v1, v0, LX/CJb;->g:Landroid/content/Intent;

    invoke-static {p1, v1}, LX/2A3;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v1

    move v1, v1

    .line 1881003
    if-nez v1, :cond_a

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 1881004
    if-eqz v0, :cond_6

    .line 1881005
    iget-object v0, p0, LX/CMq;->d:LX/CJb;

    .line 1881006
    sget-object v1, Lcom/facebook/messaging/browser/model/MessengerInAppBrowserLaunchParam;->a:Lcom/facebook/messaging/browser/model/MessengerInAppBrowserLaunchParam;

    .line 1881007
    iget-object v2, v0, LX/CJb;->b:Lcom/facebook/content/SecureContextHelper;

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 1881008
    iget-object v3, v0, LX/CJb;->d:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    sget-object v6, LX/03R;->YES:LX/03R;

    if-ne v3, v6, :cond_b

    move v3, v4

    .line 1881009
    :goto_1
    new-instance v6, LX/0DW;

    invoke-direct {v6}, LX/0DW;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v7

    iget-object v7, v7, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v6, v7}, LX/0DW;->a(Ljava/util/Locale;)LX/0DW;

    move-result-object v6

    iget-object v8, v0, LX/CJb;->e:LX/01T;

    iget-object p0, v0, LX/CJb;->f:LX/0WV;

    invoke-static {v8, p0}, LX/1Bg;->a(LX/01T;LX/0WV;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/0DW;->e(Ljava/lang/String;)LX/0DW;

    move-result-object v6

    invoke-virtual {v6, v4}, LX/0DW;->b(Z)LX/0DW;

    move-result-object v6

    invoke-virtual {v6, v4}, LX/0DW;->i(Z)LX/0DW;

    move-result-object v6

    invoke-virtual {v6, v3}, LX/0DW;->c(Z)LX/0DW;

    move-result-object v6

    if-nez v3, :cond_1

    .line 1881010
    sget-boolean v3, LX/007;->i:Z

    move v3, v3

    .line 1881011
    if-eqz v3, :cond_2

    :cond_1
    move v5, v4

    :cond_2
    invoke-virtual {v6, v5}, LX/0DW;->a(Z)LX/0DW;

    move-result-object v3

    const-string v4, "THEME_MESSENGER_IAB"

    invoke-virtual {v3, v4}, LX/0DW;->d(Ljava/lang/String;)LX/0DW;

    move-result-object v3

    .line 1881012
    iget-object v4, v0, LX/CJb;->c:LX/1Bg;

    invoke-virtual {v4, v3}, LX/1Bg;->a(LX/0DW;)V

    .line 1881013
    const-class v4, Landroid/app/Service;

    invoke-static {p1, v4}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_c

    const/4 v4, 0x1

    :goto_2
    move v4, v4

    .line 1881014
    if-nez v4, :cond_3

    .line 1881015
    const v4, 0x7f04000e

    const v5, 0x7f04000b

    const v6, 0x7f04000f

    const v7, 0x7f04000d

    invoke-virtual {v3, v4, v5, v6, v7}, LX/0DW;->a(IIII)LX/0DW;

    .line 1881016
    :cond_3
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f081ced

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const v5, 0x7f02016e

    const-string v6, "COPY_LINK"

    invoke-virtual {v3, v4, v5, v6}, LX/0DW;->b(Ljava/lang/String;ILjava/lang/String;)LX/0DW;

    .line 1881017
    iget-object v4, v0, LX/CJb;->a:LX/0Uh;

    const/16 v5, 0x1a7

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, LX/0Uh;->a(IZ)Z

    move-result v4

    if-eqz v4, :cond_4

    iget-boolean v4, v1, Lcom/facebook/messaging/browser/model/MessengerInAppBrowserLaunchParam;->c:Z

    if-nez v4, :cond_4

    .line 1881018
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f082e75

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const v5, 0x7f0214ae

    const-string v6, "SHARE_LINK_IN_MESSENGER"

    invoke-virtual {v3, v4, v5, v6}, LX/0DW;->b(Ljava/lang/String;ILjava/lang/String;)LX/0DW;

    .line 1881019
    :cond_4
    iget-boolean v4, v1, Lcom/facebook/messaging/browser/model/MessengerInAppBrowserLaunchParam;->d:Z

    if-nez v4, :cond_5

    .line 1881020
    const-string v4, "MENU_OPEN_WITH"

    invoke-virtual {v3, v4}, LX/0DW;->a(Ljava/lang/String;)LX/0DW;

    .line 1881021
    :cond_5
    invoke-static {v0, v3, v1}, LX/CJb;->a(LX/CJb;LX/0DW;Lcom/facebook/messaging/browser/model/MessengerInAppBrowserLaunchParam;)V

    .line 1881022
    new-instance v4, Landroid/content/Intent;

    const-class v5, Lcom/facebook/browser/lite/BrowserLiteActivity;

    invoke-direct {v4, p1, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v4, p2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v4

    .line 1881023
    invoke-virtual {v3}, LX/0DW;->a()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v4, v3}, Landroid/content/Intent;->putExtras(Landroid/content/Intent;)Landroid/content/Intent;

    .line 1881024
    const-string v3, "iab_click_source"

    const-string v5, "fblink_messenger"

    invoke-virtual {v4, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1881025
    move-object v3, v4

    .line 1881026
    invoke-interface {v2, v3, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1881027
    :goto_3
    return-void

    .line 1881028
    :cond_6
    iget-object v0, p0, LX/CMq;->e:LX/17d;

    .line 1881029
    const/4 v1, 0x0

    .line 1881030
    invoke-static {p2}, LX/17d;->a(Landroid/net/Uri;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 1881031
    invoke-virtual {v0, p1, p2}, LX/17d;->d(Landroid/content/Context;Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    .line 1881032
    if-nez v1, :cond_7

    .line 1881033
    invoke-static {p1, p2}, LX/17d;->e(Landroid/content/Context;Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    .line 1881034
    :cond_7
    if-nez v1, :cond_8

    .line 1881035
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1881036
    invoke-virtual {v1, p2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1881037
    :cond_8
    move-object v1, v1

    .line 1881038
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 1881039
    if-eqz v0, :cond_9

    invoke-virtual {v1, v0}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 1881040
    iget-object v0, p0, LX/CMq;->a:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v0, v1, p1}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_3

    .line 1881041
    :cond_9
    iget-object v0, p0, LX/CMq;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kL;

    new-instance v2, LX/27k;

    const v3, 0x7f082e76

    invoke-direct {v2, v3}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v2}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 1881042
    iget-object v0, p0, LX/CMq;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v2, "ActivityNotFoundForLink"

    invoke-virtual {v1}, Landroid/content/Intent;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    :cond_a
    const/4 v1, 0x0

    goto/16 :goto_0

    :cond_b
    move v3, v5

    .line 1881043
    goto/16 :goto_1

    :cond_c
    const/4 v4, 0x0

    goto/16 :goto_2
.end method
