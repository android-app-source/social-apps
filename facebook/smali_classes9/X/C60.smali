.class public LX/C60;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:I

.field public final b:LX/C62;

.field private final c:LX/0tJ;

.field public d:LX/C5z;

.field public e:LX/C5z;

.field public f:Z

.field public g:Landroid/animation/ObjectAnimator;

.field public h:Landroid/animation/ObjectAnimator;

.field public i:Landroid/animation/ObjectAnimator;

.field public j:Landroid/animation/ObjectAnimator;

.field public k:Landroid/animation/AnimatorSet;

.field public l:Landroid/animation/AnimatorSet;

.field public m:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/3Vs;",
            ">;"
        }
    .end annotation
.end field

.field private final n:LX/C5d;

.field private o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;


# direct methods
.method public constructor <init>(LX/C62;LX/0tJ;LX/C5z;LX/C5d;Ljava/util/List;Lcom/facebook/graphql/model/GraphQLTextWithEntities;)V
    .locals 7
    .param p3    # LX/C5z;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/C5d;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Ljava/util/List;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # Lcom/facebook/graphql/model/GraphQLTextWithEntities;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/C62;",
            "LX/0tJ;",
            "LX/C5z;",
            "Lcom/facebook/feedplugins/graphqlstory/footer/FeedDiscoveryBlingBarPartDefinition$FeedDiscoveryViewStateChangeListener;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLTextWithEntities;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1849550
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1849551
    iput-object p1, p0, LX/C60;->b:LX/C62;

    .line 1849552
    iput-object p3, p0, LX/C60;->d:LX/C5z;

    .line 1849553
    iput-object p2, p0, LX/C60;->c:LX/0tJ;

    .line 1849554
    iget-object v0, p0, LX/C60;->c:LX/0tJ;

    .line 1849555
    iget-object p1, v0, LX/0tJ;->a:LX/0ad;

    sget p2, LX/0wn;->N:I

    const/16 p3, 0x12c

    invoke-interface {p1, p2, p3}, LX/0ad;->a(II)I

    move-result p1

    move v0, p1

    .line 1849556
    iput v0, p0, LX/C60;->a:I

    .line 1849557
    iput-object p4, p0, LX/C60;->n:LX/C5d;

    .line 1849558
    iput-object p5, p0, LX/C60;->o:Ljava/util/List;

    .line 1849559
    iput-object p6, p0, LX/C60;->p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1849560
    iget v1, p0, LX/C60;->a:I

    .line 1849561
    iget-object v2, p0, LX/C60;->b:LX/C62;

    .line 1849562
    new-instance v3, Landroid/animation/ObjectAnimator;

    invoke-direct {v3}, Landroid/animation/ObjectAnimator;-><init>()V

    .line 1849563
    const/4 v4, 0x2

    new-array v4, v4, [Landroid/animation/PropertyValuesHolder;

    const/4 v5, 0x0

    iget-object v6, v2, LX/C62;->e:Landroid/animation/PropertyValuesHolder;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    sget-object v6, LX/C62;->a:Landroid/animation/PropertyValuesHolder;

    aput-object v6, v4, v5

    invoke-virtual {v3, v4}, Landroid/animation/ObjectAnimator;->setValues([Landroid/animation/PropertyValuesHolder;)V

    .line 1849564
    int-to-long v5, v1

    invoke-virtual {v3, v5, v6}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 1849565
    move-object v2, v3

    .line 1849566
    iput-object v2, p0, LX/C60;->g:Landroid/animation/ObjectAnimator;

    .line 1849567
    iget-object v2, p0, LX/C60;->b:LX/C62;

    .line 1849568
    new-instance v3, Landroid/animation/ObjectAnimator;

    invoke-direct {v3}, Landroid/animation/ObjectAnimator;-><init>()V

    .line 1849569
    const/4 v4, 0x2

    new-array v4, v4, [Landroid/animation/PropertyValuesHolder;

    const/4 v5, 0x0

    iget-object v6, v2, LX/C62;->c:Landroid/animation/PropertyValuesHolder;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    sget-object v6, LX/C62;->a:Landroid/animation/PropertyValuesHolder;

    aput-object v6, v4, v5

    invoke-virtual {v3, v4}, Landroid/animation/ObjectAnimator;->setValues([Landroid/animation/PropertyValuesHolder;)V

    .line 1849570
    int-to-long v5, v1

    invoke-virtual {v3, v5, v6}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 1849571
    move-object v2, v3

    .line 1849572
    iput-object v2, p0, LX/C60;->h:Landroid/animation/ObjectAnimator;

    .line 1849573
    iget-object v2, p0, LX/C60;->b:LX/C62;

    .line 1849574
    new-instance v3, Landroid/animation/ObjectAnimator;

    invoke-direct {v3}, Landroid/animation/ObjectAnimator;-><init>()V

    .line 1849575
    const/4 v4, 0x2

    new-array v4, v4, [Landroid/animation/PropertyValuesHolder;

    const/4 v5, 0x0

    iget-object v6, v2, LX/C62;->f:Landroid/animation/PropertyValuesHolder;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    sget-object v6, LX/C62;->b:Landroid/animation/PropertyValuesHolder;

    aput-object v6, v4, v5

    invoke-virtual {v3, v4}, Landroid/animation/ObjectAnimator;->setValues([Landroid/animation/PropertyValuesHolder;)V

    .line 1849576
    int-to-long v5, v1

    invoke-virtual {v3, v5, v6}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 1849577
    move-object v2, v3

    .line 1849578
    iput-object v2, p0, LX/C60;->i:Landroid/animation/ObjectAnimator;

    .line 1849579
    iget-object v2, p0, LX/C60;->b:LX/C62;

    .line 1849580
    new-instance v3, Landroid/animation/ObjectAnimator;

    invoke-direct {v3}, Landroid/animation/ObjectAnimator;-><init>()V

    .line 1849581
    const/4 v4, 0x2

    new-array v4, v4, [Landroid/animation/PropertyValuesHolder;

    const/4 v5, 0x0

    iget-object v6, v2, LX/C62;->d:Landroid/animation/PropertyValuesHolder;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    sget-object v6, LX/C62;->b:Landroid/animation/PropertyValuesHolder;

    aput-object v6, v4, v5

    invoke-virtual {v3, v4}, Landroid/animation/ObjectAnimator;->setValues([Landroid/animation/PropertyValuesHolder;)V

    .line 1849582
    int-to-long v5, v1

    invoke-virtual {v3, v5, v6}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 1849583
    move-object v1, v3

    .line 1849584
    iput-object v1, p0, LX/C60;->j:Landroid/animation/ObjectAnimator;

    .line 1849585
    iget-object v1, p0, LX/C60;->i:Landroid/animation/ObjectAnimator;

    iget-object v2, p0, LX/C60;->h:Landroid/animation/ObjectAnimator;

    invoke-static {p0, v1, v2}, LX/C60;->a(LX/C60;Landroid/animation/ObjectAnimator;Landroid/animation/ObjectAnimator;)Landroid/animation/AnimatorSet;

    move-result-object v1

    iput-object v1, p0, LX/C60;->k:Landroid/animation/AnimatorSet;

    .line 1849586
    iget-object v1, p0, LX/C60;->j:Landroid/animation/ObjectAnimator;

    iget-object v2, p0, LX/C60;->g:Landroid/animation/ObjectAnimator;

    invoke-static {p0, v1, v2}, LX/C60;->a(LX/C60;Landroid/animation/ObjectAnimator;Landroid/animation/ObjectAnimator;)Landroid/animation/AnimatorSet;

    move-result-object v1

    iput-object v1, p0, LX/C60;->l:Landroid/animation/AnimatorSet;

    .line 1849587
    return-void
.end method

.method public static a(LX/C60;Landroid/animation/ObjectAnimator;Landroid/animation/ObjectAnimator;)Landroid/animation/AnimatorSet;
    .locals 2

    .prologue
    .line 1849546
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    .line 1849547
    new-instance v1, LX/C5x;

    invoke-direct {v1, p0}, LX/C5x;-><init>(LX/C60;)V

    invoke-virtual {p2, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1849548
    invoke-virtual {v0, p1}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 1849549
    return-object v0
.end method

.method private a(Landroid/animation/ObjectAnimator;LX/C5z;)V
    .locals 3

    .prologue
    .line 1849539
    invoke-direct {p0}, LX/C60;->j()LX/3Vs;

    move-result-object v0

    .line 1849540
    if-nez v0, :cond_0

    .line 1849541
    :goto_0
    return-void

    .line 1849542
    :cond_0
    sget-object v1, LX/C5y;->a:[I

    invoke-virtual {p2}, LX/C5z;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1849543
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown view state for feed discovery: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1849544
    :pswitch_0
    invoke-interface {v0, p1}, LX/3Vs;->setAnimatorTargetToRealTimeActivity(Landroid/animation/ObjectAnimator;)V

    goto :goto_0

    .line 1849545
    :pswitch_1
    invoke-interface {v0, p1}, LX/3Vs;->setAnimatorTargetToBlingBar(Landroid/animation/ObjectAnimator;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static b(LX/C60;Landroid/animation/ObjectAnimator;Landroid/animation/ObjectAnimator;)V
    .locals 1

    .prologue
    .line 1849536
    iget-object v0, p0, LX/C60;->d:LX/C5z;

    invoke-direct {p0, p1, v0}, LX/C60;->a(Landroid/animation/ObjectAnimator;LX/C5z;)V

    .line 1849537
    iget-object v0, p0, LX/C60;->e:LX/C5z;

    invoke-direct {p0, p2, v0}, LX/C60;->a(Landroid/animation/ObjectAnimator;LX/C5z;)V

    .line 1849538
    return-void
.end method

.method public static f(LX/C60;)V
    .locals 4

    .prologue
    .line 1849533
    iget-object v0, p0, LX/C60;->k:Landroid/animation/AnimatorSet;

    iget v1, p0, LX/C60;->a:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 1849534
    iget-object v0, p0, LX/C60;->l:Landroid/animation/AnimatorSet;

    iget v1, p0, LX/C60;->a:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 1849535
    return-void
.end method

.method public static g(LX/C60;)Z
    .locals 1

    .prologue
    .line 1849588
    iget-object v0, p0, LX/C60;->k:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/C60;->k:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->isRunning()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, LX/C60;->l:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/C60;->l:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private j()LX/3Vs;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1849532
    iget-object v0, p0, LX/C60;->m:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/C60;->m:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Vs;

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/C5z;Z)V
    .locals 4
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1849517
    invoke-direct {p0}, LX/C60;->j()LX/3Vs;

    move-result-object v0

    .line 1849518
    if-nez v0, :cond_1

    .line 1849519
    :cond_0
    :goto_0
    return-void

    .line 1849520
    :cond_1
    iput-object p1, p0, LX/C60;->d:LX/C5z;

    .line 1849521
    iget-object v1, p0, LX/C60;->n:LX/C5d;

    iget-object v2, p0, LX/C60;->d:LX/C5z;

    .line 1849522
    iget-object v3, v1, LX/C5d;->a:LX/C5h;

    .line 1849523
    iput-object v2, v3, LX/C5h;->a:LX/C5z;

    .line 1849524
    sget-object v1, LX/C5y;->a:[I

    iget-object v2, p0, LX/C60;->d:LX/C5z;

    invoke-virtual {v2}, LX/C5z;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1849525
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown view state for feed discovery: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1849526
    :pswitch_0
    iget-object v1, p0, LX/C60;->p:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-interface {v0, v1}, LX/3Vs;->setText(LX/175;)V

    .line 1849527
    iget-object v1, p0, LX/C60;->o:Ljava/util/List;

    invoke-interface {v0, v1}, LX/3Vs;->setFacepileStrings(Ljava/util/List;)V

    .line 1849528
    if-eqz p2, :cond_0

    .line 1849529
    invoke-interface {v0}, LX/3Vs;->b()V

    goto :goto_0

    .line 1849530
    :pswitch_1
    if-eqz p2, :cond_0

    .line 1849531
    invoke-interface {v0}, LX/3Vs;->a()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final c()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1849501
    iget-boolean v1, p0, LX/C60;->f:Z

    if-eqz v1, :cond_0

    .line 1849502
    iput-boolean v0, p0, LX/C60;->f:Z

    .line 1849503
    :goto_0
    return v0

    .line 1849504
    :cond_0
    iget-object v0, p0, LX/C60;->d:LX/C5z;

    sget-object v1, LX/C5z;->SHOWING_BLING_BAR:LX/C5z;

    if-ne v0, v1, :cond_2

    .line 1849505
    iget-object v0, p0, LX/C60;->d:LX/C5z;

    sget-object v1, LX/C5z;->SHOWING_REAL_TIME_ACTIVITY:LX/C5z;

    if-eq v0, v1, :cond_1

    invoke-static {p0}, LX/C60;->g(LX/C60;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1849506
    :cond_1
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 1849507
    :cond_2
    iget-object v0, p0, LX/C60;->d:LX/C5z;

    sget-object v1, LX/C5z;->SHOWING_BLING_BAR:LX/C5z;

    if-eq v0, v1, :cond_3

    invoke-static {p0}, LX/C60;->g(LX/C60;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1849508
    :cond_3
    :goto_2
    goto :goto_1

    .line 1849509
    :cond_4
    sget-object v0, LX/C5z;->SHOWING_REAL_TIME_ACTIVITY:LX/C5z;

    iput-object v0, p0, LX/C60;->e:LX/C5z;

    .line 1849510
    iget-object v0, p0, LX/C60;->i:Landroid/animation/ObjectAnimator;

    iget-object v1, p0, LX/C60;->h:Landroid/animation/ObjectAnimator;

    invoke-static {p0, v0, v1}, LX/C60;->b(LX/C60;Landroid/animation/ObjectAnimator;Landroid/animation/ObjectAnimator;)V

    .line 1849511
    invoke-static {p0}, LX/C60;->f(LX/C60;)V

    .line 1849512
    iget-object v0, p0, LX/C60;->k:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    goto :goto_1

    .line 1849513
    :cond_5
    sget-object v0, LX/C5z;->SHOWING_BLING_BAR:LX/C5z;

    iput-object v0, p0, LX/C60;->e:LX/C5z;

    .line 1849514
    iget-object v0, p0, LX/C60;->j:Landroid/animation/ObjectAnimator;

    iget-object v1, p0, LX/C60;->g:Landroid/animation/ObjectAnimator;

    invoke-static {p0, v0, v1}, LX/C60;->b(LX/C60;Landroid/animation/ObjectAnimator;Landroid/animation/ObjectAnimator;)V

    .line 1849515
    invoke-static {p0}, LX/C60;->f(LX/C60;)V

    .line 1849516
    iget-object v0, p0, LX/C60;->l:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    goto :goto_2
.end method

.method public final d()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1849491
    iget-object v0, p0, LX/C60;->k:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/C60;->k:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1849492
    iget-object v0, p0, LX/C60;->k:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->end()V

    .line 1849493
    :cond_0
    iget-object v0, p0, LX/C60;->l:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/C60;->l:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1849494
    iget-object v0, p0, LX/C60;->l:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->end()V

    .line 1849495
    :cond_1
    iget-object v0, p0, LX/C60;->g:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setTarget(Ljava/lang/Object;)V

    .line 1849496
    iget-object v0, p0, LX/C60;->h:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setTarget(Ljava/lang/Object;)V

    .line 1849497
    iget-object v0, p0, LX/C60;->i:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setTarget(Ljava/lang/Object;)V

    .line 1849498
    iget-object v0, p0, LX/C60;->j:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setTarget(Ljava/lang/Object;)V

    .line 1849499
    iput-object v1, p0, LX/C60;->m:Ljava/lang/ref/WeakReference;

    .line 1849500
    return-void
.end method
