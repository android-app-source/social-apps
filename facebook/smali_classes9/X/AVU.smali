.class public LX/AVU;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/AVU;


# instance fields
.field private final a:Ljava/lang/String;

.field public final b:LX/0Zb;


# direct methods
.method public constructor <init>(Ljava/lang/String;LX/0Zb;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1679843
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1679844
    iput-object p1, p0, LX/AVU;->a:Ljava/lang/String;

    .line 1679845
    iput-object p2, p0, LX/AVU;->b:LX/0Zb;

    .line 1679846
    return-void
.end method

.method public static a(LX/0QB;)LX/AVU;
    .locals 5

    .prologue
    .line 1679847
    sget-object v0, LX/AVU;->c:LX/AVU;

    if-nez v0, :cond_1

    .line 1679848
    const-class v1, LX/AVU;

    monitor-enter v1

    .line 1679849
    :try_start_0
    sget-object v0, LX/AVU;->c:LX/AVU;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1679850
    if-eqz v2, :cond_0

    .line 1679851
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1679852
    new-instance p0, LX/AVU;

    invoke-static {v0}, LX/0dG;->b(LX/0QB;)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    invoke-direct {p0, v3, v4}, LX/AVU;-><init>(Ljava/lang/String;LX/0Zb;)V

    .line 1679853
    move-object v0, p0

    .line 1679854
    sput-object v0, LX/AVU;->c:LX/AVU;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1679855
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1679856
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1679857
    :cond_1
    sget-object v0, LX/AVU;->c:LX/AVU;

    return-object v0

    .line 1679858
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1679859
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/AVU;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1679860
    iget-object v0, p0, LX/AVU;->b:LX/0Zb;

    invoke-static {p0, p1}, LX/AVU;->d(LX/AVU;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "facecast_event_extra"

    invoke-virtual {v1, v2, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1679861
    return-void
.end method

.method public static c(LX/AVU;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1679862
    iget-object v0, p0, LX/AVU;->b:LX/0Zb;

    invoke-static {p0, p1}, LX/AVU;->d(LX/AVU;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1679863
    return-void
.end method

.method public static d(LX/AVU;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 2

    .prologue
    .line 1679864
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "facecast_broadcaster_update"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "facecast"

    .line 1679865
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1679866
    move-object v0, v0

    .line 1679867
    const-string v1, "facecast_event_name"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    iget-object v1, p0, LX/AVU;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->i(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(ZLjava/lang/String;)V
    .locals 2

    .prologue
    .line 1679868
    const-string v0, "facecast_toggle_comment_translation"

    invoke-static {p0, v0}, LX/AVU;->d(LX/AVU;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "is_translation_enabled"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "translation_toggled_from"

    invoke-virtual {v0, v1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 1679869
    iget-object v1, p0, LX/AVU;->b:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1679870
    return-void
.end method
