.class public LX/Bmo;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Lcom/facebook/content/SecureContextHelper;

.field public b:Z

.field public c:Lcom/facebook/events/ui/location/EventLocationModel;


# direct methods
.method public constructor <init>(Lcom/facebook/content/SecureContextHelper;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1819108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1819109
    iput-object p1, p0, LX/Bmo;->a:Lcom/facebook/content/SecureContextHelper;

    .line 1819110
    return-void
.end method

.method public static b(LX/0QB;)LX/Bmo;
    .locals 2

    .prologue
    .line 1819111
    new-instance v1, LX/Bmo;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    invoke-direct {v1, v0}, LX/Bmo;-><init>(Lcom/facebook/content/SecureContextHelper;)V

    .line 1819112
    return-object v1
.end method


# virtual methods
.method public final a(Landroid/app/Activity;I)V
    .locals 3

    .prologue
    .line 1819113
    invoke-static {}, Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;->newBuilder()LX/9jF;

    move-result-object v0

    sget-object v1, LX/9jG;->EVENT:LX/9jG;

    .line 1819114
    iput-object v1, v0, LX/9jF;->q:LX/9jG;

    .line 1819115
    move-object v0, v0

    .line 1819116
    iget-object v1, p0, LX/Bmo;->c:Lcom/facebook/events/ui/location/EventLocationModel;

    if-eqz v1, :cond_0

    .line 1819117
    iget-object v1, p0, LX/Bmo;->c:Lcom/facebook/events/ui/location/EventLocationModel;

    .line 1819118
    iget-object v2, v1, Lcom/facebook/events/ui/location/EventLocationModel;->a:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-object v1, v2

    .line 1819119
    if-eqz v1, :cond_2

    .line 1819120
    iget-object v1, p0, LX/Bmo;->c:Lcom/facebook/events/ui/location/EventLocationModel;

    .line 1819121
    iget-object v2, v1, Lcom/facebook/events/ui/location/EventLocationModel;->a:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-object v1, v2

    .line 1819122
    invoke-static {v1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->a(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v1

    .line 1819123
    iput-object v1, v0, LX/9jF;->c:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 1819124
    :cond_0
    :goto_0
    iget-boolean v1, p0, LX/Bmo;->b:Z

    if-eqz v1, :cond_1

    .line 1819125
    const/4 v1, 0x1

    .line 1819126
    iput-boolean v1, v0, LX/9jF;->j:Z

    .line 1819127
    :cond_1
    iget-object v1, p0, LX/Bmo;->a:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {v0}, LX/9jF;->a()Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;

    move-result-object v0

    invoke-static {p1, v0}, LX/9jD;->a(Landroid/content/Context;Lcom/facebook/places/checkin/ipc/PlacePickerConfiguration;)Landroid/content/Intent;

    move-result-object v0

    invoke-interface {v1, v0, p2, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 1819128
    return-void

    .line 1819129
    :cond_2
    iget-object v1, p0, LX/Bmo;->c:Lcom/facebook/events/ui/location/EventLocationModel;

    .line 1819130
    iget-object v2, v1, Lcom/facebook/events/ui/location/EventLocationModel;->c:Ljava/lang/String;

    move-object v1, v2

    .line 1819131
    if-eqz v1, :cond_0

    .line 1819132
    iget-object v1, p0, LX/Bmo;->c:Lcom/facebook/events/ui/location/EventLocationModel;

    .line 1819133
    iget-object v2, v1, Lcom/facebook/events/ui/location/EventLocationModel;->c:Ljava/lang/String;

    move-object v1, v2

    .line 1819134
    iput-object v1, v0, LX/9jF;->m:Ljava/lang/String;

    .line 1819135
    goto :goto_0
.end method
