.class public final LX/B8z;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/leadgen/view/LeadGenActionButtonsView;


# direct methods
.method public constructor <init>(Lcom/facebook/leadgen/view/LeadGenActionButtonsView;)V
    .locals 0

    .prologue
    .line 1750808
    iput-object p1, p0, LX/B8z;->a:Lcom/facebook/leadgen/view/LeadGenActionButtonsView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()V
    .locals 5

    .prologue
    .line 1750817
    iget-object v0, p0, LX/B8z;->a:Lcom/facebook/leadgen/view/LeadGenActionButtonsView;

    invoke-virtual {v0}, Lcom/facebook/leadgen/view/LeadGenActionButtonsView;->a()V

    .line 1750818
    iget-object v0, p0, LX/B8z;->a:Lcom/facebook/leadgen/view/LeadGenActionButtonsView;

    iget-object v0, v0, Lcom/facebook/leadgen/view/LeadGenActionButtonsView;->b:LX/B7W;

    new-instance v1, LX/B7e;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, LX/B7e;-><init>(Z)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1750819
    iget-object v0, p0, LX/B8z;->a:Lcom/facebook/leadgen/view/LeadGenActionButtonsView;

    iget-object v0, v0, Lcom/facebook/leadgen/view/LeadGenActionButtonsView;->c:LX/0if;

    sget-object v1, LX/0ig;->v:LX/0ih;

    const-string v2, "click_submit_button"

    invoke-virtual {v0, v1, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1750820
    iget-object v0, p0, LX/B8z;->a:Lcom/facebook/leadgen/view/LeadGenActionButtonsView;

    iget-object v0, v0, Lcom/facebook/leadgen/view/LeadGenActionButtonsView;->g:LX/B6S;

    iget-object v1, p0, LX/B8z;->a:Lcom/facebook/leadgen/view/LeadGenActionButtonsView;

    iget-object v1, v1, Lcom/facebook/leadgen/view/LeadGenActionButtonsView;->f:LX/B7F;

    iget-object v2, p0, LX/B8z;->a:Lcom/facebook/leadgen/view/LeadGenActionButtonsView;

    iget-object v2, v2, Lcom/facebook/leadgen/view/LeadGenActionButtonsView;->e:LX/B6u;

    invoke-virtual {v2}, LX/B6u;->e()LX/0P1;

    move-result-object v2

    iget-object v3, p0, LX/B8z;->a:Lcom/facebook/leadgen/view/LeadGenActionButtonsView;

    iget-object v3, v3, Lcom/facebook/leadgen/view/LeadGenActionButtonsView;->e:LX/B6u;

    invoke-virtual {v3}, LX/B6u;->g()LX/0P1;

    move-result-object v3

    iget-object v4, p0, LX/B8z;->a:Lcom/facebook/leadgen/view/LeadGenActionButtonsView;

    iget-object v4, v4, Lcom/facebook/leadgen/view/LeadGenActionButtonsView;->e:LX/B6u;

    .line 1750821
    iget p0, v4, LX/B6u;->h:I

    move v4, p0

    .line 1750822
    invoke-virtual {v0, v1, v2, v3, v4}, LX/B6S;->a(LX/B7F;LX/0P1;LX/0P1;I)V

    .line 1750823
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x36991507

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1750809
    iget-object v1, p0, LX/B8z;->a:Lcom/facebook/leadgen/view/LeadGenActionButtonsView;

    iget-object v1, v1, Lcom/facebook/leadgen/view/LeadGenActionButtonsView;->e:LX/B6u;

    invoke-virtual {v1}, LX/B6u;->k()LX/B77;

    move-result-object v1

    .line 1750810
    invoke-virtual {v1}, LX/B77;->isValid()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1750811
    invoke-direct {p0}, LX/B8z;->a()V

    .line 1750812
    iget-object v1, p0, LX/B8z;->a:Lcom/facebook/leadgen/view/LeadGenActionButtonsView;

    iget-object v1, v1, Lcom/facebook/leadgen/view/LeadGenActionButtonsView;->h:LX/D82;

    if-eqz v1, :cond_0

    .line 1750813
    iget-object v1, p0, LX/B8z;->a:Lcom/facebook/leadgen/view/LeadGenActionButtonsView;

    iget-object v1, v1, Lcom/facebook/leadgen/view/LeadGenActionButtonsView;->h:LX/D82;

    invoke-virtual {v1}, LX/D82;->a()V

    .line 1750814
    :cond_0
    :goto_0
    const v1, 0x5771da38

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 1750815
    :cond_1
    iget-object v1, p0, LX/B8z;->a:Lcom/facebook/leadgen/view/LeadGenActionButtonsView;

    iget-object v1, v1, Lcom/facebook/leadgen/view/LeadGenActionButtonsView;->h:LX/D82;

    if-eqz v1, :cond_0

    .line 1750816
    iget-object v1, p0, LX/B8z;->a:Lcom/facebook/leadgen/view/LeadGenActionButtonsView;

    iget-object v1, v1, Lcom/facebook/leadgen/view/LeadGenActionButtonsView;->h:LX/D82;

    invoke-virtual {v1}, LX/D82;->b()V

    goto :goto_0
.end method
