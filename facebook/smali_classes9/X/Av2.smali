.class public final LX/Av2;
.super LX/0xh;
.source ""


# instance fields
.field public final synthetic a:LX/Av3;


# direct methods
.method public constructor <init>(LX/Av3;)V
    .locals 0

    .prologue
    .line 1723622
    iput-object p1, p0, LX/Av2;->a:LX/Av3;

    invoke-direct {p0}, LX/0xh;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0wd;)V
    .locals 7

    .prologue
    const/high16 v6, 0x3f800000    # 1.0f

    .line 1723607
    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v0

    double-to-float v0, v0

    .line 1723608
    iget-object v1, p0, LX/Av2;->a:LX/Av3;

    iget-object v1, v1, LX/Av3;->b:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    sub-float v2, v6, v0

    mul-float/2addr v1, v2

    iget-object v2, p0, LX/Av2;->a:LX/Av3;

    iget-object v2, v2, LX/Av3;->c:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    int-to-float v2, v2

    mul-float/2addr v2, v0

    add-float/2addr v1, v2

    .line 1723609
    iget-object v2, p0, LX/Av2;->a:LX/Av3;

    iget-object v2, v2, LX/Av3;->b:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    sub-float v3, v6, v0

    mul-float/2addr v2, v3

    iget-object v3, p0, LX/Av2;->a:LX/Av3;

    iget-object v3, v3, LX/Av3;->c:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    int-to-float v3, v3

    mul-float/2addr v3, v0

    add-float/2addr v2, v3

    .line 1723610
    iget-object v3, p0, LX/Av2;->a:LX/Av3;

    iget-object v3, v3, LX/Av3;->b:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    int-to-float v3, v3

    sub-float v4, v6, v0

    mul-float/2addr v3, v4

    iget-object v4, p0, LX/Av2;->a:LX/Av3;

    iget-object v4, v4, LX/Av3;->c:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v4, v0

    add-float/2addr v3, v4

    float-to-int v3, v3

    .line 1723611
    iget-object v4, p0, LX/Av2;->a:LX/Av3;

    iget-object v4, v4, LX/Av3;->b:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    int-to-float v4, v4

    sub-float v5, v6, v0

    mul-float/2addr v4, v5

    iget-object v5, p0, LX/Av2;->a:LX/Av3;

    iget-object v5, v5, LX/Av3;->c:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v5, v0

    add-float/2addr v4, v5

    float-to-int v4, v4

    .line 1723612
    iget-object v5, p0, LX/Av2;->a:LX/Av3;

    iget v5, v5, LX/Av3;->d:F

    sub-float/2addr v6, v0

    mul-float/2addr v5, v6

    iget-object v6, p0, LX/Av2;->a:LX/Av3;

    iget v6, v6, LX/Av3;->e:F

    mul-float/2addr v0, v6

    add-float/2addr v0, v5

    .line 1723613
    iget-object v5, p0, LX/Av2;->a:LX/Av3;

    iget-object v5, v5, LX/Av3;->a:Landroid/widget/ImageView;

    invoke-virtual {v5, v1}, Landroid/widget/ImageView;->setTranslationX(F)V

    .line 1723614
    iget-object v1, p0, LX/Av2;->a:LX/Av3;

    iget-object v1, v1, LX/Av3;->a:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setTranslationY(F)V

    .line 1723615
    iget-object v1, p0, LX/Av2;->a:LX/Av3;

    iget-object v1, v1, LX/Av3;->a:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iput v4, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1723616
    iget-object v1, p0, LX/Av2;->a:LX/Av3;

    iget-object v1, v1, LX/Av3;->a:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iput v3, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1723617
    iget-object v1, p0, LX/Av2;->a:LX/Av3;

    iget-object v1, v1, LX/Av3;->a:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->requestLayout()V

    .line 1723618
    iget-object v1, p0, LX/Av2;->a:LX/Av3;

    iget-object v1, v1, LX/Av3;->a:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setRotation(F)V

    .line 1723619
    return-void
.end method

.method public final b(LX/0wd;)V
    .locals 1

    .prologue
    .line 1723620
    iget-object v0, p0, LX/Av2;->a:LX/Av3;

    iget-object v0, v0, LX/Av3;->f:LX/Auk;

    invoke-interface {v0}, LX/Auk;->a()V

    .line 1723621
    return-void
.end method
