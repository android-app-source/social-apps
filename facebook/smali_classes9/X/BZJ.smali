.class public LX/BZJ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnoreProperties;
    b = true
.end annotation


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/BZH;",
            ">;"
        }
    .end annotation
.end field

.field private b:LX/8vx;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1797181
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getNodes()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/BZH;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1797182
    iget-object v0, p0, LX/BZJ;->a:Ljava/util/List;

    return-object v0
.end method

.method public getVideo_render_type()LX/8vx;
    .locals 1

    .prologue
    .line 1797183
    iget-object v0, p0, LX/BZJ;->b:LX/8vx;

    return-object v0
.end method

.method public setNodes(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/BZH;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1797184
    iput-object p1, p0, LX/BZJ;->a:Ljava/util/List;

    .line 1797185
    return-void
.end method

.method public setVideo_render_type(LX/8vx;)V
    .locals 0

    .prologue
    .line 1797186
    iput-object p1, p0, LX/BZJ;->b:LX/8vx;

    .line 1797187
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1797188
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Scene{mNodes="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/BZJ;->a:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
