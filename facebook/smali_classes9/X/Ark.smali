.class public final LX/Ark;
.super LX/6Il;
.source ""


# instance fields
.field public final synthetic a:LX/Arq;


# direct methods
.method public constructor <init>(LX/Arq;)V
    .locals 0

    .prologue
    .line 1719606
    iput-object p1, p0, LX/Ark;->a:LX/Arq;

    invoke-direct {p0}, LX/6Il;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1719621
    iget-object v0, p0, LX/Ark;->a:LX/Arq;

    iget-object v0, v0, LX/Arq;->w:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 1719622
    iget-object v0, p0, LX/Ark;->a:LX/Arq;

    const-string v1, "pr_camera_open"

    invoke-static {v0, v1, p1}, LX/Arq;->a$redex0(LX/Arq;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1719623
    iget-object v0, p0, LX/Ark;->a:LX/Arq;

    iget-object v0, v0, LX/Arq;->e:LX/Arh;

    invoke-virtual {v0, p1}, LX/Arh;->a(Ljava/lang/Throwable;)V

    .line 1719624
    return-void
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 1719607
    iget-object v0, p0, LX/Ark;->a:LX/Arq;

    .line 1719608
    invoke-static {v0}, LX/Arq;->t(LX/Arq;)LX/6JB;

    move-result-object v1

    iput-object v1, v0, LX/Arq;->E:LX/6JB;

    .line 1719609
    const/4 v2, 0x0

    .line 1719610
    new-instance v1, LX/6JC;

    invoke-direct {v1, v2, v2}, LX/6JC;-><init>(LX/6JN;LX/6JO;)V

    move-object v1, v1

    .line 1719611
    iput-object v1, v0, LX/Arq;->F:LX/6JC;

    .line 1719612
    iget-object v1, v0, LX/Arq;->r:LX/6Jt;

    iget-object v2, v0, LX/Arq;->D:LX/6Ia;

    iget-object v3, v0, LX/Arq;->E:LX/6JB;

    iget-object p0, v0, LX/Arq;->B:LX/6JR;

    invoke-virtual {v1, v2, v3, p0}, LX/6Jt;->a(LX/6Ia;LX/6JB;LX/6JR;)V

    .line 1719613
    iget-object v1, v0, LX/Arq;->r:LX/6Jt;

    iget-object v2, v0, LX/Arq;->c:LX/6Ik;

    iget-object v3, v0, LX/Arq;->F:LX/6JC;

    invoke-virtual {v1, v2, v3}, LX/6Jt;->a(LX/6Ik;LX/6JC;)V

    .line 1719614
    iget-object v1, v0, LX/Arq;->v:LX/0UE;

    invoke-virtual {v1}, LX/0UE;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Arb;

    .line 1719615
    invoke-interface {v1}, LX/Arb;->a()V

    goto :goto_0

    .line 1719616
    :cond_0
    iget-object v1, v0, LX/Arq;->e:LX/Arh;

    .line 1719617
    invoke-static {v1}, LX/Arh;->j(LX/Arh;)LX/0jL;

    move-result-object v3

    iget-boolean v2, v1, LX/Arh;->w:Z

    if-eqz v2, :cond_1

    sget-object v2, LX/86q;->RECORDING_VIDEO:LX/86q;

    :goto_1
    invoke-static {v1, v3, v2}, LX/Arh;->a(LX/Arh;LX/0jL;LX/86q;)LX/0jL;

    move-result-object v2

    invoke-virtual {v2}, LX/0jL;->a()V

    .line 1719618
    const/4 v2, 0x0

    iput-boolean v2, v1, LX/Arh;->w:Z

    .line 1719619
    return-void

    .line 1719620
    :cond_1
    sget-object v2, LX/86q;->READY:LX/86q;

    goto :goto_1
.end method
