.class public LX/B6u;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0So;

.field private final b:LX/B6l;

.field public final c:Landroid/support/v4/view/ViewPager;

.field public final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/B6O;

.field public h:I

.field public i:J

.field private j:LX/B7W;

.field public k:LX/B7F;

.field private final l:LX/B6q;

.field private final m:LX/B6s;


# direct methods
.method public constructor <init>(LX/B6P;LX/0So;LX/B6l;LX/B7W;Landroid/support/v4/view/ViewPager;LX/B7F;Lcom/facebook/leadgen/cache/LeadGenFormPendingInputEntry;)V
    .locals 6
    .param p5    # Landroid/support/v4/view/ViewPager;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/B7F;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # Lcom/facebook/leadgen/cache/LeadGenFormPendingInputEntry;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1747426
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1747427
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/B6u;->d:Ljava/util/Map;

    .line 1747428
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/B6u;->e:Ljava/util/Map;

    .line 1747429
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/B6u;->f:Ljava/util/Map;

    .line 1747430
    new-instance v0, LX/B6r;

    invoke-direct {v0, p0}, LX/B6r;-><init>(LX/B6u;)V

    iput-object v0, p0, LX/B6u;->l:LX/B6q;

    .line 1747431
    new-instance v0, LX/B6t;

    invoke-direct {v0, p0}, LX/B6t;-><init>(LX/B6u;)V

    iput-object v0, p0, LX/B6u;->m:LX/B6s;

    .line 1747432
    iput-object p2, p0, LX/B6u;->a:LX/0So;

    .line 1747433
    iput-object p3, p0, LX/B6u;->b:LX/B6l;

    .line 1747434
    iput-object p5, p0, LX/B6u;->c:Landroid/support/v4/view/ViewPager;

    .line 1747435
    iput-object p6, p0, LX/B6u;->k:LX/B7F;

    .line 1747436
    iput-object p4, p0, LX/B6u;->j:LX/B7W;

    .line 1747437
    iget-object v0, p0, LX/B6u;->j:LX/B7W;

    iget-object v1, p0, LX/B6u;->l:LX/B6q;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 1747438
    iget-object v0, p0, LX/B6u;->j:LX/B7W;

    iget-object v1, p0, LX/B6u;->m:LX/B6s;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 1747439
    iget-object v2, p0, LX/B6u;->k:LX/B7F;

    const/4 v3, 0x0

    .line 1747440
    new-instance v5, LX/B6O;

    invoke-static {p1}, LX/2sb;->a(LX/0QB;)LX/2sb;

    move-result-object v4

    check-cast v4, LX/2sb;

    invoke-direct {v5, v2, p7, v3, v4}, LX/B6O;-><init>(LX/B7F;Lcom/facebook/leadgen/cache/LeadGenFormPendingInputEntry;LX/B7w;LX/2sb;)V

    .line 1747441
    move-object v2, v5

    .line 1747442
    iput-object v2, p0, LX/B6u;->g:LX/B6O;

    .line 1747443
    iget-object v2, p0, LX/B6u;->c:Landroid/support/v4/view/ViewPager;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Landroid/support/v4/view/ViewPager;->setOffscreenPageLimit(I)V

    .line 1747444
    iget-object v2, p0, LX/B6u;->c:Landroid/support/v4/view/ViewPager;

    iget-object v3, p0, LX/B6u;->g:LX/B6O;

    invoke-virtual {v2, v3}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 1747445
    const/4 v2, 0x0

    iput v2, p0, LX/B6u;->h:I

    .line 1747446
    iget-object v2, p0, LX/B6u;->a:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    iput-wide v2, p0, LX/B6u;->i:J

    .line 1747447
    if-eqz p7, :cond_0

    .line 1747448
    iget v2, p7, Lcom/facebook/leadgen/cache/LeadGenFormPendingInputEntry;->b:I

    iput v2, p0, LX/B6u;->h:I

    .line 1747449
    iget-object v2, p0, LX/B6u;->d:Ljava/util/Map;

    iget-object v3, p7, Lcom/facebook/leadgen/cache/LeadGenFormPendingInputEntry;->c:Ljava/util/Map;

    invoke-interface {v2, v3}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 1747450
    :cond_0
    iget-object v2, p0, LX/B6u;->c:Landroid/support/v4/view/ViewPager;

    iget v3, p0, LX/B6u;->h:I

    invoke-virtual {v2, v3}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 1747451
    return-void
.end method

.method private a(Ljava/util/List;)V
    .locals 5
    .param p1    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/B8s;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1747413
    if-eqz p1, :cond_0

    .line 1747414
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/B8s;

    .line 1747415
    iget-object v2, p0, LX/B6u;->d:Ljava/util/Map;

    .line 1747416
    iget-object v3, v0, LX/B8s;->b:Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;

    move-object v3, v3

    .line 1747417
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->j()Ljava/lang/String;

    move-result-object v3

    .line 1747418
    iget-object v4, v0, LX/B8s;->a:Ljava/lang/String;

    move-object v4, v4

    .line 1747419
    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1747420
    iget-object v2, p0, LX/B6u;->e:Ljava/util/Map;

    .line 1747421
    iget-object v3, v0, LX/B8s;->b:Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;

    move-object v3, v3

    .line 1747422
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->j()Ljava/lang/String;

    move-result-object v3

    .line 1747423
    iget-object v4, v0, LX/B8s;->c:Ljava/lang/String;

    move-object v0, v4

    .line 1747424
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1747425
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 1747376
    iget v1, p0, LX/B6u;->h:I

    iget-object v2, p0, LX/B6u;->g:LX/B6O;

    invoke-virtual {v2}, LX/0gG;->b()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v1, v2, :cond_0

    .line 1747377
    iget v1, p0, LX/B6u;->h:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, LX/B6u;->h:I

    .line 1747378
    iget-object v1, p0, LX/B6u;->c:Landroid/support/v4/view/ViewPager;

    iget v2, p0, LX/B6u;->h:I

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/view/ViewPager;->a(IZ)V

    .line 1747379
    iget-object v1, p0, LX/B6u;->g:LX/B6O;

    iget-object v2, p0, LX/B6u;->k:LX/B7F;

    iget-object v3, p0, LX/B6u;->d:Ljava/util/Map;

    iget v4, p0, LX/B6u;->h:I

    invoke-virtual {v1, v2, v3, v4}, LX/B6O;->a(LX/B7F;Ljava/util/Map;I)V

    .line 1747380
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 1747408
    iget v1, p0, LX/B6u;->h:I

    if-lez v1, :cond_0

    .line 1747409
    iget v1, p0, LX/B6u;->h:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, LX/B6u;->h:I

    .line 1747410
    iget-object v1, p0, LX/B6u;->c:Landroid/support/v4/view/ViewPager;

    iget v2, p0, LX/B6u;->h:I

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/view/ViewPager;->a(IZ)V

    .line 1747411
    iget-object v1, p0, LX/B6u;->g:LX/B6O;

    iget-object v2, p0, LX/B6u;->k:LX/B7F;

    iget-object v3, p0, LX/B6u;->d:Ljava/util/Map;

    iget v4, p0, LX/B6u;->h:I

    invoke-virtual {v1, v2, v3, v4}, LX/B6O;->a(LX/B7F;Ljava/util/Map;I)V

    .line 1747412
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1747407
    iget v0, p0, LX/B6u;->h:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 1747406
    iget v0, p0, LX/B6u;->h:I

    iget-object v1, p0, LX/B6u;->g:LX/B6O;

    invoke-virtual {v1}, LX/0gG;->b()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()LX/0P1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1747405
    iget-object v0, p0, LX/B6u;->d:Ljava/util/Map;

    invoke-static {v0}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    return-object v0
.end method

.method public final g()LX/0P1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1747404
    iget-object v0, p0, LX/B6u;->f:Ljava/util/Map;

    invoke-static {v0}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    return-object v0
.end method

.method public final j()V
    .locals 2

    .prologue
    .line 1747400
    iget-object v0, p0, LX/B6u;->g:LX/B6O;

    .line 1747401
    iget-object v1, v0, LX/B6O;->e:LX/B6N;

    move-object v0, v1

    .line 1747402
    invoke-interface {v0}, LX/B6N;->b()LX/0Px;

    move-result-object v0

    invoke-direct {p0, v0}, LX/B6u;->a(Ljava/util/List;)V

    .line 1747403
    return-void
.end method

.method public final k()LX/B77;
    .locals 3

    .prologue
    .line 1747390
    iget-object v0, p0, LX/B6u;->g:LX/B6O;

    .line 1747391
    iget-object v1, v0, LX/B6O;->e:LX/B6N;

    move-object v1, v1

    .line 1747392
    if-nez v1, :cond_1

    .line 1747393
    sget-object v0, LX/B77;->UNKNOWN_ERROR:LX/B77;

    .line 1747394
    :cond_0
    :goto_0
    return-object v0

    .line 1747395
    :cond_1
    iget v0, p0, LX/B6u;->h:I

    invoke-interface {v1, v0}, LX/B6N;->l_(I)LX/B77;

    move-result-object v0

    .line 1747396
    invoke-virtual {v0}, LX/B77;->isValid()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1747397
    invoke-interface {v1}, LX/B6N;->b()LX/0Px;

    move-result-object v2

    invoke-direct {p0, v2}, LX/B6u;->a(Ljava/util/List;)V

    .line 1747398
    invoke-virtual {p0}, LX/B6u;->n()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1747399
    iget-object v2, p0, LX/B6u;->f:Ljava/util/Map;

    invoke-interface {v1}, LX/B6N;->c()LX/0P1;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    goto :goto_0
.end method

.method public final l()LX/B6N;
    .locals 1

    .prologue
    .line 1747387
    iget-object v0, p0, LX/B6u;->g:LX/B6O;

    .line 1747388
    iget-object p0, v0, LX/B6O;->e:LX/B6N;

    move-object v0, p0

    .line 1747389
    return-object v0
.end method

.method public final n()Z
    .locals 2

    .prologue
    .line 1747384
    iget-object v0, p0, LX/B6u;->g:LX/B6O;

    iget v1, p0, LX/B6u;->h:I

    .line 1747385
    iget-object p0, v0, LX/B6O;->b:Ljava/util/List;

    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p0

    instance-of p0, p0, LX/B7E;

    move v0, p0

    .line 1747386
    return v0
.end method

.method public final o()Z
    .locals 2

    .prologue
    .line 1747381
    iget-object v0, p0, LX/B6u;->g:LX/B6O;

    iget v1, p0, LX/B6u;->h:I

    .line 1747382
    iget-object p0, v0, LX/B6O;->b:Ljava/util/List;

    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p0

    instance-of p0, p0, LX/B7C;

    move v0, p0

    .line 1747383
    return v0
.end method
