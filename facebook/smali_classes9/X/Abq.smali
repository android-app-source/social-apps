.class public final LX/Abq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field public final synthetic a:LX/Aby;


# direct methods
.method public constructor <init>(LX/Aby;)V
    .locals 0

    .prologue
    .line 1691542
    iput-object p1, p0, LX/Abq;->a:LX/Aby;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 9

    .prologue
    const/4 v5, 0x0

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 1691543
    iget-object v0, p0, LX/Abq;->a:LX/Aby;

    iget-object v0, v0, LX/Aby;->r:Landroid/view/GestureDetector;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/Abq;->a:LX/Aby;

    iget-boolean v0, v0, LX/Aby;->H:Z

    if-nez v0, :cond_2

    iget-object v0, p0, LX/Abq;->a:LX/Aby;

    iget-boolean v0, v0, LX/Aby;->M:Z

    if-nez v0, :cond_2

    iget-object v0, p0, LX/Abq;->a:LX/Aby;

    iget-boolean v0, v0, LX/Aby;->w:Z

    if-nez v0, :cond_2

    .line 1691544
    iget-object v0, p0, LX/Abq;->a:LX/Aby;

    .line 1691545
    iput-object p1, v0, LX/Aby;->s:Landroid/view/View;

    .line 1691546
    iget-object v0, p0, LX/Abq;->a:LX/Aby;

    iget-boolean v0, v0, LX/Aby;->w:Z

    if-nez v0, :cond_0

    .line 1691547
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    if-le v0, v8, :cond_3

    .line 1691548
    iget-object v0, p0, LX/Abq;->a:LX/Aby;

    iget-boolean v0, v0, LX/Aby;->L:Z

    if-nez v0, :cond_0

    .line 1691549
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 1691550
    const/4 v4, 0x3

    move-wide v2, v0

    move v6, v5

    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v1

    .line 1691551
    iget-object v0, p0, LX/Abq;->a:LX/Aby;

    .line 1691552
    iget-object v2, v0, LX/AVi;->a:Landroid/view/View;

    move-object v2, v2

    .line 1691553
    move-object v0, v2

    .line 1691554
    check-cast v0, Lcom/facebook/facecastdisplay/FacecastInteractionView;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/FacecastInteractionView;->b:Lcom/facebook/widget/CustomViewPager;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomViewPager;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 1691555
    iget-object v0, p0, LX/Abq;->a:LX/Aby;

    .line 1691556
    iput-boolean v8, v0, LX/Aby;->L:Z

    .line 1691557
    :cond_0
    :goto_0
    iget-object v0, p0, LX/Abq;->a:LX/Aby;

    iget-object v0, v0, LX/Aby;->r:Landroid/view/GestureDetector;

    invoke-virtual {v0, p2}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 1691558
    iget-object v0, p0, LX/Abq;->a:LX/Aby;

    iget-object v0, v0, LX/Aby;->t:LX/Ade;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/Abq;->a:LX/Aby;

    iget-boolean v0, v0, LX/Aby;->w:Z

    if-nez v0, :cond_1

    .line 1691559
    iget-object v0, p0, LX/Abq;->a:LX/Aby;

    iget-object v0, v0, LX/Aby;->t:LX/Ade;

    iput-object p2, v0, LX/Ade;->a:Landroid/view/MotionEvent;

    .line 1691560
    iget-object v0, p0, LX/Abq;->a:LX/Aby;

    iget-object v0, v0, LX/Aby;->e:LX/3HT;

    iget-object v1, p0, LX/Abq;->a:LX/Aby;

    iget-object v1, v1, LX/Aby;->t:LX/Ade;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    :cond_1
    move v7, v8

    .line 1691561
    :cond_2
    return v7

    .line 1691562
    :cond_3
    iget-object v0, p0, LX/Abq;->a:LX/Aby;

    .line 1691563
    iget-object v1, v0, LX/AVi;->a:Landroid/view/View;

    move-object v1, v1

    .line 1691564
    move-object v0, v1

    .line 1691565
    check-cast v0, Lcom/facebook/facecastdisplay/FacecastInteractionView;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/FacecastInteractionView;->b:Lcom/facebook/widget/CustomViewPager;

    invoke-virtual {v0, p2}, Lcom/facebook/widget/CustomViewPager;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 1691566
    iget-object v0, p0, LX/Abq;->a:LX/Aby;

    .line 1691567
    iput-boolean v7, v0, LX/Aby;->L:Z

    .line 1691568
    goto :goto_0
.end method
