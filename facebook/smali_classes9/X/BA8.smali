.class public LX/BA8;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/BZS;

.field public b:Z

.field public final c:LX/BA7;

.field private d:I

.field private e:I

.field public f:I

.field private g:I

.field private h:Z


# direct methods
.method public constructor <init>(LX/BZS;)V
    .locals 1
    .param p1    # LX/BZS;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param

    .prologue
    .line 1752711
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1752712
    iput-object p1, p0, LX/BA8;->a:LX/BZS;

    .line 1752713
    new-instance v0, LX/BA7;

    invoke-direct {v0, p0}, LX/BA7;-><init>(LX/BA8;)V

    iput-object v0, p0, LX/BA8;->c:LX/BA7;

    .line 1752714
    return-void
.end method

.method public static b(LX/BA8;)V
    .locals 13

    .prologue
    .line 1752715
    iget-object v0, p0, LX/BA8;->a:LX/BZS;

    iget v1, p0, LX/BA8;->d:I

    iget v2, p0, LX/BA8;->e:I

    iget v3, p0, LX/BA8;->f:I

    iget v4, p0, LX/BA8;->g:I

    iget-boolean v5, p0, LX/BA8;->h:Z

    .line 1752716
    invoke-static {v0}, LX/BZS;->f(LX/BZS;)J

    move-result-wide v6

    move v8, v1

    move v9, v2

    move v10, v3

    move v11, v4

    move v12, v5

    invoke-static/range {v6 .. v12}, Lme/msqrd/sdk/nativecalls/effectsframework/GraphicsEngineNativeCalls;->setUpImageSourceFacet(JIIIIZ)V

    .line 1752717
    return-void
.end method


# virtual methods
.method public final a(IIIZ)V
    .locals 1

    .prologue
    .line 1752718
    iput p1, p0, LX/BA8;->d:I

    .line 1752719
    iput p2, p0, LX/BA8;->e:I

    .line 1752720
    iput p3, p0, LX/BA8;->g:I

    .line 1752721
    iput-boolean p4, p0, LX/BA8;->h:Z

    .line 1752722
    iput p1, p0, LX/BA8;->f:I

    .line 1752723
    invoke-static {p0}, LX/BA8;->b(LX/BA8;)V

    .line 1752724
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/BA8;->b:Z

    .line 1752725
    return-void
.end method

.method public final a([B)V
    .locals 3

    .prologue
    .line 1752726
    if-eqz p1, :cond_0

    iget-boolean v0, p0, LX/BA8;->b:Z

    if-nez v0, :cond_1

    .line 1752727
    :cond_0
    :goto_0
    return-void

    .line 1752728
    :cond_1
    iget-object v0, p0, LX/BA8;->a:LX/BZS;

    .line 1752729
    invoke-static {v0}, LX/BZS;->f(LX/BZS;)J

    move-result-wide v1

    invoke-static {v1, v2, p1}, Lme/msqrd/sdk/nativecalls/effectsframework/GraphicsEngineNativeCalls;->writeImage(J[B)V

    .line 1752730
    goto :goto_0
.end method
