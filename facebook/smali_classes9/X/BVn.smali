.class public final enum LX/BVn;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/BVn;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/BVn;

.field public static final enum FILL_VIEWPORT:LX/BVn;


# instance fields
.field private final mValue:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1791424
    new-instance v0, LX/BVn;

    const-string v1, "FILL_VIEWPORT"

    const-string v2, "fillViewport"

    invoke-direct {v0, v1, v3, v2}, LX/BVn;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BVn;->FILL_VIEWPORT:LX/BVn;

    .line 1791425
    const/4 v0, 0x1

    new-array v0, v0, [LX/BVn;

    sget-object v1, LX/BVn;->FILL_VIEWPORT:LX/BVn;

    aput-object v1, v0, v3

    sput-object v0, LX/BVn;->$VALUES:[LX/BVn;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1791426
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1791427
    iput-object p3, p0, LX/BVn;->mValue:Ljava/lang/String;

    .line 1791428
    return-void
.end method

.method public static from(Ljava/lang/String;)LX/BVn;
    .locals 4

    .prologue
    .line 1791429
    invoke-static {}, LX/BVn;->values()[LX/BVn;

    move-result-object v2

    .line 1791430
    const/4 v0, 0x0

    :goto_0
    array-length v1, v2

    if-ge v0, v1, :cond_1

    .line 1791431
    aget-object v1, v2, v0

    .line 1791432
    iget-object v3, v1, LX/BVn;->mValue:Ljava/lang/String;

    invoke-virtual {v3, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move-object v0, v1

    .line 1791433
    :goto_1
    return-object v0

    .line 1791434
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1791435
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)LX/BVn;
    .locals 1

    .prologue
    .line 1791436
    const-class v0, LX/BVn;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/BVn;

    return-object v0
.end method

.method public static values()[LX/BVn;
    .locals 1

    .prologue
    .line 1791437
    sget-object v0, LX/BVn;->$VALUES:[LX/BVn;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/BVn;

    return-object v0
.end method
