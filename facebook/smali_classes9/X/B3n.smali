.class public final LX/B3n;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:Landroid/widget/ImageButton;

.field public final synthetic b:Lcom/facebook/heisman/category/CategoryBrowserFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/heisman/category/CategoryBrowserFragment;Landroid/widget/ImageButton;)V
    .locals 0

    .prologue
    .line 1740911
    iput-object p1, p0, LX/B3n;->b:Lcom/facebook/heisman/category/CategoryBrowserFragment;

    iput-object p2, p0, LX/B3n;->a:Landroid/widget/ImageButton;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 1740912
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1740913
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 1740898
    iget-object v0, p0, LX/B3n;->b:Lcom/facebook/heisman/category/CategoryBrowserFragment;

    iget-object v0, v0, Lcom/facebook/heisman/category/CategoryBrowserFragment;->f:LX/B3l;

    .line 1740899
    invoke-static {v0}, LX/B3l;->d(LX/B3l;)V

    .line 1740900
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1740901
    invoke-virtual {v0}, LX/B3l;->a()V

    .line 1740902
    :goto_0
    iget-object v0, p0, LX/B3n;->b:Lcom/facebook/heisman/category/CategoryBrowserFragment;

    iget-object v0, v0, Lcom/facebook/heisman/category/CategoryBrowserFragment;->i:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    .line 1740903
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1740904
    iget-object v0, p0, LX/B3n;->b:Lcom/facebook/heisman/category/CategoryBrowserFragment;

    iget-object v0, v0, Lcom/facebook/heisman/category/CategoryBrowserFragment;->g:LX/B3e;

    invoke-virtual {v0, v2}, LX/B3e;->b(Z)V

    .line 1740905
    iget-object v0, p0, LX/B3n;->a:Landroid/widget/ImageButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 1740906
    :goto_1
    return-void

    .line 1740907
    :cond_0
    iget-object v0, p0, LX/B3n;->b:Lcom/facebook/heisman/category/CategoryBrowserFragment;

    iget-object v0, v0, Lcom/facebook/heisman/category/CategoryBrowserFragment;->g:LX/B3e;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/B3e;->b(Z)V

    .line 1740908
    iget-object v0, p0, LX/B3n;->a:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_1

    .line 1740909
    :cond_1
    new-instance v3, Lcom/facebook/heisman/category/CategoryBrowserFetchController$SearchTask;

    invoke-direct {v3, v0, p1}, Lcom/facebook/heisman/category/CategoryBrowserFetchController$SearchTask;-><init>(LX/B3l;Ljava/lang/CharSequence;)V

    iput-object v3, v0, LX/B3l;->i:Lcom/facebook/heisman/category/CategoryBrowserFetchController$SearchTask;

    .line 1740910
    iget-object v3, v0, LX/B3l;->f:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/Handler;

    iget-object v4, v0, LX/B3l;->i:Lcom/facebook/heisman/category/CategoryBrowserFetchController$SearchTask;

    const-wide/16 v5, 0x1f4

    const v7, 0x5aab6061

    invoke-static {v3, v4, v5, v6, v7}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    goto :goto_0
.end method
