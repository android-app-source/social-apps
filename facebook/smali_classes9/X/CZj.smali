.class public LX/CZj;
.super Lcom/facebook/resources/ui/FbTextView;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1916511
    invoke-direct {p0, p1}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;)V

    .line 1916512
    invoke-virtual {p0}, LX/CZj;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b004e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    invoke-virtual {p0}, LX/CZj;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->scaledDensity:F

    div-float/2addr v0, v1

    .line 1916513
    const/4 v1, 0x2

    invoke-virtual {p0, v1, v0}, LX/CZj;->setTextSize(IF)V

    .line 1916514
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/CZj;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 1916515
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/CZj;->setIncludeFontPadding(Z)V

    .line 1916516
    const/16 v0, 0x10

    invoke-virtual {p0, v0}, LX/CZj;->setGravity(I)V

    .line 1916517
    invoke-virtual {p0}, LX/CZj;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0159

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, LX/CZj;->setHighlightColor(I)V

    .line 1916518
    return-void
.end method
