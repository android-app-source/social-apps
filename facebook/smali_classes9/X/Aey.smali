.class public final LX/Aey;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/Aeu;

.field public final synthetic b:LX/AeU;

.field public final synthetic c:LX/Aez;


# direct methods
.method public constructor <init>(LX/Aez;LX/Aeu;LX/AeU;)V
    .locals 0

    .prologue
    .line 1698468
    iput-object p1, p0, LX/Aey;->c:LX/Aez;

    iput-object p2, p0, LX/Aey;->a:LX/Aeu;

    iput-object p3, p0, LX/Aey;->b:LX/AeU;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x2

    const v2, -0x23d92a35

    invoke-static {v1, v0, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1698469
    iget-object v2, p0, LX/Aey;->c:LX/Aez;

    iget-object v3, p0, LX/Aey;->a:LX/Aeu;

    iget-boolean v3, v3, LX/Aeu;->g:Z

    if-nez v3, :cond_1

    :goto_0
    invoke-virtual {v2, v0}, LX/Aez;->b(Z)V

    .line 1698470
    iget-object v0, p0, LX/Aey;->c:LX/Aez;

    iget-object v0, v0, LX/Aez;->t:LX/Aev;

    iget-object v2, p0, LX/Aey;->a:LX/Aeu;

    invoke-virtual {v0, v2}, LX/Aev;->a(LX/Aeu;)V

    .line 1698471
    iget-object v0, p0, LX/Aey;->c:LX/Aez;

    iget-object v0, v0, LX/Aez;->r:LX/1b4;

    invoke-virtual {v0}, LX/1b4;->D()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Aey;->b:LX/AeU;

    iget-boolean v0, v0, LX/AeU;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Aey;->a:LX/Aeu;

    iget-boolean v0, v0, LX/Aeu;->g:Z

    if-eqz v0, :cond_0

    .line 1698472
    iget-object v0, p0, LX/Aey;->c:LX/Aez;

    iget-object v0, v0, LX/Aez;->t:LX/Aev;

    iget-object v2, p0, LX/Aey;->c:LX/Aez;

    iget-object v2, v2, LX/1a1;->a:Landroid/view/View;

    .line 1698473
    iget-object v3, v0, LX/Aev;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object p0, LX/Aev;->a:LX/0Tn;

    const/4 p1, 0x0

    invoke-interface {v3, p0, p1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1698474
    :cond_0
    :goto_1
    const v0, -0x34b6e480

    invoke-static {v0, v1}, LX/02F;->a(II)V

    return-void

    .line 1698475
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1698476
    :cond_2
    iget-object v3, v0, LX/Aev;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v3

    sget-object p0, LX/Aev;->a:LX/0Tn;

    const/4 p1, 0x1

    invoke-interface {v3, p0, p1}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v3

    invoke-interface {v3}, LX/0hN;->commit()V

    .line 1698477
    new-instance v3, LX/0hs;

    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p0

    const/4 p1, 0x2

    invoke-direct {v3, p0, p1}, LX/0hs;-><init>(Landroid/content/Context;I)V

    iput-object v3, v0, LX/Aev;->f:LX/0hs;

    .line 1698478
    iget-object v3, v0, LX/Aev;->f:LX/0hs;

    const/16 p0, 0x1770

    .line 1698479
    iput p0, v3, LX/0hs;->t:I

    .line 1698480
    iget-object v3, v0, LX/Aev;->c:LX/0wM;

    invoke-virtual {v2}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    const p1, 0x7f020995

    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    const/4 p1, -0x1

    invoke-virtual {v3, p0, p1}, LX/0wM;->a(Landroid/graphics/drawable/Drawable;I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 1698481
    iget-object p0, v0, LX/Aev;->f:LX/0hs;

    invoke-virtual {p0, v3}, LX/0hs;->b(Landroid/graphics/drawable/Drawable;)V

    .line 1698482
    iget-object v3, v0, LX/Aev;->f:LX/0hs;

    const p0, 0x7f080c47

    invoke-virtual {v3, p0}, LX/0hs;->a(I)V

    .line 1698483
    iget-object v3, v0, LX/Aev;->f:LX/0hs;

    const p0, 0x7f080c4c

    invoke-virtual {v3, p0}, LX/0hs;->b(I)V

    .line 1698484
    iget-object v3, v0, LX/Aev;->f:LX/0hs;

    invoke-virtual {v3, v2}, LX/0ht;->c(Landroid/view/View;)V

    .line 1698485
    iget-object v3, v0, LX/Aev;->f:LX/0hs;

    invoke-virtual {v3}, LX/0ht;->d()V

    goto :goto_1
.end method
