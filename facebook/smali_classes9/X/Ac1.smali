.class public LX/Ac1;
.super Landroid/graphics/drawable/ShapeDrawable;
.source ""


# direct methods
.method public varargs constructor <init>([I)V
    .locals 4

    .prologue
    .line 1692048
    new-instance v0, Landroid/graphics/drawable/shapes/RectShape;

    invoke-direct {v0}, Landroid/graphics/drawable/shapes/RectShape;-><init>()V

    invoke-direct {p0, v0}, Landroid/graphics/drawable/ShapeDrawable;-><init>(Landroid/graphics/drawable/shapes/Shape;)V

    .line 1692049
    array-length v0, p1

    new-array v1, v0, [F

    .line 1692050
    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-ge v0, v2, :cond_0

    .line 1692051
    const/high16 v2, 0x3f800000    # 1.0f

    array-length v3, v1

    add-int/lit8 v3, v3, -0x1

    int-to-float v3, v3

    div-float/2addr v2, v3

    int-to-float v3, v0

    mul-float/2addr v2, v3

    aput v2, v1, v0

    .line 1692052
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1692053
    :cond_0
    new-instance v0, LX/Ac0;

    invoke-direct {v0, p1, v1}, LX/Ac0;-><init>([I[F)V

    move-object v0, v0

    .line 1692054
    invoke-virtual {p0, v0}, LX/Ac1;->setShaderFactory(Landroid/graphics/drawable/ShapeDrawable$ShaderFactory;)V

    .line 1692055
    return-void
.end method

.method public static a(Landroid/content/res/Resources;)LX/Ac1;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1692047
    new-instance v0, LX/Ac1;

    const/4 v1, 0x3

    new-array v1, v1, [I

    aput v3, v1, v3

    const/4 v2, 0x1

    aput v3, v1, v2

    const/4 v2, 0x2

    const v3, 0x7f0a037d

    invoke-virtual {p0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    aput v3, v1, v2

    invoke-direct {v0, v1}, LX/Ac1;-><init>([I)V

    return-object v0
.end method

.method public static b(Landroid/content/res/Resources;)LX/Ac1;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1692044
    new-instance v0, LX/Ac1;

    const/4 v1, 0x2

    new-array v1, v1, [I

    const v2, 0x7f0a037f

    invoke-virtual {p0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    aput v2, v1, v3

    const/4 v2, 0x1

    aput v3, v1, v2

    invoke-direct {v0, v1}, LX/Ac1;-><init>([I)V

    return-object v0
.end method


# virtual methods
.method public final a(F)V
    .locals 1

    .prologue
    .line 1692045
    const/high16 v0, 0x437f0000    # 255.0f

    mul-float/2addr v0, p1

    float-to-int v0, v0

    invoke-virtual {p0, v0}, LX/Ac1;->setAlpha(I)V

    .line 1692046
    return-void
.end method
