.class public LX/C6w;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/1vg;

.field public final b:LX/0Zb;


# direct methods
.method public constructor <init>(LX/1vg;LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1850697
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1850698
    iput-object p1, p0, LX/C6w;->a:LX/1vg;

    .line 1850699
    iput-object p2, p0, LX/C6w;->b:LX/0Zb;

    .line 1850700
    return-void
.end method

.method public static a(FII)I
    .locals 1

    .prologue
    .line 1850701
    float-to-int v0, p0

    sub-int v0, p1, v0

    .line 1850702
    invoke-static {v0, p2}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method public static a(LX/C6w;LX/1De;Ljava/lang/String;Z)LX/1Di;
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 1850703
    if-nez p3, :cond_0

    .line 1850704
    const/4 v0, 0x0

    .line 1850705
    :goto_0
    return-object v0

    .line 1850706
    :cond_0
    iget-object v0, p0, LX/C6w;->a:LX/1vg;

    invoke-virtual {v0, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v0

    const v1, -0xc4a668

    invoke-virtual {v0, v1}, LX/2xv;->i(I)LX/2xv;

    move-result-object v0

    const v1, 0x7f020737

    invoke-virtual {v0, v1}, LX/2xv;->h(I)LX/2xv;

    move-result-object v0

    invoke-virtual {v0}, LX/1n6;->b()LX/1dc;

    move-result-object v0

    .line 1850707
    iget-object v1, p0, LX/C6w;->a:LX/1vg;

    invoke-virtual {v1, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v1

    const v2, -0x423e37

    invoke-virtual {v1, v2}, LX/2xv;->i(I)LX/2xv;

    move-result-object v1

    const v2, 0x7f02081c

    invoke-virtual {v1, v2}, LX/2xv;->h(I)LX/2xv;

    move-result-object v1

    invoke-virtual {v1}, LX/1n6;->b()LX/1dc;

    move-result-object v1

    .line 1850708
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v7}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v5}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v2

    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a009a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-interface {v2, v3}, LX/1Dh;->W(I)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/8ym;->c(LX/1De;)LX/8yk;

    move-result-object v3

    const v4, 0x7f0b10bc

    invoke-virtual {v3, v4}, LX/8yk;->h(I)LX/8yk;

    move-result-object v3

    invoke-virtual {v3, p2}, LX/8yk;->a(Ljava/lang/CharSequence;)LX/8yk;

    move-result-object v3

    const v4, 0x7f0e073d

    invoke-virtual {v3, v4}, LX/8yk;->i(I)LX/8yk;

    move-result-object v3

    invoke-virtual {v3, v0}, LX/8yk;->a(LX/1dc;)LX/8yk;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    const v3, 0x7f0b10ae

    invoke-interface {v0, v5, v3}, LX/1Di;->c(II)LX/1Di;

    move-result-object v0

    invoke-interface {v2, v0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/1o5;->a(LX/1dc;)LX/1o5;

    move-result-object v1

    sget-object v2, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v1, v2}, LX/1o5;->a(Landroid/widget/ImageView$ScaleType;)LX/1o5;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    const v2, 0x7f081a56

    invoke-interface {v1, v2}, LX/1Di;->A(I)LX/1Di;

    move-result-object v1

    const v2, 0x7f0b10af

    invoke-interface {v1, v2}, LX/1Di;->q(I)LX/1Di;

    move-result-object v1

    const v2, 0x7f0b10af

    invoke-interface {v1, v2}, LX/1Di;->i(I)LX/1Di;

    move-result-object v1

    invoke-interface {v1, v5}, LX/1Di;->c(I)LX/1Di;

    move-result-object v1

    invoke-interface {v1, v5, v6}, LX/1Di;->k(II)LX/1Di;

    move-result-object v1

    invoke-interface {v1, v7, v6}, LX/1Di;->k(II)LX/1Di;

    move-result-object v1

    .line 1850709
    const v2, 0x59104e1e

    const/4 v3, 0x0

    invoke-static {p1, v2, v3}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v2

    move-object v2, v2

    .line 1850710
    invoke-interface {v1, v2}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public static a(LX/0QB;)LX/C6w;
    .locals 5

    .prologue
    .line 1850711
    const-class v1, LX/C6w;

    monitor-enter v1

    .line 1850712
    :try_start_0
    sget-object v0, LX/C6w;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1850713
    sput-object v2, LX/C6w;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1850714
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1850715
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1850716
    new-instance p0, LX/C6w;

    invoke-static {v0}, LX/1vg;->a(LX/0QB;)LX/1vg;

    move-result-object v3

    check-cast v3, LX/1vg;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    invoke-direct {p0, v3, v4}, LX/C6w;-><init>(LX/1vg;LX/0Zb;)V

    .line 1850717
    move-object v0, p0

    .line 1850718
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1850719
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C6w;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1850720
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1850721
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
