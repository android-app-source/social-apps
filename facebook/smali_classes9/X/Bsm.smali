.class public LX/Bsm;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pb;",
        ":",
        "LX/1Pc;",
        ":",
        "LX/1Pd;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:LX/1VD;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1VD",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1VD;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1828198
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1828199
    iput-object p1, p0, LX/Bsm;->a:LX/1VD;

    .line 1828200
    return-void
.end method

.method public static a(LX/0QB;)LX/Bsm;
    .locals 4

    .prologue
    .line 1828201
    const-class v1, LX/Bsm;

    monitor-enter v1

    .line 1828202
    :try_start_0
    sget-object v0, LX/Bsm;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1828203
    sput-object v2, LX/Bsm;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1828204
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1828205
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1828206
    new-instance p0, LX/Bsm;

    invoke-static {v0}, LX/1VD;->a(LX/0QB;)LX/1VD;

    move-result-object v3

    check-cast v3, LX/1VD;

    invoke-direct {p0, v3}, LX/Bsm;-><init>(LX/1VD;)V

    .line 1828207
    move-object v0, p0

    .line 1828208
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1828209
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Bsm;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1828210
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1828211
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
