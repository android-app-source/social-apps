.class public final LX/BLO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field public final synthetic a:Landroid/widget/TextView;

.field public final synthetic b:Landroid/view/ViewTreeObserver;

.field public final synthetic c:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;Landroid/widget/TextView;Landroid/view/ViewTreeObserver;)V
    .locals 0

    .prologue
    .line 1775467
    iput-object p1, p0, LX/BLO;->c:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;

    iput-object p2, p0, LX/BLO;->a:Landroid/widget/TextView;

    iput-object p3, p0, LX/BLO;->b:Landroid/view/ViewTreeObserver;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onGlobalLayout()V
    .locals 3

    .prologue
    .line 1775468
    iget-object v0, p0, LX/BLO;->c:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;

    iget-object v1, p0, LX/BLO;->a:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v1

    iget-object v2, p0, LX/BLO;->a:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->a$redex0(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;Landroid/text/Layout;Ljava/lang/CharSequence;)V

    .line 1775469
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    .line 1775470
    iget-object v0, p0, LX/BLO;->b:Landroid/view/ViewTreeObserver;

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 1775471
    :goto_0
    return-void

    .line 1775472
    :cond_0
    iget-object v0, p0, LX/BLO;->b:Landroid/view/ViewTreeObserver;

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    goto :goto_0
.end method
