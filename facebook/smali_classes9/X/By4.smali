.class public LX/By4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Bni;


# instance fields
.field public a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

.field public b:Lcom/facebook/graphql/model/GraphQLEvent;

.field public c:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lcom/facebook/graphql/model/GraphQLStory;

.field public e:LX/0kx;

.field public f:LX/0bH;

.field public g:LX/189;

.field public h:LX/0tX;

.field private i:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public j:LX/1Sl;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLEvent;LX/0kx;LX/0bH;LX/189;LX/0tX;LX/1Ck;LX/1Sl;)V
    .locals 1
    .param p1    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/graphql/model/GraphQLEvent;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLEvent;",
            "LX/0kx;",
            "LX/0bH;",
            "LX/189;",
            "LX/0tX;",
            "LX/1Ck;",
            "LX/1Sl;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1836624
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1836625
    iput-object p1, p0, LX/By4;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1836626
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1836627
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    iput-object v0, p0, LX/By4;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1836628
    iput-object p2, p0, LX/By4;->b:Lcom/facebook/graphql/model/GraphQLEvent;

    .line 1836629
    invoke-static {p1}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    iput-object v0, p0, LX/By4;->d:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1836630
    iput-object p3, p0, LX/By4;->e:LX/0kx;

    .line 1836631
    iput-object p4, p0, LX/By4;->f:LX/0bH;

    .line 1836632
    iput-object p5, p0, LX/By4;->g:LX/189;

    .line 1836633
    iput-object p6, p0, LX/By4;->h:LX/0tX;

    .line 1836634
    iput-object p7, p0, LX/By4;->i:LX/1Ck;

    .line 1836635
    iput-object p8, p0, LX/By4;->j:LX/1Sl;

    .line 1836636
    return-void
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1836703
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "event-rsvp-"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a$redex0(LX/By4;)V
    .locals 4

    .prologue
    .line 1836704
    iget-object v0, p0, LX/By4;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/By4;->d:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/By4;->d:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1836705
    :cond_0
    :goto_0
    return-void

    .line 1836706
    :cond_1
    iget-object v0, p0, LX/By4;->f:LX/0bH;

    new-instance v1, LX/1Zg;

    iget-object v2, p0, LX/By4;->d:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;->EVENT_VIEW_PERMALINK:Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;

    invoke-direct {v1, v2, v3}, LX/1Zg;-><init>(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLFollowUpFeedUnitActionType;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)V
    .locals 4

    .prologue
    .line 1836672
    iget-object v0, p0, LX/By4;->j:LX/1Sl;

    invoke-virtual {v0}, LX/1Sl;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1836673
    iget-object v0, p0, LX/By4;->g:LX/189;

    iget-object v1, p0, LX/By4;->b:Lcom/facebook/graphql/model/GraphQLEvent;

    iget-object v2, p0, LX/By4;->d:Lcom/facebook/graphql/model/GraphQLStory;

    iget-object v3, p0, LX/By4;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0, v1, v2, v3, p2}, LX/189;->a(Lcom/facebook/graphql/model/GraphQLEvent;Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 1836674
    iget-object v1, p0, LX/By4;->f:LX/0bH;

    new-instance v2, LX/1Ne;

    invoke-direct {v2, v0}, LX/1Ne;-><init>(Lcom/facebook/graphql/model/FeedUnit;)V

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b7;)V

    .line 1836675
    :cond_0
    new-instance v0, LX/4EG;

    invoke-direct {v0}, LX/4EG;-><init>()V

    .line 1836676
    iget-object v1, p0, LX/By4;->e:LX/0kx;

    const-string v2, "native_newsfeed"

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0kx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4EG;->a(Ljava/lang/String;)LX/4EG;

    .line 1836677
    sget-object v1, Lcom/facebook/events/common/ActionMechanism;->FEED_ATTACHMENT:Lcom/facebook/events/common/ActionMechanism;

    invoke-virtual {v1}, Lcom/facebook/events/common/ActionMechanism;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4EG;->b(Ljava/lang/String;)LX/4EG;

    .line 1836678
    new-instance v1, LX/4EL;

    invoke-direct {v1}, LX/4EL;-><init>()V

    .line 1836679
    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/4EL;->a(Ljava/util/List;)LX/4EL;

    .line 1836680
    new-instance v0, LX/7ug;

    invoke-direct {v0}, LX/7ug;-><init>()V

    iget-object v2, p0, LX/By4;->b:Lcom/facebook/graphql/model/GraphQLEvent;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLEvent;->ax()Ljava/lang/String;

    move-result-object v2

    .line 1836681
    iput-object v2, v0, LX/7ug;->e:Ljava/lang/String;

    .line 1836682
    move-object v0, v0

    .line 1836683
    const/4 v2, 0x0

    .line 1836684
    iput-boolean v2, v0, LX/7ug;->g:Z

    .line 1836685
    move-object v0, v0

    .line 1836686
    iput-object p2, v0, LX/7ug;->f:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 1836687
    move-object v0, v0

    .line 1836688
    invoke-virtual {v0}, LX/7ug;->a()Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;

    move-result-object v2

    .line 1836689
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-ne p2, v0, :cond_1

    .line 1836690
    const-string v0, "going"

    .line 1836691
    :goto_0
    new-instance v3, LX/4ES;

    invoke-direct {v3}, LX/4ES;-><init>()V

    invoke-virtual {v3, v1}, LX/4ES;->a(LX/4EL;)LX/4ES;

    move-result-object v1

    iget-object v3, p0, LX/By4;->b:Lcom/facebook/graphql/model/GraphQLEvent;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLEvent;->ax()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, LX/4ES;->b(Ljava/lang/String;)LX/4ES;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/4ES;->c(Ljava/lang/String;)LX/4ES;

    move-result-object v0

    .line 1836692
    iget-object v1, p0, LX/By4;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v1}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v1

    invoke-virtual {v1}, LX/162;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4ES;->a(Ljava/util/List;)LX/4ES;

    .line 1836693
    invoke-static {}, LX/7uR;->e()LX/7uI;

    move-result-object v1

    .line 1836694
    const-string v3, "input"

    invoke-virtual {v1, v3, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1836695
    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/399;->a(LX/0jT;)LX/399;

    move-result-object v0

    .line 1836696
    iget-object v1, p0, LX/By4;->h:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    move-object v0, v0

    .line 1836697
    new-instance v1, LX/By2;

    invoke-direct {v1, p0, p2, p1}, LX/By2;-><init>(LX/By4;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)V

    move-object v1, v1

    .line 1836698
    iget-object v2, p0, LX/By4;->i:LX/1Ck;

    iget-object v3, p0, LX/By4;->b:Lcom/facebook/graphql/model/GraphQLEvent;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLEvent;->ax()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/By4;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v0, v1}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1836699
    return-void

    .line 1836700
    :cond_1
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->MAYBE:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-ne p2, v0, :cond_2

    .line 1836701
    const-string v0, "maybe"

    goto :goto_0

    .line 1836702
    :cond_2
    const-string v0, "not_going"

    goto :goto_0
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)V
    .locals 4

    .prologue
    .line 1836637
    iget-object v0, p0, LX/By4;->j:LX/1Sl;

    invoke-virtual {v0}, LX/1Sl;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1836638
    iget-object v0, p0, LX/By4;->g:LX/189;

    iget-object v1, p0, LX/By4;->b:Lcom/facebook/graphql/model/GraphQLEvent;

    iget-object v2, p0, LX/By4;->d:Lcom/facebook/graphql/model/GraphQLStory;

    iget-object v3, p0, LX/By4;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0, v1, v2, v3, p2}, LX/189;->a(Lcom/facebook/graphql/model/GraphQLEvent;Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/model/GraphQLStoryAttachment;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 1836639
    iget-object v1, p0, LX/By4;->f:LX/0bH;

    new-instance v2, LX/1Ne;

    invoke-direct {v2, v0}, LX/1Ne;-><init>(Lcom/facebook/graphql/model/FeedUnit;)V

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b7;)V

    .line 1836640
    :cond_0
    new-instance v1, LX/4EG;

    invoke-direct {v1}, LX/4EG;-><init>()V

    .line 1836641
    iget-object v0, p0, LX/By4;->e:LX/0kx;

    invoke-virtual {v0}, LX/0kx;->b()Ljava/lang/String;

    move-result-object v0

    .line 1836642
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, LX/4EG;->a(Ljava/lang/String;)LX/4EG;

    .line 1836643
    sget-object v0, Lcom/facebook/events/common/ActionMechanism;->FEED_ATTACHMENT:Lcom/facebook/events/common/ActionMechanism;

    invoke-virtual {v0}, Lcom/facebook/events/common/ActionMechanism;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/4EG;->b(Ljava/lang/String;)LX/4EG;

    .line 1836644
    new-instance v2, LX/4EL;

    invoke-direct {v2}, LX/4EL;-><init>()V

    .line 1836645
    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/4EL;->a(Ljava/util/List;)LX/4EL;

    .line 1836646
    new-instance v0, LX/7ug;

    invoke-direct {v0}, LX/7ug;-><init>()V

    iget-object v1, p0, LX/By4;->b:Lcom/facebook/graphql/model/GraphQLEvent;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEvent;->ax()Ljava/lang/String;

    move-result-object v1

    .line 1836647
    iput-object v1, v0, LX/7ug;->e:Ljava/lang/String;

    .line 1836648
    move-object v0, v0

    .line 1836649
    const/4 v1, 0x0

    .line 1836650
    iput-boolean v1, v0, LX/7ug;->g:Z

    .line 1836651
    move-object v0, v0

    .line 1836652
    iput-object p2, v0, LX/7ug;->h:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 1836653
    move-object v0, v0

    .line 1836654
    invoke-virtual {v0}, LX/7ug;->a()Lcom/facebook/events/graphql/EventsMutationsModels$OptimisticRsvpEventModel;

    move-result-object v1

    .line 1836655
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    if-ne p2, v0, :cond_2

    .line 1836656
    const-string v0, "GOING"

    .line 1836657
    :goto_1
    new-instance v3, LX/4Ed;

    invoke-direct {v3}, LX/4Ed;-><init>()V

    invoke-virtual {v3, v2}, LX/4Ed;->a(LX/4EL;)LX/4Ed;

    move-result-object v2

    iget-object v3, p0, LX/By4;->b:Lcom/facebook/graphql/model/GraphQLEvent;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLEvent;->ax()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/4Ed;->b(Ljava/lang/String;)LX/4Ed;

    move-result-object v2

    invoke-virtual {v2, v0}, LX/4Ed;->c(Ljava/lang/String;)LX/4Ed;

    move-result-object v0

    .line 1836658
    iget-object v2, p0, LX/By4;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v2}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v2

    invoke-virtual {v2}, LX/162;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/4Ed;->a(Ljava/util/List;)LX/4Ed;

    .line 1836659
    invoke-static {}, LX/7uR;->d()LX/7uQ;

    move-result-object v2

    .line 1836660
    const-string v3, "input"

    invoke-virtual {v2, v3, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1836661
    invoke-static {v2}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/399;->a(LX/0jT;)LX/399;

    move-result-object v0

    .line 1836662
    iget-object v1, p0, LX/By4;->h:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    move-object v0, v0

    .line 1836663
    new-instance v1, LX/By3;

    invoke-direct {v1, p0, p1, p2}, LX/By3;-><init>(LX/By4;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)V

    move-object v1, v1

    .line 1836664
    iget-object v2, p0, LX/By4;->i:LX/1Ck;

    iget-object v3, p0, LX/By4;->b:Lcom/facebook/graphql/model/GraphQLEvent;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLEvent;->ax()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/By4;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v0, v1}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1836665
    return-void

    .line 1836666
    :cond_1
    const-string v0, "native_newsfeed"

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 1836667
    :cond_2
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->WATCHED:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    if-ne p2, v0, :cond_3

    .line 1836668
    const-string v0, "WATCHED"

    goto :goto_1

    .line 1836669
    :cond_3
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->DECLINED:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    if-ne p2, v0, :cond_4

    .line 1836670
    const-string v0, "DECLINED"

    goto :goto_1

    .line 1836671
    :cond_4
    const-string v0, "UNWATCHED"

    goto :goto_1
.end method
