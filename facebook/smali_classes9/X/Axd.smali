.class public LX/Axd;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0Px;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/0Px",
            "<",
            "Lcom/facebook/friendsharing/souvenirs/layout/template/SouvenirTemplateEnum;",
            ">;>;"
        }
    .end annotation
.end field

.field private static final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/friendsharing/souvenirs/layout/template/SouvenirTemplateEnum;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    .line 1728734
    sget-object v0, Lcom/facebook/friendsharing/souvenirs/layout/template/SouvenirTemplateEnum;->TRIPLE_ITEMS_FIRST_PORTRAIT:Lcom/facebook/friendsharing/souvenirs/layout/template/SouvenirTemplateEnum;

    sget-object v1, Lcom/facebook/friendsharing/souvenirs/layout/template/SouvenirTemplateEnum;->TRIPLE_ITEMS_THIRD_PORTRAIT:Lcom/facebook/friendsharing/souvenirs/layout/template/SouvenirTemplateEnum;

    sget-object v2, Lcom/facebook/friendsharing/souvenirs/layout/template/SouvenirTemplateEnum;->TWO_SQUARE_ITEMS:Lcom/facebook/friendsharing/souvenirs/layout/template/SouvenirTemplateEnum;

    sget-object v3, Lcom/facebook/friendsharing/souvenirs/layout/template/SouvenirTemplateEnum;->SINGLE_ITEM:Lcom/facebook/friendsharing/souvenirs/layout/template/SouvenirTemplateEnum;

    invoke-static {v0, v1, v2, v3}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/Axd;->b:LX/0Px;

    .line 1728735
    sget-object v0, Lcom/facebook/friendsharing/souvenirs/layout/template/SouvenirTemplateEnum;->SINGLE_ITEM:Lcom/facebook/friendsharing/souvenirs/layout/template/SouvenirTemplateEnum;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sget-object v1, Lcom/facebook/friendsharing/souvenirs/layout/template/SouvenirTemplateEnum;->THREE_SQUARE_ITEMS:Lcom/facebook/friendsharing/souvenirs/layout/template/SouvenirTemplateEnum;

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    sget-object v2, Lcom/facebook/friendsharing/souvenirs/layout/template/SouvenirTemplateEnum;->SINGLE_ITEM:Lcom/facebook/friendsharing/souvenirs/layout/template/SouvenirTemplateEnum;

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    sget-object v3, Lcom/facebook/friendsharing/souvenirs/layout/template/SouvenirTemplateEnum;->TWO_SQUARE_ITEMS:Lcom/facebook/friendsharing/souvenirs/layout/template/SouvenirTemplateEnum;

    invoke-static {v3}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    sget-object v4, Lcom/facebook/friendsharing/souvenirs/layout/template/SouvenirTemplateEnum;->TRIPLE_ITEMS_FIRST_PORTRAIT:Lcom/facebook/friendsharing/souvenirs/layout/template/SouvenirTemplateEnum;

    sget-object v5, Lcom/facebook/friendsharing/souvenirs/layout/template/SouvenirTemplateEnum;->TRIPLE_ITEMS_THIRD_PORTRAIT:Lcom/facebook/friendsharing/souvenirs/layout/template/SouvenirTemplateEnum;

    invoke-static {v4, v5}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v4

    sget-object v5, Lcom/facebook/friendsharing/souvenirs/layout/template/SouvenirTemplateEnum;->SINGLE_ITEM:Lcom/facebook/friendsharing/souvenirs/layout/template/SouvenirTemplateEnum;

    invoke-static {v5}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v5

    sget-object v6, Lcom/facebook/friendsharing/souvenirs/layout/template/SouvenirTemplateEnum;->TWO_SQUARE_ITEMS:Lcom/facebook/friendsharing/souvenirs/layout/template/SouvenirTemplateEnum;

    invoke-static {v6}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v6

    invoke-static/range {v0 .. v6}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/Axd;->a:LX/0Px;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1728736
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1728737
    return-void
.end method
