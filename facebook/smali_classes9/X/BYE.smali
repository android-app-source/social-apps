.class public LX/BYE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0hc;


# instance fields
.field public final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/0hc;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1795313
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1795314
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/BYE;->a:Ljava/util/ArrayList;

    .line 1795315
    return-void
.end method

.method public constructor <init>(LX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/0hc;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1795316
    invoke-direct {p0}, LX/BYE;-><init>()V

    .line 1795317
    iget-object v0, p0, LX/BYE;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 1795318
    return-void
.end method


# virtual methods
.method public final B_(I)V
    .locals 2

    .prologue
    .line 1795319
    iget-object v0, p0, LX/BYE;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0hc;

    .line 1795320
    invoke-interface {v0, p1}, LX/0hc;->B_(I)V

    goto :goto_0

    .line 1795321
    :cond_0
    return-void
.end method

.method public final a(IFI)V
    .locals 2

    .prologue
    .line 1795322
    iget-object v0, p0, LX/BYE;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0hc;

    .line 1795323
    invoke-interface {v0, p1, p2, p3}, LX/0hc;->a(IFI)V

    goto :goto_0

    .line 1795324
    :cond_0
    return-void
.end method

.method public final b(I)V
    .locals 2

    .prologue
    .line 1795325
    iget-object v0, p0, LX/BYE;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0hc;

    .line 1795326
    invoke-interface {v0, p1}, LX/0hc;->b(I)V

    goto :goto_0

    .line 1795327
    :cond_0
    return-void
.end method
