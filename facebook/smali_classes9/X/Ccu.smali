.class public final LX/Ccu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/util/ArrayList",
        "<",
        "Lcom/facebook/places/graphql/PlacesGraphQLInterfaces$CheckinPlace;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Ccv;


# direct methods
.method public constructor <init>(LX/Ccv;)V
    .locals 0

    .prologue
    .line 1921793
    iput-object p1, p0, LX/Ccu;->a:LX/Ccv;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 1921792
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 1921783
    check-cast p1, Ljava/util/ArrayList;

    .line 1921784
    iget-object v0, p0, LX/Ccu;->a:LX/Ccv;

    iget-object v0, v0, LX/Ccv;->d:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 1921785
    iget-object v0, p0, LX/Ccu;->a:LX/Ccv;

    invoke-static {p1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    invoke-static {v1}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v1

    .line 1921786
    iput-object v1, v0, LX/Ccv;->b:LX/0am;

    .line 1921787
    iget-object v0, p0, LX/Ccu;->a:LX/Ccv;

    iget-object v0, v0, LX/Ccv;->e:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ccy;

    .line 1921788
    iget-object p0, v0, LX/Ccy;->a:Lcom/facebook/places/create/citypicker/NewCityPickerFragment;

    .line 1921789
    invoke-virtual {p0}, Lcom/facebook/places/pickers/PlaceContentPickerFragment;->l()V

    .line 1921790
    goto :goto_0

    .line 1921791
    :cond_0
    return-void
.end method
