.class public LX/CIx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0jq;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1874973
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1874974
    return-void
.end method

.method public static a(LX/0QB;)LX/CIx;
    .locals 1

    .prologue
    .line 1874975
    new-instance v0, LX/CIx;

    invoke-direct {v0}, LX/CIx;-><init>()V

    .line 1874976
    move-object v0, v0

    .line 1874977
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Landroid/support/v4/app/Fragment;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1874978
    if-eqz p1, :cond_0

    const-string v0, "ReactRouteName"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "ReactURI"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1874979
    :cond_0
    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v0

    const-string v1, "MarketplaceHomeRoute"

    .line 1874980
    iput-object v1, v0, LX/98r;->b:Ljava/lang/String;

    .line 1874981
    move-object v0, v0

    .line 1874982
    const-string v1, "/marketplace_home"

    .line 1874983
    iput-object v1, v0, LX/98r;->a:Ljava/lang/String;

    .line 1874984
    move-object v0, v0

    .line 1874985
    const/4 v1, 0x1

    .line 1874986
    iput v1, v0, LX/98r;->h:I

    .line 1874987
    move-object v0, v0

    .line 1874988
    invoke-virtual {v0}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v0

    .line 1874989
    invoke-static {v0}, Lcom/facebook/marketplace/tab/fragment/MarketplaceHomeFragment;->c(Landroid/os/Bundle;)Lcom/facebook/marketplace/tab/fragment/MarketplaceHomeFragment;

    move-result-object v0

    move-object v0, v0

    .line 1874990
    :goto_0
    return-object v0

    .line 1874991
    :cond_1
    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v0

    const-string v1, "ReactRouteName"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1874992
    iput-object v1, v0, LX/98r;->b:Ljava/lang/String;

    .line 1874993
    move-object v0, v0

    .line 1874994
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "/"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "ReactURI"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1874995
    iput-object v1, v0, LX/98r;->a:Ljava/lang/String;

    .line 1874996
    move-object v0, v0

    .line 1874997
    iput v3, v0, LX/98r;->h:I

    .line 1874998
    move-object v0, v0

    .line 1874999
    const-string v1, "non_immersive"

    invoke-virtual {p1, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 1875000
    iput-boolean v1, v0, LX/98r;->m:Z

    .line 1875001
    move-object v0, v0

    .line 1875002
    invoke-virtual {v0}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v0

    .line 1875003
    invoke-static {v0}, Lcom/facebook/marketplace/tab/fragment/MarketplaceHomeFragment;->c(Landroid/os/Bundle;)Lcom/facebook/marketplace/tab/fragment/MarketplaceHomeFragment;

    move-result-object v0

    goto :goto_0
.end method
