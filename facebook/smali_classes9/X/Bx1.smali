.class public final LX/Bx1;
.super Landroid/os/Handler;
.source ""


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/Bx2;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/Bx2;)V
    .locals 1

    .prologue
    .line 1834550
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 1834551
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/Bx1;->a:Ljava/lang/ref/WeakReference;

    .line 1834552
    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 12

    .prologue
    .line 1834553
    iget-object v0, p0, LX/Bx1;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Bx2;

    .line 1834554
    if-nez v0, :cond_0

    .line 1834555
    :goto_0
    return-void

    .line 1834556
    :cond_0
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 1834557
    :pswitch_0
    iget v2, v0, LX/Bx2;->F:I

    iget v3, v0, LX/Bx2;->z:I

    sub-int/2addr v2, v3

    iput v2, v0, LX/Bx2;->F:I

    .line 1834558
    iget v2, v0, LX/Bx2;->F:I

    if-gtz v2, :cond_3

    .line 1834559
    invoke-static {v0}, LX/Bx2;->j(LX/Bx2;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, v0, LX/Bx2;->t:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-nez v2, :cond_1

    .line 1834560
    new-instance v2, LX/0hs;

    invoke-virtual {v0}, LX/Bx2;->getContext()Landroid/content/Context;

    move-result-object v3

    const/4 v4, 0x2

    invoke-direct {v2, v3, v4}, LX/0hs;-><init>(Landroid/content/Context;I)V

    .line 1834561
    iget-object v3, v0, LX/Bx2;->s:Lcom/facebook/widget/FbImageView;

    invoke-virtual {v2, v3}, LX/0ht;->c(Landroid/view/View;)V

    .line 1834562
    iget-object v3, v0, LX/Bx2;->a:LX/0iX;

    .line 1834563
    iget-boolean v4, v3, LX/0iX;->i:Z

    move v3, v4

    .line 1834564
    if-eqz v3, :cond_5

    .line 1834565
    iget-object v3, v0, LX/Bx2;->c:LX/0ad;

    sget-char v4, LX/1rJ;->D:C

    const v5, 0x7f0828a9

    invoke-virtual {v0}, LX/Bx2;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-interface {v3, v4, v5, v6}, LX/0ad;->a(CILandroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0hs;->a(Ljava/lang/CharSequence;)V

    .line 1834566
    iget-object v3, v0, LX/Bx2;->c:LX/0ad;

    sget-char v4, LX/1rJ;->B:C

    const v5, 0x7f0828aa

    invoke-virtual {v0}, LX/Bx2;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-interface {v3, v4, v5, v6}, LX/0ad;->a(CILandroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0hs;->b(Ljava/lang/CharSequence;)V

    .line 1834567
    :goto_1
    const/4 v3, -0x1

    .line 1834568
    iput v3, v2, LX/0hs;->t:I

    .line 1834569
    invoke-virtual {v2}, LX/0ht;->d()V

    .line 1834570
    iget-object v2, v0, LX/Bx2;->d:LX/0iZ;

    sget-object v3, LX/0ia;->SHOW_TOGGLE_NUX:LX/0ia;

    invoke-virtual {v2, v3}, LX/0iZ;->a(LX/0ia;)V

    .line 1834571
    iget-object v2, v0, LX/Bx2;->b:LX/3kp;

    sget-object v3, LX/Bx2;->o:LX/0Tn;

    invoke-virtual {v2, v3}, LX/3kp;->a(LX/0Tn;)V

    .line 1834572
    iget-object v2, v0, LX/Bx2;->b:LX/3kp;

    invoke-virtual {v2}, LX/3kp;->a()V

    .line 1834573
    :cond_1
    :goto_2
    goto :goto_0

    .line 1834574
    :pswitch_1
    const/4 v6, 0x1

    .line 1834575
    iget v2, v0, LX/Bx2;->G:I

    iget v3, v0, LX/Bx2;->z:I

    sub-int/2addr v2, v3

    iput v2, v0, LX/Bx2;->G:I

    .line 1834576
    iget v2, v0, LX/Bx2;->G:I

    if-gtz v2, :cond_6

    .line 1834577
    iput-boolean v6, v0, LX/Bx2;->Q:Z

    .line 1834578
    iget v2, v0, LX/Bx2;->N:I

    iput v2, v0, LX/Bx2;->P:I

    .line 1834579
    :goto_3
    goto/16 :goto_0

    .line 1834580
    :pswitch_2
    const/4 v7, 0x2

    .line 1834581
    iget-object v2, v0, LX/2oy;->k:Lcom/facebook/video/player/RichVideoPlayer;

    if-eqz v2, :cond_2

    iget v2, v0, LX/Bx2;->O:I

    iget-object v3, v0, LX/2oy;->k:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v3}, Lcom/facebook/video/player/RichVideoPlayer;->getCurrentPositionMs()I

    move-result v3

    if-ne v2, v3, :cond_8

    .line 1834582
    :cond_2
    iget-object v2, v0, LX/Bx2;->r:LX/Bx1;

    iget v3, v0, LX/Bx2;->z:I

    int-to-long v4, v3

    invoke-virtual {v2, v7, v4, v5}, LX/Bx1;->sendEmptyMessageDelayed(IJ)Z

    .line 1834583
    :goto_4
    goto/16 :goto_0

    .line 1834584
    :cond_3
    invoke-static {v0}, LX/Bx2;->E(LX/Bx2;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1834585
    iget v2, v0, LX/Bx2;->v:I

    iput v2, v0, LX/Bx2;->F:I

    .line 1834586
    :cond_4
    iget-object v2, v0, LX/Bx2;->r:LX/Bx1;

    const/4 v3, 0x0

    iget v4, v0, LX/Bx2;->z:I

    int-to-long v4, v4

    invoke-virtual {v2, v3, v4, v5}, LX/Bx1;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_2

    .line 1834587
    :cond_5
    iget-object v3, v0, LX/Bx2;->c:LX/0ad;

    sget-char v4, LX/1rJ;->C:C

    const v5, 0x7f0828a9

    invoke-virtual {v0}, LX/Bx2;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-interface {v3, v4, v5, v6}, LX/0ad;->a(CILandroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0hs;->a(Ljava/lang/CharSequence;)V

    .line 1834588
    iget-object v3, v0, LX/Bx2;->c:LX/0ad;

    sget-char v4, LX/1rJ;->A:C

    const v5, 0x7f0828ab

    invoke-virtual {v0}, LX/Bx2;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-interface {v3, v4, v5, v6}, LX/0ad;->a(CILandroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0hs;->b(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 1834589
    :cond_6
    invoke-static {v0}, LX/Bx2;->E(LX/Bx2;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 1834590
    iget v2, v0, LX/Bx2;->w:I

    iput v2, v0, LX/Bx2;->G:I

    .line 1834591
    :cond_7
    iget-object v2, v0, LX/Bx2;->r:LX/Bx1;

    iget v3, v0, LX/Bx2;->z:I

    int-to-long v4, v3

    invoke-virtual {v2, v6, v4, v5}, LX/Bx1;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_3

    .line 1834592
    :cond_8
    iget-object v2, v0, LX/2oy;->k:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v2}, Lcom/facebook/video/player/RichVideoPlayer;->getCurrentPositionMs()I

    move-result v2

    iput v2, v0, LX/Bx2;->O:I

    .line 1834593
    iget v2, v0, LX/Bx2;->N:I

    iget v3, v0, LX/Bx2;->z:I

    add-int/2addr v2, v3

    iput v2, v0, LX/Bx2;->N:I

    .line 1834594
    iget-object v2, v0, LX/Bx2;->a:LX/0iX;

    invoke-static {v0}, LX/Bx2;->getPlayerOrigin(LX/Bx2;)LX/04D;

    move-result-object v3

    iget-object v4, v0, LX/Bx2;->D:LX/2pa;

    invoke-virtual {v2, v3, v4}, LX/0iX;->a(LX/04D;LX/2pa;)Z

    move-result v2

    if-eqz v2, :cond_9

    iget-boolean v2, v0, LX/Bx2;->Q:Z

    if-eqz v2, :cond_9

    .line 1834595
    iget-object v3, v0, LX/2oy;->k:Lcom/facebook/video/player/RichVideoPlayer;

    iget-object v4, v0, LX/Bx2;->f:LX/0iY;

    iget-object v5, v0, LX/2oy;->k:Lcom/facebook/video/player/RichVideoPlayer;

    iget v2, v0, LX/Bx2;->N:I

    iget v6, v0, LX/Bx2;->P:I

    sub-int v6, v2, v6

    iget-boolean v2, v0, LX/Bx2;->L:Z

    if-eqz v2, :cond_f

    iget-object v2, v0, LX/Bx2;->f:LX/0iY;

    .line 1834596
    iget v8, v2, LX/0iY;->p:I

    move v2, v8

    .line 1834597
    :goto_5
    add-int/2addr v2, v6

    iget v6, v0, LX/Bx2;->R:I

    invoke-virtual {v4, v5, v2, v6}, LX/0iY;->a(Lcom/facebook/video/player/RichVideoPlayer;II)F

    move-result v2

    invoke-virtual {v3, v2}, Lcom/facebook/video/player/RichVideoPlayer;->setVolume(F)V

    .line 1834598
    :cond_9
    const/4 v2, 0x0

    .line 1834599
    iget-object v3, v0, LX/Bx2;->c:LX/0ad;

    sget-short v4, LX/1rJ;->e:S

    invoke-interface {v3, v4, v2}, LX/0ad;->a(SZ)Z

    move-result v3

    if-nez v3, :cond_12

    .line 1834600
    :cond_a
    :goto_6
    move v2, v2

    .line 1834601
    if-eqz v2, :cond_c

    .line 1834602
    const/4 v8, 0x1

    iput-boolean v8, v0, LX/Bx2;->K:Z

    .line 1834603
    iget-object v8, v0, LX/Bx2;->u:Lcom/facebook/resources/ui/FbTextView;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1834604
    iget-object v8, v0, LX/Bx2;->u:Lcom/facebook/resources/ui/FbTextView;

    invoke-static {v0}, LX/Bx2;->getLabelTextId(LX/Bx2;)I

    move-result v9

    invoke-virtual {v8, v9}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 1834605
    iget-object v8, v0, LX/Bx2;->u:Lcom/facebook/resources/ui/FbTextView;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/facebook/resources/ui/FbTextView;->setAlpha(F)V

    .line 1834606
    iget-object v8, v0, LX/Bx2;->u:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v8}, Lcom/facebook/resources/ui/FbTextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v8

    const/high16 v9, 0x3f800000    # 1.0f

    invoke-virtual {v8, v9}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v8

    .line 1834607
    invoke-virtual {v0}, LX/Bx2;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0b06b6

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    invoke-virtual {v0}, LX/Bx2;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0b06b1

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    add-int/2addr v9, v10

    invoke-virtual {v0}, LX/Bx2;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0b06b4

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    sub-int/2addr v9, v10

    iget-object v10, v0, LX/Bx2;->s:Lcom/facebook/widget/FbImageView;

    invoke-virtual {v10}, Lcom/facebook/widget/FbImageView;->getWidth()I

    move-result v10

    add-int/2addr v9, v10

    move v9, v9

    .line 1834608
    mul-int/lit8 v9, v9, -0x1

    int-to-float v9, v9

    invoke-virtual {v8, v9}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v8

    const-wide/16 v10, 0x12c

    invoke-virtual {v8, v10, v11}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v8

    invoke-virtual {v8}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 1834609
    iget-boolean v8, v0, LX/Bx2;->J:Z

    if-nez v8, :cond_b

    .line 1834610
    iget-object v8, v0, LX/Bx2;->f:LX/0iY;

    .line 1834611
    iget v9, v8, LX/0iY;->s:I

    add-int/lit8 v9, v9, -0x1

    iput v9, v8, LX/0iY;->s:I

    .line 1834612
    iget v9, v8, LX/0iY;->r:I

    add-int/lit8 v9, v9, -0x1

    iput v9, v8, LX/0iY;->r:I

    .line 1834613
    iget-object v9, v8, LX/0iY;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v10, v8, LX/0iY;->k:LX/0Tn;

    const/4 v11, 0x0

    invoke-interface {v9, v10, v11}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v9

    .line 1834614
    iget-object v10, v8, LX/0iY;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v10}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v10

    iget-object v11, v8, LX/0iY;->k:LX/0Tn;

    add-int/lit8 v9, v9, 0x1

    invoke-interface {v10, v11, v9}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v9

    invoke-interface {v9}, LX/0hN;->commit()V

    .line 1834615
    :cond_b
    iget-boolean v2, v0, LX/Bx2;->J:Z

    if-eqz v2, :cond_10

    .line 1834616
    iget-object v2, v0, LX/Bx2;->d:LX/0iZ;

    sget-object v3, LX/0ia;->VIDEO_HAS_NO_SOUND_SHOWN:LX/0ia;

    invoke-virtual {v2, v3}, LX/0iZ;->a(LX/0ia;)V

    .line 1834617
    :cond_c
    :goto_7
    const/4 v2, 0x0

    .line 1834618
    iget-object v3, v0, LX/Bx2;->c:LX/0ad;

    sget-short v4, LX/1rJ;->e:S

    invoke-interface {v3, v4, v2}, LX/0ad;->a(SZ)Z

    move-result v3

    if-nez v3, :cond_16

    .line 1834619
    :cond_d
    :goto_8
    move v2, v2

    .line 1834620
    if-eqz v2, :cond_e

    .line 1834621
    invoke-static {v0}, LX/Bx2;->D(LX/Bx2;)V

    .line 1834622
    :cond_e
    iget-object v2, v0, LX/Bx2;->r:LX/Bx1;

    iget v3, v0, LX/Bx2;->z:I

    int-to-long v4, v3

    invoke-virtual {v2, v7, v4, v5}, LX/Bx1;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_4

    .line 1834623
    :cond_f
    const/4 v2, 0x0

    goto/16 :goto_5

    .line 1834624
    :cond_10
    iget-object v2, v0, LX/Bx2;->a:LX/0iX;

    .line 1834625
    iget-boolean v3, v2, LX/0iX;->i:Z

    move v2, v3

    .line 1834626
    if-eqz v2, :cond_11

    .line 1834627
    iget-object v2, v0, LX/Bx2;->d:LX/0iZ;

    sget-object v3, LX/0ia;->TAP_TO_MUTE_SHOWN:LX/0ia;

    invoke-virtual {v2, v3}, LX/0iZ;->a(LX/0ia;)V

    goto :goto_7

    .line 1834628
    :cond_11
    iget-object v2, v0, LX/Bx2;->d:LX/0iZ;

    sget-object v3, LX/0ia;->TAP_TO_SOUND_SHOWN:LX/0ia;

    invoke-virtual {v2, v3}, LX/0iZ;->a(LX/0ia;)V

    goto :goto_7

    .line 1834629
    :cond_12
    iget-boolean v3, v0, LX/Bx2;->K:Z

    if-nez v3, :cond_a

    iget v3, v0, LX/Bx2;->N:I

    iget v4, v0, LX/Bx2;->A:I

    if-le v3, v4, :cond_a

    iget v3, v0, LX/Bx2;->N:I

    iget v4, v0, LX/Bx2;->B:I

    if-ge v3, v4, :cond_a

    iget-object v3, v0, LX/Bx2;->D:LX/2pa;

    .line 1834630
    if-eqz v3, :cond_14

    iget-object v4, v3, LX/2pa;->b:LX/0P1;

    if-eqz v4, :cond_14

    iget-object v4, v3, LX/2pa;->b:LX/0P1;

    const-string v5, "IsNotifVideoKey"

    invoke-virtual {v4, v5}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_14

    .line 1834631
    iget-object v4, v3, LX/2pa;->b:LX/0P1;

    const-string v5, "IsNotifVideoKey"

    invoke-virtual {v4, v5}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    .line 1834632
    :goto_9
    move-object v3, v4

    .line 1834633
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-nez v3, :cond_a

    .line 1834634
    iget-boolean v3, v0, LX/Bx2;->J:Z

    if-nez v3, :cond_13

    iget-object v3, v0, LX/Bx2;->f:LX/0iY;

    .line 1834635
    iget v4, v3, LX/0iY;->r:I

    if-lez v4, :cond_15

    iget v4, v3, LX/0iY;->s:I

    if-lez v4, :cond_15

    const/4 v4, 0x1

    :goto_a
    move v3, v4

    .line 1834636
    if-eqz v3, :cond_a

    :cond_13
    const/4 v2, 0x1

    goto/16 :goto_6

    :cond_14
    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    goto :goto_9

    :cond_15
    const/4 v4, 0x0

    goto :goto_a

    :cond_16
    iget-boolean v3, v0, LX/Bx2;->K:Z

    if-eqz v3, :cond_d

    iget v3, v0, LX/Bx2;->N:I

    iget v4, v0, LX/Bx2;->B:I

    if-le v3, v4, :cond_d

    const/4 v2, 0x1

    goto/16 :goto_8

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
