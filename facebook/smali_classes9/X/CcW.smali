.class public LX/CcW;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:LX/1WX;


# direct methods
.method public constructor <init>(LX/1WX;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1921274
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1921275
    iput-object p1, p0, LX/CcW;->a:LX/1WX;

    .line 1921276
    return-void
.end method

.method public static a(LX/0QB;)LX/CcW;
    .locals 4

    .prologue
    .line 1921277
    const-class v1, LX/CcW;

    monitor-enter v1

    .line 1921278
    :try_start_0
    sget-object v0, LX/CcW;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1921279
    sput-object v2, LX/CcW;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1921280
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1921281
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1921282
    new-instance p0, LX/CcW;

    invoke-static {v0}, LX/1WX;->a(LX/0QB;)LX/1WX;

    move-result-object v3

    check-cast v3, LX/1WX;

    invoke-direct {p0, v3}, LX/CcW;-><init>(LX/1WX;)V

    .line 1921283
    move-object v0, p0

    .line 1921284
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1921285
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/CcW;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1921286
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1921287
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
