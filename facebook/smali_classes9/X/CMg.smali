.class public final enum LX/CMg;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/CMg;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/CMg;

.field public static final enum DISABLE_ONLY:LX/CMg;

.field public static final enum ENABLE_AND_DISABLE:LX/CMg;

.field public static final enum NONE:LX/CMg;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1880870
    new-instance v0, LX/CMg;

    const-string v1, "ENABLE_AND_DISABLE"

    invoke-direct {v0, v1, v2}, LX/CMg;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CMg;->ENABLE_AND_DISABLE:LX/CMg;

    .line 1880871
    new-instance v0, LX/CMg;

    const-string v1, "DISABLE_ONLY"

    invoke-direct {v0, v1, v3}, LX/CMg;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CMg;->DISABLE_ONLY:LX/CMg;

    .line 1880872
    new-instance v0, LX/CMg;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v4}, LX/CMg;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CMg;->NONE:LX/CMg;

    .line 1880873
    const/4 v0, 0x3

    new-array v0, v0, [LX/CMg;

    sget-object v1, LX/CMg;->ENABLE_AND_DISABLE:LX/CMg;

    aput-object v1, v0, v2

    sget-object v1, LX/CMg;->DISABLE_ONLY:LX/CMg;

    aput-object v1, v0, v3

    sget-object v1, LX/CMg;->NONE:LX/CMg;

    aput-object v1, v0, v4

    sput-object v0, LX/CMg;->$VALUES:[LX/CMg;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1880875
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/CMg;
    .locals 1

    .prologue
    .line 1880876
    const-class v0, LX/CMg;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/CMg;

    return-object v0
.end method

.method public static values()[LX/CMg;
    .locals 1

    .prologue
    .line 1880874
    sget-object v0, LX/CMg;->$VALUES:[LX/CMg;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/CMg;

    return-object v0
.end method
