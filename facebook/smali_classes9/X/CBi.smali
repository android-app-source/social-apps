.class public LX/CBi;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field public final a:LX/CBZ;

.field public final b:I

.field public final c:I

.field public final d:LX/2mO;

.field public final e:LX/1DR;

.field public f:LX/1V0;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/3mL;LX/CBZ;LX/2mO;LX/1V0;LX/0hB;LX/1DR;LX/0Ot;LX/2mJ;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "LX/3mL;",
            "LX/CBZ;",
            "LX/2mO;",
            "LX/1V0;",
            "LX/0hB;",
            "LX/1DR;",
            "LX/0Ot",
            "<",
            "LX/13P;",
            ">;",
            "Lcom/facebook/quickpromotion/action/QuickPromotionActionHandler;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1856752
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1856753
    const v0, 0x7f0b1ce1

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/CBi;->b:I

    .line 1856754
    const v0, 0x7f0b1ce0

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/CBi;->c:I

    .line 1856755
    iput-object p3, p0, LX/CBi;->a:LX/CBZ;

    .line 1856756
    iput-object p4, p0, LX/CBi;->d:LX/2mO;

    .line 1856757
    iput-object p5, p0, LX/CBi;->f:LX/1V0;

    .line 1856758
    iput-object p7, p0, LX/CBi;->e:LX/1DR;

    .line 1856759
    return-void
.end method

.method public static a(LX/0QB;)LX/CBi;
    .locals 13

    .prologue
    .line 1856760
    const-class v1, LX/CBi;

    monitor-enter v1

    .line 1856761
    :try_start_0
    sget-object v0, LX/CBi;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1856762
    sput-object v2, LX/CBi;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1856763
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1856764
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1856765
    new-instance v3, LX/CBi;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v4

    check-cast v4, Landroid/content/res/Resources;

    invoke-static {v0}, LX/3mL;->a(LX/0QB;)LX/3mL;

    move-result-object v5

    check-cast v5, LX/3mL;

    invoke-static {v0}, LX/CBZ;->a(LX/0QB;)LX/CBZ;

    move-result-object v6

    check-cast v6, LX/CBZ;

    invoke-static {v0}, LX/2mO;->a(LX/0QB;)LX/2mO;

    move-result-object v7

    check-cast v7, LX/2mO;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v8

    check-cast v8, LX/1V0;

    invoke-static {v0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v9

    check-cast v9, LX/0hB;

    invoke-static {v0}, LX/1DR;->a(LX/0QB;)LX/1DR;

    move-result-object v10

    check-cast v10, LX/1DR;

    const/16 v11, 0x104c

    invoke-static {v0, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    invoke-static {v0}, LX/2mJ;->a(LX/0QB;)LX/2mJ;

    move-result-object v12

    check-cast v12, LX/2mJ;

    invoke-direct/range {v3 .. v12}, LX/CBi;-><init>(Landroid/content/res/Resources;LX/3mL;LX/CBZ;LX/2mO;LX/1V0;LX/0hB;LX/1DR;LX/0Ot;LX/2mJ;)V

    .line 1856766
    move-object v0, v3

    .line 1856767
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1856768
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/CBi;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1856769
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1856770
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
