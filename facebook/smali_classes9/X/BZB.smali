.class public LX/BZB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnoreProperties;
    b = true
.end annotation


# instance fields
.field private background_color:Ljava/lang/String;

.field private contents:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/BZC;",
            ">;"
        }
    .end annotation
.end field

.field private font_color:Ljava/lang/String;

.field private id:Ljava/lang/String;

.field private image_url:Ljava/lang/String;

.field private order_number:I

.field private title:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1796939
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1796940
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1796934
    if-ne p0, p1, :cond_1

    .line 1796935
    :cond_0
    :goto_0
    return v0

    .line 1796936
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    .line 1796937
    :cond_3
    check-cast p1, LX/BZB;

    .line 1796938
    iget-object v2, p0, LX/BZB;->title:Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v0, p0, LX/BZB;->title:Ljava/lang/String;

    iget-object v1, p1, LX/BZB;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    :cond_4
    iget-object v2, p1, LX/BZB;->title:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public getBackground_color()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1796933
    iget-object v0, p0, LX/BZB;->background_color:Ljava/lang/String;

    return-object v0
.end method

.method public getContents()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/BZC;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1796932
    iget-object v0, p0, LX/BZB;->contents:Ljava/util/List;

    return-object v0
.end method

.method public getFont_color()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1796919
    iget-object v0, p0, LX/BZB;->font_color:Ljava/lang/String;

    return-object v0
.end method

.method public getID()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1796931
    iget-object v0, p0, LX/BZB;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getImage_url()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1796930
    iget-object v0, p0, LX/BZB;->image_url:Ljava/lang/String;

    return-object v0
.end method

.method public getOrder_number()I
    .locals 1

    .prologue
    .line 1796929
    iget v0, p0, LX/BZB;->order_number:I

    return v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1796928
    iget-object v0, p0, LX/BZB;->title:Ljava/lang/String;

    return-object v0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1796941
    iget-object v0, p0, LX/BZB;->title:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/BZB;->title:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setBackground_color(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1796926
    iput-object p1, p0, LX/BZB;->background_color:Ljava/lang/String;

    .line 1796927
    return-void
.end method

.method public setContents(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/BZC;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1796924
    iput-object p1, p0, LX/BZB;->contents:Ljava/util/List;

    .line 1796925
    return-void
.end method

.method public setFont_color(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1796922
    iput-object p1, p0, LX/BZB;->font_color:Ljava/lang/String;

    .line 1796923
    return-void
.end method

.method public setID(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1796920
    iput-object p1, p0, LX/BZB;->id:Ljava/lang/String;

    .line 1796921
    return-void
.end method

.method public setImage_url(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1796917
    iput-object p1, p0, LX/BZB;->image_url:Ljava/lang/String;

    .line 1796918
    return-void
.end method

.method public setOrder_number(I)V
    .locals 0

    .prologue
    .line 1796915
    iput p1, p0, LX/BZB;->order_number:I

    .line 1796916
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1796913
    iput-object p1, p0, LX/BZB;->title:Ljava/lang/String;

    .line 1796914
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    const/16 v2, 0x27

    .line 1796912
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Category{section_id=\'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/BZB;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", title=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/BZB;->title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", contents="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/BZB;->contents:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
