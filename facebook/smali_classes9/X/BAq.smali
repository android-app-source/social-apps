.class public final LX/BAq;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 14

    .prologue
    .line 1754214
    const/4 v10, 0x0

    .line 1754215
    const/4 v9, 0x0

    .line 1754216
    const/4 v8, 0x0

    .line 1754217
    const/4 v7, 0x0

    .line 1754218
    const/4 v6, 0x0

    .line 1754219
    const/4 v5, 0x0

    .line 1754220
    const/4 v4, 0x0

    .line 1754221
    const/4 v3, 0x0

    .line 1754222
    const/4 v2, 0x0

    .line 1754223
    const/4 v1, 0x0

    .line 1754224
    const/4 v0, 0x0

    .line 1754225
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->START_OBJECT:LX/15z;

    if-eq v11, v12, :cond_1

    .line 1754226
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1754227
    const/4 v0, 0x0

    .line 1754228
    :goto_0
    return v0

    .line 1754229
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1754230
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_a

    .line 1754231
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 1754232
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1754233
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_1

    if-eqz v11, :cond_1

    .line 1754234
    const-string v12, "__type__"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_2

    const-string v12, "__typename"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 1754235
    :cond_2
    invoke-static {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v10

    invoke-virtual {p1, v10}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v10

    goto :goto_1

    .line 1754236
    :cond_3
    const-string v12, "bucket_type"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 1754237
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v9

    goto :goto_1

    .line 1754238
    :cond_4
    const-string v12, "icon"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 1754239
    invoke-static {p0, p1}, LX/BAo;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 1754240
    :cond_5
    const-string v12, "max_count"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 1754241
    const/4 v2, 0x1

    .line 1754242
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v7

    goto :goto_1

    .line 1754243
    :cond_6
    const-string v12, "max_impression_count"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 1754244
    const/4 v1, 0x1

    .line 1754245
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v6

    goto :goto_1

    .line 1754246
    :cond_7
    const-string v12, "min_to_expire"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_8

    .line 1754247
    const/4 v0, 0x1

    .line 1754248
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v5

    goto :goto_1

    .line 1754249
    :cond_8
    const-string v12, "seen_filter"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_9

    .line 1754250
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/graphql/enums/GraphQLNotificationSeenFilter;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLNotificationSeenFilter;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v4

    goto/16 :goto_1

    .line 1754251
    :cond_9
    const-string v12, "title"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 1754252
    invoke-static {p0, p1}, LX/BAp;->a(LX/15w;LX/186;)I

    move-result v3

    goto/16 :goto_1

    .line 1754253
    :cond_a
    const/16 v11, 0x8

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 1754254
    const/4 v11, 0x0

    invoke-virtual {p1, v11, v10}, LX/186;->b(II)V

    .line 1754255
    const/4 v10, 0x1

    invoke-virtual {p1, v10, v9}, LX/186;->b(II)V

    .line 1754256
    const/4 v9, 0x2

    invoke-virtual {p1, v9, v8}, LX/186;->b(II)V

    .line 1754257
    if-eqz v2, :cond_b

    .line 1754258
    const/4 v2, 0x3

    const/4 v8, 0x0

    invoke-virtual {p1, v2, v7, v8}, LX/186;->a(III)V

    .line 1754259
    :cond_b
    if-eqz v1, :cond_c

    .line 1754260
    const/4 v1, 0x4

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v6, v2}, LX/186;->a(III)V

    .line 1754261
    :cond_c
    if-eqz v0, :cond_d

    .line 1754262
    const/4 v0, 0x5

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v5, v1}, LX/186;->a(III)V

    .line 1754263
    :cond_d
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1754264
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1754265
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const/4 v3, 0x6

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1754179
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1754180
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1754181
    if-eqz v0, :cond_0

    .line 1754182
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1754183
    invoke-static {p0, p1, v2, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1754184
    :cond_0
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 1754185
    if-eqz v0, :cond_1

    .line 1754186
    const-string v0, "bucket_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1754187
    invoke-virtual {p0, p1, v1}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1754188
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1754189
    if-eqz v0, :cond_2

    .line 1754190
    const-string v1, "icon"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1754191
    invoke-static {p0, v0, p2}, LX/BAo;->a(LX/15i;ILX/0nX;)V

    .line 1754192
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1754193
    if-eqz v0, :cond_3

    .line 1754194
    const-string v1, "max_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1754195
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1754196
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1754197
    if-eqz v0, :cond_4

    .line 1754198
    const-string v1, "max_impression_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1754199
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1754200
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1754201
    if-eqz v0, :cond_5

    .line 1754202
    const-string v1, "min_to_expire"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1754203
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1754204
    :cond_5
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 1754205
    if-eqz v0, :cond_6

    .line 1754206
    const-string v0, "seen_filter"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1754207
    invoke-virtual {p0, p1, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1754208
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1754209
    if-eqz v0, :cond_7

    .line 1754210
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1754211
    invoke-static {p0, v0, p2}, LX/BAp;->a(LX/15i;ILX/0nX;)V

    .line 1754212
    :cond_7
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1754213
    return-void
.end method
