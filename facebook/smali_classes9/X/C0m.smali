.class public LX/C0m;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/C0k;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/C0n;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1841473
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/C0m;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/C0n;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1841470
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1841471
    iput-object p1, p0, LX/C0m;->b:LX/0Ot;

    .line 1841472
    return-void
.end method

.method public static a(LX/0QB;)LX/C0m;
    .locals 4

    .prologue
    .line 1841459
    const-class v1, LX/C0m;

    monitor-enter v1

    .line 1841460
    :try_start_0
    sget-object v0, LX/C0m;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1841461
    sput-object v2, LX/C0m;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1841462
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1841463
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1841464
    new-instance v3, LX/C0m;

    const/16 p0, 0x1e81

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/C0m;-><init>(LX/0Ot;)V

    .line 1841465
    move-object v0, v3

    .line 1841466
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1841467
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C0m;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1841468
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1841469
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1841458
    const v0, -0x6744177e

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 3

    .prologue
    .line 1841437
    check-cast p2, LX/C0l;

    .line 1841438
    iget-object v0, p0, LX/C0m;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v0, p2, LX/C0l;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 p2, 0x1

    .line 1841439
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object p0

    .line 1841440
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 1841441
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->fY()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v1

    const p0, 0x7f0a0162

    invoke-virtual {v1, p0}, LX/1ne;->n(I)LX/1ne;

    move-result-object v1

    const p0, 0x7f0b004e

    invoke-virtual {v1, p0}, LX/1ne;->q(I)LX/1ne;

    move-result-object v1

    invoke-virtual {v1, p2}, LX/1ne;->j(I)LX/1ne;

    move-result-object v1

    sget-object p0, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v1, p0}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v1

    invoke-virtual {v1, p2}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    const/16 p0, 0x8

    const p2, 0x7f0b00be

    invoke-interface {v1, p0, p2}, LX/1Di;->g(II)LX/1Di;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    invoke-interface {v2, v1}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v1

    .line 1841442
    const v2, -0x6744177e

    const/4 p0, 0x0

    invoke-static {p1, v2, p0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v2

    move-object v2, v2

    .line 1841443
    invoke-interface {v1, v2}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    move-object v0, v1

    .line 1841444
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1841445
    invoke-static {}, LX/1dS;->b()V

    .line 1841446
    iget v0, p1, LX/1dQ;->b:I

    .line 1841447
    packed-switch v0, :pswitch_data_0

    .line 1841448
    :goto_0
    return-object v2

    .line 1841449
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 1841450
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1841451
    check-cast v1, LX/C0l;

    .line 1841452
    iget-object v3, p0, LX/C0m;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/C0n;

    iget-object p1, v1, LX/C0l;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1841453
    iget-object p0, v3, LX/C0n;->a:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    .line 1841454
    iget-object p2, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object p2, p2

    .line 1841455
    check-cast p2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object p2

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLNode;->bG()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object p2

    invoke-interface {p0, p2}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(Lcom/facebook/graphql/model/GraphQLStory;)Landroid/content/Intent;

    move-result-object p2

    .line 1841456
    iget-object p0, v3, LX/C0n;->b:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {p0, p2, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1841457
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x6744177e
        :pswitch_0
    .end packed-switch
.end method
