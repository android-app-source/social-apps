.class public final enum LX/BQA;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/BQA;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/BQA;

.field public static final enum INVISIBLE:LX/BQA;

.field public static final enum VISIBLE:LX/BQA;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1781699
    new-instance v0, LX/BQA;

    const-string v1, "VISIBLE"

    invoke-direct {v0, v1, v2}, LX/BQA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BQA;->VISIBLE:LX/BQA;

    .line 1781700
    new-instance v0, LX/BQA;

    const-string v1, "INVISIBLE"

    invoke-direct {v0, v1, v3}, LX/BQA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BQA;->INVISIBLE:LX/BQA;

    .line 1781701
    const/4 v0, 0x2

    new-array v0, v0, [LX/BQA;

    sget-object v1, LX/BQA;->VISIBLE:LX/BQA;

    aput-object v1, v0, v2

    sget-object v1, LX/BQA;->INVISIBLE:LX/BQA;

    aput-object v1, v0, v3

    sput-object v0, LX/BQA;->$VALUES:[LX/BQA;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1781698
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/BQA;
    .locals 1

    .prologue
    .line 1781696
    const-class v0, LX/BQA;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/BQA;

    return-object v0
.end method

.method public static values()[LX/BQA;
    .locals 1

    .prologue
    .line 1781697
    sget-object v0, LX/BQA;->$VALUES:[LX/BQA;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/BQA;

    return-object v0
.end method
