.class public LX/AVc;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Tn;

.field public final b:LX/0Tn;

.field public final c:LX/0Tn;

.field public final d:LX/0Tn;

.field public e:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/0hs;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1680125
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1680126
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "audio_live/nux/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iput-object v0, p0, LX/AVc;->a:LX/0Tn;

    .line 1680127
    iget-object v0, p0, LX/AVc;->a:LX/0Tn;

    const-string v1, "picker"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iput-object v0, p0, LX/AVc;->b:LX/0Tn;

    .line 1680128
    iget-object v0, p0, LX/AVc;->a:LX/0Tn;

    const-string v1, "toggle"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iput-object v0, p0, LX/AVc;->c:LX/0Tn;

    .line 1680129
    iget-object v0, p0, LX/AVc;->a:LX/0Tn;

    const-string v1, "start"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    iput-object v0, p0, LX/AVc;->d:LX/0Tn;

    .line 1680130
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1680131
    iput-object v0, p0, LX/AVc;->f:LX/0Ot;

    .line 1680132
    return-void
.end method

.method public static a(LX/0QB;)LX/AVc;
    .locals 1

    .prologue
    .line 1680133
    invoke-static {p0}, LX/AVc;->b(LX/0QB;)LX/AVc;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/AVc;
    .locals 3

    .prologue
    .line 1680134
    new-instance v1, LX/AVc;

    invoke-direct {v1}, LX/AVc;-><init>()V

    .line 1680135
    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const-class v2, Landroid/content/Context;

    invoke-interface {p0, v2}, LX/0QC;->getLazy(Ljava/lang/Class;)LX/0Ot;

    move-result-object v2

    .line 1680136
    iput-object v0, v1, LX/AVc;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object v2, v1, LX/AVc;->f:LX/0Ot;

    .line 1680137
    return-object v1
.end method
