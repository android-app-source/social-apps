.class public LX/AsR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0iK;
.implements LX/AsA;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "LX/0io;",
        ":",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingDataSpec$ProvidesInspirationLoggingData;",
        ":",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationStateSpec$ProvidesInspirationState;",
        ":",
        "LX/0is;",
        "DerivedData:",
        "Ljava/lang/Object;",
        "Mutation::",
        "Lcom/facebook/ipc/composer/dataaccessor/ComposerCanSave;",
        ":",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingDataSpec$SetsInspirationLoggingData",
        "<TMutation;>;:",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationStateSpec$SetsInspirationState",
        "<TMutation;>;:",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModelSpec$SetsInspirationSwipeableModel",
        "<TMutation;>;Services::",
        "LX/0ik",
        "<TDerivedData;>;:",
        "LX/0il",
        "<TModelData;>;:",
        "LX/0im",
        "<TMutation;>;>",
        "Ljava/lang/Object;",
        "LX/0iK",
        "<TModelData;TDerivedData;>;",
        "LX/AsA;"
    }
.end annotation


# static fields
.field public static final a:LX/0jK;


# instance fields
.field public final b:Landroid/content/Context;

.field public final c:LX/Ase;

.field public final d:Lcom/facebook/friendsharing/inspiration/controller/EffectsBottomTrayAdapter;

.field public final e:LX/0tS;

.field public final f:LX/AuP;

.field public final g:LX/87V;

.field public final h:LX/ArT;

.field public final i:Landroid/view/ViewStub;

.field public j:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field public k:LX/1P1;

.field public l:Z

.field public m:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TServices;>;"
        }
    .end annotation
.end field

.field public n:I

.field public o:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1720518
    const-class v0, LX/AsR;

    invoke-static {v0}, LX/0jK;->a(Ljava/lang/Class;)LX/0jK;

    move-result-object v0

    sput-object v0, LX/AsR;->a:LX/0jK;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;LX/0ik;LX/ArT;LX/ArL;Landroid/view/ViewStub;Landroid/content/Context;LX/Asf;LX/AsP;LX/0tS;LX/AuQ;LX/87V;)V
    .locals 1
    .param p1    # Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/0ik;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/ArT;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/ArL;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Landroid/view/ViewStub;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;",
            "TServices;",
            "LX/ArT;",
            "LX/ArL;",
            "Landroid/view/ViewStub;",
            "Landroid/content/Context;",
            "LX/Asf;",
            "LX/AsP;",
            "LX/0tS;",
            "LX/AuQ;",
            "LX/87V;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1720519
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1720520
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/AsR;->m:Ljava/lang/ref/WeakReference;

    .line 1720521
    iput-object p3, p0, LX/AsR;->h:LX/ArT;

    .line 1720522
    iput-object p5, p0, LX/AsR;->i:Landroid/view/ViewStub;

    .line 1720523
    iput-object p6, p0, LX/AsR;->b:Landroid/content/Context;

    .line 1720524
    invoke-virtual {p8, p4}, LX/AsP;->a(LX/ArL;)Lcom/facebook/friendsharing/inspiration/controller/EffectsBottomTrayAdapter;

    move-result-object v0

    iput-object v0, p0, LX/AsR;->d:Lcom/facebook/friendsharing/inspiration/controller/EffectsBottomTrayAdapter;

    .line 1720525
    iget-object v0, p0, LX/AsR;->m:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p7, p0, p1, v0}, LX/Asf;->a(LX/AsA;Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;Ljava/lang/Object;)LX/Ase;

    move-result-object v0

    iput-object v0, p0, LX/AsR;->c:LX/Ase;

    .line 1720526
    iput-object p9, p0, LX/AsR;->e:LX/0tS;

    .line 1720527
    const v0, 0x7f0b1a81

    invoke-virtual {p10, v0}, LX/AuQ;->a(I)LX/AuP;

    move-result-object v0

    iput-object v0, p0, LX/AsR;->f:LX/AuP;

    .line 1720528
    iput-object p11, p0, LX/AsR;->g:LX/87V;

    .line 1720529
    return-void
.end method

.method public static b(LX/AsR;LX/0io;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TModelData;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 1720530
    iget-object v0, p0, LX/AsR;->m:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ik;

    .line 1720531
    iget-object v2, p0, LX/AsR;->g:LX/87V;

    move-object v1, v0

    check-cast v1, LX/0il;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0is;

    invoke-virtual {v2, v1}, LX/87V;->d(LX/0is;)LX/0Px;

    move-result-object v2

    .line 1720532
    iget-object v4, p0, LX/AsR;->g:LX/87V;

    move-object v1, p1

    check-cast v1, LX/0is;

    invoke-virtual {v4, v1}, LX/87V;->d(LX/0is;)LX/0Px;

    move-result-object v4

    move-object v1, v0

    .line 1720533
    check-cast v1, LX/0il;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, LX/0is;

    invoke-interface {v1}, LX/0is;->o()Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->areEffectsLoading()Z

    move-result v5

    move-object v1, p1

    .line 1720534
    check-cast v1, LX/0is;

    invoke-interface {v1}, LX/0is;->o()Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->areEffectsLoading()Z

    move-result v1

    .line 1720535
    check-cast v0, LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    invoke-direct {p0, v0}, LX/AsR;->c(LX/0io;)I

    move-result v6

    .line 1720536
    invoke-direct {p0, p1}, LX/AsR;->c(LX/0io;)I

    move-result v0

    .line 1720537
    const/4 v9, 0x0

    .line 1720538
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v7

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v8

    if-eq v7, v8, :cond_b

    .line 1720539
    :cond_0
    :goto_0
    move v4, v9

    .line 1720540
    if-eqz v4, :cond_1

    if-ne v5, v1, :cond_1

    if-eq v6, v0, :cond_8

    :cond_1
    const/4 v0, 0x1

    .line 1720541
    :goto_1
    if-nez v0, :cond_2

    iget-boolean v0, p0, LX/AsR;->l:Z

    if-nez v0, :cond_5

    .line 1720542
    :cond_2
    iget-object v1, p0, LX/AsR;->d:Lcom/facebook/friendsharing/inspiration/controller/EffectsBottomTrayAdapter;

    if-eqz v5, :cond_9

    .line 1720543
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1720544
    const/4 v0, 0x0

    :goto_2
    const/4 v4, 0x7

    if-ge v0, v4, :cond_3

    .line 1720545
    invoke-static {}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->newBuilder()Lcom/facebook/friendsharing/inspiration/model/InspirationModel$Builder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/InspirationModel;

    move-result-object v4

    invoke-virtual {v2, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1720546
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1720547
    :cond_3
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    move-object v0, v0

    .line 1720548
    :goto_3
    iput-object v0, v1, Lcom/facebook/friendsharing/inspiration/controller/EffectsBottomTrayAdapter;->d:Ljava/util/List;

    .line 1720549
    iget v2, v1, Lcom/facebook/friendsharing/inspiration/controller/EffectsBottomTrayAdapter;->l:I

    if-lez v2, :cond_4

    .line 1720550
    iget-object v2, v1, Lcom/facebook/friendsharing/inspiration/controller/EffectsBottomTrayAdapter;->h:LX/As2;

    iget v4, v1, Lcom/facebook/friendsharing/inspiration/controller/EffectsBottomTrayAdapter;->l:I

    iget v5, v1, Lcom/facebook/friendsharing/inspiration/controller/EffectsBottomTrayAdapter;->m:I

    invoke-virtual {v2, v4, v5}, LX/As2;->a(II)V

    .line 1720551
    :cond_4
    iget-object v0, p0, LX/AsR;->d:Lcom/facebook/friendsharing/inspiration/controller/EffectsBottomTrayAdapter;

    .line 1720552
    iput v6, v0, Lcom/facebook/friendsharing/inspiration/controller/EffectsBottomTrayAdapter;->k:I

    .line 1720553
    iget-object v0, p0, LX/AsR;->d:Lcom/facebook/friendsharing/inspiration/controller/EffectsBottomTrayAdapter;

    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 1720554
    :cond_5
    iget-boolean v0, p0, LX/AsR;->l:Z

    if-eqz v0, :cond_6

    invoke-direct {p0, p1}, LX/AsR;->c(LX/0io;)I

    move-result v0

    if-eq v6, v0, :cond_7

    .line 1720555
    :cond_6
    iget-boolean v0, p0, LX/AsR;->o:Z

    if-eqz v0, :cond_a

    .line 1720556
    iget-object v0, p0, LX/AsR;->j:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget v1, p0, LX/AsR;->n:I

    invoke-virtual {v0, v1, v3}, Landroid/support/v7/widget/RecyclerView;->a(II)V

    .line 1720557
    iput v3, p0, LX/AsR;->n:I

    .line 1720558
    :cond_7
    :goto_4
    iput-boolean v3, p0, LX/AsR;->o:Z

    .line 1720559
    return-void

    :cond_8
    move v0, v3

    .line 1720560
    goto :goto_1

    :cond_9
    move-object v0, v2

    .line 1720561
    goto :goto_3

    .line 1720562
    :cond_a
    iget-object v0, p0, LX/AsR;->k:LX/1P1;

    if-eqz v0, :cond_7

    .line 1720563
    iget-object v0, p0, LX/AsR;->j:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    new-instance v1, Lcom/facebook/friendsharing/inspiration/controller/EffectsBottomTrayController$2;

    invoke-direct {v1, p0, v6}, Lcom/facebook/friendsharing/inspiration/controller/EffectsBottomTrayController$2;-><init>(LX/AsR;I)V

    invoke-static {v0, v1}, LX/Awl;->a(Landroid/view/View;Ljava/lang/Runnable;)V

    goto :goto_4

    :cond_b
    move v8, v9

    .line 1720564
    :goto_5
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v7

    if-ge v8, v7, :cond_c

    .line 1720565
    invoke-virtual {v2, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;

    invoke-virtual {v7}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->getId()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;

    invoke-virtual {v7}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->getId()Ljava/lang/String;

    move-result-object v7

    invoke-static {v10, v7}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-virtual {v2, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;

    invoke-virtual {v7}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->getThumbnailUri()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;

    invoke-virtual {v7}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->getThumbnailUri()Ljava/lang/String;

    move-result-object v7

    invoke-static {v10, v7}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 1720566
    add-int/lit8 v7, v8, 0x1

    move v8, v7

    goto :goto_5

    .line 1720567
    :cond_c
    const/4 v9, 0x1

    goto/16 :goto_0
.end method

.method private c(LX/0io;)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TModelData;)I"
        }
    .end annotation

    .prologue
    .line 1720568
    iget-object v1, p0, LX/AsR;->g:LX/87V;

    move-object v0, p1

    check-cast v0, LX/0is;

    invoke-virtual {v1, v0}, LX/87V;->d(LX/0is;)LX/0Px;

    move-result-object v0

    check-cast p1, LX/0is;

    invoke-interface {p1}, LX/0is;->o()Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->getSelectedId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/87Q;->b(LX/0Px;Ljava/lang/String;)I

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(LX/5L2;)V
    .locals 0

    .prologue
    .line 1720569
    return-void
.end method

.method public final a(LX/ArJ;)V
    .locals 11

    .prologue
    .line 1720570
    iget-object v0, p0, LX/AsR;->h:LX/ArT;

    .line 1720571
    iget-object v2, v0, LX/ArT;->b:LX/ArL;

    iget-object v1, v0, LX/ArT;->a:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v3

    iget-object v1, v0, LX/ArT;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0il;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->l()Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->getEffectsTraySessionStartTime()J

    move-result-wide v5

    sub-long/2addr v3, v5

    long-to-float v1, v3

    const/high16 v3, 0x447a0000    # 1000.0f

    div-float/2addr v1, v3

    .line 1720572
    sget-object v7, LX/ArH;->END_EFFECTS_TRAY_SESSION:LX/ArH;

    invoke-static {v2, v7, p1}, LX/ArL;->a(LX/ArL;LX/5oU;LX/ArJ;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const/4 v8, 0x0

    invoke-static {v2, v8}, LX/ArL;->a(LX/ArL;Z)Ljava/util/Map;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    sget-object v8, LX/ArI;->DURATION:LX/ArI;

    invoke-virtual {v8}, LX/ArI;->getName()Ljava/lang/String;

    move-result-object v8

    float-to-double v9, v1

    invoke-virtual {v7, v8, v9, v10}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    invoke-static {v2, v7}, LX/ArL;->a(LX/ArL;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1720573
    iget-object v1, v0, LX/ArT;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0il;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->l()Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;)Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->setEffectsTraySessionId(Ljava/lang/String;)Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    move-result-object v1

    invoke-static {v0, v1}, LX/ArT;->a(LX/ArT;Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;)V

    .line 1720574
    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 1720575
    check-cast p1, LX/0io;

    .line 1720576
    iget-object v0, p0, LX/AsR;->m:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ik;

    move-object v1, v0

    .line 1720577
    check-cast v1, LX/0il;

    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0io;

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v2

    move-object v1, p1

    .line 1720578
    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v1

    .line 1720579
    iget-boolean v3, p0, LX/AsR;->l:Z

    if-eqz v3, :cond_0

    .line 1720580
    invoke-static {p0, p1}, LX/AsR;->b(LX/AsR;LX/0io;)V

    .line 1720581
    :cond_0
    invoke-virtual {v2}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->getOpenBottomTray()LX/86o;

    move-result-object v3

    .line 1720582
    invoke-virtual {p0}, LX/AsR;->b()LX/86o;

    move-result-object v4

    if-ne v3, v4, :cond_1

    invoke-virtual {v3}, LX/86o;->shouldCloseOnBackPressed()Z

    move-result v4

    if-eqz v4, :cond_1

    sget-object v4, LX/86t;->TRAY:LX/86t;

    invoke-static {v1, v2, v4}, LX/87R;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationState;Lcom/facebook/friendsharing/inspiration/model/InspirationState;LX/86t;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1720583
    invoke-static {v2}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationState;)Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;

    move-result-object v4

    .line 1720584
    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->setIsBackPressed(Z)Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;

    .line 1720585
    invoke-static {v4, v3}, LX/87N;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;LX/86o;)Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;

    move-result-object v3

    .line 1720586
    check-cast v0, LX/0im;

    invoke-interface {v0}, LX/0im;->c()LX/0jJ;

    move-result-object v0

    sget-object v4, LX/AsR;->a:LX/0jK;

    invoke-virtual {v0, v4}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v0

    check-cast v0, LX/0jL;

    invoke-virtual {v3}, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/0jL;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationState;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jL;

    invoke-virtual {v0}, LX/0jL;->a()V

    .line 1720587
    sget-object v0, LX/ArJ;->TAP_BACK_BUTTON:LX/ArJ;

    invoke-virtual {p0, v0}, LX/AsR;->a(LX/ArJ;)V

    .line 1720588
    :cond_1
    invoke-virtual {p0}, LX/AsR;->b()LX/86o;

    move-result-object v0

    invoke-static {v1, v2, v0}, LX/87N;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationState;Lcom/facebook/friendsharing/inspiration/model/InspirationState;LX/86o;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1720589
    iget-boolean v0, p0, LX/AsR;->l:Z

    if-nez v0, :cond_2

    .line 1720590
    const/4 v3, 0x0

    .line 1720591
    iget-object v0, p0, LX/AsR;->c:LX/Ase;

    iget-object v1, p0, LX/AsR;->i:Landroid/view/ViewStub;

    const v2, 0x7f030685

    invoke-virtual {v0, v1, v2}, LX/Ase;->a(Landroid/view/ViewStub;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iput-object v0, p0, LX/AsR;->j:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 1720592
    new-instance v0, LX/1P0;

    iget-object v1, p0, LX/AsR;->b:Landroid/content/Context;

    invoke-direct {v0, v1, v3, v3}, LX/1P0;-><init>(Landroid/content/Context;IZ)V

    iput-object v0, p0, LX/AsR;->k:LX/1P1;

    .line 1720593
    iget-object v0, p0, LX/AsR;->j:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v1, p0, LX/AsR;->k:LX/1P1;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 1720594
    iget-object v0, p0, LX/AsR;->j:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v1, p0, LX/AsR;->f:LX/AuP;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(LX/3x6;)V

    .line 1720595
    iget-object v0, p0, LX/AsR;->d:Lcom/facebook/friendsharing/inspiration/controller/EffectsBottomTrayAdapter;

    new-instance v1, LX/AsQ;

    invoke-direct {v1, p0}, LX/AsQ;-><init>(LX/AsR;)V

    .line 1720596
    iput-object v1, v0, Lcom/facebook/friendsharing/inspiration/controller/EffectsBottomTrayAdapter;->j:LX/AsQ;

    .line 1720597
    iget-object v0, p0, LX/AsR;->j:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v1, p0, LX/AsR;->d:Lcom/facebook/friendsharing/inspiration/controller/EffectsBottomTrayAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 1720598
    iget-object v0, p0, LX/AsR;->e:LX/0tS;

    invoke-virtual {v0}, LX/0tS;->b()I

    move-result v0

    .line 1720599
    invoke-static {v0}, LX/0tS;->a(I)I

    move-result v1

    .line 1720600
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v3, -0x1

    invoke-direct {v2, v3, v0}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1720601
    iget-object v3, p0, LX/AsR;->b:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b1a81

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, v2, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    .line 1720602
    iget-object v3, p0, LX/AsR;->j:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v3, v2}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1720603
    iget-object v2, p0, LX/AsR;->d:Lcom/facebook/friendsharing/inspiration/controller/EffectsBottomTrayAdapter;

    .line 1720604
    if-lez v0, :cond_6

    const/4 v3, 0x1

    :goto_0
    invoke-static {v3}, LX/0PB;->checkArgument(Z)V

    .line 1720605
    iput v1, v2, Lcom/facebook/friendsharing/inspiration/controller/EffectsBottomTrayAdapter;->l:I

    .line 1720606
    iput v0, v2, Lcom/facebook/friendsharing/inspiration/controller/EffectsBottomTrayAdapter;->m:I

    .line 1720607
    iget-object v3, v2, Lcom/facebook/friendsharing/inspiration/controller/EffectsBottomTrayAdapter;->h:LX/As2;

    iget v4, v2, Lcom/facebook/friendsharing/inspiration/controller/EffectsBottomTrayAdapter;->l:I

    iget v5, v2, Lcom/facebook/friendsharing/inspiration/controller/EffectsBottomTrayAdapter;->m:I

    invoke-virtual {v3, v4, v5}, LX/As2;->a(II)V

    .line 1720608
    iget-object v1, p0, LX/AsR;->j:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v1}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1720609
    iget-object v0, p0, LX/AsR;->j:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v0}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->requestLayout()V

    .line 1720610
    invoke-static {p0, p1}, LX/AsR;->b(LX/AsR;LX/0io;)V

    .line 1720611
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/AsR;->l:Z

    .line 1720612
    :cond_2
    iget-object v0, p0, LX/AsR;->c:LX/Ase;

    invoke-virtual {v0}, LX/Ase;->b()V

    .line 1720613
    :cond_3
    :goto_1
    check-cast p1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    iget-object v0, p0, LX/AsR;->m:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ik;

    check-cast v0, LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-static {p1, v0}, LX/87R;->c(Lcom/facebook/composer/system/model/ComposerModelImpl;Lcom/facebook/composer/system/model/ComposerModelImpl;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1720614
    iget-object v1, p0, LX/AsR;->d:Lcom/facebook/friendsharing/inspiration/controller/EffectsBottomTrayAdapter;

    iget-object v0, p0, LX/AsR;->m:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ik;

    check-cast v0, LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->isLocationFetchAndRequeryingEffectsInProgress()Z

    move-result v0

    .line 1720615
    iput-boolean v0, v1, Lcom/facebook/friendsharing/inspiration/controller/EffectsBottomTrayAdapter;->n:Z

    .line 1720616
    :cond_4
    return-void

    .line 1720617
    :cond_5
    invoke-virtual {p0}, LX/AsR;->b()LX/86o;

    move-result-object v0

    invoke-static {v1, v2, v0}, LX/87N;->b(Lcom/facebook/friendsharing/inspiration/model/InspirationState;Lcom/facebook/friendsharing/inspiration/model/InspirationState;LX/86o;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1720618
    iget-object v1, p0, LX/AsR;->c:LX/Ase;

    invoke-virtual {v1}, LX/Ase;->c()V

    .line 1720619
    goto :goto_1

    .line 1720620
    :cond_6
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public final b()LX/86o;
    .locals 1

    .prologue
    .line 1720621
    sget-object v0, LX/86o;->EFFECTS:LX/86o;

    return-object v0
.end method
