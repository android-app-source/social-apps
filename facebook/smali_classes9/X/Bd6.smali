.class public final LX/Bd6;
.super LX/BcN;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/BcN",
        "<",
        "LX/BdA;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/Bd7;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/BdA",
            "<TTModel;>.GraphQ",
            "LRootQuerySectionImpl;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/BdA;

.field private c:[Ljava/lang/String;

.field private d:I

.field public e:Ljava/util/BitSet;


# direct methods
.method public constructor <init>(LX/BdA;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1803253
    iput-object p1, p0, LX/Bd6;->b:LX/BdA;

    invoke-direct {p0}, LX/BcN;-><init>()V

    .line 1803254
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "config"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/Bd6;->c:[Ljava/lang/String;

    .line 1803255
    iput v3, p0, LX/Bd6;->d:I

    .line 1803256
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/Bd6;->d:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/Bd6;->e:Ljava/util/BitSet;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1803257
    invoke-super {p0}, LX/BcN;->a()V

    .line 1803258
    const/4 v0, 0x0

    iput-object v0, p0, LX/Bd6;->a:LX/Bd7;

    .line 1803259
    iget-object v0, p0, LX/Bd6;->b:LX/BdA;

    iget-object v0, v0, LX/BdA;->b:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 1803260
    return-void
.end method

.method public final b()LX/BcO;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/BcO",
            "<",
            "LX/BdA;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1803261
    iget-object v1, p0, LX/Bd6;->e:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/Bd6;->e:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/Bd6;->d:I

    if-ge v1, v2, :cond_2

    .line 1803262
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1803263
    :goto_0
    iget v2, p0, LX/Bd6;->d:I

    if-ge v0, v2, :cond_1

    .line 1803264
    iget-object v2, p0, LX/Bd6;->e:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1803265
    iget-object v2, p0, LX/Bd6;->c:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1803266
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1803267
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1803268
    :cond_2
    iget-object v0, p0, LX/Bd6;->a:LX/Bd7;

    .line 1803269
    invoke-virtual {p0}, LX/Bd6;->a()V

    .line 1803270
    return-object v0
.end method
