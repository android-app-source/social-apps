.class public final LX/AVa;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$FetchCoverPhotoQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0zO;

.field public final synthetic b:Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;


# direct methods
.method public constructor <init>(Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;LX/0zO;)V
    .locals 0

    .prologue
    .line 1680049
    iput-object p1, p0, LX/AVa;->b:Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;

    iput-object p2, p0, LX/AVa;->a:LX/0zO;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1680050
    iget-object v0, p0, LX/AVa;->b:Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;

    const/4 v1, 0x1

    .line 1680051
    iput-boolean v1, v0, Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;->n:Z

    .line 1680052
    iget-object v0, p0, LX/AVa;->b:Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;

    iget-object v0, v0, Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;->f:LX/AVT;

    const-string v1, "cover photo url download fail"

    invoke-virtual {v0, v1}, LX/AVT;->g(Ljava/lang/String;)V

    .line 1680053
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1680054
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1680055
    iget-object v0, p0, LX/AVa;->a:LX/0zO;

    if-eqz v0, :cond_2

    .line 1680056
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1680057
    if-eqz v0, :cond_2

    .line 1680058
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1680059
    check-cast v0, Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$FetchCoverPhotoQueryModel;

    invoke-virtual {v0}, Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$FetchCoverPhotoQueryModel;->a()Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$FetchCoverPhotoQueryModel$ActorModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1680060
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1680061
    check-cast v0, Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$FetchCoverPhotoQueryModel;

    invoke-virtual {v0}, Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$FetchCoverPhotoQueryModel;->a()Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$FetchCoverPhotoQueryModel$ActorModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$FetchCoverPhotoQueryModel$ActorModel;->a()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 1680062
    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    if-eqz v0, :cond_4

    .line 1680063
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1680064
    check-cast v0, Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$FetchCoverPhotoQueryModel;

    invoke-virtual {v0}, Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$FetchCoverPhotoQueryModel;->a()Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$FetchCoverPhotoQueryModel$ActorModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$FetchCoverPhotoQueryModel$ActorModel;->a()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 1680065
    invoke-virtual {v3, v0, v2}, LX/15i;->g(II)I

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_1
    if-eqz v0, :cond_6

    .line 1680066
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1680067
    check-cast v0, Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$FetchCoverPhotoQueryModel;

    invoke-virtual {v0}, Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$FetchCoverPhotoQueryModel;->a()Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$FetchCoverPhotoQueryModel$ActorModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$FetchCoverPhotoQueryModel$ActorModel;->a()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 1680068
    invoke-virtual {v3, v0, v2}, LX/15i;->g(II)I

    move-result v0

    invoke-virtual {v3, v0, v2}, LX/15i;->g(II)I

    move-result v0

    if-eqz v0, :cond_5

    move v0, v1

    :goto_2
    if-eqz v0, :cond_7

    .line 1680069
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1680070
    check-cast v0, Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$FetchCoverPhotoQueryModel;

    invoke-virtual {v0}, Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$FetchCoverPhotoQueryModel;->a()Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$FetchCoverPhotoQueryModel$ActorModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$FetchCoverPhotoQueryModel$ActorModel;->a()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v3, v0, v2}, LX/15i;->g(II)I

    move-result v0

    invoke-virtual {v3, v0, v2}, LX/15i;->g(II)I

    move-result v0

    sget-object v2, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iget-object v4, p0, LX/AVa;->b:Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;

    .line 1680071
    iput-object v3, v4, Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;->g:LX/15i;

    .line 1680072
    iget-object v3, p0, LX/AVa;->b:Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;

    .line 1680073
    iput v0, v3, Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;->h:I

    .line 1680074
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1680075
    sget-object v2, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_1
    iget-object v0, p0, LX/AVa;->b:Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;

    iget-object v0, v0, Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;->g:LX/15i;

    iget-object v3, p0, LX/AVa;->b:Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;

    iget v3, v3, Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;->h:I

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    invoke-virtual {v0, v3, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 1680076
    sget-object v2, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_2
    iget-object v0, p0, LX/AVa;->b:Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;

    iget-object v0, v0, Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;->g:LX/15i;

    iget-object v3, p0, LX/AVa;->b:Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;

    iget v3, v3, Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;->h:I

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    iget-object v2, p0, LX/AVa;->b:Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;

    invoke-virtual {v0, v3, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    .line 1680077
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 1680078
    if-nez v1, :cond_8

    .line 1680079
    :cond_0
    :goto_3
    return-void

    :cond_1
    move v0, v2

    .line 1680080
    goto/16 :goto_0

    :cond_2
    move v0, v2

    goto/16 :goto_0

    :cond_3
    move v0, v2

    goto :goto_1

    :cond_4
    move v0, v2

    goto :goto_1

    :cond_5
    move v0, v2

    goto :goto_2

    :cond_6
    move v0, v2

    goto :goto_2

    .line 1680081
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    .line 1680082
    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0

    .line 1680083
    :catchall_2
    move-exception v0

    :try_start_5
    monitor-exit v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v0

    .line 1680084
    :cond_7
    iget-object v0, p0, LX/AVa;->b:Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;

    .line 1680085
    iput-boolean v1, v0, Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;->n:Z

    .line 1680086
    goto :goto_3

    .line 1680087
    :cond_8
    invoke-static {v1}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v1

    new-instance v3, LX/BJ2;

    invoke-direct {v3}, LX/BJ2;-><init>()V

    .line 1680088
    iput-object v3, v1, LX/1bX;->j:LX/33B;

    .line 1680089
    move-object v1, v1

    .line 1680090
    invoke-virtual {v1}, LX/1bX;->n()LX/1bf;

    move-result-object v1

    .line 1680091
    if-eqz v1, :cond_0

    .line 1680092
    iget-object v3, v2, Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;->d:LX/1HI;

    sget-object v4, Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v3, v1, v4}, LX/1HI;->b(LX/1bf;Ljava/lang/Object;)LX/1ca;

    move-result-object v1

    .line 1680093
    new-instance v3, LX/AVb;

    invoke-direct {v3, v2}, LX/AVb;-><init>(Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;)V

    iget-object v4, v2, Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;->c:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v1, v3, v4}, LX/1ca;->a(LX/1cj;Ljava/util/concurrent/Executor;)V

    goto :goto_3
.end method
