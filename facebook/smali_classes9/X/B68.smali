.class public final enum LX/B68;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/B68;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/B68;

.field public static final enum COMPOSER_INLINE_SPROUT:LX/B68;

.field public static final enum STORYLINE_FEED_UPSELL_BUTTON:LX/B68;

.field public static final enum STORYLINE_PROMPT:LX/B68;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1746293
    new-instance v0, LX/B68;

    const-string v1, "STORYLINE_PROMPT"

    invoke-direct {v0, v1, v2}, LX/B68;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/B68;->STORYLINE_PROMPT:LX/B68;

    .line 1746294
    new-instance v0, LX/B68;

    const-string v1, "COMPOSER_INLINE_SPROUT"

    invoke-direct {v0, v1, v3}, LX/B68;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/B68;->COMPOSER_INLINE_SPROUT:LX/B68;

    .line 1746295
    new-instance v0, LX/B68;

    const-string v1, "STORYLINE_FEED_UPSELL_BUTTON"

    invoke-direct {v0, v1, v4}, LX/B68;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/B68;->STORYLINE_FEED_UPSELL_BUTTON:LX/B68;

    .line 1746296
    const/4 v0, 0x3

    new-array v0, v0, [LX/B68;

    sget-object v1, LX/B68;->STORYLINE_PROMPT:LX/B68;

    aput-object v1, v0, v2

    sget-object v1, LX/B68;->COMPOSER_INLINE_SPROUT:LX/B68;

    aput-object v1, v0, v3

    sget-object v1, LX/B68;->STORYLINE_FEED_UPSELL_BUTTON:LX/B68;

    aput-object v1, v0, v4

    sput-object v0, LX/B68;->$VALUES:[LX/B68;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1746297
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/B68;
    .locals 1

    .prologue
    .line 1746298
    const-class v0, LX/B68;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/B68;

    return-object v0
.end method

.method public static values()[LX/B68;
    .locals 1

    .prologue
    .line 1746299
    sget-object v0, LX/B68;->$VALUES:[LX/B68;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/B68;

    return-object v0
.end method
