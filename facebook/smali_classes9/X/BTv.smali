.class public final LX/BTv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Z

.field public final synthetic c:LX/BUA;


# direct methods
.method public constructor <init>(LX/BUA;Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 1788065
    iput-object p1, p0, LX/BTv;->c:LX/BUA;

    iput-object p2, p0, LX/BTv;->a:Ljava/lang/String;

    iput-boolean p3, p0, LX/BTv;->b:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1788066
    iget-object v0, p0, LX/BTv;->c:LX/BUA;

    iget-object v0, v0, LX/BUA;->k:LX/19v;

    iget-object v1, p0, LX/BTv;->a:Ljava/lang/String;

    iget-boolean v2, p0, LX/BTv;->b:Z

    .line 1788067
    :try_start_0
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object p0, LX/7Jd;->DOWNLOAD_PLUGIN_LOAD:LX/7Jd;

    iget-object p0, p0, LX/7Jd;->value:Ljava/lang/String;

    invoke-direct {v3, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1788068
    sget-object p0, LX/7Je;->VIDEO_ID:LX/7Je;

    iget-object p0, p0, LX/7Je;->value:Ljava/lang/String;

    invoke-virtual {v3, p0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1788069
    sget-object p0, LX/7Je;->DOWNLOAD_OPTION_STATE:LX/7Je;

    iget-object p0, p0, LX/7Je;->value:Ljava/lang/String;

    invoke-virtual {v3, p0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1788070
    const-string p0, "OfflineVideoModule"

    .line 1788071
    iput-object p0, v3, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1788072
    iget-object p0, v0, LX/19v;->a:LX/0Zb;

    invoke-interface {p0, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1788073
    :goto_0
    const/4 v0, 0x0

    return-object v0

    :catch_0
    goto :goto_0
.end method
