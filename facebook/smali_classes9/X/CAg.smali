.class public final LX/CAg;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/354;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/CAh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/354",
            "<TE;>.ProfilePictureOverlayCallToActionComponentImpl;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/354;

.field private c:[Ljava/lang/String;

.field private d:I

.field public e:Ljava/util/BitSet;


# direct methods
.method public constructor <init>(LX/354;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 1855599
    iput-object p1, p0, LX/CAg;->b:LX/354;

    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 1855600
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "storyProps"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "environment"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/CAg;->c:[Ljava/lang/String;

    .line 1855601
    iput v3, p0, LX/CAg;->d:I

    .line 1855602
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/CAg;->d:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/CAg;->e:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/CAg;LX/1De;IILX/CAh;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "II",
            "LX/354",
            "<TE;>.ProfilePictureOverlayCallToActionComponentImpl;)V"
        }
    .end annotation

    .prologue
    .line 1855595
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 1855596
    iput-object p4, p0, LX/CAg;->a:LX/CAh;

    .line 1855597
    iget-object v0, p0, LX/CAg;->e:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 1855598
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1855581
    invoke-super {p0}, LX/1X5;->a()V

    .line 1855582
    const/4 v0, 0x0

    iput-object v0, p0, LX/CAg;->a:LX/CAh;

    .line 1855583
    iget-object v0, p0, LX/CAg;->b:LX/354;

    iget-object v0, v0, LX/354;->b:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 1855584
    return-void
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/354;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1855585
    iget-object v1, p0, LX/CAg;->e:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/CAg;->e:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/CAg;->d:I

    if-ge v1, v2, :cond_2

    .line 1855586
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1855587
    :goto_0
    iget v2, p0, LX/CAg;->d:I

    if-ge v0, v2, :cond_1

    .line 1855588
    iget-object v2, p0, LX/CAg;->e:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1855589
    iget-object v2, p0, LX/CAg;->c:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1855590
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1855591
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1855592
    :cond_2
    iget-object v0, p0, LX/CAg;->a:LX/CAh;

    .line 1855593
    invoke-virtual {p0}, LX/CAg;->a()V

    .line 1855594
    return-object v0
.end method
