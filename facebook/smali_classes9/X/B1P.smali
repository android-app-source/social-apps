.class public LX/B1P;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field public a:LX/1Ck;

.field public b:LX/0tX;

.field public c:Landroid/content/res/Resources;

.field public d:LX/ITa;


# direct methods
.method public constructor <init>(LX/1Ck;LX/0tX;Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1735973
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1735974
    iput-object p1, p0, LX/B1P;->a:LX/1Ck;

    .line 1735975
    iput-object p2, p0, LX/B1P;->b:LX/0tX;

    .line 1735976
    iput-object p3, p0, LX/B1P;->c:Landroid/content/res/Resources;

    .line 1735977
    return-void
.end method

.method public static a(LX/0QB;)LX/B1P;
    .locals 6

    .prologue
    .line 1735978
    const-class v1, LX/B1P;

    monitor-enter v1

    .line 1735979
    :try_start_0
    sget-object v0, LX/B1P;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1735980
    sput-object v2, LX/B1P;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1735981
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1735982
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1735983
    new-instance p0, LX/B1P;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v3

    check-cast v3, LX/1Ck;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v5

    check-cast v5, Landroid/content/res/Resources;

    invoke-direct {p0, v3, v4, v5}, LX/B1P;-><init>(LX/1Ck;LX/0tX;Landroid/content/res/Resources;)V

    .line 1735984
    move-object v0, p0

    .line 1735985
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1735986
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/B1P;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1735987
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1735988
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/ITa;)V
    .locals 0

    .prologue
    .line 1735989
    iput-object p1, p0, LX/B1P;->d:LX/ITa;

    .line 1735990
    return-void
.end method
