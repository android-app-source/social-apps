.class public final LX/Bxf;
.super LX/3mX;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/3mX",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        "TE;>;"
    }
.end annotation


# instance fields
.field public final synthetic c:LX/Bxg;

.field private final d:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/Bxg;Landroid/content/Context;LX/0Px;LX/1Pp;LX/25M;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;TE;",
            "LX/25M;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1835821
    iput-object p1, p0, LX/Bxf;->c:LX/Bxg;

    .line 1835822
    check-cast p4, LX/1Pq;

    invoke-direct {p0, p2, p3, p4, p5}, LX/3mX;-><init>(Landroid/content/Context;LX/0Px;LX/1Pq;LX/25M;)V

    .line 1835823
    iput-object p6, p0, LX/Bxf;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1835824
    return-void
.end method


# virtual methods
.method public final a(LX/1De;)LX/1X1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1835825
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/1De;Ljava/lang/Object;)LX/1X1;
    .locals 4

    .prologue
    .line 1835826
    check-cast p2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1835827
    iget-object v0, p0, LX/Bxf;->c:LX/Bxg;

    iget-object v0, v0, LX/Bxg;->b:LX/Bxj;

    const/4 v1, 0x0

    .line 1835828
    new-instance v2, LX/Bxh;

    invoke-direct {v2, v0}, LX/Bxh;-><init>(LX/Bxj;)V

    .line 1835829
    iget-object v3, v0, LX/Bxj;->b:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Bxi;

    .line 1835830
    if-nez v3, :cond_0

    .line 1835831
    new-instance v3, LX/Bxi;

    invoke-direct {v3, v0}, LX/Bxi;-><init>(LX/Bxj;)V

    .line 1835832
    :cond_0
    invoke-static {v3, p1, v1, v1, v2}, LX/Bxi;->a$redex0(LX/Bxi;LX/1De;IILX/Bxh;)V

    .line 1835833
    move-object v2, v3

    .line 1835834
    move-object v1, v2

    .line 1835835
    move-object v1, v1

    .line 1835836
    iget-object v0, p0, LX/3mX;->b:LX/1Pq;

    check-cast v0, LX/1Pp;

    .line 1835837
    iget-object v2, v1, LX/Bxi;->a:LX/Bxh;

    iput-object v0, v2, LX/Bxh;->b:LX/1Pp;

    .line 1835838
    iget-object v2, v1, LX/Bxi;->e:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 1835839
    move-object v0, v1

    .line 1835840
    iget-object v1, p0, LX/Bxf;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v1, p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 1835841
    iget-object v2, v0, LX/Bxi;->a:LX/Bxh;

    iput-object v1, v2, LX/Bxh;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1835842
    iget-object v2, v0, LX/Bxi;->e:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 1835843
    move-object v0, v0

    .line 1835844
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 1835845
    const/4 v0, 0x0

    return v0
.end method
