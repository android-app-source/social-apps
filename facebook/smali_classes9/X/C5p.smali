.class public LX/C5p;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/ui/animations/persistent/parts/AnimationPartFactory;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1849369
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1849370
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1849371
    iput-object v0, p0, LX/C5p;->a:LX/0Ot;

    .line 1849372
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1849373
    iput-object v0, p0, LX/C5p;->b:LX/0Ot;

    .line 1849374
    return-void
.end method

.method public static a(LX/0QB;)LX/C5p;
    .locals 5

    .prologue
    .line 1849356
    const-class v1, LX/C5p;

    monitor-enter v1

    .line 1849357
    :try_start_0
    sget-object v0, LX/C5p;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1849358
    sput-object v2, LX/C5p;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1849359
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1849360
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1849361
    new-instance v3, LX/C5p;

    invoke-direct {v3}, LX/C5p;-><init>()V

    .line 1849362
    const/16 v4, 0x12a4

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 p0, 0x271

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 1849363
    iput-object v4, v3, LX/C5p;->a:LX/0Ot;

    iput-object p0, v3, LX/C5p;->b:LX/0Ot;

    .line 1849364
    move-object v0, v3

    .line 1849365
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1849366
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C5p;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1849367
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1849368
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a$redex0(LX/C5p;Ljava/lang/Runnable;LX/1Wt;J)V
    .locals 1

    .prologue
    .line 1849352
    iget-object v0, p0, LX/C5p;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Sh;

    invoke-virtual {v0, p1, p3, p4}, LX/0Sh;->a(Ljava/lang/Runnable;J)V

    .line 1849353
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p2, LX/1Wt;->b:Ljava/lang/ref/WeakReference;

    .line 1849354
    sget-object v0, LX/1Wu;->SCHEDULED:LX/1Wu;

    iput-object v0, p2, LX/1Wt;->a:LX/1Wu;

    .line 1849355
    return-void
.end method


# virtual methods
.method public final a(LX/C5o;LX/C67;J)V
    .locals 9
    .param p2    # LX/C67;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x1

    .line 1849339
    iget-object v0, p1, LX/C5o;->a:LX/1Wt;

    .line 1849340
    iget-object v1, v0, LX/1Wt;->a:LX/1Wu;

    move-object v0, v1

    .line 1849341
    sget-object v1, LX/1Wu;->PLAYED:LX/1Wu;

    if-ne v0, v1, :cond_0

    .line 1849342
    :goto_0
    return-void

    .line 1849343
    :cond_0
    iget-object v0, p1, LX/C5o;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1849344
    iget-object v0, p1, LX/C5o;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput v3, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1849345
    :goto_1
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 1849346
    iget-object v1, p1, LX/C5o;->b:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 1849347
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    int-to-float v0, v0

    iget v1, p1, LX/C5o;->d:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    move v5, v0

    .line 1849348
    const/4 v0, 0x2

    new-array v4, v0, [I

    .line 1849349
    new-instance v1, Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterAnimator$1;

    move-object v2, p0

    move-object v3, p1

    move-wide v6, p3

    move-object v8, p2

    invoke-direct/range {v1 .. v8}, Lcom/facebook/feedplugins/graphqlstory/footer/TopLevelFooterAnimator$1;-><init>(LX/C5p;LX/C5o;[IIJLX/C67;)V

    .line 1849350
    iget-object v0, p1, LX/C5o;->a:LX/1Wt;

    invoke-static {p0, v1, v0, p3, p4}, LX/C5p;->a$redex0(LX/C5p;Ljava/lang/Runnable;LX/1Wt;J)V

    goto :goto_0

    .line 1849351
    :cond_1
    iget-object v0, p1, LX/C5o;->b:Landroid/view/View;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    const/4 v2, -0x1

    invoke-direct {v1, v2, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_1
.end method
