.class public final LX/Aop;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Lcom/facebook/graphql/model/GraphQLStory;

.field public final synthetic d:Ljava/lang/String;

.field public final synthetic e:Landroid/view/View;

.field public final synthetic f:LX/Aov;


# direct methods
.method public constructor <init>(LX/Aov;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 1714913
    iput-object p1, p0, LX/Aop;->f:LX/Aov;

    iput-object p2, p0, LX/Aop;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iput-object p3, p0, LX/Aop;->b:Ljava/lang/String;

    iput-object p4, p0, LX/Aop;->c:Lcom/facebook/graphql/model/GraphQLStory;

    iput-object p5, p0, LX/Aop;->d:Ljava/lang/String;

    iput-object p6, p0, LX/Aop;->e:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 10

    .prologue
    .line 1714900
    iget-object v0, p0, LX/Aop;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, LX/14w;->r(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    invoke-static {v0}, LX/14w;->s(Lcom/facebook/feed/rows/core/props/FeedProps;)Ljava/lang/String;

    move-result-object v6

    .line 1714901
    iget-object v0, p0, LX/Aop;->f:LX/Aov;

    iget-object v0, v0, LX/Aov;->b:LX/1EQ;

    iget-object v1, p0, LX/Aop;->b:Ljava/lang/String;

    iget-object v2, p0, LX/Aop;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v2}, LX/14w;->q(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    .line 1714902
    iget-object v3, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v3

    .line 1714903
    check-cast v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/Aop;->f:LX/Aov;

    iget-object v3, v3, LX/Aov;->c:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1714904
    iget-object v4, v3, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v3, v4

    .line 1714905
    iget-object v4, p0, LX/Aop;->c:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v4}, LX/214;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, LX/Aop;->d:Ljava/lang/String;

    .line 1714906
    iget-object v7, v0, LX/1EQ;->a:LX/0Zb;

    const-string v8, "feed_share_action"

    const/4 v9, 0x1

    invoke-interface {v7, v8, v9}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v7

    .line 1714907
    invoke-virtual {v7}, LX/0oG;->a()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 1714908
    invoke-virtual {v7, v1}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object v8

    const-string v9, "share_type"

    const-string p1, "copy_link"

    invoke-virtual {v8, v9, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v8

    const-string v9, "story_id"

    invoke-virtual {v8, v9, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v8

    const-string v9, "user_id"

    invoke-virtual {v8, v9, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v8

    const-string v9, "shareable_id"

    invoke-virtual {v8, v9, v4}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v8

    invoke-virtual {v8, v5}, LX/0oG;->e(Ljava/lang/String;)LX/0oG;

    .line 1714909
    invoke-virtual {v7}, LX/0oG;->d()V

    .line 1714910
    :cond_0
    iget-object v0, p0, LX/Aop;->e:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v6}, LX/47Y;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 1714911
    iget-object v0, p0, LX/Aop;->f:LX/Aov;

    iget-object v0, v0, LX/Aov;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/4nT;

    iget-object v1, p0, LX/Aop;->e:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f08109b

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4nT;->a(Ljava/lang/String;)V

    .line 1714912
    const/4 v0, 0x1

    return v0
.end method
