.class public LX/BF9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/21l;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/21l",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:LX/0QK;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QK",
            "<",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/1K9;

.field private final c:LX/6Ot;

.field public d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public e:Lcom/facebook/graphql/model/GraphQLFeedback;

.field public f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/6Vi;",
            ">;"
        }
    .end annotation
.end field

.field public g:Z


# direct methods
.method public constructor <init>(LX/0QK;LX/1K9;LX/6Ot;)V
    .locals 1
    .param p1    # LX/0QK;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QK",
            "<",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "Ljava/lang/Void;",
            ">;",
            "LX/1K9;",
            "LX/6Ot;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1765425
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1765426
    iput-object p1, p0, LX/BF9;->a:LX/0QK;

    .line 1765427
    iput-object p2, p0, LX/BF9;->b:LX/1K9;

    .line 1765428
    iput-object p3, p0, LX/BF9;->c:LX/6Ot;

    .line 1765429
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/BF9;->f:Ljava/util/List;

    .line 1765430
    return-void
.end method

.method public static c(LX/BF9;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1765431
    iget-object v0, p0, LX/BF9;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Vi;

    .line 1765432
    iget-object v2, p0, LX/BF9;->b:LX/1K9;

    invoke-virtual {v2, v0}, LX/1K9;->a(LX/6Vi;)V

    goto :goto_0

    .line 1765433
    :cond_0
    iget-object v0, p0, LX/BF9;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1765434
    iput-object v3, p0, LX/BF9;->d:LX/0Px;

    .line 1765435
    iput-object v3, p0, LX/BF9;->e:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1765436
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/BF9;->g:Z

    .line 1765437
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 5

    .prologue
    .line 1765438
    if-nez p1, :cond_1

    .line 1765439
    invoke-static {p0}, LX/BF9;->c(LX/BF9;)V

    .line 1765440
    :cond_0
    :goto_0
    return-void

    .line 1765441
    :cond_1
    invoke-static {p1}, LX/16z;->f(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    .line 1765442
    if-nez v0, :cond_2

    .line 1765443
    invoke-static {p0}, LX/BF9;->c(LX/BF9;)V

    goto :goto_0

    .line 1765444
    :cond_2
    iput-object v0, p0, LX/BF9;->e:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1765445
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/BF9;->d:LX/0Px;

    .line 1765446
    iget-boolean v0, p0, LX/BF9;->g:Z

    if-nez v0, :cond_0

    .line 1765447
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1765448
    iget-object v0, p0, LX/BF9;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, LX/BF9;->d:LX/0Px;

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/BF9;->d:LX/0Px;

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object v0

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->PLACE_LIST:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-virtual {v0, v3}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    .line 1765449
    :goto_1
    if-nez v0, :cond_4

    .line 1765450
    :goto_2
    goto :goto_0

    :cond_3
    move v0, v2

    .line 1765451
    goto :goto_1

    .line 1765452
    :cond_4
    const-class v0, LX/8pz;

    new-instance v2, LX/BF8;

    invoke-direct {v2, p0}, LX/BF8;-><init>(LX/BF9;)V

    .line 1765453
    iget-object v3, p0, LX/BF9;->b:LX/1K9;

    iget-object v4, p0, LX/BF9;->e:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v0, v4, v2}, LX/1K9;->a(Ljava/lang/Class;Ljava/lang/Object;LX/6Ve;)LX/6Vi;

    move-result-object v3

    .line 1765454
    iget-object v4, p0, LX/BF9;->b:LX/1K9;

    iget-object p1, p0, LX/BF9;->e:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v4, v0, p1, v2}, LX/1K9;->a(Ljava/lang/Class;Ljava/lang/Object;LX/6Ve;)LX/6Vi;

    move-result-object v4

    .line 1765455
    iget-object p1, p0, LX/BF9;->f:Ljava/util/List;

    invoke-interface {p1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1765456
    iget-object v3, p0, LX/BF9;->f:Ljava/util/List;

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1765457
    iput-boolean v1, p0, LX/BF9;->g:Z

    goto :goto_2
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1765458
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {p0, p1}, LX/BF9;->a(Lcom/facebook/graphql/model/GraphQLStory;)V

    return-void
.end method
