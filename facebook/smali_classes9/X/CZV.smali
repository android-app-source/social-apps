.class public final LX/CZV;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    .line 1915865
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1915866
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 1915867
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 1915868
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1915869
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_8

    .line 1915870
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1915871
    :goto_1
    move v1, v2

    .line 1915872
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1915873
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0

    .line 1915874
    :cond_1
    const-string v9, "length"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 1915875
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v4

    move v6, v4

    move v4, v3

    .line 1915876
    :cond_2
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_5

    .line 1915877
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 1915878
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1915879
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_2

    if-eqz v8, :cond_2

    .line 1915880
    const-string v9, "entity"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 1915881
    const/4 v8, 0x0

    .line 1915882
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v9, LX/15z;->START_OBJECT:LX/15z;

    if-eq v7, v9, :cond_e

    .line 1915883
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1915884
    :goto_3
    move v7, v8

    .line 1915885
    goto :goto_2

    .line 1915886
    :cond_3
    const-string v9, "offset"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 1915887
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v1

    move v5, v1

    move v1, v3

    goto :goto_2

    .line 1915888
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_2

    .line 1915889
    :cond_5
    const/4 v8, 0x3

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 1915890
    invoke-virtual {p1, v2, v7}, LX/186;->b(II)V

    .line 1915891
    if-eqz v4, :cond_6

    .line 1915892
    invoke-virtual {p1, v3, v6, v2}, LX/186;->a(III)V

    .line 1915893
    :cond_6
    if-eqz v1, :cond_7

    .line 1915894
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v5, v2}, LX/186;->a(III)V

    .line 1915895
    :cond_7
    invoke-virtual {p1}, LX/186;->d()I

    move-result v2

    goto :goto_1

    :cond_8
    move v1, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    goto :goto_2

    .line 1915896
    :cond_9
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1915897
    :cond_a
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_d

    .line 1915898
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 1915899
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1915900
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_a

    if-eqz v10, :cond_a

    .line 1915901
    const-string v11, "__type__"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_b

    const-string v11, "__typename"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_c

    .line 1915902
    :cond_b
    invoke-static {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v9

    goto :goto_4

    .line 1915903
    :cond_c
    const-string v11, "url"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_9

    .line 1915904
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_4

    .line 1915905
    :cond_d
    const/4 v10, 0x2

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 1915906
    invoke-virtual {p1, v8, v9}, LX/186;->b(II)V

    .line 1915907
    const/4 v8, 0x1

    invoke-virtual {p1, v8, v7}, LX/186;->b(II)V

    .line 1915908
    invoke-virtual {p1}, LX/186;->d()I

    move-result v8

    goto/16 :goto_3

    :cond_e
    move v7, v8

    move v9, v8

    goto :goto_4
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1915909
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1915910
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1915911
    if-eqz v0, :cond_2

    .line 1915912
    const-string v1, "entity"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1915913
    const/4 p3, 0x0

    .line 1915914
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1915915
    invoke-virtual {p0, v0, p3}, LX/15i;->g(II)I

    move-result v1

    .line 1915916
    if-eqz v1, :cond_0

    .line 1915917
    const-string v1, "__type__"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1915918
    invoke-static {p0, v0, p3, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1915919
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1915920
    if-eqz v1, :cond_1

    .line 1915921
    const-string p3, "url"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1915922
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1915923
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1915924
    :cond_2
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1915925
    if-eqz v0, :cond_3

    .line 1915926
    const-string v1, "length"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1915927
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1915928
    :cond_3
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1915929
    if-eqz v0, :cond_4

    .line 1915930
    const-string v1, "offset"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1915931
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1915932
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1915933
    return-void
.end method
