.class public LX/CbL;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/8Jc;

.field public b:LX/8Jb;

.field public c:Landroid/graphics/Matrix;

.field public d:Landroid/graphics/PointF;

.field public e:F

.field public f:LX/0am;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLInterfaces$FaceBoxInfo;",
            ">;>;"
        }
    .end annotation
.end field

.field public g:Landroid/graphics/RectF;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public final h:Landroid/graphics/RectF;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public i:Landroid/graphics/PointF;

.field public j:LX/CbN;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field private final k:LX/Caq;

.field public final l:LX/CbF;

.field public final m:LX/8nB;

.field public final n:LX/Cao;

.field public final o:Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;

.field public final p:Landroid/graphics/PointF;

.field private final q:Landroid/graphics/Matrix;

.field public r:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxInfoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;ILjava/lang/String;Ljava/lang/String;LX/8Jc;LX/Caq;LX/CbF;LX/0Ot;LX/Cao;LX/8nX;LX/0Ot;LX/0ad;)V
    .locals 2
    .param p1    # Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # I
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;",
            "I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/8Jc;",
            "LX/Caq;",
            "LX/CbF;",
            "LX/0Ot",
            "<",
            "LX/8nB;",
            ">;",
            "LX/Cao;",
            "LX/8nX;",
            "LX/0Ot",
            "<",
            "LX/8nP;",
            ">;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1919464
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1919465
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, LX/CbL;->p:Landroid/graphics/PointF;

    .line 1919466
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, LX/CbL;->q:Landroid/graphics/Matrix;

    .line 1919467
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, LX/CbL;->c:Landroid/graphics/Matrix;

    .line 1919468
    const/4 v0, 0x0

    invoke-static {v0}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/CbL;->f:LX/0am;

    .line 1919469
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, LX/CbL;->h:Landroid/graphics/RectF;

    .line 1919470
    iput-object p6, p0, LX/CbL;->k:LX/Caq;

    .line 1919471
    iput-object p5, p0, LX/CbL;->a:LX/8Jc;

    .line 1919472
    iput-object p7, p0, LX/CbL;->l:LX/CbF;

    .line 1919473
    const v0, 0x41e065f

    if-ne p2, v0, :cond_0

    sget-short v0, LX/8mx;->d:S

    const/4 v1, 0x0

    invoke-interface {p12, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1919474
    invoke-static {p3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p10, v0}, LX/8nX;->a(Ljava/lang/Long;)LX/8nW;

    move-result-object v0

    iput-object v0, p0, LX/CbL;->m:LX/8nB;

    .line 1919475
    :goto_0
    iput-object p9, p0, LX/CbL;->n:LX/Cao;

    .line 1919476
    iput-object p1, p0, LX/CbL;->o:Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;

    .line 1919477
    return-void

    .line 1919478
    :cond_0
    const-string v0, "good_friends"

    invoke-virtual {v0, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1919479
    invoke-interface {p11}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8nB;

    iput-object v0, p0, LX/CbL;->m:LX/8nB;

    goto :goto_0

    .line 1919480
    :cond_1
    invoke-interface {p8}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8nB;

    iput-object v0, p0, LX/CbL;->m:LX/8nB;

    goto :goto_0
.end method

.method private static j(LX/CbL;)LX/8Jb;
    .locals 5

    .prologue
    .line 1919455
    iget-object v0, p0, LX/CbL;->a:LX/8Jc;

    iget-object v1, p0, LX/CbL;->o:Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;

    invoke-virtual {v1}, Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;->getZoomableController()LX/5ue;

    move-result-object v1

    iget-object v2, p0, LX/CbL;->g:Landroid/graphics/RectF;

    .line 1919456
    new-instance v3, Landroid/graphics/RectF;

    invoke-direct {v3}, Landroid/graphics/RectF;-><init>()V

    .line 1919457
    new-instance v4, Landroid/graphics/Matrix;

    invoke-direct {v4}, Landroid/graphics/Matrix;-><init>()V

    .line 1919458
    invoke-virtual {v1, v4}, LX/5ua;->a(Landroid/graphics/Matrix;)V

    .line 1919459
    invoke-virtual {v4, v3, v2}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    .line 1919460
    invoke-virtual {v1}, LX/5ua;->m()F

    move-result v4

    .line 1919461
    iget p0, v1, LX/5ua;->j:F

    move p0, p0

    .line 1919462
    invoke-static {v0, v3, v4, p0}, LX/8Jc;->a(LX/8Jc;Landroid/graphics/RectF;FF)LX/8Jb;

    move-result-object v3

    move-object v0, v3

    .line 1919463
    return-object v0
.end method

.method private static k(LX/CbL;)LX/8Jb;
    .locals 2

    .prologue
    .line 1919452
    iget-object v0, p0, LX/CbL;->a:LX/8Jc;

    iget-object v1, p0, LX/CbL;->o:Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;

    invoke-virtual {v1}, Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;->getZoomableController()LX/5ue;

    move-result-object v1

    .line 1919453
    invoke-virtual {v1}, LX/5ua;->m()F

    move-result p0

    invoke-static {v0, p0}, LX/8Jc;->a(LX/8Jc;F)LX/8Jb;

    move-result-object p0

    move-object v0, p0

    .line 1919454
    return-object v0
.end method

.method private static l(LX/CbL;)V
    .locals 2

    .prologue
    .line 1919447
    iget-object v0, p0, LX/CbL;->o:Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;

    invoke-virtual {v0}, Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;->getZoomableController()LX/5ue;

    move-result-object v0

    .line 1919448
    iget-object v1, p0, LX/CbL;->c:Landroid/graphics/Matrix;

    .line 1919449
    iget-object p0, v0, LX/5ua;->o:Landroid/graphics/Matrix;

    move-object v0, p0

    .line 1919450
    invoke-virtual {v1, v0}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 1919451
    return-void
.end method

.method public static m(LX/CbL;)V
    .locals 7

    .prologue
    .line 1919428
    iget-object v0, p0, LX/CbL;->o:Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;

    invoke-virtual {v0}, Lcom/facebook/photos/mediagallery/ui/widget/GalleryDraweeView;->getZoomableController()LX/5ue;

    move-result-object v0

    .line 1919429
    iget-object v1, p0, LX/CbL;->q:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, LX/5ua;->a(Landroid/graphics/Matrix;)V

    .line 1919430
    iget-object v1, p0, LX/CbL;->q:Landroid/graphics/Matrix;

    iget-object v2, p0, LX/CbL;->h:Landroid/graphics/RectF;

    iget-object v3, p0, LX/CbL;->g:Landroid/graphics/RectF;

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    .line 1919431
    iget-object v1, p0, LX/CbL;->h:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v1

    float-to-double v2, v1

    iget-object v1, p0, LX/CbL;->h:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v1

    float-to-double v4, v1

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->hypot(DD)D

    move-result-wide v2

    double-to-float v1, v2

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    iput v1, p0, LX/CbL;->e:F

    .line 1919432
    new-instance v1, Landroid/graphics/PointF;

    iget-object v2, p0, LX/CbL;->h:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->centerX()F

    move-result v2

    iget-object v3, p0, LX/CbL;->h:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->centerY()F

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v1, p0, LX/CbL;->d:Landroid/graphics/PointF;

    .line 1919433
    iget-object v1, p0, LX/CbL;->d:Landroid/graphics/PointF;

    iget-object v2, p0, LX/CbL;->b:LX/8Jb;

    .line 1919434
    iget-object v3, v0, LX/5ua;->k:Landroid/graphics/RectF;

    move-object v3, v3

    .line 1919435
    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v4

    .line 1919436
    iget-object v3, v0, LX/5ua;->l:Landroid/graphics/RectF;

    move-object v3, v3

    .line 1919437
    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v5

    .line 1919438
    iget-object v3, v0, LX/5ua;->k:Landroid/graphics/RectF;

    move-object v3, v3

    .line 1919439
    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v3

    div-float v3, v4, v3

    .line 1919440
    iget-object v6, v0, LX/5ua;->l:Landroid/graphics/RectF;

    move-object v6, v6

    .line 1919441
    invoke-virtual {v6}, Landroid/graphics/RectF;->height()F

    move-result v6

    div-float v6, v5, v6

    .line 1919442
    cmpg-float v3, v3, v6

    if-gtz v3, :cond_0

    const/4 v3, 0x1

    .line 1919443
    :goto_0
    invoke-static {v1, v2, v4, v5, v3}, LX/8Jc;->a(Landroid/graphics/PointF;LX/8Jb;FFZ)Landroid/graphics/PointF;

    move-result-object v3

    move-object v0, v3

    .line 1919444
    iput-object v0, p0, LX/CbL;->i:Landroid/graphics/PointF;

    .line 1919445
    return-void

    .line 1919446
    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public static n(LX/CbL;)Landroid/graphics/PointF;
    .locals 3

    .prologue
    .line 1919395
    iget-object v0, p0, LX/CbL;->i:Landroid/graphics/PointF;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1919396
    new-instance v0, Landroid/graphics/PointF;

    iget-object v1, p0, LX/CbL;->h:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->centerX()F

    move-result v1

    iget-object v2, p0, LX/CbL;->h:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->bottom:F

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    return-object v0
.end method

.method public static o(LX/CbL;)Landroid/graphics/PointF;
    .locals 5

    .prologue
    .line 1919426
    iget-object v0, p0, LX/CbL;->i:Landroid/graphics/PointF;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1919427
    new-instance v0, Landroid/graphics/PointF;

    iget-object v1, p0, LX/CbL;->i:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    iget-object v2, p0, LX/CbL;->i:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    iget-object v3, p0, LX/CbL;->b:LX/8Jb;

    iget v3, v3, LX/8Jb;->b:F

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    add-float/2addr v2, v3

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    return-object v0
.end method


# virtual methods
.method public final a(LX/Can;)V
    .locals 1

    .prologue
    .line 1919424
    iget-object v0, p0, LX/CbL;->n:LX/Cao;

    invoke-virtual {v0, p1}, LX/Cao;->a(LX/Can;)V

    .line 1919425
    return-void
.end method

.method public final a(Landroid/graphics/PointF;)V
    .locals 6

    .prologue
    .line 1919415
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1919416
    invoke-static {p0}, LX/CbL;->l(LX/CbL;)V

    .line 1919417
    new-instance v0, Landroid/graphics/RectF;

    iget v1, p1, Landroid/graphics/PointF;->x:F

    iget v2, p1, Landroid/graphics/PointF;->y:F

    iget v3, p1, Landroid/graphics/PointF;->x:F

    iget v4, p1, Landroid/graphics/PointF;->y:F

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v0, p0, LX/CbL;->g:Landroid/graphics/RectF;

    .line 1919418
    invoke-static {p0}, LX/CbL;->k(LX/CbL;)LX/8Jb;

    move-result-object v0

    iput-object v0, p0, LX/CbL;->b:LX/8Jb;

    .line 1919419
    invoke-static {p0}, LX/CbL;->m(LX/CbL;)V

    .line 1919420
    new-instance v0, LX/CbN;

    const/4 v1, 0x0

    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v2

    invoke-static {p0}, LX/CbL;->n(LX/CbL;)Landroid/graphics/PointF;

    move-result-object v3

    invoke-static {p0}, LX/CbL;->o(LX/CbL;)Landroid/graphics/PointF;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, LX/CbN;-><init>(Ljava/lang/String;Ljava/util/List;Landroid/graphics/PointF;Landroid/graphics/PointF;Z)V

    iput-object v0, p0, LX/CbL;->j:LX/CbN;

    .line 1919421
    new-instance v0, LX/CbK;

    invoke-direct {v0, p0}, LX/CbK;-><init>(LX/CbL;)V

    move-object v0, v0

    .line 1919422
    invoke-virtual {p0, v0}, LX/CbL;->a(LX/Can;)V

    .line 1919423
    return-void
.end method

.method public final a(Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxInfoModel;Ljava/util/Collection;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLInterfaces$FaceBoxInfo;",
            "Ljava/util/Collection",
            "<",
            "Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLInterfaces$FaceBoxInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1919400
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1919401
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1919402
    invoke-static {p0}, LX/CbL;->l(LX/CbL;)V

    .line 1919403
    iget-object v0, p0, LX/CbL;->k:LX/Caq;

    .line 1919404
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1919405
    invoke-static {p2}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v1

    .line 1919406
    new-instance v2, LX/Cap;

    invoke-direct {v2, v0}, LX/Cap;-><init>(LX/Caq;)V

    invoke-static {v1, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1919407
    move-object v0, v1

    .line 1919408
    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/CbL;->f:LX/0am;

    .line 1919409
    iput-object p1, p0, LX/CbL;->r:Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxInfoModel;

    .line 1919410
    invoke-static {p1}, LX/Caq;->a(Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxInfoModel;)Landroid/graphics/RectF;

    move-result-object v0

    iput-object v0, p0, LX/CbL;->g:Landroid/graphics/RectF;

    .line 1919411
    invoke-static {p0}, LX/CbL;->j(LX/CbL;)LX/8Jb;

    move-result-object v0

    iput-object v0, p0, LX/CbL;->b:LX/8Jb;

    .line 1919412
    invoke-static {p0}, LX/CbL;->m(LX/CbL;)V

    .line 1919413
    new-instance v0, LX/CbN;

    invoke-virtual {p1}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$FaceBoxInfoModel;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v2

    invoke-static {p0}, LX/CbL;->n(LX/CbL;)Landroid/graphics/PointF;

    move-result-object v3

    invoke-static {p0}, LX/CbL;->o(LX/CbL;)Landroid/graphics/PointF;

    move-result-object v4

    const/4 v5, 0x1

    invoke-direct/range {v0 .. v5}, LX/CbN;-><init>(Ljava/lang/String;Ljava/util/List;Landroid/graphics/PointF;Landroid/graphics/PointF;Z)V

    iput-object v0, p0, LX/CbL;->j:LX/CbN;

    .line 1919414
    return-void
.end method

.method public final c()LX/CbN;
    .locals 1

    .prologue
    .line 1919398
    iget-object v0, p0, LX/CbL;->j:LX/CbN;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1919399
    iget-object v0, p0, LX/CbL;->j:LX/CbN;

    return-object v0
.end method

.method public final g()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1919397
    iget-object v0, p0, LX/CbL;->f:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/CbL;->f:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
