.class public final LX/Alp;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field public a:Landroid/view/View;

.field public final synthetic b:LX/AlU;


# direct methods
.method public constructor <init>(LX/AlU;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 1710277
    iput-object p1, p0, LX/Alp;->b:LX/AlU;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1710278
    iput-object p2, p0, LX/Alp;->a:Landroid/view/View;

    .line 1710279
    return-void
.end method


# virtual methods
.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 2

    .prologue
    .line 1710280
    iget-object v1, p0, LX/Alp;->a:Landroid/view/View;

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setTranslationX(F)V

    .line 1710281
    return-void
.end method
