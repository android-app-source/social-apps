.class public LX/B3V;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1740394
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/app/Activity;LX/B35;Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;ILcom/facebook/heisman/intent/ProfilePictureOverlayPivotIntentData;)V
    .locals 10

    .prologue
    const v7, 0x187a052c

    const/16 v6, 0x400

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1740395
    iget-object v0, p2, Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;->a:Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    move-object v0, v0

    .line 1740396
    new-instance v1, LX/186;

    invoke-direct {v1, v6}, LX/186;-><init>(I)V

    invoke-virtual {v1, v5}, LX/186;->c(I)V

    invoke-virtual {v1, v4, p3, v4}, LX/186;->a(III)V

    invoke-static {v1, v7}, LX/1vs;->a(LX/186;I)LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 1740397
    new-instance v3, LX/186;

    invoke-direct {v3, v6}, LX/186;-><init>(I)V

    invoke-static {v2, v1, v7, v3}, Lcom/facebook/heisman/protocol/FetchImageOverlayGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v1

    invoke-virtual {v3, v5}, LX/186;->c(I)V

    invoke-virtual {v3, v4, v1}, LX/186;->b(II)V

    const v1, -0x378f21cf

    invoke-static {v3, v1}, LX/1vs;->a(LX/186;I)LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 1740398
    new-instance v3, LX/B4k;

    invoke-direct {v3}, LX/B4k;-><init>()V

    new-instance v4, LX/B4i;

    invoke-direct {v4}, LX/B4i;-><init>()V

    .line 1740399
    iget-object v5, p2, Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;->b:Ljava/lang/String;

    move-object v5, v5

    .line 1740400
    iput-object v5, v4, LX/B4i;->a:Ljava/lang/String;

    .line 1740401
    move-object v4, v4

    .line 1740402
    invoke-virtual {v4, v2, v1}, LX/B4i;->a(LX/15i;I)LX/B4i;

    move-result-object v1

    invoke-virtual {v1}, LX/B4i;->a()Lcom/facebook/heisman/protocol/FetchImageOverlayGraphQLModels$CameraTitleFieldsModel;

    move-result-object v1

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 1740403
    iput-object v1, v3, LX/B4k;->b:LX/0Px;

    .line 1740404
    move-object v1, v3

    .line 1740405
    invoke-virtual {v1}, LX/B4k;->a()Lcom/facebook/heisman/protocol/FetchImageOverlayGraphQLModels$ImageOverlayCameraTitleFieldsModel;

    move-result-object v1

    .line 1740406
    new-instance v2, LX/B4K;

    invoke-virtual {p4}, Lcom/facebook/heisman/intent/ProfilePictureOverlayPivotIntentData;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p4}, Lcom/facebook/heisman/intent/ProfilePictureOverlayPivotIntentData;->e()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v0, v3, v4}, LX/B4K;-><init>(LX/5QV;Ljava/lang/String;Ljava/lang/String;)V

    .line 1740407
    iget-object v0, p4, Lcom/facebook/heisman/intent/ProfilePictureOverlayPivotIntentData;->a:Lcom/facebook/heisman/intent/ProfilePictureOverlayCommonParams;

    iget-object v0, v0, Lcom/facebook/heisman/intent/ProfilePictureOverlayCommonParams;->d:Ljava/lang/String;

    move-object v0, v0

    .line 1740408
    invoke-virtual {v2, v0}, LX/B4J;->a(Ljava/lang/String;)LX/B4J;

    move-result-object v0

    check-cast v0, LX/B4K;

    .line 1740409
    iget-object v8, p4, Lcom/facebook/heisman/intent/ProfilePictureOverlayPivotIntentData;->a:Lcom/facebook/heisman/intent/ProfilePictureOverlayCommonParams;

    iget-wide v8, v8, Lcom/facebook/heisman/intent/ProfilePictureOverlayCommonParams;->c:J

    move-wide v2, v8

    .line 1740410
    invoke-virtual {v0, v2, v3}, LX/B4J;->a(J)LX/B4J;

    move-result-object v0

    check-cast v0, LX/B4K;

    .line 1740411
    iput-object v1, v0, LX/B4K;->b:Lcom/facebook/heisman/protocol/FetchImageOverlayGraphQLModels$ImageOverlayCameraTitleFieldsModel;

    .line 1740412
    move-object v0, v0

    .line 1740413
    iget-object v1, p4, Lcom/facebook/heisman/intent/ProfilePictureOverlayPivotIntentData;->a:Lcom/facebook/heisman/intent/ProfilePictureOverlayCommonParams;

    iget v1, v1, Lcom/facebook/heisman/intent/ProfilePictureOverlayCommonParams;->f:I

    move v1, v1

    .line 1740414
    invoke-virtual {v0, v1}, LX/B4J;->a(I)LX/B4J;

    move-result-object v0

    check-cast v0, LX/B4K;

    .line 1740415
    iget-object v1, p4, Lcom/facebook/heisman/intent/ProfilePictureOverlayPivotIntentData;->a:Lcom/facebook/heisman/intent/ProfilePictureOverlayCommonParams;

    iget-object v1, v1, Lcom/facebook/heisman/intent/ProfilePictureOverlayCommonParams;->e:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    move-object v1, v1

    .line 1740416
    invoke-virtual {v0, v1}, LX/B4J;->a(Lcom/facebook/productionprompts/logging/PromptAnalytics;)LX/B4J;

    move-result-object v0

    check-cast v0, LX/B4K;

    invoke-virtual {v0}, LX/B4K;->a()Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;

    move-result-object v0

    .line 1740417
    const/16 v1, 0x7b

    invoke-virtual {p1, p0, v1, v0}, LX/B35;->a(Landroid/app/Activity;ILcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;)V

    .line 1740418
    return-void
.end method
