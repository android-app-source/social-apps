.class public LX/CLs;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1u2;
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field public static a:Z

.field private static final b:LX/1sv;

.field private static final c:LX/1sw;

.field private static final d:LX/1sw;


# instance fields
.field public final batchId:Ljava/lang/Long;

.field public final deliveryReceipts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/CLr;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1879786
    new-instance v0, LX/1sv;

    const-string v1, "DeliveryReceiptBatch"

    invoke-direct {v0, v1}, LX/1sv;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/CLs;->b:LX/1sv;

    .line 1879787
    new-instance v0, LX/1sw;

    const-string v1, "deliveryReceipts"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2, v4}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/CLs;->c:LX/1sw;

    .line 1879788
    new-instance v0, LX/1sw;

    const-string v1, "batchId"

    const/16 v2, 0xa

    const/4 v3, 0x2

    invoke-direct {v0, v1, v2, v3}, LX/1sw;-><init>(Ljava/lang/String;BS)V

    sput-object v0, LX/CLs;->d:LX/1sw;

    .line 1879789
    sput-boolean v4, LX/CLs;->a:Z

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Ljava/lang/Long;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/CLr;",
            ">;",
            "Ljava/lang/Long;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1879790
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1879791
    iput-object p1, p0, LX/CLs;->deliveryReceipts:Ljava/util/List;

    .line 1879792
    iput-object p2, p0, LX/CLs;->batchId:Ljava/lang/Long;

    .line 1879793
    return-void
.end method


# virtual methods
.method public final a(IZ)Ljava/lang/String;
    .locals 6

    .prologue
    .line 1879794
    if-eqz p2, :cond_3

    invoke-static {p1}, LX/7Gy;->a(I)Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    .line 1879795
    :goto_0
    if-eqz p2, :cond_4

    const-string v0, "\n"

    move-object v2, v0

    .line 1879796
    :goto_1
    if-eqz p2, :cond_5

    const-string v0, " "

    .line 1879797
    :goto_2
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v1, "DeliveryReceiptBatch"

    invoke-direct {v4, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1879798
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1879799
    const-string v1, "("

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1879800
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1879801
    const/4 v1, 0x1

    .line 1879802
    iget-object v5, p0, LX/CLs;->deliveryReceipts:Ljava/util/List;

    if-eqz v5, :cond_0

    .line 1879803
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1879804
    const-string v1, "deliveryReceipts"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1879805
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1879806
    const-string v1, ":"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1879807
    iget-object v1, p0, LX/CLs;->deliveryReceipts:Ljava/util/List;

    if-nez v1, :cond_6

    .line 1879808
    const-string v1, "null"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1879809
    :goto_3
    const/4 v1, 0x0

    .line 1879810
    :cond_0
    iget-object v5, p0, LX/CLs;->batchId:Ljava/lang/Long;

    if-eqz v5, :cond_2

    .line 1879811
    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v5, ","

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1879812
    :cond_1
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1879813
    const-string v1, "batchId"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1879814
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1879815
    const-string v1, ":"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1879816
    iget-object v0, p0, LX/CLs;->batchId:Ljava/lang/Long;

    if-nez v0, :cond_7

    .line 1879817
    const-string v0, "null"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1879818
    :cond_2
    :goto_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v3}, LX/7Gy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1879819
    const-string v0, ")"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1879820
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1879821
    :cond_3
    const-string v0, ""

    move-object v3, v0

    goto/16 :goto_0

    .line 1879822
    :cond_4
    const-string v0, ""

    move-object v2, v0

    goto/16 :goto_1

    .line 1879823
    :cond_5
    const-string v0, ""

    goto/16 :goto_2

    .line 1879824
    :cond_6
    iget-object v1, p0, LX/CLs;->deliveryReceipts:Ljava/util/List;

    add-int/lit8 v5, p1, 0x1

    invoke-static {v1, v5, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 1879825
    :cond_7
    iget-object v0, p0, LX/CLs;->batchId:Ljava/lang/Long;

    add-int/lit8 v1, p1, 0x1

    invoke-static {v0, v1, p2}, LX/7Gy;->a(Ljava/lang/Object;IZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4
.end method

.method public final a(LX/1su;)V
    .locals 3

    .prologue
    .line 1879826
    invoke-virtual {p1}, LX/1su;->a()V

    .line 1879827
    iget-object v0, p0, LX/CLs;->deliveryReceipts:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 1879828
    iget-object v0, p0, LX/CLs;->deliveryReceipts:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 1879829
    sget-object v0, LX/CLs;->c:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1879830
    new-instance v0, LX/1u3;

    const/16 v1, 0xc

    iget-object v2, p0, LX/CLs;->deliveryReceipts:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v0, v1, v2}, LX/1u3;-><init>(BI)V

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1u3;)V

    .line 1879831
    iget-object v0, p0, LX/CLs;->deliveryReceipts:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CLr;

    .line 1879832
    invoke-virtual {v0, p1}, LX/CLr;->a(LX/1su;)V

    goto :goto_0

    .line 1879833
    :cond_0
    iget-object v0, p0, LX/CLs;->batchId:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 1879834
    iget-object v0, p0, LX/CLs;->batchId:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 1879835
    sget-object v0, LX/CLs;->d:LX/1sw;

    invoke-virtual {p1, v0}, LX/1su;->a(LX/1sw;)V

    .line 1879836
    iget-object v0, p0, LX/CLs;->batchId:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, LX/1su;->a(J)V

    .line 1879837
    :cond_1
    invoke-virtual {p1}, LX/1su;->c()V

    .line 1879838
    invoke-virtual {p1}, LX/1su;->b()V

    .line 1879839
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1879840
    if-nez p1, :cond_1

    .line 1879841
    :cond_0
    :goto_0
    return v0

    .line 1879842
    :cond_1
    instance-of v1, p1, LX/CLs;

    if-eqz v1, :cond_0

    .line 1879843
    check-cast p1, LX/CLs;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1879844
    if-nez p1, :cond_3

    .line 1879845
    :cond_2
    :goto_1
    move v0, v2

    .line 1879846
    goto :goto_0

    .line 1879847
    :cond_3
    iget-object v0, p0, LX/CLs;->deliveryReceipts:Ljava/util/List;

    if-eqz v0, :cond_8

    move v0, v1

    .line 1879848
    :goto_2
    iget-object v3, p1, LX/CLs;->deliveryReceipts:Ljava/util/List;

    if-eqz v3, :cond_9

    move v3, v1

    .line 1879849
    :goto_3
    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 1879850
    :cond_4
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1879851
    iget-object v0, p0, LX/CLs;->deliveryReceipts:Ljava/util/List;

    iget-object v3, p1, LX/CLs;->deliveryReceipts:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1879852
    :cond_5
    iget-object v0, p0, LX/CLs;->batchId:Ljava/lang/Long;

    if-eqz v0, :cond_a

    move v0, v1

    .line 1879853
    :goto_4
    iget-object v3, p1, LX/CLs;->batchId:Ljava/lang/Long;

    if-eqz v3, :cond_b

    move v3, v1

    .line 1879854
    :goto_5
    if-nez v0, :cond_6

    if-eqz v3, :cond_7

    .line 1879855
    :cond_6
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    .line 1879856
    iget-object v0, p0, LX/CLs;->batchId:Ljava/lang/Long;

    iget-object v3, p1, LX/CLs;->batchId:Ljava/lang/Long;

    invoke-virtual {v0, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_7
    move v2, v1

    .line 1879857
    goto :goto_1

    :cond_8
    move v0, v2

    .line 1879858
    goto :goto_2

    :cond_9
    move v3, v2

    .line 1879859
    goto :goto_3

    :cond_a
    move v0, v2

    .line 1879860
    goto :goto_4

    :cond_b
    move v3, v2

    .line 1879861
    goto :goto_5
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1879862
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1879863
    sget-boolean v0, LX/CLs;->a:Z

    .line 1879864
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, LX/CLs;->a(IZ)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1879865
    return-object v0
.end method
