.class public final LX/Ayw;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 16

    .prologue
    .line 1731488
    const/4 v12, 0x0

    .line 1731489
    const/4 v11, 0x0

    .line 1731490
    const/4 v10, 0x0

    .line 1731491
    const/4 v9, 0x0

    .line 1731492
    const/4 v8, 0x0

    .line 1731493
    const/4 v7, 0x0

    .line 1731494
    const/4 v6, 0x0

    .line 1731495
    const/4 v5, 0x0

    .line 1731496
    const/4 v4, 0x0

    .line 1731497
    const/4 v3, 0x0

    .line 1731498
    const/4 v2, 0x0

    .line 1731499
    const/4 v1, 0x0

    .line 1731500
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->START_OBJECT:LX/15z;

    if-eq v13, v14, :cond_1

    .line 1731501
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1731502
    const/4 v1, 0x0

    .line 1731503
    :goto_0
    return v1

    .line 1731504
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1731505
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->END_OBJECT:LX/15z;

    if-eq v13, v14, :cond_9

    .line 1731506
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v13

    .line 1731507
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1731508
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v14, v15, :cond_1

    if-eqz v13, :cond_1

    .line 1731509
    const-string v14, "can_see_voice_switcher"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_2

    .line 1731510
    const/4 v4, 0x1

    .line 1731511
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v12

    goto :goto_1

    .line 1731512
    :cond_2
    const-string v14, "can_viewer_comment"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_3

    .line 1731513
    const/4 v3, 0x1

    .line 1731514
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v11

    goto :goto_1

    .line 1731515
    :cond_3
    const-string v14, "can_viewer_like"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_4

    .line 1731516
    const/4 v2, 0x1

    .line 1731517
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v10

    goto :goto_1

    .line 1731518
    :cond_4
    const-string v14, "comments"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_5

    .line 1731519
    invoke-static/range {p0 .. p1}, LX/Ayu;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 1731520
    :cond_5
    const-string v14, "does_viewer_like"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_6

    .line 1731521
    const/4 v1, 0x1

    .line 1731522
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v8

    goto :goto_1

    .line 1731523
    :cond_6
    const-string v14, "id"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_7

    .line 1731524
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 1731525
    :cond_7
    const-string v14, "legacy_api_post_id"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_8

    .line 1731526
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_1

    .line 1731527
    :cond_8
    const-string v14, "likers"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 1731528
    invoke-static/range {p0 .. p1}, LX/Ayv;->a(LX/15w;LX/186;)I

    move-result v5

    goto/16 :goto_1

    .line 1731529
    :cond_9
    const/16 v13, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->c(I)V

    .line 1731530
    if-eqz v4, :cond_a

    .line 1731531
    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v12}, LX/186;->a(IZ)V

    .line 1731532
    :cond_a
    if-eqz v3, :cond_b

    .line 1731533
    const/4 v3, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v11}, LX/186;->a(IZ)V

    .line 1731534
    :cond_b
    if-eqz v2, :cond_c

    .line 1731535
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->a(IZ)V

    .line 1731536
    :cond_c
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 1731537
    if-eqz v1, :cond_d

    .line 1731538
    const/4 v1, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v8}, LX/186;->a(IZ)V

    .line 1731539
    :cond_d
    const/4 v1, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 1731540
    const/4 v1, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 1731541
    const/4 v1, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 1731542
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1731543
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1731544
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1731545
    if-eqz v0, :cond_0

    .line 1731546
    const-string v1, "can_see_voice_switcher"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1731547
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1731548
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1731549
    if-eqz v0, :cond_1

    .line 1731550
    const-string v1, "can_viewer_comment"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1731551
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1731552
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1731553
    if-eqz v0, :cond_2

    .line 1731554
    const-string v1, "can_viewer_like"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1731555
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1731556
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1731557
    if-eqz v0, :cond_3

    .line 1731558
    const-string v1, "comments"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1731559
    invoke-static {p0, v0, p2}, LX/Ayu;->a(LX/15i;ILX/0nX;)V

    .line 1731560
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1731561
    if-eqz v0, :cond_4

    .line 1731562
    const-string v1, "does_viewer_like"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1731563
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1731564
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1731565
    if-eqz v0, :cond_5

    .line 1731566
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1731567
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1731568
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1731569
    if-eqz v0, :cond_6

    .line 1731570
    const-string v1, "legacy_api_post_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1731571
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1731572
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1731573
    if-eqz v0, :cond_7

    .line 1731574
    const-string v1, "likers"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1731575
    invoke-static {p0, v0, p2}, LX/Ayv;->a(LX/15i;ILX/0nX;)V

    .line 1731576
    :cond_7
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1731577
    return-void
.end method
