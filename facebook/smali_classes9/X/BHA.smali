.class public final LX/BHA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/9Tq;


# instance fields
.field public final synthetic a:Lcom/facebook/photos/simplepicker/SimplePickerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/simplepicker/SimplePickerFragment;)V
    .locals 0

    .prologue
    .line 1767670
    iput-object p1, p0, LX/BHA;->a:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(LX/0Px;ZLjava/lang/String;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;Z",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1767673
    iget-object v0, p0, LX/BHA;->a:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    iget-object v0, v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->z:LX/BGe;

    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v1

    .line 1767674
    sget-object v4, LX/BGd;->USE_SELECTED_ITEMS:LX/BGd;

    invoke-static {v4}, LX/BGe;->a(LX/BGd;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const-string v5, "photo_selected_count"

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    invoke-static {v0, v4}, LX/BGe;->a(LX/BGe;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1767675
    iget-object v0, p0, LX/BHA;->a:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    iget-object v0, v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->b:Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    if-nez v0, :cond_1

    .line 1767676
    :cond_0
    :goto_0
    return-void

    .line 1767677
    :cond_1
    iget-object v0, p0, LX/BHA;->a:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    iget-object v0, v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->b:Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    .line 1767678
    iget-object v1, v0, Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;->d:Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;

    move-object v0, v1

    .line 1767679
    invoke-virtual {v0}, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->k()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1767680
    iget-object v0, p0, LX/BHA;->a:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    invoke-static {v0}, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->B(Lcom/facebook/photos/simplepicker/SimplePickerFragment;)V

    goto :goto_0

    .line 1767681
    :cond_2
    sget-object v0, LX/BGv;->b:[I

    iget-object v1, p0, LX/BHA;->a:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    iget-object v1, v1, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->b:Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    .line 1767682
    iget-object v4, v1, Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;->b:LX/8A9;

    move-object v1, v4

    .line 1767683
    invoke-virtual {v1}, LX/8A9;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1767684
    iget-object v0, p0, LX/BHA;->a:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    iget-boolean v0, v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->o:Z

    if-eqz v0, :cond_5

    .line 1767685
    iget-object v0, p0, LX/BHA;->a:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    invoke-static {v0, p1}, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->h(Lcom/facebook/photos/simplepicker/SimplePickerFragment;LX/0Px;)V

    goto :goto_0

    .line 1767686
    :pswitch_0
    invoke-static {p1}, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->g(LX/0Px;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 1767687
    iget-object v0, p0, LX/BHA;->a:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    .line 1767688
    iput-object p3, v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->Z:Ljava/lang/String;

    .line 1767689
    iget-object v1, p0, LX/BHA;->a:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    invoke-virtual {p1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    invoke-static {v1, v0}, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->a$redex0(Lcom/facebook/photos/simplepicker/SimplePickerFragment;Lcom/facebook/ipc/media/MediaItem;)V

    goto :goto_0

    .line 1767690
    :pswitch_1
    invoke-static {p1}, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->g(LX/0Px;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 1767691
    iget-object v0, p0, LX/BHA;->a:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    sget-object v1, LX/5Rz;->PROFILE:LX/5Rz;

    invoke-virtual {v1}, LX/5Rz;->name()Ljava/lang/String;

    move-result-object v5

    move-object v1, p1

    move v3, v2

    move v4, v2

    .line 1767692
    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 1767693
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v7

    if-eq v7, v9, :cond_6

    .line 1767694
    sget-object v7, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->s:Ljava/lang/String;

    const-string v8, "Number of photo selected for cropping is not one"

    invoke-static {v7, v8}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1767695
    :goto_1
    goto :goto_0

    .line 1767696
    :pswitch_2
    iget-object v1, p0, LX/BHA;->a:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    invoke-virtual {p1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v0

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->a$redex0(Lcom/facebook/photos/simplepicker/SimplePickerFragment;Landroid/net/Uri;Ljava/lang/String;)V

    goto :goto_0

    .line 1767697
    :pswitch_3
    iget-object v0, p0, LX/BHA;->a:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    sget-object v1, LX/5Rz;->ADS:LX/5Rz;

    invoke-virtual {v1}, LX/5Rz;->name()Ljava/lang/String;

    move-result-object v5

    move-object v1, p1

    move v4, v2

    .line 1767698
    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 1767699
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v7

    if-eq v7, v9, :cond_f

    .line 1767700
    sget-object v7, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->s:Ljava/lang/String;

    const-string v8, "Number of photo selected for cropping is not one"

    invoke-static {v7, v8}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1767701
    :goto_2
    goto/16 :goto_0

    .line 1767702
    :pswitch_4
    iget-object v0, p0, LX/BHA;->a:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    sget-object v1, LX/5Rz;->GENERIC:LX/5Rz;

    invoke-virtual {v1}, LX/5Rz;->name()Ljava/lang/String;

    move-result-object v5

    move-object v1, p1

    move v3, v2

    move v4, v2

    .line 1767703
    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 1767704
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v7

    if-eq v7, v9, :cond_18

    .line 1767705
    sget-object v7, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->s:Ljava/lang/String;

    const-string v8, "Number of photo selected for cropping is not one"

    invoke-static {v7, v8}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1767706
    :goto_3
    goto/16 :goto_0

    .line 1767707
    :pswitch_5
    iget-object v4, p0, LX/BHA;->a:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    sget-object v0, LX/5Rz;->PROFILE:LX/5Rz;

    invoke-virtual {v0}, LX/5Rz;->name()Ljava/lang/String;

    move-result-object v9

    move-object v5, p1

    move v6, v3

    move v7, v2

    move v8, v2

    .line 1767708
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 1767709
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v0

    if-eq v0, v2, :cond_21

    .line 1767710
    sget-object v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->s:Ljava/lang/String;

    const-string v1, "Number of photo selected for cropping is not one"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1767711
    :goto_4
    goto/16 :goto_0

    .line 1767712
    :pswitch_6
    iget-object v0, p0, LX/BHA;->a:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1767713
    iget-object v0, p0, LX/BHA;->a:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    iget-boolean v0, v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->o:Z

    if-eqz v0, :cond_3

    .line 1767714
    iget-object v0, p0, LX/BHA;->a:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    .line 1767715
    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    invoke-static {}, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;->newBuilder()Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration$Builder;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration$Builder;->setMediaItems(LX/0Px;)Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration$Builder;

    move-result-object v2

    sget-object v3, LX/B66;->SIMPLE_PICKER:LX/B66;

    invoke-virtual {v2, v3}, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration$Builder;->setSource(LX/B66;)Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration$Builder;

    move-result-object v2

    iget-object v3, v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->W:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration$Builder;->setSessionId(Ljava/lang/String;)Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration$Builder;

    move-result-object v2

    sget-object v3, LX/B63;->LAUNCH_COMPOSER:LX/B63;

    invoke-virtual {v2, v3}, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration$Builder;->setActionWhenDone(LX/B63;)Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration$Builder;

    move-result-object v2

    invoke-static {v0}, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->C(Lcom/facebook/photos/simplepicker/SimplePickerFragment;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration$Builder;->setComposerConfiguration(Lcom/facebook/ipc/composer/intent/ComposerConfiguration;)Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration$Builder;->a()Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;

    move-result-object v2

    invoke-static {v1, v2}, LX/B67;->a(Landroid/content/Context;Lcom/facebook/ipc/slideshow/SlideshowEditConfiguration;)Landroid/content/Intent;

    move-result-object v2

    .line 1767716
    iget-object v1, v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->H:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    const/16 v3, 0x4dc

    invoke-interface {v1, v2, v3, v0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 1767717
    goto/16 :goto_0

    .line 1767718
    :cond_3
    iget-object v0, p0, LX/BHA;->a:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    .line 1767719
    invoke-static {v0}, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->C(Lcom/facebook/photos/simplepicker/SimplePickerFragment;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    invoke-static {p1}, Lcom/facebook/composer/attachments/ComposerAttachment;->a(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialAttachments(LX/0Px;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    .line 1767720
    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->a(Lcom/facebook/photos/simplepicker/SimplePickerFragment;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;)V

    .line 1767721
    goto/16 :goto_0

    .line 1767722
    :pswitch_7
    iget-object v0, p0, LX/BHA;->a:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    invoke-static {v0, p1}, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->h(Lcom/facebook/photos/simplepicker/SimplePickerFragment;LX/0Px;)V

    goto/16 :goto_0

    .line 1767723
    :pswitch_8
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    if-ne v0, v3, :cond_4

    .line 1767724
    invoke-virtual {p1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    .line 1767725
    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/2Qx;->a(Ljava/lang/String;)LX/434;

    move-result-object v0

    invoke-static {v0}, LX/8K7;->a(LX/434;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1767726
    iget-object v0, p0, LX/BHA;->a:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f081392

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1767727
    iget-object v0, p0, LX/BHA;->a:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    invoke-static {v0}, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->B(Lcom/facebook/photos/simplepicker/SimplePickerFragment;)V

    goto/16 :goto_0

    .line 1767728
    :cond_4
    iget-object v4, p0, LX/BHA;->a:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    sget-object v0, LX/5Rz;->FUNDRAISER_CREATION:LX/5Rz;

    invoke-virtual {v0}, LX/5Rz;->name()Ljava/lang/String;

    move-result-object v9

    move-object v5, p1

    move v6, v2

    move v7, v2

    move v8, v3

    .line 1767729
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 1767730
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v0

    if-eq v0, v2, :cond_2a

    .line 1767731
    sget-object v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->s:Ljava/lang/String;

    const-string v1, "Number of photo selected for cropping is not one"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1767732
    :goto_5
    goto/16 :goto_0

    .line 1767733
    :cond_5
    iget-object v0, p0, LX/BHA;->a:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    invoke-static {v0, p1, p2}, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->a$redex0(Lcom/facebook/photos/simplepicker/SimplePickerFragment;LX/0Px;Z)V

    goto/16 :goto_0

    .line 1767734
    :cond_6
    if-nez v2, :cond_7

    if-eqz v3, :cond_8

    .line 1767735
    :cond_7
    const/high16 v7, 0x3f000000    # 0.5f

    move p0, v7

    .line 1767736
    :goto_6
    if-eqz v3, :cond_a

    .line 1767737
    const v7, 0x3ff49603

    move v8, v7

    .line 1767738
    :goto_7
    invoke-virtual {v1, v10}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/facebook/ipc/media/MediaItem;

    .line 1767739
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p1

    new-instance p2, LX/5Rw;

    invoke-direct {p2}, LX/5Rw;-><init>()V

    invoke-virtual {v7}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v7}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v7

    .line 1767740
    iget-object v1, v7, Lcom/facebook/ipc/media/data/MediaData;->mId:Ljava/lang/String;

    move-object v7, v1

    .line 1767741
    invoke-virtual {p2, v6, v7}, LX/5Rw;->a(Landroid/net/Uri;Ljava/lang/String;)LX/5Rw;

    move-result-object v7

    sget-object p2, LX/5Rr;->CROP:LX/5Rr;

    invoke-virtual {v7, p2}, LX/5Rw;->a(LX/5Rr;)LX/5Rw;

    move-result-object v7

    sget-object p2, LX/5Rr;->DOODLE:LX/5Rr;

    invoke-virtual {v7, p2}, LX/5Rw;->b(LX/5Rr;)LX/5Rw;

    move-result-object v7

    sget-object p2, LX/5Rr;->TEXT:LX/5Rr;

    invoke-virtual {v7, p2}, LX/5Rw;->b(LX/5Rr;)LX/5Rw;

    move-result-object v7

    sget-object p2, LX/5Rr;->STICKER:LX/5Rr;

    invoke-virtual {v7, p2}, LX/5Rw;->b(LX/5Rr;)LX/5Rw;

    move-result-object v7

    sget-object p2, LX/5Rr;->FILTER:LX/5Rr;

    invoke-virtual {v7, p2}, LX/5Rw;->b(LX/5Rr;)LX/5Rw;

    move-result-object v7

    sget-object p2, LX/5Rq;->ZOOM_CROP:LX/5Rq;

    invoke-virtual {v7, p2}, LX/5Rw;->a(LX/5Rq;)LX/5Rw;

    move-result-object p2

    new-instance v7, LX/5Ry;

    invoke-direct {v7}, LX/5Ry;-><init>()V

    .line 1767742
    iput p0, v7, LX/5Ry;->c:F

    .line 1767743
    move-object v7, v7

    .line 1767744
    iput v8, v7, LX/5Ry;->d:F

    .line 1767745
    move-object v8, v7

    .line 1767746
    if-nez v2, :cond_d

    if-nez v3, :cond_d

    sget-object v7, Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;->a:LX/434;

    .line 1767747
    :goto_8
    iput-object v7, v8, LX/5Ry;->e:LX/434;

    .line 1767748
    move-object v7, v8

    .line 1767749
    invoke-virtual {v7}, LX/5Ry;->a()Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;

    move-result-object v7

    .line 1767750
    iput-object v7, p2, LX/5Rw;->m:Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;

    .line 1767751
    move-object v7, p2

    .line 1767752
    iput-boolean v9, v7, LX/5Rw;->f:Z

    .line 1767753
    move-object v8, v7

    .line 1767754
    if-nez v3, :cond_e

    if-nez v2, :cond_e

    if-nez v4, :cond_e

    move v7, v9

    .line 1767755
    :goto_9
    iput-boolean v7, v8, LX/5Rw;->n:Z

    .line 1767756
    move-object v7, v8

    .line 1767757
    invoke-virtual {v7}, LX/5Rw;->a()Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;

    move-result-object v7

    invoke-static {p1, v5, v7}, LX/5Rs;->a(Landroid/content/Context;Ljava/lang/String;Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;)Landroid/content/Intent;

    move-result-object v8

    .line 1767758
    iget-object v7, v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->H:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/facebook/content/SecureContextHelper;

    const/16 v9, 0x4d9

    invoke-interface {v7, v8, v9, v0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    goto/16 :goto_1

    .line 1767759
    :cond_8
    if-eqz v4, :cond_9

    .line 1767760
    const v7, 0x3f0f5c29    # 0.56f

    move p0, v7

    goto/16 :goto_6

    .line 1767761
    :cond_9
    const v7, 0x3f666666    # 0.9f

    move p0, v7

    goto/16 :goto_6

    .line 1767762
    :cond_a
    if-eqz v2, :cond_b

    .line 1767763
    const v7, 0x3fe38e39

    move v8, v7

    goto/16 :goto_7

    .line 1767764
    :cond_b
    if-eqz v4, :cond_c

    .line 1767765
    const v7, 0x3fe3d70a    # 1.78f

    move v8, v7

    goto/16 :goto_7

    .line 1767766
    :cond_c
    const/high16 v7, 0x3f800000    # 1.0f

    move v8, v7

    goto/16 :goto_7

    .line 1767767
    :cond_d
    sget-object v7, Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;->b:LX/434;

    goto :goto_8

    :cond_e
    move v7, v10

    goto :goto_9

    .line 1767768
    :cond_f
    if-nez v2, :cond_10

    if-eqz v3, :cond_11

    .line 1767769
    :cond_10
    const/high16 v7, 0x3f000000    # 0.5f

    move p0, v7

    .line 1767770
    :goto_a
    if-eqz v3, :cond_13

    .line 1767771
    const v7, 0x3ff49603

    move v8, v7

    .line 1767772
    :goto_b
    invoke-virtual {v1, v10}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/facebook/ipc/media/MediaItem;

    .line 1767773
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p1

    new-instance p2, LX/5Rw;

    invoke-direct {p2}, LX/5Rw;-><init>()V

    invoke-virtual {v7}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v7}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v7

    .line 1767774
    iget-object v1, v7, Lcom/facebook/ipc/media/data/MediaData;->mId:Ljava/lang/String;

    move-object v7, v1

    .line 1767775
    invoke-virtual {p2, v6, v7}, LX/5Rw;->a(Landroid/net/Uri;Ljava/lang/String;)LX/5Rw;

    move-result-object v7

    sget-object p2, LX/5Rr;->CROP:LX/5Rr;

    invoke-virtual {v7, p2}, LX/5Rw;->a(LX/5Rr;)LX/5Rw;

    move-result-object v7

    sget-object p2, LX/5Rr;->DOODLE:LX/5Rr;

    invoke-virtual {v7, p2}, LX/5Rw;->b(LX/5Rr;)LX/5Rw;

    move-result-object v7

    sget-object p2, LX/5Rr;->TEXT:LX/5Rr;

    invoke-virtual {v7, p2}, LX/5Rw;->b(LX/5Rr;)LX/5Rw;

    move-result-object v7

    sget-object p2, LX/5Rr;->STICKER:LX/5Rr;

    invoke-virtual {v7, p2}, LX/5Rw;->b(LX/5Rr;)LX/5Rw;

    move-result-object v7

    sget-object p2, LX/5Rr;->FILTER:LX/5Rr;

    invoke-virtual {v7, p2}, LX/5Rw;->b(LX/5Rr;)LX/5Rw;

    move-result-object v7

    sget-object p2, LX/5Rq;->ZOOM_CROP:LX/5Rq;

    invoke-virtual {v7, p2}, LX/5Rw;->a(LX/5Rq;)LX/5Rw;

    move-result-object p2

    new-instance v7, LX/5Ry;

    invoke-direct {v7}, LX/5Ry;-><init>()V

    .line 1767776
    iput p0, v7, LX/5Ry;->c:F

    .line 1767777
    move-object v7, v7

    .line 1767778
    iput v8, v7, LX/5Ry;->d:F

    .line 1767779
    move-object v8, v7

    .line 1767780
    if-nez v2, :cond_16

    if-nez v3, :cond_16

    sget-object v7, Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;->a:LX/434;

    .line 1767781
    :goto_c
    iput-object v7, v8, LX/5Ry;->e:LX/434;

    .line 1767782
    move-object v7, v8

    .line 1767783
    invoke-virtual {v7}, LX/5Ry;->a()Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;

    move-result-object v7

    .line 1767784
    iput-object v7, p2, LX/5Rw;->m:Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;

    .line 1767785
    move-object v7, p2

    .line 1767786
    iput-boolean v9, v7, LX/5Rw;->f:Z

    .line 1767787
    move-object v8, v7

    .line 1767788
    if-nez v3, :cond_17

    if-nez v2, :cond_17

    if-nez v4, :cond_17

    move v7, v9

    .line 1767789
    :goto_d
    iput-boolean v7, v8, LX/5Rw;->n:Z

    .line 1767790
    move-object v7, v8

    .line 1767791
    invoke-virtual {v7}, LX/5Rw;->a()Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;

    move-result-object v7

    invoke-static {p1, v5, v7}, LX/5Rs;->a(Landroid/content/Context;Ljava/lang/String;Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;)Landroid/content/Intent;

    move-result-object v8

    .line 1767792
    iget-object v7, v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->H:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/facebook/content/SecureContextHelper;

    const/16 v9, 0x4d9

    invoke-interface {v7, v8, v9, v0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    goto/16 :goto_2

    .line 1767793
    :cond_11
    if-eqz v4, :cond_12

    .line 1767794
    const v7, 0x3f0f5c29    # 0.56f

    move p0, v7

    goto/16 :goto_a

    .line 1767795
    :cond_12
    const v7, 0x3f666666    # 0.9f

    move p0, v7

    goto/16 :goto_a

    .line 1767796
    :cond_13
    if-eqz v2, :cond_14

    .line 1767797
    const v7, 0x3fe38e39

    move v8, v7

    goto/16 :goto_b

    .line 1767798
    :cond_14
    if-eqz v4, :cond_15

    .line 1767799
    const v7, 0x3fe3d70a    # 1.78f

    move v8, v7

    goto/16 :goto_b

    .line 1767800
    :cond_15
    const/high16 v7, 0x3f800000    # 1.0f

    move v8, v7

    goto/16 :goto_b

    .line 1767801
    :cond_16
    sget-object v7, Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;->b:LX/434;

    goto :goto_c

    :cond_17
    move v7, v10

    goto :goto_d

    .line 1767802
    :cond_18
    if-nez v2, :cond_19

    if-eqz v3, :cond_1a

    .line 1767803
    :cond_19
    const/high16 v7, 0x3f000000    # 0.5f

    move p0, v7

    .line 1767804
    :goto_e
    if-eqz v3, :cond_1c

    .line 1767805
    const v7, 0x3ff49603

    move v8, v7

    .line 1767806
    :goto_f
    invoke-virtual {v1, v10}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/facebook/ipc/media/MediaItem;

    .line 1767807
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p1

    new-instance p2, LX/5Rw;

    invoke-direct {p2}, LX/5Rw;-><init>()V

    invoke-virtual {v7}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v7}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v7

    .line 1767808
    iget-object v1, v7, Lcom/facebook/ipc/media/data/MediaData;->mId:Ljava/lang/String;

    move-object v7, v1

    .line 1767809
    invoke-virtual {p2, v6, v7}, LX/5Rw;->a(Landroid/net/Uri;Ljava/lang/String;)LX/5Rw;

    move-result-object v7

    sget-object p2, LX/5Rr;->CROP:LX/5Rr;

    invoke-virtual {v7, p2}, LX/5Rw;->a(LX/5Rr;)LX/5Rw;

    move-result-object v7

    sget-object p2, LX/5Rr;->DOODLE:LX/5Rr;

    invoke-virtual {v7, p2}, LX/5Rw;->b(LX/5Rr;)LX/5Rw;

    move-result-object v7

    sget-object p2, LX/5Rr;->TEXT:LX/5Rr;

    invoke-virtual {v7, p2}, LX/5Rw;->b(LX/5Rr;)LX/5Rw;

    move-result-object v7

    sget-object p2, LX/5Rr;->STICKER:LX/5Rr;

    invoke-virtual {v7, p2}, LX/5Rw;->b(LX/5Rr;)LX/5Rw;

    move-result-object v7

    sget-object p2, LX/5Rr;->FILTER:LX/5Rr;

    invoke-virtual {v7, p2}, LX/5Rw;->b(LX/5Rr;)LX/5Rw;

    move-result-object v7

    sget-object p2, LX/5Rq;->ZOOM_CROP:LX/5Rq;

    invoke-virtual {v7, p2}, LX/5Rw;->a(LX/5Rq;)LX/5Rw;

    move-result-object p2

    new-instance v7, LX/5Ry;

    invoke-direct {v7}, LX/5Ry;-><init>()V

    .line 1767810
    iput p0, v7, LX/5Ry;->c:F

    .line 1767811
    move-object v7, v7

    .line 1767812
    iput v8, v7, LX/5Ry;->d:F

    .line 1767813
    move-object v8, v7

    .line 1767814
    if-nez v2, :cond_1f

    if-nez v3, :cond_1f

    sget-object v7, Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;->a:LX/434;

    .line 1767815
    :goto_10
    iput-object v7, v8, LX/5Ry;->e:LX/434;

    .line 1767816
    move-object v7, v8

    .line 1767817
    invoke-virtual {v7}, LX/5Ry;->a()Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;

    move-result-object v7

    .line 1767818
    iput-object v7, p2, LX/5Rw;->m:Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;

    .line 1767819
    move-object v7, p2

    .line 1767820
    iput-boolean v9, v7, LX/5Rw;->f:Z

    .line 1767821
    move-object v8, v7

    .line 1767822
    if-nez v3, :cond_20

    if-nez v2, :cond_20

    if-nez v4, :cond_20

    move v7, v9

    .line 1767823
    :goto_11
    iput-boolean v7, v8, LX/5Rw;->n:Z

    .line 1767824
    move-object v7, v8

    .line 1767825
    invoke-virtual {v7}, LX/5Rw;->a()Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;

    move-result-object v7

    invoke-static {p1, v5, v7}, LX/5Rs;->a(Landroid/content/Context;Ljava/lang/String;Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;)Landroid/content/Intent;

    move-result-object v8

    .line 1767826
    iget-object v7, v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->H:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/facebook/content/SecureContextHelper;

    const/16 v9, 0x4d9

    invoke-interface {v7, v8, v9, v0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    goto/16 :goto_3

    .line 1767827
    :cond_1a
    if-eqz v4, :cond_1b

    .line 1767828
    const v7, 0x3f0f5c29    # 0.56f

    move p0, v7

    goto/16 :goto_e

    .line 1767829
    :cond_1b
    const v7, 0x3f666666    # 0.9f

    move p0, v7

    goto/16 :goto_e

    .line 1767830
    :cond_1c
    if-eqz v2, :cond_1d

    .line 1767831
    const v7, 0x3fe38e39

    move v8, v7

    goto/16 :goto_f

    .line 1767832
    :cond_1d
    if-eqz v4, :cond_1e

    .line 1767833
    const v7, 0x3fe3d70a    # 1.78f

    move v8, v7

    goto/16 :goto_f

    .line 1767834
    :cond_1e
    const/high16 v7, 0x3f800000    # 1.0f

    move v8, v7

    goto/16 :goto_f

    .line 1767835
    :cond_1f
    sget-object v7, Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;->b:LX/434;

    goto :goto_10

    :cond_20
    move v7, v10

    goto :goto_11

    .line 1767836
    :cond_21
    if-nez v6, :cond_22

    if-eqz v7, :cond_23

    .line 1767837
    :cond_22
    const/high16 v0, 0x3f000000    # 0.5f

    move p0, v0

    .line 1767838
    :goto_12
    if-eqz v7, :cond_25

    .line 1767839
    const v0, 0x3ff49603

    move v1, v0

    .line 1767840
    :goto_13
    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    .line 1767841
    invoke-virtual {v4}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p1

    new-instance p2, LX/5Rw;

    invoke-direct {p2}, LX/5Rw;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v10

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v0

    .line 1767842
    iget-object v5, v0, Lcom/facebook/ipc/media/data/MediaData;->mId:Ljava/lang/String;

    move-object v0, v5

    .line 1767843
    invoke-virtual {p2, v10, v0}, LX/5Rw;->a(Landroid/net/Uri;Ljava/lang/String;)LX/5Rw;

    move-result-object v0

    sget-object p2, LX/5Rr;->CROP:LX/5Rr;

    invoke-virtual {v0, p2}, LX/5Rw;->a(LX/5Rr;)LX/5Rw;

    move-result-object v0

    sget-object p2, LX/5Rr;->DOODLE:LX/5Rr;

    invoke-virtual {v0, p2}, LX/5Rw;->b(LX/5Rr;)LX/5Rw;

    move-result-object v0

    sget-object p2, LX/5Rr;->TEXT:LX/5Rr;

    invoke-virtual {v0, p2}, LX/5Rw;->b(LX/5Rr;)LX/5Rw;

    move-result-object v0

    sget-object p2, LX/5Rr;->STICKER:LX/5Rr;

    invoke-virtual {v0, p2}, LX/5Rw;->b(LX/5Rr;)LX/5Rw;

    move-result-object v0

    sget-object p2, LX/5Rr;->FILTER:LX/5Rr;

    invoke-virtual {v0, p2}, LX/5Rw;->b(LX/5Rr;)LX/5Rw;

    move-result-object v0

    sget-object p2, LX/5Rq;->ZOOM_CROP:LX/5Rq;

    invoke-virtual {v0, p2}, LX/5Rw;->a(LX/5Rq;)LX/5Rw;

    move-result-object p2

    new-instance v0, LX/5Ry;

    invoke-direct {v0}, LX/5Ry;-><init>()V

    .line 1767844
    iput p0, v0, LX/5Ry;->c:F

    .line 1767845
    move-object v0, v0

    .line 1767846
    iput v1, v0, LX/5Ry;->d:F

    .line 1767847
    move-object v1, v0

    .line 1767848
    if-nez v6, :cond_28

    if-nez v7, :cond_28

    sget-object v0, Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;->a:LX/434;

    .line 1767849
    :goto_14
    iput-object v0, v1, LX/5Ry;->e:LX/434;

    .line 1767850
    move-object v0, v1

    .line 1767851
    invoke-virtual {v0}, LX/5Ry;->a()Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;

    move-result-object v0

    .line 1767852
    iput-object v0, p2, LX/5Rw;->m:Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;

    .line 1767853
    move-object v0, p2

    .line 1767854
    iput-boolean v2, v0, LX/5Rw;->f:Z

    .line 1767855
    move-object v1, v0

    .line 1767856
    if-nez v7, :cond_29

    if-nez v6, :cond_29

    if-nez v8, :cond_29

    move v0, v2

    .line 1767857
    :goto_15
    iput-boolean v0, v1, LX/5Rw;->n:Z

    .line 1767858
    move-object v0, v1

    .line 1767859
    invoke-virtual {v0}, LX/5Rw;->a()Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;

    move-result-object v0

    invoke-static {p1, v9, v0}, LX/5Rs;->a(Landroid/content/Context;Ljava/lang/String;Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;)Landroid/content/Intent;

    move-result-object v1

    .line 1767860
    iget-object v0, v4, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->H:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    const/16 v2, 0x4d9

    invoke-interface {v0, v1, v2, v4}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    goto/16 :goto_4

    .line 1767861
    :cond_23
    if-eqz v8, :cond_24

    .line 1767862
    const v0, 0x3f0f5c29    # 0.56f

    move p0, v0

    goto/16 :goto_12

    .line 1767863
    :cond_24
    const v0, 0x3f666666    # 0.9f

    move p0, v0

    goto/16 :goto_12

    .line 1767864
    :cond_25
    if-eqz v6, :cond_26

    .line 1767865
    const v0, 0x3fe38e39

    move v1, v0

    goto/16 :goto_13

    .line 1767866
    :cond_26
    if-eqz v8, :cond_27

    .line 1767867
    const v0, 0x3fe3d70a    # 1.78f

    move v1, v0

    goto/16 :goto_13

    .line 1767868
    :cond_27
    const/high16 v0, 0x3f800000    # 1.0f

    move v1, v0

    goto/16 :goto_13

    .line 1767869
    :cond_28
    sget-object v0, Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;->b:LX/434;

    goto :goto_14

    :cond_29
    move v0, v3

    goto :goto_15

    .line 1767870
    :cond_2a
    if-nez v6, :cond_2b

    if-eqz v7, :cond_2c

    .line 1767871
    :cond_2b
    const/high16 v0, 0x3f000000    # 0.5f

    move p0, v0

    .line 1767872
    :goto_16
    if-eqz v7, :cond_2e

    .line 1767873
    const v0, 0x3ff49603

    move v1, v0

    .line 1767874
    :goto_17
    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    .line 1767875
    invoke-virtual {v4}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p1

    new-instance p2, LX/5Rw;

    invoke-direct {p2}, LX/5Rw;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v10

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v0

    .line 1767876
    iget-object v5, v0, Lcom/facebook/ipc/media/data/MediaData;->mId:Ljava/lang/String;

    move-object v0, v5

    .line 1767877
    invoke-virtual {p2, v10, v0}, LX/5Rw;->a(Landroid/net/Uri;Ljava/lang/String;)LX/5Rw;

    move-result-object v0

    sget-object p2, LX/5Rr;->CROP:LX/5Rr;

    invoke-virtual {v0, p2}, LX/5Rw;->a(LX/5Rr;)LX/5Rw;

    move-result-object v0

    sget-object p2, LX/5Rr;->DOODLE:LX/5Rr;

    invoke-virtual {v0, p2}, LX/5Rw;->b(LX/5Rr;)LX/5Rw;

    move-result-object v0

    sget-object p2, LX/5Rr;->TEXT:LX/5Rr;

    invoke-virtual {v0, p2}, LX/5Rw;->b(LX/5Rr;)LX/5Rw;

    move-result-object v0

    sget-object p2, LX/5Rr;->STICKER:LX/5Rr;

    invoke-virtual {v0, p2}, LX/5Rw;->b(LX/5Rr;)LX/5Rw;

    move-result-object v0

    sget-object p2, LX/5Rr;->FILTER:LX/5Rr;

    invoke-virtual {v0, p2}, LX/5Rw;->b(LX/5Rr;)LX/5Rw;

    move-result-object v0

    sget-object p2, LX/5Rq;->ZOOM_CROP:LX/5Rq;

    invoke-virtual {v0, p2}, LX/5Rw;->a(LX/5Rq;)LX/5Rw;

    move-result-object p2

    new-instance v0, LX/5Ry;

    invoke-direct {v0}, LX/5Ry;-><init>()V

    .line 1767878
    iput p0, v0, LX/5Ry;->c:F

    .line 1767879
    move-object v0, v0

    .line 1767880
    iput v1, v0, LX/5Ry;->d:F

    .line 1767881
    move-object v1, v0

    .line 1767882
    if-nez v6, :cond_31

    if-nez v7, :cond_31

    sget-object v0, Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;->a:LX/434;

    .line 1767883
    :goto_18
    iput-object v0, v1, LX/5Ry;->e:LX/434;

    .line 1767884
    move-object v0, v1

    .line 1767885
    invoke-virtual {v0}, LX/5Ry;->a()Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;

    move-result-object v0

    .line 1767886
    iput-object v0, p2, LX/5Rw;->m:Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;

    .line 1767887
    move-object v0, p2

    .line 1767888
    iput-boolean v2, v0, LX/5Rw;->f:Z

    .line 1767889
    move-object v1, v0

    .line 1767890
    if-nez v7, :cond_32

    if-nez v6, :cond_32

    if-nez v8, :cond_32

    move v0, v2

    .line 1767891
    :goto_19
    iput-boolean v0, v1, LX/5Rw;->n:Z

    .line 1767892
    move-object v0, v1

    .line 1767893
    invoke-virtual {v0}, LX/5Rw;->a()Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;

    move-result-object v0

    invoke-static {p1, v9, v0}, LX/5Rs;->a(Landroid/content/Context;Ljava/lang/String;Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;)Landroid/content/Intent;

    move-result-object v1

    .line 1767894
    iget-object v0, v4, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->H:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    const/16 v2, 0x4d9

    invoke-interface {v0, v1, v2, v4}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    goto/16 :goto_5

    .line 1767895
    :cond_2c
    if-eqz v8, :cond_2d

    .line 1767896
    const v0, 0x3f0f5c29    # 0.56f

    move p0, v0

    goto/16 :goto_16

    .line 1767897
    :cond_2d
    const v0, 0x3f666666    # 0.9f

    move p0, v0

    goto/16 :goto_16

    .line 1767898
    :cond_2e
    if-eqz v6, :cond_2f

    .line 1767899
    const v0, 0x3fe38e39

    move v1, v0

    goto/16 :goto_17

    .line 1767900
    :cond_2f
    if-eqz v8, :cond_30

    .line 1767901
    const v0, 0x3fe3d70a    # 1.78f

    move v1, v0

    goto/16 :goto_17

    .line 1767902
    :cond_30
    const/high16 v0, 0x3f800000    # 1.0f

    move v1, v0

    goto/16 :goto_17

    .line 1767903
    :cond_31
    sget-object v0, Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;->b:LX/434;

    goto :goto_18

    :cond_32
    move v0, v3

    goto :goto_19

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/0Px;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 1767904
    const-string v0, "camera"

    invoke-direct {p0, p1, p2, v0}, LX/BHA;->a(LX/0Px;ZLjava/lang/String;)V

    .line 1767905
    return-void
.end method

.method public final b(LX/0Px;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 1767671
    const-string v0, "upload"

    invoke-direct {p0, p1, p2, v0}, LX/BHA;->a(LX/0Px;ZLjava/lang/String;)V

    .line 1767672
    return-void
.end method
