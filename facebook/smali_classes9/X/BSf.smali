.class public LX/BSf;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Landroid/view/animation/Interpolator;

.field public static final b:Landroid/view/animation/Interpolator;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 1785668
    const v0, 0x3e6b851f    # 0.23f

    const v1, 0x3ea3d70a    # 0.32f

    invoke-static {v0, v2, v1, v2}, LX/2pJ;->a(FFFF)Landroid/view/animation/Interpolator;

    move-result-object v0

    sput-object v0, LX/BSf;->a:Landroid/view/animation/Interpolator;

    .line 1785669
    const v0, 0x3f4147ae    # 0.755f

    const v1, 0x3d4ccccd    # 0.05f

    const v2, 0x3f5ae148    # 0.855f

    const v3, 0x3d75c28f    # 0.06f

    invoke-static {v0, v1, v2, v3}, LX/2pJ;->a(FFFF)Landroid/view/animation/Interpolator;

    move-result-object v0

    sput-object v0, LX/BSf;->b:Landroid/view/animation/Interpolator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1785670
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/4mQ;)LX/4mQ;
    .locals 2

    .prologue
    .line 1785671
    const v0, 0x3e4ccccd    # 0.2f

    .line 1785672
    iput v0, p0, LX/4mQ;->c:F

    .line 1785673
    move-object v0, p0

    .line 1785674
    const v1, 0x3f333333    # 0.7f

    .line 1785675
    iput v1, v0, LX/4mQ;->d:F

    .line 1785676
    move-object v0, v0

    .line 1785677
    const/4 v1, 0x0

    .line 1785678
    iput v1, v0, LX/4mQ;->g:F

    .line 1785679
    move-object v0, v0

    .line 1785680
    return-object v0
.end method
