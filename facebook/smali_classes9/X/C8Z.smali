.class public final LX/C8Z;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

.field public final c:LX/2yF;

.field public final d:LX/C8b;

.field public final e:LX/2yN;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/2yF;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "LX/2yF;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1853112
    sget-object v0, LX/C8b;->ATTACHMENT:LX/C8b;

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, v0, v1}, LX/C8Z;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/2yF;LX/C8b;LX/2yN;)V

    .line 1853113
    return-void
.end method

.method public constructor <init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/2yF;LX/C8b;LX/2yN;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "LX/2yF;",
            "LX/C8b;",
            "LX/2yN",
            "<",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1853104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1853105
    iput-object p1, p0, LX/C8Z;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1853106
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1853107
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    iput-object v0, p0, LX/C8Z;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1853108
    iput-object p2, p0, LX/C8Z;->c:LX/2yF;

    .line 1853109
    iput-object p3, p0, LX/C8Z;->d:LX/C8b;

    .line 1853110
    iput-object p4, p0, LX/C8Z;->e:LX/2yN;

    .line 1853111
    return-void
.end method
