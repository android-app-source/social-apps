.class public final LX/AvD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "LX/0Px",
        "<",
        "LX/1kK;",
        ">;",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/AvE;


# direct methods
.method public constructor <init>(LX/AvE;)V
    .locals 0

    .prologue
    .line 1723880
    iput-object p1, p0, LX/AvD;->a:LX/AvE;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1723881
    check-cast p1, LX/0Px;

    .line 1723882
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1723883
    iget-object v0, p0, LX/AvD;->a:LX/AvE;

    iget-object v0, v0, LX/AvE;->c:LX/1lT;

    invoke-virtual {v0, p1}, LX/1lT;->a(LX/0Px;)LX/0Px;

    move-result-object v0

    .line 1723884
    iget-object v1, p0, LX/AvD;->a:LX/AvE;

    iget-object v1, v1, LX/AvE;->a:LX/AwF;

    iget-object v2, p0, LX/AvD;->a:LX/AvE;

    iget-object v2, v2, LX/AvE;->d:LX/1lV;

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, LX/1lV;->a(LX/0Px;LX/1RN;)LX/0Px;

    move-result-object v0

    const/4 v2, 0x0

    .line 1723885
    new-instance p0, LX/0Pz;

    invoke-direct {p0}, LX/0Pz;-><init>()V

    .line 1723886
    if-nez v0, :cond_0

    .line 1723887
    invoke-static {}, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;->newBuilder()Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel$Builder;->setDefaultInspirationLandingIndex(I)Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;

    move-result-object v2

    .line 1723888
    :goto_0
    move-object v0, v2

    .line 1723889
    return-object v0

    .line 1723890
    :cond_0
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result p1

    move v3, v2

    :goto_1
    if-ge v3, p1, :cond_2

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1RN;

    .line 1723891
    iget-object v2, v2, LX/1RN;->a:LX/1kK;

    check-cast v2, LX/1kW;

    .line 1723892
    iget-object v4, v2, LX/1kW;->a:Lcom/facebook/productionprompts/model/ProductionPrompt;

    move-object v2, v4

    .line 1723893
    invoke-virtual {v2}, Lcom/facebook/productionprompts/model/ProductionPrompt;->D()I

    move-result v4

    .line 1723894
    invoke-static {v1, v2}, LX/AwF;->a(LX/AwF;Lcom/facebook/productionprompts/model/ProductionPrompt;)Lcom/facebook/friendsharing/inspiration/model/InspirationModel;

    move-result-object v2

    .line 1723895
    if-eqz v2, :cond_1

    .line 1723896
    invoke-virtual {p0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1723897
    :cond_1
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v4

    goto :goto_1

    .line 1723898
    :cond_2
    invoke-static {}, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;->newBuilder()Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel$Builder;

    move-result-object v3

    invoke-virtual {p0}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel$Builder;->setInspirationModels(LX/0Px;)Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel$Builder;->setDefaultInspirationLandingIndex(I)Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;

    move-result-object v2

    goto :goto_0
.end method
