.class public final LX/Cam;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7yX;


# instance fields
.field public final synthetic a:LX/Cao;


# direct methods
.method public constructor <init>(LX/Cao;)V
    .locals 0

    .prologue
    .line 1918564
    iput-object p1, p0, LX/Cam;->a:LX/Cao;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/tagging/model/TaggingProfile;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1918565
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1918566
    :cond_0
    :goto_0
    return-void

    .line 1918567
    :cond_1
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 1918568
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/tagging/model/TaggingProfile;

    .line 1918569
    if-eqz v0, :cond_2

    .line 1918570
    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 1918571
    :cond_3
    iget-object v0, p0, LX/Cam;->a:LX/Cao;

    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 1918572
    iput-object v1, v0, LX/Cao;->e:LX/0Px;

    .line 1918573
    iget-object v0, p0, LX/Cam;->a:LX/Cao;

    iget-object v0, v0, LX/Cao;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Can;

    .line 1918574
    iget-object v2, p0, LX/Cam;->a:LX/Cao;

    iget-object v2, v2, LX/Cao;->e:LX/0Px;

    invoke-interface {v0, v2}, LX/Can;->a(LX/0Px;)V

    goto :goto_2

    .line 1918575
    :cond_4
    iget-object v0, p0, LX/Cam;->a:LX/Cao;

    iget-object v0, v0, LX/Cao;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    goto :goto_0
.end method
