.class public LX/BNG;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1778450
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/5tj;Lcom/facebook/graphql/model/GraphQLFeedback;)LX/5tj;
    .locals 12

    .prologue
    .line 1778451
    if-nez p0, :cond_0

    .line 1778452
    const/4 v0, 0x0

    .line 1778453
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/5tu;

    invoke-direct {v0}, LX/5tu;-><init>()V

    invoke-static {p0}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;->a(LX/5tj;)Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;

    move-result-object v0

    invoke-static {v0}, LX/5tu;->a(Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;)LX/5tu;

    move-result-object v0

    const/4 v4, 0x0

    .line 1778454
    if-nez p1, :cond_2

    .line 1778455
    :cond_1
    :goto_1
    move-object v1, v4

    .line 1778456
    invoke-static {v1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel;->a(LX/1VU;)Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel;

    move-result-object v1

    .line 1778457
    iput-object v1, v0, LX/5tu;->c:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel;

    .line 1778458
    move-object v0, v0

    .line 1778459
    invoke-virtual {v0}, LX/5tu;->a()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewWithFeedbackModel;

    move-result-object v0

    goto :goto_0

    .line 1778460
    :cond_2
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1778461
    const/4 v3, 0x0

    .line 1778462
    if-nez p1, :cond_4

    .line 1778463
    :goto_2
    move v3, v3

    .line 1778464
    if-eqz v3, :cond_1

    .line 1778465
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1778466
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1778467
    const/4 v2, 0x0

    invoke-virtual {v3, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1778468
    new-instance v2, LX/15i;

    const/4 v6, 0x1

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1778469
    instance-of v3, p1, Lcom/facebook/flatbuffers/Flattenable;

    if-eqz v3, :cond_3

    .line 1778470
    const-string v3, "DefaultGraphQLConversionHelper.getDefaultFeedbackFields"

    invoke-virtual {v2, v3, p1}, LX/15i;->a(Ljava/lang/String;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1778471
    :cond_3
    new-instance v4, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel;

    invoke-direct {v4, v2}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultFeedbackFieldsModel;-><init>(LX/15i;)V

    goto :goto_1

    .line 1778472
    :cond_4
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1778473
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1778474
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->F()Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    move-result-object v7

    const/4 v8, 0x0

    .line 1778475
    if-nez v7, :cond_5

    .line 1778476
    :goto_3
    move v7, v8

    .line 1778477
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->N()Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    move-result-object v8

    const/4 v9, 0x0

    .line 1778478
    if-nez v8, :cond_6

    .line 1778479
    :goto_4
    move v8, v9

    .line 1778480
    const/16 v9, 0xa

    invoke-virtual {v2, v9}, LX/186;->c(I)V

    .line 1778481
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->b()Z

    move-result v9

    invoke-virtual {v2, v3, v9}, LX/186;->a(IZ)V

    .line 1778482
    const/4 v3, 0x1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->c()Z

    move-result v9

    invoke-virtual {v2, v3, v9}, LX/186;->a(IZ)V

    .line 1778483
    const/4 v3, 0x2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->d()Z

    move-result v9

    invoke-virtual {v2, v3, v9}, LX/186;->a(IZ)V

    .line 1778484
    const/4 v3, 0x3

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->e()Z

    move-result v9

    invoke-virtual {v2, v3, v9}, LX/186;->a(IZ)V

    .line 1778485
    const/4 v3, 0x4

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->r_()Z

    move-result v9

    invoke-virtual {v2, v3, v9}, LX/186;->a(IZ)V

    .line 1778486
    const/4 v3, 0x5

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->s_()Z

    move-result v9

    invoke-virtual {v2, v3, v9}, LX/186;->a(IZ)V

    .line 1778487
    const/4 v3, 0x6

    invoke-virtual {v2, v3, v5}, LX/186;->b(II)V

    .line 1778488
    const/4 v3, 0x7

    invoke-virtual {v2, v3, v6}, LX/186;->b(II)V

    .line 1778489
    const/16 v3, 0x8

    invoke-virtual {v2, v3, v7}, LX/186;->b(II)V

    .line 1778490
    const/16 v3, 0x9

    invoke-virtual {v2, v3, v8}, LX/186;->b(II)V

    .line 1778491
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1778492
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    goto/16 :goto_2

    .line 1778493
    :cond_5
    const/4 v9, 0x1

    invoke-virtual {v2, v9}, LX/186;->c(I)V

    .line 1778494
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;->a()I

    move-result v9

    invoke-virtual {v2, v8, v9, v8}, LX/186;->a(III)V

    .line 1778495
    invoke-virtual {v2}, LX/186;->d()I

    move-result v8

    .line 1778496
    invoke-virtual {v2, v8}, LX/186;->d(I)V

    goto :goto_3

    .line 1778497
    :cond_6
    const/4 v10, 0x2

    invoke-virtual {v2, v10}, LX/186;->c(I)V

    .line 1778498
    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;->a()I

    move-result v10

    invoke-virtual {v2, v9, v10, v9}, LX/186;->a(III)V

    .line 1778499
    const/4 v10, 0x1

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;->b()I

    move-result v11

    invoke-virtual {v2, v10, v11, v9}, LX/186;->a(III)V

    .line 1778500
    invoke-virtual {v2}, LX/186;->d()I

    move-result v9

    .line 1778501
    invoke-virtual {v2, v9}, LX/186;->d(I)V

    goto :goto_4
.end method
