.class public final LX/C6i;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/C6j;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/C6H;

.field public final synthetic b:LX/C6j;


# direct methods
.method public constructor <init>(LX/C6j;)V
    .locals 1

    .prologue
    .line 1850179
    iput-object p1, p0, LX/C6i;->b:LX/C6j;

    .line 1850180
    move-object v0, p1

    .line 1850181
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1850182
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1850178
    const-string v0, "InlineSurveyAcknowledgementComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1850167
    if-ne p0, p1, :cond_1

    .line 1850168
    :cond_0
    :goto_0
    return v0

    .line 1850169
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1850170
    goto :goto_0

    .line 1850171
    :cond_3
    check-cast p1, LX/C6i;

    .line 1850172
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1850173
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1850174
    if-eq v2, v3, :cond_0

    .line 1850175
    iget-object v2, p0, LX/C6i;->a:LX/C6H;

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/C6i;->a:LX/C6H;

    iget-object v3, p1, LX/C6i;->a:LX/C6H;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1850176
    goto :goto_0

    .line 1850177
    :cond_4
    iget-object v2, p1, LX/C6i;->a:LX/C6H;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
