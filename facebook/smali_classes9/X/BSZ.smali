.class public final enum LX/BSZ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/BSZ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/BSZ;

.field public static final enum INTENT:LX/BSZ;

.field public static final enum START:LX/BSZ;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1785554
    new-instance v0, LX/BSZ;

    const-string v1, "INTENT"

    invoke-direct {v0, v1, v2}, LX/BSZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BSZ;->INTENT:LX/BSZ;

    new-instance v0, LX/BSZ;

    const-string v1, "START"

    invoke-direct {v0, v1, v3}, LX/BSZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BSZ;->START:LX/BSZ;

    .line 1785555
    const/4 v0, 0x2

    new-array v0, v0, [LX/BSZ;

    sget-object v1, LX/BSZ;->INTENT:LX/BSZ;

    aput-object v1, v0, v2

    sget-object v1, LX/BSZ;->START:LX/BSZ;

    aput-object v1, v0, v3

    sput-object v0, LX/BSZ;->$VALUES:[LX/BSZ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1785556
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/BSZ;
    .locals 1

    .prologue
    .line 1785557
    const-class v0, LX/BSZ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/BSZ;

    return-object v0
.end method

.method public static values()[LX/BSZ;
    .locals 1

    .prologue
    .line 1785558
    sget-object v0, LX/BSZ;->$VALUES:[LX/BSZ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/BSZ;

    return-object v0
.end method
