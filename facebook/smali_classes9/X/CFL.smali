.class public LX/CFL;
.super LX/AQ9;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "Lcom/facebook/ipc/composer/plugin/ComposerPluginDataProvider;",
        "DerivedData::",
        "Lcom/facebook/ipc/composer/plugin/ComposerPluginDerivedDataProvider;",
        "Mutation::",
        "Lcom/facebook/ipc/composer/plugin/ComposerPluginMutation",
        "<TMutation;>;>",
        "LX/AQ9",
        "<TModelData;TDerivedData;TMutation;>;"
    }
.end annotation


# instance fields
.field public final a:LX/AR2;

.field public final b:LX/AR4;

.field public final c:LX/ARD;

.field public final d:Lcom/facebook/goodwill/composer/GoodwillFriendversaryCardComposerPluginConfig;

.field public final e:LX/0lC;


# direct methods
.method public constructor <init>(LX/B5j;Lcom/facebook/goodwill/composer/GoodwillFriendversaryCardComposerPluginConfig;Landroid/content/Context;LX/AR2;LX/AR4;LX/ARD;LX/0lC;)V
    .locals 0
    .param p1    # LX/B5j;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/goodwill/composer/GoodwillFriendversaryCardComposerPluginConfig;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/B5j",
            "<TModelData;TDerivedData;TMutation;>;",
            "Lcom/facebook/goodwill/composer/GoodwillFriendversaryCardComposerPluginConfig;",
            "Landroid/content/Context;",
            "LX/AR2;",
            "LX/AR4;",
            "LX/ARD;",
            "LX/0lC;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1862988
    invoke-direct {p0, p3, p1}, LX/AQ9;-><init>(Landroid/content/Context;LX/B5j;)V

    .line 1862989
    iput-object p4, p0, LX/CFL;->a:LX/AR2;

    .line 1862990
    iput-object p6, p0, LX/CFL;->c:LX/ARD;

    .line 1862991
    iput-object p5, p0, LX/CFL;->b:LX/AR4;

    .line 1862992
    iput-object p2, p0, LX/CFL;->d:Lcom/facebook/goodwill/composer/GoodwillFriendversaryCardComposerPluginConfig;

    .line 1862993
    iput-object p7, p0, LX/CFL;->e:LX/0lC;

    .line 1862994
    return-void
.end method


# virtual methods
.method public final V()LX/ARN;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 1862995
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method

.method public final aC()LX/ARN;
    .locals 1

    .prologue
    .line 1862996
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method

.method public final aI()LX/ARN;
    .locals 1

    .prologue
    .line 1862997
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method

.method public final aa()LX/ARN;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 1862998
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method

.method public final ac()LX/ARN;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 1862999
    sget-object v0, LX/ARN;->a:LX/ARN;

    return-object v0
.end method

.method public final aj()LX/AQ4;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/AQ4",
            "<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1863000
    new-instance v0, LX/CFJ;

    invoke-direct {v0, p0}, LX/CFJ;-><init>(LX/CFL;)V

    return-object v0
.end method

.method public final aq()LX/ARN;
    .locals 1

    .prologue
    .line 1863001
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method

.method public final i()LX/ARN;
    .locals 1

    .prologue
    .line 1863002
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method
