.class public final LX/Anl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLStory;

.field public final synthetic b:LX/1Po;

.field public final synthetic c:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic d:Lcom/facebook/feedplugins/base/blingbar/BlingBarFlyoutAndVideoLauncherPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/base/blingbar/BlingBarFlyoutAndVideoLauncherPartDefinition;Lcom/facebook/graphql/model/GraphQLStory;LX/1Po;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 0

    .prologue
    .line 1712852
    iput-object p1, p0, LX/Anl;->d:Lcom/facebook/feedplugins/base/blingbar/BlingBarFlyoutAndVideoLauncherPartDefinition;

    iput-object p2, p0, LX/Anl;->a:Lcom/facebook/graphql/model/GraphQLStory;

    iput-object p3, p0, LX/Anl;->b:LX/1Po;

    iput-object p4, p0, LX/Anl;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x660a01dc

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1712853
    iget-object v0, p0, LX/Anl;->d:Lcom/facebook/feedplugins/base/blingbar/BlingBarFlyoutAndVideoLauncherPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/base/blingbar/BlingBarFlyoutAndVideoLauncherPartDefinition;->d:LX/1WR;

    iget-object v2, p0, LX/Anl;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0, v2}, LX/1WR;->d(Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 1712854
    iget-object v0, p0, LX/Anl;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/14w;->q(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v2

    .line 1712855
    iget-object v0, p0, LX/Anl;->b:LX/1Po;

    check-cast v0, LX/1Pe;

    invoke-interface {v0, v2}, LX/1Pe;->c(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1712856
    iget-object v0, p0, LX/Anl;->b:LX/1Po;

    check-cast v0, LX/1Pe;

    invoke-interface {v0, v2}, LX/1Pe;->b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    .line 1712857
    :goto_0
    const v0, 0x4e414119    # 8.1056723E8f

    invoke-static {v0, v1}, LX/02F;->a(II)V

    return-void

    .line 1712858
    :cond_0
    iget-object v0, p0, LX/Anl;->d:Lcom/facebook/feedplugins/base/blingbar/BlingBarFlyoutAndVideoLauncherPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/base/blingbar/BlingBarFlyoutAndVideoLauncherPartDefinition;->a:LX/1EN;

    iget-object v2, p0, LX/Anl;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p0, LX/Anl;->b:LX/1Po;

    invoke-interface {v3}, LX/1Po;->c()LX/1PT;

    move-result-object v3

    sget-object v4, LX/An0;->BLINGBAR:LX/An0;

    invoke-virtual {v0, v2, v3, p1, v4}, LX/1EN;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1PT;Landroid/view/View;LX/An0;)V

    goto :goto_0
.end method
