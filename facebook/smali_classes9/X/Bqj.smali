.class public final LX/Bqj;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/Bqk;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Landroid/net/Uri;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Lcom/facebook/graphql/model/GraphQLStory;

.field public e:Ljava/lang/String;

.field public final synthetic f:LX/Bqk;


# direct methods
.method public constructor <init>(LX/Bqk;)V
    .locals 1

    .prologue
    .line 1825038
    iput-object p1, p0, LX/Bqj;->f:LX/Bqk;

    .line 1825039
    move-object v0, p1

    .line 1825040
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1825041
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1825042
    const-string v0, "StoryAYMTComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1825043
    if-ne p0, p1, :cond_1

    .line 1825044
    :cond_0
    :goto_0
    return v0

    .line 1825045
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1825046
    goto :goto_0

    .line 1825047
    :cond_3
    check-cast p1, LX/Bqj;

    .line 1825048
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1825049
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1825050
    if-eq v2, v3, :cond_0

    .line 1825051
    iget-object v2, p0, LX/Bqj;->a:Landroid/net/Uri;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/Bqj;->a:Landroid/net/Uri;

    iget-object v3, p1, LX/Bqj;->a:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1825052
    goto :goto_0

    .line 1825053
    :cond_5
    iget-object v2, p1, LX/Bqj;->a:Landroid/net/Uri;

    if-nez v2, :cond_4

    .line 1825054
    :cond_6
    iget-object v2, p0, LX/Bqj;->b:Ljava/lang/String;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/Bqj;->b:Ljava/lang/String;

    iget-object v3, p1, LX/Bqj;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1825055
    goto :goto_0

    .line 1825056
    :cond_8
    iget-object v2, p1, LX/Bqj;->b:Ljava/lang/String;

    if-nez v2, :cond_7

    .line 1825057
    :cond_9
    iget-object v2, p0, LX/Bqj;->c:Ljava/lang/String;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/Bqj;->c:Ljava/lang/String;

    iget-object v3, p1, LX/Bqj;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 1825058
    goto :goto_0

    .line 1825059
    :cond_b
    iget-object v2, p1, LX/Bqj;->c:Ljava/lang/String;

    if-nez v2, :cond_a

    .line 1825060
    :cond_c
    iget-object v2, p0, LX/Bqj;->d:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v2, :cond_e

    iget-object v2, p0, LX/Bqj;->d:Lcom/facebook/graphql/model/GraphQLStory;

    iget-object v3, p1, LX/Bqj;->d:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    :cond_d
    move v0, v1

    .line 1825061
    goto :goto_0

    .line 1825062
    :cond_e
    iget-object v2, p1, LX/Bqj;->d:Lcom/facebook/graphql/model/GraphQLStory;

    if-nez v2, :cond_d

    .line 1825063
    :cond_f
    iget-object v2, p0, LX/Bqj;->e:Ljava/lang/String;

    if-eqz v2, :cond_10

    iget-object v2, p0, LX/Bqj;->e:Ljava/lang/String;

    iget-object v3, p1, LX/Bqj;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1825064
    goto :goto_0

    .line 1825065
    :cond_10
    iget-object v2, p1, LX/Bqj;->e:Ljava/lang/String;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
