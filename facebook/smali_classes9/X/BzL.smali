.class public LX/BzL;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/BzM;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/BzL",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/BzM;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1838608
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1838609
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/BzL;->b:LX/0Zi;

    .line 1838610
    iput-object p1, p0, LX/BzL;->a:LX/0Ot;

    .line 1838611
    return-void
.end method

.method public static a(LX/0QB;)LX/BzL;
    .locals 4

    .prologue
    .line 1838584
    const-class v1, LX/BzL;

    monitor-enter v1

    .line 1838585
    :try_start_0
    sget-object v0, LX/BzL;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1838586
    sput-object v2, LX/BzL;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1838587
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1838588
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1838589
    new-instance v3, LX/BzL;

    const/16 p0, 0x1e44

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/BzL;-><init>(LX/0Ot;)V

    .line 1838590
    move-object v0, v3

    .line 1838591
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1838592
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/BzL;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1838593
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1838594
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 10

    .prologue
    .line 1838605
    check-cast p2, LX/BzK;

    .line 1838606
    iget-object v0, p0, LX/BzL;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BzM;

    iget-object v2, p2, LX/BzK;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p2, LX/BzK;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v4, p2, LX/BzK;->c:LX/1Pq;

    iget-object v5, p2, LX/BzK;->d:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iget-object v6, p2, LX/BzK;->e:LX/1eK;

    iget-object v7, p2, LX/BzK;->f:Ljava/lang/String;

    iget v8, p2, LX/BzK;->g:I

    iget v9, p2, LX/BzK;->h:I

    move-object v1, p1

    invoke-virtual/range {v0 .. v9}, LX/BzM;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pq;Lcom/facebook/graphql/model/GraphQLTextWithEntities;LX/1eK;Ljava/lang/String;II)LX/1Dg;

    move-result-object v0

    .line 1838607
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1838603
    invoke-static {}, LX/1dS;->b()V

    .line 1838604
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c(LX/1De;)LX/BzJ;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/BzL",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1838595
    new-instance v1, LX/BzK;

    invoke-direct {v1, p0}, LX/BzK;-><init>(LX/BzL;)V

    .line 1838596
    iget-object v2, p0, LX/BzL;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/BzJ;

    .line 1838597
    if-nez v2, :cond_0

    .line 1838598
    new-instance v2, LX/BzJ;

    invoke-direct {v2, p0}, LX/BzJ;-><init>(LX/BzL;)V

    .line 1838599
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/BzJ;->a$redex0(LX/BzJ;LX/1De;IILX/BzK;)V

    .line 1838600
    move-object v1, v2

    .line 1838601
    move-object v0, v1

    .line 1838602
    return-object v0
.end method
