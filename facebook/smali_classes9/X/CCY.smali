.class public LX/CCY;
.super LX/34o;
.source ""


# static fields
.field private static final f:Lcom/facebook/interstitial/manager/InterstitialTrigger;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1857988
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->FEED_STORY_ONLY_ME_SHARE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    sput-object v0, LX/CCY;->f:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LX/0Rf;LX/03V;LX/16H;LX/0iA;)V
    .locals 6
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/0Rf;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/16H;",
            "LX/0iA;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1857989
    move-object v0, p0

    move-object v1, p3

    move-object v2, p2

    move-object v3, p4

    move-object v4, p5

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, LX/34o;-><init>(LX/03V;LX/0Rf;LX/16H;LX/0iA;Ljava/lang/String;)V

    .line 1857990
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/FeedUnit;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1857991
    invoke-virtual {p0, p1}, LX/34o;->d(Lcom/facebook/graphql/model/FeedUnit;)LX/0am;

    move-result-object v0

    .line 1857992
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1857993
    :goto_0
    return v0

    .line 1857994
    :cond_0
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1857995
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aD()Lcom/facebook/graphql/model/GraphQLStorySaveInfo;

    move-result-object v0

    .line 1857996
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStorySaveInfo;->m()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSavedState;->NOT_SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-eq v0, v2, :cond_2

    :cond_1
    move v0, v1

    .line 1857997
    goto :goto_0

    .line 1857998
    :cond_2
    iget-object v0, p0, LX/34o;->e:LX/0iA;

    sget-object v2, LX/CCY;->f:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    const-class v3, LX/1YV;

    invoke-virtual {v0, v2, v3}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v0

    .line 1857999
    if-eqz v0, :cond_3

    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final b(Lcom/facebook/graphql/model/FeedUnit;)V
    .locals 2

    .prologue
    .line 1858000
    iget-object v0, p0, LX/34o;->e:LX/0iA;

    invoke-virtual {v0}, LX/0iA;->a()Lcom/facebook/interstitial/manager/InterstitialLogger;

    move-result-object v0

    const-string v1, "3909"

    invoke-virtual {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialLogger;->a(Ljava/lang/String;)V

    .line 1858001
    return-void
.end method
