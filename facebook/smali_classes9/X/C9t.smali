.class public LX/C9t;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0qn;

.field private final b:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

.field private final c:LX/0ad;

.field public final d:LX/9A1;

.field public final e:LX/1CJ;

.field public final f:LX/16I;


# direct methods
.method public constructor <init>(LX/0ad;LX/0qn;Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;LX/9A1;LX/1CJ;LX/16I;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1854717
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1854718
    iput-object p1, p0, LX/C9t;->c:LX/0ad;

    .line 1854719
    iput-object p2, p0, LX/C9t;->a:LX/0qn;

    .line 1854720
    iput-object p3, p0, LX/C9t;->b:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    .line 1854721
    iput-object p4, p0, LX/C9t;->d:LX/9A1;

    .line 1854722
    iput-object p5, p0, LX/C9t;->e:LX/1CJ;

    .line 1854723
    iput-object p6, p0, LX/C9t;->f:LX/16I;

    .line 1854724
    return-void
.end method

.method public static b(LX/0QB;)LX/C9t;
    .locals 7

    .prologue
    .line 1854685
    new-instance v0, LX/C9t;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v1

    check-cast v1, LX/0ad;

    invoke-static {p0}, LX/0qn;->a(LX/0QB;)LX/0qn;

    move-result-object v2

    check-cast v2, LX/0qn;

    invoke-static {p0}, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->a(LX/0QB;)Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    move-result-object v3

    check-cast v3, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    invoke-static {p0}, LX/9A1;->a(LX/0QB;)LX/9A1;

    move-result-object v4

    check-cast v4, LX/9A1;

    invoke-static {p0}, LX/1CJ;->a(LX/0QB;)LX/1CJ;

    move-result-object v5

    check-cast v5, LX/1CJ;

    invoke-static {p0}, LX/16I;->a(LX/0QB;)LX/16I;

    move-result-object v6

    check-cast v6, LX/16I;

    invoke-direct/range {v0 .. v6}, LX/C9t;-><init>(LX/0ad;LX/0qn;Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;LX/9A1;LX/1CJ;LX/16I;)V

    .line 1854686
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1854705
    if-eqz p1, :cond_0

    invoke-static {p1}, LX/17E;->k(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v1

    .line 1854706
    :goto_0
    return v0

    .line 1854707
    :cond_1
    iget-object v0, p0, LX/C9t;->b:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->S()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->d(Ljava/lang/String;)Lcom/facebook/composer/publish/common/PendingStory;

    move-result-object v4

    .line 1854708
    if-nez v4, :cond_2

    move v0, v1

    .line 1854709
    goto :goto_0

    .line 1854710
    :cond_2
    if-eqz v4, :cond_5

    invoke-virtual {v4}, Lcom/facebook/composer/publish/common/PendingStory;->g()Lcom/facebook/composer/publish/common/ErrorDetails;

    move-result-object v0

    .line 1854711
    :goto_1
    iget-object v3, p0, LX/C9t;->a:LX/0qn;

    invoke-virtual {v3, p1}, LX/0qn;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    move-result-object v3

    .line 1854712
    sget-object v5, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->FAILED:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    if-eq v3, v5, :cond_3

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->TRANSCODING_FAILED:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    if-ne v3, v5, :cond_6

    :cond_3
    move v3, v2

    .line 1854713
    :goto_2
    if-eqz v3, :cond_7

    if-eqz v0, :cond_4

    iget-boolean v0, v0, Lcom/facebook/composer/publish/common/ErrorDetails;->isRetriable:Z

    if-eqz v0, :cond_7

    :cond_4
    invoke-virtual {v4}, Lcom/facebook/composer/publish/common/PendingStory;->k()Z

    move-result v0

    if-nez v0, :cond_7

    move v0, v2

    goto :goto_0

    .line 1854714
    :cond_5
    const/4 v0, 0x0

    goto :goto_1

    :cond_6
    move v3, v1

    .line 1854715
    goto :goto_2

    :cond_7
    move v0, v1

    .line 1854716
    goto :goto_0
.end method

.method public final c(Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 9

    .prologue
    .line 1854687
    iget-object v0, p0, LX/C9t;->b:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    if-nez v0, :cond_1

    .line 1854688
    :cond_0
    :goto_0
    return-void

    .line 1854689
    :cond_1
    iget-object v0, p0, LX/C9t;->b:Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->S()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/composer/publish/cache/pendingstory/PendingStoryStore;->d(Ljava/lang/String;)Lcom/facebook/composer/publish/common/PendingStory;

    move-result-object v0

    .line 1854690
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/composer/publish/common/PendingStory;->g()Lcom/facebook/composer/publish/common/ErrorDetails;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1854691
    iget-object v2, p0, LX/C9t;->e:LX/1CJ;

    invoke-virtual {v2, p1}, LX/1CJ;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/4Zb;

    move-result-object v4

    .line 1854692
    iget-object v2, p0, LX/C9t;->f:LX/16I;

    invoke-virtual {v2}, LX/16I;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1854693
    iget-boolean v2, v4, LX/4Zb;->a:Z

    move v2, v2

    .line 1854694
    if-nez v2, :cond_3

    const-wide/16 v2, 0x0

    .line 1854695
    :goto_1
    iget-object v5, p0, LX/C9t;->d:LX/9A1;

    .line 1854696
    const/4 v6, 0x2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->S()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, LX/9A1;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v6

    .line 1854697
    new-instance v7, Landroid/os/Bundle;

    const/4 v8, 0x1

    invoke-direct {v7, v8}, Landroid/os/Bundle;-><init>(I)V

    .line 1854698
    const-string v8, "story_key"

    invoke-static {v7, v8, p1}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1854699
    invoke-virtual {v6, v7}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 1854700
    invoke-virtual {v5, v6, v2, v3}, LX/9A1;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 1854701
    const/4 v2, 0x1

    .line 1854702
    iput-boolean v2, v4, LX/4Zb;->a:Z

    .line 1854703
    :cond_2
    goto :goto_0

    .line 1854704
    :cond_3
    const-wide/32 v2, 0x493e0

    goto :goto_1
.end method
