.class public final LX/BoP;
.super LX/1wH;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1wH",
        "<",
        "Lcom/facebook/graphql/model/HideableUnit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic b:LX/1dt;


# direct methods
.method public constructor <init>(LX/1dt;)V
    .locals 0

    .prologue
    .line 1821714
    iput-object p1, p0, LX/BoP;->b:LX/1dt;

    invoke-direct {p0, p1}, LX/1wH;-><init>(LX/1SX;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/Menu;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/HideableUnit;",
            ">;",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1821715
    iget-object v0, p0, LX/BoP;->b:LX/1dt;

    .line 1821716
    const v1, 0x7f081044

    invoke-interface {p1, v1}, Landroid/view/Menu;->add(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 1821717
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->HIDE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    invoke-static {v1}, LX/Al4;->a(Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;)I

    move-result v3

    .line 1821718
    iget-object v1, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 1821719
    check-cast v1, Lcom/facebook/graphql/model/FeedUnit;

    invoke-virtual {v0, v2, v3, v1}, LX/1SX;->a(Landroid/view/MenuItem;ILcom/facebook/graphql/model/FeedUnit;)V

    .line 1821720
    new-instance v1, LX/BoA;

    invoke-direct {v1, v0, p2, p3}, LX/BoA;-><init>(LX/1dt;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;)V

    invoke-interface {v2, v1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1821721
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1821722
    instance-of v0, v0, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    if-eqz v0, :cond_2

    .line 1821723
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1821724
    check-cast v0, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    .line 1821725
    invoke-static {v0}, LX/25C;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)LX/0Px;

    move-result-object v1

    invoke-interface {v0}, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;->I_()I

    move-result v0

    invoke-virtual {v1, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;

    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 1821726
    iget-object v0, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1821727
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    invoke-virtual {p0, v0}, LX/1wH;->a(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1821728
    invoke-virtual {p0, p1, v1, p3}, LX/1wH;->b(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;)V

    .line 1821729
    :cond_0
    invoke-virtual {p0, v1}, LX/1wH;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1821730
    invoke-virtual {p3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0, p1, v1, v0}, LX/1wH;->a(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/content/Context;)V

    .line 1821731
    :cond_1
    invoke-super {p0, p1, p2, p3}, LX/1wH;->a(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;)V

    .line 1821732
    :cond_2
    return-void
.end method
