.class public final LX/BuO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3IA;
.implements LX/3IB;


# instance fields
.field public final synthetic a:LX/BuP;


# direct methods
.method public constructor <init>(LX/BuP;)V
    .locals 0

    .prologue
    .line 1830960
    iput-object p1, p0, LX/BuO;->a:LX/BuP;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 1830929
    iget-object v0, p0, LX/BuO;->a:LX/BuP;

    iget-object v0, v0, LX/2oy;->i:LX/2oj;

    if-nez v0, :cond_0

    .line 1830930
    :goto_0
    return-void

    .line 1830931
    :cond_0
    iget-object v0, p0, LX/BuO;->a:LX/BuP;

    iget-boolean v0, v0, LX/BuP;->d:Z

    if-eqz v0, :cond_1

    .line 1830932
    iget-object v0, p0, LX/BuO;->a:LX/BuP;

    iget-object v0, v0, LX/2oy;->i:LX/2oj;

    new-instance v1, LX/7M2;

    sget-object v2, LX/7M1;->TOGGLE:LX/7M1;

    invoke-direct {v1, v2}, LX/7M2;-><init>(LX/7M1;)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    .line 1830933
    :goto_1
    new-instance v0, LX/7Lr;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, LX/7Lr;-><init>(I)V

    .line 1830934
    iget-object v1, p0, LX/BuO;->a:LX/BuP;

    iget-object v1, v1, LX/2oy;->i:LX/2oj;

    invoke-virtual {v1, v0}, LX/2oj;->a(LX/2ol;)V

    goto :goto_0

    .line 1830935
    :cond_1
    iget-object v0, p0, LX/BuO;->a:LX/BuP;

    iget-object v0, v0, LX/2oy;->i:LX/2oj;

    new-instance v1, LX/7Lw;

    sget-object v2, LX/7MP;->AUTO:LX/7MP;

    invoke-direct {v1, v2}, LX/7Lw;-><init>(LX/7MP;)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    goto :goto_1
.end method

.method public final a(FF)V
    .locals 2

    .prologue
    .line 1830956
    iget-object v0, p0, LX/BuO;->a:LX/BuP;

    iget-object v0, v0, LX/2oy;->i:LX/2oj;

    if-nez v0, :cond_0

    .line 1830957
    :goto_0
    return-void

    .line 1830958
    :cond_0
    new-instance v0, LX/7Lr;

    const/4 v1, 0x1

    invoke-direct {v0, v1, p1, p2}, LX/7Lr;-><init>(IFF)V

    .line 1830959
    iget-object v1, p0, LX/BuO;->a:LX/BuP;

    iget-object v1, v1, LX/2oy;->i:LX/2oj;

    invoke-virtual {v1, v0}, LX/2oj;->a(LX/2ol;)V

    goto :goto_0
.end method

.method public final a(F)Z
    .locals 3

    .prologue
    .line 1830951
    iget-object v0, p0, LX/BuO;->a:LX/BuP;

    iget-object v0, v0, LX/2oy;->i:LX/2oj;

    if-nez v0, :cond_0

    .line 1830952
    const/4 v0, 0x0

    .line 1830953
    :goto_0
    return v0

    .line 1830954
    :cond_0
    iget-object v0, p0, LX/BuO;->a:LX/BuP;

    iget-object v0, v0, LX/2oy;->i:LX/2oj;

    new-instance v1, LX/7Lq;

    const/4 v2, 0x2

    invoke-direct {v1, v2, p1}, LX/7Lq;-><init>(IF)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    .line 1830955
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1830947
    iget-object v0, p0, LX/BuO;->a:LX/BuP;

    iget-object v0, v0, LX/2oy;->i:LX/2oj;

    if-nez v0, :cond_0

    .line 1830948
    :goto_0
    return-void

    .line 1830949
    :cond_0
    new-instance v0, LX/7Lr;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, LX/7Lr;-><init>(I)V

    .line 1830950
    iget-object v1, p0, LX/BuO;->a:LX/BuP;

    iget-object v1, v1, LX/2oy;->i:LX/2oj;

    invoke-virtual {v1, v0}, LX/2oj;->a(LX/2ol;)V

    goto :goto_0
.end method

.method public final b(FF)V
    .locals 2

    .prologue
    .line 1830943
    iget-object v0, p0, LX/BuO;->a:LX/BuP;

    iget-object v0, v0, LX/2oy;->i:LX/2oj;

    if-nez v0, :cond_0

    .line 1830944
    :goto_0
    return-void

    .line 1830945
    :cond_0
    new-instance v0, LX/7Lr;

    const/4 v1, 0x2

    invoke-direct {v0, v1, p1, p2}, LX/7Lr;-><init>(IFF)V

    .line 1830946
    iget-object v1, p0, LX/BuO;->a:LX/BuP;

    iget-object v1, v1, LX/2oy;->i:LX/2oj;

    invoke-virtual {v1, v0}, LX/2oj;->a(LX/2ol;)V

    goto :goto_0
.end method

.method public final c()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 1830939
    iget-object v1, p0, LX/BuO;->a:LX/BuP;

    iget-object v1, v1, LX/2oy;->i:LX/2oj;

    if-nez v1, :cond_0

    .line 1830940
    const/4 v0, 0x0

    .line 1830941
    :goto_0
    return v0

    .line 1830942
    :cond_0
    iget-object v1, p0, LX/BuO;->a:LX/BuP;

    iget-object v1, v1, LX/2oy;->i:LX/2oj;

    new-instance v2, LX/7Lq;

    invoke-direct {v2, v0}, LX/7Lq;-><init>(I)V

    invoke-virtual {v1, v2}, LX/2oj;->a(LX/2ol;)V

    goto :goto_0
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 1830936
    iget-object v0, p0, LX/BuO;->a:LX/BuP;

    iget-object v0, v0, LX/2oy;->i:LX/2oj;

    if-nez v0, :cond_0

    .line 1830937
    :goto_0
    return-void

    .line 1830938
    :cond_0
    iget-object v0, p0, LX/BuO;->a:LX/BuP;

    iget-object v0, v0, LX/2oy;->i:LX/2oj;

    new-instance v1, LX/7Lq;

    const/4 v2, 0x3

    invoke-direct {v1, v2}, LX/7Lq;-><init>(I)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    goto :goto_0
.end method
