.class public LX/Bfc;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Landroid/view/View;",
        ":",
        "LX/BeO;",
        ">",
        "Lcom/facebook/widget/CustomLinearLayout;"
    }
.end annotation


# instance fields
.field public final a:I

.field public b:I

.field public c:Landroid/view/View;

.field public d:Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field public e:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

.field public f:I

.field public g:I


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 1806388
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1806389
    iput v0, p0, LX/Bfc;->f:I

    .line 1806390
    iput v0, p0, LX/Bfc;->g:I

    .line 1806391
    const v0, 0x7f030f74

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1806392
    invoke-virtual {p0, v2}, LX/Bfc;->setOrientation(I)V

    .line 1806393
    const v0, 0x7f0d253e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iput-object v0, p0, LX/Bfc;->e:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 1806394
    const v0, 0x7f0d253f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/Bfc;->c:Landroid/view/View;

    .line 1806395
    invoke-virtual {p0}, LX/Bfc;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b165d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, LX/Bfc;->a:I

    .line 1806396
    iput p2, p0, LX/Bfc;->b:I

    .line 1806397
    iget v0, p0, LX/Bfc;->b:I

    if-ne v0, v2, :cond_0

    .line 1806398
    invoke-virtual {p0}, LX/Bfc;->a()V

    .line 1806399
    :cond_0
    iget v0, p0, LX/Bfc;->b:I

    iget v1, p0, LX/Bfc;->a:I

    mul-int/2addr v0, v1

    invoke-static {p0, v0}, LX/Bfc;->setContainerMargin(LX/Bfc;I)V

    .line 1806400
    return-void
.end method

.method public static d(LX/Bfc;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1806383
    invoke-static {p0}, LX/Bfc;->getContentView(LX/Bfc;)Landroid/view/View;

    move-result-object v0

    invoke-static {v1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {v0, p1, v1}, Landroid/view/View;->measure(II)V

    .line 1806384
    invoke-static {p0}, LX/Bfc;->getContentView(LX/Bfc;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    iput v0, p0, LX/Bfc;->g:I

    .line 1806385
    iget v0, p0, LX/Bfc;->f:I

    if-nez v0, :cond_0

    .line 1806386
    iget v0, p0, LX/Bfc;->g:I

    iput v0, p0, LX/Bfc;->f:I

    .line 1806387
    :cond_0
    return-void
.end method

.method public static getContentView(LX/Bfc;)Landroid/view/View;
    .locals 1

    .prologue
    .line 1806382
    iget-object v0, p0, LX/Bfc;->d:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Bfc;->d:Landroid/view/View;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/Bfc;->e:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    goto :goto_0
.end method

.method public static setContainerMargin(LX/Bfc;I)V
    .locals 3

    .prologue
    .line 1806372
    invoke-virtual {p0}, LX/Bfc;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 1806373
    if-nez v0, :cond_0

    .line 1806374
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 1806375
    :cond_0
    if-ltz p1, :cond_1

    .line 1806376
    iput p1, v0, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 1806377
    iput p1, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 1806378
    iput p1, v0, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    .line 1806379
    const/16 v1, 0x30

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 1806380
    :cond_1
    invoke-virtual {p0, v0}, LX/Bfc;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1806381
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1806370
    iget-object v0, p0, LX/Bfc;->e:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    .line 1806371
    return-void
.end method

.method public setContentHeight(I)V
    .locals 3

    .prologue
    .line 1806363
    invoke-static {p0}, LX/Bfc;->getContentView(LX/Bfc;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 1806364
    if-nez v0, :cond_0

    .line 1806365
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1806366
    :cond_0
    iput p1, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 1806367
    iput p1, p0, LX/Bfc;->f:I

    .line 1806368
    invoke-static {p0}, LX/Bfc;->getContentView(LX/Bfc;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1806369
    return-void
.end method
