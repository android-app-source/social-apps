.class public final LX/AXR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/AV8;

.field public final synthetic b:LX/AXY;


# direct methods
.method public constructor <init>(LX/AXY;LX/AV8;)V
    .locals 0

    .prologue
    .line 1683958
    iput-object p1, p0, LX/AXR;->b:LX/AXY;

    iput-object p2, p0, LX/AXR;->a:LX/AV8;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x767b5502

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1683959
    sget-object v1, LX/AXL;->a:[I

    iget-object v2, p0, LX/AXR;->b:LX/AXY;

    iget-object v2, v2, LX/AXY;->I:Lcom/facebook/facecast/view/FacecastPreviewSaveButton;

    .line 1683960
    iget-object p1, v2, Lcom/facebook/facecast/view/FacecastPreviewSaveButton;->d:LX/Abm;

    move-object v2, p1

    .line 1683961
    invoke-virtual {v2}, LX/Abm;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1683962
    :goto_0
    const v1, 0x381763e0

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 1683963
    :pswitch_0
    iget-object v1, p0, LX/AXR;->a:LX/AV8;

    const/4 v2, 0x0

    .line 1683964
    invoke-virtual {v1}, LX/AV8;->a()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1683965
    :goto_1
    goto :goto_0

    .line 1683966
    :pswitch_1
    iget-object v1, p0, LX/AXR;->b:LX/AXY;

    const v2, 0x7f080cb9

    invoke-static {v1, v2}, LX/AXY;->c(LX/AXY;I)V

    goto :goto_0

    .line 1683967
    :cond_0
    new-instance v3, LX/AV7;

    iget-object v4, v1, LX/AV8;->e:LX/AXF;

    iget-object p0, v1, LX/AV8;->c:Ljava/io/File;

    iget-object p1, v1, LX/AV8;->d:Ljava/io/File;

    invoke-direct {v3, v4, p0, p1}, LX/AV7;-><init>(LX/AXF;Ljava/io/File;Ljava/io/File;)V

    sget-object v4, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v3, v4, v2}, LX/AV7;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 1683968
    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
