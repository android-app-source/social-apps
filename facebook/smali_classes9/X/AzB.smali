.class public final enum LX/AzB;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/AzB;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/AzB;

.field public static final enum ADD_ASSETS:LX/AzB;

.field public static final enum ATTACHED_TO_COMPOSER:LX/AzB;

.field public static final enum EDIT_TITLE:LX/AzB;

.field public static final enum FULLSCREEN_VIEW:LX/AzB;

.field public static final enum PICKER_NUX_DISMISSED:LX/AzB;

.field public static final enum PICKER_NUX_SEEN:LX/AzB;

.field public static final enum PICKER_PREVIEWS_LOADED:LX/AzB;

.field public static final enum PICKER_PREVIEW_SEEN:LX/AzB;

.field public static final enum PICKER_PREVIEW_TAPPED:LX/AzB;

.field public static final enum PROMPT_CLOSED:LX/AzB;

.field public static final enum PROMPT_DISPLAYED:LX/AzB;

.field public static final enum PROMPT_TAPPED:LX/AzB;

.field public static final enum REMOVE_ASSETS:LX/AzB;

.field public static final enum SWAP_ASSET:LX/AzB;


# instance fields
.field private final name:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1732224
    new-instance v0, LX/AzB;

    const-string v1, "PICKER_PREVIEWS_LOADED"

    const-string v2, "magic_stories_kit_previews_loaded"

    invoke-direct {v0, v1, v4, v2}, LX/AzB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/AzB;->PICKER_PREVIEWS_LOADED:LX/AzB;

    .line 1732225
    new-instance v0, LX/AzB;

    const-string v1, "PICKER_PREVIEW_SEEN"

    const-string v2, "magic_stories_kit_preview_seen"

    invoke-direct {v0, v1, v5, v2}, LX/AzB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/AzB;->PICKER_PREVIEW_SEEN:LX/AzB;

    .line 1732226
    new-instance v0, LX/AzB;

    const-string v1, "PICKER_PREVIEW_TAPPED"

    const-string v2, "magic_stories_kit_preview_tapped"

    invoke-direct {v0, v1, v6, v2}, LX/AzB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/AzB;->PICKER_PREVIEW_TAPPED:LX/AzB;

    .line 1732227
    new-instance v0, LX/AzB;

    const-string v1, "ATTACHED_TO_COMPOSER"

    const-string v2, "magic_stories_kit_attached_to_composer"

    invoke-direct {v0, v1, v7, v2}, LX/AzB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/AzB;->ATTACHED_TO_COMPOSER:LX/AzB;

    .line 1732228
    new-instance v0, LX/AzB;

    const-string v1, "FULLSCREEN_VIEW"

    const-string v2, "magic_stories_kit_view"

    invoke-direct {v0, v1, v8, v2}, LX/AzB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/AzB;->FULLSCREEN_VIEW:LX/AzB;

    .line 1732229
    new-instance v0, LX/AzB;

    const-string v1, "ADD_ASSETS"

    const/4 v2, 0x5

    const-string v3, "magic_stories_kit_add_assets"

    invoke-direct {v0, v1, v2, v3}, LX/AzB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/AzB;->ADD_ASSETS:LX/AzB;

    .line 1732230
    new-instance v0, LX/AzB;

    const-string v1, "REMOVE_ASSETS"

    const/4 v2, 0x6

    const-string v3, "magic_stories_kit_hide_asset"

    invoke-direct {v0, v1, v2, v3}, LX/AzB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/AzB;->REMOVE_ASSETS:LX/AzB;

    .line 1732231
    new-instance v0, LX/AzB;

    const-string v1, "EDIT_TITLE"

    const/4 v2, 0x7

    const-string v3, "magic_stories_kit_edit_title"

    invoke-direct {v0, v1, v2, v3}, LX/AzB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/AzB;->EDIT_TITLE:LX/AzB;

    .line 1732232
    new-instance v0, LX/AzB;

    const-string v1, "PICKER_NUX_SEEN"

    const/16 v2, 0x8

    const-string v3, "magic_stories_kit_picker_nux_seen"

    invoke-direct {v0, v1, v2, v3}, LX/AzB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/AzB;->PICKER_NUX_SEEN:LX/AzB;

    .line 1732233
    new-instance v0, LX/AzB;

    const-string v1, "PICKER_NUX_DISMISSED"

    const/16 v2, 0x9

    const-string v3, "magic_stories_kit_picker_nux_dismissed"

    invoke-direct {v0, v1, v2, v3}, LX/AzB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/AzB;->PICKER_NUX_DISMISSED:LX/AzB;

    .line 1732234
    new-instance v0, LX/AzB;

    const-string v1, "SWAP_ASSET"

    const/16 v2, 0xa

    const-string v3, "magic_stories_kit_swap_asset"

    invoke-direct {v0, v1, v2, v3}, LX/AzB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/AzB;->SWAP_ASSET:LX/AzB;

    .line 1732235
    new-instance v0, LX/AzB;

    const-string v1, "PROMPT_CLOSED"

    const/16 v2, 0xb

    const-string v3, "magic_stories_feed_prompt_closed"

    invoke-direct {v0, v1, v2, v3}, LX/AzB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/AzB;->PROMPT_CLOSED:LX/AzB;

    .line 1732236
    new-instance v0, LX/AzB;

    const-string v1, "PROMPT_DISPLAYED"

    const/16 v2, 0xc

    const-string v3, "magic_stories_feed_prompt_displayed"

    invoke-direct {v0, v1, v2, v3}, LX/AzB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/AzB;->PROMPT_DISPLAYED:LX/AzB;

    .line 1732237
    new-instance v0, LX/AzB;

    const-string v1, "PROMPT_TAPPED"

    const/16 v2, 0xd

    const-string v3, "magic_stories_feed_prompt_tapped"

    invoke-direct {v0, v1, v2, v3}, LX/AzB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/AzB;->PROMPT_TAPPED:LX/AzB;

    .line 1732238
    const/16 v0, 0xe

    new-array v0, v0, [LX/AzB;

    sget-object v1, LX/AzB;->PICKER_PREVIEWS_LOADED:LX/AzB;

    aput-object v1, v0, v4

    sget-object v1, LX/AzB;->PICKER_PREVIEW_SEEN:LX/AzB;

    aput-object v1, v0, v5

    sget-object v1, LX/AzB;->PICKER_PREVIEW_TAPPED:LX/AzB;

    aput-object v1, v0, v6

    sget-object v1, LX/AzB;->ATTACHED_TO_COMPOSER:LX/AzB;

    aput-object v1, v0, v7

    sget-object v1, LX/AzB;->FULLSCREEN_VIEW:LX/AzB;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/AzB;->ADD_ASSETS:LX/AzB;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/AzB;->REMOVE_ASSETS:LX/AzB;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/AzB;->EDIT_TITLE:LX/AzB;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/AzB;->PICKER_NUX_SEEN:LX/AzB;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/AzB;->PICKER_NUX_DISMISSED:LX/AzB;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/AzB;->SWAP_ASSET:LX/AzB;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/AzB;->PROMPT_CLOSED:LX/AzB;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/AzB;->PROMPT_DISPLAYED:LX/AzB;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/AzB;->PROMPT_TAPPED:LX/AzB;

    aput-object v2, v0, v1

    sput-object v0, LX/AzB;->$VALUES:[LX/AzB;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1732239
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1732240
    iput-object p3, p0, LX/AzB;->name:Ljava/lang/String;

    .line 1732241
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/AzB;
    .locals 1

    .prologue
    .line 1732242
    const-class v0, LX/AzB;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/AzB;

    return-object v0
.end method

.method public static values()[LX/AzB;
    .locals 1

    .prologue
    .line 1732243
    sget-object v0, LX/AzB;->$VALUES:[LX/AzB;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/AzB;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1732244
    iget-object v0, p0, LX/AzB;->name:Ljava/lang/String;

    return-object v0
.end method
