.class public LX/AvE;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/AwF;

.field private final b:LX/1kQ;

.field public final c:LX/1lT;

.field public final d:LX/1lV;

.field private final e:Ljava/util/concurrent/Executor;

.field public final f:Ljava/util/concurrent/Executor;

.field private final g:LX/0QK;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QK",
            "<",
            "LX/0Px",
            "<",
            "LX/1kK;",
            ">;",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/AwF;LX/1kQ;LX/1lT;LX/1lV;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;)V
    .locals 1
    .param p5    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/SameThreadExecutor;
        .end annotation
    .end param
    .param p6    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1723899
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1723900
    new-instance v0, LX/AvD;

    invoke-direct {v0, p0}, LX/AvD;-><init>(LX/AvE;)V

    iput-object v0, p0, LX/AvE;->g:LX/0QK;

    .line 1723901
    iput-object p1, p0, LX/AvE;->a:LX/AwF;

    .line 1723902
    iput-object p2, p0, LX/AvE;->b:LX/1kQ;

    .line 1723903
    iput-object p3, p0, LX/AvE;->c:LX/1lT;

    .line 1723904
    iput-object p4, p0, LX/AvE;->d:LX/1lV;

    .line 1723905
    iput-object p5, p0, LX/AvE;->e:Ljava/util/concurrent/Executor;

    .line 1723906
    iput-object p6, p0, LX/AvE;->f:Ljava/util/concurrent/Executor;

    .line 1723907
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel;",
            ">;>;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1723908
    iget-object v0, p0, LX/AvE;->b:LX/1kQ;

    invoke-virtual {v0, p1}, LX/1kQ;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iget-object v1, p0, LX/AvE;->g:LX/0QK;

    iget-object v2, p0, LX/AvE;->e:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
