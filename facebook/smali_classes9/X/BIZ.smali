.class public final LX/BIZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/facebook/photos/taggablegallery/PhotoGalleryContent;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1770723
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Landroid/os/Parcel;)Lcom/facebook/photos/taggablegallery/PhotoGalleryContent;
    .locals 1

    .prologue
    .line 1770722
    new-instance v0, Lcom/facebook/photos/taggablegallery/PhotoGalleryContent;

    invoke-direct {v0, p0}, Lcom/facebook/photos/taggablegallery/PhotoGalleryContent;-><init>(Landroid/os/Parcel;)V

    return-object v0
.end method


# virtual methods
.method public final synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1770721
    invoke-static {p1}, LX/BIZ;->a(Landroid/os/Parcel;)Lcom/facebook/photos/taggablegallery/PhotoGalleryContent;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1770719
    new-array v0, p1, [Lcom/facebook/photos/taggablegallery/PhotoGalleryContent;

    move-object v0, v0

    .line 1770720
    return-object v0
.end method
