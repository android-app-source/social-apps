.class public final LX/C3P;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/C3Q;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/C33;

.field public b:LX/1Pb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public c:I

.field public final synthetic d:LX/C3Q;


# direct methods
.method public constructor <init>(LX/C3Q;)V
    .locals 1

    .prologue
    .line 1845610
    iput-object p1, p0, LX/C3P;->d:LX/C3Q;

    .line 1845611
    move-object v0, p1

    .line 1845612
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1845613
    const/4 v0, 0x4

    iput v0, p0, LX/C3P;->c:I

    .line 1845614
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1845631
    const-string v0, "CondensedStoryReshareComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1845615
    if-ne p0, p1, :cond_1

    .line 1845616
    :cond_0
    :goto_0
    return v0

    .line 1845617
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1845618
    goto :goto_0

    .line 1845619
    :cond_3
    check-cast p1, LX/C3P;

    .line 1845620
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1845621
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1845622
    if-eq v2, v3, :cond_0

    .line 1845623
    iget-object v2, p0, LX/C3P;->a:LX/C33;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/C3P;->a:LX/C33;

    iget-object v3, p1, LX/C3P;->a:LX/C33;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1845624
    goto :goto_0

    .line 1845625
    :cond_5
    iget-object v2, p1, LX/C3P;->a:LX/C33;

    if-nez v2, :cond_4

    .line 1845626
    :cond_6
    iget-object v2, p0, LX/C3P;->b:LX/1Pb;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/C3P;->b:LX/1Pb;

    iget-object v3, p1, LX/C3P;->b:LX/1Pb;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1845627
    goto :goto_0

    .line 1845628
    :cond_8
    iget-object v2, p1, LX/C3P;->b:LX/1Pb;

    if-nez v2, :cond_7

    .line 1845629
    :cond_9
    iget v2, p0, LX/C3P;->c:I

    iget v3, p1, LX/C3P;->c:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 1845630
    goto :goto_0
.end method
