.class public final LX/Aua;
.super LX/0xh;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;


# direct methods
.method public constructor <init>(Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;)V
    .locals 0

    .prologue
    .line 1722829
    iput-object p1, p0, LX/Aua;->a:Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;

    invoke-direct {p0}, LX/0xh;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;B)V
    .locals 0

    .prologue
    .line 1722828
    invoke-direct {p0, p1}, LX/Aua;-><init>(Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;)V

    return-void
.end method


# virtual methods
.method public final a(LX/0wd;)V
    .locals 5

    .prologue
    .line 1722819
    iget-object v0, p0, LX/Aua;->a:Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;

    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v2

    double-to-float v1, v2

    .line 1722820
    iput v1, v0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->e:F

    .line 1722821
    iget-object v0, p0, LX/Aua;->a:Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;

    iget-object v1, p0, LX/Aua;->a:Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;

    iget v1, v1, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->j:F

    iget-object v2, p0, LX/Aua;->a:Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;

    iget v2, v2, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->e:F

    iget-object v3, p0, LX/Aua;->a:Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;

    iget v3, v3, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->k:F

    iget-object v4, p0, LX/Aua;->a:Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;

    iget v4, v4, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->j:F

    sub-float/2addr v3, v4

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    .line 1722822
    iput v1, v0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->i:F

    .line 1722823
    iget-object v0, p0, LX/Aua;->a:Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->invalidate()V

    .line 1722824
    return-void
.end method

.method public final b(LX/0wd;)V
    .locals 2

    .prologue
    .line 1722825
    iget-object v0, p0, LX/Aua;->a:Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;

    iget-object v1, p0, LX/Aua;->a:Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;

    iget v1, v1, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->k:F

    .line 1722826
    iput v1, v0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->j:F

    .line 1722827
    return-void
.end method
