.class public final LX/BJr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)V
    .locals 0

    .prologue
    .line 1772464
    iput-object p1, p0, LX/BJr;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0x7761b679

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1772465
    iget-object v1, p0, LX/BJr;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    .line 1772466
    iget-object v2, v1, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->m:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/8RJ;

    sget-object v3, LX/8RI;->COMPOSER_FRAGMENT:LX/8RI;

    invoke-virtual {v2, v3}, LX/8RJ;->a(LX/8RI;)V

    .line 1772467
    new-instance v2, Lcom/facebook/composer/privacy/common/ComposerAudienceFragment;

    invoke-direct {v2}, Lcom/facebook/composer/privacy/common/ComposerAudienceFragment;-><init>()V

    .line 1772468
    new-instance v3, LX/93W;

    iget-object v5, v1, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->ar:LX/8Q5;

    iget-object v6, v1, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->au:LX/93X;

    const/4 p1, 0x0

    invoke-direct {v3, v5, v6, p1}, LX/93W;-><init>(LX/8Q5;LX/93X;LX/8Rm;)V

    invoke-virtual {v2, v3}, Lcom/facebook/composer/privacy/common/ComposerAudienceFragment;->a(LX/93W;)V

    .line 1772469
    iget-object v3, v1, Landroid/support/v4/app/Fragment;->mFragmentManager:LX/0jz;

    move-object v3, v3

    .line 1772470
    const-string v5, "AUDIENCE_FRAGMENT_TAG"

    invoke-virtual {v2, v3, v5}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 1772471
    iget-object v1, p0, LX/BJr;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v1, v1, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->w:LX/0if;

    sget-object v2, LX/0ig;->k:LX/0ih;

    const-string v3, "privacy_view"

    invoke-virtual {v1, v2, v3}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1772472
    const v1, -0x2906c9ea

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
