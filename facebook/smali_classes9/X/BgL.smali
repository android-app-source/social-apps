.class public final LX/BgL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Landroid/support/v4/app/Fragment;

.field public final synthetic b:LX/BgW;

.field public final synthetic c:LX/BgS;

.field public final synthetic d:LX/BgQ;


# direct methods
.method public constructor <init>(LX/BgQ;Landroid/support/v4/app/Fragment;LX/BgW;LX/BgS;)V
    .locals 0

    .prologue
    .line 1807440
    iput-object p1, p0, LX/BgL;->d:LX/BgQ;

    iput-object p2, p0, LX/BgL;->a:Landroid/support/v4/app/Fragment;

    iput-object p3, p0, LX/BgL;->b:LX/BgW;

    iput-object p4, p0, LX/BgL;->c:LX/BgS;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, 0x7a47aa27

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1807441
    iget-object v1, p0, LX/BgL;->d:LX/BgQ;

    iget-object v2, p0, LX/BgL;->a:Landroid/support/v4/app/Fragment;

    iget-object v3, p0, LX/BgL;->b:LX/BgW;

    iget-object v4, p0, LX/BgL;->c:LX/BgS;

    .line 1807442
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v7

    .line 1807443
    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v8, 0x7f082903

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    iget-object v6, v1, LX/BgQ;->a:LX/0Or;

    invoke-interface {v6}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    if-eqz v6, :cond_4

    const v6, 0x7f0207b3

    :goto_0
    new-instance p0, LX/BgM;

    invoke-direct {p0, v1, v2, v3, v4}, LX/BgM;-><init>(LX/BgQ;Landroid/support/v4/app/Fragment;LX/BgW;LX/BgS;)V

    invoke-static {v7, v8, v6, p0}, LX/BgQ;->a(Ljava/util/List;Ljava/lang/String;ILandroid/view/MenuItem$OnMenuItemClickListener;)V

    .line 1807444
    invoke-virtual {v3}, LX/BgW;->e()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1807445
    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v8, 0x7f082904

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    const v8, 0x7f0218d1

    new-instance p0, LX/BgN;

    invoke-direct {p0, v1, v3}, LX/BgN;-><init>(LX/BgQ;LX/BgW;)V

    invoke-static {v7, v6, v8, p0}, LX/BgQ;->a(Ljava/util/List;Ljava/lang/String;ILandroid/view/MenuItem$OnMenuItemClickListener;)V

    .line 1807446
    :cond_0
    iget-object v6, v3, LX/BgW;->e:Lcom/facebook/crowdsourcing/suggestedits/data/SuggestEditsHeaderState;

    .line 1807447
    iget-object v8, v6, Lcom/facebook/crowdsourcing/suggestedits/data/SuggestEditsHeaderState;->d:Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;

    move-object v6, v8

    .line 1807448
    if-eqz v6, :cond_5

    iget-object v6, v3, LX/BgW;->e:Lcom/facebook/crowdsourcing/suggestedits/data/SuggestEditsHeaderState;

    .line 1807449
    iget-object v8, v6, Lcom/facebook/crowdsourcing/suggestedits/data/SuggestEditsHeaderState;->b:Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;

    move-object v6, v8

    .line 1807450
    iget-object v8, v3, LX/BgW;->e:Lcom/facebook/crowdsourcing/suggestedits/data/SuggestEditsHeaderState;

    .line 1807451
    iget-object p0, v8, Lcom/facebook/crowdsourcing/suggestedits/data/SuggestEditsHeaderState;->d:Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;

    move-object v8, p0

    .line 1807452
    invoke-static {v6, v8}, LX/BgW;->a(Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;)Z

    move-result v6

    if-eqz v6, :cond_5

    iget-object v6, v3, LX/BgW;->e:Lcom/facebook/crowdsourcing/suggestedits/data/SuggestEditsHeaderState;

    .line 1807453
    iget-object v8, v6, Lcom/facebook/crowdsourcing/suggestedits/data/SuggestEditsHeaderState;->b:Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;

    move-object v6, v8

    .line 1807454
    iget-object v8, v3, LX/BgW;->e:Lcom/facebook/crowdsourcing/suggestedits/data/SuggestEditsHeaderState;

    .line 1807455
    iget-object p0, v8, Lcom/facebook/crowdsourcing/suggestedits/data/SuggestEditsHeaderState;->c:Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;

    move-object v8, p0

    .line 1807456
    invoke-static {v6, v8}, LX/BgW;->a(Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;)Z

    move-result v6

    if-nez v6, :cond_5

    const/4 v6, 0x1

    :goto_1
    move v6, v6

    .line 1807457
    if-eqz v6, :cond_1

    .line 1807458
    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v8, 0x7f082905

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    const v8, 0x7f0218d2

    new-instance p0, LX/BgO;

    invoke-direct {p0, v1, v3}, LX/BgO;-><init>(LX/BgQ;LX/BgW;)V

    invoke-static {v7, v6, v8, p0}, LX/BgQ;->a(Ljava/util/List;Ljava/lang/String;ILandroid/view/MenuItem$OnMenuItemClickListener;)V

    .line 1807459
    :cond_1
    invoke-static {v7}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v6

    move-object v6, v6

    .line 1807460
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    const/4 v8, 0x1

    if-ne v7, v8, :cond_2

    .line 1807461
    const/4 v7, 0x0

    invoke-virtual {v6, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/BgP;

    iget-object v6, v6, LX/BgP;->c:Landroid/view/MenuItem$OnMenuItemClickListener;

    const/4 v7, 0x0

    invoke-interface {v6, v7}, Landroid/view/MenuItem$OnMenuItemClickListener;->onMenuItemClick(Landroid/view/MenuItem;)Z

    .line 1807462
    :goto_2
    const v1, 0x9c55785

    invoke-static {v5, v5, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1807463
    :cond_2
    new-instance v7, LX/6WS;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-direct {v7, v8}, LX/6WS;-><init>(Landroid/content/Context;)V

    .line 1807464
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v1

    const/4 v8, 0x0

    move p0, v8

    :goto_3
    if-ge p0, v1, :cond_3

    invoke-virtual {v6, p0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/BgP;

    .line 1807465
    invoke-virtual {v7}, LX/5OM;->c()LX/5OG;

    move-result-object v2

    iget-object v3, v8, LX/BgP;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/5OG;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v2

    .line 1807466
    iget v3, v8, LX/BgP;->b:I

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 1807467
    iget-object v8, v8, LX/BgP;->c:Landroid/view/MenuItem$OnMenuItemClickListener;

    invoke-interface {v2, v8}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1807468
    add-int/lit8 v8, p0, 0x1

    move p0, v8

    goto :goto_3

    .line 1807469
    :cond_3
    invoke-virtual {v7, p1}, LX/0ht;->f(Landroid/view/View;)V

    goto :goto_2

    .line 1807470
    :cond_4
    const v6, 0x7f0218cd

    goto/16 :goto_0

    :cond_5
    const/4 v6, 0x0

    goto :goto_1
.end method
