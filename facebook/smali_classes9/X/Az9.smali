.class public LX/Az9;
.super LX/0Rc;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Rc",
        "<",
        "Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;",
        ">;"
    }
.end annotation


# instance fields
.field private a:LX/0Rc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rc",
            "<",
            "Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;",
            ">;"
        }
    .end annotation
.end field

.field private b:Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Rc;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Rc",
            "<",
            "Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1732184
    invoke-direct {p0}, LX/0Rc;-><init>()V

    .line 1732185
    iput-object p1, p0, LX/Az9;->a:LX/0Rc;

    .line 1732186
    invoke-direct {p0}, LX/Az9;->a()Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;

    move-result-object v0

    iput-object v0, p0, LX/Az9;->b:Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;

    .line 1732187
    return-void
.end method

.method private a()Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1732188
    :cond_0
    iget-object v0, p0, LX/Az9;->a:LX/0Rc;

    invoke-virtual {v0}, LX/0Rc;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1732189
    iget-object v0, p0, LX/Az9;->a:LX/0Rc;

    invoke-virtual {v0}, LX/0Rc;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;

    .line 1732190
    invoke-static {v0}, LX/Az7;->b(Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1732191
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final hasNext()Z
    .locals 1

    .prologue
    .line 1732192
    iget-object v0, p0, LX/Az9;->b:Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final next()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1732193
    iget-object v0, p0, LX/Az9;->b:Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;

    .line 1732194
    if-nez v0, :cond_0

    .line 1732195
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 1732196
    :cond_0
    invoke-direct {p0}, LX/Az9;->a()Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;

    move-result-object v1

    iput-object v1, p0, LX/Az9;->b:Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;

    .line 1732197
    return-object v0
.end method
