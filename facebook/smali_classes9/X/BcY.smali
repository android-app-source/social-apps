.class public LX/BcY;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final b:Landroid/os/Handler;

.field private static volatile c:Landroid/os/Looper;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "ComponentTree.class"
    .end annotation
.end field


# instance fields
.field private a:Z

.field private final d:LX/BcP;

.field private final e:LX/BcX;

.field private final f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/BcW;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Landroid/os/Handler;

.field public h:LX/BcG;

.field private final i:Ljava/lang/Runnable;

.field private j:LX/BcO;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/BcO",
            "<*>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private k:LX/BcO;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/BcO",
            "<*>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private l:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "LX/BcR;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/BcJ;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1802331
    new-instance v0, LX/BcV;

    invoke-direct {v0}, LX/BcV;-><init>()V

    sput-object v0, LX/BcY;->b:Landroid/os/Handler;

    return-void
.end method

.method public constructor <init>(LX/BcU;)V
    .locals 3

    .prologue
    .line 1802315
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1802316
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/BcY;->f:Ljava/util/Map;

    .line 1802317
    new-instance v0, Lcom/facebook/components/list/SectionTree$1;

    invoke-direct {v0, p0}, Lcom/facebook/components/list/SectionTree$1;-><init>(LX/BcY;)V

    iput-object v0, p0, LX/BcY;->i:Ljava/lang/Runnable;

    .line 1802318
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/BcY;->a:Z

    .line 1802319
    iget-object v0, p1, LX/BcU;->b:LX/BcX;

    iput-object v0, p0, LX/BcY;->e:LX/BcX;

    .line 1802320
    iget-object v0, p1, LX/BcU;->a:LX/BcP;

    .line 1802321
    new-instance v1, LX/BcP;

    invoke-direct {v1, v0}, LX/BcP;-><init>(LX/1De;)V

    .line 1802322
    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v2, v1, LX/BcP;->c:Ljava/lang/ref/WeakReference;

    .line 1802323
    new-instance v2, LX/BcZ;

    invoke-direct {v2, p0}, LX/BcZ;-><init>(LX/BcY;)V

    iput-object v2, v1, LX/BcP;->e:LX/BcQ;

    .line 1802324
    move-object v0, v1

    .line 1802325
    iput-object v0, p0, LX/BcY;->d:LX/BcP;

    .line 1802326
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/BcY;->m:Ljava/util/List;

    .line 1802327
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/BcY;->l:Ljava/util/Map;

    .line 1802328
    iget-object v0, p1, LX/BcU;->c:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/BcU;->c:Landroid/os/Handler;

    :goto_0
    iput-object v0, p0, LX/BcY;->g:Landroid/os/Handler;

    .line 1802329
    return-void

    .line 1802330
    :cond_0
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, LX/BcY;->h()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    goto :goto_0
.end method

.method private static a(ILX/BcO;I)I
    .locals 2

    .prologue
    .line 1802087
    sub-int v0, p0, p2

    .line 1802088
    iget v1, p1, LX/BcO;->g:I

    move v1, v1

    .line 1802089
    if-ge v0, v1, :cond_0

    if-gez v0, :cond_1

    .line 1802090
    :cond_0
    const/4 v0, -0x1

    .line 1802091
    :cond_1
    return v0
.end method

.method private static a(LX/BcP;LX/BcO;LX/BcO;Ljava/util/Map;)LX/BcK;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/BcP;",
            "LX/BcO",
            "<*>;",
            "LX/BcO",
            "<*>;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "LX/BcR;",
            ">;>;)",
            "LX/BcK;"
        }
    .end annotation

    .prologue
    .line 1802306
    iget-object v0, p2, LX/BcO;->k:Ljava/lang/String;

    move-object v0, v0

    .line 1802307
    iput-object v0, p2, LX/BcO;->j:Ljava/lang/String;

    .line 1802308
    invoke-static {p0, p1, p2, p3}, LX/BcY;->b(LX/BcP;LX/BcO;LX/BcO;Ljava/util/Map;)V

    .line 1802309
    new-instance v0, LX/BcK;

    invoke-direct {v0}, LX/BcK;-><init>()V

    move-object v0, v0

    .line 1802310
    iput-object p1, v0, LX/BcK;->b:LX/BcO;

    .line 1802311
    iput-object p2, v0, LX/BcK;->c:LX/BcO;

    .line 1802312
    iget-object p3, v0, LX/BcK;->e:Ljava/util/List;

    invoke-static {p0, p1, p2, p3}, LX/BcK;->a(LX/BcP;LX/BcO;LX/BcO;Ljava/util/List;)LX/BcJ;

    move-result-object p3

    iput-object p3, v0, LX/BcK;->d:LX/BcJ;

    .line 1802313
    move-object v0, v0

    .line 1802314
    return-object v0
.end method

.method private static a(LX/BcO;Z)LX/BcO;
    .locals 1

    .prologue
    .line 1802305
    if-eqz p0, :cond_0

    invoke-virtual {p0, p1}, LX/BcO;->a(Z)LX/BcO;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(LX/BcY;LX/BcO;II)V
    .locals 9

    .prologue
    const/4 v6, 0x0

    .line 1802275
    iget-object v0, p0, LX/BcY;->f:Ljava/util/Map;

    .line 1802276
    iget-object v1, p1, LX/BcO;->j:Ljava/lang/String;

    move-object v1, v1

    .line 1802277
    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BcW;

    .line 1802278
    if-nez v0, :cond_2

    .line 1802279
    new-instance v0, LX/BcW;

    invoke-direct {v0}, LX/BcW;-><init>()V

    move-object v0, v0

    .line 1802280
    iget-object v1, p0, LX/BcY;->f:Ljava/util/Map;

    .line 1802281
    iget-object v2, p1, LX/BcO;->j:Ljava/lang/String;

    move-object v2, v2

    .line 1802282
    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1802283
    :cond_0
    iput p3, v0, LX/BcW;->b:I

    .line 1802284
    iput p2, v0, LX/BcW;->a:I

    .line 1802285
    iget-object v0, p1, LX/BcO;->i:LX/BcS;

    move-object v0, v0

    .line 1802286
    iget-object v1, p1, LX/BcO;->d:LX/BcP;

    move-object v1, v1

    .line 1802287
    iget v2, p1, LX/BcO;->g:I

    move v4, v2

    .line 1802288
    move v2, p2

    move v3, p3

    move-object v5, p1

    invoke-virtual/range {v0 .. v5}, LX/BcS;->a(LX/BcP;IIILX/BcO;)V

    .line 1802289
    invoke-virtual {p1}, LX/BcO;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1802290
    :cond_1
    :goto_0
    return-void

    .line 1802291
    :cond_2
    iget v1, v0, LX/BcW;->a:I

    if-ne v1, p2, :cond_0

    iget v1, v0, LX/BcW;->b:I

    if-ne v1, p3, :cond_0

    goto :goto_0

    .line 1802292
    :cond_3
    iget-object v0, p1, LX/BcO;->h:Ljava/util/List;

    move-object v5, v0

    .line 1802293
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v7

    move v3, v6

    move v4, v6

    :goto_1
    if-ge v3, v7, :cond_1

    .line 1802294
    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BcO;

    .line 1802295
    invoke-static {p2, v0, v4}, LX/BcY;->a(ILX/BcO;I)I

    move-result v2

    .line 1802296
    invoke-static {p3, v0, v4}, LX/BcY;->a(ILX/BcO;I)I

    move-result v1

    .line 1802297
    if-gez v2, :cond_4

    if-ltz v1, :cond_4

    move v2, v6

    .line 1802298
    :cond_4
    if-gez v1, :cond_5

    if-ltz v2, :cond_5

    .line 1802299
    iget v1, v0, LX/BcO;->g:I

    move v1, v1

    .line 1802300
    add-int/lit8 v1, v1, -0x1

    .line 1802301
    :cond_5
    iget v8, v0, LX/BcO;->g:I

    move v8, v8

    .line 1802302
    add-int/2addr v4, v8

    .line 1802303
    invoke-static {p0, v0, v2, v1}, LX/BcY;->a(LX/BcY;LX/BcO;II)V

    .line 1802304
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1
.end method

.method private b(LX/BcO;)V
    .locals 4

    .prologue
    .line 1802266
    iget-object v0, p1, LX/BcO;->i:LX/BcS;

    move-object v0, v0

    .line 1802267
    iget-object v1, p1, LX/BcO;->d:LX/BcP;

    move-object v1, v1

    .line 1802268
    invoke-virtual {v0, v1, p1}, LX/BcS;->e(LX/BcP;LX/BcO;)V

    .line 1802269
    invoke-virtual {p1}, LX/BcO;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1802270
    iget-object v0, p1, LX/BcO;->h:Ljava/util/List;

    move-object v2, v0

    .line 1802271
    const/4 v0, 0x0

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 1802272
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BcO;

    invoke-direct {p0, v0}, LX/BcY;->b(LX/BcO;)V

    .line 1802273
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1802274
    :cond_0
    return-void
.end method

.method private static b(LX/BcP;LX/BcO;LX/BcO;Ljava/util/Map;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/BcP;",
            "LX/BcO",
            "<*>;",
            "LX/BcO",
            "<*>;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "LX/BcR;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 1802201
    if-nez p2, :cond_0

    .line 1802202
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t generate a subtree with a null root"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1802203
    :cond_0
    invoke-static {p0, p2}, LX/BcP;->a(LX/BcP;LX/BcO;)LX/BcP;

    move-result-object v0

    .line 1802204
    iput-object v0, p2, LX/BcO;->d:LX/BcP;

    .line 1802205
    if-eqz p1, :cond_1

    .line 1802206
    iget v0, p1, LX/BcO;->g:I

    move v0, v0

    .line 1802207
    iput v0, p2, LX/BcO;->g:I

    .line 1802208
    :cond_1
    iget-object v0, p2, LX/BcO;->i:LX/BcS;

    move-object v5, v0

    .line 1802209
    if-nez p1, :cond_3

    .line 1802210
    iget-object v0, p2, LX/BcO;->d:LX/BcP;

    move-object v0, v0

    .line 1802211
    invoke-virtual {v5, v0, p2}, LX/BcS;->c(LX/BcP;LX/BcO;)V

    .line 1802212
    iget-object v0, p2, LX/BcO;->d:LX/BcP;

    move-object v0, v0

    .line 1802213
    invoke-virtual {v5, v0, p2}, LX/BcS;->d(LX/BcP;LX/BcO;)V

    .line 1802214
    :cond_2
    :goto_0
    invoke-virtual {v5}, LX/BcS;->b()Z

    move-result v0

    if-nez v0, :cond_9

    .line 1802215
    if-nez p1, :cond_6

    move-object v2, v3

    .line 1802216
    :goto_1
    iget-object v0, p2, LX/BcO;->d:LX/BcP;

    move-object v0, v0

    .line 1802217
    iget-object v1, p2, LX/BcO;->h:Ljava/util/List;

    move-object v1, v1

    .line 1802218
    invoke-virtual {v5, v0, v1, p2}, LX/BcS;->a(LX/BcP;Ljava/util/List;LX/BcO;)V

    .line 1802219
    iget-object v0, p2, LX/BcO;->h:Ljava/util/List;

    move-object v5, v0

    .line 1802220
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    move-object v6, v0

    .line 1802221
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v7

    :goto_2
    if-ge v4, v7, :cond_9

    .line 1802222
    invoke-interface {v5, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BcO;

    .line 1802223
    iput-object p2, v0, LX/BcO;->b:LX/BcO;

    .line 1802224
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1802225
    iget-object v8, p2, LX/BcO;->j:Ljava/lang/String;

    move-object v8, v8

    .line 1802226
    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1802227
    iget-object v8, v0, LX/BcO;->k:Ljava/lang/String;

    move-object v8, v8

    .line 1802228
    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1802229
    iput-object v1, v0, LX/BcO;->j:Ljava/lang/String;

    .line 1802230
    invoke-interface {v6, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 1802231
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "You have two ListComponent with the same key: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1802232
    iget-object v3, v0, LX/BcO;->k:Ljava/lang/String;

    move-object v0, v3

    .line 1802233
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", as children of "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1802234
    iget-object v2, p2, LX/BcO;->i:LX/BcS;

    move-object v2, v2

    .line 1802235
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ". Please specify different keys."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1802236
    :cond_3
    invoke-virtual {v5, p1, p2}, LX/BcS;->b(LX/BcO;LX/BcO;)V

    .line 1802237
    invoke-virtual {v5, p1, p2}, LX/BcS;->a(LX/BcO;LX/BcO;)V

    .line 1802238
    invoke-virtual {v5, p2}, LX/BcS;->a(LX/BcO;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1802239
    invoke-static {}, LX/Bcb;->a()LX/Bcb;

    move-result-object v0

    .line 1802240
    iget-object v1, p2, LX/BcO;->i:LX/BcS;

    move-object v1, v1

    .line 1802241
    invoke-virtual {v1, p2}, LX/BcS;->a(LX/BcO;)Ljava/lang/Object;

    move-result-object v2

    .line 1802242
    iget-object v1, v0, LX/Bcb;->c:Ljava/util/HashMap;

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 1802243
    if-nez v1, :cond_a

    .line 1802244
    iget-object v1, v0, LX/Bcb;->c:Ljava/util/HashMap;

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v1, v2, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1802245
    :goto_3
    iget-object v1, v0, LX/Bcb;->d:Ljava/util/Set;

    new-instance v2, LX/Bca;

    iget-object v6, v0, LX/Bcb;->b:Ljava/lang/ref/ReferenceQueue;

    invoke-direct {v2, p2, v6, p0}, LX/Bca;-><init>(LX/BcO;Ljava/lang/ref/ReferenceQueue;LX/BcP;)V

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1802246
    :cond_4
    iget-object v0, p2, LX/BcO;->j:Ljava/lang/String;

    move-object v0, v0

    .line 1802247
    invoke-interface {p3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 1802248
    if-eqz v0, :cond_2

    .line 1802249
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v6

    move v2, v4

    :goto_4
    if-ge v2, v6, :cond_5

    .line 1802250
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/BcR;

    invoke-interface {v1, p2, p2}, LX/BcR;->a(LX/BcO;LX/BcO;)V

    .line 1802251
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_4

    .line 1802252
    :cond_5
    invoke-static {p2, p1}, LX/BcS;->c(LX/BcO;LX/BcO;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1802253
    invoke-static {p2}, LX/BcO;->c(LX/BcO;)V

    .line 1802254
    goto/16 :goto_0

    .line 1802255
    :cond_6
    invoke-static {p1}, LX/BcO;->b(LX/BcO;)Ljava/util/Map;

    move-result-object v0

    move-object v2, v0

    goto/16 :goto_1

    .line 1802256
    :cond_7
    invoke-interface {v6, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1802257
    invoke-static {p0, v0}, LX/BcP;->a(LX/BcP;LX/BcO;)LX/BcP;

    move-result-object v1

    .line 1802258
    iput-object v1, v0, LX/BcO;->d:LX/BcP;

    .line 1802259
    if-nez v2, :cond_8

    move-object v1, v3

    .line 1802260
    :goto_5
    invoke-static {p0, v1, v0, p3}, LX/BcY;->b(LX/BcP;LX/BcO;LX/BcO;Ljava/util/Map;)V

    .line 1802261
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto/16 :goto_2

    .line 1802262
    :cond_8
    iget-object v1, v0, LX/BcO;->j:Ljava/lang/String;

    move-object v1, v1

    .line 1802263
    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/BcO;

    goto :goto_5

    .line 1802264
    :cond_9
    return-void

    .line 1802265
    :cond_a
    iget-object v6, v0, LX/Bcb;->c:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v6, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3
.end method

.method private static declared-synchronized b(LX/BcY;Ljava/lang/String;LX/BcR;)V
    .locals 2

    .prologue
    .line 1802188
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/BcY;->a:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 1802189
    :goto_0
    monitor-exit p0

    return-void

    .line 1802190
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/BcY;->j:LX/BcO;

    if-nez v0, :cond_1

    iget-object v0, p0, LX/BcY;->k:LX/BcO;

    if-nez v0, :cond_1

    .line 1802191
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "State set with no attached ListComponent"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1802192
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1802193
    :cond_1
    :try_start_2
    iget-object v0, p0, LX/BcY;->l:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 1802194
    if-nez v0, :cond_2

    .line 1802195
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    move-object v0, v0

    .line 1802196
    iget-object v1, p0, LX/BcY;->l:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1802197
    :cond_2
    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1802198
    iget-object v0, p0, LX/BcY;->k:LX/BcO;

    if-nez v0, :cond_3

    .line 1802199
    iget-object v0, p0, LX/BcY;->j:LX/BcO;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/BcY;->a(LX/BcO;Z)LX/BcO;

    move-result-object v0

    iput-object v0, p0, LX/BcY;->k:LX/BcO;

    goto :goto_0

    .line 1802200
    :cond_3
    iget-object v0, p0, LX/BcY;->k:LX/BcO;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/BcY;->a(LX/BcO;Z)LX/BcO;

    move-result-object v0

    iput-object v0, p0, LX/BcY;->k:LX/BcO;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method private static c(LX/BcO;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/BcO",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 1802179
    iget-object v0, p0, LX/BcO;->i:LX/BcS;

    move-object v0, v0

    .line 1802180
    iget-object v1, p0, LX/BcO;->d:LX/BcP;

    move-object v1, v1

    .line 1802181
    invoke-virtual {v0, v1, p0}, LX/BcS;->a(LX/BcP;LX/BcO;)V

    .line 1802182
    invoke-virtual {p0}, LX/BcO;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1802183
    iget-object v0, p0, LX/BcO;->h:Ljava/util/List;

    move-object v2, v0

    .line 1802184
    const/4 v0, 0x0

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 1802185
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BcO;

    invoke-static {v0}, LX/BcY;->c(LX/BcO;)V

    .line 1802186
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1802187
    :cond_0
    return-void
.end method

.method private static d(LX/BcO;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/BcO",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 1802170
    iget-object v0, p0, LX/BcO;->i:LX/BcS;

    move-object v0, v0

    .line 1802171
    iget-object v1, p0, LX/BcO;->d:LX/BcP;

    move-object v1, v1

    .line 1802172
    invoke-virtual {v0, v1, p0}, LX/BcS;->b(LX/BcP;LX/BcO;)V

    .line 1802173
    invoke-virtual {p0}, LX/BcO;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1802174
    iget-object v0, p0, LX/BcO;->h:Ljava/util/List;

    move-object v2, v0

    .line 1802175
    const/4 v0, 0x0

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 1802176
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BcO;

    invoke-static {v0}, LX/BcY;->d(LX/BcO;)V

    .line 1802177
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1802178
    :cond_0
    return-void
.end method

.method public static e(LX/BcY;)V
    .locals 12

    .prologue
    const/4 v2, 0x0

    .line 1802139
    invoke-static {}, LX/1dS;->b()V

    .line 1802140
    monitor-enter p0

    .line 1802141
    :try_start_0
    iget-boolean v0, p0, LX/BcY;->a:Z

    if-eqz v0, :cond_1

    .line 1802142
    monitor-exit p0

    .line 1802143
    :cond_0
    return-void

    .line 1802144
    :cond_1
    new-instance v4, Ljava/util/ArrayList;

    iget-object v0, p0, LX/BcY;->m:Ljava/util/List;

    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1802145
    iget-object v0, p0, LX/BcY;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1802146
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1802147
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    move v3, v2

    :goto_0
    if-ge v3, v5, :cond_0

    .line 1802148
    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BcJ;

    .line 1802149
    invoke-virtual {v0}, LX/BcJ;->a()I

    move-result v1

    if-lez v1, :cond_2

    .line 1802150
    invoke-virtual {v0}, LX/BcJ;->a()I

    move-result v6

    move v1, v2

    :goto_1
    if-ge v1, v6, :cond_2

    .line 1802151
    iget-object v7, v0, LX/BcJ;->a:Ljava/util/List;

    invoke-interface {v7, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/BcI;

    move-object v7, v7

    .line 1802152
    iget v8, v7, LX/BcI;->a:I

    move v8, v8

    .line 1802153
    packed-switch v8, :pswitch_data_0

    .line 1802154
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1802155
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 1802156
    :pswitch_0
    iget-object v8, p0, LX/BcY;->e:LX/BcX;

    .line 1802157
    iget v9, v7, LX/BcI;->b:I

    move v9, v9

    .line 1802158
    iget-object v10, v7, LX/BcI;->c:LX/1X1;

    move-object v10, v10

    .line 1802159
    iget-boolean v11, v7, LX/BcI;->d:Z

    move v7, v11

    .line 1802160
    invoke-interface {v8, v9, v10, v7}, LX/BcX;->a(ILX/1X1;Z)V

    goto :goto_2

    .line 1802161
    :pswitch_1
    iget-object v8, p0, LX/BcY;->e:LX/BcX;

    .line 1802162
    iget v9, v7, LX/BcI;->b:I

    move v9, v9

    .line 1802163
    iget-object v10, v7, LX/BcI;->c:LX/1X1;

    move-object v10, v10

    .line 1802164
    iget-boolean v11, v7, LX/BcI;->d:Z

    move v7, v11

    .line 1802165
    invoke-interface {v8, v9, v10, v7}, LX/BcX;->b(ILX/1X1;Z)V

    goto :goto_2

    .line 1802166
    :pswitch_2
    iget-object v8, p0, LX/BcY;->e:LX/BcX;

    .line 1802167
    iget v9, v7, LX/BcI;->b:I

    move v7, v9

    .line 1802168
    invoke-interface {v8, v7}, LX/BcX;->b(I)V

    goto :goto_2

    .line 1802169
    :cond_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private g()Ljava/util/Map;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "LX/BcR;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1802133
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    move-object v2, v0

    .line 1802134
    iget-object v0, p0, LX/BcY;->l:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 1802135
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1802136
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1802137
    new-instance v4, Ljava/util/ArrayList;

    iget-object v1, p0, LX/BcY;->l:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Collection;

    invoke-direct {v4, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-interface {v2, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1802138
    :cond_0
    return-object v2
.end method

.method private static declared-synchronized h()Landroid/os/Looper;
    .locals 4

    .prologue
    .line 1802127
    const-class v1, LX/BcY;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/BcY;->c:Landroid/os/Looper;

    if-nez v0, :cond_0

    .line 1802128
    new-instance v0, Landroid/os/HandlerThread;

    const-string v2, "ComponentChangeSetThread"

    const/16 v3, 0xa

    invoke-direct {v0, v2, v3}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    .line 1802129
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 1802130
    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    sput-object v0, LX/BcY;->c:Landroid/os/Looper;

    .line 1802131
    :cond_0
    sget-object v0, LX/BcY;->c:Landroid/os/Looper;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 1802132
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1802118
    monitor-enter p0

    .line 1802119
    :try_start_0
    iget-boolean v0, p0, LX/BcY;->a:Z

    if-eqz v0, :cond_0

    .line 1802120
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Calling refresh on a released tree"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1802121
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1802122
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/BcY;->j:LX/BcO;

    .line 1802123
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1802124
    if-nez v0, :cond_1

    .line 1802125
    :goto_0
    return-void

    .line 1802126
    :cond_1
    invoke-direct {p0, v0}, LX/BcY;->b(LX/BcO;)V

    goto :goto_0
.end method

.method public final a(II)V
    .locals 1

    .prologue
    .line 1802112
    monitor-enter p0

    .line 1802113
    :try_start_0
    iget-object v0, p0, LX/BcY;->j:LX/BcO;

    .line 1802114
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1802115
    invoke-static {p0, v0, p1, p2}, LX/BcY;->a(LX/BcY;LX/BcO;II)V

    .line 1802116
    return-void

    .line 1802117
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a(LX/BcG;)V
    .locals 0

    .prologue
    .line 1802110
    iput-object p1, p0, LX/BcY;->h:LX/BcG;

    .line 1802111
    return-void
.end method

.method public final a(LX/BcO;)V
    .locals 2

    .prologue
    .line 1802092
    monitor-enter p0

    .line 1802093
    :try_start_0
    iget-boolean v0, p0, LX/BcY;->a:Z

    if-eqz v0, :cond_0

    .line 1802094
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Setting root on a released tree"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1802095
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1802096
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/BcY;->j:LX/BcO;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/BcY;->j:LX/BcO;

    .line 1802097
    iget v1, v0, LX/BcO;->f:I

    move v0, v1

    .line 1802098
    iget v1, p1, LX/BcO;->f:I

    move v1, v1

    .line 1802099
    if-ne v0, v1, :cond_1

    .line 1802100
    monitor-exit p0

    .line 1802101
    :goto_0
    return-void

    .line 1802102
    :cond_1
    iget-object v0, p0, LX/BcY;->k:LX/BcO;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/BcY;->k:LX/BcO;

    .line 1802103
    iget v1, v0, LX/BcO;->f:I

    move v0, v1

    .line 1802104
    iget v1, p1, LX/BcO;->f:I

    move v1, v1

    .line 1802105
    if-ne v0, v1, :cond_2

    .line 1802106
    monitor-exit p0

    goto :goto_0

    .line 1802107
    :cond_2
    const/4 v0, 0x0

    invoke-static {p1, v0}, LX/BcY;->a(LX/BcO;Z)LX/BcO;

    move-result-object v0

    iput-object v0, p0, LX/BcY;->k:LX/BcO;

    .line 1802108
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1802109
    invoke-virtual {p0}, LX/BcY;->b()V

    goto :goto_0
.end method

.method public final declared-synchronized a(Ljava/lang/String;LX/BcR;)V
    .locals 3

    .prologue
    .line 1802082
    monitor-enter p0

    :try_start_0
    sget-object v0, LX/BcY;->b:Landroid/os/Handler;

    iget-object v1, p0, LX/BcY;->i:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1802083
    invoke-static {p0, p1, p2}, LX/BcY;->b(LX/BcY;Ljava/lang/String;LX/BcR;)V

    .line 1802084
    sget-object v0, LX/BcY;->b:Landroid/os/Handler;

    iget-object v1, p0, LX/BcY;->i:Ljava/lang/Runnable;

    const v2, -0x53578131

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1802085
    monitor-exit p0

    return-void

    .line 1802086
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b()V
    .locals 14

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v3, 0x0

    .line 1802007
    monitor-enter p0

    .line 1802008
    :try_start_0
    iget-boolean v0, p0, LX/BcY;->a:Z

    if-eqz v0, :cond_1

    .line 1802009
    monitor-exit p0

    .line 1802010
    :cond_0
    :goto_0
    return-void

    .line 1802011
    :cond_1
    iget-object v0, p0, LX/BcY;->j:LX/BcO;

    const/4 v1, 0x1

    invoke-static {v0, v1}, LX/BcY;->a(LX/BcO;Z)LX/BcO;

    move-result-object v2

    .line 1802012
    iget-object v0, p0, LX/BcY;->k:LX/BcO;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/BcY;->a(LX/BcO;Z)LX/BcO;

    move-result-object v1

    .line 1802013
    invoke-direct {p0}, LX/BcY;->g()Ljava/util/Map;

    move-result-object v0

    .line 1802014
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1802015
    :goto_1
    if-eqz v1, :cond_0

    .line 1802016
    iget-object v6, p0, LX/BcY;->d:LX/BcP;

    invoke-static {v6, v2, v1, v0}, LX/BcY;->a(LX/BcP;LX/BcO;LX/BcO;Ljava/util/Map;)LX/BcK;

    move-result-object v8

    .line 1802017
    monitor-enter p0

    .line 1802018
    if-eqz v2, :cond_b

    move v7, v5

    .line 1802019
    :goto_2
    :try_start_1
    iget-object v6, p0, LX/BcY;->j:LX/BcO;

    if-eqz v6, :cond_c

    move v6, v5

    .line 1802020
    :goto_3
    if-eqz v7, :cond_2

    if-eqz v6, :cond_2

    .line 1802021
    iget v9, v2, LX/BcO;->f:I

    move v2, v9

    .line 1802022
    iget-object v9, p0, LX/BcY;->j:LX/BcO;

    .line 1802023
    iget v10, v9, LX/BcO;->f:I

    move v9, v10

    .line 1802024
    if-eq v2, v9, :cond_3

    :cond_2
    if-nez v7, :cond_d

    if-nez v6, :cond_d

    :cond_3
    move v7, v5

    .line 1802025
    :goto_4
    if-eqz v1, :cond_e

    move v6, v5

    .line 1802026
    :goto_5
    iget-object v2, p0, LX/BcY;->k:LX/BcO;

    if-eqz v2, :cond_f

    move v2, v5

    .line 1802027
    :goto_6
    if-eqz v6, :cond_4

    if-eqz v2, :cond_4

    .line 1802028
    iget v9, v1, LX/BcO;->f:I

    move v9, v9

    .line 1802029
    iget-object v10, p0, LX/BcY;->k:LX/BcO;

    .line 1802030
    iget v11, v10, LX/BcO;->f:I

    move v10, v11

    .line 1802031
    if-eq v9, v10, :cond_5

    :cond_4
    if-nez v6, :cond_10

    if-nez v2, :cond_10

    :cond_5
    move v2, v5

    .line 1802032
    :goto_7
    and-int/2addr v2, v7

    .line 1802033
    if-eqz v2, :cond_14

    .line 1802034
    iget-object v6, p0, LX/BcY;->l:Ljava/util/Map;

    .line 1802035
    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .line 1802036
    :cond_6
    :goto_8
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_8

    .line 1802037
    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 1802038
    invoke-interface {v0, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/util/List;

    .line 1802039
    invoke-interface {v6, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/util/List;

    .line 1802040
    if-eqz v10, :cond_6

    .line 1802041
    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v12

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v13

    if-ne v12, v13, :cond_7

    .line 1802042
    invoke-interface {v6, v7}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_8

    .line 1802043
    :cond_7
    invoke-interface {v10, v9}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    goto :goto_8

    .line 1802044
    :cond_8
    iget-object v0, p0, LX/BcY;->j:LX/BcO;

    .line 1802045
    iput-object v1, p0, LX/BcY;->j:LX/BcO;

    .line 1802046
    iget-object v6, p0, LX/BcY;->l:Ljava/util/Map;

    invoke-interface {v6}, Ljava/util/Map;->size()I

    move-result v6

    if-nez v6, :cond_9

    .line 1802047
    const/4 v6, 0x0

    iput-object v6, p0, LX/BcY;->k:LX/BcO;

    .line 1802048
    :cond_9
    iget-object v6, p0, LX/BcY;->m:Ljava/util/List;

    .line 1802049
    iget-object v7, v8, LX/BcK;->d:LX/BcJ;

    move-object v7, v7

    .line 1802050
    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1802051
    :goto_9
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1802052
    if-eqz v2, :cond_12

    .line 1802053
    if-eqz v0, :cond_a

    .line 1802054
    invoke-static {v0}, LX/BcY;->d(LX/BcO;)V

    .line 1802055
    :cond_a
    invoke-static {v1}, LX/BcY;->c(LX/BcO;)V

    .line 1802056
    iget-object v0, v8, LX/BcK;->e:Ljava/util/List;

    move-object v2, v0

    .line 1802057
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v6

    move v1, v3

    :goto_a
    if-ge v1, v6, :cond_11

    .line 1802058
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BcO;

    .line 1802059
    iget-object v7, p0, LX/BcY;->f:Ljava/util/Map;

    .line 1802060
    iget-object v8, v0, LX/BcO;->j:Ljava/lang/String;

    move-object v0, v8

    .line 1802061
    invoke-interface {v7, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1802062
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_a

    .line 1802063
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_b
    move v7, v3

    .line 1802064
    goto/16 :goto_2

    :cond_c
    move v6, v3

    .line 1802065
    goto/16 :goto_3

    :cond_d
    move v7, v3

    .line 1802066
    goto/16 :goto_4

    :cond_e
    move v6, v3

    .line 1802067
    goto/16 :goto_5

    :cond_f
    move v2, v3

    .line 1802068
    goto/16 :goto_6

    :cond_10
    move v2, v3

    .line 1802069
    goto/16 :goto_7

    .line 1802070
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    .line 1802071
    :cond_11
    invoke-static {}, LX/1dS;->a()Z

    move-result v0

    if-eqz v0, :cond_15

    .line 1802072
    invoke-static {p0}, LX/BcY;->e(LX/BcY;)V

    .line 1802073
    :cond_12
    :goto_b
    monitor-enter p0

    .line 1802074
    :try_start_4
    iget-boolean v0, p0, LX/BcY;->a:Z

    if-eqz v0, :cond_13

    .line 1802075
    monitor-exit p0

    goto/16 :goto_0

    .line 1802076
    :catchall_2
    move-exception v0

    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    throw v0

    .line 1802077
    :cond_13
    :try_start_5
    iget-object v0, p0, LX/BcY;->j:LX/BcO;

    const/4 v1, 0x1

    invoke-static {v0, v1}, LX/BcY;->a(LX/BcO;Z)LX/BcO;

    move-result-object v2

    .line 1802078
    iget-object v0, p0, LX/BcY;->k:LX/BcO;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/BcY;->a(LX/BcO;Z)LX/BcO;

    move-result-object v1

    .line 1802079
    invoke-direct {p0}, LX/BcY;->g()Ljava/util/Map;

    move-result-object v0

    .line 1802080
    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    goto/16 :goto_1

    :cond_14
    move-object v1, v4

    move-object v0, v4

    goto :goto_9

    .line 1802081
    :cond_15
    sget-object v0, LX/BcY;->b:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_b
.end method
