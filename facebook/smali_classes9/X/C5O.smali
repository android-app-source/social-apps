.class public LX/C5O;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field public final a:LX/3mL;

.field public final b:LX/C5M;

.field public final c:LX/AnA;

.field public final d:LX/1LV;


# direct methods
.method public constructor <init>(LX/3mL;LX/C5M;LX/AnA;LX/1LV;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1848785
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1848786
    iput-object p1, p0, LX/C5O;->a:LX/3mL;

    .line 1848787
    iput-object p2, p0, LX/C5O;->b:LX/C5M;

    .line 1848788
    iput-object p3, p0, LX/C5O;->c:LX/AnA;

    .line 1848789
    iput-object p4, p0, LX/C5O;->d:LX/1LV;

    .line 1848790
    return-void
.end method

.method public static a(LX/0QB;)LX/C5O;
    .locals 7

    .prologue
    .line 1848791
    const-class v1, LX/C5O;

    monitor-enter v1

    .line 1848792
    :try_start_0
    sget-object v0, LX/C5O;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1848793
    sput-object v2, LX/C5O;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1848794
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1848795
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1848796
    new-instance p0, LX/C5O;

    invoke-static {v0}, LX/3mL;->a(LX/0QB;)LX/3mL;

    move-result-object v3

    check-cast v3, LX/3mL;

    const-class v4, LX/C5M;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/C5M;

    invoke-static {v0}, LX/AnA;->a(LX/0QB;)LX/AnA;

    move-result-object v5

    check-cast v5, LX/AnA;

    invoke-static {v0}, LX/1LV;->a(LX/0QB;)LX/1LV;

    move-result-object v6

    check-cast v6, LX/1LV;

    invoke-direct {p0, v3, v4, v5, v6}, LX/C5O;-><init>(LX/3mL;LX/C5M;LX/AnA;LX/1LV;)V

    .line 1848797
    move-object v0, p0

    .line 1848798
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1848799
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C5O;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1848800
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1848801
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
