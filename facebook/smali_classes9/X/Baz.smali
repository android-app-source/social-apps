.class public final LX/Baz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/AQ7;


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field public static final a:LX/89L;

.field private static final c:Ljava/lang/Object;


# instance fields
.field private final b:LX/Bb1;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1800070
    const-string v0, "social_search_composer_plugin_config"

    invoke-static {v0}, LX/89L;->a(Ljava/lang/String;)LX/89L;

    move-result-object v0

    sput-object v0, LX/Baz;->a:LX/89L;

    .line 1800071
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/Baz;->c:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/Bb1;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1800106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1800107
    iput-object p1, p0, LX/Baz;->b:LX/Bb1;

    .line 1800108
    return-void
.end method

.method public static a(LX/0QB;)LX/Baz;
    .locals 7

    .prologue
    .line 1800077
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 1800078
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 1800079
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 1800080
    if-nez v1, :cond_0

    .line 1800081
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1800082
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 1800083
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 1800084
    sget-object v1, LX/Baz;->c:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 1800085
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 1800086
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1800087
    :cond_1
    if-nez v1, :cond_4

    .line 1800088
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 1800089
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 1800090
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 1800091
    new-instance p0, LX/Baz;

    const-class v1, LX/Bb1;

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/Bb1;

    invoke-direct {p0, v1}, LX/Baz;-><init>(LX/Bb1;)V

    .line 1800092
    move-object v1, p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1800093
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 1800094
    if-nez v1, :cond_2

    .line 1800095
    sget-object v0, LX/Baz;->c:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Baz;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1800096
    :goto_1
    if-eqz v0, :cond_3

    .line 1800097
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 1800098
    :goto_3
    check-cast v0, LX/Baz;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 1800099
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 1800100
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 1800101
    :catchall_1
    move-exception v0

    .line 1800102
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 1800103
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 1800104
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 1800105
    :cond_2
    :try_start_8
    sget-object v0, LX/Baz;->c:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Baz;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method public final a(Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;LX/B5j;LX/B5f;)LX/AQ9;
    .locals 1
    .param p3    # LX/B5f;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ModelData::",
            "Lcom/facebook/ipc/composer/plugin/ComposerPluginDataProvider;",
            "DerivedData::",
            "Lcom/facebook/ipc/composer/plugin/ComposerPluginDerivedDataProvider;",
            "Mutation::",
            "Lcom/facebook/ipc/composer/plugin/ComposerPluginMutation",
            "<TMutation;>;>(",
            "Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;",
            "LX/B5j",
            "<TModelData;TDerivedData;TMutation;>;",
            "LX/B5f;",
            ")",
            "Lcom/facebook/ipc/composer/plugin/ComposerPlugin",
            "<TModelData;TDerivedData;TMutation;>;"
        }
    .end annotation

    .prologue
    .line 1800073
    iget-object v0, p0, LX/Baz;->b:LX/Bb1;

    .line 1800074
    new-instance p1, LX/Bb0;

    const-class p0, Landroid/content/Context;

    invoke-interface {v0, p0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/content/Context;

    invoke-direct {p1, p2, p0}, LX/Bb0;-><init>(LX/B5j;Landroid/content/Context;)V

    .line 1800075
    move-object v0, p1

    .line 1800076
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1800072
    sget-object v0, LX/Baz;->a:LX/89L;

    invoke-virtual {v0}, LX/89L;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
