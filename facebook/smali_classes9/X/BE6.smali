.class public LX/BE6;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:LX/0SG;


# direct methods
.method public constructor <init>(LX/0SG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1764076
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1764077
    iput-object p1, p0, LX/BE6;->a:LX/0SG;

    .line 1764078
    return-void
.end method

.method public static a(LX/0QB;)LX/BE6;
    .locals 2

    .prologue
    .line 1764079
    new-instance v1, LX/BE6;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v0

    check-cast v0, LX/0SG;

    invoke-direct {v1, v0}, LX/BE6;-><init>(LX/0SG;)V

    .line 1764080
    move-object v0, v1

    .line 1764081
    return-object v0
.end method

.method public static a(LX/2nq;)Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1764082
    invoke-interface {p0}, LX/2nq;->o()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, LX/2nq;->o()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v1

    .line 1764083
    :goto_0
    return v0

    .line 1764084
    :cond_1
    invoke-interface {p0}, LX/2nq;->o()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel;

    .line 1764085
    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel;->b()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public static a(LX/BE6;Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$CriteriaModel;)Z
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    .line 1764086
    invoke-virtual {p1}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$CriteriaModel;->c()J

    move-result-wide v0

    .line 1764087
    invoke-virtual {p1}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$CriteriaModel;->b()J

    move-result-wide v2

    .line 1764088
    iget-object v4, p0, LX/BE6;->a:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    .line 1764089
    cmp-long v6, v0, v8

    if-lez v6, :cond_0

    cmp-long v0, v0, v4

    if-gtz v0, :cond_1

    :cond_0
    cmp-long v0, v2, v8

    if-lez v0, :cond_2

    cmp-long v0, v2, v4

    if-gez v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
