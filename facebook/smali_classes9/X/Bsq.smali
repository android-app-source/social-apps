.class public LX/Bsq;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/header/components/ThrowbackSharedStoryHeaderExplanationV2ComponentSpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1828299
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/Bsq;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/header/components/ThrowbackSharedStoryHeaderExplanationV2ComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1828296
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1828297
    iput-object p1, p0, LX/Bsq;->b:LX/0Ot;

    .line 1828298
    return-void
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1828279
    const v0, -0x15e5a53a

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 2

    .prologue
    .line 1828293
    check-cast p2, LX/Bsp;

    .line 1828294
    iget-object v0, p0, LX/Bsq;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/sections/header/components/ThrowbackSharedStoryHeaderExplanationV2ComponentSpec;

    iget-object v1, p2, LX/Bsp;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v0, p1, v1}, Lcom/facebook/feed/rows/sections/header/components/ThrowbackSharedStoryHeaderExplanationV2ComponentSpec;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1Dg;

    move-result-object v0

    .line 1828295
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1828280
    invoke-static {}, LX/1dS;->b()V

    .line 1828281
    iget v0, p1, LX/1dQ;->b:I

    .line 1828282
    packed-switch v0, :pswitch_data_0

    .line 1828283
    :goto_0
    return-object v2

    .line 1828284
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 1828285
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1828286
    check-cast v1, LX/Bsp;

    .line 1828287
    iget-object v3, p0, LX/Bsq;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/sections/header/components/ThrowbackSharedStoryHeaderExplanationV2ComponentSpec;

    iget-object p1, v1, LX/Bsp;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1828288
    iget-object p2, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object p2, p2

    .line 1828289
    check-cast p2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStory;->aL()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object p2

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->a()Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object p0

    .line 1828290
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bc()Ljava/lang/String;

    move-result-object p2

    if-eqz p2, :cond_0

    .line 1828291
    iget-object p2, v3, Lcom/facebook/feed/rows/sections/header/components/ThrowbackSharedStoryHeaderExplanationV2ComponentSpec;->c:LX/0Ot;

    invoke-interface {p2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/17W;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bc()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p2, v1, p0}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 1828292
    :cond_0
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x15e5a53a
        :pswitch_0
    .end packed-switch
.end method
