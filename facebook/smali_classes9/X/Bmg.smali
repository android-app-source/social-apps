.class public final LX/Bmg;
.super LX/BcO;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/BcO",
        "<",
        "LX/Bmi;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public b:Z

.field public c:LX/1rs;

.field public d:I

.field public e:I

.field public f:Ljava/lang/String;

.field public final synthetic g:LX/Bmi;


# direct methods
.method public constructor <init>(LX/Bmi;)V
    .locals 1

    .prologue
    .line 1818785
    iput-object p1, p0, LX/Bmg;->g:LX/Bmi;

    .line 1818786
    move-object v0, p1

    .line 1818787
    invoke-direct {p0, v0}, LX/BcO;-><init>(LX/BcS;)V

    .line 1818788
    return-void
.end method


# virtual methods
.method public final a(Z)LX/BcO;
    .locals 2

    .prologue
    .line 1818806
    invoke-super {p0, p1}, LX/BcO;->a(Z)LX/BcO;

    move-result-object v0

    check-cast v0, LX/Bmg;

    .line 1818807
    if-nez p1, :cond_0

    .line 1818808
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/Bmg;->b:Z

    .line 1818809
    :cond_0
    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1818789
    if-ne p0, p1, :cond_1

    .line 1818790
    :cond_0
    :goto_0
    return v0

    .line 1818791
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1818792
    goto :goto_0

    .line 1818793
    :cond_3
    check-cast p1, LX/Bmg;

    .line 1818794
    iget-boolean v2, p0, LX/Bmg;->b:Z

    iget-boolean v3, p1, LX/Bmg;->b:Z

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 1818795
    goto :goto_0

    .line 1818796
    :cond_4
    iget-object v2, p0, LX/Bmg;->c:LX/1rs;

    if-eqz v2, :cond_6

    iget-object v2, p0, LX/Bmg;->c:LX/1rs;

    iget-object v3, p1, LX/Bmg;->c:LX/1rs;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    :cond_5
    move v0, v1

    .line 1818797
    goto :goto_0

    .line 1818798
    :cond_6
    iget-object v2, p1, LX/Bmg;->c:LX/1rs;

    if-nez v2, :cond_5

    .line 1818799
    :cond_7
    iget v2, p0, LX/Bmg;->d:I

    iget v3, p1, LX/Bmg;->d:I

    if-eq v2, v3, :cond_8

    move v0, v1

    .line 1818800
    goto :goto_0

    .line 1818801
    :cond_8
    iget v2, p0, LX/Bmg;->e:I

    iget v3, p1, LX/Bmg;->e:I

    if-eq v2, v3, :cond_9

    move v0, v1

    .line 1818802
    goto :goto_0

    .line 1818803
    :cond_9
    iget-object v2, p0, LX/Bmg;->f:Ljava/lang/String;

    if-eqz v2, :cond_a

    iget-object v2, p0, LX/Bmg;->f:Ljava/lang/String;

    iget-object v3, p1, LX/Bmg;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1818804
    goto :goto_0

    .line 1818805
    :cond_a
    iget-object v2, p1, LX/Bmg;->f:Ljava/lang/String;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
