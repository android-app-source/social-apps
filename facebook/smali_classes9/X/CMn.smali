.class public final synthetic LX/CMn;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final synthetic a:[I

.field public static final synthetic b:[I


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1880951
    invoke-static {}, Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;->values()[Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/CMn;->b:[I

    :try_start_0
    sget-object v0, LX/CMn;->b:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;->COMMERCE_FAQ_ENABLED:Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_b

    :goto_0
    :try_start_1
    sget-object v0, LX/CMn;->b:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;->IN_MESSENGER_SHOPPING_ENABLED:Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_a

    :goto_1
    :try_start_2
    sget-object v0, LX/CMn;->b:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;->COMMERCE_NUX_ENABLED:Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_9

    :goto_2
    :try_start_3
    sget-object v0, LX/CMn;->b:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;->STRUCTURED_MENU_ENABLED:Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_8

    :goto_3
    :try_start_4
    sget-object v0, LX/CMn;->b:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;->USER_CONTROL_TOPIC_MANAGE_ENABLED:Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_7

    :goto_4
    :try_start_5
    sget-object v0, LX/CMn;->b:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;->NULL_STATE_CTA_BUTTON_ALWAYS_ENABLED:Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLCommercePageSetting;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_6

    .line 1880952
    :goto_5
    invoke-static {}, Lcom/facebook/graphql/enums/GraphQLCommercePageType;->values()[Lcom/facebook/graphql/enums/GraphQLCommercePageType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/CMn;->a:[I

    :try_start_6
    sget-object v0, LX/CMn;->a:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCommercePageType;->COMMERCE_PAGE_TYPE_AGENT:Lcom/facebook/graphql/enums/GraphQLCommercePageType;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLCommercePageType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_5

    :goto_6
    :try_start_7
    sget-object v0, LX/CMn;->a:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCommercePageType;->COMMERCE_PAGE_TYPE_BANK:Lcom/facebook/graphql/enums/GraphQLCommercePageType;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLCommercePageType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_4

    :goto_7
    :try_start_8
    sget-object v0, LX/CMn;->a:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCommercePageType;->COMMERCE_PAGE_TYPE_BUSINESS:Lcom/facebook/graphql/enums/GraphQLCommercePageType;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLCommercePageType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_3

    :goto_8
    :try_start_9
    sget-object v0, LX/CMn;->a:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCommercePageType;->COMMERCE_PAGE_TYPE_RIDE_SHARE:Lcom/facebook/graphql/enums/GraphQLCommercePageType;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLCommercePageType;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_2

    :goto_9
    :try_start_a
    sget-object v0, LX/CMn;->a:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCommercePageType;->COMMERCE_PAGE_TYPE_MESSENGER_EXTENSION:Lcom/facebook/graphql/enums/GraphQLCommercePageType;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLCommercePageType;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_1

    :goto_a
    :try_start_b
    sget-object v0, LX/CMn;->a:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCommercePageType;->COMMERCE_PAGE_TYPE_NONE:Lcom/facebook/graphql/enums/GraphQLCommercePageType;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLCommercePageType;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_0

    :goto_b
    return-void

    :catch_0
    goto :goto_b

    :catch_1
    goto :goto_a

    :catch_2
    goto :goto_9

    :catch_3
    goto :goto_8

    :catch_4
    goto :goto_7

    :catch_5
    goto :goto_6

    :catch_6
    goto :goto_5

    :catch_7
    goto :goto_4

    :catch_8
    goto :goto_3

    :catch_9
    goto :goto_2

    :catch_a
    goto/16 :goto_1

    :catch_b
    goto/16 :goto_0
.end method
