.class public final LX/Bdq;
.super LX/1S3;
.source ""


# static fields
.field public static a:LX/Bdq;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Bdl;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public c:LX/Bdu;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1804277
    const/4 v0, 0x0

    sput-object v0, LX/Bdq;->a:LX/Bdq;

    .line 1804278
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/Bdq;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1804274
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1804275
    new-instance v0, LX/Bdu;

    invoke-direct {v0}, LX/Bdu;-><init>()V

    iput-object v0, p0, LX/Bdq;->c:LX/Bdu;

    .line 1804276
    return-void
.end method

.method public static a(LX/1De;LX/BcY;)LX/1dQ;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/BcY;",
            ")",
            "LX/1dQ",
            "<",
            "LX/5Jm;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1804273
    const v0, -0x5ccbdefe

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/1De;LX/BcL;Z)V
    .locals 2

    .prologue
    .line 1804266
    iget-object v0, p0, LX/1De;->g:LX/1X1;

    move-object v0, v0

    .line 1804267
    if-nez v0, :cond_0

    .line 1804268
    :goto_0
    return-void

    .line 1804269
    :cond_0
    check-cast v0, LX/Bdm;

    .line 1804270
    new-instance v1, LX/Bdo;

    invoke-direct {v1, p1, p2}, LX/Bdo;-><init>(LX/BcL;Z)V

    move-object v1, v1

    .line 1804271
    move-object v0, v1

    .line 1804272
    invoke-virtual {p0, v0}, LX/1De;->b(LX/48B;)V

    goto :goto_0
.end method

.method public static a(LX/1De;Z)V
    .locals 2

    .prologue
    .line 1804189
    iget-object v0, p0, LX/1De;->g:LX/1X1;

    move-object v0, v0

    .line 1804190
    if-nez v0, :cond_0

    .line 1804191
    :goto_0
    return-void

    .line 1804192
    :cond_0
    check-cast v0, LX/Bdm;

    .line 1804193
    new-instance v1, LX/Bdn;

    invoke-direct {v1, p1}, LX/Bdn;-><init>(Z)V

    move-object v1, v1

    .line 1804194
    move-object v0, v1

    .line 1804195
    invoke-virtual {p0, v0}, LX/1De;->b(LX/48B;)V

    goto :goto_0
.end method

.method public static c(LX/1De;)LX/Bdl;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1804258
    new-instance v1, LX/Bdm;

    invoke-direct {v1}, LX/Bdm;-><init>()V

    .line 1804259
    sget-object v2, LX/Bdq;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Bdl;

    .line 1804260
    if-nez v2, :cond_0

    .line 1804261
    new-instance v2, LX/Bdl;

    invoke-direct {v2}, LX/Bdl;-><init>()V

    .line 1804262
    :cond_0
    invoke-static {v2, p0, v0, v0, v1}, LX/Bdl;->a$redex0(LX/Bdl;LX/1De;IILX/Bdm;)V

    .line 1804263
    move-object v1, v2

    .line 1804264
    move-object v0, v1

    .line 1804265
    return-object v0
.end method

.method public static declared-synchronized q()LX/Bdq;
    .locals 2

    .prologue
    .line 1804279
    const-class v1, LX/Bdq;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/Bdq;->a:LX/Bdq;

    if-nez v0, :cond_0

    .line 1804280
    new-instance v0, LX/Bdq;

    invoke-direct {v0}, LX/Bdq;-><init>()V

    sput-object v0, LX/Bdq;->a:LX/Bdq;

    .line 1804281
    :cond_0
    sget-object v0, LX/Bdq;->a:LX/Bdq;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 1804282
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 21

    .prologue
    .line 1804255
    check-cast p2, LX/Bdm;

    .line 1804256
    move-object/from16 v0, p2

    iget-object v2, v0, LX/Bdm;->a:LX/BcO;

    move-object/from16 v0, p2

    iget-object v3, v0, LX/Bdm;->b:LX/Bdb;

    move-object/from16 v0, p2

    iget-object v4, v0, LX/Bdm;->c:LX/1X1;

    move-object/from16 v0, p2

    iget-object v5, v0, LX/Bdm;->d:LX/1X1;

    move-object/from16 v0, p2

    iget-object v6, v0, LX/Bdm;->e:LX/1X1;

    move-object/from16 v0, p2

    iget-object v7, v0, LX/Bdm;->f:LX/1OX;

    move-object/from16 v0, p2

    iget-object v8, v0, LX/Bdm;->g:LX/BcG;

    move-object/from16 v0, p2

    iget-boolean v9, v0, LX/Bdm;->h:Z

    move-object/from16 v0, p2

    iget v10, v0, LX/Bdm;->i:I

    move-object/from16 v0, p2

    iget-object v11, v0, LX/Bdm;->j:LX/3x6;

    move-object/from16 v0, p2

    iget-object v12, v0, LX/Bdm;->k:LX/5K7;

    move-object/from16 v0, p2

    iget-object v13, v0, LX/Bdm;->l:LX/1Of;

    move-object/from16 v0, p2

    iget-boolean v14, v0, LX/Bdm;->m:Z

    move-object/from16 v0, p2

    iget-object v15, v0, LX/Bdm;->n:LX/BcL;

    move-object/from16 v0, p2

    iget-boolean v0, v0, LX/Bdm;->o:Z

    move/from16 v16, v0

    move-object/from16 v0, p2

    iget-object v0, v0, LX/Bdm;->p:LX/Bdv;

    move-object/from16 v17, v0

    move-object/from16 v0, p2

    iget-object v0, v0, LX/Bdm;->q:LX/BcY;

    move-object/from16 v18, v0

    move-object/from16 v0, p2

    iget-boolean v0, v0, LX/Bdm;->r:Z

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget-object v0, v0, LX/Bdm;->s:LX/25S;

    move-object/from16 v20, v0

    move-object/from16 v1, p1

    invoke-static/range {v1 .. v20}, LX/Bdu;->a(LX/1De;LX/BcO;LX/Bdb;LX/1X1;LX/1X1;LX/1X1;LX/1OX;LX/BcG;ZILX/3x6;LX/5K7;LX/1Of;ZLX/BcL;ZLX/Bdv;LX/BcY;ZLX/25S;)LX/1Dg;

    move-result-object v1

    .line 1804257
    return-object v1
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1804248
    invoke-static {}, LX/1dS;->b()V

    .line 1804249
    iget v0, p1, LX/1dQ;->b:I

    .line 1804250
    packed-switch v0, :pswitch_data_0

    .line 1804251
    :goto_0
    return-object v2

    .line 1804252
    :pswitch_0
    iget-object v0, p1, LX/1dQ;->c:[Ljava/lang/Object;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    check-cast v0, LX/BcY;

    .line 1804253
    invoke-virtual {v0}, LX/BcY;->a()V

    .line 1804254
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x5ccbdefe
        :pswitch_0
    .end packed-switch
.end method

.method public final a(LX/1X1;LX/1X1;)V
    .locals 1

    .prologue
    .line 1804239
    check-cast p1, LX/Bdm;

    .line 1804240
    check-cast p2, LX/Bdm;

    .line 1804241
    iget-object v0, p1, LX/Bdm;->n:LX/BcL;

    iput-object v0, p2, LX/Bdm;->n:LX/BcL;

    .line 1804242
    iget-boolean v0, p1, LX/Bdm;->o:Z

    iput-boolean v0, p2, LX/Bdm;->o:Z

    .line 1804243
    iget-object v0, p1, LX/Bdm;->p:LX/Bdv;

    iput-object v0, p2, LX/Bdm;->p:LX/Bdv;

    .line 1804244
    iget-object v0, p1, LX/Bdm;->q:LX/BcY;

    iput-object v0, p2, LX/Bdm;->q:LX/BcY;

    .line 1804245
    iget-boolean v0, p1, LX/Bdm;->r:Z

    iput-boolean v0, p2, LX/Bdm;->r:Z

    .line 1804246
    iget-object v0, p1, LX/Bdm;->s:LX/25S;

    iput-object v0, p2, LX/Bdm;->s:LX/25S;

    .line 1804247
    return-void
.end method

.method public final d(LX/1De;LX/1X1;)V
    .locals 9

    .prologue
    .line 1804197
    check-cast p2, LX/Bdm;

    .line 1804198
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v2

    .line 1804199
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v3

    .line 1804200
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v4

    .line 1804201
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v5

    .line 1804202
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v6

    .line 1804203
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v7

    .line 1804204
    iget-object v1, p2, LX/Bdm;->b:LX/Bdb;

    move-object v0, p1

    .line 1804205
    new-instance v8, LX/BcP;

    invoke-direct {v8, v0}, LX/BcP;-><init>(LX/1De;)V

    .line 1804206
    new-instance p0, LX/Bdv;

    invoke-direct {p0}, LX/Bdv;-><init>()V

    .line 1804207
    invoke-interface {v1}, LX/Bdb;->a()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 1804208
    new-instance p1, LX/Bdr;

    invoke-direct {p1, v0}, LX/Bdr;-><init>(LX/1De;)V

    .line 1804209
    iput-object p1, v2, LX/1np;->a:Ljava/lang/Object;

    .line 1804210
    :cond_0
    new-instance p1, LX/BcU;

    invoke-direct {p1, v8, p0}, LX/BcU;-><init>(LX/BcP;LX/BcX;)V

    move-object v8, p1

    .line 1804211
    new-instance p1, LX/BcY;

    invoke-direct {p1, v8}, LX/BcY;-><init>(LX/BcU;)V

    move-object v8, p1

    .line 1804212
    iput-object v8, v3, LX/1np;->a:Ljava/lang/Object;

    .line 1804213
    iput-object p0, v4, LX/1np;->a:Ljava/lang/Object;

    .line 1804214
    const/4 v8, 0x1

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    .line 1804215
    iput-object v8, v6, LX/1np;->a:Ljava/lang/Object;

    .line 1804216
    sget-object v8, LX/BcL;->INITIAL_LOAD:LX/BcL;

    .line 1804217
    iput-object v8, v5, LX/1np;->a:Ljava/lang/Object;

    .line 1804218
    const/4 v8, 0x0

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    .line 1804219
    iput-object v8, v7, LX/1np;->a:Ljava/lang/Object;

    .line 1804220
    iget-object v0, v2, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 1804221
    check-cast v0, LX/25S;

    iput-object v0, p2, LX/Bdm;->s:LX/25S;

    .line 1804222
    invoke-static {v2}, LX/1cy;->a(LX/1np;)V

    .line 1804223
    iget-object v0, v3, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 1804224
    check-cast v0, LX/BcY;

    iput-object v0, p2, LX/Bdm;->q:LX/BcY;

    .line 1804225
    invoke-static {v3}, LX/1cy;->a(LX/1np;)V

    .line 1804226
    iget-object v0, v4, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 1804227
    check-cast v0, LX/Bdv;

    iput-object v0, p2, LX/Bdm;->p:LX/Bdv;

    .line 1804228
    invoke-static {v4}, LX/1cy;->a(LX/1np;)V

    .line 1804229
    iget-object v0, v5, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 1804230
    check-cast v0, LX/BcL;

    iput-object v0, p2, LX/Bdm;->n:LX/BcL;

    .line 1804231
    invoke-static {v5}, LX/1cy;->a(LX/1np;)V

    .line 1804232
    iget-object v0, v6, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 1804233
    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p2, LX/Bdm;->o:Z

    .line 1804234
    invoke-static {v6}, LX/1cy;->a(LX/1np;)V

    .line 1804235
    iget-object v0, v7, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 1804236
    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p2, LX/Bdm;->r:Z

    .line 1804237
    invoke-static {v7}, LX/1cy;->a(LX/1np;)V

    .line 1804238
    return-void
.end method

.method public final p()Z
    .locals 1

    .prologue
    .line 1804196
    const/4 v0, 0x1

    return v0
.end method
