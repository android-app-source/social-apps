.class public LX/Bwq;
.super LX/3Ga;
.source ""


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation


# static fields
.field public static final b:LX/0Tn;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public static final e:LX/0Tn;


# instance fields
.field public c:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0W3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final f:I

.field private final o:I

.field public p:Lcom/facebook/widget/FbImageView;

.field public q:Landroid/animation/AnimatorSet;

.field private r:Landroid/animation/Animator$AnimatorListener;

.field public s:I

.field public t:Z

.field public u:I

.field public v:Ljava/lang/Runnable;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1834458
    sget-object v0, LX/0Tm;->g:LX/0Tn;

    const-string v1, "video_rotate_hint/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 1834459
    sput-object v0, LX/Bwq;->b:LX/0Tn;

    const-string v1, "num_times"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/Bwq;->e:LX/0Tn;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1834456
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/Bwq;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1834457
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1834454
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/Bwq;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1834455
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    .line 1834447
    invoke-direct {p0, p1, p2, p3}, LX/3Ga;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1834448
    const-class v0, LX/Bwq;

    invoke-static {v0, p0}, LX/Bwq;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1834449
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/Bwp;

    invoke-direct {v1, p0, p0}, LX/Bwp;-><init>(LX/Bwq;LX/2oy;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1834450
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/Bwo;

    invoke-direct {v1, p0}, LX/Bwo;-><init>(LX/Bwq;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1834451
    iget-object v0, p0, LX/Bwq;->d:LX/0W3;

    sget-wide v2, LX/0X5;->ex:J

    const/16 v1, 0xa

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JI)I

    move-result v0

    iput v0, p0, LX/Bwq;->o:I

    .line 1834452
    iget-object v0, p0, LX/Bwq;->d:LX/0W3;

    sget-wide v2, LX/0X5;->ez:J

    const/4 v1, 0x3

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JI)I

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    iput v0, p0, LX/Bwq;->f:I

    .line 1834453
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/Bwq;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v1

    check-cast v1, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object p0

    check-cast p0, LX/0W3;

    iput-object v1, p1, LX/Bwq;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object p0, p1, LX/Bwq;->d:LX/0W3;

    return-void
.end method

.method private k()V
    .locals 12

    .prologue
    const/4 v11, 0x3

    const/4 v10, 0x1

    const/4 v9, 0x2

    const/4 v8, 0x0

    .line 1834429
    iget-object v0, p0, LX/Bwq;->p:Lcom/facebook/widget/FbImageView;

    sget-object v1, Landroid/view/View;->SCALE_X:Landroid/util/Property;

    new-array v2, v9, [F

    fill-array-data v2, :array_0

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 1834430
    iget-object v1, p0, LX/Bwq;->p:Lcom/facebook/widget/FbImageView;

    sget-object v2, Landroid/view/View;->SCALE_Y:Landroid/util/Property;

    new-array v3, v9, [F

    fill-array-data v3, :array_1

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 1834431
    iget-object v2, p0, LX/Bwq;->p:Lcom/facebook/widget/FbImageView;

    sget-object v3, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v4, v9, [F

    fill-array-data v4, :array_2

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 1834432
    iget-object v3, p0, LX/Bwq;->p:Lcom/facebook/widget/FbImageView;

    sget-object v4, Landroid/view/View;->ROTATION:Landroid/util/Property;

    new-array v5, v10, [F

    const/high16 v6, -0x3d4c0000    # -90.0f

    aput v6, v5, v8

    invoke-static {v3, v4, v5}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v3

    .line 1834433
    iget-object v4, p0, LX/Bwq;->p:Lcom/facebook/widget/FbImageView;

    sget-object v5, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v6, v10, [F

    const/4 v7, 0x0

    aput v7, v6, v8

    invoke-static {v4, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v4

    .line 1834434
    const-wide/16 v6, 0x1f4

    invoke-virtual {v3, v6, v7}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 1834435
    const-wide/16 v6, 0x15e

    invoke-virtual {v4, v6, v7}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 1834436
    new-instance v5, Landroid/animation/AnimatorSet;

    invoke-direct {v5}, Landroid/animation/AnimatorSet;-><init>()V

    .line 1834437
    new-array v6, v11, [Landroid/animation/Animator;

    aput-object v0, v6, v8

    aput-object v1, v6, v10

    aput-object v2, v6, v9

    invoke-virtual {v5, v6}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 1834438
    const-wide/16 v0, 0xc8

    invoke-virtual {v5, v0, v1}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 1834439
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v0, p0, LX/Bwq;->q:Landroid/animation/AnimatorSet;

    .line 1834440
    iget-object v0, p0, LX/Bwq;->q:Landroid/animation/AnimatorSet;

    new-array v1, v11, [Landroid/animation/Animator;

    aput-object v5, v1, v8

    aput-object v3, v1, v10

    aput-object v4, v1, v9

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->playSequentially([Landroid/animation/Animator;)V

    .line 1834441
    new-instance v0, LX/Bwn;

    invoke-direct {v0, p0}, LX/Bwn;-><init>(LX/Bwq;)V

    iput-object v0, p0, LX/Bwq;->r:Landroid/animation/Animator$AnimatorListener;

    .line 1834442
    iget-object v0, p0, LX/Bwq;->q:Landroid/animation/AnimatorSet;

    iget-object v1, p0, LX/Bwq;->r:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1834443
    return-void

    .line 1834444
    :array_0
    .array-data 4
        0x3f4ccccd    # 0.8f
        0x3f800000    # 1.0f
    .end array-data

    .line 1834445
    :array_1
    .array-data 4
        0x3f4ccccd    # 0.8f
        0x3f800000    # 1.0f
    .end array-data

    .line 1834446
    :array_2
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public static v(LX/Bwq;)V
    .locals 4

    .prologue
    .line 1834421
    iget-object v0, p0, LX/Bwq;->v:Ljava/lang/Runnable;

    if-nez v0, :cond_0

    .line 1834422
    new-instance v0, Lcom/facebook/feed/video/inline/RotateForFullscreenNuxPlugin$1;

    invoke-direct {v0, p0}, Lcom/facebook/feed/video/inline/RotateForFullscreenNuxPlugin$1;-><init>(LX/Bwq;)V

    iput-object v0, p0, LX/Bwq;->v:Ljava/lang/Runnable;

    .line 1834423
    :cond_0
    invoke-static {p0}, LX/Bwq;->w(LX/Bwq;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, LX/Bwq;->t:Z

    if-nez v0, :cond_1

    .line 1834424
    iget-boolean v0, p0, LX/3Ga;->c:Z

    move v0, v0

    .line 1834425
    if-eqz v0, :cond_1

    .line 1834426
    iget-object v0, p0, LX/Bwq;->p:Lcom/facebook/widget/FbImageView;

    iget-object v1, p0, LX/Bwq;->v:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/FbImageView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 1834427
    iget-object v0, p0, LX/Bwq;->p:Lcom/facebook/widget/FbImageView;

    iget-object v1, p0, LX/Bwq;->v:Ljava/lang/Runnable;

    iget v2, p0, LX/Bwq;->f:I

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/widget/FbImageView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1834428
    :cond_1
    return-void
.end method

.method public static w(LX/Bwq;)Z
    .locals 2

    .prologue
    .line 1834460
    iget v0, p0, LX/Bwq;->s:I

    iget v1, p0, LX/Bwq;->o:I

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static x(LX/Bwq;)V
    .locals 1

    .prologue
    .line 1834418
    iget-object v0, p0, LX/Bwq;->q:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Bwq;->q:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1834419
    iget-object v0, p0, LX/Bwq;->q:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    .line 1834420
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/2pa;Z)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1834406
    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v1, "GraphQLStoryProps"

    invoke-virtual {v0, v1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v1, "GraphQLStoryProps"

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v0, :cond_1

    .line 1834407
    :cond_0
    :goto_0
    return-void

    .line 1834408
    :cond_1
    invoke-static {p1}, LX/393;->d(LX/2pa;)Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    .line 1834409
    if-eqz v0, :cond_0

    .line 1834410
    iget-object v0, p0, LX/Bwq;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/Bwq;->e:LX/0Tn;

    invoke-interface {v0, v1, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;I)I

    move-result v0

    iput v0, p0, LX/Bwq;->s:I

    .line 1834411
    invoke-virtual {p0}, LX/3Ga;->g()Z

    .line 1834412
    if-eqz p2, :cond_0

    iget-wide v0, p1, LX/2pa;->d:D

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    cmpl-double v0, v0, v2

    if-ltz v0, :cond_0

    .line 1834413
    iput-boolean v4, p0, LX/Bwq;->t:Z

    .line 1834414
    iput v4, p0, LX/Bwq;->u:I

    .line 1834415
    const/4 v0, 0x0

    iput-object v0, p0, LX/Bwq;->v:Ljava/lang/Runnable;

    .line 1834416
    invoke-direct {p0}, LX/Bwq;->k()V

    .line 1834417
    invoke-static {p0}, LX/Bwq;->v(LX/Bwq;)V

    goto :goto_0
.end method

.method public final b(LX/2pa;)Z
    .locals 4

    .prologue
    .line 1834401
    iget-wide v0, p1, LX/2pa;->d:D

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    cmpl-double v0, v0, v2

    if-ltz v0, :cond_0

    iget v0, p0, LX/Bwq;->s:I

    iget v1, p0, LX/Bwq;->o:I

    if-ge v0, v1, :cond_0

    .line 1834402
    iget-object v0, p0, LX/2oy;->k:Lcom/facebook/video/player/RichVideoPlayer;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/2oy;->k:Lcom/facebook/video/player/RichVideoPlayer;

    .line 1834403
    iget-object v1, v0, Lcom/facebook/video/player/RichVideoPlayer;->H:LX/04D;

    move-object v0, v1

    .line 1834404
    sget-object v1, LX/04D;->FEED:LX/04D;

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 1834405
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 1834399
    invoke-static {p0}, LX/Bwq;->x(LX/Bwq;)V

    .line 1834400
    return-void
.end method

.method public getLayoutToInflate()I
    .locals 1

    .prologue
    .line 1834393
    const v0, 0x7f030f36

    return v0
.end method

.method public getStubLayout()I
    .locals 1

    .prologue
    .line 1834398
    const v0, 0x7f030886

    return v0
.end method

.method public setupPlugin(LX/2pa;)V
    .locals 0

    .prologue
    .line 1834397
    return-void
.end method

.method public setupViews(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1834394
    const v0, 0x7f0d24de

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/FbImageView;

    iput-object v0, p0, LX/Bwq;->p:Lcom/facebook/widget/FbImageView;

    .line 1834395
    iget-object v0, p0, LX/Bwq;->p:Lcom/facebook/widget/FbImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/FbImageView;->setImageAlpha(I)V

    .line 1834396
    return-void
.end method
