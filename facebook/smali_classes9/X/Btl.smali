.class public final LX/Btl;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/Btm;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lcom/facebook/graphql/model/GraphQLPlace;

.field public c:Z

.field public d:LX/1Pm;

.field public e:Lcom/facebook/analytics/logger/HoneyClientEvent;

.field public final synthetic f:LX/Btm;


# direct methods
.method public constructor <init>(LX/Btm;)V
    .locals 1

    .prologue
    .line 1829430
    iput-object p1, p0, LX/Btl;->f:LX/Btm;

    .line 1829431
    move-object v0, p1

    .line 1829432
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1829433
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1829434
    const-string v0, "StoryLocationPlaceInfoComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1829435
    if-ne p0, p1, :cond_1

    .line 1829436
    :cond_0
    :goto_0
    return v0

    .line 1829437
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1829438
    goto :goto_0

    .line 1829439
    :cond_3
    check-cast p1, LX/Btl;

    .line 1829440
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1829441
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1829442
    if-eq v2, v3, :cond_0

    .line 1829443
    iget-object v2, p0, LX/Btl;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/Btl;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/Btl;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1829444
    goto :goto_0

    .line 1829445
    :cond_5
    iget-object v2, p1, LX/Btl;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 1829446
    :cond_6
    iget-object v2, p0, LX/Btl;->b:Lcom/facebook/graphql/model/GraphQLPlace;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/Btl;->b:Lcom/facebook/graphql/model/GraphQLPlace;

    iget-object v3, p1, LX/Btl;->b:Lcom/facebook/graphql/model/GraphQLPlace;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1829447
    goto :goto_0

    .line 1829448
    :cond_8
    iget-object v2, p1, LX/Btl;->b:Lcom/facebook/graphql/model/GraphQLPlace;

    if-nez v2, :cond_7

    .line 1829449
    :cond_9
    iget-boolean v2, p0, LX/Btl;->c:Z

    iget-boolean v3, p1, LX/Btl;->c:Z

    if-eq v2, v3, :cond_a

    move v0, v1

    .line 1829450
    goto :goto_0

    .line 1829451
    :cond_a
    iget-object v2, p0, LX/Btl;->d:LX/1Pm;

    if-eqz v2, :cond_c

    iget-object v2, p0, LX/Btl;->d:LX/1Pm;

    iget-object v3, p1, LX/Btl;->d:LX/1Pm;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    :cond_b
    move v0, v1

    .line 1829452
    goto :goto_0

    .line 1829453
    :cond_c
    iget-object v2, p1, LX/Btl;->d:LX/1Pm;

    if-nez v2, :cond_b

    .line 1829454
    :cond_d
    iget-object v2, p0, LX/Btl;->e:Lcom/facebook/analytics/logger/HoneyClientEvent;

    if-eqz v2, :cond_e

    iget-object v2, p0, LX/Btl;->e:Lcom/facebook/analytics/logger/HoneyClientEvent;

    iget-object v3, p1, LX/Btl;->e:Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1829455
    goto :goto_0

    .line 1829456
    :cond_e
    iget-object v2, p1, LX/Btl;->e:Lcom/facebook/analytics/logger/HoneyClientEvent;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
