.class public final LX/Ay4;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:J

.field public d:J

.field private e:F

.field public f:D

.field public g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1729330
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1729331
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, LX/Ay4;->e:F

    .line 1729332
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    iput-wide v0, p0, LX/Ay4;->f:D

    .line 1729333
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;
    .locals 13

    .prologue
    .line 1729334
    new-instance v1, Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;

    iget-object v0, p0, LX/Ay4;->a:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iget-object v3, p0, LX/Ay4;->b:Ljava/lang/String;

    iget-wide v4, p0, LX/Ay4;->c:J

    iget-wide v6, p0, LX/Ay4;->d:J

    iget v8, p0, LX/Ay4;->e:F

    iget-wide v9, p0, LX/Ay4;->f:D

    iget-object v11, p0, LX/Ay4;->g:Ljava/util/Map;

    const/4 v12, 0x0

    invoke-direct/range {v1 .. v12}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;-><init>(Ljava/lang/String;Ljava/lang/String;JJFDLjava/util/Map;B)V

    return-object v1
.end method
