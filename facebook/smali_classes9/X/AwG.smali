.class public final LX/AwG;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1725966
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLComposerLinkShareActionLink;)I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1725802
    if-nez p1, :cond_0

    .line 1725803
    :goto_0
    return v0

    .line 1725804
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComposerLinkShareActionLink;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1725805
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComposerLinkShareActionLink;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1725806
    const/4 v3, 0x2

    invoke-virtual {p0, v3}, LX/186;->c(I)V

    .line 1725807
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 1725808
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v2}, LX/186;->b(II)V

    .line 1725809
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 1725810
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLCreativeFilter;)I
    .locals 8

    .prologue
    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    .line 1725811
    if-nez p1, :cond_0

    .line 1725812
    :goto_0
    return v1

    .line 1725813
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLCreativeFilter;->l()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1725814
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLCreativeFilter;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1725815
    const/4 v0, 0x7

    invoke-virtual {p0, v0}, LX/186;->c(I)V

    .line 1725816
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLCreativeFilter;->j()D

    move-result-wide v2

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1725817
    const/4 v1, 0x1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLCreativeFilter;->k()D

    move-result-wide v2

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1725818
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v6}, LX/186;->b(II)V

    .line 1725819
    const/4 v1, 0x3

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLCreativeFilter;->m()D

    move-result-wide v2

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1725820
    const/4 v0, 0x4

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLCreativeFilter;->n()Z

    move-result v1

    invoke-virtual {p0, v0, v1}, LX/186;->a(IZ)V

    .line 1725821
    const/4 v0, 0x5

    invoke-virtual {p0, v0, v7}, LX/186;->b(II)V

    .line 1725822
    const/4 v1, 0x6

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLCreativeFilter;->p()D

    move-result-wide v2

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1725823
    invoke-virtual {p0}, LX/186;->d()I

    move-result v1

    .line 1725824
    invoke-virtual {p0, v1}, LX/186;->d(I)V

    goto :goto_0
.end method

.method public static a(LX/186;Lcom/facebook/graphql/model/GraphQLEntityAtRange;)I
    .locals 11

    .prologue
    const/4 v0, 0x0

    .line 1725825
    if-nez p1, :cond_0

    .line 1725826
    :goto_0
    return v0

    .line 1725827
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEntityAtRange;->j()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v1

    const/4 v2, 0x0

    .line 1725828
    if-nez v1, :cond_1

    .line 1725829
    :goto_1
    move v1, v2

    .line 1725830
    const/4 v2, 0x3

    invoke-virtual {p0, v2}, LX/186;->c(I)V

    .line 1725831
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 1725832
    const/4 v1, 0x1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEntityAtRange;->b()I

    move-result v2

    invoke-virtual {p0, v1, v2, v0}, LX/186;->a(III)V

    .line 1725833
    const/4 v1, 0x2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEntityAtRange;->c()I

    move-result v2

    invoke-virtual {p0, v1, v2, v0}, LX/186;->a(III)V

    .line 1725834
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 1725835
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0

    .line 1725836
    :cond_1
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEntity;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    .line 1725837
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEntity;->c()LX/0Px;

    move-result-object v4

    invoke-virtual {p0, v4}, LX/186;->b(Ljava/util/List;)I

    move-result v4

    .line 1725838
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEntity;->d()Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    move-result-object v5

    invoke-virtual {p0, v5}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v5

    .line 1725839
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEntity;->e()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1725840
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEntity;->v_()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1725841
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEntity;->w_()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 1725842
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEntity;->j()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 1725843
    const/4 v10, 0x7

    invoke-virtual {p0, v10}, LX/186;->c(I)V

    .line 1725844
    invoke-virtual {p0, v2, v3}, LX/186;->b(II)V

    .line 1725845
    const/4 v2, 0x1

    invoke-virtual {p0, v2, v4}, LX/186;->b(II)V

    .line 1725846
    const/4 v2, 0x2

    invoke-virtual {p0, v2, v5}, LX/186;->b(II)V

    .line 1725847
    const/4 v2, 0x3

    invoke-virtual {p0, v2, v6}, LX/186;->b(II)V

    .line 1725848
    const/4 v2, 0x4

    invoke-virtual {p0, v2, v7}, LX/186;->b(II)V

    .line 1725849
    const/4 v2, 0x5

    invoke-virtual {p0, v2, v8}, LX/186;->b(II)V

    .line 1725850
    const/4 v2, 0x6

    invoke-virtual {p0, v2, v9}, LX/186;->b(II)V

    .line 1725851
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 1725852
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto :goto_1
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLEventTimeRange;)I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 1725853
    if-nez p1, :cond_0

    .line 1725854
    :goto_0
    return v0

    .line 1725855
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEventTimeRange;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1725856
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEventTimeRange;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1725857
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLEventTimeRange;->k()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1725858
    const/4 v4, 0x3

    invoke-virtual {p0, v4}, LX/186;->c(I)V

    .line 1725859
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 1725860
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v2}, LX/186;->b(II)V

    .line 1725861
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v3}, LX/186;->b(II)V

    .line 1725862
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 1725863
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0
.end method

.method public static a(LX/186;Lcom/facebook/graphql/model/GraphQLFrameImageAsset;)I
    .locals 14

    .prologue
    const/4 v0, 0x0

    .line 1725864
    if-nez p1, :cond_0

    .line 1725865
    :goto_0
    return v0

    .line 1725866
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;->j()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    const/4 v2, 0x0

    .line 1725867
    if-nez v1, :cond_1

    .line 1725868
    :goto_1
    move v1, v2

    .line 1725869
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;->k()Lcom/facebook/graphql/model/GraphQLFrameImageAssetSize;

    move-result-object v2

    const/4 v8, 0x0

    .line 1725870
    if-nez v2, :cond_2

    .line 1725871
    :goto_2
    move v2, v8

    .line 1725872
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;->l()Lcom/facebook/graphql/model/GraphQLFrameImageAssetSize;

    move-result-object v3

    const/4 v8, 0x0

    .line 1725873
    if-nez v3, :cond_3

    .line 1725874
    :goto_3
    move v3, v8

    .line 1725875
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;->m()Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;

    move-result-object v4

    const/4 v7, 0x0

    const-wide/16 v11, 0x0

    .line 1725876
    if-nez v4, :cond_4

    .line 1725877
    :goto_4
    move v4, v7

    .line 1725878
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;->n()Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;

    move-result-object v5

    const/4 v7, 0x0

    const-wide/16 v11, 0x0

    .line 1725879
    if-nez v5, :cond_5

    .line 1725880
    :goto_5
    move v5, v7

    .line 1725881
    const/4 v6, 0x6

    invoke-virtual {p0, v6}, LX/186;->c(I)V

    .line 1725882
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 1725883
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v2}, LX/186;->b(II)V

    .line 1725884
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v3}, LX/186;->b(II)V

    .line 1725885
    const/4 v0, 0x3

    invoke-virtual {p0, v0, v4}, LX/186;->b(II)V

    .line 1725886
    const/4 v0, 0x4

    invoke-virtual {p0, v0, v5}, LX/186;->b(II)V

    .line 1725887
    const/4 v1, 0x5

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;->o()D

    move-result-wide v2

    const-wide/16 v4, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1725888
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 1725889
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0

    .line 1725890
    :cond_1
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1725891
    const/4 v4, 0x3

    invoke-virtual {p0, v4}, LX/186;->c(I)V

    .line 1725892
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v4

    invoke-virtual {p0, v2, v4, v2}, LX/186;->a(III)V

    .line 1725893
    const/4 v4, 0x1

    invoke-virtual {p0, v4, v3}, LX/186;->b(II)V

    .line 1725894
    const/4 v3, 0x2

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v4

    invoke-virtual {p0, v3, v4, v2}, LX/186;->a(III)V

    .line 1725895
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 1725896
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto :goto_1

    .line 1725897
    :cond_2
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFrameImageAssetSize;->j()Lcom/facebook/graphql/enums/GraphQLAssetSizeDimensionType;

    move-result-object v7

    invoke-virtual {p0, v7}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v13

    .line 1725898
    const/4 v7, 0x2

    invoke-virtual {p0, v7}, LX/186;->c(I)V

    .line 1725899
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFrameImageAssetSize;->a()D

    move-result-wide v9

    const-wide/16 v11, 0x0

    move-object v7, p0

    invoke-virtual/range {v7 .. v12}, LX/186;->a(IDD)V

    .line 1725900
    const/4 v7, 0x1

    invoke-virtual {p0, v7, v13}, LX/186;->b(II)V

    .line 1725901
    invoke-virtual {p0}, LX/186;->d()I

    move-result v8

    .line 1725902
    invoke-virtual {p0, v8}, LX/186;->d(I)V

    goto/16 :goto_2

    .line 1725903
    :cond_3
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFrameImageAssetSize;->j()Lcom/facebook/graphql/enums/GraphQLAssetSizeDimensionType;

    move-result-object v7

    invoke-virtual {p0, v7}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v13

    .line 1725904
    const/4 v7, 0x2

    invoke-virtual {p0, v7}, LX/186;->c(I)V

    .line 1725905
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFrameImageAssetSize;->a()D

    move-result-wide v9

    const-wide/16 v11, 0x0

    move-object v7, p0

    invoke-virtual/range {v7 .. v12}, LX/186;->a(IDD)V

    .line 1725906
    const/4 v7, 0x1

    invoke-virtual {p0, v7, v13}, LX/186;->b(II)V

    .line 1725907
    invoke-virtual {p0}, LX/186;->d()I

    move-result v8

    .line 1725908
    invoke-virtual {p0, v8}, LX/186;->d(I)V

    goto/16 :goto_3

    .line 1725909
    :cond_4
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;->a()Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

    move-result-object v8

    invoke-virtual {p0, v8}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v8

    .line 1725910
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;->k()Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;

    move-result-object v9

    invoke-virtual {p0, v9}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v13

    .line 1725911
    const/4 v9, 0x4

    invoke-virtual {p0, v9}, LX/186;->c(I)V

    .line 1725912
    invoke-virtual {p0, v7, v8}, LX/186;->b(II)V

    .line 1725913
    const/4 v8, 0x1

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;->j()D

    move-result-wide v9

    move-object v7, p0

    invoke-virtual/range {v7 .. v12}, LX/186;->a(IDD)V

    .line 1725914
    const/4 v7, 0x2

    invoke-virtual {p0, v7, v13}, LX/186;->b(II)V

    .line 1725915
    const/4 v8, 0x3

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;->l()D

    move-result-wide v9

    move-object v7, p0

    invoke-virtual/range {v7 .. v12}, LX/186;->a(IDD)V

    .line 1725916
    invoke-virtual {p0}, LX/186;->d()I

    move-result v7

    .line 1725917
    invoke-virtual {p0, v7}, LX/186;->d(I)V

    goto/16 :goto_4

    .line 1725918
    :cond_5
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;->a()Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

    move-result-object v8

    invoke-virtual {p0, v8}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v8

    .line 1725919
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;->k()Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;

    move-result-object v9

    invoke-virtual {p0, v9}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v13

    .line 1725920
    const/4 v9, 0x4

    invoke-virtual {p0, v9}, LX/186;->c(I)V

    .line 1725921
    invoke-virtual {p0, v7, v8}, LX/186;->b(II)V

    .line 1725922
    const/4 v8, 0x1

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;->j()D

    move-result-wide v9

    move-object v7, p0

    invoke-virtual/range {v7 .. v12}, LX/186;->a(IDD)V

    .line 1725923
    const/4 v7, 0x2

    invoke-virtual {p0, v7, v13}, LX/186;->b(II)V

    .line 1725924
    const/4 v8, 0x3

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;->l()D

    move-result-wide v9

    move-object v7, p0

    invoke-virtual/range {v7 .. v12}, LX/186;->a(IDD)V

    .line 1725925
    invoke-virtual {p0}, LX/186;->d()I

    move-result v7

    .line 1725926
    invoke-virtual {p0, v7}, LX/186;->d(I)V

    goto/16 :goto_5
.end method

.method public static a(LX/186;Lcom/facebook/graphql/model/GraphQLFrameTextAsset;)I
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/4 v2, 0x0

    .line 1725927
    if-nez p1, :cond_0

    .line 1725928
    :goto_0
    return v2

    .line 1725929
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->j()LX/0Px;

    move-result-object v3

    .line 1725930
    if-eqz v3, :cond_2

    .line 1725931
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    new-array v4, v0, [I

    move v1, v2

    .line 1725932
    :goto_1
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1725933
    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFrameDynamicClientTextAsset;

    const/4 v5, 0x0

    .line 1725934
    if-nez v0, :cond_3

    .line 1725935
    :goto_2
    move v0, v5

    .line 1725936
    aput v0, v4, v1

    .line 1725937
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1725938
    :cond_1
    invoke-virtual {p0, v4, v11}, LX/186;->a([IZ)I

    move-result v0

    .line 1725939
    :goto_3
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->k()Lcom/facebook/graphql/model/GraphQLMediaEffectCustomFontResourceConnection;

    move-result-object v1

    invoke-static {p0, v1}, LX/AwG;->a(LX/186;Lcom/facebook/graphql/model/GraphQLMediaEffectCustomFontResourceConnection;)I

    move-result v1

    .line 1725940
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->l()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1725941
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->m()Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;

    move-result-object v4

    invoke-static {p0, v4}, LX/AwG;->c(LX/186;Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;)I

    move-result v4

    .line 1725942
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->n()Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;

    move-result-object v5

    invoke-static {p0, v5}, LX/AwG;->d(LX/186;Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;)I

    move-result v5

    .line 1725943
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->p()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1725944
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->q()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1725945
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->r()Lcom/facebook/graphql/model/GraphQLFrameTextAssetSize;

    move-result-object v8

    invoke-static {p0, v8}, LX/AwG;->a(LX/186;Lcom/facebook/graphql/model/GraphQLFrameTextAssetSize;)I

    move-result v8

    .line 1725946
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->s()Lcom/facebook/graphql/model/GraphQLFrameTextAssetSize;

    move-result-object v9

    invoke-static {p0, v9}, LX/AwG;->b(LX/186;Lcom/facebook/graphql/model/GraphQLFrameTextAssetSize;)I

    move-result v9

    .line 1725947
    const/16 v10, 0xa

    invoke-virtual {p0, v10}, LX/186;->c(I)V

    .line 1725948
    invoke-virtual {p0, v2, v0}, LX/186;->b(II)V

    .line 1725949
    invoke-virtual {p0, v11, v1}, LX/186;->b(II)V

    .line 1725950
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v3}, LX/186;->b(II)V

    .line 1725951
    const/4 v0, 0x3

    invoke-virtual {p0, v0, v4}, LX/186;->b(II)V

    .line 1725952
    const/4 v0, 0x4

    invoke-virtual {p0, v0, v5}, LX/186;->b(II)V

    .line 1725953
    const/4 v1, 0x5

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;->o()D

    move-result-wide v2

    const-wide/16 v4, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1725954
    const/4 v0, 0x6

    invoke-virtual {p0, v0, v6}, LX/186;->b(II)V

    .line 1725955
    const/4 v0, 0x7

    invoke-virtual {p0, v0, v7}, LX/186;->b(II)V

    .line 1725956
    const/16 v0, 0x8

    invoke-virtual {p0, v0, v8}, LX/186;->b(II)V

    .line 1725957
    const/16 v0, 0x9

    invoke-virtual {p0, v0, v9}, LX/186;->b(II)V

    .line 1725958
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 1725959
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto/16 :goto_0

    :cond_2
    move v0, v2

    goto :goto_3

    .line 1725960
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFrameDynamicClientTextAsset;->j()Lcom/facebook/graphql/enums/GraphQLClientGeneratedTextType;

    move-result-object v6

    invoke-virtual {p0, v6}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v6

    .line 1725961
    const/4 v7, 0x2

    invoke-virtual {p0, v7}, LX/186;->c(I)V

    .line 1725962
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFrameDynamicClientTextAsset;->a()I

    move-result v7

    invoke-virtual {p0, v5, v7, v5}, LX/186;->a(III)V

    .line 1725963
    const/4 v5, 0x1

    invoke-virtual {p0, v5, v6}, LX/186;->b(II)V

    .line 1725964
    invoke-virtual {p0}, LX/186;->d()I

    move-result v5

    .line 1725965
    invoke-virtual {p0, v5}, LX/186;->d(I)V

    goto/16 :goto_2
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLFrameTextAssetSize;)I
    .locals 6

    .prologue
    const/4 v0, 0x0

    const-wide/16 v4, 0x0

    .line 1725967
    if-nez p1, :cond_0

    .line 1725968
    :goto_0
    return v0

    .line 1725969
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFrameTextAssetSize;->a()Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 1725970
    const/4 v2, 0x3

    invoke-virtual {p0, v2}, LX/186;->c(I)V

    .line 1725971
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 1725972
    const/4 v1, 0x1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFrameTextAssetSize;->j()D

    move-result-wide v2

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1725973
    const/4 v1, 0x2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFrameTextAssetSize;->k()D

    move-result-wide v2

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1725974
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 1725975
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLImageOverlay;)I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1726317
    if-nez p1, :cond_0

    .line 1726318
    :goto_0
    return v0

    .line 1726319
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLImageOverlay;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v1

    .line 1726320
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLImageOverlay;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1726321
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLImageOverlay;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    const/4 v4, 0x0

    .line 1726322
    if-nez v3, :cond_1

    .line 1726323
    :goto_1
    move v3, v4

    .line 1726324
    const/4 v4, 0x3

    invoke-virtual {p0, v4}, LX/186;->c(I)V

    .line 1726325
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 1726326
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v2}, LX/186;->b(II)V

    .line 1726327
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v3}, LX/186;->b(II)V

    .line 1726328
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 1726329
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0

    .line 1726330
    :cond_1
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1726331
    const/4 p1, 0x1

    invoke-virtual {p0, p1}, LX/186;->c(I)V

    .line 1726332
    invoke-virtual {p0, v4, v5}, LX/186;->b(II)V

    .line 1726333
    invoke-virtual {p0}, LX/186;->d()I

    move-result v4

    .line 1726334
    invoke-virtual {p0, v4}, LX/186;->d(I)V

    goto :goto_1
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLInlineActivity;)I
    .locals 13

    .prologue
    const/4 v0, 0x0

    .line 1725976
    if-nez p1, :cond_0

    .line 1725977
    :goto_0
    return v0

    .line 1725978
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLInlineActivity;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1725979
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLInlineActivity;->k()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    const/4 v3, 0x0

    .line 1725980
    if-nez v2, :cond_1

    .line 1725981
    :goto_1
    move v2, v3

    .line 1725982
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLInlineActivity;->l()Lcom/facebook/graphql/model/GraphQLTaggableActivity;

    move-result-object v3

    invoke-static {p0, v3}, LX/AwG;->a(LX/186;Lcom/facebook/graphql/model/GraphQLTaggableActivity;)I

    move-result v3

    .line 1725983
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLInlineActivity;->m()Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;

    move-result-object v4

    const/4 v5, 0x0

    .line 1725984
    if-nez v4, :cond_5

    .line 1725985
    :goto_2
    move v4, v5

    .line 1725986
    const/4 v5, 0x4

    invoke-virtual {p0, v5}, LX/186;->c(I)V

    .line 1725987
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 1725988
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v2}, LX/186;->b(II)V

    .line 1725989
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v3}, LX/186;->b(II)V

    .line 1725990
    const/4 v0, 0x3

    invoke-virtual {p0, v0, v4}, LX/186;->b(II)V

    .line 1725991
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 1725992
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0

    .line 1725993
    :cond_1
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v4

    invoke-virtual {p0, v4}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v4

    .line 1725994
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1725995
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->fJ()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1725996
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->fP()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v7

    const/4 v8, 0x0

    .line 1725997
    if-nez v7, :cond_2

    .line 1725998
    :goto_3
    move v7, v8

    .line 1725999
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->ga()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v8

    const/4 v9, 0x0

    .line 1726000
    if-nez v8, :cond_3

    .line 1726001
    :goto_4
    move v8, v9

    .line 1726002
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->hn()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v9

    const/4 v10, 0x0

    .line 1726003
    if-nez v9, :cond_4

    .line 1726004
    :goto_5
    move v9, v10

    .line 1726005
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->kn()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 1726006
    const/16 v11, 0x9

    invoke-virtual {p0, v11}, LX/186;->c(I)V

    .line 1726007
    invoke-virtual {p0, v3, v4}, LX/186;->b(II)V

    .line 1726008
    const/4 v3, 0x1

    invoke-virtual {p0, v3, v5}, LX/186;->b(II)V

    .line 1726009
    const/4 v3, 0x2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->eM()Z

    move-result v4

    invoke-virtual {p0, v3, v4}, LX/186;->a(IZ)V

    .line 1726010
    const/4 v3, 0x3

    invoke-virtual {p0, v3, v6}, LX/186;->b(II)V

    .line 1726011
    const/4 v3, 0x4

    invoke-virtual {p0, v3, v7}, LX/186;->b(II)V

    .line 1726012
    const/4 v3, 0x5

    invoke-virtual {p0, v3, v8}, LX/186;->b(II)V

    .line 1726013
    const/4 v3, 0x6

    invoke-virtual {p0, v3, v9}, LX/186;->b(II)V

    .line 1726014
    const/16 v3, 0x8

    invoke-virtual {p0, v3, v10}, LX/186;->b(II)V

    .line 1726015
    invoke-virtual {p0}, LX/186;->d()I

    move-result v3

    .line 1726016
    invoke-virtual {p0, v3}, LX/186;->d(I)V

    goto/16 :goto_1

    .line 1726017
    :cond_2
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object v9

    invoke-virtual {p0, v9}, LX/186;->c(Ljava/util/List;)I

    move-result v9

    .line 1726018
    const/4 v10, 0x1

    invoke-virtual {p0, v10}, LX/186;->c(I)V

    .line 1726019
    invoke-virtual {p0, v8, v9}, LX/186;->b(II)V

    .line 1726020
    invoke-virtual {p0}, LX/186;->d()I

    move-result v8

    .line 1726021
    invoke-virtual {p0, v8}, LX/186;->d(I)V

    goto :goto_3

    .line 1726022
    :cond_3
    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLPage;->Q()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 1726023
    const/4 v11, 0x2

    invoke-virtual {p0, v11}, LX/186;->c(I)V

    .line 1726024
    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLPage;->I()Z

    move-result v11

    invoke-virtual {p0, v9, v11}, LX/186;->a(IZ)V

    .line 1726025
    const/4 v9, 0x1

    invoke-virtual {p0, v9, v10}, LX/186;->b(II)V

    .line 1726026
    invoke-virtual {p0}, LX/186;->d()I

    move-result v9

    .line 1726027
    invoke-virtual {p0, v9}, LX/186;->d(I)V

    goto :goto_4

    .line 1726028
    :cond_4
    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {p0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 1726029
    const/4 v12, 0x1

    invoke-virtual {p0, v12}, LX/186;->c(I)V

    .line 1726030
    invoke-virtual {p0, v10, v11}, LX/186;->b(II)V

    .line 1726031
    invoke-virtual {p0}, LX/186;->d()I

    move-result v10

    .line 1726032
    invoke-virtual {p0, v10}, LX/186;->d(I)V

    goto/16 :goto_5

    .line 1726033
    :cond_5
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLTaggableActivityIcon;->m()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v6

    const/4 v7, 0x0

    .line 1726034
    if-nez v6, :cond_6

    .line 1726035
    :goto_6
    move v6, v7

    .line 1726036
    const/4 v7, 0x1

    invoke-virtual {p0, v7}, LX/186;->c(I)V

    .line 1726037
    invoke-virtual {p0, v5, v6}, LX/186;->b(II)V

    .line 1726038
    invoke-virtual {p0}, LX/186;->d()I

    move-result v5

    .line 1726039
    invoke-virtual {p0, v5}, LX/186;->d(I)V

    goto/16 :goto_2

    .line 1726040
    :cond_6
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 1726041
    const/4 v4, 0x3

    invoke-virtual {p0, v4}, LX/186;->c(I)V

    .line 1726042
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v4

    invoke-virtual {p0, v7, v4, v7}, LX/186;->a(III)V

    .line 1726043
    const/4 v4, 0x1

    invoke-virtual {p0, v4, v8}, LX/186;->b(II)V

    .line 1726044
    const/4 v8, 0x2

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v4

    invoke-virtual {p0, v8, v4, v7}, LX/186;->a(III)V

    .line 1726045
    invoke-virtual {p0}, LX/186;->d()I

    move-result v7

    .line 1726046
    invoke-virtual {p0, v7}, LX/186;->d(I)V

    goto :goto_6
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLMaskEffect;)I
    .locals 14

    .prologue
    const/4 v0, 0x0

    .line 1726047
    if-nez p1, :cond_0

    .line 1726048
    :goto_0
    return v0

    .line 1726049
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLMaskEffect;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    const/4 v2, 0x0

    .line 1726050
    if-nez v1, :cond_1

    .line 1726051
    :goto_1
    move v1, v2

    .line 1726052
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLMaskEffect;->k()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    const/4 v3, 0x0

    .line 1726053
    if-nez v2, :cond_2

    .line 1726054
    :goto_2
    move v2, v3

    .line 1726055
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLMaskEffect;->l()Lcom/facebook/graphql/model/GraphQLNativeMask;

    move-result-object v3

    const/4 v10, 0x1

    const/4 v6, 0x0

    .line 1726056
    if-nez v3, :cond_3

    .line 1726057
    :goto_3
    move v3, v6

    .line 1726058
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLMaskEffect;->n()Lcom/facebook/graphql/model/GraphQLMediaEffectInstruction;

    move-result-object v4

    const/4 v5, 0x0

    .line 1726059
    if-nez v4, :cond_8

    .line 1726060
    :goto_4
    move v4, v5

    .line 1726061
    const/4 v5, 0x6

    invoke-virtual {p0, v5}, LX/186;->c(I)V

    .line 1726062
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 1726063
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v2}, LX/186;->b(II)V

    .line 1726064
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v3}, LX/186;->b(II)V

    .line 1726065
    const/4 v0, 0x3

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLMaskEffect;->m()Z

    move-result v1

    invoke-virtual {p0, v0, v1}, LX/186;->a(IZ)V

    .line 1726066
    const/4 v0, 0x4

    invoke-virtual {p0, v0, v4}, LX/186;->b(II)V

    .line 1726067
    const/4 v0, 0x5

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLMaskEffect;->o()Z

    move-result v1

    invoke-virtual {p0, v0, v1}, LX/186;->a(IZ)V

    .line 1726068
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 1726069
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0

    .line 1726070
    :cond_1
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1726071
    const/4 v4, 0x1

    invoke-virtual {p0, v4}, LX/186;->c(I)V

    .line 1726072
    invoke-virtual {p0, v2, v3}, LX/186;->b(II)V

    .line 1726073
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 1726074
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto :goto_1

    .line 1726075
    :cond_2
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1726076
    const/4 v5, 0x1

    invoke-virtual {p0, v5}, LX/186;->c(I)V

    .line 1726077
    invoke-virtual {p0, v3, v4}, LX/186;->b(II)V

    .line 1726078
    invoke-virtual {p0}, LX/186;->d()I

    move-result v3

    .line 1726079
    invoke-virtual {p0, v3}, LX/186;->d(I)V

    goto :goto_2

    .line 1726080
    :cond_3
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLNativeMask;->j()LX/0Px;

    move-result-object v7

    .line 1726081
    if-eqz v7, :cond_5

    .line 1726082
    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v4

    new-array v8, v4, [I

    move v5, v6

    .line 1726083
    :goto_5
    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v4

    if-ge v5, v4, :cond_4

    .line 1726084
    invoke-virtual {v7, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLMask3DAsset;

    const/4 v9, 0x0

    .line 1726085
    if-nez v4, :cond_6

    .line 1726086
    :goto_6
    move v4, v9

    .line 1726087
    aput v4, v8, v5

    .line 1726088
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_5

    .line 1726089
    :cond_4
    invoke-virtual {p0, v8, v10}, LX/186;->a([IZ)I

    move-result v4

    .line 1726090
    :goto_7
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLNativeMask;->k()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1726091
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLNativeMask;->l()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1726092
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLNativeMask;->m()Lcom/facebook/graphql/model/GraphQLMask3DAsset;

    move-result-object v8

    const/4 v9, 0x0

    .line 1726093
    if-nez v8, :cond_7

    .line 1726094
    :goto_8
    move v8, v9

    .line 1726095
    const/4 v9, 0x4

    invoke-virtual {p0, v9}, LX/186;->c(I)V

    .line 1726096
    invoke-virtual {p0, v6, v4}, LX/186;->b(II)V

    .line 1726097
    invoke-virtual {p0, v10, v5}, LX/186;->b(II)V

    .line 1726098
    const/4 v4, 0x2

    invoke-virtual {p0, v4, v7}, LX/186;->b(II)V

    .line 1726099
    const/4 v4, 0x3

    invoke-virtual {p0, v4, v8}, LX/186;->b(II)V

    .line 1726100
    invoke-virtual {p0}, LX/186;->d()I

    move-result v6

    .line 1726101
    invoke-virtual {p0, v6}, LX/186;->d(I)V

    goto/16 :goto_3

    :cond_5
    move v4, v6

    goto :goto_7

    .line 1726102
    :cond_6
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLMask3DAsset;->j()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {p0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 1726103
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLMask3DAsset;->k()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {p0, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 1726104
    const/4 v13, 0x2

    invoke-virtual {p0, v13}, LX/186;->c(I)V

    .line 1726105
    invoke-virtual {p0, v9, v11}, LX/186;->b(II)V

    .line 1726106
    const/4 v9, 0x1

    invoke-virtual {p0, v9, v12}, LX/186;->b(II)V

    .line 1726107
    invoke-virtual {p0}, LX/186;->d()I

    move-result v9

    .line 1726108
    invoke-virtual {p0, v9}, LX/186;->d(I)V

    goto :goto_6

    .line 1726109
    :cond_7
    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLMask3DAsset;->j()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {p0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 1726110
    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLMask3DAsset;->k()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {p0, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 1726111
    const/4 v13, 0x2

    invoke-virtual {p0, v13}, LX/186;->c(I)V

    .line 1726112
    invoke-virtual {p0, v9, v11}, LX/186;->b(II)V

    .line 1726113
    const/4 v9, 0x1

    invoke-virtual {p0, v9, v12}, LX/186;->b(II)V

    .line 1726114
    invoke-virtual {p0}, LX/186;->d()I

    move-result v9

    .line 1726115
    invoke-virtual {p0, v9}, LX/186;->d(I)V

    goto :goto_8

    .line 1726116
    :cond_8
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLMediaEffectInstruction;->j()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1726117
    const/4 v7, 0x1

    invoke-virtual {p0, v7}, LX/186;->c(I)V

    .line 1726118
    invoke-virtual {p0, v5, v6}, LX/186;->b(II)V

    .line 1726119
    invoke-virtual {p0}, LX/186;->d()I

    move-result v5

    .line 1726120
    invoke-virtual {p0, v5}, LX/186;->d(I)V

    goto/16 :goto_4
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLMediaEffectCustomFontResourceConnection;)I
    .locals 11

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 1726121
    if-nez p1, :cond_0

    .line 1726122
    :goto_0
    return v2

    .line 1726123
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLMediaEffectCustomFontResourceConnection;->a()LX/0Px;

    move-result-object v3

    .line 1726124
    if-eqz v3, :cond_2

    .line 1726125
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    new-array v4, v0, [I

    move v1, v2

    .line 1726126
    :goto_1
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1726127
    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLMediaEffectCustomFontResourceEdge;

    const/4 v6, 0x0

    .line 1726128
    if-nez v0, :cond_3

    .line 1726129
    :goto_2
    move v0, v6

    .line 1726130
    aput v0, v4, v1

    .line 1726131
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1726132
    :cond_1
    invoke-virtual {p0, v4, v5}, LX/186;->a([IZ)I

    move-result v0

    .line 1726133
    :goto_3
    invoke-virtual {p0, v5}, LX/186;->c(I)V

    .line 1726134
    invoke-virtual {p0, v2, v0}, LX/186;->b(II)V

    .line 1726135
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 1726136
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_3

    .line 1726137
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMediaEffectCustomFontResourceEdge;->a()Lcom/facebook/graphql/model/GraphQLMediaEffectCustomFontResource;

    move-result-object v7

    const/4 v8, 0x0

    .line 1726138
    if-nez v7, :cond_4

    .line 1726139
    :goto_4
    move v7, v8

    .line 1726140
    const/4 v8, 0x1

    invoke-virtual {p0, v8}, LX/186;->c(I)V

    .line 1726141
    invoke-virtual {p0, v6, v7}, LX/186;->b(II)V

    .line 1726142
    invoke-virtual {p0}, LX/186;->d()I

    move-result v6

    .line 1726143
    invoke-virtual {p0, v6}, LX/186;->d(I)V

    goto :goto_2

    .line 1726144
    :cond_4
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLMediaEffectCustomFontResource;->j()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 1726145
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLMediaEffectCustomFontResource;->k()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 1726146
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLMediaEffectCustomFontResource;->l()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, LX/186;->b(Ljava/lang/String;)I

    move-result p1

    .line 1726147
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, LX/186;->c(I)V

    .line 1726148
    invoke-virtual {p0, v8, v9}, LX/186;->b(II)V

    .line 1726149
    const/4 v8, 0x1

    invoke-virtual {p0, v8, v10}, LX/186;->b(II)V

    .line 1726150
    const/4 v8, 0x2

    invoke-virtual {p0, v8, p1}, LX/186;->b(II)V

    .line 1726151
    invoke-virtual {p0}, LX/186;->d()I

    move-result v8

    .line 1726152
    invoke-virtual {p0, v8}, LX/186;->d(I)V

    goto :goto_4
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLMemeCategory;)I
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 1726153
    if-nez p1, :cond_0

    .line 1726154
    :goto_0
    return v0

    .line 1726155
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLMemeCategory;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1726156
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLMemeCategory;->k()Lcom/facebook/graphql/model/GraphQLMemeStoriesConnection;

    move-result-object v2

    const/4 v8, 0x1

    const/4 v5, 0x0

    .line 1726157
    if-nez v2, :cond_1

    .line 1726158
    :goto_1
    move v2, v5

    .line 1726159
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLMemeCategory;->l()LX/0Px;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/186;->b(Ljava/util/List;)I

    move-result v3

    .line 1726160
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLMemeCategory;->m()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1726161
    const/4 v5, 0x4

    invoke-virtual {p0, v5}, LX/186;->c(I)V

    .line 1726162
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 1726163
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v2}, LX/186;->b(II)V

    .line 1726164
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v3}, LX/186;->b(II)V

    .line 1726165
    const/4 v0, 0x3

    invoke-virtual {p0, v0, v4}, LX/186;->b(II)V

    .line 1726166
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 1726167
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0

    .line 1726168
    :cond_1
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMemeStoriesConnection;->j()LX/0Px;

    move-result-object v6

    .line 1726169
    if-eqz v6, :cond_3

    .line 1726170
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v3

    new-array v7, v3, [I

    move v4, v5

    .line 1726171
    :goto_2
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v3

    if-ge v4, v3, :cond_2

    .line 1726172
    invoke-virtual {v6, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {p0, v3}, LX/AwG;->a(LX/186;Lcom/facebook/graphql/model/GraphQLStory;)I

    move-result v3

    aput v3, v7, v4

    .line 1726173
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_2

    .line 1726174
    :cond_2
    invoke-virtual {p0, v7, v8}, LX/186;->a([IZ)I

    move-result v3

    .line 1726175
    :goto_3
    const/4 v4, 0x2

    invoke-virtual {p0, v4}, LX/186;->c(I)V

    .line 1726176
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMemeStoriesConnection;->a()I

    move-result v4

    invoke-virtual {p0, v5, v4, v5}, LX/186;->a(III)V

    .line 1726177
    invoke-virtual {p0, v8, v3}, LX/186;->b(II)V

    .line 1726178
    invoke-virtual {p0}, LX/186;->d()I

    move-result v5

    .line 1726179
    invoke-virtual {p0, v5}, LX/186;->d(I)V

    goto :goto_1

    :cond_3
    move v3, v5

    goto :goto_3
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLParticleEffect;)I
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 1726180
    if-nez p1, :cond_0

    .line 1726181
    :goto_0
    return v0

    .line 1726182
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLParticleEffect;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    const/4 v2, 0x0

    .line 1726183
    if-nez v1, :cond_1

    .line 1726184
    :goto_1
    move v1, v2

    .line 1726185
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLParticleEffect;->k()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    const/4 v3, 0x0

    .line 1726186
    if-nez v2, :cond_2

    .line 1726187
    :goto_2
    move v2, v3

    .line 1726188
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLParticleEffect;->l()Lcom/facebook/graphql/model/GraphQLParticleEffectToEmittersConnection;

    move-result-object v3

    const/4 v9, 0x1

    const/4 v6, 0x0

    .line 1726189
    if-nez v3, :cond_3

    .line 1726190
    :goto_3
    move v3, v6

    .line 1726191
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLParticleEffect;->n()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1726192
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLParticleEffect;->o()Lcom/facebook/graphql/model/GraphQLMediaEffectInstruction;

    move-result-object v5

    const/4 v6, 0x0

    .line 1726193
    if-nez v5, :cond_6

    .line 1726194
    :goto_4
    move v5, v6

    .line 1726195
    const/4 v6, 0x7

    invoke-virtual {p0, v6}, LX/186;->c(I)V

    .line 1726196
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 1726197
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v2}, LX/186;->b(II)V

    .line 1726198
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v3}, LX/186;->b(II)V

    .line 1726199
    const/4 v0, 0x3

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLParticleEffect;->m()Z

    move-result v1

    invoke-virtual {p0, v0, v1}, LX/186;->a(IZ)V

    .line 1726200
    const/4 v0, 0x4

    invoke-virtual {p0, v0, v4}, LX/186;->b(II)V

    .line 1726201
    const/4 v0, 0x5

    invoke-virtual {p0, v0, v5}, LX/186;->b(II)V

    .line 1726202
    const/4 v0, 0x6

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLParticleEffect;->p()Z

    move-result v1

    invoke-virtual {p0, v0, v1}, LX/186;->a(IZ)V

    .line 1726203
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 1726204
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0

    .line 1726205
    :cond_1
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1726206
    const/4 v4, 0x1

    invoke-virtual {p0, v4}, LX/186;->c(I)V

    .line 1726207
    invoke-virtual {p0, v2, v3}, LX/186;->b(II)V

    .line 1726208
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 1726209
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto :goto_1

    .line 1726210
    :cond_2
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1726211
    const/4 v5, 0x1

    invoke-virtual {p0, v5}, LX/186;->c(I)V

    .line 1726212
    invoke-virtual {p0, v3, v4}, LX/186;->b(II)V

    .line 1726213
    invoke-virtual {p0}, LX/186;->d()I

    move-result v3

    .line 1726214
    invoke-virtual {p0, v3}, LX/186;->d(I)V

    goto :goto_2

    .line 1726215
    :cond_3
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLParticleEffectToEmittersConnection;->a()LX/0Px;

    move-result-object v7

    .line 1726216
    if-eqz v7, :cond_5

    .line 1726217
    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v4

    new-array v8, v4, [I

    move v5, v6

    .line 1726218
    :goto_5
    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v4

    if-ge v5, v4, :cond_4

    .line 1726219
    invoke-virtual {v7, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;

    invoke-static {p0, v4}, LX/AwG;->a(LX/186;Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;)I

    move-result v4

    aput v4, v8, v5

    .line 1726220
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_5

    .line 1726221
    :cond_4
    invoke-virtual {p0, v8, v9}, LX/186;->a([IZ)I

    move-result v4

    .line 1726222
    :goto_6
    invoke-virtual {p0, v9}, LX/186;->c(I)V

    .line 1726223
    invoke-virtual {p0, v6, v4}, LX/186;->b(II)V

    .line 1726224
    invoke-virtual {p0}, LX/186;->d()I

    move-result v6

    .line 1726225
    invoke-virtual {p0, v6}, LX/186;->d(I)V

    goto/16 :goto_3

    :cond_5
    move v4, v6

    goto :goto_6

    .line 1726226
    :cond_6
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLMediaEffectInstruction;->j()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1726227
    const/4 v8, 0x1

    invoke-virtual {p0, v8}, LX/186;->c(I)V

    .line 1726228
    invoke-virtual {p0, v6, v7}, LX/186;->b(II)V

    .line 1726229
    invoke-virtual {p0}, LX/186;->d()I

    move-result v6

    .line 1726230
    invoke-virtual {p0, v6}, LX/186;->d(I)V

    goto/16 :goto_4
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLParticleEffectColorHSVA;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    .line 1726231
    if-nez p1, :cond_0

    .line 1726232
    :goto_0
    return v1

    .line 1726233
    :cond_0
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, LX/186;->c(I)V

    .line 1726234
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLParticleEffectColorHSVA;->a()D

    move-result-wide v2

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1726235
    const/4 v1, 0x1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLParticleEffectColorHSVA;->j()D

    move-result-wide v2

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1726236
    const/4 v1, 0x2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLParticleEffectColorHSVA;->k()D

    move-result-wide v2

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1726237
    const/4 v1, 0x3

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLParticleEffectColorHSVA;->l()D

    move-result-wide v2

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1726238
    invoke-virtual {p0}, LX/186;->d()I

    move-result v1

    .line 1726239
    invoke-virtual {p0, v1}, LX/186;->d(I)V

    goto :goto_0
.end method

.method public static a(LX/186;Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;)I
    .locals 21

    .prologue
    .line 1726240
    if-nez p1, :cond_0

    .line 1726241
    const/4 v2, 0x0

    .line 1726242
    :goto_0
    return v2

    .line 1726243
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->j()Lcom/facebook/graphql/model/GraphQLParticleEffectEmitterToAnimationAssetsConnection;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/AwG;->a(LX/186;Lcom/facebook/graphql/model/GraphQLParticleEffectEmitterToAnimationAssetsConnection;)I

    move-result v2

    .line 1726244
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->l()Lcom/facebook/graphql/model/GraphQLParticleEffectEmitterToEmitterAssetsConnection;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/AwG;->a(LX/186;Lcom/facebook/graphql/model/GraphQLParticleEffectEmitterToEmitterAssetsConnection;)I

    move-result v8

    .line 1726245
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->m()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 1726246
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->n()Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/AwG;->a(LX/186;Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;)I

    move-result v10

    .line 1726247
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 1726248
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->p()Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/AwG;->a(LX/186;Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;)I

    move-result v12

    .line 1726249
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->r()Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/AwG;->a(LX/186;Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;)I

    move-result v13

    .line 1726250
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->s()Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/AwG;->a(LX/186;Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;)I

    move-result v14

    .line 1726251
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->u()Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/AwG;->a(LX/186;Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;)I

    move-result v15

    .line 1726252
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->w()Lcom/facebook/graphql/model/GraphQLParticleEffectColorHSVA;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/AwG;->a(LX/186;Lcom/facebook/graphql/model/GraphQLParticleEffectColorHSVA;)I

    move-result v16

    .line 1726253
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->B()Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/AwG;->a(LX/186;Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;)I

    move-result v17

    .line 1726254
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->D()Lcom/facebook/graphql/model/GraphQLParticleEffectColorHSVA;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/AwG;->a(LX/186;Lcom/facebook/graphql/model/GraphQLParticleEffectColorHSVA;)I

    move-result v18

    .line 1726255
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->H()Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/AwG;->a(LX/186;Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;)I

    move-result v19

    .line 1726256
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->L()Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL2D;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/AwG;->a(LX/186;Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL2D;)I

    move-result v20

    .line 1726257
    const/16 v3, 0x1d

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 1726258
    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v2}, LX/186;->b(II)V

    .line 1726259
    const/4 v3, 0x1

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->k()D

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1726260
    const/4 v2, 0x2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 1726261
    const/4 v2, 0x3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 1726262
    const/4 v2, 0x4

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 1726263
    const/4 v2, 0x5

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 1726264
    const/4 v2, 0x6

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 1726265
    const/4 v3, 0x7

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->q()D

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1726266
    const/16 v2, 0x8

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 1726267
    const/16 v2, 0x9

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 1726268
    const/16 v3, 0xa

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->t()D

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1726269
    const/16 v2, 0xb

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 1726270
    const/16 v2, 0xc

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->v()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 1726271
    const/16 v2, 0xd

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1726272
    const/16 v2, 0xe

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->x()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 1726273
    const/16 v3, 0xf

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->y()D

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1726274
    const/16 v2, 0x10

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->z()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 1726275
    const/16 v3, 0x11

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->A()D

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1726276
    const/16 v2, 0x12

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1726277
    const/16 v3, 0x13

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->C()D

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1726278
    const/16 v2, 0x14

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1726279
    const/16 v2, 0x15

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->E()I

    move-result v3

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3, v4}, LX/186;->a(III)V

    .line 1726280
    const/16 v3, 0x16

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->F()D

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1726281
    const/16 v3, 0x17

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->G()D

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1726282
    const/16 v2, 0x18

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1726283
    const/16 v3, 0x19

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->I()D

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1726284
    const/16 v3, 0x1a

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->J()D

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1726285
    const/16 v3, 0x1b

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitter;->K()D

    move-result-wide v4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 1726286
    const/16 v2, 0x1c

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1726287
    invoke-virtual/range {p0 .. p0}, LX/186;->d()I

    move-result v2

    .line 1726288
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/186;->d(I)V

    goto/16 :goto_0
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLParticleEffectEmitterToAnimationAssetsConnection;)I
    .locals 9

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 1726289
    if-nez p1, :cond_0

    .line 1726290
    :goto_0
    return v2

    .line 1726291
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitterToAnimationAssetsConnection;->a()LX/0Px;

    move-result-object v3

    .line 1726292
    if-eqz v3, :cond_2

    .line 1726293
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    new-array v4, v0, [I

    move v1, v2

    .line 1726294
    :goto_1
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1726295
    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLParticleEffectAnimation;

    const/4 v6, 0x0

    .line 1726296
    if-nez v0, :cond_3

    .line 1726297
    :goto_2
    move v0, v6

    .line 1726298
    aput v0, v4, v1

    .line 1726299
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1726300
    :cond_1
    invoke-virtual {p0, v4, v5}, LX/186;->a([IZ)I

    move-result v0

    .line 1726301
    :goto_3
    invoke-virtual {p0, v5}, LX/186;->c(I)V

    .line 1726302
    invoke-virtual {p0, v2, v0}, LX/186;->b(II)V

    .line 1726303
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 1726304
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_3

    .line 1726305
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLParticleEffectAnimation;->j()Lcom/facebook/graphql/enums/GraphQLParticleEffectAnimationTypeEnum;

    move-result-object v7

    invoke-virtual {p0, v7}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v7

    .line 1726306
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLParticleEffectAnimation;->k()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v8

    invoke-static {p0, v8}, LX/AwG;->k(LX/186;Lcom/facebook/graphql/model/GraphQLImage;)I

    move-result v8

    .line 1726307
    const/4 p1, 0x7

    invoke-virtual {p0, p1}, LX/186;->c(I)V

    .line 1726308
    invoke-virtual {p0, v6, v7}, LX/186;->b(II)V

    .line 1726309
    const/4 v7, 0x1

    invoke-virtual {p0, v7, v8}, LX/186;->b(II)V

    .line 1726310
    const/4 v7, 0x2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLParticleEffectAnimation;->l()I

    move-result v8

    invoke-virtual {p0, v7, v8, v6}, LX/186;->a(III)V

    .line 1726311
    const/4 v7, 0x3

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLParticleEffectAnimation;->m()I

    move-result v8

    invoke-virtual {p0, v7, v8, v6}, LX/186;->a(III)V

    .line 1726312
    const/4 v7, 0x4

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLParticleEffectAnimation;->n()I

    move-result v8

    invoke-virtual {p0, v7, v8, v6}, LX/186;->a(III)V

    .line 1726313
    const/4 v7, 0x5

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLParticleEffectAnimation;->o()I

    move-result v8

    invoke-virtual {p0, v7, v8, v6}, LX/186;->a(III)V

    .line 1726314
    const/4 v7, 0x6

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLParticleEffectAnimation;->p()I

    move-result v8

    invoke-virtual {p0, v7, v8, v6}, LX/186;->a(III)V

    .line 1726315
    invoke-virtual {p0}, LX/186;->d()I

    move-result v6

    .line 1726316
    invoke-virtual {p0, v6}, LX/186;->d(I)V

    goto :goto_2
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLParticleEffectEmitterToEmitterAssetsConnection;)I
    .locals 8

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 1725774
    if-nez p1, :cond_0

    .line 1725775
    :goto_0
    return v2

    .line 1725776
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLParticleEffectEmitterToEmitterAssetsConnection;->a()LX/0Px;

    move-result-object v3

    .line 1725777
    if-eqz v3, :cond_2

    .line 1725778
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    new-array v4, v0, [I

    move v1, v2

    .line 1725779
    :goto_1
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1725780
    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLParticleEffectAsset;

    const/4 v6, 0x0

    .line 1725781
    if-nez v0, :cond_3

    .line 1725782
    :goto_2
    move v0, v6

    .line 1725783
    aput v0, v4, v1

    .line 1725784
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1725785
    :cond_1
    invoke-virtual {p0, v4, v5}, LX/186;->a([IZ)I

    move-result v0

    .line 1725786
    :goto_3
    invoke-virtual {p0, v5}, LX/186;->c(I)V

    .line 1725787
    invoke-virtual {p0, v2, v0}, LX/186;->b(II)V

    .line 1725788
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 1725789
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_3

    .line 1725790
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLParticleEffectAsset;->j()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    invoke-static {p0, v7}, LX/AwG;->k(LX/186;Lcom/facebook/graphql/model/GraphQLImage;)I

    move-result v7

    .line 1725791
    const/4 p1, 0x1

    invoke-virtual {p0, p1}, LX/186;->c(I)V

    .line 1725792
    invoke-virtual {p0, v6, v7}, LX/186;->b(II)V

    .line 1725793
    invoke-virtual {p0}, LX/186;->d()I

    move-result v6

    .line 1725794
    invoke-virtual {p0, v6}, LX/186;->d(I)V

    goto :goto_2
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL2D;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    .line 1725795
    if-nez p1, :cond_0

    .line 1725796
    :goto_0
    return v1

    .line 1725797
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, LX/186;->c(I)V

    .line 1725798
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL2D;->a()D

    move-result-wide v2

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1725799
    const/4 v1, 0x1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL2D;->j()D

    move-result-wide v2

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1725800
    invoke-virtual {p0}, LX/186;->d()I

    move-result v1

    .line 1725801
    invoke-virtual {p0, v1}, LX/186;->d(I)V

    goto :goto_0
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    .line 1725252
    if-nez p1, :cond_0

    .line 1725253
    :goto_0
    return v1

    .line 1725254
    :cond_0
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, LX/186;->c(I)V

    .line 1725255
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;->a()D

    move-result-wide v2

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1725256
    const/4 v1, 0x1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;->j()D

    move-result-wide v2

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1725257
    const/4 v1, 0x2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLParticleEffectOpenGL3D;->k()D

    move-result-wide v2

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1725258
    invoke-virtual {p0}, LX/186;->d()I

    move-result v1

    .line 1725259
    invoke-virtual {p0, v1}, LX/186;->d(I)V

    goto :goto_0
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLPlace;)I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 1725284
    if-nez p1, :cond_0

    .line 1725285
    :goto_0
    return v0

    .line 1725286
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPlace;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v1

    .line 1725287
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPlace;->w()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1725288
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPlace;->A()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1725289
    const/4 v4, 0x3

    invoke-virtual {p0, v4}, LX/186;->c(I)V

    .line 1725290
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 1725291
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v2}, LX/186;->b(II)V

    .line 1725292
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v3}, LX/186;->b(II)V

    .line 1725293
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 1725294
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0
.end method

.method public static a(LX/186;Lcom/facebook/graphql/model/GraphQLProductionPrompt;)I
    .locals 13

    .prologue
    const/4 v0, 0x0

    .line 1725295
    if-nez p1, :cond_0

    .line 1725296
    :goto_0
    return v0

    .line 1725297
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1725298
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->l()Lcom/facebook/graphql/enums/GraphQLPromptConfidence;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    .line 1725299
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->m()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    const/4 v9, 0x1

    const/4 v6, 0x0

    .line 1725300
    if-nez v3, :cond_1

    .line 1725301
    :goto_1
    move v3, v6

    .line 1725302
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->n()Lcom/facebook/graphql/enums/GraphQLPromptFeedType;

    move-result-object v4

    invoke-virtual {p0, v4}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v4

    .line 1725303
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->o()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    const/4 v6, 0x0

    .line 1725304
    if-nez v5, :cond_4

    .line 1725305
    :goto_2
    move v5, v6

    .line 1725306
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->u()Lcom/facebook/graphql/model/GraphQLProductionPromptSurvey;

    move-result-object v6

    const/4 v7, 0x0

    .line 1725307
    if-nez v6, :cond_5

    .line 1725308
    :goto_3
    move v6, v7

    .line 1725309
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->p()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v7

    const/4 v8, 0x0

    .line 1725310
    if-nez v7, :cond_6

    .line 1725311
    :goto_4
    move v7, v8

    .line 1725312
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->q()Lcom/facebook/graphql/enums/GraphQLPromptType;

    move-result-object v8

    invoke-virtual {p0, v8}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v8

    .line 1725313
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->v()Lcom/facebook/graphql/model/GraphQLSuggestedCompositionsConnection;

    move-result-object v9

    invoke-static {p0, v9}, LX/AwG;->a(LX/186;Lcom/facebook/graphql/model/GraphQLSuggestedCompositionsConnection;)I

    move-result v9

    .line 1725314
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->s()Lcom/facebook/graphql/model/GraphQLEventTimeRange;

    move-result-object v10

    invoke-static {p0, v10}, LX/AwG;->a(LX/186;Lcom/facebook/graphql/model/GraphQLEventTimeRange;)I

    move-result v10

    .line 1725315
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->t()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {p0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 1725316
    const/16 v12, 0xd

    invoke-virtual {p0, v12}, LX/186;->c(I)V

    .line 1725317
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 1725318
    const/4 v1, 0x1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->k()I

    move-result v12

    invoke-virtual {p0, v1, v12, v0}, LX/186;->a(III)V

    .line 1725319
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v2}, LX/186;->b(II)V

    .line 1725320
    const/4 v0, 0x3

    invoke-virtual {p0, v0, v3}, LX/186;->b(II)V

    .line 1725321
    const/4 v0, 0x4

    invoke-virtual {p0, v0, v4}, LX/186;->b(II)V

    .line 1725322
    const/4 v0, 0x5

    invoke-virtual {p0, v0, v5}, LX/186;->b(II)V

    .line 1725323
    const/4 v0, 0x6

    invoke-virtual {p0, v0, v6}, LX/186;->b(II)V

    .line 1725324
    const/4 v0, 0x7

    invoke-virtual {p0, v0, v7}, LX/186;->b(II)V

    .line 1725325
    const/16 v0, 0x8

    invoke-virtual {p0, v0, v8}, LX/186;->b(II)V

    .line 1725326
    const/16 v1, 0x9

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLProductionPrompt;->r()D

    move-result-wide v2

    const-wide/16 v4, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1725327
    const/16 v0, 0xa

    invoke-virtual {p0, v0, v9}, LX/186;->b(II)V

    .line 1725328
    const/16 v0, 0xb

    invoke-virtual {p0, v0, v10}, LX/186;->b(II)V

    .line 1725329
    const/16 v0, 0xc

    invoke-virtual {p0, v0, v11}, LX/186;->b(II)V

    .line 1725330
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 1725331
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto/16 :goto_0

    .line 1725332
    :cond_1
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->b()LX/0Px;

    move-result-object v7

    .line 1725333
    if-eqz v7, :cond_3

    .line 1725334
    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v4

    new-array v8, v4, [I

    move v5, v6

    .line 1725335
    :goto_5
    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v4

    if-ge v5, v4, :cond_2

    .line 1725336
    invoke-virtual {v7, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLEntityAtRange;

    invoke-static {p0, v4}, LX/AwG;->a(LX/186;Lcom/facebook/graphql/model/GraphQLEntityAtRange;)I

    move-result v4

    aput v4, v8, v5

    .line 1725337
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_5

    .line 1725338
    :cond_2
    invoke-virtual {p0, v8, v9}, LX/186;->a([IZ)I

    move-result v4

    .line 1725339
    :goto_6
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1725340
    const/4 v7, 0x2

    invoke-virtual {p0, v7}, LX/186;->c(I)V

    .line 1725341
    invoke-virtual {p0, v6, v4}, LX/186;->b(II)V

    .line 1725342
    invoke-virtual {p0, v9, v5}, LX/186;->b(II)V

    .line 1725343
    invoke-virtual {p0}, LX/186;->d()I

    move-result v6

    .line 1725344
    invoke-virtual {p0, v6}, LX/186;->d(I)V

    goto/16 :goto_1

    :cond_3
    move v4, v6

    goto :goto_6

    .line 1725345
    :cond_4
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1725346
    const/4 v8, 0x1

    invoke-virtual {p0, v8}, LX/186;->c(I)V

    .line 1725347
    invoke-virtual {p0, v6, v7}, LX/186;->b(II)V

    .line 1725348
    invoke-virtual {p0}, LX/186;->d()I

    move-result v6

    .line 1725349
    invoke-virtual {p0, v6}, LX/186;->d(I)V

    goto/16 :goto_2

    .line 1725350
    :cond_5
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLProductionPromptSurvey;->a()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 1725351
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLProductionPromptSurvey;->j()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 1725352
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLProductionPromptSurvey;->k()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 1725353
    const/4 v11, 0x3

    invoke-virtual {p0, v11}, LX/186;->c(I)V

    .line 1725354
    invoke-virtual {p0, v7, v8}, LX/186;->b(II)V

    .line 1725355
    const/4 v7, 0x1

    invoke-virtual {p0, v7, v9}, LX/186;->b(II)V

    .line 1725356
    const/4 v7, 0x2

    invoke-virtual {p0, v7, v10}, LX/186;->b(II)V

    .line 1725357
    invoke-virtual {p0}, LX/186;->d()I

    move-result v7

    .line 1725358
    invoke-virtual {p0, v7}, LX/186;->d(I)V

    goto/16 :goto_3

    .line 1725359
    :cond_6
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 1725360
    const/4 v10, 0x1

    invoke-virtual {p0, v10}, LX/186;->c(I)V

    .line 1725361
    invoke-virtual {p0, v8, v9}, LX/186;->b(II)V

    .line 1725362
    invoke-virtual {p0}, LX/186;->d()I

    move-result v8

    .line 1725363
    invoke-virtual {p0, v8}, LX/186;->d(I)V

    goto/16 :goto_4
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLShaderFilter;)I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 1725364
    if-nez p1, :cond_0

    .line 1725365
    :goto_0
    return v0

    .line 1725366
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLShaderFilter;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1725367
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLShaderFilter;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1725368
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLShaderFilter;->l()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1725369
    const/4 v4, 0x3

    invoke-virtual {p0, v4}, LX/186;->c(I)V

    .line 1725370
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 1725371
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v2}, LX/186;->b(II)V

    .line 1725372
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v3}, LX/186;->b(II)V

    .line 1725373
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 1725374
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0
.end method

.method public static a(LX/186;Lcom/facebook/graphql/model/GraphQLStory;)I
    .locals 13

    .prologue
    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 1725375
    if-nez p1, :cond_0

    .line 1725376
    :goto_0
    return v2

    .line 1725377
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v3

    .line 1725378
    if-eqz v3, :cond_4

    .line 1725379
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    new-array v4, v0, [I

    move v1, v2

    .line 1725380
    :goto_1
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1725381
    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    const/4 v5, 0x0

    .line 1725382
    if-nez v0, :cond_5

    .line 1725383
    :goto_2
    move v0, v5

    .line 1725384
    aput v0, v4, v1

    .line 1725385
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1725386
    :cond_1
    invoke-virtual {p0, v4, v6}, LX/186;->a([IZ)I

    move-result v0

    move v1, v0

    .line 1725387
    :goto_3
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v4

    .line 1725388
    if-eqz v4, :cond_3

    .line 1725389
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v0

    new-array v5, v0, [I

    move v3, v2

    .line 1725390
    :goto_4
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v0

    if-ge v3, v0, :cond_2

    .line 1725391
    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    const/4 v7, 0x0

    .line 1725392
    if-nez v0, :cond_6

    .line 1725393
    :goto_5
    move v0, v7

    .line 1725394
    aput v0, v5, v3

    .line 1725395
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_4

    .line 1725396
    :cond_2
    invoke-virtual {p0, v5, v6}, LX/186;->a([IZ)I

    move-result v0

    .line 1725397
    :goto_6
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1725398
    const/4 v4, 0x3

    invoke-virtual {p0, v4}, LX/186;->c(I)V

    .line 1725399
    invoke-virtual {p0, v2, v1}, LX/186;->b(II)V

    .line 1725400
    invoke-virtual {p0, v6, v0}, LX/186;->b(II)V

    .line 1725401
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v3}, LX/186;->b(II)V

    .line 1725402
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 1725403
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_6

    :cond_4
    move v1, v2

    goto :goto_3

    .line 1725404
    :cond_5
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v7

    invoke-virtual {p0, v7}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v7

    .line 1725405
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 1725406
    const/4 v9, 0x2

    invoke-virtual {p0, v9}, LX/186;->c(I)V

    .line 1725407
    invoke-virtual {p0, v5, v7}, LX/186;->b(II)V

    .line 1725408
    const/4 v5, 0x1

    invoke-virtual {p0, v5, v8}, LX/186;->b(II)V

    .line 1725409
    invoke-virtual {p0}, LX/186;->d()I

    move-result v5

    .line 1725410
    invoke-virtual {p0, v5}, LX/186;->d(I)V

    goto :goto_2

    .line 1725411
    :cond_6
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v8

    const/4 v9, 0x0

    .line 1725412
    if-nez v8, :cond_7

    .line 1725413
    :goto_7
    move v8, v9

    .line 1725414
    const/4 v9, 0x1

    invoke-virtual {p0, v9}, LX/186;->c(I)V

    .line 1725415
    invoke-virtual {p0, v7, v8}, LX/186;->b(II)V

    .line 1725416
    invoke-virtual {p0}, LX/186;->d()I

    move-result v7

    .line 1725417
    invoke-virtual {p0, v7}, LX/186;->d(I)V

    goto :goto_5

    .line 1725418
    :cond_7
    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLMedia;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v10

    invoke-virtual {p0, v10}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v10

    .line 1725419
    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v11

    const/4 v12, 0x0

    .line 1725420
    if-nez v11, :cond_8

    .line 1725421
    :goto_8
    move v11, v12

    .line 1725422
    const/4 v12, 0x2

    invoke-virtual {p0, v12}, LX/186;->c(I)V

    .line 1725423
    invoke-virtual {p0, v9, v10}, LX/186;->b(II)V

    .line 1725424
    const/4 v9, 0x1

    invoke-virtual {p0, v9, v11}, LX/186;->b(II)V

    .line 1725425
    invoke-virtual {p0}, LX/186;->d()I

    move-result v9

    .line 1725426
    invoke-virtual {p0, v9}, LX/186;->d(I)V

    goto :goto_7

    .line 1725427
    :cond_8
    invoke-virtual {v11}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1725428
    const/4 v8, 0x3

    invoke-virtual {p0, v8}, LX/186;->c(I)V

    .line 1725429
    invoke-virtual {v11}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v8

    invoke-virtual {p0, v12, v8, v12}, LX/186;->a(III)V

    .line 1725430
    const/4 v8, 0x1

    invoke-virtual {p0, v8, v0}, LX/186;->b(II)V

    .line 1725431
    const/4 v0, 0x2

    invoke-virtual {v11}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v8

    invoke-virtual {p0, v0, v8, v12}, LX/186;->a(III)V

    .line 1725432
    invoke-virtual {p0}, LX/186;->d()I

    move-result v12

    .line 1725433
    invoke-virtual {p0, v12}, LX/186;->d(I)V

    goto :goto_8
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;)I
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 1725434
    if-nez p1, :cond_0

    .line 1725435
    :goto_0
    return v0

    .line 1725436
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    const/4 v2, 0x0

    .line 1725437
    if-nez v1, :cond_1

    .line 1725438
    :goto_1
    move v1, v2

    .line 1725439
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;->k()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    const/4 v3, 0x0

    .line 1725440
    if-nez v2, :cond_2

    .line 1725441
    :goto_2
    move v2, v3

    .line 1725442
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1725443
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;->n()Lcom/facebook/graphql/model/GraphQLMediaEffectInstruction;

    move-result-object v4

    const/4 v5, 0x0

    .line 1725444
    if-nez v4, :cond_3

    .line 1725445
    :goto_3
    move v4, v5

    .line 1725446
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;->p()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1725447
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;->q()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1725448
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;->r()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1725449
    const/16 v8, 0x9

    invoke-virtual {p0, v8}, LX/186;->c(I)V

    .line 1725450
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 1725451
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v2}, LX/186;->b(II)V

    .line 1725452
    const/4 v0, 0x2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;->l()Z

    move-result v1

    invoke-virtual {p0, v0, v1}, LX/186;->a(IZ)V

    .line 1725453
    const/4 v0, 0x3

    invoke-virtual {p0, v0, v3}, LX/186;->b(II)V

    .line 1725454
    const/4 v0, 0x4

    invoke-virtual {p0, v0, v4}, LX/186;->b(II)V

    .line 1725455
    const/4 v0, 0x5

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;->o()Z

    move-result v1

    invoke-virtual {p0, v0, v1}, LX/186;->a(IZ)V

    .line 1725456
    const/4 v0, 0x6

    invoke-virtual {p0, v0, v5}, LX/186;->b(II)V

    .line 1725457
    const/4 v0, 0x7

    invoke-virtual {p0, v0, v6}, LX/186;->b(II)V

    .line 1725458
    const/16 v0, 0x8

    invoke-virtual {p0, v0, v7}, LX/186;->b(II)V

    .line 1725459
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 1725460
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0

    .line 1725461
    :cond_1
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1725462
    const/4 v4, 0x1

    invoke-virtual {p0, v4}, LX/186;->c(I)V

    .line 1725463
    invoke-virtual {p0, v2, v3}, LX/186;->b(II)V

    .line 1725464
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 1725465
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto :goto_1

    .line 1725466
    :cond_2
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1725467
    const/4 v5, 0x1

    invoke-virtual {p0, v5}, LX/186;->c(I)V

    .line 1725468
    invoke-virtual {p0, v3, v4}, LX/186;->b(II)V

    .line 1725469
    invoke-virtual {p0}, LX/186;->d()I

    move-result v3

    .line 1725470
    invoke-virtual {p0, v3}, LX/186;->d(I)V

    goto/16 :goto_2

    .line 1725471
    :cond_3
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLMediaEffectInstruction;->j()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1725472
    const/4 v7, 0x1

    invoke-virtual {p0, v7}, LX/186;->c(I)V

    .line 1725473
    invoke-virtual {p0, v5, v6}, LX/186;->b(II)V

    .line 1725474
    invoke-virtual {p0}, LX/186;->d()I

    move-result v5

    .line 1725475
    invoke-virtual {p0, v5}, LX/186;->d(I)V

    goto/16 :goto_3
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLSuggestedComposition;)I
    .locals 18

    .prologue
    .line 1725476
    if-nez p1, :cond_0

    .line 1725477
    const/4 v2, 0x0

    .line 1725478
    :goto_0
    return v2

    .line 1725479
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->a()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/AwG;->a(LX/186;Lcom/facebook/graphql/model/GraphQLPlace;)I

    move-result v4

    .line 1725480
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->o()Lcom/facebook/graphql/model/GraphQLSwipeableFrame;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/AwG;->a(LX/186;Lcom/facebook/graphql/model/GraphQLSwipeableFrame;)I

    move-result v5

    .line 1725481
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->p()Lcom/facebook/graphql/model/GraphQLSwipeableFramePack;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/AwG;->a(LX/186;Lcom/facebook/graphql/model/GraphQLSwipeableFramePack;)I

    move-result v6

    .line 1725482
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->q()Lcom/facebook/graphql/model/GraphQLComposerLinkShareActionLink;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/AwG;->a(LX/186;Lcom/facebook/graphql/model/GraphQLComposerLinkShareActionLink;)I

    move-result v7

    .line 1725483
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->r()Lcom/facebook/graphql/model/GraphQLMaskEffect;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/AwG;->a(LX/186;Lcom/facebook/graphql/model/GraphQLMaskEffect;)I

    move-result v8

    .line 1725484
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->s()Lcom/facebook/graphql/model/GraphQLMemeCategory;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/AwG;->a(LX/186;Lcom/facebook/graphql/model/GraphQLMemeCategory;)I

    move-result v9

    .line 1725485
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->j()Lcom/facebook/graphql/model/GraphQLInlineActivity;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/AwG;->a(LX/186;Lcom/facebook/graphql/model/GraphQLInlineActivity;)I

    move-result v10

    .line 1725486
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->t()Lcom/facebook/graphql/model/GraphQLParticleEffect;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/AwG;->a(LX/186;Lcom/facebook/graphql/model/GraphQLParticleEffect;)I

    move-result v11

    .line 1725487
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->k()Lcom/facebook/graphql/model/GraphQLImageOverlay;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/AwG;->a(LX/186;Lcom/facebook/graphql/model/GraphQLImageOverlay;)I

    move-result v12

    .line 1725488
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->u()Lcom/facebook/graphql/model/GraphQLShaderFilter;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/AwG;->a(LX/186;Lcom/facebook/graphql/model/GraphQLShaderFilter;)I

    move-result v13

    .line 1725489
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->v()Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/AwG;->a(LX/186;Lcom/facebook/graphql/model/GraphQLStyleTransferEffect;)I

    move-result v14

    .line 1725490
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->l()LX/0Px;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/util/List;)I

    move-result v15

    .line 1725491
    const/4 v2, 0x0

    .line 1725492
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->n()LX/0Px;

    move-result-object v16

    .line 1725493
    if-eqz v16, :cond_2

    .line 1725494
    invoke-virtual/range {v16 .. v16}, LX/0Px;->size()I

    move-result v2

    new-array v0, v2, [I

    move-object/from16 v17, v0

    .line 1725495
    const/4 v2, 0x0

    move v3, v2

    :goto_1
    invoke-virtual/range {v16 .. v16}, LX/0Px;->size()I

    move-result v2

    if-ge v3, v2, :cond_1

    .line 1725496
    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLUser;

    move-object/from16 v0, p0

    invoke-static {v0, v2}, LX/AwG;->a(LX/186;Lcom/facebook/graphql/model/GraphQLUser;)I

    move-result v2

    aput v2, v17, v3

    .line 1725497
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    .line 1725498
    :cond_1
    const/4 v2, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v2}, LX/186;->a([IZ)I

    move-result v2

    .line 1725499
    :cond_2
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/model/GraphQLSuggestedComposition;->m()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/AwG;->o(LX/186;Lcom/facebook/graphql/model/GraphQLImage;)I

    move-result v3

    .line 1725500
    const/16 v16, 0xe

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 1725501
    const/16 v16, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 1725502
    const/4 v4, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v5}, LX/186;->b(II)V

    .line 1725503
    const/4 v4, 0x2

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v6}, LX/186;->b(II)V

    .line 1725504
    const/4 v4, 0x3

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v7}, LX/186;->b(II)V

    .line 1725505
    const/4 v4, 0x4

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v8}, LX/186;->b(II)V

    .line 1725506
    const/4 v4, 0x5

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v9}, LX/186;->b(II)V

    .line 1725507
    const/4 v4, 0x6

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v10}, LX/186;->b(II)V

    .line 1725508
    const/4 v4, 0x7

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v11}, LX/186;->b(II)V

    .line 1725509
    const/16 v4, 0x8

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v12}, LX/186;->b(II)V

    .line 1725510
    const/16 v4, 0x9

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v13}, LX/186;->b(II)V

    .line 1725511
    const/16 v4, 0xa

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v14}, LX/186;->b(II)V

    .line 1725512
    const/16 v4, 0xb

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v15}, LX/186;->b(II)V

    .line 1725513
    const/16 v4, 0xc

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v2}, LX/186;->b(II)V

    .line 1725514
    const/16 v2, 0xd

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 1725515
    invoke-virtual/range {p0 .. p0}, LX/186;->d()I

    move-result v2

    .line 1725516
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, LX/186;->d(I)V

    goto/16 :goto_0
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLSuggestedCompositionsConnection;)I
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 1725517
    if-nez p1, :cond_0

    .line 1725518
    :goto_0
    return v2

    .line 1725519
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSuggestedCompositionsConnection;->a()LX/0Px;

    move-result-object v3

    .line 1725520
    if-eqz v3, :cond_2

    .line 1725521
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    new-array v4, v0, [I

    move v1, v2

    .line 1725522
    :goto_1
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1725523
    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSuggestedCompositionsEdge;

    invoke-static {p0, v0}, LX/AwG;->a(LX/186;Lcom/facebook/graphql/model/GraphQLSuggestedCompositionsEdge;)I

    move-result v0

    aput v0, v4, v1

    .line 1725524
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1725525
    :cond_1
    invoke-virtual {p0, v4, v5}, LX/186;->a([IZ)I

    move-result v0

    .line 1725526
    :goto_2
    invoke-virtual {p0, v5}, LX/186;->c(I)V

    .line 1725527
    invoke-virtual {p0, v2, v0}, LX/186;->b(II)V

    .line 1725528
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 1725529
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_2
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLSuggestedCompositionsEdge;)I
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 1725530
    if-nez p1, :cond_0

    .line 1725531
    :goto_0
    return v0

    .line 1725532
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSuggestedCompositionsEdge;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1725533
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSuggestedCompositionsEdge;->j()Lcom/facebook/graphql/model/GraphQLSuggestedComposition;

    move-result-object v2

    invoke-static {p0, v2}, LX/AwG;->a(LX/186;Lcom/facebook/graphql/model/GraphQLSuggestedComposition;)I

    move-result v2

    .line 1725534
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSuggestedCompositionsEdge;->k()Lcom/facebook/graphql/enums/GraphQLPromptOpenAction;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    .line 1725535
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSuggestedCompositionsEdge;->l()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1725536
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSuggestedCompositionsEdge;->m()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1725537
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSuggestedCompositionsEdge;->n()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1725538
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSuggestedCompositionsEdge;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v7

    const/4 v8, 0x0

    .line 1725539
    if-nez v7, :cond_1

    .line 1725540
    :goto_1
    move v7, v8

    .line 1725541
    const/4 v8, 0x7

    invoke-virtual {p0, v8}, LX/186;->c(I)V

    .line 1725542
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 1725543
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v2}, LX/186;->b(II)V

    .line 1725544
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v3}, LX/186;->b(II)V

    .line 1725545
    const/4 v0, 0x3

    invoke-virtual {p0, v0, v4}, LX/186;->b(II)V

    .line 1725546
    const/4 v0, 0x4

    invoke-virtual {p0, v0, v5}, LX/186;->b(II)V

    .line 1725547
    const/4 v0, 0x5

    invoke-virtual {p0, v0, v6}, LX/186;->b(II)V

    .line 1725548
    const/4 v0, 0x6

    invoke-virtual {p0, v0, v7}, LX/186;->b(II)V

    .line 1725549
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 1725550
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0

    .line 1725551
    :cond_1
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 1725552
    const/4 p1, 0x1

    invoke-virtual {p0, p1}, LX/186;->c(I)V

    .line 1725553
    invoke-virtual {p0, v8, v9}, LX/186;->b(II)V

    .line 1725554
    invoke-virtual {p0}, LX/186;->d()I

    move-result v8

    .line 1725555
    invoke-virtual {p0, v8}, LX/186;->d(I)V

    goto :goto_1
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLSwipeableFrame;)I
    .locals 14

    .prologue
    const-wide/16 v4, 0x0

    const/4 v0, 0x0

    .line 1725556
    if-nez p1, :cond_0

    .line 1725557
    :goto_0
    return v0

    .line 1725558
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    const/4 v2, 0x0

    .line 1725559
    if-nez v1, :cond_1

    .line 1725560
    :goto_1
    move v1, v2

    .line 1725561
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->k()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    const/4 v3, 0x0

    .line 1725562
    if-nez v2, :cond_2

    .line 1725563
    :goto_2
    move v2, v3

    .line 1725564
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->l()Lcom/facebook/graphql/model/GraphQLCreativeFilter;

    move-result-object v3

    invoke-static {p0, v3}, LX/AwG;->a(LX/186;Lcom/facebook/graphql/model/GraphQLCreativeFilter;)I

    move-result v3

    .line 1725565
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->n()Lcom/facebook/graphql/model/GraphQLFrameImageAssetConnection;

    move-result-object v6

    const/4 v12, 0x1

    const/4 v9, 0x0

    .line 1725566
    if-nez v6, :cond_3

    .line 1725567
    :goto_3
    move v6, v9

    .line 1725568
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->o()Lcom/facebook/graphql/model/GraphQLFrameTextAssetConnection;

    move-result-object v7

    const/4 v13, 0x1

    const/4 v10, 0x0

    .line 1725569
    if-nez v7, :cond_6

    .line 1725570
    :goto_4
    move v7, v10

    .line 1725571
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->q()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 1725572
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->r()Lcom/facebook/graphql/model/GraphQLMediaEffectInstruction;

    move-result-object v9

    const/4 v10, 0x0

    .line 1725573
    if-nez v9, :cond_9

    .line 1725574
    :goto_5
    move v9, v10

    .line 1725575
    const/16 v10, 0xb

    invoke-virtual {p0, v10}, LX/186;->c(I)V

    .line 1725576
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 1725577
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v2}, LX/186;->b(II)V

    .line 1725578
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v3}, LX/186;->b(II)V

    .line 1725579
    const/4 v1, 0x3

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->m()J

    move-result-wide v2

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1725580
    const/4 v0, 0x4

    invoke-virtual {p0, v0, v6}, LX/186;->b(II)V

    .line 1725581
    const/4 v0, 0x5

    invoke-virtual {p0, v0, v7}, LX/186;->b(II)V

    .line 1725582
    const/4 v0, 0x6

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->p()Z

    move-result v1

    invoke-virtual {p0, v0, v1}, LX/186;->a(IZ)V

    .line 1725583
    const/4 v0, 0x7

    invoke-virtual {p0, v0, v8}, LX/186;->b(II)V

    .line 1725584
    const/16 v0, 0x8

    invoke-virtual {p0, v0, v9}, LX/186;->b(II)V

    .line 1725585
    const/16 v0, 0x9

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->s()Z

    move-result v1

    invoke-virtual {p0, v0, v1}, LX/186;->a(IZ)V

    .line 1725586
    const/16 v1, 0xa

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;->t()J

    move-result-wide v2

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1725587
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 1725588
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto/16 :goto_0

    .line 1725589
    :cond_1
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1725590
    const/4 v6, 0x1

    invoke-virtual {p0, v6}, LX/186;->c(I)V

    .line 1725591
    invoke-virtual {p0, v2, v3}, LX/186;->b(II)V

    .line 1725592
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 1725593
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto/16 :goto_1

    .line 1725594
    :cond_2
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1725595
    const/4 v7, 0x1

    invoke-virtual {p0, v7}, LX/186;->c(I)V

    .line 1725596
    invoke-virtual {p0, v3, v6}, LX/186;->b(II)V

    .line 1725597
    invoke-virtual {p0}, LX/186;->d()I

    move-result v3

    .line 1725598
    invoke-virtual {p0, v3}, LX/186;->d(I)V

    goto/16 :goto_2

    .line 1725599
    :cond_3
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLFrameImageAssetConnection;->a()LX/0Px;

    move-result-object v10

    .line 1725600
    if-eqz v10, :cond_5

    .line 1725601
    invoke-virtual {v10}, LX/0Px;->size()I

    move-result v7

    new-array v11, v7, [I

    move v8, v9

    .line 1725602
    :goto_6
    invoke-virtual {v10}, LX/0Px;->size()I

    move-result v7

    if-ge v8, v7, :cond_4

    .line 1725603
    invoke-virtual {v10, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/facebook/graphql/model/GraphQLFrameImageAsset;

    invoke-static {p0, v7}, LX/AwG;->a(LX/186;Lcom/facebook/graphql/model/GraphQLFrameImageAsset;)I

    move-result v7

    aput v7, v11, v8

    .line 1725604
    add-int/lit8 v7, v8, 0x1

    move v8, v7

    goto :goto_6

    .line 1725605
    :cond_4
    invoke-virtual {p0, v11, v12}, LX/186;->a([IZ)I

    move-result v7

    .line 1725606
    :goto_7
    invoke-virtual {p0, v12}, LX/186;->c(I)V

    .line 1725607
    invoke-virtual {p0, v9, v7}, LX/186;->b(II)V

    .line 1725608
    invoke-virtual {p0}, LX/186;->d()I

    move-result v9

    .line 1725609
    invoke-virtual {p0, v9}, LX/186;->d(I)V

    goto/16 :goto_3

    :cond_5
    move v7, v9

    goto :goto_7

    .line 1725610
    :cond_6
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLFrameTextAssetConnection;->a()LX/0Px;

    move-result-object v11

    .line 1725611
    if-eqz v11, :cond_8

    .line 1725612
    invoke-virtual {v11}, LX/0Px;->size()I

    move-result v8

    new-array v12, v8, [I

    move v9, v10

    .line 1725613
    :goto_8
    invoke-virtual {v11}, LX/0Px;->size()I

    move-result v8

    if-ge v9, v8, :cond_7

    .line 1725614
    invoke-virtual {v11, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/facebook/graphql/model/GraphQLFrameTextAsset;

    invoke-static {p0, v8}, LX/AwG;->a(LX/186;Lcom/facebook/graphql/model/GraphQLFrameTextAsset;)I

    move-result v8

    aput v8, v12, v9

    .line 1725615
    add-int/lit8 v8, v9, 0x1

    move v9, v8

    goto :goto_8

    .line 1725616
    :cond_7
    invoke-virtual {p0, v12, v13}, LX/186;->a([IZ)I

    move-result v8

    .line 1725617
    :goto_9
    invoke-virtual {p0, v13}, LX/186;->c(I)V

    .line 1725618
    invoke-virtual {p0, v10, v8}, LX/186;->b(II)V

    .line 1725619
    invoke-virtual {p0}, LX/186;->d()I

    move-result v10

    .line 1725620
    invoke-virtual {p0, v10}, LX/186;->d(I)V

    goto/16 :goto_4

    :cond_8
    move v8, v10

    goto :goto_9

    .line 1725621
    :cond_9
    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLMediaEffectInstruction;->j()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {p0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 1725622
    const/4 v12, 0x1

    invoke-virtual {p0, v12}, LX/186;->c(I)V

    .line 1725623
    invoke-virtual {p0, v10, v11}, LX/186;->b(II)V

    .line 1725624
    invoke-virtual {p0}, LX/186;->d()I

    move-result v10

    .line 1725625
    invoke-virtual {p0, v10}, LX/186;->d(I)V

    goto/16 :goto_5
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLSwipeableFramePack;)I
    .locals 10

    .prologue
    const-wide/16 v4, 0x0

    const/4 v9, 0x1

    const/4 v1, 0x0

    .line 1725626
    if-nez p1, :cond_0

    .line 1725627
    :goto_0
    return v1

    .line 1725628
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSwipeableFramePack;->k()LX/0Px;

    move-result-object v3

    .line 1725629
    if-eqz v3, :cond_2

    .line 1725630
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    new-array v6, v0, [I

    move v2, v1

    .line 1725631
    :goto_1
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    .line 1725632
    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLSwipeableFrame;

    invoke-static {p0, v0}, LX/AwG;->a(LX/186;Lcom/facebook/graphql/model/GraphQLSwipeableFrame;)I

    move-result v0

    aput v0, v6, v2

    .line 1725633
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 1725634
    :cond_1
    invoke-virtual {p0, v6, v9}, LX/186;->a([IZ)I

    move-result v0

    move v6, v0

    .line 1725635
    :goto_2
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSwipeableFramePack;->l()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1725636
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSwipeableFramePack;->m()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 1725637
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, LX/186;->c(I)V

    .line 1725638
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSwipeableFramePack;->j()J

    move-result-wide v2

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1725639
    invoke-virtual {p0, v9, v6}, LX/186;->b(II)V

    .line 1725640
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v7}, LX/186;->b(II)V

    .line 1725641
    const/4 v0, 0x3

    invoke-virtual {p0, v0, v8}, LX/186;->b(II)V

    .line 1725642
    const/4 v1, 0x4

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLSwipeableFramePack;->n()J

    move-result-wide v2

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1725643
    invoke-virtual {p0}, LX/186;->d()I

    move-result v1

    .line 1725644
    invoke-virtual {p0, v1}, LX/186;->d(I)V

    goto :goto_0

    :cond_2
    move v6, v1

    goto :goto_2
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLTaggableActivity;)I
    .locals 14

    .prologue
    .line 1725645
    if-nez p1, :cond_0

    .line 1725646
    const/4 v0, 0x0

    .line 1725647
    :goto_0
    return v0

    .line 1725648
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->j()Lcom/facebook/graphql/model/GraphQLTaggableActivityAllIconsConnection;

    move-result-object v0

    const/4 v1, 0x0

    .line 1725649
    if-nez v0, :cond_1

    .line 1725650
    :goto_1
    move v0, v1

    .line 1725651
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->k()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    const/4 v2, 0x0

    .line 1725652
    if-nez v1, :cond_2

    .line 1725653
    :goto_2
    move v1, v2

    .line 1725654
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    const/4 v3, 0x0

    .line 1725655
    if-nez v2, :cond_3

    .line 1725656
    :goto_3
    move v2, v3

    .line 1725657
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1725658
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1725659
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->q()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1725660
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->r()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v6

    invoke-static {p0, v6}, LX/AwG;->a(LX/186;Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;)I

    move-result v6

    .line 1725661
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->s()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v7

    invoke-static {p0, v7}, LX/AwG;->a(LX/186;Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;)I

    move-result v7

    .line 1725662
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->t()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v8

    invoke-static {p0, v8}, LX/AwG;->a(LX/186;Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;)I

    move-result v8

    .line 1725663
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->u()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v9

    invoke-static {p0, v9}, LX/AwG;->a(LX/186;Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;)I

    move-result v9

    .line 1725664
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->v()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v10

    invoke-static {p0, v10}, LX/AwG;->a(LX/186;Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;)I

    move-result v10

    .line 1725665
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->w()Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;

    move-result-object v11

    invoke-static {p0, v11}, LX/AwG;->a(LX/186;Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;)I

    move-result v11

    .line 1725666
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->x()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {p0, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 1725667
    const/16 v13, 0x12

    invoke-virtual {p0, v13}, LX/186;->c(I)V

    .line 1725668
    const/4 v13, 0x0

    invoke-virtual {p0, v13, v0}, LX/186;->b(II)V

    .line 1725669
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 1725670
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v2}, LX/186;->b(II)V

    .line 1725671
    const/4 v0, 0x3

    invoke-virtual {p0, v0, v3}, LX/186;->b(II)V

    .line 1725672
    const/4 v0, 0x4

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->n()Z

    move-result v1

    invoke-virtual {p0, v0, v1}, LX/186;->a(IZ)V

    .line 1725673
    const/4 v0, 0x5

    invoke-virtual {p0, v0, v4}, LX/186;->b(II)V

    .line 1725674
    const/4 v0, 0x6

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->p()I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, LX/186;->a(III)V

    .line 1725675
    const/4 v0, 0x7

    invoke-virtual {p0, v0, v5}, LX/186;->b(II)V

    .line 1725676
    const/16 v0, 0x8

    invoke-virtual {p0, v0, v6}, LX/186;->b(II)V

    .line 1725677
    const/16 v0, 0x9

    invoke-virtual {p0, v0, v7}, LX/186;->b(II)V

    .line 1725678
    const/16 v0, 0xa

    invoke-virtual {p0, v0, v8}, LX/186;->b(II)V

    .line 1725679
    const/16 v0, 0xb

    invoke-virtual {p0, v0, v9}, LX/186;->b(II)V

    .line 1725680
    const/16 v0, 0xc

    invoke-virtual {p0, v0, v10}, LX/186;->b(II)V

    .line 1725681
    const/16 v0, 0xd

    invoke-virtual {p0, v0, v11}, LX/186;->b(II)V

    .line 1725682
    const/16 v0, 0xe

    invoke-virtual {p0, v0, v12}, LX/186;->b(II)V

    .line 1725683
    const/16 v0, 0xf

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->y()Z

    move-result v1

    invoke-virtual {p0, v0, v1}, LX/186;->a(IZ)V

    .line 1725684
    const/16 v0, 0x10

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->z()Z

    move-result v1

    invoke-virtual {p0, v0, v1}, LX/186;->a(IZ)V

    .line 1725685
    const/16 v0, 0x11

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTaggableActivity;->A()Z

    move-result v1

    invoke-virtual {p0, v0, v1}, LX/186;->a(IZ)V

    .line 1725686
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 1725687
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto/16 :goto_0

    .line 1725688
    :cond_1
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, LX/186;->c(I)V

    .line 1725689
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTaggableActivityAllIconsConnection;->a()I

    move-result v2

    invoke-virtual {p0, v1, v2, v1}, LX/186;->a(III)V

    .line 1725690
    invoke-virtual {p0}, LX/186;->d()I

    move-result v1

    .line 1725691
    invoke-virtual {p0, v1}, LX/186;->d(I)V

    goto/16 :goto_1

    .line 1725692
    :cond_2
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1725693
    const/4 v4, 0x1

    invoke-virtual {p0, v4}, LX/186;->c(I)V

    .line 1725694
    invoke-virtual {p0, v2, v3}, LX/186;->b(II)V

    .line 1725695
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 1725696
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto/16 :goto_2

    .line 1725697
    :cond_3
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1725698
    const/4 v5, 0x1

    invoke-virtual {p0, v5}, LX/186;->c(I)V

    .line 1725699
    invoke-virtual {p0, v3, v4}, LX/186;->b(II)V

    .line 1725700
    invoke-virtual {p0}, LX/186;->d()I

    move-result v3

    .line 1725701
    invoke-virtual {p0, v3}, LX/186;->d(I)V

    goto/16 :goto_3
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;)I
    .locals 9

    .prologue
    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 1725260
    if-nez p1, :cond_0

    .line 1725261
    :goto_0
    return v2

    .line 1725262
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1725263
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTaggableActivityPreviewTemplate;->j()LX/0Px;

    move-result-object v4

    .line 1725264
    if-eqz v4, :cond_2

    .line 1725265
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v0

    new-array v5, v0, [I

    move v1, v2

    .line 1725266
    :goto_1
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1725267
    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActivityTemplateToken;

    const/4 v7, 0x0

    .line 1725268
    if-nez v0, :cond_3

    .line 1725269
    :goto_2
    move v0, v7

    .line 1725270
    aput v0, v5, v1

    .line 1725271
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1725272
    :cond_1
    invoke-virtual {p0, v5, v6}, LX/186;->a([IZ)I

    move-result v0

    .line 1725273
    :goto_3
    const/4 v1, 0x2

    invoke-virtual {p0, v1}, LX/186;->c(I)V

    .line 1725274
    invoke-virtual {p0, v2, v3}, LX/186;->b(II)V

    .line 1725275
    invoke-virtual {p0, v6, v0}, LX/186;->b(II)V

    .line 1725276
    invoke-virtual {p0}, LX/186;->d()I

    move-result v2

    .line 1725277
    invoke-virtual {p0, v2}, LX/186;->d(I)V

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_3

    .line 1725278
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActivityTemplateToken;->j()Lcom/facebook/graphql/enums/GraphQLActivityTemplateTokenType;

    move-result-object v8

    invoke-virtual {p0, v8}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v8

    .line 1725279
    const/4 p1, 0x2

    invoke-virtual {p0, p1}, LX/186;->c(I)V

    .line 1725280
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActivityTemplateToken;->a()I

    move-result p1

    invoke-virtual {p0, v7, p1, v7}, LX/186;->a(III)V

    .line 1725281
    const/4 v7, 0x1

    invoke-virtual {p0, v7, v8}, LX/186;->b(II)V

    .line 1725282
    invoke-virtual {p0}, LX/186;->d()I

    move-result v7

    .line 1725283
    invoke-virtual {p0, v7}, LX/186;->d(I)V

    goto :goto_2
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLUser;)I
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 1725702
    if-nez p1, :cond_0

    .line 1725703
    :goto_0
    return v0

    .line 1725704
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLUser;->v()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1725705
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLUser;->F()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1725706
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLUser;->N()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v3

    const/4 v4, 0x0

    .line 1725707
    if-nez v3, :cond_1

    .line 1725708
    :goto_1
    move v3, v4

    .line 1725709
    const/4 v4, 0x3

    invoke-virtual {p0, v4}, LX/186;->c(I)V

    .line 1725710
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 1725711
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v2}, LX/186;->b(II)V

    .line 1725712
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v3}, LX/186;->b(II)V

    .line 1725713
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 1725714
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0

    .line 1725715
    :cond_1
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPhoto;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    const/4 v6, 0x0

    .line 1725716
    if-nez v5, :cond_2

    .line 1725717
    :goto_2
    move v5, v6

    .line 1725718
    const/4 v6, 0x1

    invoke-virtual {p0, v6}, LX/186;->c(I)V

    .line 1725719
    invoke-virtual {p0, v4, v5}, LX/186;->b(II)V

    .line 1725720
    invoke-virtual {p0}, LX/186;->d()I

    move-result v4

    .line 1725721
    invoke-virtual {p0, v4}, LX/186;->d(I)V

    goto :goto_1

    .line 1725722
    :cond_2
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, LX/186;->b(Ljava/lang/String;)I

    move-result p1

    .line 1725723
    const/4 v3, 0x1

    invoke-virtual {p0, v3}, LX/186;->c(I)V

    .line 1725724
    invoke-virtual {p0, v6, p1}, LX/186;->b(II)V

    .line 1725725
    invoke-virtual {p0}, LX/186;->d()I

    move-result v6

    .line 1725726
    invoke-virtual {p0, v6}, LX/186;->d(I)V

    goto :goto_2
.end method

.method private static b(LX/186;Lcom/facebook/graphql/model/GraphQLFrameTextAssetSize;)I
    .locals 6

    .prologue
    const/4 v0, 0x0

    const-wide/16 v4, 0x0

    .line 1725727
    if-nez p1, :cond_0

    .line 1725728
    :goto_0
    return v0

    .line 1725729
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFrameTextAssetSize;->a()Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 1725730
    const/4 v2, 0x3

    invoke-virtual {p0, v2}, LX/186;->c(I)V

    .line 1725731
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 1725732
    const/4 v1, 0x1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFrameTextAssetSize;->j()D

    move-result-wide v2

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1725733
    const/4 v1, 0x2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFrameTextAssetSize;->k()D

    move-result-wide v2

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1725734
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 1725735
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0
.end method

.method private static c(LX/186;Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;)I
    .locals 7

    .prologue
    const/4 v0, 0x0

    const-wide/16 v4, 0x0

    .line 1725736
    if-nez p1, :cond_0

    .line 1725737
    :goto_0
    return v0

    .line 1725738
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;->a()Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 1725739
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;->k()Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v6

    .line 1725740
    const/4 v2, 0x4

    invoke-virtual {p0, v2}, LX/186;->c(I)V

    .line 1725741
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 1725742
    const/4 v1, 0x1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;->j()D

    move-result-wide v2

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1725743
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v6}, LX/186;->b(II)V

    .line 1725744
    const/4 v1, 0x3

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;->l()D

    move-result-wide v2

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1725745
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 1725746
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0
.end method

.method private static d(LX/186;Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;)I
    .locals 7

    .prologue
    const/4 v0, 0x0

    const-wide/16 v4, 0x0

    .line 1725747
    if-nez p1, :cond_0

    .line 1725748
    :goto_0
    return v0

    .line 1725749
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;->a()Lcom/facebook/graphql/enums/GraphQLAssetHorizontalAlignmentType;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 1725750
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;->k()Lcom/facebook/graphql/enums/GraphQLAssetVerticalAlignmentType;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v6

    .line 1725751
    const/4 v2, 0x4

    invoke-virtual {p0, v2}, LX/186;->c(I)V

    .line 1725752
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 1725753
    const/4 v1, 0x1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;->j()D

    move-result-wide v2

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1725754
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v6}, LX/186;->b(II)V

    .line 1725755
    const/4 v1, 0x3

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFrameAssetAnchoring;->l()D

    move-result-wide v2

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1725756
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 1725757
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0
.end method

.method public static k(LX/186;Lcom/facebook/graphql/model/GraphQLImage;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1725758
    if-nez p1, :cond_0

    .line 1725759
    :goto_0
    return v0

    .line 1725760
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1725761
    const/4 v2, 0x3

    invoke-virtual {p0, v2}, LX/186;->c(I)V

    .line 1725762
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v2

    invoke-virtual {p0, v0, v2, v0}, LX/186;->a(III)V

    .line 1725763
    const/4 v2, 0x1

    invoke-virtual {p0, v2, v1}, LX/186;->b(II)V

    .line 1725764
    const/4 v1, 0x2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v2

    invoke-virtual {p0, v1, v2, v0}, LX/186;->a(III)V

    .line 1725765
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 1725766
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0
.end method

.method private static o(LX/186;Lcom/facebook/graphql/model/GraphQLImage;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1725767
    if-nez p1, :cond_0

    .line 1725768
    :goto_0
    return v0

    .line 1725769
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1725770
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, LX/186;->c(I)V

    .line 1725771
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 1725772
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 1725773
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0
.end method
