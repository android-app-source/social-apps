.class public final LX/BN4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/profilelist/ProfilesListFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/profilelist/ProfilesListFragment;)V
    .locals 0

    .prologue
    .line 1778234
    iput-object p1, p0, LX/BN4;->a:Lcom/facebook/profilelist/ProfilesListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 1778235
    iget-object v0, p0, LX/BN4;->a:Lcom/facebook/profilelist/ProfilesListFragment;

    iget-object v0, v0, Lcom/facebook/profilelist/ProfilesListFragment;->c:LX/BN0;

    .line 1778236
    instance-of p0, p2, Lcom/facebook/profilelist/ProfileView;

    if-nez p0, :cond_0

    .line 1778237
    :goto_0
    return-void

    .line 1778238
    :cond_0
    invoke-virtual {v0, p3}, LX/BN0;->getItem(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/BMz;

    iget-object p1, p0, LX/BMz;->b:Lcom/facebook/ipc/model/FacebookProfile;

    .line 1778239
    check-cast p2, Lcom/facebook/profilelist/ProfileView;

    .line 1778240
    iget-object p0, p2, Lcom/facebook/profilelist/ProfileView;->d:Landroid/widget/CheckBox;

    invoke-virtual {p0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result p0

    move p0, p0

    .line 1778241
    if-nez p0, :cond_1

    const/4 p0, 0x1

    .line 1778242
    :goto_1
    invoke-virtual {p2, p0}, Lcom/facebook/profilelist/ProfileView;->setIsSelected(Z)V

    .line 1778243
    if-eqz p0, :cond_2

    .line 1778244
    iget-object p0, v0, LX/BN0;->e:Ljava/util/Set;

    invoke-interface {p0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1778245
    :cond_1
    const/4 p0, 0x0

    goto :goto_1

    .line 1778246
    :cond_2
    iget-object p0, v0, LX/BN0;->e:Ljava/util/Set;

    invoke-interface {p0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method
