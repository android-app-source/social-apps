.class public LX/CEp;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field public final b:LX/0tX;

.field public final c:LX/CEw;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1861975
    const-class v0, LX/CEp;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/CEp;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(LX/0tX;LX/CEw;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1861976
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1861977
    iput-object p1, p0, LX/CEp;->b:LX/0tX;

    .line 1861978
    iput-object p2, p0, LX/CEp;->c:LX/CEw;

    .line 1861979
    return-void
.end method

.method public static a(Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1861969
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1

    .line 1861970
    :cond_0
    :goto_0
    return v0

    .line 1861971
    :cond_1
    :try_start_0
    invoke-static {p0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1861972
    const/4 v0, 0x1

    goto :goto_0

    .line 1861973
    :catch_0
    move-exception v1

    .line 1861974
    sget-object v2, LX/CEp;->a:Ljava/lang/String;

    const-string v3, "invalid color"

    invoke-static {v2, v3, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/CEp;
    .locals 3

    .prologue
    .line 1861967
    new-instance v2, LX/CEp;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-static {p0}, LX/CEw;->a(LX/0QB;)LX/CEw;

    move-result-object v1

    check-cast v1, LX/CEw;

    invoke-direct {v2, v0, v1}, LX/CEp;-><init>(LX/0tX;LX/CEw;)V

    .line 1861968
    return-object v2
.end method


# virtual methods
.method public final a(F)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(F)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/1c9",
            "<",
            "Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1861955
    sget-object v0, LX/0zS;->a:LX/0zS;

    .line 1861956
    iget-object v1, p0, LX/CEp;->c:LX/CEw;

    .line 1861957
    iget-object v2, v1, LX/CEw;->a:LX/1c9;

    move-object v1, v2

    .line 1861958
    if-eqz v1, :cond_0

    .line 1861959
    invoke-static {v1}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 1861960
    :goto_0
    move-object v0, v1

    .line 1861961
    return-object v0

    .line 1861962
    :cond_0
    new-instance v3, LX/CEq;

    invoke-direct {v3}, LX/CEq;-><init>()V

    move-object v3, v3

    .line 1861963
    const-string v4, "default_image_scale"

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1861964
    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    invoke-virtual {v3, v0}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v3

    const-wide/32 v5, 0x15180

    invoke-virtual {v3, v5, v6}, LX/0zO;->a(J)LX/0zO;

    move-result-object v3

    .line 1861965
    iget-object v4, p0, LX/CEp;->b:LX/0tX;

    invoke-virtual {v4, v3}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v3

    move-object v1, v3

    .line 1861966
    new-instance v2, LX/CEo;

    invoke-direct {v2, p0}, LX/CEo;-><init>(LX/CEp;)V

    invoke-static {v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    goto :goto_0
.end method
