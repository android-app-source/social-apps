.class public final LX/BTm;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/video/downloadmanager/graphql/SavedVideoMutationModels$SavedVideoDownloadStateMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/BTn;


# direct methods
.method public constructor <init>(LX/BTn;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1787981
    iput-object p1, p0, LX/BTm;->b:LX/BTn;

    iput-object p2, p0, LX/BTm;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 1787982
    sget-object v0, LX/BTn;->a:Ljava/lang/String;

    const-string v1, "Download complete notification failed %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LX/BTm;->a:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v0, p1, v1, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1787983
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1787977
    iget-object v0, p0, LX/BTm;->b:LX/BTn;

    iget-object v0, v0, LX/BTn;->c:LX/15V;

    .line 1787978
    new-instance v1, Ljava/util/Timer;

    invoke-direct {v1}, Ljava/util/Timer;-><init>()V

    .line 1787979
    new-instance v2, Lcom/facebook/video/downloadmanager/DownloadVideoUtils$3;

    invoke-direct {v2, v0}, Lcom/facebook/video/downloadmanager/DownloadVideoUtils$3;-><init>(LX/15V;)V

    const-wide/16 v3, 0x7d0

    invoke-virtual {v1, v2, v3, v4}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 1787980
    return-void
.end method
