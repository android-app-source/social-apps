.class public LX/Ah9;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:LX/0tX;

.field public final c:Ljava/util/concurrent/ScheduledExecutorService;

.field public final d:LX/03V;

.field public final e:Landroid/os/Handler;

.field public final f:Lcom/facebook/auth/viewercontext/ViewerContext;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1702193
    const-class v0, LX/Ah9;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Ah9;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0tX;LX/03V;Ljava/util/concurrent/ScheduledExecutorService;Landroid/os/Handler;Lcom/facebook/auth/viewercontext/ViewerContext;)V
    .locals 0
    .param p3    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/SingleThreadedExecutorService;
        .end annotation
    .end param
    .param p4    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1702194
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1702195
    iput-object p1, p0, LX/Ah9;->b:LX/0tX;

    .line 1702196
    iput-object p2, p0, LX/Ah9;->d:LX/03V;

    .line 1702197
    iput-object p3, p0, LX/Ah9;->c:Ljava/util/concurrent/ScheduledExecutorService;

    .line 1702198
    iput-object p4, p0, LX/Ah9;->e:Landroid/os/Handler;

    .line 1702199
    iput-object p5, p0, LX/Ah9;->f:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1702200
    return-void
.end method

.method public static a(LX/0QB;)LX/Ah9;
    .locals 1

    .prologue
    .line 1702201
    invoke-static {p0}, LX/Ah9;->b(LX/0QB;)LX/Ah9;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/Ah9;
    .locals 6

    .prologue
    .line 1702202
    new-instance v0, LX/Ah9;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v1

    check-cast v1, LX/0tX;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v2

    check-cast v2, LX/03V;

    invoke-static {p0}, LX/0UA;->b(LX/0QB;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {p0}, LX/0Ss;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v4

    check-cast v4, Landroid/os/Handler;

    invoke-static {p0}, LX/0eQ;->b(LX/0QB;)Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v5

    check-cast v5, Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-direct/range {v0 .. v5}, LX/Ah9;-><init>(LX/0tX;LX/03V;Ljava/util/concurrent/ScheduledExecutorService;Landroid/os/Handler;Lcom/facebook/auth/viewercontext/ViewerContext;)V

    .line 1702203
    return-object v0
.end method
