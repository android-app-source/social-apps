.class public LX/BR2;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field private final b:LX/0ad;

.field public final c:LX/0iA;

.field public d:Z


# direct methods
.method private constructor <init>(Landroid/content/Context;LX/0ad;LX/0iA;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1783134
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1783135
    iput-object p1, p0, LX/BR2;->a:Landroid/content/Context;

    .line 1783136
    iput-object p2, p0, LX/BR2;->b:LX/0ad;

    .line 1783137
    iput-object p3, p0, LX/BR2;->c:LX/0iA;

    .line 1783138
    return-void
.end method

.method public static b(LX/0QB;)LX/BR2;
    .locals 4

    .prologue
    .line 1783132
    new-instance v3, LX/BR2;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v1

    check-cast v1, LX/0ad;

    invoke-static {p0}, LX/0iA;->a(LX/0QB;)LX/0iA;

    move-result-object v2

    check-cast v2, LX/0iA;

    invoke-direct {v3, v0, v1, v2}, LX/BR2;-><init>(Landroid/content/Context;LX/0ad;LX/0iA;)V

    .line 1783133
    return-object v3
.end method
