.class public final LX/BR7;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Landroid/net/Uri;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/5QV;

.field public final synthetic b:LX/BR8;


# direct methods
.method public constructor <init>(LX/BR8;LX/5QV;)V
    .locals 0

    .prologue
    .line 1783207
    iput-object p1, p0, LX/BR7;->b:LX/BR8;

    iput-object p2, p0, LX/BR7;->a:LX/5QV;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1783208
    iget-object v0, p0, LX/BR7;->b:LX/BR8;

    iget-object v0, v0, LX/BR8;->b:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    iget-object v0, v0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->v:LX/03V;

    const-string v1, "timeline_staging_grouund"

    const-string v2, "Failure applying profile pic frame on USE button click"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1783209
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1783210
    check-cast p1, Landroid/net/Uri;

    .line 1783211
    iget-object v0, p0, LX/BR7;->b:LX/BR8;

    iget-object v0, v0, LX/BR8;->b:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    iget-object v1, p0, LX/BR7;->b:LX/BR8;

    iget-object v1, v1, LX/BR8;->b:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    iget-object v1, v1, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->g:Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;

    invoke-static {v1}, LX/5Ru;->a(Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;)LX/5Ru;

    move-result-object v1

    iget-object v2, p0, LX/BR7;->b:LX/BR8;

    iget-object v2, v2, LX/BR8;->b:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    iget-object v2, v2, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->h:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 1783212
    iput-object v2, v1, LX/5Ru;->d:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 1783213
    move-object v1, v1

    .line 1783214
    iput-object p1, v1, LX/5Ru;->a:Landroid/net/Uri;

    .line 1783215
    move-object v1, v1

    .line 1783216
    iget-object v2, p0, LX/BR7;->a:LX/5QV;

    invoke-interface {v2}, LX/5QV;->c()Ljava/lang/String;

    move-result-object v2

    .line 1783217
    iput-object v2, v1, LX/5Ru;->g:Ljava/lang/String;

    .line 1783218
    move-object v1, v1

    .line 1783219
    invoke-virtual {v1}, LX/5Ru;->a()Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;

    move-result-object v1

    .line 1783220
    iput-object v1, v0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->g:Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;

    .line 1783221
    iget-object v0, p0, LX/BR7;->b:LX/BR8;

    iget-object v0, v0, LX/BR8;->b:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    iget-object v1, p0, LX/BR7;->b:LX/BR8;

    iget-object v1, v1, LX/BR8;->a:LX/BRg;

    invoke-static {v0, v1}, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->a$redex0(Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;LX/BRg;)V

    .line 1783222
    return-void
.end method
