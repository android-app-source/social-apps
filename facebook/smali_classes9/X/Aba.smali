.class public final LX/Aba;
.super LX/63W;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/facecast/restriction/FacecastAudienceDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/facecast/restriction/FacecastAudienceDialogFragment;)V
    .locals 0

    .prologue
    .line 1690896
    iput-object p1, p0, LX/Aba;->a:Lcom/facebook/facecast/restriction/FacecastAudienceDialogFragment;

    invoke-direct {p0}, LX/63W;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V
    .locals 4

    .prologue
    .line 1690897
    iget-object v0, p0, LX/Aba;->a:Lcom/facebook/facecast/restriction/FacecastAudienceDialogFragment;

    iget-object v0, v0, Lcom/facebook/facecast/restriction/FacecastAudienceDialogFragment;->u:LX/AbS;

    if-eqz v0, :cond_0

    .line 1690898
    iget-object v0, p0, LX/Aba;->a:Lcom/facebook/facecast/restriction/FacecastAudienceDialogFragment;

    iget-object v0, v0, Lcom/facebook/facecast/restriction/FacecastAudienceDialogFragment;->u:LX/AbS;

    .line 1690899
    iget-object v1, v0, LX/AbS;->e:Lcom/facebook/facecast/restriction/FacecastAudienceDialogFragment;

    if-nez v1, :cond_1

    .line 1690900
    :cond_0
    :goto_0
    iget-object v0, p0, LX/Aba;->a:Lcom/facebook/facecast/restriction/FacecastAudienceDialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 1690901
    return-void

    .line 1690902
    :cond_1
    iget-object v1, v0, LX/AbS;->e:Lcom/facebook/facecast/restriction/FacecastAudienceDialogFragment;

    .line 1690903
    iget-object v2, v1, Lcom/facebook/facecast/restriction/FacecastAudienceDialogFragment;->o:Lcom/facebook/facecast/restriction/RestrictionSwitchView;

    invoke-virtual {v2}, Lcom/facebook/facecast/restriction/RestrictionSwitchView;->a()Z

    move-result v2

    if-nez v2, :cond_5

    .line 1690904
    const/4 v2, 0x0

    .line 1690905
    :goto_1
    move-object v1, v2

    .line 1690906
    iget-object v2, v0, LX/AbS;->f:Lcom/facebook/facecast/restriction/AudienceRestrictionData;

    if-nez v2, :cond_2

    if-eqz v1, :cond_0

    .line 1690907
    :cond_2
    iput-object v1, v0, LX/AbS;->f:Lcom/facebook/facecast/restriction/AudienceRestrictionData;

    .line 1690908
    iget-object v2, v0, LX/AbS;->d:Landroid/widget/TextView;

    if-nez v1, :cond_4

    const v1, 0x7f080bbc

    :goto_2
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(I)V

    .line 1690909
    iget-object v1, v0, LX/AbS;->f:Lcom/facebook/facecast/restriction/AudienceRestrictionData;

    if-nez v1, :cond_7

    .line 1690910
    const-string v1, "{}"

    .line 1690911
    :goto_3
    move-object v1, v1

    .line 1690912
    iget-object v2, v0, LX/AbS;->g:LX/AYP;

    if-eqz v2, :cond_3

    .line 1690913
    iget-object v2, v0, LX/AbS;->g:LX/AYP;

    invoke-interface {v2, v1}, LX/AYP;->a(Ljava/lang/String;)V

    .line 1690914
    :cond_3
    iget-object v2, v0, LX/AbS;->b:LX/AVT;

    const-string v3, "geotargeting_done_tapped"

    invoke-virtual {v2, v3, v1}, LX/AVT;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1690915
    :cond_4
    const v1, 0x7f080bbd

    goto :goto_2

    .line 1690916
    :cond_5
    new-instance v2, LX/AbU;

    invoke-direct {v2}, LX/AbU;-><init>()V

    move-object v2, v2

    .line 1690917
    iget-object v3, v1, Lcom/facebook/facecast/restriction/FacecastAudienceDialogFragment;->q:Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;

    .line 1690918
    iget p1, v3, Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;->c:I

    move v3, p1

    .line 1690919
    iput v3, v2, LX/AbU;->a:I

    .line 1690920
    move-object v2, v2

    .line 1690921
    iget-object v3, v1, Lcom/facebook/facecast/restriction/FacecastAudienceDialogFragment;->q:Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;

    .line 1690922
    iget p1, v3, Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;->d:I

    move v3, p1

    .line 1690923
    iput v3, v2, LX/AbU;->b:I

    .line 1690924
    move-object v2, v2

    .line 1690925
    sget-object v3, LX/Abb;->a:[I

    iget-object p1, v1, Lcom/facebook/facecast/restriction/FacecastAudienceDialogFragment;->r:Lcom/facebook/adinterfaces/ui/view/AdInterfacesGenderView;

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/ui/view/AdInterfacesGenderView;->getSelectedGender()LX/ACy;

    move-result-object p1

    invoke-virtual {p1}, LX/ACy;->ordinal()I

    move-result p1

    aget v3, v3, p1

    packed-switch v3, :pswitch_data_0

    .line 1690926
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;->ALL:Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;

    :goto_4
    move-object v3, v3

    .line 1690927
    iput-object v3, v2, LX/AbU;->c:Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;

    .line 1690928
    move-object v2, v2

    .line 1690929
    iget-object v3, v1, Lcom/facebook/facecast/restriction/FacecastAudienceDialogFragment;->t:Lcom/facebook/facecast/restriction/LocationControlView;

    invoke-virtual {v3}, Lcom/facebook/facecast/restriction/LocationControlView;->getIsInclude()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 1690930
    iget-object v3, v1, Lcom/facebook/facecast/restriction/FacecastAudienceDialogFragment;->n:LX/0Px;

    .line 1690931
    iput-object v3, v2, LX/AbU;->d:LX/0Px;

    .line 1690932
    :goto_5
    new-instance v3, Lcom/facebook/facecast/restriction/AudienceRestrictionData;

    invoke-direct {v3, v2}, Lcom/facebook/facecast/restriction/AudienceRestrictionData;-><init>(LX/AbU;)V

    move-object v2, v3

    .line 1690933
    goto :goto_1

    .line 1690934
    :cond_6
    iget-object v3, v1, Lcom/facebook/facecast/restriction/FacecastAudienceDialogFragment;->n:LX/0Px;

    .line 1690935
    iput-object v3, v2, LX/AbU;->e:LX/0Px;

    .line 1690936
    goto :goto_5

    .line 1690937
    :pswitch_0
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;->MALE:Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;

    goto :goto_4

    .line 1690938
    :pswitch_1
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;->FEMALE:Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;

    goto :goto_4

    .line 1690939
    :cond_7
    const-string v1, "{}"

    .line 1690940
    :try_start_0
    invoke-static {}, LX/0lB;->i()LX/0lB;

    move-result-object v2

    iget-object v3, v0, LX/AbS;->f:Lcom/facebook/facecast/restriction/AudienceRestrictionData;

    invoke-virtual {v2, v3}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch LX/28F; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_3

    .line 1690941
    :catch_0
    move-exception v2

    .line 1690942
    iget-object v3, v0, LX/AbS;->c:LX/03V;

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object p2, LX/AbS;->a:Ljava/lang/String;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p1

    const-string p2, "_json_processing_exception"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v3, p1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
