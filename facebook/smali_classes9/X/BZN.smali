.class public final enum LX/BZN;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/BZN;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/BZN;

.field public static final enum high_normal:LX/BZN;

.field public static final enum high_without_mouth:LX/BZN;

.field public static final enum index1:LX/BZN;

.field public static final enum index2:LX/BZN;

.field public static final enum index3:LX/BZN;

.field public static final enum index4:LX/BZN;

.field public static final enum low_normal:LX/BZN;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1797213
    new-instance v0, LX/BZN;

    const-string v1, "low_normal"

    invoke-direct {v0, v1, v3}, LX/BZN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BZN;->low_normal:LX/BZN;

    .line 1797214
    new-instance v0, LX/BZN;

    const-string v1, "index1"

    invoke-direct {v0, v1, v4}, LX/BZN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BZN;->index1:LX/BZN;

    .line 1797215
    new-instance v0, LX/BZN;

    const-string v1, "index2"

    invoke-direct {v0, v1, v5}, LX/BZN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BZN;->index2:LX/BZN;

    .line 1797216
    new-instance v0, LX/BZN;

    const-string v1, "index3"

    invoke-direct {v0, v1, v6}, LX/BZN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BZN;->index3:LX/BZN;

    .line 1797217
    new-instance v0, LX/BZN;

    const-string v1, "index4"

    invoke-direct {v0, v1, v7}, LX/BZN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BZN;->index4:LX/BZN;

    .line 1797218
    new-instance v0, LX/BZN;

    const-string v1, "high_normal"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/BZN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BZN;->high_normal:LX/BZN;

    .line 1797219
    new-instance v0, LX/BZN;

    const-string v1, "high_without_mouth"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/BZN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BZN;->high_without_mouth:LX/BZN;

    .line 1797220
    const/4 v0, 0x7

    new-array v0, v0, [LX/BZN;

    sget-object v1, LX/BZN;->low_normal:LX/BZN;

    aput-object v1, v0, v3

    sget-object v1, LX/BZN;->index1:LX/BZN;

    aput-object v1, v0, v4

    sget-object v1, LX/BZN;->index2:LX/BZN;

    aput-object v1, v0, v5

    sget-object v1, LX/BZN;->index3:LX/BZN;

    aput-object v1, v0, v6

    sget-object v1, LX/BZN;->index4:LX/BZN;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/BZN;->high_normal:LX/BZN;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/BZN;->high_without_mouth:LX/BZN;

    aput-object v2, v0, v1

    sput-object v0, LX/BZN;->$VALUES:[LX/BZN;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1797223
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/BZN;
    .locals 1

    .prologue
    .line 1797222
    const-class v0, LX/BZN;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/BZN;

    return-object v0
.end method

.method public static values()[LX/BZN;
    .locals 1

    .prologue
    .line 1797221
    sget-object v0, LX/BZN;->$VALUES:[LX/BZN;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/BZN;

    return-object v0
.end method
