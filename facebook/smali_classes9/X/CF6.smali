.class public final LX/CF6;
.super LX/AQ9;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "Lcom/facebook/ipc/composer/plugin/ComposerPluginDataProvider;",
        "DerivedData::",
        "Lcom/facebook/ipc/composer/plugin/ComposerPluginDerivedDataProvider;",
        "Mutation::",
        "Lcom/facebook/ipc/composer/plugin/ComposerPluginMutation",
        "<TMutation;>;>",
        "LX/AQ9",
        "<TModelData;TDerivedData;TMutation;>;"
    }
.end annotation


# instance fields
.field public final a:Lcom/facebook/goodwill/composer/GoodwillCampaignComposerPluginConfig;


# direct methods
.method public constructor <init>(LX/B5j;Lcom/facebook/goodwill/composer/GoodwillCampaignComposerPluginConfig;Landroid/content/Context;)V
    .locals 0
    .param p1    # LX/B5j;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/goodwill/composer/GoodwillCampaignComposerPluginConfig;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/B5j",
            "<TModelData;TDerivedData;TMutation;>;",
            "Lcom/facebook/goodwill/composer/GoodwillCampaignComposerPluginConfig;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1862418
    invoke-direct {p0, p3, p1}, LX/AQ9;-><init>(Landroid/content/Context;LX/B5j;)V

    .line 1862419
    iput-object p2, p0, LX/CF6;->a:Lcom/facebook/goodwill/composer/GoodwillCampaignComposerPluginConfig;

    .line 1862420
    return-void
.end method


# virtual methods
.method public final V()LX/ARN;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 1862404
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method

.method public final X()LX/ARN;
    .locals 1

    .prologue
    .line 1862417
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method

.method public final Y()LX/AQ4;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/AQ4",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1862416
    new-instance v0, LX/CF3;

    invoke-direct {v0, p0}, LX/CF3;-><init>(LX/CF6;)V

    return-object v0
.end method

.method public final Z()LX/ARN;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 1862415
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method

.method public final aB()LX/AQ4;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/AQ4",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1862414
    new-instance v0, LX/CF4;

    invoke-direct {v0, p0}, LX/CF4;-><init>(LX/CF6;)V

    return-object v0
.end method

.method public final aC()LX/ARN;
    .locals 1

    .prologue
    .line 1862413
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method

.method public final aI()LX/ARN;
    .locals 1

    .prologue
    .line 1862412
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method

.method public final aa()LX/ARN;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 1862421
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method

.method public final ac()LX/ARN;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 1862411
    sget-object v0, LX/ARN;->a:LX/ARN;

    return-object v0
.end method

.method public final ad()LX/ARN;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 1862410
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method

.method public final ae()LX/ARN;
    .locals 1

    .prologue
    .line 1862409
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method

.method public final al()LX/ARN;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 1862408
    sget-object v0, LX/ARN;->a:LX/ARN;

    return-object v0
.end method

.method public final an()LX/ARN;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1862407
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method

.method public final aq()LX/ARN;
    .locals 1

    .prologue
    .line 1862406
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method

.method public final i()LX/ARN;
    .locals 1

    .prologue
    .line 1862405
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method
