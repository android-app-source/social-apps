.class public final LX/BG2;
.super LX/46Z;
.source ""


# instance fields
.field public final synthetic a:LX/5fk;

.field public final synthetic b:Ljava/io/File;

.field public final synthetic c:Lcom/google/common/util/concurrent/SettableFuture;

.field public final synthetic d:Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;LX/5fk;Ljava/io/File;Lcom/google/common/util/concurrent/SettableFuture;)V
    .locals 0

    .prologue
    .line 1766512
    iput-object p1, p0, LX/BG2;->d:Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;

    iput-object p2, p0, LX/BG2;->a:LX/5fk;

    iput-object p3, p0, LX/BG2;->b:Ljava/io/File;

    iput-object p4, p0, LX/BG2;->c:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-direct {p0}, LX/46Z;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Bitmap;)V
    .locals 6
    .param p1    # Landroid/graphics/Bitmap;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1766513
    if-eqz p1, :cond_0

    .line 1766514
    iget-object v0, p0, LX/BG2;->d:Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;

    iget-object v1, p0, LX/BG2;->a:LX/5fk;

    iget-object v2, p0, LX/BG2;->d:Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;

    iget v3, v2, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->w:I

    iget-object v4, p0, LX/BG2;->b:Ljava/io/File;

    iget-object v5, p0, LX/BG2;->c:Lcom/google/common/util/concurrent/SettableFuture;

    move-object v2, p1

    .line 1766515
    invoke-static/range {v0 .. v5}, Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;->a$redex0(Lcom/facebook/photos/creativecam/ui/OpticCameraPreviewController;LX/5fk;Landroid/graphics/Bitmap;ILjava/io/File;Lcom/google/common/util/concurrent/SettableFuture;)V

    .line 1766516
    :goto_0
    return-void

    .line 1766517
    :cond_0
    iget-object v0, p0, LX/BG2;->c:Lcom/google/common/util/concurrent/SettableFuture;

    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Original bitmap from file was null"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    goto :goto_0
.end method

.method public final f(LX/1ca;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1766518
    iget-object v0, p0, LX/BG2;->c:Lcom/google/common/util/concurrent/SettableFuture;

    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Failed to load original image file"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    .line 1766519
    return-void
.end method
