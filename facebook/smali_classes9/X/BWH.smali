.class public LX/BWH;
.super LX/BW6;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/BWW;LX/BWV;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/44G;LX/03R;LX/48V;)V
    .locals 0

    .prologue
    .line 1791823
    invoke-direct/range {p0 .. p7}, LX/BW6;-><init>(Landroid/content/Context;LX/BWW;LX/BWV;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/44G;LX/03R;LX/48V;)V

    .line 1791824
    return-void
.end method


# virtual methods
.method public onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1791825
    invoke-super {p0, p1, p2}, LX/BW6;->onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 1791826
    instance-of v0, p1, Lcom/facebook/webview/FacebookWebView;

    if-nez v0, :cond_0

    .line 1791827
    :goto_0
    return-void

    .line 1791828
    :cond_0
    check-cast p1, Lcom/facebook/webview/FacebookWebView;

    .line 1791829
    const-string v0, "fbNativeReady"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Lcom/facebook/webview/FacebookWebView;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1791830
    invoke-super {p0, p1, p2, p3, p4}, LX/BW6;->onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V

    .line 1791831
    sget-boolean v0, LX/007;->i:Z

    move v0, v0

    .line 1791832
    if-eqz v0, :cond_0

    .line 1791833
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1791834
    const-string v1, "url "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " failed (code: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; description: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1791835
    :cond_0
    return-void
.end method

.method public shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1791836
    invoke-super {p0, p1, p2}, LX/BW6;->shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 1791837
    :goto_0
    return v0

    .line 1791838
    :cond_0
    instance-of v0, p1, Lcom/facebook/webview/FacebookWebView;

    if-nez v0, :cond_1

    move v0, v2

    .line 1791839
    goto :goto_0

    :cond_1
    move-object v0, p1

    .line 1791840
    check-cast v0, Lcom/facebook/webview/FacebookWebView;

    .line 1791841
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 1791842
    invoke-virtual {v3}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v4

    .line 1791843
    invoke-virtual {v0, v4}, Lcom/facebook/webview/FacebookWebView;->a(Ljava/lang/String;)LX/BWE;

    move-result-object v4

    .line 1791844
    if-eqz v4, :cond_2

    .line 1791845
    iget-object v0, p0, LX/BW6;->b:Landroid/content/Context;

    move-object v0, v0

    .line 1791846
    check-cast p1, Lcom/facebook/webview/FacebookWebView;

    invoke-interface {v4, v0, p1, v3}, LX/BWE;->a(Landroid/content/Context;Lcom/facebook/webview/FacebookWebView;Landroid/net/Uri;)V

    move v0, v1

    .line 1791847
    goto :goto_0

    .line 1791848
    :cond_2
    iget-object v4, v0, Lcom/facebook/webview/FacebookWebView;->l:LX/BWU;

    move-object v0, v4

    .line 1791849
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1791850
    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1791851
    invoke-virtual {p1}, Landroid/webkit/WebView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1}, Landroid/webkit/WebView;->getUrl()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, v4, p0, p2}, LX/BWU;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v4

    .line 1791852
    if-nez v4, :cond_4

    .line 1791853
    const/4 v4, 0x0

    .line 1791854
    :goto_1
    move v0, v4

    .line 1791855
    if-eqz v0, :cond_3

    move v0, v1

    .line 1791856
    goto :goto_0

    :cond_3
    move v0, v2

    .line 1791857
    goto :goto_0

    .line 1791858
    :cond_4
    iget-object p0, v0, LX/BWU;->a:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p1}, Landroid/webkit/WebView;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-interface {p0, v4, p2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1791859
    const/4 v4, 0x1

    goto :goto_1
.end method
