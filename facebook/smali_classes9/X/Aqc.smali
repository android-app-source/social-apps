.class public LX/Aqc;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0lB;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1718091
    const-class v0, LX/Aqc;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Aqc;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0lB;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0lB;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1718092
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1718093
    iput-object p1, p0, LX/Aqc;->b:LX/0Ot;

    .line 1718094
    iput-object p2, p0, LX/Aqc;->c:LX/0lB;

    .line 1718095
    return-void
.end method

.method private b(Ljava/lang/String;)LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/friendsharing/gif/model/GifModelContainer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1718096
    :try_start_0
    iget-object v0, p0, LX/Aqc;->c:LX/0lB;

    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/facebook/friendsharing/gif/model/GifList;

    invoke-virtual {v0, v1, v2}, LX/0lC;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/gif/model/GifList;

    .line 1718097
    invoke-virtual {v0}, Lcom/facebook/friendsharing/gif/model/GifList;->a()LX/0Px;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 1718098
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 1718099
    iget-object v0, p0, LX/Aqc;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v2, LX/Aqc;->a:Ljava/lang/String;

    const-string v3, "Error while parsing JSON"

    invoke-virtual {v0, v2, v3, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1718100
    throw v1
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/0Px;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/friendsharing/gif/model/GifModelContainer;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1718101
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 1718102
    :try_start_0
    new-instance v1, Ljava/net/URL;

    invoke-direct {v1, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 1718103
    new-instance v3, Ljava/io/InputStreamReader;

    invoke-virtual {v1}, Ljava/net/URL;->openStream()Ljava/io/InputStream;

    move-result-object v1

    invoke-direct {v3, v1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_1

    .line 1718104
    :try_start_1
    new-instance v1, Ljava/io/BufferedReader;

    invoke-direct {v1, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_1
    .catch Ljava/net/UnknownHostException; {:try_start_1 .. :try_end_1} :catch_2

    .line 1718105
    :goto_0
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 1718106
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_2
    .catch Ljava/net/UnknownHostException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 1718107
    :catch_0
    move-exception v0

    move-object v2, v3

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    .line 1718108
    :goto_1
    if-eqz v0, :cond_0

    .line 1718109
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V

    .line 1718110
    :cond_0
    if-eqz v2, :cond_1

    .line 1718111
    invoke-virtual {v2}, Ljava/io/InputStreamReader;->close()V

    .line 1718112
    :cond_1
    iget-object v0, p0, LX/Aqc;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v2, LX/Aqc;->a:Ljava/lang/String;

    const-string v3, "Error while fetching GIFs"

    invoke-virtual {v0, v2, v3, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1718113
    throw v1

    .line 1718114
    :cond_2
    :try_start_3
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V

    .line 1718115
    invoke-virtual {v3}, Ljava/io/InputStreamReader;->close()V

    .line 1718116
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, LX/Aqc;->b(Ljava/lang/String;)LX/0Px;
    :try_end_3
    .catch Ljava/net/UnknownHostException; {:try_start_3 .. :try_end_3} :catch_0

    move-result-object v0

    return-object v0

    .line 1718117
    :catch_1
    move-exception v0

    move-object v1, v0

    move-object v0, v2

    goto :goto_1

    :catch_2
    move-exception v0

    move-object v1, v0

    move-object v0, v2

    move-object v2, v3

    goto :goto_1
.end method
