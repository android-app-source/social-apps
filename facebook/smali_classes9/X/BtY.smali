.class public final LX/BtY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;)V
    .locals 0

    .prologue
    .line 1829322
    iput-object p1, p0, LX/BtY;->a:Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 1829309
    iget-object v1, p0, LX/BtY;->a:Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;

    iget-object v0, p0, LX/BtY;->a:Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;

    iget-object v0, v0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->V:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v2

    .line 1829310
    iput-wide v2, v1, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->aB:J

    .line 1829311
    iget-object v0, p0, LX/BtY;->a:Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;

    iget-object v0, v0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->v:LX/BFQ;

    invoke-virtual {v0}, LX/BFQ;->c()V

    .line 1829312
    iget-object v0, p0, LX/BtY;->a:Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;

    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->ds_()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1829313
    :cond_0
    :goto_0
    return-void

    .line 1829314
    :cond_1
    iget-object v0, p0, LX/BtY;->a:Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;

    const v1, 0x7f08006f

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1829315
    iget-object v1, p0, LX/BtY;->a:Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;

    iget-object v1, v1, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->am:LX/62O;

    iget-object v2, p0, LX/BtY;->a:Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;

    iget-object v2, v2, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ak:LX/1DI;

    invoke-virtual {v1, v0, v2}, LX/62O;->a(Ljava/lang/String;LX/1DI;)V

    .line 1829316
    iget-object v0, p0, LX/BtY;->a:Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;

    iget-object v0, v0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->Z:Lcom/facebook/widget/FbSwipeRefreshLayout;

    if-eqz v0, :cond_2

    .line 1829317
    iget-object v0, p0, LX/BtY;->a:Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;

    iget-object v0, v0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->Z:Lcom/facebook/widget/FbSwipeRefreshLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 1829318
    :cond_2
    iget-object v0, p0, LX/BtY;->a:Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;

    iget-boolean v0, v0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->an:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/BtY;->a:Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;

    iget-object v0, v0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->D:LX/1ry;

    invoke-virtual {v0}, LX/1ry;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1829319
    iget-object v0, p0, LX/BtY;->a:Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;

    iget-object v0, v0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ad:LX/9DG;

    iget-object v1, p0, LX/BtY;->a:Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;

    iget-object v1, v1, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ah:Lcom/facebook/permalink/PermalinkParams;

    .line 1829320
    iget-object v2, v1, Lcom/facebook/permalink/PermalinkParams;->f:Ljava/lang/String;

    move-object v1, v2

    .line 1829321
    invoke-virtual {v0, v1}, LX/9DG;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 6
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1829323
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1829324
    iget-object v0, p0, LX/BtY;->a:Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;

    .line 1829325
    if-nez p1, :cond_4

    .line 1829326
    :cond_0
    :goto_0
    iget-object v1, p0, LX/BtY;->a:Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;

    iget-object v0, p0, LX/BtY;->a:Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;

    iget-object v0, v0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->V:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v2

    .line 1829327
    iput-wide v2, v1, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->aB:J

    .line 1829328
    if-eqz p1, :cond_1

    invoke-static {p1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    invoke-static {v0}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v0

    .line 1829329
    :goto_1
    iget-object v1, p0, LX/BtY;->a:Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;

    invoke-static {v1, p1}, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->a(Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    invoke-static {v1}, LX/16z;->f(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    .line 1829330
    invoke-static {v1}, LX/21y;->getOrder(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/21y;

    move-result-object v2

    sget-object v3, LX/21y;->RANKED_ORDER:LX/21y;

    invoke-virtual {v2, v3}, LX/21y;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, LX/BtY;->a:Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;

    iget-object v2, v2, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->S:LX/0tF;

    invoke-virtual {v2}, LX/0tF;->b()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1829331
    iget-object v2, p0, LX/BtY;->a:Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;

    iget-object v2, v2, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->J:LX/3H7;

    invoke-virtual {v2, v1}, LX/3H7;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    new-instance v2, LX/BtX;

    invoke-direct {v2, p0, p1, v0}, LX/BtX;-><init>(LX/BtY;Lcom/facebook/graphql/model/GraphQLStory;LX/162;)V

    iget-object v0, p0, LX/BtY;->a:Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;

    iget-object v0, v0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->K:Ljava/util/concurrent/Executor;

    invoke-static {v1, v2, v0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1829332
    :goto_2
    return-void

    .line 1829333
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 1829334
    :cond_2
    iget-object v1, p0, LX/BtY;->a:Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;

    invoke-virtual {v1, p1, v0}, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->a(Lcom/facebook/graphql/model/FeedUnit;LX/162;)V

    .line 1829335
    iget-object v0, p0, LX/BtY;->a:Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;

    iget-object v0, v0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ah:Lcom/facebook/permalink/PermalinkParams;

    .line 1829336
    iget-object v1, v0, Lcom/facebook/permalink/PermalinkParams;->a:LX/89m;

    move-object v0, v1

    .line 1829337
    invoke-virtual {v0}, LX/89m;->isJsonType()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1829338
    iget-object v0, p0, LX/BtY;->a:Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;

    invoke-static {v0}, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->y(Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;)V

    .line 1829339
    :cond_3
    iget-object v0, p0, LX/BtY;->a:Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;

    iget-object v0, v0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ad:LX/9DG;

    iget-object v1, p0, LX/BtY;->a:Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;

    iget-object v1, v1, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ah:Lcom/facebook/permalink/PermalinkParams;

    .line 1829340
    iget-object v2, v1, Lcom/facebook/permalink/PermalinkParams;->f:Ljava/lang/String;

    move-object v1, v2

    .line 1829341
    invoke-virtual {v0, v1}, LX/9DG;->a(Ljava/lang/String;)V

    goto :goto_2

    .line 1829342
    :cond_4
    iget-object v1, v0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->N:LX/3Cm;

    invoke-virtual {v1, p1}, LX/3Cm;->b(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v2

    .line 1829343
    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1829344
    iget-object v1, v0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->O:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/03V;

    const-class v3, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Redirecting to fallback URL for notification story "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1829345
    iget-object v1, v0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->P:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/17W;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v1, v3, v2}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 1829346
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto/16 :goto_0
.end method
