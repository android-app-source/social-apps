.class public final LX/Af6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:LX/Aeu;

.field public final synthetic b:LX/AfD;


# direct methods
.method public constructor <init>(LX/AfD;LX/Aeu;)V
    .locals 0

    .prologue
    .line 1698678
    iput-object p1, p0, LX/Af6;->b:LX/AfD;

    iput-object p2, p0, LX/Af6;->a:LX/Aeu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 9

    .prologue
    .line 1698670
    iget-object v0, p0, LX/Af6;->b:LX/AfD;

    iget-object v0, v0, LX/AfD;->j:LX/Aev;

    iget-object v1, p0, LX/Af6;->a:LX/Aeu;

    iget-object v1, v1, LX/AeP;->a:LX/AcC;

    iget-object v1, v1, LX/AcC;->b:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, LX/Aev;->a(Ljava/lang/String;Z)V

    .line 1698671
    iget-object v0, p0, LX/Af6;->b:LX/AfD;

    iget-object v1, p0, LX/Af6;->a:LX/Aeu;

    iget-object v1, v1, LX/AeP;->a:LX/AcC;

    iget-object v1, v1, LX/AcC;->b:Ljava/lang/String;

    .line 1698672
    new-instance v3, Lcom/facebook/friends/methods/BlockUserMethod$Params;

    iget-object v4, v0, LX/AfD;->c:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v5

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-direct {v3, v5, v6, v7, v8}, Lcom/facebook/friends/methods/BlockUserMethod$Params;-><init>(JJ)V

    .line 1698673
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 1698674
    const-string v5, "blockUser"

    invoke-virtual {v4, v5, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1698675
    iget-object v3, v0, LX/AfD;->b:LX/0aG;

    const-string v5, "friending_block_user"

    const v6, 0x5ed83b7b

    invoke-static {v3, v5, v4, v6}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v3

    invoke-interface {v3}, LX/1MF;->start()LX/1ML;

    move-result-object v3

    .line 1698676
    new-instance v4, LX/AfC;

    invoke-direct {v4, v0, v1}, LX/AfC;-><init>(LX/AfD;Ljava/lang/String;)V

    iget-object v5, v0, LX/AfD;->e:Ljava/util/concurrent/Executor;

    invoke-static {v3, v4, v5}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1698677
    const/4 v0, 0x0

    return v0
.end method
