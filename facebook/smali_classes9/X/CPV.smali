.class public final LX/CPV;
.super LX/1dc;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1dc",
        "<",
        "Landroid/graphics/drawable/Drawable;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Landroid/graphics/drawable/GradientDrawable$Orientation;

.field public b:[I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1884612
    invoke-static {}, LX/CPW;->a()LX/CPW;

    move-result-object v0

    invoke-direct {p0, v0}, LX/1dc;-><init>(LX/1n4;)V

    .line 1884613
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1884601
    const-string v0, "NTGradientDrawableReference"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1884602
    if-ne p0, p1, :cond_1

    .line 1884603
    :cond_0
    :goto_0
    return v0

    .line 1884604
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1884605
    goto :goto_0

    .line 1884606
    :cond_3
    check-cast p1, LX/CPV;

    .line 1884607
    iget-object v2, p0, LX/CPV;->a:Landroid/graphics/drawable/GradientDrawable$Orientation;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/CPV;->a:Landroid/graphics/drawable/GradientDrawable$Orientation;

    iget-object v3, p1, LX/CPV;->a:Landroid/graphics/drawable/GradientDrawable$Orientation;

    invoke-virtual {v2, v3}, Landroid/graphics/drawable/GradientDrawable$Orientation;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1884608
    goto :goto_0

    .line 1884609
    :cond_5
    iget-object v2, p1, LX/CPV;->a:Landroid/graphics/drawable/GradientDrawable$Orientation;

    if-nez v2, :cond_4

    .line 1884610
    :cond_6
    iget-object v2, p0, LX/CPV;->b:[I

    iget-object v3, p1, LX/CPV;->b:[I

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1884611
    goto :goto_0
.end method
