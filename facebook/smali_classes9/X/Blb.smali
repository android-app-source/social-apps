.class public final enum LX/Blb;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Blb;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Blb;

.field public static final enum CONTACTS:LX/Blb;

.field public static final enum FACEBOOK:LX/Blb;


# instance fields
.field public final resourceId:I

.field private final serializedValue:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1816272
    new-instance v0, LX/Blb;

    const-string v1, "FACEBOOK"

    const v2, 0x7f081eae

    const-string v3, "facebook"

    invoke-direct {v0, v1, v4, v2, v3}, LX/Blb;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, LX/Blb;->FACEBOOK:LX/Blb;

    .line 1816273
    new-instance v0, LX/Blb;

    const-string v1, "CONTACTS"

    const v2, 0x7f081eaf

    const-string v3, "contacts"

    invoke-direct {v0, v1, v5, v2, v3}, LX/Blb;-><init>(Ljava/lang/String;IILjava/lang/String;)V

    sput-object v0, LX/Blb;->CONTACTS:LX/Blb;

    .line 1816274
    const/4 v0, 0x2

    new-array v0, v0, [LX/Blb;

    sget-object v1, LX/Blb;->FACEBOOK:LX/Blb;

    aput-object v1, v0, v4

    sget-object v1, LX/Blb;->CONTACTS:LX/Blb;

    aput-object v1, v0, v5

    sput-object v0, LX/Blb;->$VALUES:[LX/Blb;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1816275
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1816276
    iput p3, p0, LX/Blb;->resourceId:I

    .line 1816277
    iput-object p4, p0, LX/Blb;->serializedValue:Ljava/lang/String;

    .line 1816278
    return-void
.end method

.method public static fromString(Ljava/lang/String;)LX/Blb;
    .locals 5

    .prologue
    .line 1816279
    invoke-static {}, LX/Blb;->values()[LX/Blb;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 1816280
    iget-object v4, v0, LX/Blb;->serializedValue:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1816281
    :goto_1
    return-object v0

    .line 1816282
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1816283
    :cond_1
    sget-object v0, LX/Blb;->FACEBOOK:LX/Blb;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)LX/Blb;
    .locals 1

    .prologue
    .line 1816284
    const-class v0, LX/Blb;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Blb;

    return-object v0
.end method

.method public static values()[LX/Blb;
    .locals 1

    .prologue
    .line 1816285
    sget-object v0, LX/Blb;->$VALUES:[LX/Blb;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Blb;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1816286
    iget-object v0, p0, LX/Blb;->serializedValue:Ljava/lang/String;

    return-object v0
.end method
