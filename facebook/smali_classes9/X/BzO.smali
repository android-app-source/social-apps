.class public LX/BzO;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/BzP;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1838673
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/BzO;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/BzP;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1838674
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1838675
    iput-object p1, p0, LX/BzO;->b:LX/0Ot;

    .line 1838676
    return-void
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 1838677
    check-cast p2, LX/BzN;

    .line 1838678
    iget-object v0, p0, LX/BzO;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BzP;

    iget-object v1, p2, LX/BzN;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/BzN;->b:Ljava/lang/Boolean;

    const/4 v4, 0x0

    .line 1838679
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 1838680
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v5, v0, LX/BzP;->b:LX/2y3;

    .line 1838681
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 1838682
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v3

    invoke-virtual {v5, v3}, LX/2y3;->a(Lcom/facebook/graphql/model/GraphQLMedia;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1838683
    :cond_0
    const/4 v3, 0x0

    .line 1838684
    :goto_0
    move-object v0, v3

    .line 1838685
    return-object v0

    :cond_1
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    iget-object v3, v0, LX/BzP;->c:LX/Ap5;

    invoke-virtual {v3, p1}, LX/Ap5;->c(LX/1De;)LX/Ap4;

    move-result-object v3

    invoke-virtual {v3, v4}, LX/Ap4;->h(I)LX/Ap4;

    move-result-object p0

    .line 1838686
    iget-object v3, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 1838687
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1838688
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object p2

    const p1, 0x3ff745d1

    iget-object v1, v0, LX/BzP;->a:LX/1qb;

    invoke-static {v1}, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentCoverPhotoPartDefinition;->a(LX/1qb;)LX/2yA;

    move-result-object v1

    invoke-static {p2, p1, v1}, Lcom/facebook/feedplugins/attachments/linkshare/BaseAttachmentCoverPhotoPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLMedia;FLX/2yA;)Ljava/lang/String;

    move-result-object p2

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p2

    move-object v3, p2

    .line 1838689
    invoke-virtual {p0, v3}, LX/Ap4;->a(Landroid/net/Uri;)LX/Ap4;

    move-result-object p0

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x1

    :goto_1
    invoke-virtual {p0, v3}, LX/Ap4;->i(I)LX/Ap4;

    move-result-object v3

    invoke-interface {v5, v3}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    goto :goto_0

    :cond_2
    move v3, v4

    goto :goto_1
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1838690
    invoke-static {}, LX/1dS;->b()V

    .line 1838691
    const/4 v0, 0x0

    return-object v0
.end method
