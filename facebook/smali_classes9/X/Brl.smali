.class public LX/Brl;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Brj;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile c:LX/Brl;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Brm;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1826556
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/Brl;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/Brm;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1826557
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1826558
    iput-object p1, p0, LX/Brl;->b:LX/0Ot;

    .line 1826559
    return-void
.end method

.method public static a(LX/0QB;)LX/Brl;
    .locals 4

    .prologue
    .line 1826560
    sget-object v0, LX/Brl;->c:LX/Brl;

    if-nez v0, :cond_1

    .line 1826561
    const-class v1, LX/Brl;

    monitor-enter v1

    .line 1826562
    :try_start_0
    sget-object v0, LX/Brl;->c:LX/Brl;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1826563
    if-eqz v2, :cond_0

    .line 1826564
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1826565
    new-instance v3, LX/Brl;

    const/16 p0, 0x1d1c

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/Brl;-><init>(LX/0Ot;)V

    .line 1826566
    move-object v0, v3

    .line 1826567
    sput-object v0, LX/Brl;->c:LX/Brl;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1826568
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1826569
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1826570
    :cond_1
    sget-object v0, LX/Brl;->c:LX/Brl;

    return-object v0

    .line 1826571
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1826572
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 1

    .prologue
    .line 1826573
    iget-object v0, p0, LX/Brl;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    .line 1826574
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    const/4 p0, 0x2

    invoke-interface {v0, p0}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v0

    const/4 p0, 0x1

    const/16 p2, 0xc

    invoke-interface {v0, p0, p2}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v0

    const/4 p0, 0x3

    const/16 p2, 0x12

    invoke-interface {v0, p0, p2}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v0

    const p0, 0x7f0a0168

    invoke-interface {v0, p0}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v0

    invoke-static {p1}, LX/5Jt;->c(LX/1De;)LX/5Jr;

    move-result-object p0

    invoke-virtual {p0}, LX/1X5;->c()LX/1Di;

    move-result-object p0

    const/16 p2, 0x18

    invoke-interface {p0, p2}, LX/1Di;->r(I)LX/1Di;

    move-result-object p0

    invoke-interface {v0, p0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    move-object v0, v0

    .line 1826575
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1826576
    invoke-static {}, LX/1dS;->b()V

    .line 1826577
    const/4 v0, 0x0

    return-object v0
.end method
