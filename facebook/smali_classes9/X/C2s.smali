.class public LX/C2s;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/C2r;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/C2t;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1844708
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/C2s;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/C2t;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1844709
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1844710
    iput-object p1, p0, LX/C2s;->b:LX/0Ot;

    .line 1844711
    return-void
.end method

.method public static a(LX/0QB;)LX/C2s;
    .locals 4

    .prologue
    .line 1844712
    const-class v1, LX/C2s;

    monitor-enter v1

    .line 1844713
    :try_start_0
    sget-object v0, LX/C2s;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1844714
    sput-object v2, LX/C2s;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1844715
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1844716
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1844717
    new-instance v3, LX/C2s;

    const/16 p0, 0x1eb2

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/C2s;-><init>(LX/0Ot;)V

    .line 1844718
    move-object v0, v3

    .line 1844719
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1844720
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C2s;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1844721
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1844722
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1844723
    const v0, 0x77397329

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 7

    .prologue
    .line 1844724
    check-cast p2, Lcom/facebook/feedplugins/commerce/SingleProductImageComponent$SingleProductImageComponentImpl;

    .line 1844725
    iget-object v0, p0, LX/C2s;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/C2t;

    iget v2, p2, Lcom/facebook/feedplugins/commerce/SingleProductImageComponent$SingleProductImageComponentImpl;->a:I

    iget-object v3, p2, Lcom/facebook/feedplugins/commerce/SingleProductImageComponent$SingleProductImageComponentImpl;->b:Ljava/lang/String;

    iget-object v4, p2, Lcom/facebook/feedplugins/commerce/SingleProductImageComponent$SingleProductImageComponentImpl;->c:Lcom/facebook/common/callercontext/CallerContext;

    iget-boolean v5, p2, Lcom/facebook/feedplugins/commerce/SingleProductImageComponent$SingleProductImageComponentImpl;->d:Z

    iget-boolean v6, p2, Lcom/facebook/feedplugins/commerce/SingleProductImageComponent$SingleProductImageComponentImpl;->e:Z

    move-object v1, p1

    .line 1844726
    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object p1

    const/4 p2, 0x0

    if-eqz v5, :cond_0

    const p0, 0x7f0b010f

    :goto_0
    invoke-interface {p1, p2, p0}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object p1

    const/4 p2, 0x2

    if-eqz v6, :cond_1

    const p0, 0x7f0b010f

    :goto_1
    invoke-interface {p1, p2, p0}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object p1

    invoke-static {v1}, LX/1um;->c(LX/1De;)LX/1up;

    move-result-object p0

    const/high16 p2, 0x3f800000    # 1.0f

    invoke-virtual {p0, p2}, LX/1up;->c(F)LX/1up;

    move-result-object p2

    iget-object p0, v0, LX/C2t;->a:LX/0Or;

    invoke-interface {p0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/1Ad;

    invoke-virtual {p0, v3}, LX/1Ad;->b(Ljava/lang/String;)LX/1Ad;

    move-result-object p0

    invoke-virtual {p0, v4}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object p0

    invoke-virtual {p0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object p0

    invoke-virtual {p2, p0}, LX/1up;->a(LX/1aZ;)LX/1up;

    move-result-object p0

    invoke-virtual {p0}, LX/1X5;->c()LX/1Di;

    move-result-object p0

    invoke-interface {p0, v2}, LX/1Di;->o(I)LX/1Di;

    move-result-object p0

    .line 1844727
    const p2, 0x77397329

    const/4 v0, 0x0

    invoke-static {v1, p2, v0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p2

    move-object p2, p2

    .line 1844728
    invoke-interface {p0, p2}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object p0

    invoke-interface {p1, p0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object p0

    invoke-interface {p0}, LX/1Di;->k()LX/1Dg;

    move-result-object p0

    move-object v0, p0

    .line 1844729
    return-object v0

    :cond_0
    const p0, 0x7f0b08b5

    goto :goto_0

    :cond_1
    const p0, 0x7f0b08b5

    goto :goto_1
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1844730
    invoke-static {}, LX/1dS;->b()V

    .line 1844731
    iget v0, p1, LX/1dQ;->b:I

    .line 1844732
    packed-switch v0, :pswitch_data_0

    .line 1844733
    :goto_0
    return-object v2

    .line 1844734
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 1844735
    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1844736
    check-cast v1, Lcom/facebook/feedplugins/commerce/SingleProductImageComponent$SingleProductImageComponentImpl;

    .line 1844737
    iget-object p1, p0, LX/C2s;->b:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object p1, v1, Lcom/facebook/feedplugins/commerce/SingleProductImageComponent$SingleProductImageComponentImpl;->f:LX/C2l;

    .line 1844738
    if-eqz p1, :cond_0

    .line 1844739
    invoke-virtual {p1}, LX/C2l;->onClick()V

    .line 1844740
    :cond_0
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x77397329
        :pswitch_0
    .end packed-switch
.end method
