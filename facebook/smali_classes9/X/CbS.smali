.class public LX/CbS;
.super LX/3Ag;
.source ""


# instance fields
.field public c:LX/3iT;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/Bc2;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final e:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

.field public final f:Landroid/content/Context;

.field public g:Ljava/lang/String;

.field public h:Lcom/google/common/util/concurrent/SettableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/SettableFuture",
            "<",
            "LX/175;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1919520
    invoke-direct {p0, p1}, LX/3Ag;-><init>(Landroid/content/Context;)V

    .line 1919521
    const-class v0, LX/CbS;

    invoke-static {v0, p0}, LX/CbS;->a(Ljava/lang/Class;Landroid/app/Dialog;)V

    .line 1919522
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/CbS;->requestWindowFeature(I)Z

    .line 1919523
    const v0, 0x7f030446

    invoke-virtual {p0, v0}, LX/CbS;->setContentView(I)V

    .line 1919524
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/CbS;->setCancelable(Z)V

    .line 1919525
    iput-object p1, p0, LX/CbS;->f:Landroid/content/Context;

    .line 1919526
    const v0, 0x7f0d0bd4

    invoke-virtual {p0, v0}, LX/CbS;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    iput-object v0, p0, LX/CbS;->e:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    .line 1919527
    const v0, 0x7f0d088b

    invoke-virtual {p0, v0}, LX/CbS;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1919528
    new-instance v1, LX/CbO;

    invoke-direct {v1, p0}, LX/CbO;-><init>(LX/CbS;)V

    move-object v1, v1

    .line 1919529
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1919530
    const v0, 0x7f0d0d01

    invoke-virtual {p0, v0}, LX/CbS;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1919531
    new-instance v1, LX/CbP;

    invoke-direct {v1, p0}, LX/CbP;-><init>(LX/CbS;)V

    move-object v1, v1

    .line 1919532
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1919533
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/app/Dialog;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/app/Dialog;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/CbS;

    invoke-static {p0}, LX/3iT;->b(LX/0QB;)LX/3iT;

    move-result-object v1

    check-cast v1, LX/3iT;

    invoke-static {p0}, LX/Bc2;->a(LX/0QB;)LX/Bc2;

    move-result-object p0

    check-cast p0, LX/Bc2;

    iput-object v1, p1, LX/CbS;->c:LX/3iT;

    iput-object p0, p1, LX/CbS;->d:LX/Bc2;

    return-void
.end method

.method public static c(LX/CbS;)Z
    .locals 2

    .prologue
    .line 1919534
    iget-object v0, p0, LX/CbS;->e:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->getEncodedText()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/CbS;->g:Ljava/lang/String;

    invoke-static {v0, v1}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final onBackPressed()V
    .locals 3

    .prologue
    .line 1919535
    invoke-static {p0}, LX/CbS;->c(LX/CbS;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1919536
    invoke-virtual {p0}, LX/CbS;->dismiss()V

    .line 1919537
    :goto_0
    return-void

    .line 1919538
    :cond_0
    iget-object v0, p0, LX/CbS;->f:Landroid/content/Context;

    instance-of v0, v0, Landroid/app/Activity;

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1919539
    iget-object v0, p0, LX/CbS;->f:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-static {v0}, LX/Bc2;->a(Landroid/app/Activity;)LX/31Y;

    move-result-object v0

    .line 1919540
    const v1, 0x7f0819b3

    invoke-virtual {v0, v1}, LX/0ju;->a(I)LX/0ju;

    .line 1919541
    const v1, 0x7f0819b4

    invoke-virtual {v0, v1}, LX/0ju;->b(I)LX/0ju;

    .line 1919542
    const v1, 0x7f0819b5

    new-instance v2, LX/CbQ;

    invoke-direct {v2, p0}, LX/CbQ;-><init>(LX/CbS;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 1919543
    const v1, 0x7f0819b6

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 1919544
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/0ju;->a(Z)LX/0ju;

    .line 1919545
    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    .line 1919546
    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 1919547
    goto :goto_0
.end method

.method public final onStop()V
    .locals 2

    .prologue
    .line 1919548
    invoke-super {p0}, LX/3Ag;->onStop()V

    .line 1919549
    iget-object v0, p0, LX/CbS;->h:Lcom/google/common/util/concurrent/SettableFuture;

    new-instance v1, Ljava/util/concurrent/CancellationException;

    invoke-direct {v1}, Ljava/util/concurrent/CancellationException;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    .line 1919550
    return-void
.end method
