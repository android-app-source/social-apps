.class public LX/Bf7;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/Bf7;


# instance fields
.field public final a:LX/Bf4;

.field public final b:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/Bf4;LX/1Ck;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1805728
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1805729
    iput-object p1, p0, LX/Bf7;->a:LX/Bf4;

    .line 1805730
    iput-object p2, p0, LX/Bf7;->b:LX/1Ck;

    .line 1805731
    return-void
.end method

.method public static a(LX/0QB;)LX/Bf7;
    .locals 6

    .prologue
    .line 1805732
    sget-object v0, LX/Bf7;->c:LX/Bf7;

    if-nez v0, :cond_1

    .line 1805733
    const-class v1, LX/Bf7;

    monitor-enter v1

    .line 1805734
    :try_start_0
    sget-object v0, LX/Bf7;->c:LX/Bf7;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1805735
    if-eqz v2, :cond_0

    .line 1805736
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1805737
    new-instance v5, LX/Bf7;

    .line 1805738
    new-instance p0, LX/Bf4;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v4

    check-cast v4, LX/0TD;

    invoke-direct {p0, v3, v4}, LX/Bf4;-><init>(LX/0tX;LX/0TD;)V

    .line 1805739
    move-object v3, p0

    .line 1805740
    check-cast v3, LX/Bf4;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v4

    check-cast v4, LX/1Ck;

    invoke-direct {v5, v3, v4}, LX/Bf7;-><init>(LX/Bf4;LX/1Ck;)V

    .line 1805741
    move-object v0, v5

    .line 1805742
    sput-object v0, LX/Bf7;->c:LX/Bf7;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1805743
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1805744
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1805745
    :cond_1
    sget-object v0, LX/Bf7;->c:LX/Bf7;

    return-object v0

    .line 1805746
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1805747
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
