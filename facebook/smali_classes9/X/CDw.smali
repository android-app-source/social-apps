.class public final LX/CDw;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/CDx;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:LX/2pa;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1860305
    invoke-static {}, LX/CDx;->q()LX/CDx;

    move-result-object v0

    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1860306
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1860284
    const-string v0, "FigProfileVideoComponent"

    return-object v0
.end method

.method public final a(LX/1X1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1",
            "<",
            "LX/CDx;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1860302
    check-cast p1, LX/CDw;

    .line 1860303
    iget-object v0, p1, LX/CDw;->c:LX/2pa;

    iput-object v0, p0, LX/CDw;->c:LX/2pa;

    .line 1860304
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1860288
    if-ne p0, p1, :cond_1

    .line 1860289
    :cond_0
    :goto_0
    return v0

    .line 1860290
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1860291
    goto :goto_0

    .line 1860292
    :cond_3
    check-cast p1, LX/CDw;

    .line 1860293
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1860294
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1860295
    if-eq v2, v3, :cond_0

    .line 1860296
    iget-object v2, p0, LX/CDw;->a:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/CDw;->a:Ljava/lang/String;

    iget-object v3, p1, LX/CDw;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1860297
    goto :goto_0

    .line 1860298
    :cond_5
    iget-object v2, p1, LX/CDw;->a:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 1860299
    :cond_6
    iget-object v2, p0, LX/CDw;->b:Ljava/lang/String;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/CDw;->b:Ljava/lang/String;

    iget-object v3, p1, LX/CDw;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1860300
    goto :goto_0

    .line 1860301
    :cond_7
    iget-object v2, p1, LX/CDw;->b:Ljava/lang/String;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public final g()LX/1X1;
    .locals 2

    .prologue
    .line 1860285
    invoke-super {p0}, LX/1X1;->g()LX/1X1;

    move-result-object v0

    check-cast v0, LX/CDw;

    .line 1860286
    const/4 v1, 0x0

    iput-object v1, v0, LX/CDw;->c:LX/2pa;

    .line 1860287
    return-object v0
.end method
