.class public abstract LX/AlU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/24J;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<",
        "L:Lcom/facebook/feed/photoreminder/common/ScrollingImagePromptViewAnimationBuilder$BaseTrayAnimatorListener;",
        "V:",
        "Landroid/view/View;",
        ":",
        "LX/AkB;",
        ">",
        "Ljava/lang/Object;",
        "LX/24J",
        "<",
        "LX/24P;",
        "TV;>;"
    }
.end annotation


# instance fields
.field public final a:LX/0hB;

.field private final b:LX/1Rg;

.field private final c:I


# direct methods
.method public constructor <init>(LX/0hB;LX/1Rg;I)V
    .locals 0

    .prologue
    .line 1709745
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1709746
    iput-object p1, p0, LX/AlU;->a:LX/0hB;

    .line 1709747
    iput-object p2, p0, LX/AlU;->b:LX/1Rg;

    .line 1709748
    iput p3, p0, LX/AlU;->c:I

    .line 1709749
    return-void
.end method

.method private a(Landroid/view/View;)LX/8sj;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)",
            "LX/8sj;"
        }
    .end annotation

    .prologue
    const-wide/16 v2, 0x1f4

    const/4 v10, 0x0

    .line 1709750
    invoke-static {p1}, LX/AlU;->c(Landroid/view/View;)Landroid/view/View;

    move-result-object v1

    .line 1709751
    invoke-static {p1}, LX/AlU;->d(Landroid/view/View;)Landroid/view/View;

    move-result-object v7

    .line 1709752
    iget-object v0, p0, LX/AlU;->b:LX/1Rg;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v4

    iget v5, p0, LX/AlU;->c:I

    const/4 v6, 0x1

    invoke-virtual {p0, p1, v7, v6}, LX/AlU;->a(Landroid/view/View;Landroid/view/View;Z)LX/2tA;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, LX/1Rg;->a(Landroid/view/View;JIILandroid/animation/Animator$AnimatorListener;)LX/8sj;

    move-result-object v0

    .line 1709753
    iget-object v4, p0, LX/AlU;->b:LX/1Rg;

    new-instance v5, LX/Alp;

    invoke-direct {v5, p0, v7}, LX/Alp;-><init>(LX/AlU;Landroid/view/View;)V

    const-wide/16 v6, 0x258

    iget-object v1, p0, LX/AlU;->a:LX/0hB;

    invoke-virtual {v1}, LX/0hB;->c()I

    move-result v1

    int-to-float v8, v1

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v10}, LX/1Rg;->a(Ljava/lang/Object;JFFLandroid/animation/Animator$AnimatorListener;)LX/8sj;

    move-result-object v1

    .line 1709754
    iget-object v4, p0, LX/AlU;->b:LX/1Rg;

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-virtual {v4, v0, v2, v3, v10}, LX/1Rg;->a(LX/0Px;JLandroid/animation/Animator$AnimatorListener;)LX/8sj;

    move-result-object v0

    return-object v0
.end method

.method private b(Landroid/view/View;)LX/8sj;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)",
            "LX/8sj;"
        }
    .end annotation

    .prologue
    .line 1709739
    invoke-static {p1}, LX/AlU;->c(Landroid/view/View;)Landroid/view/View;

    move-result-object v7

    .line 1709740
    invoke-static {p1}, LX/AlU;->d(Landroid/view/View;)Landroid/view/View;

    move-result-object v8

    .line 1709741
    iget-object v0, p0, LX/AlU;->b:LX/1Rg;

    new-instance v1, LX/Alp;

    invoke-direct {v1, p0, v8}, LX/Alp;-><init>(LX/AlU;Landroid/view/View;)V

    const-wide/16 v2, 0x258

    const/4 v4, 0x0

    iget-object v5, p0, LX/AlU;->a:LX/0hB;

    invoke-virtual {v5}, LX/0hB;->c()I

    move-result v5

    int-to-float v5, v5

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, LX/1Rg;->a(Ljava/lang/Object;JFFLandroid/animation/Animator$AnimatorListener;)LX/8sj;

    move-result-object v9

    .line 1709742
    iget-object v0, p0, LX/AlU;->b:LX/1Rg;

    const-wide/16 v2, 0x1f4

    iget v4, p0, LX/AlU;->c:I

    invoke-virtual {v7}, Landroid/view/View;->getHeight()I

    move-result v5

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v8, v1}, LX/AlU;->a(Landroid/view/View;Landroid/view/View;Z)LX/2tA;

    move-result-object v6

    move-object v1, v7

    invoke-virtual/range {v0 .. v6}, LX/1Rg;->a(Landroid/view/View;JIILandroid/animation/Animator$AnimatorListener;)LX/8sj;

    move-result-object v0

    .line 1709743
    iget-object v1, p0, LX/AlU;->b:LX/1Rg;

    invoke-static {v9, v0}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    const-wide/16 v2, 0x1f4

    const/4 v4, 0x0

    invoke-virtual {v1, v0, v2, v3, v4}, LX/1Rg;->a(LX/0Px;JLandroid/animation/Animator$AnimatorListener;)LX/8sj;

    move-result-object v0

    return-object v0
.end method

.method private static c(Landroid/view/View;)Landroid/view/View;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)",
            "Landroid/view/View;"
        }
    .end annotation

    .prologue
    .line 1709744
    const v0, 0x7f0d1139

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private static d(Landroid/view/View;)Landroid/view/View;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)",
            "Landroid/view/View;"
        }
    .end annotation

    .prologue
    .line 1709738
    const v0, 0x7f0d250b

    invoke-virtual {p0, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public abstract a(Landroid/view/View;Landroid/view/View;Z)LX/2tA;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;",
            "Landroid/view/View;",
            "Z)T",
            "L;"
        }
    .end annotation
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;Landroid/view/View;)Ljava/lang/Runnable;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 1709737
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/util/List;Ljava/lang/Object;Ljava/lang/Object;Landroid/view/View;)V
    .locals 4

    .prologue
    .line 1709729
    check-cast p2, LX/24P;

    check-cast p3, LX/24P;

    .line 1709730
    if-ne p2, p3, :cond_1

    .line 1709731
    :cond_0
    :goto_0
    return-void

    .line 1709732
    :cond_1
    sget-object v0, LX/24P;->MAXIMIZED:LX/24P;

    if-ne p2, v0, :cond_2

    .line 1709733
    invoke-direct {p0, p4}, LX/AlU;->b(Landroid/view/View;)LX/8sj;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1709734
    :cond_2
    sget-object v0, LX/24P;->MAXIMIZED:LX/24P;

    if-ne p3, v0, :cond_0

    .line 1709735
    iget-object v0, p0, LX/AlU;->b:LX/1Rg;

    const-wide/16 v2, 0x190

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v3, v1}, LX/1Rg;->a(JLandroid/animation/Animator$AnimatorListener;)LX/8sj;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1709736
    invoke-direct {p0, p4}, LX/AlU;->a(Landroid/view/View;)LX/8sj;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method
