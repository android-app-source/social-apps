.class public final enum LX/CUB;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/CUB;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/CUB;

.field public static final enum DEFAULT_CLOSE:LX/CUB;

.field public static final enum EVENT_HANDLER:LX/CUB;

.field public static final enum NONE:LX/CUB;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1896286
    new-instance v0, LX/CUB;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, LX/CUB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CUB;->NONE:LX/CUB;

    .line 1896287
    new-instance v0, LX/CUB;

    const-string v1, "DEFAULT_CLOSE"

    invoke-direct {v0, v1, v3}, LX/CUB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CUB;->DEFAULT_CLOSE:LX/CUB;

    .line 1896288
    new-instance v0, LX/CUB;

    const-string v1, "EVENT_HANDLER"

    invoke-direct {v0, v1, v4}, LX/CUB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CUB;->EVENT_HANDLER:LX/CUB;

    .line 1896289
    const/4 v0, 0x3

    new-array v0, v0, [LX/CUB;

    sget-object v1, LX/CUB;->NONE:LX/CUB;

    aput-object v1, v0, v2

    sget-object v1, LX/CUB;->DEFAULT_CLOSE:LX/CUB;

    aput-object v1, v0, v3

    sget-object v1, LX/CUB;->EVENT_HANDLER:LX/CUB;

    aput-object v1, v0, v4

    sput-object v0, LX/CUB;->$VALUES:[LX/CUB;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1896290
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/CUB;
    .locals 1

    .prologue
    .line 1896291
    const-class v0, LX/CUB;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/CUB;

    return-object v0
.end method

.method public static values()[LX/CUB;
    .locals 1

    .prologue
    .line 1896292
    sget-object v0, LX/CUB;->$VALUES:[LX/CUB;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/CUB;

    return-object v0
.end method
