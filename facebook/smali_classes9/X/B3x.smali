.class public LX/B3x;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Landroid/content/Context;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1741061
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1741062
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/common/callercontext/CallerContext;Ljava/lang/String;Ljava/lang/String;I)Landroid/graphics/drawable/LayerDrawable;
    .locals 4

    .prologue
    .line 1741063
    new-instance v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v1, p0, LX/B3x;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;-><init>(Landroid/content/Context;)V

    .line 1741064
    new-instance v1, LX/1a3;

    invoke-direct {v1, p4, p4}, LX/1a3;-><init>(II)V

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1741065
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1741066
    invoke-virtual {v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1741067
    new-instance v1, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v2, p0, LX/B3x;->a:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;-><init>(Landroid/content/Context;)V

    .line 1741068
    new-instance v2, LX/1a3;

    invoke-direct {v2, p4, p4}, LX/1a3;-><init>(II)V

    invoke-virtual {v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1741069
    invoke-static {p3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1741070
    invoke-virtual {v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 1741071
    const/4 v2, 0x2

    new-array v2, v2, [Landroid/graphics/drawable/Drawable;

    .line 1741072
    const/4 v3, 0x0

    aput-object v0, v2, v3

    .line 1741073
    const/4 v0, 0x1

    aput-object v1, v2, v0

    .line 1741074
    new-instance v0, Landroid/graphics/drawable/LayerDrawable;

    invoke-direct {v0, v2}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    return-object v0
.end method
