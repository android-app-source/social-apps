.class public LX/CXC;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""


# instance fields
.field public a:LX/5fv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/resources/ui/FbImageButton;

.field public c:Landroid/widget/LinearLayout;

.field public d:Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentProductView;

.field public e:Lcom/facebook/fbui/glyph/GlyphView;

.field public f:Z

.field public g:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/CU0;",
            ">;"
        }
    .end annotation
.end field

.field public h:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public i:I

.field public j:LX/CWl;

.field public k:LX/34b;

.field public l:Landroid/view/MenuItem$OnMenuItemClickListener;

.field public m:Landroid/view/View$OnClickListener;

.field public n:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1909564
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, LX/CXC;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1909565
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1909603
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1909604
    const-class v0, LX/CXC;

    invoke-static {v0, p0}, LX/CXC;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1909605
    const v0, 0x7f030fa1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1909606
    const v0, 0x7f0d25c0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/CXC;->c:Landroid/widget/LinearLayout;

    .line 1909607
    const v0, 0x7f0d032f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbImageButton;

    iput-object v0, p0, LX/CXC;->b:Lcom/facebook/resources/ui/FbImageButton;

    .line 1909608
    const v0, 0x7f0d25b7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, LX/CXC;->e:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1909609
    iget-object v0, p0, LX/CXC;->c:Landroid/widget/LinearLayout;

    sget-object p1, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->PRODUCT:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    invoke-static {v0, p1}, LX/CSi;->b(Landroid/view/ViewGroup;Lcom/facebook/graphql/enums/GraphQLScreenElementType;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentProductView;

    iput-object v0, p0, LX/CXC;->d:Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentProductView;

    .line 1909610
    iget-object v0, p0, LX/CXC;->d:Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentProductView;

    invoke-virtual {v0}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentProductView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 1909611
    const/high16 p1, 0x3f800000    # 1.0f

    iput p1, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 1909612
    iget-object v0, p0, LX/CXC;->c:Landroid/widget/LinearLayout;

    iget-object p1, p0, LX/CXC;->d:Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentProductView;

    const/4 p2, 0x1

    invoke-virtual {v0, p1, p2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 1909613
    new-instance v0, LX/CX9;

    invoke-direct {v0, p0}, LX/CX9;-><init>(LX/CXC;)V

    iput-object v0, p0, LX/CXC;->m:Landroid/view/View$OnClickListener;

    .line 1909614
    new-instance v0, LX/CXA;

    invoke-direct {v0, p0}, LX/CXA;-><init>(LX/CXC;)V

    iput-object v0, p0, LX/CXC;->l:Landroid/view/MenuItem$OnMenuItemClickListener;

    .line 1909615
    new-instance v0, LX/CXB;

    invoke-direct {v0, p0}, LX/CXB;-><init>(LX/CXC;)V

    iput-object v0, p0, LX/CXC;->n:Landroid/view/View$OnClickListener;

    .line 1909616
    iget-object v0, p0, LX/CXC;->b:Lcom/facebook/resources/ui/FbImageButton;

    iget-object p1, p0, LX/CXC;->m:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1909617
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/CXC;->h:Ljava/util/Set;

    .line 1909618
    new-instance v0, LX/34b;

    invoke-virtual {p0}, LX/CXC;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-direct {v0, p1}, LX/34b;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/CXC;->k:LX/34b;

    .line 1909619
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/CXC;

    invoke-static {p0}, LX/5fv;->b(LX/0QB;)LX/5fv;

    move-result-object p0

    check-cast p0, LX/5fv;

    iput-object p0, p1, LX/CXC;->a:LX/5fv;

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/ArrayList;LX/CWl;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "LX/CU0;",
            ">;",
            "Lcom/facebook/pages/common/platform/ui/form_fields/ProductSelectionRadioButtonWithSelector$OnProductSelectorStateChangedListener;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 1909572
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1909573
    iput-object p2, p0, LX/CXC;->j:LX/CWl;

    .line 1909574
    iput-object p1, p0, LX/CXC;->g:Ljava/util/ArrayList;

    .line 1909575
    iget-object v0, p0, LX/CXC;->h:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 1909576
    iget-object v0, p0, LX/CXC;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v2

    :goto_0
    if-ge v1, v3, :cond_0

    iget-object v0, p0, LX/CXC;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CU0;

    .line 1909577
    iget-object v4, p0, LX/CXC;->h:Ljava/util/Set;

    iget-object v0, v0, LX/CU0;->a:Ljava/lang/String;

    invoke-interface {v4, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1909578
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1909579
    :cond_0
    iput v2, p0, LX/CXC;->i:I

    .line 1909580
    iget-object v1, p0, LX/CXC;->d:Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentProductView;

    iget-object v0, p0, LX/CXC;->g:Ljava/util/ArrayList;

    iget v3, p0, LX/CXC;->i:I

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CU0;

    invoke-virtual {v1, v0}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentProductView;->a(LX/CU0;)V

    .line 1909581
    iget-object v0, p0, LX/CXC;->k:LX/34b;

    invoke-virtual {v0}, LX/34c;->clear()V

    .line 1909582
    iget-object v0, p0, LX/CXC;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le v0, v5, :cond_5

    move v1, v2

    .line 1909583
    :goto_1
    iget-object v0, p0, LX/CXC;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 1909584
    iget-object v0, p0, LX/CXC;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CU0;

    .line 1909585
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 1909586
    iget-object v4, v0, LX/CU0;->b:Ljava/lang/String;

    invoke-static {v4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 1909587
    iget-object v4, v0, LX/CU0;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1909588
    :cond_1
    iget-object v4, v0, LX/CU0;->g:Ljava/lang/String;

    invoke-static {v4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 1909589
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    if-lez v4, :cond_2

    .line 1909590
    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1909591
    :cond_2
    iget-object v4, v0, LX/CU0;->g:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1909592
    :cond_3
    iget-object v4, p0, LX/CXC;->a:LX/5fv;

    invoke-virtual {v0, v4}, LX/CU0;->a(LX/5fv;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 1909593
    const-string v4, " - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1909594
    iget-object v4, p0, LX/CXC;->a:LX/5fv;

    invoke-virtual {v0, v4}, LX/CU0;->a(LX/5fv;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1909595
    :cond_4
    iget-object v0, p0, LX/CXC;->k:LX/34b;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/34c;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v0

    .line 1909596
    iget-object v3, p0, LX/CXC;->l:Landroid/view/MenuItem$OnMenuItemClickListener;

    invoke-virtual {v0, v3}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1909597
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1909598
    :cond_5
    iget-object v0, p0, LX/CXC;->d:Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentProductView;

    iget-object v1, p0, LX/CXC;->n:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentProductView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1909599
    iget-object v0, p0, LX/CXC;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le v0, v5, :cond_6

    .line 1909600
    iget-object v0, p0, LX/CXC;->e:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1909601
    :goto_2
    return-void

    .line 1909602
    :cond_6
    iget-object v0, p0, LX/CXC;->e:Lcom/facebook/fbui/glyph/GlyphView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    goto :goto_2
.end method

.method public setChecked(Z)V
    .locals 1

    .prologue
    .line 1909566
    iget-boolean v0, p0, LX/CXC;->f:Z

    if-eq v0, p1, :cond_0

    .line 1909567
    iput-boolean p1, p0, LX/CXC;->f:Z

    .line 1909568
    iget-object v0, p0, LX/CXC;->b:Lcom/facebook/resources/ui/FbImageButton;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbImageButton;->setSelected(Z)V

    .line 1909569
    iget-object v0, p0, LX/CXC;->j:LX/CWl;

    invoke-virtual {v0, p1}, LX/CWl;->a(Z)V

    .line 1909570
    invoke-super {p0}, Lcom/facebook/widget/CustomRelativeLayout;->refreshDrawableState()V

    .line 1909571
    :cond_0
    return-void
.end method
