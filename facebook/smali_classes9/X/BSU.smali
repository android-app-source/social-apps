.class public final enum LX/BSU;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/BSU;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/BSU;

.field public static final enum NONE:LX/BSU;

.field public static final enum NONLIVE_POST_ROLL:LX/BSU;

.field public static final enum NORMAL:LX/BSU;

.field public static final enum STALL_STREAM:LX/BSU;

.field public static final enum STREAM_DRY_OUT:LX/BSU;


# instance fields
.field private final mCommercialBreakPlaybackTriggerEvent:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1785504
    new-instance v0, LX/BSU;

    const-string v1, "NONE"

    const-string v2, "none"

    invoke-direct {v0, v1, v3, v2}, LX/BSU;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BSU;->NONE:LX/BSU;

    .line 1785505
    new-instance v0, LX/BSU;

    const-string v1, "NORMAL"

    const-string v2, "normal"

    invoke-direct {v0, v1, v4, v2}, LX/BSU;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BSU;->NORMAL:LX/BSU;

    .line 1785506
    new-instance v0, LX/BSU;

    const-string v1, "STREAM_DRY_OUT"

    const-string v2, "stream_dry_out"

    invoke-direct {v0, v1, v5, v2}, LX/BSU;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BSU;->STREAM_DRY_OUT:LX/BSU;

    .line 1785507
    new-instance v0, LX/BSU;

    const-string v1, "STALL_STREAM"

    const-string v2, "stall_stream"

    invoke-direct {v0, v1, v6, v2}, LX/BSU;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BSU;->STALL_STREAM:LX/BSU;

    .line 1785508
    new-instance v0, LX/BSU;

    const-string v1, "NONLIVE_POST_ROLL"

    const-string v2, "nonlive_post_roll"

    invoke-direct {v0, v1, v7, v2}, LX/BSU;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BSU;->NONLIVE_POST_ROLL:LX/BSU;

    .line 1785509
    const/4 v0, 0x5

    new-array v0, v0, [LX/BSU;

    sget-object v1, LX/BSU;->NONE:LX/BSU;

    aput-object v1, v0, v3

    sget-object v1, LX/BSU;->NORMAL:LX/BSU;

    aput-object v1, v0, v4

    sget-object v1, LX/BSU;->STREAM_DRY_OUT:LX/BSU;

    aput-object v1, v0, v5

    sget-object v1, LX/BSU;->STALL_STREAM:LX/BSU;

    aput-object v1, v0, v6

    sget-object v1, LX/BSU;->NONLIVE_POST_ROLL:LX/BSU;

    aput-object v1, v0, v7

    sput-object v0, LX/BSU;->$VALUES:[LX/BSU;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1785510
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1785511
    iput-object p3, p0, LX/BSU;->mCommercialBreakPlaybackTriggerEvent:Ljava/lang/String;

    .line 1785512
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/BSU;
    .locals 1

    .prologue
    .line 1785513
    const-class v0, LX/BSU;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/BSU;

    return-object v0
.end method

.method public static values()[LX/BSU;
    .locals 1

    .prologue
    .line 1785514
    sget-object v0, LX/BSU;->$VALUES:[LX/BSU;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/BSU;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1785515
    iget-object v0, p0, LX/BSU;->mCommercialBreakPlaybackTriggerEvent:Ljava/lang/String;

    return-object v0
.end method
