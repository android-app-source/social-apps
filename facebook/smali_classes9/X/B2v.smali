.class public LX/B2v;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1739295
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1739296
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 3

    .prologue
    .line 1739300
    check-cast p1, Ljava/lang/String;

    .line 1739301
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1739302
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "group_id"

    invoke-direct {v1, v2, p1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1739303
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "blacklistGroupsYouShouldJoin"

    .line 1739304
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 1739305
    move-object v1, v1

    .line 1739306
    const-string v2, "POST"

    .line 1739307
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 1739308
    move-object v1, v1

    .line 1739309
    const-string v2, "me/blacklisted_gysj_groups"

    .line 1739310
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 1739311
    move-object v1, v1

    .line 1739312
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 1739313
    move-object v0, v1

    .line 1739314
    sget-object v1, Lcom/facebook/http/interfaces/RequestPriority;->NON_INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0, v1}, LX/14O;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/14O;

    move-result-object v0

    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 1739315
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 1739316
    move-object v0, v0

    .line 1739317
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1739297
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 1739298
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    const-string v1, "success"

    invoke-virtual {v0, v1}, LX/0lF;->e(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 1739299
    invoke-static {v0}, LX/16N;->g(LX/0lF;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
