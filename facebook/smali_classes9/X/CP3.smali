.class public final LX/CP3;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/CP3;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/CP1;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/CP4;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1883930
    const/4 v0, 0x0

    sput-object v0, LX/CP3;->a:LX/CP3;

    .line 1883931
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/CP3;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1883932
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1883933
    new-instance v0, LX/CP4;

    invoke-direct {v0}, LX/CP4;-><init>()V

    iput-object v0, p0, LX/CP3;->c:LX/CP4;

    .line 1883934
    return-void
.end method

.method public static declared-synchronized q()LX/CP3;
    .locals 2

    .prologue
    .line 1883935
    const-class v1, LX/CP3;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/CP3;->a:LX/CP3;

    if-nez v0, :cond_0

    .line 1883936
    new-instance v0, LX/CP3;

    invoke-direct {v0}, LX/CP3;-><init>()V

    sput-object v0, LX/CP3;->a:LX/CP3;

    .line 1883937
    :cond_0
    sget-object v0, LX/CP3;->a:LX/CP3;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 1883938
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 1883898
    check-cast p2, LX/CP2;

    .line 1883899
    iget-object v0, p2, LX/CP2;->a:LX/CNb;

    iget-object v1, p2, LX/CP2;->b:LX/CNc;

    iget-object v2, p2, LX/CP2;->c:Ljava/util/List;

    .line 1883900
    invoke-static {p1}, LX/5Jt;->c(LX/1De;)LX/5Jr;

    move-result-object v4

    .line 1883901
    const-string v3, "color"

    invoke-virtual {v0, v3}, LX/CNb;->c(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1883902
    const-string v3, "color"

    const/4 v5, 0x0

    invoke-virtual {v0, v3, v5}, LX/CNb;->b(Ljava/lang/String;I)I

    move-result v3

    .line 1883903
    iget-object v5, v4, LX/5Jr;->a:LX/5Js;

    iput v3, v5, LX/5Js;->a:I

    .line 1883904
    :cond_0
    const-string v3, "size"

    const-string v5, "SMALL"

    invoke-virtual {v0, v3, v5}, LX/CNb;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v5, "SMALL"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    const/16 v3, 0x10

    .line 1883905
    :goto_0
    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    invoke-interface {v4, v3}, LX/1Di;->j(I)LX/1Di;

    move-result-object v4

    invoke-interface {v4, v3}, LX/1Di;->r(I)LX/1Di;

    move-result-object v3

    .line 1883906
    const-string v4, "touch-up-inside-actions"

    invoke-static {v1, v0, v2, v4}, LX/CPx;->a(LX/CNc;LX/CNb;Ljava/util/List;Ljava/lang/String;)LX/CNe;

    move-result-object v4

    .line 1883907
    if-eqz v4, :cond_1

    .line 1883908
    const v5, 0x20a8d8c

    const/4 p0, 0x1

    new-array p0, p0, [Ljava/lang/Object;

    const/4 p2, 0x0

    aput-object v4, p0, p2

    invoke-static {p1, v5, p0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v5

    move-object v4, v5

    .line 1883909
    invoke-interface {v3, v4}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    .line 1883910
    :cond_1
    const-string v4, "long-press-actions"

    invoke-static {v1, v0, v2, v4}, LX/CPx;->a(LX/CNc;LX/CNb;Ljava/util/List;Ljava/lang/String;)LX/CNe;

    move-result-object v4

    .line 1883911
    if-eqz v4, :cond_2

    .line 1883912
    const v5, -0x18ede7d7

    const/4 p0, 0x1

    new-array p0, p0, [Ljava/lang/Object;

    const/4 p2, 0x0

    aput-object v4, p0, p2

    invoke-static {p1, v5, p0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v5

    move-object v4, v5

    .line 1883913
    invoke-interface {v3, v4}, LX/1Di;->b(LX/1dQ;)LX/1Di;

    .line 1883914
    :cond_2
    invoke-static {v3, p1, v0, v2}, LX/CPx;->a(LX/1Di;LX/1De;LX/CNb;Ljava/util/List;)LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 1883915
    return-object v0

    .line 1883916
    :cond_3
    const/16 v3, 0x30

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 1883917
    invoke-static {}, LX/1dS;->b()V

    .line 1883918
    iget v0, p1, LX/1dQ;->b:I

    .line 1883919
    sparse-switch v0, :sswitch_data_0

    move-object v0, v1

    .line 1883920
    :goto_0
    return-object v0

    .line 1883921
    :sswitch_0
    iget-object v0, p1, LX/1dQ;->c:[Ljava/lang/Object;

    aget-object v0, v0, v2

    check-cast v0, LX/CNe;

    .line 1883922
    invoke-interface {v0}, LX/CNe;->a()V

    .line 1883923
    move-object v0, v1

    .line 1883924
    goto :goto_0

    .line 1883925
    :sswitch_1
    iget-object v0, p1, LX/1dQ;->c:[Ljava/lang/Object;

    aget-object v0, v0, v2

    check-cast v0, LX/CNe;

    .line 1883926
    invoke-interface {v0}, LX/CNe;->a()V

    .line 1883927
    const/4 v2, 0x1

    move v2, v2

    .line 1883928
    move v0, v2

    .line 1883929
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x18ede7d7 -> :sswitch_1
        0x20a8d8c -> :sswitch_0
    .end sparse-switch
.end method
