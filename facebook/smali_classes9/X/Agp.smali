.class public final LX/Agp;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:Lcom/facebook/payments/paymentmethods/picker/PaymentMethodsPickerScreenConfig;

.field public final synthetic c:I

.field public final synthetic d:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;ZLcom/facebook/payments/paymentmethods/picker/PaymentMethodsPickerScreenConfig;I)V
    .locals 0

    .prologue
    .line 1701538
    iput-object p1, p0, LX/Agp;->d:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;

    iput-boolean p2, p0, LX/Agp;->a:Z

    iput-object p3, p0, LX/Agp;->b:Lcom/facebook/payments/paymentmethods/picker/PaymentMethodsPickerScreenConfig;

    iput p4, p0, LX/Agp;->c:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0x55e06999

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1701534
    iget-object v1, p0, LX/Agp;->d:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;

    iget-object v1, v1, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->g:LX/Ah5;

    iget-boolean v2, p0, LX/Agp;->a:Z

    invoke-virtual {v1, v2}, LX/Ah5;->a(Z)V

    .line 1701535
    iget-object v1, p0, LX/Agp;->d:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;

    iget-object v1, v1, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->J:LX/6qh;

    iget-object v2, p0, LX/Agp;->d:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, LX/Agp;->b:Lcom/facebook/payments/paymentmethods/picker/PaymentMethodsPickerScreenConfig;

    invoke-static {v2, v3}, Lcom/facebook/payments/picker/PickerScreenActivity;->a(Landroid/content/Context;Lcom/facebook/payments/picker/model/PickerScreenConfig;)Landroid/content/Intent;

    move-result-object v2

    iget v3, p0, LX/Agp;->c:I

    invoke-virtual {v1, v2, v3}, LX/6qh;->a(Landroid/content/Intent;I)V

    .line 1701536
    iget-object v1, p0, LX/Agp;->d:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;

    iget-object v1, v1, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->t:LX/Ah2;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/Ah2;->b(Z)V

    .line 1701537
    const v1, -0x16261b39

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
