.class public LX/BuP;
.super LX/2oy;
.source ""


# instance fields
.field private final a:Landroid/view/View;

.field public final b:LX/3IC;

.field private final c:LX/BuO;

.field public d:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1830966
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/BuP;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1830967
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1830964
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/BuP;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1830965
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 1830968
    invoke-direct {p0, p1, p2, p3}, LX/2oy;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1830969
    const v0, 0x7f030733

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1830970
    const v0, 0x7f0d1344

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/BuP;->a:Landroid/view/View;

    .line 1830971
    iget-object v0, p0, LX/BuP;->a:Landroid/view/View;

    new-instance v1, LX/BuN;

    invoke-direct {v1, p0}, LX/BuN;-><init>(LX/BuP;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1830972
    new-instance v0, LX/BuO;

    invoke-direct {v0, p0}, LX/BuO;-><init>(LX/BuP;)V

    iput-object v0, p0, LX/BuP;->c:LX/BuO;

    .line 1830973
    new-instance v0, LX/3IC;

    iget-object v1, p0, LX/BuP;->c:LX/BuO;

    iget-object v2, p0, LX/BuP;->c:LX/BuO;

    invoke-direct {v0, p1, v1, v2}, LX/3IC;-><init>(Landroid/content/Context;LX/3IA;LX/3IB;)V

    iput-object v0, p0, LX/BuP;->b:LX/3IC;

    .line 1830974
    return-void
.end method


# virtual methods
.method public final a(LX/2pa;Z)V
    .locals 1

    .prologue
    .line 1830961
    invoke-super {p0, p1, p2}, LX/2oy;->a(LX/2pa;Z)V

    .line 1830962
    invoke-virtual {p1}, LX/2pa;->d()Z

    move-result v0

    iput-boolean v0, p0, LX/BuP;->d:Z

    .line 1830963
    return-void
.end method
