.class public final LX/BsI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

.field public final synthetic b:Lcom/facebook/feed/rows/sections/header/ThrowbackSharedStoryHeaderExplanationPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/sections/header/ThrowbackSharedStoryHeaderExplanationPartDefinition;Lcom/facebook/graphql/model/GraphQLStoryActionLink;)V
    .locals 0

    .prologue
    .line 1827357
    iput-object p1, p0, LX/BsI;->b:Lcom/facebook/feed/rows/sections/header/ThrowbackSharedStoryHeaderExplanationPartDefinition;

    iput-object p2, p0, LX/BsI;->a:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x7e751b65

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1827358
    iget-object v0, p0, LX/BsI;->a:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    if-nez v0, :cond_0

    sget-object v0, LX/0ax;->fz:Ljava/lang/String;

    const-string v2, "shared_feed_story"

    invoke-static {v0, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1827359
    :goto_0
    iget-object v2, p0, LX/BsI;->b:Lcom/facebook/feed/rows/sections/header/ThrowbackSharedStoryHeaderExplanationPartDefinition;

    iget-object v2, v2, Lcom/facebook/feed/rows/sections/header/ThrowbackSharedStoryHeaderExplanationPartDefinition;->h:LX/17W;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 1827360
    const v0, -0x7214bcfb

    invoke-static {v0, v1}, LX/02F;->a(II)V

    return-void

    .line 1827361
    :cond_0
    iget-object v0, p0, LX/BsI;->a:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->bc()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
