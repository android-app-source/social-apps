.class public LX/ByH;
.super Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;
.source ""

# interfaces
.implements LX/2fC;
.implements LX/2eZ;


# instance fields
.field private final a:Landroid/widget/LinearLayout;

.field public b:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1837002
    invoke-direct {p0, p1}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;-><init>(Landroid/content/Context;)V

    .line 1837003
    const v0, 0x7f030521

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1837004
    const v0, 0x7f0d0e91

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/ByH;->a:Landroid/widget/LinearLayout;

    .line 1837005
    return-void
.end method


# virtual methods
.method public final a(IZZ)V
    .locals 0

    .prologue
    .line 1837024
    invoke-virtual {p0, p1}, LX/ByH;->setWidth(I)V

    .line 1837025
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1837023
    iget-boolean v0, p0, LX/ByH;->b:Z

    return v0
.end method

.method public final addView(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1837021
    iget-object v0, p0, LX/ByH;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1837022
    return-void
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x1e201941

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1837017
    invoke-super {p0}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;->onAttachedToWindow()V

    .line 1837018
    const/4 v1, 0x1

    .line 1837019
    iput-boolean v1, p0, LX/ByH;->b:Z

    .line 1837020
    const/16 v1, 0x2d

    const v2, -0xb0a8f1e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x2bfb7bdb    # 1.7869E-12f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1837013
    invoke-super {p0}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;->onDetachedFromWindow()V

    .line 1837014
    const/4 v1, 0x0

    .line 1837015
    iput-boolean v1, p0, LX/ByH;->b:Z

    .line 1837016
    const/16 v1, 0x2d

    const v2, -0x4fc3be36

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final removeAllViews()V
    .locals 1

    .prologue
    .line 1837011
    iget-object v0, p0, LX/ByH;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 1837012
    return-void
.end method

.method public setWidth(I)V
    .locals 2

    .prologue
    .line 1837006
    iget-object v0, p0, LX/ByH;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 1837007
    iput p1, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 1837008
    iget-object v1, p0, LX/ByH;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1837009
    iget-object v0, p0, LX/ByH;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->requestLayout()V

    .line 1837010
    return-void
.end method
