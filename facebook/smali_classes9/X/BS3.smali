.class public LX/BS3;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/BS3;


# instance fields
.field private final a:LX/0ad;


# direct methods
.method public constructor <init>(LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1784674
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1784675
    iput-object p1, p0, LX/BS3;->a:LX/0ad;

    .line 1784676
    return-void
.end method

.method public static a(LX/0QB;)LX/BS3;
    .locals 4

    .prologue
    .line 1784677
    sget-object v0, LX/BS3;->b:LX/BS3;

    if-nez v0, :cond_1

    .line 1784678
    const-class v1, LX/BS3;

    monitor-enter v1

    .line 1784679
    :try_start_0
    sget-object v0, LX/BS3;->b:LX/BS3;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1784680
    if-eqz v2, :cond_0

    .line 1784681
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1784682
    new-instance p0, LX/BS3;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-direct {p0, v3}, LX/BS3;-><init>(LX/0ad;)V

    .line 1784683
    move-object v0, p0

    .line 1784684
    sput-object v0, LX/BS3;->b:LX/BS3;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1784685
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1784686
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1784687
    :cond_1
    sget-object v0, LX/BS3;->b:LX/BS3;

    return-object v0

    .line 1784688
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1784689
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/0gW;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0gW",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 1784690
    const-string v0, "show_friending_suggestion_action_bar"

    iget-object v1, p0, LX/BS3;->a:LX/0ad;

    sget-short v2, LX/AqT;->a:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 1784691
    return-void
.end method
