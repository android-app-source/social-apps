.class public final LX/BqV;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/BqW;",
        ">;"
    }
.end annotation


# static fields
.field private static b:[Ljava/lang/String;

.field private static c:I


# instance fields
.field public a:LX/BqU;

.field private d:Ljava/util/BitSet;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x3

    .line 1824762
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "userCount"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "labelText"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "countColor"

    aput-object v2, v0, v1

    sput-object v0, LX/BqV;->b:[Ljava/lang/String;

    .line 1824763
    sput v3, LX/BqV;->c:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1824746
    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 1824747
    new-instance v0, Ljava/util/BitSet;

    sget v1, LX/BqV;->c:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/BqV;->d:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/BqV;LX/1De;IILX/BqU;)V
    .locals 1

    .prologue
    .line 1824764
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 1824765
    iput-object p4, p0, LX/BqV;->a:LX/BqU;

    .line 1824766
    iget-object v0, p0, LX/BqV;->d:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 1824767
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1824748
    invoke-super {p0}, LX/1X5;->a()V

    .line 1824749
    const/4 v0, 0x0

    iput-object v0, p0, LX/BqV;->a:LX/BqU;

    .line 1824750
    sget-object v0, LX/BqW;->b:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 1824751
    return-void
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/BqW;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1824752
    iget-object v1, p0, LX/BqV;->d:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/BqV;->d:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    sget v2, LX/BqV;->c:I

    if-ge v1, v2, :cond_2

    .line 1824753
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1824754
    :goto_0
    sget v2, LX/BqV;->c:I

    if-ge v0, v2, :cond_1

    .line 1824755
    iget-object v2, p0, LX/BqV;->d:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1824756
    sget-object v2, LX/BqV;->b:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1824757
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1824758
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1824759
    :cond_2
    iget-object v0, p0, LX/BqV;->a:LX/BqU;

    .line 1824760
    invoke-virtual {p0}, LX/BqV;->a()V

    .line 1824761
    return-object v0
.end method

.method public final h(I)LX/BqV;
    .locals 2

    .prologue
    .line 1824737
    iget-object v0, p0, LX/BqV;->a:LX/BqU;

    iput p1, v0, LX/BqU;->a:I

    .line 1824738
    iget-object v0, p0, LX/BqV;->d:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1824739
    return-object p0
.end method

.method public final i(I)LX/BqV;
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 1824740
    iget-object v0, p0, LX/BqV;->a:LX/BqU;

    invoke-virtual {p0, p1}, LX/1Dp;->b(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/BqU;->b:Ljava/lang/CharSequence;

    .line 1824741
    iget-object v0, p0, LX/BqV;->d:Ljava/util/BitSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1824742
    return-object p0
.end method

.method public final j(I)LX/BqV;
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/ColorRes;
        .end annotation
    .end param

    .prologue
    .line 1824743
    iget-object v0, p0, LX/BqV;->a:LX/BqU;

    invoke-virtual {p0, p1}, LX/1Dp;->d(I)I

    move-result v1

    iput v1, v0, LX/BqU;->c:I

    .line 1824744
    iget-object v0, p0, LX/BqV;->d:Ljava/util/BitSet;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1824745
    return-object p0
.end method
