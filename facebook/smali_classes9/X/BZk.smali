.class public final LX/BZk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/29r;


# direct methods
.method public constructor <init>(LX/29r;)V
    .locals 0

    .prologue
    .line 1797985
    iput-object p1, p0, LX/BZk;->a:LX/29r;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1797986
    iget-object v0, p0, LX/BZk;->a:LX/29r;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/29r;->a(Lcom/facebook/appirater/ratingdialog/RatingDialogSaveState;)V

    .line 1797987
    iget-object v0, p0, LX/BZk;->a:LX/29r;

    const/4 v1, 0x0

    .line 1797988
    iput-boolean v1, v0, LX/29r;->t:Z

    .line 1797989
    iget-object v0, p0, LX/BZk;->a:LX/29r;

    iget-object v0, v0, LX/29r;->f:LX/03V;

    const-string v1, "InternalStarRating-CreateReport-Fail"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1797990
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1797991
    iget-object v0, p0, LX/BZk;->a:LX/29r;

    iget-object v0, v0, LX/29r;->c:LX/29s;

    const/4 v1, 0x1

    .line 1797992
    iget-object v2, v0, LX/29s;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    sget-object p1, LX/BZn;->e:LX/0Tn;

    invoke-interface {v2, p1, v1}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v2

    invoke-interface {v2}, LX/0hN;->commit()V

    .line 1797993
    iget-object v0, p0, LX/BZk;->a:LX/29r;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/29r;->a(Lcom/facebook/appirater/ratingdialog/RatingDialogSaveState;)V

    .line 1797994
    iget-object v0, p0, LX/BZk;->a:LX/29r;

    const/4 v1, 0x0

    .line 1797995
    iput-boolean v1, v0, LX/29r;->t:Z

    .line 1797996
    return-void
.end method
