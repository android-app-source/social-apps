.class public LX/Az7;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:LX/0Rl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rl",
            "<",
            "Lcom/facebook/friendsharing/souvenirs/models/SouvenirUriItem;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:LX/0Rl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rl",
            "<",
            "Lcom/facebook/friendsharing/souvenirs/models/SouvenirUriItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1732142
    new-instance v0, LX/Az5;

    invoke-direct {v0}, LX/Az5;-><init>()V

    sput-object v0, LX/Az7;->a:LX/0Rl;

    .line 1732143
    new-instance v0, LX/Az6;

    invoke-direct {v0}, LX/Az6;-><init>()V

    sput-object v0, LX/Az7;->b:LX/0Rl;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1732144
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;)LX/0Rc;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;",
            ")",
            "LX/0Rc",
            "<",
            "Lcom/facebook/friendsharing/souvenirs/models/SouvenirUriItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1732145
    new-instance v0, LX/Az1;

    invoke-virtual {p0}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Py;->iterator()LX/0Rc;

    move-result-object v1

    invoke-direct {v0, v1}, LX/Az1;-><init>(LX/0Rc;)V

    return-object v0
.end method

.method public static a(Lcom/facebook/friendsharing/souvenirs/models/SouvenirUriItem;)Z
    .locals 6
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 1732146
    invoke-virtual {p0}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirUriItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v1

    .line 1732147
    iget-object v2, v1, Lcom/facebook/ipc/media/data/MediaData;->mUri:Landroid/net/Uri;

    move-object v1, v2

    .line 1732148
    invoke-static {v1}, LX/1be;->a(Landroid/net/Uri;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1732149
    :cond_0
    :goto_0
    return v0

    .line 1732150
    :cond_1
    new-instance v2, Ljava/io/File;

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1732151
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v2}, Ljava/io/File;->canRead()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-gtz v1, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;)Z
    .locals 2

    .prologue
    .line 1732152
    new-instance v0, LX/Az8;

    invoke-virtual {p0}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Py;->iterator()LX/0Rc;

    move-result-object v1

    invoke-direct {v0, v1}, LX/Az8;-><init>(LX/0Rc;)V

    move-object v0, v0

    .line 1732153
    invoke-virtual {v0}, LX/0Rc;->hasNext()Z

    move-result v1

    invoke-static {v1}, LX/0PB;->checkArgument(Z)V

    .line 1732154
    invoke-virtual {v0}, LX/0Rc;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/friendsharing/souvenirs/models/SouvenirUriItem;

    :goto_0
    invoke-virtual {v0}, LX/0Rc;->hasNext()Z

    move-result p0

    if-eqz p0, :cond_1

    .line 1732155
    invoke-static {v1}, LX/Az7;->a(Lcom/facebook/friendsharing/souvenirs/models/SouvenirUriItem;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1732156
    const/4 v1, 0x0

    .line 1732157
    :goto_1
    move v0, v1

    .line 1732158
    return v0

    .line 1732159
    :cond_0
    invoke-virtual {v0}, LX/0Rc;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/friendsharing/souvenirs/models/SouvenirUriItem;

    goto :goto_0

    .line 1732160
    :cond_1
    const/4 v1, 0x1

    goto :goto_1
.end method
