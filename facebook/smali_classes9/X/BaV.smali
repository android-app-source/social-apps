.class public LX/BaV;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static volatile j:LX/BaV;


# instance fields
.field public final b:Ljava/util/concurrent/ExecutorService;

.field public final c:LX/18V;

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/7hC;

.field public final f:LX/8NH;

.field public final g:LX/8LV;

.field public final h:LX/1Eo;

.field public final i:LX/0aG;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1799164
    const-class v0, LX/BaV;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/BaV;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/ExecutorService;LX/18V;LX/0Or;LX/7hC;LX/8NH;LX/8LV;LX/1Eo;LX/0aG;)V
    .locals 0
    .param p1    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/ExecutorService;",
            "Lcom/facebook/http/protocol/ApiMethodRunner;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/7hC;",
            "LX/8NH;",
            "LX/8LV;",
            "LX/1Eo;",
            "LX/0aG;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1799165
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1799166
    iput-object p1, p0, LX/BaV;->b:Ljava/util/concurrent/ExecutorService;

    .line 1799167
    iput-object p2, p0, LX/BaV;->c:LX/18V;

    .line 1799168
    iput-object p3, p0, LX/BaV;->d:LX/0Or;

    .line 1799169
    iput-object p4, p0, LX/BaV;->e:LX/7hC;

    .line 1799170
    iput-object p5, p0, LX/BaV;->f:LX/8NH;

    .line 1799171
    iput-object p6, p0, LX/BaV;->g:LX/8LV;

    .line 1799172
    iput-object p7, p0, LX/BaV;->h:LX/1Eo;

    .line 1799173
    iput-object p8, p0, LX/BaV;->i:LX/0aG;

    .line 1799174
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/7gi;Ljava/lang/String;Ljava/lang/String;JLX/0Px;ZLX/0Px;Ljava/lang/String;)LX/0Px;
    .locals 11
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p9    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p11    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/7gi;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "J",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;Z",
            "LX/0Px",
            "<",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationPromptAnalytics;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1799175
    invoke-static/range {p5 .. p5}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1799176
    const/16 p5, 0x0

    .line 1799177
    :cond_0
    new-instance v9, LX/14U;

    invoke-direct {v9}, LX/14U;-><init>()V

    .line 1799178
    sget-object v2, Lcom/facebook/http/interfaces/RequestPriority;->NON_INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v9, v2}, LX/14U;->a(Lcom/facebook/http/interfaces/RequestPriority;)V

    .line 1799179
    const-wide/16 v2, 0x3e8

    div-long v2, p7, v2

    long-to-int v10, v2

    .line 1799180
    new-instance v2, LX/7hD;

    sget-object v3, LX/7gi;->PHOTO:LX/7gi;

    if-ne p4, v3, :cond_1

    const-string v4, "photo"

    :goto_0
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object v3, p0, LX/BaV;->d:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    move-object v3, p1

    move-object/from16 v7, p6

    move-object/from16 v8, p12

    invoke-direct/range {v2 .. v8}, LX/7hD;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, p2}, LX/7hD;->b(Ljava/lang/String;)LX/7hD;

    move-result-object v2

    invoke-virtual {v2, p3}, LX/7hD;->c(Ljava/lang/String;)LX/7hD;

    move-result-object v2

    move-object/from16 v0, p5

    invoke-virtual {v2, v0}, LX/7hD;->a(Ljava/lang/String;)LX/7hD;

    move-result-object v2

    invoke-virtual {v2, v10}, LX/7hD;->a(I)LX/7hD;

    move-result-object v2

    move-object/from16 v0, p9

    invoke-virtual {v2, v0}, LX/7hD;->a(LX/0Px;)LX/7hD;

    move-result-object v2

    move/from16 v0, p10

    invoke-virtual {v2, v0}, LX/7hD;->a(Z)LX/7hD;

    move-result-object v2

    move-object/from16 v0, p11

    invoke-virtual {v2, v0}, LX/7hD;->c(LX/0Px;)LX/7hD;

    move-result-object v2

    invoke-virtual {v2}, LX/7hD;->a()LX/7hE;

    move-result-object v2

    .line 1799181
    iget-object v3, p0, LX/BaV;->c:LX/18V;

    iget-object v4, p0, LX/BaV;->e:LX/7hC;

    invoke-virtual {v3, v4, v2, v9}, LX/18V;->a(LX/0e6;Ljava/lang/Object;LX/14U;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/audience/upload/protocol/ShotCreateResult;

    .line 1799182
    invoke-virtual {v2}, Lcom/facebook/audience/upload/protocol/ShotCreateResult;->a()LX/0Px;

    move-result-object v2

    return-object v2

    .line 1799183
    :cond_1
    const-string v4, "video"

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/BaV;
    .locals 12

    .prologue
    .line 1799184
    sget-object v0, LX/BaV;->j:LX/BaV;

    if-nez v0, :cond_1

    .line 1799185
    const-class v1, LX/BaV;

    monitor-enter v1

    .line 1799186
    :try_start_0
    sget-object v0, LX/BaV;->j:LX/BaV;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1799187
    if-eqz v2, :cond_0

    .line 1799188
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1799189
    new-instance v3, LX/BaV;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/18V;->a(LX/0QB;)LX/18V;

    move-result-object v5

    check-cast v5, LX/18V;

    const/16 v6, 0x15e7

    invoke-static {v0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-static {v0}, LX/7hC;->a(LX/0QB;)LX/7hC;

    move-result-object v7

    check-cast v7, LX/7hC;

    invoke-static {v0}, LX/8NH;->b(LX/0QB;)LX/8NH;

    move-result-object v8

    check-cast v8, LX/8NH;

    invoke-static {v0}, LX/8LV;->b(LX/0QB;)LX/8LV;

    move-result-object v9

    check-cast v9, LX/8LV;

    invoke-static {v0}, LX/1Eo;->a(LX/0QB;)LX/1Eo;

    move-result-object v10

    check-cast v10, LX/1Eo;

    invoke-static {v0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v11

    check-cast v11, LX/0aG;

    invoke-direct/range {v3 .. v11}, LX/BaV;-><init>(Ljava/util/concurrent/ExecutorService;LX/18V;LX/0Or;LX/7hC;LX/8NH;LX/8LV;LX/1Eo;LX/0aG;)V

    .line 1799190
    move-object v0, v3

    .line 1799191
    sput-object v0, LX/BaV;->j:LX/BaV;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1799192
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1799193
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1799194
    :cond_1
    sget-object v0, LX/BaV;->j:LX/BaV;

    return-object v0

    .line 1799195
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1799196
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a$redex0(LX/BaV;Ljava/lang/String;Lcom/facebook/audience/model/UploadShot;)LX/0Px;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/facebook/audience/model/UploadShot;",
            ")",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1799197
    invoke-virtual/range {p2 .. p2}, Lcom/facebook/audience/model/UploadShot;->getCaption()Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {p2 .. p2}, Lcom/facebook/audience/model/UploadShot;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {p2 .. p2}, Lcom/facebook/audience/model/UploadShot;->getMediaType()LX/7gi;

    move-result-object v5

    invoke-virtual/range {p2 .. p2}, Lcom/facebook/audience/model/UploadShot;->getReactionId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {p2 .. p2}, Lcom/facebook/audience/model/UploadShot;->getBackstagePostType()LX/7h9;

    move-result-object v0

    invoke-virtual {v0}, LX/7h9;->getType()Ljava/lang/String;

    move-result-object v7

    invoke-virtual/range {p2 .. p2}, Lcom/facebook/audience/model/UploadShot;->getCreatedAtTime()J

    move-result-wide v8

    invoke-virtual/range {p2 .. p2}, Lcom/facebook/audience/model/UploadShot;->getAudience()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/7h8;->a(LX/0Px;)LX/0Px;

    move-result-object v10

    invoke-virtual/range {p2 .. p2}, Lcom/facebook/audience/model/UploadShot;->getIsPrivate()Z

    move-result v11

    invoke-virtual/range {p2 .. p2}, Lcom/facebook/audience/model/UploadShot;->getInspirationPromptAnalytics()LX/0Px;

    move-result-object v12

    invoke-virtual/range {p2 .. p2}, Lcom/facebook/audience/model/UploadShot;->getReplyThreadId()Ljava/lang/String;

    move-result-object v13

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v1 .. v13}, LX/BaV;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/7gi;Ljava/lang/String;Ljava/lang/String;JLX/0Px;ZLX/0Px;Ljava/lang/String;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
