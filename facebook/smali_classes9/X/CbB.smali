.class public LX/CbB;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/17Y;

.field private final c:Lcom/facebook/content/SecureContextHelper;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/17Y;Lcom/facebook/content/SecureContextHelper;)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1919275
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1919276
    iput-object p1, p0, LX/CbB;->a:Landroid/content/Context;

    .line 1919277
    iput-object p2, p0, LX/CbB;->b:LX/17Y;

    .line 1919278
    iput-object p3, p0, LX/CbB;->c:Lcom/facebook/content/SecureContextHelper;

    .line 1919279
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1919280
    invoke-static {p2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1919281
    invoke-static {p1, p2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1919282
    iget-object v1, p0, LX/CbB;->b:LX/17Y;

    iget-object v2, p0, LX/CbB;->a:Landroid/content/Context;

    invoke-interface {v1, v2, v0}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1919283
    if-eqz v0, :cond_0

    .line 1919284
    iget-object v1, p0, LX/CbB;->c:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/CbB;->a:Landroid/content/Context;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1919285
    :cond_0
    return-void

    .line 1919286
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
