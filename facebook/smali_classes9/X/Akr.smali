.class public final LX/Akr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Z

.field public final synthetic d:LX/BUA;

.field public final synthetic e:Ljava/lang/String;

.field public final synthetic f:LX/1wH;


# direct methods
.method public constructor <init>(LX/1wH;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;ZLX/BUA;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1709328
    iput-object p1, p0, LX/Akr;->f:LX/1wH;

    iput-object p2, p0, LX/Akr;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iput-object p3, p0, LX/Akr;->b:Ljava/lang/String;

    iput-boolean p4, p0, LX/Akr;->c:Z

    iput-object p5, p0, LX/Akr;->d:LX/BUA;

    iput-object p6, p0, LX/Akr;->e:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1709329
    iget-object v0, p0, LX/Akr;->f:LX/1wH;

    iget-object v0, v0, LX/1wH;->a:LX/1SX;

    iget-object v1, p0, LX/Akr;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    iget-object v3, p0, LX/Akr;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3, v4}, LX/1SX;->a(Lcom/facebook/feed/rows/core/props/FeedProps;ILjava/lang/String;Z)V

    .line 1709330
    iget-object v0, p0, LX/Akr;->f:LX/1wH;

    iget-object v0, v0, LX/1wH;->a:LX/1SX;

    iget-object v0, v0, LX/1SX;->g:LX/1Sj;

    iget-object v1, p0, LX/Akr;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    const-string v2, "caret_menu"

    iget-object v3, p0, LX/Akr;->f:LX/1wH;

    iget-object v3, v3, LX/1wH;->a:LX/1SX;

    invoke-static {v3}, LX/1SX;->g(LX/1SX;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, LX/1Sj;->b(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Ljava/lang/String;)V

    .line 1709331
    iget-boolean v0, p0, LX/Akr;->c:Z

    if-eqz v0, :cond_0

    .line 1709332
    iget-object v0, p0, LX/Akr;->d:LX/BUA;

    iget-object v1, p0, LX/Akr;->e:Ljava/lang/String;

    sget-object v2, LX/7Jb;->USER_INITIATED:LX/7Jb;

    invoke-virtual {v0, v1, v2}, LX/BUA;->a(Ljava/lang/String;LX/7Jb;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1709333
    iget-object v0, p0, LX/Akr;->f:LX/1wH;

    iget-object v0, v0, LX/1wH;->a:LX/1SX;

    iget-object v0, v0, LX/1SX;->f:LX/1Sa;

    iget-object v1, p0, LX/Akr;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/1Sa;->a(Ljava/lang/String;)V

    .line 1709334
    :cond_0
    iget-object v0, p0, LX/Akr;->f:LX/1wH;

    iget-object v0, v0, LX/1wH;->a:LX/1SX;

    iget-object v0, v0, LX/1SX;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/79m;

    invoke-virtual {v0}, LX/79m;->a()V

    .line 1709335
    return v4
.end method
