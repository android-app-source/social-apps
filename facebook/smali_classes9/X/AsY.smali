.class public final LX/AsY;
.super LX/4o9;
.source ""


# instance fields
.field public final synthetic a:LX/Ase;


# direct methods
.method public constructor <init>(LX/Ase;)V
    .locals 0

    .prologue
    .line 1720662
    iput-object p1, p0, LX/AsY;->a:LX/Ase;

    invoke-direct {p0}, LX/4o9;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1720663
    iget-object v0, p0, LX/AsY;->a:LX/Ase;

    iget-object v0, v0, LX/Ase;->d:LX/AsA;

    invoke-interface {v0}, LX/AsA;->b()LX/86o;

    move-result-object v0

    invoke-virtual {v0}, LX/86o;->shouldBeDismissedBySwipeDownAndTapOutside()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1720664
    iget-object v0, p0, LX/AsY;->a:LX/Ase;

    iget-object v0, v0, LX/Ase;->e:Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;

    iget-object v3, p0, LX/AsY;->a:LX/Ase;

    iget-object v3, v3, LX/Ase;->m:LX/Asb;

    invoke-virtual {v0, v3}, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->c(LX/Asb;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1720665
    iget-object v0, p0, LX/AsY;->a:LX/Ase;

    iget-object v0, v0, LX/Ase;->e:Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;

    iget-object v3, p0, LX/AsY;->a:LX/Ase;

    iget-object v3, v3, LX/Ase;->m:LX/Asb;

    invoke-virtual {v0, v3}, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->a(LX/Asb;)V

    .line 1720666
    iget-object v0, p0, LX/AsY;->a:LX/Ase;

    iget-object v0, v0, LX/Ase;->e:Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;

    iget-object v3, p0, LX/AsY;->a:LX/Ase;

    iget-object v3, v3, LX/Ase;->n:LX/Ard;

    invoke-virtual {v0, v3}, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->c(LX/Ard;)Z

    move-result v0

    if-nez v0, :cond_2

    :goto_1
    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 1720667
    iget-object v0, p0, LX/AsY;->a:LX/Ase;

    iget-object v0, v0, LX/Ase;->e:Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;

    iget-object v1, p0, LX/AsY;->a:LX/Ase;

    iget-object v1, v1, LX/Ase;->n:LX/Ard;

    invoke-virtual {v0, v1}, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->a(LX/Ard;)V

    .line 1720668
    :cond_0
    iget-object v0, p0, LX/AsY;->a:LX/Ase;

    invoke-static {v0}, LX/Ase;->f$redex0(LX/Ase;)V

    .line 1720669
    return-void

    :cond_1
    move v0, v2

    .line 1720670
    goto :goto_0

    :cond_2
    move v1, v2

    .line 1720671
    goto :goto_1
.end method
