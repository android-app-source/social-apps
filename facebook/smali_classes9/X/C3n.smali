.class public final LX/C3n;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/C3p;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/C3o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/C3p",
            "<TE;>.CondensedStoryPhotoComponentImpl;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/C3p;

.field private c:[Ljava/lang/String;

.field private d:I

.field private e:Ljava/util/BitSet;


# direct methods
.method public constructor <init>(LX/C3p;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1846240
    iput-object p1, p0, LX/C3n;->b:LX/C3p;

    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 1846241
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "props"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/C3n;->c:[Ljava/lang/String;

    .line 1846242
    iput v3, p0, LX/C3n;->d:I

    .line 1846243
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/C3n;->d:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/C3n;->e:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/C3n;LX/1De;IILX/C3o;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "II",
            "LX/C3p",
            "<TE;>.CondensedStoryPhotoComponentImpl;)V"
        }
    .end annotation

    .prologue
    .line 1846244
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 1846245
    iput-object p4, p0, LX/C3n;->a:LX/C3o;

    .line 1846246
    iget-object v0, p0, LX/C3n;->e:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 1846247
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/C3n;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "LX/C3p",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 1846248
    iget-object v0, p0, LX/C3n;->a:LX/C3o;

    iput-object p1, v0, LX/C3o;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1846249
    iget-object v0, p0, LX/C3n;->e:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1846250
    return-object p0
.end method

.method public final a(Z)LX/C3n;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "LX/C3p",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 1846251
    iget-object v0, p0, LX/C3n;->a:LX/C3o;

    iput-boolean p1, v0, LX/C3o;->c:Z

    .line 1846252
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 1846253
    invoke-super {p0}, LX/1X5;->a()V

    .line 1846254
    const/4 v0, 0x0

    iput-object v0, p0, LX/C3n;->a:LX/C3o;

    .line 1846255
    iget-object v0, p0, LX/C3n;->b:LX/C3p;

    iget-object v0, v0, LX/C3p;->b:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 1846256
    return-void
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/C3p;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1846257
    iget-object v1, p0, LX/C3n;->e:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/C3n;->e:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/C3n;->d:I

    if-ge v1, v2, :cond_2

    .line 1846258
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1846259
    :goto_0
    iget v2, p0, LX/C3n;->d:I

    if-ge v0, v2, :cond_1

    .line 1846260
    iget-object v2, p0, LX/C3n;->e:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1846261
    iget-object v2, p0, LX/C3n;->c:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1846262
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1846263
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1846264
    :cond_2
    iget-object v0, p0, LX/C3n;->a:LX/C3o;

    .line 1846265
    invoke-virtual {p0}, LX/C3n;->a()V

    .line 1846266
    return-object v0
.end method
