.class public final LX/BBE;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/BB8;

.field private final b:LX/BB0;

.field private final c:LX/BB1;

.field private final d:LX/BB3;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1756766
    new-instance v0, LX/BB8;

    invoke-direct {v0}, LX/BB8;-><init>()V

    invoke-direct {p0, v0}, LX/BBE;-><init>(LX/BB8;)V

    .line 1756767
    return-void
.end method

.method private constructor <init>(LX/BB0;)V
    .locals 5

    .prologue
    .line 1756735
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1756736
    const/4 v0, 0x0

    iput-object v0, p0, LX/BBE;->a:LX/BB8;

    .line 1756737
    iput-object p1, p0, LX/BBE;->b:LX/BB0;

    .line 1756738
    iget-object v0, p1, LX/BB0;->b:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;

    .line 1756739
    new-instance v1, LX/BB1;

    invoke-direct {v1}, LX/BB1;-><init>()V

    .line 1756740
    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->r()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$AppIconModel;

    move-result-object v2

    iput-object v2, v1, LX/BB1;->a:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$AppIconModel;

    .line 1756741
    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->c()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, LX/BB1;->b:Ljava/lang/String;

    .line 1756742
    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->d()LX/0Px;

    move-result-object v2

    iput-object v2, v1, LX/BB1;->c:LX/0Px;

    .line 1756743
    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->e()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, LX/BB1;->d:Ljava/lang/String;

    .line 1756744
    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->hW_()Z

    move-result v2

    iput-boolean v2, v1, LX/BB1;->e:Z

    .line 1756745
    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->hX_()I

    move-result v2

    iput v2, v1, LX/BB1;->f:I

    .line 1756746
    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->s()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NavigationEndpointModel;

    move-result-object v2

    iput-object v2, v1, LX/BB1;->g:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NavigationEndpointModel;

    .line 1756747
    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->t()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NotifImageModel;

    move-result-object v2

    iput-object v2, v1, LX/BB1;->h:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NotifImageModel;

    .line 1756748
    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->u()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NotifOptionSetsModel;

    move-result-object v2

    iput-object v2, v1, LX/BB1;->i:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NotifOptionSetsModel;

    .line 1756749
    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->m()J

    move-result-wide v3

    iput-wide v3, v1, LX/BB1;->j:J

    .line 1756750
    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->v()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;

    move-result-object v2

    iput-object v2, v1, LX/BB1;->k:Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;

    .line 1756751
    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->o()Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v2

    iput-object v2, v1, LX/BB1;->l:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    .line 1756752
    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->w()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$TitleModel;

    move-result-object v2

    iput-object v2, v1, LX/BB1;->m:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$TitleModel;

    .line 1756753
    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->q()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, LX/BB1;->n:Ljava/lang/String;

    .line 1756754
    move-object v0, v1

    .line 1756755
    iput-object v0, p0, LX/BBE;->c:LX/BB1;

    .line 1756756
    iget-object v0, p1, LX/BB0;->b:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;

    if-nez v0, :cond_0

    new-instance v0, LX/BB3;

    invoke-direct {v0}, LX/BB3;-><init>()V

    :goto_0
    iput-object v0, p0, LX/BBE;->d:LX/BB3;

    .line 1756757
    return-void

    .line 1756758
    :cond_0
    iget-object v0, p1, LX/BB0;->b:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->s()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NavigationEndpointModel;

    move-result-object v0

    .line 1756759
    new-instance v1, LX/BB3;

    invoke-direct {v1}, LX/BB3;-><init>()V

    .line 1756760
    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NavigationEndpointModel;->a()LX/0Px;

    move-result-object v2

    iput-object v2, v1, LX/BB3;->a:LX/0Px;

    .line 1756761
    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NavigationEndpointModel;->b()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, LX/BB3;->b:Ljava/lang/String;

    .line 1756762
    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NavigationEndpointModel;->c()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    iput-object v2, v1, LX/BB3;->c:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1756763
    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NavigationEndpointModel;->j()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NavigationEndpointModel$TargetIdModel;

    move-result-object v2

    iput-object v2, v1, LX/BB3;->d:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NavigationEndpointModel$TargetIdModel;

    .line 1756764
    move-object v0, v1

    .line 1756765
    goto :goto_0
.end method

.method private constructor <init>(LX/BB8;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1756729
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1756730
    iput-object p1, p0, LX/BBE;->a:LX/BB8;

    .line 1756731
    iput-object v0, p0, LX/BBE;->b:LX/BB0;

    .line 1756732
    iput-object v0, p0, LX/BBE;->c:LX/BB1;

    .line 1756733
    iput-object v0, p0, LX/BBE;->d:LX/BB3;

    .line 1756734
    return-void
.end method

.method public static a(Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;)LX/BBE;
    .locals 3

    .prologue
    .line 1756712
    invoke-static {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;->g(Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, LX/BBE;

    iget-object v1, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;->c:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel;

    .line 1756713
    new-instance v2, LX/BB0;

    invoke-direct {v2}, LX/BB0;-><init>()V

    .line 1756714
    invoke-virtual {v1}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel;->a()Ljava/lang/String;

    move-result-object p0

    iput-object p0, v2, LX/BB0;->a:Ljava/lang/String;

    .line 1756715
    invoke-virtual {v1}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel;->j()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;

    move-result-object p0

    iput-object p0, v2, LX/BB0;->b:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;

    .line 1756716
    move-object v1, v2

    .line 1756717
    invoke-direct {v0, v1}, LX/BBE;-><init>(LX/BB0;)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/BBE;

    iget-object v1, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;->b:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;

    .line 1756718
    new-instance v2, LX/BB8;

    invoke-direct {v2}, LX/BB8;-><init>()V

    .line 1756719
    invoke-virtual {v1}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;->hZ_()Ljava/lang/String;

    move-result-object p0

    iput-object p0, v2, LX/BB8;->a:Ljava/lang/String;

    .line 1756720
    invoke-virtual {v1}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;->ia_()Ljava/lang/String;

    move-result-object p0

    iput-object p0, v2, LX/BB8;->b:Ljava/lang/String;

    .line 1756721
    invoke-virtual {v1}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;->j()LX/0Px;

    move-result-object p0

    iput-object p0, v2, LX/BB8;->c:LX/0Px;

    .line 1756722
    invoke-virtual {v1}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;->k()Z

    move-result p0

    iput-boolean p0, v2, LX/BB8;->d:Z

    .line 1756723
    invoke-virtual {v1}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;->l()I

    move-result p0

    iput p0, v2, LX/BB8;->e:I

    .line 1756724
    invoke-virtual {v1}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object p0

    iput-object p0, v2, LX/BB8;->f:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1756725
    invoke-virtual {v1}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;->p()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel$NotifOptionSetsModel;

    move-result-object p0

    iput-object p0, v2, LX/BB8;->g:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel$NotifOptionSetsModel;

    .line 1756726
    invoke-virtual {v1}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;->q()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;

    move-result-object p0

    iput-object p0, v2, LX/BB8;->h:Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;

    .line 1756727
    move-object v1, v2

    .line 1756728
    invoke-direct {v0, v1}, LX/BBE;-><init>(LX/BB8;)V

    goto :goto_0
.end method

.method private b()Z
    .locals 1

    .prologue
    .line 1756711
    iget-object v0, p0, LX/BBE;->b:LX/BB0;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(I)LX/BBE;
    .locals 1

    .prologue
    .line 1756704
    invoke-direct {p0}, LX/BBE;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1756705
    iget-object v0, p0, LX/BBE;->c:LX/BB1;

    .line 1756706
    iput p1, v0, LX/BB1;->f:I

    .line 1756707
    :goto_0
    return-object p0

    .line 1756708
    :cond_0
    iget-object v0, p0, LX/BBE;->a:LX/BB8;

    .line 1756709
    iput p1, v0, LX/BB8;->e:I

    .line 1756710
    goto :goto_0
.end method

.method public final a(LX/0Px;)LX/BBE;
    .locals 1
    .param p1    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;",
            ">;)",
            "LX/BBE;"
        }
    .end annotation

    .prologue
    .line 1756768
    invoke-direct {p0}, LX/BBE;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1756769
    iget-object v0, p0, LX/BBE;->c:LX/BB1;

    .line 1756770
    iput-object p1, v0, LX/BB1;->c:LX/0Px;

    .line 1756771
    :goto_0
    return-object p0

    .line 1756772
    :cond_0
    iget-object v0, p0, LX/BBE;->a:LX/BB8;

    .line 1756773
    iput-object p1, v0, LX/BB8;->c:LX/0Px;

    .line 1756774
    goto :goto_0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStory;)LX/BBE;
    .locals 1
    .param p1    # Lcom/facebook/graphql/model/GraphQLStory;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1756697
    invoke-direct {p0}, LX/BBE;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1756698
    iget-object v0, p0, LX/BBE;->d:LX/BB3;

    .line 1756699
    iput-object p1, v0, LX/BB3;->c:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1756700
    :goto_0
    return-object p0

    .line 1756701
    :cond_0
    iget-object v0, p0, LX/BBE;->a:LX/BB8;

    .line 1756702
    iput-object p1, v0, LX/BB8;->f:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1756703
    goto :goto_0
.end method

.method public final a(Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel;)LX/BBE;
    .locals 2
    .param p1    # Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 1756689
    invoke-direct {p0}, LX/BBE;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1756690
    iget-object v1, p0, LX/BBE;->c:LX/BB1;

    if-nez p1, :cond_0

    .line 1756691
    :goto_0
    iput-object v0, v1, LX/BB1;->i:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NotifOptionSetsModel;

    .line 1756692
    :goto_1
    return-object p0

    .line 1756693
    :cond_0
    iget-object v0, p1, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel;->b:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NotifOptionSetsModel;

    goto :goto_0

    .line 1756694
    :cond_1
    iget-object v1, p0, LX/BBE;->a:LX/BB8;

    if-nez p1, :cond_2

    .line 1756695
    :goto_2
    iput-object v0, v1, LX/BB8;->g:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel$NotifOptionSetsModel;

    .line 1756696
    goto :goto_1

    :cond_2
    iget-object v0, p1, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel;->a:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel$NotifOptionSetsModel;

    goto :goto_2
.end method

.method public final a(Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;)LX/BBE;
    .locals 1
    .param p1    # Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1756682
    invoke-direct {p0}, LX/BBE;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1756683
    iget-object v0, p0, LX/BBE;->c:LX/BB1;

    .line 1756684
    iput-object p1, v0, LX/BB1;->k:Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;

    .line 1756685
    :goto_0
    return-object p0

    .line 1756686
    :cond_0
    iget-object v0, p0, LX/BBE;->a:LX/BB8;

    .line 1756687
    iput-object p1, v0, LX/BB8;->h:Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;

    .line 1756688
    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)LX/BBE;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1756675
    invoke-direct {p0}, LX/BBE;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1756676
    iget-object v0, p0, LX/BBE;->c:LX/BB1;

    .line 1756677
    iput-object p1, v0, LX/BB1;->b:Ljava/lang/String;

    .line 1756678
    :goto_0
    return-object p0

    .line 1756679
    :cond_0
    iget-object v0, p0, LX/BBE;->a:LX/BB8;

    .line 1756680
    iput-object p1, v0, LX/BB8;->a:Ljava/lang/String;

    .line 1756681
    goto :goto_0
.end method

.method public final a(Z)LX/BBE;
    .locals 1

    .prologue
    .line 1756668
    invoke-direct {p0}, LX/BBE;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1756669
    iget-object v0, p0, LX/BBE;->c:LX/BB1;

    .line 1756670
    iput-boolean p1, v0, LX/BB1;->e:Z

    .line 1756671
    :goto_0
    return-object p0

    .line 1756672
    :cond_0
    iget-object v0, p0, LX/BBE;->a:LX/BB8;

    .line 1756673
    iput-boolean p1, v0, LX/BB8;->d:Z

    .line 1756674
    goto :goto_0
.end method

.method public final a()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;
    .locals 15

    .prologue
    .line 1756594
    invoke-direct {p0}, LX/BBE;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1756595
    iget-object v0, p0, LX/BBE;->c:LX/BB1;

    iget-object v1, p0, LX/BBE;->d:LX/BB3;

    const/4 v6, 0x1

    const/4 v10, 0x0

    const/4 v4, 0x0

    .line 1756596
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1756597
    iget-object v3, v1, LX/BB3;->a:LX/0Px;

    invoke-static {v2, v3}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v3

    .line 1756598
    iget-object v5, v1, LX/BB3;->b:Ljava/lang/String;

    invoke-virtual {v2, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1756599
    iget-object v7, v1, LX/BB3;->c:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v2, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 1756600
    iget-object v8, v1, LX/BB3;->d:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NavigationEndpointModel$TargetIdModel;

    invoke-static {v2, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 1756601
    const/4 v9, 0x4

    invoke-virtual {v2, v9}, LX/186;->c(I)V

    .line 1756602
    invoke-virtual {v2, v10, v3}, LX/186;->b(II)V

    .line 1756603
    invoke-virtual {v2, v6, v5}, LX/186;->b(II)V

    .line 1756604
    const/4 v3, 0x2

    invoke-virtual {v2, v3, v7}, LX/186;->b(II)V

    .line 1756605
    const/4 v3, 0x3

    invoke-virtual {v2, v3, v8}, LX/186;->b(II)V

    .line 1756606
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1756607
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1756608
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1756609
    invoke-virtual {v3, v10}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1756610
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1756611
    new-instance v3, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NavigationEndpointModel;

    invoke-direct {v3, v2}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NavigationEndpointModel;-><init>(LX/15i;)V

    .line 1756612
    iget-object v2, v1, LX/BB3;->c:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v2, :cond_0

    .line 1756613
    invoke-virtual {v3}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NavigationEndpointModel;->c()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    iget-object v4, v1, LX/BB3;->c:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->L_()LX/0x2;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/facebook/graphql/model/GraphQLStory;->a(LX/0x2;)V

    .line 1756614
    :cond_0
    move-object v1, v3

    .line 1756615
    iput-object v1, v0, LX/BB1;->g:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NavigationEndpointModel;

    .line 1756616
    iget-object v0, p0, LX/BBE;->b:LX/BB0;

    iget-object v1, p0, LX/BBE;->c:LX/BB1;

    .line 1756617
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1756618
    iget-object v3, v1, LX/BB1;->a:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$AppIconModel;

    invoke-static {v2, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1756619
    iget-object v4, v1, LX/BB1;->b:Ljava/lang/String;

    invoke-virtual {v2, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1756620
    iget-object v5, v1, LX/BB1;->c:LX/0Px;

    invoke-virtual {v2, v5}, LX/186;->c(Ljava/util/List;)I

    move-result v5

    .line 1756621
    iget-object v6, v1, LX/BB1;->d:Ljava/lang/String;

    invoke-virtual {v2, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1756622
    iget-object v7, v1, LX/BB1;->g:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NavigationEndpointModel;

    invoke-static {v2, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 1756623
    iget-object v8, v1, LX/BB1;->h:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NotifImageModel;

    invoke-static {v2, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 1756624
    iget-object v9, v1, LX/BB1;->i:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NotifOptionSetsModel;

    invoke-static {v2, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 1756625
    iget-object v10, v1, LX/BB1;->k:Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;

    invoke-static {v2, v10}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 1756626
    iget-object v11, v1, LX/BB1;->l:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    invoke-virtual {v2, v11}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v11

    .line 1756627
    iget-object v12, v1, LX/BB1;->m:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$TitleModel;

    invoke-static {v2, v12}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 1756628
    iget-object v13, v1, LX/BB1;->n:Ljava/lang/String;

    invoke-virtual {v2, v13}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    .line 1756629
    const/16 v14, 0xe

    invoke-virtual {v2, v14}, LX/186;->c(I)V

    .line 1756630
    const/4 v14, 0x0

    invoke-virtual {v2, v14, v3}, LX/186;->b(II)V

    .line 1756631
    const/4 v3, 0x1

    invoke-virtual {v2, v3, v4}, LX/186;->b(II)V

    .line 1756632
    const/4 v3, 0x2

    invoke-virtual {v2, v3, v5}, LX/186;->b(II)V

    .line 1756633
    const/4 v3, 0x3

    invoke-virtual {v2, v3, v6}, LX/186;->b(II)V

    .line 1756634
    const/4 v3, 0x4

    iget-boolean v4, v1, LX/BB1;->e:Z

    invoke-virtual {v2, v3, v4}, LX/186;->a(IZ)V

    .line 1756635
    const/4 v3, 0x5

    iget v4, v1, LX/BB1;->f:I

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, LX/186;->a(III)V

    .line 1756636
    const/4 v3, 0x6

    invoke-virtual {v2, v3, v7}, LX/186;->b(II)V

    .line 1756637
    const/4 v3, 0x7

    invoke-virtual {v2, v3, v8}, LX/186;->b(II)V

    .line 1756638
    const/16 v3, 0x8

    invoke-virtual {v2, v3, v9}, LX/186;->b(II)V

    .line 1756639
    const/16 v3, 0x9

    iget-wide v4, v1, LX/BB1;->j:J

    const-wide/16 v6, 0x0

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1756640
    const/16 v3, 0xa

    invoke-virtual {v2, v3, v10}, LX/186;->b(II)V

    .line 1756641
    const/16 v3, 0xb

    invoke-virtual {v2, v3, v11}, LX/186;->b(II)V

    .line 1756642
    const/16 v3, 0xc

    invoke-virtual {v2, v3, v12}, LX/186;->b(II)V

    .line 1756643
    const/16 v3, 0xd

    invoke-virtual {v2, v3, v13}, LX/186;->b(II)V

    .line 1756644
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1756645
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1756646
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1756647
    const/4 v2, 0x0

    invoke-virtual {v3, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1756648
    new-instance v2, LX/15i;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1756649
    new-instance v3, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;

    invoke-direct {v3, v2}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;-><init>(LX/15i;)V

    .line 1756650
    move-object v1, v3

    .line 1756651
    iput-object v1, v0, LX/BB0;->b:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;

    .line 1756652
    new-instance v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;

    iget-object v1, p0, LX/BBE;->b:LX/BB0;

    const/4 v6, 0x1

    const/4 v8, 0x0

    const/4 v4, 0x0

    .line 1756653
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1756654
    iget-object v3, v1, LX/BB0;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1756655
    iget-object v5, v1, LX/BB0;->b:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;

    invoke-static {v2, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1756656
    const/4 v7, 0x2

    invoke-virtual {v2, v7}, LX/186;->c(I)V

    .line 1756657
    invoke-virtual {v2, v8, v3}, LX/186;->b(II)V

    .line 1756658
    invoke-virtual {v2, v6, v5}, LX/186;->b(II)V

    .line 1756659
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1756660
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1756661
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1756662
    invoke-virtual {v3, v8}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1756663
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1756664
    new-instance v3, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel;

    invoke-direct {v3, v2}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel;-><init>(LX/15i;)V

    .line 1756665
    move-object v1, v3

    .line 1756666
    invoke-direct {v0, v1}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;-><init>(Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel;)V

    .line 1756667
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;

    iget-object v1, p0, LX/BBE;->a:LX/BB8;

    invoke-virtual {v1}, LX/BB8;->a()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;-><init>(Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)LX/BBE;
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1756587
    invoke-direct {p0}, LX/BBE;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1756588
    iget-object v0, p0, LX/BBE;->b:LX/BB0;

    .line 1756589
    iput-object p1, v0, LX/BB0;->a:Ljava/lang/String;

    .line 1756590
    :goto_0
    return-object p0

    .line 1756591
    :cond_0
    iget-object v0, p0, LX/BBE;->a:LX/BB8;

    .line 1756592
    iput-object p1, v0, LX/BB8;->b:Ljava/lang/String;

    .line 1756593
    goto :goto_0
.end method
