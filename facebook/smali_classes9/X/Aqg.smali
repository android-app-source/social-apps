.class public LX/Aqg;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1718189
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1718190
    iput-object p1, p0, LX/Aqg;->a:LX/0Zb;

    .line 1718191
    return-void
.end method


# virtual methods
.method public final e(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1718192
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "gif_picker_giphy_returned_error"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "session_id"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 1718193
    iget-object v1, p0, LX/Aqg;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1718194
    return-void
.end method
