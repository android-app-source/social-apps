.class public LX/BQB;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static I:LX/0Xm;


# instance fields
.field public A:Z

.field public B:Z

.field public final C:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public D:Z

.field public E:Z

.field public F:Ljava/lang/String;

.field public G:I

.field public H:Ljava/lang/String;

.field private a:LX/BQA;

.field public final b:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field public final c:LX/BQD;

.field public final d:LX/0kb;

.field public final e:LX/0So;

.field public final f:LX/BPC;

.field public final g:LX/BQ7;

.field public h:Lcom/facebook/timeline/protocol/ResultSource;

.field public i:Lcom/facebook/timeline/protocol/ResultSource;

.field public j:LX/9lQ;

.field public k:Z

.field public l:Z

.field private m:Z

.field private n:Z

.field private o:Z

.field private p:Z

.field public q:Z

.field public r:Z

.field public s:Z

.field private t:Z

.field public u:Z

.field private v:Z

.field public w:Z

.field public x:Z

.field private y:Z

.field public z:Z


# direct methods
.method public constructor <init>(Lcom/facebook/quicklog/QuickPerformanceLogger;LX/BQD;LX/0kb;LX/0So;LX/BPC;LX/BQ7;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1781880
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1781881
    sget-object v0, Lcom/facebook/timeline/protocol/ResultSource;->UNDEFINED:Lcom/facebook/timeline/protocol/ResultSource;

    iput-object v0, p0, LX/BQB;->h:Lcom/facebook/timeline/protocol/ResultSource;

    .line 1781882
    sget-object v0, Lcom/facebook/timeline/protocol/ResultSource;->UNDEFINED:Lcom/facebook/timeline/protocol/ResultSource;

    iput-object v0, p0, LX/BQB;->i:Lcom/facebook/timeline/protocol/ResultSource;

    .line 1781883
    sget-object v0, LX/9lQ;->UNDEFINED:LX/9lQ;

    iput-object v0, p0, LX/BQB;->j:LX/9lQ;

    .line 1781884
    iput-boolean v1, p0, LX/BQB;->k:Z

    .line 1781885
    iput-boolean v1, p0, LX/BQB;->l:Z

    .line 1781886
    iput-boolean v1, p0, LX/BQB;->m:Z

    .line 1781887
    iput-boolean v1, p0, LX/BQB;->n:Z

    .line 1781888
    iput-boolean v1, p0, LX/BQB;->o:Z

    .line 1781889
    iput-boolean v1, p0, LX/BQB;->p:Z

    .line 1781890
    iput-boolean v1, p0, LX/BQB;->q:Z

    .line 1781891
    iput-boolean v1, p0, LX/BQB;->r:Z

    .line 1781892
    iput-boolean v1, p0, LX/BQB;->s:Z

    .line 1781893
    iput-boolean v1, p0, LX/BQB;->t:Z

    .line 1781894
    iput-boolean v1, p0, LX/BQB;->u:Z

    .line 1781895
    iput-boolean v1, p0, LX/BQB;->v:Z

    .line 1781896
    iput-boolean v1, p0, LX/BQB;->w:Z

    .line 1781897
    iput-boolean v1, p0, LX/BQB;->x:Z

    .line 1781898
    iput-boolean v1, p0, LX/BQB;->y:Z

    .line 1781899
    iput-boolean v1, p0, LX/BQB;->z:Z

    .line 1781900
    iput-boolean v1, p0, LX/BQB;->A:Z

    .line 1781901
    iput-boolean v1, p0, LX/BQB;->B:Z

    .line 1781902
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LX/BQB;->C:Ljava/util/Set;

    .line 1781903
    iput-boolean v1, p0, LX/BQB;->D:Z

    .line 1781904
    iput-boolean v1, p0, LX/BQB;->E:Z

    .line 1781905
    const-string v0, "NA"

    iput-object v0, p0, LX/BQB;->F:Ljava/lang/String;

    .line 1781906
    const/4 v0, -0x1

    iput v0, p0, LX/BQB;->G:I

    .line 1781907
    const-string v0, "NA"

    iput-object v0, p0, LX/BQB;->H:Ljava/lang/String;

    .line 1781908
    iput-object p1, p0, LX/BQB;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 1781909
    iput-object p2, p0, LX/BQB;->c:LX/BQD;

    .line 1781910
    iput-object p3, p0, LX/BQB;->d:LX/0kb;

    .line 1781911
    iput-object p4, p0, LX/BQB;->e:LX/0So;

    .line 1781912
    iput-object p5, p0, LX/BQB;->f:LX/BPC;

    .line 1781913
    iput-object p6, p0, LX/BQB;->g:LX/BQ7;

    .line 1781914
    return-void
.end method

.method public static a(LX/0QB;)LX/BQB;
    .locals 10

    .prologue
    .line 1781869
    const-class v1, LX/BQB;

    monitor-enter v1

    .line 1781870
    :try_start_0
    sget-object v0, LX/BQB;->I:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1781871
    sput-object v2, LX/BQB;->I:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1781872
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1781873
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1781874
    new-instance v3, LX/BQB;

    invoke-static {v0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v4

    check-cast v4, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static {v0}, LX/BQD;->a(LX/0QB;)LX/BQD;

    move-result-object v5

    check-cast v5, LX/BQD;

    invoke-static {v0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v6

    check-cast v6, LX/0kb;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v7

    check-cast v7, LX/0So;

    invoke-static {v0}, LX/BPC;->a(LX/0QB;)LX/BPC;

    move-result-object v8

    check-cast v8, LX/BPC;

    invoke-static {v0}, LX/BQ7;->a(LX/0QB;)LX/BQ7;

    move-result-object v9

    check-cast v9, LX/BQ7;

    invoke-direct/range {v3 .. v9}, LX/BQB;-><init>(Lcom/facebook/quicklog/QuickPerformanceLogger;LX/BQD;LX/0kb;LX/0So;LX/BPC;LX/BQ7;)V

    .line 1781875
    move-object v0, v3

    .line 1781876
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1781877
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/BQB;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1781878
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1781879
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static f(LX/BQB;Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v2, 0x0

    const v1, 0x1a0028

    .line 1781702
    iget-boolean v0, p0, LX/BQB;->B:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/BQB;->w:Z

    if-eqz v0, :cond_0

    .line 1781703
    iput-boolean v6, p0, LX/BQB;->B:Z

    .line 1781704
    iget-boolean v0, p0, LX/BQB;->q:Z

    if-eqz v0, :cond_1

    .line 1781705
    iget-object v0, p0, LX/BQB;->e:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v4

    .line 1781706
    iget-object v0, p0, LX/BQB;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, v1, v2, v4, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->markerStart(IIJ)V

    .line 1781707
    iget-object v0, p0, LX/BQB;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/4 v3, 0x2

    invoke-interface/range {v0 .. v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->markerEnd(IISJ)V

    .line 1781708
    iget-object v0, p0, LX/BQB;->c:LX/BQD;

    const-string v1, "ProtilesWaitTime"

    invoke-static {p1}, LX/BQB;->h(Ljava/lang/String;)LX/0P1;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/BQD;->a(Ljava/lang/String;LX/0P1;)V

    .line 1781709
    iget-object v0, p0, LX/BQB;->c:LX/BQD;

    const-string v1, "ProtilesWaitTime"

    invoke-virtual {v0, v1}, LX/BQD;->b(Ljava/lang/String;)V

    .line 1781710
    iput-boolean v6, p0, LX/BQB;->x:Z

    .line 1781711
    invoke-static {p0}, LX/BQB;->w(LX/BQB;)V

    .line 1781712
    :goto_0
    iget-object v0, p0, LX/BQB;->c:LX/BQD;

    const-string v1, "ProtilesWaitTimeStarted"

    invoke-virtual {v0, v1}, LX/BQD;->f(Ljava/lang/String;)V

    .line 1781713
    :cond_0
    return-void

    .line 1781714
    :cond_1
    iget-object v0, p0, LX/BQB;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 1781715
    iget-object v0, p0, LX/BQB;->c:LX/BQD;

    const-string v1, "ProtilesWaitTime"

    invoke-static {p1}, LX/BQB;->h(Ljava/lang/String;)LX/0P1;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/BQD;->a(Ljava/lang/String;LX/0P1;)V

    goto :goto_0
.end method

.method public static g(LX/BQB;Ljava/lang/String;)V
    .locals 9

    .prologue
    .line 1781853
    iget-boolean v0, p0, LX/BQB;->A:Z

    if-nez v0, :cond_0

    .line 1781854
    iget-boolean v0, p0, LX/BQB;->y:Z

    if-eqz v0, :cond_0

    .line 1781855
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/BQB;->A:Z

    .line 1781856
    iget-boolean v0, p0, LX/BQB;->s:Z

    if-eqz v0, :cond_1

    .line 1781857
    invoke-static {p1}, LX/BQB;->h(Ljava/lang/String;)LX/0P1;

    move-result-object v0

    const v4, 0x1a0002

    const/4 v5, 0x0

    .line 1781858
    iget-object v3, p0, LX/BQB;->e:LX/0So;

    invoke-interface {v3}, LX/0So;->now()J

    move-result-wide v7

    .line 1781859
    iget-object v3, p0, LX/BQB;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v3, v4, v5, v7, v8}, Lcom/facebook/quicklog/QuickPerformanceLogger;->markerStart(IIJ)V

    .line 1781860
    iget-object v3, p0, LX/BQB;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/4 v6, 0x2

    invoke-interface/range {v3 .. v8}, Lcom/facebook/quicklog/QuickPerformanceLogger;->markerEnd(IISJ)V

    .line 1781861
    iget-object v3, p0, LX/BQB;->c:LX/BQD;

    const-string v4, "TimelineFetchInitialUnitsWaitTime"

    invoke-virtual {v3, v4, v0}, LX/BQD;->a(Ljava/lang/String;LX/0P1;)V

    .line 1781862
    iget-object v3, p0, LX/BQB;->c:LX/BQD;

    const-string v4, "TimelineFetchInitialUnitsWaitTime"

    invoke-virtual {v3, v4}, LX/BQD;->b(Ljava/lang/String;)V

    .line 1781863
    const/4 v3, 0x1

    iput-boolean v3, p0, LX/BQB;->z:Z

    .line 1781864
    invoke-static {p0}, LX/BQB;->w(LX/BQB;)V

    .line 1781865
    :goto_0
    iget-object v0, p0, LX/BQB;->c:LX/BQD;

    const-string v1, "TimelineFetchInitialUnitsWaitTimeStarted"

    invoke-virtual {v0, v1}, LX/BQD;->f(Ljava/lang/String;)V

    .line 1781866
    :cond_0
    return-void

    .line 1781867
    :cond_1
    iget-object v0, p0, LX/BQB;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x1a0002

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 1781868
    iget-object v0, p0, LX/BQB;->c:LX/BQD;

    const-string v1, "TimelineFetchInitialUnitsWaitTime"

    invoke-static {p1}, LX/BQB;->h(Ljava/lang/String;)LX/0P1;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/BQD;->a(Ljava/lang/String;LX/0P1;)V

    goto :goto_0
.end method

.method private static h(Ljava/lang/String;)LX/0P1;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1781852
    new-instance v0, LX/0P2;

    invoke-direct {v0}, LX/0P2;-><init>()V

    const-string v1, "initial_units_wait_time_start_reason"

    invoke-virtual {v0, v1, p0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    return-object v0
.end method

.method public static i(LX/BQB;Ljava/lang/String;)V
    .locals 9

    .prologue
    const v7, 0x1a001a

    const v6, 0x1a000e

    const v5, 0x1a000d

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 1781816
    iget-boolean v0, p0, LX/BQB;->n:Z

    if-eqz v0, :cond_1

    .line 1781817
    :cond_0
    :goto_0
    return-void

    .line 1781818
    :cond_1
    iget-object v0, p0, LX/BQB;->f:LX/BPC;

    invoke-virtual {v0}, LX/BPC;->h()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1781819
    iget-object v0, p0, LX/BQB;->c:LX/BQD;

    const-string v1, "TimelineRenderLowResHeader"

    invoke-virtual {v0, v1}, LX/BQD;->d(Ljava/lang/String;)V

    .line 1781820
    iget-object v0, p0, LX/BQB;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/4 v1, 0x3

    invoke-interface {v0, v7, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 1781821
    iget-object v0, p0, LX/BQB;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->d(I)V

    .line 1781822
    iget-object v0, p0, LX/BQB;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, v6}, Lcom/facebook/quicklog/QuickPerformanceLogger;->d(I)V

    .line 1781823
    iget-object v0, p0, LX/BQB;->c:LX/BQD;

    const-string v1, "TimelineRenderLowResHeaderCoverphotoOptional"

    invoke-virtual {v0, v1}, LX/BQD;->d(Ljava/lang/String;)V

    .line 1781824
    iput-boolean v3, p0, LX/BQB;->n:Z

    .line 1781825
    iput-boolean v3, p0, LX/BQB;->m:Z

    goto :goto_0

    .line 1781826
    :cond_2
    iget-object v0, p0, LX/BQB;->f:LX/BPC;

    invoke-virtual {v0}, LX/BPC;->j()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-boolean v0, p0, LX/BQB;->m:Z

    if-nez v0, :cond_3

    .line 1781827
    new-instance v0, LX/0P2;

    invoke-direct {v0}, LX/0P2;-><init>()V

    const-string v1, "source_header"

    iget-object v2, p0, LX/BQB;->h:Lcom/facebook/timeline/protocol/ResultSource;

    invoke-virtual {v2}, Lcom/facebook/timeline/protocol/ResultSource;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "relationship_type"

    iget-object v2, p0, LX/BQB;->j:LX/9lQ;

    invoke-virtual {v2}, LX/9lQ;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "last_finish_element"

    invoke-virtual {v0, v1, p1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "profile pic load state"

    iget-object v2, p0, LX/BQB;->f:LX/BPC;

    .line 1781828
    iget-object v8, v2, LX/BPC;->d:LX/BPA;

    move-object v2, v8

    .line 1781829
    invoke-virtual {v2}, LX/BPA;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "cover photo load state"

    iget-object v2, p0, LX/BQB;->f:LX/BPC;

    .line 1781830
    iget-object v8, v2, LX/BPC;->e:LX/BPA;

    move-object v2, v8

    .line 1781831
    invoke-virtual {v2}, LX/BPA;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    .line 1781832
    iget-object v1, p0, LX/BQB;->c:LX/BQD;

    const-string v2, "TimelineRenderLowResHeaderCoverphotoOptional"

    invoke-virtual {v1, v2, v0}, LX/BQD;->b(Ljava/lang/String;LX/0P1;)V

    .line 1781833
    iput-boolean v3, p0, LX/BQB;->m:Z

    .line 1781834
    :cond_3
    iget-object v0, p0, LX/BQB;->f:LX/BPC;

    invoke-virtual {v0}, LX/BPC;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/BQB;->n:Z

    if-nez v0, :cond_0

    .line 1781835
    iget-object v0, p0, LX/BQB;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, v7, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 1781836
    new-instance v0, LX/0P2;

    invoke-direct {v0}, LX/0P2;-><init>()V

    const-string v1, "source_header"

    iget-object v2, p0, LX/BQB;->h:Lcom/facebook/timeline/protocol/ResultSource;

    invoke-virtual {v2}, Lcom/facebook/timeline/protocol/ResultSource;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "relationship_type"

    iget-object v2, p0, LX/BQB;->j:LX/9lQ;

    invoke-virtual {v2}, LX/9lQ;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "last_finish_element"

    invoke-virtual {v0, v1, p1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "profile pic load state"

    iget-object v2, p0, LX/BQB;->f:LX/BPC;

    .line 1781837
    iget-object v7, v2, LX/BPC;->d:LX/BPA;

    move-object v2, v7

    .line 1781838
    invoke-virtual {v2}, LX/BPA;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "cover photo load state"

    iget-object v2, p0, LX/BQB;->f:LX/BPC;

    .line 1781839
    iget-object v7, v2, LX/BPC;->e:LX/BPA;

    move-object v2, v7

    .line 1781840
    invoke-virtual {v2}, LX/BPA;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "intro_bio_view_type"

    iget-object v2, p0, LX/BQB;->F:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "intro_num_about_items"

    iget v2, p0, LX/BQB;->G:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "intro_photos_view_type"

    iget-object v2, p0, LX/BQB;->H:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    .line 1781841
    iget-object v1, p0, LX/BQB;->c:LX/BQD;

    const-string v2, "TimelineRenderLowResHeader"

    invoke-virtual {v1, v2, v0}, LX/BQD;->b(Ljava/lang/String;LX/0P1;)V

    .line 1781842
    iget-object v0, p0, LX/BQB;->g:LX/BQ7;

    const-string v1, "TimelineRenderLowResHeader"

    invoke-virtual {v0, v1}, LX/BQ7;->b(Ljava/lang/String;)V

    .line 1781843
    iget-object v0, p0, LX/BQB;->c:LX/BQD;

    .line 1781844
    iget-object v1, v0, LX/BQD;->b:LX/0id;

    const-string v2, "LoadTimelineHeader"

    invoke-virtual {v1, v2, p1}, LX/0id;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1781845
    iput-boolean v3, p0, LX/BQB;->n:Z

    .line 1781846
    sget-object v0, Lcom/facebook/timeline/protocol/ResultSource;->SERVER:Lcom/facebook/timeline/protocol/ResultSource;

    iget-object v1, p0, LX/BQB;->h:Lcom/facebook/timeline/protocol/ResultSource;

    invoke-virtual {v0, v1}, Lcom/facebook/timeline/protocol/ResultSource;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1781847
    iget-object v0, p0, LX/BQB;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, v5, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 1781848
    iget-object v0, p0, LX/BQB;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, v6}, Lcom/facebook/quicklog/QuickPerformanceLogger;->d(I)V

    goto/16 :goto_0

    .line 1781849
    :cond_4
    sget-object v0, Lcom/facebook/timeline/protocol/ResultSource;->DISK_CACHE:Lcom/facebook/timeline/protocol/ResultSource;

    iget-object v1, p0, LX/BQB;->h:Lcom/facebook/timeline/protocol/ResultSource;

    invoke-virtual {v0, v1}, Lcom/facebook/timeline/protocol/ResultSource;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1781850
    iget-object v0, p0, LX/BQB;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, v6, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 1781851
    iget-object v0, p0, LX/BQB;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->d(I)V

    goto/16 :goto_0
.end method

.method public static v(LX/BQB;)V
    .locals 7

    .prologue
    const v2, 0x1a0009

    const/4 v6, 0x1

    const v5, 0x1a000b

    const v4, 0x1a000a

    const/4 v3, 0x2

    .line 1781796
    iget-boolean v0, p0, LX/BQB;->o:Z

    if-eqz v0, :cond_0

    .line 1781797
    :goto_0
    return-void

    .line 1781798
    :cond_0
    iget-object v0, p0, LX/BQB;->f:LX/BPC;

    invoke-virtual {v0}, LX/BPC;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1781799
    iget-object v0, p0, LX/BQB;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/4 v1, 0x3

    invoke-interface {v0, v2, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 1781800
    iget-object v0, p0, LX/BQB;->c:LX/BQD;

    const-string v1, "TimelineRenderEntireHeader"

    invoke-virtual {v0, v1}, LX/BQD;->d(Ljava/lang/String;)V

    .line 1781801
    iget-object v0, p0, LX/BQB;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->d(I)V

    .line 1781802
    iget-object v0, p0, LX/BQB;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->d(I)V

    .line 1781803
    iput-boolean v6, p0, LX/BQB;->o:Z

    goto :goto_0

    .line 1781804
    :cond_1
    iget-object v0, p0, LX/BQB;->f:LX/BPC;

    invoke-virtual {v0}, LX/BPC;->l()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-boolean v0, p0, LX/BQB;->o:Z

    if-nez v0, :cond_3

    .line 1781805
    iget-object v0, p0, LX/BQB;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 1781806
    new-instance v0, LX/0P2;

    invoke-direct {v0}, LX/0P2;-><init>()V

    const-string v1, "source_header"

    iget-object v2, p0, LX/BQB;->h:Lcom/facebook/timeline/protocol/ResultSource;

    invoke-virtual {v2}, Lcom/facebook/timeline/protocol/ResultSource;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "source_initial_units"

    iget-object v2, p0, LX/BQB;->i:Lcom/facebook/timeline/protocol/ResultSource;

    invoke-virtual {v2}, Lcom/facebook/timeline/protocol/ResultSource;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "relationship_type"

    iget-object v2, p0, LX/BQB;->j:LX/9lQ;

    invoke-virtual {v2}, LX/9lQ;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    .line 1781807
    iget-object v1, p0, LX/BQB;->c:LX/BQD;

    const-string v2, "TimelineRenderEntireHeader"

    invoke-virtual {v1, v2, v0}, LX/BQD;->b(Ljava/lang/String;LX/0P1;)V

    .line 1781808
    sget-object v0, Lcom/facebook/timeline/protocol/ResultSource;->SERVER:Lcom/facebook/timeline/protocol/ResultSource;

    iget-object v1, p0, LX/BQB;->h:Lcom/facebook/timeline/protocol/ResultSource;

    invoke-virtual {v0, v1}, Lcom/facebook/timeline/protocol/ResultSource;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1781809
    iget-object v0, p0, LX/BQB;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, v4, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 1781810
    iget-object v0, p0, LX/BQB;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->d(I)V

    .line 1781811
    :cond_2
    :goto_1
    iput-boolean v6, p0, LX/BQB;->o:Z

    .line 1781812
    :cond_3
    invoke-static {p0}, LX/BQB;->w(LX/BQB;)V

    goto/16 :goto_0

    .line 1781813
    :cond_4
    sget-object v0, Lcom/facebook/timeline/protocol/ResultSource;->DISK_CACHE:Lcom/facebook/timeline/protocol/ResultSource;

    iget-object v1, p0, LX/BQB;->h:Lcom/facebook/timeline/protocol/ResultSource;

    invoke-virtual {v0, v1}, Lcom/facebook/timeline/protocol/ResultSource;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1781814
    iget-object v0, p0, LX/BQB;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, v5, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 1781815
    iget-object v0, p0, LX/BQB;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v0, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->d(I)V

    goto :goto_1
.end method

.method public static w(LX/BQB;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 1781786
    iget-boolean v0, p0, LX/BQB;->p:Z

    if-eqz v0, :cond_1

    .line 1781787
    :cond_0
    :goto_0
    return-void

    .line 1781788
    :cond_1
    iget-object v0, p0, LX/BQB;->f:LX/BPC;

    invoke-virtual {v0}, LX/BPC;->h()Z

    move-result v0

    if-nez v0, :cond_2

    iget-boolean v0, p0, LX/BQB;->t:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, LX/BQB;->r:Z

    if-eqz v0, :cond_3

    .line 1781789
    :cond_2
    iget-object v0, p0, LX/BQB;->c:LX/BQD;

    .line 1781790
    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-static {v0, v2, v3}, LX/BQD;->a(LX/BQD;ZZ)V

    .line 1781791
    iput-boolean v1, p0, LX/BQB;->p:Z

    .line 1781792
    :cond_3
    iget-object v0, p0, LX/BQB;->f:LX/BPC;

    invoke-virtual {v0}, LX/BPC;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/BQB;->x:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/BQB;->z:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/BQB;->p:Z

    if-nez v0, :cond_0

    .line 1781793
    iget-object v0, p0, LX/BQB;->c:LX/BQD;

    const/4 v2, 0x0

    .line 1781794
    invoke-static {v0, v2, v2}, LX/BQD;->a(LX/BQD;ZZ)V

    .line 1781795
    iput-boolean v1, p0, LX/BQB;->p:Z

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 1781770
    iget-object v0, p0, LX/BQB;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x1a0009

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->d(I)V

    .line 1781771
    iget-object v0, p0, LX/BQB;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x1a000a

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->d(I)V

    .line 1781772
    iget-object v0, p0, LX/BQB;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x1a000b

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->d(I)V

    .line 1781773
    iget-object v0, p0, LX/BQB;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x1a000d

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->d(I)V

    .line 1781774
    iget-object v0, p0, LX/BQB;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x1a000e

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->d(I)V

    .line 1781775
    iget-object v0, p0, LX/BQB;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x1a001a

    const/4 v2, 0x4

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 1781776
    iget-object v0, p0, LX/BQB;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x1a0012

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->d(I)V

    .line 1781777
    iget-object v0, p0, LX/BQB;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x1a0013

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->d(I)V

    .line 1781778
    iget-object v0, p0, LX/BQB;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x1a0014

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->d(I)V

    .line 1781779
    const/4 v2, 0x4

    .line 1781780
    iget-object v0, p0, LX/BQB;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x1a0028

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 1781781
    iget-object v0, p0, LX/BQB;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x1a0002

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 1781782
    iget-object v0, p0, LX/BQB;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x1a0018

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->d(I)V

    .line 1781783
    iget-object v0, p0, LX/BQB;->c:LX/BQD;

    .line 1781784
    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, LX/BQD;->a(LX/BQD;ZZ)V

    .line 1781785
    return-void
.end method

.method public final a(J)V
    .locals 13

    .prologue
    .line 1781760
    iget-object v0, p0, LX/BQB;->e:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    .line 1781761
    iget-object v2, p0, LX/BQB;->c:LX/BQD;

    const-string v3, "TimelineEarlyFetchHeadStart"

    const/4 v7, 0x0

    .line 1781762
    iget-object v5, v2, LX/BQD;->a:LX/11i;

    sget-object v6, LX/BQF;->a:LX/BQE;

    invoke-interface {v5, v6}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v5

    .line 1781763
    if-eqz v5, :cond_0

    .line 1781764
    const v11, 0x5caaf2ff

    move-object v6, v3

    move-object v8, v7

    move-wide v9, v0

    invoke-static/range {v5 .. v11}, LX/096;->a(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;JI)LX/11o;

    .line 1781765
    :cond_0
    iget-object v2, p0, LX/BQB;->c:LX/BQD;

    const-string v3, "TimelineEarlyFetchHeadStart"

    add-long/2addr v0, p1

    const/4 v7, 0x0

    .line 1781766
    iget-object v5, v2, LX/BQD;->a:LX/11i;

    sget-object v6, LX/BQF;->a:LX/BQE;

    invoke-interface {v5, v6}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v5

    .line 1781767
    if-eqz v5, :cond_1

    .line 1781768
    const v11, -0x685b2690

    move-object v6, v3

    move-object v8, v7

    move-wide v9, v0

    invoke-static/range {v5 .. v11}, LX/096;->b(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;JI)LX/11o;

    .line 1781769
    :cond_1
    return-void
.end method

.method public final a(Ljava/lang/String;Z)V
    .locals 3

    .prologue
    .line 1781740
    iget-object v0, p0, LX/BQB;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x1a0009

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 1781741
    iget-object v0, p0, LX/BQB;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x1a000a

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 1781742
    iget-object v0, p0, LX/BQB;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x1a000b

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 1781743
    iget-object v0, p0, LX/BQB;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x1a000d

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 1781744
    iget-object v0, p0, LX/BQB;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x1a000e

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 1781745
    iget-object v0, p0, LX/BQB;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x1a001a

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 1781746
    iget-object v0, p0, LX/BQB;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x1a0012

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 1781747
    iget-object v0, p0, LX/BQB;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x1a0013

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 1781748
    iget-object v0, p0, LX/BQB;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x1a0014

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 1781749
    iget-object v0, p0, LX/BQB;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x1a0018

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 1781750
    iget-object v0, p0, LX/BQB;->c:LX/BQD;

    .line 1781751
    iget-object v1, v0, LX/BQD;->a:LX/11i;

    sget-object v2, LX/BQF;->a:LX/BQE;

    invoke-interface {v1, v2}, LX/11i;->a(LX/0Pq;)LX/11o;

    .line 1781752
    new-instance v0, LX/0P2;

    invoke-direct {v0}, LX/0P2;-><init>()V

    const-string v1, "navigation_source"

    if-eqz p1, :cond_0

    :goto_0
    invoke-virtual {v0, v1, p1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "is_first_load"

    invoke-static {p2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    .line 1781753
    iget-object v1, p0, LX/BQB;->c:LX/BQD;

    const-string v2, "TimelineFragmentCreate"

    invoke-virtual {v1, v2, v0}, LX/BQD;->a(Ljava/lang/String;LX/0P1;)V

    .line 1781754
    iget-object v0, p0, LX/BQB;->c:LX/BQD;

    const-string v1, "TimelineRenderEntireHeader"

    invoke-virtual {v0, v1}, LX/BQD;->a(Ljava/lang/String;)V

    .line 1781755
    iget-object v0, p0, LX/BQB;->c:LX/BQD;

    const-string v1, "TimelineRenderLowResHeader"

    invoke-virtual {v0, v1}, LX/BQD;->a(Ljava/lang/String;)V

    .line 1781756
    iget-object v0, p0, LX/BQB;->c:LX/BQD;

    const-string v1, "TimelineRenderLowResHeaderCoverphotoOptional"

    invoke-virtual {v0, v1}, LX/BQD;->a(Ljava/lang/String;)V

    .line 1781757
    iget-object v0, p0, LX/BQB;->g:LX/BQ7;

    const-string v1, "TimelineRenderLowResHeader"

    invoke-virtual {v0, v1}, LX/BQ7;->a(Ljava/lang/String;)V

    .line 1781758
    return-void

    .line 1781759
    :cond_0
    const-string p1, "unknown"

    goto :goto_0
.end method

.method public final a(ZLX/BQA;)V
    .locals 1

    .prologue
    .line 1781734
    if-eqz p1, :cond_1

    .line 1781735
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/BQB;->y:Z

    .line 1781736
    iput-object p2, p0, LX/BQB;->a:LX/BQA;

    .line 1781737
    :cond_0
    :goto_0
    return-void

    .line 1781738
    :cond_1
    iget-object v0, p0, LX/BQB;->a:LX/BQA;

    if-ne p2, v0, :cond_0

    .line 1781739
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/BQB;->y:Z

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1781732
    iget-object v0, p0, LX/BQB;->c:LX/BQD;

    const-string v1, "TimelineFragmentFbInjectorInjectMe"

    invoke-virtual {v0, v1}, LX/BQD;->a(Ljava/lang/String;)V

    .line 1781733
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 1781730
    iget-object v0, p0, LX/BQB;->c:LX/BQD;

    const-string v1, "TimelineFragmentFbInjectorInjectMe"

    invoke-virtual {v0, v1}, LX/BQD;->b(Ljava/lang/String;)V

    .line 1781731
    return-void
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 1781727
    iget-object v0, p0, LX/BQB;->c:LX/BQD;

    const-string v1, "TimelineLoadHeader"

    invoke-virtual {v0, v1}, LX/BQD;->a(Ljava/lang/String;)V

    .line 1781728
    iget-object v0, p0, LX/BQB;->g:LX/BQ7;

    const-string v1, "TimelineLoadHeader"

    invoke-virtual {v0, v1}, LX/BQ7;->a(Ljava/lang/String;)V

    .line 1781729
    return-void
.end method

.method public final j()V
    .locals 3

    .prologue
    const/4 v2, 0x3

    .line 1781716
    iget-object v0, p0, LX/BQB;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x1a0012

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 1781717
    iget-object v0, p0, LX/BQB;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x1a0013

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->d(I)V

    .line 1781718
    iget-object v0, p0, LX/BQB;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x1a0014

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->d(I)V

    .line 1781719
    iget-object v0, p0, LX/BQB;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x1a0010

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->d(I)V

    .line 1781720
    iget-object v0, p0, LX/BQB;->c:LX/BQD;

    const-string v1, "TimelineFirstUnitsNetworkFetch"

    invoke-virtual {v0, v1}, LX/BQD;->d(Ljava/lang/String;)V

    .line 1781721
    iget-object v0, p0, LX/BQB;->c:LX/BQD;

    const-string v1, "TimelineInitialFetchUnits"

    invoke-virtual {v0, v1}, LX/BQD;->d(Ljava/lang/String;)V

    .line 1781722
    iget-object v0, p0, LX/BQB;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x1a0002

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 1781723
    iget-object v0, p0, LX/BQB;->c:LX/BQD;

    const-string v1, "TimelineFetchInitialUnitsWaitTime"

    invoke-virtual {v0, v1}, LX/BQD;->d(Ljava/lang/String;)V

    .line 1781724
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/BQB;->t:Z

    .line 1781725
    invoke-static {p0}, LX/BQB;->w(LX/BQB;)V

    .line 1781726
    return-void
.end method
