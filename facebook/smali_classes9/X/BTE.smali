.class public final enum LX/BTE;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/BTE;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/BTE;

.field public static final enum AUDIO_BUTTON_CLICKS:LX/BTE;

.field public static final enum HD_UPLOAD_STATE:LX/BTE;

.field public static final enum IS_MUTED:LX/BTE;

.field public static final enum LEFT_HANDLE_MOVES:LX/BTE;

.field public static final enum LEFT_HANDLE_ZOOM_INS:LX/BTE;

.field public static final enum ORIGINAL_LENGTH:LX/BTE;

.field public static final enum RIGHT_HANDLE_MOVES:LX/BTE;

.field public static final enum RIGHT_HANDLE_ZOOM_INS:LX/BTE;

.field public static final enum ROTATION:LX/BTE;

.field public static final enum SCRUBBER_MOVES:LX/BTE;

.field public static final enum TRIMMED_LENGTH:LX/BTE;


# instance fields
.field private final name:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1787018
    new-instance v0, LX/BTE;

    const-string v1, "ORIGINAL_LENGTH"

    const-string v2, "original_length"

    invoke-direct {v0, v1, v4, v2}, LX/BTE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BTE;->ORIGINAL_LENGTH:LX/BTE;

    .line 1787019
    new-instance v0, LX/BTE;

    const-string v1, "TRIMMED_LENGTH"

    const-string v2, "trimmed_length"

    invoke-direct {v0, v1, v5, v2}, LX/BTE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BTE;->TRIMMED_LENGTH:LX/BTE;

    .line 1787020
    new-instance v0, LX/BTE;

    const-string v1, "IS_MUTED"

    const-string v2, "is_muted"

    invoke-direct {v0, v1, v6, v2}, LX/BTE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BTE;->IS_MUTED:LX/BTE;

    .line 1787021
    new-instance v0, LX/BTE;

    const-string v1, "ROTATION"

    const-string v2, "rotation"

    invoke-direct {v0, v1, v7, v2}, LX/BTE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BTE;->ROTATION:LX/BTE;

    .line 1787022
    new-instance v0, LX/BTE;

    const-string v1, "LEFT_HANDLE_MOVES"

    const-string v2, "left_handle_moves"

    invoke-direct {v0, v1, v8, v2}, LX/BTE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BTE;->LEFT_HANDLE_MOVES:LX/BTE;

    .line 1787023
    new-instance v0, LX/BTE;

    const-string v1, "RIGHT_HANDLE_MOVES"

    const/4 v2, 0x5

    const-string v3, "right_handle_moves"

    invoke-direct {v0, v1, v2, v3}, LX/BTE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BTE;->RIGHT_HANDLE_MOVES:LX/BTE;

    .line 1787024
    new-instance v0, LX/BTE;

    const-string v1, "LEFT_HANDLE_ZOOM_INS"

    const/4 v2, 0x6

    const-string v3, "left_handle_zoom_ins"

    invoke-direct {v0, v1, v2, v3}, LX/BTE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BTE;->LEFT_HANDLE_ZOOM_INS:LX/BTE;

    .line 1787025
    new-instance v0, LX/BTE;

    const-string v1, "RIGHT_HANDLE_ZOOM_INS"

    const/4 v2, 0x7

    const-string v3, "right_handle_zoom_ins"

    invoke-direct {v0, v1, v2, v3}, LX/BTE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BTE;->RIGHT_HANDLE_ZOOM_INS:LX/BTE;

    .line 1787026
    new-instance v0, LX/BTE;

    const-string v1, "SCRUBBER_MOVES"

    const/16 v2, 0x8

    const-string v3, "scrubber_moves"

    invoke-direct {v0, v1, v2, v3}, LX/BTE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BTE;->SCRUBBER_MOVES:LX/BTE;

    .line 1787027
    new-instance v0, LX/BTE;

    const-string v1, "AUDIO_BUTTON_CLICKS"

    const/16 v2, 0x9

    const-string v3, "audio_button_clicks"

    invoke-direct {v0, v1, v2, v3}, LX/BTE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BTE;->AUDIO_BUTTON_CLICKS:LX/BTE;

    .line 1787028
    new-instance v0, LX/BTE;

    const-string v1, "HD_UPLOAD_STATE"

    const/16 v2, 0xa

    const-string v3, "hd_upload_state"

    invoke-direct {v0, v1, v2, v3}, LX/BTE;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BTE;->HD_UPLOAD_STATE:LX/BTE;

    .line 1787029
    const/16 v0, 0xb

    new-array v0, v0, [LX/BTE;

    sget-object v1, LX/BTE;->ORIGINAL_LENGTH:LX/BTE;

    aput-object v1, v0, v4

    sget-object v1, LX/BTE;->TRIMMED_LENGTH:LX/BTE;

    aput-object v1, v0, v5

    sget-object v1, LX/BTE;->IS_MUTED:LX/BTE;

    aput-object v1, v0, v6

    sget-object v1, LX/BTE;->ROTATION:LX/BTE;

    aput-object v1, v0, v7

    sget-object v1, LX/BTE;->LEFT_HANDLE_MOVES:LX/BTE;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/BTE;->RIGHT_HANDLE_MOVES:LX/BTE;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/BTE;->LEFT_HANDLE_ZOOM_INS:LX/BTE;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/BTE;->RIGHT_HANDLE_ZOOM_INS:LX/BTE;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/BTE;->SCRUBBER_MOVES:LX/BTE;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/BTE;->AUDIO_BUTTON_CLICKS:LX/BTE;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/BTE;->HD_UPLOAD_STATE:LX/BTE;

    aput-object v2, v0, v1

    sput-object v0, LX/BTE;->$VALUES:[LX/BTE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1787032
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1787033
    iput-object p3, p0, LX/BTE;->name:Ljava/lang/String;

    .line 1787034
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/BTE;
    .locals 1

    .prologue
    .line 1787035
    const-class v0, LX/BTE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/BTE;

    return-object v0
.end method

.method public static values()[LX/BTE;
    .locals 1

    .prologue
    .line 1787031
    sget-object v0, LX/BTE;->$VALUES:[LX/BTE;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/BTE;

    return-object v0
.end method


# virtual methods
.method public final getParamKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1787030
    iget-object v0, p0, LX/BTE;->name:Ljava/lang/String;

    return-object v0
.end method
