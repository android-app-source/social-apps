.class public LX/Ca4;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/Ca0;

.field public final c:LX/0Zb;

.field public final d:LX/03V;

.field public final e:LX/1nI;

.field public f:LX/CEW;

.field public final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1My;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0TF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0TF",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            ">;>;"
        }
    .end annotation
.end field

.field public final i:LX/3id;

.field public final j:LX/961;

.field public k:LX/1My;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/Ca0;LX/0Zb;LX/03V;LX/1nI;LX/0Or;LX/3id;LX/961;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/Ca0;",
            "LX/0Zb;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/1nI;",
            "LX/0Or",
            "<",
            "LX/1My;",
            ">;",
            "LX/3id;",
            "LX/961;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1917010
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1917011
    iput-object p1, p0, LX/Ca4;->a:Landroid/content/Context;

    .line 1917012
    iput-object p2, p0, LX/Ca4;->b:LX/Ca0;

    .line 1917013
    iput-object p3, p0, LX/Ca4;->c:LX/0Zb;

    .line 1917014
    iput-object p4, p0, LX/Ca4;->d:LX/03V;

    .line 1917015
    iput-object p5, p0, LX/Ca4;->e:LX/1nI;

    .line 1917016
    iput-object p6, p0, LX/Ca4;->g:LX/0Or;

    .line 1917017
    iput-object p7, p0, LX/Ca4;->i:LX/3id;

    .line 1917018
    iput-object p8, p0, LX/Ca4;->j:LX/961;

    .line 1917019
    new-instance v0, LX/Ca1;

    invoke-direct {v0, p0}, LX/Ca1;-><init>(LX/Ca4;)V

    iput-object v0, p0, LX/Ca4;->h:LX/0TF;

    .line 1917020
    return-void
.end method

.method public static b(LX/0QB;)LX/Ca4;
    .locals 9

    .prologue
    .line 1917002
    new-instance v0, LX/Ca4;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/Ca0;->a(LX/0QB;)LX/Ca0;

    move-result-object v2

    check-cast v2, LX/Ca0;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-static {p0}, LX/1nH;->a(LX/0QB;)LX/1nH;

    move-result-object v5

    check-cast v5, LX/1nI;

    const/16 v6, 0xafc

    invoke-static {p0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-static {p0}, LX/3id;->b(LX/0QB;)LX/3id;

    move-result-object v7

    check-cast v7, LX/3id;

    invoke-static {p0}, LX/961;->b(LX/0QB;)LX/961;

    move-result-object v8

    check-cast v8, LX/961;

    invoke-direct/range {v0 .. v8}, LX/Ca4;-><init>(Landroid/content/Context;LX/Ca0;LX/0Zb;LX/03V;LX/1nI;LX/0Or;LX/3id;LX/961;)V

    .line 1917003
    return-object v0
.end method

.method public static e(LX/Ca4;)LX/0gv;
    .locals 2

    .prologue
    .line 1917005
    iget-object v0, p0, LX/Ca4;->a:Landroid/content/Context;

    invoke-static {v0}, LX/4tq;->a(Landroid/content/Context;)LX/0ew;

    move-result-object v0

    invoke-interface {v0}, LX/0ew;->iC_()LX/0gc;

    move-result-object v0

    .line 1917006
    const-string v1, "chromeless:content:fragment:tag"

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 1917007
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->isVisible()Z

    move-result v1

    if-eqz v1, :cond_0

    instance-of v1, v0, LX/0gv;

    if-eqz v1, :cond_0

    .line 1917008
    check-cast v0, LX/0gv;

    .line 1917009
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final b()Z
    .locals 1

    .prologue
    .line 1917004
    invoke-static {p0}, LX/Ca4;->e(LX/Ca4;)LX/0gv;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
