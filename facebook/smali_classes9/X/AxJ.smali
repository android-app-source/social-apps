.class public LX/AxJ;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1727794
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeCategoryFieldsModel;)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLInterfaces$MemeCategoryFields;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLInterfaces$MemeStory;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1727795
    invoke-virtual {p0}, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeCategoryFieldsModel;->b()Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeCategoryFieldsModel$MemeStoriesModel;

    move-result-object v0

    .line 1727796
    if-eqz v0, :cond_0

    .line 1727797
    invoke-virtual {v0}, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeCategoryFieldsModel$MemeStoriesModel;->b()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    .line 1727798
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel;)Lcom/facebook/ipc/composer/intent/ComposerShareParams;
    .locals 6
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1727799
    invoke-virtual {p0}, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel;->c()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1727800
    invoke-virtual {p0}, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel;->c()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel$AttachmentsModel;

    .line 1727801
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel$AttachmentsModel;->a()Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel$AttachmentsModel$MediaModel;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {v0}, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel$AttachmentsModel;->a()Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel$AttachmentsModel$MediaModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel$AttachmentsModel$MediaModel;->a()Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel$AttachmentsModel$MediaModel$ImageModel;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 1727802
    invoke-virtual {v0}, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel$AttachmentsModel;->a()Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel$AttachmentsModel$MediaModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel$AttachmentsModel$MediaModel;->a()Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel$AttachmentsModel$MediaModel$ImageModel;

    move-result-object v0

    .line 1727803
    :goto_1
    move-object v0, v0

    .line 1727804
    if-eqz v0, :cond_0

    .line 1727805
    new-instance v1, LX/170;

    invoke-direct {v1}, LX/170;-><init>()V

    invoke-virtual {p0}, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel;->d()Ljava/lang/String;

    move-result-object v2

    .line 1727806
    iput-object v2, v1, LX/170;->o:Ljava/lang/String;

    .line 1727807
    move-object v1, v1

    .line 1727808
    invoke-virtual {v1}, LX/170;->a()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v1

    .line 1727809
    new-instance v2, LX/39x;

    invoke-direct {v2}, LX/39x;-><init>()V

    new-instance v3, LX/4XB;

    invoke-direct {v3}, LX/4XB;-><init>()V

    new-instance v4, LX/2dc;

    invoke-direct {v4}, LX/2dc;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel$AttachmentsModel$MediaModel$ImageModel;->b()Ljava/lang/String;

    move-result-object v5

    .line 1727810
    iput-object v5, v4, LX/2dc;->h:Ljava/lang/String;

    .line 1727811
    move-object v4, v4

    .line 1727812
    invoke-virtual {v0}, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel$AttachmentsModel$MediaModel$ImageModel;->a()I

    move-result v5

    .line 1727813
    iput v5, v4, LX/2dc;->c:I

    .line 1727814
    move-object v4, v4

    .line 1727815
    invoke-virtual {v0}, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel$AttachmentsModel$MediaModel$ImageModel;->c()I

    move-result v0

    .line 1727816
    iput v0, v4, LX/2dc;->i:I

    .line 1727817
    move-object v0, v4

    .line 1727818
    invoke-virtual {v0}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    .line 1727819
    iput-object v0, v3, LX/4XB;->U:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1727820
    move-object v0, v3

    .line 1727821
    invoke-virtual {v0}, LX/4XB;->a()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    .line 1727822
    iput-object v0, v2, LX/39x;->j:Lcom/facebook/graphql/model/GraphQLMedia;

    .line 1727823
    move-object v0, v2

    .line 1727824
    invoke-virtual {p0}, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel;->b()LX/0Px;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 1727825
    invoke-virtual {p0}, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel;->b()LX/0Px;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel$ActorsModel;

    invoke-virtual {v2}, Lcom/facebook/friendsharing/meme/graphql/MemeGraphQLModels$MemeStoryModel$ActorsModel;->a()Ljava/lang/String;

    move-result-object v2

    .line 1727826
    :goto_2
    move-object v2, v2

    .line 1727827
    invoke-static {v2}, LX/16z;->a(Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    .line 1727828
    iput-object v2, v0, LX/39x;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1727829
    move-object v0, v0

    .line 1727830
    invoke-virtual {v0}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 1727831
    invoke-static {v1}, LX/89G;->a(Lcom/facebook/graphql/model/GraphQLEntity;)LX/89G;

    move-result-object v1

    .line 1727832
    iput-object v0, v1, LX/89G;->c:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1727833
    move-object v0, v1

    .line 1727834
    const/4 v1, 0x1

    .line 1727835
    iput-boolean v1, v0, LX/89G;->g:Z

    .line 1727836
    move-object v0, v0

    .line 1727837
    invoke-virtual {v0}, LX/89G;->b()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v0

    .line 1727838
    :goto_3
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_3

    .line 1727839
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_0

    .line 1727840
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    const/4 v2, 0x0

    goto :goto_2
.end method

.method public static a(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerShareParams;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1727841
    if-eqz p0, :cond_0

    .line 1727842
    invoke-static {p0}, LX/89G;->a(Ljava/lang/String;)LX/89G;

    move-result-object v0

    invoke-virtual {v0}, LX/89G;->b()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v0

    .line 1727843
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
