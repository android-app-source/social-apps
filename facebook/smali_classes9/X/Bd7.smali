.class public final LX/Bd7;
.super LX/BcO;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/BcO",
        "<",
        "LX/BdA;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public b:Z

.field public c:LX/BdC;

.field public d:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TTModel;"
        }
    .end annotation
.end field

.field public e:Ljava/lang/Throwable;

.field public f:LX/Egm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/components/list/fb/datasources/GraphQLRootQuerySectionSpec$Configuration",
            "<TTModel;>;"
        }
    .end annotation
.end field

.field public g:LX/BdF;

.field public final synthetic h:LX/BdA;


# direct methods
.method public constructor <init>(LX/BdA;)V
    .locals 1

    .prologue
    .line 1803298
    iput-object p1, p0, LX/Bd7;->h:LX/BdA;

    .line 1803299
    move-object v0, p1

    .line 1803300
    invoke-direct {p0, v0}, LX/BcO;-><init>(LX/BcS;)V

    .line 1803301
    return-void
.end method


# virtual methods
.method public final a(Z)LX/BcO;
    .locals 3

    .prologue
    .line 1803290
    const/4 v2, 0x0

    .line 1803291
    invoke-super {p0, p1}, LX/BcO;->a(Z)LX/BcO;

    move-result-object v0

    check-cast v0, LX/Bd7;

    .line 1803292
    if-nez p1, :cond_0

    .line 1803293
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/Bd7;->b:Z

    .line 1803294
    iput-object v2, v0, LX/Bd7;->c:LX/BdC;

    .line 1803295
    iput-object v2, v0, LX/Bd7;->d:Ljava/lang/Object;

    .line 1803296
    iput-object v2, v0, LX/Bd7;->e:Ljava/lang/Throwable;

    .line 1803297
    :cond_0
    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1803271
    if-ne p0, p1, :cond_1

    .line 1803272
    :cond_0
    :goto_0
    return v0

    .line 1803273
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1803274
    goto :goto_0

    .line 1803275
    :cond_3
    check-cast p1, LX/Bd7;

    .line 1803276
    iget-boolean v2, p0, LX/Bd7;->b:Z

    iget-boolean v3, p1, LX/Bd7;->b:Z

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 1803277
    goto :goto_0

    .line 1803278
    :cond_4
    iget-object v2, p0, LX/Bd7;->c:LX/BdC;

    if-eqz v2, :cond_6

    iget-object v2, p0, LX/Bd7;->c:LX/BdC;

    iget-object v3, p1, LX/Bd7;->c:LX/BdC;

    invoke-virtual {v2, v3}, LX/BdC;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    :cond_5
    move v0, v1

    .line 1803279
    goto :goto_0

    .line 1803280
    :cond_6
    iget-object v2, p1, LX/Bd7;->c:LX/BdC;

    if-nez v2, :cond_5

    .line 1803281
    :cond_7
    iget-object v2, p0, LX/Bd7;->d:Ljava/lang/Object;

    if-eqz v2, :cond_9

    iget-object v2, p0, LX/Bd7;->d:Ljava/lang/Object;

    iget-object v3, p1, LX/Bd7;->d:Ljava/lang/Object;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    :cond_8
    move v0, v1

    .line 1803282
    goto :goto_0

    .line 1803283
    :cond_9
    iget-object v2, p1, LX/Bd7;->d:Ljava/lang/Object;

    if-nez v2, :cond_8

    .line 1803284
    :cond_a
    iget-object v2, p0, LX/Bd7;->e:Ljava/lang/Throwable;

    if-eqz v2, :cond_c

    iget-object v2, p0, LX/Bd7;->e:Ljava/lang/Throwable;

    iget-object v3, p1, LX/Bd7;->e:Ljava/lang/Throwable;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    :cond_b
    move v0, v1

    .line 1803285
    goto :goto_0

    .line 1803286
    :cond_c
    iget-object v2, p1, LX/Bd7;->e:Ljava/lang/Throwable;

    if-nez v2, :cond_b

    .line 1803287
    :cond_d
    iget-object v2, p0, LX/Bd7;->f:LX/Egm;

    if-eqz v2, :cond_e

    iget-object v2, p0, LX/Bd7;->f:LX/Egm;

    iget-object v3, p1, LX/Bd7;->f:LX/Egm;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1803288
    goto :goto_0

    .line 1803289
    :cond_e
    iget-object v2, p1, LX/Bd7;->f:LX/Egm;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
