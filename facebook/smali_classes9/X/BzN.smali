.class public final LX/BzN;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/BzO;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public b:Ljava/lang/Boolean;


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1838672
    const-string v0, "AttachmentCoverPhotoComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1838658
    if-ne p0, p1, :cond_1

    .line 1838659
    :cond_0
    :goto_0
    return v0

    .line 1838660
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1838661
    goto :goto_0

    .line 1838662
    :cond_3
    check-cast p1, LX/BzN;

    .line 1838663
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1838664
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1838665
    if-eq v2, v3, :cond_0

    .line 1838666
    iget-object v2, p0, LX/BzN;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/BzN;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/BzN;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1838667
    goto :goto_0

    .line 1838668
    :cond_5
    iget-object v2, p1, LX/BzN;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 1838669
    :cond_6
    iget-object v2, p0, LX/BzN;->b:Ljava/lang/Boolean;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/BzN;->b:Ljava/lang/Boolean;

    iget-object v3, p1, LX/BzN;->b:Ljava/lang/Boolean;

    invoke-virtual {v2, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1838670
    goto :goto_0

    .line 1838671
    :cond_7
    iget-object v2, p1, LX/BzN;->b:Ljava/lang/Boolean;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
