.class public LX/CcS;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "Lcom/facebook/photos/mediagallery/ui/widget/MediaGallerySphericalPhotoPagerListener;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1921145
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1921146
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v0, p0, LX/CcS;->a:Ljava/util/WeakHashMap;

    .line 1921147
    return-void
.end method

.method public static a(LX/0QB;)LX/CcS;
    .locals 3

    .prologue
    .line 1921148
    const-class v1, LX/CcS;

    monitor-enter v1

    .line 1921149
    :try_start_0
    sget-object v0, LX/CcS;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1921150
    sput-object v2, LX/CcS;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1921151
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1921152
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 1921153
    new-instance v0, LX/CcS;

    invoke-direct {v0}, LX/CcS;-><init>()V

    .line 1921154
    move-object v0, v0

    .line 1921155
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1921156
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/CcS;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1921157
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1921158
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final c(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1921159
    iget-object v0, p0, LX/CcS;->a:Ljava/util/WeakHashMap;

    invoke-virtual {v0}, Ljava/util/WeakHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Caa;

    .line 1921160
    if-eqz v0, :cond_0

    .line 1921161
    iget-object v2, v0, LX/Caa;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;

    iget-object v2, v2, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;->i:LX/CaV;

    if-nez v2, :cond_3

    .line 1921162
    :cond_1
    :goto_1
    goto :goto_0

    .line 1921163
    :cond_2
    return-void

    .line 1921164
    :cond_3
    iget-object v2, v0, LX/Caa;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;

    invoke-static {v2, p1}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;->a$redex0(Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, v0, LX/Caa;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;

    iget-boolean v2, v2, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;->o:Z

    if-eqz v2, :cond_1

    .line 1921165
    iget-object v2, v0, LX/Caa;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;

    const/4 p0, 0x0

    .line 1921166
    iput-boolean p0, v2, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;->o:Z

    .line 1921167
    iget-object v2, v0, LX/Caa;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;

    iget-object v2, v2, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;->i:LX/CaV;

    invoke-virtual {v2}, LX/CaV;->s()V

    goto :goto_1
.end method

.method public final d(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1921168
    iget-object v0, p0, LX/CcS;->a:Ljava/util/WeakHashMap;

    invoke-virtual {v0}, Ljava/util/WeakHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Caa;

    .line 1921169
    if-eqz v0, :cond_0

    .line 1921170
    iget-object v2, v0, LX/Caa;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;

    iget-object v2, v2, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;->i:LX/CaV;

    if-nez v2, :cond_3

    .line 1921171
    :cond_1
    :goto_1
    goto :goto_0

    .line 1921172
    :cond_2
    return-void

    .line 1921173
    :cond_3
    iget-object v2, v0, LX/Caa;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;

    invoke-static {v2, p1}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;->a$redex0(Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, v0, LX/Caa;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;

    iget-boolean v2, v2, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;->o:Z

    if-nez v2, :cond_1

    .line 1921174
    iget-object v2, v0, LX/Caa;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;

    const/4 p0, 0x1

    .line 1921175
    iput-boolean p0, v2, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;->o:Z

    .line 1921176
    iget-object v2, v0, LX/Caa;->a:Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;

    iget-object v2, v2, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;->i:LX/CaV;

    invoke-virtual {v2}, LX/CaV;->r()V

    goto :goto_1
.end method
