.class public final LX/BM4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/net/URI;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/platformattribution/nux/LayoutNuxDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/platformattribution/nux/LayoutNuxDialogFragment;)V
    .locals 0

    .prologue
    .line 1776764
    iput-object p1, p0, LX/BM4;->a:Lcom/facebook/platformattribution/nux/LayoutNuxDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1776765
    iget-object v0, p0, LX/BM4;->a:Lcom/facebook/platformattribution/nux/LayoutNuxDialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->d()V

    .line 1776766
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1776767
    check-cast p1, Ljava/net/URI;

    .line 1776768
    if-nez p1, :cond_1

    .line 1776769
    :cond_0
    :goto_0
    return-void

    .line 1776770
    :cond_1
    invoke-virtual {p1}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1776771
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1776772
    iget-object v1, p0, LX/BM4;->a:Lcom/facebook/platformattribution/nux/LayoutNuxDialogFragment;

    iget-object v1, v1, Lcom/facebook/platformattribution/nux/LayoutNuxDialogFragment;->q:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1776773
    iget-object v1, p0, LX/BM4;->a:Lcom/facebook/platformattribution/nux/LayoutNuxDialogFragment;

    iget-object v1, v1, Lcom/facebook/platformattribution/nux/LayoutNuxDialogFragment;->r:Landroid/widget/ProgressBar;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1776774
    iget-object v1, p0, LX/BM4;->a:Lcom/facebook/platformattribution/nux/LayoutNuxDialogFragment;

    iget-object v1, v1, Lcom/facebook/platformattribution/nux/LayoutNuxDialogFragment;->q:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sget-object v2, Lcom/facebook/platformattribution/nux/LayoutNuxDialogFragment;->p:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    goto :goto_0
.end method
