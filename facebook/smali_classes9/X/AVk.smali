.class public abstract LX/AVk;
.super LX/AVj;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        "M::",
        "LX/AW2;",
        "H::",
        "LX/AW2;",
        ">",
        "LX/AVj",
        "<TV;TM;>;"
    }
.end annotation


# instance fields
.field public d:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<TH;>;"
        }
    .end annotation
.end field

.field public e:LX/AW2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TH;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1680232
    invoke-direct {p0}, LX/AVj;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/AW2;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TM;)Z"
        }
    .end annotation

    .prologue
    .line 1680231
    iget-object v0, p0, LX/AVj;->b:LX/AW2;

    if-eq p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(LX/0Px;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<TH;>;)V"
        }
    .end annotation

    .prologue
    .line 1680233
    invoke-static {p1}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1680234
    :goto_0
    return-void

    .line 1680235
    :cond_0
    invoke-static {p1}, LX/0R9;->b(Ljava/lang/Iterable;)Ljava/util/LinkedList;

    move-result-object v0

    iput-object v0, p0, LX/AVk;->d:Ljava/util/LinkedList;

    .line 1680236
    iget-object v0, p0, LX/AVk;->d:Ljava/util/LinkedList;

    new-instance v1, LX/AVm;

    invoke-direct {v1, p0}, LX/AVm;-><init>(LX/AVj;)V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    goto :goto_0
.end method

.method public abstract d()V
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 1680230
    iget-object v0, p0, LX/AVj;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/AVk;->d:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/AVj;->b:LX/AW2;

    if-nez v0, :cond_0

    iget-object v0, p0, LX/AVk;->e:LX/AW2;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()V
    .locals 2

    .prologue
    .line 1680223
    iget-object v0, p0, LX/AVj;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget v1, p0, LX/AVj;->c:I

    iget-object v0, p0, LX/AVj;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AW2;

    invoke-interface {v0}, LX/AW2;->a()I

    move-result v0

    if-le v1, v0, :cond_0

    .line 1680224
    iget-object v0, p0, LX/AVj;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AW2;

    iput-object v0, p0, LX/AVk;->b:LX/AW2;

    .line 1680225
    invoke-virtual {p0}, LX/AVk;->d()V

    .line 1680226
    :cond_0
    iget-object v0, p0, LX/AVk;->d:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget v1, p0, LX/AVj;->c:I

    iget-object v0, p0, LX/AVk;->d:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AW2;

    invoke-interface {v0}, LX/AW2;->a()I

    move-result v0

    if-le v1, v0, :cond_1

    .line 1680227
    iget-object v0, p0, LX/AVk;->d:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AW2;

    iput-object v0, p0, LX/AVk;->e:LX/AW2;

    .line 1680228
    invoke-virtual {p0}, LX/AVj;->e()V

    .line 1680229
    :cond_1
    return-void
.end method
