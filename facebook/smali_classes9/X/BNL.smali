.class public LX/BNL;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/BNL;


# instance fields
.field private final a:Landroid/content/res/Resources;

.field private final b:LX/1CW;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/1CW;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1778575
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1778576
    iput-object p1, p0, LX/BNL;->a:Landroid/content/res/Resources;

    .line 1778577
    iput-object p2, p0, LX/BNL;->b:LX/1CW;

    .line 1778578
    return-void
.end method

.method public static a(LX/0QB;)LX/BNL;
    .locals 5

    .prologue
    .line 1778562
    sget-object v0, LX/BNL;->c:LX/BNL;

    if-nez v0, :cond_1

    .line 1778563
    const-class v1, LX/BNL;

    monitor-enter v1

    .line 1778564
    :try_start_0
    sget-object v0, LX/BNL;->c:LX/BNL;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1778565
    if-eqz v2, :cond_0

    .line 1778566
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1778567
    new-instance p0, LX/BNL;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-static {v0}, LX/1CW;->b(LX/0QB;)LX/1CW;

    move-result-object v4

    check-cast v4, LX/1CW;

    invoke-direct {p0, v3, v4}, LX/BNL;-><init>(Landroid/content/res/Resources;LX/1CW;)V

    .line 1778568
    move-object v0, p0

    .line 1778569
    sput-object v0, LX/BNL;->c:LX/BNL;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1778570
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1778571
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1778572
    :cond_1
    sget-object v0, LX/BNL;->c:LX/BNL;

    return-object v0

    .line 1778573
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1778574
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1778558
    iget-object v0, p0, LX/BNL;->a:Landroid/content/res/Resources;

    const v1, 0x7f0814e6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/fbservice/service/ServiceException;)Ljava/lang/String;
    .locals 3
    .param p1    # Lcom/facebook/fbservice/service/ServiceException;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 1778559
    iget-object v0, p0, LX/BNL;->b:LX/1CW;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, LX/1CW;->a(Lcom/facebook/fbservice/service/ServiceException;ZZ)Ljava/lang/String;

    move-result-object v0

    .line 1778560
    if-eqz v0, :cond_0

    .line 1778561
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, LX/BNL;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
