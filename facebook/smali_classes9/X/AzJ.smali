.class public final LX/AzJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Landroid/net/Uri;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/1lZ;

.field public final synthetic b:LX/1FJ;

.field public final synthetic c:Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotoEditHelper;


# direct methods
.method public constructor <init>(Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotoEditHelper;LX/1lZ;LX/1FJ;)V
    .locals 0

    .prologue
    .line 1732387
    iput-object p1, p0, LX/AzJ;->c:Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotoEditHelper;

    iput-object p2, p0, LX/AzJ;->a:LX/1lZ;

    iput-object p3, p0, LX/AzJ;->b:LX/1FJ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1732400
    iget-object v0, p0, LX/AzJ;->a:LX/1lZ;

    invoke-static {v0}, LX/1md;->a(Ljava/io/InputStream;)V

    .line 1732401
    iget-object v0, p0, LX/AzJ;->b:LX/1FJ;

    invoke-static {v0}, LX/1FJ;->c(LX/1FJ;)V

    .line 1732402
    iget-object v0, p0, LX/AzJ;->c:Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotoEditHelper;

    invoke-static {v0}, Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotoEditHelper;->b(Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotoEditHelper;)V

    .line 1732403
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 11

    .prologue
    .line 1732388
    check-cast p1, Landroid/net/Uri;

    .line 1732389
    iget-object v0, p0, LX/AzJ;->c:Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotoEditHelper;

    .line 1732390
    iget-object v1, v0, Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotoEditHelper;->h:LX/74n;

    sget-object v2, LX/74j;->SINGLE_SHOT_CAMERA:LX/74j;

    invoke-virtual {v1, p1, v2}, LX/74n;->a(Landroid/net/Uri;LX/74j;)Lcom/facebook/ipc/media/MediaItem;

    move-result-object v1

    .line 1732391
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1732392
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1732393
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 1732394
    const-string v1, "extra_media_items"

    invoke-virtual {v3, v1, v2}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 1732395
    iget-object v7, v0, Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotoEditHelper;->n:LX/BP7;

    if-nez v7, :cond_0

    .line 1732396
    iget-object v7, v0, Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotoEditHelper;->i:LX/BP8;

    iget-object v8, v0, Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotoEditHelper;->j:Ljava/lang/String;

    invoke-static {v8}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v7, v8}, LX/BP8;->a(Ljava/lang/Long;)LX/BP7;

    move-result-object v7

    iput-object v7, v0, Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotoEditHelper;->n:LX/BP7;

    .line 1732397
    :cond_0
    iget-object v7, v0, Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotoEditHelper;->n:LX/BP7;

    move-object v1, v7

    .line 1732398
    iget-object v2, v0, Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotoEditHelper;->b:Landroid/app/Activity;

    const/4 v4, 0x1

    iget-object v5, v0, Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotoEditHelper;->c:Ljava/lang/String;

    iget-object v6, v0, Lcom/facebook/friendsharing/suggestedcoverphotos/SuggestedCoverPhotoEditHelper;->m:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    invoke-virtual/range {v1 .. v6}, LX/BP7;->a(Landroid/app/Activity;Landroid/content/Intent;ZLjava/lang/String;Lcom/facebook/productionprompts/logging/PromptAnalytics;)V

    .line 1732399
    return-void
.end method
