.class public LX/Aw7;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

.field public static final b:Lcom/facebook/interstitial/manager/InterstitialTrigger;

.field public static final c:Lcom/facebook/interstitial/manager/InterstitialTrigger;


# instance fields
.field public final d:Landroid/content/Context;

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0iA;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/15W;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1724958
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->INSPIRATION_CAPTURE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    sput-object v0, LX/Aw7;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    .line 1724959
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->INSPIRATION_SHARE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    sput-object v0, LX/Aw7;->b:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    .line 1724960
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->INSPIRATION_CAMERA_IN_FEED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    sput-object v0, LX/Aw7;->c:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1724961
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1724962
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1724963
    iput-object v0, p0, LX/Aw7;->e:LX/0Ot;

    .line 1724964
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1724965
    iput-object v0, p0, LX/Aw7;->f:LX/0Ot;

    .line 1724966
    iput-object p1, p0, LX/Aw7;->d:Landroid/content/Context;

    .line 1724967
    return-void
.end method

.method public static b(LX/0QB;)LX/Aw7;
    .locals 3

    .prologue
    .line 1724968
    new-instance v1, LX/Aw7;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-direct {v1, v0}, LX/Aw7;-><init>(Landroid/content/Context;)V

    .line 1724969
    const/16 v0, 0xbd2

    invoke-static {p0, v0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v0

    const/16 v2, 0xbca

    invoke-static {p0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    .line 1724970
    iput-object v0, v1, LX/Aw7;->e:LX/0Ot;

    iput-object v2, v1, LX/Aw7;->f:LX/0Ot;

    .line 1724971
    return-object v1
.end method
