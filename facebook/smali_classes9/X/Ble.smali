.class public LX/Ble;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Lcom/facebook/content/SecureContextHelper;

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/content/SecureContextHelper;LX/0Or;)V
    .locals 0
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/base/activity/FragmentChromeActivity;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1816364
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1816365
    iput-object p1, p0, LX/Ble;->a:Lcom/facebook/content/SecureContextHelper;

    .line 1816366
    iput-object p2, p0, LX/Ble;->b:LX/0Or;

    .line 1816367
    return-void
.end method

.method public static a(Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;)Landroid/os/Bundle;
    .locals 2

    .prologue
    .line 1816361
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1816362
    const-string v1, "GUEST_LIST_INITIALIZATION_MODEL"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1816363
    return-object v0
.end method

.method public static a(Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;LX/Blc;)Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 1816358
    invoke-static {p0}, LX/Ble;->a(Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;)Landroid/os/Bundle;

    move-result-object v0

    .line 1816359
    const-string v1, "EVENT_GUEST_LIST_RSVP_TYPE"

    invoke-virtual {p1}, LX/Blc;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1816360
    return-object v0
.end method

.method public static a(LX/Ble;Landroid/content/Context;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 1816353
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    iget-object v0, p0, LX/Ble;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    .line 1816354
    const-string v1, "target_fragment"

    sget-object v2, LX/0cQ;->EVENTS_GUEST_LIST_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1816355
    invoke-virtual {v0, p2}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 1816356
    iget-object v1, p0, LX/Ble;->a:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1816357
    return-void
.end method

.method public static b(LX/0QB;)LX/Ble;
    .locals 3

    .prologue
    .line 1816351
    new-instance v1, LX/Ble;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    const/16 v2, 0xc

    invoke-static {p0, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-direct {v1, v0, v2}, LX/Ble;-><init>(Lcom/facebook/content/SecureContextHelper;LX/0Or;)V

    .line 1816352
    return-object v1
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;LX/Blc;)V
    .locals 1

    .prologue
    .line 1816349
    invoke-static {p2, p3}, LX/Ble;->a(Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;LX/Blc;)Landroid/os/Bundle;

    move-result-object v0

    invoke-static {p0, p1, v0}, LX/Ble;->a(LX/Ble;Landroid/content/Context;Landroid/os/Bundle;)V

    .line 1816350
    return-void
.end method
