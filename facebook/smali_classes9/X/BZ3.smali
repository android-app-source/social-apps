.class public LX/BZ3;
.super LX/BYo;
.source ""


# instance fields
.field private a:F

.field private b:F

.field private c:F

.field private d:F

.field private e:F

.field private f:F

.field private g:F

.field private h:F

.field private i:F


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1796732
    invoke-direct {p0}, LX/BYo;-><init>()V

    return-void
.end method


# virtual methods
.method public getRotationX()F
    .locals 1

    .prologue
    .line 1796731
    iget v0, p0, LX/BZ3;->d:F

    return v0
.end method

.method public getRotationY()F
    .locals 1

    .prologue
    .line 1796730
    iget v0, p0, LX/BZ3;->e:F

    return v0
.end method

.method public getRotationZ()F
    .locals 1

    .prologue
    .line 1796729
    iget v0, p0, LX/BZ3;->f:F

    return v0
.end method

.method public getScaleX()F
    .locals 1

    .prologue
    .line 1796728
    iget v0, p0, LX/BZ3;->g:F

    return v0
.end method

.method public getScaleY()F
    .locals 1

    .prologue
    .line 1796727
    iget v0, p0, LX/BZ3;->h:F

    return v0
.end method

.method public getScaleZ()F
    .locals 1

    .prologue
    .line 1796726
    iget v0, p0, LX/BZ3;->i:F

    return v0
.end method

.method public getX()F
    .locals 1

    .prologue
    .line 1796725
    iget v0, p0, LX/BZ3;->a:F

    return v0
.end method

.method public getY()F
    .locals 1

    .prologue
    .line 1796724
    iget v0, p0, LX/BZ3;->b:F

    return v0
.end method

.method public getZ()F
    .locals 1

    .prologue
    .line 1796733
    iget v0, p0, LX/BZ3;->c:F

    return v0
.end method

.method public setRotationX(F)V
    .locals 0

    .prologue
    .line 1796706
    iput p1, p0, LX/BZ3;->d:F

    .line 1796707
    return-void
.end method

.method public setRotationY(F)V
    .locals 0

    .prologue
    .line 1796710
    iput p1, p0, LX/BZ3;->e:F

    .line 1796711
    return-void
.end method

.method public setRotationZ(F)V
    .locals 0

    .prologue
    .line 1796712
    iput p1, p0, LX/BZ3;->f:F

    .line 1796713
    return-void
.end method

.method public setScaleX(F)V
    .locals 0

    .prologue
    .line 1796714
    iput p1, p0, LX/BZ3;->g:F

    .line 1796715
    return-void
.end method

.method public setScaleY(F)V
    .locals 0

    .prologue
    .line 1796716
    iput p1, p0, LX/BZ3;->h:F

    .line 1796717
    return-void
.end method

.method public setScaleZ(F)V
    .locals 0

    .prologue
    .line 1796718
    iput p1, p0, LX/BZ3;->i:F

    .line 1796719
    return-void
.end method

.method public setX(F)V
    .locals 0

    .prologue
    .line 1796720
    iput p1, p0, LX/BZ3;->a:F

    .line 1796721
    return-void
.end method

.method public setY(F)V
    .locals 0

    .prologue
    .line 1796708
    iput p1, p0, LX/BZ3;->b:F

    .line 1796709
    return-void
.end method

.method public setZ(F)V
    .locals 0

    .prologue
    .line 1796722
    iput p1, p0, LX/BZ3;->c:F

    .line 1796723
    return-void
.end method
