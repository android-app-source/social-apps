.class public LX/AWL;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/61B;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/5Pc;

.field private final c:[F

.field private d:[F

.field private final e:[F

.field public f:Z


# direct methods
.method public constructor <init>(Ljava/util/List;LX/5Pc;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/61B;",
            ">;",
            "LX/5Pc;",
            ")V"
        }
    .end annotation

    .prologue
    const/16 v2, 0x10

    const/4 v1, 0x0

    .line 1682731
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1682732
    iput-object p1, p0, LX/AWL;->a:Ljava/util/List;

    .line 1682733
    iput-object p2, p0, LX/AWL;->b:LX/5Pc;

    .line 1682734
    new-array v0, v2, [F

    iput-object v0, p0, LX/AWL;->c:[F

    .line 1682735
    iget-object v0, p0, LX/AWL;->c:[F

    invoke-static {v0, v1}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 1682736
    new-array v0, v2, [F

    iput-object v0, p0, LX/AWL;->d:[F

    .line 1682737
    iget-object v0, p0, LX/AWL;->d:[F

    invoke-static {v0, v1}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 1682738
    new-array v0, v2, [F

    iput-object v0, p0, LX/AWL;->e:[F

    .line 1682739
    iget-object v0, p0, LX/AWL;->e:[F

    invoke-static {v0, v1}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 1682740
    return-void
.end method

.method private static a(FFIF[F)V
    .locals 6

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    const/4 v0, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    .line 1682741
    const/4 v1, 0x0

    invoke-static {p4, v1}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 1682742
    div-float v1, p0, p1

    .line 1682743
    if-eqz p2, :cond_0

    const/4 v2, 0x2

    if-ne p2, v2, :cond_1

    .line 1682744
    :cond_0
    div-float v1, v4, v1

    .line 1682745
    :cond_1
    cmpg-float v2, p3, v1

    if-gez v2, :cond_2

    .line 1682746
    mul-float/2addr v1, v4

    .line 1682747
    mul-float v2, p3, v4

    .line 1682748
    sub-float v2, v1, v2

    div-float/2addr v2, v3

    .line 1682749
    div-float v1, v2, v1

    .line 1682750
    :goto_0
    new-instance v2, Landroid/graphics/RectF;

    sub-float v3, v4, v1

    sub-float/2addr v4, v0

    invoke-direct {v2, v1, v0, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 1682751
    invoke-static {p4, v2}, LX/61I;->a([FLandroid/graphics/RectF;)V

    .line 1682752
    return-void

    .line 1682753
    :cond_2
    div-float v1, v4, v1

    .line 1682754
    div-float v2, v4, p3

    .line 1682755
    sub-float v2, v1, v2

    div-float/2addr v2, v3

    .line 1682756
    div-float v1, v2, v1

    move v5, v1

    move v1, v0

    move v0, v5

    goto :goto_0
.end method


# virtual methods
.method public final a(IIIF)V
    .locals 4

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    .line 1682757
    int-to-float v0, p1

    int-to-float v1, p2

    iget-object v2, p0, LX/AWL;->c:[F

    invoke-static {v0, v1, p3, p4, v2}, LX/AWL;->a(FFIF[F)V

    .line 1682758
    cmpl-float v0, p4, v3

    if-nez v0, :cond_0

    .line 1682759
    iget-object v0, p0, LX/AWL;->c:[F

    invoke-virtual {v0}, [F->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [F

    iput-object v0, p0, LX/AWL;->d:[F

    .line 1682760
    :goto_0
    return-void

    .line 1682761
    :cond_0
    int-to-float v0, p1

    int-to-float v1, p2

    iget-object v2, p0, LX/AWL;->d:[F

    invoke-static {v0, v1, p3, v3, v2}, LX/AWL;->a(FFIF[F)V

    goto :goto_0
.end method

.method public final a([FLX/5Pf;LX/61B;Z)V
    .locals 6

    .prologue
    .line 1682762
    if-nez p2, :cond_0

    .line 1682763
    :goto_0
    return-void

    .line 1682764
    :cond_0
    if-eqz p4, :cond_1

    iget-object v2, p0, LX/AWL;->d:[F

    :goto_1
    iget-object v3, p0, LX/AWL;->e:[F

    const-wide/16 v4, 0x0

    move-object v0, p3

    move-object v1, p1

    invoke-interface/range {v0 .. v5}, LX/61B;->a([F[F[FJ)V

    goto :goto_0

    :cond_1
    iget-object v2, p0, LX/AWL;->c:[F

    goto :goto_1
.end method
