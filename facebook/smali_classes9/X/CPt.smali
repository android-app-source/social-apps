.class public final LX/CPt;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/CPt;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/CPr;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/CPu;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1885093
    const/4 v0, 0x0

    sput-object v0, LX/CPt;->a:LX/CPt;

    .line 1885094
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/CPt;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1885090
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1885091
    new-instance v0, LX/CPu;

    invoke-direct {v0}, LX/CPu;-><init>()V

    iput-object v0, p0, LX/CPt;->c:LX/CPu;

    .line 1885092
    return-void
.end method

.method public static onClick(LX/1X1;ZLX/CNe;)LX/1dQ;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            "Z",
            "LX/CNe;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1885089
    const v0, 0x498e5903

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object p2, v1, v2

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method

.method public static declared-synchronized q()LX/CPt;
    .locals 2

    .prologue
    .line 1885015
    const-class v1, LX/CPt;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/CPt;->a:LX/CPt;

    if-nez v0, :cond_0

    .line 1885016
    new-instance v0, LX/CPt;

    invoke-direct {v0}, LX/CPt;-><init>()V

    sput-object v0, LX/CPt;->a:LX/CPt;

    .line 1885017
    :cond_0
    sget-object v0, LX/CPt;->a:LX/CPt;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 1885018
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 12

    .prologue
    .line 1885048
    check-cast p2, LX/CPs;

    .line 1885049
    iget-object v0, p2, LX/CPs;->a:LX/CNb;

    iget-object v1, p2, LX/CPs;->b:LX/CNc;

    iget-object v2, p2, LX/CPs;->c:Ljava/util/List;

    const/4 v9, 0x3

    const/4 p0, 0x2

    const/4 v10, 0x1

    const/4 v11, 0x0

    .line 1885050
    const-string v3, "selected"

    invoke-virtual {v0, v3, v11}, LX/CNb;->a(Ljava/lang/String;Z)Z

    move-result v7

    .line 1885051
    if-eqz v7, :cond_3

    const-string v3, "deselect-action"

    :goto_0
    invoke-virtual {v0, v3}, LX/CNb;->b(Ljava/lang/String;)LX/CNb;

    move-result-object v3

    .line 1885052
    if-eqz v3, :cond_4

    invoke-static {v3, v1}, LX/CNd;->a(LX/CNb;LX/CNc;)LX/CNe;

    move-result-object v3

    .line 1885053
    :goto_1
    const-string v4, "font-size"

    const/high16 v5, 0x41500000    # 13.0f

    invoke-virtual {v0, v4, v5}, LX/CNb;->a(Ljava/lang/String;F)F

    move-result v8

    .line 1885054
    const-string v4, "enabled"

    invoke-virtual {v0, v4, v10}, LX/CNb;->a(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 1885055
    const-string v4, "pressed"

    invoke-virtual {v0, v4, v11}, LX/CNb;->a(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 1885056
    new-array v5, v9, [Ljava/lang/String;

    const-string v4, "pressed-text"

    aput-object v4, v5, v11

    if-eqz v7, :cond_5

    const-string v4, "selected-text"

    :goto_2
    aput-object v4, v5, v10

    const-string v4, "text"

    aput-object v4, v5, p0

    invoke-static {v0, v5}, LX/CPu;->a(LX/CNb;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1885057
    new-array v5, v9, [Ljava/lang/String;

    const-string v4, "pressed-text-color"

    aput-object v4, v5, v11

    if-eqz v7, :cond_6

    const-string v4, "selected-text-color"

    :goto_3
    aput-object v4, v5, v10

    const-string v4, "text-color"

    aput-object v4, v5, p0

    invoke-static {v0, v5}, LX/CPu;->b(LX/CNb;[Ljava/lang/String;)I

    move-result v5

    .line 1885058
    new-array v9, v9, [Ljava/lang/String;

    const-string v4, "pressed-background-image"

    aput-object v4, v9, v11

    if-eqz v7, :cond_7

    const-string v4, "selected-background-image"

    :goto_4
    aput-object v4, v9, v10

    const-string v4, "background-image"

    aput-object v4, v9, p0

    invoke-static {v0, v1, p1, v9}, LX/CPu;->a(LX/CNb;LX/CNc;LX/1De;[Ljava/lang/String;)LX/3rL;

    move-result-object v4

    .line 1885059
    :goto_5
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v7

    invoke-interface {v7, p0}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v7

    invoke-interface {v7, v10}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v7

    .line 1885060
    const-string v9, "enabled"

    invoke-virtual {v0, v9, v10}, LX/CNb;->a(Ljava/lang/String;Z)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 1885061
    const v9, 0x556f3d0c

    const/4 v10, 0x0

    invoke-static {p1, v9, v10}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v9

    move-object v9, v9

    .line 1885062
    invoke-interface {v7, v9}, LX/1Dh;->f(LX/1dQ;)LX/1Dh;

    move-result-object v9

    const-string v10, "selected"

    invoke-virtual {v0, v10, v11}, LX/CNb;->a(Ljava/lang/String;Z)Z

    move-result v10

    .line 1885063
    const v11, 0x498e5903

    const/4 p0, 0x2

    new-array p0, p0, [Ljava/lang/Object;

    const/4 p2, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    aput-object v1, p0, p2

    const/4 p2, 0x1

    aput-object v3, p0, p2

    invoke-static {p1, v11, p0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v11

    move-object v3, v11

    .line 1885064
    invoke-interface {v9, v3}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    .line 1885065
    :cond_0
    invoke-static {v6}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1885066
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, v6}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, v8}, LX/1ne;->g(F)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, v5}, LX/1ne;->m(I)LX/1ne;

    move-result-object v3

    const-string v5, "font-weight"

    invoke-static {v0, v5}, LX/CPv;->a(LX/CNb;Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v3, v5}, LX/1ne;->t(I)LX/1ne;

    move-result-object v3

    invoke-interface {v7, v3}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    .line 1885067
    :cond_1
    if-eqz v4, :cond_2

    .line 1885068
    invoke-static {p1}, LX/CNY;->a(LX/1De;)LX/CNW;

    move-result-object v5

    iget-object v3, v4, LX/3rL;->a:Ljava/lang/Object;

    check-cast v3, LX/CO8;

    invoke-virtual {v5, v3}, LX/CNW;->a(LX/CO8;)LX/CNW;

    move-result-object v5

    iget-object v3, v4, LX/3rL;->b:Ljava/lang/Object;

    check-cast v3, LX/CNb;

    invoke-virtual {v5, v3}, LX/CNW;->a(LX/CNb;)LX/CNW;

    move-result-object v3

    invoke-interface {v7, v3}, LX/1Dh;->c(LX/1n6;)LX/1Dh;

    .line 1885069
    :cond_2
    invoke-static {v7, p1, v0, v2}, LX/CPx;->a(LX/1Di;LX/1De;LX/CNb;Ljava/util/List;)LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 1885070
    return-object v0

    .line 1885071
    :cond_3
    const-string v3, "select-action"

    goto/16 :goto_0

    .line 1885072
    :cond_4
    const/4 v3, 0x0

    goto/16 :goto_1

    .line 1885073
    :cond_5
    const-string v4, "text"

    goto/16 :goto_2

    .line 1885074
    :cond_6
    const-string v4, "text-color"

    goto/16 :goto_3

    .line 1885075
    :cond_7
    const-string v4, "background-image"

    goto/16 :goto_4

    .line 1885076
    :cond_8
    if-eqz v7, :cond_9

    .line 1885077
    new-array v4, p0, [Ljava/lang/String;

    const-string v5, "selected-text"

    aput-object v5, v4, v11

    const-string v5, "text"

    aput-object v5, v4, v10

    invoke-static {v0, v4}, LX/CPu;->a(LX/CNb;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1885078
    new-array v4, p0, [Ljava/lang/String;

    const-string v5, "selected-text-color"

    aput-object v5, v4, v11

    const-string v5, "text-color"

    aput-object v5, v4, v10

    invoke-static {v0, v4}, LX/CPu;->b(LX/CNb;[Ljava/lang/String;)I

    move-result v5

    .line 1885079
    new-array v4, p0, [Ljava/lang/String;

    const-string v7, "selected-background-image"

    aput-object v7, v4, v11

    const-string v7, "background-image"

    aput-object v7, v4, v10

    invoke-static {v0, v1, p1, v4}, LX/CPu;->a(LX/CNb;LX/CNc;LX/1De;[Ljava/lang/String;)LX/3rL;

    move-result-object v4

    goto/16 :goto_5

    .line 1885080
    :cond_9
    new-array v4, v10, [Ljava/lang/String;

    const-string v5, "text"

    aput-object v5, v4, v11

    invoke-static {v0, v4}, LX/CPu;->a(LX/CNb;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1885081
    new-array v4, v10, [Ljava/lang/String;

    const-string v5, "text-color"

    aput-object v5, v4, v11

    invoke-static {v0, v4}, LX/CPu;->b(LX/CNb;[Ljava/lang/String;)I

    move-result v5

    .line 1885082
    new-array v4, v10, [Ljava/lang/String;

    const-string v7, "background-image"

    aput-object v7, v4, v11

    invoke-static {v0, v1, p1, v4}, LX/CPu;->a(LX/CNb;LX/CNc;LX/1De;[Ljava/lang/String;)LX/3rL;

    move-result-object v4

    goto/16 :goto_5

    .line 1885083
    :cond_a
    new-array v5, v9, [Ljava/lang/String;

    const-string v4, "disabled-text"

    aput-object v4, v5, v11

    if-eqz v7, :cond_b

    const-string v4, "selected-text"

    :goto_6
    aput-object v4, v5, v10

    const-string v4, "text"

    aput-object v4, v5, p0

    invoke-static {v0, v5}, LX/CPu;->a(LX/CNb;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1885084
    new-array v5, v9, [Ljava/lang/String;

    const-string v4, "disabled-text-color"

    aput-object v4, v5, v11

    if-eqz v7, :cond_c

    const-string v4, "selected-text-color"

    :goto_7
    aput-object v4, v5, v10

    const-string v4, "text-color"

    aput-object v4, v5, p0

    invoke-static {v0, v5}, LX/CPu;->b(LX/CNb;[Ljava/lang/String;)I

    move-result v5

    .line 1885085
    new-array v9, v9, [Ljava/lang/String;

    const-string v4, "disabled-background-image"

    aput-object v4, v9, v11

    if-eqz v7, :cond_d

    const-string v4, "selected-background-image"

    :goto_8
    aput-object v4, v9, v10

    const-string v4, "background-image"

    aput-object v4, v9, p0

    invoke-static {v0, v1, p1, v9}, LX/CPu;->a(LX/CNb;LX/CNc;LX/1De;[Ljava/lang/String;)LX/3rL;

    move-result-object v4

    goto/16 :goto_5

    .line 1885086
    :cond_b
    const-string v4, "text"

    goto :goto_6

    .line 1885087
    :cond_c
    const-string v4, "text-color"

    goto :goto_7

    .line 1885088
    :cond_d
    const-string v4, "background-image"

    goto :goto_8
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1885019
    invoke-static {}, LX/1dS;->b()V

    .line 1885020
    iget v0, p1, LX/1dQ;->b:I

    .line 1885021
    sparse-switch v0, :sswitch_data_0

    move-object v0, v1

    .line 1885022
    :goto_0
    return-object v0

    .line 1885023
    :sswitch_0
    iget-object v0, p1, LX/1dQ;->c:[Ljava/lang/Object;

    const/4 v2, 0x0

    aget-object v0, v0, v2

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    iget-object v0, p1, LX/1dQ;->c:[Ljava/lang/Object;

    const/4 v3, 0x1

    aget-object v0, v0, v3

    check-cast v0, LX/CNe;

    iget-object v3, p1, LX/1dQ;->a:LX/1X1;

    .line 1885024
    check-cast v3, LX/CPs;

    .line 1885025
    iget-object v4, v3, LX/CPs;->b:LX/CNc;

    iget-object v5, v3, LX/CPs;->a:LX/CNb;

    .line 1885026
    iget-object p0, v4, LX/CNc;->b:LX/CNS;

    invoke-virtual {p0}, LX/CNS;->a()V

    .line 1885027
    if-eqz v2, :cond_2

    .line 1885028
    iget-object p0, v4, LX/CNc;->b:LX/CNS;

    const-string p1, "0"

    const-string p2, "0"

    invoke-static {p1, p2}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object p1

    const-string p2, "selected"

    const-string v3, "pressed"

    invoke-static {p2, v3}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object p2

    invoke-virtual {p0, p1, p2, v5}, LX/CNS;->a(LX/0Px;LX/0Px;LX/CNb;)V

    .line 1885029
    :goto_1
    if-eqz v0, :cond_0

    .line 1885030
    invoke-interface {v0}, LX/CNe;->a()V

    .line 1885031
    :cond_0
    iget-object p0, v4, LX/CNc;->b:LX/CNS;

    invoke-virtual {p0}, LX/CNS;->b()V

    .line 1885032
    move-object v0, v1

    .line 1885033
    goto :goto_0

    .line 1885034
    :sswitch_1
    check-cast p2, LX/48J;

    .line 1885035
    iget-object v0, p2, LX/48J;->b:Landroid/view/MotionEvent;

    iget-object v1, p2, LX/48J;->a:Landroid/view/View;

    iget-object v2, p1, LX/1dQ;->a:LX/1X1;

    .line 1885036
    check-cast v2, LX/CPs;

    .line 1885037
    iget-object v3, v2, LX/CPs;->b:LX/CNc;

    iget-object v4, v2, LX/CPs;->a:LX/CNb;

    const/4 v2, 0x1

    .line 1885038
    invoke-virtual {v0}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v5

    if-nez v5, :cond_3

    .line 1885039
    iget-object v5, v3, LX/CNc;->b:LX/CNS;

    invoke-virtual {v5}, LX/CNS;->a()V

    .line 1885040
    iget-object v5, v3, LX/CNc;->b:LX/CNS;

    const-string p0, "1"

    invoke-static {p0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object p0

    const-string p1, "pressed"

    invoke-static {p1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object p1

    invoke-virtual {v5, p0, p1, v4}, LX/CNS;->a(LX/0Px;LX/0Px;LX/CNb;)V

    .line 1885041
    iget-object v5, v3, LX/CNc;->b:LX/CNS;

    invoke-virtual {v5}, LX/CNS;->b()V

    .line 1885042
    :cond_1
    :goto_2
    move v3, v2

    .line 1885043
    move v0, v3

    .line 1885044
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 1885045
    :cond_2
    iget-object p0, v4, LX/CNc;->b:LX/CNS;

    const-string p1, "1"

    const-string p2, "0"

    invoke-static {p1, p2}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object p1

    const-string p2, "selected"

    const-string v3, "pressed"

    invoke-static {p2, v3}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object p2

    invoke-virtual {p0, p1, p2, v5}, LX/CNS;->a(LX/0Px;LX/0Px;LX/CNb;)V

    goto :goto_1

    .line 1885046
    :cond_3
    invoke-virtual {v0}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v5

    const/4 p0, 0x3

    if-eq v5, p0, :cond_4

    invoke-virtual {v0}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v5

    if-ne v5, v2, :cond_1

    .line 1885047
    :cond_4
    invoke-virtual {v1}, Landroid/view/View;->performClick()Z

    goto :goto_2

    nop

    :sswitch_data_0
    .sparse-switch
        0x498e5903 -> :sswitch_0
        0x556f3d0c -> :sswitch_1
    .end sparse-switch
.end method
