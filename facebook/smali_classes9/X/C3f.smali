.class public LX/C3f;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:LX/C3b;


# direct methods
.method public constructor <init>(LX/C3b;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1846047
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1846048
    iput-object p1, p0, LX/C3f;->a:LX/C3b;

    .line 1846049
    return-void
.end method

.method public static a(LX/0QB;)LX/C3f;
    .locals 4

    .prologue
    .line 1846050
    const-class v1, LX/C3f;

    monitor-enter v1

    .line 1846051
    :try_start_0
    sget-object v0, LX/C3f;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1846052
    sput-object v2, LX/C3f;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1846053
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1846054
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1846055
    new-instance p0, LX/C3f;

    invoke-static {v0}, LX/C3b;->b(LX/0QB;)LX/C3b;

    move-result-object v3

    check-cast v3, LX/C3b;

    invoke-direct {p0, v3}, LX/C3f;-><init>(LX/C3b;)V

    .line 1846056
    move-object v0, p0

    .line 1846057
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1846058
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C3f;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1846059
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1846060
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
