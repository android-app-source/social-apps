.class public final LX/Asp;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field public final synthetic a:LX/Ass;


# direct methods
.method public constructor <init>(LX/Ass;)V
    .locals 0

    .prologue
    .line 1720905
    iput-object p1, p0, LX/Asp;->a:LX/Ass;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1720906
    iget-object v1, p0, LX/Asp;->a:LX/Ass;

    iget-boolean v1, v1, LX/Ass;->B:Z

    if-nez v1, :cond_1

    .line 1720907
    :cond_0
    :goto_0
    return v0

    .line 1720908
    :cond_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 1720909
    :pswitch_0
    iget-object v1, p0, LX/Asp;->a:LX/Ass;

    iget-object v1, v1, LX/Ass;->u:LX/0wd;

    const-wide v2, 0x3ff1c28f60000000L    # 1.1100000143051147

    invoke-virtual {v1, v2, v3}, LX/0wd;->b(D)LX/0wd;

    goto :goto_0

    .line 1720910
    :pswitch_1
    iget-object v1, p0, LX/Asp;->a:LX/Ass;

    iget-object v1, v1, LX/Ass;->u:LX/0wd;

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v1, v2, v3}, LX/0wd;->b(D)LX/0wd;

    .line 1720911
    iget-object v1, p0, LX/Asp;->a:LX/Ass;

    iget-boolean v1, v1, LX/Ass;->z:Z

    if-eqz v1, :cond_0

    .line 1720912
    iget-object v1, p0, LX/Asp;->a:LX/Ass;

    iget-object v1, v1, LX/Ass;->o:Landroid/view/View;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v1, v2}, Landroid/view/View;->setAlpha(F)V

    .line 1720913
    iget-object v1, p0, LX/Asp;->a:LX/Ass;

    invoke-static {v1}, LX/Ass;->r(LX/Ass;)V

    .line 1720914
    iget-object v1, p0, LX/Asp;->a:LX/Ass;

    .line 1720915
    iput-boolean v0, v1, LX/Ass;->z:Z

    .line 1720916
    iget-object v1, p0, LX/Asp;->a:LX/Ass;

    .line 1720917
    iput-boolean v0, v1, LX/Ass;->B:Z

    .line 1720918
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
