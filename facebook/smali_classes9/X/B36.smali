.class public final LX/B36;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/89S;


# instance fields
.field public final synthetic a:Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;)V
    .locals 0

    .prologue
    .line 1739565
    iput-object p1, p0, LX/B36;->a:Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/net/Uri;LX/89R;)V
    .locals 2

    .prologue
    .line 1739562
    iget-object v0, p0, LX/B36;->a:Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;

    iget-object v0, v0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->B:Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;

    if-eqz v0, :cond_0

    .line 1739563
    iget-object v0, p0, LX/B36;->a:Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;

    iget-object v0, v0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->B:Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;

    iget-object v1, p2, LX/89R;->c:Ljava/lang/String;

    invoke-virtual {v0, p1, v1}, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->a(Landroid/net/Uri;Ljava/lang/String;)V

    .line 1739564
    :cond_0
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1739561
    const/4 v0, 0x0

    return v0
.end method

.method public final b()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/FrameGraphQLInterfaces$Frame;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1739560
    iget-object v0, p0, LX/B36;->a:Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;

    iget-object v0, v0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->B:Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;

    invoke-virtual {v0}, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->d()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final b(Landroid/net/Uri;LX/89R;)V
    .locals 1

    .prologue
    .line 1739566
    iget-object v0, p0, LX/B36;->a:Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;

    iget-object v0, v0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->B:Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;

    if-eqz v0, :cond_0

    .line 1739567
    iget-object v0, p0, LX/B36;->a:Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;

    iget-object v0, v0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->B:Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->a(Landroid/net/Uri;LX/89R;)V

    .line 1739568
    :cond_0
    return-void
.end method

.method public final c()LX/89c;
    .locals 1

    .prologue
    .line 1739555
    iget-object v0, p0, LX/B36;->a:Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;

    iget-object v0, v0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->B:Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;

    return-object v0
.end method

.method public final d()LX/89f;
    .locals 1

    .prologue
    .line 1739559
    iget-object v0, p0, LX/B36;->a:Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;

    iget-object v0, v0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->B:Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;

    return-object v0
.end method

.method public final e()LX/BFW;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1739556
    const/4 v0, 0x0

    return-object v0
.end method

.method public final f()LX/B4P;
    .locals 1

    .prologue
    .line 1739557
    iget-object v0, p0, LX/B36;->a:Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;

    invoke-static {v0}, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->r(Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;)LX/B4P;

    move-result-object v0

    return-object v0
.end method

.method public final g()LX/89e;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1739558
    const/4 v0, 0x0

    return-object v0
.end method
