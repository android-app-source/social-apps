.class public abstract LX/B1d;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<MODE",
        "L:Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final e:Ljava/lang/Class;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Z

.field public c:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Enum;",
            "LX/3rL",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;>;"
        }
    .end annotation
.end field

.field public d:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

.field private final f:LX/1Ck;

.field public final g:LX/0tX;

.field public final h:LX/B1b;

.field public i:Ljava/util/concurrent/Callable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Callable",
            "<",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TMODE",
            "L;",
            ">;>;>;"
        }
    .end annotation
.end field

.field public j:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1736154
    const-class v0, LX/B1d;

    sput-object v0, LX/B1d;->e:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/1Ck;LX/0tX;LX/B1b;)V
    .locals 1

    .prologue
    .line 1736168
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-direct {p0, p1, p2, p3, v0}, LX/B1d;-><init>(LX/1Ck;LX/0tX;LX/B1b;Ljava/lang/Boolean;)V

    .line 1736169
    return-void
.end method

.method public constructor <init>(LX/1Ck;LX/0tX;LX/B1b;Ljava/lang/Boolean;)V
    .locals 1

    .prologue
    .line 1736159
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1736160
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/B1d;->c:Ljava/util/HashMap;

    .line 1736161
    iput-object p1, p0, LX/B1d;->f:LX/1Ck;

    .line 1736162
    iput-object p2, p0, LX/B1d;->g:LX/0tX;

    .line 1736163
    iput-object p3, p0, LX/B1d;->h:LX/B1b;

    .line 1736164
    invoke-virtual {p4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/B1d;->j:Z

    .line 1736165
    iget-object v0, p0, LX/B1d;->h:LX/B1b;

    if-eqz v0, :cond_0

    .line 1736166
    new-instance v0, LX/B1X;

    invoke-direct {v0, p0}, LX/B1X;-><init>(LX/B1d;)V

    iput-object v0, p0, LX/B1d;->i:Ljava/util/concurrent/Callable;

    .line 1736167
    :cond_0
    return-void
.end method

.method public static declared-synchronized a$redex0(LX/B1d;Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1736155
    monitor-enter p0

    :try_start_0
    sget-object v0, LX/B1d;->e:Ljava/lang/Class;

    invoke-virtual {p0}, LX/B1d;->h()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1736156
    iget-object v0, p0, LX/B1d;->h:LX/B1b;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/B1b;->a(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1736157
    monitor-exit p0

    return-void

    .line 1736158
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized b(LX/B1d;Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TMODE",
            "L;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1736115
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/B1d;->h:LX/B1b;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/B1b;->a(Z)V

    .line 1736116
    invoke-virtual {p0, p1}, LX/B1d;->a(Lcom/facebook/graphql/executor/GraphQLResult;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1736117
    monitor-exit p0

    return-void

    .line 1736118
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public a(Ljava/lang/Enum;)LX/0gW;
    .locals 1

    .prologue
    .line 1736153
    const/4 v0, 0x0

    return-object v0
.end method

.method public abstract a(Ljava/lang/String;)LX/0gW;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0gW",
            "<TMODE",
            "L;",
            ">;"
        }
    .end annotation
.end method

.method public abstract a(Lcom/facebook/graphql/executor/GraphQLResult;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TMODE",
            "L;",
            ">;)V"
        }
    .end annotation
.end method

.method public final declared-synchronized a(Ljava/lang/Enum;LX/B1b;)V
    .locals 4

    .prologue
    .line 1736147
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/B1d;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/B1d;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    if-nez v0, :cond_1

    .line 1736148
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 1736149
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/B1d;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3rL;

    .line 1736150
    iget-object v1, v0, LX/3rL;->b:Ljava/lang/Object;

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/3rL;->b:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1736151
    iget-object v1, p0, LX/B1d;->f:LX/1Ck;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, LX/B1c;->FETCH_MEMBERS_FOR_SECTION:LX/B1c;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, LX/B1Z;

    invoke-direct {v3, p0, p1, v0, p2}, LX/B1Z;-><init>(LX/B1d;Ljava/lang/Enum;LX/3rL;LX/B1b;)V

    new-instance v0, LX/B1a;

    invoke-direct {v0, p0, p2}, LX/B1a;-><init>(LX/B1d;LX/B1b;)V

    invoke-virtual {v1, v2, v3, v0}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1736152
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1736170
    invoke-virtual {p0}, LX/B1d;->i()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/B1d;->b:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Enum;)Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1736145
    iget-object v0, p0, LX/B1d;->c:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/B1d;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    .line 1736146
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, LX/B1d;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3rL;

    iget-object v0, v0, LX/3rL;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 1736144
    const/4 v0, 0x0

    return v0
.end method

.method public final d()LX/0P1;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0P1",
            "<",
            "Ljava/lang/Enum;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1736135
    invoke-virtual {p0}, LX/B1d;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1736136
    const/4 v0, 0x0

    .line 1736137
    :goto_0
    return-object v0

    .line 1736138
    :cond_0
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v2

    .line 1736139
    iget-object v0, p0, LX/B1d;->c:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Enum;

    .line 1736140
    iget-object v1, p0, LX/B1d;->c:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3rL;

    .line 1736141
    if-eqz v1, :cond_1

    iget-object v4, v1, LX/3rL;->b:Ljava/lang/Object;

    if-eqz v4, :cond_1

    iget-object v1, v1, LX/3rL;->b:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    :goto_2
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    goto :goto_1

    :cond_1
    const/4 v1, 0x1

    goto :goto_2

    .line 1736142
    :cond_2
    invoke-virtual {v2}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    move-object v0, v0

    .line 1736143
    goto :goto_0
.end method

.method public final declared-synchronized e()V
    .locals 1

    .prologue
    .line 1736128
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/B1d;->f:LX/1Ck;

    invoke-virtual {v0}, LX/1Ck;->c()V

    .line 1736129
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/B1d;->b:Z

    .line 1736130
    invoke-virtual {p0}, LX/B1d;->j()V

    .line 1736131
    const/4 v0, 0x0

    iput-object v0, p0, LX/B1d;->a:Ljava/lang/String;

    .line 1736132
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/B1d;->c:Ljava/util/HashMap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1736133
    monitor-exit p0

    return-void

    .line 1736134
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized f()V
    .locals 4

    .prologue
    .line 1736122
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/B1d;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 1736123
    :goto_0
    monitor-exit p0

    return-void

    .line 1736124
    :cond_0
    :try_start_1
    iget-object v0, p0, LX/B1d;->f:LX/1Ck;

    .line 1736125
    sget-object v1, LX/B1c;->FETCH_GROUP_MEMBERS:LX/B1c;

    move-object v1, v1

    .line 1736126
    iget-object v2, p0, LX/B1d;->i:Ljava/util/concurrent/Callable;

    new-instance v3, LX/B1Y;

    invoke-direct {v3, p0}, LX/B1Y;-><init>(LX/B1d;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1736127
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final g()V
    .locals 3

    .prologue
    .line 1736120
    iget-object v0, p0, LX/B1d;->h:LX/B1b;

    invoke-virtual {p0}, LX/B1d;->i()LX/0Px;

    move-result-object v1

    iget-object v2, p0, LX/B1d;->d:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    invoke-interface {v0, v1, v2}, LX/B1b;->a(LX/0Px;Lcom/facebook/graphql/enums/GraphQLGroupVisibility;)V

    .line 1736121
    return-void
.end method

.method public h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1736119
    const-string v0, "Group members fetch failed"

    return-object v0
.end method

.method public abstract i()LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end method

.method public abstract j()V
.end method
