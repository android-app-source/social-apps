.class public LX/AmA;
.super LX/4or;
.source ""


# instance fields
.field public a:Lcom/facebook/feed/util/injection/FeedClientSideInjectionTool;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1710512
    invoke-direct {p0, p1}, LX/4or;-><init>(Landroid/content/Context;)V

    .line 1710513
    const-class v0, LX/AmA;

    invoke-static {v0, p0, p1}, LX/AmA;->a(Ljava/lang/Class;Ljava/lang/Object;Landroid/content/Context;)V

    .line 1710514
    invoke-virtual {p0, v2}, LX/AmA;->setPersistent(Z)V

    .line 1710515
    const-string v0, "Client Side Injection Helper"

    invoke-virtual {p0, v0}, LX/AmA;->setTitle(Ljava/lang/CharSequence;)V

    .line 1710516
    const-string v0, "Some helper functions for story injection"

    invoke-virtual {p0, v0}, LX/AmA;->setSummary(Ljava/lang/CharSequence;)V

    .line 1710517
    const-string v0, "Do things!"

    invoke-virtual {p0, v0}, LX/AmA;->setDialogTitle(Ljava/lang/CharSequence;)V

    .line 1710518
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "Detect a new set of stories and clear cache"

    aput-object v1, v0, v2

    const-string v1, "Clear cached stories"

    aput-object v1, v0, v3

    const-string v1, "Remove feed-inject.json file"

    aput-object v1, v0, v4

    invoke-virtual {p0, v0}, LX/AmA;->setEntries([Ljava/lang/CharSequence;)V

    .line 1710519
    new-array v0, v5, [Ljava/lang/String;

    sget-object v1, LX/32M;->DETECT:LX/32M;

    invoke-virtual {v1}, LX/32M;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v2

    sget-object v1, LX/32M;->CLEAR:LX/32M;

    invoke-virtual {v1}, LX/32M;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v3

    sget-object v1, LX/32M;->REMOVE:LX/32M;

    invoke-virtual {v1}, LX/32M;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v4

    invoke-virtual {p0, v0}, LX/AmA;->setEntryValues([Ljava/lang/CharSequence;)V

    .line 1710520
    return-void
.end method

.method private static a(Ljava/lang/Class;Ljava/lang/Object;Landroid/content/Context;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-static {p2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/AmA;

    invoke-static {p0}, Lcom/facebook/feed/util/injection/FeedClientSideInjectionTool;->a(LX/0QB;)Lcom/facebook/feed/util/injection/FeedClientSideInjectionTool;

    move-result-object p0

    check-cast p0, Lcom/facebook/feed/util/injection/FeedClientSideInjectionTool;

    iput-object p0, p1, LX/AmA;->a:Lcom/facebook/feed/util/injection/FeedClientSideInjectionTool;

    return-void
.end method


# virtual methods
.method public final getPersistedString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1710521
    const-string v0, ""

    return-object v0
.end method

.method public final persistString(Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1710522
    if-nez p1, :cond_0

    .line 1710523
    :goto_0
    return v2

    .line 1710524
    :cond_0
    :try_start_0
    invoke-static {p1}, LX/32M;->valueOf(Ljava/lang/String;)LX/32M;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1710525
    iget-object v1, p0, LX/AmA;->a:Lcom/facebook/feed/util/injection/FeedClientSideInjectionTool;

    invoke-virtual {v1, v0}, Lcom/facebook/feed/util/injection/FeedClientSideInjectionTool;->a(LX/32M;)V

    goto :goto_0

    .line 1710526
    :catch_0
    goto :goto_0
.end method
