.class public LX/AYv;
.super LX/AWU;
.source ""

# interfaces
.implements LX/1bG;


# instance fields
.field public a:LX/BSb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/AYq;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/3HT;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final f:LX/AZ4;

.field private final g:LX/AYz;

.field public final h:LX/AZ9;

.field private final i:LX/AYu;

.field public j:Lcom/facebook/facecast/plugin/commercialbreak/FacecastStartCommercialBreakPlugin;

.field public k:Z

.field public l:Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1686333
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/AYv;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1686334
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1686331
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/AYv;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1686332
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 1686322
    invoke-direct {p0, p1, p2, p3}, LX/AWU;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1686323
    new-instance v0, LX/AYu;

    invoke-direct {v0, p0}, LX/AYu;-><init>(LX/AYv;)V

    iput-object v0, p0, LX/AYv;->i:LX/AYu;

    .line 1686324
    const-class v0, LX/AYv;

    invoke-static {v0, p0}, LX/AYv;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1686325
    new-instance v0, LX/AZ4;

    invoke-direct {v0, p1}, LX/AZ4;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/AYv;->f:LX/AZ4;

    .line 1686326
    new-instance v0, LX/AYz;

    invoke-direct {v0, p1}, LX/AYz;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/AYv;->g:LX/AYz;

    .line 1686327
    new-instance v0, LX/AZ9;

    invoke-direct {v0, p1}, LX/AZ9;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/AYv;->h:LX/AZ9;

    .line 1686328
    const/4 v0, 0x3

    new-array v0, v0, [LX/AWT;

    iget-object v1, p0, LX/AYv;->f:LX/AZ4;

    aput-object v1, v0, v2

    const/4 v1, 0x1

    iget-object v2, p0, LX/AYv;->g:LX/AYz;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, LX/AYv;->h:LX/AZ9;

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, LX/AWU;->a([LX/AWT;)V

    .line 1686329
    iget-object v0, p0, LX/AYv;->b:LX/AYq;

    iget-object v1, p0, LX/AYv;->h:LX/AZ9;

    invoke-virtual {v0, v1}, LX/AYq;->a(LX/1bG;)V

    .line 1686330
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/AYv;

    invoke-static {p0}, LX/BSb;->a(LX/0QB;)LX/BSb;

    move-result-object v1

    check-cast v1, LX/BSb;

    invoke-static {p0}, LX/AYq;->a(LX/0QB;)LX/AYq;

    move-result-object v2

    check-cast v2, LX/AYq;

    invoke-static {p0}, LX/3HT;->a(LX/0QB;)LX/3HT;

    move-result-object p0

    check-cast p0, LX/3HT;

    iput-object v1, p1, LX/AYv;->a:LX/BSb;

    iput-object v2, p1, LX/AYv;->b:LX/AYq;

    iput-object p0, p1, LX/AYv;->c:LX/3HT;

    return-void
.end method

.method private c()V
    .locals 13

    .prologue
    .line 1686335
    iget-object v0, p0, LX/AYv;->l:Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;

    if-nez v0, :cond_0

    .line 1686336
    :goto_0
    return-void

    .line 1686337
    :cond_0
    iget-object v0, p0, LX/AYv;->a:LX/BSb;

    iget-object v1, p0, LX/AYv;->l:Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;

    iget v1, v1, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->commercialBreakLengthMs:I

    int-to-long v2, v1

    iget-object v1, p0, LX/AYv;->b:LX/AYq;

    .line 1686338
    iget v4, v1, LX/AYq;->e:I

    move v1, v4

    .line 1686339
    iget-object v4, v0, LX/BSb;->e:Ljava/lang/String;

    if-nez v4, :cond_1

    .line 1686340
    :goto_1
    iget-object v0, p0, LX/AYv;->f:LX/AZ4;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/AZ4;->setVisibility(I)V

    .line 1686341
    iget-object v0, p0, LX/AYv;->f:LX/AZ4;

    .line 1686342
    iget-object v4, v0, LX/AZ4;->r:Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;

    if-nez v4, :cond_2

    .line 1686343
    :goto_2
    goto :goto_0

    .line 1686344
    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, v0, LX/BSb;->d:LX/2ml;

    invoke-virtual {v5}, LX/2ml;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v0, LX/BSb;->e:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1686345
    iget-object v5, v0, LX/BSb;->c:LX/0lC;

    invoke-virtual {v5}, LX/0lC;->e()LX/0m9;

    move-result-object v5

    .line 1686346
    const-string v6, "type"

    sget-object v7, LX/BSZ;->INTENT:LX/BSZ;

    invoke-virtual {v7}, LX/BSZ;->name()Ljava/lang/String;

    move-result-object v7

    sget-object v8, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v7, v8}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 1686347
    const-string v6, "commercial_break_length_ms"

    invoke-virtual {v5, v6, v2, v3}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    .line 1686348
    const-string v6, "index"

    invoke-virtual {v5, v6, v1}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 1686349
    invoke-static {v0, v4, v5}, LX/BSb;->a(LX/BSb;Ljava/lang/String;LX/0m9;)V

    goto :goto_1

    .line 1686350
    :cond_2
    const/4 v5, 0x0

    .line 1686351
    iget-object v4, v0, LX/AZ4;->h:LX/1b4;

    .line 1686352
    iget-object v6, v4, LX/1b4;->b:LX/0Uh;

    const/16 v7, 0x356

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, LX/0Uh;->a(IZ)Z

    move-result v6

    move v4, v6

    .line 1686353
    if-nez v4, :cond_3

    .line 1686354
    iget-object v4, v0, LX/AZ4;->p:LX/AZ3;

    invoke-virtual {v4}, LX/AZ3;->start()Landroid/os/CountDownTimer;

    .line 1686355
    iget-object v4, v0, LX/AZ4;->m:Landroid/widget/ProgressBar;

    invoke-virtual {v4, v5}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1686356
    iget-object v4, v0, LX/AZ4;->n:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    invoke-virtual {v4, v5}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;->setEnabled(Z)V

    .line 1686357
    iget-object v4, v0, LX/AZ4;->o:Lcom/facebook/widget/text/BetterTextView;

    const/4 v5, 0x4

    invoke-virtual {v4, v5}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 1686358
    :goto_3
    invoke-static {v0}, LX/AZ4;->h(LX/AZ4;)V

    .line 1686359
    iget-object v4, v0, LX/AZ4;->g:LX/AYr;

    iget v5, v0, LX/AZ4;->s:I

    iget v6, v0, LX/AZ4;->t:I

    iget v7, v0, LX/AZ4;->u:I

    iget-object v8, v0, LX/AZ4;->r:Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;

    iget-object v8, v8, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->currencyCode:Ljava/lang/String;

    .line 1686360
    new-instance v9, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v10, "commercial_break_broadcaster_intent"

    invoke-direct {v9, v10}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v10, "commercial_break_broadcaster"

    .line 1686361
    iput-object v10, v9, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1686362
    move-object v9, v9

    .line 1686363
    const-string v10, "broadcaster_id"

    iget-object v11, v4, LX/AYr;->b:Ljava/lang/String;

    invoke-virtual {v9, v10, v11}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v9

    const-string v10, "video_id"

    iget-object v11, v4, LX/AYr;->c:Ljava/lang/String;

    invoke-virtual {v9, v10, v11}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v9

    const-string v10, "time_offset_ms"

    iget-wide v11, v4, LX/AYr;->d:J

    invoke-virtual {v9, v10, v11, v12}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v9

    const-string v10, "concurrent_viewer_count"

    invoke-virtual {v9, v10, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v9

    const-string v10, "estimate_high"

    invoke-virtual {v9, v10, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v9

    const-string v10, "estimate_low"

    invoke-virtual {v9, v10, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v9

    const-string v10, "currency"

    invoke-virtual {v9, v10, v8}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v9

    .line 1686364
    iget-object v10, v4, LX/AYr;->a:LX/0Zb;

    invoke-interface {v10, v9}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1686365
    iget-object v4, v0, LX/AZ4;->k:Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v4

    .line 1686366
    iget-object v5, v0, LX/AZ4;->k:Landroid/widget/LinearLayout;

    int-to-float v6, v4

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->setTranslationY(F)V

    .line 1686367
    iget-object v5, v0, LX/AZ4;->k:Landroid/widget/LinearLayout;

    invoke-virtual {v5}, Landroid/widget/LinearLayout;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    neg-int v4, v4

    int-to-float v4, v4

    invoke-virtual {v5, v4}, Landroid/view/ViewPropertyAnimator;->translationYBy(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    const-wide/16 v6, 0x258

    invoke-virtual {v4, v6, v7}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    sget-object v5, LX/BSf;->a:Landroid/view/animation/Interpolator;

    invoke-virtual {v4, v5}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/ViewPropertyAnimator;->start()V

    goto/16 :goto_2

    .line 1686368
    :cond_3
    invoke-static {v0}, LX/AZ4;->j(LX/AZ4;)V

    goto :goto_3
.end method


# virtual methods
.method public final a(JJ)LX/AZC;
    .locals 7

    .prologue
    const-wide/16 v4, 0x3e8

    .line 1686304
    iget-object v0, p0, LX/AYv;->l:Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;

    if-nez v0, :cond_0

    .line 1686305
    sget-object v0, LX/AZC;->NOT_ONBOARDED:LX/AZC;

    .line 1686306
    :goto_0
    return-object v0

    .line 1686307
    :cond_0
    iget-object v0, p0, LX/AYv;->b:LX/AYq;

    .line 1686308
    iget-object v1, v0, LX/AYq;->b:LX/AYp;

    move-object v0, v1

    .line 1686309
    sget-object v1, LX/AYp;->COMMERCIAL_BREAK_INELIGIBLE_DUE_TO_VIOLATION:LX/AYp;

    if-ne v0, v1, :cond_1

    .line 1686310
    sget-object v0, LX/AZC;->VIOLATION:LX/AZC;

    goto :goto_0

    .line 1686311
    :cond_1
    iget-object v1, p0, LX/AYv;->l:Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;

    iget-boolean v1, v1, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->isOnboarded:Z

    if-nez v1, :cond_2

    .line 1686312
    sget-object v0, LX/AZC;->NOT_ONBOARDED:LX/AZC;

    goto :goto_0

    .line 1686313
    :cond_2
    sget-object v1, LX/AYp;->COMMERCIAL_BREAK_STARTED:LX/AYp;

    if-ne v0, v1, :cond_3

    .line 1686314
    sget-object v0, LX/AZC;->IN_COMMERCIAL_BREAK:LX/AZC;

    goto :goto_0

    .line 1686315
    :cond_3
    iget-object v0, p0, LX/AYv;->l:Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;

    iget v0, v0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->firstCommercialEligibleSecs:I

    int-to-long v0, v0

    mul-long/2addr v0, v4

    cmp-long v0, p1, v0

    if-gez v0, :cond_4

    .line 1686316
    sget-object v0, LX/AZC;->INSUFFICIENT_BEGINNING_LIVE_TIME:LX/AZC;

    goto :goto_0

    .line 1686317
    :cond_4
    const-wide/16 v0, 0x0

    cmp-long v0, p3, v0

    if-lez v0, :cond_5

    sub-long v0, p1, p3

    iget-object v2, p0, LX/AYv;->l:Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;

    iget v2, v2, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->timeBetweenCommercialsEligibleSecs:I

    int-to-long v2, v2

    mul-long/2addr v2, v4

    cmp-long v0, v0, v2

    if-gez v0, :cond_5

    .line 1686318
    sget-object v0, LX/AZC;->TOO_CLOSE_TO_PREVIOUS_COMMERCIAL_BREAK:LX/AZC;

    goto :goto_0

    .line 1686319
    :cond_5
    iget-boolean v0, p0, LX/AYv;->k:Z

    if-nez v0, :cond_6

    .line 1686320
    sget-object v0, LX/AZC;->VIEWER_COUNT_TOO_LOW:LX/AZC;

    goto :goto_0

    .line 1686321
    :cond_6
    sget-object v0, LX/AZC;->ELIGIBLE:LX/AZC;

    goto :goto_0
.end method

.method public final a(LX/AYp;LX/AYp;)V
    .locals 6

    .prologue
    .line 1686291
    sget-object v0, LX/AYt;->a:[I

    invoke-virtual {p2}, LX/AYp;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1686292
    :goto_0
    sget-object v0, LX/AYt;->a:[I

    invoke-virtual {p1}, LX/AYp;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    .line 1686293
    :goto_1
    return-void

    .line 1686294
    :pswitch_0
    iget-object v0, p0, LX/AYv;->f:LX/AZ4;

    .line 1686295
    iget-object v2, v0, LX/AZ4;->k:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v2

    .line 1686296
    iget-object v3, v0, LX/AZ4;->k:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    int-to-float v2, v2

    invoke-virtual {v3, v2}, Landroid/view/ViewPropertyAnimator;->translationYBy(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    const-wide/16 v4, 0x258

    invoke-virtual {v2, v4, v5}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    sget-object v3, LX/BSf;->a:Landroid/view/animation/Interpolator;

    invoke-virtual {v2, v3}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 1686297
    goto :goto_0

    .line 1686298
    :pswitch_1
    invoke-direct {p0}, LX/AYv;->c()V

    goto :goto_1

    .line 1686299
    :pswitch_2
    iget-object v0, p0, LX/AYv;->h:LX/AZ9;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/AZ9;->setVisibility(I)V

    .line 1686300
    goto :goto_1

    .line 1686301
    :pswitch_3
    iget-object v0, p0, LX/AYv;->j:Lcom/facebook/facecast/plugin/commercialbreak/FacecastStartCommercialBreakPlugin;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/facecast/plugin/commercialbreak/FacecastStartCommercialBreakPlugin;->setVisibility(I)V

    .line 1686302
    iget-object v0, p0, LX/AYv;->h:LX/AZ9;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, LX/AZ9;->setVisibility(I)V

    .line 1686303
    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final a(LX/AZC;JJ)V
    .locals 8

    .prologue
    .line 1686258
    iget-object v0, p0, LX/AYv;->l:Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;

    if-nez v0, :cond_0

    .line 1686259
    :goto_0
    return-void

    .line 1686260
    :cond_0
    sget-object v0, LX/AZC;->TOO_CLOSE_TO_PREVIOUS_COMMERCIAL_BREAK:LX/AZC;

    if-ne p1, v0, :cond_1

    .line 1686261
    iget-object v0, p0, LX/AYv;->l:Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;

    iget v0, v0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->timeBetweenCommercialsEligibleSecs:I

    int-to-long v0, v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    sub-long v2, p2, p4

    sub-long p2, v0, v2

    .line 1686262
    :cond_1
    iget-object v0, p0, LX/AYv;->g:LX/AYz;

    iget-object v1, p0, LX/AYv;->l:Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;

    const/4 v5, 0x1

    const/4 p4, 0x0

    .line 1686263
    const/16 v3, 0x8

    .line 1686264
    iget-object v2, v0, LX/AYz;->j:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v2, v3}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 1686265
    iget-object v2, v0, LX/AYz;->g:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v2, v3}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1686266
    iget-object v2, v0, LX/AYz;->f:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v2, v3}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1686267
    iget-object v2, v0, LX/AYz;->h:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v2, v3}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1686268
    sget-object v2, LX/AYx;->a:[I

    invoke-virtual {p1}, LX/AZC;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 1686269
    :goto_1
    iget-object v0, p0, LX/AYv;->g:LX/AYz;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/AYz;->setVisibility(I)V

    .line 1686270
    iget-object v0, p0, LX/AYv;->g:LX/AYz;

    .line 1686271
    iget-boolean v4, v0, LX/AYz;->m:Z

    if-eqz v4, :cond_2

    .line 1686272
    :goto_2
    goto :goto_0

    .line 1686273
    :pswitch_0
    iget-object v2, v0, LX/AYz;->h:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v2, p4}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1686274
    iget-object v2, v0, LX/AYz;->k:Landroid/widget/TextView;

    invoke-virtual {v0}, LX/AYz;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080ce4

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1686275
    iget-object v2, v0, LX/AYz;->i:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0}, LX/AYz;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080ce5

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 1686276
    :pswitch_1
    new-instance v2, LX/AYy;

    invoke-direct {v2, v0, p2, p3}, LX/AYy;-><init>(LX/AYz;J)V

    iput-object v2, v0, LX/AYz;->l:LX/AYy;

    .line 1686277
    iget-object v2, v0, LX/AYz;->j:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v2, p4}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 1686278
    iget-object v2, v0, LX/AYz;->l:LX/AYy;

    invoke-virtual {v2}, LX/AYy;->start()Landroid/os/CountDownTimer;

    .line 1686279
    iget-object v2, v0, LX/AYz;->k:Landroid/widget/TextView;

    invoke-virtual {v0}, LX/AYz;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080ce3

    new-array v5, v5, [Ljava/lang/Object;

    iget v6, v1, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->timeBetweenCommercialsEligibleSecs:I

    invoke-static {v6}, LX/AYz;->c(I)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, p4

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1686280
    iget-object v2, v0, LX/AYz;->i:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0}, LX/AYz;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080ccf

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 1686281
    :pswitch_2
    iget-object v2, v0, LX/AYz;->k:Landroid/widget/TextView;

    invoke-virtual {v0}, LX/AYz;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080ce2

    new-array v5, v5, [Ljava/lang/Object;

    iget v6, v1, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->firstCommercialEligibleSecs:I

    invoke-static {v6}, LX/AYz;->c(I)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, p4

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1686282
    iget-object v2, v0, LX/AYz;->g:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v2, p4}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1686283
    iget-object v2, v0, LX/AYz;->i:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0}, LX/AYz;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080ccd

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 1686284
    :pswitch_3
    iget-object v2, v0, LX/AYz;->k:Landroid/widget/TextView;

    invoke-virtual {v0}, LX/AYz;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080ce6

    new-array v5, v5, [Ljava/lang/Object;

    iget v6, v1, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->viewerCountThreshold:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, p4

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1686285
    iget-object v2, v0, LX/AYz;->f:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v2, p4}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1686286
    iget-object v2, v0, LX/AYz;->i:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0}, LX/AYz;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080cce

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 1686287
    :cond_2
    const/4 v4, 0x1

    iput-boolean v4, v0, LX/AYz;->m:Z

    .line 1686288
    iget-object v4, v0, LX/AYz;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v4}, Landroid/widget/LinearLayout;->getHeight()I

    move-result v4

    .line 1686289
    iget-object v5, v0, LX/AYz;->c:Landroid/widget/LinearLayout;

    int-to-float v6, v4

    invoke-virtual {v5, v6}, Landroid/widget/LinearLayout;->setTranslationY(F)V

    .line 1686290
    iget-object v5, v0, LX/AYz;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v5}, Landroid/widget/LinearLayout;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v5

    neg-int v4, v4

    int-to-float v4, v4

    invoke-virtual {v5, v4}, Landroid/view/ViewPropertyAnimator;->translationYBy(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    const-wide/16 v6, 0x258

    invoke-virtual {v4, v6, v7}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    sget-object v5, LX/BSf;->a:Landroid/view/animation/Interpolator;

    invoke-virtual {v4, v5}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/ViewPropertyAnimator;->start()V

    goto/16 :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final a(Landroid/view/ViewGroup;LX/AVF;)V
    .locals 2

    .prologue
    .line 1686221
    invoke-super {p0, p1, p2}, LX/AWU;->a(Landroid/view/ViewGroup;LX/AVF;)V

    .line 1686222
    iget-object v0, p0, LX/AYv;->b:LX/AYq;

    iget-object v1, p0, LX/AYv;->j:Lcom/facebook/facecast/plugin/commercialbreak/FacecastStartCommercialBreakPlugin;

    invoke-virtual {v0, v1}, LX/AYq;->b(LX/1bG;)V

    .line 1686223
    iget-object v0, p0, LX/AYv;->b:LX/AYq;

    iget-object v1, p0, LX/AYv;->h:LX/AZ9;

    invoke-virtual {v0, v1}, LX/AYq;->b(LX/1bG;)V

    .line 1686224
    iget-object v0, p0, LX/AYv;->c:LX/3HT;

    iget-object v1, p0, LX/AYv;->i:LX/AYu;

    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 1686225
    return-void
.end method

.method public final a(Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;Landroid/view/View;LX/AY8;ZZ)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/AY8;",
            ":",
            "Lcom/facebook/facecast/plugin/commercialbreak/FacecastCommercialBreakPromptPlugin$FacecastCommercialBreakPromptListener;",
            ":",
            "Lcom/facebook/facecast/plugin/commercialbreak/FacecastPlayCommercialBreakPlugin$FacecastPlayCommercialBreakListener;",
            ">(",
            "Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;",
            "Landroid/view/View;",
            "TT;ZZ)V"
        }
    .end annotation

    .prologue
    .line 1686226
    iput-object p1, p0, LX/AYv;->l:Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;

    .line 1686227
    iget v0, p1, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->viewerCountThreshold:I

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LX/AYv;->k:Z

    .line 1686228
    const v0, 0x7f0d1059

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecast/plugin/commercialbreak/FacecastStartCommercialBreakPlugin;

    iput-object v0, p0, LX/AYv;->j:Lcom/facebook/facecast/plugin/commercialbreak/FacecastStartCommercialBreakPlugin;

    .line 1686229
    iget-object v0, p0, LX/AYv;->b:LX/AYq;

    iget-object v1, p0, LX/AYv;->j:Lcom/facebook/facecast/plugin/commercialbreak/FacecastStartCommercialBreakPlugin;

    invoke-virtual {v0, v1}, LX/AYq;->a(LX/1bG;)V

    .line 1686230
    iget-object v0, p0, LX/AYv;->j:Lcom/facebook/facecast/plugin/commercialbreak/FacecastStartCommercialBreakPlugin;

    invoke-virtual {v0, p1, p3}, Lcom/facebook/facecast/plugin/commercialbreak/FacecastStartCommercialBreakPlugin;->a(Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;LX/AY8;)V

    .line 1686231
    iget-object v1, p0, LX/AYv;->f:LX/AZ4;

    move-object v0, p3

    check-cast v0, LX/AY9;

    iget-object v2, p0, LX/AYv;->l:Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;

    .line 1686232
    iput-object v0, v1, LX/AZ4;->q:LX/AY9;

    .line 1686233
    iput-object v2, v1, LX/AZ4;->r:Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;

    .line 1686234
    iget-object v3, v1, LX/AZ4;->c:LX/3HT;

    iget-object p1, v1, LX/AZ4;->l:LX/AZ2;

    invoke-virtual {v3, p1}, LX/0b4;->a(LX/0b2;)Z

    .line 1686235
    invoke-static {v1}, LX/AZ4;->h(LX/AZ4;)V

    .line 1686236
    iget-object p1, v1, LX/AZ4;->k:Landroid/widget/LinearLayout;

    if-eqz p4, :cond_2

    const v3, 0x3f666666    # 0.9f

    :goto_1
    invoke-virtual {p1, v3}, Landroid/widget/LinearLayout;->setAlpha(F)V

    .line 1686237
    if-eqz p5, :cond_3

    .line 1686238
    iget-object v3, v1, LX/AZ4;->b:LX/0hB;

    invoke-virtual {v3}, LX/0hB;->d()I

    move-result v3

    int-to-float v3, v3

    const p1, 0x3f19999a    # 0.6f

    mul-float/2addr v3, p1

    float-to-int v3, v3

    .line 1686239
    iget-object p1, v1, LX/AZ4;->k:Landroid/widget/LinearLayout;

    invoke-virtual {p1, v3}, Landroid/widget/LinearLayout;->setMinimumHeight(I)V

    .line 1686240
    :goto_2
    iget-object v0, p0, LX/AYv;->g:LX/AYz;

    .line 1686241
    iget-object v2, v0, LX/AYz;->c:Landroid/widget/LinearLayout;

    if-eqz p4, :cond_4

    const v1, 0x3f666666    # 0.9f

    :goto_3
    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->setAlpha(F)V

    .line 1686242
    if-eqz p5, :cond_5

    .line 1686243
    iget-object v1, v0, LX/AYz;->a:LX/0hB;

    invoke-virtual {v1}, LX/0hB;->d()I

    move-result v1

    int-to-float v1, v1

    const v2, 0x3f19999a    # 0.6f

    mul-float/2addr v1, v2

    float-to-int v1, v1

    .line 1686244
    iget-object v2, v0, LX/AYz;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->setMinimumHeight(I)V

    .line 1686245
    :goto_4
    iget-object v0, p0, LX/AYv;->h:LX/AZ9;

    check-cast p3, LX/AY9;

    .line 1686246
    iput-object p3, v0, LX/AZ9;->l:LX/AY9;

    .line 1686247
    iget-object v0, p0, LX/AYv;->l:Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;

    iget-object v0, v0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->broadcasterViolations:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/AYv;->l:Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;

    iget-object v0, v0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->broadcasterViolations:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1686248
    iget-object v0, p0, LX/AYv;->b:LX/AYq;

    sget-object v1, LX/AYp;->COMMERCIAL_BREAK_INELIGIBLE_DUE_TO_VIOLATION:LX/AYp;

    invoke-virtual {v0, v1}, LX/AYq;->a(LX/AYp;)V

    .line 1686249
    :cond_0
    iget-object v0, p0, LX/AYv;->c:LX/3HT;

    iget-object v1, p0, LX/AYv;->i:LX/AYu;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 1686250
    return-void

    .line 1686251
    :cond_1
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 1686252
    :cond_2
    const/high16 v3, 0x3f800000    # 1.0f

    goto :goto_1

    .line 1686253
    :cond_3
    iget-object v3, v1, LX/AZ4;->k:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1686254
    const/4 p1, 0x3

    const p2, 0x7f0d0fc9

    invoke-virtual {v3, p1, p2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    goto :goto_2

    .line 1686255
    :cond_4
    const/high16 v1, 0x3f800000    # 1.0f

    goto :goto_3

    .line 1686256
    :cond_5
    iget-object v1, v0, LX/AYz;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1686257
    const/4 v2, 0x3

    const v3, 0x7f0d0fc9

    invoke-virtual {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    goto :goto_4
.end method
