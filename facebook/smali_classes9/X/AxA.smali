.class public LX/AxA;
.super LX/3mX;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/3mX",
        "<",
        "Ljava/lang/String;",
        "TE;>;"
    }
.end annotation


# instance fields
.field private final c:LX/AxF;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Px;LX/25M;LX/1Pq;LX/AxF;)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/0Px;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/25M;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/1Pq;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/25M;",
            "TE;",
            "LX/AxF;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1727611
    invoke-direct {p0, p1, p2, p4, p3}, LX/3mX;-><init>(Landroid/content/Context;LX/0Px;LX/1Pq;LX/25M;)V

    .line 1727612
    iput-object p5, p0, LX/AxA;->c:LX/AxF;

    .line 1727613
    return-void
.end method


# virtual methods
.method public final a(LX/1De;)LX/1X1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1727615
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/1De;Ljava/lang/Object;)LX/1X1;
    .locals 3

    .prologue
    .line 1727616
    check-cast p2, Ljava/lang/String;

    .line 1727617
    iget-object v0, p0, LX/AxA;->c:LX/AxF;

    const/4 v1, 0x0

    .line 1727618
    new-instance v2, LX/AxE;

    invoke-direct {v2, v0}, LX/AxE;-><init>(LX/AxF;)V

    .line 1727619
    sget-object p0, LX/AxF;->a:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/AxD;

    .line 1727620
    if-nez p0, :cond_0

    .line 1727621
    new-instance p0, LX/AxD;

    invoke-direct {p0}, LX/AxD;-><init>()V

    .line 1727622
    :cond_0
    invoke-static {p0, p1, v1, v1, v2}, LX/AxD;->a$redex0(LX/AxD;LX/1De;IILX/AxE;)V

    .line 1727623
    move-object v2, p0

    .line 1727624
    move-object v1, v2

    .line 1727625
    move-object v0, v1

    .line 1727626
    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b1a15

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 1727627
    iget-object v2, v0, LX/AxD;->a:LX/AxE;

    iput v1, v2, LX/AxE;->a:I

    .line 1727628
    iget-object v2, v0, LX/AxD;->d:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v2, p0}, Ljava/util/BitSet;->set(I)V

    .line 1727629
    move-object v0, v0

    .line 1727630
    iget-object v1, v0, LX/AxD;->a:LX/AxE;

    iput-object p2, v1, LX/AxE;->b:Ljava/lang/String;

    .line 1727631
    iget-object v1, v0, LX/AxD;->d:Ljava/util/BitSet;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->set(I)V

    .line 1727632
    move-object v0, v0

    .line 1727633
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 1727614
    const/4 v0, 0x0

    return v0
.end method
