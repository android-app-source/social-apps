.class public LX/Cdp;
.super LX/0bY;
.source ""


# instance fields
.field private final a:LX/Cdn;


# direct methods
.method public constructor <init>(LX/Cdn;)V
    .locals 0

    .prologue
    .line 1923471
    invoke-direct {p0}, LX/0bY;-><init>()V

    .line 1923472
    iput-object p1, p0, LX/Cdp;->a:LX/Cdn;

    .line 1923473
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 6

    .prologue
    .line 1923474
    iget-object v0, p0, LX/Cdp;->a:LX/Cdn;

    .line 1923475
    iget v1, v0, LX/Cdn;->e:I

    move v0, v1

    .line 1923476
    if-nez v0, :cond_0

    .line 1923477
    const-string v0, "Empty Ble Scan: %d millis"

    iget-object v1, p0, LX/Cdp;->a:LX/Cdn;

    .line 1923478
    iget-wide v4, v1, LX/Cdn;->d:J

    move-wide v2, v4

    .line 1923479
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1923480
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "Successful Ble Scan: %d millis\n%d detection events across %d devices"

    iget-object v1, p0, LX/Cdp;->a:LX/Cdn;

    .line 1923481
    iget-wide v4, v1, LX/Cdn;->d:J

    move-wide v2, v4

    .line 1923482
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iget-object v2, p0, LX/Cdp;->a:LX/Cdn;

    .line 1923483
    iget v3, v2, LX/Cdn;->e:I

    move v2, v3

    .line 1923484
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget-object v3, p0, LX/Cdp;->a:LX/Cdn;

    .line 1923485
    iget-object v4, v3, LX/Cdn;->a:LX/0Px;

    move-object v3, v4

    .line 1923486
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
