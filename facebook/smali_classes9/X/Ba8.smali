.class public LX/Ba8;
.super LX/BZy;
.source ""

# interfaces
.implements LX/8pS;


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:LX/29r;

.field public final c:LX/03V;

.field private d:Landroid/widget/TextView;

.field public e:Lcom/facebook/widget/ratingbar/BetterRatingBar;

.field private f:Landroid/widget/TextView;

.field private g:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1798553
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, LX/Ba8;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Ba8;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/29r;LX/03V;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1798545
    invoke-direct {p0}, LX/BZy;-><init>()V

    .line 1798546
    iput-object v0, p0, LX/Ba8;->d:Landroid/widget/TextView;

    .line 1798547
    iput-object v0, p0, LX/Ba8;->e:Lcom/facebook/widget/ratingbar/BetterRatingBar;

    .line 1798548
    iput-object v0, p0, LX/Ba8;->f:Landroid/widget/TextView;

    .line 1798549
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Ba8;->g:Z

    .line 1798550
    iput-object p1, p0, LX/Ba8;->b:LX/29r;

    .line 1798551
    iput-object p2, p0, LX/Ba8;->c:LX/03V;

    .line 1798552
    return-void
.end method

.method private c(I)V
    .locals 2

    .prologue
    .line 1798536
    iget-object v0, p0, LX/Ba8;->f:Landroid/widget/TextView;

    if-nez v0, :cond_0

    .line 1798537
    :goto_0
    return-void

    .line 1798538
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 1798539
    iget-object v0, p0, LX/Ba8;->f:Landroid/widget/TextView;

    const-string v1, "____"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1798540
    :pswitch_0
    iget-object v0, p0, LX/Ba8;->f:Landroid/widget/TextView;

    const v1, 0x7f08295c

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 1798541
    :pswitch_1
    iget-object v0, p0, LX/Ba8;->f:Landroid/widget/TextView;

    const v1, 0x7f08295d

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 1798542
    :pswitch_2
    iget-object v0, p0, LX/Ba8;->f:Landroid/widget/TextView;

    const v1, 0x7f08295e

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 1798543
    :pswitch_3
    iget-object v0, p0, LX/Ba8;->f:Landroid/widget/TextView;

    const v1, 0x7f08295f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 1798544
    :pswitch_4
    iget-object v0, p0, LX/Ba8;->f:Landroid/widget/TextView;

    const v1, 0x7f082960

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public final a(Landroid/view/LayoutInflater;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1798523
    const v0, 0x7f0300fc

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1798524
    const v0, 0x7f0d0587

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/Ba8;->d:Landroid/widget/TextView;

    .line 1798525
    const v0, 0x7f0d0589

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/ratingbar/BetterRatingBar;

    iput-object v0, p0, LX/Ba8;->e:Lcom/facebook/widget/ratingbar/BetterRatingBar;

    .line 1798526
    const v0, 0x7f0d0588

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/Ba8;->f:Landroid/widget/TextView;

    .line 1798527
    iget-object v0, p0, LX/Ba8;->e:Lcom/facebook/widget/ratingbar/BetterRatingBar;

    .line 1798528
    iget v2, v0, Lcom/facebook/widget/ratingbar/BetterRatingBar;->f:I

    move v0, v2

    .line 1798529
    invoke-direct {p0, v0}, LX/Ba8;->c(I)V

    .line 1798530
    iget-object v0, p0, LX/Ba8;->e:Lcom/facebook/widget/ratingbar/BetterRatingBar;

    invoke-virtual {v0, p0}, Lcom/facebook/widget/ratingbar/BetterRatingBar;->a(LX/8pS;)V

    .line 1798531
    iget-object v0, p0, LX/BZy;->a:Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;

    move-object v0, v0

    .line 1798532
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1798533
    const v2, 0x7f082952

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const v4, 0x7f080011

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1798534
    iget-object v2, p0, LX/Ba8;->d:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1798535
    return-object v1
.end method

.method public final a()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1798519
    iput-object v0, p0, LX/Ba8;->d:Landroid/widget/TextView;

    .line 1798520
    iput-object v0, p0, LX/Ba8;->e:Lcom/facebook/widget/ratingbar/BetterRatingBar;

    .line 1798521
    invoke-super {p0}, LX/BZy;->a()V

    .line 1798522
    return-void
.end method

.method public final a(I)V
    .locals 0

    .prologue
    .line 1798518
    return-void
.end method

.method public final a(II)V
    .locals 1

    .prologue
    .line 1798514
    invoke-direct {p0, p2}, LX/Ba8;->c(I)V

    .line 1798515
    iget-boolean v0, p0, LX/Ba8;->g:Z

    if-nez v0, :cond_0

    .line 1798516
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Ba8;->g:Z

    .line 1798517
    :cond_0
    return-void
.end method

.method public final a(Landroid/content/Context;LX/2EJ;)V
    .locals 3

    .prologue
    .line 1798510
    const v0, 0x7f082951

    invoke-virtual {p2, v0}, LX/2EJ;->setTitle(I)V

    .line 1798511
    const/4 v0, -0x1

    const v1, 0x7f08294f

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/Ba6;

    invoke-direct {v2, p0}, LX/Ba6;-><init>(LX/Ba8;)V

    invoke-virtual {p2, v0, v1, v2}, LX/2EJ;->a(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 1798512
    const/4 v0, -0x2

    const v1, 0x7f082950

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/Ba7;

    invoke-direct {v2, p0}, LX/Ba7;-><init>(LX/Ba8;)V

    invoke-virtual {p2, v0, v1, v2}, LX/2EJ;->a(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 1798513
    return-void
.end method
