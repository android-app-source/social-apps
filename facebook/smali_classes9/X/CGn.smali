.class public final LX/CGn;
.super LX/1Cv;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/groups/composer/groupspollcomposer/view/GroupsPollComposerFooterView;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/composer/groupspollcomposer/view/GroupsPollComposerFooterView;)V
    .locals 0

    .prologue
    .line 1865797
    iput-object p1, p0, LX/CGn;->a:Lcom/facebook/groups/composer/groupspollcomposer/view/GroupsPollComposerFooterView;

    invoke-direct {p0}, LX/1Cv;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 1865798
    new-instance v0, LX/CGo;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/CGo;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public final a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 2

    .prologue
    .line 1865799
    check-cast p3, LX/CGo;

    .line 1865800
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, LX/CGo;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object v0, p2

    .line 1865801
    check-cast v0, Ljava/lang/String;

    new-instance v1, LX/CGm;

    invoke-direct {v1, p0, p2}, LX/CGm;-><init>(LX/CGn;Ljava/lang/Object;)V

    .line 1865802
    iget-object p0, p3, LX/CGo;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1865803
    iget-object p0, p3, LX/CGo;->b:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {p0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1865804
    return-void
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 1865805
    iget-object v0, p0, LX/CGn;->a:Lcom/facebook/groups/composer/groupspollcomposer/view/GroupsPollComposerFooterView;

    iget-object v0, v0, Lcom/facebook/groups/composer/groupspollcomposer/view/GroupsPollComposerFooterView;->d:Lcom/facebook/groups/composer/groupspollcomposer/GroupsPollDataModel;

    invoke-virtual {v0}, Lcom/facebook/groups/composer/groupspollcomposer/GroupsPollDataModel;->a()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1865806
    iget-object v0, p0, LX/CGn;->a:Lcom/facebook/groups/composer/groupspollcomposer/view/GroupsPollComposerFooterView;

    iget-object v0, v0, Lcom/facebook/groups/composer/groupspollcomposer/view/GroupsPollComposerFooterView;->d:Lcom/facebook/groups/composer/groupspollcomposer/GroupsPollDataModel;

    invoke-virtual {v0, p1}, Lcom/facebook/groups/composer/groupspollcomposer/GroupsPollDataModel;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 1865807
    const-wide/16 v0, 0x0

    return-wide v0
.end method
