.class public LX/C1C;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/17z;
.implements LX/0jQ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/17z",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "LX/0jQ;"
    }
.end annotation


# instance fields
.field public final a:Z

.field public final b:Lcom/facebook/graphql/model/GraphQLNode;

.field public final c:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(ZLcom/facebook/graphql/model/GraphQLNode;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Lcom/facebook/graphql/model/GraphQLNode;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1842079
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1842080
    iput-boolean p1, p0, LX/C1C;->a:Z

    .line 1842081
    iput-object p2, p0, LX/C1C;->b:Lcom/facebook/graphql/model/GraphQLNode;

    .line 1842082
    iput-object p3, p0, LX/C1C;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1842083
    invoke-static {p3}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    iput-object v0, p0, LX/C1C;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1842084
    return-void
.end method


# virtual methods
.method public final c()Lcom/facebook/graphql/model/FeedUnit;
    .locals 1

    .prologue
    .line 1842075
    iget-object v0, p0, LX/C1C;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1842076
    iget-object p0, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, p0

    .line 1842077
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    return-object v0
.end method

.method public final g()Lcom/facebook/feed/rows/core/props/FeedProps;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1842078
    iget-object v0, p0, LX/C1C;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    return-object v0
.end method
