.class public final LX/Bcg;
.super LX/BcN;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/BcN",
        "<",
        "LX/Bck;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/Bch;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/Bck",
            "<TTEdge;TTUserInfo;>.GraphQ",
            "LConnectionChangeSetImpl;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/Bck;

.field private c:[Ljava/lang/String;

.field private d:I

.field public e:Ljava/util/BitSet;


# direct methods
.method public constructor <init>(LX/Bck;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1802474
    iput-object p1, p0, LX/Bcg;->b:LX/Bck;

    invoke-direct {p0}, LX/BcN;-><init>()V

    .line 1802475
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "connectionController"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/Bcg;->c:[Ljava/lang/String;

    .line 1802476
    iput v3, p0, LX/Bcg;->d:I

    .line 1802477
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/Bcg;->d:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/Bcg;->e:Ljava/util/BitSet;

    return-void
.end method


# virtual methods
.method public final a(I)LX/Bcg;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/Bck",
            "<TTEdge;TTUserInfo;>.Builder;"
        }
    .end annotation

    .prologue
    .line 1802459
    iget-object v0, p0, LX/Bcg;->a:LX/Bch;

    iput p1, v0, LX/Bch;->d:I

    .line 1802460
    return-object p0
.end method

.method public final a(LX/2kW;)LX/Bcg;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/controller/connectioncontroller/common/ConnectionController",
            "<TTEdge;TTUserInfo;>;)",
            "LX/Bck",
            "<TTEdge;TTUserInfo;>.Builder;"
        }
    .end annotation

    .prologue
    .line 1802471
    iget-object v0, p0, LX/Bcg;->a:LX/Bch;

    iput-object p1, v0, LX/Bch;->c:LX/2kW;

    .line 1802472
    iget-object v0, p0, LX/Bcg;->e:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1802473
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 1802467
    invoke-super {p0}, LX/BcN;->a()V

    .line 1802468
    const/4 v0, 0x0

    iput-object v0, p0, LX/Bcg;->a:LX/Bch;

    .line 1802469
    iget-object v0, p0, LX/Bcg;->b:LX/Bck;

    iget-object v0, v0, LX/Bck;->c:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 1802470
    return-void
.end method

.method public final b()LX/BcO;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/BcO",
            "<",
            "LX/Bck;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1802478
    iget-object v1, p0, LX/Bcg;->e:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/Bcg;->e:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/Bcg;->d:I

    if-ge v1, v2, :cond_2

    .line 1802479
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1802480
    :goto_0
    iget v2, p0, LX/Bcg;->d:I

    if-ge v0, v2, :cond_1

    .line 1802481
    iget-object v2, p0, LX/Bcg;->e:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1802482
    iget-object v2, p0, LX/Bcg;->c:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1802483
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1802484
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1802485
    :cond_2
    iget-object v0, p0, LX/Bcg;->a:LX/Bch;

    .line 1802486
    invoke-virtual {p0}, LX/Bcg;->a()V

    .line 1802487
    return-object v0
.end method

.method public final b(LX/BcQ;)LX/Bcg;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/BcQ;",
            ")",
            "LX/Bck",
            "<TTEdge;TTUserInfo;>.Builder;"
        }
    .end annotation

    .prologue
    .line 1802465
    iget-object v0, p0, LX/Bcg;->a:LX/Bch;

    iput-object p1, v0, LX/Bch;->l:LX/BcQ;

    .line 1802466
    return-object p0
.end method

.method public final b(Ljava/lang/String;)LX/Bcg;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/Bck",
            "<TTEdge;TTUserInfo;>.Builder;"
        }
    .end annotation

    .prologue
    .line 1802463
    invoke-super {p0, p1}, LX/BcN;->a(Ljava/lang/String;)V

    .line 1802464
    return-object p0
.end method

.method public final c(LX/BcQ;)LX/Bcg;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/BcQ",
            "<",
            "LX/BcM;",
            ">;)",
            "LX/Bck",
            "<TTEdge;TTUserInfo;>.Builder;"
        }
    .end annotation

    .prologue
    .line 1802461
    invoke-super {p0, p1}, LX/BcN;->a(LX/BcQ;)V

    .line 1802462
    return-object p0
.end method
