.class public LX/CJ5;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/CJ5;


# instance fields
.field public final a:LX/0tX;

.field public final b:LX/3N1;


# direct methods
.method public constructor <init>(LX/0tX;LX/3N1;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1875050
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1875051
    iput-object p1, p0, LX/CJ5;->a:LX/0tX;

    .line 1875052
    iput-object p2, p0, LX/CJ5;->b:LX/3N1;

    .line 1875053
    return-void
.end method

.method public static a(LX/0QB;)LX/CJ5;
    .locals 5

    .prologue
    .line 1875054
    sget-object v0, LX/CJ5;->c:LX/CJ5;

    if-nez v0, :cond_1

    .line 1875055
    const-class v1, LX/CJ5;

    monitor-enter v1

    .line 1875056
    :try_start_0
    sget-object v0, LX/CJ5;->c:LX/CJ5;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1875057
    if-eqz v2, :cond_0

    .line 1875058
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1875059
    new-instance p0, LX/CJ5;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {v0}, LX/3N1;->b(LX/0QB;)LX/3N1;

    move-result-object v4

    check-cast v4, LX/3N1;

    invoke-direct {p0, v3, v4}, LX/CJ5;-><init>(LX/0tX;LX/3N1;)V

    .line 1875060
    move-object v0, p0

    .line 1875061
    sput-object v0, LX/CJ5;->c:LX/CJ5;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1875062
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1875063
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1875064
    :cond_1
    sget-object v0, LX/CJ5;->c:LX/CJ5;

    return-object v0

    .line 1875065
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1875066
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
