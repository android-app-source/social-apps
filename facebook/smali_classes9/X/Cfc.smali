.class public final enum LX/Cfc;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Cfc;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Cfc;

.field public static final enum ACCEPT_ADMIN_INVITE_TAP:LX/Cfc;

.field public static final enum ADD_MORE_CARDS_TAP:LX/Cfc;

.field public static final enum ADD_PAGE_COVER_PHOTO:LX/Cfc;

.field public static final enum ADD_PAGE_CTA:LX/Cfc;

.field public static final enum ADD_PAGE_PROFILE_PIC:LX/Cfc;

.field public static final enum ADD_PHOTOS_AT_PLACE_TAP:LX/Cfc;

.field public static final enum ALBUM_TAP:LX/Cfc;

.field public static final enum APPLINK_TAP:LX/Cfc;

.field public static final enum BOOST_POST_TAP:LX/Cfc;

.field public static final enum BROWSE_QUERY_TAP:LX/Cfc;

.field public static final enum CALL_PHONE_TAP:LX/Cfc;

.field public static final enum CHECKIN_TAP:LX/Cfc;

.field public static final enum COLLAPSE_RICH_ATTACHMENT_TAP:LX/Cfc;

.field public static final enum CONFIRM_FRIEND_REQUEST_FROM_HSCROLL_TAP:LX/Cfc;

.field public static final enum CONFIRM_FRIEND_REQUEST_TAP:LX/Cfc;

.field public static final enum CONTACT_FUNDRAISER_SUPPORTERS:LX/Cfc;

.field public static final enum COPY_LINK_TAP:LX/Cfc;

.field public static final enum CREATION_TAB:LX/Cfc;

.field public static final enum CRISIS_MARK_AS_SAFE:LX/Cfc;

.field public static final enum CRISIS_NOT_IN_AREA:LX/Cfc;

.field public static final enum CRISIS_UNMARK:LX/Cfc;

.field public static final enum CRITIC_REVIEW_PAGE_TAP:LX/Cfc;

.field public static final enum DECLINE_ADMIN_INVITE_TAP:LX/Cfc;

.field public static final enum DELETE_FRIEND_REQUEST_FROM_HSCROLL_TAP:LX/Cfc;

.field public static final enum DELETE_FRIEND_REQUEST_TAP:LX/Cfc;

.field public static final enum DISABLE_UNIT_TAP:LX/Cfc;

.field public static final enum DISMISS_UNIT_TAP:LX/Cfc;

.field public static final enum EDIT_PAGE_INFO_TAP:LX/Cfc;

.field public static final enum EVENT_CARD_GOING_TAP:LX/Cfc;

.field public static final enum EVENT_CARD_INTERESTED_TAP:LX/Cfc;

.field public static final enum EVENT_CARD_MAYBE_TAP:LX/Cfc;

.field public static final enum EVENT_CARD_NOT_GOING_TAP:LX/Cfc;

.field public static final enum EVENT_CARD_TAP:LX/Cfc;

.field public static final enum EVENT_CARD_UNWATCHED_TAP:LX/Cfc;

.field public static final enum EVENT_CARD_WATCHED_TAP:LX/Cfc;

.field public static final enum EVENT_MESSAGE_FRIENDS:LX/Cfc;

.field public static final enum EXPAND_RICH_ATTACHMENT_TAP:LX/Cfc;

.field public static final enum EXTERNAL_PROFILE_LINK_SHARE:LX/Cfc;

.field public static final enum FACEWEB_URL_TAP:LX/Cfc;

.field public static final enum FUNDRAISER_COMPOSER:LX/Cfc;

.field public static final enum GO_LIVE_TAP:LX/Cfc;

.field public static final enum GROUP_JOIN_TAP:LX/Cfc;

.field public static final enum GROUP_LEAVE_TAP:LX/Cfc;

.field public static final enum HIDE_ADS_AFTER_PARTY_AYMT_TIP:LX/Cfc;

.field public static final enum INLINE_EXPANSION_TAP:LX/Cfc;

.field public static final enum INNER_SCROLL:LX/Cfc;

.field public static final enum INVITE_FRIENDS_TO_EVENT:LX/Cfc;

.field public static final enum INVITE_FUNDRAISER_GUEST:LX/Cfc;

.field public static final enum JOIN_EVENT_TAP:LX/Cfc;

.field public static final enum LEAVE_FUNDRAISER:LX/Cfc;

.field public static final enum LIKE_COMMENT_TAP:LX/Cfc;

.field public static final enum LOCAL_CONTENT_REVIEW_UNIT_TAP:LX/Cfc;

.field public static final enum MESSAGE_TAP:LX/Cfc;

.field public static final enum NEARBY_FRIENDS_NUX_TAP:LX/Cfc;

.field public static final enum NEARBY_FRIENDS_TAP:LX/Cfc;

.field public static final enum NEARBY_PLACES_TAP:LX/Cfc;

.field public static final enum NOTIFICATIONS_INLINE_EXPANSION_TAP:LX/Cfc;

.field public static final enum NOTIFICATIONS_TAP:LX/Cfc;

.field public static final enum NUX_CONTINUE_TAP:LX/Cfc;

.field public static final enum OPEN_ACORN_WEATHER_SETTINGS_TAP:LX/Cfc;

.field public static final enum OPEN_ALL_NOTIFICATIONS_TAP:LX/Cfc;

.field public static final enum OPEN_APPOINTMENT_TAP:LX/Cfc;

.field public static final enum OPEN_COMPOSER_TAP:LX/Cfc;

.field public static final enum OPEN_FRIEND_INVITER:LX/Cfc;

.field public static final enum OPEN_LOCAL_SEARCH_TAP:LX/Cfc;

.field public static final enum OPEN_MAP_TAP:LX/Cfc;

.field public static final enum OPEN_PAGE_ADD_PRODUCT_TAP:LX/Cfc;

.field public static final enum OPEN_PAGE_CHILD_LOCATIONS:LX/Cfc;

.field public static final enum OPEN_PAGE_COMMERCE_TAP:LX/Cfc;

.field public static final enum OPEN_PAGE_INFO_TAP:LX/Cfc;

.field public static final enum OPEN_PAGE_MAP_TAP:LX/Cfc;

.field public static final enum OPEN_PAGE_NAVIGATION_TAP:LX/Cfc;

.field public static final enum OPEN_PAGE_PROMOTION:LX/Cfc;

.field public static final enum OPEN_PHOTO_COMPOSER_TAP:LX/Cfc;

.field public static final enum OPEN_UPCOMING_BIRTHDAYS_TAP:LX/Cfc;

.field public static final enum OPEN_VERTICAL_ACTION_SHEET_TAP:LX/Cfc;

.field public static final enum OPEN_VIDEO_CHANNEL:LX/Cfc;

.field public static final enum PAGE_ADD_TAB_TAP:LX/Cfc;

.field public static final enum PAGE_BROADCASTER_GO_LIVE_TAP:LX/Cfc;

.field public static final enum PAGE_EVENT_CREATE_TAP:LX/Cfc;

.field public static final enum PAGE_MANAGER_TAP:LX/Cfc;

.field public static final enum PAGE_NEW_ACTIVITY_COUNT_TAP:LX/Cfc;

.field public static final enum PAGE_OPEN_CTA_SETUP_TAP:LX/Cfc;

.field public static final enum PAGE_OPEN_GET_QUOTE_SETUP_TAP:LX/Cfc;

.field public static final enum PAGE_OPEN_REQUEST_APPOINTMENT_SETUP_TAP:LX/Cfc;

.field public static final enum PHOTO_TAP:LX/Cfc;

.field public static final enum PLACE_TIPS_HIDE_TAP:LX/Cfc;

.field public static final enum PROFILE_TAP:LX/Cfc;

.field public static final enum PROMOTE_LOCAL_BUSINESS:LX/Cfc;

.field public static final enum PROMOTE_PAGE:LX/Cfc;

.field public static final enum PROMOTE_WEBSITE:LX/Cfc;

.field public static final enum QUESTION_ANSWER_TAP:LX/Cfc;

.field public static final enum QUESTION_SKIP_TAP:LX/Cfc;

.field public static final enum RATING_TAP:LX/Cfc;

.field public static final enum RELOAD_UNIT_TAP:LX/Cfc;

.field public static final enum REPLACE_UNIT_TAP:LX/Cfc;

.field public static final enum REPLY_COMMMENT_TAP:LX/Cfc;

.field public static final enum SAVE_PAGE_TAP:LX/Cfc;

.field public static final enum SEE_ALL_COMPONENTS:LX/Cfc;

.field public static final enum SEE_ALL_FRIEND_REQUESTS:LX/Cfc;

.field public static final enum SEE_ALL_RATINGS_TAP:LX/Cfc;

.field public static final enum SEE_ALL_SUGGESTED_EVENTS_TAP:LX/Cfc;

.field public static final enum SEE_JOB_DETAIL:LX/Cfc;

.field public static final enum SEE_MORE_APPS_TAP:LX/Cfc;

.field public static final enum SEE_MORE_EVENTS:LX/Cfc;

.field public static final enum SEE_MORE_TAP:LX/Cfc;

.field public static final enum SEE_OFFER_DETAIL:LX/Cfc;

.field public static final enum SEE_PAGE_ADS_AND_PROMOTIONS:LX/Cfc;

.field public static final enum SEE_PAGE_LIKES_TAP:LX/Cfc;

.field public static final enum SHARE_COMMENT_TAP:LX/Cfc;

.field public static final enum SHARE_EVENT_TAP:LX/Cfc;

.field public static final enum SHARE_FUNDRAISER:LX/Cfc;

.field public static final enum SHARE_PHOTO_TO_PAGE_TAP:LX/Cfc;

.field public static final enum STACK_CHILD_INTERACTION:LX/Cfc;

.field public static final enum STORY_TAP:LX/Cfc;

.field public static final enum SUBSCRIBE_PAGE_EVENTS_TAP:LX/Cfc;

.field public static final enum TEXT_ENTITY_TAP:LX/Cfc;

.field public static final enum TOPIC_TAP:LX/Cfc;

.field public static final enum UNHIGHLIGHT_RICH_NOTIFICATION_TAP:LX/Cfc;

.field public static final enum UNSAVE_PAGE_TAP:LX/Cfc;

.field public static final enum VIDEO_TAP:LX/Cfc;

.field public static final enum VIEW_APP_TAP:LX/Cfc;

.field public static final enum VIEW_COMMENT_ATTACHMENT_TAP:LX/Cfc;

.field public static final enum VIEW_COMMENT_TAP:LX/Cfc;

.field public static final enum VIEW_EVENTS_DASHBOARD_TAP:LX/Cfc;

.field public static final enum VIEW_EVENT_GUEST_LIST:LX/Cfc;

.field public static final enum VIEW_EVENT_POSTS:LX/Cfc;

.field public static final enum VIEW_FRIEND_CENTER_SUGGESTIONS_TAP:LX/Cfc;

.field public static final enum VIEW_FRIEND_REQUESTS_TAP:LX/Cfc;

.field public static final enum VIEW_FRIEND_REQUEST_PROFILE_TAP:LX/Cfc;

.field public static final enum VIEW_FUNDRAISER:LX/Cfc;

.field public static final enum VIEW_FUNDRAISER_SUPPORTERS:LX/Cfc;

.field public static final enum VIEW_GROUP_EVENTS_TAP:LX/Cfc;

.field public static final enum VIEW_GROUP_PHOTOS_TAP:LX/Cfc;

.field public static final enum VIEW_GROUP_TAP:LX/Cfc;

.field public static final enum VIEW_PAGE_CHECKINS:LX/Cfc;

.field public static final enum VIEW_PAGE_INSIGHTS:LX/Cfc;

.field public static final enum VIEW_PAGE_LIKERS:LX/Cfc;

.field public static final enum VIEW_PAGE_MENTIONS:LX/Cfc;

.field public static final enum VIEW_PAGE_PROFILE:LX/Cfc;

.field public static final enum VIEW_PAGE_SERVICE_TAP:LX/Cfc;

.field public static final enum VIEW_PAGE_SETTINGS:LX/Cfc;

.field public static final enum VIEW_PAGE_SHARES:LX/Cfc;

.field public static final enum VIEW_PHOTO_MENU_TAP:LX/Cfc;

.field public static final enum VIEW_POST_INSIGHTS:LX/Cfc;

.field public static final enum VIEW_PROFILE_TAP:LX/Cfc;

.field public static final enum VIEW_PYML_TAP:LX/Cfc;

.field public static final enum VIEW_STORY_INSIGHTS_TAP:LX/Cfc;

.field public static final enum VIEW_STORY_TAP:LX/Cfc;

.field public static final enum VIEW_STRUCTURE_MENU_TAP:LX/Cfc;

.field public static final enum WHITELIST_UNIT_TAP:LX/Cfc;

.field public static final enum WRITE_POST_TO_PAGE_TAP:LX/Cfc;

.field public static final enum WRITE_REVIEW_PUBLISH:LX/Cfc;

.field public static final enum WRITE_REVIEW_TAP:LX/Cfc;

.field public static final enum XOUT_TAP:LX/Cfc;


# instance fields
.field public final name:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1926498
    new-instance v0, LX/Cfc;

    const-string v1, "ADD_PAGE_COVER_PHOTO"

    const-string v2, "add_page_cover_photo"

    invoke-direct {v0, v1, v4, v2}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->ADD_PAGE_COVER_PHOTO:LX/Cfc;

    .line 1926499
    new-instance v0, LX/Cfc;

    const-string v1, "ADD_PAGE_CTA"

    const-string v2, "add_page_cta"

    invoke-direct {v0, v1, v5, v2}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->ADD_PAGE_CTA:LX/Cfc;

    .line 1926500
    new-instance v0, LX/Cfc;

    const-string v1, "ADD_PAGE_PROFILE_PIC"

    const-string v2, "add_page_profile_pic"

    invoke-direct {v0, v1, v6, v2}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->ADD_PAGE_PROFILE_PIC:LX/Cfc;

    .line 1926501
    new-instance v0, LX/Cfc;

    const-string v1, "ACCEPT_ADMIN_INVITE_TAP"

    const-string v2, "accept_admin_invite_tap"

    invoke-direct {v0, v1, v7, v2}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->ACCEPT_ADMIN_INVITE_TAP:LX/Cfc;

    .line 1926502
    new-instance v0, LX/Cfc;

    const-string v1, "ADD_MORE_CARDS_TAP"

    const-string v2, "add_more_cards_tap"

    invoke-direct {v0, v1, v8, v2}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->ADD_MORE_CARDS_TAP:LX/Cfc;

    .line 1926503
    new-instance v0, LX/Cfc;

    const-string v1, "ADD_PHOTOS_AT_PLACE_TAP"

    const/4 v2, 0x5

    const-string v3, "add_photos_at_place_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->ADD_PHOTOS_AT_PLACE_TAP:LX/Cfc;

    .line 1926504
    new-instance v0, LX/Cfc;

    const-string v1, "ALBUM_TAP"

    const/4 v2, 0x6

    const-string v3, "album_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->ALBUM_TAP:LX/Cfc;

    .line 1926505
    new-instance v0, LX/Cfc;

    const-string v1, "APPLINK_TAP"

    const/4 v2, 0x7

    const-string v3, "applink_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->APPLINK_TAP:LX/Cfc;

    .line 1926506
    new-instance v0, LX/Cfc;

    const-string v1, "BOOST_POST_TAP"

    const/16 v2, 0x8

    const-string v3, "boost_post_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->BOOST_POST_TAP:LX/Cfc;

    .line 1926507
    new-instance v0, LX/Cfc;

    const-string v1, "BROWSE_QUERY_TAP"

    const/16 v2, 0x9

    const-string v3, "browse_query_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->BROWSE_QUERY_TAP:LX/Cfc;

    .line 1926508
    new-instance v0, LX/Cfc;

    const-string v1, "CALL_PHONE_TAP"

    const/16 v2, 0xa

    const-string v3, "call_phone_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->CALL_PHONE_TAP:LX/Cfc;

    .line 1926509
    new-instance v0, LX/Cfc;

    const-string v1, "CHECKIN_TAP"

    const/16 v2, 0xb

    const-string v3, "checkin_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->CHECKIN_TAP:LX/Cfc;

    .line 1926510
    new-instance v0, LX/Cfc;

    const-string v1, "COLLAPSE_RICH_ATTACHMENT_TAP"

    const/16 v2, 0xc

    const-string v3, "collapse_rich_attachment_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->COLLAPSE_RICH_ATTACHMENT_TAP:LX/Cfc;

    .line 1926511
    new-instance v0, LX/Cfc;

    const-string v1, "CONFIRM_FRIEND_REQUEST_FROM_HSCROLL_TAP"

    const/16 v2, 0xd

    const-string v3, "confirm_friend_request_from_hscroll_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->CONFIRM_FRIEND_REQUEST_FROM_HSCROLL_TAP:LX/Cfc;

    .line 1926512
    new-instance v0, LX/Cfc;

    const-string v1, "CONFIRM_FRIEND_REQUEST_TAP"

    const/16 v2, 0xe

    const-string v3, "confirm_friend_request_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->CONFIRM_FRIEND_REQUEST_TAP:LX/Cfc;

    .line 1926513
    new-instance v0, LX/Cfc;

    const-string v1, "CONTACT_FUNDRAISER_SUPPORTERS"

    const/16 v2, 0xf

    const-string v3, "contact_fundraiser_supporters"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->CONTACT_FUNDRAISER_SUPPORTERS:LX/Cfc;

    .line 1926514
    new-instance v0, LX/Cfc;

    const-string v1, "COPY_LINK_TAP"

    const/16 v2, 0x10

    const-string v3, "copy_link_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->COPY_LINK_TAP:LX/Cfc;

    .line 1926515
    new-instance v0, LX/Cfc;

    const-string v1, "CREATION_TAB"

    const/16 v2, 0x11

    const-string v3, "creation_tab"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->CREATION_TAB:LX/Cfc;

    .line 1926516
    new-instance v0, LX/Cfc;

    const-string v1, "CRISIS_MARK_AS_SAFE"

    const/16 v2, 0x12

    const-string v3, "crisis_mark_as_safe"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->CRISIS_MARK_AS_SAFE:LX/Cfc;

    .line 1926517
    new-instance v0, LX/Cfc;

    const-string v1, "CRISIS_NOT_IN_AREA"

    const/16 v2, 0x13

    const-string v3, "crisis_not_in_area"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->CRISIS_NOT_IN_AREA:LX/Cfc;

    .line 1926518
    new-instance v0, LX/Cfc;

    const-string v1, "CRISIS_UNMARK"

    const/16 v2, 0x14

    const-string v3, "crisis_unmark"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->CRISIS_UNMARK:LX/Cfc;

    .line 1926519
    new-instance v0, LX/Cfc;

    const-string v1, "CRITIC_REVIEW_PAGE_TAP"

    const/16 v2, 0x15

    const-string v3, "critic_review_page_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->CRITIC_REVIEW_PAGE_TAP:LX/Cfc;

    .line 1926520
    new-instance v0, LX/Cfc;

    const-string v1, "DECLINE_ADMIN_INVITE_TAP"

    const/16 v2, 0x16

    const-string v3, "decline_admin_invite_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->DECLINE_ADMIN_INVITE_TAP:LX/Cfc;

    .line 1926521
    new-instance v0, LX/Cfc;

    const-string v1, "DELETE_FRIEND_REQUEST_FROM_HSCROLL_TAP"

    const/16 v2, 0x17

    const-string v3, "delete_friend_request_from_hscroll_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->DELETE_FRIEND_REQUEST_FROM_HSCROLL_TAP:LX/Cfc;

    .line 1926522
    new-instance v0, LX/Cfc;

    const-string v1, "DELETE_FRIEND_REQUEST_TAP"

    const/16 v2, 0x18

    const-string v3, "delete_friend_request_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->DELETE_FRIEND_REQUEST_TAP:LX/Cfc;

    .line 1926523
    new-instance v0, LX/Cfc;

    const-string v1, "DISABLE_UNIT_TAP"

    const/16 v2, 0x19

    const-string v3, "disable_unit_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->DISABLE_UNIT_TAP:LX/Cfc;

    .line 1926524
    new-instance v0, LX/Cfc;

    const-string v1, "DISMISS_UNIT_TAP"

    const/16 v2, 0x1a

    const-string v3, "dismiss_unit_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->DISMISS_UNIT_TAP:LX/Cfc;

    .line 1926525
    new-instance v0, LX/Cfc;

    const-string v1, "EDIT_PAGE_INFO_TAP"

    const/16 v2, 0x1b

    const-string v3, "edit_page_info_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->EDIT_PAGE_INFO_TAP:LX/Cfc;

    .line 1926526
    new-instance v0, LX/Cfc;

    const-string v1, "EVENT_CARD_GOING_TAP"

    const/16 v2, 0x1c

    const-string v3, "event_card_going_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->EVENT_CARD_GOING_TAP:LX/Cfc;

    .line 1926527
    new-instance v0, LX/Cfc;

    const-string v1, "EVENT_CARD_INTERESTED_TAP"

    const/16 v2, 0x1d

    const-string v3, "event_card_interested_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->EVENT_CARD_INTERESTED_TAP:LX/Cfc;

    .line 1926528
    new-instance v0, LX/Cfc;

    const-string v1, "EVENT_CARD_MAYBE_TAP"

    const/16 v2, 0x1e

    const-string v3, "event_card_maybe_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->EVENT_CARD_MAYBE_TAP:LX/Cfc;

    .line 1926529
    new-instance v0, LX/Cfc;

    const-string v1, "EVENT_CARD_NOT_GOING_TAP"

    const/16 v2, 0x1f

    const-string v3, "event_card_not_going_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->EVENT_CARD_NOT_GOING_TAP:LX/Cfc;

    .line 1926530
    new-instance v0, LX/Cfc;

    const-string v1, "EVENT_CARD_TAP"

    const/16 v2, 0x20

    const-string v3, "event_card_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->EVENT_CARD_TAP:LX/Cfc;

    .line 1926531
    new-instance v0, LX/Cfc;

    const-string v1, "EVENT_CARD_UNWATCHED_TAP"

    const/16 v2, 0x21

    const-string v3, "event_card_unwatched_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->EVENT_CARD_UNWATCHED_TAP:LX/Cfc;

    .line 1926532
    new-instance v0, LX/Cfc;

    const-string v1, "EVENT_CARD_WATCHED_TAP"

    const/16 v2, 0x22

    const-string v3, "event_card_watched_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->EVENT_CARD_WATCHED_TAP:LX/Cfc;

    .line 1926533
    new-instance v0, LX/Cfc;

    const-string v1, "EVENT_MESSAGE_FRIENDS"

    const/16 v2, 0x23

    const-string v3, "event_message_friends"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->EVENT_MESSAGE_FRIENDS:LX/Cfc;

    .line 1926534
    new-instance v0, LX/Cfc;

    const-string v1, "EXPAND_RICH_ATTACHMENT_TAP"

    const/16 v2, 0x24

    const-string v3, "expand_rich_attachment_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->EXPAND_RICH_ATTACHMENT_TAP:LX/Cfc;

    .line 1926535
    new-instance v0, LX/Cfc;

    const-string v1, "EXTERNAL_PROFILE_LINK_SHARE"

    const/16 v2, 0x25

    const-string v3, "external_profile_link_share"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->EXTERNAL_PROFILE_LINK_SHARE:LX/Cfc;

    .line 1926536
    new-instance v0, LX/Cfc;

    const-string v1, "FACEWEB_URL_TAP"

    const/16 v2, 0x26

    const-string v3, "faceweb_url_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->FACEWEB_URL_TAP:LX/Cfc;

    .line 1926537
    new-instance v0, LX/Cfc;

    const-string v1, "FUNDRAISER_COMPOSER"

    const/16 v2, 0x27

    const-string v3, "fundraiser_composer"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->FUNDRAISER_COMPOSER:LX/Cfc;

    .line 1926538
    new-instance v0, LX/Cfc;

    const-string v1, "GO_LIVE_TAP"

    const/16 v2, 0x28

    const-string v3, "go_live_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->GO_LIVE_TAP:LX/Cfc;

    .line 1926539
    new-instance v0, LX/Cfc;

    const-string v1, "GROUP_JOIN_TAP"

    const/16 v2, 0x29

    const-string v3, "group_join_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->GROUP_JOIN_TAP:LX/Cfc;

    .line 1926540
    new-instance v0, LX/Cfc;

    const-string v1, "GROUP_LEAVE_TAP"

    const/16 v2, 0x2a

    const-string v3, "group_leave_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->GROUP_LEAVE_TAP:LX/Cfc;

    .line 1926541
    new-instance v0, LX/Cfc;

    const-string v1, "HIDE_ADS_AFTER_PARTY_AYMT_TIP"

    const/16 v2, 0x2b

    const-string v3, "hide_ads_after_party_aymt_tip"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->HIDE_ADS_AFTER_PARTY_AYMT_TIP:LX/Cfc;

    .line 1926542
    new-instance v0, LX/Cfc;

    const-string v1, "INLINE_EXPANSION_TAP"

    const/16 v2, 0x2c

    const-string v3, "inline_expansion_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->INLINE_EXPANSION_TAP:LX/Cfc;

    .line 1926543
    new-instance v0, LX/Cfc;

    const-string v1, "INNER_SCROLL"

    const/16 v2, 0x2d

    const-string v3, "inner_scroll"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->INNER_SCROLL:LX/Cfc;

    .line 1926544
    new-instance v0, LX/Cfc;

    const-string v1, "INVITE_FRIENDS_TO_EVENT"

    const/16 v2, 0x2e

    const-string v3, "invite_friends_to_event"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->INVITE_FRIENDS_TO_EVENT:LX/Cfc;

    .line 1926545
    new-instance v0, LX/Cfc;

    const-string v1, "INVITE_FUNDRAISER_GUEST"

    const/16 v2, 0x2f

    const-string v3, "invite_fundraiser_guest"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->INVITE_FUNDRAISER_GUEST:LX/Cfc;

    .line 1926546
    new-instance v0, LX/Cfc;

    const-string v1, "JOIN_EVENT_TAP"

    const/16 v2, 0x30

    const-string v3, "join_event_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->JOIN_EVENT_TAP:LX/Cfc;

    .line 1926547
    new-instance v0, LX/Cfc;

    const-string v1, "LEAVE_FUNDRAISER"

    const/16 v2, 0x31

    const-string v3, "leave_fundraiser"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->LEAVE_FUNDRAISER:LX/Cfc;

    .line 1926548
    new-instance v0, LX/Cfc;

    const-string v1, "LIKE_COMMENT_TAP"

    const/16 v2, 0x32

    const-string v3, "like_comment_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->LIKE_COMMENT_TAP:LX/Cfc;

    .line 1926549
    new-instance v0, LX/Cfc;

    const-string v1, "LOCAL_CONTENT_REVIEW_UNIT_TAP"

    const/16 v2, 0x33

    const-string v3, "local_content_review_unit_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->LOCAL_CONTENT_REVIEW_UNIT_TAP:LX/Cfc;

    .line 1926550
    new-instance v0, LX/Cfc;

    const-string v1, "MESSAGE_TAP"

    const/16 v2, 0x34

    const-string v3, "message_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->MESSAGE_TAP:LX/Cfc;

    .line 1926551
    new-instance v0, LX/Cfc;

    const-string v1, "NEARBY_FRIENDS_NUX_TAP"

    const/16 v2, 0x35

    const-string v3, "nearby_friends_nux_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->NEARBY_FRIENDS_NUX_TAP:LX/Cfc;

    .line 1926552
    new-instance v0, LX/Cfc;

    const-string v1, "NEARBY_FRIENDS_TAP"

    const/16 v2, 0x36

    const-string v3, "nearby_friends_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->NEARBY_FRIENDS_TAP:LX/Cfc;

    .line 1926553
    new-instance v0, LX/Cfc;

    const-string v1, "NEARBY_PLACES_TAP"

    const/16 v2, 0x37

    const-string v3, "nearby_places_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->NEARBY_PLACES_TAP:LX/Cfc;

    .line 1926554
    new-instance v0, LX/Cfc;

    const-string v1, "NOTIFICATIONS_INLINE_EXPANSION_TAP"

    const/16 v2, 0x38

    const-string v3, "notifications_inline_expansion_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->NOTIFICATIONS_INLINE_EXPANSION_TAP:LX/Cfc;

    .line 1926555
    new-instance v0, LX/Cfc;

    const-string v1, "NOTIFICATIONS_TAP"

    const/16 v2, 0x39

    const-string v3, "notifications_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->NOTIFICATIONS_TAP:LX/Cfc;

    .line 1926556
    new-instance v0, LX/Cfc;

    const-string v1, "NUX_CONTINUE_TAP"

    const/16 v2, 0x3a

    const-string v3, "nux_continue_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->NUX_CONTINUE_TAP:LX/Cfc;

    .line 1926557
    new-instance v0, LX/Cfc;

    const-string v1, "OPEN_ACORN_WEATHER_SETTINGS_TAP"

    const/16 v2, 0x3b

    const-string v3, "open_acorn_weather_settings_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->OPEN_ACORN_WEATHER_SETTINGS_TAP:LX/Cfc;

    .line 1926558
    new-instance v0, LX/Cfc;

    const-string v1, "OPEN_ALL_NOTIFICATIONS_TAP"

    const/16 v2, 0x3c

    const-string v3, "open_all_notifications_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->OPEN_ALL_NOTIFICATIONS_TAP:LX/Cfc;

    .line 1926559
    new-instance v0, LX/Cfc;

    const-string v1, "OPEN_APPOINTMENT_TAP"

    const/16 v2, 0x3d

    const-string v3, "open_appointment_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->OPEN_APPOINTMENT_TAP:LX/Cfc;

    .line 1926560
    new-instance v0, LX/Cfc;

    const-string v1, "OPEN_COMPOSER_TAP"

    const/16 v2, 0x3e

    const-string v3, "open_composer_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->OPEN_COMPOSER_TAP:LX/Cfc;

    .line 1926561
    new-instance v0, LX/Cfc;

    const-string v1, "OPEN_FRIEND_INVITER"

    const/16 v2, 0x3f

    const-string v3, "open_friend_inviter"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->OPEN_FRIEND_INVITER:LX/Cfc;

    .line 1926562
    new-instance v0, LX/Cfc;

    const-string v1, "OPEN_LOCAL_SEARCH_TAP"

    const/16 v2, 0x40

    const-string v3, "open_local_search_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->OPEN_LOCAL_SEARCH_TAP:LX/Cfc;

    .line 1926563
    new-instance v0, LX/Cfc;

    const-string v1, "OPEN_MAP_TAP"

    const/16 v2, 0x41

    const-string v3, "open_map_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->OPEN_MAP_TAP:LX/Cfc;

    .line 1926564
    new-instance v0, LX/Cfc;

    const-string v1, "OPEN_PAGE_ADD_PRODUCT_TAP"

    const/16 v2, 0x42

    const-string v3, "open_page_add_product_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->OPEN_PAGE_ADD_PRODUCT_TAP:LX/Cfc;

    .line 1926565
    new-instance v0, LX/Cfc;

    const-string v1, "OPEN_PAGE_CHILD_LOCATIONS"

    const/16 v2, 0x43

    const-string v3, "open_page_child_locations"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->OPEN_PAGE_CHILD_LOCATIONS:LX/Cfc;

    .line 1926566
    new-instance v0, LX/Cfc;

    const-string v1, "OPEN_PAGE_COMMERCE_TAP"

    const/16 v2, 0x44

    const-string v3, "open_page_commerce_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->OPEN_PAGE_COMMERCE_TAP:LX/Cfc;

    .line 1926567
    new-instance v0, LX/Cfc;

    const-string v1, "OPEN_PAGE_INFO_TAP"

    const/16 v2, 0x45

    const-string v3, "open_page_info_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->OPEN_PAGE_INFO_TAP:LX/Cfc;

    .line 1926568
    new-instance v0, LX/Cfc;

    const-string v1, "OPEN_PAGE_MAP_TAP"

    const/16 v2, 0x46

    const-string v3, "open_page_map_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->OPEN_PAGE_MAP_TAP:LX/Cfc;

    .line 1926569
    new-instance v0, LX/Cfc;

    const-string v1, "OPEN_PAGE_NAVIGATION_TAP"

    const/16 v2, 0x47

    const-string v3, "open_page_navigation_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->OPEN_PAGE_NAVIGATION_TAP:LX/Cfc;

    .line 1926570
    new-instance v0, LX/Cfc;

    const-string v1, "OPEN_PAGE_PROMOTION"

    const/16 v2, 0x48

    const-string v3, "open_page_promotion"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->OPEN_PAGE_PROMOTION:LX/Cfc;

    .line 1926571
    new-instance v0, LX/Cfc;

    const-string v1, "OPEN_PHOTO_COMPOSER_TAP"

    const/16 v2, 0x49

    const-string v3, "open_photo_composer_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->OPEN_PHOTO_COMPOSER_TAP:LX/Cfc;

    .line 1926572
    new-instance v0, LX/Cfc;

    const-string v1, "OPEN_UPCOMING_BIRTHDAYS_TAP"

    const/16 v2, 0x4a

    const-string v3, "open_upcoming_birthdays_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->OPEN_UPCOMING_BIRTHDAYS_TAP:LX/Cfc;

    .line 1926573
    new-instance v0, LX/Cfc;

    const-string v1, "OPEN_VERTICAL_ACTION_SHEET_TAP"

    const/16 v2, 0x4b

    const-string v3, "open_vertical_action_sheet_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->OPEN_VERTICAL_ACTION_SHEET_TAP:LX/Cfc;

    .line 1926574
    new-instance v0, LX/Cfc;

    const-string v1, "OPEN_VIDEO_CHANNEL"

    const/16 v2, 0x4c

    const-string v3, "open_video_channel"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->OPEN_VIDEO_CHANNEL:LX/Cfc;

    .line 1926575
    new-instance v0, LX/Cfc;

    const-string v1, "PAGE_ADD_TAB_TAP"

    const/16 v2, 0x4d

    const-string v3, "page_add_tab_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->PAGE_ADD_TAB_TAP:LX/Cfc;

    .line 1926576
    new-instance v0, LX/Cfc;

    const-string v1, "PAGE_BROADCASTER_GO_LIVE_TAP"

    const/16 v2, 0x4e

    const-string v3, "page_broadcaster_go_live_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->PAGE_BROADCASTER_GO_LIVE_TAP:LX/Cfc;

    .line 1926577
    new-instance v0, LX/Cfc;

    const-string v1, "PAGE_EVENT_CREATE_TAP"

    const/16 v2, 0x4f

    const-string v3, "page_event_create_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->PAGE_EVENT_CREATE_TAP:LX/Cfc;

    .line 1926578
    new-instance v0, LX/Cfc;

    const-string v1, "PAGE_MANAGER_TAP"

    const/16 v2, 0x50

    const-string v3, "page_manager_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->PAGE_MANAGER_TAP:LX/Cfc;

    .line 1926579
    new-instance v0, LX/Cfc;

    const-string v1, "PAGE_NEW_ACTIVITY_COUNT_TAP"

    const/16 v2, 0x51

    const-string v3, "page_new_activity_count_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->PAGE_NEW_ACTIVITY_COUNT_TAP:LX/Cfc;

    .line 1926580
    new-instance v0, LX/Cfc;

    const-string v1, "PAGE_OPEN_CTA_SETUP_TAP"

    const/16 v2, 0x52

    const-string v3, "page_open_cta_setup_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->PAGE_OPEN_CTA_SETUP_TAP:LX/Cfc;

    .line 1926581
    new-instance v0, LX/Cfc;

    const-string v1, "PAGE_OPEN_GET_QUOTE_SETUP_TAP"

    const/16 v2, 0x53

    const-string v3, "page_open_get_quote_setup_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->PAGE_OPEN_GET_QUOTE_SETUP_TAP:LX/Cfc;

    .line 1926582
    new-instance v0, LX/Cfc;

    const-string v1, "PAGE_OPEN_REQUEST_APPOINTMENT_SETUP_TAP"

    const/16 v2, 0x54

    const-string v3, "page_open_request_appointment_setup_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->PAGE_OPEN_REQUEST_APPOINTMENT_SETUP_TAP:LX/Cfc;

    .line 1926583
    new-instance v0, LX/Cfc;

    const-string v1, "PHOTO_TAP"

    const/16 v2, 0x55

    const-string v3, "photo_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->PHOTO_TAP:LX/Cfc;

    .line 1926584
    new-instance v0, LX/Cfc;

    const-string v1, "PLACE_TIPS_HIDE_TAP"

    const/16 v2, 0x56

    const-string v3, "place_tips_hide_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->PLACE_TIPS_HIDE_TAP:LX/Cfc;

    .line 1926585
    new-instance v0, LX/Cfc;

    const-string v1, "PROFILE_TAP"

    const/16 v2, 0x57

    const-string v3, "profile_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->PROFILE_TAP:LX/Cfc;

    .line 1926586
    new-instance v0, LX/Cfc;

    const-string v1, "PROMOTE_LOCAL_BUSINESS"

    const/16 v2, 0x58

    const-string v3, "promote_local_business"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->PROMOTE_LOCAL_BUSINESS:LX/Cfc;

    .line 1926587
    new-instance v0, LX/Cfc;

    const-string v1, "PROMOTE_PAGE"

    const/16 v2, 0x59

    const-string v3, "promote_page"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->PROMOTE_PAGE:LX/Cfc;

    .line 1926588
    new-instance v0, LX/Cfc;

    const-string v1, "PROMOTE_WEBSITE"

    const/16 v2, 0x5a

    const-string v3, "promote_website"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->PROMOTE_WEBSITE:LX/Cfc;

    .line 1926589
    new-instance v0, LX/Cfc;

    const-string v1, "QUESTION_ANSWER_TAP"

    const/16 v2, 0x5b

    const-string v3, "question_answer_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->QUESTION_ANSWER_TAP:LX/Cfc;

    .line 1926590
    new-instance v0, LX/Cfc;

    const-string v1, "QUESTION_SKIP_TAP"

    const/16 v2, 0x5c

    const-string v3, "question_skip_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->QUESTION_SKIP_TAP:LX/Cfc;

    .line 1926591
    new-instance v0, LX/Cfc;

    const-string v1, "RATING_TAP"

    const/16 v2, 0x5d

    const-string v3, "rating_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->RATING_TAP:LX/Cfc;

    .line 1926592
    new-instance v0, LX/Cfc;

    const-string v1, "RELOAD_UNIT_TAP"

    const/16 v2, 0x5e

    const-string v3, "reload_unit_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->RELOAD_UNIT_TAP:LX/Cfc;

    .line 1926593
    new-instance v0, LX/Cfc;

    const-string v1, "REPLACE_UNIT_TAP"

    const/16 v2, 0x5f

    const-string v3, "replace_unit_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->REPLACE_UNIT_TAP:LX/Cfc;

    .line 1926594
    new-instance v0, LX/Cfc;

    const-string v1, "REPLY_COMMMENT_TAP"

    const/16 v2, 0x60

    const-string v3, "reply_comment_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->REPLY_COMMMENT_TAP:LX/Cfc;

    .line 1926595
    new-instance v0, LX/Cfc;

    const-string v1, "SAVE_PAGE_TAP"

    const/16 v2, 0x61

    const-string v3, "save_page_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->SAVE_PAGE_TAP:LX/Cfc;

    .line 1926596
    new-instance v0, LX/Cfc;

    const-string v1, "SEE_ALL_COMPONENTS"

    const/16 v2, 0x62

    const-string v3, "see_all_components"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->SEE_ALL_COMPONENTS:LX/Cfc;

    .line 1926597
    new-instance v0, LX/Cfc;

    const-string v1, "SEE_ALL_FRIEND_REQUESTS"

    const/16 v2, 0x63

    const-string v3, "see_all_friend_requests"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->SEE_ALL_FRIEND_REQUESTS:LX/Cfc;

    .line 1926598
    new-instance v0, LX/Cfc;

    const-string v1, "SEE_ALL_RATINGS_TAP"

    const/16 v2, 0x64

    const-string v3, "see_all_ratings_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->SEE_ALL_RATINGS_TAP:LX/Cfc;

    .line 1926599
    new-instance v0, LX/Cfc;

    const-string v1, "SEE_ALL_SUGGESTED_EVENTS_TAP"

    const/16 v2, 0x65

    const-string v3, "see_all_suggested_events_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->SEE_ALL_SUGGESTED_EVENTS_TAP:LX/Cfc;

    .line 1926600
    new-instance v0, LX/Cfc;

    const-string v1, "SEE_MORE_APPS_TAP"

    const/16 v2, 0x66

    const-string v3, "see_more_apps_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->SEE_MORE_APPS_TAP:LX/Cfc;

    .line 1926601
    new-instance v0, LX/Cfc;

    const-string v1, "SEE_MORE_EVENTS"

    const/16 v2, 0x67

    const-string v3, "see_more_events"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->SEE_MORE_EVENTS:LX/Cfc;

    .line 1926602
    new-instance v0, LX/Cfc;

    const-string v1, "SEE_MORE_TAP"

    const/16 v2, 0x68

    const-string v3, "see_more_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->SEE_MORE_TAP:LX/Cfc;

    .line 1926603
    new-instance v0, LX/Cfc;

    const-string v1, "SEE_JOB_DETAIL"

    const/16 v2, 0x69

    const-string v3, "see_job_detail"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->SEE_JOB_DETAIL:LX/Cfc;

    .line 1926604
    new-instance v0, LX/Cfc;

    const-string v1, "SEE_OFFER_DETAIL"

    const/16 v2, 0x6a

    const-string v3, "see_offer_detail"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->SEE_OFFER_DETAIL:LX/Cfc;

    .line 1926605
    new-instance v0, LX/Cfc;

    const-string v1, "SEE_PAGE_ADS_AND_PROMOTIONS"

    const/16 v2, 0x6b

    const-string v3, "see_page_ads_and_promotions"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->SEE_PAGE_ADS_AND_PROMOTIONS:LX/Cfc;

    .line 1926606
    new-instance v0, LX/Cfc;

    const-string v1, "SEE_PAGE_LIKES_TAP"

    const/16 v2, 0x6c

    const-string v3, "see_page_likes_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->SEE_PAGE_LIKES_TAP:LX/Cfc;

    .line 1926607
    new-instance v0, LX/Cfc;

    const-string v1, "SHARE_COMMENT_TAP"

    const/16 v2, 0x6d

    const-string v3, "share_comment_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->SHARE_COMMENT_TAP:LX/Cfc;

    .line 1926608
    new-instance v0, LX/Cfc;

    const-string v1, "SHARE_EVENT_TAP"

    const/16 v2, 0x6e

    const-string v3, "share_event_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->SHARE_EVENT_TAP:LX/Cfc;

    .line 1926609
    new-instance v0, LX/Cfc;

    const-string v1, "SHARE_FUNDRAISER"

    const/16 v2, 0x6f

    const-string v3, "share_fundraiser"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->SHARE_FUNDRAISER:LX/Cfc;

    .line 1926610
    new-instance v0, LX/Cfc;

    const-string v1, "SHARE_PHOTO_TO_PAGE_TAP"

    const/16 v2, 0x70

    const-string v3, "share_photo_to_page_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->SHARE_PHOTO_TO_PAGE_TAP:LX/Cfc;

    .line 1926611
    new-instance v0, LX/Cfc;

    const-string v1, "STACK_CHILD_INTERACTION"

    const/16 v2, 0x71

    const-string v3, "stack_child_interaction"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->STACK_CHILD_INTERACTION:LX/Cfc;

    .line 1926612
    new-instance v0, LX/Cfc;

    const-string v1, "STORY_TAP"

    const/16 v2, 0x72

    const-string v3, "story_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->STORY_TAP:LX/Cfc;

    .line 1926613
    new-instance v0, LX/Cfc;

    const-string v1, "SUBSCRIBE_PAGE_EVENTS_TAP"

    const/16 v2, 0x73

    const-string v3, "subscribe_page_events_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->SUBSCRIBE_PAGE_EVENTS_TAP:LX/Cfc;

    .line 1926614
    new-instance v0, LX/Cfc;

    const-string v1, "TEXT_ENTITY_TAP"

    const/16 v2, 0x74

    const-string v3, "text_entity_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->TEXT_ENTITY_TAP:LX/Cfc;

    .line 1926615
    new-instance v0, LX/Cfc;

    const-string v1, "TOPIC_TAP"

    const/16 v2, 0x75

    const-string v3, "topic_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->TOPIC_TAP:LX/Cfc;

    .line 1926616
    new-instance v0, LX/Cfc;

    const-string v1, "UNHIGHLIGHT_RICH_NOTIFICATION_TAP"

    const/16 v2, 0x76

    const-string v3, "unhighlight_rich_notification_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->UNHIGHLIGHT_RICH_NOTIFICATION_TAP:LX/Cfc;

    .line 1926617
    new-instance v0, LX/Cfc;

    const-string v1, "UNSAVE_PAGE_TAP"

    const/16 v2, 0x77

    const-string v3, "unsave_page_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->UNSAVE_PAGE_TAP:LX/Cfc;

    .line 1926618
    new-instance v0, LX/Cfc;

    const-string v1, "VIDEO_TAP"

    const/16 v2, 0x78

    const-string v3, "video_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->VIDEO_TAP:LX/Cfc;

    .line 1926619
    new-instance v0, LX/Cfc;

    const-string v1, "VIEW_APP_TAP"

    const/16 v2, 0x79

    const-string v3, "view_app_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->VIEW_APP_TAP:LX/Cfc;

    .line 1926620
    new-instance v0, LX/Cfc;

    const-string v1, "VIEW_COMMENT_ATTACHMENT_TAP"

    const/16 v2, 0x7a

    const-string v3, "view_comment_attachment_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->VIEW_COMMENT_ATTACHMENT_TAP:LX/Cfc;

    .line 1926621
    new-instance v0, LX/Cfc;

    const-string v1, "VIEW_COMMENT_TAP"

    const/16 v2, 0x7b

    const-string v3, "view_comment_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->VIEW_COMMENT_TAP:LX/Cfc;

    .line 1926622
    new-instance v0, LX/Cfc;

    const-string v1, "VIEW_EVENT_GUEST_LIST"

    const/16 v2, 0x7c

    const-string v3, "view_event_guest_list"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->VIEW_EVENT_GUEST_LIST:LX/Cfc;

    .line 1926623
    new-instance v0, LX/Cfc;

    const-string v1, "VIEW_EVENT_POSTS"

    const/16 v2, 0x7d

    const-string v3, "view_event_posts"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->VIEW_EVENT_POSTS:LX/Cfc;

    .line 1926624
    new-instance v0, LX/Cfc;

    const-string v1, "VIEW_EVENTS_DASHBOARD_TAP"

    const/16 v2, 0x7e

    const-string v3, "view_events_dashboard_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->VIEW_EVENTS_DASHBOARD_TAP:LX/Cfc;

    .line 1926625
    new-instance v0, LX/Cfc;

    const-string v1, "VIEW_FRIEND_CENTER_SUGGESTIONS_TAP"

    const/16 v2, 0x7f

    const-string v3, "view_friend_center_suggestions_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->VIEW_FRIEND_CENTER_SUGGESTIONS_TAP:LX/Cfc;

    .line 1926626
    new-instance v0, LX/Cfc;

    const-string v1, "VIEW_FRIEND_REQUEST_PROFILE_TAP"

    const/16 v2, 0x80

    const-string v3, "view_friend_request_profile_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->VIEW_FRIEND_REQUEST_PROFILE_TAP:LX/Cfc;

    .line 1926627
    new-instance v0, LX/Cfc;

    const-string v1, "VIEW_FRIEND_REQUESTS_TAP"

    const/16 v2, 0x81

    const-string v3, "view_friend_requests_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->VIEW_FRIEND_REQUESTS_TAP:LX/Cfc;

    .line 1926628
    new-instance v0, LX/Cfc;

    const-string v1, "VIEW_FUNDRAISER"

    const/16 v2, 0x82

    const-string v3, "view_fundraiser"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->VIEW_FUNDRAISER:LX/Cfc;

    .line 1926629
    new-instance v0, LX/Cfc;

    const-string v1, "VIEW_FUNDRAISER_SUPPORTERS"

    const/16 v2, 0x83

    const-string v3, "view_fundraiser_supporters"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->VIEW_FUNDRAISER_SUPPORTERS:LX/Cfc;

    .line 1926630
    new-instance v0, LX/Cfc;

    const-string v1, "VIEW_GROUP_EVENTS_TAP"

    const/16 v2, 0x84

    const-string v3, "view_group_events_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->VIEW_GROUP_EVENTS_TAP:LX/Cfc;

    .line 1926631
    new-instance v0, LX/Cfc;

    const-string v1, "VIEW_GROUP_PHOTOS_TAP"

    const/16 v2, 0x85

    const-string v3, "view_group_photos_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->VIEW_GROUP_PHOTOS_TAP:LX/Cfc;

    .line 1926632
    new-instance v0, LX/Cfc;

    const-string v1, "VIEW_GROUP_TAP"

    const/16 v2, 0x86

    const-string v3, "view_group_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->VIEW_GROUP_TAP:LX/Cfc;

    .line 1926633
    new-instance v0, LX/Cfc;

    const-string v1, "VIEW_PAGE_CHECKINS"

    const/16 v2, 0x87

    const-string v3, "view_page_checkins"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->VIEW_PAGE_CHECKINS:LX/Cfc;

    .line 1926634
    new-instance v0, LX/Cfc;

    const-string v1, "VIEW_PAGE_INSIGHTS"

    const/16 v2, 0x88

    const-string v3, "view_page_insights"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->VIEW_PAGE_INSIGHTS:LX/Cfc;

    .line 1926635
    new-instance v0, LX/Cfc;

    const-string v1, "VIEW_PAGE_LIKERS"

    const/16 v2, 0x89

    const-string v3, "view_page_likers"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->VIEW_PAGE_LIKERS:LX/Cfc;

    .line 1926636
    new-instance v0, LX/Cfc;

    const-string v1, "VIEW_PAGE_MENTIONS"

    const/16 v2, 0x8a

    const-string v3, "view_page_mentions"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->VIEW_PAGE_MENTIONS:LX/Cfc;

    .line 1926637
    new-instance v0, LX/Cfc;

    const-string v1, "VIEW_PAGE_PROFILE"

    const/16 v2, 0x8b

    const-string v3, "view_page_profile"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->VIEW_PAGE_PROFILE:LX/Cfc;

    .line 1926638
    new-instance v0, LX/Cfc;

    const-string v1, "VIEW_PAGE_SERVICE_TAP"

    const/16 v2, 0x8c

    const-string v3, "view_page_service_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->VIEW_PAGE_SERVICE_TAP:LX/Cfc;

    .line 1926639
    new-instance v0, LX/Cfc;

    const-string v1, "VIEW_PAGE_SETTINGS"

    const/16 v2, 0x8d

    const-string v3, "view_page_settings"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->VIEW_PAGE_SETTINGS:LX/Cfc;

    .line 1926640
    new-instance v0, LX/Cfc;

    const-string v1, "VIEW_PAGE_SHARES"

    const/16 v2, 0x8e

    const-string v3, "view_page_shares"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->VIEW_PAGE_SHARES:LX/Cfc;

    .line 1926641
    new-instance v0, LX/Cfc;

    const-string v1, "VIEW_PHOTO_MENU_TAP"

    const/16 v2, 0x8f

    const-string v3, "view_photo_menu_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->VIEW_PHOTO_MENU_TAP:LX/Cfc;

    .line 1926642
    new-instance v0, LX/Cfc;

    const-string v1, "VIEW_POST_INSIGHTS"

    const/16 v2, 0x90

    const-string v3, "view_post_insights"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->VIEW_POST_INSIGHTS:LX/Cfc;

    .line 1926643
    new-instance v0, LX/Cfc;

    const-string v1, "VIEW_PROFILE_TAP"

    const/16 v2, 0x91

    const-string v3, "view_profile_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->VIEW_PROFILE_TAP:LX/Cfc;

    .line 1926644
    new-instance v0, LX/Cfc;

    const-string v1, "VIEW_PYML_TAP"

    const/16 v2, 0x92

    const-string v3, "view_pyml_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->VIEW_PYML_TAP:LX/Cfc;

    .line 1926645
    new-instance v0, LX/Cfc;

    const-string v1, "VIEW_STORY_INSIGHTS_TAP"

    const/16 v2, 0x93

    const-string v3, "view_story_insights_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->VIEW_STORY_INSIGHTS_TAP:LX/Cfc;

    .line 1926646
    new-instance v0, LX/Cfc;

    const-string v1, "VIEW_STORY_TAP"

    const/16 v2, 0x94

    const-string v3, "view_story_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->VIEW_STORY_TAP:LX/Cfc;

    .line 1926647
    new-instance v0, LX/Cfc;

    const-string v1, "VIEW_STRUCTURE_MENU_TAP"

    const/16 v2, 0x95

    const-string v3, "view_structured_menu_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->VIEW_STRUCTURE_MENU_TAP:LX/Cfc;

    .line 1926648
    new-instance v0, LX/Cfc;

    const-string v1, "WHITELIST_UNIT_TAP"

    const/16 v2, 0x96

    const-string v3, "whitelist_unit_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->WHITELIST_UNIT_TAP:LX/Cfc;

    .line 1926649
    new-instance v0, LX/Cfc;

    const-string v1, "WRITE_POST_TO_PAGE_TAP"

    const/16 v2, 0x97

    const-string v3, "write_post_to_page_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->WRITE_POST_TO_PAGE_TAP:LX/Cfc;

    .line 1926650
    new-instance v0, LX/Cfc;

    const-string v1, "WRITE_REVIEW_PUBLISH"

    const/16 v2, 0x98

    const-string v3, "write_review_publish"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->WRITE_REVIEW_PUBLISH:LX/Cfc;

    .line 1926651
    new-instance v0, LX/Cfc;

    const-string v1, "WRITE_REVIEW_TAP"

    const/16 v2, 0x99

    const-string v3, "write_review_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->WRITE_REVIEW_TAP:LX/Cfc;

    .line 1926652
    new-instance v0, LX/Cfc;

    const-string v1, "XOUT_TAP"

    const/16 v2, 0x9a

    const-string v3, "xout_tap"

    invoke-direct {v0, v1, v2, v3}, LX/Cfc;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Cfc;->XOUT_TAP:LX/Cfc;

    .line 1926653
    const/16 v0, 0x9b

    new-array v0, v0, [LX/Cfc;

    sget-object v1, LX/Cfc;->ADD_PAGE_COVER_PHOTO:LX/Cfc;

    aput-object v1, v0, v4

    sget-object v1, LX/Cfc;->ADD_PAGE_CTA:LX/Cfc;

    aput-object v1, v0, v5

    sget-object v1, LX/Cfc;->ADD_PAGE_PROFILE_PIC:LX/Cfc;

    aput-object v1, v0, v6

    sget-object v1, LX/Cfc;->ACCEPT_ADMIN_INVITE_TAP:LX/Cfc;

    aput-object v1, v0, v7

    sget-object v1, LX/Cfc;->ADD_MORE_CARDS_TAP:LX/Cfc;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/Cfc;->ADD_PHOTOS_AT_PLACE_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/Cfc;->ALBUM_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/Cfc;->APPLINK_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/Cfc;->BOOST_POST_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/Cfc;->BROWSE_QUERY_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/Cfc;->CALL_PHONE_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/Cfc;->CHECKIN_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/Cfc;->COLLAPSE_RICH_ATTACHMENT_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/Cfc;->CONFIRM_FRIEND_REQUEST_FROM_HSCROLL_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/Cfc;->CONFIRM_FRIEND_REQUEST_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/Cfc;->CONTACT_FUNDRAISER_SUPPORTERS:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/Cfc;->COPY_LINK_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/Cfc;->CREATION_TAB:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/Cfc;->CRISIS_MARK_AS_SAFE:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/Cfc;->CRISIS_NOT_IN_AREA:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, LX/Cfc;->CRISIS_UNMARK:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, LX/Cfc;->CRITIC_REVIEW_PAGE_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, LX/Cfc;->DECLINE_ADMIN_INVITE_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, LX/Cfc;->DELETE_FRIEND_REQUEST_FROM_HSCROLL_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, LX/Cfc;->DELETE_FRIEND_REQUEST_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, LX/Cfc;->DISABLE_UNIT_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, LX/Cfc;->DISMISS_UNIT_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, LX/Cfc;->EDIT_PAGE_INFO_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, LX/Cfc;->EVENT_CARD_GOING_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, LX/Cfc;->EVENT_CARD_INTERESTED_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, LX/Cfc;->EVENT_CARD_MAYBE_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, LX/Cfc;->EVENT_CARD_NOT_GOING_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, LX/Cfc;->EVENT_CARD_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, LX/Cfc;->EVENT_CARD_UNWATCHED_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, LX/Cfc;->EVENT_CARD_WATCHED_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, LX/Cfc;->EVENT_MESSAGE_FRIENDS:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, LX/Cfc;->EXPAND_RICH_ATTACHMENT_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, LX/Cfc;->EXTERNAL_PROFILE_LINK_SHARE:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, LX/Cfc;->FACEWEB_URL_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, LX/Cfc;->FUNDRAISER_COMPOSER:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, LX/Cfc;->GO_LIVE_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, LX/Cfc;->GROUP_JOIN_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, LX/Cfc;->GROUP_LEAVE_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, LX/Cfc;->HIDE_ADS_AFTER_PARTY_AYMT_TIP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, LX/Cfc;->INLINE_EXPANSION_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, LX/Cfc;->INNER_SCROLL:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, LX/Cfc;->INVITE_FRIENDS_TO_EVENT:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, LX/Cfc;->INVITE_FUNDRAISER_GUEST:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x30

    sget-object v2, LX/Cfc;->JOIN_EVENT_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x31

    sget-object v2, LX/Cfc;->LEAVE_FUNDRAISER:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x32

    sget-object v2, LX/Cfc;->LIKE_COMMENT_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x33

    sget-object v2, LX/Cfc;->LOCAL_CONTENT_REVIEW_UNIT_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x34

    sget-object v2, LX/Cfc;->MESSAGE_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x35

    sget-object v2, LX/Cfc;->NEARBY_FRIENDS_NUX_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x36

    sget-object v2, LX/Cfc;->NEARBY_FRIENDS_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x37

    sget-object v2, LX/Cfc;->NEARBY_PLACES_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x38

    sget-object v2, LX/Cfc;->NOTIFICATIONS_INLINE_EXPANSION_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x39

    sget-object v2, LX/Cfc;->NOTIFICATIONS_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    sget-object v2, LX/Cfc;->NUX_CONTINUE_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    sget-object v2, LX/Cfc;->OPEN_ACORN_WEATHER_SETTINGS_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    sget-object v2, LX/Cfc;->OPEN_ALL_NOTIFICATIONS_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    sget-object v2, LX/Cfc;->OPEN_APPOINTMENT_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    sget-object v2, LX/Cfc;->OPEN_COMPOSER_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    sget-object v2, LX/Cfc;->OPEN_FRIEND_INVITER:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x40

    sget-object v2, LX/Cfc;->OPEN_LOCAL_SEARCH_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x41

    sget-object v2, LX/Cfc;->OPEN_MAP_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x42

    sget-object v2, LX/Cfc;->OPEN_PAGE_ADD_PRODUCT_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x43

    sget-object v2, LX/Cfc;->OPEN_PAGE_CHILD_LOCATIONS:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x44

    sget-object v2, LX/Cfc;->OPEN_PAGE_COMMERCE_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x45

    sget-object v2, LX/Cfc;->OPEN_PAGE_INFO_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x46

    sget-object v2, LX/Cfc;->OPEN_PAGE_MAP_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x47

    sget-object v2, LX/Cfc;->OPEN_PAGE_NAVIGATION_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x48

    sget-object v2, LX/Cfc;->OPEN_PAGE_PROMOTION:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x49

    sget-object v2, LX/Cfc;->OPEN_PHOTO_COMPOSER_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x4a

    sget-object v2, LX/Cfc;->OPEN_UPCOMING_BIRTHDAYS_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x4b

    sget-object v2, LX/Cfc;->OPEN_VERTICAL_ACTION_SHEET_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x4c

    sget-object v2, LX/Cfc;->OPEN_VIDEO_CHANNEL:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x4d

    sget-object v2, LX/Cfc;->PAGE_ADD_TAB_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x4e

    sget-object v2, LX/Cfc;->PAGE_BROADCASTER_GO_LIVE_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x4f

    sget-object v2, LX/Cfc;->PAGE_EVENT_CREATE_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x50

    sget-object v2, LX/Cfc;->PAGE_MANAGER_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x51

    sget-object v2, LX/Cfc;->PAGE_NEW_ACTIVITY_COUNT_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x52

    sget-object v2, LX/Cfc;->PAGE_OPEN_CTA_SETUP_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x53

    sget-object v2, LX/Cfc;->PAGE_OPEN_GET_QUOTE_SETUP_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x54

    sget-object v2, LX/Cfc;->PAGE_OPEN_REQUEST_APPOINTMENT_SETUP_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x55

    sget-object v2, LX/Cfc;->PHOTO_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x56

    sget-object v2, LX/Cfc;->PLACE_TIPS_HIDE_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x57

    sget-object v2, LX/Cfc;->PROFILE_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x58

    sget-object v2, LX/Cfc;->PROMOTE_LOCAL_BUSINESS:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x59

    sget-object v2, LX/Cfc;->PROMOTE_PAGE:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x5a

    sget-object v2, LX/Cfc;->PROMOTE_WEBSITE:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x5b

    sget-object v2, LX/Cfc;->QUESTION_ANSWER_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x5c

    sget-object v2, LX/Cfc;->QUESTION_SKIP_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x5d

    sget-object v2, LX/Cfc;->RATING_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x5e

    sget-object v2, LX/Cfc;->RELOAD_UNIT_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x5f

    sget-object v2, LX/Cfc;->REPLACE_UNIT_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x60

    sget-object v2, LX/Cfc;->REPLY_COMMMENT_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x61

    sget-object v2, LX/Cfc;->SAVE_PAGE_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x62

    sget-object v2, LX/Cfc;->SEE_ALL_COMPONENTS:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x63

    sget-object v2, LX/Cfc;->SEE_ALL_FRIEND_REQUESTS:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x64

    sget-object v2, LX/Cfc;->SEE_ALL_RATINGS_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x65

    sget-object v2, LX/Cfc;->SEE_ALL_SUGGESTED_EVENTS_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x66

    sget-object v2, LX/Cfc;->SEE_MORE_APPS_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x67

    sget-object v2, LX/Cfc;->SEE_MORE_EVENTS:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x68

    sget-object v2, LX/Cfc;->SEE_MORE_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x69

    sget-object v2, LX/Cfc;->SEE_JOB_DETAIL:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x6a

    sget-object v2, LX/Cfc;->SEE_OFFER_DETAIL:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x6b

    sget-object v2, LX/Cfc;->SEE_PAGE_ADS_AND_PROMOTIONS:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x6c

    sget-object v2, LX/Cfc;->SEE_PAGE_LIKES_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x6d

    sget-object v2, LX/Cfc;->SHARE_COMMENT_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x6e

    sget-object v2, LX/Cfc;->SHARE_EVENT_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x6f

    sget-object v2, LX/Cfc;->SHARE_FUNDRAISER:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x70

    sget-object v2, LX/Cfc;->SHARE_PHOTO_TO_PAGE_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x71

    sget-object v2, LX/Cfc;->STACK_CHILD_INTERACTION:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x72

    sget-object v2, LX/Cfc;->STORY_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x73

    sget-object v2, LX/Cfc;->SUBSCRIBE_PAGE_EVENTS_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x74

    sget-object v2, LX/Cfc;->TEXT_ENTITY_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x75

    sget-object v2, LX/Cfc;->TOPIC_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x76

    sget-object v2, LX/Cfc;->UNHIGHLIGHT_RICH_NOTIFICATION_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x77

    sget-object v2, LX/Cfc;->UNSAVE_PAGE_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x78

    sget-object v2, LX/Cfc;->VIDEO_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x79

    sget-object v2, LX/Cfc;->VIEW_APP_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x7a

    sget-object v2, LX/Cfc;->VIEW_COMMENT_ATTACHMENT_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x7b

    sget-object v2, LX/Cfc;->VIEW_COMMENT_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x7c

    sget-object v2, LX/Cfc;->VIEW_EVENT_GUEST_LIST:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x7d

    sget-object v2, LX/Cfc;->VIEW_EVENT_POSTS:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x7e

    sget-object v2, LX/Cfc;->VIEW_EVENTS_DASHBOARD_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x7f

    sget-object v2, LX/Cfc;->VIEW_FRIEND_CENTER_SUGGESTIONS_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x80

    sget-object v2, LX/Cfc;->VIEW_FRIEND_REQUEST_PROFILE_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x81

    sget-object v2, LX/Cfc;->VIEW_FRIEND_REQUESTS_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x82

    sget-object v2, LX/Cfc;->VIEW_FUNDRAISER:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x83

    sget-object v2, LX/Cfc;->VIEW_FUNDRAISER_SUPPORTERS:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x84

    sget-object v2, LX/Cfc;->VIEW_GROUP_EVENTS_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x85

    sget-object v2, LX/Cfc;->VIEW_GROUP_PHOTOS_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x86

    sget-object v2, LX/Cfc;->VIEW_GROUP_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x87

    sget-object v2, LX/Cfc;->VIEW_PAGE_CHECKINS:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x88

    sget-object v2, LX/Cfc;->VIEW_PAGE_INSIGHTS:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x89

    sget-object v2, LX/Cfc;->VIEW_PAGE_LIKERS:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x8a

    sget-object v2, LX/Cfc;->VIEW_PAGE_MENTIONS:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x8b

    sget-object v2, LX/Cfc;->VIEW_PAGE_PROFILE:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x8c

    sget-object v2, LX/Cfc;->VIEW_PAGE_SERVICE_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x8d

    sget-object v2, LX/Cfc;->VIEW_PAGE_SETTINGS:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x8e

    sget-object v2, LX/Cfc;->VIEW_PAGE_SHARES:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x8f

    sget-object v2, LX/Cfc;->VIEW_PHOTO_MENU_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x90

    sget-object v2, LX/Cfc;->VIEW_POST_INSIGHTS:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x91

    sget-object v2, LX/Cfc;->VIEW_PROFILE_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x92

    sget-object v2, LX/Cfc;->VIEW_PYML_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x93

    sget-object v2, LX/Cfc;->VIEW_STORY_INSIGHTS_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x94

    sget-object v2, LX/Cfc;->VIEW_STORY_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x95

    sget-object v2, LX/Cfc;->VIEW_STRUCTURE_MENU_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x96

    sget-object v2, LX/Cfc;->WHITELIST_UNIT_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x97

    sget-object v2, LX/Cfc;->WRITE_POST_TO_PAGE_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x98

    sget-object v2, LX/Cfc;->WRITE_REVIEW_PUBLISH:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x99

    sget-object v2, LX/Cfc;->WRITE_REVIEW_TAP:LX/Cfc;

    aput-object v2, v0, v1

    const/16 v1, 0x9a

    sget-object v2, LX/Cfc;->XOUT_TAP:LX/Cfc;

    aput-object v2, v0, v1

    sput-object v0, LX/Cfc;->$VALUES:[LX/Cfc;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1926654
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1926655
    iput-object p3, p0, LX/Cfc;->name:Ljava/lang/String;

    .line 1926656
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Cfc;
    .locals 1

    .prologue
    .line 1926657
    const-class v0, LX/Cfc;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Cfc;

    return-object v0
.end method

.method public static values()[LX/Cfc;
    .locals 1

    .prologue
    .line 1926658
    sget-object v0, LX/Cfc;->$VALUES:[LX/Cfc;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Cfc;

    return-object v0
.end method
