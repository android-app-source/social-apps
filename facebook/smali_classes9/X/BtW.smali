.class public final LX/BtW;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/model/GraphQLFeedback;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;)V
    .locals 0

    .prologue
    .line 1829293
    iput-object p1, p0, LX/BtW;->a:Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 1829294
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1829295
    check-cast p1, Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1829296
    iget-object v0, p0, LX/BtW;->a:Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;

    .line 1829297
    if-eqz p1, :cond_0

    iget-object v1, v0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ao:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->x()J

    move-result-wide v1

    iget-object v3, v0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ao:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->F_()J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-gez v1, :cond_1

    .line 1829298
    :cond_0
    :goto_0
    return-void

    .line 1829299
    :cond_1
    invoke-static {v0, p1}, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->a$redex0(Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;Lcom/facebook/graphql/model/GraphQLFeedback;)V

    goto :goto_0
.end method
