.class public LX/COu;
.super LX/3mY;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/3mY",
        "<",
        "Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;",
        "LX/COt;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/1X1;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/COr;

.field private final c:LX/1OX;

.field private d:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

.field public e:Z

.field public f:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "LX/1X1;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1883738
    new-instance v0, LX/COt;

    invoke-direct {v0}, LX/COt;-><init>()V

    invoke-direct {p0, p1, v0}, LX/3mY;-><init>(Landroid/content/Context;LX/3me;)V

    .line 1883739
    const/4 v0, 0x0

    iput v0, p0, LX/COu;->f:I

    .line 1883740
    new-instance v0, LX/COr;

    invoke-direct {v0, p1, p0}, LX/COr;-><init>(Landroid/content/Context;LX/COu;)V

    iput-object v0, p0, LX/COu;->b:LX/COr;

    .line 1883741
    new-instance v0, LX/COs;

    invoke-direct {v0, p0}, LX/COs;-><init>(LX/COu;)V

    iput-object v0, p0, LX/COu;->c:LX/1OX;

    .line 1883742
    iget-object v0, p0, LX/COu;->b:LX/COr;

    .line 1883743
    iput-object v0, p0, LX/3mY;->h:LX/3mh;

    .line 1883744
    iput-object p2, p0, LX/COu;->a:Ljava/util/List;

    .line 1883745
    return-void
.end method


# virtual methods
.method public final a(LX/1De;I)LX/1X1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "I)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1883721
    iget-object v0, p0, LX/COu;->a:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1X1;

    return-object v0
.end method

.method public final b(III)Z
    .locals 1

    .prologue
    .line 1883722
    const/4 v0, 0x1

    return v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 1883723
    iget-object v0, p0, LX/COu;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final e(Landroid/view/ViewGroup;)V
    .locals 1

    .prologue
    .line 1883724
    check-cast p1, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    .line 1883725
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/COu;->e:Z

    .line 1883726
    iput-object p1, p0, LX/COu;->d:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    .line 1883727
    iget-object v0, p0, LX/COu;->b:LX/COr;

    invoke-virtual {p1, v0}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 1883728
    iget-object v0, p0, LX/COu;->c:LX/1OX;

    invoke-virtual {p1, v0}, Landroid/support/v7/widget/RecyclerView;->a(LX/1OX;)V

    .line 1883729
    return-void
.end method

.method public final f()V
    .locals 3

    .prologue
    .line 1883730
    const/4 v0, 0x0

    invoke-virtual {p0}, LX/COu;->e()I

    move-result v1

    const/4 v2, 0x3

    invoke-virtual {p0, v0, v1, v2}, LX/3mY;->a(III)V

    .line 1883731
    return-void
.end method

.method public final f(Landroid/view/ViewGroup;)V
    .locals 3

    .prologue
    .line 1883732
    const/4 v2, 0x0

    .line 1883733
    iget-object v0, p0, LX/COu;->d:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    iget-object v1, p0, LX/COu;->c:LX/1OX;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->b(LX/1OX;)V

    .line 1883734
    iget-object v0, p0, LX/COu;->d:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 1883735
    iput-object v2, p0, LX/COu;->d:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    .line 1883736
    return-void
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 1883737
    const/4 v0, 0x0

    return v0
.end method
