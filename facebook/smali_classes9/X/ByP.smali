.class public LX/ByP;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public final a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private final b:Landroid/widget/TextView;

.field public c:Landroid/view/View;

.field public d:Lcom/facebook/gif/AnimatedImagePlayButtonView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1837304
    const v0, 0x7f0308d5

    invoke-direct {p0, p1, v0}, LX/ByP;-><init>(Landroid/content/Context;I)V

    .line 1837305
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;I)V
    .locals 4

    .prologue
    .line 1837286
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1837287
    invoke-virtual {p0, p2}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1837288
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/ByP;->setOrientation(I)V

    .line 1837289
    const v0, 0x7f0d16ea

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, LX/ByP;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1837290
    const v0, 0x7f0d16ec

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/ByP;->b:Landroid/widget/TextView;

    .line 1837291
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    .line 1837292
    new-instance v0, Lcom/facebook/gif/AnimatedImagePlayButtonView;

    invoke-virtual {p0}, LX/ByP;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/gif/AnimatedImagePlayButtonView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/ByP;->d:Lcom/facebook/gif/AnimatedImagePlayButtonView;

    .line 1837293
    iget-object v0, p0, LX/ByP;->d:Lcom/facebook/gif/AnimatedImagePlayButtonView;

    iput-object v0, p0, LX/ByP;->c:Landroid/view/View;

    .line 1837294
    iget-object v0, p0, LX/ByP;->c:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1837295
    const v0, 0x7f0d16eb

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 1837296
    iget-object v1, p0, LX/ByP;->c:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 1837297
    new-instance v0, LX/1Uo;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    .line 1837298
    iget-object v1, p0, LX/ByP;->d:Lcom/facebook/gif/AnimatedImagePlayButtonView;

    if-nez v1, :cond_0

    .line 1837299
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f021af6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 1837300
    new-instance v2, Lcom/facebook/drawee/drawable/AutoRotateDrawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    const/16 v3, 0x3e8

    invoke-direct {v2, v1, v3}, Lcom/facebook/drawee/drawable/AutoRotateDrawable;-><init>(Landroid/graphics/drawable/Drawable;I)V

    .line 1837301
    iput-object v2, v0, LX/1Uo;->l:Landroid/graphics/drawable/Drawable;

    .line 1837302
    :cond_0
    iget-object v1, p0, LX/ByP;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 1837303
    return-void
.end method


# virtual methods
.method public getImage()Lcom/facebook/drawee/fbpipeline/FbDraweeView;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1837285
    iget-object v0, p0, LX/ByP;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    return-object v0
.end method

.method public getPlayButton()Landroid/view/View;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1837284
    iget-object v0, p0, LX/ByP;->c:Landroid/view/View;

    return-object v0
.end method

.method public setAspectRatio(F)V
    .locals 1

    .prologue
    .line 1837282
    iget-object v0, p0, LX/ByP;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, p1}, Lcom/facebook/drawee/view/DraweeView;->setAspectRatio(F)V

    .line 1837283
    return-void
.end method

.method public setBottomText(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1837280
    iget-object v0, p0, LX/ByP;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1837281
    return-void
.end method

.method public setBottomTextVisibility(Z)V
    .locals 2

    .prologue
    .line 1837277
    iget-object v1, p0, LX/ByP;->b:Landroid/widget/TextView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1837278
    return-void

    .line 1837279
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setImageController(LX/1aZ;)V
    .locals 1
    .param p1    # LX/1aZ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1837265
    iget-object v0, p0, LX/ByP;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, p1}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 1837266
    return-void
.end method

.method public setOnImageClickListener(Landroid/view/View$OnClickListener;)V
    .locals 2
    .param p1    # Landroid/view/View$OnClickListener;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1837273
    iget-object v0, p0, LX/ByP;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1837274
    if-nez p1, :cond_0

    .line 1837275
    iget-object v0, p0, LX/ByP;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setClickable(Z)V

    .line 1837276
    :cond_0
    return-void
.end method

.method public setPlayButtonState(LX/6Wv;)V
    .locals 2

    .prologue
    .line 1837267
    iget-object v0, p0, LX/ByP;->d:Lcom/facebook/gif/AnimatedImagePlayButtonView;

    if-eqz v0, :cond_0

    .line 1837268
    iget-object v0, p0, LX/ByP;->d:Lcom/facebook/gif/AnimatedImagePlayButtonView;

    invoke-virtual {v0, p1}, Lcom/facebook/gif/AnimatedImagePlayButtonView;->setState(LX/6Wv;)V

    .line 1837269
    :goto_0
    return-void

    .line 1837270
    :cond_0
    sget-object v0, LX/6Wv;->READY_TO_PLAY:LX/6Wv;

    if-ne p1, v0, :cond_1

    .line 1837271
    iget-object v0, p0, LX/ByP;->c:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 1837272
    :cond_1
    iget-object v0, p0, LX/ByP;->c:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method
