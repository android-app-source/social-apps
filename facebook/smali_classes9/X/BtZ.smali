.class public final LX/BtZ;
.super LX/BNO;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;)V
    .locals 0

    .prologue
    .line 1829347
    iput-object p1, p0, LX/BtZ;->a:Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;

    invoke-direct {p0}, LX/BNO;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/fbservice/service/OperationResult;)V
    .locals 3

    .prologue
    .line 1829348
    iget-object v0, p0, LX/BtZ;->a:Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;

    invoke-virtual {v0}, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->d()V

    .line 1829349
    iget-object v1, p0, LX/BtZ;->a:Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;

    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5tj;

    .line 1829350
    iget-object v2, v1, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ao:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1829351
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->B()LX/0Px;

    move-result-object p0

    const p1, -0x452d50ee

    invoke-static {p0, p1}, LX/1VX;->a(Ljava/util/List;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object p0

    .line 1829352
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aF()Lcom/facebook/graphql/model/GraphQLContactRecommendationField;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aF()Lcom/facebook/graphql/model/GraphQLContactRecommendationField;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->l()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object p1

    if-nez p1, :cond_2

    .line 1829353
    :cond_0
    const/4 p0, 0x0

    .line 1829354
    :goto_0
    move-object p0, p0

    .line 1829355
    if-nez p0, :cond_1

    .line 1829356
    :goto_1
    return-void

    .line 1829357
    :cond_1
    iget-object v2, v1, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->o:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Ch5;

    invoke-static {p0, v0}, LX/ChH;->a(Ljava/lang/String;LX/5tj;)LX/ChF;

    move-result-object p0

    invoke-virtual {v2, p0}, LX/0b4;->a(LX/0b7;)V

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aF()Lcom/facebook/graphql/model/GraphQLContactRecommendationField;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLContactRecommendationField;->l()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method
