.class public final LX/ByL;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/ByM;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/1Pq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public c:Ljava/lang/String;

.field public final synthetic d:LX/ByM;


# direct methods
.method public constructor <init>(LX/ByM;)V
    .locals 1

    .prologue
    .line 1837090
    iput-object p1, p0, LX/ByL;->d:LX/ByM;

    .line 1837091
    move-object v0, p1

    .line 1837092
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1837093
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1837094
    const-string v0, "EventTicketAttachmentFooterComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1837095
    if-ne p0, p1, :cond_1

    .line 1837096
    :cond_0
    :goto_0
    return v0

    .line 1837097
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1837098
    goto :goto_0

    .line 1837099
    :cond_3
    check-cast p1, LX/ByL;

    .line 1837100
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1837101
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1837102
    if-eq v2, v3, :cond_0

    .line 1837103
    iget-object v2, p0, LX/ByL;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/ByL;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/ByL;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1837104
    goto :goto_0

    .line 1837105
    :cond_5
    iget-object v2, p1, LX/ByL;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 1837106
    :cond_6
    iget-object v2, p0, LX/ByL;->b:LX/1Pq;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/ByL;->b:LX/1Pq;

    iget-object v3, p1, LX/ByL;->b:LX/1Pq;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1837107
    goto :goto_0

    .line 1837108
    :cond_8
    iget-object v2, p1, LX/ByL;->b:LX/1Pq;

    if-nez v2, :cond_7

    .line 1837109
    :cond_9
    iget-object v2, p0, LX/ByL;->c:Ljava/lang/String;

    if-eqz v2, :cond_a

    iget-object v2, p0, LX/ByL;->c:Ljava/lang/String;

    iget-object v3, p1, LX/ByL;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1837110
    goto :goto_0

    .line 1837111
    :cond_a
    iget-object v2, p1, LX/ByL;->c:Ljava/lang/String;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
