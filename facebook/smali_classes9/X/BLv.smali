.class public LX/BLv;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarController$DataProvider;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/03V;

.field public final c:Landroid/content/res/Resources;

.field private final d:Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarView;

.field private final e:LX/8Rp;

.field private final f:Lcom/facebook/platform/composer/titlebar/PlatformComposerTargetImageController;

.field public final g:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>(Landroid/view/ViewStub;LX/BKK;LX/03V;Landroid/content/res/Resources;LX/BKi;LX/8Rp;LX/BLk;Ljava/lang/Boolean;)V
    .locals 3
    .param p1    # Landroid/view/ViewStub;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/BKK;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p8    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1776518
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1776519
    iput-object p3, p0, LX/BLv;->b:LX/03V;

    .line 1776520
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/BLv;->a:Ljava/lang/ref/WeakReference;

    .line 1776521
    iput-object p4, p0, LX/BLv;->c:Landroid/content/res/Resources;

    .line 1776522
    iput-object p6, p0, LX/BLv;->e:LX/8Rp;

    .line 1776523
    iput-object p8, p0, LX/BLv;->g:Ljava/lang/Boolean;

    .line 1776524
    invoke-virtual {p2}, LX/BKK;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1776525
    const v0, 0x7f030fd0

    invoke-virtual {p1, v0}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 1776526
    invoke-virtual {p1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarView;

    iput-object v0, p0, LX/BLv;->d:Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarView;

    .line 1776527
    iget-object v0, p0, LX/BLv;->d:Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarView;

    const v1, 0x7f0d2628

    invoke-virtual {v0, v1}, Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1776528
    iget-object v1, p0, LX/BLv;->d:Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarView;

    const v2, 0x7f0d2629

    invoke-virtual {v1, v2}, Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 1776529
    new-instance p1, Lcom/facebook/platform/composer/titlebar/PlatformComposerTargetImageController;

    const/16 v2, 0x12cb

    invoke-static {p7, v2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p3

    invoke-static {p7}, LX/8Sa;->a(LX/0QB;)LX/8Sa;

    move-result-object v2

    check-cast v2, LX/8Sa;

    invoke-direct {p1, v0, v1, p3, v2}, Lcom/facebook/platform/composer/titlebar/PlatformComposerTargetImageController;-><init>(Landroid/view/View;Landroid/view/View;LX/0Or;LX/8Sa;)V

    .line 1776530
    move-object v0, p1

    .line 1776531
    iput-object v0, p0, LX/BLv;->f:Lcom/facebook/platform/composer/titlebar/PlatformComposerTargetImageController;

    .line 1776532
    :goto_0
    iget-object v0, p0, LX/BLv;->d:Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarView;

    .line 1776533
    new-instance v1, LX/BKH;

    invoke-direct {v1, p2}, LX/BKH;-><init>(LX/BKK;)V

    move-object v1, v1

    .line 1776534
    iput-object v1, v0, Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarView;->a:Landroid/view/View$OnClickListener;

    .line 1776535
    iget-object v0, p0, LX/BLv;->d:Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarView;

    .line 1776536
    new-instance v1, LX/BKI;

    invoke-direct {v1, p2}, LX/BKI;-><init>(LX/BKK;)V

    move-object v1, v1

    .line 1776537
    iput-object v1, v0, Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarView;->b:Landroid/view/View$OnClickListener;

    .line 1776538
    iget-object v0, p0, LX/BLv;->d:Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarView;

    .line 1776539
    new-instance v1, LX/BKJ;

    invoke-direct {v1, p2}, LX/BKJ;-><init>(LX/BKK;)V

    move-object v1, v1

    .line 1776540
    iput-object v1, v0, Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarView;->c:Landroid/view/View$OnClickListener;

    .line 1776541
    iget-object v1, p0, LX/BLv;->d:Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarView;

    iget-object v0, p5, LX/BKi;->b:LX/BKh;

    sget-object v2, LX/BKh;->TOP:LX/BKh;

    if-ne v0, v2, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v1, v0}, Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarView;->setShowPostButton(Z)V

    .line 1776542
    invoke-virtual {p0}, LX/BLv;->a()V

    .line 1776543
    return-void

    .line 1776544
    :cond_0
    const v0, 0x7f030fcf

    invoke-virtual {p1, v0}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 1776545
    invoke-virtual {p1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarView;

    iput-object v0, p0, LX/BLv;->d:Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarView;

    .line 1776546
    const/4 v0, 0x0

    iput-object v0, p0, LX/BLv;->f:Lcom/facebook/platform/composer/titlebar/PlatformComposerTargetImageController;

    goto :goto_0

    .line 1776547
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private a(Lcom/facebook/composer/privacy/model/ComposerPrivacyData;Z)Ljava/lang/String;
    .locals 4

    .prologue
    .line 1776548
    iget-object v0, p1, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    iget-object v0, v0, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    if-eqz v0, :cond_0

    .line 1776549
    iget-object v0, p0, LX/BLv;->e:LX/8Rp;

    iget-object v1, p1, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    iget-object v2, p0, LX/BLv;->c:Landroid/content/res/Resources;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, p2, v3}, LX/8Rp;->a(Lcom/facebook/privacy/model/SelectablePrivacyData;Landroid/content/res/Resources;ZZ)LX/0Px;

    move-result-object v0

    .line 1776550
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1776551
    const-string v2, ", "

    new-instance v3, LX/BKw;

    invoke-direct {v3}, LX/BKw;-><init>()V

    const/4 p0, 0x1

    new-array p0, p0, [Ljava/lang/Object;

    const/4 p1, 0x0

    aput-object v0, p0, p1

    invoke-static {v1, v2, v3, p0}, LX/0YN;->a(Ljava/lang/StringBuilder;Ljava/lang/String;LX/1jt;[Ljava/lang/Object;)V

    .line 1776552
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1776553
    :goto_0
    return-object v0

    .line 1776554
    :cond_0
    iget-object v0, p1, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->a:Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;

    if-eqz v0, :cond_1

    .line 1776555
    iget-object v0, p1, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->a:Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;

    iget-object v0, v0, Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;->b:Ljava/lang/String;

    goto :goto_0

    .line 1776556
    :cond_1
    iget-boolean v0, p1, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->d:Z

    if-eqz v0, :cond_2

    .line 1776557
    const-string v0, "..."

    goto :goto_0

    .line 1776558
    :cond_2
    iget-object v0, p0, LX/BLv;->b:LX/03V;

    const-string v1, "platform_composer_update_titlebar"

    const-string v2, "No privacy data"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1776559
    const-string v0, ""

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 8

    .prologue
    .line 1776560
    iget-object v0, p0, LX/BLv;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BKK;

    .line 1776561
    iget-object v1, p0, LX/BLv;->d:Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarView;

    if-nez v1, :cond_0

    .line 1776562
    iget-object v0, p0, LX/BLv;->b:LX/03V;

    const-string v1, "composer_update_titlebar"

    const-string v2, "TitleBar is not set in Composer Fragment!"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1776563
    :goto_0
    return-void

    .line 1776564
    :cond_0
    iget-object v1, p0, LX/BLv;->d:Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarView;

    .line 1776565
    iget-object v2, v0, LX/BKK;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    invoke-static {v2}, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->s(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)Z

    move-result v2

    move v2, v2

    .line 1776566
    invoke-virtual {v1, v2}, Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarView;->setPostButtonEnabled(Z)V

    .line 1776567
    invoke-virtual {v0}, LX/BKK;->c()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1776568
    iget-object v1, p0, LX/BLv;->d:Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarView;

    .line 1776569
    iget-object v2, p0, LX/BLv;->c:Landroid/content/res/Resources;

    const v3, 0x7f082488

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object v2, v2

    .line 1776570
    invoke-virtual {v1, v2}, Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarView;->setTitlePrefix(Ljava/lang/String;)V

    .line 1776571
    iget-object v1, p0, LX/BLv;->d:Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarView;

    invoke-virtual {v0}, LX/BKK;->a()LX/BKm;

    move-result-object v2

    iget-object v2, v2, LX/BKm;->e:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    .line 1776572
    sget-object v3, LX/BLu;->a:[I

    iget-object v4, v2, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    invoke-virtual {v4}, LX/2rw;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 1776573
    iget-object v3, v2, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetName:Ljava/lang/String;

    :goto_1
    move-object v2, v3

    .line 1776574
    invoke-virtual {v1, v2}, Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarView;->setTitle(Ljava/lang/String;)V

    .line 1776575
    iget-object v1, p0, LX/BLv;->d:Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarView;

    .line 1776576
    iget-object v2, p0, LX/BLv;->c:Landroid/content/res/Resources;

    const v3, 0x7f08248a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object v2, v2

    .line 1776577
    invoke-virtual {v1, v2}, Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarView;->setSubtitlePrefix(Ljava/lang/String;)V

    .line 1776578
    iget-object v1, p0, LX/BLv;->d:Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarView;

    invoke-virtual {v0}, LX/BKK;->a()LX/BKm;

    move-result-object v2

    iget-object v2, v2, LX/BKm;->d:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    .line 1776579
    iget-object v3, v0, LX/BKK;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    invoke-static {v3}, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->M(Lcom/facebook/platform/composer/composer/PlatformComposerFragment;)Z

    move-result v3

    move v3, v3

    .line 1776580
    invoke-direct {p0, v2, v3}, LX/BLv;->a(Lcom/facebook/composer/privacy/model/ComposerPrivacyData;Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarView;->setSubtitle(Ljava/lang/String;)V

    .line 1776581
    iget-object v1, p0, LX/BLv;->f:Lcom/facebook/platform/composer/titlebar/PlatformComposerTargetImageController;

    invoke-virtual {v0}, LX/BKK;->a()LX/BKm;

    move-result-object v2

    iget-object v2, v2, LX/BKm;->e:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    invoke-virtual {v0}, LX/BKK;->a()LX/BKm;

    move-result-object v0

    iget-object v0, v0, LX/BKm;->d:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    .line 1776582
    iget-object v3, v2, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    sget-object v4, LX/2rw;->USER:LX/2rw;

    if-ne v3, v4, :cond_2

    .line 1776583
    iget-object v3, v1, Lcom/facebook/platform/composer/titlebar/PlatformComposerTargetImageController;->d:Landroid/view/View;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1776584
    iget-object v3, v1, Lcom/facebook/platform/composer/titlebar/PlatformComposerTargetImageController;->c:Landroid/view/View;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1776585
    iget-object v3, v1, Lcom/facebook/platform/composer/titlebar/PlatformComposerTargetImageController;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v4, v2, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetProfilePicUrl:Ljava/lang/String;

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    iget-object v0, v1, Lcom/facebook/platform/composer/titlebar/PlatformComposerTargetImageController;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v3, v4, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1776586
    :goto_2
    goto/16 :goto_0

    .line 1776587
    :cond_1
    iget-object v1, p0, LX/BLv;->d:Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarView;

    invoke-virtual {v0}, LX/BKK;->a()LX/BKm;

    move-result-object v0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1776588
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1776589
    iget-object v2, v0, LX/BKm;->b:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    .line 1776590
    iget-object v3, v0, LX/BKm;->e:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    .line 1776591
    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1776592
    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1776593
    sget-object v4, LX/BLu;->a:[I

    iget-object v5, v3, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    invoke-virtual {v5}, LX/2rw;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_1

    .line 1776594
    iget-object v2, v3, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetName:Ljava/lang/String;

    :goto_3
    move-object v0, v2

    .line 1776595
    invoke-virtual {v1, v0}, Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarView;->setTitle(Ljava/lang/String;)V

    .line 1776596
    iget-object v0, p0, LX/BLv;->d:Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarView;

    .line 1776597
    iget-object v1, p0, LX/BLv;->c:Landroid/content/res/Resources;

    const v2, 0x7f08247c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    move-object v1, v1

    .line 1776598
    invoke-virtual {v0, v1}, Lcom/facebook/platform/composer/titlebar/PlatformComposerTitleBarView;->setSubtitle(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1776599
    :pswitch_0
    iget-object v3, p0, LX/BLv;->c:Landroid/content/res/Resources;

    const v4, 0x7f08133d

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_1

    .line 1776600
    :pswitch_1
    iget-object v3, p0, LX/BLv;->c:Landroid/content/res/Resources;

    const v4, 0x7f082489

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, v2, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetName:Ljava/lang/String;

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_1

    .line 1776601
    :cond_2
    iget-object v3, v1, Lcom/facebook/platform/composer/titlebar/PlatformComposerTargetImageController;->d:Landroid/view/View;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1776602
    iget-object v3, v1, Lcom/facebook/platform/composer/titlebar/PlatformComposerTargetImageController;->c:Landroid/view/View;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1776603
    iget-object v3, v1, Lcom/facebook/platform/composer/titlebar/PlatformComposerTargetImageController;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v4, 0x0

    iget-object v5, v1, Lcom/facebook/platform/composer/titlebar/PlatformComposerTargetImageController;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v3, v4, v5}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1776604
    iget-object v3, v0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    if-eqz v3, :cond_4

    iget-object v3, v0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    iget-object v3, v3, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    if-eqz v3, :cond_4

    iget-object v3, v0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1776605
    iget-object v4, v3, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v3, v4

    .line 1776606
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    if-eqz v3, :cond_4

    .line 1776607
    iget-object v3, v0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1776608
    iget-object v4, v3, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v3, v4

    .line 1776609
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLImage;->d()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->fromIconName(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    move-result-object v3

    .line 1776610
    const/16 v6, 0x8

    const/4 v5, 0x0

    .line 1776611
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->EVERYONE:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    if-ne v3, v4, :cond_5

    .line 1776612
    iget-object v4, v1, Lcom/facebook/platform/composer/titlebar/PlatformComposerTargetImageController;->f:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v4, v6}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1776613
    iget-object v4, v1, Lcom/facebook/platform/composer/titlebar/PlatformComposerTargetImageController;->g:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v4, v5}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1776614
    iget-object v4, v1, Lcom/facebook/platform/composer/titlebar/PlatformComposerTargetImageController;->g:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1776615
    :goto_4
    invoke-virtual {v4}, Lcom/facebook/fbui/glyph/GlyphView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget-object v0, LX/8SZ;->GLYPH:LX/8SZ;

    invoke-static {v3, v0}, LX/8Sa;->a(Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;LX/8SZ;)I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/facebook/fbui/glyph/GlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1776616
    :cond_3
    :goto_5
    goto/16 :goto_2

    .line 1776617
    :cond_4
    iget-object v3, v0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->a:Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;

    if-eqz v3, :cond_3

    .line 1776618
    iget-object v3, v0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->a:Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;

    .line 1776619
    iget-object v4, v1, Lcom/facebook/platform/composer/titlebar/PlatformComposerTargetImageController;->f:Lcom/facebook/fbui/glyph/GlyphView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1776620
    iget-object v4, v1, Lcom/facebook/platform/composer/titlebar/PlatformComposerTargetImageController;->g:Lcom/facebook/fbui/glyph/GlyphView;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1776621
    iget-object v4, v1, Lcom/facebook/platform/composer/titlebar/PlatformComposerTargetImageController;->f:Lcom/facebook/fbui/glyph/GlyphView;

    iget-object v5, v1, Lcom/facebook/platform/composer/titlebar/PlatformComposerTargetImageController;->f:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v5}, Lcom/facebook/fbui/glyph/GlyphView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    iget-object v2, v3, Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;->a:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    sget-object v0, LX/8SZ;->PILL:LX/8SZ;

    invoke-static {v2, v0}, LX/8Sa;->a(Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;LX/8SZ;)I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/facebook/fbui/glyph/GlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1776622
    goto :goto_5

    .line 1776623
    :cond_5
    iget-object v4, v1, Lcom/facebook/platform/composer/titlebar/PlatformComposerTargetImageController;->f:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v4, v5}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1776624
    iget-object v4, v1, Lcom/facebook/platform/composer/titlebar/PlatformComposerTargetImageController;->g:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v4, v6}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1776625
    iget-object v4, v1, Lcom/facebook/platform/composer/titlebar/PlatformComposerTargetImageController;->f:Lcom/facebook/fbui/glyph/GlyphView;

    goto :goto_4

    .line 1776626
    :pswitch_2
    invoke-virtual {v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getComposerType()LX/2rt;

    move-result-object v2

    sget-object v3, LX/2rt;->SHARE:LX/2rt;

    if-ne v2, v3, :cond_7

    .line 1776627
    iget-object v3, p0, LX/BLv;->c:Landroid/content/res/Resources;

    iget-object v2, p0, LX/BLv;->g:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_6

    const v2, 0x7f08245d

    :goto_6
    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_3

    :cond_6
    const v2, 0x7f08245c

    goto :goto_6

    .line 1776628
    :cond_7
    iget-object v3, p0, LX/BLv;->c:Landroid/content/res/Resources;

    iget-object v2, p0, LX/BLv;->g:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_8

    const v2, 0x7f08245f

    :goto_7
    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_3

    :cond_8
    const v2, 0x7f08245e

    goto :goto_7

    .line 1776629
    :pswitch_3
    iget-object v2, p0, LX/BLv;->c:Landroid/content/res/Resources;

    const v4, 0x7f082460

    new-array v5, v7, [Ljava/lang/Object;

    iget-object v3, v3, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetName:Ljava/lang/String;

    aput-object v3, v5, v6

    invoke-virtual {v2, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_3

    .line 1776630
    :pswitch_4
    iget-object v2, v0, LX/BKm;->i:Lcom/facebook/ipc/composer/intent/ComposerPageData;

    if-eqz v2, :cond_9

    iget-object v2, v0, LX/BKm;->i:Lcom/facebook/ipc/composer/intent/ComposerPageData;

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/intent/ComposerPageData;->getPostAsPageViewerContext()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v2

    if-eqz v2, :cond_9

    iget-object v2, p0, LX/BLv;->c:Landroid/content/res/Resources;

    const v4, 0x7f082461

    new-array v5, v7, [Ljava/lang/Object;

    iget-object v3, v3, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetName:Ljava/lang/String;

    aput-object v3, v5, v6

    invoke-virtual {v2, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_3

    :cond_9
    iget-object v2, p0, LX/BLv;->c:Landroid/content/res/Resources;

    const v4, 0x7f082462

    new-array v5, v7, [Ljava/lang/Object;

    iget-object v3, v3, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetName:Ljava/lang/String;

    aput-object v3, v5, v6

    invoke-virtual {v2, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_3

    .line 1776631
    :pswitch_5
    iget-object v2, p0, LX/BLv;->c:Landroid/content/res/Resources;

    const v4, 0x7f082463

    new-array v5, v7, [Ljava/lang/Object;

    iget-object v3, v3, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetName:Ljava/lang/String;

    aput-object v3, v5, v6

    invoke-virtual {v2, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
