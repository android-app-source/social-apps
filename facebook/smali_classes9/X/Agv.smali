.class public final enum LX/Agv;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Agv;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Agv;

.field public static final enum FAILED:LX/Agv;

.field public static final enum PROCESSING:LX/Agv;

.field public static final enum SUCCEED:LX/Agv;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1701816
    new-instance v0, LX/Agv;

    const-string v1, "PROCESSING"

    invoke-direct {v0, v1, v2}, LX/Agv;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Agv;->PROCESSING:LX/Agv;

    .line 1701817
    new-instance v0, LX/Agv;

    const-string v1, "SUCCEED"

    invoke-direct {v0, v1, v3}, LX/Agv;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Agv;->SUCCEED:LX/Agv;

    .line 1701818
    new-instance v0, LX/Agv;

    const-string v1, "FAILED"

    invoke-direct {v0, v1, v4}, LX/Agv;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Agv;->FAILED:LX/Agv;

    .line 1701819
    const/4 v0, 0x3

    new-array v0, v0, [LX/Agv;

    sget-object v1, LX/Agv;->PROCESSING:LX/Agv;

    aput-object v1, v0, v2

    sget-object v1, LX/Agv;->SUCCEED:LX/Agv;

    aput-object v1, v0, v3

    sget-object v1, LX/Agv;->FAILED:LX/Agv;

    aput-object v1, v0, v4

    sput-object v0, LX/Agv;->$VALUES:[LX/Agv;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1701820
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Agv;
    .locals 1

    .prologue
    .line 1701821
    const-class v0, LX/Agv;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Agv;

    return-object v0
.end method

.method public static values()[LX/Agv;
    .locals 1

    .prologue
    .line 1701822
    sget-object v0, LX/Agv;->$VALUES:[LX/Agv;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Agv;

    return-object v0
.end method
