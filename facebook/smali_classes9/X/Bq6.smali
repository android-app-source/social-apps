.class public final LX/Bq6;
.super Landroid/text/style/ClickableSpan;
.source ""


# instance fields
.field public final synthetic a:LX/5kD;

.field public final synthetic b:LX/1Ps;

.field public final synthetic c:Lcom/facebook/feed/rows/photosfeed/PhotosFeedObjectionableAttachmentMessagePartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/photosfeed/PhotosFeedObjectionableAttachmentMessagePartDefinition;LX/5kD;LX/1Ps;)V
    .locals 0

    .prologue
    .line 1824109
    iput-object p1, p0, LX/Bq6;->c:Lcom/facebook/feed/rows/photosfeed/PhotosFeedObjectionableAttachmentMessagePartDefinition;

    iput-object p2, p0, LX/Bq6;->a:LX/5kD;

    iput-object p3, p0, LX/Bq6;->b:LX/1Ps;

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1824110
    iget-object v0, p0, LX/Bq6;->a:LX/5kD;

    invoke-interface {v0}, LX/5kD;->C()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataCreationStoryModel;

    move-result-object v0

    invoke-static {v0}, LX/5k9;->a(Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataCreationStoryModel;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 1824111
    iget-object v0, p0, LX/Bq6;->c:Lcom/facebook/feed/rows/photosfeed/PhotosFeedObjectionableAttachmentMessagePartDefinition;

    iget-object v0, v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedObjectionableAttachmentMessagePartDefinition;->c:LX/1WM;

    iget-object v2, p0, LX/Bq6;->a:LX/5kD;

    invoke-interface {v2}, LX/5kD;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2, v3}, LX/1WM;->b(Ljava/lang/String;Z)V

    .line 1824112
    iget-object v0, p0, LX/Bq6;->b:LX/1Ps;

    check-cast v0, LX/1Pq;

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    aput-object v1, v2, v3

    invoke-interface {v0, v2}, LX/1Pq;->a([Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1824113
    return-void
.end method

.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 2

    .prologue
    .line 1824114
    invoke-super {p0, p1}, Landroid/text/style/ClickableSpan;->updateDrawState(Landroid/text/TextPaint;)V

    .line 1824115
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    .line 1824116
    iget-object v0, p0, LX/Bq6;->c:Lcom/facebook/feed/rows/photosfeed/PhotosFeedObjectionableAttachmentMessagePartDefinition;

    iget-object v0, v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedObjectionableAttachmentMessagePartDefinition;->f:Landroid/content/Context;

    const v1, 0x7f0a00d9

    invoke-static {v0, v1}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 1824117
    return-void
.end method
