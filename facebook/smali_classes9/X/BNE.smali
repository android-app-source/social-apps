.class public LX/BNE;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/BNE;


# instance fields
.field private final a:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/BNU;

.field private final c:LX/0tX;


# direct methods
.method public constructor <init>(LX/1Ck;LX/BNU;LX/0tX;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1778395
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1778396
    iput-object p1, p0, LX/BNE;->a:LX/1Ck;

    .line 1778397
    iput-object p2, p0, LX/BNE;->b:LX/BNU;

    .line 1778398
    iput-object p3, p0, LX/BNE;->c:LX/0tX;

    .line 1778399
    return-void
.end method

.method public static a(LX/0QB;)LX/BNE;
    .locals 7

    .prologue
    .line 1778400
    sget-object v0, LX/BNE;->d:LX/BNE;

    if-nez v0, :cond_1

    .line 1778401
    const-class v1, LX/BNE;

    monitor-enter v1

    .line 1778402
    :try_start_0
    sget-object v0, LX/BNE;->d:LX/BNE;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1778403
    if-eqz v2, :cond_0

    .line 1778404
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1778405
    new-instance v6, LX/BNE;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v3

    check-cast v3, LX/1Ck;

    .line 1778406
    new-instance p0, LX/BNU;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v5

    check-cast v5, LX/0TD;

    invoke-direct {p0, v4, v5}, LX/BNU;-><init>(LX/0tX;LX/0TD;)V

    .line 1778407
    move-object v4, p0

    .line 1778408
    check-cast v4, LX/BNU;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v5

    check-cast v5, LX/0tX;

    invoke-direct {v6, v3, v4, v5}, LX/BNE;-><init>(LX/1Ck;LX/BNU;LX/0tX;)V

    .line 1778409
    move-object v0, v6

    .line 1778410
    sput-object v0, LX/BNE;->d:LX/BNE;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1778411
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1778412
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1778413
    :cond_1
    sget-object v0, LX/BNE;->d:LX/BNE;

    return-object v0

    .line 1778414
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1778415
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/BNC;)V
    .locals 6

    .prologue
    .line 1778416
    iget-object v0, p0, LX/BNE;->a:LX/1Ck;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "task_key_load_overall_rating"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/BNE;->b:LX/BNU;

    .line 1778417
    iget-object v4, v2, LX/BNU;->a:LX/0tX;

    .line 1778418
    new-instance v3, LX/BNR;

    invoke-direct {v3}, LX/BNR;-><init>()V

    move-object v3, v3

    .line 1778419
    const-string v5, "page_id"

    invoke-virtual {v3, v5, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v3

    check-cast v3, LX/BNR;

    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    invoke-virtual {v4, v3}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v3

    .line 1778420
    new-instance v4, LX/BNT;

    invoke-direct {v4, v2}, LX/BNT;-><init>(LX/BNU;)V

    iget-object v5, v2, LX/BNU;->b:LX/0TD;

    invoke-static {v3, v4, v5}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    move-object v2, v3

    .line 1778421
    new-instance v3, LX/BNB;

    invoke-direct {v3, p0, p2}, LX/BNB;-><init>(LX/BNE;LX/BNC;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1778422
    return-void
.end method

.method public final a(Ljava/lang/String;LX/BND;)V
    .locals 4

    .prologue
    .line 1778423
    iget-object v1, p0, LX/BNE;->c:LX/0tX;

    .line 1778424
    new-instance v0, LX/BNa;

    invoke-direct {v0}, LX/BNa;-><init>()V

    move-object v0, v0

    .line 1778425
    const-string v2, "page_id"

    invoke-virtual {v0, v2, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/BNa;

    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 1778426
    iget-object v1, p0, LX/BNE;->a:LX/1Ck;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "task_key_load_single_viewer_review"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, LX/BN9;

    invoke-direct {v3, p0, v0}, LX/BN9;-><init>(LX/BNE;Lcom/google/common/util/concurrent/ListenableFuture;)V

    new-instance v0, LX/BNA;

    invoke-direct {v0, p0, p2}, LX/BNA;-><init>(LX/BNE;LX/BND;)V

    invoke-virtual {v1, v2, v3, v0}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 1778427
    return-void
.end method
