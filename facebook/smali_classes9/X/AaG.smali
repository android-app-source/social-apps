.class public final LX/AaG;
.super LX/1a1;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public final m:Lcom/facebook/resources/ui/FbTextView;

.field public final n:Lcom/facebook/resources/ui/FbTextView;

.field public final o:Lcom/facebook/resources/ui/FbTextView;

.field public final p:Lcom/facebook/fbui/glyph/GlyphView;

.field public final synthetic q:Lcom/facebook/facecast/plugin/donation/FacecastDonationConnectedFundraiserAdapter;


# direct methods
.method public constructor <init>(Lcom/facebook/facecast/plugin/donation/FacecastDonationConnectedFundraiserAdapter;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1688163
    iput-object p1, p0, LX/AaG;->q:Lcom/facebook/facecast/plugin/donation/FacecastDonationConnectedFundraiserAdapter;

    .line 1688164
    invoke-direct {p0, p2}, LX/1a1;-><init>(Landroid/view/View;)V

    .line 1688165
    const v0, 0x7f0d0fe3

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, LX/AaG;->l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1688166
    const v0, 0x7f0d0fe4

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/AaG;->m:Lcom/facebook/resources/ui/FbTextView;

    .line 1688167
    const v0, 0x7f0d0fe5

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/AaG;->n:Lcom/facebook/resources/ui/FbTextView;

    .line 1688168
    const v0, 0x7f0d0fe6

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/AaG;->o:Lcom/facebook/resources/ui/FbTextView;

    .line 1688169
    const v0, 0x7f0d0fe7

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, LX/AaG;->p:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1688170
    invoke-virtual {p2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1688171
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x2

    const v0, -0x164a1bee

    invoke-static {v3, v5, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1688172
    invoke-virtual {p0}, LX/1a1;->e()I

    move-result v1

    .line 1688173
    iget-object v2, p0, LX/AaG;->q:Lcom/facebook/facecast/plugin/donation/FacecastDonationConnectedFundraiserAdapter;

    iget v2, v2, Lcom/facebook/facecast/plugin/donation/FacecastDonationConnectedFundraiserAdapter;->d:I

    .line 1688174
    if-ne v2, v1, :cond_1

    .line 1688175
    iget-object v1, p0, LX/AaG;->q:Lcom/facebook/facecast/plugin/donation/FacecastDonationConnectedFundraiserAdapter;

    const/4 v2, -0x1

    .line 1688176
    iput v2, v1, Lcom/facebook/facecast/plugin/donation/FacecastDonationConnectedFundraiserAdapter;->d:I

    .line 1688177
    iget-object v1, p0, LX/AaG;->p:Lcom/facebook/fbui/glyph/GlyphView;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1688178
    iget-object v1, p0, LX/AaG;->q:Lcom/facebook/facecast/plugin/donation/FacecastDonationConnectedFundraiserAdapter;

    iget-object v1, v1, Lcom/facebook/facecast/plugin/donation/FacecastDonationConnectedFundraiserAdapter;->f:Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;

    if-eqz v1, :cond_0

    .line 1688179
    iget-object v1, p0, LX/AaG;->q:Lcom/facebook/facecast/plugin/donation/FacecastDonationConnectedFundraiserAdapter;

    iget-object v1, v1, Lcom/facebook/facecast/plugin/donation/FacecastDonationConnectedFundraiserAdapter;->f:Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;

    invoke-virtual {v1, v5}, Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;->a(Z)V

    .line 1688180
    :cond_0
    const v1, -0x6684502a

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1688181
    :goto_0
    return-void

    .line 1688182
    :cond_1
    iget-object v3, p0, LX/AaG;->q:Lcom/facebook/facecast/plugin/donation/FacecastDonationConnectedFundraiserAdapter;

    .line 1688183
    iput v1, v3, Lcom/facebook/facecast/plugin/donation/FacecastDonationConnectedFundraiserAdapter;->d:I

    .line 1688184
    iget-object v1, p0, LX/AaG;->p:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v1, v4}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1688185
    if-ltz v2, :cond_2

    .line 1688186
    iget-object v1, p0, LX/AaG;->q:Lcom/facebook/facecast/plugin/donation/FacecastDonationConnectedFundraiserAdapter;

    invoke-virtual {v1, v2}, LX/1OM;->i_(I)V

    .line 1688187
    :cond_2
    iget-object v1, p0, LX/AaG;->q:Lcom/facebook/facecast/plugin/donation/FacecastDonationConnectedFundraiserAdapter;

    iget-object v1, v1, Lcom/facebook/facecast/plugin/donation/FacecastDonationConnectedFundraiserAdapter;->f:Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;

    if-eqz v1, :cond_3

    .line 1688188
    iget-object v1, p0, LX/AaG;->q:Lcom/facebook/facecast/plugin/donation/FacecastDonationConnectedFundraiserAdapter;

    iget-object v1, v1, Lcom/facebook/facecast/plugin/donation/FacecastDonationConnectedFundraiserAdapter;->f:Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;

    invoke-virtual {v1, v4}, Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;->a(Z)V

    .line 1688189
    :cond_3
    const v1, -0x63db4d1b

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto :goto_0
.end method
