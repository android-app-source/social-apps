.class public final LX/Aic;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:I

.field public final synthetic b:I

.field public final synthetic c:I

.field public final synthetic d:I

.field public final synthetic e:LX/Aie;


# direct methods
.method public constructor <init>(LX/Aie;IIII)V
    .locals 0

    .prologue
    .line 1706542
    iput-object p1, p0, LX/Aic;->e:LX/Aie;

    iput p2, p0, LX/Aic;->a:I

    iput p3, p0, LX/Aic;->b:I

    iput p4, p0, LX/Aic;->c:I

    iput p5, p0, LX/Aic;->d:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1706534
    new-instance v0, LX/Ahb;

    invoke-direct {v0}, LX/Ahb;-><init>()V

    move-object v0, v0

    .line 1706535
    const-string v1, "num_page_profiles"

    iget v2, p0, LX/Aic;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1706536
    const-string v1, "min_suggested_page_count"

    iget v2, p0, LX/Aic;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1706537
    const-string v1, "num_connected_friends"

    iget v2, p0, LX/Aic;->c:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1706538
    const-string v1, "connected_friends_profile_picture_size"

    iget v2, p0, LX/Aic;->d:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1706539
    invoke-static {v0}, LX/Aie;->b(LX/0gW;)V

    .line 1706540
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 1706541
    iget-object v1, p0, LX/Aic;->e:LX/Aie;

    iget-object v1, v1, LX/Aie;->b:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    return-object v0
.end method
