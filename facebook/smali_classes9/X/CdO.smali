.class public LX/CdO;
.super Landroid/widget/BaseAdapter;
.source ""


# instance fields
.field private final a:Landroid/content/Context;

.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/CdS;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1922438
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 1922439
    iput-object p1, p0, LX/CdO;->a:Landroid/content/Context;

    .line 1922440
    return-void
.end method

.method private a(I)LX/CdS;
    .locals 1

    .prologue
    .line 1922429
    iget-object v0, p0, LX/CdO;->b:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CdS;

    return-object v0
.end method


# virtual methods
.method public final getCount()I
    .locals 1

    .prologue
    .line 1922437
    iget-object v0, p0, LX/CdO;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method

.method public final synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1922441
    invoke-direct {p0, p1}, LX/CdO;->a(I)LX/CdS;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 1922436
    invoke-direct {p0, p1}, LX/CdO;->a(I)LX/CdS;

    move-result-object v0

    iget-object v0, v0, LX/CdS;->a:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bv_()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 1922430
    check-cast p2, Lcom/facebook/places/suggestions/PlaceRowView;

    .line 1922431
    if-nez p2, :cond_0

    .line 1922432
    new-instance p2, Lcom/facebook/places/suggestions/PlaceRowView;

    iget-object v0, p0, LX/CdO;->a:Landroid/content/Context;

    invoke-direct {p2, v0}, Lcom/facebook/places/suggestions/PlaceRowView;-><init>(Landroid/content/Context;)V

    .line 1922433
    :cond_0
    invoke-direct {p0, p1}, LX/CdO;->a(I)LX/CdS;

    move-result-object v0

    .line 1922434
    invoke-virtual {p2, v0}, Lcom/facebook/places/suggestions/PlaceRowView;->setInfo(LX/CdS;)V

    .line 1922435
    return-object p2
.end method

.method public final hasStableIds()Z
    .locals 1

    .prologue
    .line 1922428
    const/4 v0, 0x1

    return v0
.end method
