.class public final LX/Bfz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Vj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Vj",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionMutationsModels$PlaceQuestionAnswerSubmitModel;",
        ">;",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;


# direct methods
.method public constructor <init>(Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;)V
    .locals 0

    .prologue
    .line 1806826
    iput-object p1, p0, LX/Bfz;->a:Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/graphql/executor/GraphQLResult;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionMutationsModels$PlaceQuestionAnswerSubmitModel;",
            ">;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1806827
    if-eqz p1, :cond_0

    .line 1806828
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1806829
    if-eqz v0, :cond_0

    .line 1806830
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1806831
    check-cast v0, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionMutationsModels$PlaceQuestionAnswerSubmitModel;

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionMutationsModels$PlaceQuestionAnswerSubmitModel;->a()LX/0Px;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1806832
    :cond_0
    iget-object v0, p0, LX/Bfz;->a:Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;

    iget-object v0, v0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;->g:LX/03V;

    sget-object v1, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;->a:Ljava/lang/String;

    const-string v2, "Unexpected null result received from submit query"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1806833
    const/4 v0, 0x0

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1806834
    :goto_0
    return-object v0

    .line 1806835
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1806836
    check-cast v0, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionMutationsModels$PlaceQuestionAnswerSubmitModel;

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionMutationsModels$PlaceQuestionAnswerSubmitModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1

    .prologue
    .line 1806837
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    invoke-direct {p0, p1}, LX/Bfz;->a(Lcom/facebook/graphql/executor/GraphQLResult;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
