.class public final LX/BKX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field public final synthetic a:Lcom/facebook/platform/composer/composer/PlatformComposerStatusView;


# direct methods
.method public constructor <init>(Lcom/facebook/platform/composer/composer/PlatformComposerStatusView;)V
    .locals 0

    .prologue
    .line 1773859
    iput-object p1, p0, LX/BKX;->a:Lcom/facebook/platform/composer/composer/PlatformComposerStatusView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    .line 1773860
    iget-object v0, p0, LX/BKX;->a:Lcom/facebook/platform/composer/composer/PlatformComposerStatusView;

    iget-object v0, v0, Lcom/facebook/platform/composer/composer/PlatformComposerStatusView;->d:Lcom/facebook/platform/composer/composer/PlatformComposerEditText;

    invoke-virtual {v0}, Lcom/facebook/platform/composer/composer/PlatformComposerEditText;->getWidth()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    iget-object v2, p0, LX/BKX;->a:Lcom/facebook/platform/composer/composer/PlatformComposerStatusView;

    iget-object v2, v2, Lcom/facebook/platform/composer/composer/PlatformComposerStatusView;->d:Lcom/facebook/platform/composer/composer/PlatformComposerEditText;

    invoke-virtual {v2}, Lcom/facebook/platform/composer/composer/PlatformComposerEditText;->getHeight()I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v1, v2

    invoke-virtual {p2, v0, v1}, Landroid/view/MotionEvent;->setLocation(FF)V

    .line 1773861
    iget-object v0, p0, LX/BKX;->a:Lcom/facebook/platform/composer/composer/PlatformComposerStatusView;

    iget-object v0, v0, Lcom/facebook/platform/composer/composer/PlatformComposerStatusView;->d:Lcom/facebook/platform/composer/composer/PlatformComposerEditText;

    invoke-virtual {v0, p2}, Lcom/facebook/platform/composer/composer/PlatformComposerEditText;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 1773862
    const/4 v0, 0x0

    return v0
.end method
