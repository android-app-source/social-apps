.class public final LX/AsO;
.super LX/1a1;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# instance fields
.field public l:LX/As1;

.field public final synthetic m:Lcom/facebook/friendsharing/inspiration/controller/EffectsBottomTrayAdapter;


# direct methods
.method public constructor <init>(Lcom/facebook/friendsharing/inspiration/controller/EffectsBottomTrayAdapter;Lcom/facebook/drawee/fbpipeline/FbDraweeView;)V
    .locals 1

    .prologue
    .line 1720421
    iput-object p1, p0, LX/AsO;->m:Lcom/facebook/friendsharing/inspiration/controller/EffectsBottomTrayAdapter;

    .line 1720422
    invoke-direct {p0, p2}, LX/1a1;-><init>(Landroid/view/View;)V

    .line 1720423
    new-instance v0, LX/As1;

    invoke-direct {v0, p2}, LX/As1;-><init>(Lcom/facebook/drawee/fbpipeline/FbDraweeView;)V

    iput-object v0, p0, LX/AsO;->l:LX/As1;

    .line 1720424
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 12

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, 0x34d492a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1720425
    invoke-virtual {p0}, LX/1a1;->e()I

    move-result v1

    .line 1720426
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 1720427
    iget-object v1, p0, LX/AsO;->m:Lcom/facebook/friendsharing/inspiration/controller/EffectsBottomTrayAdapter;

    iget-object v1, v1, Lcom/facebook/friendsharing/inspiration/controller/EffectsBottomTrayAdapter;->j:LX/AsQ;

    invoke-virtual {p0}, LX/1a1;->e()I

    move-result v2

    .line 1720428
    iget-object v4, v1, LX/AsQ;->a:LX/AsR;

    const/4 v10, 0x1

    .line 1720429
    iput-boolean v10, v4, LX/AsR;->o:Z

    .line 1720430
    iget-object v5, v4, LX/AsR;->m:Ljava/lang/ref/WeakReference;

    invoke-virtual {v5}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v5

    invoke-static {v5}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0ik;

    move-object v6, v5

    .line 1720431
    check-cast v6, LX/0il;

    invoke-interface {v6}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/0io;

    check-cast v6, LX/0is;

    invoke-interface {v6}, LX/0is;->o()Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->areEffectsLoading()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1720432
    :cond_0
    :goto_0
    const v1, -0x4be97fbb

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    :cond_1
    move-object v6, v5

    .line 1720433
    check-cast v6, LX/0il;

    invoke-interface {v6}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/0io;

    check-cast v6, LX/0is;

    invoke-interface {v6}, LX/0is;->o()Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;

    move-result-object v6

    invoke-static {v6}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;)Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel$Builder;

    move-result-object v7

    .line 1720434
    invoke-virtual {v7, v10}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel$Builder;->setIsFromTray(Z)Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel$Builder;

    move-object v6, v5

    .line 1720435
    check-cast v6, LX/0il;

    invoke-interface {v6}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/0io;

    check-cast v6, LX/0is;

    invoke-interface {v6}, LX/0is;->o()Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;->getSelectedId()Ljava/lang/String;

    move-result-object v8

    .line 1720436
    iget-object v6, v4, LX/AsR;->d:Lcom/facebook/friendsharing/inspiration/controller/EffectsBottomTrayAdapter;

    .line 1720437
    iget-object v9, v6, Lcom/facebook/friendsharing/inspiration/controller/EffectsBottomTrayAdapter;->d:Ljava/util/List;

    if-nez v9, :cond_5

    .line 1720438
    const/4 v9, 0x0

    .line 1720439
    :goto_1
    move-object v6, v9

    .line 1720440
    invoke-static {v6}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;

    invoke-virtual {v6}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->getId()Ljava/lang/String;

    move-result-object v9

    .line 1720441
    invoke-virtual {v9, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1720442
    const-string v6, "1752514608329267"

    invoke-virtual {v7, v6}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel$Builder;->setSelectedId(Ljava/lang/String;)Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel$Builder;

    .line 1720443
    :cond_2
    :goto_2
    iget-object v6, v4, LX/AsR;->h:LX/ArT;

    sget-object v8, LX/ArJ;->CLICK_THUMBNAIL:LX/ArJ;

    invoke-virtual {v6, v8}, LX/ArT;->q(LX/ArJ;)V

    move-object v6, v5

    .line 1720444
    check-cast v6, LX/0im;

    invoke-interface {v6}, LX/0im;->c()LX/0jJ;

    move-result-object v6

    sget-object v8, LX/AsR;->a:LX/0jK;

    invoke-virtual {v6, v8}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v6

    check-cast v6, LX/0jL;

    invoke-virtual {v7}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/0jL;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/0jL;

    check-cast v6, LX/0jL;

    check-cast v5, LX/0il;

    invoke-interface {v5}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0io;

    check-cast v5, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v5}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v5

    invoke-static {v5}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationState;)Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;

    move-result-object v5

    invoke-virtual {v5, v10}, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->setHasChangedInspiration(Z)Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v5

    invoke-virtual {v6, v5}, LX/0jL;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationState;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0jL;

    invoke-virtual {v5}, LX/0jL;->a()V

    .line 1720445
    iget-object v5, v4, LX/AsR;->h:LX/ArT;

    sget-object v6, LX/ArJ;->CLICK_THUMBNAIL:LX/ArJ;

    invoke-virtual {v5, v6}, LX/ArT;->p(LX/ArJ;)V

    goto/16 :goto_0

    .line 1720446
    :cond_3
    invoke-virtual {v7, v9}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel$Builder;->setSelectedId(Ljava/lang/String;)Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel$Builder;

    .line 1720447
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    check-cast v6, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1720448
    invoke-static {v6}, LX/1ck;->a(Landroid/view/ViewGroup$MarginLayoutParams;)I

    move-result v8

    .line 1720449
    invoke-static {v6}, LX/1ck;->b(Landroid/view/ViewGroup$MarginLayoutParams;)I

    move-result v11

    .line 1720450
    iget-object v6, v4, LX/AsR;->f:LX/AuP;

    .line 1720451
    iget p0, v6, LX/AuP;->a:I

    move p0, p0

    .line 1720452
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    .line 1720453
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v6

    add-int/2addr v6, v1

    add-int/2addr v6, v11

    add-int/2addr v6, p0

    add-int/2addr v6, v8

    add-int/2addr v6, v1

    iget-object v2, v4, LX/AsR;->j:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v2}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->getMeasuredWidth()I

    move-result v2

    if-ge v6, v2, :cond_6

    const/4 v6, 0x1

    .line 1720454
    :goto_3
    if-nez v6, :cond_4

    .line 1720455
    iget-object v6, v4, LX/AsR;->j:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v6}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->getMeasuredWidth()I

    move-result v6

    add-int v2, v1, v11

    add-int/2addr v2, p0

    add-int/2addr v8, v2

    add-int/2addr v8, v1

    add-int/2addr v8, v11

    add-int/2addr v8, p0

    sub-int/2addr v6, v8

    .line 1720456
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v8

    sub-int v6, v8, v6

    .line 1720457
    iput v6, v4, LX/AsR;->n:I

    .line 1720458
    :cond_4
    iget-object v8, v4, LX/AsR;->g:LX/87V;

    move-object v6, v5

    check-cast v6, LX/0il;

    invoke-interface {v6}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/0is;

    invoke-virtual {v8, v6}, LX/87V;->b(LX/0is;)LX/0Px;

    move-result-object v6

    invoke-static {v6, v9}, LX/87V;->a(LX/0Px;Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1720459
    invoke-virtual {v7, v9}, Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel$Builder;->setStarId(Ljava/lang/String;)Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModel$Builder;

    goto/16 :goto_2

    :cond_5
    iget-object v9, v6, Lcom/facebook/friendsharing/inspiration/controller/EffectsBottomTrayAdapter;->d:Ljava/util/List;

    invoke-interface {v9, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;

    goto/16 :goto_1

    .line 1720460
    :cond_6
    const/4 v6, 0x0

    goto :goto_3
.end method
