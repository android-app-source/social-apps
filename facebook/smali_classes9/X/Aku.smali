.class public final LX/Aku;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic b:I

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:Lcom/facebook/graphql/model/GraphQLStory;

.field public final synthetic e:Landroid/content/Context;

.field public final synthetic f:LX/1wH;


# direct methods
.method public constructor <init>(LX/1wH;Lcom/facebook/feed/rows/core/props/FeedProps;ILjava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1709361
    iput-object p1, p0, LX/Aku;->f:LX/1wH;

    iput-object p2, p0, LX/Aku;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iput p3, p0, LX/Aku;->b:I

    iput-object p4, p0, LX/Aku;->c:Ljava/lang/String;

    iput-object p5, p0, LX/Aku;->d:Lcom/facebook/graphql/model/GraphQLStory;

    iput-object p6, p0, LX/Aku;->e:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 13

    .prologue
    const/4 v4, 0x1

    .line 1709362
    iget-object v0, p0, LX/Aku;->f:LX/1wH;

    iget-object v0, v0, LX/1wH;->a:LX/1SX;

    iget-object v1, p0, LX/Aku;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget v2, p0, LX/Aku;->b:I

    iget-object v3, p0, LX/Aku;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3, v4}, LX/1SX;->a(Lcom/facebook/feed/rows/core/props/FeedProps;ILjava/lang/String;Z)V

    .line 1709363
    iget-object v0, p0, LX/Aku;->f:LX/1wH;

    iget-object v1, p0, LX/Aku;->d:Lcom/facebook/graphql/model/GraphQLStory;

    iget-object v2, p0, LX/Aku;->e:Landroid/content/Context;

    .line 1709364
    iget-object v5, v0, LX/1wH;->a:LX/1SX;

    iget-object v5, v5, LX/1SX;->s:LX/0Or;

    invoke-interface {v5}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/BNQ;

    const/16 v7, 0x6df

    const-class v6, Landroid/app/Activity;

    invoke-static {v2, v6}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/app/Activity;

    .line 1709365
    sget-object v6, LX/21D;->PERMALINK:LX/21D;

    move-object v9, v6

    .line 1709366
    const-string v6, "native_review_permalink"

    move-object v10, v6

    .line 1709367
    iget-object v6, v0, LX/1wH;->a:LX/1SX;

    invoke-static {v6}, LX/1SX;->g(LX/1SX;)Ljava/lang/String;

    move-result-object v11

    const-string v12, "edit_menu"

    move-object v6, v1

    invoke-virtual/range {v5 .. v12}, LX/BNQ;->a(Lcom/facebook/graphql/model/GraphQLStory;ILandroid/app/Activity;LX/21D;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1709368
    iget-object v5, v0, LX/1wH;->a:LX/1SX;

    iget-object v5, v5, LX/1SX;->m:LX/0Or;

    invoke-interface {v5}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0kL;

    new-instance v6, LX/27k;

    const v7, 0x7f0814e8

    invoke-direct {v6, v7}, LX/27k;-><init>(I)V

    invoke-virtual {v5, v6}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 1709369
    :cond_0
    return v4
.end method
