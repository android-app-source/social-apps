.class public final LX/C4H;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/C4H;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/C4F;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/C4I;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1847063
    const/4 v0, 0x0

    sput-object v0, LX/C4H;->a:LX/C4H;

    .line 1847064
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/C4H;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1847060
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1847061
    new-instance v0, LX/C4I;

    invoke-direct {v0}, LX/C4I;-><init>()V

    iput-object v0, p0, LX/C4H;->c:LX/C4I;

    .line 1847062
    return-void
.end method

.method public static c(LX/1De;)LX/C4F;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1847052
    new-instance v1, LX/C4G;

    invoke-direct {v1}, LX/C4G;-><init>()V

    .line 1847053
    sget-object v2, LX/C4H;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/C4F;

    .line 1847054
    if-nez v2, :cond_0

    .line 1847055
    new-instance v2, LX/C4F;

    invoke-direct {v2}, LX/C4F;-><init>()V

    .line 1847056
    :cond_0
    invoke-static {v2, p0, v0, v0, v1}, LX/C4F;->a$redex0(LX/C4F;LX/1De;IILX/C4G;)V

    .line 1847057
    move-object v1, v2

    .line 1847058
    move-object v0, v1

    .line 1847059
    return-object v0
.end method

.method public static declared-synchronized q()LX/C4H;
    .locals 2

    .prologue
    .line 1847048
    const-class v1, LX/C4H;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/C4H;->a:LX/C4H;

    if-nez v0, :cond_0

    .line 1847049
    new-instance v0, LX/C4H;

    invoke-direct {v0}, LX/C4H;-><init>()V

    sput-object v0, LX/C4H;->a:LX/C4H;

    .line 1847050
    :cond_0
    sget-object v0, LX/C4H;->a:LX/C4H;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 1847051
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 3

    .prologue
    .line 1847039
    const/4 v2, 0x2

    const/4 v1, 0x1

    const/high16 p2, 0x3f000000    # 0.5f

    .line 1847040
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, v2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, v2}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, v1}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, v1}, LX/1Dh;->C(I)LX/1Dh;

    move-result-object v0

    const/16 v1, 0x8

    const v2, 0x7f0b0115

    invoke-interface {v0, v1, v2}, LX/1Dh;->x(II)LX/1Dh;

    move-result-object v0

    const/4 v1, -0x1

    invoke-interface {v0, v1}, LX/1Dh;->W(I)LX/1Dh;

    move-result-object v1

    .line 1847041
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, p2}, LX/1Dh;->e(F)LX/1Dh;

    move-result-object v0

    invoke-interface {v1, v0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 1847042
    const/4 v0, 0x0

    :goto_0
    const/16 v2, 0xa

    if-ge v0, v2, :cond_0

    .line 1847043
    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v2

    const p0, 0x7f0215f7

    invoke-virtual {v2, p0}, LX/1o5;->h(I)LX/1o5;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    const/high16 p0, 0x3f800000    # 1.0f

    invoke-interface {v2, p0}, LX/1Di;->b(F)LX/1Di;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 1847044
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1847045
    :cond_0
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, p2}, LX/1Dh;->e(F)LX/1Dh;

    move-result-object v0

    invoke-interface {v1, v0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 1847046
    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    move-object v0, v0

    .line 1847047
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1847037
    invoke-static {}, LX/1dS;->b()V

    .line 1847038
    const/4 v0, 0x0

    return-object v0
.end method
