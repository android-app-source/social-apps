.class public LX/AhW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0jq;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1703181
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1703182
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Landroid/support/v4/app/Fragment;
    .locals 4

    .prologue
    .line 1703183
    const-string v0, "profile_list_type"

    sget-object v1, LX/89l;->PROFILES:LX/89l;

    invoke-virtual {v1}, LX/89l;->ordinal()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 1703184
    invoke-static {}, LX/89l;->values()[LX/89l;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    aget-object v1, v1, v0

    .line 1703185
    const-string v0, "fragment_title"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1703186
    sget-object v0, LX/89l;->PROFILES:LX/89l;

    if-ne v1, v0, :cond_0

    .line 1703187
    new-instance v0, LX/8qQ;

    invoke-direct {v0}, LX/8qQ;-><init>()V

    .line 1703188
    iput-object v2, v0, LX/8qQ;->k:Ljava/lang/String;

    .line 1703189
    move-object v0, v0

    .line 1703190
    invoke-virtual {v0}, LX/8qQ;->a()Lcom/facebook/ufiservices/flyout/ProfileListParams;

    move-result-object v1

    .line 1703191
    new-instance v0, Lcom/facebook/feed/ui/PermalinkProfileListFragment;

    invoke-direct {v0}, Lcom/facebook/feed/ui/PermalinkProfileListFragment;-><init>()V

    .line 1703192
    invoke-virtual {v1}, Lcom/facebook/ufiservices/flyout/ProfileListParams;->m()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1703193
    :goto_0
    return-object v0

    .line 1703194
    :cond_0
    sget-object v0, LX/89l;->VOTERS_FOR_POLL_OPTION_ID:LX/89l;

    if-ne v1, v0, :cond_1

    .line 1703195
    const-string v0, "graphql_poll_option_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1703196
    :goto_1
    new-instance v3, LX/8qQ;

    invoke-direct {v3}, LX/8qQ;-><init>()V

    .line 1703197
    iput-object v0, v3, LX/8qQ;->a:Ljava/lang/String;

    .line 1703198
    move-object v0, v3

    .line 1703199
    iput-object v1, v0, LX/8qQ;->d:LX/89l;

    .line 1703200
    move-object v0, v0

    .line 1703201
    iput-object v2, v0, LX/8qQ;->k:Ljava/lang/String;

    .line 1703202
    move-object v0, v0

    .line 1703203
    invoke-virtual {v0}, LX/8qQ;->a()Lcom/facebook/ufiservices/flyout/ProfileListParams;

    move-result-object v1

    .line 1703204
    new-instance v0, Lcom/facebook/feed/ui/PermalinkProfileListFragment;

    invoke-direct {v0}, Lcom/facebook/feed/ui/PermalinkProfileListFragment;-><init>()V

    .line 1703205
    invoke-virtual {v1}, Lcom/facebook/ufiservices/flyout/ProfileListParams;->m()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    goto :goto_0

    .line 1703206
    :cond_1
    const-string v0, "graphql_feedback_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method
