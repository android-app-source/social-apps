.class public final LX/Aup;
.super Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;
.source ""


# instance fields
.field public final synthetic a:LX/Aur;


# direct methods
.method public constructor <init>(LX/Aur;)V
    .locals 0

    .prologue
    .line 1723256
    iput-object p1, p0, LX/Aup;->a:LX/Aur;

    invoke-direct {p0}, Landroid/view/ScaleGestureDetector$SimpleOnScaleGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method public final onScale(Landroid/view/ScaleGestureDetector;)Z
    .locals 10

    .prologue
    const/4 v0, 0x0

    const/high16 v8, 0x3f000000    # 0.5f

    .line 1723257
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    move-result v1

    float-to-double v2, v1

    .line 1723258
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    sub-double/2addr v4, v2

    invoke-static {v4, v5}, Ljava/lang/Math;->abs(D)D

    move-result-wide v4

    const-wide v6, 0x3f747ae147ae147bL    # 0.005

    cmpg-double v1, v4, v6

    if-gez v1, :cond_1

    .line 1723259
    :cond_0
    :goto_0
    return v0

    .line 1723260
    :cond_1
    const-wide v4, 0x3fe999999999999aL    # 0.8

    const-wide v6, 0x3ff3333333333333L    # 1.2

    invoke-static {v2, v3, v6, v7}, Ljava/lang/Math;->min(DD)D

    move-result-wide v2

    invoke-static {v4, v5, v2, v3}, Ljava/lang/Math;->max(DD)D

    move-result-wide v2

    .line 1723261
    iget-object v1, p0, LX/Aup;->a:LX/Aur;

    iget-wide v4, v1, LX/Aur;->k:D

    mul-double/2addr v4, v2

    const-wide/high16 v6, 0x4024000000000000L    # 10.0

    cmpl-double v1, v4, v6

    if-gtz v1, :cond_0

    .line 1723262
    iget-object v0, p0, LX/Aup;->a:LX/Aur;

    iget-object v1, p0, LX/Aup;->a:LX/Aur;

    iget-wide v4, v1, LX/Aur;->k:D

    mul-double/2addr v2, v4

    .line 1723263
    iput-wide v2, v0, LX/Aur;->k:D

    .line 1723264
    iget-object v0, p0, LX/Aup;->a:LX/Aur;

    iget-object v0, v0, LX/Aur;->b:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1723265
    int-to-double v2, v0

    iget-object v1, p0, LX/Aup;->a:LX/Aur;

    iget-wide v4, v1, LX/Aur;->i:D

    mul-double/2addr v2, v4

    double-to-int v1, v2

    .line 1723266
    iget-object v2, p0, LX/Aup;->a:LX/Aur;

    iget v2, v2, LX/Aur;->s:I

    int-to-double v2, v2

    iget-object v4, p0, LX/Aup;->a:LX/Aur;

    iget-wide v4, v4, LX/Aur;->k:D

    mul-double/2addr v2, v4

    double-to-int v2, v2

    .line 1723267
    int-to-double v4, v2

    iget-object v3, p0, LX/Aup;->a:LX/Aur;

    iget-wide v6, v3, LX/Aur;->i:D

    mul-double/2addr v4, v6

    double-to-int v3, v4

    .line 1723268
    sub-int v0, v2, v0

    int-to-float v0, v0

    mul-float/2addr v0, v8

    .line 1723269
    sub-int v1, v3, v1

    int-to-float v1, v1

    mul-float/2addr v1, v8

    .line 1723270
    iget-object v4, p0, LX/Aup;->a:LX/Aur;

    iget-object v4, v4, LX/Aur;->b:Landroid/widget/ImageView;

    iget-object v5, p0, LX/Aup;->a:LX/Aur;

    iget-object v5, v5, LX/Aur;->b:Landroid/widget/ImageView;

    invoke-virtual {v5}, Landroid/widget/ImageView;->getTranslationX()F

    move-result v5

    sub-float v1, v5, v1

    invoke-virtual {v4, v1}, Landroid/widget/ImageView;->setTranslationX(F)V

    .line 1723271
    iget-object v1, p0, LX/Aup;->a:LX/Aur;

    iget-object v1, v1, LX/Aur;->b:Landroid/widget/ImageView;

    iget-object v4, p0, LX/Aup;->a:LX/Aur;

    iget-object v4, v4, LX/Aur;->b:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getTranslationY()F

    move-result v4

    sub-float v0, v4, v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setTranslationY(F)V

    .line 1723272
    iget-object v0, p0, LX/Aup;->a:LX/Aur;

    iget-object v0, v0, LX/Aur;->b:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1723273
    iget-object v0, p0, LX/Aup;->a:LX/Aur;

    iget-object v0, v0, LX/Aur;->b:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput v3, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1723274
    iget-object v0, p0, LX/Aup;->a:LX/Aur;

    iget-object v0, v0, LX/Aur;->b:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->requestLayout()V

    .line 1723275
    const/4 v0, 0x1

    goto/16 :goto_0
.end method

.method public final onScaleBegin(Landroid/view/ScaleGestureDetector;)Z
    .locals 1

    .prologue
    .line 1723276
    const/4 v0, 0x1

    return v0
.end method
