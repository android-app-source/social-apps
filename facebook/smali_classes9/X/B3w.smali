.class public LX/B3w;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1741053
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/heisman/protocol/ProfileOverlayCategoryGraphQLModels$ProfileOverlayCategoryPageFieldsModel;)Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;
    .locals 3

    .prologue
    .line 1741054
    new-instance v1, LX/B5x;

    invoke-virtual {p0}, Lcom/facebook/heisman/protocol/ProfileOverlayCategoryGraphQLModels$ProfileOverlayCategoryPageFieldsModel;->d()Lcom/facebook/heisman/protocol/ProfileOverlayCategoryGraphQLModels$PageProfilePictureOverlaysConnectionFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/heisman/protocol/ProfileOverlayCategoryGraphQLModels$PageProfilePictureOverlaysConnectionFieldsModel;->a()LX/0Px;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5QV;

    invoke-static {v0}, Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;->a(LX/5QV;)Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/heisman/protocol/ProfileOverlayCategoryGraphQLModels$ProfileOverlayCategoryPageFieldsModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v0, v2}, LX/B5x;-><init>(Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;Ljava/lang/String;)V

    .line 1741055
    invoke-virtual {p0}, Lcom/facebook/heisman/protocol/ProfileOverlayCategoryGraphQLModels$ProfileOverlayCategoryPageFieldsModel;->c()Lcom/facebook/heisman/protocol/ProfileOverlayCategoryGraphQLModels$ProfileOverlayCategoryPageFieldsModel$PageLogoModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1741056
    invoke-virtual {p0}, Lcom/facebook/heisman/protocol/ProfileOverlayCategoryGraphQLModels$ProfileOverlayCategoryPageFieldsModel;->c()Lcom/facebook/heisman/protocol/ProfileOverlayCategoryGraphQLModels$ProfileOverlayCategoryPageFieldsModel$PageLogoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/heisman/protocol/ProfileOverlayCategoryGraphQLModels$ProfileOverlayCategoryPageFieldsModel$PageLogoModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 1741057
    iput-object v0, v1, LX/B5x;->e:Ljava/lang/String;

    .line 1741058
    move-object v0, v1

    .line 1741059
    invoke-virtual {v0}, LX/B5x;->a()Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;

    move-result-object v0

    .line 1741060
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v1}, LX/B5x;->a()Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;

    move-result-object v0

    goto :goto_0
.end method
