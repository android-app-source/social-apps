.class public LX/CDd;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1859882
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/CDg;Lcom/facebook/video/analytics/VideoFeedStoryInfo;LX/04g;)V
    .locals 1

    .prologue
    .line 1859883
    iput-object p2, p1, Lcom/facebook/video/analytics/VideoFeedStoryInfo;->b:LX/04g;

    .line 1859884
    iget-object v0, p0, LX/CDg;->d:LX/2oL;

    invoke-virtual {v0, p2}, LX/2oL;->a(LX/04g;)V

    .line 1859885
    return-void
.end method

.method public static a(Lcom/facebook/video/player/RichVideoPlayer;LX/CDg;Lcom/facebook/video/analytics/VideoFeedStoryInfo;LX/198;LX/04g;LX/7K4;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 1859886
    iget-boolean v1, p1, LX/CDg;->e:Z

    if-nez v1, :cond_0

    .line 1859887
    iput-boolean v0, p1, LX/CDg;->e:Z

    .line 1859888
    invoke-static {}, LX/5Mr;->c()LX/5Mr;

    move-result-object v1

    invoke-virtual {p3, v1}, LX/198;->a(LX/1YD;)V

    .line 1859889
    :cond_0
    iget-object v1, p1, LX/CDg;->d:LX/2oL;

    invoke-virtual {v1}, LX/2oL;->c()LX/04g;

    move-result-object v1

    sget-object v2, LX/04g;->UNSET:LX/04g;

    invoke-virtual {v1, v2}, LX/04g;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object v1, LX/04g;->BY_AUTOPLAY:LX/04g;

    invoke-virtual {v1, p4}, LX/04g;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    sget-object v1, LX/04g;->BY_USER:LX/04g;

    invoke-virtual {v1, p4}, LX/04g;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1859890
    :cond_1
    invoke-static {p1, p2, p4}, LX/CDd;->a(LX/CDg;Lcom/facebook/video/analytics/VideoFeedStoryInfo;LX/04g;)V

    .line 1859891
    :cond_2
    if-eqz p2, :cond_3

    .line 1859892
    iget-object v1, p2, Lcom/facebook/video/analytics/VideoFeedStoryInfo;->b:LX/04g;

    move-object v1, v1

    .line 1859893
    invoke-virtual {p0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->setOriginalPlayReason(LX/04g;)V

    .line 1859894
    iget-object v1, p2, Lcom/facebook/video/analytics/VideoFeedStoryInfo;->c:LX/04H;

    move-object v1, v1

    .line 1859895
    invoke-virtual {p0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->setChannelEligibility(LX/04H;)V

    .line 1859896
    :cond_3
    iget-boolean v1, p1, LX/CDg;->f:Z

    if-nez v1, :cond_4

    iget-object v1, p1, LX/CDg;->g:LX/0iX;

    iget-object v2, p1, LX/CDg;->h:LX/04D;

    .line 1859897
    iget-object v3, p0, Lcom/facebook/video/player/RichVideoPlayer;->D:LX/2pa;

    move-object v3, v3

    .line 1859898
    invoke-virtual {v1, v2, v3}, LX/0iX;->a(LX/04D;LX/2pa;)Z

    move-result v1

    if-nez v1, :cond_4

    :goto_0
    sget-object v1, LX/04g;->BY_PLAYER:LX/04g;

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->a(ZLX/04g;)V

    .line 1859899
    iget v0, p5, LX/7K4;->c:I

    invoke-virtual {p0, v0, p4}, Lcom/facebook/video/player/RichVideoPlayer;->a(ILX/04g;)V

    .line 1859900
    iget v0, p5, LX/7K4;->d:I

    invoke-virtual {p0, p4, v0}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/04g;I)V

    .line 1859901
    return-void

    .line 1859902
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method
