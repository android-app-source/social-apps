.class public LX/CPx;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1885137
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    const-string v1, "NT:BOX_CHILD"

    const-string v2, "children"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "NT:STACK_LAYOUT_ABSOLUTE_CHILD"

    const-string v2, "children"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "NT:RATIO_LAYOUT"

    const-string v2, "children"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    sput-object v0, LX/CPx;->a:LX/0P1;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1885138
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/1Di;LX/1De;LX/CNb;Ljava/util/List;)LX/1Dg;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Di;",
            "LX/1De;",
            "LX/CNb;",
            "Ljava/util/List",
            "<",
            "LX/CNb;",
            ">;)",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    .line 1885139
    const-string v0, "visibility"

    const-string v1, ""

    invoke-virtual {p2, v0, v1}, LX/CNb;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "NONE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1885140
    const/4 v0, 0x0

    .line 1885141
    :goto_0
    return-object v0

    .line 1885142
    :cond_0
    invoke-static {p2}, LX/3jU;->a(LX/CNb;)LX/3j9;

    move-result-object v0

    .line 1885143
    instance-of v1, v0, LX/3jE;

    if-eqz v1, :cond_6

    .line 1885144
    const-string v1, "height-value"

    invoke-virtual {p2, v1}, LX/CNb;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1885145
    const-string v1, "height-value"

    invoke-virtual {p2, v1, p1}, LX/CNb;->a(Ljava/lang/String;Landroid/content/Context;)I

    move-result v1

    invoke-interface {p0, v1}, LX/1Di;->o(I)LX/1Di;

    .line 1885146
    :cond_1
    const-string v1, "width-value"

    invoke-virtual {p2, v1}, LX/CNb;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1885147
    const-string v1, "width-value"

    invoke-virtual {p2, v1, p1}, LX/CNb;->a(Ljava/lang/String;Landroid/content/Context;)I

    move-result v1

    invoke-interface {p0, v1}, LX/1Di;->g(I)LX/1Di;

    .line 1885148
    :cond_2
    const-string v1, "min-height-value"

    invoke-virtual {p2, v1}, LX/CNb;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1885149
    const-string v1, "min-height-value"

    invoke-virtual {p2, v1, p1}, LX/CNb;->a(Ljava/lang/String;Landroid/content/Context;)I

    move-result v1

    invoke-interface {p0, v1}, LX/1Di;->s(I)LX/1Di;

    .line 1885150
    :cond_3
    const-string v1, "min-width-value"

    invoke-virtual {p2, v1}, LX/CNb;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1885151
    const-string v1, "min-width-value"

    invoke-virtual {p2, v1, p1}, LX/CNb;->a(Ljava/lang/String;Landroid/content/Context;)I

    move-result v1

    invoke-interface {p0, v1}, LX/1Di;->k(I)LX/1Di;

    .line 1885152
    :cond_4
    const-string v1, "max-height-value"

    invoke-virtual {p2, v1}, LX/CNb;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1885153
    const-string v1, "max-height-value"

    invoke-virtual {p2, v1, p1}, LX/CNb;->a(Ljava/lang/String;Landroid/content/Context;)I

    move-result v1

    invoke-interface {p0, v1}, LX/1Di;->u(I)LX/1Di;

    .line 1885154
    :cond_5
    const-string v1, "max-width-value"

    invoke-virtual {p2, v1}, LX/CNb;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1885155
    const-string v1, "max-width-value"

    invoke-virtual {p2, v1, p1}, LX/CNb;->a(Ljava/lang/String;Landroid/content/Context;)I

    move-result v1

    invoke-interface {p0, v1}, LX/1Di;->l(I)LX/1Di;

    .line 1885156
    :cond_6
    instance-of v0, v0, LX/3jB;

    if-eqz v0, :cond_8

    .line 1885157
    const-string v0, "interaction-enabled"

    const/4 v1, 0x1

    invoke-virtual {p2, v0, v1}, LX/CNb;->a(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_7

    .line 1885158
    const/4 v0, 0x0

    invoke-interface {p0, v0}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    .line 1885159
    :cond_7
    const-string v0, "clips-to-bounds"

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, LX/CNb;->a(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1885160
    invoke-interface {p0}, LX/1Di;->j()LX/1Di;

    .line 1885161
    :cond_8
    const-string v0, "FB:BORDER"

    invoke-static {p3, v0}, LX/CPx;->a(Ljava/util/List;Ljava/lang/String;)LX/CNb;

    move-result-object v1

    .line 1885162
    invoke-static {p2}, LX/CPx;->a(LX/CNb;)Ljava/lang/Integer;

    move-result-object v2

    .line 1885163
    if-nez v1, :cond_f

    if-nez v2, :cond_f

    .line 1885164
    const/4 v0, 0x0

    .line 1885165
    :cond_9
    :goto_1
    move-object v0, v0

    .line 1885166
    if-eqz v0, :cond_a

    .line 1885167
    invoke-interface {p0, v0}, LX/1Di;->a(LX/1n6;)LX/1Di;

    .line 1885168
    :cond_a
    const/4 v6, 0x0

    const/high16 v5, 0x3f800000    # 1.0f

    .line 1885169
    const-string v0, "NT:TOUCH_HIGHLIGHT"

    invoke-static {p3, v0}, LX/CPx;->a(Ljava/util/List;Ljava/lang/String;)LX/CNb;

    move-result-object v1

    .line 1885170
    const-string v0, "FB:BORDER"

    invoke-static {p3, v0}, LX/CPx;->a(Ljava/util/List;Ljava/lang/String;)LX/CNb;

    move-result-object v2

    .line 1885171
    if-nez v1, :cond_13

    if-eqz v2, :cond_b

    const-string v0, "background-color"

    invoke-virtual {v2, v0}, LX/CNb;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_13

    .line 1885172
    :cond_b
    const/4 v0, 0x0

    .line 1885173
    :cond_c
    :goto_2
    move-object v0, v0

    .line 1885174
    if-eqz v0, :cond_d

    .line 1885175
    invoke-interface {p0, v0}, LX/1Di;->b(LX/1n6;)LX/1Di;

    .line 1885176
    :cond_d
    const/4 v0, 0x0

    .line 1885177
    const-string v1, "accessibility-label"

    invoke-virtual {p2, v1, v0}, LX/CNb;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1885178
    const-string v2, "accessibility-hint"

    invoke-virtual {p2, v2, v0}, LX/CNb;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1885179
    if-nez v1, :cond_16

    if-nez v2, :cond_16

    .line 1885180
    :goto_3
    move-object v0, v0

    .line 1885181
    if-eqz v0, :cond_e

    .line 1885182
    invoke-interface {p0, v0}, LX/1Di;->a(Ljava/lang/CharSequence;)LX/1Di;

    .line 1885183
    :cond_e
    invoke-interface {p0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    goto/16 :goto_0

    .line 1885184
    :cond_f
    new-instance v0, LX/CQ0;

    invoke-direct {v0}, LX/CQ0;-><init>()V

    .line 1885185
    sget-object v3, LX/CQ1;->b:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/CPz;

    .line 1885186
    if-nez v3, :cond_10

    .line 1885187
    new-instance v3, LX/CPz;

    invoke-direct {v3}, LX/CPz;-><init>()V

    .line 1885188
    :cond_10
    invoke-static {v3, p1, v0}, LX/CPz;->a$redex0(LX/CPz;LX/1De;LX/CQ0;)V

    .line 1885189
    move-object v0, v3

    .line 1885190
    move-object v0, v0

    .line 1885191
    if-eqz v1, :cond_11

    .line 1885192
    const-string v3, "border-width"

    invoke-virtual {v1, v3, p1}, LX/CNb;->a(Ljava/lang/String;Landroid/content/Context;)I

    move-result v3

    .line 1885193
    iget-object v4, v0, LX/CPz;->a:LX/CQ0;

    iput v3, v4, LX/CQ0;->c:I

    .line 1885194
    move-object v4, v0

    .line 1885195
    const-string v5, "border-color"

    const/high16 v6, -0x1000000

    invoke-virtual {v1, v5, v6}, LX/CNb;->b(Ljava/lang/String;I)I

    move-result v5

    .line 1885196
    iget-object v6, v4, LX/CPz;->a:LX/CQ0;

    iput v5, v6, LX/CQ0;->b:I

    .line 1885197
    const/16 v4, 0x8

    invoke-interface {p0, v4, v3}, LX/1Di;->i(II)LX/1Di;

    .line 1885198
    :cond_11
    if-eqz v1, :cond_12

    const-string v3, "background-color"

    invoke-virtual {v1, v3}, LX/CNb;->c(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_12

    .line 1885199
    const-string v3, "border-radius"

    invoke-virtual {v1, v3, p1}, LX/CNb;->a(Ljava/lang/String;Landroid/content/Context;)I

    move-result v1

    .line 1885200
    iget-object v3, v0, LX/CPz;->a:LX/CQ0;

    iput v1, v3, LX/CQ0;->d:I

    .line 1885201
    :cond_12
    if-eqz v2, :cond_9

    .line 1885202
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 1885203
    iget-object v2, v0, LX/CPz;->a:LX/CQ0;

    iput v1, v2, LX/CQ0;->a:I

    .line 1885204
    goto/16 :goto_1

    .line 1885205
    :cond_13
    new-instance v0, LX/CQ4;

    invoke-direct {v0}, LX/CQ4;-><init>()V

    .line 1885206
    sget-object v3, LX/CQ5;->b:LX/0Zi;

    invoke-virtual {v3}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/CQ3;

    .line 1885207
    if-nez v3, :cond_14

    .line 1885208
    new-instance v3, LX/CQ3;

    invoke-direct {v3}, LX/CQ3;-><init>()V

    .line 1885209
    :cond_14
    invoke-static {v3, p1, v0}, LX/CQ3;->a$redex0(LX/CQ3;LX/1De;LX/CQ4;)V

    .line 1885210
    move-object v0, v3

    .line 1885211
    move-object v0, v0

    .line 1885212
    if-eqz v1, :cond_15

    .line 1885213
    const-string v3, "underlay-color"

    invoke-virtual {v1, v3, v6}, LX/CNb;->b(Ljava/lang/String;I)I

    move-result v3

    const-string v4, "active-opacity"

    invoke-virtual {v1, v4, v5}, LX/CNb;->a(Ljava/lang/String;F)F

    move-result v1

    sub-float v1, v5, v1

    .line 1885214
    const/4 v4, 0x3

    new-array v4, v4, [F

    .line 1885215
    invoke-static {v3, v4}, Landroid/graphics/Color;->colorToHSV(I[F)V

    .line 1885216
    const/high16 v5, 0x437f0000    # 255.0f

    mul-float/2addr v5, v1

    float-to-int v5, v5

    invoke-static {v5, v4}, Landroid/graphics/Color;->HSVToColor(I[F)I

    move-result v4

    move v1, v4

    .line 1885217
    iget-object v3, v0, LX/CQ3;->a:LX/CQ4;

    iput v1, v3, LX/CQ4;->a:I

    .line 1885218
    :cond_15
    if-eqz v2, :cond_c

    const-string v1, "background-color"

    invoke-virtual {v2, v1}, LX/CNb;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 1885219
    const-string v1, "border-radius"

    invoke-virtual {v2, v1, p1}, LX/CNb;->a(Ljava/lang/String;Landroid/content/Context;)I

    move-result v1

    .line 1885220
    iget-object v3, v0, LX/CQ3;->a:LX/CQ4;

    iput v1, v3, LX/CQ4;->b:I

    .line 1885221
    move-object v1, v0

    .line 1885222
    const-string v3, "background-color"

    invoke-virtual {v2, v3, v6}, LX/CNb;->b(Ljava/lang/String;I)I

    move-result v2

    .line 1885223
    iget-object v3, v1, LX/CQ3;->a:LX/CQ4;

    iput v2, v3, LX/CQ4;->c:I

    .line 1885224
    goto/16 :goto_2

    .line 1885225
    :cond_16
    if-eqz v1, :cond_17

    if-nez v2, :cond_17

    move-object v0, v1

    .line 1885226
    goto/16 :goto_3

    .line 1885227
    :cond_17
    if-nez v1, :cond_18

    move-object v0, v2

    .line 1885228
    goto/16 :goto_3

    .line 1885229
    :cond_18
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f082994

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    const/4 v1, 0x1

    aput-object v2, v4, v1

    invoke-virtual {v0, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_3
.end method

.method public static a(Ljava/util/List;Ljava/lang/String;)LX/CNb;
    .locals 3
    .param p0    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/CNb;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "LX/CNb;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1885230
    if-nez p0, :cond_0

    move-object v0, v2

    .line 1885231
    :goto_0
    return-object v0

    .line 1885232
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 1885233
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CNb;

    invoke-virtual {v0}, LX/CNb;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1885234
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CNb;

    goto :goto_0

    .line 1885235
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_2
    move-object v0, v2

    .line 1885236
    goto :goto_0
.end method

.method public static a(LX/CNc;LX/CNb;Ljava/util/List;Ljava/lang/String;)LX/CNe;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CNc;",
            "LX/CNb;",
            "Ljava/util/List",
            "<",
            "LX/CNb;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "LX/CNe;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1885237
    const-string v1, "NT:TOUCH_HIGHLIGHT"

    invoke-static {p2, v1}, LX/CPx;->a(Ljava/util/List;Ljava/lang/String;)LX/CNb;

    move-result-object v2

    .line 1885238
    if-eqz v2, :cond_1

    invoke-virtual {v2, p3}, LX/CNb;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1885239
    invoke-virtual {v2, p3}, LX/CNb;->b(Ljava/lang/String;)LX/CNb;

    move-result-object v1

    .line 1885240
    :goto_0
    if-eqz v1, :cond_3

    .line 1885241
    invoke-static {v1, p0}, LX/CNd;->a(LX/CNb;LX/CNc;)LX/CNe;

    move-result-object v0

    .line 1885242
    :cond_0
    :goto_1
    return-object v0

    .line 1885243
    :cond_1
    invoke-virtual {p1, p3}, LX/CNb;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1885244
    invoke-virtual {p1, p3}, LX/CNb;->b(Ljava/lang/String;)LX/CNb;

    move-result-object v1

    goto :goto_0

    :cond_2
    move-object v1, v0

    .line 1885245
    goto :goto_0

    .line 1885246
    :cond_3
    if-eqz v2, :cond_0

    .line 1885247
    new-instance v0, LX/CPw;

    invoke-direct {v0}, LX/CPw;-><init>()V

    goto :goto_1
.end method

.method public static a(LX/CNb;)Ljava/lang/Integer;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1885248
    invoke-static {p0}, LX/3jU;->a(LX/CNb;)LX/3j9;

    move-result-object v0

    instance-of v0, v0, LX/3jB;

    if-eqz v0, :cond_1

    const-string v0, "background-color"

    invoke-virtual {p0, v0}, LX/CNb;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1885249
    const-string v0, "background-color"

    invoke-virtual {p0, v0, v1}, LX/CNb;->b(Ljava/lang/String;I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 1885250
    :cond_0
    :goto_0
    return-object v0

    .line 1885251
    :cond_1
    sget-object v0, LX/CPx;->a:LX/0P1;

    invoke-virtual {p0}, LX/CNb;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1885252
    invoke-virtual {p0, v0}, LX/CNb;->a(Ljava/lang/String;)LX/0Px;

    move-result-object v2

    .line 1885253
    if-eqz v2, :cond_2

    .line 1885254
    :goto_1
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 1885255
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CNb;

    invoke-static {v0}, LX/CPx;->a(LX/CNb;)Ljava/lang/Integer;

    move-result-object v0

    .line 1885256
    if-nez v0, :cond_0

    .line 1885257
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1885258
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
