.class public LX/Bc4;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Z

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Bc2;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(ZLX/0Ot;)V
    .locals 0
    .param p1    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "LX/0Ot",
            "<",
            "LX/Bc2;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1801484
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1801485
    iput-boolean p1, p0, LX/Bc4;->a:Z

    .line 1801486
    iput-object p2, p0, LX/Bc4;->b:LX/0Ot;

    .line 1801487
    return-void
.end method

.method public static b(LX/Bc4;)V
    .locals 2

    .prologue
    .line 1801488
    iget-boolean v0, p0, LX/Bc4;->a:Z

    move v0, v0

    .line 1801489
    const-string v1, "Not able to show, call canShowNux before calling"

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1801490
    return-void
.end method
