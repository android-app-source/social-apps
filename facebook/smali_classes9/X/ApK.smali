.class public final LX/ApK;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/ApL;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/fig/components/attachment/footercomponents/FigAttachmentFooterMediaComponent$FigAttachmentFooterMediaComponentImpl;

.field private b:[Ljava/lang/String;

.field private c:I

.field private d:Ljava/util/BitSet;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1715808
    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 1715809
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "type"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/ApK;->b:[Ljava/lang/String;

    .line 1715810
    iput v3, p0, LX/ApK;->c:I

    .line 1715811
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/ApK;->c:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/ApK;->d:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/ApK;LX/1De;IILcom/facebook/fig/components/attachment/footercomponents/FigAttachmentFooterMediaComponent$FigAttachmentFooterMediaComponentImpl;)V
    .locals 1

    .prologue
    .line 1715812
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 1715813
    iput-object p4, p0, LX/ApK;->a:Lcom/facebook/fig/components/attachment/footercomponents/FigAttachmentFooterMediaComponent$FigAttachmentFooterMediaComponentImpl;

    .line 1715814
    iget-object v0, p0, LX/ApK;->d:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 1715815
    return-void
.end method


# virtual methods
.method public final a(Landroid/net/Uri;)LX/ApK;
    .locals 1

    .prologue
    .line 1715816
    iget-object v0, p0, LX/ApK;->a:Lcom/facebook/fig/components/attachment/footercomponents/FigAttachmentFooterMediaComponent$FigAttachmentFooterMediaComponentImpl;

    iput-object p1, v0, Lcom/facebook/fig/components/attachment/footercomponents/FigAttachmentFooterMediaComponent$FigAttachmentFooterMediaComponentImpl;->b:Landroid/net/Uri;

    .line 1715817
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 1715818
    invoke-super {p0}, LX/1X5;->a()V

    .line 1715819
    const/4 v0, 0x0

    iput-object v0, p0, LX/ApK;->a:Lcom/facebook/fig/components/attachment/footercomponents/FigAttachmentFooterMediaComponent$FigAttachmentFooterMediaComponentImpl;

    .line 1715820
    sget-object v0, LX/ApL;->a:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 1715821
    return-void
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/ApL;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1715822
    iget-object v1, p0, LX/ApK;->d:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/ApK;->d:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/ApK;->c:I

    if-ge v1, v2, :cond_2

    .line 1715823
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1715824
    :goto_0
    iget v2, p0, LX/ApK;->c:I

    if-ge v0, v2, :cond_1

    .line 1715825
    iget-object v2, p0, LX/ApK;->d:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1715826
    iget-object v2, p0, LX/ApK;->b:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1715827
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1715828
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1715829
    :cond_2
    iget-object v0, p0, LX/ApK;->a:Lcom/facebook/fig/components/attachment/footercomponents/FigAttachmentFooterMediaComponent$FigAttachmentFooterMediaComponentImpl;

    .line 1715830
    invoke-virtual {p0}, LX/ApK;->a()V

    .line 1715831
    return-object v0
.end method

.method public final h(I)LX/ApK;
    .locals 2
    .param p1    # I
        .annotation build Lcom/facebook/fig/components/attachment/annotations/FigAttachmentFooterMediaType;
        .end annotation
    .end param

    .prologue
    .line 1715832
    iget-object v0, p0, LX/ApK;->a:Lcom/facebook/fig/components/attachment/footercomponents/FigAttachmentFooterMediaComponent$FigAttachmentFooterMediaComponentImpl;

    iput p1, v0, Lcom/facebook/fig/components/attachment/footercomponents/FigAttachmentFooterMediaComponent$FigAttachmentFooterMediaComponentImpl;->a:I

    .line 1715833
    iget-object v0, p0, LX/ApK;->d:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1715834
    return-object p0
.end method
