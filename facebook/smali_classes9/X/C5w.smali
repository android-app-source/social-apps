.class public final enum LX/C5w;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/C5w;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/C5w;

.field public static final enum DISABLED:LX/C5w;

.field public static final enum ENABLED:LX/C5w;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1849475
    new-instance v0, LX/C5w;

    const-string v1, "ENABLED"

    invoke-direct {v0, v1, v2}, LX/C5w;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/C5w;->ENABLED:LX/C5w;

    .line 1849476
    new-instance v0, LX/C5w;

    const-string v1, "DISABLED"

    invoke-direct {v0, v1, v3}, LX/C5w;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/C5w;->DISABLED:LX/C5w;

    .line 1849477
    const/4 v0, 0x2

    new-array v0, v0, [LX/C5w;

    sget-object v1, LX/C5w;->ENABLED:LX/C5w;

    aput-object v1, v0, v2

    sget-object v1, LX/C5w;->DISABLED:LX/C5w;

    aput-object v1, v0, v3

    sput-object v0, LX/C5w;->$VALUES:[LX/C5w;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1849478
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/C5w;
    .locals 1

    .prologue
    .line 1849479
    const-class v0, LX/C5w;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/C5w;

    return-object v0
.end method

.method public static values()[LX/C5w;
    .locals 1

    .prologue
    .line 1849480
    sget-object v0, LX/C5w;->$VALUES:[LX/C5w;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/C5w;

    return-object v0
.end method
