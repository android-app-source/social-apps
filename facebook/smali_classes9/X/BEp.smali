.class public final enum LX/BEp;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/BEp;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/BEp;

.field public static final enum PAYPAL:LX/BEp;

.field public static final enum STRIPE:LX/BEp;

.field public static final enum UNKNOWN:LX/BEp;


# instance fields
.field private final mInformServerToPoll:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1765078
    new-instance v0, LX/BEp;

    const-string v1, "PAYPAL"

    invoke-direct {v0, v1, v2, v3}, LX/BEp;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/BEp;->PAYPAL:LX/BEp;

    .line 1765079
    new-instance v0, LX/BEp;

    const-string v1, "STRIPE"

    invoke-direct {v0, v1, v3, v2}, LX/BEp;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/BEp;->STRIPE:LX/BEp;

    .line 1765080
    new-instance v0, LX/BEp;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v4, v2}, LX/BEp;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, LX/BEp;->UNKNOWN:LX/BEp;

    .line 1765081
    const/4 v0, 0x3

    new-array v0, v0, [LX/BEp;

    sget-object v1, LX/BEp;->PAYPAL:LX/BEp;

    aput-object v1, v0, v2

    sget-object v1, LX/BEp;->STRIPE:LX/BEp;

    aput-object v1, v0, v3

    sget-object v1, LX/BEp;->UNKNOWN:LX/BEp;

    aput-object v1, v0, v4

    sput-object v0, LX/BEp;->$VALUES:[LX/BEp;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)V"
        }
    .end annotation

    .prologue
    .line 1765082
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1765083
    iput-boolean p3, p0, LX/BEp;->mInformServerToPoll:Z

    .line 1765084
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/BEp;
    .locals 1

    .prologue
    .line 1765085
    const-class v0, LX/BEp;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/BEp;

    return-object v0
.end method

.method public static values()[LX/BEp;
    .locals 1

    .prologue
    .line 1765086
    sget-object v0, LX/BEp;->$VALUES:[LX/BEp;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/BEp;

    return-object v0
.end method


# virtual methods
.method public final informServerToPoll()Z
    .locals 1

    .prologue
    .line 1765087
    iget-boolean v0, p0, LX/BEp;->mInformServerToPoll:Z

    return v0
.end method
