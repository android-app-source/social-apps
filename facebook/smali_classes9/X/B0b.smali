.class public final LX/B0b;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0rl;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0rl",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/B0d;

.field public final synthetic b:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field public final synthetic c:LX/B0M;

.field public final synthetic d:LX/B0c;

.field private e:LX/B0d;


# direct methods
.method public constructor <init>(LX/B0c;LX/B0d;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/B0M;)V
    .locals 1

    .prologue
    .line 1734051
    iput-object p1, p0, LX/B0b;->d:LX/B0c;

    iput-object p2, p0, LX/B0b;->a:LX/B0d;

    iput-object p3, p0, LX/B0b;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iput-object p4, p0, LX/B0b;->c:LX/B0M;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1734052
    iget-object v0, p0, LX/B0b;->a:LX/B0d;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/B0d;

    iput-object v0, p0, LX/B0b;->e:LX/B0d;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1734053
    iget-object v0, p0, LX/B0b;->c:LX/B0M;

    invoke-virtual {v0}, LX/B0M;->b()V

    .line 1734054
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1734055
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1734056
    if-eqz p1, :cond_0

    .line 1734057
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1734058
    if-nez v0, :cond_1

    .line 1734059
    :cond_0
    const-class v0, LX/B0c;

    const-string v1, "Null response from network"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 1734060
    :goto_0
    return-void

    .line 1734061
    :cond_1
    iget-object v0, p0, LX/B0b;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget-object v1, p0, LX/B0b;->d:LX/B0c;

    iget-object v1, v1, LX/B0c;->a:LX/B0V;

    iget-object v2, p0, LX/B0b;->e:LX/B0d;

    invoke-static {v0, v1, v2, p1}, LX/B0W;->a(Lcom/facebook/quicklog/QuickPerformanceLogger;LX/B0V;LX/B0d;Lcom/facebook/graphql/executor/GraphQLResult;)LX/B0N;

    move-result-object v1

    .line 1734062
    invoke-interface {v1}, LX/B0N;->d()LX/B0d;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/B0d;

    iput-object v0, p0, LX/B0b;->e:LX/B0d;

    .line 1734063
    iget-object v0, p0, LX/B0b;->c:LX/B0M;

    invoke-virtual {v0, v1}, LX/B0M;->a(LX/B0N;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1734064
    iget-object v0, p0, LX/B0b;->c:LX/B0M;

    invoke-virtual {v0, p1}, LX/B0M;->a(Ljava/lang/Throwable;)V

    .line 1734065
    invoke-virtual {p0}, LX/B0b;->a()V

    .line 1734066
    return-void
.end method
