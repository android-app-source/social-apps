.class public final LX/C8A;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/C8A;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/C88;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/C8B;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1852524
    const/4 v0, 0x0

    sput-object v0, LX/C8A;->a:LX/C8A;

    .line 1852525
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/C8A;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1852526
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1852527
    new-instance v0, LX/C8B;

    invoke-direct {v0}, LX/C8B;-><init>()V

    iput-object v0, p0, LX/C8A;->c:LX/C8B;

    .line 1852528
    return-void
.end method

.method public static declared-synchronized q()LX/C8A;
    .locals 2

    .prologue
    .line 1852520
    const-class v1, LX/C8A;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/C8A;->a:LX/C8A;

    if-nez v0, :cond_0

    .line 1852521
    new-instance v0, LX/C8A;

    invoke-direct {v0}, LX/C8A;-><init>()V

    sput-object v0, LX/C8A;->a:LX/C8A;

    .line 1852522
    :cond_0
    sget-object v0, LX/C8A;->a:LX/C8A;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 1852523
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 3

    .prologue
    .line 1852514
    check-cast p2, LX/C89;

    .line 1852515
    iget-object v0, p2, LX/C89;->a:Ljava/lang/String;

    .line 1852516
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    const/4 v2, 0x2

    invoke-interface {v1, v2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v1

    const/4 p2, 0x1

    .line 1852517
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, v0}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v2

    const p0, 0x7f0b0050

    invoke-virtual {v2, p0}, LX/1ne;->q(I)LX/1ne;

    move-result-object v2

    const/high16 p0, -0x1000000

    invoke-virtual {v2, p0}, LX/1ne;->m(I)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, p2}, LX/1ne;->t(I)LX/1ne;

    move-result-object v2

    const/4 p0, 0x0

    invoke-virtual {v2, p0}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, p2}, LX/1ne;->c(Z)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, p2}, LX/1ne;->j(I)LX/1ne;

    move-result-object v2

    sget-object p0, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v2, p0}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    const p0, 0x7f0b08fe

    invoke-interface {v2, p2, p0}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    invoke-interface {v2, p2}, LX/1Di;->b(I)LX/1Di;

    move-result-object v2

    move-object v2, v2

    .line 1852518
    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    move-object v0, v1

    .line 1852519
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1852512
    invoke-static {}, LX/1dS;->b()V

    .line 1852513
    const/4 v0, 0x0

    return-object v0
.end method
