.class public final LX/B0M;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/B0O;

.field private final b:Z

.field private c:Z

.field private d:Z


# direct methods
.method public constructor <init>(LX/B0O;Z)V
    .locals 1

    .prologue
    .line 1733760
    iput-object p1, p0, LX/B0M;->a:LX/B0O;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1733761
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/B0M;->c:Z

    .line 1733762
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/B0M;->d:Z

    .line 1733763
    iput-boolean p2, p0, LX/B0M;->b:Z

    .line 1733764
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1733765
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/B0M;->c:Z

    .line 1733766
    return-void
.end method

.method public final a(LX/B0N;)V
    .locals 10

    .prologue
    const/4 v8, 0x1

    const/4 v0, 0x0

    .line 1733767
    iget-object v1, p0, LX/B0M;->a:LX/B0O;

    iget-object v1, v1, LX/B0O;->j:LX/0Sh;

    invoke-virtual {v1}, LX/0Sh;->b()V

    .line 1733768
    iget-boolean v1, p0, LX/B0M;->c:Z

    if-eqz v1, :cond_1

    .line 1733769
    :cond_0
    :goto_0
    return-void

    .line 1733770
    :cond_1
    iget-object v1, p0, LX/B0M;->a:LX/B0O;

    iget-boolean v1, v1, LX/B0O;->m:Z

    if-nez v1, :cond_2

    invoke-interface {p1}, LX/B0N;->e()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/B0M;->a:LX/B0O;

    iget-object v1, v1, LX/B0O;->b:LX/B0U;

    invoke-interface {v1}, LX/B0U;->c()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1733771
    new-instance v1, LX/9JI;

    invoke-interface {p1}, LX/B0N;->a()Ljava/nio/ByteBuffer;

    move-result-object v2

    invoke-interface {p1}, LX/B0N;->b()LX/95b;

    move-result-object v3

    invoke-interface {p1}, LX/B0N;->b()LX/95b;

    move-result-object v4

    invoke-interface {v4}, LX/95b;->a()I

    move-result v4

    invoke-direct {v1, v2, v3, v4}, LX/9JI;-><init>(Ljava/nio/ByteBuffer;LX/95b;I)V

    .line 1733772
    iget-object v2, p0, LX/B0M;->a:LX/B0O;

    iget-object v2, v2, LX/B0O;->g:Ljava/util/concurrent/Executor;

    new-instance v3, Lcom/facebook/graphql/connection/ConnectionTailLoaderManager$ConnectionFetcherState$1;

    invoke-direct {v3, p0, v1}, Lcom/facebook/graphql/connection/ConnectionTailLoaderManager$ConnectionFetcherState$1;-><init>(LX/B0M;LX/9JI;)V

    const v1, -0xe1e1546

    invoke-static {v2, v3, v1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1733773
    :cond_2
    iget-boolean v1, p0, LX/B0M;->b:Z

    if-eqz v1, :cond_4

    iget-boolean v1, p0, LX/B0M;->d:Z

    if-eqz v1, :cond_4

    invoke-interface {p1}, LX/B0N;->e()Z

    move-result v1

    if-eqz v1, :cond_4

    move v7, v8

    .line 1733774
    :goto_1
    invoke-interface {p1}, LX/B0N;->e()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1733775
    iput-boolean v0, p0, LX/B0M;->d:Z

    .line 1733776
    :cond_3
    iget-object v0, p0, LX/B0M;->a:LX/B0O;

    iget-object v0, v0, LX/B0O;->b:LX/B0U;

    invoke-interface {v0}, LX/B0U;->a()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    mul-long v4, v0, v2

    .line 1733777
    iget-object v0, p0, LX/B0M;->a:LX/B0O;

    iget-object v0, v0, LX/B0O;->i:LX/2k0;

    iget-object v1, p0, LX/B0M;->a:LX/B0O;

    iget-object v1, v1, LX/B0O;->l:LX/2kb;

    invoke-interface {p1}, LX/B0N;->a()Ljava/nio/ByteBuffer;

    move-result-object v2

    invoke-interface {p1}, LX/B0N;->b()LX/95b;

    move-result-object v3

    invoke-interface {p1}, LX/B0N;->c()Lcom/facebook/graphql/cursor/edgestore/PageInfo;

    move-result-object v6

    invoke-virtual/range {v0 .. v7}, LX/2k0;->b(LX/2kb;Ljava/nio/ByteBuffer;LX/95b;JLcom/facebook/graphql/cursor/edgestore/PageInfo;Z)Landroid/os/Bundle;

    move-result-object v0

    .line 1733778
    if-eqz v0, :cond_0

    .line 1733779
    iget-object v1, p0, LX/B0M;->a:LX/B0O;

    const-string v2, "CHANGE_NUMBER"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 1733780
    iput-wide v2, v1, LX/B0O;->q:J

    .line 1733781
    iget-object v0, p0, LX/B0M;->a:LX/B0O;

    .line 1733782
    iput-boolean v8, v0, LX/B0O;->m:Z

    .line 1733783
    iget-object v0, p0, LX/B0M;->a:LX/B0O;

    invoke-static {v0}, LX/B0O;->g(LX/B0O;)V

    goto/16 :goto_0

    :cond_4
    move v7, v0

    .line 1733784
    goto :goto_1
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1733785
    iget-object v0, p0, LX/B0M;->a:LX/B0O;

    iget-object v0, v0, LX/B0O;->j:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 1733786
    iget-boolean v0, p0, LX/B0M;->c:Z

    if-eqz v0, :cond_0

    .line 1733787
    :goto_0
    return-void

    .line 1733788
    :cond_0
    const-string v0, "ConnectionTailLoaderManager"

    const-string v1, "Error loading rows from network"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, p1, v1, v2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1733789
    iget-object v0, p0, LX/B0M;->a:LX/B0O;

    iget-object v0, v0, LX/B0O;->g:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/facebook/graphql/connection/ConnectionTailLoaderManager$ConnectionFetcherState$2;

    invoke-direct {v1, p0, p1}, Lcom/facebook/graphql/connection/ConnectionTailLoaderManager$ConnectionFetcherState$2;-><init>(LX/B0M;Ljava/lang/Throwable;)V

    const v2, -0x19055989

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1733790
    iget-object v0, p0, LX/B0M;->a:LX/B0O;

    iget-object v0, v0, LX/B0O;->j:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->b()V

    .line 1733791
    iget-boolean v0, p0, LX/B0M;->c:Z

    if-eqz v0, :cond_0

    .line 1733792
    :goto_0
    return-void

    .line 1733793
    :cond_0
    iget-object v0, p0, LX/B0M;->a:LX/B0O;

    iget-object v0, v0, LX/B0O;->o:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 1733794
    iget-object v0, p0, LX/B0M;->a:LX/B0O;

    invoke-static {v0}, LX/B0O;->h(LX/B0O;)V

    goto :goto_0
.end method
