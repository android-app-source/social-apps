.class public LX/CO0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/CNe;


# instance fields
.field private final a:LX/CNb;

.field private final b:LX/CNq;


# direct methods
.method public constructor <init>(LX/CNb;LX/CNq;)V
    .locals 0

    .prologue
    .line 1882444
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1882445
    iput-object p1, p0, LX/CO0;->a:LX/CNb;

    .line 1882446
    iput-object p2, p0, LX/CO0;->b:LX/CNq;

    .line 1882447
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1882448
    iget-object v0, p0, LX/CO0;->a:LX/CNb;

    const-string v1, "event"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, LX/CNb;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1882449
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1882450
    const-class v0, LX/CO0;

    const-string v1, "Event is required for log action"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 1882451
    :goto_0
    return-void

    .line 1882452
    :cond_0
    iget-object v1, p0, LX/CO0;->b:LX/CNq;

    .line 1882453
    iget-object v2, v1, LX/CNq;->d:LX/CNs;

    iget-object v2, v2, LX/CNs;->g:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Zb;

    move-object v2, v2

    .line 1882454
    iget-object v1, p0, LX/CO0;->a:LX/CNb;

    const-string v3, "extra"

    invoke-virtual {v1, v3, v5}, LX/CNb;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1882455
    iget-object v3, p0, LX/CO0;->a:LX/CNb;

    const-string v4, "module"

    invoke-virtual {v3, v4, v5}, LX/CNb;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1882456
    new-instance v4, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v4, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1882457
    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1882458
    iput-object v3, v4, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1882459
    :cond_1
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1882460
    :try_start_0
    iget-object v0, p0, LX/CO0;->b:LX/CNq;

    invoke-virtual {v0}, LX/CNq;->m()LX/0lC;

    move-result-object v0

    invoke-virtual {v0}, LX/0lD;->b()LX/0lp;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0lp;->a(Ljava/lang/String;)LX/15w;

    move-result-object v0

    .line 1882461
    iget-object v1, p0, LX/CO0;->b:LX/CNq;

    invoke-virtual {v1}, LX/CNq;->m()LX/0lC;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/0lD;->a(LX/15w;)LX/0lG;

    move-result-object v0

    check-cast v0, LX/0lF;

    .line 1882462
    invoke-virtual {v0}, LX/0lF;->H()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1882463
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1882464
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    invoke-virtual {v0}, LX/0lF;->s()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 1882465
    :catch_0
    move-exception v0

    .line 1882466
    const-class v1, LX/CO0;

    const-string v3, "Error parsing extras"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v1, v0, v3, v5}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1882467
    :cond_2
    invoke-interface {v2, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto :goto_0
.end method
