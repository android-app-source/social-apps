.class public final LX/C8H;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/C8C;

.field public final synthetic b:LX/C8O;

.field public final synthetic c:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public final synthetic d:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic e:LX/C8M;

.field public final synthetic f:Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;LX/C8C;LX/C8O;Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;Lcom/facebook/feed/rows/core/props/FeedProps;LX/C8M;)V
    .locals 0

    .prologue
    .line 1852578
    iput-object p1, p0, LX/C8H;->f:Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;

    iput-object p2, p0, LX/C8H;->a:LX/C8C;

    iput-object p3, p0, LX/C8H;->b:LX/C8O;

    iput-object p4, p0, LX/C8H;->c:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    iput-object p5, p0, LX/C8H;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    iput-object p6, p0, LX/C8H;->e:LX/C8M;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10

    .prologue
    const/4 v7, 0x2

    const/4 v0, 0x1

    const v1, -0x51dd0ff8

    invoke-static {v7, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v6

    .line 1852579
    iget-object v0, p0, LX/C8H;->f:Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;

    iget-object v1, p0, LX/C8H;->a:LX/C8C;

    iget-object v2, p0, LX/C8H;->b:LX/C8O;

    iget-object v3, p0, LX/C8H;->c:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    iget-object v4, p0, LX/C8H;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v5, p0, LX/C8H;->e:LX/C8M;

    .line 1852580
    iget-object v8, v4, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v8, v8

    .line 1852581
    check-cast v8, Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;

    .line 1852582
    sget-object v9, LX/C8E;->b:[I

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->ordinal()I

    move-result p0

    aget v9, v9, p0

    packed-switch v9, :pswitch_data_0

    .line 1852583
    const/4 p0, 0x0

    .line 1852584
    move v9, p0

    :goto_0
    iget-object v8, v2, LX/C8O;->e:Landroid/view/ViewGroup;

    invoke-virtual {v8}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v8

    if-ge v9, v8, :cond_1

    .line 1852585
    iget-object v8, v2, LX/C8O;->e:Landroid/view/ViewGroup;

    invoke-virtual {v8, v9}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, LX/C8C;

    .line 1852586
    if-ne v8, v1, :cond_0

    const/4 p1, 0x1

    :goto_1
    invoke-virtual {v8, p1}, LX/C8C;->setSelected(Z)V

    .line 1852587
    add-int/lit8 v8, v9, 0x1

    move v9, v8

    goto :goto_0

    :cond_0
    move p1, p0

    .line 1852588
    goto :goto_1

    .line 1852589
    :cond_1
    const/4 v8, 0x0

    invoke-virtual {v2, v8}, LX/C8O;->setEnabled(Z)V

    .line 1852590
    iget-object v8, v0, Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;->e:LX/1du;

    new-instance v9, LX/C8I;

    invoke-direct {v9, v0, v2, v4, v5}, LX/C8I;-><init>(Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;LX/C8O;Lcom/facebook/feed/rows/core/props/FeedProps;LX/C8M;)V

    invoke-virtual {v8, v4, v3, v9}, LX/1du;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;LX/0TF;)V

    .line 1852591
    :cond_2
    :goto_2
    const v0, -0x4a9b533b

    invoke-static {v7, v7, v0, v6}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1852592
    :pswitch_0
    iget-object v9, v0, Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;->f:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-virtual {v2}, LX/C8O;->getContext()Landroid/content/Context;

    move-result-object p0

    sget-object p1, LX/0ax;->cO:Ljava/lang/String;

    invoke-interface {v9, p0, p1}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 1852593
    sget-object v9, Lcom/facebook/graphql/enums/StoryVisibility;->GONE:Lcom/facebook/graphql/enums/StoryVisibility;

    invoke-static {v0, v9, v8, v5}, Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;->a$redex0(Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;Lcom/facebook/graphql/enums/StoryVisibility;Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;LX/C8M;)V

    goto :goto_2

    .line 1852594
    :pswitch_1
    if-eqz v8, :cond_2

    iget-object v8, v5, LX/C8M;->a:Ljava/lang/String;

    if-eqz v8, :cond_2

    .line 1852595
    iget-object v8, v0, Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;->h:LX/9yI;

    iget-object v9, v0, Lcom/facebook/feedplugins/hidden/FeedHiddenUnitPartDefinition;->c:Landroid/support/v4/app/FragmentActivity;

    sget-object p0, LX/0wD;->NEWSFEED:LX/0wD;

    invoke-virtual {p0}, LX/0wD;->stringValueOf()Ljava/lang/String;

    move-result-object p0

    const/4 p1, 0x0

    invoke-virtual {v8, v9, v4, p0, p1}, LX/9yI;->a(Landroid/content/Context;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;LX/9lZ;)V

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
