.class public final LX/BIY;
.super LX/0gG;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/photos/taggablegallery/PhotoGallery;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/taggablegallery/PhotoGallery;)V
    .locals 0

    .prologue
    .line 1770668
    iput-object p1, p0, LX/BIY;->a:Lcom/facebook/photos/taggablegallery/PhotoGallery;

    invoke-direct {p0}, LX/0gG;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/photos/taggablegallery/PhotoGallery;B)V
    .locals 0

    .prologue
    .line 1770669
    invoke-direct {p0, p1}, LX/BIY;-><init>(Lcom/facebook/photos/taggablegallery/PhotoGallery;)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1770630
    const/4 v0, -0x2

    return v0
.end method

.method public final a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 1770653
    iget-object v0, p0, LX/BIY;->a:Lcom/facebook/photos/taggablegallery/PhotoGallery;

    iget-object v0, v0, Lcom/facebook/photos/taggablegallery/PhotoGallery;->d:LX/BIi;

    invoke-interface {v0, p2}, LX/BIi;->a(I)Lcom/facebook/photos/taggablegallery/PhotoGalleryContent;

    move-result-object v0

    .line 1770654
    iget-object v1, p0, LX/BIY;->a:Lcom/facebook/photos/taggablegallery/PhotoGallery;

    iget-object v1, v1, Lcom/facebook/photos/taggablegallery/PhotoGallery;->a:LX/75S;

    .line 1770655
    iget-object v2, v0, Lcom/facebook/photos/taggablegallery/PhotoGalleryContent;->a:Lcom/facebook/photos/base/media/PhotoItem;

    move-object v2, v2

    .line 1770656
    invoke-virtual {v1, v2}, LX/75S;->a(Lcom/facebook/photos/base/media/PhotoItem;)V

    .line 1770657
    iget-object v1, p0, LX/BIY;->a:Lcom/facebook/photos/taggablegallery/PhotoGallery;

    iget-object v1, v1, Lcom/facebook/photos/taggablegallery/PhotoGallery;->e:LX/9iR;

    .line 1770658
    iget-object v2, v0, Lcom/facebook/photos/taggablegallery/PhotoGalleryContent;->a:Lcom/facebook/photos/base/media/PhotoItem;

    move-object v2, v2

    .line 1770659
    iget-object v3, v2, Lcom/facebook/photos/base/media/PhotoItem;->i:Lcom/facebook/photos/base/photos/LocalPhoto;

    move-object v2, v3

    .line 1770660
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 1770661
    iget-object v4, v0, Lcom/facebook/photos/taggablegallery/PhotoGalleryContent;->b:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-object v0, v4

    .line 1770662
    invoke-virtual {v1, v2, v3, v0}, LX/9iR;->a(LX/74w;Landroid/content/Context;Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)LX/9iQ;

    move-result-object v1

    .line 1770663
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/9iQ;->setTag(Ljava/lang/Object;)V

    .line 1770664
    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1770665
    iget-object v0, p0, LX/BIY;->a:Lcom/facebook/photos/taggablegallery/PhotoGallery;

    iget-object v0, v0, Lcom/facebook/photos/taggablegallery/PhotoGallery;->f:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BIX;

    .line 1770666
    invoke-interface {v0, v1, p2}, LX/BIX;->a(LX/9iQ;I)V

    goto :goto_0

    .line 1770667
    :cond_0
    return-object v1
.end method

.method public final a(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 0

    .prologue
    .line 1770670
    check-cast p3, Landroid/view/View;

    invoke-virtual {p1, p3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 1770671
    return-void
.end method

.method public final a(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1770652
    if-ne p1, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 1770651
    iget-object v0, p0, LX/BIY;->a:Lcom/facebook/photos/taggablegallery/PhotoGallery;

    iget-object v0, v0, Lcom/facebook/photos/taggablegallery/PhotoGallery;->d:LX/BIi;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/BIY;->a:Lcom/facebook/photos/taggablegallery/PhotoGallery;

    iget-object v0, v0, Lcom/facebook/photos/taggablegallery/PhotoGallery;->d:LX/BIi;

    invoke-interface {v0}, LX/BIi;->a()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1770631
    if-nez p3, :cond_1

    .line 1770632
    iget-object v0, p0, LX/BIY;->a:Lcom/facebook/photos/taggablegallery/PhotoGallery;

    .line 1770633
    iput-object v1, v0, Lcom/facebook/photos/taggablegallery/PhotoGallery;->g:LX/74w;

    .line 1770634
    iget-object v0, p0, LX/BIY;->a:Lcom/facebook/photos/taggablegallery/PhotoGallery;

    .line 1770635
    iput-object v1, v0, Lcom/facebook/photos/taggablegallery/PhotoGallery;->h:LX/9iQ;

    .line 1770636
    :cond_0
    return-void

    .line 1770637
    :cond_1
    check-cast p3, LX/9iQ;

    .line 1770638
    iget-object v0, p0, LX/BIY;->a:Lcom/facebook/photos/taggablegallery/PhotoGallery;

    .line 1770639
    iput-object p3, v0, Lcom/facebook/photos/taggablegallery/PhotoGallery;->h:LX/9iQ;

    .line 1770640
    invoke-virtual {p3}, LX/9iQ;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1770641
    const v0, 0x7f020346

    .line 1770642
    iget-object v1, p3, LX/9iQ;->c:Lcom/facebook/widget/images/UrlImage;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/images/UrlImage;->setPlaceHolderResourceId(I)V

    .line 1770643
    iget-object v1, p3, LX/9iQ;->c:Lcom/facebook/widget/images/UrlImage;

    sget-object p1, Landroid/widget/ImageView$ScaleType;->MATRIX:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v1, p1}, Lcom/facebook/widget/images/UrlImage;->setPlaceHolderScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 1770644
    :cond_2
    iget-object v0, p3, LX/9iQ;->b:LX/74w;

    move-object v0, v0

    .line 1770645
    iget-object v1, p0, LX/BIY;->a:Lcom/facebook/photos/taggablegallery/PhotoGallery;

    iget-object v1, v1, Lcom/facebook/photos/taggablegallery/PhotoGallery;->g:LX/74w;

    if-eq v0, v1, :cond_0

    .line 1770646
    iget-object v0, p0, LX/BIY;->a:Lcom/facebook/photos/taggablegallery/PhotoGallery;

    .line 1770647
    iget-object v1, p3, LX/9iQ;->b:LX/74w;

    move-object v1, v1

    .line 1770648
    iput-object v1, v0, Lcom/facebook/photos/taggablegallery/PhotoGallery;->g:LX/74w;

    .line 1770649
    iget-object v0, p0, LX/BIY;->a:Lcom/facebook/photos/taggablegallery/PhotoGallery;

    iget-object v0, v0, Lcom/facebook/photos/taggablegallery/PhotoGallery;->f:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BIX;

    .line 1770650
    invoke-interface {v0, p3, p2}, LX/BIX;->b(LX/9iQ;I)V

    goto :goto_0
.end method
