.class public final LX/AsZ;
.super LX/4o9;
.source ""


# instance fields
.field public final synthetic a:LX/Ase;


# direct methods
.method public constructor <init>(LX/Ase;)V
    .locals 0

    .prologue
    .line 1720672
    iput-object p1, p0, LX/AsZ;->a:LX/Ase;

    invoke-direct {p0}, LX/4o9;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 2

    .prologue
    .line 1720673
    iget-object v0, p0, LX/AsZ;->a:LX/Ase;

    iget-object v0, v0, LX/Ase;->d:LX/AsA;

    invoke-interface {v0}, LX/AsA;->b()LX/86o;

    move-result-object v0

    invoke-virtual {v0}, LX/86o;->shouldBeDismissedBySwipeDownAndTapOutside()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1720674
    iget-object v0, p0, LX/AsZ;->a:LX/Ase;

    iget-object v0, v0, LX/Ase;->e:Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;

    iget-object v1, p0, LX/AsZ;->a:LX/Ase;

    iget-object v1, v1, LX/Ase;->m:LX/Asb;

    invoke-virtual {v0, v1}, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->c(LX/Asb;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1720675
    iget-object v0, p0, LX/AsZ;->a:LX/Ase;

    iget-object v0, v0, LX/Ase;->e:Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;

    iget-object v1, p0, LX/AsZ;->a:LX/Ase;

    iget-object v1, v1, LX/Ase;->m:LX/Asb;

    invoke-virtual {v0, v1}, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->b(LX/Asb;)Z

    .line 1720676
    iget-object v0, p0, LX/AsZ;->a:LX/Ase;

    iget-object v0, v0, LX/Ase;->e:Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;

    iget-object v1, p0, LX/AsZ;->a:LX/Ase;

    iget-object v1, v1, LX/Ase;->n:LX/Ard;

    invoke-virtual {v0, v1}, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->c(LX/Ard;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1720677
    iget-object v0, p0, LX/AsZ;->a:LX/Ase;

    iget-object v0, v0, LX/Ase;->e:Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;

    iget-object v1, p0, LX/AsZ;->a:LX/Ase;

    iget-object v1, v1, LX/Ase;->n:LX/Ard;

    invoke-virtual {v0, v1}, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->b(LX/Ard;)Z

    .line 1720678
    :cond_0
    iget-object v0, p0, LX/AsZ;->a:LX/Ase;

    invoke-static {v0}, LX/Ase;->f$redex0(LX/Ase;)V

    .line 1720679
    return-void
.end method
