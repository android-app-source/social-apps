.class public LX/C50;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final a:Lcom/facebook/intent/feed/IFeedIntentBuilder;

.field private final b:LX/1Kf;

.field private final c:LX/03V;

.field private final d:LX/1PT;

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private final g:LX/1Cn;

.field private final h:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/intent/feed/IFeedIntentBuilder;LX/1Kf;LX/03V;LX/1Cn;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1PT;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p5    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/1PT;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p8    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            "LX/1Kf;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/1Cn;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "LX/1PT;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1848193
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1848194
    iput-object p1, p0, LX/C50;->a:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    .line 1848195
    iput-object p2, p0, LX/C50;->b:LX/1Kf;

    .line 1848196
    iput-object p3, p0, LX/C50;->c:LX/03V;

    .line 1848197
    iput-object p4, p0, LX/C50;->g:LX/1Cn;

    .line 1848198
    iput-object p5, p0, LX/C50;->h:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1848199
    iput-object p6, p0, LX/C50;->d:LX/1PT;

    .line 1848200
    iput-object p7, p0, LX/C50;->e:Ljava/lang/String;

    .line 1848201
    iput-object p8, p0, LX/C50;->f:Ljava/lang/String;

    .line 1848202
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v6, 0x1

    const/4 v2, 0x0

    const/4 v0, 0x2

    const v1, 0x4b706b9b    # 1.5756187E7f

    invoke-static {v0, v6, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v3

    .line 1848203
    iget-object v0, p0, LX/C50;->d:LX/1PT;

    invoke-static {v0}, LX/9Ir;->a(LX/1PT;)LX/21D;

    move-result-object v4

    .line 1848204
    sget-object v0, LX/21D;->ON_THIS_DAY_FEED:LX/21D;

    invoke-virtual {v0, v4}, LX/21D;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1848205
    const-string v0, "permalink"

    move-object v1, v0

    .line 1848206
    :goto_0
    iget-object v5, p0, LX/C50;->g:LX/1Cn;

    iget-object v0, p0, LX/C50;->h:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1848207
    iget-object v7, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v7

    .line 1848208
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/C50;->h:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1848209
    iget-object v7, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v7

    .line 1848210
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v5, v0, v1}, LX/1Cn;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1848211
    iget-object v0, p0, LX/C50;->a:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    iget-object v1, p0, LX/C50;->h:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v5, p0, LX/C50;->e:Ljava/lang/String;

    invoke-interface {v0, v1, v4, v5}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setIsThrowbackPost(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    iget-object v1, p0, LX/C50;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setNectarModule(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    .line 1848212
    iget-object v1, p0, LX/C50;->b:LX/1Kf;

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v4

    const/16 v5, 0x6dc

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v6, Landroid/app/Activity;

    invoke-static {v0, v6}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-interface {v1, v2, v4, v5, v0}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/app/Activity;)V

    .line 1848213
    const v0, -0x3e9ae2ef

    invoke-static {v0, v3}, LX/02F;->a(II)V

    return-void

    .line 1848214
    :cond_0
    sget-object v0, LX/21D;->NEWSFEED:LX/21D;

    invoke-virtual {v0, v4}, LX/21D;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1848215
    const-string v0, "promotion"

    move-object v1, v0

    goto :goto_0

    :cond_1
    move-object v0, v2

    .line 1848216
    goto :goto_1

    :cond_2
    move-object v1, v2

    goto :goto_0
.end method
