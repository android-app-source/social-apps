.class public final LX/CSw;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/CT8;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/CSz;


# direct methods
.method public constructor <init>(LX/CSz;LX/CT8;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1895158
    iput-object p1, p0, LX/CSw;->c:LX/CSz;

    iput-object p2, p0, LX/CSw;->a:LX/CT8;

    iput-object p3, p0, LX/CSw;->b:Ljava/lang/String;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 1895110
    iget-object v0, p0, LX/CSw;->a:LX/CT8;

    if-eqz v0, :cond_0

    .line 1895111
    iget-object v0, p0, LX/CSw;->a:LX/CT8;

    invoke-virtual {v0}, LX/CT8;->a()V

    .line 1895112
    :cond_0
    iget-object v0, p0, LX/CSw;->c:LX/CSz;

    iget-object v0, v0, LX/CSz;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CXd;

    sget-object v1, LX/CXc;->DATA_FETCH:LX/CXc;

    const-string v2, "component_screen_invalid_data"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Component flow fetch failed for CTA ID:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, LX/CSw;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, LX/CXd;->a(LX/CXc;Ljava/lang/String;Ljava/lang/String;)V

    .line 1895113
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 1895114
    check-cast p1, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel;

    .line 1895115
    :try_start_0
    iget-object v0, p0, LX/CSw;->a:LX/CT8;

    if-eqz v0, :cond_4

    .line 1895116
    iget-object v0, p0, LX/CSw;->a:LX/CT8;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1895117
    :try_start_1
    sget-object v1, LX/CTA;->c:[I

    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel;->a()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;->z()Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1895118
    iget-object v1, v0, LX/CT8;->a:LX/CTC;

    .line 1895119
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1895120
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel;->a()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;

    move-result-object v2

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1895121
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel;->a()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 1895122
    const/4 v2, 0x0

    :goto_0
    move-object v2, v2

    .line 1895123
    iget-object v3, v2, LX/CUD;->a:LX/CUA;

    iget-object v3, v3, LX/CUA;->a:Ljava/lang/String;

    .line 1895124
    iget-object v4, v1, LX/CTC;->e:LX/CSc;

    const/4 v5, 0x0

    invoke-virtual {v4, v3, v5}, LX/CSc;->a(Ljava/lang/String;LX/CSb;)LX/CSb;

    move-result-object v3

    .line 1895125
    invoke-static {v2}, LX/CTC;->c(LX/CUD;)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, v1, LX/CTC;->t:Ljava/util/HashSet;

    iget-object v5, v2, LX/CUD;->a:LX/CUA;

    iget-object v5, v5, LX/CUA;->c:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1895126
    :goto_1
    iget-object v4, v1, LX/CTC;->s:Ljava/util/Stack;

    invoke-virtual {v4}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/3rL;

    iget-object v4, v4, LX/3rL;->a:Ljava/lang/Object;

    check-cast v4, LX/CUD;

    .line 1895127
    iget-object v5, v2, LX/CUD;->a:LX/CUA;

    iget-object v5, v5, LX/CUA;->c:Ljava/lang/String;

    iget-object p1, v4, LX/CUD;->a:LX/CUA;

    iget-object p1, p1, LX/CUA;->c:Ljava/lang/String;

    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    move v4, v5

    .line 1895128
    if-nez v4, :cond_0

    .line 1895129
    invoke-static {v1}, LX/CTC;->r(LX/CTC;)V

    goto :goto_1

    .line 1895130
    :cond_0
    iget-object v4, v1, LX/CTC;->s:Ljava/util/Stack;

    invoke-virtual {v4}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    .line 1895131
    iget-object v4, v1, LX/CTC;->u:Ljava/util/Stack;

    invoke-virtual {v4}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/3rL;

    iput-object v4, v1, LX/CTC;->v:LX/3rL;

    .line 1895132
    :cond_1
    invoke-static {v2}, LX/CTC;->c(LX/CUD;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1895133
    iget-object v4, v1, LX/CTC;->t:Ljava/util/HashSet;

    iget-object v5, v2, LX/CUD;->a:LX/CUA;

    iget-object v5, v5, LX/CUA;->c:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1895134
    :cond_2
    iget-object v4, v1, LX/CTC;->s:Ljava/util/Stack;

    new-instance v5, LX/3rL;

    invoke-direct {v5, v2, v3}, LX/3rL;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v4, v5}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1895135
    iget-object v4, v1, LX/CTC;->w:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v4}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()LX/1OR;

    move-result-object v4

    check-cast v4, LX/1P1;

    .line 1895136
    invoke-virtual {v4}, LX/1P1;->l()I

    move-result v5

    .line 1895137
    invoke-virtual {v4, v5}, LX/1OR;->c(I)Landroid/view/View;

    move-result-object v4

    .line 1895138
    if-ltz v5, :cond_3

    if-nez v4, :cond_5

    .line 1895139
    :cond_3
    sget-object v4, LX/CTC;->a:LX/3rL;

    .line 1895140
    :goto_2
    move-object v4, v4

    .line 1895141
    iget-object v5, v1, LX/CTC;->u:Ljava/util/Stack;

    invoke-virtual {v5, v4}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1895142
    sget-object v4, LX/CTC;->a:LX/3rL;

    iput-object v4, v1, LX/CTC;->v:LX/3rL;

    .line 1895143
    sget-object v3, LX/CTB;->READY:LX/CTB;

    invoke-static {v1, v3}, LX/CTC;->a$redex0(LX/CTC;LX/CTB;)V

    .line 1895144
    iget-object v3, v2, LX/CUD;->h:Ljava/util/HashMap;

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;->ON_LOADING:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1895145
    iget-object v3, v1, LX/CTC;->k:LX/CT1;

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;->ON_LOADING:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;

    iget-object v2, v2, LX/CUD;->h:Ljava/util/HashMap;

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;->ON_LOADING:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;

    invoke-virtual {v2, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/CTv;

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v2, v5}, LX/CT1;->a(Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;LX/CTv;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 1895146
    :cond_4
    :goto_3
    return-void

    .line 1895147
    :catch_0
    move-exception v0

    .line 1895148
    iget-object v1, p0, LX/CSw;->c:LX/CSz;

    iget-object v1, v1, LX/CSz;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-static {v0}, LX/CXd;->a(Ljava/lang/Exception;)V

    .line 1895149
    invoke-virtual {p0, v0}, LX/CSw;->onNonCancellationFailure(Ljava/lang/Throwable;)V

    goto :goto_3

    .line 1895150
    :pswitch_0
    :try_start_2
    iget-object v1, v0, LX/CT8;->a:LX/CTC;

    invoke-static {v1, p1}, LX/CTC;->a$redex0(LX/CTC;Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    :try_start_3
    goto :goto_3

    .line 1895151
    :catch_1
    move-exception v1

    .line 1895152
    iget-object v2, v0, LX/CT8;->a:LX/CTC;

    iget-object v2, v2, LX/CTC;->d:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-static {v1}, LX/CXd;->a(Ljava/lang/Exception;)V

    .line 1895153
    invoke-virtual {v0}, LX/CT8;->a()V

    goto :goto_3
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    .line 1895154
    :sswitch_0
    new-instance v2, LX/CUG;

    invoke-static {}, LX/CSv;->a()LX/CSr;

    move-result-object v3

    invoke-direct {v2, p1, v3}, LX/CUG;-><init>(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel;LX/CSr;)V

    goto/16 :goto_0

    .line 1895155
    :sswitch_1
    new-instance v2, LX/CUH;

    invoke-static {}, LX/CSv;->a()LX/CSr;

    move-result-object v3

    invoke-direct {v2, p1, v3}, LX/CUH;-><init>(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel;LX/CSr;)V

    goto/16 :goto_0

    .line 1895156
    :cond_5
    invoke-virtual {v4}, Landroid/view/View;->getTop()I

    move-result p1

    .line 1895157
    new-instance v4, LX/3rL;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-direct {v4, v5, p1}, LX/3rL;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_0
    .end packed-switch

    :sswitch_data_0
    .sparse-switch
        0xe4fe0d5 -> :sswitch_1
        0x5de9ea56 -> :sswitch_0
    .end sparse-switch
.end method
