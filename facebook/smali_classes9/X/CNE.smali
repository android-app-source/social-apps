.class public LX/CNE;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Landroid/content/ContentResolver;

.field public final c:LX/0TD;

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Ma;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/ui/media/attachments/MediaResourceHelper;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final f:LX/CN2;

.field private final g:LX/1Er;

.field private final h:LX/Be5;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1881356
    const-class v0, LX/CNE;

    sput-object v0, LX/CNE;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/ContentResolver;LX/0TD;LX/CN2;LX/1Er;LX/Be5;)V
    .locals 1
    .param p2    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1881357
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1881358
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1881359
    iput-object v0, p0, LX/CNE;->d:LX/0Ot;

    .line 1881360
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1881361
    iput-object v0, p0, LX/CNE;->e:LX/0Ot;

    .line 1881362
    iput-object p1, p0, LX/CNE;->b:Landroid/content/ContentResolver;

    .line 1881363
    iput-object p2, p0, LX/CNE;->c:LX/0TD;

    .line 1881364
    iput-object p3, p0, LX/CNE;->f:LX/CN2;

    .line 1881365
    iput-object p4, p0, LX/CNE;->g:LX/1Er;

    .line 1881366
    iput-object p5, p0, LX/CNE;->h:LX/Be5;

    .line 1881367
    return-void
.end method

.method private a(Lcom/facebook/ui/media/attachments/MediaResource;Ljava/lang/String;Ljava/lang/String;LX/46h;)Lcom/facebook/ui/media/attachments/MediaResource;
    .locals 7

    .prologue
    .line 1881368
    iget-object v0, p0, LX/CNE;->f:LX/CN2;

    iget-object v1, p1, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    const/16 v2, 0x800

    const/16 v3, 0x55

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-virtual/range {v0 .. v6}, LX/CN2;->a(Landroid/net/Uri;IILjava/lang/String;Ljava/lang/String;LX/46h;)LX/CN4;

    move-result-object v0

    .line 1881369
    sget-object v1, LX/CND;->a:[I

    iget-object v2, v0, LX/CN4;->a:LX/CN3;

    invoke-virtual {v2}, LX/CN3;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1881370
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unknown resize state returned from PhotoResizeHelper.resizePhoto()"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1881371
    :pswitch_0
    new-instance v1, LX/5zn;

    invoke-direct {v1}, LX/5zn;-><init>()V

    invoke-virtual {v1, p1}, LX/5zn;->a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/5zn;

    move-result-object v1

    iget-object v0, v0, LX/CN4;->b:Ljava/io/File;

    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    .line 1881372
    iput-object v0, v1, LX/5zn;->b:Landroid/net/Uri;

    .line 1881373
    move-object v0, v1

    .line 1881374
    invoke-virtual {v0}, LX/5zn;->G()Lcom/facebook/ui/media/attachments/MediaResource;

    move-result-object v0

    return-object v0

    .line 1881375
    :pswitch_1
    iget-object v0, v0, LX/CN4;->c:Ljava/lang/Exception;

    invoke-static {v0}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static a(Landroid/os/ParcelFileDescriptor;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1881376
    :try_start_0
    new-instance v1, Lcom/facebook/common/dextricks/DalvikInternals$Stat;

    invoke-direct {v1}, Lcom/facebook/common/dextricks/DalvikInternals$Stat;-><init>()V

    .line 1881377
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v2

    .line 1881378
    if-nez v2, :cond_1

    .line 1881379
    :cond_0
    :goto_0
    return v0

    .line 1881380
    :cond_1
    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/facebook/common/dextricks/DalvikInternals;->statNamedFile(Ljava/lang/String;Lcom/facebook/common/dextricks/DalvikInternals$Stat;)V

    .line 1881381
    iget-wide v2, v1, Lcom/facebook/common/dextricks/DalvikInternals$Stat;->device:J

    .line 1881382
    invoke-virtual {p0}, Landroid/os/ParcelFileDescriptor;->getFd()I

    move-result v4

    invoke-static {v4, v1}, Lcom/facebook/common/dextricks/DalvikInternals;->statOpenFile(ILcom/facebook/common/dextricks/DalvikInternals$Stat;)V

    .line 1881383
    iget-wide v4, v1, Lcom/facebook/common/dextricks/DalvikInternals$Stat;->device:J
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1881384
    cmp-long v1, v4, v2

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    .line 1881385
    :catch_0
    goto :goto_0
.end method

.method public static a$redex0(LX/CNE;Lcom/facebook/ui/media/attachments/MediaResource;)Lcom/facebook/ui/media/attachments/MediaResource;
    .locals 11

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1881386
    iget-object v6, p1, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    .line 1881387
    :try_start_0
    const-string v0, "android.resource"

    invoke-virtual {v6}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1881388
    iget-object v0, p0, LX/CNE;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Ma;

    iget-object v4, p1, Lcom/facebook/ui/media/attachments/MediaResource;->r:Ljava/lang/String;

    invoke-virtual {v0, v4}, LX/2Ma;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1881389
    iget-object v4, p1, Lcom/facebook/ui/media/attachments/MediaResource;->r:Ljava/lang/String;

    .line 1881390
    iget-object v0, p0, LX/CNE;->f:LX/CN2;

    invoke-virtual {v0, v4}, LX/CN2;->a(Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    move-object v2, v3

    :goto_0
    move v9, v1

    move-object v1, v2

    move-object v2, v4

    move-object v4, v5

    move v5, v0

    move v0, v9

    .line 1881391
    :goto_1
    :try_start_1
    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 1881392
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "Unable to find extension for "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p1, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1881393
    :catchall_0
    move-exception v0

    :goto_2
    if-eqz v1, :cond_0

    .line 1881394
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 1881395
    :cond_0
    if-eqz v3, :cond_1

    .line 1881396
    invoke-virtual {v3}, Landroid/os/ParcelFileDescriptor;->close()V

    :cond_1
    throw v0

    .line 1881397
    :cond_2
    :try_start_2
    iget-object v0, p0, LX/CNE;->b:Landroid/content/ContentResolver;

    invoke-virtual {v0, v6}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v0

    move v9, v2

    move-object v2, v0

    move v0, v9

    goto :goto_0

    .line 1881398
    :cond_3
    invoke-static {v6}, LX/1be;->c(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1881399
    iget-object v0, p0, LX/CNE;->b:Landroid/content/ContentResolver;

    invoke-virtual {v0, v6}, Landroid/content/ContentResolver;->getType(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v4

    .line 1881400
    if-eqz v4, :cond_4

    move v0, v1

    :goto_3
    const-string v5, "Unable to get mime type for %s"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v6, v7, v8

    invoke-static {v0, v5, v7}, LX/0PB;->checkState(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 1881401
    iget-object v0, p0, LX/CNE;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Ma;

    invoke-virtual {v0, v4}, LX/2Ma;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1881402
    iget-object v0, p0, LX/CNE;->f:LX/CN2;

    invoke-virtual {v0, v4}, LX/CN2;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    move v2, v1

    move-object v0, v3

    :goto_4
    move v9, v1

    move-object v1, v0

    move v0, v9

    move-object v10, v5

    move v5, v2

    move-object v2, v4

    move-object v4, v10

    .line 1881403
    goto :goto_1

    :cond_4
    move v0, v2

    .line 1881404
    goto :goto_3

    .line 1881405
    :cond_5
    iget-object v0, p0, LX/CNE;->b:Landroid/content/ContentResolver;

    invoke-virtual {v0, v6}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object v0

    goto :goto_4

    .line 1881406
    :cond_6
    invoke-static {v6}, LX/1be;->b(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1881407
    iget-object v0, p0, LX/CNE;->b:Landroid/content/ContentResolver;

    invoke-virtual {v0, v6}, Landroid/content/ContentResolver;->getType(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v5

    .line 1881408
    if-eqz v5, :cond_7

    .line 1881409
    iget-object v0, p0, LX/CNE;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Ma;

    invoke-virtual {v0, v5}, LX/2Ma;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1881410
    :goto_5
    iget-object v4, p0, LX/CNE;->h:LX/Be5;

    invoke-virtual {v4, v6}, LX/Be5;->a(Landroid/net/Uri;)Landroid/os/ParcelFileDescriptor;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v4

    .line 1881411
    :try_start_3
    iget-object v6, p0, LX/CNE;->f:LX/CN2;

    invoke-virtual {v6, v5}, LX/CN2;->a(Ljava/lang/String;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    move-result v6

    if-eqz v6, :cond_8

    move v2, v1

    .line 1881412
    :goto_6
    :try_start_4
    invoke-static {v4}, LX/CNE;->a(Landroid/os/ParcelFileDescriptor;)Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    move-result v1

    move v9, v1

    move-object v1, v3

    move-object v3, v4

    move-object v4, v0

    move v0, v9

    move-object v10, v5

    move v5, v2

    move-object v2, v10

    goto/16 :goto_1

    .line 1881413
    :cond_7
    :try_start_5
    invoke-virtual {v6}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/webkit/MimeTypeMap;->getFileExtensionFromUrl(Ljava/lang/String;)Ljava/lang/String;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    move-result-object v0

    goto :goto_5

    .line 1881414
    :cond_8
    :try_start_6
    new-instance v1, Ljava/io/FileInputStream;

    invoke-virtual {v4}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v6

    invoke-direct {v1, v6}, Ljava/io/FileInputStream;-><init>(Ljava/io/FileDescriptor;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    move-object v3, v1

    goto :goto_6

    .line 1881415
    :cond_9
    :try_start_7
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Uri must be a content, resource, or file uri"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 1881416
    :catchall_1
    move-exception v0

    move-object v1, v3

    goto/16 :goto_2

    .line 1881417
    :cond_a
    :try_start_8
    const-string v6, "orca_share_media"

    .line 1881418
    if-nez v0, :cond_10

    .line 1881419
    sget-object v7, LX/46h;->REQUIRE_PRIVATE:LX/46h;

    .line 1881420
    :goto_7
    move-object v0, v7

    .line 1881421
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v7, "."

    invoke-direct {v2, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1881422
    if-eqz v5, :cond_d

    .line 1881423
    invoke-direct {p0, p1, v6, v2, v0}, LX/CNE;->a(Lcom/facebook/ui/media/attachments/MediaResource;Ljava/lang/String;Ljava/lang/String;LX/46h;)Lcom/facebook/ui/media/attachments/MediaResource;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    move-result-object v0

    .line 1881424
    if-eqz v1, :cond_b

    .line 1881425
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 1881426
    :cond_b
    if-eqz v3, :cond_c

    .line 1881427
    invoke-virtual {v3}, Landroid/os/ParcelFileDescriptor;->close()V

    :cond_c
    :goto_8
    return-object v0

    .line 1881428
    :cond_d
    :try_start_9
    iget-object v4, p0, LX/CNE;->g:LX/1Er;

    invoke-virtual {v4, v6, v2, v0}, LX/1Er;->a(Ljava/lang/String;Ljava/lang/String;LX/46h;)Ljava/io/File;

    move-result-object v0

    .line 1881429
    if-nez v0, :cond_e

    .line 1881430
    new-instance v0, Ljava/io/IOException;

    const-string v2, "Failed to create temp file"

    invoke-direct {v0, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1881431
    :cond_e
    const/4 v2, 0x0

    new-array v2, v2, [LX/3AS;

    invoke-static {v0, v2}, LX/1t3;->a(Ljava/io/File;[LX/3AS;)LX/3AU;

    move-result-object v2

    .line 1881432
    invoke-virtual {v2, v1}, LX/3AU;->a(Ljava/io/InputStream;)J

    .line 1881433
    new-instance v2, LX/5zn;

    invoke-direct {v2}, LX/5zn;-><init>()V

    invoke-virtual {v2, p1}, LX/5zn;->a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/5zn;

    move-result-object v2

    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    .line 1881434
    iput-object v0, v2, LX/5zn;->b:Landroid/net/Uri;

    .line 1881435
    move-object v0, v2

    .line 1881436
    invoke-virtual {v0}, LX/5zn;->G()Lcom/facebook/ui/media/attachments/MediaResource;
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    move-result-object v0

    .line 1881437
    if-eqz v1, :cond_f

    .line 1881438
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 1881439
    :cond_f
    if-eqz v3, :cond_c

    .line 1881440
    invoke-virtual {v3}, Landroid/os/ParcelFileDescriptor;->close()V

    goto :goto_8

    .line 1881441
    :catchall_2
    move-exception v0

    move-object v1, v3

    move-object v3, v4

    goto/16 :goto_2

    :catchall_3
    move-exception v0

    move-object v1, v3

    move-object v3, v4

    goto/16 :goto_2

    .line 1881442
    :cond_10
    if-nez v2, :cond_11

    .line 1881443
    sget-object v7, LX/46h;->REQUIRE_PRIVATE:LX/46h;

    goto :goto_7

    .line 1881444
    :cond_11
    const-string v7, "image/"

    invoke-virtual {v2, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_12

    const-string v7, "video/"

    invoke-virtual {v2, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_12

    const-string v7, "audio/"

    invoke-virtual {v2, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_13

    .line 1881445
    :cond_12
    sget-object v7, LX/46h;->PREFER_SDCARD:LX/46h;

    goto/16 :goto_7

    .line 1881446
    :cond_13
    sget-object v7, LX/46h;->REQUIRE_PRIVATE:LX/46h;

    goto/16 :goto_7
.end method

.method public static b(LX/0QB;)LX/CNE;
    .locals 6

    .prologue
    .line 1881447
    new-instance v0, LX/CNE;

    invoke-static {p0}, LX/0cd;->b(LX/0QB;)Landroid/content/ContentResolver;

    move-result-object v1

    check-cast v1, Landroid/content/ContentResolver;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v2

    check-cast v2, LX/0TD;

    invoke-static {p0}, LX/CN2;->a(LX/0QB;)LX/CN2;

    move-result-object v3

    check-cast v3, LX/CN2;

    invoke-static {p0}, LX/1Er;->a(LX/0QB;)LX/1Er;

    move-result-object v4

    check-cast v4, LX/1Er;

    invoke-static {p0}, LX/Be5;->a(LX/0QB;)LX/Be5;

    move-result-object v5

    check-cast v5, LX/Be5;

    invoke-direct/range {v0 .. v5}, LX/CNE;-><init>(Landroid/content/ContentResolver;LX/0TD;LX/CN2;LX/1Er;LX/Be5;)V

    .line 1881448
    const/16 v1, 0x12b5

    invoke-static {p0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0x12b6

    invoke-static {p0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    .line 1881449
    iput-object v1, v0, LX/CNE;->d:LX/0Ot;

    iput-object v2, v0, LX/CNE;->e:LX/0Ot;

    .line 1881450
    return-object v0
.end method
