.class public final LX/CAb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Zx;


# instance fields
.field public final synthetic a:Landroid/app/Activity;

.field public final synthetic b:LX/CAc;


# direct methods
.method public constructor <init>(LX/CAc;Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 1855543
    iput-object p1, p0, LX/CAb;->b:LX/CAc;

    iput-object p2, p0, LX/CAb;->a:Landroid/app/Activity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 1855544
    iget-object v0, p0, LX/CAb;->b:LX/CAc;

    iget-object v0, v0, LX/CAc;->b:Lcom/facebook/feedplugins/profile/calltoaction/MsqrdCallToActionPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/profile/calltoaction/MsqrdCallToActionPartDefinition;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1855545
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1855546
    const-string v1, "profile_media_extras_bundle_key_default_expiration_time_in_secs_since_epoch"

    iget-object v2, p0, LX/CAb;->b:LX/CAc;

    iget-object v2, v2, LX/CAc;->a:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->w()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 1855547
    invoke-static {}, Lcom/facebook/feedplugins/profile/calltoaction/MsqrdCallToActionPartDefinition;->d()Landroid/content/Intent;

    move-result-object v2

    move-object v1, v2

    .line 1855548
    const-string v2, "\u0438\u0434\u0435\u043d\u0442\u0438\u0444\u0438\u043a\u0430\u0442\u043e\u0440_\u043a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u0438"

    iget-object v3, p0, LX/CAb;->b:LX/CAc;

    iget-object v3, v3, LX/CAc;->a:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->am()Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMask;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMask;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1855549
    const-string v2, "\u0438\u0434\u0435\u043d\u0442\u0438\u0444\u0438\u043a\u0430\u0442\u043e\u0440_\u043c\u0430\u0441\u043a\u0438"

    iget-object v3, p0, LX/CAb;->b:LX/CAc;

    iget-object v3, v3, LX/CAc;->a:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->am()Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMask;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLProfileMediaOverlayMask;->k()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1855550
    const-string v2, "profile_media_extras_bundle"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 1855551
    const/high16 v0, 0x14000000

    invoke-virtual {v1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1855552
    iget-object v0, p0, LX/CAb;->b:LX/CAc;

    iget-object v0, v0, LX/CAc;->b:Lcom/facebook/feedplugins/profile/calltoaction/MsqrdCallToActionPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/profile/calltoaction/MsqrdCallToActionPartDefinition;->d:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/CAb;->a:Landroid/app/Activity;

    invoke-interface {v0, v1, v2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1855553
    :goto_0
    return-void

    .line 1855554
    :cond_0
    iget-object v0, p0, LX/CAb;->b:LX/CAc;

    iget-object v0, v0, LX/CAc;->b:Lcom/facebook/feedplugins/profile/calltoaction/MsqrdCallToActionPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/profile/calltoaction/MsqrdCallToActionPartDefinition;->e:LX/17T;

    iget-object v1, p0, LX/CAb;->a:Landroid/app/Activity;

    const-string v2, "me.msqrd.android"

    invoke-virtual {v0, v1, v2}, LX/17T;->a(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a([Ljava/lang/String;[Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1855542
    return-void
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 1855541
    return-void
.end method
