.class public LX/Bi1;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/Bi1;


# instance fields
.field private final a:LX/0Zb;

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0gh;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Zb;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Zb;",
            "LX/0Ot",
            "<",
            "LX/0gh;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1809884
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1809885
    iput-object p1, p0, LX/Bi1;->a:LX/0Zb;

    .line 1809886
    iput-object p2, p0, LX/Bi1;->b:LX/0Ot;

    .line 1809887
    return-void
.end method

.method public static a(LX/0QB;)LX/Bi1;
    .locals 5

    .prologue
    .line 1809871
    sget-object v0, LX/Bi1;->c:LX/Bi1;

    if-nez v0, :cond_1

    .line 1809872
    const-class v1, LX/Bi1;

    monitor-enter v1

    .line 1809873
    :try_start_0
    sget-object v0, LX/Bi1;->c:LX/Bi1;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1809874
    if-eqz v2, :cond_0

    .line 1809875
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1809876
    new-instance v4, LX/Bi1;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    const/16 p0, 0x97

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v4, v3, p0}, LX/Bi1;-><init>(LX/0Zb;LX/0Ot;)V

    .line 1809877
    move-object v0, v4

    .line 1809878
    sput-object v0, LX/Bi1;->c:LX/Bi1;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1809879
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1809880
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1809881
    :cond_1
    sget-object v0, LX/Bi1;->c:LX/Bi1;

    return-object v0

    .line 1809882
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1809883
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/Bi1;LX/Bi0;LX/Bi3;Ljava/lang/String;Ljava/lang/String;ILX/0am;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Bi0;",
            "LX/Bi3;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1809862
    iget-object v0, p0, LX/Bi1;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gh;

    const-string v1, "timeline_context_item"

    invoke-virtual {v0, v1}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    .line 1809863
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    iget-object v1, p1, LX/Bi0;->name:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "entity_cards.context_items_view_controller"

    .line 1809864
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1809865
    move-object v0, v0

    .line 1809866
    const-string v1, "context_item_surface"

    iget-object v2, p2, LX/Bi3;->name:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "target_type"

    invoke-virtual {v0, v1, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "fbid"

    invoke-virtual {v0, v1, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "position"

    invoke-virtual {v0, v1, p5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 1809867
    invoke-virtual {p6}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1809868
    const-string v2, "logging_param"

    invoke-virtual {p6}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1809869
    :cond_0
    iget-object v0, p0, LX/Bi1;->a:LX/0Zb;

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1809870
    return-void
.end method


# virtual methods
.method public final a(LX/Bi3;Ljava/lang/String;Ljava/lang/String;ILX/0am;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Bi3;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1809888
    sget-object v1, LX/Bi0;->TAP:LX/Bi0;

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    move-object v6, p5

    invoke-static/range {v0 .. v6}, LX/Bi1;->a(LX/Bi1;LX/Bi0;LX/Bi3;Ljava/lang/String;Ljava/lang/String;ILX/0am;)V

    .line 1809889
    return-void
.end method

.method public final b(LX/Bi3;Ljava/lang/String;Ljava/lang/String;ILX/0am;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Bi3;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1809860
    sget-object v1, LX/Bi0;->IMPRESSION:LX/Bi0;

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    move-object v6, p5

    invoke-static/range {v0 .. v6}, LX/Bi1;->a(LX/Bi1;LX/Bi0;LX/Bi3;Ljava/lang/String;Ljava/lang/String;ILX/0am;)V

    .line 1809861
    return-void
.end method

.method public final b(Ljava/lang/String;I)V
    .locals 7

    .prologue
    .line 1809858
    sget-object v1, LX/Bi0;->TAP:LX/Bi0;

    sget-object v2, LX/Bi3;->PAGE_HEADER:LX/Bi3;

    const-string v3, "expand"

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v6

    move-object v0, p0

    move-object v4, p1

    move v5, p2

    invoke-static/range {v0 .. v6}, LX/Bi1;->a(LX/Bi1;LX/Bi0;LX/Bi3;Ljava/lang/String;Ljava/lang/String;ILX/0am;)V

    .line 1809859
    return-void
.end method
