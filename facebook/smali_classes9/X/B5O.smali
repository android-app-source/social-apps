.class public final LX/B5O;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1744225
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(LX/186;Lcom/facebook/graphql/model/GraphQLImageOverlay;)I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1744226
    if-nez p1, :cond_0

    .line 1744227
    :goto_0
    return v0

    .line 1744228
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLImageOverlay;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v1

    .line 1744229
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLImageOverlay;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1744230
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLImageOverlay;->l()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    const/4 v4, 0x0

    .line 1744231
    if-nez v3, :cond_1

    .line 1744232
    :goto_1
    move v3, v4

    .line 1744233
    const/4 v4, 0x3

    invoke-virtual {p0, v4}, LX/186;->c(I)V

    .line 1744234
    invoke-virtual {p0, v0, v1}, LX/186;->b(II)V

    .line 1744235
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v2}, LX/186;->b(II)V

    .line 1744236
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v3}, LX/186;->b(II)V

    .line 1744237
    invoke-virtual {p0}, LX/186;->d()I

    move-result v0

    .line 1744238
    invoke-virtual {p0, v0}, LX/186;->d(I)V

    goto :goto_0

    .line 1744239
    :cond_1
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1744240
    const/4 p1, 0x1

    invoke-virtual {p0, p1}, LX/186;->c(I)V

    .line 1744241
    invoke-virtual {p0, v4, v5}, LX/186;->b(II)V

    .line 1744242
    invoke-virtual {p0}, LX/186;->d()I

    move-result v4

    .line 1744243
    invoke-virtual {p0, v4}, LX/186;->d(I)V

    goto :goto_1
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLImageOverlay;)LX/5QV;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1744244
    if-nez p0, :cond_1

    .line 1744245
    :cond_0
    :goto_0
    return-object v2

    .line 1744246
    :cond_1
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1744247
    invoke-static {v0, p0}, LX/B5O;->a(LX/186;Lcom/facebook/graphql/model/GraphQLImageOverlay;)I

    move-result v1

    .line 1744248
    if-eqz v1, :cond_0

    .line 1744249
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1744250
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1744251
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1744252
    new-instance v0, LX/15i;

    const/4 v4, 0x1

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1744253
    instance-of v1, p0, Lcom/facebook/flatbuffers/Flattenable;

    if-eqz v1, :cond_2

    .line 1744254
    const-string v1, "ImageOverlayConversionHelper.getImageOverlayFields"

    invoke-virtual {v0, v1, p0}, LX/15i;->a(Ljava/lang/String;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1744255
    :cond_2
    new-instance v2, Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    invoke-direct {v2, v0}, Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;-><init>(LX/15i;)V

    goto :goto_0
.end method
