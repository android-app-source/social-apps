.class public final LX/AlR;
.super LX/1NN;
.source ""


# instance fields
.field public final synthetic a:LX/AlS;


# direct methods
.method public constructor <init>(LX/AlS;)V
    .locals 0

    .prologue
    .line 1709695
    iput-object p1, p0, LX/AlR;->a:LX/AlS;

    invoke-direct {p0}, LX/1NN;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 3

    .prologue
    .line 1709696
    check-cast p1, LX/1Nd;

    .line 1709697
    iget-object v0, p0, LX/AlR;->a:LX/AlS;

    iget-object v0, v0, LX/AlS;->n:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v0, :cond_1

    .line 1709698
    :cond_0
    :goto_0
    return-void

    .line 1709699
    :cond_1
    iget-object v0, p0, LX/AlR;->a:LX/AlS;

    iget-object v1, v0, LX/AlS;->c:LX/189;

    iget-object v0, p0, LX/AlR;->a:LX/AlS;

    iget-object v0, v0, LX/AlS;->n:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1709700
    iget-object v2, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v2

    .line 1709701
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1, v0}, LX/189;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 1709702
    iget-object v1, p1, LX/1Nd;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p1, LX/1Nd;->a:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1709703
    invoke-static {v0}, LX/6X8;->a(Lcom/facebook/graphql/model/FeedUnit;)LX/6X8;

    move-result-object v0

    iget-object v1, p1, LX/1Nd;->d:Lcom/facebook/graphql/enums/StoryVisibility;

    invoke-virtual {v0, v1}, LX/6X8;->a(Lcom/facebook/graphql/enums/StoryVisibility;)LX/6X8;

    move-result-object v0

    iget v1, p1, LX/1Nd;->e:I

    invoke-virtual {v0, v1}, LX/6X8;->a(I)LX/6X8;

    move-result-object v0

    invoke-virtual {v0}, LX/6X8;->a()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1709704
    iget-object v1, p0, LX/AlR;->a:LX/AlS;

    iget-object v1, v1, LX/AlS;->f:LX/0QK;

    invoke-interface {v1, v0}, LX/0QK;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
