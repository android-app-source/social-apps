.class public LX/AzD;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Zb;

.field private final b:LX/0SG;

.field private c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Zb;LX/0SG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1732267
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1732268
    iput-object p1, p0, LX/AzD;->a:LX/0Zb;

    .line 1732269
    iput-object p2, p0, LX/AzD;->b:LX/0SG;

    .line 1732270
    return-void
.end method

.method public static a(LX/AzD;Ljava/lang/String;ILcom/facebook/friendsharing/souvenirs/models/SouvenirModel;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 6

    .prologue
    .line 1732271
    iget-object v0, p0, LX/AzD;->b:LX/0SG;

    invoke-static {p3, v0}, Lcom/facebook/friendsharing/souvenirs/util/SouvenirLoggingParameters;->a(Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;LX/0SG;)Lcom/facebook/friendsharing/souvenirs/util/SouvenirLoggingParameters;

    move-result-object v0

    .line 1732272
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v2, "souvenirs"

    .line 1732273
    iput-object v2, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1732274
    move-object v1, v1

    .line 1732275
    sget-object v2, LX/AzC;->PHOTOS_COUNT:LX/AzC;

    invoke-virtual {v2}, LX/AzC;->getParamKey()Ljava/lang/String;

    move-result-object v2

    iget v3, v0, Lcom/facebook/friendsharing/souvenirs/util/SouvenirLoggingParameters;->a:I

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    sget-object v2, LX/AzC;->VIDEOS_COUNT:LX/AzC;

    invoke-virtual {v2}, LX/AzC;->getParamKey()Ljava/lang/String;

    move-result-object v2

    iget v3, v0, Lcom/facebook/friendsharing/souvenirs/util/SouvenirLoggingParameters;->b:I

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    sget-object v2, LX/AzC;->BURST_COUNT:LX/AzC;

    invoke-virtual {v2}, LX/AzC;->getParamKey()Ljava/lang/String;

    move-result-object v2

    iget v3, v0, Lcom/facebook/friendsharing/souvenirs/util/SouvenirLoggingParameters;->c:I

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    sget-object v2, LX/AzC;->TILE_COUNT:LX/AzC;

    invoke-virtual {v2}, LX/AzC;->getParamKey()Ljava/lang/String;

    move-result-object v2

    iget v3, v0, Lcom/facebook/friendsharing/souvenirs/util/SouvenirLoggingParameters;->d:I

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    sget-object v2, LX/AzC;->TOTAL_ASSETS_COUNT:LX/AzC;

    invoke-virtual {v2}, LX/AzC;->getParamKey()Ljava/lang/String;

    move-result-object v2

    iget v3, v0, Lcom/facebook/friendsharing/souvenirs/util/SouvenirLoggingParameters;->e:I

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    sget-object v2, LX/AzC;->TOTAL_ASSETS_WITHIN_BURSTS_COUNT:LX/AzC;

    invoke-virtual {v2}, LX/AzC;->getParamKey()Ljava/lang/String;

    move-result-object v2

    iget v3, v0, Lcom/facebook/friendsharing/souvenirs/util/SouvenirLoggingParameters;->f:I

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    sget-object v2, LX/AzC;->TIME_SINCE_STORY:LX/AzC;

    invoke-virtual {v2}, LX/AzC;->getParamKey()Ljava/lang/String;

    move-result-object v2

    iget-wide v4, v0, Lcom/facebook/friendsharing/souvenirs/util/SouvenirLoggingParameters;->g:J

    invoke-virtual {v1, v2, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    sget-object v1, LX/AzC;->SOUVENIR_UNIQUE_ID:LX/AzC;

    invoke-virtual {v1}, LX/AzC;->getParamKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;->a()Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 1732276
    sget-object v0, LX/AzB;->PICKER_PREVIEW_SEEN:LX/AzB;

    invoke-virtual {v0}, LX/AzB;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1732277
    invoke-virtual {p3}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;->a()Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirMetadata;->e()Ljava/util/Map;

    move-result-object v2

    .line 1732278
    if-eqz v2, :cond_0

    .line 1732279
    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1732280
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, LX/AzC;->CLASSIFIER_TAG:LX/AzC;

    invoke-virtual {v5}, LX/AzC;->getParamKey()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v4, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    goto :goto_0

    .line 1732281
    :cond_0
    const/4 v0, -0x1

    if-eq p2, v0, :cond_1

    .line 1732282
    sget-object v0, LX/AzC;->CURRENT_INDEX:LX/AzC;

    invoke-virtual {v0}, LX/AzC;->getParamKey()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1732283
    :cond_1
    return-object v1
.end method

.method public static a(LX/AzD;Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 1

    .prologue
    .line 1732284
    iget-object v0, p0, LX/AzD;->c:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1732285
    iget-object v0, p0, LX/AzD;->c:Ljava/lang/String;

    .line 1732286
    iput-object v0, p1, Lcom/facebook/analytics/logger/HoneyClientEvent;->f:Ljava/lang/String;

    .line 1732287
    :cond_0
    iget-object v0, p0, LX/AzD;->a:LX/0Zb;

    invoke-interface {v0, p1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1732288
    return-void
.end method

.method public static b(LX/0QB;)LX/AzD;
    .locals 3

    .prologue
    .line 1732289
    new-instance v2, LX/AzD;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v1

    check-cast v1, LX/0SG;

    invoke-direct {v2, v0, v1}, LX/AzD;-><init>(LX/0Zb;LX/0SG;)V

    .line 1732290
    return-object v2
.end method


# virtual methods
.method public final b(LX/Ayb;)V
    .locals 3

    .prologue
    .line 1732291
    sget-object v0, LX/AzB;->PROMPT_DISPLAYED:LX/AzB;

    invoke-virtual {v0}, LX/AzB;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, -0x1

    iget-object v2, p1, LX/Ayb;->a:Lcom/facebook/friendsharing/souvenirs/models/SouvenirModel;

    invoke-static {p0, v0, v1, v2}, LX/AzD;->a(LX/AzD;Ljava/lang/String;ILcom/facebook/friendsharing/souvenirs/models/SouvenirModel;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    invoke-static {p0, v0}, LX/AzD;->a(LX/AzD;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1732292
    return-void
.end method
