.class public final LX/BDC;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Lcom/facebook/graphql/enums/GraphQLStorySeenState;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1762696
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationStorySeenStateMutationFieldsModel;
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v6, 0x0

    const/4 v2, 0x0

    .line 1762697
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1762698
    iget-object v1, p0, LX/BDC;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1762699
    iget-object v3, p0, LX/BDC;->b:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    invoke-virtual {v0, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    .line 1762700
    const/4 v5, 0x2

    invoke-virtual {v0, v5}, LX/186;->c(I)V

    .line 1762701
    invoke-virtual {v0, v6, v1}, LX/186;->b(II)V

    .line 1762702
    invoke-virtual {v0, v4, v3}, LX/186;->b(II)V

    .line 1762703
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 1762704
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1762705
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 1762706
    invoke-virtual {v1, v6}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1762707
    new-instance v0, LX/15i;

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1762708
    new-instance v1, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationStorySeenStateMutationFieldsModel;

    invoke-direct {v1, v0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationStorySeenStateMutationFieldsModel;-><init>(LX/15i;)V

    .line 1762709
    return-object v1
.end method
