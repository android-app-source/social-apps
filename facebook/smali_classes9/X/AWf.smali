.class public LX/AWf;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/user/model/User;


# direct methods
.method public constructor <init>(Lcom/facebook/user/model/User;)V
    .locals 0
    .param p1    # Lcom/facebook/user/model/User;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1683186
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1683187
    iput-object p1, p0, LX/AWf;->a:Lcom/facebook/user/model/User;

    .line 1683188
    return-void
.end method

.method public static a(LX/0QB;)LX/AWf;
    .locals 1

    .prologue
    .line 1683199
    invoke-static {p0}, LX/AWf;->b(LX/0QB;)LX/AWf;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/AWf;
    .locals 2

    .prologue
    .line 1683197
    new-instance v1, LX/AWf;

    invoke-static {p0}, LX/0XE;->b(LX/0QB;)Lcom/facebook/user/model/User;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    invoke-direct {v1, v0}, LX/AWf;-><init>(Lcom/facebook/user/model/User;)V

    .line 1683198
    return-object v1
.end method


# virtual methods
.method public final a(Lcom/facebook/ipc/composer/intent/ComposerTargetData;IZ)LX/8t9;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1683189
    if-eqz p1, :cond_1

    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    sget-object v1, LX/2rw;->PAGE:LX/2rw;

    if-ne v0, v1, :cond_1

    .line 1683190
    new-instance v0, LX/0XI;

    invoke-direct {v0}, LX/0XI;-><init>()V

    sget-object v1, LX/0XG;->FACEBOOK:LX/0XG;

    iget-wide v2, p1, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0XI;->a(LX/0XG;Ljava/lang/String;)LX/0XI;

    move-result-object v0

    .line 1683191
    if-nez p3, :cond_0

    .line 1683192
    new-instance v1, Lcom/facebook/user/model/PicSquareUrlWithSize;

    iget-object v2, p1, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetProfilePicUrl:Ljava/lang/String;

    invoke-direct {v1, p2, v2}, Lcom/facebook/user/model/PicSquareUrlWithSize;-><init>(ILjava/lang/String;)V

    .line 1683193
    new-instance v2, Lcom/facebook/user/model/PicSquare;

    invoke-direct {v2, v1, v4, v4}, Lcom/facebook/user/model/PicSquare;-><init>(Lcom/facebook/user/model/PicSquareUrlWithSize;Lcom/facebook/user/model/PicSquareUrlWithSize;Lcom/facebook/user/model/PicSquareUrlWithSize;)V

    .line 1683194
    iput-object v2, v0, LX/0XI;->p:Lcom/facebook/user/model/PicSquare;

    .line 1683195
    :cond_0
    invoke-virtual {v0}, LX/0XI;->aj()Lcom/facebook/user/model/User;

    move-result-object v0

    invoke-static {v0}, LX/8t9;->a(Lcom/facebook/user/model/User;)LX/8t9;

    move-result-object v0

    .line 1683196
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, LX/AWf;->a:Lcom/facebook/user/model/User;

    invoke-static {v0}, LX/8t9;->a(Lcom/facebook/user/model/User;)LX/8t9;

    move-result-object v0

    goto :goto_0
.end method
