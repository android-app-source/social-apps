.class public final LX/CZu;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/model/GraphQLComment;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;)V
    .locals 0

    .prologue
    .line 1916628
    iput-object p1, p0, LX/CZu;->a:Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1916629
    iget-object v0, p0, LX/CZu;->a:Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;

    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->ds_()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1916630
    :cond_0
    :goto_0
    return-void

    .line 1916631
    :cond_1
    iget-object v0, p0, LX/CZu;->a:Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;

    const v1, 0x7f08006f

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1916632
    iget-object v1, p0, LX/CZu;->a:Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;

    iget-object v1, v1, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->D:LX/62O;

    iget-object v2, p0, LX/CZu;->a:Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;

    iget-object v2, v2, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->B:LX/1DI;

    invoke-virtual {v1, v0, v2}, LX/62O;->a(Ljava/lang/String;LX/1DI;)V

    .line 1916633
    iget-object v0, p0, LX/CZu;->a:Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;

    iget-object v0, v0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->s:Lcom/facebook/widget/FbSwipeRefreshLayout;

    if-eqz v0, :cond_2

    .line 1916634
    iget-object v0, p0, LX/CZu;->a:Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;

    iget-object v0, v0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->s:Lcom/facebook/widget/FbSwipeRefreshLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 1916635
    :cond_2
    iget-object v0, p0, LX/CZu;->a:Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;

    iget-object v0, v0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->F:Lcom/facebook/graphql/model/GraphQLComment;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/CZu;->a:Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;

    iget-object v0, v0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->k:LX/1ry;

    invoke-virtual {v0}, LX/1ry;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1916636
    iget-object v0, p0, LX/CZu;->a:Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;

    iget-object v1, p0, LX/CZu;->a:Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;

    iget-object v1, v1, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->F:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-static {v0, v1}, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->c(Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;Lcom/facebook/graphql/model/GraphQLComment;)V

    goto :goto_0
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 1916637
    check-cast p1, Lcom/facebook/graphql/model/GraphQLComment;

    .line 1916638
    iget-object v0, p0, LX/CZu;->a:Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;

    invoke-static {v0, p1}, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->c(Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;Lcom/facebook/graphql/model/GraphQLComment;)V

    .line 1916639
    return-void
.end method
