.class public final LX/BwD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/9lZ;


# instance fields
.field public final synthetic a:LX/Bur;

.field private b:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/Bur;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1833297
    iput-object p1, p0, LX/BwD;->a:LX/Bur;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1833298
    iput-object p2, p0, LX/BwD;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1833299
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    .line 1833300
    new-instance v0, LX/4XQ;

    invoke-direct {v0}, LX/4XQ;-><init>()V

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->HIDE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 1833301
    iput-object v1, v0, LX/4XQ;->g:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    .line 1833302
    move-object v0, v0

    .line 1833303
    invoke-virtual {v0}, LX/4XQ;->a()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;

    move-result-object v1

    .line 1833304
    iget-object v0, p0, LX/BwD;->a:LX/Bur;

    iget-object v0, v0, LX/Bur;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1dv;

    iget-object v2, p0, LX/BwD;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p0, LX/BwD;->a:LX/Bur;

    iget-object v3, v3, LX/Bur;->y:LX/1Sp;

    invoke-interface {v3}, LX/1Sp;->a()LX/0wD;

    move-result-object v3

    invoke-virtual {v3}, LX/0wD;->stringValueOf()Ljava/lang/String;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {v0, v2, v1, v3, v4}, LX/1dv;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1833305
    return-void
.end method
