.class public abstract LX/BPB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/BP9;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/timeline/data/DataSource",
        "<TT;>;",
        "LX/BP9;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# instance fields
.field public a:I

.field public b:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field public c:I

.field public d:I

.field public e:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1780762
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1780763
    iput v0, p0, LX/BPB;->a:I

    .line 1780764
    iput v0, p0, LX/BPB;->d:I

    .line 1780765
    iput v0, p0, LX/BPB;->e:I

    return-void
.end method

.method private static a(I)I
    .locals 3

    .prologue
    .line 1780778
    packed-switch p0, :pswitch_data_0

    .line 1780779
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected DataType: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1780780
    :pswitch_0
    const/4 v0, 0x2

    .line 1780781
    :goto_0
    return v0

    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a()LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 1780777
    iget-object v0, p0, LX/BPB;->b:Ljava/lang/Object;

    invoke-static {v0}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/Object;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;I)V"
        }
    .end annotation

    .prologue
    .line 1780772
    iput-object p1, p0, LX/BPB;->b:Ljava/lang/Object;

    .line 1780773
    iput p2, p0, LX/BPB;->c:I

    .line 1780774
    invoke-static {p2}, LX/BPB;->a(I)I

    move-result v0

    iput v0, p0, LX/BPB;->d:I

    .line 1780775
    iget v0, p0, LX/BPB;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/BPB;->a:I

    .line 1780776
    return-void
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 1780771
    return-void
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 1780770
    iget-object v0, p0, LX/BPB;->b:Ljava/lang/Object;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 1780768
    const/4 v0, 0x1

    iput v0, p0, LX/BPB;->e:I

    .line 1780769
    return-void
.end method

.method public final h()V
    .locals 1

    .prologue
    .line 1780766
    const/4 v0, 0x0

    iput v0, p0, LX/BPB;->e:I

    .line 1780767
    return-void
.end method
