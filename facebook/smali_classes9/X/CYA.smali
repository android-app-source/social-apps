.class public final enum LX/CYA;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/CYA;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/CYA;

.field public static final enum NARROW:LX/CYA;

.field public static final enum WIDE:LX/CYA;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1910827
    new-instance v0, LX/CYA;

    const-string v1, "WIDE"

    invoke-direct {v0, v1, v2}, LX/CYA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CYA;->WIDE:LX/CYA;

    .line 1910828
    new-instance v0, LX/CYA;

    const-string v1, "NARROW"

    invoke-direct {v0, v1, v3}, LX/CYA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CYA;->NARROW:LX/CYA;

    .line 1910829
    const/4 v0, 0x2

    new-array v0, v0, [LX/CYA;

    sget-object v1, LX/CYA;->WIDE:LX/CYA;

    aput-object v1, v0, v2

    sget-object v1, LX/CYA;->NARROW:LX/CYA;

    aput-object v1, v0, v3

    sput-object v0, LX/CYA;->$VALUES:[LX/CYA;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1910830
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/CYA;
    .locals 1

    .prologue
    .line 1910831
    const-class v0, LX/CYA;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/CYA;

    return-object v0
.end method

.method public static values()[LX/CYA;
    .locals 1

    .prologue
    .line 1910832
    sget-object v0, LX/CYA;->$VALUES:[LX/CYA;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/CYA;

    return-object v0
.end method
