.class public LX/B0k;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/B0k;


# instance fields
.field public final a:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1734190
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1734191
    iput-object p1, p0, LX/B0k;->a:LX/0Zb;

    .line 1734192
    return-void
.end method

.method public static a(LX/0QB;)LX/B0k;
    .locals 4

    .prologue
    .line 1734177
    sget-object v0, LX/B0k;->b:LX/B0k;

    if-nez v0, :cond_1

    .line 1734178
    const-class v1, LX/B0k;

    monitor-enter v1

    .line 1734179
    :try_start_0
    sget-object v0, LX/B0k;->b:LX/B0k;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1734180
    if-eqz v2, :cond_0

    .line 1734181
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1734182
    new-instance p0, LX/B0k;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-direct {p0, v3}, LX/B0k;-><init>(LX/0Zb;)V

    .line 1734183
    move-object v0, p0

    .line 1734184
    sput-object v0, LX/B0k;->b:LX/B0k;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1734185
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1734186
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1734187
    :cond_1
    sget-object v0, LX/B0k;->b:LX/B0k;

    return-object v0

    .line 1734188
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1734189
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;LX/21D;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 3

    .prologue
    .line 1734172
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v0, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1734173
    if-eqz p1, :cond_0

    .line 1734174
    const-string v1, "story_id"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1734175
    :cond_0
    const-string v1, "surface"

    invoke-virtual {p2}, LX/21D;->getAnalyticsName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1734176
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/21D;)V
    .locals 2

    .prologue
    .line 1734166
    iget-object v0, p0, LX/B0k;->a:LX/0Zb;

    const-string v1, "composer_group_sale_post_intercept"

    invoke-static {v1, p1, p2}, LX/B0k;->a(Ljava/lang/String;Ljava/lang/String;LX/21D;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1734167
    return-void
.end method

.method public final b(Ljava/lang/String;LX/21D;)V
    .locals 2

    .prologue
    .line 1734170
    iget-object v0, p0, LX/B0k;->a:LX/0Zb;

    const-string v1, "composer_group_sale_post_intercept_accepted"

    invoke-static {v1, p1, p2}, LX/B0k;->a(Ljava/lang/String;Ljava/lang/String;LX/21D;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1734171
    return-void
.end method

.method public final c(Ljava/lang/String;LX/21D;)V
    .locals 2

    .prologue
    .line 1734168
    iget-object v0, p0, LX/B0k;->a:LX/0Zb;

    const-string v1, "composer_group_sale_post_intercept_declined"

    invoke-static {v1, p1, p2}, LX/B0k;->a(Ljava/lang/String;Ljava/lang/String;LX/21D;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1734169
    return-void
.end method
