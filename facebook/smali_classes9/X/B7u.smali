.class public final LX/B7u;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/app/DatePickerDialog$OnDateSetListener;


# instance fields
.field public final synthetic a:LX/B7v;


# direct methods
.method public constructor <init>(LX/B7v;)V
    .locals 0

    .prologue
    .line 1748261
    iput-object p1, p0, LX/B7u;->a:LX/B7v;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onDateSet(Landroid/widget/DatePicker;III)V
    .locals 10

    .prologue
    .line 1748262
    iget-object v0, p0, LX/B7u;->a:LX/B7v;

    iget-object v0, v0, LX/B7v;->h:Ljava/util/Calendar;

    if-eqz v0, :cond_0

    .line 1748263
    iget-object v0, p0, LX/B7u;->a:LX/B7v;

    .line 1748264
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v4

    iput-object v4, v0, LX/B7v;->h:Ljava/util/Calendar;

    .line 1748265
    iget-object v4, v0, LX/B7v;->h:Ljava/util/Calendar;

    invoke-virtual {v4, p2, p3, p4}, Ljava/util/Calendar;->set(III)V

    .line 1748266
    iget-object v5, v0, LX/B7v;->f:Lcom/facebook/resources/ui/FbButton;

    iget-object v4, v0, LX/B7v;->b:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/11S;

    sget-object v6, LX/1lB;->DATE_PICKER_STYLE:LX/1lB;

    iget-object v7, v0, LX/B7v;->h:Ljava/util/Calendar;

    invoke-virtual {v7}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v8

    invoke-interface {v4, v6, v8, v9}, LX/11S;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 1748267
    :cond_0
    iget-object v0, p0, LX/B7u;->a:LX/B7v;

    iget-object v0, v0, LX/B7v;->e:LX/B7w;

    if-eqz v0, :cond_1

    .line 1748268
    iget-object v0, p0, LX/B7u;->a:LX/B7v;

    iget-object v0, v0, LX/B7v;->d:Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->j()Ljava/lang/String;

    iget-object v0, p0, LX/B7u;->a:LX/B7v;

    iget-object v0, v0, LX/B7v;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11S;

    sget-object v1, LX/1lB;->DATE_PICKER_STYLE:LX/1lB;

    iget-object v2, p0, LX/B7u;->a:LX/B7v;

    iget-object v2, v2, LX/B7v;->h:Ljava/util/Calendar;

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, LX/11S;->a(LX/1lB;J)Ljava/lang/String;

    .line 1748269
    :cond_1
    return-void
.end method
