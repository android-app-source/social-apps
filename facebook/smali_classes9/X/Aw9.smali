.class public final LX/Aw9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Arb;


# instance fields
.field public final synthetic a:LX/AwC;


# direct methods
.method public constructor <init>(LX/AwC;)V
    .locals 0

    .prologue
    .line 1725018
    iput-object p1, p0, LX/Aw9;->a:LX/AwC;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1725019
    iget-object v0, p0, LX/Aw9;->a:LX/AwC;

    invoke-static {v0}, LX/AwC;->i(LX/AwC;)V

    .line 1725020
    return-void
.end method

.method public final b()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1725021
    iget-object v0, p0, LX/Aw9;->a:LX/AwC;

    iget-object v2, p0, LX/Aw9;->a:LX/AwC;

    invoke-static {v2}, LX/AwC;->m(LX/AwC;)LX/0UE;

    move-result-object v2

    iput-object v2, v0, LX/AwC;->b:LX/0UE;

    .line 1725022
    iget-object v0, p0, LX/Aw9;->a:LX/AwC;

    iget-object v2, v0, LX/AwC;->c:LX/Arh;

    new-instance v3, LX/BV8;

    invoke-direct {v3}, LX/BV8;-><init>()V

    iget-object v0, p0, LX/Aw9;->a:LX/AwC;

    iget-object v0, v0, LX/AwC;->a:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/Aw9;->a:LX/AwC;

    iget-boolean v0, v0, LX/AwC;->l:Z

    if-eqz v0, :cond_1

    const-string v0, "0"

    .line 1725023
    :goto_0
    iput-object v0, v3, LX/BV8;->a:Ljava/lang/String;

    .line 1725024
    move-object v0, v3

    .line 1725025
    iget-object v3, p0, LX/Aw9;->a:LX/AwC;

    iget-object v3, v3, LX/AwC;->b:LX/0UE;

    invoke-virtual {v3}, LX/0UE;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    new-instance v1, Ljava/util/ArrayList;

    iget-object v3, p0, LX/Aw9;->a:LX/AwC;

    iget-object v3, v3, LX/AwC;->b:LX/0UE;

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1725026
    :cond_0
    iput-object v1, v0, LX/BV8;->b:Ljava/util/List;

    .line 1725027
    move-object v0, v0

    .line 1725028
    iget-object v1, p0, LX/Aw9;->a:LX/AwC;

    iget-object v1, v1, LX/AwC;->d:LX/BVN;

    .line 1725029
    iput-object v1, v0, LX/BV8;->c:LX/BVN;

    .line 1725030
    move-object v0, v0

    .line 1725031
    invoke-virtual {v0}, LX/BV8;->a()LX/BVA;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/Arh;->a(LX/BVA;)V

    .line 1725032
    return-void

    .line 1725033
    :cond_1
    iget-object v0, p0, LX/Aw9;->a:LX/AwC;

    iget-object v0, v0, LX/AwC;->a:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;->e()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method
