.class public LX/Aw8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/89q;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;",
        "LX/89q",
        "<",
        "Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig;",
        ">;"
    }
.end annotation


# static fields
.field public static final f:Ljava/lang/String;


# instance fields
.field public a:LX/1HI;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/0Sh;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:LX/03V;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1725005
    const-class v0, LX/Aw8;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Aw8;->f:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1725006
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1725007
    return-void
.end method

.method public static b(LX/0QB;)LX/Aw8;
    .locals 6

    .prologue
    .line 1725008
    new-instance v0, LX/Aw8;

    invoke-direct {v0}, LX/Aw8;-><init>()V

    .line 1725009
    invoke-static {p0}, LX/1Fo;->a(LX/0QB;)LX/1HI;

    move-result-object v1

    check-cast v1, LX/1HI;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v4

    check-cast v4, LX/0Sh;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    .line 1725010
    iput-object v1, v0, LX/Aw8;->a:LX/1HI;

    iput-object v2, v0, LX/Aw8;->b:Ljava/util/concurrent/ExecutorService;

    iput-object v3, v0, LX/Aw8;->c:Ljava/util/concurrent/ExecutorService;

    iput-object v4, v0, LX/Aw8;->d:LX/0Sh;

    iput-object v5, v0, LX/Aw8;->e:LX/03V;

    .line 1725011
    return-object v0
.end method

.method public static b(LX/Aw8;Landroid/net/Uri;Ljava/util/List;Ljava/util/List;LX/89p;)V
    .locals 2
    .param p3    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "LX/89p;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1725012
    invoke-interface {p2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1725013
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v1

    if-ne v0, v1, :cond_0

    if-eqz p4, :cond_0

    .line 1725014
    iget-object v0, p0, LX/Aw8;->d:LX/0Sh;

    new-instance v1, Lcom/facebook/friendsharing/inspiration/particle/InspirationParticleEffectAssetLoader$3;

    invoke-direct {v1, p0, p4}, Lcom/facebook/friendsharing/inspiration/particle/InspirationParticleEffectAssetLoader$3;-><init>(LX/Aw8;LX/89p;)V

    invoke-virtual {v0, v1}, LX/0Sh;->a(Ljava/lang/Runnable;)V

    .line 1725015
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig;LX/89p;)V
    .locals 3
    .param p2    # LX/89p;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1725016
    iget-object v0, p0, LX/Aw8;->c:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/friendsharing/inspiration/particle/InspirationParticleEffectAssetLoader$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/facebook/friendsharing/inspiration/particle/InspirationParticleEffectAssetLoader$1;-><init>(LX/Aw8;Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig;LX/89p;)V

    const v2, -0x4a1331da

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1725017
    return-void
.end method
