.class public final LX/AcO;
.super LX/2oa;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2oa",
        "<",
        "LX/2ou;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/facecastdisplay/LiveEventsPlugin;


# direct methods
.method public constructor <init>(Lcom/facebook/facecastdisplay/LiveEventsPlugin;)V
    .locals 0

    .prologue
    .line 1692486
    iput-object p1, p0, LX/AcO;->a:Lcom/facebook/facecastdisplay/LiveEventsPlugin;

    invoke-direct {p0}, LX/2oa;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/2ou;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1692487
    const-class v0, LX/2ou;

    return-object v0
.end method

.method public final b(LX/0b7;)V
    .locals 2

    .prologue
    .line 1692488
    check-cast p1, LX/2ou;

    .line 1692489
    iget-object v0, p0, LX/AcO;->a:Lcom/facebook/facecastdisplay/LiveEventsPlugin;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->z:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/AcO;->a:Lcom/facebook/facecastdisplay/LiveEventsPlugin;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->z:Ljava/lang/String;

    iget-object v1, p1, LX/2ou;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1692490
    :cond_0
    :goto_0
    return-void

    .line 1692491
    :cond_1
    iget-object v0, p0, LX/AcO;->a:Lcom/facebook/facecastdisplay/LiveEventsPlugin;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->o:LX/AcP;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, LX/AcP;->removeMessages(I)V

    .line 1692492
    iget-object v0, p1, LX/2ou;->b:LX/2qV;

    sget-object v1, LX/2qV;->PLAYBACK_COMPLETE:LX/2qV;

    if-ne v0, v1, :cond_4

    iget-object v0, p0, LX/AcO;->a:Lcom/facebook/facecastdisplay/LiveEventsPlugin;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->n:LX/37Y;

    iget-object v1, p0, LX/AcO;->a:Lcom/facebook/facecastdisplay/LiveEventsPlugin;

    iget-object v1, v1, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->z:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/37Y;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1692493
    iget-object v0, p0, LX/AcO;->a:Lcom/facebook/facecastdisplay/LiveEventsPlugin;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->c:LX/Aby;

    invoke-virtual {v0}, LX/Aby;->f()V

    .line 1692494
    iget-object v0, p0, LX/AcO;->a:Lcom/facebook/facecastdisplay/LiveEventsPlugin;

    iget-boolean v0, v0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->w:Z

    if-eqz v0, :cond_2

    .line 1692495
    iget-object v0, p0, LX/AcO;->a:Lcom/facebook/facecastdisplay/LiveEventsPlugin;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->c:LX/Aby;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, LX/Aby;->b(I)V

    .line 1692496
    :cond_2
    iget-object v0, p0, LX/AcO;->a:Lcom/facebook/facecastdisplay/LiveEventsPlugin;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->e:LX/Agt;

    .line 1692497
    iget-object v1, v0, LX/Agt;->a:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;

    if-eqz v1, :cond_3

    iget-object v1, v0, LX/Agt;->a:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->isVisible()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1692498
    iget-object v1, v0, LX/Agt;->a:Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;

    invoke-virtual {v1}, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->d()V

    .line 1692499
    :cond_3
    goto :goto_0

    .line 1692500
    :cond_4
    iget-object v0, p1, LX/2ou;->b:LX/2qV;

    sget-object v1, LX/2qV;->PLAYING:LX/2qV;

    if-ne v0, v1, :cond_0

    .line 1692501
    iget-object v0, p0, LX/AcO;->a:Lcom/facebook/facecastdisplay/LiveEventsPlugin;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->g()V

    goto :goto_0
.end method
