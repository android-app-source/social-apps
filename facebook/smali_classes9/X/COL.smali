.class public final LX/COL;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/COL;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/COJ;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/COM;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1882892
    const/4 v0, 0x0

    sput-object v0, LX/COL;->a:LX/COL;

    .line 1882893
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/COL;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1882894
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1882895
    new-instance v0, LX/COM;

    invoke-direct {v0}, LX/COM;-><init>()V

    iput-object v0, p0, LX/COL;->c:LX/COM;

    .line 1882896
    return-void
.end method

.method public static declared-synchronized q()LX/COL;
    .locals 2

    .prologue
    .line 1882897
    const-class v1, LX/COL;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/COL;->a:LX/COL;

    if-nez v0, :cond_0

    .line 1882898
    new-instance v0, LX/COL;

    invoke-direct {v0}, LX/COL;-><init>()V

    sput-object v0, LX/COL;->a:LX/COL;

    .line 1882899
    :cond_0
    sget-object v0, LX/COL;->a:LX/COL;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 1882900
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 1882901
    check-cast p2, LX/COK;

    .line 1882902
    iget-object v0, p2, LX/COK;->a:LX/CNb;

    iget-object v1, p2, LX/COK;->b:LX/CNq;

    iget-object v2, p2, LX/COK;->c:Ljava/util/List;

    const/4 v3, 0x0

    .line 1882903
    const-string v4, "twe"

    invoke-virtual {v0, v4}, LX/CNb;->d(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    instance-of v4, v4, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v4, :cond_4

    .line 1882904
    const/4 v3, 0x0

    .line 1882905
    new-instance v4, LX/COX;

    invoke-direct {v4}, LX/COX;-><init>()V

    .line 1882906
    sget-object v5, LX/COY;->b:LX/0Zi;

    invoke-virtual {v5}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/COW;

    .line 1882907
    if-nez v5, :cond_0

    .line 1882908
    new-instance v5, LX/COW;

    invoke-direct {v5}, LX/COW;-><init>()V

    .line 1882909
    :cond_0
    invoke-static {v5, p1, v3, v3, v4}, LX/COW;->a$redex0(LX/COW;LX/1De;IILX/COX;)V

    .line 1882910
    move-object v4, v5

    .line 1882911
    move-object v3, v4

    .line 1882912
    move-object v3, v3

    .line 1882913
    iget-object v4, v3, LX/COW;->a:LX/COX;

    iput-object v0, v4, LX/COX;->a:LX/CNb;

    .line 1882914
    iget-object v4, v3, LX/COW;->d:Ljava/util/BitSet;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Ljava/util/BitSet;->set(I)V

    .line 1882915
    move-object v3, v3

    .line 1882916
    iget-object v4, v3, LX/COW;->a:LX/COX;

    iput-object v1, v4, LX/COX;->b:LX/CNq;

    .line 1882917
    iget-object v4, v3, LX/COW;->d:Ljava/util/BitSet;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Ljava/util/BitSet;->set(I)V

    .line 1882918
    move-object v4, v3

    .line 1882919
    const-string v3, "twe"

    invoke-virtual {v0, v3}, LX/CNb;->d(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1882920
    iget-object v5, v4, LX/COW;->a:LX/COX;

    iput-object v3, v5, LX/COX;->c:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1882921
    iget-object v5, v4, LX/COW;->d:Ljava/util/BitSet;

    const/4 p0, 0x2

    invoke-virtual {v5, p0}, Ljava/util/BitSet;->set(I)V

    .line 1882922
    move-object v3, v4

    .line 1882923
    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    .line 1882924
    :goto_0
    const-string v4, "touch-up-inside-actions"

    invoke-static {v1, v0, v2, v4}, LX/CPx;->a(LX/CNc;LX/CNb;Ljava/util/List;Ljava/lang/String;)LX/CNe;

    move-result-object v4

    .line 1882925
    if-eqz v4, :cond_1

    .line 1882926
    const v5, -0x51ca2c36

    const/4 p0, 0x1

    new-array p0, p0, [Ljava/lang/Object;

    const/4 p2, 0x0

    aput-object v4, p0, p2

    invoke-static {p1, v5, p0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v5

    move-object v4, v5

    .line 1882927
    invoke-interface {v3, v4}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    .line 1882928
    :cond_1
    const-string v4, "long-press-actions"

    invoke-static {v1, v0, v2, v4}, LX/CPx;->a(LX/CNc;LX/CNb;Ljava/util/List;Ljava/lang/String;)LX/CNe;

    move-result-object v4

    .line 1882929
    if-eqz v4, :cond_2

    .line 1882930
    const v5, -0x6cc2a199

    const/4 p0, 0x1

    new-array p0, p0, [Ljava/lang/Object;

    const/4 p2, 0x0

    aput-object v4, p0, p2

    invoke-static {p1, v5, p0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v5

    move-object v4, v5

    .line 1882931
    invoke-interface {v3, v4}, LX/1Di;->b(LX/1dQ;)LX/1Di;

    .line 1882932
    :cond_2
    invoke-static {v3, p1, v0, v2}, LX/CPx;->a(LX/1Di;LX/1De;LX/CNb;Ljava/util/List;)LX/1Dg;

    move-result-object v3

    :cond_3
    move-object v0, v3

    .line 1882933
    return-object v0

    .line 1882934
    :cond_4
    const-string v4, "plain-text"

    invoke-virtual {v0, v4, v3}, LX/CNb;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 1882935
    const/4 v4, 0x0

    .line 1882936
    new-instance v5, LX/COS;

    invoke-direct {v5}, LX/COS;-><init>()V

    .line 1882937
    sget-object p0, LX/COT;->b:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/COR;

    .line 1882938
    if-nez p0, :cond_5

    .line 1882939
    new-instance p0, LX/COR;

    invoke-direct {p0}, LX/COR;-><init>()V

    .line 1882940
    :cond_5
    invoke-static {p0, p1, v4, v4, v5}, LX/COR;->a$redex0(LX/COR;LX/1De;IILX/COS;)V

    .line 1882941
    move-object v5, p0

    .line 1882942
    move-object v4, v5

    .line 1882943
    move-object v4, v4

    .line 1882944
    iget-object v5, v4, LX/COR;->a:LX/COS;

    iput-object v0, v5, LX/COS;->a:LX/CNb;

    .line 1882945
    iget-object v5, v4, LX/COR;->d:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v5, p0}, Ljava/util/BitSet;->set(I)V

    .line 1882946
    move-object v4, v4

    .line 1882947
    iget-object v5, v4, LX/COR;->a:LX/COS;

    iput-object v1, v5, LX/COS;->b:LX/CNc;

    .line 1882948
    iget-object v5, v4, LX/COR;->d:Ljava/util/BitSet;

    const/4 p0, 0x1

    invoke-virtual {v5, p0}, Ljava/util/BitSet;->set(I)V

    .line 1882949
    move-object v4, v4

    .line 1882950
    const-string v5, "plain-text"

    invoke-virtual {v0, v5, v3}, LX/CNb;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1882951
    iget-object v5, v4, LX/COR;->a:LX/COS;

    iput-object v3, v5, LX/COS;->c:Ljava/lang/String;

    .line 1882952
    iget-object v5, v4, LX/COR;->d:Ljava/util/BitSet;

    const/4 p0, 0x2

    invoke-virtual {v5, p0}, Ljava/util/BitSet;->set(I)V

    .line 1882953
    move-object v3, v4

    .line 1882954
    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    goto/16 :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 1882955
    invoke-static {}, LX/1dS;->b()V

    .line 1882956
    iget v0, p1, LX/1dQ;->b:I

    .line 1882957
    sparse-switch v0, :sswitch_data_0

    move-object v0, v1

    .line 1882958
    :goto_0
    return-object v0

    .line 1882959
    :sswitch_0
    iget-object v0, p1, LX/1dQ;->c:[Ljava/lang/Object;

    aget-object v0, v0, v2

    check-cast v0, LX/CNe;

    .line 1882960
    invoke-interface {v0}, LX/CNe;->a()V

    .line 1882961
    move-object v0, v1

    .line 1882962
    goto :goto_0

    .line 1882963
    :sswitch_1
    iget-object v0, p1, LX/1dQ;->c:[Ljava/lang/Object;

    aget-object v0, v0, v2

    check-cast v0, LX/CNe;

    .line 1882964
    invoke-interface {v0}, LX/CNe;->a()V

    .line 1882965
    const/4 v2, 0x1

    move v2, v2

    .line 1882966
    move v0, v2

    .line 1882967
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x6cc2a199 -> :sswitch_1
        -0x51ca2c36 -> :sswitch_0
    .end sparse-switch
.end method
