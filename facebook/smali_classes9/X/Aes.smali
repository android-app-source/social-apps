.class public LX/Aes;
.super LX/AeQ;
.source ""


# static fields
.field public static final f:Ljava/lang/String;


# instance fields
.field private final g:Ljava/util/concurrent/ExecutorService;

.field private final h:LX/0tX;

.field public final i:LX/03V;

.field public final j:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private k:J

.field public volatile l:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoAnnouncementsQueryModel;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1698273
    const-class v0, LX/Aes;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Aes;->f:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/ExecutorService;LX/0tX;LX/03V;LX/0SG;)V
    .locals 1
    .param p1    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1698294
    invoke-direct {p0, p4}, LX/AeQ;-><init>(LX/0SG;)V

    .line 1698295
    iput-object p1, p0, LX/Aes;->g:Ljava/util/concurrent/ExecutorService;

    .line 1698296
    iput-object p2, p0, LX/Aes;->h:LX/0tX;

    .line 1698297
    iput-object p3, p0, LX/Aes;->i:LX/03V;

    .line 1698298
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/Aes;->j:Ljava/util/Map;

    .line 1698299
    return-void
.end method


# virtual methods
.method public final declared-synchronized c()V
    .locals 6

    .prologue
    .line 1698276
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/AeQ;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1698277
    iget-object v0, p0, LX/Aes;->i:LX/03V;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, LX/Aes;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_startFetching"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Tried to fetch without a story id."

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1698278
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 1698279
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/AeQ;->a:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    .line 1698280
    iget-wide v2, p0, LX/Aes;->k:J

    sub-long v2, v0, v2

    const-wide/16 v4, 0xa

    cmp-long v2, v2, v4

    if-ltz v2, :cond_0

    .line 1698281
    invoke-super {p0}, LX/AeQ;->c()V

    .line 1698282
    new-instance v2, LX/6S7;

    invoke-direct {v2}, LX/6S7;-><init>()V

    move-object v2, v2

    .line 1698283
    const-string v3, "targetID"

    iget-object v4, p0, LX/AeQ;->c:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1698284
    iget-object v3, p0, LX/Aes;->h:LX/0tX;

    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v2

    invoke-virtual {v3, v2}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v2

    iput-object v2, p0, LX/Aes;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1698285
    iget-object v2, p0, LX/Aes;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v3, LX/Aer;

    invoke-direct {v3, p0}, LX/Aer;-><init>(LX/Aes;)V

    iget-object v4, p0, LX/Aes;->g:Ljava/util/concurrent/ExecutorService;

    invoke-static {v2, v3, v4}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1698286
    iput-wide v0, p0, LX/Aes;->k:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1698287
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized e()V
    .locals 2

    .prologue
    .line 1698288
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Aes;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    .line 1698289
    iget-object v0, p0, LX/Aes;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 1698290
    const/4 v0, 0x0

    iput-object v0, p0, LX/Aes;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1698291
    iget-object v0, p0, LX/Aes;->j:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1698292
    :cond_0
    monitor-exit p0

    return-void

    .line 1698293
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized f()Z
    .locals 1

    .prologue
    .line 1698275
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Aes;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Aes;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isDone()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final g()LX/AeN;
    .locals 1

    .prologue
    .line 1698274
    sget-object v0, LX/AeN;->LIVE_ANNOUNCEMENT_EVENT:LX/AeN;

    return-object v0
.end method
