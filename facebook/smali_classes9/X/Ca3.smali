.class public final LX/Ca3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1L9;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1L9",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Ca6;

.field public final synthetic b:Z

.field public final synthetic c:LX/Ca4;


# direct methods
.method public constructor <init>(LX/Ca4;LX/Ca6;Z)V
    .locals 0

    .prologue
    .line 1916972
    iput-object p1, p0, LX/Ca3;->c:LX/Ca4;

    iput-object p2, p0, LX/Ca3;->a:LX/Ca6;

    iput-boolean p3, p0, LX/Ca3;->b:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 1916973
    iget-object v0, p0, LX/Ca3;->a:LX/Ca6;

    iget-boolean v1, p0, LX/Ca3;->b:Z

    .line 1916974
    iput-boolean v1, v0, LX/Ca6;->j:Z

    .line 1916975
    iget-object v1, p0, LX/Ca3;->a:LX/Ca6;

    iget-boolean v0, p0, LX/Ca3;->b:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/Ca3;->a:LX/Ca6;

    .line 1916976
    iget v2, v0, LX/Ca6;->f:I

    move v0, v2

    .line 1916977
    add-int/lit8 v0, v0, 0x1

    .line 1916978
    :goto_0
    iput v0, v1, LX/Ca6;->f:I

    .line 1916979
    iget-object v0, p0, LX/Ca3;->a:LX/Ca6;

    .line 1916980
    iget-object v1, v0, LX/Ca6;->b:LX/Ca5;

    move-object v0, v1

    .line 1916981
    sget-object v1, LX/Ca5;->PHOTO:LX/Ca5;

    if-ne v0, v1, :cond_0

    .line 1916982
    iget-object v0, p0, LX/Ca3;->c:LX/Ca4;

    iget-object v0, v0, LX/Ca4;->b:LX/Ca0;

    iget-object v1, p0, LX/Ca3;->a:LX/Ca6;

    .line 1916983
    iget-wide v4, v1, LX/Ca6;->e:J

    move-wide v2, v4

    .line 1916984
    invoke-virtual {v0, v2, v3}, LX/Ca0;->a(J)LX/CZz;

    move-result-object v0

    .line 1916985
    if-eqz v0, :cond_0

    .line 1916986
    iget-object v1, p0, LX/Ca3;->a:LX/Ca6;

    .line 1916987
    iget-boolean v2, v1, LX/Ca6;->j:Z

    move v1, v2

    .line 1916988
    iput-boolean v1, v0, LX/CZz;->m:Z

    .line 1916989
    iget-object v1, p0, LX/Ca3;->a:LX/Ca6;

    .line 1916990
    iget v2, v1, LX/Ca6;->f:I

    move v1, v2

    .line 1916991
    iput v1, v0, LX/CZz;->i:I

    .line 1916992
    :cond_0
    iget-object v0, p0, LX/Ca3;->c:LX/Ca4;

    iget-object v0, v0, LX/Ca4;->f:LX/CEW;

    if-eqz v0, :cond_1

    .line 1916993
    iget-object v0, p0, LX/Ca3;->c:LX/Ca4;

    iget-object v0, v0, LX/Ca4;->f:LX/CEW;

    iget-object v1, p0, LX/Ca3;->a:LX/Ca6;

    invoke-virtual {v0, v1}, LX/CEW;->a(LX/Ca6;)V

    .line 1916994
    :cond_1
    return-void

    .line 1916995
    :cond_2
    iget-object v0, p0, LX/Ca3;->a:LX/Ca6;

    .line 1916996
    iget v2, v0, LX/Ca6;->f:I

    move v0, v2

    .line 1916997
    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 3

    .prologue
    .line 1916998
    iget-object v0, p0, LX/Ca3;->c:LX/Ca4;

    iget-object v0, v0, LX/Ca4;->d:LX/03V;

    const-string v1, "SnowflakeUfiController"

    const-string v2, "Failed to set like "

    invoke-virtual {v0, v1, v2, p2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1916999
    return-void
.end method

.method public final bridge synthetic b(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1917000
    return-void
.end method

.method public final bridge synthetic c(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1917001
    return-void
.end method
