.class public LX/AwC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/89u;


# instance fields
.field public a:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;

.field public b:LX/0UE;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0UE",
            "<",
            "LX/BV9;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/Arh;

.field public final d:LX/BVN;

.field private final e:LX/86j;

.field public final f:Ljava/util/concurrent/ArrayBlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ArrayBlockingQueue",
            "<",
            "Ljava/nio/ByteBuffer;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/AsL;

.field public h:Ljava/util/concurrent/ExecutorService;

.field private i:LX/Aw8;

.field private j:Ljava/lang/String;

.field private k:Z

.field public l:Z

.field private m:Lcom/facebook/photos/creativeediting/model/SwipeableParams;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:LX/AwB;

.field private final o:LX/Arb;


# direct methods
.method public constructor <init>(LX/Arh;LX/AsL;Ljava/util/concurrent/ExecutorService;LX/86j;LX/Aw8;)V
    .locals 3
    .param p1    # LX/Arh;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/AsL;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/SingleThreadedExecutorService;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1725105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1725106
    new-instance v0, LX/BVN;

    invoke-direct {v0}, LX/BVN;-><init>()V

    iput-object v0, p0, LX/AwC;->d:LX/BVN;

    .line 1725107
    new-instance v0, Ljava/util/concurrent/ArrayBlockingQueue;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/ArrayBlockingQueue;-><init>(I)V

    iput-object v0, p0, LX/AwC;->f:Ljava/util/concurrent/ArrayBlockingQueue;

    .line 1725108
    iput-boolean v2, p0, LX/AwC;->k:Z

    .line 1725109
    iput-boolean v2, p0, LX/AwC;->l:Z

    .line 1725110
    const/4 v0, 0x0

    iput-object v0, p0, LX/AwC;->n:LX/AwB;

    .line 1725111
    new-instance v0, LX/Aw9;

    invoke-direct {v0, p0}, LX/Aw9;-><init>(LX/AwC;)V

    iput-object v0, p0, LX/AwC;->o:LX/Arb;

    .line 1725112
    iput-object p1, p0, LX/AwC;->c:LX/Arh;

    .line 1725113
    iput-object p2, p0, LX/AwC;->g:LX/AsL;

    .line 1725114
    iput-object p3, p0, LX/AwC;->h:Ljava/util/concurrent/ExecutorService;

    .line 1725115
    iput-object p4, p0, LX/AwC;->e:LX/86j;

    .line 1725116
    iput-object p5, p0, LX/AwC;->i:LX/Aw8;

    .line 1725117
    return-void
.end method

.method private static a(Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;)LX/BVG;
    .locals 16

    .prologue
    .line 1725176
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->aS_()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    move-result-object v0

    .line 1725177
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->l()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    move-result-object v1

    .line 1725178
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->aR_()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    move-result-object v2

    .line 1725179
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->A()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    move-result-object v3

    .line 1725180
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->u()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    move-result-object v4

    .line 1725181
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->n()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    move-result-object v5

    .line 1725182
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->k()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;

    move-result-object v6

    .line 1725183
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->w()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectHSVAFragmentModel;

    move-result-object v7

    .line 1725184
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->p()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectHSVAFragmentModel;

    move-result-object v8

    .line 1725185
    new-instance v9, LX/BVG;

    invoke-direct {v9}, LX/BVG;-><init>()V

    invoke-virtual {v7}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectHSVAFragmentModel;->b()D

    move-result-wide v10

    double-to-float v10, v10

    invoke-virtual {v7}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectHSVAFragmentModel;->c()D

    move-result-wide v12

    double-to-float v11, v12

    invoke-virtual {v7}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectHSVAFragmentModel;->d()D

    move-result-wide v12

    double-to-float v12, v12

    invoke-virtual {v7}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectHSVAFragmentModel;->a()D

    move-result-wide v14

    double-to-float v7, v14

    invoke-virtual {v9, v10, v11, v12, v7}, LX/BVG;->a(FFFF)LX/BVG;

    move-result-object v7

    invoke-virtual {v8}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectHSVAFragmentModel;->b()D

    move-result-wide v10

    double-to-float v9, v10

    invoke-virtual {v8}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectHSVAFragmentModel;->c()D

    move-result-wide v10

    double-to-float v10, v10

    invoke-virtual {v8}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectHSVAFragmentModel;->d()D

    move-result-wide v12

    double-to-float v11, v12

    invoke-virtual {v8}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectHSVAFragmentModel;->a()D

    move-result-wide v12

    double-to-float v8, v12

    invoke-virtual {v7, v9, v10, v11, v8}, LX/BVG;->b(FFFF)LX/BVG;

    move-result-object v7

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;->a()D

    move-result-wide v8

    double-to-float v8, v8

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;->b()D

    move-result-wide v10

    double-to-float v1, v10

    invoke-virtual {v7, v8, v1}, LX/BVG;->a(FF)LX/BVG;

    move-result-object v1

    invoke-virtual {v2}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;->a()D

    move-result-wide v8

    double-to-float v7, v8

    invoke-virtual {v2}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;->b()D

    move-result-wide v8

    double-to-float v2, v8

    invoke-virtual {v1, v7, v2}, LX/BVG;->b(FF)LX/BVG;

    move-result-object v1

    invoke-virtual {v3}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;->a()D

    move-result-wide v8

    double-to-float v2, v8

    invoke-virtual {v3}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;->b()D

    move-result-wide v8

    double-to-float v3, v8

    invoke-virtual {v1, v2, v3}, LX/BVG;->c(FF)LX/BVG;

    move-result-object v1

    invoke-virtual {v4}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;->a()D

    move-result-wide v2

    double-to-float v2, v2

    invoke-virtual {v4}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;->b()D

    move-result-wide v8

    double-to-float v3, v8

    invoke-virtual {v1, v2, v3}, LX/BVG;->d(FF)LX/BVG;

    move-result-object v1

    invoke-virtual {v5}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;->a()D

    move-result-wide v2

    double-to-float v2, v2

    invoke-virtual {v5}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;->b()D

    move-result-wide v4

    double-to-float v3, v4

    invoke-virtual {v1, v2, v3}, LX/BVG;->g(FF)LX/BVG;

    move-result-object v1

    invoke-virtual {v6}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;->a()D

    move-result-wide v2

    double-to-float v2, v2

    invoke-virtual {v6}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;->b()D

    move-result-wide v4

    double-to-float v3, v4

    invoke-virtual {v1, v2, v3}, LX/BVG;->h(FF)LX/BVG;

    move-result-object v1

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->x()I

    move-result v2

    invoke-virtual {v1, v2}, LX/BVG;->c(I)LX/BVG;

    move-result-object v1

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->q()I

    move-result v2

    invoke-virtual {v1, v2}, LX/BVG;->d(I)LX/BVG;

    move-result-object v1

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->z()D

    move-result-wide v2

    double-to-float v2, v2

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->t()D

    move-result-wide v4

    double-to-float v3, v4

    invoke-virtual {v1, v2, v3}, LX/BVG;->f(FF)LX/BVG;

    move-result-object v1

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->y()D

    move-result-wide v2

    double-to-float v2, v2

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->r()D

    move-result-wide v4

    double-to-float v3, v4

    invoke-virtual {v1, v2, v3}, LX/BVG;->i(FF)LX/BVG;

    move-result-object v1

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->m()D

    move-result-wide v2

    double-to-float v2, v2

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->j()D

    move-result-wide v4

    double-to-float v3, v4

    invoke-virtual {v1, v2, v3}, LX/BVG;->j(FF)LX/BVG;

    move-result-object v1

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->B()D

    move-result-wide v2

    double-to-float v2, v2

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->v()D

    move-result-wide v4

    double-to-float v3, v4

    invoke-virtual {v1, v2, v3}, LX/BVG;->k(FF)LX/BVG;

    move-result-object v1

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->C()D

    move-result-wide v2

    double-to-float v2, v2

    invoke-virtual {v1, v2}, LX/BVG;->b(F)LX/BVG;

    move-result-object v1

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->D()D

    move-result-wide v2

    double-to-float v2, v2

    invoke-virtual {v1, v2}, LX/BVG;->a(F)LX/BVG;

    move-result-object v1

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->o()I

    move-result v2

    invoke-virtual {v1, v2}, LX/BVG;->a(I)LX/BVG;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;->a()D

    move-result-wide v2

    double-to-float v2, v2

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect3DVectorFragmentModel;->b()D

    move-result-wide v4

    double-to-float v0, v4

    invoke-virtual {v1, v2, v0}, LX/BVG;->e(FF)LX/BVG;

    move-result-object v0

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->s()I

    move-result v1

    invoke-virtual {v0, v1}, LX/BVG;->b(I)LX/BVG;

    move-result-object v0

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->c()D

    move-result-wide v2

    double-to-float v1, v2

    invoke-virtual {v0, v1}, LX/BVG;->c(F)LX/BVG;

    move-result-object v0

    return-object v0
.end method

.method private b(Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;)LX/BVB;
    .locals 7

    .prologue
    const/16 v5, 0xc

    const/4 v6, 0x0

    .line 1725147
    invoke-virtual {p1}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->E()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect2DVectorFragmentModel;

    move-result-object v0

    .line 1725148
    iget-object v1, p0, LX/AwC;->c:LX/Arh;

    .line 1725149
    iget-object v2, v1, LX/Arh;->j:LX/Arq;

    invoke-virtual {v2}, LX/Arq;->c()Z

    move-result v2

    move v1, v2

    .line 1725150
    new-instance v2, LX/BVB;

    invoke-direct {v2}, LX/BVB;-><init>()V

    iget-object v3, p0, LX/AwC;->c:LX/Arh;

    invoke-virtual {v3}, LX/Arh;->b()I

    move-result v3

    iget-object v4, p0, LX/AwC;->c:LX/Arh;

    invoke-virtual {v4}, LX/Arh;->c()I

    move-result v4

    invoke-virtual {v2, v3, v4}, LX/BVB;->a(II)LX/BVB;

    move-result-object v2

    .line 1725151
    iput v5, v2, LX/BVB;->c:I

    .line 1725152
    iput v5, v2, LX/BVB;->d:I

    .line 1725153
    move-object v2, v2

    .line 1725154
    iget-object v3, p0, LX/AwC;->c:LX/Arh;

    .line 1725155
    iget-object v4, v3, LX/Arh;->j:LX/Arq;

    .line 1725156
    iget-object v3, v4, LX/Arq;->D:LX/6Ia;

    invoke-interface {v3}, LX/6Ia;->g()I

    move-result v3

    move v4, v3

    .line 1725157
    move v3, v4

    .line 1725158
    const/4 v5, 0x1

    const/4 p0, 0x0

    .line 1725159
    new-instance p1, Landroid/hardware/Camera$CameraInfo;

    invoke-direct {p1}, Landroid/hardware/Camera$CameraInfo;-><init>()V

    .line 1725160
    if-eqz v1, :cond_0

    move v4, v5

    :goto_0
    invoke-static {v4, p1}, Landroid/hardware/Camera;->getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V

    .line 1725161
    packed-switch v3, :pswitch_data_0

    .line 1725162
    :goto_1
    :pswitch_0
    iget v4, p1, Landroid/hardware/Camera$CameraInfo;->facing:I

    if-ne v4, v5, :cond_1

    .line 1725163
    iget v4, p1, Landroid/hardware/Camera$CameraInfo;->orientation:I

    add-int/2addr v4, p0

    rem-int/lit16 v4, v4, 0x168

    .line 1725164
    rsub-int v4, v4, 0x168

    rem-int/lit16 v4, v4, 0x168

    .line 1725165
    :goto_2
    move v3, v4

    .line 1725166
    iput v3, v2, LX/BVB;->e:I

    .line 1725167
    move-object v2, v2

    .line 1725168
    iput-boolean v1, v2, LX/BVB;->g:Z

    .line 1725169
    move-object v1, v2

    .line 1725170
    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect2DVectorFragmentModel;->a()D

    move-result-wide v2

    double-to-float v2, v2

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffect2DVectorFragmentModel;->b()D

    move-result-wide v4

    double-to-float v0, v4

    invoke-virtual {v1, v2, v0}, LX/BVB;->a(FF)LX/BVB;

    move-result-object v0

    invoke-virtual {v0, v6, v6}, LX/BVB;->b(FF)LX/BVB;

    move-result-object v0

    return-object v0

    :cond_0
    move v4, p0

    .line 1725171
    goto :goto_0

    .line 1725172
    :pswitch_1
    const/16 p0, 0x5a

    goto :goto_1

    .line 1725173
    :pswitch_2
    const/16 p0, 0xb4

    goto :goto_1

    .line 1725174
    :pswitch_3
    const/16 p0, 0x10e

    goto :goto_1

    .line 1725175
    :cond_1
    iget v4, p1, Landroid/hardware/Camera$CameraInfo;->orientation:I

    sub-int/2addr v4, p0

    add-int/lit16 v4, v4, 0x168

    rem-int/lit16 v4, v4, 0x168

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static i(LX/AwC;)V
    .locals 1

    .prologue
    .line 1725143
    invoke-static {p0}, LX/AwC;->m(LX/AwC;)LX/0UE;

    move-result-object v0

    iput-object v0, p0, LX/AwC;->b:LX/0UE;

    .line 1725144
    iget-boolean v0, p0, LX/AwC;->k:Z

    if-eqz v0, :cond_0

    .line 1725145
    invoke-direct {p0}, LX/AwC;->j()V

    .line 1725146
    :cond_0
    return-void
.end method

.method private j()V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 1725118
    iget-object v0, p0, LX/AwC;->b:LX/0UE;

    if-eqz v0, :cond_3

    .line 1725119
    iget-object v0, p0, LX/AwC;->n:LX/AwB;

    if-nez v0, :cond_1

    .line 1725120
    const/4 v3, 0x0

    .line 1725121
    iget-object v0, p0, LX/AwC;->f:Ljava/util/concurrent/ArrayBlockingQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ArrayBlockingQueue;->clear()V

    .line 1725122
    iget-object v0, p0, LX/AwC;->c:LX/Arh;

    invoke-virtual {v0}, LX/Arh;->b()I

    move-result v4

    .line 1725123
    iget-object v0, p0, LX/AwC;->c:LX/Arh;

    invoke-virtual {v0}, LX/Arh;->c()I

    move-result v5

    move v2, v3

    .line 1725124
    :goto_0
    if-gtz v2, :cond_0

    .line 1725125
    iget-object v6, p0, LX/AwC;->f:Ljava/util/concurrent/ArrayBlockingQueue;

    mul-int v0, v4, v5

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    move-result-object v0

    check-cast v0, Ljava/nio/ByteBuffer;

    invoke-virtual {v6, v0}, Ljava/util/concurrent/ArrayBlockingQueue;->add(Ljava/lang/Object;)Z

    .line 1725126
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1725127
    :cond_0
    new-instance v0, LX/AwB;

    invoke-direct {v0, p0, v4, v5}, LX/AwB;-><init>(LX/AwC;II)V

    iput-object v0, p0, LX/AwC;->n:LX/AwB;

    .line 1725128
    :cond_1
    iget-object v0, p0, LX/AwC;->g:LX/AsL;

    invoke-virtual {v0}, LX/AsL;->c()V

    .line 1725129
    iget-object v0, p0, LX/AwC;->g:LX/AsL;

    iget-object v2, p0, LX/AwC;->j:Ljava/lang/String;

    invoke-virtual {v0, v2}, LX/AsL;->a(Ljava/lang/String;)V

    .line 1725130
    iget-object v2, p0, LX/AwC;->c:LX/Arh;

    new-instance v3, LX/BV8;

    invoke-direct {v3}, LX/BV8;-><init>()V

    iget-object v0, p0, LX/AwC;->a:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/AwC;->a:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;->e()Ljava/lang/String;

    move-result-object v0

    .line 1725131
    :goto_1
    iput-object v0, v3, LX/BV8;->a:Ljava/lang/String;

    .line 1725132
    move-object v0, v3

    .line 1725133
    iget-object v3, p0, LX/AwC;->b:LX/0UE;

    invoke-virtual {v3}, LX/0UE;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    new-instance v1, Ljava/util/ArrayList;

    iget-object v3, p0, LX/AwC;->b:LX/0UE;

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1725134
    :cond_2
    iput-object v1, v0, LX/BV8;->b:Ljava/util/List;

    .line 1725135
    move-object v0, v0

    .line 1725136
    iget-object v1, p0, LX/AwC;->d:LX/BVN;

    .line 1725137
    iput-object v1, v0, LX/BV8;->c:LX/BVN;

    .line 1725138
    move-object v0, v0

    .line 1725139
    invoke-virtual {v0}, LX/BV8;->a()LX/BVA;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/Arh;->a(LX/BVA;)V

    .line 1725140
    iget-object v0, p0, LX/AwC;->c:LX/Arh;

    iget-object v1, p0, LX/AwC;->o:LX/Arb;

    invoke-virtual {v0, v1}, LX/Arh;->a(LX/Arb;)V

    .line 1725141
    :cond_3
    return-void

    :cond_4
    move-object v0, v1

    .line 1725142
    goto :goto_1
.end method

.method private l()Z
    .locals 1

    .prologue
    .line 1725046
    iget-object v0, p0, LX/AwC;->c:LX/Arh;

    invoke-virtual {v0}, LX/Arh;->b()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static m(LX/AwC;)LX/0UE;
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0UE",
            "<",
            "LX/BV9;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1725083
    invoke-direct/range {p0 .. p0}, LX/AwC;->l()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1725084
    new-instance v1, LX/0UE;

    invoke-direct {v1}, LX/0UE;-><init>()V

    .line 1725085
    :goto_0
    return-object v1

    .line 1725086
    :cond_0
    new-instance v11, LX/0UE;

    invoke-direct {v11}, LX/0UE;-><init>()V

    .line 1725087
    move-object/from16 v0, p0

    iget-object v1, v0, LX/AwC;->a:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;->d()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel$EmittersModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel$EmittersModel;->a()LX/0Px;

    move-result-object v14

    invoke-virtual {v14}, LX/0Px;->size()I

    move-result v15

    const/4 v1, 0x0

    move v13, v1

    :goto_1
    if-ge v13, v15, :cond_4

    invoke-virtual {v14, v13}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;

    .line 1725088
    invoke-virtual {v8}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->d()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel$EmitterAssetsModel;

    move-result-object v1

    .line 1725089
    if-eqz v1, :cond_3

    .line 1725090
    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel$EmitterAssetsModel;->a()LX/0Px;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel$EmitterAssetsModel$NodesModel;

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel$EmitterAssetsModel$NodesModel;->a()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectImageFragmentModel;

    move-result-object v16

    .line 1725091
    if-eqz v16, :cond_3

    .line 1725092
    const/4 v10, 0x0

    .line 1725093
    const/4 v9, 0x0

    .line 1725094
    invoke-virtual {v8}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->b()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel$AnimationAssetsModel;

    move-result-object v1

    .line 1725095
    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel$AnimationAssetsModel;->a()LX/0Px;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, LX/0Px;->size()I

    move-result v18

    const/4 v1, 0x0

    move v12, v1

    :goto_2
    move/from16 v0, v18

    if-ge v12, v0, :cond_2

    move-object/from16 v0, v17

    invoke-virtual {v0, v12}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel$AnimationAssetsModel$NodesModel;

    .line 1725096
    invoke-virtual {v7}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel$AnimationAssetsModel$NodesModel;->b()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectImageFragmentModel;

    move-result-object v2

    .line 1725097
    if-eqz v2, :cond_5

    invoke-virtual {v2}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectImageFragmentModel;->a()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 1725098
    invoke-virtual {v7}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel$AnimationAssetsModel$NodesModel;->a()Lcom/facebook/graphql/enums/GraphQLParticleEffectAnimationTypeEnum;

    move-result-object v1

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLParticleEffectAnimationTypeEnum;->BIRTH:Lcom/facebook/graphql/enums/GraphQLParticleEffectAnimationTypeEnum;

    if-ne v1, v3, :cond_1

    .line 1725099
    new-instance v1, LX/7SR;

    invoke-virtual {v2}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectImageFragmentModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v7}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel$AnimationAssetsModel$NodesModel;->d()I

    move-result v3

    invoke-virtual {v7}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel$AnimationAssetsModel$NodesModel;->aU_()I

    move-result v4

    invoke-virtual {v7}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel$AnimationAssetsModel$NodesModel;->c()I

    move-result v5

    invoke-virtual {v7}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel$AnimationAssetsModel$NodesModel;->aT_()I

    move-result v6

    invoke-virtual {v7}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel$AnimationAssetsModel$NodesModel;->e()I

    move-result v7

    invoke-direct/range {v1 .. v7}, LX/7SR;-><init>(Landroid/net/Uri;IIIII)V

    move-object v2, v1

    move-object v1, v9

    .line 1725100
    :goto_3
    add-int/lit8 v3, v12, 0x1

    move v12, v3

    move-object v9, v1

    move-object v10, v2

    goto :goto_2

    .line 1725101
    :cond_1
    new-instance v1, LX/7SR;

    invoke-virtual {v2}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectImageFragmentModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v7}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel$AnimationAssetsModel$NodesModel;->d()I

    move-result v3

    invoke-virtual {v7}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel$AnimationAssetsModel$NodesModel;->aU_()I

    move-result v4

    invoke-virtual {v7}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel$AnimationAssetsModel$NodesModel;->c()I

    move-result v5

    invoke-virtual {v7}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel$AnimationAssetsModel$NodesModel;->aT_()I

    move-result v6

    invoke-virtual {v7}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel$AnimationAssetsModel$NodesModel;->e()I

    move-result v7

    invoke-direct/range {v1 .. v7}, LX/7SR;-><init>(Landroid/net/Uri;IIIII)V

    move-object v2, v10

    goto :goto_3

    .line 1725102
    :cond_2
    new-instance v1, LX/BV9;

    invoke-virtual/range {v16 .. v16}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectImageFragmentModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v8}, LX/AwC;->a(Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;)LX/BVG;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-direct {v0, v8}, LX/AwC;->b(Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;)LX/BVB;

    move-result-object v4

    invoke-virtual {v8}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectEmitterModel;->e()Ljava/lang/String;

    move-result-object v5

    move-object v6, v10

    move-object v7, v9

    invoke-direct/range {v1 .. v7}, LX/BV9;-><init>(Landroid/net/Uri;LX/BVG;LX/BVB;Ljava/lang/String;LX/7SR;LX/7SR;)V

    invoke-virtual {v11, v1}, LX/0UE;->add(Ljava/lang/Object;)Z

    .line 1725103
    :cond_3
    add-int/lit8 v1, v13, 0x1

    move v13, v1

    goto/16 :goto_1

    :cond_4
    move-object v1, v11

    .line 1725104
    goto/16 :goto_0

    :cond_5
    move-object v1, v9

    move-object v2, v10

    goto :goto_3
.end method


# virtual methods
.method public final a()LX/86d;
    .locals 1

    .prologue
    .line 1725082
    iget-object v0, p0, LX/AwC;->e:LX/86j;

    return-object v0
.end method

.method public final a(Lcom/facebook/friendsharing/inspiration/model/InspirationModel;)V
    .locals 3

    .prologue
    .line 1725186
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->getParticleEffect()Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1725187
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->getId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/AwC;->j:Ljava/lang/String;

    .line 1725188
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->getParticleEffect()Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig;->getParticleEffectModel()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;

    move-result-object v0

    iput-object v0, p0, LX/AwC;->a:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;

    .line 1725189
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->isLoggingDisabled()Z

    move-result v0

    iput-boolean v0, p0, LX/AwC;->l:Z

    .line 1725190
    iget-object v0, p0, LX/AwC;->i:LX/Aw8;

    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->getParticleEffect()Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig;

    move-result-object v1

    new-instance v2, LX/AwA;

    invoke-direct {v2, p0}, LX/AwA;-><init>(LX/AwC;)V

    invoke-virtual {v0, v1, v2}, LX/Aw8;->a(Lcom/facebook/videocodec/effects/model/ParticleEffectGLConfig;LX/89p;)V

    .line 1725191
    :cond_0
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 1725081
    return-void
.end method

.method public final b()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1725066
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/AwC;->k:Z

    .line 1725067
    iget-object v2, p0, LX/AwC;->c:LX/Arh;

    new-instance v3, LX/BV8;

    invoke-direct {v3}, LX/BV8;-><init>()V

    iget-object v0, p0, LX/AwC;->a:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/AwC;->a:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;->e()Ljava/lang/String;

    move-result-object v0

    .line 1725068
    :goto_0
    iput-object v0, v3, LX/BV8;->a:Ljava/lang/String;

    .line 1725069
    move-object v0, v3

    .line 1725070
    iput-object v1, v0, LX/BV8;->b:Ljava/util/List;

    .line 1725071
    move-object v0, v0

    .line 1725072
    iget-object v3, p0, LX/AwC;->d:LX/BVN;

    .line 1725073
    iput-object v3, v0, LX/BV8;->c:LX/BVN;

    .line 1725074
    move-object v0, v0

    .line 1725075
    invoke-virtual {v0}, LX/BV8;->a()LX/BVA;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/Arh;->a(LX/BVA;)V

    .line 1725076
    iget-object v0, p0, LX/AwC;->c:LX/Arh;

    iget-object v2, p0, LX/AwC;->o:LX/Arb;

    invoke-virtual {v0, v2}, LX/Arh;->b(LX/Arb;)V

    .line 1725077
    iget-object v0, p0, LX/AwC;->f:Ljava/util/concurrent/ArrayBlockingQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ArrayBlockingQueue;->clear()V

    .line 1725078
    iput-object v1, p0, LX/AwC;->n:LX/AwB;

    .line 1725079
    return-void

    :cond_0
    move-object v0, v1

    .line 1725080
    goto :goto_0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1725060
    iget-object v0, p0, LX/AwC;->g:LX/AsL;

    invoke-virtual {v0}, LX/AsL;->b()V

    .line 1725061
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/AwC;->k:Z

    .line 1725062
    iget-object v0, p0, LX/AwC;->b:LX/0UE;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/AwC;->b:LX/0UE;

    invoke-virtual {v0}, LX/0UE;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1725063
    invoke-static {p0}, LX/AwC;->m(LX/AwC;)LX/0UE;

    move-result-object v0

    iput-object v0, p0, LX/AwC;->b:LX/0UE;

    .line 1725064
    :cond_0
    invoke-direct {p0}, LX/AwC;->j()V

    .line 1725065
    return-void
.end method

.method public final d()Lcom/facebook/photos/creativeediting/model/SwipeableParams;
    .locals 1

    .prologue
    .line 1725057
    iget-object v0, p0, LX/AwC;->m:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    if-nez v0, :cond_0

    .line 1725058
    iget-object v0, p0, LX/AwC;->j:Ljava/lang/String;

    invoke-static {v0}, LX/8GN;->a(Ljava/lang/String;)Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    move-result-object v0

    iput-object v0, p0, LX/AwC;->m:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    .line 1725059
    :cond_0
    iget-object v0, p0, LX/AwC;->m:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    return-object v0
.end method

.method public final e()Landroid/net/Uri;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1725056
    const/4 v0, 0x0

    return-object v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1725053
    iget-boolean v0, p0, LX/AwC;->k:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/AwC;->a:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;->aV_()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel$InstructionsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1725054
    iget-object v0, p0, LX/AwC;->a:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;->aV_()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel$InstructionsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel$InstructionsModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 1725055
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1725050
    iget-boolean v0, p0, LX/AwC;->k:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/AwC;->a:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;->b()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel$AttributionTextModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1725051
    iget-object v0, p0, LX/AwC;->a:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;->b()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel$AttributionTextModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel$AttributionTextModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 1725052
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()Landroid/net/Uri;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1725047
    iget-boolean v0, p0, LX/AwC;->k:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/AwC;->a:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;->c()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel$AttributionThumbnailModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1725048
    iget-object v0, p0, LX/AwC;->a:Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel;->c()Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel$AttributionThumbnailModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/graphql/ParticleEffectGraphQLModels$ParticleEffectModel$AttributionThumbnailModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1725049
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
