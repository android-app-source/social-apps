.class public final LX/AXQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3Ha;


# instance fields
.field public final synthetic a:LX/AXY;


# direct methods
.method public constructor <init>(LX/AXY;)V
    .locals 0

    .prologue
    .line 1683957
    iput-object p1, p0, LX/AXQ;->a:LX/AXY;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastFragmentModel;)V
    .locals 0

    .prologue
    .line 1683956
    return-void
.end method

.method public final a(Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastPollerShortFragmentModel;)V
    .locals 0

    .prologue
    .line 1683955
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1683954
    return-void
.end method

.method public final b(Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastFragmentModel;)V
    .locals 3

    .prologue
    .line 1683936
    invoke-virtual {p1}, Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastFragmentModel;->k()Ljava/lang/String;

    move-result-object v0

    .line 1683937
    if-nez v0, :cond_0

    .line 1683938
    iget-object v0, p0, LX/AXQ;->a:LX/AXY;

    iget-object v0, v0, LX/AXY;->b:LX/03V;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, LX/AXY;->x:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_onBroadcastCompleted"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Broadcast status is VOD_READY but playableUrl is null"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1683939
    :goto_0
    return-void

    .line 1683940
    :cond_0
    iget-object v1, p0, LX/AXQ;->a:LX/AXY;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v1, v0}, LX/AXY;->a$redex0(LX/AXY;Landroid/net/Uri;)V

    goto :goto_0
.end method

.method public final b(Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastPollerShortFragmentModel;)V
    .locals 2

    .prologue
    .line 1683950
    invoke-virtual {p1}, Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastPollerShortFragmentModel;->j()Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->VOD_READY:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-ne v0, v1, :cond_0

    .line 1683951
    iget-object v0, p0, LX/AXQ;->a:LX/AXY;

    iget-object v0, v0, LX/AXY;->p:LX/3Hc;

    invoke-virtual {v0}, LX/3Hc;->a()V

    .line 1683952
    iget-object v0, p0, LX/AXQ;->a:LX/AXY;

    iget-object v0, v0, LX/AXY;->p:LX/3Hc;

    iget-object v1, p0, LX/AXQ;->a:LX/AXY;

    iget-object v1, v1, LX/AXY;->aa:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/3Hc;->c(Ljava/lang/String;)V

    .line 1683953
    :cond_0
    return-void
.end method

.method public final c(Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastFragmentModel;)V
    .locals 0

    .prologue
    .line 1683949
    return-void
.end method

.method public final d(Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastFragmentModel;)V
    .locals 3

    .prologue
    .line 1683942
    invoke-virtual {p1}, Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastFragmentModel;->d()Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->VOD_READY:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-ne v0, v1, :cond_0

    .line 1683943
    iget-object v0, p0, LX/AXQ;->a:LX/AXY;

    iget-object v0, v0, LX/AXY;->p:LX/3Hc;

    invoke-virtual {v0}, LX/3Hc;->a()V

    .line 1683944
    invoke-virtual {p1}, Lcom/facebook/facecastdisplay/protocol/FetchVideoBroadcastQueryModels$VideoBroadcastFragmentModel;->k()Ljava/lang/String;

    move-result-object v0

    .line 1683945
    if-nez v0, :cond_1

    .line 1683946
    iget-object v0, p0, LX/AXQ;->a:LX/AXY;

    iget-object v0, v0, LX/AXY;->b:LX/03V;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, LX/AXY;->x:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_onBroadcastCompleted"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Broadcast status is VOD_READY but playableUrl is null"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1683947
    :cond_0
    :goto_0
    return-void

    .line 1683948
    :cond_1
    iget-object v1, p0, LX/AXQ;->a:LX/AXY;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v1, v0}, LX/AXY;->a$redex0(LX/AXY;Landroid/net/Uri;)V

    goto :goto_0
.end method

.method public final s_(I)V
    .locals 0

    .prologue
    .line 1683941
    return-void
.end method
