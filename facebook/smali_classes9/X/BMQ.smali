.class public LX/BMQ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/ARD;

.field private final b:LX/AR4;

.field private final c:LX/AR2;


# direct methods
.method public constructor <init>(LX/ARD;LX/AR4;LX/AR2;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1777163
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1777164
    iput-object p1, p0, LX/BMQ;->a:LX/ARD;

    .line 1777165
    iput-object p2, p0, LX/BMQ;->b:LX/AR4;

    .line 1777166
    iput-object p3, p0, LX/BMQ;->c:LX/AR2;

    .line 1777167
    return-void
.end method

.method public static a(Lcom/facebook/productionprompts/logging/PromptAnalytics;Ljava/lang/Class;Ljava/lang/Boolean;)Landroid/os/Bundle;
    .locals 3
    .param p2    # Ljava/lang/Boolean;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/productionprompts/logging/PromptAnalytics;",
            "Ljava/lang/Class",
            "<+",
            "LX/1kK;",
            ">;",
            "Ljava/lang/Boolean;",
            ")",
            "Landroid/os/Bundle;"
        }
    .end annotation

    .prologue
    .line 1777168
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1777169
    if-eqz p0, :cond_0

    .line 1777170
    const-string v1, "prompt_entry_point_analytics_extra"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1777171
    const-string v1, "prompt_object_class_name_extra"

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1777172
    if-eqz p2, :cond_0

    .line 1777173
    const-string v1, "did_use_prompt_extra"

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1777174
    :cond_0
    return-object v0
.end method

.method private static a(ZLcom/facebook/productionprompts/logging/PromptAnalytics;)Z
    .locals 2

    .prologue
    .line 1777175
    if-eqz p0, :cond_0

    iget-object v0, p1, Lcom/facebook/productionprompts/logging/PromptAnalytics;->promptType:Ljava/lang/String;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPromptType;->FEED_STORY:Lcom/facebook/graphql/enums/GraphQLPromptType;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLPromptType;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/BMQ;
    .locals 4

    .prologue
    .line 1777176
    new-instance v3, LX/BMQ;

    const-class v0, LX/ARD;

    invoke-interface {p0, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/ARD;

    const-class v1, LX/AR4;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/AR4;

    const-class v2, LX/AR2;

    invoke-interface {p0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/AR2;

    invoke-direct {v3, v0, v1, v2}, LX/BMQ;-><init>(LX/ARD;LX/AR4;LX/AR2;)V

    .line 1777177
    return-object v3
.end method


# virtual methods
.method public final a(LX/B5j;LX/ARN;Lcom/facebook/productionprompts/logging/PromptAnalytics;Ljava/lang/Class;Z)Landroid/content/Intent;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/B5j;",
            "LX/ARN;",
            "Lcom/facebook/productionprompts/logging/PromptAnalytics;",
            "Ljava/lang/Class",
            "<+",
            "LX/1kK;",
            ">;Z)",
            "Landroid/content/Intent;"
        }
    .end annotation

    .prologue
    .line 1777178
    iget-object v0, p0, LX/BMQ;->c:LX/AR2;

    invoke-virtual {v0, p1, p2}, LX/AR2;->a(Ljava/lang/Object;LX/ARN;)LX/AR1;

    move-result-object v2

    .line 1777179
    iget-object v0, p0, LX/BMQ;->b:LX/AR4;

    invoke-virtual {v0, p1}, LX/AR4;->a(Ljava/lang/Object;)LX/AR3;

    move-result-object v3

    .line 1777180
    iget-object v0, p0, LX/BMQ;->a:LX/ARD;

    invoke-virtual {v0, p1, v3, v2}, LX/ARD;->a(Ljava/lang/Object;LX/AR3;LX/AR1;)LX/ARC;

    move-result-object v4

    .line 1777181
    invoke-virtual {v4}, LX/ARC;->a()Lcom/facebook/composer/publish/common/PublishPostParams;

    move-result-object v0

    .line 1777182
    invoke-static {p5, p3}, LX/BMQ;->a(ZLcom/facebook/productionprompts/logging/PromptAnalytics;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, LX/5M9;

    invoke-direct {v1, v0}, LX/5M9;-><init>(Lcom/facebook/composer/publish/common/PublishPostParams;)V

    .line 1777183
    iput-object p3, v1, LX/5M9;->ag:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    .line 1777184
    move-object v0, v1

    .line 1777185
    invoke-virtual {v0}, LX/5M9;->a()Lcom/facebook/composer/publish/common/PublishPostParams;

    move-result-object v0

    :cond_0
    invoke-static {p5, p3}, LX/BMQ;->a(ZLcom/facebook/productionprompts/logging/PromptAnalytics;)Z

    move-result v1

    if-eqz v1, :cond_1

    move-object v1, p3

    :goto_0
    invoke-virtual {v2}, LX/AR1;->a()Z

    move-result v2

    .line 1777186
    invoke-virtual {v3}, LX/AR3;->a()Z

    move-result p0

    if-nez p0, :cond_2

    .line 1777187
    const/4 p0, 0x0

    .line 1777188
    :goto_1
    move v1, p0

    .line 1777189
    invoke-virtual {v4, v0, v1}, LX/ARC;->a(Lcom/facebook/composer/publish/common/PublishPostParams;Z)Landroid/content/Intent;

    move-result-object v0

    .line 1777190
    invoke-static {p5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p3, p4, v1}, LX/BMQ;->a(Lcom/facebook/productionprompts/logging/PromptAnalytics;Ljava/lang/Class;Ljava/lang/Boolean;)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 1777191
    return-object v0

    .line 1777192
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 1777193
    :cond_2
    new-instance p0, LX/8LQ;

    invoke-virtual {v3}, LX/AR3;->c()Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object p1

    invoke-direct {p0, p1}, LX/8LQ;-><init>(Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 1777194
    iput-object v1, p0, LX/8LQ;->aa:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    .line 1777195
    move-object p0, p0

    .line 1777196
    invoke-virtual {p0}, LX/8LQ;->a()Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object p0

    .line 1777197
    invoke-virtual {v3, p0}, LX/AR3;->a(Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 1777198
    if-nez v2, :cond_3

    .line 1777199
    invoke-virtual {v3}, LX/AR3;->e()V

    .line 1777200
    :cond_3
    const/4 p0, 0x1

    goto :goto_1
.end method
