.class public LX/BHl;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/support/v7/widget/RecyclerView;

.field public final b:LX/BHe;

.field public final c:LX/BH5;

.field public final d:Z

.field public final e:Z

.field public f:LX/BHd;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:LX/BGf;

.field public h:I


# direct methods
.method public constructor <init>(Landroid/support/v7/widget/RecyclerView;LX/BHe;LX/BH5;ZZ)V
    .locals 1
    .param p1    # Landroid/support/v7/widget/RecyclerView;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/BH5;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1769636
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1769637
    const/4 v0, 0x0

    iput v0, p0, LX/BHl;->h:I

    .line 1769638
    iput-object p1, p0, LX/BHl;->a:Landroid/support/v7/widget/RecyclerView;

    .line 1769639
    iput-object p2, p0, LX/BHl;->b:LX/BHe;

    .line 1769640
    iput-object p3, p0, LX/BHl;->c:LX/BH5;

    .line 1769641
    iput-boolean p4, p0, LX/BHl;->d:Z

    .line 1769642
    iput-boolean p5, p0, LX/BHl;->e:Z

    .line 1769643
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1769635
    iget-object v0, p0, LX/BHl;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getChildCount()I

    move-result v0

    return v0
.end method

.method public final a(I)Landroid/view/View;
    .locals 1

    .prologue
    .line 1769634
    iget-object v0, p0, LX/BHl;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/RecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/4gI;)V
    .locals 3

    .prologue
    .line 1769625
    iget-object v0, p0, LX/BHl;->f:LX/BHd;

    if-eqz v0, :cond_1

    .line 1769626
    iget-object v0, p0, LX/BHl;->f:LX/BHd;

    .line 1769627
    iget-object v1, v0, LX/BHd;->e:Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;

    const/4 v2, 0x0

    .line 1769628
    iget-object p0, v1, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->K:LX/4gI;

    if-eq p0, p1, :cond_0

    .line 1769629
    iput-object p1, v1, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->K:LX/4gI;

    .line 1769630
    if-eqz v2, :cond_0

    .line 1769631
    const p0, -0x61ac5d76

    invoke-static {v1, p0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1769632
    :cond_0
    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 1769633
    :cond_1
    return-void
.end method

.method public final a(LX/BGf;)V
    .locals 1

    .prologue
    .line 1769619
    iput-object p1, p0, LX/BHl;->g:LX/BGf;

    .line 1769620
    iget-object v0, p0, LX/BHl;->f:LX/BHd;

    if-eqz v0, :cond_0

    .line 1769621
    iget-object v0, p0, LX/BHl;->f:LX/BHd;

    .line 1769622
    iget-object p0, v0, LX/BHd;->e:Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;

    .line 1769623
    iput-object p1, p0, Lcom/facebook/photos/simplepicker/controller/SimplePickerGridViewCursorAdapter;->I:LX/BGf;

    .line 1769624
    :cond_0
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 1769616
    iget-object v0, p0, LX/BHl;->f:LX/BHd;

    if-eqz v0, :cond_0

    .line 1769617
    iget-object v0, p0, LX/BHl;->f:LX/BHd;

    invoke-virtual {v0}, LX/1OM;->ij_()I

    move-result v0

    .line 1769618
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
