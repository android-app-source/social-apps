.class public LX/Bx2;
.super LX/2oy;
.source ""

# interfaces
.implements LX/0ib;


# static fields
.field public static final o:LX/0Tn;

.field private static final p:LX/0Tn;


# instance fields
.field public final A:I

.field public final B:I

.field public final C:Landroid/animation/AnimatorListenerAdapter;

.field public D:LX/2pa;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public E:Lcom/facebook/video/engine/VideoPlayerParams;

.field public F:I

.field public G:I

.field private H:I

.field private I:I

.field public J:Z

.field public K:Z

.field public L:Z

.field public M:Z

.field public N:I

.field public O:I

.field public P:I

.field public Q:Z

.field public R:I

.field public a:LX/0iX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/3kp;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0iZ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0gt;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0iY;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/1C2;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final q:LX/Bx0;

.field public final r:LX/Bx1;

.field public final s:Lcom/facebook/widget/FbImageView;

.field public final t:Landroid/view/View;

.field public final u:Lcom/facebook/resources/ui/FbTextView;

.field public final v:I

.field public final w:I

.field private final x:I

.field private final y:I

.field public final z:I


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1834637
    sget-object v0, LX/0Tm;->g:LX/0Tn;

    const-string v1, "inline_sound_toggle_primary_nux_shown"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/Bx2;->o:LX/0Tn;

    .line 1834638
    sget-object v0, LX/0Tm;->g:LX/0Tn;

    const-string v1, "inline_sound_toggle_secondary_nux_shown"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/Bx2;->p:LX/0Tn;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1834747
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/Bx2;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1834748
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1834749
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/Bx2;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1834750
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 9
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ConstructorMayLeakThis"
        }
    .end annotation

    .prologue
    const/16 v4, 0x3e8

    const/4 v3, 0x3

    const/4 v1, 0x0

    .line 1834751
    invoke-direct {p0, p1, p2, p3}, LX/2oy;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1834752
    new-instance v0, LX/Bx0;

    invoke-direct {v0, p0}, LX/Bx0;-><init>(LX/Bx2;)V

    iput-object v0, p0, LX/Bx2;->q:LX/Bx0;

    .line 1834753
    iput v1, p0, LX/Bx2;->H:I

    .line 1834754
    iput v1, p0, LX/Bx2;->I:I

    .line 1834755
    iput-boolean v1, p0, LX/Bx2;->J:Z

    .line 1834756
    iput-boolean v1, p0, LX/Bx2;->K:Z

    .line 1834757
    iput-boolean v1, p0, LX/Bx2;->L:Z

    .line 1834758
    iput-boolean v1, p0, LX/Bx2;->M:Z

    .line 1834759
    iput v1, p0, LX/Bx2;->N:I

    .line 1834760
    iput v1, p0, LX/Bx2;->O:I

    .line 1834761
    iput v1, p0, LX/Bx2;->P:I

    .line 1834762
    iput-boolean v1, p0, LX/Bx2;->Q:Z

    .line 1834763
    iput v1, p0, LX/Bx2;->R:I

    .line 1834764
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v5, p0

    check-cast v5, LX/Bx2;

    invoke-static {v0}, LX/0iX;->a(LX/0QB;)LX/0iX;

    move-result-object v6

    check-cast v6, LX/0iX;

    invoke-static {v0}, LX/3kp;->b(LX/0QB;)LX/3kp;

    move-result-object v7

    check-cast v7, LX/3kp;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v8

    check-cast v8, LX/0ad;

    invoke-static {v0}, LX/0iZ;->a(LX/0QB;)LX/0iZ;

    move-result-object p1

    check-cast p1, LX/0iZ;

    const/16 p2, 0x122d

    invoke-static {v0, p2}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p2

    invoke-static {v0}, LX/0iY;->a(LX/0QB;)LX/0iY;

    move-result-object p3

    check-cast p3, LX/0iY;

    invoke-static {v0}, LX/1C2;->a(LX/0QB;)LX/1C2;

    move-result-object v0

    check-cast v0, LX/1C2;

    iput-object v6, v5, LX/Bx2;->a:LX/0iX;

    iput-object v7, v5, LX/Bx2;->b:LX/3kp;

    iput-object v8, v5, LX/Bx2;->c:LX/0ad;

    iput-object p1, v5, LX/Bx2;->d:LX/0iZ;

    iput-object p2, v5, LX/Bx2;->e:LX/0Or;

    iput-object p3, v5, LX/Bx2;->f:LX/0iY;

    iput-object v0, v5, LX/Bx2;->n:LX/1C2;

    .line 1834765
    const v0, 0x7f03091b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1834766
    const v0, 0x7f0d175a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/FbImageView;

    iput-object v0, p0, LX/Bx2;->s:Lcom/facebook/widget/FbImageView;

    .line 1834767
    const v0, 0x7f0d1759

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/Bx2;->t:Landroid/view/View;

    .line 1834768
    const v0, 0x7f0d175b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/Bx2;->u:Lcom/facebook/resources/ui/FbTextView;

    .line 1834769
    iget-object v0, p0, LX/Bx2;->t:Landroid/view/View;

    new-instance v2, LX/Bww;

    invoke-direct {v2, p0}, LX/Bww;-><init>(LX/Bx2;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1834770
    iget-object v0, p0, LX/Bx2;->u:Lcom/facebook/resources/ui/FbTextView;

    new-instance v2, LX/Bwx;

    invoke-direct {v2, p0}, LX/Bwx;-><init>(LX/Bx2;)V

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1834771
    new-instance v0, LX/Bx1;

    invoke-direct {v0, p0}, LX/Bx1;-><init>(LX/Bx2;)V

    iput-object v0, p0, LX/Bx2;->r:LX/Bx1;

    .line 1834772
    iget-object v0, p0, LX/Bx2;->c:LX/0ad;

    sget v2, LX/1rJ;->b:I

    invoke-interface {v0, v2, v4}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/Bx2;->v:I

    .line 1834773
    iget-object v0, p0, LX/Bx2;->c:LX/0ad;

    sget v2, LX/1rJ;->s:I

    invoke-interface {v0, v2, v3}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/Bx2;->x:I

    .line 1834774
    iget-object v0, p0, LX/Bx2;->c:LX/0ad;

    sget v2, LX/1rJ;->t:I

    invoke-interface {v0, v2, v3}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/Bx2;->y:I

    .line 1834775
    iget-object v0, p0, LX/Bx2;->c:LX/0ad;

    sget v2, LX/1rJ;->I:I

    const/16 v3, 0xc8

    invoke-interface {v0, v2, v3}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/Bx2;->z:I

    .line 1834776
    iget-object v0, p0, LX/Bx2;->c:LX/0ad;

    sget v2, LX/1rJ;->l:I

    invoke-interface {v0, v2, v4}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/Bx2;->A:I

    .line 1834777
    iget-object v0, p0, LX/Bx2;->c:LX/0ad;

    sget v2, LX/1rJ;->m:I

    const/16 v3, 0x1388

    invoke-interface {v0, v2, v3}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/Bx2;->B:I

    .line 1834778
    iget-object v0, p0, LX/Bx2;->c:LX/0ad;

    sget-short v2, LX/1rJ;->i:S

    invoke-interface {v0, v2, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    .line 1834779
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Bx2;->c:LX/0ad;

    sget v2, LX/1rJ;->a:I

    invoke-interface {v0, v2, v1}, LX/0ad;->a(II)I

    move-result v0

    :goto_0
    iput v0, p0, LX/Bx2;->w:I

    .line 1834780
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    iget-object v1, p0, LX/Bx2;->q:LX/Bx0;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1834781
    new-instance v0, LX/Bwy;

    invoke-direct {v0, p0}, LX/Bwy;-><init>(LX/Bx2;)V

    iput-object v0, p0, LX/Bx2;->C:Landroid/animation/AnimatorListenerAdapter;

    .line 1834782
    return-void

    :cond_0
    move v0, v1

    .line 1834783
    goto :goto_0
.end method

.method public static D(LX/Bx2;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1834784
    iget-object v0, p0, LX/Bx2;->u:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbTextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 1834785
    return-void
.end method

.method public static E(LX/Bx2;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1834786
    iget-object v2, p0, LX/2oy;->k:Lcom/facebook/video/player/RichVideoPlayer;

    if-nez v2, :cond_1

    .line 1834787
    :cond_0
    :goto_0
    return v0

    .line 1834788
    :cond_1
    const/4 v2, 0x2

    new-array v2, v2, [I

    fill-array-data v2, :array_0

    .line 1834789
    iget-object v3, p0, LX/2oy;->k:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v3, v2}, Lcom/facebook/video/player/RichVideoPlayer;->getLocationInWindow([I)V

    .line 1834790
    iget v3, p0, LX/Bx2;->H:I

    aget v4, v2, v0

    if-ne v3, v4, :cond_2

    iget v3, p0, LX/Bx2;->I:I

    aget v4, v2, v1

    if-eq v3, v4, :cond_0

    .line 1834791
    :cond_2
    aget v0, v2, v0

    iput v0, p0, LX/Bx2;->H:I

    .line 1834792
    aget v0, v2, v1

    iput v0, p0, LX/Bx2;->I:I

    move v0, v1

    .line 1834793
    goto :goto_0

    .line 1834794
    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method

.method private a(ZLX/04g;)V
    .locals 1

    .prologue
    .line 1834795
    iget-object v0, p0, LX/2oy;->k:Lcom/facebook/video/player/RichVideoPlayer;

    if-nez v0, :cond_0

    .line 1834796
    :goto_0
    return-void

    .line 1834797
    :cond_0
    iget-object v0, p0, LX/2oy;->k:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/video/player/RichVideoPlayer;->a(ZLX/04g;)V

    goto :goto_0
.end method

.method public static a$redex0(LX/Bx2;Z)V
    .locals 8

    .prologue
    .line 1834798
    iget-object v0, p0, LX/Bx2;->E:Lcom/facebook/video/engine/VideoPlayerParams;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2oy;->k:Lcom/facebook/video/player/RichVideoPlayer;

    if-nez v0, :cond_1

    .line 1834799
    :cond_0
    :goto_0
    return-void

    .line 1834800
    :cond_1
    if-eqz p1, :cond_2

    .line 1834801
    iget-object v0, p0, LX/Bx2;->n:LX/1C2;

    iget-object v1, p0, LX/Bx2;->E:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v1, v1, Lcom/facebook/video/engine/VideoPlayerParams;->e:LX/162;

    sget-object v2, LX/04G;->INLINE_PLAYER:LX/04G;

    sget-object v3, LX/04g;->BY_USER:LX/04g;

    iget-object v3, v3, LX/04g;->value:Ljava/lang/String;

    iget-object v4, p0, LX/2oy;->k:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v4}, Lcom/facebook/video/player/RichVideoPlayer;->getCurrentPositionMs()I

    move-result v4

    iget-object v5, p0, LX/Bx2;->E:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v5, v5, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    invoke-static {p0}, LX/Bx2;->getPlayerOrigin(LX/Bx2;)LX/04D;

    move-result-object v6

    iget-object v7, p0, LX/Bx2;->E:Lcom/facebook/video/engine/VideoPlayerParams;

    invoke-virtual/range {v0 .. v7}, LX/1C2;->a(LX/0lF;LX/04G;Ljava/lang/String;ILjava/lang/String;LX/04D;LX/098;)LX/1C2;

    goto :goto_0

    .line 1834802
    :cond_2
    iget-object v0, p0, LX/Bx2;->n:LX/1C2;

    iget-object v1, p0, LX/Bx2;->E:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v1, v1, Lcom/facebook/video/engine/VideoPlayerParams;->e:LX/162;

    sget-object v2, LX/04G;->INLINE_PLAYER:LX/04G;

    sget-object v3, LX/04g;->BY_USER:LX/04g;

    iget-object v3, v3, LX/04g;->value:Ljava/lang/String;

    iget-object v4, p0, LX/2oy;->k:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v4}, Lcom/facebook/video/player/RichVideoPlayer;->getCurrentPositionMs()I

    move-result v4

    iget-object v5, p0, LX/Bx2;->E:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v5, v5, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    invoke-static {p0}, LX/Bx2;->getPlayerOrigin(LX/Bx2;)LX/04D;

    move-result-object v6

    iget-object v7, p0, LX/Bx2;->E:Lcom/facebook/video/engine/VideoPlayerParams;

    invoke-virtual/range {v0 .. v7}, LX/1C2;->b(LX/0lF;LX/04G;Ljava/lang/String;ILjava/lang/String;LX/04D;LX/098;)LX/1C2;

    goto :goto_0
.end method

.method private b(LX/04g;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1834803
    iget-object v0, p0, LX/Bx2;->a:LX/0iX;

    .line 1834804
    iget-boolean v1, v0, LX/0iX;->i:Z

    move v0, v1

    .line 1834805
    if-eqz v0, :cond_1

    .line 1834806
    iget-object v0, p0, LX/Bx2;->s:Lcom/facebook/widget/FbImageView;

    const v1, 0x7f021add

    invoke-virtual {v0, v1}, Lcom/facebook/widget/FbImageView;->setImageResource(I)V

    .line 1834807
    :goto_0
    iget-object v0, p0, LX/Bx2;->a:LX/0iX;

    invoke-static {p0}, LX/Bx2;->getPlayerOrigin(LX/Bx2;)LX/04D;

    move-result-object v1

    iget-object v2, p0, LX/Bx2;->D:LX/2pa;

    invoke-virtual {v0, v1, v2}, LX/0iX;->a(LX/04D;LX/2pa;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1834808
    invoke-direct {p0, v3, p1}, LX/Bx2;->a(ZLX/04g;)V

    .line 1834809
    :cond_0
    :goto_1
    return-void

    .line 1834810
    :cond_1
    iget-object v0, p0, LX/Bx2;->s:Lcom/facebook/widget/FbImageView;

    const v1, 0x7f021adc

    invoke-virtual {v0, v1}, Lcom/facebook/widget/FbImageView;->setImageResource(I)V

    goto :goto_0

    .line 1834811
    :cond_2
    sget-object v0, LX/04g;->BY_PLAYER:LX/04g;

    if-eq p1, v0, :cond_0

    invoke-static {p0}, LX/Bx2;->j(LX/Bx2;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1834812
    iput-boolean v3, p0, LX/Bx2;->L:Z

    .line 1834813
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, LX/Bx2;->a(ZLX/04g;)V

    goto :goto_1
.end method

.method public static getLabelTextId(LX/Bx2;)I
    .locals 1

    .prologue
    .line 1834739
    iget-boolean v0, p0, LX/Bx2;->J:Z

    if-eqz v0, :cond_0

    .line 1834740
    const v0, 0x7f0828b0

    .line 1834741
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/Bx2;->a:LX/0iX;

    .line 1834742
    iget-boolean p0, v0, LX/0iX;->i:Z

    move v0, p0

    .line 1834743
    if-eqz v0, :cond_1

    const v0, 0x7f0828af

    goto :goto_0

    :cond_1
    const v0, 0x7f0828ae

    goto :goto_0
.end method

.method public static getPlayerOrigin(LX/Bx2;)LX/04D;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1834744
    iget-object v0, p0, LX/2oy;->k:Lcom/facebook/video/player/RichVideoPlayer;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/2oy;->k:Lcom/facebook/video/player/RichVideoPlayer;

    .line 1834745
    iget-object p0, v0, Lcom/facebook/video/player/RichVideoPlayer;->H:LX/04D;

    move-object v0, p0

    .line 1834746
    goto :goto_0
.end method

.method public static h(LX/Bx2;)V
    .locals 7

    .prologue
    .line 1834711
    iget-object v1, p0, LX/Bx2;->d:LX/0iZ;

    iget-object v0, p0, LX/Bx2;->a:LX/0iX;

    .line 1834712
    iget-boolean v2, v0, LX/0iX;->i:Z

    move v0, v2

    .line 1834713
    if-eqz v0, :cond_2

    sget-object v0, LX/0ia;->TURN_OFF_BY_TOGGLE:LX/0ia;

    :goto_0
    invoke-virtual {v1, v0}, LX/0iZ;->a(LX/0ia;)V

    .line 1834714
    iget-object v1, p0, LX/Bx2;->a:LX/0iX;

    iget-object v0, p0, LX/Bx2;->a:LX/0iX;

    .line 1834715
    iget-boolean v2, v0, LX/0iX;->i:Z

    move v0, v2

    .line 1834716
    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_1
    sget-object v2, LX/04g;->BY_USER:LX/04g;

    invoke-virtual {v1, v0, v2}, LX/0iX;->b(ZLX/04g;)V

    .line 1834717
    const v4, 0x3f666666    # 0.9f

    .line 1834718
    iget-object v3, p0, LX/Bx2;->s:Lcom/facebook/widget/FbImageView;

    invoke-virtual {v3}, Lcom/facebook/widget/FbImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    const-wide/16 v5, 0x96

    invoke-virtual {v3, v5, v6}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    iget-object v4, p0, LX/Bx2;->C:Landroid/animation/AnimatorListenerAdapter;

    invoke-virtual {v3, v4}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 1834719
    iget-object v0, p0, LX/Bx2;->a:LX/0iX;

    .line 1834720
    iget v1, v0, LX/0iX;->n:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, LX/0iX;->n:I

    .line 1834721
    invoke-static {p0}, LX/Bx2;->y(LX/Bx2;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1834722
    :cond_0
    :goto_2
    iget-object v0, p0, LX/2oy;->i:LX/2oj;

    if-eqz v0, :cond_1

    .line 1834723
    iget-object v0, p0, LX/2oy;->i:LX/2oj;

    new-instance v1, LX/7MF;

    invoke-direct {v1}, LX/7MF;-><init>()V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    .line 1834724
    :cond_1
    return-void

    .line 1834725
    :cond_2
    sget-object v0, LX/0ia;->TURN_ON_BY_TOGGLE:LX/0ia;

    goto :goto_0

    .line 1834726
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 1834727
    :cond_4
    invoke-virtual {p0}, LX/Bx2;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, LX/0f0;

    invoke-static {v0, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0f0;

    .line 1834728
    if-eqz v0, :cond_0

    .line 1834729
    const-class v1, LX/0f3;

    invoke-interface {v0, v1}, LX/0f0;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0f3;

    .line 1834730
    if-eqz v0, :cond_0

    invoke-interface {v0}, LX/0f3;->n()Lcom/facebook/apptab/state/TabTag;

    move-result-object v1

    sget-object v2, Lcom/facebook/feed/tab/FeedTab;->l:Lcom/facebook/feed/tab/FeedTab;

    if-ne v1, v2, :cond_0

    .line 1834731
    new-instance v1, LX/0hs;

    invoke-virtual {p0}, LX/Bx2;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x2

    invoke-direct {v1, v2, v3}, LX/0hs;-><init>(Landroid/content/Context;I)V

    .line 1834732
    iget-object v2, p0, LX/Bx2;->c:LX/0ad;

    sget-char v3, LX/1rJ;->z:C

    const v4, 0x7f0828ac

    invoke-virtual {p0}, LX/Bx2;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-interface {v2, v3, v4, v5}, LX/0ad;->a(CILandroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0hs;->a(Ljava/lang/CharSequence;)V

    .line 1834733
    iget-object v2, p0, LX/Bx2;->c:LX/0ad;

    sget-char v3, LX/1rJ;->y:C

    const v4, 0x7f0828ad

    invoke-virtual {p0}, LX/Bx2;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-interface {v2, v3, v4, v5}, LX/0ad;->a(CILandroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0hs;->b(Ljava/lang/CharSequence;)V

    .line 1834734
    const/4 v2, -0x1

    .line 1834735
    iput v2, v1, LX/0hs;->t:I

    .line 1834736
    sget-object v2, Lcom/facebook/bookmark/tab/BookmarkTab;->l:Lcom/facebook/bookmark/tab/BookmarkTab;

    invoke-interface {v0, v2, v1}, LX/0f3;->a(Lcom/facebook/apptab/state/TabTag;LX/0hs;)V

    .line 1834737
    iget-object v0, p0, LX/Bx2;->d:LX/0iZ;

    sget-object v1, LX/0ia;->SHOW_SETTING_NUX:LX/0ia;

    invoke-virtual {v0, v1}, LX/0iZ;->a(LX/0ia;)V

    .line 1834738
    iget-object v0, p0, LX/Bx2;->b:LX/3kp;

    invoke-virtual {v0}, LX/3kp;->a()V

    goto :goto_2
.end method

.method public static j(LX/Bx2;)Z
    .locals 1

    .prologue
    .line 1834710
    iget-object v0, p0, LX/2oy;->k:Lcom/facebook/video/player/RichVideoPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2oy;->k:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static y(LX/Bx2;)Z
    .locals 5

    .prologue
    .line 1834701
    iget-object v0, p0, LX/Bx2;->b:LX/3kp;

    iget v1, p0, LX/Bx2;->y:I

    .line 1834702
    iput v1, v0, LX/3kp;->b:I

    .line 1834703
    iget-object v0, p0, LX/Bx2;->b:LX/3kp;

    sget-object v1, LX/Bx2;->p:LX/0Tn;

    invoke-virtual {v0, v1}, LX/3kp;->a(LX/0Tn;)V

    .line 1834704
    iget-object v0, p0, LX/Bx2;->b:LX/3kp;

    invoke-virtual {v0}, LX/3kp;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/Bx2;->a:LX/0iX;

    const/4 v1, 0x0

    .line 1834705
    iget v2, v0, LX/0iX;->n:I

    iget-object v3, v0, LX/0iX;->c:LX/0ad;

    sget v4, LX/1rJ;->o:I

    const/4 p0, 0x3

    invoke-interface {v3, v4, p0}, LX/0ad;->a(II)I

    move-result v3

    if-lt v2, v3, :cond_0

    .line 1834706
    iput v1, v0, LX/0iX;->n:I

    .line 1834707
    const/4 v1, 0x1

    .line 1834708
    :cond_0
    move v0, v1

    .line 1834709
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/04g;)V
    .locals 1

    .prologue
    .line 1834694
    iget-boolean v0, p0, LX/Bx2;->M:Z

    if-nez v0, :cond_1

    .line 1834695
    :cond_0
    :goto_0
    return-void

    .line 1834696
    :cond_1
    invoke-direct {p0, p1}, LX/Bx2;->b(LX/04g;)V

    .line 1834697
    invoke-static {p0}, LX/Bx2;->j(LX/Bx2;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1834698
    iget-object v0, p0, LX/Bx2;->a:LX/0iX;

    .line 1834699
    iget-boolean p1, v0, LX/0iX;->i:Z

    move v0, p1

    .line 1834700
    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-static {p0, v0}, LX/Bx2;->a$redex0(LX/Bx2;Z)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(LX/2pa;Z)V
    .locals 3

    .prologue
    .line 1834675
    iget-object v0, p0, LX/Bx2;->a:LX/0iX;

    invoke-static {p0}, LX/Bx2;->getPlayerOrigin(LX/Bx2;)LX/04D;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, LX/0iX;->b(LX/04D;LX/2pa;)Z

    move-result v0

    iput-boolean v0, p0, LX/Bx2;->M:Z

    .line 1834676
    iget-boolean v0, p0, LX/Bx2;->M:Z

    if-nez v0, :cond_0

    .line 1834677
    iget-object v0, p0, LX/Bx2;->t:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1834678
    :goto_0
    return-void

    .line 1834679
    :cond_0
    invoke-static {p1}, LX/393;->d(LX/2pa;)Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    .line 1834680
    if-eqz v1, :cond_1

    .line 1834681
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->bJ()Lcom/facebook/graphql/enums/GraphQLAudioAvailability;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLAudioAvailability;->UNAVAILABLE:Lcom/facebook/graphql/enums/GraphQLAudioAvailability;

    if-ne v0, v2, :cond_3

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, LX/Bx2;->J:Z

    .line 1834682
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->bX()I

    move-result v0

    iput v0, p0, LX/Bx2;->R:I

    .line 1834683
    :cond_1
    iget-object v0, p0, LX/Bx2;->a:LX/0iX;

    invoke-virtual {v0, p0}, LX/0iX;->a(LX/0ib;)V

    .line 1834684
    iput-object p1, p0, LX/Bx2;->D:LX/2pa;

    .line 1834685
    iget-object v0, p1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iput-object v0, p0, LX/Bx2;->E:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 1834686
    iget-object v0, p0, LX/2oy;->k:Lcom/facebook/video/player/RichVideoPlayer;

    if-eqz v0, :cond_2

    .line 1834687
    iget-object v0, p0, LX/2oy;->k:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->getCurrentPositionMs()I

    move-result v0

    iput v0, p0, LX/Bx2;->O:I

    .line 1834688
    :cond_2
    sget-object v0, LX/04g;->BY_PLAYER:LX/04g;

    invoke-direct {p0, v0}, LX/Bx2;->b(LX/04g;)V

    .line 1834689
    iget-object v0, p0, LX/Bx2;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gt;

    const-string v1, "1768718466676737"

    .line 1834690
    iput-object v1, v0, LX/0gt;->a:Ljava/lang/String;

    .line 1834691
    move-object v0, v0

    .line 1834692
    invoke-virtual {v0}, LX/0gt;->b()V

    goto :goto_0

    .line 1834693
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final d()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1834670
    iget-object v0, p0, LX/Bx2;->a:LX/0iX;

    invoke-virtual {v0, p0}, LX/0iX;->b(LX/0ib;)V

    .line 1834671
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Bx2;->M:Z

    .line 1834672
    iput-object v1, p0, LX/Bx2;->D:LX/2pa;

    .line 1834673
    iput-object v1, p0, LX/Bx2;->E:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 1834674
    return-void
.end method

.method public final dI_()V
    .locals 4

    .prologue
    .line 1834659
    iget-boolean v0, p0, LX/Bx2;->M:Z

    if-nez v0, :cond_1

    .line 1834660
    :cond_0
    :goto_0
    return-void

    .line 1834661
    :cond_1
    iget v0, p0, LX/Bx2;->v:I

    iput v0, p0, LX/Bx2;->F:I

    .line 1834662
    iget v0, p0, LX/Bx2;->w:I

    iput v0, p0, LX/Bx2;->G:I

    .line 1834663
    iget-object v0, p0, LX/Bx2;->b:LX/3kp;

    iget v1, p0, LX/Bx2;->x:I

    .line 1834664
    iput v1, v0, LX/3kp;->b:I

    .line 1834665
    iget-object v0, p0, LX/Bx2;->b:LX/3kp;

    sget-object v1, LX/Bx2;->o:LX/0Tn;

    invoke-virtual {v0, v1}, LX/3kp;->a(LX/0Tn;)V

    .line 1834666
    iget-object v0, p0, LX/Bx2;->b:LX/3kp;

    invoke-virtual {v0}, LX/3kp;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1834667
    iget-object v0, p0, LX/Bx2;->c:LX/0ad;

    sget-short v1, LX/1rJ;->d:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    move v0, v0

    .line 1834668
    if-eqz v0, :cond_0

    .line 1834669
    iget-object v0, p0, LX/Bx2;->r:LX/Bx1;

    const/4 v1, 0x0

    iget v2, p0, LX/Bx2;->z:I

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, LX/Bx1;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method

.method public final g()V
    .locals 3

    .prologue
    .line 1834648
    iget-boolean v0, p0, LX/Bx2;->M:Z

    if-nez v0, :cond_1

    .line 1834649
    :cond_0
    :goto_0
    return-void

    .line 1834650
    :cond_1
    invoke-static {p0}, LX/Bx2;->D(LX/Bx2;)V

    .line 1834651
    invoke-static {p0}, LX/Bx2;->j(LX/Bx2;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Bx2;->a:LX/0iX;

    .line 1834652
    iget-boolean v1, v0, LX/0iX;->i:Z

    move v0, v1

    .line 1834653
    if-nez v0, :cond_0

    .line 1834654
    iget-object v0, p0, LX/Bx2;->a:LX/0iX;

    .line 1834655
    iget-boolean v1, v0, LX/0iX;->h:Z

    move v0, v1

    .line 1834656
    if-eqz v0, :cond_2

    .line 1834657
    iget-object v0, p0, LX/Bx2;->d:LX/0iZ;

    sget-object v1, LX/0ia;->TURN_ON_BY_VOLUME:LX/0ia;

    invoke-virtual {v0, v1}, LX/0iZ;->a(LX/0ia;)V

    .line 1834658
    :cond_2
    iget-object v0, p0, LX/Bx2;->a:LX/0iX;

    const/4 v1, 0x1

    sget-object v2, LX/04g;->BY_USER:LX/04g;

    invoke-virtual {v0, v1, v2}, LX/0iX;->b(ZLX/04g;)V

    goto :goto_0
.end method

.method public final t()V
    .locals 2

    .prologue
    .line 1834639
    iget-boolean v0, p0, LX/Bx2;->M:Z

    if-nez v0, :cond_0

    .line 1834640
    :goto_0
    return-void

    .line 1834641
    :cond_0
    iget-object v0, p0, LX/Bx2;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gt;

    const-string v1, "1768718466676737"

    .line 1834642
    iput-object v1, v0, LX/0gt;->a:Ljava/lang/String;

    .line 1834643
    move-object v0, v0

    .line 1834644
    invoke-virtual {p0}, LX/Bx2;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0gt;->b(Landroid/content/Context;)V

    .line 1834645
    iget-object v0, p0, LX/Bx2;->r:LX/Bx1;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/Bx1;->removeMessages(I)V

    .line 1834646
    iget-object v0, p0, LX/Bx2;->r:LX/Bx1;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/Bx1;->removeMessages(I)V

    .line 1834647
    iget-object v0, p0, LX/Bx2;->r:LX/Bx1;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, LX/Bx1;->removeMessages(I)V

    goto :goto_0
.end method
