.class public final LX/AnE;
.super LX/99W;
.source ""


# instance fields
.field private final a:LX/AnC;

.field private final b:LX/AnF;

.field public final c:Lcom/facebook/graphql/model/GraphQLStory;

.field private final d:Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;


# direct methods
.method public constructor <init>(LX/0ja;LX/AnF;LX/AnC;Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)V
    .locals 0

    .prologue
    .line 1712218
    invoke-direct {p0, p1}, LX/99W;-><init>(LX/0ja;)V

    .line 1712219
    iput-object p2, p0, LX/AnE;->b:LX/AnF;

    .line 1712220
    iput-object p3, p0, LX/AnE;->a:LX/AnC;

    .line 1712221
    iput-object p4, p0, LX/AnE;->c:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1712222
    iput-object p5, p0, LX/AnE;->d:Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    .line 1712223
    return-void
.end method


# virtual methods
.method public final a(II)LX/99X;
    .locals 1

    .prologue
    .line 1712206
    goto :goto_1

    .line 1712207
    :goto_0
    return-object v0

    :goto_1
    invoke-super {p0, p1, p2}, LX/99W;->a(II)LX/99X;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Landroid/view/View;Ljava/lang/Object;I)V
    .locals 6

    .prologue
    .line 1712214
    iget-object v0, p0, LX/AnE;->b:LX/AnF;

    .line 1712215
    iget-object v1, p0, LX/99W;->a:Ljava/util/List;

    move-object v1, v1

    .line 1712216
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {p0, p3, v1}, LX/AnE;->a(II)LX/99X;

    move-result-object v3

    iget-object v4, p0, LX/AnE;->a:LX/AnC;

    iget-object v5, p0, LX/AnE;->d:Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    move-object v1, p1

    move-object v2, p2

    invoke-virtual/range {v0 .. v5}, LX/AnF;->a(Landroid/view/View;Ljava/lang/Object;LX/99X;LX/AnC;Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)V

    .line 1712217
    return-void
.end method

.method public final b(Landroid/view/View;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 1712212
    iget-object v0, p0, LX/AnE;->b:LX/AnF;

    invoke-virtual {v0, p1}, LX/AnF;->a(Landroid/view/View;)V

    .line 1712213
    return-void
.end method

.method public final d(I)F
    .locals 2

    .prologue
    .line 1712209
    iget-object v0, p0, LX/AnE;->b:LX/AnF;

    .line 1712210
    iget-object v1, p0, LX/99W;->a:Ljava/util/List;

    move-object v1, v1

    .line 1712211
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {p0, p1, v1}, LX/AnE;->a(II)LX/99X;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2sw;->a(LX/99X;)F

    move-result v0

    return v0
.end method

.method public final e()LX/2eR;
    .locals 1

    .prologue
    .line 1712208
    iget-object v0, p0, LX/AnE;->b:LX/AnF;

    invoke-virtual {v0}, LX/2sw;->a()LX/2eR;

    move-result-object v0

    return-object v0
.end method
