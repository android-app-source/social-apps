.class public final LX/Byj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/Byk;


# direct methods
.method public constructor <init>(LX/Byk;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1837748
    iput-object p1, p0, LX/Byj;->c:LX/Byk;

    iput-object p2, p0, LX/Byj;->a:Ljava/lang/String;

    iput-object p3, p0, LX/Byj;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0xb1214a0

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1837739
    iget-object v1, p0, LX/Byj;->c:LX/Byk;

    iget-object v1, v1, LX/Byk;->b:LX/1xP;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, LX/Byj;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v5, v5}, LX/1xP;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/util/Map;)V

    .line 1837740
    iget-object v1, p0, LX/Byj;->c:LX/Byk;

    iget-object v2, p0, LX/Byj;->b:Ljava/lang/String;

    .line 1837741
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v5, "spherical_video_fallback_cta_clicked"

    invoke-direct {v3, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v5, "video"

    .line 1837742
    iput-object v5, v3, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1837743
    move-object v3, v3

    .line 1837744
    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1837745
    const-string v5, "video_id"

    invoke-virtual {v3, v5, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1837746
    :cond_0
    iget-object v5, v1, LX/Byk;->c:LX/0Zb;

    invoke-interface {v5, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1837747
    const v1, -0x330bfbcd

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
