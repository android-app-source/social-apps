.class public LX/C3R;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pb;",
        ":",
        "LX/1Pc;",
        ":",
        "LX/1Pd;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field public final a:LX/1VL;

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/C3B;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/C3I;

.field public final d:LX/C39;

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/C3p;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1VL;LX/0Ot;LX/C3I;LX/C39;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1VL;",
            "LX/0Ot",
            "<",
            "LX/C3B;",
            ">;",
            "LX/C3I;",
            "LX/C39;",
            "LX/0Ot",
            "<",
            "LX/C3p;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1845720
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1845721
    iput-object p1, p0, LX/C3R;->a:LX/1VL;

    .line 1845722
    iput-object p2, p0, LX/C3R;->b:LX/0Ot;

    .line 1845723
    iput-object p3, p0, LX/C3R;->c:LX/C3I;

    .line 1845724
    iput-object p4, p0, LX/C3R;->d:LX/C39;

    .line 1845725
    iput-object p5, p0, LX/C3R;->e:LX/0Ot;

    .line 1845726
    return-void
.end method

.method public static a(LX/0QB;)LX/C3R;
    .locals 9

    .prologue
    .line 1845709
    const-class v1, LX/C3R;

    monitor-enter v1

    .line 1845710
    :try_start_0
    sget-object v0, LX/C3R;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1845711
    sput-object v2, LX/C3R;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1845712
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1845713
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1845714
    new-instance v3, LX/C3R;

    invoke-static {v0}, LX/1VL;->b(LX/0QB;)LX/1VL;

    move-result-object v4

    check-cast v4, LX/1VL;

    const/16 v5, 0x1ebc

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static {v0}, LX/C3I;->a(LX/0QB;)LX/C3I;

    move-result-object v6

    check-cast v6, LX/C3I;

    invoke-static {v0}, LX/C39;->a(LX/0QB;)LX/C39;

    move-result-object v7

    check-cast v7, LX/C39;

    const/16 v8, 0x1ed1

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-direct/range {v3 .. v8}, LX/C3R;-><init>(LX/1VL;LX/0Ot;LX/C3I;LX/C39;LX/0Ot;)V

    .line 1845715
    move-object v0, v3

    .line 1845716
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1845717
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C3R;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1845718
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1845719
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/C33;)Z
    .locals 2

    .prologue
    .line 1845699
    iget-object v0, p0, LX/C33;->b:LX/C34;

    sget-object v1, LX/C34;->DENSE_SEARCH_STORIES:LX/C34;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/C33;->b:LX/C34;

    sget-object v1, LX/C34;->COMPACT_GROUPS_FEED:LX/C34;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/C33;->b:LX/C34;

    sget-object v1, LX/C34;->GROUP_RELATED_STORIES:LX/C34;

    if-ne v0, v1, :cond_1

    :cond_0
    iget-object v0, p0, LX/C33;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1845700
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 1845701
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/C33;)Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1845702
    iget-object v0, p0, LX/C33;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1845703
    iget-object p0, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, p0

    .line 1845704
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 1845705
    if-nez v0, :cond_0

    move-object v0, v1

    .line 1845706
    :goto_0
    return-object v0

    .line 1845707
    :cond_0
    invoke-static {v0}, LX/16y;->b(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    .line 1845708
    if-nez v0, :cond_1

    move-object v0, v1

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
