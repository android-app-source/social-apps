.class public LX/ATR;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/6bO;

.field private final b:LX/74n;

.field public final c:Landroid/os/Handler;

.field private final d:Landroid/os/Handler;


# direct methods
.method public constructor <init>(LX/6bO;LX/74n;Landroid/os/Handler;Landroid/os/Handler;)V
    .locals 0
    .param p3    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p4    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1676027
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1676028
    iput-object p1, p0, LX/ATR;->a:LX/6bO;

    .line 1676029
    iput-object p2, p0, LX/ATR;->b:LX/74n;

    .line 1676030
    iput-object p3, p0, LX/ATR;->c:Landroid/os/Handler;

    .line 1676031
    iput-object p4, p0, LX/ATR;->d:Landroid/os/Handler;

    .line 1676032
    return-void
.end method

.method public static a(LX/0QB;)LX/ATR;
    .locals 5

    .prologue
    .line 1676033
    new-instance v4, LX/ATR;

    invoke-static {p0}, LX/6bO;->b(LX/0QB;)LX/6bO;

    move-result-object v0

    check-cast v0, LX/6bO;

    invoke-static {p0}, LX/74n;->b(LX/0QB;)LX/74n;

    move-result-object v1

    check-cast v1, LX/74n;

    invoke-static {p0}, LX/0Ss;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v2

    check-cast v2, Landroid/os/Handler;

    invoke-static {p0}, LX/0kY;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v3

    check-cast v3, Landroid/os/Handler;

    invoke-direct {v4, v0, v1, v2, v3}, LX/ATR;-><init>(LX/6bO;LX/74n;Landroid/os/Handler;Landroid/os/Handler;)V

    .line 1676034
    move-object v0, v4

    .line 1676035
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/composer/attachments/ComposerAttachment;Ljava/lang/Runnable;)V
    .locals 3

    .prologue
    .line 1676036
    invoke-virtual {p1}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->m()LX/4gF;

    move-result-object v0

    sget-object v1, LX/4gF;->VIDEO:LX/4gF;

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/composer/attachments/ComposerAttachment;->c()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/composer/attachments/ComposerAttachment;->c()Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, LX/74n;->a(Landroid/net/Uri;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1676037
    :cond_0
    :goto_0
    return-void

    .line 1676038
    :cond_1
    iget-object v0, p0, LX/ATR;->d:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/composer/ui/underwood/UnderwoodVideoTranscodingUtil$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/facebook/composer/ui/underwood/UnderwoodVideoTranscodingUtil$1;-><init>(LX/ATR;Lcom/facebook/composer/attachments/ComposerAttachment;Ljava/lang/Runnable;)V

    const v2, -0x663f637c

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto :goto_0
.end method
