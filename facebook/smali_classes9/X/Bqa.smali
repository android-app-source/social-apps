.class public final LX/Bqa;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/Bqa;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/BqZ;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/Bqb;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1824829
    const/4 v0, 0x0

    sput-object v0, LX/Bqa;->a:LX/Bqa;

    .line 1824830
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/Bqa;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1824831
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1824832
    new-instance v0, LX/Bqb;

    invoke-direct {v0}, LX/Bqb;-><init>()V

    iput-object v0, p0, LX/Bqa;->c:LX/Bqb;

    .line 1824833
    return-void
.end method

.method public static declared-synchronized q()LX/Bqa;
    .locals 2

    .prologue
    .line 1824834
    const-class v1, LX/Bqa;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/Bqa;->a:LX/Bqa;

    if-nez v0, :cond_0

    .line 1824835
    new-instance v0, LX/Bqa;

    invoke-direct {v0}, LX/Bqa;-><init>()V

    sput-object v0, LX/Bqa;->a:LX/Bqa;

    .line 1824836
    :cond_0
    sget-object v0, LX/Bqa;->a:LX/Bqa;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 1824837
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 2

    .prologue
    .line 1824838
    check-cast p2, LX/BqY;

    .line 1824839
    iget-object v0, p2, LX/BqY;->a:Lcom/facebook/graphql/model/GraphQLStoryInsights;

    .line 1824840
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    const/4 p0, 0x2

    invoke-interface {v1, p0}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v1

    const/4 p0, 0x4

    invoke-interface {v1, p0}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v1

    const/4 p0, 0x1

    const p2, 0x7f0b09c8

    invoke-interface {v1, p0, p2}, LX/1Dh;->q(II)LX/1Dh;

    move-result-object v1

    const/4 p0, 0x3

    const p2, 0x7f0b09c8

    invoke-interface {v1, p0, p2}, LX/1Dh;->q(II)LX/1Dh;

    move-result-object v1

    invoke-static {p1}, LX/BqW;->c(LX/1De;)LX/BqV;

    move-result-object p0

    const p2, 0x7f0a045b

    invoke-virtual {p0, p2}, LX/BqV;->j(I)LX/BqV;

    move-result-object p0

    const p2, 0x7f0810c3

    invoke-virtual {p0, p2}, LX/BqV;->i(I)LX/BqV;

    move-result-object p0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryInsights;->k()I

    move-result p2

    invoke-virtual {p0, p2}, LX/BqV;->h(I)LX/BqV;

    move-result-object p0

    invoke-interface {v1, p0}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v1

    invoke-static {p1}, LX/Bqb;->a(LX/1De;)LX/1Dg;

    move-result-object p0

    invoke-interface {v1, p0}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v1

    invoke-static {p1}, LX/BqW;->c(LX/1De;)LX/BqV;

    move-result-object p0

    const p2, 0x7f0a045c

    invoke-virtual {p0, p2}, LX/BqV;->j(I)LX/BqV;

    move-result-object p0

    const p2, 0x7f0810c4

    invoke-virtual {p0, p2}, LX/BqV;->i(I)LX/BqV;

    move-result-object p0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryInsights;->l()I

    move-result p2

    invoke-virtual {p0, p2}, LX/BqV;->h(I)LX/BqV;

    move-result-object p0

    invoke-interface {v1, p0}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v1

    invoke-static {p1}, LX/Bqb;->a(LX/1De;)LX/1Dg;

    move-result-object p0

    invoke-interface {v1, p0}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v1

    invoke-static {p1}, LX/BqW;->c(LX/1De;)LX/BqV;

    move-result-object p0

    const p2, 0x7f0a010c

    invoke-virtual {p0, p2}, LX/BqV;->j(I)LX/BqV;

    move-result-object p0

    const p2, 0x7f0810c8

    invoke-virtual {p0, p2}, LX/BqV;->i(I)LX/BqV;

    move-result-object p0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryInsights;->j()I

    move-result p2

    invoke-virtual {p0, p2}, LX/BqV;->h(I)LX/BqV;

    move-result-object p0

    invoke-interface {v1, p0}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    move-object v0, v1

    .line 1824841
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1824842
    invoke-static {}, LX/1dS;->b()V

    .line 1824843
    const/4 v0, 0x0

    return-object v0
.end method
