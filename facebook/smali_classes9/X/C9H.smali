.class public final LX/C9H;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final a:Lcom/facebook/graphql/model/GraphQLStory;

.field private final b:LX/C99;


# direct methods
.method public constructor <init>(Lcom/facebook/graphql/model/GraphQLStory;LX/C99;)V
    .locals 0

    .prologue
    .line 1853883
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1853884
    iput-object p1, p0, LX/C9H;->a:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1853885
    iput-object p2, p0, LX/C9H;->b:LX/C99;

    .line 1853886
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0x756fa207

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 1853887
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, LX/CA6;

    .line 1853888
    iget-object v1, v0, LX/CA6;->i:LX/5ON;

    move-object v1, v1

    .line 1853889
    if-nez v1, :cond_0

    .line 1853890
    iget-object v1, p0, LX/C9H;->b:LX/C99;

    invoke-virtual {v1}, LX/C99;->a()LX/5ON;

    move-result-object v1

    .line 1853891
    iput-object v1, v0, LX/CA6;->i:LX/5ON;

    .line 1853892
    :cond_0
    iget-object v3, v0, LX/CA6;->l:LX/C98;

    move-object v0, v3

    .line 1853893
    iput-object v0, v1, LX/5OM;->p:LX/5OO;

    .line 1853894
    iget-object v0, p0, LX/C9H;->b:LX/C99;

    iget-object v3, p0, LX/C9H;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0, v1, v3, p1}, LX/C99;->a(LX/5ON;Lcom/facebook/graphql/model/GraphQLStory;Landroid/view/View;)V

    .line 1853895
    const v0, 0x54aeea7b

    invoke-static {v4, v4, v0, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
