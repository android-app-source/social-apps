.class public LX/Brd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/26L;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/26L",
        "<",
        "LX/26M;",
        ">;"
    }
.end annotation


# static fields
.field public static a:LX/0Rl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rl",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;",
            "LX/26P;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final c:LX/Bra;

.field private final d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;>;"
        }
    .end annotation
.end field

.field private final e:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/26M;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 1826468
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->PHOTO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    sget-object v1, LX/26P;->Photo:LX/26P;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->VIDEO:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    sget-object v3, LX/26P;->Video:LX/26P;

    invoke-static {v0, v1, v2, v3}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v0

    sput-object v0, LX/Brd;->b:LX/0P1;

    .line 1826469
    new-instance v0, LX/Brc;

    invoke-direct {v0}, LX/Brc;-><init>()V

    sput-object v0, LX/Brd;->a:LX/0Rl;

    return-void
.end method

.method public constructor <init>(LX/Brb;LX/0Px;)V
    .locals 6
    .param p2    # LX/0Px;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Brb;",
            "LX/0Px",
            "<",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;>;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1826449
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1826450
    invoke-static {p2}, LX/Brd;->a(LX/0Px;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1826451
    iput-object p2, p0, LX/Brd;->d:LX/0Px;

    .line 1826452
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/Brd;->e:Ljava/util/HashMap;

    .line 1826453
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    move v1, v2

    .line 1826454
    :goto_0
    iget-object v0, p0, LX/Brd;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 1826455
    iget-object v4, p0, LX/Brd;->e:Ljava/util/HashMap;

    iget-object v0, p0, LX/Brd;->d:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1826456
    iget-object v5, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v5

    .line 1826457
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1826458
    new-instance v4, LX/26M;

    iget-object v0, p0, LX/Brd;->d:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {v4, v0}, LX/26M;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-virtual {v3, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1826459
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1826460
    :cond_0
    iget-object v0, p0, LX/Brd;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v0, p0, LX/Brd;->d:LX/0Px;

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1826461
    iget-object v2, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v2

    .line 1826462
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v0}, LX/1VO;->r(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-static {v0}, LX/16z;->b(Lcom/facebook/graphql/model/GraphQLImage;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 1826463
    new-instance v2, LX/Bra;

    invoke-direct {v2, v1, v0}, LX/Bra;-><init>(Ljava/lang/Integer;Ljava/lang/Boolean;)V

    .line 1826464
    move-object v0, v2

    .line 1826465
    iput-object v0, p0, LX/Brd;->c:LX/Bra;

    .line 1826466
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/Brd;->f:LX/0Px;

    .line 1826467
    return-void
.end method

.method public static a(LX/0Px;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;>;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1826440
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v0

    const/4 v2, 0x2

    if-lt v0, v2, :cond_0

    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v0

    const/4 v2, 0x5

    if-le v0, v2, :cond_1

    :cond_0
    move v0, v1

    .line 1826441
    :goto_0
    return v0

    .line 1826442
    :cond_1
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v3

    move v2, v1

    :goto_1
    if-ge v2, v3, :cond_3

    invoke-virtual {p0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1826443
    sget-object v4, LX/Brd;->a:LX/0Rl;

    .line 1826444
    iget-object v5, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v5

    .line 1826445
    invoke-interface {v4, v0}, LX/0Rl;->apply(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 1826446
    goto :goto_0

    .line 1826447
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 1826448
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)I
    .locals 2

    .prologue
    .line 1826395
    iget-object v0, p0, LX/Brd;->e:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    const-string v1, "Provided attachment is not one of the visible attachments"

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1826396
    iget-object v0, p0, LX/Brd;->e:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public static b(Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;)Z
    .locals 1

    .prologue
    .line 1826439
    sget-object v0, LX/Brd;->b:LX/0P1;

    invoke-virtual {v0, p0}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(LX/26N;)I
    .locals 2

    .prologue
    .line 1826431
    check-cast p1, LX/26M;

    .line 1826432
    iget-object v1, p0, LX/Brd;->c:LX/Bra;

    .line 1826433
    iget-object v0, p1, LX/26M;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v0

    .line 1826434
    iget-object p1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, p1

    .line 1826435
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-direct {p0, v0}, LX/Brd;->b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)I

    move-result v0

    .line 1826436
    iget p0, v1, LX/Bra;->f:I

    invoke-static {v0, p0}, LX/0PB;->checkElementIndex(II)I

    .line 1826437
    iget-boolean p0, v1, LX/Bra;->e:Z

    if-nez p0, :cond_0

    iget p0, v1, LX/Bra;->f:I

    const/4 p1, 0x5

    if-ne p0, p1, :cond_1

    :cond_0
    iget-object p0, v1, LX/Bra;->a:LX/BrJ;

    invoke-interface {p0, v0}, LX/BrJ;->a(I)I

    move-result p0

    :goto_0
    move v0, p0

    .line 1826438
    return v0

    :cond_1
    iget-object p0, v1, LX/Bra;->b:LX/BrJ;

    invoke-interface {p0, v0}, LX/BrJ;->a(I)I

    move-result p0

    goto :goto_0
.end method

.method public final a()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/26M;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1826430
    iget-object v0, p0, LX/Brd;->f:LX/0Px;

    return-object v0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/26P;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1826423
    iget-object v0, p0, LX/Brd;->e:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1826424
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1826425
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    .line 1826426
    invoke-static {v0}, LX/Brd;->b(Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1826427
    sget-object v1, LX/Brd;->b:LX/0P1;

    invoke-virtual {v1, v0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/26P;

    return-object v0

    .line 1826428
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1826429
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Attachment not supported."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 1826422
    const/4 v0, 0x4

    return v0
.end method

.method public final b(LX/26N;)I
    .locals 2

    .prologue
    .line 1826414
    check-cast p1, LX/26M;

    .line 1826415
    iget-object v1, p0, LX/Brd;->c:LX/Bra;

    .line 1826416
    iget-object v0, p1, LX/26M;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v0

    .line 1826417
    iget-object p1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, p1

    .line 1826418
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-direct {p0, v0}, LX/Brd;->b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)I

    move-result v0

    .line 1826419
    iget p0, v1, LX/Bra;->f:I

    invoke-static {v0, p0}, LX/0PB;->checkElementIndex(II)I

    .line 1826420
    iget-boolean p0, v1, LX/Bra;->e:Z

    if-nez p0, :cond_0

    iget p0, v1, LX/Bra;->f:I

    const/4 p1, 0x5

    if-ne p0, p1, :cond_1

    :cond_0
    iget-object p0, v1, LX/Bra;->b:LX/BrJ;

    invoke-interface {p0, v0}, LX/BrJ;->a(I)I

    move-result p0

    :goto_0
    move v0, p0

    .line 1826421
    return v0

    :cond_1
    iget-object p0, v1, LX/Bra;->a:LX/BrJ;

    invoke-interface {p0, v0}, LX/BrJ;->a(I)I

    move-result p0

    goto :goto_0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 1826413
    const/4 v0, 0x6

    return v0
.end method

.method public final c(LX/26N;)I
    .locals 2

    .prologue
    .line 1826405
    check-cast p1, LX/26M;

    .line 1826406
    iget-object v1, p0, LX/Brd;->c:LX/Bra;

    .line 1826407
    iget-object v0, p1, LX/26M;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v0

    .line 1826408
    iget-object p1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, p1

    .line 1826409
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-direct {p0, v0}, LX/Brd;->b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)I

    move-result v0

    .line 1826410
    iget p0, v1, LX/Bra;->f:I

    invoke-static {v0, p0}, LX/0PB;->checkElementIndex(II)I

    .line 1826411
    iget-boolean p0, v1, LX/Bra;->e:Z

    if-nez p0, :cond_0

    iget p0, v1, LX/Bra;->f:I

    const/4 p1, 0x5

    if-ne p0, p1, :cond_1

    :cond_0
    iget-object p0, v1, LX/Bra;->c:LX/BrL;

    invoke-interface {p0, v0}, LX/BrL;->a(I)I

    move-result p0

    :goto_0
    move v0, p0

    .line 1826412
    return v0

    :cond_1
    iget-object p0, v1, LX/Bra;->d:LX/BrL;

    invoke-interface {p0, v0}, LX/BrL;->a(I)I

    move-result p0

    goto :goto_0
.end method

.method public final d(LX/26N;)I
    .locals 2

    .prologue
    .line 1826397
    check-cast p1, LX/26M;

    .line 1826398
    iget-object v1, p0, LX/Brd;->c:LX/Bra;

    .line 1826399
    iget-object v0, p1, LX/26M;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v0

    .line 1826400
    iget-object p1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, p1

    .line 1826401
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-direct {p0, v0}, LX/Brd;->b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)I

    move-result v0

    .line 1826402
    iget p0, v1, LX/Bra;->f:I

    invoke-static {v0, p0}, LX/0PB;->checkElementIndex(II)I

    .line 1826403
    iget-boolean p0, v1, LX/Bra;->e:Z

    if-nez p0, :cond_0

    iget p0, v1, LX/Bra;->f:I

    const/4 p1, 0x5

    if-ne p0, p1, :cond_1

    :cond_0
    iget-object p0, v1, LX/Bra;->d:LX/BrL;

    invoke-interface {p0, v0}, LX/BrL;->a(I)I

    move-result p0

    :goto_0
    move v0, p0

    .line 1826404
    return v0

    :cond_1
    iget-object p0, v1, LX/Bra;->c:LX/BrL;

    invoke-interface {p0, v0}, LX/BrL;->a(I)I

    move-result p0

    goto :goto_0
.end method
