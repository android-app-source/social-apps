.class public final LX/CcO;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;

.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public c:Landroid/view/Menu;

.field public d:Lcom/facebook/base/fragment/FbFragment;

.field public e:LX/5kD;

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryActionsGenerator;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;)V
    .locals 1

    .prologue
    .line 1920913
    iput-object p1, p0, LX/CcO;->a:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1920914
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/CcO;->b:Ljava/util/List;

    .line 1920915
    return-void
.end method

.method public static a(Landroid/net/Uri;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 1920908
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SEND"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1920909
    const-string v1, "image/jpeg"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 1920910
    const-string v1, "android.intent.extra.STREAM"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1920911
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 1920912
    return-object v0
.end method

.method public static a(LX/CcO;Landroid/content/Context;LX/5kD;)Landroid/view/MenuItem$OnMenuItemClickListener;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1920907
    new-instance v0, LX/Cc9;

    invoke-direct {v0, p0, p1, p2}, LX/Cc9;-><init>(LX/CcO;Landroid/content/Context;LX/5kD;)V

    return-object v0
.end method

.method public static a(LX/CcO;Landroid/content/Context;LX/5kD;Ljava/lang/String;Ljava/lang/String;)Landroid/view/MenuItem$OnMenuItemClickListener;
    .locals 6
    .param p2    # LX/5kD;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1920849
    new-instance v0, LX/CcF;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, LX/CcF;-><init>(LX/CcO;Landroid/content/Context;LX/5kD;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static a(LX/CcO;Lcom/facebook/base/fragment/FbFragment;LX/5kD;)Landroid/view/MenuItem$OnMenuItemClickListener;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1920906
    new-instance v0, LX/CcD;

    invoke-direct {v0, p0, p2, p1}, LX/CcD;-><init>(LX/CcO;LX/5kD;Lcom/facebook/base/fragment/FbFragment;)V

    return-object v0
.end method

.method public static a(IIILandroid/view/MenuItem$OnMenuItemClickListener;Landroid/view/Menu;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1920901
    invoke-interface {p4, p0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1920902
    :goto_0
    return-void

    .line 1920903
    :cond_0
    invoke-interface {p4, v1, p0, v1, p1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    .line 1920904
    invoke-interface {v0, p2}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 1920905
    invoke-interface {v0, p3}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public static synthetic a(LX/CcO;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1920899
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 1920900
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a$redex0(LX/CcO;J)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1920893
    iget-object v0, p0, LX/CcO;->a:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->B:LX/9fy;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Long;

    const/4 v2, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v1}, LX/0R9;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v1

    new-instance v2, Lcom/facebook/photos/base/photos/PhotoFetchInfo;

    sget-object v3, LX/3WA;->UNKNOWN:LX/3WA;

    sget-object v4, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-direct {v2, v3, v4}, Lcom/facebook/photos/base/photos/PhotoFetchInfo;-><init>(LX/3WA;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1920894
    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    .line 1920895
    new-instance v9, Lcom/facebook/photos/data/method/FetchPhotosMetadataParams;

    invoke-direct {v9, v1, v2}, Lcom/facebook/photos/data/method/FetchPhotosMetadataParams;-><init>(Ljava/util/List;Lcom/facebook/photos/base/photos/PhotoFetchInfo;)V

    .line 1920896
    const-string v5, "fetchPhotosMetadataParams"

    invoke-virtual {v7, v5, v9}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1920897
    iget-object v5, v0, LX/9fy;->a:LX/0aG;

    const-string v6, "fetch_photos_extra_logging_metadata"

    sget-object v8, LX/1ME;->BY_EXCEPTION:LX/1ME;

    invoke-virtual {v9}, Lcom/facebook/photos/data/method/FetchPhotosMetadataParams;->c()Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v9

    const v10, -0x20efc97a

    invoke-static/range {v5 .. v10}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v5

    invoke-interface {v5}, LX/1MF;->start()LX/1ML;

    move-result-object v5

    move-object v0, v5

    .line 1920898
    return-object v0
.end method

.method public static a$redex0(LX/CcO;Landroid/net/Uri;Landroid/content/Context;)V
    .locals 7

    .prologue
    .line 1920865
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1920866
    iget-object v0, p0, LX/CcO;->e:LX/5kD;

    if-eqz v0, :cond_1

    .line 1920867
    iget-object v0, p0, LX/CcO;->e:LX/5kD;

    invoke-interface {v0}, LX/5kD;->d()Ljava/lang/String;

    move-result-object v1

    .line 1920868
    iget-object v0, p0, LX/CcO;->e:LX/5kD;

    invoke-interface {v0}, LX/5kD;->Z()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataOwnerModel;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/CcO;->e:LX/5kD;

    invoke-interface {v0}, LX/5kD;->Z()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataOwnerModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataOwnerModel;->c()Ljava/lang/String;

    move-result-object v0

    .line 1920869
    :goto_0
    iget-object v2, p0, LX/CcO;->a:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;

    iget-object v2, v2, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->p:LX/745;

    .line 1920870
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1920871
    invoke-static {v2}, LX/745;->d(LX/745;)Ljava/util/HashMap;

    move-result-object v3

    .line 1920872
    const-string v4, "content_id"

    invoke-virtual {v3, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1920873
    if-eqz v0, :cond_0

    .line 1920874
    const-string v4, "owner_id"

    invoke-virtual {v3, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1920875
    :cond_0
    sget-object v4, LX/74R;->PHOTO_SHARE_EXTERNALLY:LX/74R;

    invoke-static {v2, v4, v3, v1}, LX/745;->a(LX/745;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 1920876
    :cond_1
    iget-object v0, p0, LX/CcO;->a:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->D:LX/0Uh;

    const/16 v1, 0x3cd

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1920877
    const/4 v6, 0x0

    .line 1920878
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1920879
    invoke-virtual {p2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-static {p1}, LX/CcO;->a(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v2

    const/high16 v3, 0x10000

    invoke-virtual {v0, v2, v3}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 1920880
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1920881
    :goto_1
    return-void

    .line 1920882
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 1920883
    :cond_3
    iget-object v0, p0, LX/CcO;->a:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->f:Lcom/facebook/content/SecureContextHelper;

    invoke-static {p1}, LX/CcO;->a(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    const v2, 0x7f0819df

    invoke-virtual {p2, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_1

    .line 1920884
    :cond_4
    invoke-virtual {p2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    .line 1920885
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_5
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 1920886
    invoke-static {p1}, LX/CcO;->a(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v4

    .line 1920887
    iget-object v5, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v5, v5, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v5, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_5

    .line 1920888
    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/PackageItemInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v4, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 1920889
    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1920890
    :cond_6
    invoke-interface {v1, v6}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    const v2, 0x7f0819df

    invoke-virtual {p2, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v2

    .line 1920891
    const-string v3, "android.intent.extra.INITIAL_INTENTS"

    new-array v0, v6, [Landroid/os/Parcelable;

    invoke-interface {v1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/os/Parcelable;

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1920892
    iget-object v0, p0, LX/CcO;->a:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->f:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v0, v2, p2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_1
.end method

.method public static b(LX/CcO;Landroid/content/Context;LX/5kD;)Landroid/view/MenuItem$OnMenuItemClickListener;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1920864
    new-instance v0, LX/CcE;

    invoke-direct {v0, p0, p1, p2}, LX/CcE;-><init>(LX/CcO;Landroid/content/Context;LX/5kD;)V

    return-object v0
.end method

.method public static b(LX/CcO;Landroid/content/Context;LX/5kD;Ljava/lang/String;Ljava/lang/String;)Landroid/view/MenuItem$OnMenuItemClickListener;
    .locals 6
    .param p2    # LX/5kD;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1920863
    new-instance v0, LX/CcG;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, LX/CcG;-><init>(LX/CcO;Landroid/content/Context;LX/5kD;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static c(LX/CcO;LX/5kD;)Landroid/view/MenuItem$OnMenuItemClickListener;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1920862
    new-instance v0, LX/CcJ;

    invoke-direct {v0, p0, p1}, LX/CcJ;-><init>(LX/CcO;LX/5kD;)V

    return-object v0
.end method

.method public static c(LX/CcO;Landroid/content/Context;LX/5kD;)Landroid/view/MenuItem$OnMenuItemClickListener;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1920861
    new-instance v0, LX/CcK;

    invoke-direct {v0, p0, p1, p2}, LX/CcK;-><init>(LX/CcO;Landroid/content/Context;LX/5kD;)V

    return-object v0
.end method

.method public static d(LX/CcO;LX/5kD;)Landroid/view/MenuItem$OnMenuItemClickListener;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1920860
    new-instance v0, LX/CcN;

    invoke-direct {v0, p0, p1}, LX/CcN;-><init>(LX/CcO;LX/5kD;)V

    return-object v0
.end method

.method public static d(LX/CcO;Landroid/content/Context;LX/5kD;)Landroid/view/MenuItem$OnMenuItemClickListener;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1920859
    new-instance v0, LX/CcL;

    invoke-direct {v0, p0, p1, p2}, LX/CcL;-><init>(LX/CcO;Landroid/content/Context;LX/5kD;)V

    return-object v0
.end method

.method public static e(LX/CcO;Landroid/content/Context;LX/5kD;)Landroid/view/MenuItem$OnMenuItemClickListener;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1920858
    new-instance v0, LX/Cbt;

    invoke-direct {v0, p0, p2, p1}, LX/Cbt;-><init>(LX/CcO;LX/5kD;Landroid/content/Context;)V

    return-object v0
.end method

.method public static l(LX/CcO;)V
    .locals 2

    .prologue
    .line 1920856
    iget-object v0, p0, LX/CcO;->a:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->t:Landroid/support/v4/app/FragmentActivity;

    new-instance v1, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper$Builder$18;

    invoke-direct {v1, p0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper$Builder$18;-><init>(LX/CcO;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 1920857
    return-void
.end method

.method public static m(LX/CcO;)V
    .locals 2

    .prologue
    .line 1920854
    iget-object v0, p0, LX/CcO;->a:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->t:Landroid/support/v4/app/FragmentActivity;

    new-instance v1, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper$Builder$19;

    invoke-direct {v1, p0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper$Builder$19;-><init>(LX/CcO;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 1920855
    return-void
.end method

.method public static n(LX/CcO;)V
    .locals 2

    .prologue
    .line 1920852
    iget-object v0, p0, LX/CcO;->a:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->t:Landroid/support/v4/app/FragmentActivity;

    new-instance v1, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper$Builder$20;

    invoke-direct {v1, p0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper$Builder$20;-><init>(LX/CcO;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 1920853
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;)LX/1cj;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "LX/1cj",
            "<",
            "LX/1FJ",
            "<",
            "Lcom/facebook/imagepipeline/memory/PooledByteBuffer;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1920851
    new-instance v0, LX/Cbv;

    invoke-direct {v0, p0, p1}, LX/Cbv;-><init>(LX/CcO;Landroid/content/Context;)V

    return-object v0
.end method

.method public final b()LX/1cj;
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1cj",
            "<",
            "LX/1FJ",
            "<",
            "Lcom/facebook/imagepipeline/memory/PooledByteBuffer;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1920850
    new-instance v0, LX/Cc8;

    invoke-direct {v0, p0}, LX/Cc8;-><init>(LX/CcO;)V

    return-object v0
.end method
