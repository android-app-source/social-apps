.class public final LX/C4d;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/8TZ;

.field public final synthetic b:LX/8Ta;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

.field public final synthetic e:Lcom/facebook/feedplugins/games/calltoaction/QuicksilverCallToActionPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/games/calltoaction/QuicksilverCallToActionPartDefinition;LX/8TZ;LX/8Ta;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStoryActionLink;)V
    .locals 0

    .prologue
    .line 1847704
    iput-object p1, p0, LX/C4d;->e:Lcom/facebook/feedplugins/games/calltoaction/QuicksilverCallToActionPartDefinition;

    iput-object p2, p0, LX/C4d;->a:LX/8TZ;

    iput-object p3, p0, LX/C4d;->b:LX/8Ta;

    iput-object p4, p0, LX/C4d;->c:Ljava/lang/String;

    iput-object p5, p0, LX/C4d;->d:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x57a86bb3

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1847705
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v3, Lcom/facebook/quicksilver/QuicksilverActivity;

    invoke-direct {v2, v0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1847706
    const-string v0, "source"

    iget-object v3, p0, LX/C4d;->a:LX/8TZ;

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 1847707
    const-string v0, "source_context"

    iget-object v3, p0, LX/C4d;->b:LX/8Ta;

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 1847708
    const-string v0, "source_id"

    iget-object v3, p0, LX/C4d;->c:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1847709
    const-string v0, "app_id"

    iget-object v3, p0, LX/C4d;->d:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->r()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLApplication;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1847710
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1847711
    instance-of v3, v0, Landroid/app/Activity;

    if-eqz v3, :cond_0

    .line 1847712
    iget-object v3, p0, LX/C4d;->e:Lcom/facebook/feedplugins/games/calltoaction/QuicksilverCallToActionPartDefinition;

    iget-object v3, v3, Lcom/facebook/feedplugins/games/calltoaction/QuicksilverCallToActionPartDefinition;->b:Lcom/facebook/content/SecureContextHelper;

    const/16 v4, 0x22b2

    check-cast v0, Landroid/app/Activity;

    invoke-interface {v3, v2, v4, v0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 1847713
    :goto_0
    const v0, -0x1e951f9a

    invoke-static {v0, v1}, LX/02F;->a(II)V

    return-void

    .line 1847714
    :cond_0
    iget-object v3, p0, LX/C4d;->e:Lcom/facebook/feedplugins/games/calltoaction/QuicksilverCallToActionPartDefinition;

    iget-object v3, v3, Lcom/facebook/feedplugins/games/calltoaction/QuicksilverCallToActionPartDefinition;->b:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v3, v2, v0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0
.end method
