.class public final LX/CcL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:Landroid/content/Context;

.field public final synthetic b:LX/5kD;

.field public final synthetic c:LX/CcO;


# direct methods
.method public constructor <init>(LX/CcO;Landroid/content/Context;LX/5kD;)V
    .locals 0

    .prologue
    .line 1920828
    iput-object p1, p0, LX/CcL;->c:LX/CcO;

    iput-object p2, p0, LX/CcL;->a:Landroid/content/Context;

    iput-object p3, p0, LX/CcL;->b:LX/5kD;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 7

    .prologue
    .line 1920829
    iget-object v0, p0, LX/CcL;->c:LX/CcO;

    iget-object v0, v0, LX/CcO;->a:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->g:LX/1Ck;

    const/16 v1, 0x7d7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p0, LX/CcL;->c:LX/CcO;

    iget-object v2, v2, LX/CcO;->h:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryActionsGenerator;

    iget-object v3, p0, LX/CcL;->a:Landroid/content/Context;

    .line 1920830
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v4

    .line 1920831
    new-instance v5, LX/0ju;

    invoke-direct {v5, v3}, LX/0ju;-><init>(Landroid/content/Context;)V

    const v6, 0x7f0819ec

    invoke-virtual {v5, v6}, LX/0ju;->a(I)LX/0ju;

    move-result-object v5

    const v6, 0x7f0819ed

    invoke-virtual {v5, v6}, LX/0ju;->b(I)LX/0ju;

    move-result-object v5

    const v6, 0x7f080017

    new-instance p1, LX/Cbb;

    invoke-direct {p1, v2, v4}, LX/Cbb;-><init>(Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryActionsGenerator;Lcom/google/common/util/concurrent/SettableFuture;)V

    invoke-virtual {v5, v6, p1}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v5

    new-instance v6, LX/Cba;

    invoke-direct {v6, v2, v4}, LX/Cba;-><init>(Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryActionsGenerator;Lcom/google/common/util/concurrent/SettableFuture;)V

    invoke-virtual {v5, v6}, LX/0ju;->a(Landroid/content/DialogInterface$OnCancelListener;)LX/0ju;

    move-result-object v5

    const v6, 0x7f080019

    new-instance p1, LX/CbZ;

    invoke-direct {p1, v2, v4}, LX/CbZ;-><init>(Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryActionsGenerator;Lcom/google/common/util/concurrent/SettableFuture;)V

    invoke-virtual {v5, v6, p1}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v5

    invoke-virtual {v5}, LX/0ju;->b()LX/2EJ;

    .line 1920832
    move-object v2, v4

    .line 1920833
    iget-object v3, p0, LX/CcL;->c:LX/CcO;

    iget-object v4, p0, LX/CcL;->b:LX/5kD;

    .line 1920834
    new-instance v5, LX/CcC;

    invoke-direct {v5, v3, v4}, LX/CcC;-><init>(LX/CcO;LX/5kD;)V

    move-object v3, v5

    .line 1920835
    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1920836
    const/4 v0, 0x1

    return v0
.end method
