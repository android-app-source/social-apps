.class public LX/Af4;
.super LX/AeQ;
.source ""


# static fields
.field public static final f:Ljava/lang/String;


# instance fields
.field private final g:Ljava/util/concurrent/ExecutorService;

.field private final h:LX/AaZ;

.field public final i:LX/03V;

.field private final j:LX/0tX;

.field public k:Z

.field public volatile l:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoNewestCommentsQueryModel;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1698667
    const-class v0, LX/Af4;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Af4;->f:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/ExecutorService;LX/0tX;LX/AaZ;LX/03V;LX/0SG;)V
    .locals 0
    .param p1    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1698637
    invoke-direct {p0, p5}, LX/AeQ;-><init>(LX/0SG;)V

    .line 1698638
    iput-object p1, p0, LX/Af4;->g:Ljava/util/concurrent/ExecutorService;

    .line 1698639
    iput-object p2, p0, LX/Af4;->j:LX/0tX;

    .line 1698640
    iput-object p3, p0, LX/Af4;->h:LX/AaZ;

    .line 1698641
    iput-object p4, p0, LX/Af4;->i:LX/03V;

    .line 1698642
    return-void
.end method

.method public static a$redex0(LX/Af4;Ljava/lang/Throwable;)V
    .locals 3
    .param p0    # LX/Af4;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    .line 1698643
    iget-boolean v0, p0, LX/AeQ;->e:Z

    if-nez v0, :cond_1

    move v0, v1

    .line 1698644
    :goto_0
    iput-boolean v1, p0, LX/Af4;->e:Z

    .line 1698645
    iget-object v1, p0, LX/AeQ;->b:LX/AeV;

    if-eqz v1, :cond_0

    .line 1698646
    iget-object v1, p0, LX/AeQ;->b:LX/AeV;

    invoke-virtual {p0}, LX/Af4;->g()LX/AeN;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, LX/AeV;->a(LX/AeN;Z)V

    .line 1698647
    :cond_0
    return-void

    .line 1698648
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized c()V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1698649
    monitor-enter p0

    :try_start_0
    invoke-super {p0}, LX/AeQ;->c()V

    .line 1698650
    iget-object v1, p0, LX/AeQ;->c:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1698651
    iget-object v0, p0, LX/Af4;->i:LX/03V;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, LX/Af4;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_startFetching"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Tried to fetch without a story id."

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1698652
    :goto_0
    monitor-exit p0

    return-void

    .line 1698653
    :cond_0
    :try_start_1
    new-instance v1, LX/6SA;

    invoke-direct {v1}, LX/6SA;-><init>()V

    move-object v1, v1

    .line 1698654
    const-string v2, "targetID"

    iget-object v3, p0, LX/AeQ;->c:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1698655
    const-string v2, "count"

    const/4 v3, 0x5

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1698656
    const-string v2, "translation_enabled"

    iget-object v3, p0, LX/Af4;->h:LX/AaZ;

    invoke-virtual {v3}, LX/AaZ;->a()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-boolean v3, p0, LX/Af4;->k:Z

    if-nez v3, :cond_1

    const/4 v0, 0x1

    :cond_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 1698657
    iget-object v0, p0, LX/Af4;->j:LX/0tX;

    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    iput-object v0, p0, LX/Af4;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1698658
    iget-object v0, p0, LX/Af4;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v1, LX/Af3;

    invoke-direct {v1, p0}, LX/Af3;-><init>(LX/Af4;)V

    iget-object v2, p0, LX/Af4;->g:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1698659
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 1698660
    const/4 v0, 0x3

    return v0
.end method

.method public final declared-synchronized e()V
    .locals 2

    .prologue
    .line 1698661
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Af4;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    .line 1698662
    iget-object v0, p0, LX/Af4;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1698663
    :cond_0
    monitor-exit p0

    return-void

    .line 1698664
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized f()Z
    .locals 1

    .prologue
    .line 1698665
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Af4;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Af4;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isDone()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final g()LX/AeN;
    .locals 1

    .prologue
    .line 1698666
    sget-object v0, LX/AeN;->LIVE_COMMENT_EVENT:LX/AeN;

    return-object v0
.end method
