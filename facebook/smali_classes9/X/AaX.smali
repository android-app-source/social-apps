.class public LX/AaX;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private final b:LX/0tX;

.field private final c:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation
.end field

.field public final d:LX/03V;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1688670
    const-class v0, LX/AaX;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/AaX;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0tX;LX/03V;Ljava/util/concurrent/ExecutorService;)V
    .locals 0
    .param p3    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1688677
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1688678
    iput-object p1, p0, LX/AaX;->b:LX/0tX;

    .line 1688679
    iput-object p3, p0, LX/AaX;->c:Ljava/util/concurrent/ExecutorService;

    .line 1688680
    iput-object p2, p0, LX/AaX;->d:LX/03V;

    .line 1688681
    return-void
.end method


# virtual methods
.method public final a(LX/AXW;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1688671
    new-instance v0, LX/AbC;

    invoke-direct {v0}, LX/AbC;-><init>()V

    move-object v0, v0

    .line 1688672
    const-string v1, "videoID"

    invoke-virtual {v0, v1, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1688673
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 1688674
    iget-object v1, p0, LX/AaX;->b:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 1688675
    new-instance v1, LX/AaW;

    invoke-direct {v1, p0, p1}, LX/AaW;-><init>(LX/AaX;LX/AXW;)V

    iget-object v2, p0, LX/AaX;->c:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1688676
    return-void
.end method
