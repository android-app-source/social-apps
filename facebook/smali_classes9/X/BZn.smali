.class public LX/BZn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0pQ;


# static fields
.field public static final a:LX/0Tn;

.field public static final b:LX/0Tn;

.field public static final c:LX/0Tn;

.field public static final d:LX/0Tn;

.field public static final e:LX/0Tn;

.field public static final f:LX/0Tn;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1798006
    sget-object v0, LX/2tq;->a:LX/0Tn;

    const-string v1, "isr/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 1798007
    sput-object v0, LX/BZn;->a:LX/0Tn;

    const-string v1, "api_ping_response"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/BZn;->b:LX/0Tn;

    .line 1798008
    sget-object v0, LX/BZn;->a:LX/0Tn;

    const-string v1, "report"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/BZn;->c:LX/0Tn;

    .line 1798009
    sget-object v0, LX/BZn;->a:LX/0Tn;

    const-string v1, "displayed"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/BZn;->d:LX/0Tn;

    .line 1798010
    sget-object v0, LX/BZn;->a:LX/0Tn;

    const-string v1, "report_sent"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/BZn;->e:LX/0Tn;

    .line 1798011
    sget-object v0, LX/BZn;->a:LX/0Tn;

    const-string v1, "dialog_save_state"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/BZn;->f:LX/0Tn;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1798012
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1798013
    return-void
.end method


# virtual methods
.method public final a()LX/0Rf;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "LX/0Tn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1798014
    sget-object v0, LX/BZn;->b:LX/0Tn;

    sget-object v1, LX/BZn;->c:LX/0Tn;

    sget-object v2, LX/BZn;->d:LX/0Tn;

    sget-object v3, LX/BZn;->e:LX/0Tn;

    sget-object v4, LX/BZn;->f:LX/0Tn;

    invoke-static {v0, v1, v2, v3, v4}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method
