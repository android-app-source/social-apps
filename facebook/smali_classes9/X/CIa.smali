.class public LX/CIa;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1874370
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1874371
    if-nez p0, :cond_1

    .line 1874372
    const/4 p0, 0x0

    .line 1874373
    :cond_0
    :goto_0
    return-object p0

    :cond_1
    const-string v0, "#"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "#"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 10

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1874374
    if-nez p0, :cond_1

    if-nez p1, :cond_1

    .line 1874375
    :cond_0
    :goto_0
    return v0

    .line 1874376
    :cond_1
    if-eqz p0, :cond_2

    invoke-static {p0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Landroid/graphics/Color;->alpha(I)I

    move-result v2

    const/16 v3, 0x32

    if-ge v2, v3, :cond_3

    :cond_2
    move v2, v0

    .line 1874377
    :goto_1
    if-eqz v2, :cond_4

    .line 1874378
    :goto_2
    if-eqz p1, :cond_0

    .line 1874379
    invoke-static {p1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    .line 1874380
    invoke-static {v2}, Landroid/graphics/Color;->red(I)I

    move-result v3

    int-to-float v3, v3

    .line 1874381
    invoke-static {v2}, Landroid/graphics/Color;->green(I)I

    move-result v4

    int-to-float v4, v4

    .line 1874382
    invoke-static {v2}, Landroid/graphics/Color;->blue(I)I

    move-result v2

    int-to-float v2, v2

    .line 1874383
    const-wide v6, 0x3fcb333333333333L    # 0.2125

    float-to-double v8, v3

    mul-double/2addr v6, v8

    const-wide v8, 0x3fe6e48e8a71de6aL    # 0.7154

    float-to-double v4, v4

    mul-double/2addr v4, v8

    add-double/2addr v4, v6

    const-wide v6, 0x3fb27525460aa64cL    # 0.0721

    float-to-double v2, v2

    mul-double/2addr v2, v6

    add-double/2addr v2, v4

    double-to-int v2, v2

    .line 1874384
    const/16 v3, 0x7f

    if-gt v2, v3, :cond_0

    move v0, v1

    goto :goto_0

    :cond_3
    move v2, v1

    .line 1874385
    goto :goto_1

    :cond_4
    move-object p1, p0

    .line 1874386
    goto :goto_2
.end method
