.class public final LX/BxA;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/BxC;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

.field public c:LX/1Pm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public final synthetic d:LX/BxC;


# direct methods
.method public constructor <init>(LX/BxC;)V
    .locals 1

    .prologue
    .line 1835066
    iput-object p1, p0, LX/BxA;->d:LX/BxC;

    .line 1835067
    move-object v0, p1

    .line 1835068
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1835069
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1835070
    const-string v0, "ArticleSaveButtonComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1835071
    if-ne p0, p1, :cond_1

    .line 1835072
    :cond_0
    :goto_0
    return v0

    .line 1835073
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1835074
    goto :goto_0

    .line 1835075
    :cond_3
    check-cast p1, LX/BxA;

    .line 1835076
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1835077
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1835078
    if-eq v2, v3, :cond_0

    .line 1835079
    iget-object v2, p0, LX/BxA;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/BxA;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/BxA;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1835080
    goto :goto_0

    .line 1835081
    :cond_5
    iget-object v2, p1, LX/BxA;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 1835082
    :cond_6
    iget-object v2, p0, LX/BxA;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/BxA;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    iget-object v3, p1, LX/BxA;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v2, v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1835083
    goto :goto_0

    .line 1835084
    :cond_8
    iget-object v2, p1, LX/BxA;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    if-nez v2, :cond_7

    .line 1835085
    :cond_9
    iget-object v2, p0, LX/BxA;->c:LX/1Pm;

    if-eqz v2, :cond_a

    iget-object v2, p0, LX/BxA;->c:LX/1Pm;

    iget-object v3, p1, LX/BxA;->c:LX/1Pm;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1835086
    goto :goto_0

    .line 1835087
    :cond_a
    iget-object v2, p1, LX/BxA;->c:LX/1Pm;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
