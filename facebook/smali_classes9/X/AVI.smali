.class public LX/AVI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field public final a:Landroid/graphics/Rect;

.field private final b:[I

.field private final c:Lcom/facebook/facecastdisplay/FullScreenAdjustResizeHelper$RefocusRunnable;

.field public final d:Landroid/view/ViewGroup;

.field private e:I

.field public f:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/ViewGroup;)V
    .locals 2

    .prologue
    .line 1679142
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1679143
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, LX/AVI;->a:Landroid/graphics/Rect;

    .line 1679144
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, LX/AVI;->b:[I

    .line 1679145
    new-instance v0, Lcom/facebook/facecastdisplay/FullScreenAdjustResizeHelper$RefocusRunnable;

    invoke-direct {v0, p0}, Lcom/facebook/facecastdisplay/FullScreenAdjustResizeHelper$RefocusRunnable;-><init>(LX/AVI;)V

    iput-object v0, p0, LX/AVI;->c:Lcom/facebook/facecastdisplay/FullScreenAdjustResizeHelper$RefocusRunnable;

    .line 1679146
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 1679147
    iput-object p1, p0, LX/AVI;->d:Landroid/view/ViewGroup;

    .line 1679148
    const/4 p1, 0x0

    .line 1679149
    new-instance v0, Landroid/widget/EditText;

    iget-object v1, p0, LX/AVI;->d:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/AVI;->f:Landroid/view/View;

    .line 1679150
    iget-object v0, p0, LX/AVI;->d:Landroid/view/ViewGroup;

    iget-object v1, p0, LX/AVI;->f:Landroid/view/View;

    invoke-virtual {v0, v1, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 1679151
    iget-object v0, p0, LX/AVI;->f:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 1679152
    iput p1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1679153
    const/4 v1, -0x1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1679154
    iget-object v0, p0, LX/AVI;->f:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1679155
    return-void
.end method

.method public static a(Landroid/view/ViewGroup;)LX/AVI;
    .locals 1

    .prologue
    .line 1679156
    new-instance v0, LX/AVI;

    invoke-direct {v0, p0}, LX/AVI;-><init>(Landroid/view/ViewGroup;)V

    return-object v0
.end method


# virtual methods
.method public a(II)Z
    .locals 2

    .prologue
    .line 1679157
    iget-object v0, p0, LX/AVI;->d:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 1679158
    iput p1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1679159
    iget-object v1, p0, LX/AVI;->d:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1679160
    const/4 v0, 0x1

    return v0
.end method

.method public final onGlobalLayout()V
    .locals 4

    .prologue
    .line 1679161
    iget-object v0, p0, LX/AVI;->d:Landroid/view/ViewGroup;

    iget-object v1, p0, LX/AVI;->a:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 1679162
    iget-object v0, p0, LX/AVI;->a:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    iget-object v1, p0, LX/AVI;->a:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    sub-int/2addr v0, v1

    .line 1679163
    iget v1, p0, LX/AVI;->e:I

    if-ne v0, v1, :cond_1

    .line 1679164
    :cond_0
    :goto_0
    return-void

    .line 1679165
    :cond_1
    iget v1, p0, LX/AVI;->e:I

    .line 1679166
    iput v0, p0, LX/AVI;->e:I

    .line 1679167
    invoke-virtual {p0, v0, v1}, LX/AVI;->a(II)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1679168
    if-ge v0, v1, :cond_0

    .line 1679169
    iget-object v1, p0, LX/AVI;->d:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->findFocus()Landroid/view/View;

    move-result-object v1

    .line 1679170
    iget-object v2, p0, LX/AVI;->c:Lcom/facebook/facecastdisplay/FullScreenAdjustResizeHelper$RefocusRunnable;

    iput-object v1, v2, Lcom/facebook/facecastdisplay/FullScreenAdjustResizeHelper$RefocusRunnable;->a:Landroid/view/View;

    .line 1679171
    if-eqz v1, :cond_0

    .line 1679172
    iget-object v2, p0, LX/AVI;->b:[I

    invoke-virtual {v1, v2}, Landroid/view/View;->getLocationInWindow([I)V

    .line 1679173
    iget-object v2, p0, LX/AVI;->b:[I

    const/4 v3, 0x1

    aget v2, v2, v3

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    add-int/2addr v1, v2

    if-gt v0, v1, :cond_0

    .line 1679174
    iget-object v0, p0, LX/AVI;->f:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1679175
    iget-object v0, p0, LX/AVI;->f:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 1679176
    iget-object v0, p0, LX/AVI;->d:Landroid/view/ViewGroup;

    iget-object v1, p0, LX/AVI;->c:Lcom/facebook/facecastdisplay/FullScreenAdjustResizeHelper$RefocusRunnable;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method
