.class public final LX/AjA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$FetchCurationFlowModel$FeedCurationFlowStepModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0TF;

.field public final synthetic b:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic c:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

.field public final synthetic d:Ljava/lang/String;

.field public final synthetic e:LX/1du;


# direct methods
.method public constructor <init>(LX/1du;LX/0TF;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1707640
    iput-object p1, p0, LX/AjA;->e:LX/1du;

    iput-object p2, p0, LX/AjA;->a:LX/0TF;

    iput-object p3, p0, LX/AjA;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iput-object p4, p0, LX/AjA;->c:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    iput-object p5, p0, LX/AjA;->d:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 1707636
    iget-object v0, p0, LX/AjA;->a:LX/0TF;

    if-eqz v0, :cond_0

    .line 1707637
    iget-object v0, p0, LX/AjA;->a:LX/0TF;

    invoke-interface {v0, p1}, LX/0TF;->onFailure(Ljava/lang/Throwable;)V

    .line 1707638
    :cond_0
    iget-object v0, p0, LX/AjA;->e:LX/1du;

    iget-object v1, p0, LX/AjA;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p0, LX/AjA;->c:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    iget-object v3, p0, LX/AjA;->d:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3}, LX/1du;->a$redex0(LX/1du;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1707639
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1707632
    check-cast p1, Lcom/facebook/feed/protocol/FetchCurationFlowGraphQLModels$FetchCurationFlowModel$FeedCurationFlowStepModel;

    .line 1707633
    iget-object v0, p0, LX/AjA;->a:LX/0TF;

    if-eqz v0, :cond_0

    .line 1707634
    iget-object v0, p0, LX/AjA;->a:LX/0TF;

    invoke-interface {v0, p1}, LX/0TF;->onSuccess(Ljava/lang/Object;)V

    .line 1707635
    :cond_0
    return-void
.end method
