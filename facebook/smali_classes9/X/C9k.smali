.class public final LX/C9k;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/C9m;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/1Pn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public c:Ljava/lang/Integer;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "labelState"
        processor = "com.facebook.thecount.transformer.Transformer"
    .end annotation
.end field

.field public final synthetic d:LX/C9m;


# direct methods
.method public constructor <init>(LX/C9m;)V
    .locals 1

    .prologue
    .line 1854383
    iput-object p1, p0, LX/C9k;->d:LX/C9m;

    .line 1854384
    move-object v0, p1

    .line 1854385
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1854386
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1854407
    const-string v0, "OfflineRetryComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1854390
    if-ne p0, p1, :cond_1

    .line 1854391
    :cond_0
    :goto_0
    return v0

    .line 1854392
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1854393
    goto :goto_0

    .line 1854394
    :cond_3
    check-cast p1, LX/C9k;

    .line 1854395
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1854396
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1854397
    if-eq v2, v3, :cond_0

    .line 1854398
    iget-object v2, p0, LX/C9k;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/C9k;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/C9k;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1854399
    goto :goto_0

    .line 1854400
    :cond_5
    iget-object v2, p1, LX/C9k;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 1854401
    :cond_6
    iget-object v2, p0, LX/C9k;->b:LX/1Pn;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/C9k;->b:LX/1Pn;

    iget-object v3, p1, LX/C9k;->b:LX/1Pn;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1854402
    goto :goto_0

    .line 1854403
    :cond_8
    iget-object v2, p1, LX/C9k;->b:LX/1Pn;

    if-nez v2, :cond_7

    .line 1854404
    :cond_9
    iget-object v2, p0, LX/C9k;->c:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2, v4}, LX/3CW;->c(II)Z

    move-result v2

    if-nez v2, :cond_a

    iget-object v2, p0, LX/C9k;->c:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v3, p1, LX/C9k;->c:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, LX/3CW;->a(II)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1854405
    goto :goto_0

    .line 1854406
    :cond_a
    iget-object v2, p1, LX/C9k;->c:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2, v4}, LX/3CW;->c(II)Z

    move-result v2

    if-nez v2, :cond_0

    goto :goto_1
.end method

.method public final g()LX/1X1;
    .locals 2

    .prologue
    .line 1854387
    invoke-super {p0}, LX/1X1;->g()LX/1X1;

    move-result-object v0

    check-cast v0, LX/C9k;

    .line 1854388
    const/4 v1, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, LX/C9k;->c:Ljava/lang/Integer;

    .line 1854389
    return-object v0
.end method
