.class public final LX/AWq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/AWy;


# direct methods
.method public constructor <init>(LX/AWy;)V
    .locals 0

    .prologue
    .line 1683418
    iput-object p1, p0, LX/AWq;->a:LX/AWy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x663c5c71

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1683419
    iget-object v0, p0, LX/AWq;->a:LX/AWy;

    iget-object v0, v0, LX/AWy;->D:Lcom/facebook/facecast/model/FacecastPrivacyData;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/AWq;->a:LX/AWy;

    iget-object v0, v0, LX/AWy;->D:Lcom/facebook/facecast/model/FacecastPrivacyData;

    .line 1683420
    iget-object v2, v0, Lcom/facebook/facecast/model/FacecastPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-object v0, v2

    .line 1683421
    if-eqz v0, :cond_0

    .line 1683422
    new-instance v2, Lcom/facebook/privacy/selector/AudienceFragmentDialog;

    invoke-direct {v2}, Lcom/facebook/privacy/selector/AudienceFragmentDialog;-><init>()V

    .line 1683423
    new-instance v0, LX/AWv;

    iget-object v3, p0, LX/AWq;->a:LX/AWy;

    invoke-direct {v0, v3}, LX/AWv;-><init>(LX/AWy;)V

    .line 1683424
    iput-object v0, v2, Lcom/facebook/privacy/selector/AudienceFragmentDialog;->q:LX/8Q5;

    .line 1683425
    new-instance v0, LX/AWw;

    iget-object v3, p0, LX/AWq;->a:LX/AWy;

    invoke-direct {v0, v3}, LX/AWw;-><init>(LX/AWy;)V

    invoke-virtual {v2, v0}, Lcom/facebook/privacy/selector/AudienceFragmentDialog;->a(LX/8Qm;)V

    .line 1683426
    iget-object v0, p0, LX/AWq;->a:LX/AWy;

    iget-object v0, v0, LX/AWy;->b:LX/8RJ;

    sget-object v3, LX/8RI;->FACECAST_ACTIVITY:LX/8RI;

    invoke-virtual {v0, v3}, LX/8RJ;->a(LX/8RI;)V

    .line 1683427
    iget-object v0, p0, LX/AWq;->a:LX/AWy;

    invoke-virtual {v0}, LX/AWy;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v3, Landroid/support/v4/app/FragmentActivity;

    invoke-static {v0, v3}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    .line 1683428
    if-eqz v0, :cond_0

    .line 1683429
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const-string v3, "FACECAST_AUDIENCE_FRAGMENT_TAG"

    invoke-virtual {v2, v0, v3}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 1683430
    :cond_0
    const v0, 0x1b248691

    invoke-static {v4, v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
