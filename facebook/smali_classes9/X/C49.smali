.class public final LX/C49;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/39e;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/1Po;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public c:Z

.field public d:I

.field public e:LX/21I;

.field public f:[F

.field public g:Ljava/lang/Boolean;

.field public h:LX/1zt;

.field public final synthetic i:LX/39e;


# direct methods
.method public constructor <init>(LX/39e;)V
    .locals 1

    .prologue
    .line 1846899
    iput-object p1, p0, LX/C49;->i:LX/39e;

    .line 1846900
    move-object v0, p1

    .line 1846901
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1846902
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1846930
    const-string v0, "ReactionsFooterComponent"

    return-object v0
.end method

.method public final a(LX/1X1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1",
            "<",
            "LX/39e;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1846931
    check-cast p1, LX/C49;

    .line 1846932
    iget-object v0, p1, LX/C49;->e:LX/21I;

    iput-object v0, p0, LX/C49;->e:LX/21I;

    .line 1846933
    iget-object v0, p1, LX/C49;->f:[F

    iput-object v0, p0, LX/C49;->f:[F

    .line 1846934
    iget-object v0, p1, LX/C49;->g:Ljava/lang/Boolean;

    iput-object v0, p0, LX/C49;->g:Ljava/lang/Boolean;

    .line 1846935
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1846909
    if-ne p0, p1, :cond_1

    .line 1846910
    :cond_0
    :goto_0
    return v0

    .line 1846911
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1846912
    goto :goto_0

    .line 1846913
    :cond_3
    check-cast p1, LX/C49;

    .line 1846914
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1846915
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1846916
    if-eq v2, v3, :cond_0

    .line 1846917
    iget-object v2, p0, LX/C49;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/C49;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/C49;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1846918
    goto :goto_0

    .line 1846919
    :cond_5
    iget-object v2, p1, LX/C49;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 1846920
    :cond_6
    iget-object v2, p0, LX/C49;->b:LX/1Po;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/C49;->b:LX/1Po;

    iget-object v3, p1, LX/C49;->b:LX/1Po;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1846921
    goto :goto_0

    .line 1846922
    :cond_8
    iget-object v2, p1, LX/C49;->b:LX/1Po;

    if-nez v2, :cond_7

    .line 1846923
    :cond_9
    iget-boolean v2, p0, LX/C49;->c:Z

    iget-boolean v3, p1, LX/C49;->c:Z

    if-eq v2, v3, :cond_a

    move v0, v1

    .line 1846924
    goto :goto_0

    .line 1846925
    :cond_a
    iget v2, p0, LX/C49;->d:I

    iget v3, p1, LX/C49;->d:I

    if-eq v2, v3, :cond_b

    move v0, v1

    .line 1846926
    goto :goto_0

    .line 1846927
    :cond_b
    iget-object v2, p0, LX/C49;->h:LX/1zt;

    if-eqz v2, :cond_c

    iget-object v2, p0, LX/C49;->h:LX/1zt;

    iget-object v3, p1, LX/C49;->h:LX/1zt;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1846928
    goto :goto_0

    .line 1846929
    :cond_c
    iget-object v2, p1, LX/C49;->h:LX/1zt;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public final g()LX/1X1;
    .locals 2

    .prologue
    .line 1846903
    const/4 v1, 0x0

    .line 1846904
    invoke-super {p0}, LX/1X1;->g()LX/1X1;

    move-result-object v0

    check-cast v0, LX/C49;

    .line 1846905
    iput-object v1, v0, LX/C49;->e:LX/21I;

    .line 1846906
    iput-object v1, v0, LX/C49;->f:[F

    .line 1846907
    iput-object v1, v0, LX/C49;->g:Ljava/lang/Boolean;

    .line 1846908
    return-object v0
.end method
