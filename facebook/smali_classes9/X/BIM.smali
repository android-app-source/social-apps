.class public LX/BIM;
.super LX/BIH;
.source ""


# instance fields
.field public d:LX/9d6;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/8GN;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/9dO;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private h:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:LX/9d5;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:LX/BGf;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/optic/CameraPreviewView;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1770262
    invoke-direct {p0, p1}, LX/BIH;-><init>(Landroid/content/Context;)V

    .line 1770263
    const-class p1, LX/BIM;

    invoke-static {p1, p0}, LX/BIM;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1770264
    const p1, 0x7f0d0861

    invoke-static {p0, p1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/optic/CameraPreviewView;

    iput-object p1, p0, LX/BIM;->k:Lcom/facebook/optic/CameraPreviewView;

    .line 1770265
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v4

    check-cast p1, LX/BIM;

    const-class v1, LX/9d6;

    invoke-interface {v4, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/9d6;

    invoke-static {v4}, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->b(LX/0QB;)Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;

    move-result-object v2

    check-cast v2, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;

    invoke-static {v4}, LX/8GN;->b(LX/0QB;)LX/8GN;

    move-result-object v3

    check-cast v3, LX/8GN;

    const-class p0, LX/9dO;

    invoke-interface {v4, p0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/9dO;

    iput-object v1, p1, LX/BIM;->d:LX/9d6;

    iput-object v2, p1, LX/BIM;->e:Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;

    iput-object v3, p1, LX/BIM;->f:LX/8GN;

    iput-object v4, p1, LX/BIM;->g:LX/9dO;

    return-void
.end method

.method private l()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1770232
    iget-object v0, p0, LX/BIM;->i:LX/9d5;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/BIM;->h:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;

    if-nez v0, :cond_1

    .line 1770233
    :cond_0
    :goto_0
    return-void

    .line 1770234
    :cond_1
    iget-object v0, p0, LX/BIM;->i:LX/9d5;

    iget-object v1, p0, LX/BIM;->h:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;->c()LX/0Px;

    move-result-object v1

    invoke-static {v1}, LX/8G8;->a(LX/0Px;)LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1, v2, v2}, LX/9d5;->a(LX/0Px;LX/8G6;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public getItemType()LX/BHH;
    .locals 1

    .prologue
    .line 1770261
    sget-object v0, LX/BHH;->LIVE_CAMERA:LX/BHH;

    return-object v0
.end method

.method public getLayoutResourceId()I
    .locals 1

    .prologue
    .line 1770266
    const v0, 0x7f030f59

    return v0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 1770260
    const/4 v0, 0x0

    return v0
.end method

.method public final onFinishInflate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x7777dad0

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1770257
    invoke-super {p0}, LX/BIH;->onFinishInflate()V

    .line 1770258
    invoke-direct {p0}, LX/BIM;->l()V

    .line 1770259
    const/16 v1, 0x2d

    const v2, -0x4fba767c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onMeasure(II)V
    .locals 7

    .prologue
    .line 1770246
    invoke-super {p0, p1, p2}, LX/BIH;->onMeasure(II)V

    .line 1770247
    const/4 v5, 0x1

    .line 1770248
    iget-boolean v0, p0, LX/BIM;->l:Z

    if-eqz v0, :cond_1

    .line 1770249
    :cond_0
    :goto_0
    return-void

    .line 1770250
    :cond_1
    const v0, 0x7f0d1252

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;

    .line 1770251
    new-instance v0, LX/BIK;

    invoke-direct {v0, p0}, LX/BIK;-><init>(LX/BIM;)V

    invoke-virtual {v6, v0}, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 1770252
    iget-object v0, p0, LX/BIM;->i:LX/9d5;

    if-nez v0, :cond_0

    invoke-virtual {p0}, LX/BIM;->getMeasuredHeight()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p0}, LX/BIM;->getMeasuredWidth()I

    move-result v0

    if-lez v0, :cond_0

    .line 1770253
    iget-object v0, p0, LX/BIM;->d:LX/9d6;

    new-instance v1, LX/9c0;

    invoke-direct {v1}, LX/9c0;-><init>()V

    iget-object v2, p0, LX/BIM;->e:Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;

    const/4 v3, 0x0

    const-string v4, "-1"

    invoke-virtual/range {v0 .. v5}, LX/9d6;->a(LX/9bz;Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;Landroid/net/Uri;Ljava/lang/String;Z)LX/9d5;

    move-result-object v0

    iput-object v0, p0, LX/BIM;->i:LX/9d5;

    .line 1770254
    iget-object v0, p0, LX/BIM;->i:LX/9d5;

    invoke-virtual {p0}, LX/BIM;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {p0}, LX/BIM;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {v0, v6, v1, v2, v5}, LX/9d5;->a(Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;IIZ)V

    .line 1770255
    iget-object v0, p0, LX/BIM;->i:LX/9d5;

    invoke-virtual {v0}, LX/9d5;->c()V

    .line 1770256
    iget-object v0, p0, LX/BIM;->i:LX/9d5;

    new-instance v1, LX/BIL;

    invoke-direct {v1, p0}, LX/BIL;-><init>(LX/BIM;)V

    invoke-virtual {v0, v1}, LX/9d5;->a(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public setFramePack(Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;)V
    .locals 6
    .param p1    # Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x1

    .line 1770235
    iput-object p1, p0, LX/BIM;->h:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;

    .line 1770236
    iget-object v0, p0, LX/BIM;->i:LX/9d5;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/BIM;->h:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;

    if-nez v0, :cond_1

    .line 1770237
    :cond_0
    :goto_0
    return-void

    .line 1770238
    :cond_1
    iget-object v0, p0, LX/BIM;->i:LX/9d5;

    iget-object v1, p0, LX/BIM;->g:LX/9dO;

    iget-object v2, p0, LX/BIM;->f:LX/8GN;

    iget-object v3, p0, LX/BIM;->h:Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;

    invoke-virtual {v2, v3, v4, v4}, LX/8GN;->a(Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;II)LX/0Px;

    move-result-object v2

    .line 1770239
    new-instance v4, LX/9dN;

    .line 1770240
    new-instance p1, LX/9dL;

    const-class v3, Landroid/content/Context;

    invoke-interface {v1, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v1}, LX/8Gc;->b(LX/0QB;)LX/8Gc;

    move-result-object v5

    check-cast v5, LX/8Gc;

    invoke-direct {p1, v3, v5}, LX/9dL;-><init>(Landroid/content/Context;LX/8Gc;)V

    .line 1770241
    move-object v3, p1

    .line 1770242
    check-cast v3, LX/9dL;

    invoke-direct {v4, v2, v3}, LX/9dN;-><init>(LX/0Px;LX/9dL;)V

    .line 1770243
    move-object v1, v4

    .line 1770244
    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/9d5;->a(LX/0Px;)V

    .line 1770245
    invoke-direct {p0}, LX/BIM;->l()V

    goto :goto_0
.end method
