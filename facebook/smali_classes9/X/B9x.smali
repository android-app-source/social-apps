.class public final LX/B9x;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/FYC;

.field public final synthetic b:Landroid/content/Context;

.field public final synthetic c:Z

.field public final synthetic d:Ljava/lang/String;

.field public final synthetic e:LX/B9y;


# direct methods
.method public constructor <init>(LX/B9y;LX/FYC;Landroid/content/Context;ZLjava/lang/String;)V
    .locals 0

    .prologue
    .line 1752375
    iput-object p1, p0, LX/B9x;->e:LX/B9y;

    iput-object p2, p0, LX/B9x;->a:LX/FYC;

    iput-object p3, p0, LX/B9x;->b:Landroid/content/Context;

    iput-boolean p4, p0, LX/B9x;->c:Z

    iput-object p5, p0, LX/B9x;->d:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1752373
    iget-object v0, p0, LX/B9x;->a:LX/FYC;

    invoke-virtual {v0}, LX/FYC;->a()V

    .line 1752374
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1752364
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1752365
    if-eqz p1, :cond_0

    .line 1752366
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1752367
    if-nez v0, :cond_1

    .line 1752368
    :cond_0
    iget-object v0, p0, LX/B9x;->a:LX/FYC;

    invoke-virtual {v0}, LX/FYC;->a()V

    .line 1752369
    :goto_0
    return-void

    .line 1752370
    :cond_1
    iget-object v1, p0, LX/B9x;->e:LX/B9y;

    .line 1752371
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1752372
    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;

    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    iget-object v2, p0, LX/B9x;->b:Landroid/content/Context;

    iget-boolean v3, p0, LX/B9x;->c:Z

    iget-object v4, p0, LX/B9x;->d:Ljava/lang/String;

    invoke-virtual {v1, v0, v2, v3, v4}, LX/B9y;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/content/Context;ZLjava/lang/String;)V

    goto :goto_0
.end method
