.class public final LX/CWd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/CSY;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/CTN;

.field public final synthetic d:I

.field public final synthetic e:LX/CT7;

.field public final synthetic f:Ljava/util/Calendar;

.field public final synthetic g:I

.field public final synthetic h:LX/CT6;

.field public final synthetic i:LX/CT1;

.field public final synthetic j:Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDatepickerView;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDatepickerView;LX/CSY;Ljava/lang/String;LX/CTN;ILX/CT7;Ljava/util/Calendar;ILX/CT6;LX/CT1;)V
    .locals 0

    .prologue
    .line 1908489
    iput-object p1, p0, LX/CWd;->j:Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDatepickerView;

    iput-object p2, p0, LX/CWd;->a:LX/CSY;

    iput-object p3, p0, LX/CWd;->b:Ljava/lang/String;

    iput-object p4, p0, LX/CWd;->c:LX/CTN;

    iput p5, p0, LX/CWd;->d:I

    iput-object p6, p0, LX/CWd;->e:LX/CT7;

    iput-object p7, p0, LX/CWd;->f:Ljava/util/Calendar;

    iput p8, p0, LX/CWd;->g:I

    iput-object p9, p0, LX/CWd;->h:LX/CT6;

    iput-object p10, p0, LX/CWd;->i:LX/CT1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 12

    .prologue
    const/4 v11, 0x2

    const/4 v10, 0x1

    const v0, -0x50b3db08

    invoke-static {v11, v10, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 1908475
    iget-object v0, p0, LX/CWd;->a:LX/CSY;

    iget-object v1, p0, LX/CWd;->b:Ljava/lang/String;

    iget-object v3, p0, LX/CWd;->c:LX/CTN;

    iget-wide v4, v3, LX/CTN;->a:J

    iget v3, p0, LX/CWd;->d:I

    int-to-long v6, v3

    const-wide/32 v8, 0x15180

    mul-long/2addr v6, v8

    add-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, LX/CSY;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1908476
    iget-object v0, p0, LX/CWd;->e:LX/CT7;

    iget-object v1, p0, LX/CWd;->c:LX/CTN;

    iget-object v1, v1, LX/CTJ;->o:Ljava/lang/String;

    iget-object v3, p0, LX/CWd;->c:LX/CTN;

    iget-object v3, v3, LX/CTK;->e:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    iget-object v4, p0, LX/CWd;->a:LX/CSY;

    invoke-virtual {v0, v1, v3, v4}, LX/CT7;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;LX/CSY;)V

    .line 1908477
    iget-object v0, p0, LX/CWd;->f:Ljava/util/Calendar;

    new-instance v1, Ljava/util/Date;

    iget-object v3, p0, LX/CWd;->c:LX/CTN;

    iget-wide v4, v3, LX/CTN;->a:J

    iget v3, p0, LX/CWd;->g:I

    int-to-long v6, v3

    sub-long/2addr v4, v6

    const-wide/16 v6, 0x3e8

    mul-long/2addr v4, v6

    invoke-direct {v1, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 1908478
    iget-object v0, p0, LX/CWd;->f:Ljava/util/Calendar;

    const/4 v1, 0x5

    iget v3, p0, LX/CWd;->d:I

    invoke-virtual {v0, v1, v3}, Ljava/util/Calendar;->add(II)V

    .line 1908479
    iget-object v0, p0, LX/CWd;->j:Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDatepickerView;

    iget-object v0, v0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDatepickerView;->d:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, p0, LX/CWd;->j:Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDatepickerView;

    iget-object v1, v1, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDatepickerView;->e:Ljava/text/SimpleDateFormat;

    iget-object v3, p0, LX/CWd;->f:Ljava/util/Calendar;

    invoke-virtual {v3}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1908480
    iget-object v0, p0, LX/CWd;->j:Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDatepickerView;

    iget-object v0, v0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDatepickerView;->c:Landroid/widget/LinearLayout;

    iget-object v1, p0, LX/CWd;->j:Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDatepickerView;

    iget v1, v1, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDatepickerView;->f:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldDatepickerDayView;

    .line 1908481
    iget-object v1, p0, LX/CWd;->j:Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDatepickerView;

    iget-object v1, v1, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDatepickerView;->c:Landroid/widget/LinearLayout;

    iget v3, p0, LX/CWd;->d:I

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldDatepickerDayView;

    .line 1908482
    const/4 v3, 0x0

    invoke-virtual {v0, v3, v10}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldDatepickerDayView;->a(ZZ)V

    .line 1908483
    invoke-virtual {v1, v10, v10}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldDatepickerDayView;->a(ZZ)V

    .line 1908484
    iget-object v0, p0, LX/CWd;->j:Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDatepickerView;

    iget v1, p0, LX/CWd;->d:I

    .line 1908485
    iput v1, v0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDatepickerView;->f:I

    .line 1908486
    iget-object v0, p0, LX/CWd;->c:LX/CTN;

    iget-object v3, p0, LX/CWd;->i:LX/CT1;

    .line 1908487
    invoke-static {v0, v3}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDatepickerView;->a(LX/CTN;LX/CT1;)V

    .line 1908488
    const v0, -0x2ad832fd

    invoke-static {v11, v11, v0, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
