.class public final LX/AsE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field public final synthetic a:LX/AsI;


# direct methods
.method public constructor <init>(LX/AsI;)V
    .locals 0

    .prologue
    .line 1720240
    iput-object p1, p0, LX/AsE;->a:LX/AsI;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onGlobalLayout()V
    .locals 5

    .prologue
    .line 1720241
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 1720242
    iget-object v1, p0, LX/AsE;->a:LX/AsI;

    iget-object v1, v1, LX/AsI;->j:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 1720243
    new-instance v1, Landroid/util/DisplayMetrics;

    invoke-direct {v1}, Landroid/util/DisplayMetrics;-><init>()V

    .line 1720244
    iget-object v2, p0, LX/AsE;->a:LX/AsI;

    iget-object v2, v2, LX/AsI;->d:Landroid/view/WindowManager;

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 1720245
    iget v1, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    sub-int v0, v1, v0

    .line 1720246
    iget-object v1, p0, LX/AsE;->a:LX/AsI;

    const/4 v4, 0x0

    .line 1720247
    iget-object v2, v1, LX/AsI;->j:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout$LayoutParams;

    .line 1720248
    iget v3, v2, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    if-ne v0, v3, :cond_1

    .line 1720249
    :goto_0
    iget-object v1, p0, LX/AsE;->a:LX/AsI;

    invoke-static {v1}, LX/AsI;->e(LX/AsI;)LX/870;

    move-result-object v1

    sget-object v2, LX/870;->TEXT_EDITING:LX/870;

    if-ne v1, v2, :cond_0

    iget-object v1, p0, LX/AsE;->a:LX/AsI;

    .line 1720250
    iget-object v2, v1, LX/AsI;->e:LX/0w3;

    .line 1720251
    iget v1, v2, LX/0w3;->e:I

    move v2, v1

    .line 1720252
    if-ne v0, v2, :cond_2

    const/4 v2, 0x1

    :goto_1
    move v0, v2

    .line 1720253
    if-eqz v0, :cond_0

    .line 1720254
    iget-object v0, p0, LX/AsE;->a:LX/AsI;

    iget-object v0, v0, LX/AsI;->k:Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 1720255
    iget-object v0, p0, LX/AsE;->a:LX/AsI;

    iget-object v0, v0, LX/AsI;->b:LX/Ase;

    invoke-virtual {v0}, LX/Ase;->b()V

    .line 1720256
    iget-object v0, p0, LX/AsE;->a:LX/AsI;

    const/4 v1, 0x1

    .line 1720257
    iput-boolean v1, v0, LX/AsI;->i:Z

    .line 1720258
    :cond_0
    return-void

    .line 1720259
    :cond_1
    invoke-virtual {v2, v4, v4, v4, v0}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 1720260
    iget-object v3, v1, LX/AsI;->j:Landroid/view/View;

    invoke-virtual {v3, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    goto :goto_1
.end method
