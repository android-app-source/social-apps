.class public final LX/C0x;
.super Landroid/os/Handler;
.source ""


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/C0w;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/C0w;)V
    .locals 1

    .prologue
    .line 1841715
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 1841716
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/C0x;->a:Ljava/lang/ref/WeakReference;

    .line 1841717
    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 7

    .prologue
    .line 1841671
    iget-object v0, p0, LX/C0x;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/C0w;

    .line 1841672
    if-nez v0, :cond_0

    .line 1841673
    :goto_0
    return-void

    .line 1841674
    :cond_0
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 1841675
    :pswitch_0
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/facebook/components/feed/FeedComponentView;

    .line 1841676
    iget v2, p1, Landroid/os/Message;->arg1:I

    const/4 v6, 0x0

    .line 1841677
    iget v3, v0, LX/C0w;->h:I

    if-ne v2, v3, :cond_1

    iget-object v3, v0, LX/C0w;->g:LX/0hs;

    if-nez v3, :cond_4

    iget-object v3, v0, LX/C0w;->a:LX/1w9;

    invoke-virtual {v3}, LX/1w9;->a()Z

    move-result v3

    if-nez v3, :cond_4

    .line 1841678
    :cond_1
    :goto_1
    goto :goto_0

    .line 1841679
    :pswitch_1
    iget-object v1, v0, LX/C0w;->g:LX/0hs;

    if-eqz v1, :cond_2

    .line 1841680
    iget-object v1, v0, LX/C0w;->g:LX/0hs;

    invoke-virtual {v1}, LX/0ht;->l()V

    .line 1841681
    const/4 v1, 0x0

    iput-object v1, v0, LX/C0w;->g:LX/0hs;

    .line 1841682
    :cond_2
    goto :goto_0

    .line 1841683
    :pswitch_2
    iget-object v1, v0, LX/C0w;->g:LX/0hs;

    if-eqz v1, :cond_3

    iget-object v1, v0, LX/C0w;->g:LX/0hs;

    .line 1841684
    iget-boolean v2, v1, LX/0ht;->r:Z

    move v1, v2

    .line 1841685
    if-eqz v1, :cond_3

    iget-object v1, v0, LX/C0w;->a:LX/1w9;

    invoke-virtual {v1}, LX/1w9;->a()Z

    move-result v1

    if-nez v1, :cond_d

    .line 1841686
    :cond_3
    :goto_2
    goto :goto_0

    .line 1841687
    :cond_4
    iget-object v3, v0, LX/C0w;->g:LX/0hs;

    if-nez v3, :cond_8

    iget v3, v0, LX/C0w;->i:I

    if-lez v3, :cond_8

    iget v3, v0, LX/C0w;->f:I

    invoke-static {v0, v1, v3}, LX/C0w;->a(LX/C0w;Landroid/view/View;I)Z

    move-result v3

    if-eqz v3, :cond_8

    const/4 v3, 0x1

    :goto_3
    move v3, v3

    .line 1841688
    if-eqz v3, :cond_6

    .line 1841689
    iget v3, v0, LX/C0w;->e:I

    iput v3, v0, LX/C0w;->i:I

    .line 1841690
    :cond_5
    iget-object v3, v0, LX/C0w;->c:LX/C0x;

    iget-object v4, v0, LX/C0w;->c:LX/C0x;

    iget v5, v0, LX/C0w;->h:I

    add-int/lit8 v5, v5, 0x1

    iput v5, v0, LX/C0w;->h:I

    invoke-virtual {v4, v6, v5, v6, v1}, LX/C0x;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v4

    iget v5, v0, LX/C0w;->d:I

    int-to-long v5, v5

    invoke-virtual {v3, v4, v5, v6}, LX/C0x;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_1

    .line 1841691
    :cond_6
    iget v3, v0, LX/C0w;->i:I

    iget v4, v0, LX/C0w;->d:I

    sub-int/2addr v3, v4

    iput v3, v0, LX/C0w;->i:I

    .line 1841692
    iget-object v3, v0, LX/C0w;->g:LX/0hs;

    if-nez v3, :cond_9

    iget v3, v0, LX/C0w;->i:I

    if-gtz v3, :cond_9

    .line 1841693
    invoke-static {v0, v1}, LX/C0w;->d(LX/C0w;Lcom/facebook/components/feed/FeedComponentView;)V

    .line 1841694
    const/4 v3, 0x1

    .line 1841695
    :goto_4
    move v3, v3

    .line 1841696
    if-eqz v3, :cond_7

    iget-object v4, v0, LX/C0w;->g:LX/0hs;

    if-eqz v4, :cond_1

    .line 1841697
    :cond_7
    if-nez v3, :cond_5

    .line 1841698
    iget-object v3, v0, LX/C0w;->g:LX/0hs;

    if-nez v3, :cond_a

    .line 1841699
    const/4 v3, 0x0

    .line 1841700
    :goto_5
    move v3, v3

    .line 1841701
    if-eqz v3, :cond_5

    iget-object v3, v0, LX/C0w;->g:LX/0hs;

    if-nez v3, :cond_5

    goto :goto_1

    :cond_8
    const/4 v3, 0x0

    goto :goto_3

    :cond_9
    const/4 v3, 0x0

    goto :goto_4

    .line 1841702
    :cond_a
    iget-object v3, v0, LX/C0w;->g:LX/0hs;

    .line 1841703
    iget-boolean v4, v3, LX/0ht;->r:Z

    move v3, v4

    .line 1841704
    if-nez v3, :cond_c

    .line 1841705
    const/4 v3, 0x0

    iput-object v3, v0, LX/C0w;->g:LX/0hs;

    .line 1841706
    :cond_b
    :goto_6
    const/4 v3, 0x1

    goto :goto_5

    .line 1841707
    :cond_c
    iget v3, v0, LX/C0w;->f:I

    invoke-static {v0, v1, v3}, LX/C0w;->a(LX/C0w;Landroid/view/View;I)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 1841708
    iget-object v3, v0, LX/C0w;->g:LX/0hs;

    invoke-virtual {v1, v3}, Lcom/facebook/components/feed/FeedComponentView;->a(LX/0hs;)V

    goto :goto_6

    .line 1841709
    :cond_d
    iget-object v1, v0, LX/C0w;->a:LX/1w9;

    .line 1841710
    const/4 v2, 0x1

    iput-boolean v2, v1, LX/1w9;->g:Z

    .line 1841711
    iget-object v2, v1, LX/1w9;->b:LX/1xK;

    .line 1841712
    iget-object v0, v2, LX/1xK;->b:LX/0iA;

    invoke-virtual {v0}, LX/0iA;->a()Lcom/facebook/interstitial/manager/InterstitialLogger;

    move-result-object v0

    const-string v1, "4509"

    invoke-virtual {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialLogger;->a(Ljava/lang/String;)V

    .line 1841713
    sget-object v0, LX/10S;->INELIGIBLE:LX/10S;

    iput-object v0, v2, LX/1xK;->c:LX/10S;

    .line 1841714
    goto/16 :goto_2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
