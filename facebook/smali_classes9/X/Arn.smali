.class public final LX/Arn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6JG;


# instance fields
.field public final synthetic a:Ljava/io/File;

.field public final synthetic b:LX/Arq;


# direct methods
.method public constructor <init>(LX/Arq;Ljava/io/File;)V
    .locals 0

    .prologue
    .line 1719706
    iput-object p1, p0, LX/Arn;->b:LX/Arq;

    iput-object p2, p0, LX/Arn;->a:Ljava/io/File;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 1719697
    iget-object v0, p0, LX/Arn;->b:LX/Arq;

    iget-object v0, v0, LX/Arq;->e:LX/Arh;

    .line 1719698
    iget-object v1, v0, LX/Arh;->q:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v1

    iput-wide v1, v0, LX/Arh;->z:J

    .line 1719699
    iget-object v1, v0, LX/Arh;->f:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0il;

    .line 1719700
    invoke-interface {v1}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    .line 1719701
    invoke-virtual {v1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->j()Lcom/facebook/friendsharing/inspiration/model/CameraState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/friendsharing/inspiration/model/CameraState;->getCaptureState()LX/86q;

    move-result-object v2

    sget-object v3, LX/86q;->START_RECORD_VIDEO_REQUESTED:LX/86q;

    if-eq v2, v3, :cond_0

    .line 1719702
    :goto_0
    return-void

    .line 1719703
    :cond_0
    check-cast v1, LX/0io;

    invoke-static {v1}, LX/2rf;->b(LX/0io;)Lcom/facebook/composer/attachments/ComposerAttachment;

    move-result-object v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_1
    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 1719704
    invoke-static {v0}, LX/Arh;->j(LX/Arh;)LX/0jL;

    move-result-object v1

    sget-object v2, LX/86q;->RECORDING_VIDEO:LX/86q;

    invoke-static {v0, v1, v2}, LX/Arh;->a(LX/Arh;LX/0jL;LX/86q;)LX/0jL;

    move-result-object v1

    invoke-virtual {v1}, LX/0jL;->a()V

    goto :goto_0

    .line 1719705
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public final a(LX/6JJ;)V
    .locals 2

    .prologue
    .line 1719693
    iget-object v0, p0, LX/Arn;->b:LX/Arq;

    const-string v1, "pr_camera_take_video"

    invoke-static {v0, v1, p1}, LX/Arq;->a$redex0(LX/Arq;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1719694
    iget-object v0, p0, LX/Arn;->b:LX/Arq;

    iget-object v0, v0, LX/Arq;->e:LX/Arh;

    invoke-virtual {v0}, LX/Arh;->h()V

    .line 1719695
    iget-object v0, p0, LX/Arn;->b:LX/Arq;

    iget-object v1, p0, LX/Arn;->a:Ljava/io/File;

    invoke-static {v0, v1}, LX/Arq;->a$redex0(LX/Arq;Ljava/io/File;)V

    .line 1719696
    return-void
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 1719689
    iget-object v0, p0, LX/Arn;->b:LX/Arq;

    iget-object v1, p0, LX/Arn;->a:Ljava/io/File;

    invoke-static {v0, v1}, LX/Arq;->a$redex0(LX/Arq;Ljava/io/File;)V

    .line 1719690
    iget-object v0, p0, LX/Arn;->b:LX/Arq;

    iget-object v1, p0, LX/Arn;->a:Ljava/io/File;

    .line 1719691
    iget-object v2, v0, LX/Arq;->i:Ljava/util/concurrent/ExecutorService;

    new-instance v3, Lcom/facebook/friendsharing/inspiration/capture/InspirationCaptureHelper$2;

    invoke-direct {v3, v0, v1}, Lcom/facebook/friendsharing/inspiration/capture/InspirationCaptureHelper$2;-><init>(LX/Arq;Ljava/io/File;)V

    const p0, -0x38340966

    invoke-static {v2, v3, p0}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1719692
    return-void
.end method
