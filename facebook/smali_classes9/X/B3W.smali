.class public LX/B3W;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/B3W;


# instance fields
.field private final a:LX/0sa;


# direct methods
.method public constructor <init>(LX/0sa;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1740423
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1740424
    iput-object p1, p0, LX/B3W;->a:LX/0sa;

    .line 1740425
    return-void
.end method

.method public static a(LX/0QB;)LX/B3W;
    .locals 4

    .prologue
    .line 1740426
    sget-object v0, LX/B3W;->b:LX/B3W;

    if-nez v0, :cond_1

    .line 1740427
    const-class v1, LX/B3W;

    monitor-enter v1

    .line 1740428
    :try_start_0
    sget-object v0, LX/B3W;->b:LX/B3W;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1740429
    if-eqz v2, :cond_0

    .line 1740430
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1740431
    new-instance p0, LX/B3W;

    invoke-static {v0}, LX/0sa;->a(LX/0QB;)LX/0sa;

    move-result-object v3

    check-cast v3, LX/0sa;

    invoke-direct {p0, v3}, LX/B3W;-><init>(LX/0sa;)V

    .line 1740432
    move-object v0, p0

    .line 1740433
    sput-object v0, LX/B3W;->b:LX/B3W;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1740434
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1740435
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1740436
    :cond_1
    sget-object v0, LX/B3W;->b:LX/B3W;

    return-object v0

    .line 1740437
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1740438
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/Integer;)LX/B4o;
    .locals 2

    .prologue
    .line 1740421
    new-instance v0, LX/B4o;

    invoke-direct {v0}, LX/B4o;-><init>()V

    move-object v0, v0

    .line 1740422
    const-string v1, "profile_id"

    invoke-virtual {v0, v1, p0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "image_size"

    invoke-virtual {v0, v1, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/B4o;

    return-object v0
.end method

.method public static final b(Ljava/lang/String;)LX/B4o;
    .locals 1

    .prologue
    .line 1740419
    invoke-static {}, LX/0wB;->d()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    move-object v0, v0

    .line 1740420
    invoke-static {p0, v0}, LX/B3W;->a(Ljava/lang/String;Ljava/lang/Integer;)LX/B4o;

    move-result-object v0

    return-object v0
.end method
