.class public LX/Ari;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/Arh;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1719560
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 1719561
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;Ljava/lang/Object;Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;LX/Aqx;LX/ArL;LX/ArT;)LX/Arh;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ModelData:",
            "Ljava/lang/Object;",
            ":",
            "Lcom/facebook/friendsharing/inspiration/model/CameraStateSpec$ProvidesCameraState;",
            ":",
            "LX/0io;",
            ":",
            "LX/0iq;",
            ":",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingDataSpec$ProvidesInspirationLoggingData;",
            ":",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationStateSpec$ProvidesInspirationState;",
            ":",
            "LX/0is;",
            "DerivedData:",
            "Ljava/lang/Object;",
            "Mutation:",
            "Ljava/lang/Object;",
            ":",
            "Lcom/facebook/ipc/composer/dataaccessor/ComposerCanSave;",
            ":",
            "Lcom/facebook/composer/attachments/ComposerAttachment$SetsAttachments",
            "<TMutation;>;:",
            "Lcom/facebook/composer/privacy/model/ComposerPrivacyData$SetsPrivacyData",
            "<TMutation;>;:",
            "Lcom/facebook/friendsharing/inspiration/model/CameraStateSpec$SetsCameraState",
            "<TMutation;>;:",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingDataSpec$SetsInspirationLoggingData",
            "<TMutation;>;:",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBoundsSpec$SetsInspirationPreviewBounds",
            "<TMutation;>;Services:",
            "Ljava/lang/Object;",
            ":",
            "LX/0il",
            "<TModelData;>;:",
            "LX/0ik",
            "<TDerivedData;>;:",
            "LX/0im",
            "<TMutation;>;>(",
            "Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;",
            "TServices;",
            "Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;",
            "Lcom/facebook/friendsharing/inspiration/capture/InspirationCameraPreviewController$Delegate;",
            "LX/ArL;",
            "LX/ArT;",
            ")",
            "LX/Arh",
            "<TModelData;TDerivedData;TMutation;TServices;>;"
        }
    .end annotation

    .prologue
    .line 1719562
    new-instance v0, LX/Arh;

    move-object/from16 v2, p2

    check-cast v2, LX/0il;

    const-class v1, LX/Arr;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/Arr;

    const-class v1, LX/Aru;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/Aru;

    invoke-static {p0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v9

    check-cast v9, Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-class v1, LX/193;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v10

    check-cast v10, LX/193;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v11

    check-cast v11, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, Lcom/facebook/friendsharing/inspiration/capture/cache/InspirationPhotoCaptureCache;->a(LX/0QB;)Lcom/facebook/friendsharing/inspiration/capture/cache/InspirationPhotoCaptureCache;

    move-result-object v12

    check-cast v12, Lcom/facebook/friendsharing/inspiration/capture/cache/InspirationPhotoCaptureCache;

    invoke-static {p0}, LX/74n;->b(LX/0QB;)LX/74n;

    move-result-object v13

    check-cast v13, LX/74n;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v14

    check-cast v14, LX/0SG;

    move-object/from16 v1, p1

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    invoke-direct/range {v0 .. v14}, LX/Arh;-><init>(Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;LX/0il;Lcom/facebook/cameracore/ui/components/CameraCorePreviewView;LX/Aqx;LX/ArL;LX/ArT;LX/Arr;LX/Aru;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/193;Lcom/facebook/prefs/shared/FbSharedPreferences;Lcom/facebook/friendsharing/inspiration/capture/cache/InspirationPhotoCaptureCache;LX/74n;LX/0SG;)V

    .line 1719563
    const/16 v1, 0x12c4

    invoke-static {p0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    const/16 v2, 0xa8a

    invoke-static {p0, v2}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    const/16 v3, 0x6a1

    invoke-static {p0, v3}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, LX/Arh;->a(LX/Arh;LX/0Or;LX/0Or;LX/0Or;)V

    .line 1719564
    return-object v0
.end method
