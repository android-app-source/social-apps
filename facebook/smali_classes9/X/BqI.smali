.class public LX/BqI;
.super LX/2oV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2oV",
        "<",
        "LX/3JH;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Z

.field public b:LX/BqH;

.field public c:LX/1Bv;


# direct methods
.method public constructor <init>(Ljava/lang/String;LX/BqH;LX/1Bv;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/BqH;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1824488
    invoke-direct {p0, p1}, LX/2oV;-><init>(Ljava/lang/String;)V

    .line 1824489
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/BqI;->a:Z

    .line 1824490
    iput-object p2, p0, LX/BqI;->b:LX/BqH;

    .line 1824491
    iput-object p3, p0, LX/BqI;->c:LX/1Bv;

    .line 1824492
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1824493
    check-cast p1, LX/3JH;

    .line 1824494
    iget-boolean v0, p0, LX/BqI;->a:Z

    if-nez v0, :cond_0

    .line 1824495
    :goto_0
    return-void

    .line 1824496
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/BqI;->a:Z

    .line 1824497
    iget-object v0, p0, LX/BqI;->b:LX/BqH;

    invoke-virtual {p1}, LX/3JH;->getSeekPosition()I

    move-result v1

    invoke-virtual {v0, v1}, LX/BqH;->a(I)V

    .line 1824498
    iget-object v0, p1, LX/3JH;->a:Lcom/facebook/video/player/InlineVideoPlayer2;

    move-object v0, v0

    .line 1824499
    invoke-interface {v0}, LX/7Kf;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1824500
    iget-object v1, p0, LX/BqI;->b:LX/BqH;

    invoke-interface {v0}, LX/7Kf;->getCurrentPosition()I

    move-result v2

    invoke-virtual {v1, v2}, LX/BqH;->a(I)V

    .line 1824501
    sget-object v1, LX/04g;->BY_AUTOPLAY:LX/04g;

    invoke-interface {v0, v1}, LX/7Kf;->a(LX/04g;)V

    .line 1824502
    :cond_1
    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 1824503
    check-cast p1, LX/3JH;

    .line 1824504
    iget-boolean v0, p0, LX/BqI;->a:Z

    if-eqz v0, :cond_1

    .line 1824505
    :cond_0
    :goto_0
    return-void

    .line 1824506
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/BqI;->a:Z

    .line 1824507
    iget-object v0, p0, LX/BqI;->b:LX/BqH;

    .line 1824508
    iget-boolean v1, v0, LX/BqH;->b:Z

    move v0, v1

    .line 1824509
    if-nez v0, :cond_2

    iget-object v0, p0, LX/BqI;->c:LX/1Bv;

    invoke-virtual {v0}, LX/1Bv;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 1824510
    if-eqz v0, :cond_0

    .line 1824511
    iget-object v0, p0, LX/BqI;->b:LX/BqH;

    invoke-virtual {v0}, LX/BqH;->a()I

    move-result v0

    .line 1824512
    iget-object v1, p1, LX/3JH;->a:Lcom/facebook/video/player/InlineVideoPlayer2;

    move-object v1, v1

    .line 1824513
    sget-object v2, LX/04g;->BY_AUTOPLAY:LX/04g;

    new-instance v3, LX/7K4;

    invoke-direct {v3, v0, v0}, LX/7K4;-><init>(II)V

    .line 1824514
    iget-boolean p0, v1, Lcom/facebook/video/player/InlineVideoPlayer2;->j:Z

    if-nez p0, :cond_3

    .line 1824515
    :goto_2
    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 1824516
    :cond_3
    iget-object p1, v1, Lcom/facebook/video/player/InlineVideoPlayer2;->a:Lcom/facebook/video/player/RichVideoPlayer;

    iget-boolean p0, v1, Lcom/facebook/video/player/InlineVideoPlayer2;->f:Z

    if-nez p0, :cond_5

    const/4 p0, 0x1

    :goto_3
    sget-object v0, LX/04g;->BY_PLAYER:LX/04g;

    invoke-virtual {p1, p0, v0}, Lcom/facebook/video/player/RichVideoPlayer;->a(ZLX/04g;)V

    .line 1824517
    sget-object p0, LX/04g;->BY_INLINE_FULLSCREEN_TRANSITION:LX/04g;

    if-eq v2, p0, :cond_4

    .line 1824518
    iget-object p0, v1, Lcom/facebook/video/player/InlineVideoPlayer2;->a:Lcom/facebook/video/player/RichVideoPlayer;

    iget p1, v3, LX/7K4;->c:I

    invoke-virtual {p0, p1, v2}, Lcom/facebook/video/player/RichVideoPlayer;->a(ILX/04g;)V

    .line 1824519
    :cond_4
    iget-object p0, v1, Lcom/facebook/video/player/InlineVideoPlayer2;->a:Lcom/facebook/video/player/RichVideoPlayer;

    iget p1, v3, LX/7K4;->d:I

    invoke-virtual {p0, v2, p1}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/04g;I)V

    goto :goto_2

    .line 1824520
    :cond_5
    const/4 p0, 0x0

    goto :goto_3
.end method
