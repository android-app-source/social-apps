.class public final LX/BtD;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/1W6;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/1Pn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public c:Z

.field public final synthetic d:LX/1W6;


# direct methods
.method public constructor <init>(LX/1W6;)V
    .locals 1

    .prologue
    .line 1828722
    iput-object p1, p0, LX/BtD;->d:LX/1W6;

    .line 1828723
    move-object v0, p1

    .line 1828724
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1828725
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/BtD;->c:Z

    .line 1828726
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1828727
    const-string v0, "StoryRichTextComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1828728
    if-ne p0, p1, :cond_1

    .line 1828729
    :cond_0
    :goto_0
    return v0

    .line 1828730
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1828731
    goto :goto_0

    .line 1828732
    :cond_3
    check-cast p1, LX/BtD;

    .line 1828733
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1828734
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1828735
    if-eq v2, v3, :cond_0

    .line 1828736
    iget-object v2, p0, LX/BtD;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/BtD;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/BtD;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1828737
    goto :goto_0

    .line 1828738
    :cond_5
    iget-object v2, p1, LX/BtD;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 1828739
    :cond_6
    iget-object v2, p0, LX/BtD;->b:LX/1Pn;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/BtD;->b:LX/1Pn;

    iget-object v3, p1, LX/BtD;->b:LX/1Pn;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1828740
    goto :goto_0

    .line 1828741
    :cond_8
    iget-object v2, p1, LX/BtD;->b:LX/1Pn;

    if-nez v2, :cond_7

    .line 1828742
    :cond_9
    iget-boolean v2, p0, LX/BtD;->c:Z

    iget-boolean v3, p1, LX/BtD;->c:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 1828743
    goto :goto_0
.end method
