.class public final LX/BbJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic b:Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 0

    .prologue
    .line 1800548
    iput-object p1, p0, LX/BbJ;->b:Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;

    iput-object p2, p0, LX/BbJ;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, 0x5e551ca1

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1800549
    iget-object v0, p0, LX/BbJ;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;->l(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/BbJ;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1800550
    iget-object v3, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v3

    .line 1800551
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->qd()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1800552
    :cond_0
    const v0, -0x38e6f442

    invoke-static {v2, v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1800553
    :goto_0
    return-void

    .line 1800554
    :cond_1
    iget-object v0, p0, LX/BbJ;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1800555
    iget-object v2, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v2

    .line 1800556
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v2

    .line 1800557
    iget-object v0, p0, LX/BbJ;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1800558
    iget-object v3, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v3

    .line 1800559
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->qd()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v0

    .line 1800560
    iget-object v3, p0, LX/BbJ;->b:Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;

    iget-object v3, v3, Lcom/facebook/checkin/socialsearch/feed/SocialSearchAttachmentDescriptionPartDefinition;->i:LX/8yM;

    new-instance v4, LX/BbI;

    invoke-direct {v4, p0}, LX/BbI;-><init>(LX/BbJ;)V

    invoke-virtual {v3, v2, v0, v4}, LX/8yM;->a(Ljava/lang/String;Ljava/lang/String;LX/0TF;)V

    .line 1800561
    const v0, 0x650f372e

    invoke-static {v0, v1}, LX/02F;->a(II)V

    goto :goto_0
.end method
