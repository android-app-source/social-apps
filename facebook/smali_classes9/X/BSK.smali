.class public LX/BSK;
.super Landroid/app/Dialog;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public a:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/BSB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1785365
    const v0, 0x7f0e09c2

    invoke-direct {p0, p1, v0}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 1785366
    const-class v0, LX/BSK;

    invoke-static {v0, p0}, LX/BSK;->a(Ljava/lang/Class;Landroid/app/Dialog;)V

    .line 1785367
    const v0, 0x7f030168

    invoke-virtual {p0, v0}, LX/BSK;->setContentView(I)V

    .line 1785368
    const v0, 0x1020002

    invoke-virtual {p0, v0}, LX/BSK;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1785369
    const v0, 0x7f0d0553

    invoke-virtual {p0, v0}, LX/BSK;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, LX/BSH;

    invoke-direct {v1, p0}, LX/BSH;-><init>(LX/BSK;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1785370
    const v0, 0x7f0d0677

    invoke-virtual {p0, v0}, LX/BSK;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1785371
    const v0, 0x7f0d0678

    invoke-virtual {p0, v0}, LX/BSK;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, LX/BSI;

    invoke-direct {v1, p0}, LX/BSI;-><init>(LX/BSK;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1785372
    return-void
.end method

.method public static a(LX/BSK;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1785363
    iget-object v0, p0, LX/BSK;->b:LX/BSB;

    const-string v1, "NuxSettingsDialog"

    invoke-virtual {v0, p1, p2, v1}, LX/BSB;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1785364
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/app/Dialog;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/app/Dialog;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/BSK;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v1

    check-cast v1, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/BSB;->b(LX/0QB;)LX/BSB;

    move-result-object p0

    check-cast p0, LX/BSB;

    iput-object v1, p1, LX/BSK;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object p0, p1, LX/BSK;->b:LX/BSB;

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, 0x48dff255

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1785360
    const-string v1, "close"

    const/4 v2, 0x0

    invoke-static {p0, v1, v2}, LX/BSK;->a(LX/BSK;Ljava/lang/String;Ljava/lang/String;)V

    .line 1785361
    invoke-virtual {p0}, LX/BSK;->dismiss()V

    .line 1785362
    const v1, 0x572f87a9

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
