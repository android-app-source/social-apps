.class public final LX/BLj;
.super Landroid/widget/Filter;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/platform/composer/targetprivacy/PlatformGroupSelectorAdapter;


# direct methods
.method public constructor <init>(Lcom/facebook/platform/composer/targetprivacy/PlatformGroupSelectorAdapter;)V
    .locals 0

    .prologue
    .line 1776372
    iput-object p1, p0, LX/BLj;->a:Lcom/facebook/platform/composer/targetprivacy/PlatformGroupSelectorAdapter;

    invoke-direct {p0}, Landroid/widget/Filter;-><init>()V

    return-void
.end method


# virtual methods
.method public final performFiltering(Ljava/lang/CharSequence;)Landroid/widget/Filter$FilterResults;
    .locals 7

    .prologue
    .line 1776355
    new-instance v1, Landroid/widget/Filter$FilterResults;

    invoke-direct {v1}, Landroid/widget/Filter$FilterResults;-><init>()V

    .line 1776356
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 1776357
    :cond_0
    iget-object v0, p0, LX/BLj;->a:Lcom/facebook/platform/composer/targetprivacy/PlatformGroupSelectorAdapter;

    iget-object v0, v0, Lcom/facebook/platform/composer/targetprivacy/PlatformGroupSelectorAdapter;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iput v0, v1, Landroid/widget/Filter$FilterResults;->count:I

    .line 1776358
    iget-object v0, p0, LX/BLj;->a:Lcom/facebook/platform/composer/targetprivacy/PlatformGroupSelectorAdapter;

    iget-object v0, v0, Lcom/facebook/platform/composer/targetprivacy/PlatformGroupSelectorAdapter;->i:Ljava/util/List;

    iput-object v0, v1, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    .line 1776359
    iget-object v0, p0, LX/BLj;->a:Lcom/facebook/platform/composer/targetprivacy/PlatformGroupSelectorAdapter;

    const/4 v2, 0x0

    .line 1776360
    iput-object v2, v0, Lcom/facebook/platform/composer/targetprivacy/PlatformGroupSelectorAdapter;->h:Ljava/lang/String;

    .line 1776361
    :goto_0
    return-object v1

    .line 1776362
    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1776363
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    sget-object v3, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    invoke-virtual {v0, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    .line 1776364
    iget-object v0, p0, LX/BLj;->a:Lcom/facebook/platform/composer/targetprivacy/PlatformGroupSelectorAdapter;

    iget-object v0, v0, Lcom/facebook/platform/composer/targetprivacy/PlatformGroupSelectorAdapter;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/model/FacebookProfile;

    .line 1776365
    iget-object v5, v0, Lcom/facebook/ipc/model/FacebookProfile;->mDisplayName:Ljava/lang/String;

    sget-object v6, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    invoke-virtual {v5, v6}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1776366
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1776367
    :cond_3
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    iput v0, v1, Landroid/widget/Filter$FilterResults;->count:I

    .line 1776368
    iput-object v2, v1, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    .line 1776369
    iget-object v0, p0, LX/BLj;->a:Lcom/facebook/platform/composer/targetprivacy/PlatformGroupSelectorAdapter;

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1776370
    iput-object v2, v0, Lcom/facebook/platform/composer/targetprivacy/PlatformGroupSelectorAdapter;->h:Ljava/lang/String;

    .line 1776371
    goto :goto_0
.end method

.method public final publishResults(Ljava/lang/CharSequence;Landroid/widget/Filter$FilterResults;)V
    .locals 2

    .prologue
    .line 1776350
    iget-object v0, p2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    if-eqz v0, :cond_0

    .line 1776351
    iget-object v1, p0, LX/BLj;->a:Lcom/facebook/platform/composer/targetprivacy/PlatformGroupSelectorAdapter;

    iget-object v0, p2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    .line 1776352
    iput-object v0, v1, Lcom/facebook/platform/composer/targetprivacy/PlatformGroupSelectorAdapter;->e:Ljava/util/List;

    .line 1776353
    iget-object v0, p0, LX/BLj;->a:Lcom/facebook/platform/composer/targetprivacy/PlatformGroupSelectorAdapter;

    const v1, 0x7072cc09

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1776354
    :cond_0
    return-void
.end method
