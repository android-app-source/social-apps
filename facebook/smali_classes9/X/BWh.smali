.class public LX/BWh;
.super LX/1Cv;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ITEM_ACTION_TYPES:",
        "Ljava/lang/Enum;",
        ">",
        "LX/1Cv;"
    }
.end annotation


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/BWd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/BWd",
            "<TITEM_ACTION_TYPES;>;"
        }
    .end annotation
.end field

.field private final c:LX/BWf;

.field public final d:LX/BWg;

.field private final e:Landroid/view/View$OnClickListener;

.field private final f:Landroid/view/View$OnLongClickListener;

.field private final g:LX/BWc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/widget/compositeadapter/CompositeAdapter$ItemListener",
            "<TITEM_ACTION_TYPES;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/BWf;LX/BWg;LX/BWd;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/BWf;",
            "LX/BWg;",
            "LX/BWd",
            "<TITEM_ACTION_TYPES;>;)V"
        }
    .end annotation

    .prologue
    .line 1792180
    invoke-direct {p0}, LX/1Cv;-><init>()V

    .line 1792181
    new-instance v0, LX/BWa;

    invoke-direct {v0, p0}, LX/BWa;-><init>(LX/BWh;)V

    iput-object v0, p0, LX/BWh;->e:Landroid/view/View$OnClickListener;

    .line 1792182
    new-instance v0, LX/BWb;

    invoke-direct {v0, p0}, LX/BWb;-><init>(LX/BWh;)V

    iput-object v0, p0, LX/BWh;->f:Landroid/view/View$OnLongClickListener;

    .line 1792183
    new-instance v0, LX/BWc;

    invoke-direct {v0, p0}, LX/BWc;-><init>(LX/BWh;)V

    iput-object v0, p0, LX/BWh;->g:LX/BWc;

    .line 1792184
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, LX/BWh;->a:Landroid/content/Context;

    .line 1792185
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BWf;

    iput-object v0, p0, LX/BWh;->c:LX/BWf;

    .line 1792186
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BWg;

    iput-object v0, p0, LX/BWh;->d:LX/BWg;

    .line 1792187
    invoke-static {p4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BWd;

    iput-object v0, p0, LX/BWh;->b:LX/BWd;

    .line 1792188
    return-void
.end method


# virtual methods
.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 1792174
    iget-object v0, p0, LX/BWh;->d:LX/BWg;

    invoke-interface {v0, p1, p2}, LX/BWg;->a(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 1792175
    iget-object v1, p0, LX/BWh;->b:LX/BWd;

    invoke-interface {v1, p1}, LX/BWd;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1792176
    iget-object v1, p0, LX/BWh;->e:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1792177
    :cond_0
    iget-object v1, p0, LX/BWh;->b:LX/BWd;

    invoke-interface {v1}, LX/BWd;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1792178
    iget-object v1, p0, LX/BWh;->f:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 1792179
    :cond_1
    return-object v0
.end method

.method public final a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 1

    .prologue
    .line 1792170
    iget-object v0, p0, LX/BWh;->d:LX/BWg;

    invoke-interface {v0, p2, p3, p4, p5}, LX/BWg;->a(Ljava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V

    .line 1792171
    iget-object v0, p0, LX/BWh;->b:LX/BWd;

    invoke-interface {v0, p4}, LX/BWd;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/BWh;->b:LX/BWd;

    invoke-interface {v0}, LX/BWd;->a()Z

    move-result v0

    if-nez v0, :cond_0

    instance-of v0, p3, LX/BWe;

    if-eqz v0, :cond_1

    .line 1792172
    :cond_0
    const v0, 0x7f0d01cd

    invoke-virtual {p3, v0, p2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 1792173
    :cond_1
    return-void
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 1792169
    iget-object v0, p0, LX/BWh;->c:LX/BWf;

    invoke-interface {v0}, LX/BWf;->a()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1792168
    iget-object v0, p0, LX/BWh;->c:LX/BWf;

    invoke-interface {v0, p1}, LX/BWf;->a(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 1792164
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 1792167
    iget-object v0, p0, LX/BWh;->c:LX/BWf;

    invoke-interface {v0, p1}, LX/BWf;->b(I)I

    move-result v0

    return v0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 1792166
    iget-object v0, p0, LX/BWh;->c:LX/BWf;

    invoke-interface {v0}, LX/BWf;->b()I

    move-result v0

    return v0
.end method

.method public final hasStableIds()Z
    .locals 1

    .prologue
    .line 1792165
    const/4 v0, 0x0

    return v0
.end method
