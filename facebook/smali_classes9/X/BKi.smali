.class public LX/BKi;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static h:LX/0Xm;


# instance fields
.field public a:LX/BKf;

.field public b:LX/BKh;

.field public final c:LX/BKg;

.field public final d:Z

.field public final e:Z

.field public final f:Z

.field public final g:Z


# direct methods
.method public constructor <init>(LX/0ad;)V
    .locals 5
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1774113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1774114
    sget-object v0, LX/0c0;->Live:LX/0c0;

    sget v1, LX/BJ8;->f:I

    invoke-interface {p1, v0, v1, v3}, LX/0ad;->a(LX/0c0;II)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1774115
    sget-object v0, LX/BKf;->FULL:LX/BKf;

    iput-object v0, p0, LX/BKi;->a:LX/BKf;

    .line 1774116
    :goto_0
    sget-object v0, LX/0c0;->Live:LX/0c0;

    sget-char v1, LX/BJ8;->k:C

    const-string v2, "default"

    invoke-interface {p1, v0, v1, v2}, LX/0ad;->a(LX/0c0;CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1774117
    const-string v1, "bottom"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1774118
    sget-object v0, LX/BKh;->BOTTOM:LX/BKh;

    iput-object v0, p0, LX/BKi;->b:LX/BKh;

    .line 1774119
    :goto_1
    sget-object v0, LX/0c0;->Live:LX/0c0;

    sget-short v1, LX/BJ8;->a:S

    invoke-interface {p1, v0, v1, v4}, LX/0ad;->a(LX/0c0;SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/BKi;->d:Z

    .line 1774120
    sget-object v0, LX/0c0;->Live:LX/0c0;

    sget-char v1, LX/BJ8;->c:C

    const-string v2, "default"

    invoke-interface {p1, v0, v1, v2}, LX/0ad;->a(LX/0c0;CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1774121
    const-string v1, "target_privacy"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1774122
    sget-object v0, LX/BKg;->TARGET_PRIVACY:LX/BKg;

    iput-object v0, p0, LX/BKi;->c:LX/BKg;

    .line 1774123
    iput-boolean v3, p0, LX/BKi;->e:Z

    .line 1774124
    :goto_2
    sget-object v0, LX/0c0;->Live:LX/0c0;

    sget-short v1, LX/BJ8;->i:S

    invoke-interface {p1, v0, v1, v3}, LX/0ad;->a(LX/0c0;SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/BKi;->f:Z

    .line 1774125
    sget-object v0, LX/0c0;->Live:LX/0c0;

    sget-short v1, LX/BJ8;->d:S

    invoke-interface {p1, v0, v1, v3}, LX/0ad;->a(LX/0c0;SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/BKi;->g:Z

    .line 1774126
    return-void

    .line 1774127
    :pswitch_0
    sget-object v0, LX/BKf;->MINI:LX/BKf;

    iput-object v0, p0, LX/BKi;->a:LX/BKf;

    goto :goto_0

    .line 1774128
    :cond_0
    const-string v1, "big_bottom"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1774129
    sget-object v0, LX/BKh;->BIG_BOTTOM:LX/BKh;

    iput-object v0, p0, LX/BKi;->b:LX/BKh;

    goto :goto_1

    .line 1774130
    :cond_1
    sget-object v0, LX/BKh;->TOP:LX/BKh;

    iput-object v0, p0, LX/BKi;->b:LX/BKh;

    goto :goto_1

    .line 1774131
    :cond_2
    sget-object v0, LX/BKg;->STACKED:LX/BKg;

    iput-object v0, p0, LX/BKi;->c:LX/BKg;

    .line 1774132
    iput-boolean v4, p0, LX/BKi;->e:Z

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public static a(LX/0QB;)LX/BKi;
    .locals 4

    .prologue
    .line 1774133
    const-class v1, LX/BKi;

    monitor-enter v1

    .line 1774134
    :try_start_0
    sget-object v0, LX/BKi;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1774135
    sput-object v2, LX/BKi;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1774136
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1774137
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1774138
    new-instance p0, LX/BKi;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-direct {p0, v3}, LX/BKi;-><init>(LX/0ad;)V

    .line 1774139
    move-object v0, p0

    .line 1774140
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1774141
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/BKi;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1774142
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1774143
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
