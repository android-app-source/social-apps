.class public final LX/C7U;
.super LX/0Vd;
.source ""


# instance fields
.field public final synthetic a:LX/1Pq;

.field public final synthetic b:Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;

.field public final synthetic c:Lcom/facebook/feedplugins/graphqlstory/usertopictombstone/UserTopicTombstonePartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/graphqlstory/usertopictombstone/UserTopicTombstonePartDefinition;LX/1Pq;Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;)V
    .locals 0

    .prologue
    .line 1851379
    iput-object p1, p0, LX/C7U;->c:Lcom/facebook/feedplugins/graphqlstory/usertopictombstone/UserTopicTombstonePartDefinition;

    iput-object p2, p0, LX/C7U;->a:LX/1Pq;

    iput-object p3, p0, LX/C7U;->b:Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1851380
    iget-object v0, p0, LX/C7U;->c:Lcom/facebook/feedplugins/graphqlstory/usertopictombstone/UserTopicTombstonePartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/graphqlstory/usertopictombstone/UserTopicTombstonePartDefinition;->c:LX/03V;

    sget-object v1, Lcom/facebook/feedplugins/graphqlstory/usertopictombstone/UserTopicTombstonePartDefinition;->b:Ljava/lang/String;

    const-string v2, "unhide topic from user failed"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1851381
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1851382
    iget-object v0, p0, LX/C7U;->a:LX/1Pq;

    check-cast v0, LX/1Pr;

    new-instance v1, LX/C7W;

    iget-object v3, p0, LX/C7U;->b:Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;

    invoke-direct {v1, v3}, LX/C7W;-><init>(Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;)V

    iget-object v3, p0, LX/C7U;->b:Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;

    invoke-interface {v0, v1, v3}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/C7V;

    const/4 v1, 0x0

    .line 1851383
    iput-object v1, v0, LX/C7V;->a:Ljava/lang/String;

    .line 1851384
    iput-object v1, v0, LX/C7V;->b:Ljava/lang/String;

    .line 1851385
    iput-object v1, v0, LX/C7V;->c:Ljava/lang/String;

    .line 1851386
    iput-object v1, v0, LX/C7V;->d:Ljava/lang/String;

    .line 1851387
    iget-object v0, p0, LX/C7U;->c:Lcom/facebook/feedplugins/graphqlstory/usertopictombstone/UserTopicTombstonePartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/graphqlstory/usertopictombstone/UserTopicTombstonePartDefinition;->f:LX/1e8;

    iget-object v1, p0, LX/C7U;->b:Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, LX/1e8;->a(Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;Z)V

    .line 1851388
    iget-object v0, p0, LX/C7U;->c:Lcom/facebook/feedplugins/graphqlstory/usertopictombstone/UserTopicTombstonePartDefinition;

    iget-object v6, v0, Lcom/facebook/feedplugins/graphqlstory/usertopictombstone/UserTopicTombstonePartDefinition;->d:LX/0bH;

    new-instance v0, LX/1Nd;

    iget-object v1, p0, LX/C7U;->b:Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;

    invoke-interface {v1}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v1

    sget-object v4, Lcom/facebook/graphql/enums/StoryVisibility;->VISIBLE:Lcom/facebook/graphql/enums/StoryVisibility;

    iget-object v3, p0, LX/C7U;->b:Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;

    invoke-interface {v3}, Lcom/facebook/graphql/model/HideableUnit;->H_()I

    move-result v5

    move-object v3, v2

    invoke-direct/range {v0 .. v5}, LX/1Nd;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/StoryVisibility;I)V

    invoke-virtual {v6, v0}, LX/0b4;->a(LX/0b7;)V

    .line 1851389
    iget-object v0, p0, LX/C7U;->c:Lcom/facebook/feedplugins/graphqlstory/usertopictombstone/UserTopicTombstonePartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/graphqlstory/usertopictombstone/UserTopicTombstonePartDefinition;->d:LX/0bH;

    new-instance v1, LX/1YM;

    invoke-direct {v1}, LX/1YM;-><init>()V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1851390
    return-void
.end method
