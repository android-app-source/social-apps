.class public final LX/Bd3;
.super LX/BcS;
.source ""


# static fields
.field public static a:LX/Bd3;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Bd0;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public c:LX/Bd5;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1803186
    const/4 v0, 0x0

    sput-object v0, LX/Bd3;->a:LX/Bd3;

    .line 1803187
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/Bd3;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1803242
    invoke-direct {p0}, LX/BcS;-><init>()V

    .line 1803243
    new-instance v0, LX/Bd5;

    invoke-direct {v0}, LX/Bd5;-><init>()V

    iput-object v0, p0, LX/Bd3;->c:LX/Bd5;

    .line 1803244
    return-void
.end method

.method public static a(LX/BcP;Z)V
    .locals 2

    .prologue
    .line 1803235
    invoke-virtual {p0}, LX/BcP;->i()LX/BcO;

    move-result-object v0

    .line 1803236
    if-nez v0, :cond_0

    .line 1803237
    :goto_0
    return-void

    .line 1803238
    :cond_0
    check-cast v0, LX/Bd1;

    .line 1803239
    new-instance v1, LX/Bd2;

    invoke-direct {v1, p1}, LX/Bd2;-><init>(Z)V

    move-object v1, v1

    .line 1803240
    move-object v0, v1

    .line 1803241
    invoke-virtual {p0, v0}, LX/BcP;->a(LX/BcR;)V

    goto :goto_0
.end method

.method public static b(LX/BcP;)LX/Bd0;
    .locals 3

    .prologue
    .line 1803226
    new-instance v0, LX/Bd1;

    invoke-direct {v0}, LX/Bd1;-><init>()V

    .line 1803227
    sget-object v1, LX/Bd3;->b:LX/0Zi;

    invoke-virtual {v1}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Bd0;

    .line 1803228
    if-nez v1, :cond_0

    .line 1803229
    new-instance v1, LX/Bd0;

    invoke-direct {v1}, LX/Bd0;-><init>()V

    .line 1803230
    :cond_0
    iput-object v0, v1, LX/BcN;->a:LX/BcO;

    .line 1803231
    iput-object v0, v1, LX/Bd0;->a:LX/Bd1;

    .line 1803232
    iget-object v2, v1, LX/Bd0;->d:Ljava/util/BitSet;

    invoke-virtual {v2}, Ljava/util/BitSet;->clear()V

    .line 1803233
    move-object v0, v1

    .line 1803234
    return-object v0
.end method

.method public static declared-synchronized d()LX/Bd3;
    .locals 2

    .prologue
    .line 1803245
    const-class v1, LX/Bd3;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/Bd3;->a:LX/Bd3;

    if-nez v0, :cond_0

    .line 1803246
    new-instance v0, LX/Bd3;

    invoke-direct {v0}, LX/Bd3;-><init>()V

    sput-object v0, LX/Bd3;->a:LX/Bd3;

    .line 1803247
    :cond_0
    sget-object v0, LX/Bd3;->a:LX/Bd3;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 1803248
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/BcQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1803212
    invoke-static {}, LX/1dS;->b()V

    .line 1803213
    iget v0, p1, LX/BcQ;->b:I

    .line 1803214
    packed-switch v0, :pswitch_data_0

    .line 1803215
    :goto_0
    return-object v6

    .line 1803216
    :pswitch_0
    check-cast p2, LX/BcM;

    .line 1803217
    iget-boolean v1, p2, LX/BcM;->a:Z

    iget-object v2, p2, LX/BcM;->b:LX/BcL;

    iget-object v3, p2, LX/BcM;->c:Ljava/lang/Throwable;

    iget-object v0, p1, LX/BcQ;->c:[Ljava/lang/Object;

    const/4 v4, 0x0

    aget-object v4, v0, v4

    check-cast v4, LX/BcP;

    .line 1803218
    const/4 v5, 0x0

    .line 1803219
    sget-object v0, LX/Bd4;->a:[I

    invoke-virtual {v2}, LX/BcL;->ordinal()I

    move-result p0

    aget v0, v0, p0

    packed-switch v0, :pswitch_data_1

    .line 1803220
    :goto_1
    invoke-static {v4, v1, v2, v3}, LX/BcS;->a(LX/BcP;ZLX/BcL;Ljava/lang/Throwable;)V

    .line 1803221
    goto :goto_0

    .line 1803222
    :pswitch_1
    invoke-static {v4, v5}, LX/Bd3;->a(LX/BcP;Z)V

    goto :goto_1

    .line 1803223
    :pswitch_2
    const/4 v0, 0x1

    invoke-static {v4, v0}, LX/Bd3;->a(LX/BcP;Z)V

    goto :goto_1

    .line 1803224
    :pswitch_3
    invoke-static {v4, v5}, LX/Bd3;->a(LX/BcP;Z)V

    goto :goto_1

    .line 1803225
    :pswitch_4
    invoke-static {v4, v5}, LX/Bd3;->a(LX/BcP;Z)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x7cd969b6
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final a(LX/BcO;LX/BcO;)V
    .locals 1

    .prologue
    .line 1803208
    check-cast p1, LX/Bd1;

    .line 1803209
    check-cast p2, LX/Bd1;

    .line 1803210
    iget-boolean v0, p1, LX/Bd1;->b:Z

    iput-boolean v0, p2, LX/Bd1;->b:Z

    .line 1803211
    return-void
.end method

.method public final a(LX/BcP;Ljava/util/List;LX/BcO;)V
    .locals 8

    .prologue
    .line 1803195
    check-cast p3, LX/Bd1;

    .line 1803196
    iget-boolean v2, p3, LX/Bd1;->b:Z

    iget v3, p3, LX/Bd1;->c:I

    iget v4, p3, LX/Bd1;->d:I

    iget-object v5, p3, LX/Bd1;->e:LX/1X1;

    iget-object v6, p3, LX/Bd1;->f:LX/BcQ;

    iget-object v7, p3, LX/Bd1;->g:LX/2kW;

    move-object v0, p1

    move-object v1, p2

    .line 1803197
    invoke-static {}, LX/Bck;->d()LX/Bck;

    move-result-object p0

    invoke-virtual {p0, v0}, LX/Bck;->c(LX/BcP;)LX/Bcg;

    move-result-object p0

    invoke-virtual {p0, v7}, LX/Bcg;->a(LX/2kW;)LX/Bcg;

    move-result-object p0

    if-nez v3, :cond_0

    move v3, v4

    .line 1803198
    :cond_0
    iget-object p1, p0, LX/Bcg;->a:LX/Bch;

    iput v3, p1, LX/Bch;->e:I

    .line 1803199
    move-object p0, p0

    .line 1803200
    invoke-virtual {p0, v4}, LX/Bcg;->a(I)LX/Bcg;

    move-result-object p0

    invoke-virtual {p0, v6}, LX/Bcg;->b(LX/BcQ;)LX/Bcg;

    move-result-object p0

    .line 1803201
    const p1, 0x7cd969b6

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v0, p1, v3}, LX/BcS;->a(LX/BcP;I[Ljava/lang/Object;)LX/BcQ;

    move-result-object p1

    move-object p1, p1

    .line 1803202
    invoke-virtual {p0, p1}, LX/Bcg;->c(LX/BcQ;)LX/Bcg;

    move-result-object p0

    invoke-virtual {p0}, LX/Bcg;->b()LX/BcO;

    move-result-object p0

    invoke-interface {v1, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1803203
    if-nez v5, :cond_1

    .line 1803204
    invoke-static {v0}, LX/Bdj;->c(LX/1De;)LX/Bdh;

    move-result-object p0

    invoke-virtual {p0}, LX/1X5;->d()LX/1X1;

    move-result-object v5

    .line 1803205
    :cond_1
    if-eqz v2, :cond_2

    .line 1803206
    invoke-static {v0}, LX/Bce;->b(LX/BcP;)LX/Bcc;

    move-result-object p0

    invoke-virtual {p0, v5}, LX/Bcc;->a(LX/1X1;)LX/Bcc;

    move-result-object p0

    invoke-virtual {p0}, LX/Bcc;->b()LX/BcO;

    move-result-object p0

    invoke-interface {v1, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1803207
    :cond_2
    return-void
.end method

.method public final c(LX/BcP;LX/BcO;)V
    .locals 1

    .prologue
    .line 1803188
    check-cast p2, LX/Bd1;

    .line 1803189
    invoke-static {}, LX/BcS;->a()LX/1np;

    move-result-object v0

    .line 1803190
    const/4 p0, 0x0

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    .line 1803191
    iput-object p0, v0, LX/1np;->a:Ljava/lang/Object;

    .line 1803192
    iget-object p0, v0, LX/1np;->a:Ljava/lang/Object;

    move-object v0, p0

    .line 1803193
    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p2, LX/Bd1;->b:Z

    .line 1803194
    return-void
.end method
