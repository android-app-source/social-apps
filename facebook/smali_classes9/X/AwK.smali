.class public LX/AwK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/89u;


# instance fields
.field public a:Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;

.field private final b:LX/Arh;

.field private final c:LX/86k;

.field private final d:LX/AsL;

.field private e:Ljava/lang/String;

.field public f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/friendsharing/inspiration/fetch/asset/InspirationAssetDownloader;",
            ">;"
        }
    .end annotation
.end field

.field private g:Lcom/facebook/photos/creativeediting/model/SwipeableParams;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Z


# direct methods
.method public constructor <init>(LX/Arh;LX/AsL;LX/86k;)V
    .locals 1
    .param p1    # LX/Arh;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/AsL;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1726387
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1726388
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1726389
    iput-object v0, p0, LX/AwK;->f:LX/0Ot;

    .line 1726390
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/AwK;->h:Z

    .line 1726391
    iput-object p1, p0, LX/AwK;->b:LX/Arh;

    .line 1726392
    iput-object p2, p0, LX/AwK;->d:LX/AsL;

    .line 1726393
    iput-object p3, p0, LX/AwK;->c:LX/86k;

    .line 1726394
    return-void
.end method

.method public static i(LX/AwK;)V
    .locals 2

    .prologue
    .line 1726364
    iget-object v0, p0, LX/AwK;->a:Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/AwK;->h:Z

    if-eqz v0, :cond_0

    .line 1726365
    iget-object v0, p0, LX/AwK;->d:LX/AsL;

    iget-object v1, p0, LX/AwK;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/AsL;->a(Ljava/lang/String;)V

    .line 1726366
    iget-object v0, p0, LX/AwK;->b:LX/Arh;

    iget-object v1, p0, LX/AwK;->a:Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;

    invoke-virtual {v0, v1}, LX/Arh;->a(Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;)V

    .line 1726367
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()LX/86d;
    .locals 1

    .prologue
    .line 1726386
    iget-object v0, p0, LX/AwK;->c:LX/86k;

    return-object v0
.end method

.method public final a(Lcom/facebook/friendsharing/inspiration/model/InspirationModel;)V
    .locals 7

    .prologue
    .line 1726378
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->getShaderFilter()Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1726379
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->getId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/AwK;->e:Ljava/lang/String;

    .line 1726380
    invoke-virtual {p1}, Lcom/facebook/friendsharing/inspiration/model/InspirationModel;->getShaderFilter()Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;->getShaderFilterModel()Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;

    move-result-object v6

    .line 1726381
    if-nez v6, :cond_1

    .line 1726382
    const/4 v0, 0x0

    iput-object v0, p0, LX/AwK;->a:Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;

    .line 1726383
    :cond_0
    :goto_0
    return-void

    .line 1726384
    :cond_1
    new-instance v0, LX/AvG;

    invoke-virtual {v6}, Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;->k()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6}, Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;->k()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".zip"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6}, Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;->l()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v6}, Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;->j()Ljava/lang/String;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, LX/AvG;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    .line 1726385
    iget-object v1, p0, LX/AwK;->f:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/friendsharing/inspiration/fetch/asset/InspirationAssetDownloader;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    new-instance v2, LX/AwJ;

    invoke-direct {v2, p0, v6}, LX/AwJ;-><init>(LX/AwK;Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;)V

    invoke-virtual {v1, v0, v2}, Lcom/facebook/friendsharing/inspiration/fetch/asset/InspirationAssetDownloader;->a(LX/0Px;LX/AvS;)V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 1726377
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1726374
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/AwK;->h:Z

    .line 1726375
    iget-object v0, p0, LX/AwK;->b:LX/Arh;

    const/4 v1, 0x0

    invoke-static {v1}, Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;->a(Lcom/facebook/photos/creativeediting/model/graphql/ShaderFilterGraphQLModels$ShaderFilterModel;)Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig$Builder;->a()Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Arh;->a(Lcom/facebook/videocodec/effects/model/ShaderFilterGLConfig;)V

    .line 1726376
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1726395
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/AwK;->h:Z

    .line 1726396
    iget-object v0, p0, LX/AwK;->d:LX/AsL;

    invoke-virtual {v0}, LX/AsL;->b()V

    .line 1726397
    invoke-static {p0}, LX/AwK;->i(LX/AwK;)V

    .line 1726398
    return-void
.end method

.method public final d()Lcom/facebook/photos/creativeediting/model/SwipeableParams;
    .locals 1

    .prologue
    .line 1726371
    iget-object v0, p0, LX/AwK;->g:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    if-nez v0, :cond_0

    .line 1726372
    iget-object v0, p0, LX/AwK;->e:Ljava/lang/String;

    invoke-static {v0}, LX/8GN;->a(Ljava/lang/String;)Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    move-result-object v0

    iput-object v0, p0, LX/AwK;->g:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    .line 1726373
    :cond_0
    iget-object v0, p0, LX/AwK;->g:Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    return-object v0
.end method

.method public final e()Landroid/net/Uri;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1726370
    const/4 v0, 0x0

    return-object v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1726369
    const/4 v0, 0x0

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1726368
    const/4 v0, 0x0

    return-object v0
.end method

.method public final h()Landroid/net/Uri;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1726363
    const/4 v0, 0x0

    return-object v0
.end method
