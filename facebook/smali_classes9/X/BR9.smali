.class public final LX/BR9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/23P;

.field public final synthetic b:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;LX/23P;)V
    .locals 0

    .prologue
    .line 1783274
    iput-object p1, p0, LX/BR9;->b:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    iput-object p2, p0, LX/BR9;->a:LX/23P;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v6, 0x1

    const/4 v0, 0x2

    const v2, -0x3fd33e08

    invoke-static {v0, v6, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 1783275
    iget-object v0, p0, LX/BR9;->b:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    iget-object v0, v0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->w:LX/BQo;

    iget-object v3, p0, LX/BR9;->b:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    iget-object v3, v3, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->k:LX/BR1;

    iget-object v3, v3, LX/BR1;->g:Ljava/lang/String;

    iget-object v4, p0, LX/BR9;->b:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    iget-object v4, v4, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->k:LX/BR1;

    iget-object v4, v4, LX/BR1;->f:Ljava/lang/String;

    iget-object v5, p0, LX/BR9;->b:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    iget-object v5, v5, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->n:LX/BRP;

    .line 1783276
    iget-object p1, v5, LX/BRP;->a:Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;

    move-object v5, p1

    .line 1783277
    invoke-virtual {v5}, Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;->f()Ljava/lang/String;

    move-result-object v5

    .line 1783278
    const-string p1, "staging_ground_tap_edit_button"

    invoke-static {v0, p1, v3, v4, v5}, LX/BQo;->c(LX/BQo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1783279
    iget-object v0, p0, LX/BR9;->b:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    const-string v3, "enter_crop_view"

    .line 1783280
    iput-object v3, v0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->D:Ljava/lang/String;

    .line 1783281
    iget-object v0, p0, LX/BR9;->b:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    iget-object v0, v0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->n:LX/BRP;

    .line 1783282
    iget-object v3, v0, LX/BRP;->a:Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;

    move-object v0, v3

    .line 1783283
    invoke-virtual {v0}, Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1783284
    iget-object v0, p0, LX/BR9;->b:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    iget-object v0, v0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->n:LX/BRP;

    .line 1783285
    iget-object v3, v0, LX/BRP;->a:Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;

    move-object v0, v3

    .line 1783286
    invoke-virtual {v0}, Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;->c()LX/5QV;

    move-result-object v0

    .line 1783287
    invoke-static {v0}, LX/B5P;->a(LX/5QV;)Lcom/facebook/photos/creativeediting/model/StickerParams;

    move-result-object v0

    .line 1783288
    :goto_0
    new-instance v3, LX/5Ry;

    invoke-direct {v3}, LX/5Ry;-><init>()V

    iget-object v4, p0, LX/BR9;->b:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    iget-object v4, v4, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->k:LX/BR1;

    iget-object v4, v4, LX/BR1;->c:Landroid/graphics/RectF;

    .line 1783289
    iput-object v4, v3, LX/5Ry;->a:Landroid/graphics/RectF;

    .line 1783290
    move-object v3, v3

    .line 1783291
    iput-object v0, v3, LX/5Ry;->b:Lcom/facebook/photos/creativeediting/model/StickerParams;

    .line 1783292
    move-object v0, v3

    .line 1783293
    sget-object v3, Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;->a:LX/434;

    .line 1783294
    iput-object v3, v0, LX/5Ry;->e:LX/434;

    .line 1783295
    move-object v0, v0

    .line 1783296
    invoke-virtual {v0}, LX/5Ry;->a()Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;

    move-result-object v0

    .line 1783297
    new-instance v3, LX/5Rw;

    iget-object v4, p0, LX/BR9;->b:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    iget-object v4, v4, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->i:Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;

    invoke-direct {v3, v4}, LX/5Rw;-><init>(Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;)V

    .line 1783298
    iput-boolean v6, v3, LX/5Rw;->i:Z

    .line 1783299
    move-object v3, v3

    .line 1783300
    iget-object v4, p0, LX/BR9;->b:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    iget-object v4, v4, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->h:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 1783301
    iput-object v4, v3, LX/5Rw;->k:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 1783302
    move-object v3, v3

    .line 1783303
    iget-object v4, p0, LX/BR9;->b:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    iget-object v4, v4, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->k:LX/BR1;

    iget-object v4, v4, LX/BR1;->b:Landroid/net/Uri;

    iget-object v5, p0, LX/BR9;->b:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    iget-object v5, v5, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->k:LX/BR1;

    iget-object v5, v5, LX/BR1;->f:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, LX/5Rw;->a(Landroid/net/Uri;Ljava/lang/String;)LX/5Rw;

    move-result-object v3

    const/4 v4, 0x0

    .line 1783304
    iput-boolean v4, v3, LX/5Rw;->f:Z

    .line 1783305
    move-object v3, v3

    .line 1783306
    iput-object v0, v3, LX/5Rw;->m:Lcom/facebook/ipc/editgallery/EditGalleryZoomCropParams;

    .line 1783307
    move-object v0, v3

    .line 1783308
    iget-object v3, p0, LX/BR9;->b:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    iget-object v3, v3, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->k:LX/BR1;

    iget-object v3, v3, LX/BR1;->g:Ljava/lang/String;

    invoke-virtual {v0, v3}, LX/5Rw;->b(Ljava/lang/String;)LX/5Rw;

    move-result-object v0

    iget-object v3, p0, LX/BR9;->a:LX/23P;

    iget-object v4, p0, LX/BR9;->b:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    iget-object v4, v4, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->t:Landroid/content/Context;

    const v5, 0x7f081894

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v1}, LX/23P;->getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1783309
    iput-object v1, v0, LX/5Rw;->j:Ljava/lang/String;

    .line 1783310
    move-object v0, v0

    .line 1783311
    iget-object v1, p0, LX/BR9;->b:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    iget-object v1, v1, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->n:LX/BRP;

    .line 1783312
    iget-object v3, v1, LX/BRP;->a:Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;

    move-object v1, v3

    .line 1783313
    invoke-virtual {v1}, Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1783314
    sget-object v1, LX/5Rr;->DOODLE:LX/5Rr;

    invoke-virtual {v0, v1}, LX/5Rw;->b(LX/5Rr;)LX/5Rw;

    move-result-object v1

    sget-object v3, LX/5Rr;->FILTER:LX/5Rr;

    invoke-virtual {v1, v3}, LX/5Rw;->b(LX/5Rr;)LX/5Rw;

    move-result-object v1

    sget-object v3, LX/5Rr;->TEXT:LX/5Rr;

    invoke-virtual {v1, v3}, LX/5Rw;->b(LX/5Rr;)LX/5Rw;

    move-result-object v1

    sget-object v3, LX/5Rr;->STICKER:LX/5Rr;

    invoke-virtual {v1, v3}, LX/5Rw;->b(LX/5Rr;)LX/5Rw;

    .line 1783315
    :cond_0
    iget-object v1, p0, LX/BR9;->b:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    iget-object v1, v1, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->t:Landroid/content/Context;

    iget-object v3, p0, LX/BR9;->b:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    iget-object v3, v3, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->l:Ljava/lang/String;

    invoke-virtual {v0}, LX/5Rw;->a()Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;

    move-result-object v0

    invoke-static {v1, v3, v0}, LX/5Rs;->a(Landroid/content/Context;Ljava/lang/String;Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;)Landroid/content/Intent;

    move-result-object v0

    .line 1783316
    iget-object v1, p0, LX/BR9;->b:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    iget-object v1, v1, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->y:Lcom/facebook/content/SecureContextHelper;

    iget-object v3, p0, LX/BR9;->b:Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    iget-object v3, v3, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->u:Lcom/facebook/base/fragment/FbFragment;

    invoke-interface {v1, v0, v6, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 1783317
    const v0, -0x68bffb6c

    invoke-static {v0, v2}, LX/02F;->a(II)V

    return-void

    :cond_1
    move-object v0, v1

    .line 1783318
    goto/16 :goto_0
.end method
