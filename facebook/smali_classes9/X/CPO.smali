.class public final LX/CPO;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/CPO;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/CPM;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/CPP;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1884425
    const/4 v0, 0x0

    sput-object v0, LX/CPO;->a:LX/CPO;

    .line 1884426
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/CPO;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1884427
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1884428
    new-instance v0, LX/CPP;

    invoke-direct {v0}, LX/CPP;-><init>()V

    iput-object v0, p0, LX/CPO;->c:LX/CPP;

    .line 1884429
    return-void
.end method

.method public static declared-synchronized q()LX/CPO;
    .locals 2

    .prologue
    .line 1884430
    const-class v1, LX/CPO;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/CPO;->a:LX/CPO;

    if-nez v0, :cond_0

    .line 1884431
    new-instance v0, LX/CPO;

    invoke-direct {v0}, LX/CPO;-><init>()V

    sput-object v0, LX/CPO;->a:LX/CPO;

    .line 1884432
    :cond_0
    sget-object v0, LX/CPO;->a:LX/CPO;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 1884433
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 1884434
    check-cast p2, LX/CPN;

    .line 1884435
    iget-object v0, p2, LX/CPN;->a:LX/CNb;

    iget-object v1, p2, LX/CPN;->b:LX/CNc;

    iget-object v2, p2, LX/CPN;->c:Ljava/util/List;

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 1884436
    const-string v3, "check-action"

    invoke-virtual {v0, v3}, LX/CNb;->c(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1884437
    const-string v3, "check-action"

    invoke-virtual {v0, v3}, LX/CNb;->b(Ljava/lang/String;)LX/CNb;

    move-result-object v3

    invoke-static {v3, v1}, LX/CNd;->a(LX/CNb;LX/CNc;)LX/CNe;

    move-result-object v3

    .line 1884438
    :goto_0
    const-string p0, "uncheck-action"

    invoke-virtual {v0, p0}, LX/CNb;->c(Ljava/lang/String;)Z

    move-result p0

    if-eqz p0, :cond_0

    .line 1884439
    const-string v4, "uncheck-action"

    invoke-virtual {v0, v4}, LX/CNb;->b(Ljava/lang/String;)LX/CNb;

    move-result-object v4

    invoke-static {v4, v1}, LX/CNd;->a(LX/CNb;LX/CNc;)LX/CNe;

    move-result-object v4

    .line 1884440
    :cond_0
    const/4 p0, 0x0

    .line 1884441
    new-instance p2, LX/Apy;

    invoke-direct {p2}, LX/Apy;-><init>()V

    .line 1884442
    sget-object v1, LX/Apz;->b:LX/0Zi;

    invoke-virtual {v1}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Apx;

    .line 1884443
    if-nez v1, :cond_1

    .line 1884444
    new-instance v1, LX/Apx;

    invoke-direct {v1}, LX/Apx;-><init>()V

    .line 1884445
    :cond_1
    invoke-static {v1, p1, p0, p0, p2}, LX/Apx;->a$redex0(LX/Apx;LX/1De;IILX/Apy;)V

    .line 1884446
    move-object p2, v1

    .line 1884447
    move-object p0, p2

    .line 1884448
    move-object p0, p0

    .line 1884449
    const-string p2, "checked"

    invoke-virtual {v0, p2, v5}, LX/CNb;->a(Ljava/lang/String;Z)Z

    move-result p2

    .line 1884450
    iget-object v1, p0, LX/Apx;->a:LX/Apy;

    iput-boolean p2, v1, LX/Apy;->a:Z

    .line 1884451
    move-object p0, p0

    .line 1884452
    const-string p2, "disabled"

    invoke-virtual {v0, p2, v5}, LX/CNb;->a(Ljava/lang/String;Z)Z

    move-result p2

    if-nez p2, :cond_2

    const/4 v5, 0x1

    .line 1884453
    :cond_2
    iget-object p2, p0, LX/Apx;->a:LX/Apy;

    iput-boolean v5, p2, LX/Apy;->b:Z

    .line 1884454
    move-object v5, p0

    .line 1884455
    const p0, -0x73bc1900

    const/4 p2, 0x2

    new-array p2, p2, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object v3, p2, v1

    const/4 v1, 0x1

    aput-object v4, p2, v1

    invoke-static {p1, p0, p2}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p0

    move-object v3, p0

    .line 1884456
    iget-object v4, v5, LX/Apx;->a:LX/Apy;

    iput-object v3, v4, LX/Apy;->c:LX/1dQ;

    .line 1884457
    move-object v3, v5

    .line 1884458
    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    .line 1884459
    invoke-static {v3, p1, v0, v2}, LX/CPx;->a(LX/1Di;LX/1De;LX/CNb;Ljava/util/List;)LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 1884460
    return-object v0

    :cond_3
    move-object v3, v4

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1884461
    invoke-static {}, LX/1dS;->b()V

    .line 1884462
    iget v0, p1, LX/1dQ;->b:I

    .line 1884463
    packed-switch v0, :pswitch_data_0

    .line 1884464
    :goto_0
    return-object v4

    .line 1884465
    :pswitch_0
    check-cast p2, LX/Apr;

    .line 1884466
    iget-boolean v2, p2, LX/Apr;->a:Z

    iget-object v0, p1, LX/1dQ;->c:[Ljava/lang/Object;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    check-cast v0, LX/CNe;

    iget-object v1, p1, LX/1dQ;->c:[Ljava/lang/Object;

    const/4 v3, 0x1

    aget-object v1, v1, v3

    check-cast v1, LX/CNe;

    .line 1884467
    if-eqz v2, :cond_1

    if-eqz v0, :cond_1

    .line 1884468
    invoke-interface {v0}, LX/CNe;->a()V

    .line 1884469
    :cond_0
    :goto_1
    goto :goto_0

    .line 1884470
    :cond_1
    if-nez v2, :cond_0

    if-eqz v1, :cond_0

    .line 1884471
    invoke-interface {v1}, LX/CNe;->a()V

    goto :goto_1

    :pswitch_data_0
    .packed-switch -0x73bc1900
        :pswitch_0
    .end packed-switch
.end method
