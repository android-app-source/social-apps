.class public final LX/CFR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;)V
    .locals 0

    .prologue
    .line 1863244
    iput-object p1, p0, LX/CFR;->a:Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x448964c1

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1863245
    iget-object v1, p0, LX/CFR;->a:Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;

    invoke-virtual {v1}, Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1863246
    iget-object v1, p0, LX/CFR;->a:Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;

    .line 1863247
    iget-object v2, v1, Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;->b:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    sget-object v4, LX/0ax;->fi:Ljava/lang/String;

    iget-object p0, v1, Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;->k:Lcom/facebook/goodwill/composer/GoodwillComposerEvent;

    .line 1863248
    iget-object p1, p0, Lcom/facebook/goodwill/composer/GoodwillComposerEvent;->a:Ljava/lang/String;

    move-object p0, p1

    .line 1863249
    invoke-static {v4, p0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->b(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    .line 1863250
    const-string v3, "extra_photo_title_text"

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const p0, 0x7f082a2b

    invoke-virtual {v4, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1863251
    iget-object v3, v1, Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;->a:Lcom/facebook/content/SecureContextHelper;

    const/16 v4, 0x26b9

    invoke-interface {v3, v2, v4, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 1863252
    :goto_0
    const v1, 0x3127970a

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 1863253
    :cond_0
    iget-object v1, p0, LX/CFR;->a:Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;

    invoke-virtual {v1}, Lcom/facebook/goodwill/composer/photofragment/PhotoFromFbOrCameraFragment;->d()V

    goto :goto_0
.end method
