.class public final LX/CVb;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1904175
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_a

    .line 1904176
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1904177
    :goto_0
    return v1

    .line 1904178
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_6

    .line 1904179
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 1904180
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1904181
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_0

    if-eqz v10, :cond_0

    .line 1904182
    const-string v11, "default_cash"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 1904183
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v4

    move v9, v4

    move v4, v2

    goto :goto_1

    .line 1904184
    :cond_1
    const-string v11, "default_selected_percentage"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 1904185
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v3

    move v8, v3

    move v3, v2

    goto :goto_1

    .line 1904186
    :cond_2
    const-string v11, "is_cash_allowed"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 1904187
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v7, v0

    move v0, v2

    goto :goto_1

    .line 1904188
    :cond_3
    const-string v11, "name"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 1904189
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 1904190
    :cond_4
    const-string v11, "selectable_percentages"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 1904191
    invoke-static {p0, p1}, LX/2gu;->b(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 1904192
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1904193
    :cond_6
    const/4 v10, 0x5

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 1904194
    if-eqz v4, :cond_7

    .line 1904195
    invoke-virtual {p1, v1, v9}, LX/186;->a(IZ)V

    .line 1904196
    :cond_7
    if-eqz v3, :cond_8

    .line 1904197
    invoke-virtual {p1, v2, v8, v1}, LX/186;->a(III)V

    .line 1904198
    :cond_8
    if-eqz v0, :cond_9

    .line 1904199
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v7}, LX/186;->a(IZ)V

    .line 1904200
    :cond_9
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1904201
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1904202
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_a
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    move v9, v1

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x0

    .line 1904203
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1904204
    invoke-virtual {p0, p1, v2}, LX/15i;->b(II)Z

    move-result v0

    .line 1904205
    if-eqz v0, :cond_0

    .line 1904206
    const-string v1, "default_cash"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1904207
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1904208
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1904209
    if-eqz v0, :cond_1

    .line 1904210
    const-string v1, "default_selected_percentage"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1904211
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1904212
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1904213
    if-eqz v0, :cond_2

    .line 1904214
    const-string v1, "is_cash_allowed"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1904215
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1904216
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1904217
    if-eqz v0, :cond_3

    .line 1904218
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1904219
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1904220
    :cond_3
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 1904221
    if-eqz v0, :cond_4

    .line 1904222
    const-string v0, "selectable_percentages"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1904223
    invoke-virtual {p0, p1, v3}, LX/15i;->e(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->c(Ljava/util/Iterator;LX/0nX;)V

    .line 1904224
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1904225
    return-void
.end method
