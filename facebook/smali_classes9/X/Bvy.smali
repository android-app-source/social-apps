.class public LX/Bvy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "LX/Bvz;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1833223
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1833224
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 4

    .prologue
    .line 1833205
    check-cast p1, LX/Bvz;

    .line 1833206
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1833207
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "format"

    const-string v3, "json"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1833208
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "videoDelete"

    .line 1833209
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 1833210
    move-object v1, v1

    .line 1833211
    const-string v2, "DELETE"

    .line 1833212
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 1833213
    move-object v1, v1

    .line 1833214
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "v2.3/"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p1, LX/Bvz;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1833215
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 1833216
    move-object v1, v1

    .line 1833217
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 1833218
    move-object v0, v1

    .line 1833219
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 1833220
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 1833221
    move-object v0, v0

    .line 1833222
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1833203
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 1833204
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
