.class public LX/C7d;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pb;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pu;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field public final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/1g8;

.field public final c:LX/C7m;

.field public final d:LX/1wB;


# direct methods
.method public constructor <init>(LX/1wB;LX/1g8;LX/C7m;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1wB;",
            "LX/1g8;",
            "LX/C7m;",
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1851548
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1851549
    iput-object p1, p0, LX/C7d;->d:LX/1wB;

    .line 1851550
    iput-object p2, p0, LX/C7d;->b:LX/1g8;

    .line 1851551
    iput-object p3, p0, LX/C7d;->c:LX/C7m;

    .line 1851552
    iput-object p4, p0, LX/C7d;->a:LX/0Ot;

    .line 1851553
    return-void
.end method

.method public static a(LX/0QB;)LX/C7d;
    .locals 7

    .prologue
    .line 1851554
    const-class v1, LX/C7d;

    monitor-enter v1

    .line 1851555
    :try_start_0
    sget-object v0, LX/C7d;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1851556
    sput-object v2, LX/C7d;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1851557
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1851558
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1851559
    new-instance v6, LX/C7d;

    invoke-static {v0}, LX/1wB;->a(LX/0QB;)LX/1wB;

    move-result-object v3

    check-cast v3, LX/1wB;

    invoke-static {v0}, LX/1g8;->a(LX/0QB;)LX/1g8;

    move-result-object v4

    check-cast v4, LX/1g8;

    invoke-static {v0}, LX/C7m;->a(LX/0QB;)LX/C7m;

    move-result-object v5

    check-cast v5, LX/C7m;

    const/16 p0, 0x2eb

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v6, v3, v4, v5, p0}, LX/C7d;-><init>(LX/1wB;LX/1g8;LX/C7m;LX/0Ot;)V

    .line 1851560
    move-object v0, v6

    .line 1851561
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1851562
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C7d;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1851563
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1851564
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
