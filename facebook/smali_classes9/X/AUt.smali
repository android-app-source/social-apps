.class public final LX/AUt;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/facecast/FacecastActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/facecast/FacecastActivity;)V
    .locals 0

    .prologue
    .line 1678448
    iput-object p1, p0, LX/AUt;->a:Lcom/facebook/facecast/FacecastActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 1678449
    iget-object v0, p0, LX/AUt;->a:Lcom/facebook/facecast/FacecastActivity;

    const/4 v1, -0x1

    invoke-static {v0, v1}, Lcom/facebook/facecast/FacecastActivity;->b(Lcom/facebook/facecast/FacecastActivity;I)V

    .line 1678450
    iget-object v0, p0, LX/AUt;->a:Lcom/facebook/facecast/FacecastActivity;

    iget-object v0, v0, Lcom/facebook/facecast/FacecastActivity;->S:LX/Abh;

    .line 1678451
    iget-object v1, v0, LX/Abh;->f:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, v0, LX/Abh;->f:Ljava/lang/String;

    invoke-static {v1}, LX/7Fv;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 1678452
    if-eqz v0, :cond_0

    .line 1678453
    iget-object v0, p0, LX/AUt;->a:Lcom/facebook/facecast/FacecastActivity;

    iget-object v0, v0, Lcom/facebook/facecast/FacecastActivity;->S:LX/Abh;

    .line 1678454
    iget-object v1, v0, LX/Abh;->e:LX/0gt;

    iget-object v2, v0, LX/Abh;->b:Landroid/content/Context;

    invoke-virtual {v1, v2}, LX/0gt;->b(Landroid/content/Context;)V

    .line 1678455
    iget-object v1, v0, LX/Abh;->d:LX/AVT;

    iget-object v2, v0, LX/Abh;->f:Ljava/lang/String;

    .line 1678456
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 1678457
    const-string p0, "facecast_event_name"

    const-string v0, "facecast_broadcast_end_survey"

    invoke-interface {v3, p0, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1678458
    const-string p0, "facecast_event_extra"

    invoke-interface {v3, p0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1678459
    invoke-virtual {v1, v3}, LX/AVT;->a(Ljava/util/Map;)V

    .line 1678460
    :goto_1
    return-void

    .line 1678461
    :cond_0
    iget-object v0, p0, LX/AUt;->a:Lcom/facebook/facecast/FacecastActivity;

    invoke-virtual {v0}, Lcom/facebook/facecast/FacecastActivity;->finish()V

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method
