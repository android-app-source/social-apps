.class public final LX/AXG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/AX5;


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:LX/AXY;


# direct methods
.method public constructor <init>(LX/AXY;Z)V
    .locals 0

    .prologue
    .line 1683851
    iput-object p1, p0, LX/AXG;->b:LX/AXY;

    iput-boolean p2, p0, LX/AXG;->a:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;ILcom/facebook/graphql/model/GraphQLFeedback;)V
    .locals 8
    .param p1    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/graphql/model/GraphQLFeedback;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/AcC;",
            ">;I",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1683833
    iget-object v0, p0, LX/AXG;->b:LX/AXY;

    iget-boolean v1, p0, LX/AXG;->a:Z

    .line 1683834
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1683835
    :cond_0
    invoke-virtual {v0}, LX/AXY;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-wide v4, v0, LX/AXY;->ah:J

    iget-object v3, v0, LX/AXY;->ad:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    iget-object v6, v0, LX/AXY;->S:Ljava/lang/String;

    invoke-static {v2, v4, v5, v3, v6}, LX/AXB;->a(Landroid/content/res/Resources;JLcom/facebook/ipc/composer/intent/ComposerTargetData;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 1683836
    if-nez v2, :cond_1

    .line 1683837
    iget-object v3, v0, LX/AXY;->b:LX/03V;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, LX/AXY;->x:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "_getEmptyFacepileString"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Unsupported type "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, v0, LX/AXY;->ad:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    iget-object v6, v6, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " for "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v0, LX/AXY;->ad:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    iget-wide v6, v6, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " is called."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1683838
    :cond_1
    iget-object v3, v0, LX/AXY;->M:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v3, v2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1683839
    :cond_2
    :goto_0
    return-void

    .line 1683840
    :cond_3
    iget-object v2, v0, LX/AXY;->H:Lcom/facebook/facecast/FacecastFacepileView;

    if-nez v2, :cond_4

    .line 1683841
    iget-object v2, v0, LX/AXY;->D:Landroid/view/ViewStub;

    invoke-virtual {v2}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/facecast/FacecastFacepileView;

    iput-object v2, v0, LX/AXY;->H:Lcom/facebook/facecast/FacecastFacepileView;

    .line 1683842
    :cond_4
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1683843
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/AcC;

    .line 1683844
    iget-object v2, v2, LX/AcC;->b:Ljava/lang/String;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1683845
    :cond_5
    iget-object v2, v0, LX/AXY;->H:Lcom/facebook/facecast/FacecastFacepileView;

    invoke-virtual {v2, v3}, Lcom/facebook/facecast/FacecastFacepileView;->setFBIDs(Ljava/util/List;)V

    .line 1683846
    iget-object v2, v0, LX/AXY;->ai:LX/AYh;

    if-eqz v2, :cond_6

    .line 1683847
    iget-object v2, v0, LX/AXY;->M:Lcom/facebook/widget/text/BetterTextView;

    iget-object v3, v0, LX/AXY;->ai:LX/AYh;

    invoke-virtual {v3, p1, p2}, LX/AYh;->a(Ljava/util/List;I)Landroid/text/SpannableString;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1683848
    :cond_6
    if-eqz p3, :cond_2

    if-nez v1, :cond_2

    .line 1683849
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget-object v3, v0, LX/AXY;->N:Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;

    invoke-static {p3, v2, v3}, LX/39G;->a(Lcom/facebook/graphql/model/GraphQLFeedback;Ljava/lang/Integer;Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;)V

    .line 1683850
    iget-object v2, v0, LX/AXY;->N:Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->setVisibility(I)V

    goto :goto_0
.end method
