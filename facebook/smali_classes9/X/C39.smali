.class public LX/C39;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pb;",
        ":",
        "LX/1Pc;",
        ":",
        "LX/1Pd;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/C3A;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/C39",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/C3A;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1845143
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1845144
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/C39;->b:LX/0Zi;

    .line 1845145
    iput-object p1, p0, LX/C39;->a:LX/0Ot;

    .line 1845146
    return-void
.end method

.method public static a(LX/0QB;)LX/C39;
    .locals 4

    .prologue
    .line 1845147
    const-class v1, LX/C39;

    monitor-enter v1

    .line 1845148
    :try_start_0
    sget-object v0, LX/C39;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1845149
    sput-object v2, LX/C39;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1845150
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1845151
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1845152
    new-instance v3, LX/C39;

    const/16 p0, 0x1ebb

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/C39;-><init>(LX/0Ot;)V

    .line 1845153
    move-object v0, v3

    .line 1845154
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1845155
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C39;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1845156
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1845157
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 5

    .prologue
    .line 1845136
    check-cast p2, LX/C38;

    .line 1845137
    iget-object v0, p0, LX/C39;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/C3A;

    iget-object v1, p2, LX/C38;->a:LX/C33;

    iget-object v2, p2, LX/C38;->b:LX/1Pb;

    iget v3, p2, LX/C38;->c:I

    .line 1845138
    iget-object v4, v0, LX/C3A;->a:LX/1WE;

    invoke-virtual {v4, p1}, LX/1WE;->c(LX/1De;)LX/1XG;

    move-result-object v4

    iget-object p0, v1, LX/C33;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v4, p0}, LX/1XG;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1XG;

    move-result-object v4

    check-cast v2, LX/1Pn;

    invoke-virtual {v4, v2}, LX/1XG;->a(LX/1Pn;)LX/1XG;

    move-result-object v4

    const p0, 0x7f0b1d6b

    invoke-virtual {v4, p0}, LX/1XG;->h(I)LX/1XG;

    move-result-object v4

    const p0, 0x7f0a00a8

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-virtual {v4, p0}, LX/1XG;->a(Ljava/lang/Integer;)LX/1XG;

    move-result-object v4

    .line 1845139
    iget-object p0, v4, LX/1XG;->a:LX/1XE;

    iput v3, p0, LX/1XE;->k:I

    .line 1845140
    move-object v4, v4

    .line 1845141
    invoke-virtual {v4}, LX/1X5;->d()LX/1X1;

    move-result-object v4

    invoke-static {p1, v4}, LX/1n9;->a(LX/1De;LX/1X1;)LX/1Di;

    move-result-object v4

    const/4 p0, 0x6

    const p2, 0x7f0b1d6d

    invoke-interface {v4, p0, p2}, LX/1Di;->g(II)LX/1Di;

    move-result-object v4

    const/4 p0, 0x1

    const p2, 0x7f0b1d7f

    invoke-interface {v4, p0, p2}, LX/1Di;->c(II)LX/1Di;

    move-result-object v4

    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    move-object v0, v4

    .line 1845142
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1845134
    invoke-static {}, LX/1dS;->b()V

    .line 1845135
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c(LX/1De;)LX/C37;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/C39",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1845126
    new-instance v1, LX/C38;

    invoke-direct {v1, p0}, LX/C38;-><init>(LX/C39;)V

    .line 1845127
    iget-object v2, p0, LX/C39;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/C37;

    .line 1845128
    if-nez v2, :cond_0

    .line 1845129
    new-instance v2, LX/C37;

    invoke-direct {v2, p0}, LX/C37;-><init>(LX/C39;)V

    .line 1845130
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/C37;->a$redex0(LX/C37;LX/1De;IILX/C38;)V

    .line 1845131
    move-object v1, v2

    .line 1845132
    move-object v0, v1

    .line 1845133
    return-object v0
.end method
