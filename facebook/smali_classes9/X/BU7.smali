.class public final LX/BU7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# instance fields
.field public final synthetic a:LX/BUA;


# direct methods
.method public constructor <init>(LX/BUA;)V
    .locals 0

    .prologue
    .line 1788273
    iput-object p1, p0, LX/BU7;->a:LX/BUA;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 5

    .prologue
    .line 1788274
    iget-object v0, p0, LX/BU7;->a:LX/BUA;

    iget-object v0, v0, LX/BUA;->e:LX/19w;

    sget-object v1, LX/1A0;->DOWNLOAD_FAILED:LX/1A0;

    invoke-virtual {v0, v1}, LX/19w;->a(LX/1A0;)Ljava/util/List;

    move-result-object v0

    .line 1788275
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Jg;

    .line 1788276
    :try_start_0
    iget-object v2, p0, LX/BU7;->a:LX/BUA;

    iget-object v3, v0, LX/7Jg;->a:Ljava/lang/String;

    sget-object v4, LX/1A0;->DOWNLOAD_NOT_STARTED:LX/1A0;

    invoke-static {v2, v3, v4}, LX/BUA;->a$redex0(LX/BUA;Ljava/lang/String;LX/1A0;)V

    .line 1788277
    iget-object v2, p0, LX/BU7;->a:LX/BUA;

    .line 1788278
    invoke-static {v2, v0}, LX/BUA;->a$redex0(LX/BUA;LX/7Jg;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1788279
    goto :goto_0

    .line 1788280
    :catch_0
    iget-object v2, p0, LX/BU7;->a:LX/BUA;

    iget-object v0, v0, LX/7Jg;->a:Ljava/lang/String;

    invoke-static {v2, v0}, LX/BUA;->g(LX/BUA;Ljava/lang/String;)V

    goto :goto_0

    .line 1788281
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method
