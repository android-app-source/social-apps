.class public final LX/BwJ;
.super LX/2oa;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2oa",
        "<",
        "LX/2ou;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/feed/video/fullscreen/WatchAndMoreFullscreenVideoControlsPlugin;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/video/fullscreen/WatchAndMoreFullscreenVideoControlsPlugin;)V
    .locals 0

    .prologue
    .line 1833500
    iput-object p1, p0, LX/BwJ;->a:Lcom/facebook/feed/video/fullscreen/WatchAndMoreFullscreenVideoControlsPlugin;

    invoke-direct {p0}, LX/2oa;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/2ou;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1833501
    const-class v0, LX/2ou;

    return-object v0
.end method

.method public final b(LX/0b7;)V
    .locals 2

    .prologue
    .line 1833490
    check-cast p1, LX/2ou;

    .line 1833491
    if-eqz p1, :cond_0

    iget-object v0, p1, LX/2ou;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/2ou;->a:Ljava/lang/String;

    iget-object v1, p0, LX/BwJ;->a:Lcom/facebook/feed/video/fullscreen/WatchAndMoreFullscreenVideoControlsPlugin;

    iget-object v1, v1, Lcom/facebook/feed/video/fullscreen/WatchAndMoreFullscreenVideoControlsPlugin;->u:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/2ou;->c:LX/04g;

    sget-object v1, LX/04g;->BY_USER:LX/04g;

    if-eq v0, v1, :cond_1

    .line 1833492
    :cond_0
    :goto_0
    return-void

    .line 1833493
    :cond_1
    iget-object v0, p1, LX/2ou;->b:LX/2qV;

    sget-object v1, LX/2qV;->ATTEMPT_TO_PAUSE:LX/2qV;

    if-eq v0, v1, :cond_2

    iget-object v0, p1, LX/2ou;->b:LX/2qV;

    sget-object v1, LX/2qV;->PAUSED:LX/2qV;

    if-ne v0, v1, :cond_3

    .line 1833494
    :cond_2
    iget-object v0, p0, LX/BwJ;->a:Lcom/facebook/feed/video/fullscreen/WatchAndMoreFullscreenVideoControlsPlugin;

    const/4 v1, 0x1

    .line 1833495
    iput-boolean v1, v0, Lcom/facebook/feed/video/fullscreen/WatchAndMoreFullscreenVideoControlsPlugin;->r:Z

    .line 1833496
    :cond_3
    iget-object v0, p1, LX/2ou;->b:LX/2qV;

    sget-object v1, LX/2qV;->ATTEMPT_TO_PLAY:LX/2qV;

    if-eq v0, v1, :cond_4

    iget-object v0, p1, LX/2ou;->b:LX/2qV;

    sget-object v1, LX/2qV;->PLAYING:LX/2qV;

    if-ne v0, v1, :cond_0

    .line 1833497
    :cond_4
    iget-object v0, p0, LX/BwJ;->a:Lcom/facebook/feed/video/fullscreen/WatchAndMoreFullscreenVideoControlsPlugin;

    const/4 v1, 0x0

    .line 1833498
    iput-boolean v1, v0, Lcom/facebook/feed/video/fullscreen/WatchAndMoreFullscreenVideoControlsPlugin;->r:Z

    .line 1833499
    goto :goto_0
.end method
