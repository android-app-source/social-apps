.class public final LX/Bjh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/app/TimePickerDialog$OnTimeSetListener;


# instance fields
.field public final synthetic a:LX/Bji;


# direct methods
.method public constructor <init>(LX/Bji;)V
    .locals 0

    .prologue
    .line 1812707
    iput-object p1, p0, LX/Bjh;->a:LX/Bji;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onTimeSet(Landroid/widget/TimePicker;II)V
    .locals 6

    .prologue
    const/16 v5, 0xc

    const/16 v4, 0xb

    .line 1812708
    iget-object v0, p0, LX/Bjh;->a:LX/Bji;

    iget-object v0, v0, LX/Bji;->e:Ljava/util/Calendar;

    iget-object v1, p0, LX/Bjh;->a:LX/Bji;

    iget-object v1, v1, LX/Bji;->f:Ljava/util/Calendar;

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 1812709
    iget-object v0, p0, LX/Bjh;->a:LX/Bji;

    iget-object v0, v0, LX/Bji;->e:Ljava/util/Calendar;

    invoke-virtual {v0, v4, p2}, Ljava/util/Calendar;->set(II)V

    .line 1812710
    iget-object v0, p0, LX/Bjh;->a:LX/Bji;

    iget-object v0, v0, LX/Bji;->e:Ljava/util/Calendar;

    invoke-virtual {v0, v5, p3}, Ljava/util/Calendar;->set(II)V

    .line 1812711
    iget-object v0, p0, LX/Bjh;->a:LX/Bji;

    iget-object v1, p0, LX/Bjh;->a:LX/Bji;

    iget-object v1, v1, LX/Bji;->e:Ljava/util/Calendar;

    invoke-static {v0, v1}, LX/Bji;->a$redex0(LX/Bji;Ljava/util/Calendar;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1812712
    iget-object v0, p0, LX/Bjh;->a:LX/Bji;

    iget-object v0, v0, LX/Bji;->f:Ljava/util/Calendar;

    invoke-virtual {v0, v4, p2}, Ljava/util/Calendar;->set(II)V

    .line 1812713
    iget-object v0, p0, LX/Bjh;->a:LX/Bji;

    iget-object v0, v0, LX/Bji;->f:Ljava/util/Calendar;

    invoke-virtual {v0, v5, p3}, Ljava/util/Calendar;->set(II)V

    .line 1812714
    iget-object v0, p0, LX/Bjh;->a:LX/Bji;

    invoke-static {v0}, LX/Bji;->e(LX/Bji;)V

    .line 1812715
    :cond_0
    return-void
.end method
