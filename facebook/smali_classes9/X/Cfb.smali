.class public final LX/Cfb;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/reaction/ReactionUtil;

.field private b:Ljava/lang/String;

.field private c:J


# direct methods
.method public constructor <init>(Lcom/facebook/reaction/ReactionUtil;Ljava/lang/String;)V
    .locals 2
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 1926421
    iput-object p1, p0, LX/Cfb;->a:Lcom/facebook/reaction/ReactionUtil;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    .line 1926422
    iput-object p2, p0, LX/Cfb;->b:Ljava/lang/String;

    .line 1926423
    iget-object v0, p1, Lcom/facebook/reaction/ReactionUtil;->c:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, LX/Cfb;->c:J

    .line 1926424
    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1926425
    iget-object v0, p0, LX/Cfb;->a:Lcom/facebook/reaction/ReactionUtil;

    iget-object v0, v0, Lcom/facebook/reaction/ReactionUtil;->o:LX/1vC;

    const v1, 0x1e0008

    iget-object v2, p0, LX/Cfb;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/1vC;->b(ILjava/lang/String;)V

    .line 1926426
    iget-object v0, p0, LX/Cfb;->a:Lcom/facebook/reaction/ReactionUtil;

    iget-object v0, v0, Lcom/facebook/reaction/ReactionUtil;->o:LX/1vC;

    const v1, 0x1e000e

    iget-object v2, p0, LX/Cfb;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/1vC;->b(ILjava/lang/String;)V

    .line 1926427
    iget-object v0, p0, LX/Cfb;->a:Lcom/facebook/reaction/ReactionUtil;

    iget-object v0, v0, Lcom/facebook/reaction/ReactionUtil;->n:LX/1vi;

    new-instance v1, LX/2jQ;

    iget-object v2, p0, LX/Cfb;->b:Ljava/lang/String;

    invoke-direct {v1, p1, v2}, LX/2jQ;-><init>(Ljava/lang/Throwable;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1926428
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 11

    .prologue
    .line 1926429
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const v4, 0x1e000e

    const v3, 0x1e0008

    .line 1926430
    if-eqz p1, :cond_0

    .line 1926431
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1926432
    if-eqz v0, :cond_0

    .line 1926433
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1926434
    check-cast v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel;->j()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel$ReactionUnitsModel;

    move-result-object v0

    if-nez v0, :cond_3

    .line 1926435
    :cond_0
    const-string v0, "ERROR_INVALID_RESPONSE"

    .line 1926436
    :goto_0
    move-object v0, v0

    .line 1926437
    const-string v1, "SUCCESS"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1926438
    sget-object v0, LX/0ta;->FROM_SERVER:LX/0ta;

    .line 1926439
    iget-object v1, p1, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v1, v1

    .line 1926440
    invoke-virtual {v0, v1}, LX/0ta;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0x1b

    .line 1926441
    :goto_1
    iget-object v1, p0, LX/Cfb;->a:Lcom/facebook/reaction/ReactionUtil;

    iget-object v1, v1, Lcom/facebook/reaction/ReactionUtil;->o:LX/1vC;

    iget-object v2, p0, LX/Cfb;->b:Ljava/lang/String;

    .line 1926442
    iget-object v5, v1, LX/1vC;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v6

    invoke-interface {v5, v3, v6, v0}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    .line 1926443
    iget-object v0, p0, LX/Cfb;->a:Lcom/facebook/reaction/ReactionUtil;

    iget-object v0, v0, Lcom/facebook/reaction/ReactionUtil;->o:LX/1vC;

    iget-object v1, p0, LX/Cfb;->b:Ljava/lang/String;

    invoke-virtual {v0, v3, v1}, LX/1vC;->a(ILjava/lang/String;)V

    .line 1926444
    iget-object v0, p0, LX/Cfb;->a:Lcom/facebook/reaction/ReactionUtil;

    iget-object v0, v0, Lcom/facebook/reaction/ReactionUtil;->o:LX/1vC;

    iget-object v1, p0, LX/Cfb;->b:Ljava/lang/String;

    invoke-virtual {v0, v4, v1}, LX/1vC;->a(ILjava/lang/String;)V

    .line 1926445
    iget-wide v9, p1, Lcom/facebook/fbservice/results/BaseResult;->clientTimeMs:J

    move-wide v0, v9

    .line 1926446
    iget-wide v2, p0, LX/Cfb;->c:J

    sub-long v4, v0, v2

    .line 1926447
    iget-object v0, p0, LX/Cfb;->a:Lcom/facebook/reaction/ReactionUtil;

    iget-object v8, v0, Lcom/facebook/reaction/ReactionUtil;->n:LX/1vi;

    new-instance v1, LX/2jV;

    .line 1926448
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1926449
    check-cast v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel;->j()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel$ReactionUnitsModel;

    move-result-object v2

    iget-object v3, p0, LX/Cfb;->b:Ljava/lang/String;

    iget-wide v6, p0, LX/Cfb;->c:J

    invoke-direct/range {v1 .. v7}, LX/2jV;-><init>(LX/9qT;Ljava/lang/String;JJ)V

    invoke-virtual {v8, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1926450
    :goto_2
    return-void

    .line 1926451
    :cond_1
    const/16 v0, 0x19

    goto :goto_1

    .line 1926452
    :cond_2
    iget-object v1, p0, LX/Cfb;->a:Lcom/facebook/reaction/ReactionUtil;

    iget-object v1, v1, Lcom/facebook/reaction/ReactionUtil;->o:LX/1vC;

    iget-object v2, p0, LX/Cfb;->b:Ljava/lang/String;

    invoke-virtual {v1, v3, v2}, LX/1vC;->b(ILjava/lang/String;)V

    .line 1926453
    iget-object v1, p0, LX/Cfb;->a:Lcom/facebook/reaction/ReactionUtil;

    iget-object v1, v1, Lcom/facebook/reaction/ReactionUtil;->o:LX/1vC;

    iget-object v2, p0, LX/Cfb;->b:Ljava/lang/String;

    invoke-virtual {v1, v4, v2}, LX/1vC;->b(ILjava/lang/String;)V

    .line 1926454
    iget-object v1, p0, LX/Cfb;->a:Lcom/facebook/reaction/ReactionUtil;

    iget-object v1, v1, Lcom/facebook/reaction/ReactionUtil;->n:LX/1vi;

    new-instance v2, LX/2jS;

    iget-object v3, p0, LX/Cfb;->b:Ljava/lang/String;

    invoke-direct {v2, v0, v3}, LX/2jS;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b7;)V

    goto :goto_2

    .line 1926455
    :cond_3
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1926456
    check-cast v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel;->j()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel$ReactionUnitsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionQueryFragmentModel$ReactionUnitsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1926457
    const-string v0, "NO_UNITS_RETURNED"

    goto/16 :goto_0

    .line 1926458
    :cond_4
    const-string v0, "SUCCESS"

    goto/16 :goto_0
.end method
