.class public final enum LX/AUj;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/AUj;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/AUj;

.field public static final enum ASSIGN_DEFAULT:LX/AUj;

.field public static final enum DROP_ALL_TABLES:LX/AUj;

.field public static final enum DROP_TABLE:LX/AUj;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1678327
    new-instance v0, LX/AUj;

    const-string v1, "ASSIGN_DEFAULT"

    invoke-direct {v0, v1, v2}, LX/AUj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AUj;->ASSIGN_DEFAULT:LX/AUj;

    .line 1678328
    new-instance v0, LX/AUj;

    const-string v1, "DROP_TABLE"

    invoke-direct {v0, v1, v3}, LX/AUj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AUj;->DROP_TABLE:LX/AUj;

    .line 1678329
    new-instance v0, LX/AUj;

    const-string v1, "DROP_ALL_TABLES"

    invoke-direct {v0, v1, v4}, LX/AUj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AUj;->DROP_ALL_TABLES:LX/AUj;

    .line 1678330
    const/4 v0, 0x3

    new-array v0, v0, [LX/AUj;

    sget-object v1, LX/AUj;->ASSIGN_DEFAULT:LX/AUj;

    aput-object v1, v0, v2

    sget-object v1, LX/AUj;->DROP_TABLE:LX/AUj;

    aput-object v1, v0, v3

    sget-object v1, LX/AUj;->DROP_ALL_TABLES:LX/AUj;

    aput-object v1, v0, v4

    sput-object v0, LX/AUj;->$VALUES:[LX/AUj;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1678332
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/AUj;
    .locals 1

    .prologue
    .line 1678333
    const-class v0, LX/AUj;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/AUj;

    return-object v0
.end method

.method public static values()[LX/AUj;
    .locals 1

    .prologue
    .line 1678331
    sget-object v0, LX/AUj;->$VALUES:[LX/AUj;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/AUj;

    return-object v0
.end method
