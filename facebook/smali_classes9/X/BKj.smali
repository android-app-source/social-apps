.class public LX/BKj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0io;
.implements LX/0ip;
.implements LX/0iq;
.implements LX/0iu;
.implements LX/2rg;
.implements LX/0j0;
.implements LX/0j1;
.implements LX/0j2;
.implements LX/0ik;
.implements LX/0il;
.implements LX/0j3;
.implements LX/0j4;
.implements LX/0j5;
.implements LX/0j6;
.implements LX/5RE;
.implements LX/0j8;
.implements LX/0j9;
.implements LX/0jB;
.implements LX/0jD;
.implements LX/0jE;
.implements LX/0jF;
.implements LX/0jH;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0io;",
        "LX/0ip;",
        "LX/0iq;",
        "LX/0iu;",
        "LX/2rg;",
        "LX/0j0;",
        "LX/0j1;",
        "LX/0j2;",
        "LX/0ik",
        "<",
        "LX/BKj;",
        ">;",
        "LX/0il",
        "<",
        "LX/BKj;",
        ">;",
        "LX/0j3;",
        "LX/0j4;",
        "LX/0j5;",
        "LX/0j6;",
        "LX/5RE;",
        "LX/0j8;",
        "LX/0j9;",
        "LX/0jB;",
        "LX/0jD;",
        "LX/0jE;",
        "LX/0jF;",
        "LX/0jH;"
    }
.end annotation


# instance fields
.field private final a:LX/BJq;


# direct methods
.method public constructor <init>(LX/BJq;)V
    .locals 1
    .param p1    # LX/BJq;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1774160
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1774161
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BJq;

    iput-object v0, p0, LX/BKj;->a:LX/BJq;

    .line 1774162
    return-void
.end method


# virtual methods
.method public final I()LX/5RF;
    .locals 2

    .prologue
    .line 1774163
    iget-object v0, p0, LX/BKj;->a:LX/BJq;

    invoke-virtual {v0}, LX/BJq;->a()LX/BKm;

    move-result-object v0

    iget-object v0, v0, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v0}, Lcom/facebook/platform/composer/model/PlatformComposition;->e()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1774164
    iget-object v0, p0, LX/BKj;->a:LX/BJq;

    invoke-virtual {v0}, LX/BJq;->a()LX/BKm;

    move-result-object v0

    iget-object v0, v0, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v0}, Lcom/facebook/platform/composer/model/PlatformComposition;->e()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    sget-object v0, LX/5RF;->SINGLE_PHOTO:LX/5RF;

    .line 1774165
    :goto_0
    return-object v0

    .line 1774166
    :cond_0
    sget-object v0, LX/5RF;->MULTIPLE_PHOTOS:LX/5RF;

    goto :goto_0

    .line 1774167
    :cond_1
    sget-object v0, LX/5RF;->NO_ATTACHMENTS:LX/5RF;

    goto :goto_0
.end method

.method public final a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1774168
    return-object p0
.end method

.method public final b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1774169
    return-object p0
.end method

.method public final getAttachments()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1774170
    iget-object v0, p0, LX/BKj;->a:LX/BJq;

    invoke-virtual {v0}, LX/BJq;->a()LX/BKm;

    move-result-object v0

    iget-object v0, v0, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v0}, Lcom/facebook/platform/composer/model/PlatformComposition;->e()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;
    .locals 1

    .prologue
    .line 1774171
    iget-object v0, p0, LX/BKj;->a:LX/BJq;

    invoke-virtual {v0}, LX/BJq;->a()LX/BKm;

    move-result-object v0

    iget-object v0, v0, LX/BKm;->b:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    return-object v0
.end method

.method public final getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;
    .locals 1

    .prologue
    .line 1774172
    iget-object v0, p0, LX/BKj;->a:LX/BJq;

    invoke-virtual {v0}, LX/BJq;->a()LX/BKm;

    move-result-object v0

    iget-object v0, v0, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v0}, Lcom/facebook/platform/composer/model/PlatformComposition;->h()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v0

    return-object v0
.end method

.method public final getMinutiaeObject()Lcom/facebook/composer/minutiae/model/MinutiaeObject;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1774173
    iget-object v0, p0, LX/BKj;->a:LX/BJq;

    invoke-virtual {v0}, LX/BJq;->a()LX/BKm;

    move-result-object v0

    iget-object v0, v0, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v0}, Lcom/facebook/platform/composer/model/PlatformComposition;->i()Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v0

    return-object v0
.end method

.method public final getPageData()Lcom/facebook/ipc/composer/intent/ComposerPageData;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1774174
    iget-object v0, p0, LX/BKj;->a:LX/BJq;

    invoke-virtual {v0}, LX/BJq;->a()LX/BKm;

    move-result-object v0

    iget-object v0, v0, LX/BKm;->i:Lcom/facebook/ipc/composer/intent/ComposerPageData;

    return-object v0
.end method

.method public final getProductItemAttachment()Lcom/facebook/ipc/composer/model/ProductItemAttachment;
    .locals 1

    .prologue
    .line 1774175
    iget-object v0, p0, LX/BKj;->a:LX/BJq;

    invoke-virtual {v0}, LX/BJq;->a()LX/BKm;

    move-result-object v0

    iget-object v0, v0, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v0}, Lcom/facebook/platform/composer/model/PlatformComposition;->j()Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    move-result-object v0

    return-object v0
.end method

.method public final getPromptAnalytics()Lcom/facebook/productionprompts/logging/PromptAnalytics;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1774158
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getPublishMode()LX/5Rn;
    .locals 1

    .prologue
    .line 1774159
    iget-object v0, p0, LX/BKj;->a:LX/BJq;

    invoke-virtual {v0}, LX/BJq;->a()LX/BKm;

    move-result-object v0

    iget-object v0, v0, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v0}, Lcom/facebook/platform/composer/model/PlatformComposition;->l()LX/5Rn;

    move-result-object v0

    return-object v0
.end method

.method public final getReferencedStickerData()Lcom/facebook/ipc/composer/model/ComposerStickerData;
    .locals 1

    .prologue
    .line 1774144
    iget-object v0, p0, LX/BKj;->a:LX/BJq;

    invoke-virtual {v0}, LX/BJq;->a()LX/BKm;

    move-result-object v0

    iget-object v0, v0, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v0}, Lcom/facebook/platform/composer/model/PlatformComposition;->o()Lcom/facebook/ipc/composer/model/ComposerStickerData;

    move-result-object v0

    return-object v0
.end method

.method public final getRichTextStyle()Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1774145
    iget-object v0, p0, LX/BKj;->a:LX/BJq;

    invoke-virtual {v0}, LX/BJq;->a()LX/BKm;

    move-result-object v0

    iget-object v0, v0, LX/BKm;->o:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    return-object v0
.end method

.method public final getSessionId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1774146
    iget-object v0, p0, LX/BKj;->a:LX/BJq;

    invoke-virtual {v0}, LX/BJq;->a()LX/BKm;

    move-result-object v0

    iget-object v0, v0, LX/BKm;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final getShareParams()Lcom/facebook/ipc/composer/intent/ComposerShareParams;
    .locals 1

    .prologue
    .line 1774147
    iget-object v0, p0, LX/BKj;->a:LX/BJq;

    invoke-virtual {v0}, LX/BJq;->a()LX/BKm;

    move-result-object v0

    iget-object v0, v0, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v0}, Lcom/facebook/platform/composer/model/PlatformComposition;->p()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v0

    return-object v0
.end method

.method public final getTaggedUsers()LX/0Px;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/composer/model/ComposerTaggedUser;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1774148
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1774149
    iget-object v0, p0, LX/BKj;->a:LX/BJq;

    invoke-virtual {v0}, LX/BJq;->a()LX/BKm;

    move-result-object v0

    iget-object v0, v0, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v0}, Lcom/facebook/platform/composer/model/PlatformComposition;->t()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 1774150
    invoke-static {v4, v5}, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;->a(J)LX/5Rc;

    move-result-object v0

    invoke-virtual {v0}, LX/5Rc;->a()Lcom/facebook/ipc/composer/model/ComposerTaggedUser;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1774151
    :cond_0
    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final getTargetAlbum()Lcom/facebook/graphql/model/GraphQLAlbum;
    .locals 1

    .prologue
    .line 1774152
    iget-object v0, p0, LX/BKj;->a:LX/BJq;

    invoke-virtual {v0}, LX/BJq;->a()LX/BKm;

    move-result-object v0

    iget-object v0, v0, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v0}, Lcom/facebook/platform/composer/model/PlatformComposition;->c()Lcom/facebook/graphql/model/GraphQLAlbum;

    move-result-object v0

    return-object v0
.end method

.method public final getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;
    .locals 1

    .prologue
    .line 1774153
    iget-object v0, p0, LX/BKj;->a:LX/BJq;

    invoke-virtual {v0}, LX/BJq;->a()LX/BKm;

    move-result-object v0

    iget-object v0, v0, LX/BKm;->e:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    return-object v0
.end method

.method public final getTextWithEntities()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 1

    .prologue
    .line 1774154
    iget-object v0, p0, LX/BKj;->a:LX/BJq;

    invoke-virtual {v0}, LX/BJq;->a()LX/BKm;

    move-result-object v0

    iget-object v0, v0, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v0}, Lcom/facebook/platform/composer/model/PlatformComposition;->f()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    return-object v0
.end method

.method public final isBackoutDraft()Z
    .locals 1

    .prologue
    .line 1774155
    iget-object v0, p0, LX/BKj;->a:LX/BJq;

    invoke-virtual {v0}, LX/BJq;->a()LX/BKm;

    move-result-object v0

    iget-object v0, v0, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v0}, Lcom/facebook/platform/composer/model/PlatformComposition;->v()Z

    move-result v0

    return v0
.end method

.method public final n()Z
    .locals 1

    .prologue
    .line 1774156
    const/4 v0, 0x0

    return v0
.end method

.method public final q()Lcom/facebook/composer/privacy/model/ComposerPrivacyData;
    .locals 1

    .prologue
    .line 1774157
    iget-object v0, p0, LX/BKj;->a:LX/BJq;

    invoke-virtual {v0}, LX/BJq;->a()LX/BKm;

    move-result-object v0

    iget-object v0, v0, LX/BKm;->d:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    return-object v0
.end method
