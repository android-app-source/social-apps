.class public LX/Azu;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0hB;

.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/Azt;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/Azv;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/9J2;

.field public e:LX/Azt;


# direct methods
.method public constructor <init>(LX/9J2;LX/0hB;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1733052
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1733053
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/Azu;->b:Ljava/util/List;

    .line 1733054
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/Azu;->c:Ljava/util/List;

    .line 1733055
    iput-object p2, p0, LX/Azu;->a:LX/0hB;

    .line 1733056
    iput-object p1, p0, LX/Azu;->d:LX/9J2;

    .line 1733057
    return-void
.end method

.method public static a$redex0(LX/Azu;LX/Azt;)V
    .locals 2

    .prologue
    .line 1733058
    iget-object v0, p0, LX/Azu;->e:LX/Azt;

    if-eqz v0, :cond_0

    .line 1733059
    iget-object v0, p0, LX/Azu;->e:LX/Azt;

    sget-object v1, LX/Azs;->PAUSED:LX/Azs;

    invoke-virtual {v0, v1}, LX/Azt;->a(LX/Azs;)V

    .line 1733060
    :cond_0
    iput-object p1, p0, LX/Azu;->e:LX/Azt;

    .line 1733061
    iget-object v0, p0, LX/Azu;->e:LX/Azt;

    sget-object v1, LX/Azs;->RESUMED:LX/Azs;

    invoke-virtual {v0, v1}, LX/Azt;->a(LX/Azs;)V

    .line 1733062
    iget-object v0, p0, LX/Azu;->d:LX/9J2;

    iget-object v1, p0, LX/Azu;->e:LX/Azt;

    .line 1733063
    iget-object p0, v1, LX/Azt;->b:LX/Azj;

    invoke-interface {p0}, LX/Azj;->getLoggingType()Ljava/lang/String;

    move-result-object p0

    move-object v1, p0

    .line 1733064
    new-instance p0, Landroid/os/Bundle;

    invoke-direct {p0}, Landroid/os/Bundle;-><init>()V

    .line 1733065
    const-string p1, "composer_tab_type"

    invoke-virtual {p0, p1, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1733066
    sget-object p1, LX/9J1;->COMPOSER_TAB_TAPPED:LX/9J1;

    invoke-static {v0, p1, p0}, LX/9J2;->a(LX/9J2;LX/9J1;Landroid/os/Bundle;)V

    .line 1733067
    return-void
.end method
