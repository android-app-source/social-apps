.class public final LX/BeG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/crowdsourcing/protocol/graphql/FeatherQueriesModels$FeatherQuestionsQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:J

.field public final synthetic b:Z

.field public final synthetic c:LX/BeI;

.field public final synthetic d:Landroid/content/Context;

.field public final synthetic e:LX/BeH;


# direct methods
.method public constructor <init>(LX/BeH;JZLX/BeI;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1804748
    iput-object p1, p0, LX/BeG;->e:LX/BeH;

    iput-wide p2, p0, LX/BeG;->a:J

    iput-boolean p4, p0, LX/BeG;->b:Z

    iput-object p5, p0, LX/BeG;->c:LX/BeI;

    iput-object p6, p0, LX/BeG;->d:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1804712
    iget-object v0, p0, LX/BeG;->e:LX/BeH;

    iget-object v1, v0, LX/BeH;->e:LX/0if;

    sget-object v2, LX/0ig;->au:LX/0ih;

    iget-boolean v0, p0, LX/BeG;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "fetch_failed"

    :goto_0
    invoke-virtual {v1, v2, v0}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1804713
    iget-object v0, p0, LX/BeG;->e:LX/BeH;

    iget-object v0, v0, LX/BeH;->a:LX/03V;

    const-string v1, "Feather"

    const-string v2, "Failed to load Feather questions"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1804714
    return-void

    .line 1804715
    :cond_0
    const-string v0, "prefetch_failed"

    goto :goto_0
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 6
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1804716
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1804717
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    iget-wide v2, p0, LX/BeG;->a:J

    sub-long/2addr v0, v2

    const-wide v2, 0xee6b2800L

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 1804718
    iget-object v0, p0, LX/BeG;->e:LX/BeH;

    iget-object v1, v0, LX/BeH;->e:LX/0if;

    sget-object v2, LX/0ig;->au:LX/0ih;

    iget-boolean v0, p0, LX/BeG;->b:Z

    if-eqz v0, :cond_0

    const-string v0, "fetch_successful_too_late"

    :goto_0
    invoke-virtual {v1, v2, v0}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1804719
    iget-object v0, p0, LX/BeG;->e:LX/BeH;

    iget-object v0, v0, LX/BeH;->e:LX/0if;

    sget-object v1, LX/0ig;->au:LX/0ih;

    invoke-virtual {v0, v1}, LX/0if;->c(LX/0ih;)V

    .line 1804720
    :goto_1
    return-void

    .line 1804721
    :cond_0
    const-string v0, "prefetch_successful_too_late"

    goto :goto_0

    .line 1804722
    :cond_1
    if-eqz p1, :cond_2

    .line 1804723
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1804724
    if-eqz v0, :cond_2

    .line 1804725
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1804726
    check-cast v0, Lcom/facebook/crowdsourcing/protocol/graphql/FeatherQueriesModels$FeatherQuestionsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/FeatherQueriesModels$FeatherQuestionsQueryModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1804727
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1804728
    check-cast v0, Lcom/facebook/crowdsourcing/protocol/graphql/FeatherQueriesModels$FeatherQuestionsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/FeatherQueriesModels$FeatherQuestionsQueryModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1804729
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1804730
    check-cast v0, Lcom/facebook/crowdsourcing/protocol/graphql/FeatherQueriesModels$FeatherQuestionsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/FeatherQueriesModels$FeatherQuestionsQueryModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1804731
    :cond_2
    iget-object v0, p0, LX/BeG;->e:LX/BeH;

    iget-object v1, v0, LX/BeH;->e:LX/0if;

    sget-object v2, LX/0ig;->au:LX/0ih;

    iget-boolean v0, p0, LX/BeG;->b:Z

    if-eqz v0, :cond_3

    const-string v0, "fetch_successful_no_data"

    :goto_2
    invoke-virtual {v1, v2, v0}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1804732
    iget-object v0, p0, LX/BeG;->e:LX/BeH;

    iget-object v0, v0, LX/BeH;->e:LX/0if;

    sget-object v1, LX/0ig;->au:LX/0ih;

    invoke-virtual {v0, v1}, LX/0if;->c(LX/0ih;)V

    goto :goto_1

    .line 1804733
    :cond_3
    const-string v0, "prefetch_successful_no_data"

    goto :goto_2

    .line 1804734
    :cond_4
    iget-object v0, p0, LX/BeG;->e:LX/BeH;

    iget-object v1, v0, LX/BeH;->e:LX/0if;

    sget-object v2, LX/0ig;->au:LX/0ih;

    iget-boolean v0, p0, LX/BeG;->b:Z

    if-eqz v0, :cond_5

    const-string v0, "fetch_successful"

    :goto_3
    invoke-virtual {v1, v2, v0}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1804735
    iget-object v0, p0, LX/BeG;->e:LX/BeH;

    iget-object v1, p0, LX/BeG;->c:LX/BeI;

    iget-object v1, v1, LX/BeI;->a:Ljava/lang/String;

    .line 1804736
    iget-object v2, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v2, v2

    .line 1804737
    check-cast v2, Lcom/facebook/crowdsourcing/protocol/graphql/FeatherQueriesModels$FeatherQuestionsQueryModel;

    invoke-virtual {v2}, Lcom/facebook/crowdsourcing/protocol/graphql/FeatherQueriesModels$FeatherQuestionsQueryModel;->j()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/BeG;->c:LX/BeI;

    iget-object v3, v3, LX/BeI;->b:Ljava/lang/String;

    .line 1804738
    iget-object v4, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v4, v4

    .line 1804739
    check-cast v4, Lcom/facebook/crowdsourcing/protocol/graphql/FeatherQueriesModels$FeatherQuestionsQueryModel;

    invoke-virtual {v4}, Lcom/facebook/crowdsourcing/protocol/graphql/FeatherQueriesModels$FeatherQuestionsQueryModel;->a()LX/0Px;

    move-result-object v4

    iget-object v5, p0, LX/BeG;->d:Landroid/content/Context;

    .line 1804740
    new-instance p0, Landroid/content/Intent;

    const-class p1, Lcom/facebook/crowdsourcing/feather/activity/FeatherActivity;

    invoke-direct {p0, v5, p1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1804741
    const-string p1, "page_id"

    invoke-virtual {p0, p1, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1804742
    const-string p1, "page_name"

    invoke-virtual {p0, p1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1804743
    const-string p1, "entry_point"

    invoke-virtual {p0, p1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1804744
    const-string p1, "questions"

    invoke-static {p0, p1, v4}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/util/List;)V

    .line 1804745
    iget-object p1, v0, LX/BeH;->d:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {p1, p0, v5}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1804746
    goto/16 :goto_1

    .line 1804747
    :cond_5
    const-string v0, "prefetch_successful"

    goto :goto_3
.end method
