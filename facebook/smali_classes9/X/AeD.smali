.class public final LX/AeD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "LX/0P1",
        "<",
        "Ljava/lang/String;",
        "LX/0Px",
        "<",
        "Lcom/facebook/user/model/User;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/facecastdisplay/friendInviter/LiveVideoSingleClickInviterFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/facecastdisplay/friendInviter/LiveVideoSingleClickInviterFragment;)V
    .locals 0

    .prologue
    .line 1696957
    iput-object p1, p0, LX/AeD;->a:Lcom/facebook/facecastdisplay/friendInviter/LiveVideoSingleClickInviterFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 9

    .prologue
    .line 1696958
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v2

    .line 1696959
    iget-object v0, p0, LX/AeD;->a:Lcom/facebook/facecastdisplay/friendInviter/LiveVideoSingleClickInviterFragment;

    .line 1696960
    invoke-virtual {v0}, Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;->l()LX/0Px;

    move-result-object v1

    move-object v3, v1

    .line 1696961
    iget-object v0, p0, LX/AeD;->a:Lcom/facebook/facecastdisplay/friendInviter/LiveVideoSingleClickInviterFragment;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoSingleClickInviterFragment;->i:LX/AeB;

    .line 1696962
    iget-object v1, v0, LX/AeB;->j:LX/0Px;

    move-object v4, v1

    .line 1696963
    invoke-virtual {v4}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1696964
    sget-object v0, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoSingleClickInviterFragment;->g:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 1696965
    invoke-virtual {v2}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    .line 1696966
    :goto_0
    return-object v0

    .line 1696967
    :cond_0
    new-instance v5, LX/0Pz;

    invoke-direct {v5}, LX/0Pz;-><init>()V

    .line 1696968
    new-instance v6, LX/0Pz;

    invoke-direct {v6}, LX/0Pz;-><init>()V

    .line 1696969
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v7

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v7, :cond_2

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 1696970
    iget-object v8, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v8, v8

    .line 1696971
    invoke-virtual {v4, v8}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 1696972
    invoke-virtual {v5, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1696973
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1696974
    :cond_1
    invoke-virtual {v6, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_2

    .line 1696975
    :cond_2
    sget-object v0, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoSingleClickInviterFragment;->f:Ljava/lang/String;

    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 1696976
    sget-object v0, Lcom/facebook/facecastdisplay/friendInviter/LiveVideoSingleClickInviterFragment;->g:Ljava/lang/String;

    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 1696977
    invoke-virtual {v2}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    goto :goto_0
.end method
