.class public final LX/CFz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/39D;


# instance fields
.field public final synthetic a:LX/CG2;


# direct methods
.method public constructor <init>(LX/CG2;)V
    .locals 0

    .prologue
    .line 1864761
    iput-object p1, p0, LX/CFz;->a:LX/CG2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 1864756
    iget-object v0, p0, LX/CFz;->a:LX/CG2;

    invoke-static {v0}, LX/CG2;->c(LX/CG2;)V

    .line 1864757
    iget-object v0, p0, LX/CFz;->a:LX/CG2;

    invoke-static {v0}, LX/CG2;->d(LX/CG2;)D

    move-result-wide v0

    const-wide v2, 0x3fd999999999999aL    # 0.4

    cmpl-double v0, v0, v2

    if-lez v0, :cond_0

    .line 1864758
    iget-object v0, p0, LX/CFz;->a:LX/CG2;

    iget-object v0, v0, LX/CG2;->d:LX/8YK;

    iget-object v1, p0, LX/CFz;->a:LX/CG2;

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-static {v1, v2, v3}, LX/CG2;->b$redex0(LX/CG2;D)D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, LX/8YK;->b(D)LX/8YK;

    .line 1864759
    :goto_0
    return-void

    .line 1864760
    :cond_0
    iget-object v0, p0, LX/CFz;->a:LX/CG2;

    iget-object v0, v0, LX/CG2;->d:LX/8YK;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, LX/8YK;->b(D)LX/8YK;

    goto :goto_0
.end method

.method public final a(LX/31M;I)V
    .locals 12

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    .line 1864762
    iget-object v0, p0, LX/CFz;->a:LX/CG2;

    iget-boolean v0, v0, LX/CG2;->k:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/CFz;->a:LX/CG2;

    iget-object v0, v0, LX/CG2;->g:LX/31M;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, LX/CFz;->a:LX/CG2;

    invoke-static {v0}, LX/CG2;->d(LX/CG2;)D

    move-result-wide v8

    const-wide v10, 0x3fe999999999999aL    # 0.8

    cmpl-double v0, v8, v10

    if-gtz v0, :cond_1

    :cond_0
    iget-object v0, p0, LX/CFz;->a:LX/CG2;

    iget-object v0, v0, LX/CG2;->g:LX/31M;

    if-eq p1, v0, :cond_4

    iget-object v0, p0, LX/CFz;->a:LX/CG2;

    invoke-static {v0}, LX/CG2;->d(LX/CG2;)D

    move-result-wide v8

    const-wide v10, 0x3fc999999999999aL    # 0.2

    cmpg-double v0, v8, v10

    if-gez v0, :cond_4

    :cond_1
    iget-object v0, p0, LX/CFz;->a:LX/CG2;

    iget-object v0, v0, LX/CG2;->g:LX/31M;

    if-ne p1, v0, :cond_3

    iget-object v0, p0, LX/CFz;->a:LX/CG2;

    iget v0, v0, LX/CG2;->j:I

    :goto_0
    invoke-virtual {p1, v0}, LX/31M;->isSetInFlags(I)Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    .line 1864763
    :goto_1
    if-eqz v0, :cond_2

    .line 1864764
    iget-object v0, p0, LX/CFz;->a:LX/CG2;

    invoke-static {v0}, LX/CG2;->d(LX/CG2;)D

    move-result-wide v8

    const-wide/high16 v10, 0x3fe0000000000000L    # 0.5

    cmpl-double v0, v8, v10

    if-lez v0, :cond_5

    move v0, v1

    .line 1864765
    :goto_2
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    invoke-virtual {p1}, LX/31M;->name()Ljava/lang/String;

    .line 1864766
    iget-object v1, p0, LX/CFz;->a:LX/CG2;

    iget-object v1, v1, LX/CG2;->d:LX/8YK;

    iget-object v8, p0, LX/CFz;->a:LX/CG2;

    if-eqz v0, :cond_6

    move-wide v2, v4

    :goto_3
    invoke-static {v8, v2, v3}, LX/CG2;->b$redex0(LX/CG2;D)D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, LX/8YK;->a(D)LX/8YK;

    .line 1864767
    iget-object v1, p0, LX/CFz;->a:LX/CG2;

    invoke-static {v1, v0}, LX/CG2;->a$redex0(LX/CG2;Z)V

    .line 1864768
    iget-object v0, p0, LX/CFz;->a:LX/CG2;

    invoke-static {v0, p1}, LX/CG2;->c(LX/CG2;LX/31M;)V

    .line 1864769
    :cond_2
    iget-object v0, p0, LX/CFz;->a:LX/CG2;

    invoke-static {v0}, LX/CG2;->c(LX/CG2;)V

    .line 1864770
    iget-object v0, p0, LX/CFz;->a:LX/CG2;

    iget-object v0, v0, LX/CG2;->g:LX/31M;

    if-ne p1, v0, :cond_7

    .line 1864771
    iget-object v0, p0, LX/CFz;->a:LX/CG2;

    iget-object v0, v0, LX/CG2;->d:LX/8YK;

    iget-object v1, p0, LX/CFz;->a:LX/CG2;

    invoke-static {v1, v4, v5}, LX/CG2;->b$redex0(LX/CG2;D)D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, LX/8YK;->b(D)LX/8YK;

    .line 1864772
    iget-object v0, p0, LX/CFz;->a:LX/CG2;

    iget-object v0, v0, LX/CG2;->d:LX/8YK;

    int-to-double v2, p2

    invoke-virtual {v0, v2, v3}, LX/8YK;->c(D)LX/8YK;

    .line 1864773
    :goto_4
    return-void

    .line 1864774
    :cond_3
    iget-object v0, p0, LX/CFz;->a:LX/CG2;

    iget v0, v0, LX/CG2;->i:I

    goto :goto_0

    :cond_4
    move v0, v2

    goto :goto_1

    :cond_5
    move v0, v2

    .line 1864775
    goto :goto_2

    :cond_6
    move-wide v2, v6

    .line 1864776
    goto :goto_3

    .line 1864777
    :cond_7
    iget-object v0, p0, LX/CFz;->a:LX/CG2;

    iget-object v0, v0, LX/CG2;->d:LX/8YK;

    invoke-virtual {v0, v6, v7}, LX/8YK;->b(D)LX/8YK;

    .line 1864778
    iget-object v0, p0, LX/CFz;->a:LX/CG2;

    iget-object v0, v0, LX/CG2;->d:LX/8YK;

    int-to-double v2, p2

    invoke-virtual {v0, v2, v3}, LX/8YK;->c(D)LX/8YK;

    goto :goto_4
.end method

.method public final a(FFLX/31M;)Z
    .locals 2

    .prologue
    .line 1864746
    iget-object v0, p0, LX/CFz;->a:LX/CG2;

    iget-object v0, v0, LX/CG2;->h:LX/CG1;

    sget-object v1, LX/CG1;->IDLE:LX/CG1;

    if-ne v0, v1, :cond_0

    .line 1864747
    iget-object v0, p0, LX/CFz;->a:LX/CG2;

    invoke-static {v0, p3}, LX/CG2;->c(LX/CG2;LX/31M;)V

    .line 1864748
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 1864749
    :cond_0
    iget-object v0, p0, LX/CFz;->a:LX/CG2;

    const/4 p0, 0x1

    .line 1864750
    iget-object v1, v0, LX/CG2;->h:LX/CG1;

    sget-object p1, LX/CG1;->SPRINGING:LX/CG1;

    if-ne v1, p1, :cond_1

    move v1, p0

    :goto_1
    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 1864751
    iget-object v1, v0, LX/CG2;->d:LX/8YK;

    invoke-virtual {v1}, LX/8YK;->f()LX/8YK;

    .line 1864752
    iput-boolean p0, v0, LX/CG2;->k:Z

    .line 1864753
    sget-object v1, LX/CG1;->DRAGGING:LX/CG1;

    iput-object v1, v0, LX/CG2;->h:LX/CG1;

    .line 1864754
    goto :goto_0

    .line 1864755
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 1864745
    return-void
.end method

.method public final b(FFLX/31M;)V
    .locals 10

    .prologue
    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    .line 1864730
    iget-object v0, p0, LX/CFz;->a:LX/CG2;

    iget-object v0, v0, LX/CG2;->g:LX/31M;

    invoke-virtual {v0}, LX/31M;->isYAxis()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1864731
    :goto_0
    iget-object v0, p0, LX/CFz;->a:LX/CG2;

    iget-object v0, v0, LX/CG2;->d:LX/8YK;

    invoke-virtual {v0}, LX/8YK;->b()D

    move-result-wide v0

    float-to-double v2, p2

    add-double/2addr v0, v2

    .line 1864732
    iget-object v2, p0, LX/CFz;->a:LX/CG2;

    invoke-static {v2, v0, v1}, LX/CG2;->c(LX/CG2;D)D

    move-result-wide v2

    .line 1864733
    cmpl-double v4, v2, v8

    if-lez v4, :cond_2

    iget-object v4, p0, LX/CFz;->a:LX/CG2;

    iget-object v4, v4, LX/CG2;->g:LX/31M;

    iget-object v5, p0, LX/CFz;->a:LX/CG2;

    iget v5, v5, LX/CG2;->j:I

    invoke-virtual {v4, v5}, LX/31M;->isSetInFlags(I)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1864734
    iget-object v0, p0, LX/CFz;->a:LX/CG2;

    iget-object v0, v0, LX/CG2;->d:LX/8YK;

    iget-object v1, p0, LX/CFz;->a:LX/CG2;

    invoke-static {v1, v8, v9}, LX/CG2;->b$redex0(LX/CG2;D)D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, LX/8YK;->a(D)LX/8YK;

    .line 1864735
    iget-object v0, p0, LX/CFz;->a:LX/CG2;

    const/4 v1, 0x1

    invoke-static {v0, v1}, LX/CG2;->a$redex0(LX/CG2;Z)V

    .line 1864736
    iget-object v0, p0, LX/CFz;->a:LX/CG2;

    iget-object v1, p0, LX/CFz;->a:LX/CG2;

    iget-object v1, v1, LX/CG2;->g:LX/31M;

    invoke-static {v0, v1}, LX/CG2;->c(LX/CG2;LX/31M;)V

    .line 1864737
    :cond_0
    :goto_1
    return-void

    :cond_1
    move p2, p1

    .line 1864738
    goto :goto_0

    .line 1864739
    :cond_2
    cmpg-double v2, v2, v6

    if-gez v2, :cond_3

    iget-object v2, p0, LX/CFz;->a:LX/CG2;

    iget-object v2, v2, LX/CG2;->g:LX/31M;

    invoke-static {v2}, LX/CG2;->d(LX/31M;)LX/31M;

    move-result-object v2

    iget-object v3, p0, LX/CFz;->a:LX/CG2;

    iget v3, v3, LX/CG2;->i:I

    invoke-virtual {v2, v3}, LX/31M;->isSetInFlags(I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1864740
    iget-object v0, p0, LX/CFz;->a:LX/CG2;

    iget-object v0, v0, LX/CG2;->d:LX/8YK;

    iget-object v1, p0, LX/CFz;->a:LX/CG2;

    invoke-static {v1, v6, v7}, LX/CG2;->b$redex0(LX/CG2;D)D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, LX/8YK;->a(D)LX/8YK;

    .line 1864741
    iget-object v0, p0, LX/CFz;->a:LX/CG2;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/CG2;->a$redex0(LX/CG2;Z)V

    .line 1864742
    iget-object v0, p0, LX/CFz;->a:LX/CG2;

    iget-object v1, p0, LX/CFz;->a:LX/CG2;

    iget-object v1, v1, LX/CG2;->g:LX/31M;

    invoke-static {v1}, LX/CG2;->d(LX/31M;)LX/31M;

    move-result-object v1

    invoke-static {v0, v1}, LX/CG2;->c(LX/CG2;LX/31M;)V

    goto :goto_1

    .line 1864743
    :cond_3
    iget-object v2, p0, LX/CFz;->a:LX/CG2;

    invoke-static {v2, v0, v1}, LX/CG2;->a(LX/CG2;D)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1864744
    iget-object v2, p0, LX/CFz;->a:LX/CG2;

    iget-object v2, v2, LX/CG2;->d:LX/8YK;

    invoke-virtual {v2, v0, v1}, LX/8YK;->a(D)LX/8YK;

    goto :goto_1
.end method
