.class public final LX/CAu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Landroid/content/Context;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Lcom/facebook/feedplugins/prompts/SocialPromptFeedEntryPointPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/prompts/SocialPromptFeedEntryPointPartDefinition;Landroid/content/Context;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1855826
    iput-object p1, p0, LX/CAu;->c:Lcom/facebook/feedplugins/prompts/SocialPromptFeedEntryPointPartDefinition;

    iput-object p2, p0, LX/CAu;->a:Landroid/content/Context;

    iput-object p3, p0, LX/CAu;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x112ded4d

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1855818
    iget-object v0, p0, LX/CAu;->c:Lcom/facebook/feedplugins/prompts/SocialPromptFeedEntryPointPartDefinition;

    iget-object v0, v0, Lcom/facebook/feedplugins/prompts/SocialPromptFeedEntryPointPartDefinition;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CAt;

    iget-object v2, p0, LX/CAu;->a:Landroid/content/Context;

    iget-object v3, p0, LX/CAu;->b:Ljava/lang/String;

    .line 1855819
    sget-object v5, LX/0ax;->iE:Ljava/lang/String;

    invoke-static {v5, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 1855820
    iget-object v6, v0, LX/CAt;->c:LX/17Y;

    invoke-interface {v6, v2, v5}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v5

    .line 1855821
    sget-object v6, LX/CAr;->a:Ljava/lang/String;

    invoke-virtual {v5, v6, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1855822
    move-object v6, v5

    .line 1855823
    if-nez v6, :cond_0

    .line 1855824
    :goto_0
    const v0, -0x54c9a44a

    invoke-static {v4, v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1855825
    :cond_0
    iget-object p0, v0, LX/CAt;->b:Lcom/facebook/content/SecureContextHelper;

    sget p1, LX/CAr;->b:I

    const-class v5, Landroid/app/Activity;

    invoke-static {v2, v5}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/Activity;

    invoke-interface {p0, v6, p1, v5}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    goto :goto_0
.end method
