.class public LX/Anw;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Anx;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Anw",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/Anx;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1713132
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1713133
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/Anw;->b:LX/0Zi;

    .line 1713134
    iput-object p1, p0, LX/Anw;->a:LX/0Ot;

    .line 1713135
    return-void
.end method

.method public static a(LX/0QB;)LX/Anw;
    .locals 4

    .prologue
    .line 1713121
    const-class v1, LX/Anw;

    monitor-enter v1

    .line 1713122
    :try_start_0
    sget-object v0, LX/Anw;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1713123
    sput-object v2, LX/Anw;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1713124
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1713125
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1713126
    new-instance v3, LX/Anw;

    const/16 p0, 0x1ea8

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/Anw;-><init>(LX/0Ot;)V

    .line 1713127
    move-object v0, v3

    .line 1713128
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1713129
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Anw;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1713130
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1713131
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 13

    .prologue
    .line 1713099
    check-cast p2, LX/Anu;

    .line 1713100
    iget-object v0, p0, LX/Anw;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Anx;

    iget-object v2, p2, LX/Anu;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-boolean v3, p2, LX/Anu;->b:Z

    iget-boolean v4, p2, LX/Anu;->c:Z

    iget-boolean v5, p2, LX/Anu;->d:Z

    iget-object v6, p2, LX/Anu;->e:LX/1Wk;

    iget v7, p2, LX/Anu;->f:I

    iget-object v8, p2, LX/Anu;->g:LX/1Pr;

    iget v9, p2, LX/Anu;->h:I

    move-object v1, p1

    .line 1713101
    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v10

    const/4 v11, 0x2

    invoke-interface {v10, v11}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v10

    const v11, 0x7f0b00a6

    invoke-interface {v10, v11}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v10

    const/4 v11, 0x1

    invoke-interface {v10, v11}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v12

    .line 1713102
    iget-object v10, v0, LX/Anx;->b:LX/21T;

    invoke-virtual {v10, v3, v4, v5}, LX/21T;->a(ZZZ)LX/21Y;

    move-result-object v10

    .line 1713103
    invoke-virtual {v10, v7}, LX/21Y;->a(I)LX/21c;

    move-result-object v11

    .line 1713104
    invoke-static {v11}, LX/21c;->hasIcons(LX/21c;)Z

    move-result p0

    .line 1713105
    invoke-virtual {v10, v11}, LX/21Y;->a(LX/21c;)[F

    move-result-object p1

    .line 1713106
    const/4 v11, 0x0

    .line 1713107
    if-eqz v3, :cond_0

    .line 1713108
    iget-object v10, v0, LX/Anx;->a:LX/39c;

    const/4 v11, 0x0

    invoke-virtual {v10, v1, v11, v9}, LX/39c;->a(LX/1De;II)LX/39u;

    move-result-object v10

    invoke-virtual {v10, v2}, LX/39u;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/39u;

    move-result-object v10

    sget-object v11, LX/20X;->LIKE:LX/20X;

    invoke-virtual {v10, v11}, LX/39u;->a(LX/20X;)LX/39u;

    move-result-object v10

    invoke-virtual {v10, v6}, LX/39u;->a(LX/1Wk;)LX/39u;

    move-result-object v10

    invoke-virtual {v10, v8}, LX/39u;->a(LX/1Pr;)LX/39u;

    move-result-object v10

    invoke-virtual {v10, p0}, LX/39u;->a(Z)LX/39u;

    move-result-object v10

    invoke-virtual {v10}, LX/1X5;->c()LX/1Di;

    move-result-object v10

    .line 1713109
    const v11, -0x1a74372b

    const/4 p2, 0x0

    invoke-static {v1, v11, p2}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v11

    move-object v11, v11

    .line 1713110
    invoke-interface {v10, v11}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v10

    const/4 p2, 0x0

    const/4 v11, 0x1

    aget p2, p1, p2

    invoke-interface {v10, p2}, LX/1Di;->a(F)LX/1Di;

    move-result-object v10

    invoke-interface {v12, v10}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 1713111
    :cond_0
    if-eqz v4, :cond_2

    .line 1713112
    iget-object v10, v0, LX/Anx;->a:LX/39c;

    const/4 p2, 0x0

    invoke-virtual {v10, v1, p2, v9}, LX/39c;->a(LX/1De;II)LX/39u;

    move-result-object v10

    invoke-virtual {v10, v2}, LX/39u;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/39u;

    move-result-object v10

    sget-object p2, LX/20X;->COMMENT:LX/20X;

    invoke-virtual {v10, p2}, LX/39u;->a(LX/20X;)LX/39u;

    move-result-object v10

    invoke-virtual {v10, v6}, LX/39u;->a(LX/1Wk;)LX/39u;

    move-result-object v10

    invoke-virtual {v10, v8}, LX/39u;->a(LX/1Pr;)LX/39u;

    move-result-object v10

    invoke-virtual {v10, p0}, LX/39u;->a(Z)LX/39u;

    move-result-object v10

    invoke-virtual {v10}, LX/1X5;->c()LX/1Di;

    move-result-object v10

    .line 1713113
    const p2, -0x1a7434a0

    const/4 v3, 0x0

    invoke-static {v1, p2, v3}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p2

    move-object p2, p2

    .line 1713114
    invoke-interface {v10, p2}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object p2

    add-int/lit8 v10, v11, 0x1

    aget v11, p1, v11

    invoke-interface {p2, v11}, LX/1Di;->a(F)LX/1Di;

    move-result-object v11

    invoke-interface {v12, v11}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 1713115
    :goto_0
    if-eqz v5, :cond_1

    .line 1713116
    iget-object v11, v0, LX/Anx;->a:LX/39c;

    const/4 p2, 0x0

    invoke-virtual {v11, v1, p2, v9}, LX/39c;->a(LX/1De;II)LX/39u;

    move-result-object v11

    invoke-virtual {v11, v2}, LX/39u;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/39u;

    move-result-object v11

    sget-object p2, LX/20X;->SHARE:LX/20X;

    invoke-virtual {v11, p2}, LX/39u;->a(LX/20X;)LX/39u;

    move-result-object v11

    invoke-virtual {v11, v6}, LX/39u;->a(LX/1Wk;)LX/39u;

    move-result-object v11

    invoke-virtual {v11, v8}, LX/39u;->a(LX/1Pr;)LX/39u;

    move-result-object v11

    invoke-virtual {v11, p0}, LX/39u;->a(Z)LX/39u;

    move-result-object v11

    invoke-virtual {v11}, LX/1X5;->c()LX/1Di;

    move-result-object v11

    .line 1713117
    const p0, -0x1a743234

    const/4 p2, 0x0

    invoke-static {v1, p0, p2}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p0

    move-object p0, p0

    .line 1713118
    invoke-interface {v11, p0}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v11

    aget v10, p1, v10

    invoke-interface {v11, v10}, LX/1Di;->a(F)LX/1Di;

    move-result-object v10

    invoke-interface {v12, v10}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 1713119
    :cond_1
    invoke-interface {v12}, LX/1Di;->k()LX/1Dg;

    move-result-object v10

    move-object v0, v10

    .line 1713120
    return-object v0

    :cond_2
    move v10, v11

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1713077
    invoke-static {}, LX/1dS;->b()V

    .line 1713078
    iget v0, p1, LX/1dQ;->b:I

    .line 1713079
    sparse-switch v0, :sswitch_data_0

    .line 1713080
    :goto_0
    return-object v2

    .line 1713081
    :sswitch_0
    check-cast p2, LX/3Ae;

    .line 1713082
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1713083
    check-cast v1, LX/Anu;

    .line 1713084
    iget-object p1, p0, LX/Anw;->a:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object p1, v1, LX/Anu;->i:LX/20Z;

    .line 1713085
    sget-object p0, LX/20X;->LIKE:LX/20X;

    invoke-interface {p1, v0, p0}, LX/20Z;->a(Landroid/view/View;LX/20X;)V

    .line 1713086
    goto :goto_0

    .line 1713087
    :sswitch_1
    check-cast p2, LX/3Ae;

    .line 1713088
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1713089
    check-cast v1, LX/Anu;

    .line 1713090
    iget-object p1, p0, LX/Anw;->a:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object p1, v1, LX/Anu;->i:LX/20Z;

    .line 1713091
    sget-object p0, LX/20X;->COMMENT:LX/20X;

    invoke-interface {p1, v0, p0}, LX/20Z;->a(Landroid/view/View;LX/20X;)V

    .line 1713092
    goto :goto_0

    .line 1713093
    :sswitch_2
    check-cast p2, LX/3Ae;

    .line 1713094
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1713095
    check-cast v1, LX/Anu;

    .line 1713096
    iget-object p1, p0, LX/Anw;->a:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object p1, v1, LX/Anu;->i:LX/20Z;

    .line 1713097
    sget-object p0, LX/20X;->SHARE:LX/20X;

    invoke-interface {p1, v0, p0}, LX/20Z;->a(Landroid/view/View;LX/20X;)V

    .line 1713098
    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x1a74372b -> :sswitch_0
        -0x1a7434a0 -> :sswitch_1
        -0x1a743234 -> :sswitch_2
    .end sparse-switch
.end method

.method public final c(LX/1De;)LX/Anv;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/Anw",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1713069
    new-instance v1, LX/Anu;

    invoke-direct {v1, p0}, LX/Anu;-><init>(LX/Anw;)V

    .line 1713070
    iget-object v2, p0, LX/Anw;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Anv;

    .line 1713071
    if-nez v2, :cond_0

    .line 1713072
    new-instance v2, LX/Anv;

    invoke-direct {v2, p0}, LX/Anv;-><init>(LX/Anw;)V

    .line 1713073
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/Anv;->a$redex0(LX/Anv;LX/1De;IILX/Anu;)V

    .line 1713074
    move-object v1, v2

    .line 1713075
    move-object v0, v1

    .line 1713076
    return-object v0
.end method
