.class public LX/BJR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:LX/0aG;

.field public final c:LX/0SI;

.field public final d:Landroid/content/res/Resources;

.field public final e:Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentViewBinder;

.field private final f:LX/03V;

.field public final g:LX/BJx;

.field public final h:LX/BK8;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1771900
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->FALLBACK:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/BJR;->a:LX/0Px;

    return-void
.end method

.method public constructor <init>(LX/0aG;LX/0SI;Landroid/content/res/Resources;Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentViewBinder;LX/03V;LX/BJx;LX/BK8;)V
    .locals 0
    .param p6    # LX/BJx;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # LX/BK8;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1771901
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1771902
    iput-object p1, p0, LX/BJR;->b:LX/0aG;

    .line 1771903
    iput-object p2, p0, LX/BJR;->c:LX/0SI;

    .line 1771904
    iput-object p3, p0, LX/BJR;->d:Landroid/content/res/Resources;

    .line 1771905
    iput-object p4, p0, LX/BJR;->e:Lcom/facebook/platform/composer/composer/PlatformComposerAttachmentViewBinder;

    .line 1771906
    iput-object p5, p0, LX/BJR;->f:LX/03V;

    .line 1771907
    iput-object p6, p0, LX/BJR;->g:LX/BJx;

    .line 1771908
    iput-object p7, p0, LX/BJR;->h:LX/BK8;

    .line 1771909
    return-void
.end method

.method public static a(LX/BJR;Landroid/view/View;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    .line 1771910
    const/4 v4, -0x2

    .line 1771911
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 1771912
    invoke-virtual {v0, p1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 1771913
    new-instance v1, Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/facebook/fbui/glyph/GlyphView;-><init>(Landroid/content/Context;)V

    .line 1771914
    iget-object v2, p0, LX/BJR;->d:Landroid/content/res/Resources;

    const v3, 0x7f020818

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1771915
    iget-object v2, p0, LX/BJR;->d:Landroid/content/res/Resources;

    const v3, 0x7f0a073e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 1771916
    new-instance v2, LX/BJO;

    invoke-direct {v2, p0, v0}, LX/BJO;-><init>(LX/BJR;Landroid/widget/FrameLayout;)V

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1771917
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    const/16 v3, 0x15

    invoke-direct {v2, v4, v4, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    .line 1771918
    invoke-virtual {v0, v1, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1771919
    move-object v0, v0

    .line 1771920
    new-instance v1, Landroid/widget/LinearLayout;

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 1771921
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 1771922
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v3, -0x1

    const/4 v4, -0x2

    invoke-direct {v2, v3, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1771923
    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1771924
    invoke-virtual {v1, p2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1771925
    return-object v1
.end method

.method public static a(LX/BJR;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1771926
    iget-object v0, p0, LX/BJR;->g:LX/BJx;

    invoke-virtual {v0}, LX/BJx;->c()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->quoteText:Ljava/lang/String;

    .line 1771927
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f0310e6

    const/4 p0, 0x0

    invoke-virtual {v1, v2, p1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 1771928
    const v1, 0x7f0d281f

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/rows/sections/attachments/ui/quoteshare/QuoteExpandingEllipsizingTextView;

    .line 1771929
    invoke-virtual {v1, v0}, Lcom/facebook/feed/rows/sections/attachments/ui/quoteshare/QuoteExpandingEllipsizingTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1771930
    move-object v0, v2

    .line 1771931
    return-object v0
.end method

.method public static a$redex0(LX/BJR;Lcom/facebook/ipc/composer/intent/SharePreview;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;
    .locals 3
    .param p0    # LX/BJR;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1771932
    if-nez p1, :cond_0

    .line 1771933
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "sharePreview is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, LX/BJR;->a(Ljava/lang/Throwable;)LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1771934
    :goto_0
    return-object v0

    .line 1771935
    :cond_0
    iget-object v0, p0, LX/BJR;->g:LX/BJx;

    invoke-virtual {v0}, LX/BJx;->d()Lcom/facebook/share/model/ComposerAppAttribution;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/BJR;->g:LX/BJx;

    invoke-virtual {v0}, LX/BJx;->d()Lcom/facebook/share/model/ComposerAppAttribution;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/share/model/ComposerAppAttribution;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, LX/BJR;->g:LX/BJx;

    invoke-virtual {v0}, LX/BJx;->d()Lcom/facebook/share/model/ComposerAppAttribution;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/share/model/ComposerAppAttribution;->b()Ljava/lang/String;

    move-result-object v0

    .line 1771936
    :goto_1
    new-instance v1, LX/BJQ;

    invoke-direct {v1}, LX/BJQ;-><init>()V

    iget-object v2, p1, Lcom/facebook/ipc/composer/intent/SharePreview;->title:Ljava/lang/String;

    .line 1771937
    iget-object p0, v1, LX/BJQ;->a:LX/39x;

    .line 1771938
    iput-object v2, p0, LX/39x;->t:Ljava/lang/String;

    .line 1771939
    move-object v1, v1

    .line 1771940
    iget-object v2, p1, Lcom/facebook/ipc/composer/intent/SharePreview;->subTitle:Ljava/lang/String;

    .line 1771941
    iget-object p0, v1, LX/BJQ;->a:LX/39x;

    .line 1771942
    iput-object v2, p0, LX/39x;->r:Ljava/lang/String;

    .line 1771943
    move-object v1, v1

    .line 1771944
    invoke-virtual {v1, v0}, LX/BJQ;->c(Ljava/lang/String;)LX/BJQ;

    move-result-object v0

    iget-object v1, p1, Lcom/facebook/ipc/composer/intent/SharePreview;->imageUrl:Ljava/lang/String;

    .line 1771945
    iget-object v2, v0, LX/BJQ;->a:LX/39x;

    new-instance p0, LX/4XR;

    invoke-direct {p0}, LX/4XR;-><init>()V

    new-instance p1, LX/2dc;

    invoke-direct {p1}, LX/2dc;-><init>()V

    .line 1771946
    iput-object v1, p1, LX/2dc;->h:Ljava/lang/String;

    .line 1771947
    move-object p1, p1

    .line 1771948
    invoke-virtual {p1}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object p1

    .line 1771949
    iput-object p1, p0, LX/4XR;->kR:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1771950
    move-object p0, p0

    .line 1771951
    invoke-virtual {p0}, LX/4XR;->a()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object p0

    .line 1771952
    iput-object p0, v2, LX/39x;->s:Lcom/facebook/graphql/model/GraphQLNode;

    .line 1771953
    move-object v0, v0

    .line 1771954
    invoke-virtual {v0}, LX/BJQ;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    goto :goto_0

    .line 1771955
    :cond_1
    iget-object v0, p1, Lcom/facebook/ipc/composer/intent/SharePreview;->summary:Ljava/lang/String;

    goto :goto_1
.end method

.method public static g(LX/BJR;)Z
    .locals 1

    .prologue
    .line 1771956
    iget-object v0, p0, LX/BJR;->g:LX/BJx;

    invoke-virtual {v0}, LX/BJx;->c()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->quoteText:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/Throwable;)LX/0am;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Throwable;",
            ")",
            "LX/0am",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1771957
    iget-object v0, p0, LX/BJR;->f:LX/03V;

    const-string v1, "composer_feed_attachment_error_fallback"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "sessionId: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/BJR;->g:LX/BJx;

    invoke-virtual {v3}, LX/BJx;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v1

    .line 1771958
    iput-object p1, v1, LX/0VK;->c:Ljava/lang/Throwable;

    .line 1771959
    move-object v1, v1

    .line 1771960
    const/4 v2, 0x1

    .line 1771961
    iput v2, v1, LX/0VK;->e:I

    .line 1771962
    move-object v1, v1

    .line 1771963
    invoke-virtual {v1}, LX/0VK;->g()LX/0VG;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/03V;->a(LX/0VG;)V

    .line 1771964
    new-instance v0, LX/BJQ;

    invoke-direct {v0}, LX/BJQ;-><init>()V

    iget-object v1, p0, LX/BJR;->d:Landroid/content/res/Resources;

    const v2, 0x7f082457

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/BJQ;->c(Ljava/lang/String;)LX/BJQ;

    move-result-object v0

    invoke-virtual {v0}, LX/BJQ;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    return-object v0
.end method
