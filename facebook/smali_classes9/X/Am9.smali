.class public LX/Am9;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1710500
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0g5;LX/0fz;LX/1UQ;)LX/162;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1710501
    invoke-static {p0, p1, p2}, LX/Am9;->b(LX/0g5;LX/0fz;LX/1UQ;)I

    move-result v0

    .line 1710502
    if-ltz v0, :cond_0

    invoke-virtual {p1}, LX/0fz;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1710503
    invoke-virtual {p1, v0}, LX/0fz;->b(I)Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-result-object v0

    .line 1710504
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1710505
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    invoke-static {v0}, LX/1fz;->a(Lcom/facebook/graphql/model/FeedUnit;)LX/162;

    move-result-object v0

    .line 1710506
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/0g5;LX/0fz;LX/1UQ;)I
    .locals 3

    .prologue
    const/4 v0, -0x1

    .line 1710507
    if-eqz p1, :cond_0

    invoke-interface {p2}, LX/1Qr;->d()I

    move-result v1

    if-nez v1, :cond_1

    .line 1710508
    :cond_0
    :goto_0
    return v0

    .line 1710509
    :cond_1
    invoke-virtual {p0}, LX/0g7;->q()I

    move-result v1

    .line 1710510
    invoke-virtual {p2}, LX/1UQ;->b()I

    move-result v2

    if-lt v1, v2, :cond_0

    invoke-virtual {p2}, LX/1UQ;->c()I

    move-result v2

    if-gt v1, v2, :cond_0

    .line 1710511
    invoke-interface {p2, v1}, LX/1Qr;->h_(I)I

    move-result v0

    goto :goto_0
.end method
