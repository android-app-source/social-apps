.class public final LX/CTL;
.super LX/CTK;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final a:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;LX/CSS;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1895695
    invoke-direct {p0, p1, p2, p3}, LX/CTK;-><init>(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;LX/CSS;Ljava/lang/String;)V

    .line 1895696
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/CTL;->a:Ljava/util/HashMap;

    .line 1895697
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;->ad()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$DefaultValueModel;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 1895698
    :goto_0
    if-eqz v0, :cond_0

    .line 1895699
    iget-object v0, p0, LX/CTL;->a:Ljava/util/HashMap;

    const-string v1, "address1"

    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;->ad()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$DefaultValueModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$DefaultValueModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/CXh;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1895700
    iget-object v0, p0, LX/CTL;->a:Ljava/util/HashMap;

    const-string v1, "address2"

    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;->ad()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$DefaultValueModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$DefaultValueModel;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/CXh;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1895701
    iget-object v0, p0, LX/CTL;->a:Ljava/util/HashMap;

    const-string v1, "city"

    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;->ad()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$DefaultValueModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$DefaultValueModel;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/CXh;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1895702
    iget-object v0, p0, LX/CTL;->a:Ljava/util/HashMap;

    const-string v1, "state"

    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;->ad()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$DefaultValueModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$DefaultValueModel;->m()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/CXh;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1895703
    iget-object v0, p0, LX/CTL;->a:Ljava/util/HashMap;

    const-string v1, "zipcode"

    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;->ad()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$DefaultValueModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$DefaultValueModel;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/CXh;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1895704
    :cond_0
    return-void

    .line 1895705
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
