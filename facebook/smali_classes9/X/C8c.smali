.class public LX/C8c;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2yN;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/2yN",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/17V;

.field private final b:LX/2mt;

.field private final c:LX/C8b;


# direct methods
.method public constructor <init>(LX/17V;LX/2mt;LX/C8b;)V
    .locals 0
    .param p3    # LX/C8b;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1853125
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1853126
    iput-object p1, p0, LX/C8c;->a:LX/17V;

    .line 1853127
    iput-object p2, p0, LX/C8c;->b:LX/2mt;

    .line 1853128
    iput-object p3, p0, LX/C8c;->c:LX/C8b;

    .line 1853129
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Landroid/view/View;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 7
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1853130
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v3, 0x0

    .line 1853131
    const v0, 0x7f0d0083

    invoke-virtual {p2, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 1853132
    iget-object v0, p0, LX/C8c;->b:LX/2mt;

    invoke-static {p1, v0}, LX/C8a;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/2mt;)Ljava/lang/String;

    move-result-object v1

    .line 1853133
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1853134
    :goto_0
    return-object v3

    .line 1853135
    :cond_0
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v0

    .line 1853136
    if-eqz v0, :cond_1

    invoke-static {v0}, LX/181;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v3

    .line 1853137
    :cond_1
    iget-object v0, p0, LX/C8c;->c:LX/C8b;

    sget-object v2, LX/C8b;->ATTACHMENT:LX/C8b;

    if-ne v0, v2, :cond_2

    .line 1853138
    invoke-static {p1}, LX/2mt;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v2

    const-string v4, "native_newsfeed"

    .line 1853139
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v6, v0

    .line 1853140
    check-cast v6, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v6}, LX/1VO;->z(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;

    move-result-object v6

    invoke-static/range {v1 .. v6}, LX/17V;->b(Ljava/lang/String;ZLX/0lF;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    goto :goto_0

    .line 1853141
    :cond_2
    invoke-static {p1}, LX/2mt;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v2

    const-string v4, "native_newsfeed"

    .line 1853142
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v6, v0

    .line 1853143
    check-cast v6, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v6}, LX/1VO;->z(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Ljava/lang/String;

    move-result-object v6

    invoke-static/range {v1 .. v6}, LX/17V;->a(Ljava/lang/String;ZLX/0lF;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    goto :goto_0
.end method
