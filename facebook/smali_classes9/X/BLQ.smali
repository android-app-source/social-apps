.class public final LX/BLQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;

.field public b:Z


# direct methods
.method public constructor <init>(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;)V
    .locals 0

    .prologue
    .line 1775474
    iput-object p1, p0, LX/BLQ;->a:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 3

    .prologue
    .line 1775475
    iget-object v0, p0, LX/BLQ;->a:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;

    iget-object v0, v0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->i:LX/8tB;

    invoke-virtual {v0}, LX/3Tf;->a()LX/333;

    move-result-object v0

    iget-object v1, p0, LX/BLQ;->a:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;

    iget-object v1, v1, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getUserEnteredPlainText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v0, v1}, LX/333;->a(Ljava/lang/CharSequence;)V

    .line 1775476
    iget-object v0, p0, LX/BLQ;->a:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;

    iget-object v0, v0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->z:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    .line 1775477
    :goto_0
    iget-object v1, p0, LX/BLQ;->a:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;

    iget-object v2, p0, LX/BLQ;->a:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;

    iget-object v2, v2, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-static {v2}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->b(Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;)Ljava/util/List;

    move-result-object v2

    .line 1775478
    iput-object v2, v1, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->z:Ljava/util/List;

    .line 1775479
    if-nez v0, :cond_0

    iget-object v0, p0, LX/BLQ;->a:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;

    iget-object v0, v0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->z:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1775480
    iget-object v0, p0, LX/BLQ;->a:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;

    invoke-static {v0}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->s(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;)V

    .line 1775481
    :cond_0
    iget-object v0, p0, LX/BLQ;->a:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;

    iget-object v0, v0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->z:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    iget-object v0, p0, LX/BLQ;->a:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;

    iget-object v0, v0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->z:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8QL;

    iget-object v0, v0, LX/8QL;->a:LX/8vA;

    sget-object v1, LX/8vA;->TAG_EXPANSION:LX/8vA;

    if-ne v0, v1, :cond_1

    .line 1775482
    iget-object v0, p0, LX/BLQ;->a:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;

    invoke-static {v0}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->m(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;)V

    .line 1775483
    iget-object v0, p0, LX/BLQ;->a:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;

    iget-object v0, v0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->i:LX/8tB;

    sget v1, LX/BLY;->a:I

    invoke-virtual {v0, v1}, LX/8tB;->g(I)LX/621;

    move-result-object v0

    check-cast v0, LX/8Rn;

    .line 1775484
    invoke-virtual {v0}, LX/8Rn;->j()V

    .line 1775485
    :cond_1
    iget-object v0, p0, LX/BLQ;->a:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;

    iget-object v0, v0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->i:LX/8tB;

    iget-object v1, p0, LX/BLQ;->a:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;

    iget-object v1, v1, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->z:Ljava/util/List;

    .line 1775486
    iput-object v1, v0, LX/8tB;->i:Ljava/util/List;

    .line 1775487
    iget-object v0, p0, LX/BLQ;->a:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;

    iget-object v1, p0, LX/BLQ;->a:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;

    iget-object v1, v1, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->z:Ljava/util/List;

    invoke-static {v0, v1}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->c$redex0(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;Ljava/util/List;)V

    .line 1775488
    return-void

    .line 1775489
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 1

    .prologue
    .line 1775490
    if-ge p4, p3, :cond_0

    .line 1775491
    iget-object v0, p0, LX/BLQ;->a:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;

    invoke-static {v0}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->o(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;)LX/8QY;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1775492
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/BLQ;->b:Z

    .line 1775493
    :cond_0
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 1

    .prologue
    .line 1775494
    if-ge p4, p3, :cond_1

    .line 1775495
    iget-object v0, p0, LX/BLQ;->a:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;

    iget-object v0, v0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->d()V

    .line 1775496
    iget-object v0, p0, LX/BLQ;->a:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;

    iget-object v0, v0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->z:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/BLQ;->b:Z

    if-nez v0, :cond_2

    .line 1775497
    :cond_0
    :goto_0
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/BLQ;->b:Z

    .line 1775498
    :cond_1
    return-void

    .line 1775499
    :cond_2
    iget-object v0, p0, LX/BLQ;->a:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;

    invoke-static {v0}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->o(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;)LX/8QY;

    move-result-object v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    .line 1775500
    :goto_1
    if-nez v0, :cond_0

    .line 1775501
    iget-object v0, p0, LX/BLQ;->a:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;

    invoke-static {v0}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->e$redex0(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;)V

    goto :goto_0

    .line 1775502
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method
