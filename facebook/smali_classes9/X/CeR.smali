.class public final LX/CeR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Vj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Vj",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/placetips/settings/graphql/GravitySettingsMutationModels$GravitySettingsMutationModel;",
        ">;",
        "Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentInterfaces$GravitySettingsGraphQlFragment;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/CeT;


# direct methods
.method public constructor <init>(LX/CeT;)V
    .locals 0

    .prologue
    .line 1924640
    iput-object p1, p0, LX/CeR;->a:LX/CeT;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1

    .prologue
    .line 1924641
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1924642
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1924643
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1924644
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1924645
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1924646
    check-cast v0, Lcom/facebook/placetips/settings/graphql/GravitySettingsMutationModels$GravitySettingsMutationModel;

    invoke-virtual {v0}, Lcom/facebook/placetips/settings/graphql/GravitySettingsMutationModels$GravitySettingsMutationModel;->a()Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1924647
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1924648
    check-cast v0, Lcom/facebook/placetips/settings/graphql/GravitySettingsMutationModels$GravitySettingsMutationModel;

    invoke-virtual {v0}, Lcom/facebook/placetips/settings/graphql/GravitySettingsMutationModels$GravitySettingsMutationModel;->a()Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;

    move-result-object v0

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
