.class public final LX/Anv;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/Anw;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/Anu;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/Anw",
            "<TE;>.BasicFooterButtonsComponentImpl;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/Anw;

.field private c:[Ljava/lang/String;

.field private d:I

.field private e:Ljava/util/BitSet;


# direct methods
.method public constructor <init>(LX/Anw;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    .line 1713062
    iput-object p1, p0, LX/Anv;->b:LX/Anw;

    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 1713063
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "storyProps"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "canLike"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "canComment"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "canShare"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "downstateType"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "ufiWidthPx"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "environment"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "buttonClickedListener"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/Anv;->c:[Ljava/lang/String;

    .line 1713064
    iput v3, p0, LX/Anv;->d:I

    .line 1713065
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/Anv;->d:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/Anv;->e:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/Anv;LX/1De;IILX/Anu;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "II",
            "LX/Anw",
            "<TE;>.BasicFooterButtonsComponentImpl;)V"
        }
    .end annotation

    .prologue
    .line 1713058
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 1713059
    iput-object p4, p0, LX/Anv;->a:LX/Anu;

    .line 1713060
    iget-object v0, p0, LX/Anv;->e:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 1713061
    return-void
.end method


# virtual methods
.method public final a(LX/1Pr;)LX/Anv;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)",
            "LX/Anw",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 1713055
    iget-object v0, p0, LX/Anv;->a:LX/Anu;

    iput-object p1, v0, LX/Anu;->g:LX/1Pr;

    .line 1713056
    iget-object v0, p0, LX/Anv;->e:Ljava/util/BitSet;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1713057
    return-object p0
.end method

.method public final a(LX/1Wk;)LX/Anv;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Wk;",
            ")",
            "LX/Anw",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 1713052
    iget-object v0, p0, LX/Anv;->a:LX/Anu;

    iput-object p1, v0, LX/Anu;->e:LX/1Wk;

    .line 1713053
    iget-object v0, p0, LX/Anv;->e:Ljava/util/BitSet;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1713054
    return-object p0
.end method

.method public final a(LX/20Z;)LX/Anv;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/20Z;",
            ")",
            "LX/Anw",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 1713023
    iget-object v0, p0, LX/Anv;->a:LX/Anu;

    iput-object p1, v0, LX/Anu;->i:LX/20Z;

    .line 1713024
    iget-object v0, p0, LX/Anv;->e:Ljava/util/BitSet;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1713025
    return-object p0
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/Anv;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "LX/Anw",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 1713049
    iget-object v0, p0, LX/Anv;->a:LX/Anu;

    iput-object p1, v0, LX/Anu;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1713050
    iget-object v0, p0, LX/Anv;->e:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1713051
    return-object p0
.end method

.method public final a(Z)LX/Anv;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "LX/Anw",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 1713066
    iget-object v0, p0, LX/Anv;->a:LX/Anu;

    iput-boolean p1, v0, LX/Anu;->b:Z

    .line 1713067
    iget-object v0, p0, LX/Anv;->e:Ljava/util/BitSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1713068
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 1713045
    invoke-super {p0}, LX/1X5;->a()V

    .line 1713046
    const/4 v0, 0x0

    iput-object v0, p0, LX/Anv;->a:LX/Anu;

    .line 1713047
    iget-object v0, p0, LX/Anv;->b:LX/Anw;

    iget-object v0, v0, LX/Anw;->b:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 1713048
    return-void
.end method

.method public final b(Z)LX/Anv;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "LX/Anw",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 1713042
    iget-object v0, p0, LX/Anv;->a:LX/Anu;

    iput-boolean p1, v0, LX/Anu;->c:Z

    .line 1713043
    iget-object v0, p0, LX/Anv;->e:Ljava/util/BitSet;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1713044
    return-object p0
.end method

.method public final c(Z)LX/Anv;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "LX/Anw",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 1713039
    iget-object v0, p0, LX/Anv;->a:LX/Anu;

    iput-boolean p1, v0, LX/Anu;->d:Z

    .line 1713040
    iget-object v0, p0, LX/Anv;->e:Ljava/util/BitSet;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1713041
    return-object p0
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/Anw;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1713029
    iget-object v1, p0, LX/Anv;->e:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/Anv;->e:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/Anv;->d:I

    if-ge v1, v2, :cond_2

    .line 1713030
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1713031
    :goto_0
    iget v2, p0, LX/Anv;->d:I

    if-ge v0, v2, :cond_1

    .line 1713032
    iget-object v2, p0, LX/Anv;->e:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1713033
    iget-object v2, p0, LX/Anv;->c:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1713034
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1713035
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1713036
    :cond_2
    iget-object v0, p0, LX/Anv;->a:LX/Anu;

    .line 1713037
    invoke-virtual {p0}, LX/Anv;->a()V

    .line 1713038
    return-object v0
.end method

.method public final h(I)LX/Anv;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/Anw",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 1713026
    iget-object v0, p0, LX/Anv;->a:LX/Anu;

    iput p1, v0, LX/Anu;->f:I

    .line 1713027
    iget-object v0, p0, LX/Anv;->e:Ljava/util/BitSet;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1713028
    return-object p0
.end method
