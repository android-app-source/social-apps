.class public LX/CcY;
.super LX/16T;
.source ""

# interfaces
.implements LX/16E;


# instance fields
.field private final a:LX/0y3;

.field public b:LX/5Od;

.field public c:Landroid/view/View;


# direct methods
.method public constructor <init>(LX/0y3;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1921330
    invoke-direct {p0}, LX/16T;-><init>()V

    .line 1921331
    iput-object p1, p0, LX/CcY;->a:LX/0y3;

    .line 1921332
    return-void
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 1921329
    const-wide/32 v0, 0x5265c00

    return-wide v0
.end method

.method public final a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/10S;
    .locals 2

    .prologue
    .line 1921326
    iget-object v0, p0, LX/CcY;->a:LX/0y3;

    invoke-virtual {v0}, LX/0y3;->b()LX/1rv;

    move-result-object v0

    iget-object v0, v0, LX/1rv;->a:LX/0yG;

    sget-object v1, LX/0yG;->OKAY:LX/0yG;

    if-ne v0, v1, :cond_0

    .line 1921327
    sget-object v0, LX/10S;->ELIGIBLE:LX/10S;

    .line 1921328
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/10S;->INELIGIBLE:LX/10S;

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1921317
    iget-object v0, p0, LX/CcY;->c:Landroid/view/View;

    if-nez v0, :cond_0

    .line 1921318
    :goto_0
    return-void

    .line 1921319
    :cond_0
    new-instance v0, LX/0hs;

    iget-object v1, p0, LX/CcY;->c:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, LX/0hs;-><init>(Landroid/content/Context;I)V

    .line 1921320
    const/4 v1, -0x1

    .line 1921321
    iput v1, v0, LX/0hs;->t:I

    .line 1921322
    const v1, 0x7f0823c9

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0hs;->b(Ljava/lang/CharSequence;)V

    .line 1921323
    sget-object v1, LX/3AV;->BELOW:LX/3AV;

    invoke-virtual {v0, v1}, LX/0ht;->a(LX/3AV;)V

    .line 1921324
    iget-object v1, p0, LX/CcY;->b:LX/5Od;

    invoke-virtual {v0, v1}, LX/0hs;->a(LX/5Od;)V

    .line 1921325
    iget-object v1, p0, LX/CcY;->c:Landroid/view/View;

    invoke-virtual {v0, v1}, LX/0ht;->a(Landroid/view/View;)V

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1921316
    const-string v0, "4003"

    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1921315
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->MEDIA_GALLERY_OPENED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
