.class public final LX/ATg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "LX/0Px",
        "<",
        "Lcom/facebook/photos/creativeediting/model/FrameGraphQLInterfaces$FramePack;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/9dV;

.field public final synthetic b:LX/0Px;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

.field public e:LX/9dY;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;LX/9dV;LX/0Px;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1676280
    iput-object p1, p0, LX/ATg;->d:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iput-object p2, p0, LX/ATg;->a:LX/9dV;

    iput-object p3, p0, LX/ATg;->b:LX/0Px;

    iput-object p4, p0, LX/ATg;->c:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1676281
    sget-object v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->L:Ljava/lang/String;

    const-string v1, "Failed to load swipable frames"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1676282
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 10
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1676283
    check-cast p1, LX/0Px;

    const/4 v1, 0x0

    .line 1676284
    if-eqz p1, :cond_0

    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/ATg;->d:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v0, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ap:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9d5;

    invoke-virtual {v0}, LX/9d5;->n()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1676285
    :cond_0
    return-void

    .line 1676286
    :cond_1
    iget-object v0, p0, LX/ATg;->a:LX/9dV;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/ATg;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/ATg;->d:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-boolean v0, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aM:Z

    if-nez v0, :cond_2

    .line 1676287
    iget-object v0, p0, LX/ATg;->d:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v2, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->u:LX/9dZ;

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;

    .line 1676288
    new-instance v4, LX/9dY;

    const/16 v3, 0x2e36

    invoke-static {v2, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static {v2}, LX/8G7;->b(LX/0QB;)LX/8G7;

    move-result-object v3

    check-cast v3, LX/8G7;

    invoke-direct {v4, v0, v5, v3}, LX/9dY;-><init>(Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;LX/0Ot;LX/8G7;)V

    .line 1676289
    move-object v0, v4

    .line 1676290
    iput-object v0, p0, LX/ATg;->e:LX/9dY;

    .line 1676291
    iget-object v0, p0, LX/ATg;->d:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v0, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ap:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9d5;

    iget-object v2, p0, LX/ATg;->a:LX/9dV;

    iget-object v3, p0, LX/ATg;->e:LX/9dY;

    invoke-static {v2, v3}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/9d5;->a(LX/0Px;)V

    .line 1676292
    :cond_2
    iget-object v0, p0, LX/ATg;->d:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v5, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aL:LX/0Px;

    .line 1676293
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1676294
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1676295
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v7

    move v6, v1

    :goto_0
    if-ge v6, v7, :cond_0

    invoke-virtual {p1, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;

    .line 1676296
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1676297
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1676298
    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;->e()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1676299
    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;->c()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/8G8;->a(LX/0Px;)LX/0Px;

    move-result-object v8

    .line 1676300
    iget-object v0, p0, LX/ATg;->d:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v9, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->B:Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;

    new-instance v0, LX/ATf;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, LX/ATf;-><init>(LX/ATg;Ljava/util/List;Ljava/util/List;Ljava/util/List;LX/0Px;)V

    invoke-virtual {v9, v8, v0}, Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;->a(LX/0Px;LX/8G6;)V

    .line 1676301
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0
.end method
