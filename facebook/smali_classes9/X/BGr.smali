.class public final LX/BGr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/74q;


# instance fields
.field public final synthetic a:Lcom/facebook/photos/simplepicker/SimplePickerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/simplepicker/SimplePickerFragment;)V
    .locals 0

    .prologue
    .line 1767530
    iput-object p1, p0, LX/BGr;->a:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 1767531
    return-void
.end method

.method public final a(Lcom/facebook/ipc/media/MediaItem;LX/74p;)V
    .locals 3

    .prologue
    .line 1767532
    sget-object v0, LX/74p;->SELECT:LX/74p;

    if-ne p2, v0, :cond_0

    iget-object v0, p0, LX/BGr;->a:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    invoke-static {v0}, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->z(Lcom/facebook/photos/simplepicker/SimplePickerFragment;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    iget-object v0, p0, LX/BGr;->a:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    const/4 v1, 0x0

    .line 1767533
    iget-object v2, v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->b:Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    if-eqz v2, :cond_1

    iget-object v2, v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->b:Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    .line 1767534
    iget-object p1, v2, Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;->d:Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;

    move-object v2, p1

    .line 1767535
    if-nez v2, :cond_4

    .line 1767536
    :cond_1
    :goto_0
    move v0, v1

    .line 1767537
    if-eqz v0, :cond_3

    .line 1767538
    :cond_2
    iget-object v0, p0, LX/BGr;->a:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    invoke-virtual {v0}, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->e()V

    .line 1767539
    :cond_3
    return-void

    .line 1767540
    :cond_4
    iget-object v2, v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->b:Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    .line 1767541
    iget-object p1, v2, Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;->d:Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;

    move-object v2, p1

    .line 1767542
    iget-object p1, v2, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->e:LX/8AB;

    move-object v2, p1

    .line 1767543
    iget-object p1, v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->av:LX/0ad;

    sget-short p2, LX/1EB;->ao:S

    invoke-interface {p1, p2, v1}, LX/0ad;->a(SZ)Z

    move-result p1

    .line 1767544
    if-eqz p1, :cond_1

    sget-object p1, LX/8AB;->FEED:LX/8AB;

    if-eq v2, p1, :cond_5

    sget-object p1, LX/8AB;->TIMELINE:LX/8AB;

    if-eq v2, p1, :cond_5

    sget-object p1, LX/8AB;->GROUP:LX/8AB;

    if-eq v2, p1, :cond_5

    sget-object p1, LX/8AB;->PAGE:LX/8AB;

    if-ne v2, p1, :cond_1

    :cond_5
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 1767545
    return-void
.end method
