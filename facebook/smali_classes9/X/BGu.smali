.class public final LX/BGu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:LX/BGl;

.field public final synthetic b:Lcom/facebook/photos/simplepicker/SimplePickerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/simplepicker/SimplePickerFragment;LX/BGl;)V
    .locals 0

    .prologue
    .line 1767558
    iput-object p1, p0, LX/BGu;->b:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    iput-object p2, p0, LX/BGu;->a:LX/BGl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 1767559
    iget-object v0, p0, LX/BGu;->a:LX/BGl;

    .line 1767560
    iget-object v1, v0, LX/BGl;->a:LX/BGm;

    iget-object v1, v1, LX/BGm;->b:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    iget-object v1, v1, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->z:LX/BGe;

    iget-object v2, v0, LX/BGl;->a:LX/BGm;

    iget-object v2, v2, LX/BGm;->a:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    .line 1767561
    sget-object p0, LX/BGd;->PICKER_MOTION_PHOTOS_SHARE_AS_VIDEOS:LX/BGd;

    invoke-static {p0}, LX/BGe;->a(LX/BGd;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    const-string p1, "photo_selected_count"

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    invoke-static {v1, p0}, LX/BGe;->a(LX/BGe;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1767562
    iget-object v1, v0, LX/BGl;->a:LX/BGm;

    iget-object v1, v1, LX/BGm;->b:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    iget-object v1, v1, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->x:LX/BHA;

    if-eqz v1, :cond_0

    .line 1767563
    iget-object v1, v0, LX/BGl;->a:LX/BGm;

    iget-object v1, v1, LX/BGm;->b:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    iget-object v2, v0, LX/BGl;->a:LX/BGm;

    iget-object v2, v2, LX/BGm;->a:LX/0Px;

    .line 1767564
    new-instance p0, LX/4BY;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-direct {p0, p1}, LX/4BY;-><init>(Landroid/content/Context;)V

    .line 1767565
    const/4 p1, 0x0

    .line 1767566
    iput p1, p0, LX/4BY;->d:I

    .line 1767567
    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const p2, 0x7f081391

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, LX/2EJ;->a(Ljava/lang/CharSequence;)V

    .line 1767568
    invoke-virtual {p0}, LX/4BY;->show()V

    .line 1767569
    iget-object p1, v1, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->aB:Ljava/util/concurrent/ExecutorService;

    new-instance p2, Lcom/facebook/photos/simplepicker/SimplePickerFragment$17;

    invoke-direct {p2, v1, v2, p0}, Lcom/facebook/photos/simplepicker/SimplePickerFragment$17;-><init>(Lcom/facebook/photos/simplepicker/SimplePickerFragment;LX/0Px;LX/4BY;)V

    const p0, -0x5ed17435

    invoke-static {p1, p2, p0}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1767570
    :cond_0
    return-void
.end method
