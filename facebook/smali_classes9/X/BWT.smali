.class public LX/BWT;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public static final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/BWQ;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    .line 1792051
    const-class v0, LX/BWT;

    sput-object v0, LX/BWT;->a:Ljava/lang/Class;

    .line 1792052
    const/16 v0, 0xb

    new-array v0, v0, [LX/BWQ;

    const/4 v1, 0x0

    const-string v2, "findfriends/browse.*"

    invoke-static {v2}, LX/BWR;->b(Ljava/lang/String;)LX/BWQ;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "friends/center.*"

    invoke-static {v2}, LX/BWR;->b(Ljava/lang/String;)LX/BWQ;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "home.php.*"

    invoke-static {v2}, LX/BWR;->b(Ljava/lang/String;)LX/BWQ;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "profile.php.*"

    invoke-static {v2}, LX/BWR;->b(Ljava/lang/String;)LX/BWQ;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "about/graphsearch.*"

    invoke-static {v2}, LX/BWR;->b(Ljava/lang/String;)LX/BWQ;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "map"

    .line 1792053
    new-instance v3, LX/BWS;

    invoke-direct {v3, v2}, LX/BWS;-><init>(Ljava/lang/String;)V

    move-object v2, v3

    .line 1792054
    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "events/\\d+.*"

    invoke-static {v2}, LX/BWR;->b(Ljava/lang/String;)LX/BWQ;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "saved/.*"

    invoke-static {v2}, LX/BWR;->b(Ljava/lang/String;)LX/BWQ;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "profile/edit/questions/.*"

    invoke-static {v2}, LX/BWR;->b(Ljava/lang/String;)LX/BWQ;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "appcenter.*"

    invoke-static {v2}, LX/BWR;->b(Ljava/lang/String;)LX/BWQ;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "travel/slideshow.*"

    invoke-static {v2}, LX/BWR;->b(Ljava/lang/String;)LX/BWQ;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0R9;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    sput-object v0, LX/BWT;->b:Ljava/util/List;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1792055
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1792056
    return-void
.end method
