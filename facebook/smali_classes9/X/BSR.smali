.class public final enum LX/BSR;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/BSR;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/BSR;

.field public static final enum CUT_OFF:LX/BSR;

.field public static final enum ERROR:LX/BSR;

.field public static final enum HIDE_AD:LX/BSR;

.field public static final enum NONE:LX/BSR;

.field public static final enum PLAYBACK_FINISHED:LX/BSR;


# instance fields
.field private final mCommercialBreakEndReason:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1785465
    new-instance v0, LX/BSR;

    const-string v1, "NONE"

    const-string v2, "none"

    invoke-direct {v0, v1, v3, v2}, LX/BSR;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BSR;->NONE:LX/BSR;

    .line 1785466
    new-instance v0, LX/BSR;

    const-string v1, "CUT_OFF"

    const-string v2, "cut_off"

    invoke-direct {v0, v1, v4, v2}, LX/BSR;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BSR;->CUT_OFF:LX/BSR;

    .line 1785467
    new-instance v0, LX/BSR;

    const-string v1, "PLAYBACK_FINISHED"

    const-string v2, "playback_finished"

    invoke-direct {v0, v1, v5, v2}, LX/BSR;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BSR;->PLAYBACK_FINISHED:LX/BSR;

    .line 1785468
    new-instance v0, LX/BSR;

    const-string v1, "ERROR"

    const-string v2, "error"

    invoke-direct {v0, v1, v6, v2}, LX/BSR;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BSR;->ERROR:LX/BSR;

    .line 1785469
    new-instance v0, LX/BSR;

    const-string v1, "HIDE_AD"

    const-string v2, "hide_ad"

    invoke-direct {v0, v1, v7, v2}, LX/BSR;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BSR;->HIDE_AD:LX/BSR;

    .line 1785470
    const/4 v0, 0x5

    new-array v0, v0, [LX/BSR;

    sget-object v1, LX/BSR;->NONE:LX/BSR;

    aput-object v1, v0, v3

    sget-object v1, LX/BSR;->CUT_OFF:LX/BSR;

    aput-object v1, v0, v4

    sget-object v1, LX/BSR;->PLAYBACK_FINISHED:LX/BSR;

    aput-object v1, v0, v5

    sget-object v1, LX/BSR;->ERROR:LX/BSR;

    aput-object v1, v0, v6

    sget-object v1, LX/BSR;->HIDE_AD:LX/BSR;

    aput-object v1, v0, v7

    sput-object v0, LX/BSR;->$VALUES:[LX/BSR;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1785471
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1785472
    iput-object p3, p0, LX/BSR;->mCommercialBreakEndReason:Ljava/lang/String;

    .line 1785473
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/BSR;
    .locals 1

    .prologue
    .line 1785474
    const-class v0, LX/BSR;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/BSR;

    return-object v0
.end method

.method public static values()[LX/BSR;
    .locals 1

    .prologue
    .line 1785475
    sget-object v0, LX/BSR;->$VALUES:[LX/BSR;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/BSR;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1785476
    iget-object v0, p0, LX/BSR;->mCommercialBreakEndReason:Ljava/lang/String;

    return-object v0
.end method
