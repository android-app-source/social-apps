.class public final LX/Bia;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/events/create/protocol/EventCreationMutationsModels$EditEventCoreMutationFieldsModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:J

.field public final synthetic b:LX/Bib;


# direct methods
.method public constructor <init>(LX/Bib;J)V
    .locals 0

    .prologue
    .line 1810568
    iput-object p1, p0, LX/Bia;->b:LX/Bib;

    iput-wide p2, p0, LX/Bia;->a:J

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1810566
    const-class v0, LX/Bib;

    const-string v1, "Error editing event: "

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1810567
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 1810560
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1810561
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1810562
    check-cast v0, Lcom/facebook/events/create/protocol/EventCreationMutationsModels$EditEventCoreMutationFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/events/create/protocol/EventCreationMutationsModels$EditEventCoreMutationFieldsModel;->a()Lcom/facebook/events/create/protocol/EventCreationMutationsModels$EditEventCoreMutationFieldsModel$EventModel;

    move-result-object v0

    .line 1810563
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/events/create/protocol/EventCreationMutationsModels$EditEventCoreMutationFieldsModel$EventModel;->j()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/events/create/protocol/EventCreationMutationsModels$EditEventCoreMutationFieldsModel$EventModel;->j()Ljava/lang/String;

    .line 1810564
    :cond_0
    iget-object v0, p0, LX/Bia;->b:LX/Bib;

    iget-object v0, v0, LX/Bib;->b:LX/Bl6;

    new-instance v1, LX/BlL;

    iget-wide v2, p0, LX/Bia;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LX/BlL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1810565
    return-void
.end method
