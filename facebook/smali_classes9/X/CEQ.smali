.class public final LX/CEQ;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Ljava/util/List",
        "<",
        "Ljava/lang/Object;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;)V
    .locals 0

    .prologue
    .line 1861134
    iput-object p1, p0, LX/CEQ;->a:Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1861135
    iget-object v0, p0, LX/CEQ;->a:Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, LX/CEQ;->a:Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;

    iget-object v1, v1, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->t:LX/2hU;

    iget-object v2, p0, LX/CEQ;->a:Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;

    iget-object v2, v2, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->s:LX/16I;

    invoke-static {v0, v1, v2}, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->a(Landroid/content/Context;LX/2hU;LX/16I;)Z

    .line 1861136
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 14

    .prologue
    .line 1861137
    check-cast p1, Ljava/util/List;

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 1861138
    iget-object v0, p0, LX/CEQ;->a:Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;

    iget-object v0, v0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->E:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1861139
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/greetingcards/verve/model/VMDeck;

    if-eqz v0, :cond_0

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirsFetchResult;

    if-nez v0, :cond_1

    .line 1861140
    :cond_0
    iget-object v0, p0, LX/CEQ;->a:Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 1861141
    :goto_0
    return-void

    .line 1861142
    :cond_1
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirsFetchResult;

    .line 1861143
    iget-object v1, p0, LX/CEQ;->a:Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;

    iget-object v2, v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirsFetchResult;->a:Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel;

    invoke-static {v2, v3}, LX/AzA;->b(Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel;Z)I

    move-result v2

    .line 1861144
    iput v2, v1, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->R:I

    .line 1861145
    iget-object v1, p0, LX/CEQ;->a:Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;

    iget v1, v1, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->R:I

    if-gtz v1, :cond_2

    .line 1861146
    iget-object v0, p0, LX/CEQ;->a:Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->c()V

    goto :goto_0

    .line 1861147
    :cond_2
    iget-object v1, p0, LX/CEQ;->a:Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;

    iget-object v2, v1, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->q:LX/CEd;

    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/greetingcards/verve/model/VMDeck;

    const/4 v6, 0x0

    .line 1861148
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1861149
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1861150
    new-instance v7, Ljava/util/HashMap;

    iget-object v3, v1, Lcom/facebook/greetingcards/verve/model/VMDeck;->slides:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    invoke-direct {v7, v3}, Ljava/util/HashMap;-><init>(I)V

    .line 1861151
    iget-object v5, v1, Lcom/facebook/greetingcards/verve/model/VMDeck;->slides:LX/0Px;

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v8

    move v4, v6

    :goto_1
    if-ge v4, v8, :cond_3

    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/greetingcards/verve/model/VMSlide;

    .line 1861152
    iget-object v9, v3, Lcom/facebook/greetingcards/verve/model/VMSlide;->name:Ljava/lang/String;

    invoke-virtual {v7, v9, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1861153
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_1

    .line 1861154
    :cond_3
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v8

    .line 1861155
    iget-object v3, v1, Lcom/facebook/greetingcards/verve/model/VMDeck;->initialSlide:Ljava/lang/String;

    invoke-virtual {v7, v3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 1861156
    iget-object v3, v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirsFetchResult;->a:Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel;

    invoke-virtual {v3}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel;->m()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v3

    .line 1861157
    if-eqz v3, :cond_8

    invoke-interface {v3}, LX/174;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_8

    .line 1861158
    const-string v3, "Cover_0"

    .line 1861159
    :goto_2
    invoke-virtual {v7, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/greetingcards/verve/model/VMSlide;

    const/4 p1, 0x0

    .line 1861160
    new-instance v5, LX/CFm;

    invoke-direct {v5}, LX/CFm;-><init>()V

    .line 1861161
    invoke-virtual {v5, v3}, LX/CFm;->a(Lcom/facebook/greetingcards/verve/model/VMSlide;)LX/CFm;

    .line 1861162
    iput-boolean p1, v5, LX/CFm;->e:Z

    .line 1861163
    new-instance v9, LX/0Pz;

    invoke-direct {v9}, LX/0Pz;-><init>()V

    .line 1861164
    const-string v10, "media_1"

    iget-object v4, v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirsFetchResult;->a:Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel;

    invoke-virtual {v4}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel;->j()Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$ContainerPostModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$ContainerPostModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$ContainerPostModel$ActorsModel;

    .line 1861165
    if-eqz v4, :cond_4

    invoke-virtual {v4}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$ContainerPostModel$ActorsModel;->a()Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$ContainerPostModel$ActorsModel$ProfilePictureModel;

    move-result-object v11

    if-nez v11, :cond_a

    .line 1861166
    :cond_4
    const/4 v11, 0x0

    .line 1861167
    :goto_3
    move-object v4, v11

    .line 1861168
    const/4 v11, 0x0

    invoke-static {v10, v4, v11}, Lcom/facebook/greetingcards/verve/model/VMSlideValue;->a(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/PointF;)Lcom/facebook/greetingcards/verve/model/VMSlideValue;

    move-result-object v4

    invoke-virtual {v9, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1861169
    iget-object v4, v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirsFetchResult;->a:Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel;

    invoke-virtual {v4}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel;->m()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v4

    .line 1861170
    if-eqz v4, :cond_5

    invoke-interface {v4}, LX/174;->a()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_5

    .line 1861171
    const-string v10, "title"

    invoke-interface {v4}, LX/174;->a()Ljava/lang/String;

    move-result-object v4

    invoke-static {v10, v4}, Lcom/facebook/greetingcards/verve/model/VMSlideValue;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/greetingcards/verve/model/VMSlideValue;

    move-result-object v4

    invoke-virtual {v9, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1861172
    :cond_5
    const-string v10, "subtitle"

    iget-object v11, v2, LX/CEd;->a:Landroid/content/res/Resources;

    const v12, 0x7f082750

    const/4 v4, 0x1

    new-array v13, v4, [Ljava/lang/Object;

    iget-object v4, v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirsFetchResult;->a:Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel;

    invoke-virtual {v4}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel;->j()Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$ContainerPostModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$ContainerPostModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$ContainerPostModel$ActorsModel;

    invoke-virtual {v4}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$ContainerPostModel$ActorsModel;->j()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v13, p1

    invoke-virtual {v11, v12, v13}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    iget-object v11, v2, LX/CEd;->c:LX/0W9;

    invoke-virtual {v11}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v11

    invoke-virtual {v4, v11}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v10, v4}, Lcom/facebook/greetingcards/verve/model/VMSlideValue;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/greetingcards/verve/model/VMSlideValue;

    move-result-object v4

    invoke-virtual {v9, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1861173
    invoke-virtual {v9}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    .line 1861174
    iput-object v4, v5, LX/CFm;->g:LX/0Px;

    .line 1861175
    invoke-virtual {v5}, LX/CFm;->a()Lcom/facebook/greetingcards/verve/model/VMSlide;

    move-result-object v4

    move-object v3, v4

    .line 1861176
    invoke-virtual {v8, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1861177
    :cond_6
    invoke-static {v2, v7, v0}, LX/CEd;->a(LX/CEd;Ljava/util/HashMap;Lcom/facebook/friendsharing/souvenirs/models/SouvenirsFetchResult;)LX/0Px;

    move-result-object v9

    .line 1861178
    iget-object v3, v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirsFetchResult;->a:Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel;

    invoke-virtual {v3}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel;->l()Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$MediaElementsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$MediaElementsModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Py;->iterator()LX/0Rc;

    move-result-object v10

    .line 1861179
    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v11

    move v5, v6

    :goto_4
    if-ge v5, v11, :cond_9

    invoke-virtual {v9, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/friendsharing/souvenirs/models/SouvenirFormattingStringModel;

    .line 1861180
    invoke-virtual {v3}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirFormattingStringModel;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v7, v4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 1861181
    new-instance v12, LX/CFm;

    invoke-direct {v12}, LX/CFm;-><init>()V

    .line 1861182
    invoke-virtual {v3}, Lcom/facebook/friendsharing/souvenirs/models/SouvenirFormattingStringModel;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v7, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/greetingcards/verve/model/VMSlide;

    invoke-virtual {v12, v4}, LX/CFm;->a(Lcom/facebook/greetingcards/verve/model/VMSlide;)LX/CFm;

    .line 1861183
    invoke-static {v3, v10}, LX/CEd;->a(Lcom/facebook/friendsharing/souvenirs/models/SouvenirFormattingStringModel;LX/0Rc;)LX/0Px;

    move-result-object v3

    .line 1861184
    iput-object v3, v12, LX/CFm;->g:LX/0Px;

    .line 1861185
    iput-boolean v6, v12, LX/CFm;->e:Z

    .line 1861186
    invoke-virtual {v12}, LX/CFm;->a()Lcom/facebook/greetingcards/verve/model/VMSlide;

    move-result-object v3

    invoke-virtual {v8, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1861187
    :cond_7
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_4

    .line 1861188
    :cond_8
    const-string v3, "Cover_0_notitle"

    goto/16 :goto_2

    .line 1861189
    :cond_9
    iget-object v3, v1, Lcom/facebook/greetingcards/verve/model/VMDeck;->slides:LX/0Px;

    invoke-virtual {v8, v3}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 1861190
    new-instance v3, LX/CFk;

    invoke-direct {v3}, LX/CFk;-><init>()V

    .line 1861191
    iget-object v4, v1, Lcom/facebook/greetingcards/verve/model/VMDeck;->size:LX/0Px;

    iput-object v4, v3, LX/CFk;->a:LX/0Px;

    .line 1861192
    iget-object v4, v1, Lcom/facebook/greetingcards/verve/model/VMDeck;->slides:LX/0Px;

    iput-object v4, v3, LX/CFk;->b:LX/0Px;

    .line 1861193
    iget-object v4, v1, Lcom/facebook/greetingcards/verve/model/VMDeck;->resources:LX/0P1;

    iput-object v4, v3, LX/CFk;->c:LX/0P1;

    .line 1861194
    iget-object v4, v1, Lcom/facebook/greetingcards/verve/model/VMDeck;->initialSlide:Ljava/lang/String;

    iput-object v4, v3, LX/CFk;->d:Ljava/lang/String;

    .line 1861195
    iget-object v4, v1, Lcom/facebook/greetingcards/verve/model/VMDeck;->styles:LX/0Px;

    iput-object v4, v3, LX/CFk;->e:LX/0Px;

    .line 1861196
    iget-object v4, v1, Lcom/facebook/greetingcards/verve/model/VMDeck;->theme:Ljava/lang/String;

    iput-object v4, v3, LX/CFk;->f:Ljava/lang/String;

    .line 1861197
    iget-object v4, v1, Lcom/facebook/greetingcards/verve/model/VMDeck;->bgColor:Lcom/facebook/greetingcards/verve/model/VMColor;

    iput-object v4, v3, LX/CFk;->g:Lcom/facebook/greetingcards/verve/model/VMColor;

    .line 1861198
    invoke-virtual {v8}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    .line 1861199
    iput-object v4, v3, LX/CFk;->b:LX/0Px;

    .line 1861200
    invoke-virtual {v3}, LX/CFk;->a()Lcom/facebook/greetingcards/verve/model/VMDeck;

    move-result-object v3

    move-object v1, v3

    .line 1861201
    iget-object v2, p0, LX/CEQ;->a:Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;

    iget-object v2, v2, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->D:Lcom/facebook/greetingcards/verve/render/FlatVerveRecyclerView;

    iget-object v3, p0, LX/CEQ;->a:Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;

    iget-object v3, v3, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->p:LX/CEn;

    iget-object v4, v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirsFetchResult;->a:Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel;

    iget-object v5, p0, LX/CEQ;->a:Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;

    iget-object v5, v5, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->L:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1861202
    new-instance v7, Lcom/facebook/friendsharing/souvenirs/verve/SouvenirsVerveViewSupplier;

    const/16 v8, 0xf2f

    invoke-static {v3, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    .line 1861203
    new-instance v9, LX/AzA;

    invoke-direct {v9}, LX/AzA;-><init>()V

    .line 1861204
    move-object v9, v9

    .line 1861205
    move-object v9, v9

    .line 1861206
    check-cast v9, LX/AzA;

    invoke-static {v3}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v10

    check-cast v10, Landroid/content/res/Resources;

    invoke-static {v3}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v11

    check-cast v11, Lcom/facebook/content/SecureContextHelper;

    move-object v12, v4

    move-object v13, v5

    invoke-direct/range {v7 .. v13}, Lcom/facebook/friendsharing/souvenirs/verve/SouvenirsVerveViewSupplier;-><init>(LX/0Or;LX/AzA;Landroid/content/res/Resources;Lcom/facebook/content/SecureContextHelper;Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1861207
    move-object v3, v7

    .line 1861208
    new-instance v4, LX/CEP;

    invoke-direct {v4, p0}, LX/CEP;-><init>(LX/CEQ;)V

    new-instance v5, LX/CEV;

    iget-object v6, p0, LX/CEQ;->a:Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;

    iget-object v0, v0, Lcom/facebook/friendsharing/souvenirs/models/SouvenirsFetchResult;->a:Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel;

    invoke-direct {v5, v6, v0}, LX/CEV;-><init>(Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel;)V

    invoke-virtual {v2, v1, v3, v4, v5}, Lcom/facebook/greetingcards/verve/render/FlatVerveRecyclerView;->a(Lcom/facebook/greetingcards/verve/model/VMDeck;LX/CEl;LX/CEO;LX/CEU;)V

    goto/16 :goto_0

    :cond_a
    invoke-virtual {v4}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$ContainerPostModel$ActorsModel;->a()Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$ContainerPostModel$ActorsModel$ProfilePictureModel;

    move-result-object v11

    invoke-virtual {v11}, Lcom/facebook/friendsharing/souvenirs/protocols/FetchSouvenirsModels$SouvenirsDetailsFieldsModel$ContainerPostModel$ActorsModel$ProfilePictureModel;->a()Ljava/lang/String;

    move-result-object v11

    goto/16 :goto_3
.end method
