.class public final LX/BLn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

.field public final synthetic b:LX/2rw;

.field public final synthetic c:LX/BLr;


# direct methods
.method public constructor <init>(LX/BLr;Lcom/facebook/platform/composer/composer/PlatformComposerFragment;LX/2rw;)V
    .locals 0

    .prologue
    .line 1776430
    iput-object p1, p0, LX/BLn;->c:LX/BLr;

    iput-object p2, p0, LX/BLn;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iput-object p3, p0, LX/BLn;->b:LX/2rw;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 5

    .prologue
    .line 1776417
    iget-object v0, p0, LX/BLn;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v1, p0, LX/BLn;->b:LX/2rw;

    .line 1776418
    sget-object v2, LX/2rw;->UNDIRECTED:LX/2rw;

    if-ne v1, v2, :cond_1

    .line 1776419
    iget-object v2, v0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v2, v2, LX/BKm;->e:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    iget-object v2, v2, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    sget-object v3, LX/2rw;->UNDIRECTED:LX/2rw;

    if-eq v2, v3, :cond_0

    .line 1776420
    sget-object v2, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->a:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->a(Lcom/facebook/ipc/composer/intent/ComposerTargetData;Lcom/facebook/auth/viewercontext/ViewerContext;)V

    .line 1776421
    :cond_0
    :goto_0
    iget-object v0, p0, LX/BLn;->c:LX/BLr;

    iget-object v0, v0, LX/BLr;->g:LX/0if;

    sget-object v1, LX/0ig;->k:LX/0ih;

    const-string v2, "launch_target_selection_clicked"

    invoke-virtual {v0, v1, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1776422
    const/4 v0, 0x1

    return v0

    .line 1776423
    :cond_1
    :try_start_0
    iget-object v2, v0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->T:LX/BLr;

    .line 1776424
    iget-object v3, v2, LX/BLr;->f:LX/0P1;

    invoke-virtual {v3, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/BLm;

    iget-object v3, v3, LX/BLm;->e:Ljava/lang/Class;

    move-object v2, v3

    .line 1776425
    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1776426
    new-instance v3, Landroid/content/Intent;

    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v4

    invoke-direct {v3, v4, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1776427
    const/16 v2, 0x1e

    invoke-virtual {v0, v3, v2}, Landroid/support/v4/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 1776428
    :catch_0
    iget-object v2, v0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->w:LX/0if;

    sget-object v3, LX/0ig;->k:LX/0ih;

    const-string v4, "no_activity_for_composer_target_selection"

    invoke-virtual {v2, v3, v4}, LX/0if;->a(LX/0ih;Ljava/lang/String;)V

    .line 1776429
    iget-object v2, v0, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->s:LX/03V;

    const-string v3, "no_activity_for_composer_target_selection"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string p1, "Couldn\'t find activity for target type: "

    invoke-direct {v4, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
