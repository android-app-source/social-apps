.class public final LX/B3E;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Landroid/net/Uri;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;


# direct methods
.method public constructor <init>(Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;)V
    .locals 0

    .prologue
    .line 1739824
    iput-object p1, p0, LX/B3E;->a:Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method

.method private a(Landroid/net/Uri;)V
    .locals 3

    .prologue
    .line 1739825
    iget-object v0, p0, LX/B3E;->a:Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;

    const/4 v1, 0x0

    .line 1739826
    iput-object v1, v0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->x:LX/1Mv;

    .line 1739827
    if-eqz p1, :cond_0

    .line 1739828
    iget-object v0, p0, LX/B3E;->a:Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;

    iget-object v0, v0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->g:LX/B3O;

    iget-object v1, p0, LX/B3E;->a:Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;

    iget-object v1, v1, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->g:LX/B3O;

    invoke-virtual {v1}, LX/B3O;->a()Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;->j()LX/B3N;

    move-result-object v1

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1739829
    iput-object v2, v1, LX/B3N;->c:Ljava/lang/String;

    .line 1739830
    move-object v1, v1

    .line 1739831
    invoke-virtual {v1}, LX/B3N;->a()Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/B3O;->a(Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;)V

    .line 1739832
    :cond_0
    iget-object v0, p0, LX/B3E;->a:Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;

    invoke-static {v0}, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->h(Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;)V

    .line 1739833
    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1739834
    iget-object v0, p0, LX/B3E;->a:Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;

    const/4 v1, 0x0

    .line 1739835
    iput-object v1, v0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->x:LX/1Mv;

    .line 1739836
    instance-of v0, p1, Ljava/io/IOException;

    if-nez v0, :cond_0

    .line 1739837
    iget-object v0, p0, LX/B3E;->a:Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;

    iget-object v0, v0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "heisman_fetch_recent_video_thumbnail_failed"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1739838
    :cond_0
    iget-object v0, p0, LX/B3E;->a:Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;

    invoke-static {v0}, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->m(Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;)V

    .line 1739839
    return-void
.end method

.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1739840
    check-cast p1, Landroid/net/Uri;

    invoke-direct {p0, p1}, LX/B3E;->a(Landroid/net/Uri;)V

    return-void
.end method
