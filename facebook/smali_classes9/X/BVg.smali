.class public final enum LX/BVg;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/BVg;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/BVg;

.field public static final enum BUTTON:LX/BVg;

.field public static final enum FRAME_LAYOUT:LX/BVg;

.field public static final enum IMAGE_BUTTON:LX/BVg;

.field public static final enum IMAGE_VIEW:LX/BVg;

.field public static final enum LINEAR_LAYOUT:LX/BVg;

.field public static final enum RATING_BAR:LX/BVg;

.field public static final enum RATING_BAR_OLD:LX/BVg;

.field public static final enum RELATIVE_LAYOUT:LX/BVg;

.field public static final enum SCROLL_VIEW:LX/BVg;

.field public static final enum TEXT_VIEW:LX/BVg;

.field public static final enum URL_IMAGE:LX/BVg;

.field public static final enum URL_IMAGE_OLD:LX/BVg;

.field public static final enum VIEW:LX/BVg;

.field public static final enum VIEW_STUB:LX/BVg;


# instance fields
.field private final mValue:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1791305
    new-instance v0, LX/BVg;

    const-string v1, "TEXT_VIEW"

    const-string v2, "TextView"

    invoke-direct {v0, v1, v4, v2}, LX/BVg;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BVg;->TEXT_VIEW:LX/BVg;

    .line 1791306
    new-instance v0, LX/BVg;

    const-string v1, "URL_IMAGE_OLD"

    const-string v2, "com.facebook.widget.UrlImage"

    invoke-direct {v0, v1, v5, v2}, LX/BVg;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BVg;->URL_IMAGE_OLD:LX/BVg;

    .line 1791307
    new-instance v0, LX/BVg;

    const-string v1, "URL_IMAGE"

    const-string v2, "com.facebook.widget.images.UrlImage"

    invoke-direct {v0, v1, v6, v2}, LX/BVg;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BVg;->URL_IMAGE:LX/BVg;

    .line 1791308
    new-instance v0, LX/BVg;

    const-string v1, "RATING_BAR_OLD"

    const-string v2, "com.facebook.nearby.ui.FractionalRatingBar"

    invoke-direct {v0, v1, v7, v2}, LX/BVg;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BVg;->RATING_BAR_OLD:LX/BVg;

    .line 1791309
    new-instance v0, LX/BVg;

    const-string v1, "RATING_BAR"

    const-string v2, "com.facebook.widget.ratingbar.FractionalRatingBar"

    invoke-direct {v0, v1, v8, v2}, LX/BVg;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BVg;->RATING_BAR:LX/BVg;

    .line 1791310
    new-instance v0, LX/BVg;

    const-string v1, "FRAME_LAYOUT"

    const/4 v2, 0x5

    const-string v3, "FrameLayout"

    invoke-direct {v0, v1, v2, v3}, LX/BVg;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BVg;->FRAME_LAYOUT:LX/BVg;

    .line 1791311
    new-instance v0, LX/BVg;

    const-string v1, "RELATIVE_LAYOUT"

    const/4 v2, 0x6

    const-string v3, "RelativeLayout"

    invoke-direct {v0, v1, v2, v3}, LX/BVg;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BVg;->RELATIVE_LAYOUT:LX/BVg;

    .line 1791312
    new-instance v0, LX/BVg;

    const-string v1, "LINEAR_LAYOUT"

    const/4 v2, 0x7

    const-string v3, "LinearLayout"

    invoke-direct {v0, v1, v2, v3}, LX/BVg;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BVg;->LINEAR_LAYOUT:LX/BVg;

    .line 1791313
    new-instance v0, LX/BVg;

    const-string v1, "BUTTON"

    const/16 v2, 0x8

    const-string v3, "Button"

    invoke-direct {v0, v1, v2, v3}, LX/BVg;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BVg;->BUTTON:LX/BVg;

    .line 1791314
    new-instance v0, LX/BVg;

    const-string v1, "VIEW"

    const/16 v2, 0x9

    const-string v3, "View"

    invoke-direct {v0, v1, v2, v3}, LX/BVg;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BVg;->VIEW:LX/BVg;

    .line 1791315
    new-instance v0, LX/BVg;

    const-string v1, "IMAGE_VIEW"

    const/16 v2, 0xa

    const-string v3, "ImageView"

    invoke-direct {v0, v1, v2, v3}, LX/BVg;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BVg;->IMAGE_VIEW:LX/BVg;

    .line 1791316
    new-instance v0, LX/BVg;

    const-string v1, "IMAGE_BUTTON"

    const/16 v2, 0xb

    const-string v3, "ImageButton"

    invoke-direct {v0, v1, v2, v3}, LX/BVg;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BVg;->IMAGE_BUTTON:LX/BVg;

    .line 1791317
    new-instance v0, LX/BVg;

    const-string v1, "VIEW_STUB"

    const/16 v2, 0xc

    const-string v3, "ViewStub"

    invoke-direct {v0, v1, v2, v3}, LX/BVg;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BVg;->VIEW_STUB:LX/BVg;

    .line 1791318
    new-instance v0, LX/BVg;

    const-string v1, "SCROLL_VIEW"

    const/16 v2, 0xd

    const-string v3, "ScrollView"

    invoke-direct {v0, v1, v2, v3}, LX/BVg;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BVg;->SCROLL_VIEW:LX/BVg;

    .line 1791319
    const/16 v0, 0xe

    new-array v0, v0, [LX/BVg;

    sget-object v1, LX/BVg;->TEXT_VIEW:LX/BVg;

    aput-object v1, v0, v4

    sget-object v1, LX/BVg;->URL_IMAGE_OLD:LX/BVg;

    aput-object v1, v0, v5

    sget-object v1, LX/BVg;->URL_IMAGE:LX/BVg;

    aput-object v1, v0, v6

    sget-object v1, LX/BVg;->RATING_BAR_OLD:LX/BVg;

    aput-object v1, v0, v7

    sget-object v1, LX/BVg;->RATING_BAR:LX/BVg;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/BVg;->FRAME_LAYOUT:LX/BVg;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/BVg;->RELATIVE_LAYOUT:LX/BVg;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/BVg;->LINEAR_LAYOUT:LX/BVg;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/BVg;->BUTTON:LX/BVg;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/BVg;->VIEW:LX/BVg;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/BVg;->IMAGE_VIEW:LX/BVg;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/BVg;->IMAGE_BUTTON:LX/BVg;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/BVg;->VIEW_STUB:LX/BVg;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/BVg;->SCROLL_VIEW:LX/BVg;

    aput-object v2, v0, v1

    sput-object v0, LX/BVg;->$VALUES:[LX/BVg;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1791320
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1791321
    iput-object p3, p0, LX/BVg;->mValue:Ljava/lang/String;

    .line 1791322
    return-void
.end method

.method public static from(Ljava/lang/String;)LX/BVg;
    .locals 5

    .prologue
    .line 1791323
    invoke-static {}, LX/BVg;->values()[LX/BVg;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 1791324
    iget-object v4, v3, LX/BVg;->mValue:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1791325
    return-object v3

    .line 1791326
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1791327
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unknown view = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/BVg;
    .locals 1

    .prologue
    .line 1791328
    const-class v0, LX/BVg;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/BVg;

    return-object v0
.end method

.method public static values()[LX/BVg;
    .locals 1

    .prologue
    .line 1791329
    sget-object v0, LX/BVg;->$VALUES:[LX/BVg;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/BVg;

    return-object v0
.end method
