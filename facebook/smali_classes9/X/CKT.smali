.class public LX/CKT;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/03V;

.field public final b:LX/0tX;

.field private final c:LX/CK7;

.field public final d:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/messaging/business/subscription/common/graphql/BusinessSubscriptionMutationsModels$ContentPageSubscribeMutationModel;",
            ">;>;"
        }
    .end annotation
.end field

.field public f:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/messaging/business/subscription/common/graphql/BusinessSubscriptionMutationsModels$ContentPageUnsubscribeMutationModel;",
            ">;>;"
        }
    .end annotation
.end field

.field private g:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/messaging/business/subscription/common/graphql/BusinessSubscriptionMutationsModels$ContentStationSubscribeMutationModel;",
            ">;>;"
        }
    .end annotation
.end field

.field private h:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/messaging/business/subscription/common/graphql/BusinessSubscriptionMutationsModels$ContentStationUnsubscribeMutationModel;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/03V;LX/0tX;LX/CK7;LX/1Ck;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1877212
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1877213
    iput-object p1, p0, LX/CKT;->a:LX/03V;

    .line 1877214
    iput-object p2, p0, LX/CKT;->b:LX/0tX;

    .line 1877215
    iput-object p3, p0, LX/CKT;->c:LX/CK7;

    .line 1877216
    iput-object p4, p0, LX/CKT;->d:LX/1Ck;

    .line 1877217
    return-void
.end method

.method public static b(LX/0QB;)LX/CKT;
    .locals 5

    .prologue
    .line 1877173
    new-instance v4, LX/CKT;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v0

    check-cast v0, LX/03V;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v1

    check-cast v1, LX/0tX;

    invoke-static {p0}, LX/CK7;->b(LX/0QB;)LX/CK7;

    move-result-object v2

    check-cast v2, LX/CK7;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v3

    check-cast v3, LX/1Ck;

    invoke-direct {v4, v0, v1, v2, v3}, LX/CKT;-><init>(LX/03V;LX/0tX;LX/CK7;LX/1Ck;)V

    .line 1877174
    return-object v4
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/CKR;)V
    .locals 4
    .param p2    # LX/CKR;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1877201
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1877202
    iget-object v0, p0, LX/CKT;->a:LX/03V;

    const-string v1, "BusinessSubscriptionMutationHelper"

    const-string v2, "Subscribe station id is null"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1877203
    if-eqz p2, :cond_0

    .line 1877204
    invoke-interface {p2}, LX/CKR;->b()V

    .line 1877205
    :cond_0
    :goto_0
    return-void

    .line 1877206
    :cond_1
    new-instance v0, LX/4H8;

    invoke-direct {v0}, LX/4H8;-><init>()V

    .line 1877207
    const-string v1, "station_id"

    invoke-virtual {v0, v1, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1877208
    new-instance v1, LX/CKA;

    invoke-direct {v1}, LX/CKA;-><init>()V

    move-object v1, v1

    .line 1877209
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1877210
    iget-object v0, p0, LX/CKT;->b:LX/0tX;

    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, LX/CKT;->g:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1877211
    iget-object v0, p0, LX/CKT;->d:LX/1Ck;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "subscribe_to_station"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/CKT;->g:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v3, LX/CKP;

    invoke-direct {v3, p0, p2}, LX/CKP;-><init>(LX/CKT;LX/CKR;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;LX/CKR;)V
    .locals 4
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # LX/CKR;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1877186
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1877187
    invoke-static {p2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1877188
    iget-object v0, p0, LX/CKT;->a:LX/03V;

    const-string v1, "BusinessSubscriptionMutationHelper"

    const-string v2, "Subscribe page id is null"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1877189
    :goto_1
    return-void

    .line 1877190
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1877191
    :cond_1
    iget-object v0, p0, LX/CKT;->c:LX/CK7;

    .line 1877192
    iget-object v1, v0, LX/CK7;->a:LX/0Zb;

    const-string v2, "messenger_content_subscription_on_subscribe"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v1

    .line 1877193
    invoke-virtual {v1}, LX/0oG;->a()Z

    move-result v2

    if-nez v2, :cond_2

    .line 1877194
    :goto_2
    new-instance v0, LX/4H6;

    invoke-direct {v0}, LX/4H6;-><init>()V

    .line 1877195
    const-string v1, "page_id"

    invoke-virtual {v0, v1, p2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1877196
    new-instance v1, LX/CK8;

    invoke-direct {v1}, LX/CK8;-><init>()V

    move-object v1, v1

    .line 1877197
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1877198
    iget-object v0, p0, LX/CKT;->b:LX/0tX;

    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, LX/CKT;->e:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1877199
    iget-object v0, p0, LX/CKT;->d:LX/1Ck;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "subscribe_to_page"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/CKT;->e:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v3, LX/CKN;

    invoke-direct {v3, p0, p3}, LX/CKN;-><init>(LX/CKT;LX/CKR;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto :goto_1

    .line 1877200
    :cond_2
    const-string v2, "source"

    invoke-virtual {v1, v2, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v1

    const-string v2, "page_id"

    invoke-virtual {v1, v2, p2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v1

    invoke-virtual {v1}, LX/0oG;->d()V

    goto :goto_2
.end method

.method public final b(Ljava/lang/String;LX/CKS;)V
    .locals 4
    .param p2    # LX/CKS;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1877175
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1877176
    iget-object v0, p0, LX/CKT;->a:LX/03V;

    const-string v1, "BusinessSubscriptionMutationHelper"

    const-string v2, "Unsubscribe station id is null"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1877177
    if-eqz p2, :cond_0

    .line 1877178
    invoke-interface {p2}, LX/CKS;->b()V

    .line 1877179
    :cond_0
    :goto_0
    return-void

    .line 1877180
    :cond_1
    new-instance v0, LX/4H9;

    invoke-direct {v0}, LX/4H9;-><init>()V

    .line 1877181
    const-string v1, "station_id"

    invoke-virtual {v0, v1, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1877182
    new-instance v1, LX/CKB;

    invoke-direct {v1}, LX/CKB;-><init>()V

    move-object v1, v1

    .line 1877183
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1877184
    iget-object v0, p0, LX/CKT;->b:LX/0tX;

    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, LX/CKT;->h:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1877185
    iget-object v0, p0, LX/CKT;->d:LX/1Ck;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "subscribe_to_station"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/CKT;->h:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v3, LX/CKQ;

    invoke-direct {v3, p0, p2}, LX/CKQ;-><init>(LX/CKT;LX/CKS;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto :goto_0
.end method
