.class public final LX/BSy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# instance fields
.field public final synthetic a:Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;


# direct methods
.method public constructor <init>(Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;)V
    .locals 0

    .prologue
    .line 1786351
    iput-object p1, p0, LX/BSy;->a:Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 1786352
    return-void
.end method

.method public final onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2

    .prologue
    .line 1786347
    iget-object v0, p0, LX/BSy;->a:Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;

    iget-object v0, v0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->a:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1786348
    iget-object v0, p0, LX/BSy;->a:Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;

    iget-object v0, v0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->m:Lcom/facebook/widget/FbImageView;

    iget-object v1, p0, LX/BSy;->a:Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;

    iget v1, v1, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->s:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/FbImageView;->setRotation(F)V

    .line 1786349
    iget-object v0, p0, LX/BSy;->a:Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;

    iget-object v0, v0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->i:LX/BSm;

    iget-object v1, p0, LX/BSy;->a:Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;

    iget v1, v1, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->s:I

    add-int/lit16 v1, v1, 0x168

    rem-int/lit16 v1, v1, 0x168

    invoke-interface {v0, v1}, LX/BSm;->a(I)V

    .line 1786350
    return-void
.end method

.method public final onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 1786346
    return-void
.end method

.method public final onAnimationStart(Landroid/animation/Animator;)V
    .locals 2

    .prologue
    .line 1786344
    iget-object v0, p0, LX/BSy;->a:Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;

    iget-object v0, v0, Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;->a:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1786345
    return-void
.end method
