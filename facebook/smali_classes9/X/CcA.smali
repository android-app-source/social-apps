.class public final LX/CcA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/CcC;


# direct methods
.method public constructor <init>(LX/CcC;)V
    .locals 0

    .prologue
    .line 1920694
    iput-object p1, p0, LX/CcA;->a:LX/CcC;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 6

    .prologue
    .line 1920695
    iget-object v0, p0, LX/CcA;->a:LX/CcC;

    iget-object v0, v0, LX/CcC;->a:LX/5kD;

    invoke-interface {v0}, LX/5kD;->an()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;

    move-result-object v0

    .line 1920696
    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel;

    .line 1920697
    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel;->b()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 1920698
    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel;->b()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;->c()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, LX/CcA;->a:LX/CcC;

    iget-object v5, v5, LX/CcC;->b:LX/CcO;

    iget-object v5, v5, LX/CcO;->a:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;

    iget-object v5, v5, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->d:LX/0Or;

    invoke-interface {v5}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1920699
    iget-object v1, p0, LX/CcA;->a:LX/CcC;

    iget-object v1, v1, LX/CcC;->b:LX/CcO;

    iget-object v1, v1, LX/CcO;->a:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;

    iget-object v1, v1, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->i:LX/9hh;

    iget-object v2, p0, LX/CcA;->a:LX/CcC;

    iget-object v2, v2, LX/CcC;->a:LX/5kD;

    invoke-interface {v2}, LX/5kD;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel;->b()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/9hh;->a(Ljava/lang/String;Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$TagInfoQueryModel$EdgesModel$NodeModel;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0

    .line 1920700
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1920701
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "viewer\'s tag can\'t be found in tag list"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
