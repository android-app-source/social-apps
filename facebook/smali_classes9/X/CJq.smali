.class public final LX/CJq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/CJl;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/CJl;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 1875863
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1875864
    iput-object p1, p0, LX/CJq;->a:LX/0QB;

    .line 1875865
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1875866
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/CJq;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1875867
    packed-switch p2, :pswitch_data_0

    .line 1875868
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1875869
    :pswitch_0
    new-instance p0, LX/JeM;

    invoke-static {p1}, LX/2Oi;->a(LX/0QB;)LX/2Oi;

    move-result-object v0

    check-cast v0, LX/2Oi;

    invoke-static {p1}, LX/JdU;->b(LX/0QB;)LX/JdU;

    move-result-object v1

    check-cast v1, LX/JdU;

    invoke-direct {p0, v0, v1}, LX/JeM;-><init>(LX/2Oi;LX/JdU;)V

    .line 1875870
    move-object v0, p0

    .line 1875871
    :goto_0
    return-object v0

    .line 1875872
    :pswitch_1
    new-instance v2, LX/IZ2;

    const-class v3, Landroid/content/Context;

    invoke-interface {p1, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    .line 1875873
    new-instance v5, LX/IZ3;

    invoke-static {p1}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    invoke-direct {v5, v4}, LX/IZ3;-><init>(LX/0Zb;)V

    .line 1875874
    move-object v4, v5

    .line 1875875
    check-cast v4, LX/IZ3;

    invoke-static {p1}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v5

    check-cast v5, Lcom/facebook/content/SecureContextHelper;

    .line 1875876
    new-instance v9, LX/IZ4;

    invoke-static {p1}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v6

    check-cast v6, LX/03V;

    invoke-static {p1}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v7

    check-cast v7, LX/0tX;

    invoke-static {p1}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v8

    check-cast v8, LX/1Ck;

    invoke-direct {v9, v6, v7, v8}, LX/IZ4;-><init>(LX/03V;LX/0tX;LX/1Ck;)V

    .line 1875877
    move-object v6, v9

    .line 1875878
    check-cast v6, LX/IZ4;

    invoke-static {p1}, LX/IZK;->b(LX/0QB;)LX/IZK;

    move-result-object v7

    check-cast v7, LX/IZK;

    invoke-direct/range {v2 .. v7}, LX/IZ2;-><init>(Landroid/content/Context;LX/IZ3;Lcom/facebook/content/SecureContextHelper;LX/IZ4;LX/IZK;)V

    .line 1875879
    move-object v0, v2

    .line 1875880
    goto :goto_0

    .line 1875881
    :pswitch_2
    new-instance v0, LX/CJn;

    .line 1875882
    new-instance v1, LX/0U8;

    invoke-interface {p1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v2

    new-instance v3, LX/CJp;

    invoke-direct {v3, p1}, LX/CJp;-><init>(LX/0QB;)V

    invoke-direct {v1, v2, v3}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v1, v1

    .line 1875883
    invoke-direct {v0, v1}, LX/CJn;-><init>(Ljava/util/Set;)V

    .line 1875884
    const-class v1, Landroid/content/Context;

    invoke-interface {p1, v1}, LX/0QC;->getLazy(Ljava/lang/Class;)LX/0Ot;

    move-result-object v1

    const/16 v2, 0x27c5

    invoke-static {p1, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x2eb

    invoke-static {p1, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    .line 1875885
    iput-object v1, v0, LX/CJn;->a:LX/0Ot;

    iput-object v2, v0, LX/CJn;->b:LX/0Ot;

    iput-object v3, v0, LX/CJn;->c:LX/0Ot;

    .line 1875886
    move-object v0, v0

    .line 1875887
    goto :goto_0

    .line 1875888
    :pswitch_3
    new-instance v1, LX/IcR;

    invoke-static {p1}, LX/Ic4;->b(LX/0QB;)LX/Ic4;

    move-result-object v0

    check-cast v0, LX/Ic4;

    invoke-direct {v1, v0}, LX/IcR;-><init>(LX/Ic4;)V

    .line 1875889
    move-object v0, v1

    .line 1875890
    goto :goto_0

    .line 1875891
    :pswitch_4
    new-instance v2, LX/Jfv;

    invoke-direct {v2}, LX/Jfv;-><init>()V

    .line 1875892
    invoke-static {p1}, LX/CKT;->b(LX/0QB;)LX/CKT;

    move-result-object v0

    check-cast v0, LX/CKT;

    invoke-static {p1}, LX/Jg8;->a(LX/0QB;)LX/Jg8;

    move-result-object v1

    check-cast v1, LX/Jg8;

    .line 1875893
    iput-object v0, v2, LX/Jfv;->a:LX/CKT;

    iput-object v1, v2, LX/Jfv;->b:LX/Jg8;

    .line 1875894
    move-object v0, v2

    .line 1875895
    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 1875896
    const/4 v0, 0x5

    return v0
.end method
