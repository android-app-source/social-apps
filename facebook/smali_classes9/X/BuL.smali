.class public final LX/BuL;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/feed/protocol/FetchVideoBroadcastPlayAndFeedbackCountGraphQLModels$FetchVideoBroadcastPlayAndFeedbackCountQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/feed/video/fullscreen/FeedbackWithViewsAndCountPlugin;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/video/fullscreen/FeedbackWithViewsAndCountPlugin;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1830811
    iput-object p1, p0, LX/BuL;->b:Lcom/facebook/feed/video/fullscreen/FeedbackWithViewsAndCountPlugin;

    iput-object p2, p0, LX/BuL;->a:Ljava/lang/String;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 1830866
    iget-object v0, p0, LX/BuL;->b:Lcom/facebook/feed/video/fullscreen/FeedbackWithViewsAndCountPlugin;

    iget-object v0, v0, Lcom/facebook/feed/video/fullscreen/FeedbackWithViewsAndCountPlugin;->t:LX/03V;

    sget-object v1, Lcom/facebook/feed/video/fullscreen/FeedbackWithViewsAndCountPlugin;->v:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "video broadcast request failed for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/BuL;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1830867
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 9
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1830812
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 1830813
    if-nez p1, :cond_1

    .line 1830814
    :cond_0
    :goto_0
    return-void

    .line 1830815
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1830816
    check-cast v0, Lcom/facebook/feed/protocol/FetchVideoBroadcastPlayAndFeedbackCountGraphQLModels$FetchVideoBroadcastPlayAndFeedbackCountQueryModel;

    .line 1830817
    if-nez v0, :cond_2

    .line 1830818
    goto :goto_0

    .line 1830819
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/feed/protocol/FetchVideoBroadcastPlayAndFeedbackCountGraphQLModels$FetchVideoBroadcastPlayAndFeedbackCountQueryModel;->j()I

    .line 1830820
    invoke-virtual {v0}, Lcom/facebook/feed/protocol/FetchVideoBroadcastPlayAndFeedbackCountGraphQLModels$FetchVideoBroadcastPlayAndFeedbackCountQueryModel;->j()I

    move-result v1

    .line 1830821
    if-lez v1, :cond_4

    .line 1830822
    iget-object v2, p0, LX/BuL;->b:Lcom/facebook/feed/video/fullscreen/FeedbackWithViewsAndCountPlugin;

    invoke-virtual {v2}, Lcom/facebook/feed/video/fullscreen/FeedbackWithViewsAndCountPlugin;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080d73

    const v4, 0x7f080d74

    new-array v5, v8, [Ljava/lang/Object;

    iget-object v6, p0, LX/BuL;->b:Lcom/facebook/feed/video/fullscreen/FeedbackWithViewsAndCountPlugin;

    iget-object v6, v6, LX/BuJ;->n:LX/154;

    invoke-virtual {v6, v1, v8}, LX/154;->a(II)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v2, v3, v4, v1, v5}, LX/1z0;->a(Landroid/content/res/Resources;III[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1830823
    iget-object v1, p0, LX/BuL;->b:Lcom/facebook/feed/video/fullscreen/FeedbackWithViewsAndCountPlugin;

    iget-object v1, v1, Lcom/facebook/feed/video/fullscreen/FeedbackWithViewsAndCountPlugin;->w:Lcom/facebook/resources/ui/FbTextView;

    if-nez v1, :cond_3

    .line 1830824
    iget-object v3, p0, LX/BuL;->b:Lcom/facebook/feed/video/fullscreen/FeedbackWithViewsAndCountPlugin;

    iget-object v1, p0, LX/BuL;->b:Lcom/facebook/feed/video/fullscreen/FeedbackWithViewsAndCountPlugin;

    iget-object v1, v1, LX/BuJ;->b:Landroid/view/ViewStub;

    invoke-virtual {v1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbTextView;

    .line 1830825
    iput-object v1, v3, Lcom/facebook/feed/video/fullscreen/FeedbackWithViewsAndCountPlugin;->w:Lcom/facebook/resources/ui/FbTextView;

    .line 1830826
    :cond_3
    iget-object v1, p0, LX/BuL;->b:Lcom/facebook/feed/video/fullscreen/FeedbackWithViewsAndCountPlugin;

    iget-object v1, v1, Lcom/facebook/feed/video/fullscreen/FeedbackWithViewsAndCountPlugin;->w:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1830827
    iget-object v1, p0, LX/BuL;->b:Lcom/facebook/feed/video/fullscreen/FeedbackWithViewsAndCountPlugin;

    iget-object v1, v1, Lcom/facebook/feed/video/fullscreen/FeedbackWithViewsAndCountPlugin;->w:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v7}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1830828
    :cond_4
    invoke-virtual {v0}, Lcom/facebook/feed/protocol/FetchVideoBroadcastPlayAndFeedbackCountGraphQLModels$FetchVideoBroadcastPlayAndFeedbackCountQueryModel;->a()Lcom/facebook/feed/protocol/FetchVideoBroadcastPlayAndFeedbackCountGraphQLModels$FetchVideoBroadcastPlayAndFeedbackCountQueryModel$FeedbackModel;

    move-result-object v1

    .line 1830829
    if-eqz v1, :cond_0

    .line 1830830
    iget-object v0, p0, LX/BuL;->b:Lcom/facebook/feed/video/fullscreen/FeedbackWithViewsAndCountPlugin;

    iget-object v0, v0, LX/BuJ;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1830831
    iget-object v2, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v2

    .line 1830832
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    .line 1830833
    if-eqz v0, :cond_0

    .line 1830834
    invoke-static {v0}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)LX/3dM;

    move-result-object v2

    .line 1830835
    invoke-virtual {v1}, Lcom/facebook/feed/protocol/FetchVideoBroadcastPlayAndFeedbackCountGraphQLModels$FetchVideoBroadcastPlayAndFeedbackCountQueryModel$FeedbackModel;->j()Lcom/facebook/feed/protocol/FetchVideoBroadcastPlayAndFeedbackCountGraphQLModels$FetchVideoBroadcastPlayAndFeedbackCountQueryModel$FeedbackModel$LikersModel;

    move-result-object v3

    .line 1830836
    if-eqz v3, :cond_5

    .line 1830837
    invoke-static {v0}, LX/16z;->n(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    move-result-object v4

    .line 1830838
    invoke-static {v4}, LX/6X9;->a(Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;)LX/6X9;

    move-result-object v4

    invoke-virtual {v3}, Lcom/facebook/feed/protocol/FetchVideoBroadcastPlayAndFeedbackCountGraphQLModels$FetchVideoBroadcastPlayAndFeedbackCountQueryModel$FeedbackModel$LikersModel;->a()I

    move-result v3

    invoke-virtual {v4, v3}, LX/6X9;->a(I)LX/6X9;

    move-result-object v3

    .line 1830839
    iget-object v4, v3, LX/6X9;->a:Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;

    move-object v3, v4

    .line 1830840
    invoke-virtual {v2, v3}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLLikersOfContentConnection;)LX/3dM;

    .line 1830841
    :cond_5
    invoke-virtual {v1}, Lcom/facebook/feed/protocol/FetchVideoBroadcastPlayAndFeedbackCountGraphQLModels$FetchVideoBroadcastPlayAndFeedbackCountQueryModel$FeedbackModel;->a()Lcom/facebook/feed/protocol/FetchVideoBroadcastPlayAndFeedbackCountGraphQLModels$FetchVideoBroadcastPlayAndFeedbackCountQueryModel$FeedbackModel$CommentsModel;

    move-result-object v1

    .line 1830842
    if-eqz v1, :cond_6

    .line 1830843
    invoke-static {v0}, LX/16z;->m(Lcom/facebook/graphql/model/GraphQLFeedback;)Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    move-result-object v0

    .line 1830844
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1830845
    new-instance v3, LX/6XC;

    invoke-direct {v3, v0}, LX/6XC;-><init>(Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;)V

    move-object v0, v3

    .line 1830846
    invoke-virtual {v1}, Lcom/facebook/feed/protocol/FetchVideoBroadcastPlayAndFeedbackCountGraphQLModels$FetchVideoBroadcastPlayAndFeedbackCountQueryModel$FeedbackModel$CommentsModel;->a()I

    move-result v3

    .line 1830847
    iget-object v4, v0, LX/6XC;->a:Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    .line 1830848
    invoke-virtual {v4, v3}, Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;->a(I)V

    .line 1830849
    move-object v0, v0

    .line 1830850
    invoke-virtual {v1}, Lcom/facebook/feed/protocol/FetchVideoBroadcastPlayAndFeedbackCountGraphQLModels$FetchVideoBroadcastPlayAndFeedbackCountQueryModel$FeedbackModel$CommentsModel;->a()I

    move-result v1

    .line 1830851
    iget-object v3, v0, LX/6XC;->a:Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    .line 1830852
    invoke-virtual {v3, v1}, Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;->a(I)V

    .line 1830853
    move-object v0, v0

    .line 1830854
    iget-object v1, v0, LX/6XC;->a:Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;

    move-object v0, v1

    .line 1830855
    invoke-virtual {v2, v0}, LX/3dM;->a(Lcom/facebook/graphql/model/GraphQLTopLevelCommentsConnection;)LX/3dM;

    .line 1830856
    :cond_6
    iget-object v0, p0, LX/BuL;->b:Lcom/facebook/feed/video/fullscreen/FeedbackWithViewsAndCountPlugin;

    iget-object v0, v0, LX/BuJ;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1830857
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 1830858
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object v0

    invoke-virtual {v2}, LX/3dM;->a()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    .line 1830859
    iput-object v1, v0, LX/23u;->D:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1830860
    move-object v0, v0

    .line 1830861
    invoke-virtual {v0}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 1830862
    iget-object v1, p0, LX/BuL;->b:Lcom/facebook/feed/video/fullscreen/FeedbackWithViewsAndCountPlugin;

    iget-object v2, p0, LX/BuL;->b:Lcom/facebook/feed/video/fullscreen/FeedbackWithViewsAndCountPlugin;

    iget-object v2, v2, LX/BuJ;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->b(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    iput-object v0, v1, Lcom/facebook/feed/video/fullscreen/FeedbackWithViewsAndCountPlugin;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1830863
    iget-object v1, p0, LX/BuL;->b:Lcom/facebook/feed/video/fullscreen/FeedbackWithViewsAndCountPlugin;

    iget-object v0, p0, LX/BuL;->b:Lcom/facebook/feed/video/fullscreen/FeedbackWithViewsAndCountPlugin;

    iget-object v0, v0, LX/BuJ;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1830864
    iget-object v2, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v2

    .line 1830865
    check-cast v0, LX/16n;

    invoke-virtual {v1, v0}, LX/BuJ;->a(LX/16n;)V

    goto/16 :goto_0
.end method
