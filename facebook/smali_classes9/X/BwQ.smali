.class public abstract LX/BwQ;
.super LX/3Gb;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/7Lf;",
        ">",
        "LX/3Gb",
        "<TE;>;"
    }
.end annotation


# static fields
.field public static final f:Ljava/lang/String;


# instance fields
.field public A:LX/2pa;

.field public B:Z

.field public a:LX/BUA;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0tQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/19w;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/16U;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final o:LX/BwP;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/BwQ",
            "<TE;>.VideoDownloadEventHandler;"
        }
    .end annotation
.end field

.field private final p:Landroid/os/Handler;

.field public final q:Landroid/view/View$OnClickListener;

.field private final r:Landroid/view/View$OnClickListener;

.field private final s:Lcom/facebook/fbui/glyph/GlyphView;

.field public final t:Lcom/facebook/widget/FacebookProgressCircleViewAnimated;

.field public final u:Lcom/facebook/widget/text/BetterTextView;

.field public final v:Lcom/facebook/widget/text/BetterTextView;

.field public final w:Landroid/view/View;

.field private final x:Ljava/lang/String;

.field public y:LX/2fs;

.field public z:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1833759
    const-class v0, LX/BwQ;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/BwQ;->f:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    .prologue
    .line 1833741
    invoke-direct {p0, p1, p2, p3}, LX/3Gb;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1833742
    new-instance v0, LX/BwP;

    invoke-direct {v0, p0}, LX/BwP;-><init>(LX/BwQ;)V

    iput-object v0, p0, LX/BwQ;->o:LX/BwP;

    .line 1833743
    new-instance v0, LX/BwO;

    invoke-direct {v0, p0}, LX/BwO;-><init>(LX/BwQ;)V

    iput-object v0, p0, LX/BwQ;->q:Landroid/view/View$OnClickListener;

    .line 1833744
    new-instance v0, LX/BwN;

    invoke-direct {v0, p0}, LX/BwN;-><init>(LX/BwQ;)V

    iput-object v0, p0, LX/BwQ;->r:Landroid/view/View$OnClickListener;

    .line 1833745
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, LX/BwQ;

    invoke-static {v0}, LX/BUA;->a(LX/0QB;)LX/BUA;

    move-result-object v3

    check-cast v3, LX/BUA;

    invoke-static {v0}, LX/0tQ;->a(LX/0QB;)LX/0tQ;

    move-result-object v4

    check-cast v4, LX/0tQ;

    invoke-static {v0}, LX/19w;->a(LX/0QB;)LX/19w;

    move-result-object p2

    check-cast p2, LX/19w;

    invoke-static {v0}, LX/16U;->a(LX/0QB;)LX/16U;

    move-result-object p3

    check-cast p3, LX/16U;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v3, v2, LX/BwQ;->a:LX/BUA;

    iput-object v4, v2, LX/BwQ;->b:LX/0tQ;

    iput-object p2, v2, LX/BwQ;->c:LX/19w;

    iput-object p3, v2, LX/BwQ;->d:LX/16U;

    iput-object v0, v2, LX/BwQ;->e:Ljava/util/concurrent/Executor;

    .line 1833746
    invoke-virtual {p0}, LX/BwQ;->getContentView()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1833747
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, LX/BwQ;->p:Landroid/os/Handler;

    .line 1833748
    const v0, 0x7f0d07f4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, LX/BwQ;->s:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1833749
    const v0, 0x7f0d07f3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/FacebookProgressCircleViewAnimated;

    iput-object v0, p0, LX/BwQ;->t:Lcom/facebook/widget/FacebookProgressCircleViewAnimated;

    .line 1833750
    const v0, 0x7f0d07f5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/BwQ;->u:Lcom/facebook/widget/text/BetterTextView;

    .line 1833751
    const v0, 0x7f0d07f6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/BwQ;->v:Lcom/facebook/widget/text/BetterTextView;

    .line 1833752
    const v0, 0x7f0d07f2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/BwQ;->w:Landroid/view/View;

    .line 1833753
    iget-object v0, p0, LX/BwQ;->b:LX/0tQ;

    .line 1833754
    iget-object v1, v0, LX/0tQ;->a:LX/0ad;

    sget-char v2, LX/0wh;->c:C

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1833755
    if-nez v0, :cond_0

    .line 1833756
    const v0, 0x7f080da4

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1833757
    :cond_0
    iput-object v0, p0, LX/BwQ;->x:Ljava/lang/String;

    .line 1833758
    return-void
.end method

.method public static a$redex0(LX/BwQ;Ljava/lang/String;LX/2fs;)V
    .locals 3

    .prologue
    .line 1833738
    iget-object v0, p0, LX/BwQ;->A:LX/2pa;

    invoke-static {v0}, LX/393;->f(LX/2pa;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1833739
    iget-object v0, p0, LX/BwQ;->p:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/feed/video/inline/BufferVideoBasePlugin$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/facebook/feed/video/inline/BufferVideoBasePlugin$1;-><init>(LX/BwQ;Ljava/lang/String;LX/2fs;)V

    const v2, 0xd4233f4

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1833740
    :cond_0
    return-void
.end method

.method public static getProgressPercent(LX/BwQ;)F
    .locals 5

    .prologue
    const-wide/16 v0, 0x0

    .line 1833732
    iget-object v2, p0, LX/BwQ;->A:LX/2pa;

    invoke-static {v2}, LX/393;->e(LX/2pa;)J

    move-result-wide v2

    .line 1833733
    cmp-long v4, v2, v0

    if-eqz v4, :cond_1

    .line 1833734
    iget-object v4, p0, LX/BwQ;->y:LX/2fs;

    if-eqz v4, :cond_0

    .line 1833735
    iget-object v0, p0, LX/BwQ;->y:LX/2fs;

    iget-wide v0, v0, LX/2fs;->b:J

    .line 1833736
    :cond_0
    long-to-float v0, v0

    const/high16 v1, 0x42c80000    # 100.0f

    mul-float/2addr v0, v1

    long-to-float v1, v2

    div-float/2addr v0, v1

    .line 1833737
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getProgressString(LX/BwQ;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 1833725
    iget-object v0, p0, LX/BwQ;->y:LX/2fs;

    iget-wide v0, v0, LX/2fs;->b:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 1833726
    const-string v0, ""

    .line 1833727
    :goto_0
    return-object v0

    .line 1833728
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1833729
    const-string v1, "%.2f"

    invoke-static {p0}, LX/BwQ;->getProgressPercent(LX/BwQ;)F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1833730
    const-string v1, "%"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1833731
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private setDownloadOptionVisibility(I)V
    .locals 2

    .prologue
    .line 1833666
    iget-object v0, p0, LX/BwQ;->s:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1833667
    iget-object v0, p0, LX/BwQ;->u:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 1833668
    iget-object v0, p0, LX/BwQ;->y:LX/2fs;

    iget-object v0, v0, LX/2fs;->c:LX/1A0;

    sget-object v1, LX/1A0;->DOWNLOAD_NOT_REQUESTED:LX/1A0;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/BwQ;->y:LX/2fs;

    iget-object v0, v0, LX/2fs;->c:LX/1A0;

    sget-object v1, LX/1A0;->DOWNLOAD_ABORTED:LX/1A0;

    if-ne v0, v1, :cond_1

    .line 1833669
    :cond_0
    iget-object v0, p0, LX/BwQ;->v:Lcom/facebook/widget/text/BetterTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 1833670
    :goto_0
    return-void

    .line 1833671
    :cond_1
    iget-object v0, p0, LX/BwQ;->v:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public static u(LX/BwQ;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 1833717
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1833718
    iget-object v1, p0, LX/BwQ;->y:LX/2fs;

    iget-wide v2, v1, LX/2fs;->b:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    .line 1833719
    invoke-virtual {p0}, LX/BwQ;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080da5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 1833720
    :goto_0
    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1833721
    iget-object v1, p0, LX/BwQ;->b:LX/0tQ;

    invoke-virtual {v1}, LX/0tQ;->p()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1833722
    iget-object v1, p0, LX/BwQ;->A:LX/2pa;

    invoke-static {v1}, LX/393;->e(LX/2pa;)J

    move-result-wide v2

    invoke-static {v2, v3}, LX/15V;->a(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1833723
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1833724
    :cond_1
    invoke-virtual {p0}, LX/BwQ;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080da6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public static v(LX/BwQ;)V
    .locals 5

    .prologue
    const/16 v2, 0x8

    const/4 v4, 0x0

    .line 1833696
    iget-object v0, p0, LX/BwQ;->w:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    .line 1833697
    :goto_0
    return-void

    .line 1833698
    :cond_0
    iget-object v0, p0, LX/BwQ;->A:LX/2pa;

    invoke-static {v0}, LX/393;->f(LX/2pa;)Ljava/lang/String;

    .line 1833699
    sget-object v0, LX/BwM;->a:[I

    iget-object v1, p0, LX/BwQ;->y:LX/2fs;

    iget-object v1, v1, LX/2fs;->c:LX/1A0;

    invoke-virtual {v1}, LX/1A0;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1833700
    invoke-direct {p0, v4}, LX/BwQ;->setDownloadOptionVisibility(I)V

    .line 1833701
    iget-object v0, p0, LX/BwQ;->s:Lcom/facebook/fbui/glyph/GlyphView;

    const v1, 0x7f020841

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 1833702
    iget-object v0, p0, LX/BwQ;->u:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, p0, LX/BwQ;->x:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1833703
    iget-object v0, p0, LX/BwQ;->v:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, p0, LX/BwQ;->A:LX/2pa;

    invoke-static {v1}, LX/393;->e(LX/2pa;)J

    move-result-wide v2

    invoke-static {v2, v3}, LX/15V;->a(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1833704
    iget-object v0, p0, LX/BwQ;->w:Landroid/view/View;

    iget-object v1, p0, LX/BwQ;->q:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1833705
    iget-object v0, p0, LX/BwQ;->t:Lcom/facebook/widget/FacebookProgressCircleViewAnimated;

    invoke-static {p0}, LX/BwQ;->getProgressPercent(LX/BwQ;)F

    move-result v1

    float-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Lcom/facebook/widget/FacebookProgressCircleView;->setProgress(J)V

    goto :goto_0

    .line 1833706
    :pswitch_0
    iget-object v0, p0, LX/BwQ;->s:Lcom/facebook/fbui/glyph/GlyphView;

    const v1, 0x7f020818

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 1833707
    iget-object v0, p0, LX/BwQ;->u:Lcom/facebook/widget/text/BetterTextView;

    invoke-static {p0}, LX/BwQ;->u(LX/BwQ;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1833708
    iget-object v0, p0, LX/BwQ;->v:Lcom/facebook/widget/text/BetterTextView;

    invoke-static {p0}, LX/BwQ;->getProgressString(LX/BwQ;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1833709
    iget-object v0, p0, LX/BwQ;->t:Lcom/facebook/widget/FacebookProgressCircleViewAnimated;

    invoke-static {p0}, LX/BwQ;->getProgressPercent(LX/BwQ;)F

    move-result v1

    float-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Lcom/facebook/widget/FacebookProgressCircleView;->setProgress(J)V

    .line 1833710
    iget-object v0, p0, LX/BwQ;->w:Landroid/view/View;

    iget-object v1, p0, LX/BwQ;->r:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1833711
    invoke-direct {p0, v4}, LX/BwQ;->setDownloadOptionVisibility(I)V

    goto :goto_0

    .line 1833712
    :pswitch_1
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    if-eqz v0, :cond_1

    .line 1833713
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    iget-object v1, p0, LX/BwQ;->A:LX/2pa;

    iget-object v1, v1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    invoke-virtual {v0, v1}, LX/2pb;->a(Lcom/facebook/video/engine/VideoPlayerParams;)V

    .line 1833714
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    sget-object v1, LX/04g;->BY_AUTOPLAY:LX/04g;

    invoke-virtual {v0, v1}, LX/2pb;->a(LX/04g;)V

    .line 1833715
    :cond_1
    invoke-direct {p0, v2}, LX/BwQ;->setDownloadOptionVisibility(I)V

    goto/16 :goto_0

    .line 1833716
    :pswitch_2
    invoke-direct {p0, v2}, LX/BwQ;->setDownloadOptionVisibility(I)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public declared-synchronized a(LX/2pa;Z)V
    .locals 4

    .prologue
    .line 1833678
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, LX/BwQ;->A:LX/2pa;

    .line 1833679
    if-eqz p2, :cond_0

    .line 1833680
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/BwQ;->B:Z

    .line 1833681
    iget-object v0, p0, LX/BwQ;->d:LX/16U;

    const-class v1, LX/1ub;

    iget-object v2, p0, LX/BwQ;->o:LX/BwP;

    invoke-virtual {v0, v1, v2}, LX/16V;->a(Ljava/lang/Class;LX/16Y;)V

    .line 1833682
    :cond_0
    iget-object v0, p0, LX/2oy;->k:Lcom/facebook/video/player/RichVideoPlayer;

    if-eqz v0, :cond_1

    .line 1833683
    iget-object v0, p0, LX/2oy;->k:Lcom/facebook/video/player/RichVideoPlayer;

    .line 1833684
    iget-object v1, v0, Lcom/facebook/video/player/RichVideoPlayer;->H:LX/04D;

    move-object v0, v1

    .line 1833685
    iget-object v0, v0, LX/04D;->origin:Ljava/lang/String;

    iput-object v0, p0, LX/BwQ;->z:Ljava/lang/String;

    .line 1833686
    :goto_0
    iget-object v0, p0, LX/BwQ;->c:LX/19w;

    iget-object v1, p0, LX/BwQ;->A:LX/2pa;

    invoke-static {v1}, LX/393;->f(LX/2pa;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/19w;->c(Ljava/lang/String;)LX/2fs;

    move-result-object v0

    iput-object v0, p0, LX/BwQ;->y:LX/2fs;

    .line 1833687
    iget-object v0, p0, LX/BwQ;->t:Lcom/facebook/widget/FacebookProgressCircleViewAnimated;

    invoke-static {p0}, LX/BwQ;->getProgressPercent(LX/BwQ;)F

    move-result v1

    float-to-double v2, v1

    invoke-virtual {v0, v2, v3}, Lcom/facebook/widget/FacebookProgressCircleView;->setProgress(D)V

    .line 1833688
    iget-object v0, p0, LX/BwQ;->y:LX/2fs;

    iget-boolean v0, v0, LX/2fs;->e:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/BwQ;->c:LX/19w;

    iget-object v1, p0, LX/BwQ;->A:LX/2pa;

    invoke-static {v1}, LX/393;->f(LX/2pa;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/19w;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1833689
    iget-object v0, p0, LX/BwQ;->w:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1833690
    invoke-static {p0}, LX/BwQ;->v(LX/BwQ;)V

    .line 1833691
    :goto_1
    iget-object v0, p0, LX/BwQ;->A:LX/2pa;

    invoke-static {v0}, LX/393;->f(LX/2pa;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1833692
    monitor-exit p0

    return-void

    .line 1833693
    :cond_1
    :try_start_1
    sget-object v0, LX/04D;->UNKNOWN:LX/04D;

    iget-object v0, v0, LX/04D;->origin:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/BwQ;->z:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1833694
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1833695
    :cond_2
    :try_start_2
    iget-object v0, p0, LX/BwQ;->w:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method public declared-synchronized d()V
    .locals 3

    .prologue
    .line 1833675
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/BwQ;->d:LX/16U;

    const-class v1, LX/1ub;

    iget-object v2, p0, LX/BwQ;->o:LX/BwP;

    invoke-virtual {v0, v1, v2}, LX/16V;->b(Ljava/lang/Class;LX/16Y;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1833676
    monitor-exit p0

    return-void

    .line 1833677
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final g()V
    .locals 2

    .prologue
    .line 1833673
    iget-object v0, p0, LX/BwQ;->w:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1833674
    return-void
.end method

.method public abstract getContentView()I
.end method

.method public final i()Z
    .locals 2

    .prologue
    .line 1833672
    iget-object v0, p0, LX/BwQ;->y:LX/2fs;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/BwQ;->y:LX/2fs;

    iget-boolean v0, v0, LX/2fs;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/BwQ;->y:LX/2fs;

    iget-object v0, v0, LX/2fs;->c:LX/1A0;

    sget-object v1, LX/1A0;->DOWNLOAD_NOT_REQUESTED:LX/1A0;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/BwQ;->y:LX/2fs;

    iget-object v0, v0, LX/2fs;->c:LX/1A0;

    sget-object v1, LX/1A0;->DOWNLOAD_COMPLETED:LX/1A0;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/BwQ;->y:LX/2fs;

    iget-object v0, v0, LX/2fs;->c:LX/1A0;

    sget-object v1, LX/1A0;->DOWNLOAD_ABORTED:LX/1A0;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
