.class public final LX/Bpi;
.super LX/1Yy;
.source ""


# instance fields
.field public final synthetic a:LX/Bpl;


# direct methods
.method public constructor <init>(LX/Bpl;)V
    .locals 0

    .prologue
    .line 1823485
    iput-object p1, p0, LX/Bpi;->a:LX/Bpl;

    invoke-direct {p0}, LX/1Yy;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 5

    .prologue
    .line 1823486
    check-cast p1, LX/1Zj;

    .line 1823487
    iget-object v0, p0, LX/Bpi;->a:LX/Bpl;

    iget-object v0, v0, LX/Bpl;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v1, p1, LX/1Zj;->a:Ljava/lang/String;

    invoke-static {v0, v1}, LX/182;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 1823488
    if-eqz v1, :cond_0

    .line 1823489
    iget-object v0, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1823490
    if-eqz v0, :cond_0

    .line 1823491
    iget-object v0, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1823492
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->w()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1823493
    :cond_0
    :goto_0
    return-void

    .line 1823494
    :cond_1
    new-instance v2, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    invoke-static {v1}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v0

    const-string v3, "photos_feed_ufi"

    const-string v4, "photos_feed"

    invoke-direct {v2, v0, v3, v4}, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;-><init>(LX/162;Ljava/lang/String;Ljava/lang/String;)V

    .line 1823495
    iget-object v0, p0, LX/Bpi;->a:LX/Bpl;

    iget-object v3, v0, LX/Bpl;->b:LX/3iK;

    .line 1823496
    iget-object v0, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1823497
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-virtual {v3, v0, v2}, LX/3iK;->a(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)V

    goto :goto_0
.end method
