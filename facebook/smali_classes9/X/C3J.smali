.class public LX/C3J;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pb;",
        ":",
        "LX/1Pc;",
        ":",
        "LX/1Pd;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public final a:LX/1xv;

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1VD;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3my;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/1xv;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/1VD;",
            ">;",
            "LX/1xv;",
            "LX/0Ot",
            "<",
            "LX/3my;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1845397
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1845398
    iput-object p1, p0, LX/C3J;->b:LX/0Ot;

    .line 1845399
    iput-object p2, p0, LX/C3J;->a:LX/1xv;

    .line 1845400
    iput-object p3, p0, LX/C3J;->c:LX/0Ot;

    .line 1845401
    return-void
.end method

.method public static a(LX/0QB;)LX/C3J;
    .locals 6

    .prologue
    .line 1845402
    const-class v1, LX/C3J;

    monitor-enter v1

    .line 1845403
    :try_start_0
    sget-object v0, LX/C3J;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1845404
    sput-object v2, LX/C3J;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1845405
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1845406
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1845407
    new-instance v4, LX/C3J;

    const/16 v3, 0x71f

    invoke-static {v0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static {v0}, LX/1xv;->a(LX/0QB;)LX/1xv;

    move-result-object v3

    check-cast v3, LX/1xv;

    const/16 p0, 0xb2c

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v4, v5, v3, p0}, LX/C3J;-><init>(LX/0Ot;LX/1xv;LX/0Ot;)V

    .line 1845408
    move-object v0, v4

    .line 1845409
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1845410
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C3J;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1845411
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1845412
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
