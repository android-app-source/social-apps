.class public LX/AcB;
.super LX/2oy;
.source ""


# instance fields
.field public a:Lcom/facebook/fbui/glyph/GlyphView;

.field public b:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1692212
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/AcB;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1692213
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1692214
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/AcB;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1692215
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1692216
    invoke-direct {p0, p1, p2, p3}, LX/2oy;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1692217
    const p1, 0x7f030279

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1692218
    const p1, 0x7f0d091c

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object p1, p0, LX/AcB;->a:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1692219
    iget-object p1, p0, LX/2oy;->h:Ljava/util/List;

    new-instance p2, LX/Ac9;

    invoke-direct {p2, p0}, LX/Ac9;-><init>(LX/AcB;)V

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1692220
    iget-object p1, p0, LX/2oy;->h:Ljava/util/List;

    new-instance p2, LX/AcA;

    invoke-direct {p2, p0}, LX/AcA;-><init>(LX/AcB;)V

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1692221
    new-instance p1, LX/Ac7;

    invoke-direct {p1, p0}, LX/Ac7;-><init>(LX/AcB;)V

    iput-object p1, p0, LX/AcB;->b:Landroid/view/View$OnClickListener;

    .line 1692222
    iget-object p1, p0, LX/AcB;->a:Lcom/facebook/fbui/glyph/GlyphView;

    iget-object p2, p0, LX/AcB;->b:Landroid/view/View$OnClickListener;

    invoke-virtual {p1, p2}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1692223
    iget-object p1, p0, LX/AcB;->a:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {p0}, LX/AcB;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    const p3, 0x7f0b053e

    invoke-virtual {p2, p3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p2

    invoke-static {p1, p2}, LX/190;->a(Landroid/view/View;I)Landroid/view/TouchDelegate;

    move-result-object p1

    invoke-virtual {p0, p1}, LX/AcB;->setTouchDelegate(Landroid/view/TouchDelegate;)V

    .line 1692224
    return-void
.end method

.method public static i(LX/AcB;)V
    .locals 4

    .prologue
    .line 1692225
    invoke-virtual {p0}, LX/AcB;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0628

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 1692226
    iget-object v1, p0, LX/AcB;->a:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1692227
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1692228
    iput v0, v2, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    .line 1692229
    iput v0, v2, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 1692230
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 p0, 0x11

    if-lt v3, p0, :cond_0

    .line 1692231
    invoke-virtual {v2, v0}, Landroid/widget/RelativeLayout$LayoutParams;->setMarginEnd(I)V

    .line 1692232
    :cond_0
    invoke-virtual {v1, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1692233
    return-void
.end method


# virtual methods
.method public final a(LX/2pa;Z)V
    .locals 2

    .prologue
    .line 1692234
    invoke-super {p0, p1, p2}, LX/2oy;->a(LX/2pa;Z)V

    .line 1692235
    if-eqz p2, :cond_0

    .line 1692236
    iget-object v0, p0, LX/AcB;->a:Lcom/facebook/fbui/glyph/GlyphView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1692237
    :cond_0
    invoke-static {p0}, LX/AcB;->i(LX/AcB;)V

    .line 1692238
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 1692239
    invoke-super {p0}, LX/2oy;->d()V

    .line 1692240
    iget-object v0, p0, LX/AcB;->a:Lcom/facebook/fbui/glyph/GlyphView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1692241
    return-void
.end method
