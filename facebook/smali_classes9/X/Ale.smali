.class public abstract LX/Ale;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements LX/AkB;


# instance fields
.field public a:LX/0hB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field public c:Landroid/view/View;

.field public d:Landroid/animation/ValueAnimator;

.field public e:Landroid/animation/ValueAnimator;

.field public f:Landroid/animation/ObjectAnimator;

.field private g:I

.field public h:I

.field public i:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1709984
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 1709985
    invoke-virtual {p0}, LX/Ale;->b()V

    .line 1709986
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1709981
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1709982
    invoke-virtual {p0}, LX/Ale;->b()V

    .line 1709983
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1709978
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1709979
    invoke-virtual {p0}, LX/Ale;->b()V

    .line 1709980
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/Ale;

    invoke-static {p0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object p0

    check-cast p0, LX/0hB;

    iput-object p0, p1, LX/Ale;->a:LX/0hB;

    return-void
.end method


# virtual methods
.method public b()V
    .locals 2

    .prologue
    .line 1709959
    const-class v0, LX/Ale;

    invoke-static {v0, p0}, LX/Ale;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1709960
    const v0, 0x7f03063e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1709961
    invoke-virtual {p0}, LX/Ale;->getImageTrayHeight()I

    move-result v0

    iput v0, p0, LX/Ale;->g:I

    .line 1709962
    const v0, 0x7f0d1139

    invoke-virtual {p0, v0}, LX/Ale;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/Ale;->c:Landroid/view/View;

    .line 1709963
    iget-object v0, p0, LX/Ale;->a:LX/0hB;

    invoke-virtual {v0}, LX/0hB;->c()I

    move-result v0

    iput v0, p0, LX/Ale;->h:I

    .line 1709964
    invoke-virtual {p0}, LX/Ale;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1709965
    const v0, 0x7f0d113a

    invoke-virtual {p0, v0}, LX/Ale;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 1709966
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/photoreminder/common/SuperPhotoReminderGridView;

    iput-object v0, p0, LX/Ale;->b:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 1709967
    iget-object v0, p0, LX/Ale;->b:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget v1, p0, LX/Ale;->h:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setTranslationX(F)V

    .line 1709968
    :goto_0
    iget v0, p0, LX/Ale;->g:I

    invoke-virtual {p0, v0}, LX/Ale;->setMinimumHeight(I)V

    .line 1709969
    iget-object v0, p0, LX/Ale;->c:Landroid/view/View;

    iget v1, p0, LX/Ale;->g:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setMinimumHeight(I)V

    .line 1709970
    iget-object v0, p0, LX/Ale;->d:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Ale;->e:Landroid/animation/ValueAnimator;

    if-nez v0, :cond_1

    .line 1709971
    :cond_0
    invoke-virtual {p0}, LX/Ale;->f()Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, LX/Ale;->d:Landroid/animation/ValueAnimator;

    .line 1709972
    invoke-virtual {p0}, LX/Ale;->g()Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, LX/Ale;->e:Landroid/animation/ValueAnimator;

    .line 1709973
    invoke-virtual {p0}, LX/Ale;->j()Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, LX/Ale;->f:Landroid/animation/ObjectAnimator;

    .line 1709974
    :cond_1
    return-void

    .line 1709975
    :cond_2
    const v0, 0x7f0d113b

    invoke-virtual {p0, v0}, LX/Ale;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 1709976
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/photoreminder/common/PromptHScrollView;

    iput-object v0, p0, LX/Ale;->b:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 1709977
    iget-object v0, p0, LX/Ale;->b:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget v1, p0, LX/Ale;->h:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setTranslationX(F)V

    goto :goto_0
.end method

.method public abstract e()Z
.end method

.method public f()Landroid/animation/ValueAnimator;
    .locals 4

    .prologue
    .line 1709953
    iget-object v0, p0, LX/Ale;->c:Landroid/view/View;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1709954
    const/4 v0, 0x2

    new-array v0, v0, [I

    const/4 v1, 0x0

    iget-object v2, p0, LX/Ale;->c:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, LX/Ale;->g:I

    aput v2, v0, v1

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v0

    .line 1709955
    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 1709956
    new-instance v1, LX/Alk;

    invoke-direct {v1, p0}, LX/Alk;-><init>(LX/Ale;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 1709957
    new-instance v1, LX/All;

    invoke-direct {v1, p0}, LX/All;-><init>(LX/Ale;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1709958
    return-object v0
.end method

.method public g()Landroid/animation/ValueAnimator;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1709948
    const/4 v0, 0x2

    new-array v0, v0, [I

    iget v1, p0, LX/Ale;->g:I

    aput v1, v0, v2

    const/4 v1, 0x1

    aput v2, v0, v1

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v0

    .line 1709949
    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 1709950
    new-instance v1, LX/Alm;

    invoke-direct {v1, p0}, LX/Alm;-><init>(LX/Ale;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 1709951
    new-instance v1, LX/Aln;

    invoke-direct {v1, p0}, LX/Aln;-><init>(LX/Ale;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1709952
    return-object v0
.end method

.method public abstract getImageTrayHeight()I
.end method

.method public getMinimumHeight()I
    .locals 2

    .prologue
    .line 1709945
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_0

    .line 1709946
    invoke-virtual {p0}, LX/Ale;->getSuggestedMinimumHeight()I

    move-result v0

    .line 1709947
    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->getMinimumHeight()I

    move-result v0

    goto :goto_0
.end method

.method public getPhotoTray()Landroid/view/View;
    .locals 1

    .prologue
    .line 1709944
    const v0, 0x7f0d250b

    invoke-virtual {p0, v0}, LX/Ale;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public abstract h()Z
.end method

.method public abstract i()Z
.end method

.method public j()Landroid/animation/ObjectAnimator;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1709935
    iget-object v0, p0, LX/Ale;->b:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    const-string v1, "translationX"

    const/4 v2, 0x2

    new-array v2, v2, [F

    iget v3, p0, LX/Ale;->h:I

    int-to-float v3, v3

    aput v3, v2, v5

    const/4 v3, 0x1

    const/4 v4, 0x0

    aput v4, v2, v3

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 1709936
    const-wide/16 v2, 0x258

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 1709937
    new-instance v1, LX/Alo;

    invoke-direct {v1, p0}, LX/Alo;-><init>(LX/Ale;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1709938
    return-object v0
.end method

.method public abstract setHasBeenShown(Z)V
.end method

.method public setHeight(I)V
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SetLayoutParams"
        }
    .end annotation

    .prologue
    .line 1709939
    invoke-virtual {p0}, LX/Ale;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    instance-of v0, v0, Landroid/view/ViewGroup$LayoutParams;

    const-string v1, "expected the container to be a ViewGroup, update ScrollingImagePromptView"

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1709940
    invoke-virtual {p0}, LX/Ale;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 1709941
    iput p1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1709942
    invoke-virtual {p0, v0}, LX/Ale;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1709943
    return-void
.end method

.method public abstract setIsAnimationRunning(Z)V
.end method
