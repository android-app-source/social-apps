.class public LX/B7U;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0tX;

.field public final b:LX/03V;


# direct methods
.method public constructor <init>(LX/0tX;LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1748066
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1748067
    iput-object p1, p0, LX/B7U;->a:LX/0tX;

    .line 1748068
    iput-object p2, p0, LX/B7U;->b:LX/03V;

    .line 1748069
    return-void
.end method

.method public static a()LX/0jT;
    .locals 10

    .prologue
    .line 1748070
    new-instance v0, LX/ADf;

    invoke-direct {v0}, LX/ADf;-><init>()V

    const/4 v1, 0x1

    .line 1748071
    iput-boolean v1, v0, LX/ADf;->a:Z

    .line 1748072
    move-object v0, v0

    .line 1748073
    const/4 v6, 0x1

    const/4 v9, 0x0

    const/4 v4, 0x0

    .line 1748074
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1748075
    iget-object v3, v0, LX/ADf;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1748076
    iget-object v5, v0, LX/ADf;->c:Ljava/lang/String;

    invoke-virtual {v2, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1748077
    iget-object v7, v0, LX/ADf;->d:Ljava/lang/String;

    invoke-virtual {v2, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1748078
    const/4 v8, 0x4

    invoke-virtual {v2, v8}, LX/186;->c(I)V

    .line 1748079
    iget-boolean v8, v0, LX/ADf;->a:Z

    invoke-virtual {v2, v9, v8}, LX/186;->a(IZ)V

    .line 1748080
    invoke-virtual {v2, v6, v3}, LX/186;->b(II)V

    .line 1748081
    const/4 v3, 0x2

    invoke-virtual {v2, v3, v5}, LX/186;->b(II)V

    .line 1748082
    const/4 v3, 0x3

    invoke-virtual {v2, v3, v7}, LX/186;->b(II)V

    .line 1748083
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1748084
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1748085
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1748086
    invoke-virtual {v3, v9}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1748087
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1748088
    new-instance v3, Lcom/facebook/api/graphql/leadgen/LeadGenDeepLinkUserInfoCreateMutationModels$LeadGenDeepLinkUserInfoCoreMutationFieldsModel$LeadGenDeepLinkUserStatusModel;

    invoke-direct {v3, v2}, Lcom/facebook/api/graphql/leadgen/LeadGenDeepLinkUserInfoCreateMutationModels$LeadGenDeepLinkUserInfoCoreMutationFieldsModel$LeadGenDeepLinkUserStatusModel;-><init>(LX/15i;)V

    .line 1748089
    move-object v0, v3

    .line 1748090
    new-instance v1, LX/ADe;

    invoke-direct {v1}, LX/ADe;-><init>()V

    .line 1748091
    iput-object v0, v1, LX/ADe;->a:Lcom/facebook/api/graphql/leadgen/LeadGenDeepLinkUserInfoCreateMutationModels$LeadGenDeepLinkUserInfoCoreMutationFieldsModel$LeadGenDeepLinkUserStatusModel;

    .line 1748092
    move-object v0, v1

    .line 1748093
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 1748094
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1748095
    iget-object v3, v0, LX/ADe;->a:Lcom/facebook/api/graphql/leadgen/LeadGenDeepLinkUserInfoCreateMutationModels$LeadGenDeepLinkUserInfoCoreMutationFieldsModel$LeadGenDeepLinkUserStatusModel;

    invoke-static {v2, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1748096
    invoke-virtual {v2, v6}, LX/186;->c(I)V

    .line 1748097
    invoke-virtual {v2, v5, v3}, LX/186;->b(II)V

    .line 1748098
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1748099
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1748100
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1748101
    invoke-virtual {v3, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1748102
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1748103
    new-instance v3, Lcom/facebook/api/graphql/leadgen/LeadGenDeepLinkUserInfoCreateMutationModels$LeadGenDeepLinkUserInfoCoreMutationFieldsModel;

    invoke-direct {v3, v2}, Lcom/facebook/api/graphql/leadgen/LeadGenDeepLinkUserInfoCreateMutationModels$LeadGenDeepLinkUserInfoCoreMutationFieldsModel;-><init>(LX/15i;)V

    .line 1748104
    move-object v0, v3

    .line 1748105
    return-object v0
.end method
