.class public final enum LX/CYC;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/CYC;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/CYC;

.field public static final enum AUTO_PROVISION_CALL_CTA:LX/CYC;

.field public static final enum AUTO_PROVISION_MESSENGER_CTA:LX/CYC;

.field public static final enum AUTO_PROVISION_SHOP_CTA:LX/CYC;

.field public static final enum CREATE_CTA:LX/CYC;

.field public static final enum EDIT_CTA:LX/CYC;

.field public static final enum NONE:LX/CYC;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1910843
    new-instance v0, LX/CYC;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v3}, LX/CYC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CYC;->NONE:LX/CYC;

    .line 1910844
    new-instance v0, LX/CYC;

    const-string v1, "AUTO_PROVISION_SHOP_CTA"

    invoke-direct {v0, v1, v4}, LX/CYC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CYC;->AUTO_PROVISION_SHOP_CTA:LX/CYC;

    .line 1910845
    new-instance v0, LX/CYC;

    const-string v1, "AUTO_PROVISION_CALL_CTA"

    invoke-direct {v0, v1, v5}, LX/CYC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CYC;->AUTO_PROVISION_CALL_CTA:LX/CYC;

    .line 1910846
    new-instance v0, LX/CYC;

    const-string v1, "AUTO_PROVISION_MESSENGER_CTA"

    invoke-direct {v0, v1, v6}, LX/CYC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CYC;->AUTO_PROVISION_MESSENGER_CTA:LX/CYC;

    .line 1910847
    new-instance v0, LX/CYC;

    const-string v1, "CREATE_CTA"

    invoke-direct {v0, v1, v7}, LX/CYC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CYC;->CREATE_CTA:LX/CYC;

    .line 1910848
    new-instance v0, LX/CYC;

    const-string v1, "EDIT_CTA"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/CYC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CYC;->EDIT_CTA:LX/CYC;

    .line 1910849
    const/4 v0, 0x6

    new-array v0, v0, [LX/CYC;

    sget-object v1, LX/CYC;->NONE:LX/CYC;

    aput-object v1, v0, v3

    sget-object v1, LX/CYC;->AUTO_PROVISION_SHOP_CTA:LX/CYC;

    aput-object v1, v0, v4

    sget-object v1, LX/CYC;->AUTO_PROVISION_CALL_CTA:LX/CYC;

    aput-object v1, v0, v5

    sget-object v1, LX/CYC;->AUTO_PROVISION_MESSENGER_CTA:LX/CYC;

    aput-object v1, v0, v6

    sget-object v1, LX/CYC;->CREATE_CTA:LX/CYC;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/CYC;->EDIT_CTA:LX/CYC;

    aput-object v2, v0, v1

    sput-object v0, LX/CYC;->$VALUES:[LX/CYC;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1910842
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/CYC;
    .locals 1

    .prologue
    .line 1910840
    const-class v0, LX/CYC;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/CYC;

    return-object v0
.end method

.method public static values()[LX/CYC;
    .locals 1

    .prologue
    .line 1910841
    sget-object v0, LX/CYC;->$VALUES:[LX/CYC;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/CYC;

    return-object v0
.end method
