.class public LX/CE0;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/CDy;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CE3;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1860488
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/CE0;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/CE3;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1860464
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1860465
    iput-object p1, p0, LX/CE0;->b:LX/0Ot;

    .line 1860466
    return-void
.end method

.method public static a(LX/0QB;)LX/CE0;
    .locals 4

    .prologue
    .line 1860477
    const-class v1, LX/CE0;

    monitor-enter v1

    .line 1860478
    :try_start_0
    sget-object v0, LX/CE0;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1860479
    sput-object v2, LX/CE0;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1860480
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1860481
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1860482
    new-instance v3, LX/CE0;

    const/16 p0, 0x220d

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/CE0;-><init>(LX/0Ot;)V

    .line 1860483
    move-object v0, v3

    .line 1860484
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1860485
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/CE0;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1860486
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1860487
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 13

    .prologue
    .line 1860489
    check-cast p2, LX/CDz;

    .line 1860490
    iget-object v0, p0, LX/CE0;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v1, p2, LX/CDz;->a:Ljava/lang/CharSequence;

    iget-object v2, p2, LX/CDz;->b:LX/1X1;

    iget v3, p2, LX/CDz;->c:I

    iget-object v4, p2, LX/CDz;->d:Ljava/lang/CharSequence;

    iget-object v5, p2, LX/CDz;->e:LX/0Or;

    iget-boolean v6, p2, LX/CDz;->f:Z

    iget-boolean v7, p2, LX/CDz;->g:Z

    iget-object v8, p2, LX/CDz;->h:LX/1X5;

    iget-object v9, p2, LX/CDz;->i:LX/1X1;

    iget-object v10, p2, LX/CDz;->j:LX/1dQ;

    iget-boolean v11, p2, LX/CDz;->k:Z

    iget-boolean v12, p2, LX/CDz;->l:Z

    move-object v0, p1

    invoke-static/range {v0 .. v12}, LX/CE3;->a(LX/1De;Ljava/lang/CharSequence;LX/1X1;ILjava/lang/CharSequence;LX/0Or;ZZLX/1X5;LX/1X1;LX/1dQ;ZZ)LX/1Dg;

    move-result-object v0

    .line 1860491
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1860475
    invoke-static {}, LX/1dS;->b()V

    .line 1860476
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c(LX/1De;)LX/CDy;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1860467
    new-instance v1, LX/CDz;

    invoke-direct {v1, p0}, LX/CDz;-><init>(LX/CE0;)V

    .line 1860468
    sget-object v2, LX/CE0;->a:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/CDy;

    .line 1860469
    if-nez v2, :cond_0

    .line 1860470
    new-instance v2, LX/CDy;

    invoke-direct {v2}, LX/CDy;-><init>()V

    .line 1860471
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/CDy;->a$redex0(LX/CDy;LX/1De;IILX/CDz;)V

    .line 1860472
    move-object v1, v2

    .line 1860473
    move-object v0, v1

    .line 1860474
    return-object v0
.end method
