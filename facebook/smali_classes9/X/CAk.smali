.class public final LX/CAk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

.field public final synthetic b:Lcom/facebook/graphql/model/GraphQLStory;

.field public final synthetic c:LX/355;


# direct methods
.method public constructor <init>(LX/355;Lcom/facebook/graphql/model/GraphQLStoryActionLink;Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 0

    .prologue
    .line 1855664
    iput-object p1, p0, LX/CAk;->c:LX/355;

    iput-object p2, p0, LX/CAk;->a:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    iput-object p3, p0, LX/CAk;->b:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 14

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x2

    const/4 v1, 0x1

    const v3, -0x4b9af987

    invoke-static {v0, v1, v3}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v3

    .line 1855653
    iget-object v0, p0, LX/CAk;->a:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aU()Lcom/facebook/graphql/model/GraphQLImageOverlay;

    move-result-object v1

    .line 1855654
    iget-object v0, p0, LX/CAk;->c:LX/355;

    iget-object v0, v0, LX/355;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/B4O;

    iget-object v4, p0, LX/CAk;->b:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v4}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->B()Ljava/lang/String;

    move-result-object v4

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImageOverlay;->k()Ljava/lang/String;

    move-result-object v1

    .line 1855655
    :goto_0
    const-string v9, "heisman_navigate_to_page_sponsor"

    const-string v10, "tracking"

    const-string v12, "profile_pic_frame_id"

    move-object v8, v0

    move-object v11, v4

    move-object v13, v1

    invoke-static/range {v8 .. v13}, LX/B4O;->a(LX/B4O;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1855656
    iget-object v0, p0, LX/CAk;->a:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->az()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v1

    .line 1855657
    iget-object v0, p0, LX/CAk;->c:LX/355;

    iget-object v0, v0, LX/355;->d:LX/1nG;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLProfile;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v4

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, LX/1nG;->a(Lcom/facebook/graphql/enums/GraphQLObjectType;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 1855658
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 1855659
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLProfile;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v6

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLProfile;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLProfile;->J()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLProfile;->D()Ljava/lang/String;

    move-result-object v1

    invoke-static {v5, v6, v7, v0, v1}, LX/5ve;->a(Landroid/os/Bundle;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1855660
    iget-object v0, p0, LX/CAk;->c:LX/355;

    iget-object v0, v0, LX/355;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {v0, v1, v4, v5, v2}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/util/Map;)Z

    .line 1855661
    const v0, -0x3f236f5b

    invoke-static {v0, v3}, LX/02F;->a(II)V

    return-void

    :cond_0
    move-object v1, v2

    .line 1855662
    goto :goto_0

    :cond_1
    move-object v0, v2

    .line 1855663
    goto :goto_1
.end method
