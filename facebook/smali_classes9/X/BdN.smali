.class public final LX/BdN;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/BdN;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/BdL;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/BdO;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1803700
    const/4 v0, 0x0

    sput-object v0, LX/BdN;->a:LX/BdN;

    .line 1803701
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/BdN;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1803734
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1803735
    new-instance v0, LX/BdO;

    invoke-direct {v0}, LX/BdO;-><init>()V

    iput-object v0, p0, LX/BdN;->c:LX/BdO;

    .line 1803736
    return-void
.end method

.method public static c(LX/1De;)LX/BdL;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1803726
    new-instance v1, LX/BdM;

    invoke-direct {v1}, LX/BdM;-><init>()V

    .line 1803727
    sget-object v2, LX/BdN;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/BdL;

    .line 1803728
    if-nez v2, :cond_0

    .line 1803729
    new-instance v2, LX/BdL;

    invoke-direct {v2}, LX/BdL;-><init>()V

    .line 1803730
    :cond_0
    invoke-static {v2, p0, v0, v0, v1}, LX/BdL;->a$redex0(LX/BdL;LX/1De;IILX/BdM;)V

    .line 1803731
    move-object v1, v2

    .line 1803732
    move-object v0, v1

    .line 1803733
    return-object v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1803737
    const v0, -0x1360f9e7

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method

.method public static declared-synchronized q()LX/BdN;
    .locals 2

    .prologue
    .line 1803722
    const-class v1, LX/BdN;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/BdN;->a:LX/BdN;

    if-nez v0, :cond_0

    .line 1803723
    new-instance v0, LX/BdN;

    invoke-direct {v0}, LX/BdN;-><init>()V

    sput-object v0, LX/BdN;->a:LX/BdN;

    .line 1803724
    :cond_0
    sget-object v0, LX/BdN;->a:LX/BdN;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 1803725
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 1803713
    check-cast p2, LX/BdM;

    .line 1803714
    iget-object v0, p2, LX/BdM;->a:Ljava/lang/CharSequence;

    iget-object v1, p2, LX/BdM;->b:Ljava/lang/CharSequence;

    iget-boolean v2, p2, LX/BdM;->c:Z

    const/4 p2, 0x2

    .line 1803715
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x1

    invoke-interface {v3, v4}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v4

    invoke-virtual {v4, v0}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v4

    const v5, 0x7f0b0054

    invoke-virtual {v4, v5}, LX/1ne;->q(I)LX/1ne;

    move-result-object v4

    const v5, 0x7f0a00ab

    invoke-virtual {v4, v5}, LX/1ne;->n(I)LX/1ne;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    invoke-interface {v4, p2}, LX/1Di;->b(I)LX/1Di;

    move-result-object v4

    const/16 v5, 0x8

    const p0, 0x7f0b007b

    invoke-interface {v4, v5, p0}, LX/1Di;->g(II)LX/1Di;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    .line 1803716
    if-eqz v2, :cond_1

    .line 1803717
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v4

    if-nez v1, :cond_0

    const v5, 0x7f080041

    invoke-virtual {p1, v5}, LX/1De;->getString(I)Ljava/lang/String;

    move-result-object v1

    :cond_0
    invoke-virtual {v4, v1}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v4

    const v5, 0x7f0b0052

    invoke-virtual {v4, v5}, LX/1ne;->q(I)LX/1ne;

    move-result-object v4

    const v5, 0x7f0a00a8

    invoke-virtual {v4, v5}, LX/1ne;->n(I)LX/1ne;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    .line 1803718
    const v5, -0x1360f9e7

    const/4 p0, 0x0

    invoke-static {p1, v5, p0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v5

    move-object v5, v5

    .line 1803719
    invoke-interface {v4, v5}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v4

    invoke-interface {v4, p2}, LX/1Di;->b(I)LX/1Di;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 1803720
    :cond_1
    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 1803721
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1803702
    invoke-static {}, LX/1dS;->b()V

    .line 1803703
    iget v0, p1, LX/1dQ;->b:I

    .line 1803704
    packed-switch v0, :pswitch_data_0

    .line 1803705
    :goto_0
    return-object v2

    .line 1803706
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 1803707
    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1803708
    check-cast v1, LX/BdM;

    .line 1803709
    iget-object p0, v1, LX/BdM;->d:Ljava/lang/Runnable;

    .line 1803710
    if-eqz p0, :cond_0

    .line 1803711
    invoke-interface {p0}, Ljava/lang/Runnable;->run()V

    .line 1803712
    :cond_0
    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x1360f9e7
        :pswitch_0
    .end packed-switch
.end method
