.class public final LX/C1L;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/C1M;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/C1O;

.field public final synthetic b:LX/C1M;


# direct methods
.method public constructor <init>(LX/C1M;)V
    .locals 1

    .prologue
    .line 1842306
    iput-object p1, p0, LX/C1L;->b:LX/C1M;

    .line 1842307
    move-object v0, p1

    .line 1842308
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1842309
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1842294
    const-string v0, "PollSeeMoreComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1842295
    if-ne p0, p1, :cond_1

    .line 1842296
    :cond_0
    :goto_0
    return v0

    .line 1842297
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1842298
    goto :goto_0

    .line 1842299
    :cond_3
    check-cast p1, LX/C1L;

    .line 1842300
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1842301
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1842302
    if-eq v2, v3, :cond_0

    .line 1842303
    iget-object v2, p0, LX/C1L;->a:LX/C1O;

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/C1L;->a:LX/C1O;

    iget-object v3, p1, LX/C1L;->a:LX/C1O;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1842304
    goto :goto_0

    .line 1842305
    :cond_4
    iget-object v2, p1, LX/C1L;->a:LX/C1O;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
