.class public final LX/BXt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# instance fields
.field public final synthetic a:Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;)V
    .locals 0

    .prologue
    .line 1793922
    iput-object p1, p0, LX/BXt;->a:Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFocusChange(Landroid/view/View;Z)V
    .locals 3

    .prologue
    .line 1793923
    if-nez p2, :cond_0

    .line 1793924
    iget-object v0, p0, LX/BXt;->a:Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;

    .line 1793925
    iget-object v1, v0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;->k:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {p1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    const/4 p2, 0x0

    invoke-virtual {v1, v2, p2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1793926
    iget-object v0, p0, LX/BXt;->a:Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;

    iget-object v0, v0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;->a:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getUserEnteredPlainText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_0

    .line 1793927
    iget-object v0, p0, LX/BXt;->a:Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;->b(Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;Z)V

    .line 1793928
    :cond_0
    return-void
.end method
