.class public LX/C5G;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pk;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/C5H;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/C5G",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/C5H;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1848589
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1848590
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/C5G;->b:LX/0Zi;

    .line 1848591
    iput-object p1, p0, LX/C5G;->a:LX/0Ot;

    .line 1848592
    return-void
.end method

.method public static a(LX/0QB;)LX/C5G;
    .locals 4

    .prologue
    .line 1848593
    const-class v1, LX/C5G;

    monitor-enter v1

    .line 1848594
    :try_start_0
    sget-object v0, LX/C5G;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1848595
    sput-object v2, LX/C5G;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1848596
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1848597
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1848598
    new-instance v3, LX/C5G;

    const/16 p0, 0x1f66

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/C5G;-><init>(LX/0Ot;)V

    .line 1848599
    move-object v0, v3

    .line 1848600
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1848601
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C5G;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1848602
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1848603
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 5

    .prologue
    .line 1848604
    check-cast p2, LX/C5F;

    .line 1848605
    iget-object v0, p0, LX/C5G;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/C5H;

    iget-object v1, p2, LX/C5F;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 p2, 0x3

    const/4 p0, 0x1

    .line 1848606
    iget-object v2, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v2, v2

    .line 1848607
    check-cast v2, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v2}, LX/1Wr;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    .line 1848608
    instance-of v3, v2, Lcom/facebook/graphql/model/GraphQLArticleChainingFeedUnit;

    if-eqz v3, :cond_0

    .line 1848609
    iget-object v3, v0, LX/C5H;->a:LX/1vg;

    invoke-virtual {v3, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v3

    const v4, 0x7f0a00e6

    invoke-virtual {v3, v4}, LX/2xv;->j(I)LX/2xv;

    move-result-object v3

    const v4, 0x7f020aea

    invoke-virtual {v3, v4}, LX/2xv;->h(I)LX/2xv;

    move-result-object v3

    invoke-virtual {v3}, LX/1n6;->b()LX/1dc;

    move-result-object v3

    .line 1848610
    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v4

    invoke-virtual {v4, v3}, LX/1o5;->a(LX/1dc;)LX/1o5;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const v4, 0x7f0b00e3

    invoke-interface {v3, v4}, LX/1Di;->i(I)LX/1Di;

    move-result-object v3

    const v4, 0x7f0b00e9

    invoke-interface {v3, v4}, LX/1Di;->q(I)LX/1Di;

    move-result-object v3

    .line 1848611
    const v4, 0x3bcb5a3b

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object v2, v0, v1

    invoke-static {p1, v4, v0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v4

    move-object v2, v4

    .line 1848612
    invoke-interface {v3, v2}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v2

    const v3, 0x7f0810fa

    invoke-interface {v2, v3}, LX/1Di;->A(I)LX/1Di;

    move-result-object v2

    const/4 v3, 0x4

    const v4, 0x7f0b00eb

    invoke-interface {v2, v3, v4}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    const/4 v3, 0x5

    const v4, 0x7f0b00ea

    invoke-interface {v2, v3, v4}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    const v3, 0x7f0b00ed

    invoke-interface {v2, p0, v3}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    const v3, 0x7f0b00ee

    invoke-interface {v2, p2, v3}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    .line 1848613
    :goto_0
    move-object v0, v2

    .line 1848614
    return-object v0

    .line 1848615
    :cond_0
    iget-object v3, v0, LX/C5H;->a:LX/1vg;

    invoke-virtual {v3, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v3

    const v4, 0x7f0a00e6

    invoke-virtual {v3, v4}, LX/2xv;->j(I)LX/2xv;

    move-result-object v3

    const v4, 0x7f02081c

    invoke-virtual {v3, v4}, LX/2xv;->h(I)LX/2xv;

    move-result-object v3

    invoke-virtual {v3}, LX/1n6;->b()LX/1dc;

    move-result-object v3

    .line 1848616
    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v4

    invoke-virtual {v4, v3}, LX/1o5;->a(LX/1dc;)LX/1o5;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    .line 1848617
    const v4, 0x357c76ce

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object v2, v0, v1

    invoke-static {p1, v4, v0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v4

    move-object v2, v4

    .line 1848618
    invoke-interface {v3, v2}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v2

    const v3, 0x7f080018

    invoke-interface {v2, v3}, LX/1Di;->A(I)LX/1Di;

    move-result-object v2

    const/4 v3, 0x6

    const v4, 0x7f0b09b3

    invoke-interface {v2, v3, v4}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    const v3, 0x7f0b00fb

    invoke-interface {v2, p0, v3}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    const v3, 0x7f0b00fc

    invoke-interface {v2, p2, v3}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 1848619
    invoke-static {}, LX/1dS;->b()V

    .line 1848620
    iget v0, p1, LX/1dQ;->b:I

    .line 1848621
    sparse-switch v0, :sswitch_data_0

    .line 1848622
    :goto_0
    return-object v3

    .line 1848623
    :sswitch_0
    check-cast p2, LX/3Ae;

    .line 1848624
    iget-object v1, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v0, p1, LX/1dQ;->c:[Ljava/lang/Object;

    aget-object v0, v0, v2

    check-cast v0, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    iget-object v2, p1, LX/1dQ;->a:LX/1X1;

    .line 1848625
    check-cast v2, LX/C5F;

    .line 1848626
    iget-object v4, p0, LX/C5G;->a:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v4, v2, LX/C5F;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object p1, v2, LX/C5F;->b:LX/1Pq;

    .line 1848627
    invoke-static {v0}, LX/1mc;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)I

    move-result p2

    .line 1848628
    invoke-interface {v0}, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;->p()Ljava/util/List;

    move-result-object p0

    .line 1848629
    if-ltz p2, :cond_0

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v2

    if-lt p2, v2, :cond_1

    .line 1848630
    :cond_0
    :goto_1
    goto :goto_0

    .line 1848631
    :sswitch_1
    iget-object v0, p1, LX/1dQ;->c:[Ljava/lang/Object;

    aget-object v0, v0, v2

    check-cast v0, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1848632
    check-cast v1, LX/C5F;

    .line 1848633
    iget-object v2, p0, LX/C5G;->a:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/C5H;

    iget-object v4, v1, LX/C5F;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1848634
    iget-object p1, v2, LX/C5H;->b:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/C5a;

    .line 1848635
    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object p0

    .line 1848636
    iget-object v1, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 1848637
    check-cast v1, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    .line 1848638
    instance-of v0, v1, Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnit;

    if-eqz v0, :cond_2

    .line 1848639
    check-cast v1, Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnit;

    invoke-static {v1}, LX/1mc;->c(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)LX/162;

    move-result-object v1

    .line 1848640
    :goto_2
    move-object p0, v1

    .line 1848641
    const/4 v1, 0x0

    invoke-static {p0, v1}, LX/17Q;->c(LX/0lF;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    .line 1848642
    iget-object v1, p1, LX/C5a;->a:LX/0Zb;

    invoke-interface {v1, p0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1848643
    iget-object p1, v2, LX/C5H;->c:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/3iV;

    .line 1848644
    iget-object p0, v4, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object p0, p0

    .line 1848645
    check-cast p0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object p0

    const/4 v1, 0x0

    invoke-virtual {p1, p0, v1}, LX/3iV;->a(Ljava/lang/String;Lcom/facebook/graphql/model/FeedUnit;)V

    .line 1848646
    goto :goto_0

    .line 1848647
    :cond_1
    invoke-interface {p0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1848648
    iget-object p0, v4, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object p0, p0

    .line 1848649
    invoke-static {v0, p0}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object p0

    invoke-static {p2, p0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;LX/0Px;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object p2

    .line 1848650
    check-cast p1, LX/1Pk;

    invoke-interface {p1}, LX/1Pk;->e()LX/1SX;

    move-result-object p0

    invoke-virtual {p0, p2, v1}, LX/1SX;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;)V

    goto :goto_1

    .line 1848651
    :cond_2
    instance-of v0, v1, Lcom/facebook/graphql/model/GraphQLStorySet;

    if-eqz v0, :cond_3

    .line 1848652
    invoke-interface {v1}, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;->p()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1848653
    invoke-interface {v1}, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;->p()Ljava/util/List;

    move-result-object v1

    const/4 v0, 0x0

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1848654
    invoke-virtual {p0, v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 1848655
    invoke-static {v1}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v1

    goto :goto_2

    .line 1848656
    :cond_3
    invoke-static {p0}, LX/181;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v1

    goto :goto_2

    nop

    :sswitch_data_0
    .sparse-switch
        0x357c76ce -> :sswitch_1
        0x3bcb5a3b -> :sswitch_0
    .end sparse-switch
.end method
