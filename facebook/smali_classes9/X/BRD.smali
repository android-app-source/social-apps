.class public final LX/BRD;
.super LX/63W;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/base/fragment/FbFragment;

.field public final synthetic b:LX/BRg;

.field public final synthetic c:LX/BRG;


# direct methods
.method public constructor <init>(LX/BRG;Lcom/facebook/base/fragment/FbFragment;LX/BRg;)V
    .locals 0

    .prologue
    .line 1783606
    iput-object p1, p0, LX/BRD;->c:LX/BRG;

    iput-object p2, p0, LX/BRD;->a:Lcom/facebook/base/fragment/FbFragment;

    iput-object p3, p0, LX/BRD;->b:LX/BRg;

    invoke-direct {p0}, LX/63W;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V
    .locals 4

    .prologue
    .line 1783596
    iget-object v0, p0, LX/BRD;->a:Lcom/facebook/base/fragment/FbFragment;

    .line 1783597
    iget-object v1, v0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v1

    .line 1783598
    iget-object v1, p0, LX/BRD;->c:LX/BRG;

    iget-object v1, v1, LX/BRG;->g:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    iget-object v2, p0, LX/BRD;->a:Lcom/facebook/base/fragment/FbFragment;

    invoke-virtual {v2}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v2

    sget-object v3, LX/0ax;->bD:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->b(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 1783599
    const-string v2, "session_id"

    iget-object v3, p0, LX/BRD;->c:LX/BRG;

    iget-object v3, v3, LX/BRG;->c:LX/BR1;

    iget-object v3, v3, LX/BR1;->g:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1783600
    const-string v2, "video_edit_data"

    iget-object v3, p0, LX/BRD;->c:LX/BRG;

    iget-object v3, v3, LX/BRG;->b:Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1783601
    const-string v2, "video_caption"

    iget-object v3, p0, LX/BRD;->c:LX/BRG;

    iget-object v3, v3, LX/BRG;->c:LX/BR1;

    iget-object v3, v3, LX/BR1;->h:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1783602
    const-string v2, "video_model"

    const-string v3, "video_model"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1783603
    const-string v0, "video_expiration"

    iget-object v2, p0, LX/BRD;->b:LX/BRg;

    invoke-virtual {v2}, LX/BRg;->a()J

    move-result-wide v2

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1783604
    iget-object v0, p0, LX/BRD;->c:LX/BRG;

    iget-object v0, v0, LX/BRG;->f:Lcom/facebook/content/SecureContextHelper;

    const/4 v2, 0x2

    iget-object v3, p0, LX/BRD;->a:Lcom/facebook/base/fragment/FbFragment;

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 1783605
    return-void
.end method
