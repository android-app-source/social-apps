.class public final LX/C2O;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Landroid/view/View$OnClickListener;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:Landroid/view/View$OnClickListener;

.field public final d:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:Landroid/graphics/drawable/Drawable;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

.field public final g:Ljava/lang/CharSequence;

.field public final h:Ljava/lang/String;

.field public final i:LX/1Ua;

.field public final j:Z


# direct methods
.method private constructor <init>(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Landroid/net/Uri;Landroid/graphics/drawable/Drawable;Lcom/facebook/graphql/model/GraphQLStoryActionLink;Ljava/lang/CharSequence;LX/1Ua;ZLjava/lang/String;)V
    .locals 2
    .param p2    # Landroid/view/View$OnClickListener;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Landroid/graphics/drawable/Drawable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Landroid/view/View$OnClickListener;",
            "Landroid/view/View$OnClickListener;",
            "Landroid/net/Uri;",
            "Landroid/graphics/drawable/Drawable;",
            "Lcom/facebook/graphql/model/GraphQLStoryActionLink;",
            "Ljava/lang/CharSequence;",
            "LX/1Ua;",
            "Z",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1844316
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1844317
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1844318
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1844319
    if-eqz p4, :cond_0

    if-nez p5, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v1, "Do not specify both a URI and a resource drawable"

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1844320
    iput-object p1, p0, LX/C2O;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1844321
    iput-object p2, p0, LX/C2O;->b:Landroid/view/View$OnClickListener;

    .line 1844322
    iput-object p3, p0, LX/C2O;->c:Landroid/view/View$OnClickListener;

    .line 1844323
    iput-object p4, p0, LX/C2O;->d:Landroid/net/Uri;

    .line 1844324
    iput-object p5, p0, LX/C2O;->e:Landroid/graphics/drawable/Drawable;

    .line 1844325
    iput-object p6, p0, LX/C2O;->f:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 1844326
    iput-object p7, p0, LX/C2O;->g:Ljava/lang/CharSequence;

    .line 1844327
    iput-object p8, p0, LX/C2O;->i:LX/1Ua;

    .line 1844328
    iput-boolean p9, p0, LX/C2O;->j:Z

    .line 1844329
    iput-object p10, p0, LX/C2O;->h:Ljava/lang/String;

    .line 1844330
    return-void

    .line 1844331
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Landroid/net/Uri;Landroid/graphics/drawable/Drawable;)LX/C2O;
    .locals 7
    .param p1    # Landroid/view/View$OnClickListener;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Landroid/graphics/drawable/Drawable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Landroid/view/View$OnClickListener;",
            "Landroid/view/View$OnClickListener;",
            "Landroid/net/Uri;",
            "Landroid/graphics/drawable/Drawable;",
            ")",
            "LX/C2O;"
        }
    .end annotation

    .prologue
    .line 1844335
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1844336
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionPartDefinition;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    .line 1844337
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->x()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aZ()Ljava/lang/String;

    move-result-object v6

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-static/range {v0 .. v6}, LX/C2O;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Landroid/net/Uri;Landroid/graphics/drawable/Drawable;Lcom/facebook/graphql/model/GraphQLTextWithEntities;Ljava/lang/String;)LX/C2O;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Landroid/net/Uri;Landroid/graphics/drawable/Drawable;Lcom/facebook/graphql/model/GraphQLTextWithEntities;Ljava/lang/String;)LX/C2O;
    .locals 11
    .param p1    # Landroid/view/View$OnClickListener;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Landroid/graphics/drawable/Drawable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Landroid/view/View$OnClickListener;",
            "Landroid/view/View$OnClickListener;",
            "Landroid/net/Uri;",
            "Landroid/graphics/drawable/Drawable;",
            "Lcom/facebook/graphql/model/GraphQLTextWithEntities;",
            "Ljava/lang/String;",
            ")",
            "LX/C2O;"
        }
    .end annotation

    .prologue
    .line 1844333
    invoke-virtual {p0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionPartDefinition;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v6

    .line 1844334
    new-instance v0, LX/C2O;

    invoke-static/range {p5 .. p5}, Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Ljava/lang/CharSequence;

    move-result-object v7

    sget-object v8, LX/1Ua;->m:LX/1Ua;

    const/4 v9, 0x1

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v10, p6

    invoke-direct/range {v0 .. v10}, LX/C2O;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Landroid/net/Uri;Landroid/graphics/drawable/Drawable;Lcom/facebook/graphql/model/GraphQLStoryActionLink;Ljava/lang/CharSequence;LX/1Ua;ZLjava/lang/String;)V

    return-object v0
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLStoryActionLink;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Landroid/net/Uri;Landroid/graphics/drawable/Drawable;Ljava/lang/CharSequence;Z)LX/C2O;
    .locals 11
    .param p2    # Landroid/view/View$OnClickListener;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLStoryActionLink;",
            "Landroid/view/View$OnClickListener;",
            "Landroid/view/View$OnClickListener;",
            "Landroid/net/Uri;",
            "Landroid/graphics/drawable/Drawable;",
            "Ljava/lang/CharSequence;",
            "Z)",
            "LX/C2O;"
        }
    .end annotation

    .prologue
    .line 1844332
    new-instance v0, LX/C2O;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->x()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    if-nez v1, :cond_0

    move-object/from16 v7, p6

    :goto_0
    sget-object v8, LX/1Ua;->n:LX/1Ua;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aZ()Ljava/lang/String;

    move-result-object v10

    move-object v1, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p5

    move-object v6, p1

    move/from16 v9, p7

    invoke-direct/range {v0 .. v10}, LX/C2O;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Landroid/net/Uri;Landroid/graphics/drawable/Drawable;Lcom/facebook/graphql/model/GraphQLStoryActionLink;Ljava/lang/CharSequence;LX/1Ua;ZLjava/lang/String;)V

    return-object v0

    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->x()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/feedplugins/calltoaction/ActionLinkCallToActionPartDefinition;->b(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Ljava/lang/CharSequence;

    move-result-object v7

    goto :goto_0
.end method
