.class public LX/CaZ;
.super LX/9iD;
.source ""


# instance fields
.field private final a:LX/CaK;

.field private final b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final c:Landroid/net/Uri;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final d:Z

.field private final e:Z

.field private final f:Z

.field private final g:I

.field private final h:Ljava/lang/String;

.field private final i:Ljava/lang/String;

.field private final j:LX/1Up;

.field private final k:Lcom/facebook/graphql/model/GraphQLStory;


# direct methods
.method public constructor <init>(LX/0gc;LX/CaK;Ljava/lang/String;Landroid/net/Uri;ZZZILjava/lang/String;Ljava/lang/String;LX/1Up;Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 0
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p9    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p10    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p11    # LX/1Up;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1918096
    invoke-direct {p0, p1}, LX/9iD;-><init>(LX/0gc;)V

    .line 1918097
    iput-object p2, p0, LX/CaZ;->a:LX/CaK;

    .line 1918098
    iput-object p3, p0, LX/CaZ;->b:Ljava/lang/String;

    .line 1918099
    iput-object p4, p0, LX/CaZ;->c:Landroid/net/Uri;

    .line 1918100
    iput-boolean p5, p0, LX/CaZ;->d:Z

    .line 1918101
    iput-boolean p6, p0, LX/CaZ;->e:Z

    .line 1918102
    iput-boolean p7, p0, LX/CaZ;->f:Z

    .line 1918103
    iput p8, p0, LX/CaZ;->g:I

    .line 1918104
    iput-object p9, p0, LX/CaZ;->h:Ljava/lang/String;

    .line 1918105
    iput-object p10, p0, LX/CaZ;->i:Ljava/lang/String;

    .line 1918106
    iput-object p11, p0, LX/CaZ;->j:LX/1Up;

    .line 1918107
    iput-object p12, p0, LX/CaZ;->k:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1918108
    return-void
.end method

.method private static a(LX/5kD;)Z
    .locals 2

    .prologue
    .line 1918094
    invoke-static {p0}, LX/5k9;->a(LX/5kD;)Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    .line 1918095
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->aN()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->aN()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)I
    .locals 5

    .prologue
    const/4 v2, -0x2

    .line 1918084
    move-object v0, p1

    check-cast v0, LX/CaH;

    .line 1918085
    invoke-interface {v0}, LX/CaH;->a()Ljava/lang/String;

    move-result-object v3

    .line 1918086
    iget-object v0, p0, LX/CaZ;->a:LX/CaK;

    .line 1918087
    iget-object v1, v0, LX/CaK;->b:LX/0Px;

    move-object v4, v1

    .line 1918088
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 1918089
    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5kD;

    invoke-interface {v0}, LX/5kD;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1918090
    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5kD;

    invoke-static {v0}, LX/CaZ;->a(LX/5kD;)Z

    move-result v0

    if-eqz v0, :cond_0

    instance-of v0, p1, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;

    if-nez v0, :cond_0

    move v1, v2

    .line 1918091
    :cond_0
    :goto_1
    return v1

    .line 1918092
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    move v1, v2

    .line 1918093
    goto :goto_1
.end method

.method public final a(I)Landroid/support/v4/app/Fragment;
    .locals 8

    .prologue
    .line 1918056
    const/4 v3, 0x1

    .line 1918057
    invoke-virtual {p0, p1}, LX/CaZ;->e(I)LX/5kD;

    move-result-object v0

    .line 1918058
    iget-boolean v1, p0, LX/CaZ;->d:Z

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/5kD;->S()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1918059
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1918060
    new-instance v1, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;

    invoke-direct {v1}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;-><init>()V

    .line 1918061
    invoke-interface {v0}, LX/5kD;->d()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/facebook/photos/mediagallery/ui/MediaGalleryVideoPageFragment;->j:Ljava/lang/String;

    .line 1918062
    move-object v0, v1

    .line 1918063
    :goto_0
    return-object v0

    .line 1918064
    :cond_0
    invoke-static {v0}, LX/CaZ;->a(LX/5kD;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-boolean v1, p0, LX/CaZ;->e:Z

    if-eqz v1, :cond_2

    .line 1918065
    iget-object v1, p0, LX/CaZ;->a:LX/CaK;

    .line 1918066
    iget-object v2, v1, LX/CaK;->b:LX/0Px;

    move-object v1, v2

    .line 1918067
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-ne v1, v3, :cond_1

    .line 1918068
    iget-object v1, p0, LX/CaZ;->b:Ljava/lang/String;

    iget-object v2, p0, LX/CaZ;->c:Landroid/net/Uri;

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;->a(LX/5kD;Ljava/lang/String;Landroid/net/Uri;Z)Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;

    move-result-object v0

    goto :goto_0

    .line 1918069
    :cond_1
    iget-boolean v1, p0, LX/CaZ;->f:Z

    if-eqz v1, :cond_2

    .line 1918070
    iget-object v1, p0, LX/CaZ;->b:Ljava/lang/String;

    iget-object v2, p0, LX/CaZ;->c:Landroid/net/Uri;

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;->a(LX/5kD;Ljava/lang/String;Landroid/net/Uri;Z)Lcom/facebook/photos/mediagallery/ui/MediaGalleryPhoto360PageFragment;

    move-result-object v0

    goto :goto_0

    .line 1918071
    :cond_2
    iget-object v1, p0, LX/CaZ;->b:Ljava/lang/String;

    iget-object v2, p0, LX/CaZ;->c:Landroid/net/Uri;

    iget v3, p0, LX/CaZ;->g:I

    iget-object v4, p0, LX/CaZ;->h:Ljava/lang/String;

    iget-object v5, p0, LX/CaZ;->i:Ljava/lang/String;

    iget-object v6, p0, LX/CaZ;->j:LX/1Up;

    iget-object v7, p0, LX/CaZ;->k:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1918072
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1918073
    new-instance p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPageFragment;

    invoke-direct {p0}, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPageFragment;-><init>()V

    .line 1918074
    invoke-interface {v0}, LX/5kD;->d()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPageFragment;->k:Ljava/lang/String;

    .line 1918075
    iput-object v1, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPageFragment;->l:Ljava/lang/String;

    .line 1918076
    iput-object v2, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPageFragment;->m:Landroid/net/Uri;

    .line 1918077
    iput v3, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPageFragment;->q:I

    .line 1918078
    iput-object v4, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPageFragment;->r:Ljava/lang/String;

    .line 1918079
    iput-object v5, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPageFragment;->t:Ljava/lang/String;

    .line 1918080
    if-nez v6, :cond_3

    sget-object v6, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPageFragment;->i:LX/1Up;

    :cond_3
    iput-object v6, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPageFragment;->u:LX/1Up;

    .line 1918081
    iput-object v7, p0, Lcom/facebook/photos/mediagallery/ui/MediaGalleryPageFragment;->v:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1918082
    move-object v0, p0

    .line 1918083
    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;I)Z
    .locals 2

    .prologue
    .line 1918051
    invoke-virtual {p0}, LX/0gG;->b()I

    move-result v0

    if-lt p2, v0, :cond_0

    .line 1918052
    const/4 v0, 0x0

    .line 1918053
    :goto_0
    return v0

    .line 1918054
    :cond_0
    check-cast p1, LX/CaH;

    .line 1918055
    invoke-interface {p1}, LX/CaH;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p2}, LX/CaZ;->e(I)LX/5kD;

    move-result-object v1

    invoke-interface {v1}, LX/5kD;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 1918048
    iget-object v0, p0, LX/CaZ;->a:LX/CaK;

    .line 1918049
    iget-object p0, v0, LX/CaK;->b:LX/0Px;

    move-object v0, p0

    .line 1918050
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method

.method public final b(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1918047
    invoke-virtual {p0, p1}, LX/CaZ;->e(I)LX/5kD;

    move-result-object v0

    invoke-interface {v0}, LX/5kD;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final e(I)LX/5kD;
    .locals 1

    .prologue
    .line 1918044
    iget-object v0, p0, LX/CaZ;->a:LX/CaK;

    .line 1918045
    iget-object p0, v0, LX/CaK;->b:LX/0Px;

    move-object v0, p0

    .line 1918046
    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5kD;

    return-object v0
.end method
