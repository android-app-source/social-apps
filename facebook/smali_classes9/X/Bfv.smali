.class public final LX/Bfv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/BeT;


# instance fields
.field public final synthetic a:LX/BeT;

.field public final synthetic b:Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;


# direct methods
.method public constructor <init>(Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;LX/BeT;)V
    .locals 0

    .prologue
    .line 1806799
    iput-object p1, p0, LX/Bfv;->b:Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;

    iput-object p2, p0, LX/Bfv;->a:LX/BeT;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1806795
    iget-object v0, p0, LX/Bfv;->b:Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;

    iget-object v0, v0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1806796
    :goto_0
    return-void

    .line 1806797
    :cond_0
    iget-object v0, p0, LX/Bfv;->b:Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;

    iget-object v0, v0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1806798
    iget-object v0, p0, LX/Bfv;->a:LX/BeT;

    invoke-interface {v0, p1}, LX/BeT;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b(Lcom/google/common/util/concurrent/ListenableFuture;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<+",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1806793
    iget-object v0, p0, LX/Bfv;->a:LX/BeT;

    invoke-interface {v0, p1}, LX/BeT;->b(Lcom/google/common/util/concurrent/ListenableFuture;)V

    .line 1806794
    return-void
.end method

.method public final kD_()V
    .locals 1

    .prologue
    .line 1806791
    iget-object v0, p0, LX/Bfv;->a:LX/BeT;

    invoke-interface {v0}, LX/BeT;->kD_()V

    .line 1806792
    return-void
.end method
