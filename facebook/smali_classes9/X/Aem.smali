.class public LX/Aem;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static l:LX/0Xm;


# instance fields
.field private final a:Landroid/content/Context;

.field public final b:LX/AVU;

.field public final c:LX/AaZ;

.field public final d:LX/961;

.field public final e:LX/3Gp;

.field public f:LX/AcT;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:LX/Aef;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:LX/Aef;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:LX/Aef;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:LX/Aef;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/AVU;LX/AaZ;LX/961;LX/3Gp;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1698099
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1698100
    iput-object p1, p0, LX/Aem;->a:Landroid/content/Context;

    .line 1698101
    iput-object p2, p0, LX/Aem;->b:LX/AVU;

    .line 1698102
    iput-object p3, p0, LX/Aem;->c:LX/AaZ;

    .line 1698103
    iput-object p4, p0, LX/Aem;->d:LX/961;

    .line 1698104
    iput-object p5, p0, LX/Aem;->e:LX/3Gp;

    .line 1698105
    return-void
.end method

.method public static a(LX/0QB;)LX/Aem;
    .locals 9

    .prologue
    .line 1698106
    const-class v1, LX/Aem;

    monitor-enter v1

    .line 1698107
    :try_start_0
    sget-object v0, LX/Aem;->l:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1698108
    sput-object v2, LX/Aem;->l:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1698109
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1698110
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1698111
    new-instance v3, LX/Aem;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/AVU;->a(LX/0QB;)LX/AVU;

    move-result-object v5

    check-cast v5, LX/AVU;

    invoke-static {v0}, LX/AaZ;->b(LX/0QB;)LX/AaZ;

    move-result-object v6

    check-cast v6, LX/AaZ;

    invoke-static {v0}, LX/961;->b(LX/0QB;)LX/961;

    move-result-object v7

    check-cast v7, LX/961;

    invoke-static {v0}, LX/3Gp;->a(LX/0QB;)LX/3Gp;

    move-result-object v8

    check-cast v8, LX/3Gp;

    invoke-direct/range {v3 .. v8}, LX/Aem;-><init>(Landroid/content/Context;LX/AVU;LX/AaZ;LX/961;LX/3Gp;)V

    .line 1698112
    move-object v0, v3

    .line 1698113
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1698114
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Aem;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1698115
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1698116
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a$redex0(LX/Aem;LX/AeU;)V
    .locals 2

    .prologue
    .line 1698117
    iget-object v0, p1, LX/AeU;->d:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 1698118
    :goto_0
    return-void

    .line 1698119
    :cond_0
    new-instance v0, LX/BUo;

    invoke-direct {v0}, LX/BUo;-><init>()V

    .line 1698120
    iget-object v1, p0, LX/Aem;->a:Landroid/content/Context;

    .line 1698121
    iput-object v1, v0, LX/BUo;->a:Landroid/content/Context;

    .line 1698122
    move-object v0, v0

    .line 1698123
    iget-object v1, p1, LX/AeU;->d:Ljava/lang/String;

    .line 1698124
    iput-object v1, v0, LX/BUo;->b:Ljava/lang/String;

    .line 1698125
    move-object v0, v0

    .line 1698126
    new-instance v1, LX/Ael;

    invoke-direct {v1, p0, p1}, LX/Ael;-><init>(LX/Aem;LX/AeU;)V

    .line 1698127
    iput-object v1, v0, LX/BUo;->d:LX/Aek;

    .line 1698128
    move-object v0, v0

    .line 1698129
    invoke-virtual {v0}, LX/BUo;->a()LX/3Ag;

    move-result-object v0

    invoke-virtual {v0}, LX/3Ag;->show()V

    goto :goto_0
.end method

.method public static a$redex0(LX/Aem;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1698130
    iget-object v0, p0, LX/Aem;->e:LX/3Gp;

    const-string v1, "IN_LIVE_EXPERIENCE"

    invoke-virtual {v0, p1, v1}, LX/3Gp;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1698131
    return-void
.end method

.method public static b$redex0(LX/Aem;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1698132
    iget-object v0, p0, LX/Aem;->e:LX/3Gp;

    const-string v1, "IN_LIVE_EXPERIENCE"

    invoke-virtual {v0, p1, v1}, LX/3Gp;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1698133
    return-void
.end method
