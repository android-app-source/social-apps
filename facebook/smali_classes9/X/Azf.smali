.class public final LX/Azf;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/goodfriends/camera/CameraActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/goodfriends/camera/CameraActivity;)V
    .locals 0

    .prologue
    .line 1732819
    iput-object p1, p0, LX/Azf;->a:Lcom/facebook/goodfriends/camera/CameraActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1732820
    iget-object v0, p0, LX/Azf;->a:Lcom/facebook/goodfriends/camera/CameraActivity;

    iget-object v0, v0, Lcom/facebook/goodfriends/camera/CameraActivity;->t:LX/9J2;

    sget-object v1, LX/9J1;->CAMERA_EXITED:LX/9J1;

    invoke-virtual {v0, v1}, LX/9J2;->a(LX/9J1;)V

    .line 1732821
    iget-object v0, p0, LX/Azf;->a:Lcom/facebook/goodfriends/camera/CameraActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->d()V

    .line 1732822
    return-void
.end method

.method public final a(Landroid/net/Uri;)V
    .locals 7

    .prologue
    .line 1732823
    iget-object v0, p0, LX/Azf;->a:Lcom/facebook/goodfriends/camera/CameraActivity;

    .line 1732824
    invoke-static {v0}, Lcom/facebook/goodfriends/camera/CameraActivity;->b$redex0(Lcom/facebook/goodfriends/camera/CameraActivity;)V

    .line 1732825
    iget-object v0, p0, LX/Azf;->a:Lcom/facebook/goodfriends/camera/CameraActivity;

    const/4 v1, -0x1

    iget-object v2, p0, LX/Azf;->a:Lcom/facebook/goodfriends/camera/CameraActivity;

    iget-object v2, v2, Lcom/facebook/goodfriends/camera/CameraActivity;->q:LX/B0K;

    .line 1732826
    const/4 v3, 0x0

    .line 1732827
    new-instance v4, LX/B0J;

    .line 1732828
    iget-object v5, v2, LX/B0K;->f:LX/74n;

    sget-object v6, LX/74j;->CREATIVECAM_MEDIA:LX/74j;

    invoke-virtual {v5, p1, v6}, LX/74n;->a(Landroid/net/Uri;LX/74j;)Lcom/facebook/ipc/media/MediaItem;

    move-result-object v5

    .line 1732829
    invoke-static {v5}, LX/7kv;->a(Lcom/facebook/ipc/media/MediaItem;)LX/7kv;

    move-result-object v5

    invoke-virtual {v5}, LX/7kv;->a()Lcom/facebook/composer/attachments/ComposerAttachment;

    move-result-object v5

    move-object v5, v5

    .line 1732830
    invoke-direct {v4, v2, v5, v3}, LX/B0J;-><init>(LX/B0K;Lcom/facebook/composer/attachments/ComposerAttachment;Lcom/facebook/privacy/model/SelectablePrivacyData;)V

    .line 1732831
    invoke-static {v2, v4}, LX/B0K;->a(LX/B0K;LX/B0J;)Landroid/content/Intent;

    move-result-object v4

    move-object v3, v4

    .line 1732832
    move-object v2, v3

    .line 1732833
    invoke-virtual {v0, v1, v2}, Lcom/facebook/goodfriends/camera/CameraActivity;->setResult(ILandroid/content/Intent;)V

    .line 1732834
    iget-object v0, p0, LX/Azf;->a:Lcom/facebook/goodfriends/camera/CameraActivity;

    iget-object v0, v0, Lcom/facebook/goodfriends/camera/CameraActivity;->t:LX/9J2;

    const-string v1, "photo"

    invoke-virtual {v0, v1}, LX/9J2;->e(Ljava/lang/String;)V

    .line 1732835
    iget-object v0, p0, LX/Azf;->a:Lcom/facebook/goodfriends/camera/CameraActivity;

    invoke-virtual {v0}, Lcom/facebook/goodfriends/camera/CameraActivity;->finish()V

    .line 1732836
    return-void
.end method

.method public final b(Landroid/net/Uri;)V
    .locals 6

    .prologue
    .line 1732837
    iget-object v0, p0, LX/Azf;->a:Lcom/facebook/goodfriends/camera/CameraActivity;

    .line 1732838
    invoke-static {v0}, Lcom/facebook/goodfriends/camera/CameraActivity;->b$redex0(Lcom/facebook/goodfriends/camera/CameraActivity;)V

    .line 1732839
    iget-object v0, p0, LX/Azf;->a:Lcom/facebook/goodfriends/camera/CameraActivity;

    const/4 v1, -0x1

    iget-object v2, p0, LX/Azf;->a:Lcom/facebook/goodfriends/camera/CameraActivity;

    iget-object v2, v2, Lcom/facebook/goodfriends/camera/CameraActivity;->q:LX/B0K;

    .line 1732840
    const/4 v3, 0x0

    .line 1732841
    iget-object v4, v2, LX/B0K;->f:LX/74n;

    invoke-static {p1, v4}, LX/7kv;->a(Landroid/net/Uri;LX/74n;)LX/7kv;

    move-result-object v4

    invoke-virtual {v4}, LX/7kv;->a()Lcom/facebook/composer/attachments/ComposerAttachment;

    move-result-object v4

    .line 1732842
    new-instance v5, LX/B0J;

    invoke-direct {v5, v2, v4, v3}, LX/B0J;-><init>(LX/B0K;Lcom/facebook/composer/attachments/ComposerAttachment;Lcom/facebook/privacy/model/SelectablePrivacyData;)V

    invoke-static {v2, v5}, LX/B0K;->a(LX/B0K;LX/B0J;)Landroid/content/Intent;

    move-result-object v4

    move-object v3, v4

    .line 1732843
    move-object v2, v3

    .line 1732844
    invoke-virtual {v0, v1, v2}, Lcom/facebook/goodfriends/camera/CameraActivity;->setResult(ILandroid/content/Intent;)V

    .line 1732845
    iget-object v0, p0, LX/Azf;->a:Lcom/facebook/goodfriends/camera/CameraActivity;

    iget-object v0, v0, Lcom/facebook/goodfriends/camera/CameraActivity;->t:LX/9J2;

    const-string v1, "video"

    invoke-virtual {v0, v1}, LX/9J2;->e(Ljava/lang/String;)V

    .line 1732846
    iget-object v0, p0, LX/Azf;->a:Lcom/facebook/goodfriends/camera/CameraActivity;

    invoke-virtual {v0}, Lcom/facebook/goodfriends/camera/CameraActivity;->finish()V

    .line 1732847
    return-void
.end method
