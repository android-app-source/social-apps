.class public final LX/ATZ;
.super LX/9cs;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;)V
    .locals 0

    .prologue
    .line 1676174
    iput-object p1, p0, LX/ATZ;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    invoke-direct {p0}, LX/9cs;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/5iG;)V
    .locals 12

    .prologue
    .line 1676175
    iget-object v0, p0, LX/ATZ;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v0, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->e:LX/7kn;

    iget-object v1, p0, LX/ATZ;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v1, v1, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->al:Ljava/lang/String;

    iget-object v2, p0, LX/ATZ;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v2, v2, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v2}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/media/MediaItem;->c()J

    move-result-wide v2

    .line 1676176
    iget-object v4, v0, LX/7kn;->b:LX/11i;

    sget-object v5, LX/7kn;->a:LX/7km;

    invoke-interface {v4, v5, v1}, LX/11i;->b(LX/0Pq;Ljava/lang/String;)LX/11o;

    move-result-object v4

    invoke-static {v4}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v4

    .line 1676177
    invoke-virtual {v4}, LX/0am;->isPresent()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1676178
    invoke-virtual {v4}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/11o;

    const-string v5, "loadPhoto"

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    iget-object v8, v0, LX/7kn;->c:LX/0So;

    invoke-interface {v8}, LX/0So;->now()J

    move-result-wide v8

    const v10, 0x488b8d3c    # 285801.88f

    invoke-static/range {v4 .. v10}, LX/096;->b(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;JI)LX/11o;

    .line 1676179
    :cond_0
    iget-object v0, p0, LX/ATZ;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget v0, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ak:I

    if-nez v0, :cond_1

    .line 1676180
    iget-object v0, p0, LX/ATZ;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v0, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->e:LX/7kn;

    iget-object v1, p0, LX/ATZ;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v1, v1, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->al:Ljava/lang/String;

    iget-object v2, p0, LX/ATZ;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v2, v2, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v2}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/media/MediaItem;->c()J

    move-result-wide v2

    .line 1676181
    iget-object v4, v0, LX/7kn;->b:LX/11i;

    sget-object v5, LX/7kn;->a:LX/7km;

    invoke-interface {v4, v5, v1}, LX/11i;->b(LX/0Pq;Ljava/lang/String;)LX/11o;

    move-result-object v4

    invoke-static {v4}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v4

    .line 1676182
    invoke-virtual {v4}, LX/0am;->isPresent()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1676183
    invoke-virtual {v4}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/11o;

    const-string v5, "loadTopPhoto"

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    iget-object v8, v0, LX/7kn;->c:LX/0So;

    invoke-interface {v8}, LX/0So;->now()J

    move-result-wide v8

    const v10, -0x66972efe

    invoke-static/range {v4 .. v10}, LX/096;->b(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;JI)LX/11o;

    .line 1676184
    :cond_1
    iget-object v0, p0, LX/ATZ;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v0, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ac:Lcom/facebook/photos/creativeediting/renderers/CreativeEditingPhotoOverlayView;

    if-eqz v0, :cond_2

    .line 1676185
    iget-object v0, p0, LX/ATZ;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v0, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ac:Lcom/facebook/photos/creativeediting/renderers/CreativeEditingPhotoOverlayView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/photos/creativeediting/renderers/CreativeEditingPhotoOverlayView;->setVisibility(I)V

    .line 1676186
    :cond_2
    iget-object v0, p0, LX/ATZ;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v0, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ad:LX/ATL;

    .line 1676187
    iget-object v4, v0, LX/ATL;->a:LX/ATO;

    .line 1676188
    iget v5, v4, LX/ATO;->O:I

    add-int/lit8 v6, v5, 0x1

    iput v6, v4, LX/ATO;->O:I

    .line 1676189
    iget-object v4, v0, LX/ATL;->a:LX/ATO;

    iget v4, v4, LX/ATO;->O:I

    iget-object v5, v0, LX/ATL;->a:LX/ATO;

    iget v5, v5, LX/ATO;->P:I

    if-ne v4, v5, :cond_3

    .line 1676190
    iget-object v4, v0, LX/ATL;->a:LX/ATO;

    iget-object v4, v4, LX/ATO;->m:LX/7kn;

    iget-object v5, v0, LX/ATL;->a:LX/ATO;

    iget-object v5, v5, LX/ATO;->N:Ljava/lang/String;

    .line 1676191
    iget-object v6, v4, LX/7kn;->b:LX/11i;

    sget-object v7, LX/7kn;->a:LX/7km;

    const/4 v9, 0x0

    iget-object v8, v4, LX/7kn;->c:LX/0So;

    invoke-interface {v8}, LX/0So;->now()J

    move-result-wide v10

    move-object v8, v5

    invoke-interface/range {v6 .. v11}, LX/11i;->b(LX/0Pq;Ljava/lang/String;LX/0P1;J)V

    .line 1676192
    :cond_3
    iget-object v0, p0, LX/ATZ;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v1, p0, LX/ATZ;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-boolean v1, v1, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aE:Z

    invoke-virtual {v0, v1}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->setFaceBoxesAndTags(Z)V

    .line 1676193
    return-void
.end method

.method public final b(LX/5iG;LX/5iG;LX/5iG;)V
    .locals 1

    .prologue
    .line 1676194
    iget-object v0, p0, LX/ATZ;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    invoke-virtual {v0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->invalidate()V

    .line 1676195
    return-void
.end method
