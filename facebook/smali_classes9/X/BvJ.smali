.class public final LX/BvJ;
.super LX/2oa;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2oa",
        "<",
        "LX/2ow;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/feed/video/fullscreen/PreviouslyLiveSeekBarPlugin;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/video/fullscreen/PreviouslyLiveSeekBarPlugin;)V
    .locals 0

    .prologue
    .line 1832132
    iput-object p1, p0, LX/BvJ;->a:Lcom/facebook/feed/video/fullscreen/PreviouslyLiveSeekBarPlugin;

    invoke-direct {p0}, LX/2oa;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/feed/video/fullscreen/PreviouslyLiveSeekBarPlugin;B)V
    .locals 0

    .prologue
    .line 1832133
    invoke-direct {p0, p1}, LX/BvJ;-><init>(Lcom/facebook/feed/video/fullscreen/PreviouslyLiveSeekBarPlugin;)V

    return-void
.end method

.method private a(LX/2ow;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 1832134
    sget-object v0, LX/BvI;->b:[I

    iget-object v1, p1, LX/2ow;->a:LX/2oN;

    invoke-virtual {v1}, LX/2oN;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1832135
    :cond_0
    :goto_0
    return-void

    .line 1832136
    :pswitch_0
    iget-object v0, p0, LX/BvJ;->a:Lcom/facebook/feed/video/fullscreen/PreviouslyLiveSeekBarPlugin;

    iget-object v0, v0, Lcom/facebook/feed/video/fullscreen/PreviouslyLiveSeekBarPlugin;->r:Landroid/widget/SeekBar;

    invoke-virtual {v0, v2}, Landroid/widget/SeekBar;->setVisibility(I)V

    .line 1832137
    iget-object v0, p0, LX/BvJ;->a:Lcom/facebook/feed/video/fullscreen/PreviouslyLiveSeekBarPlugin;

    iget-object v0, v0, Lcom/facebook/feed/video/fullscreen/PreviouslyLiveSeekBarPlugin;->q:Lcom/facebook/facecastdisplay/heatmap/HeatmapView;

    if-eqz v0, :cond_0

    .line 1832138
    iget-object v0, p0, LX/BvJ;->a:Lcom/facebook/feed/video/fullscreen/PreviouslyLiveSeekBarPlugin;

    iget-object v0, v0, Lcom/facebook/feed/video/fullscreen/PreviouslyLiveSeekBarPlugin;->q:Lcom/facebook/facecastdisplay/heatmap/HeatmapView;

    invoke-virtual {v0, v2}, Lcom/facebook/facecastdisplay/heatmap/HeatmapView;->setVisibility(I)V

    goto :goto_0

    .line 1832139
    :pswitch_1
    iget-object v0, p0, LX/BvJ;->a:Lcom/facebook/feed/video/fullscreen/PreviouslyLiveSeekBarPlugin;

    iget-object v0, v0, Lcom/facebook/feed/video/fullscreen/PreviouslyLiveSeekBarPlugin;->r:Landroid/widget/SeekBar;

    invoke-virtual {v0, v3}, Landroid/widget/SeekBar;->setVisibility(I)V

    .line 1832140
    iget-object v0, p0, LX/BvJ;->a:Lcom/facebook/feed/video/fullscreen/PreviouslyLiveSeekBarPlugin;

    iget-object v0, v0, Lcom/facebook/feed/video/fullscreen/PreviouslyLiveSeekBarPlugin;->q:Lcom/facebook/facecastdisplay/heatmap/HeatmapView;

    if-eqz v0, :cond_0

    .line 1832141
    iget-object v0, p0, LX/BvJ;->a:Lcom/facebook/feed/video/fullscreen/PreviouslyLiveSeekBarPlugin;

    iget-object v0, v0, Lcom/facebook/feed/video/fullscreen/PreviouslyLiveSeekBarPlugin;->q:Lcom/facebook/facecastdisplay/heatmap/HeatmapView;

    invoke-virtual {v0, v3}, Lcom/facebook/facecastdisplay/heatmap/HeatmapView;->setVisibility(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/2ow;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1832142
    const-class v0, LX/2ow;

    return-object v0
.end method

.method public final synthetic b(LX/0b7;)V
    .locals 0

    .prologue
    .line 1832143
    check-cast p1, LX/2ow;

    invoke-direct {p0, p1}, LX/BvJ;->a(LX/2ow;)V

    return-void
.end method
