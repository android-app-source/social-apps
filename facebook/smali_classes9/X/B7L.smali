.class public final LX/B7L;
.super LX/B7K;
.source ""

# interfaces
.implements LX/B7I;
.implements LX/B7J;


# instance fields
.field private a:Landroid/net/Uri;

.field private b:Ljava/lang/String;

.field public c:Lcom/facebook/graphql/model/GraphQLPhoto;

.field public d:Landroid/net/Uri;

.field public e:Lcom/facebook/graphql/model/GraphQLLeadGenContextPage;


# direct methods
.method public constructor <init>(Lcom/facebook/graphql/model/GraphQLLeadGenData;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;)V
    .locals 1
    .param p1    # Lcom/facebook/graphql/model/GraphQLLeadGenData;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1747949
    invoke-direct {p0}, LX/B7K;-><init>()V

    .line 1747950
    iput-object p3, p0, LX/B7L;->a:Landroid/net/Uri;

    .line 1747951
    iput-object p4, p0, LX/B7L;->b:Ljava/lang/String;

    .line 1747952
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLLeadGenData;->a()Lcom/facebook/graphql/model/GraphQLLeadGenContextPage;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1747953
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLLeadGenData;->a()Lcom/facebook/graphql/model/GraphQLLeadGenContextPage;

    move-result-object v0

    iput-object v0, p0, LX/B7L;->e:Lcom/facebook/graphql/model/GraphQLLeadGenContextPage;

    .line 1747954
    iget-object v0, p0, LX/B7L;->e:Lcom/facebook/graphql/model/GraphQLLeadGenContextPage;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLLeadGenContextPage;->a()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v0

    iput-object v0, p0, LX/B7L;->c:Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 1747955
    :cond_0
    const/4 v0, 0x0

    .line 1747956
    iget-object p1, p0, LX/B7L;->e:Lcom/facebook/graphql/model/GraphQLLeadGenContextPage;

    if-eqz p1, :cond_1

    .line 1747957
    iget-object p1, p0, LX/B7L;->e:Lcom/facebook/graphql/model/GraphQLLeadGenContextPage;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLLeadGenContextPage;->m()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object p1

    .line 1747958
    if-eqz p1, :cond_1

    .line 1747959
    invoke-static {p1}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v0

    .line 1747960
    :cond_1
    if-nez v0, :cond_2

    .line 1747961
    :goto_0
    move-object v0, p2

    .line 1747962
    iput-object v0, p0, LX/B7L;->d:Landroid/net/Uri;

    .line 1747963
    return-void

    :cond_2
    move-object p2, v0

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1747964
    iget-object v0, p0, LX/B7L;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Landroid/net/Uri;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1747965
    iget-object v0, p0, LX/B7L;->a:Landroid/net/Uri;

    return-object v0
.end method
