.class public LX/Bsl;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pb;",
        ":",
        "LX/1Pc;",
        ":",
        "LX/1Pd;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Bsm;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Bsl",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/Bsm;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1828177
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1828178
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/Bsl;->b:LX/0Zi;

    .line 1828179
    iput-object p1, p0, LX/Bsl;->a:LX/0Ot;

    .line 1828180
    return-void
.end method

.method public static a(LX/0QB;)LX/Bsl;
    .locals 4

    .prologue
    .line 1828181
    const-class v1, LX/Bsl;

    monitor-enter v1

    .line 1828182
    :try_start_0
    sget-object v0, LX/Bsl;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1828183
    sput-object v2, LX/Bsl;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1828184
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1828185
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1828186
    new-instance v3, LX/Bsl;

    const/16 p0, 0x1d39

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/Bsl;-><init>(LX/0Ot;)V

    .line 1828187
    move-object v0, v3

    .line 1828188
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1828189
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Bsl;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1828190
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1828191
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 7

    .prologue
    .line 1828192
    check-cast p2, LX/Bsk;

    .line 1828193
    iget-object v0, p0, LX/Bsl;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Bsm;

    iget-object v1, p2, LX/Bsk;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/Bsk;->b:LX/1Pb;

    const/4 p2, 0x1

    .line 1828194
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    iget-object v4, v0, LX/Bsm;->a:LX/1VD;

    invoke-virtual {v4, p1}, LX/1VD;->c(LX/1De;)LX/1X4;

    move-result-object v4

    invoke-virtual {v4, v1}, LX/1X4;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1X4;

    move-result-object v4

    invoke-virtual {v4, v2}, LX/1X4;->a(LX/1Pb;)LX/1X4;

    move-result-object v4

    invoke-virtual {v4, p2}, LX/1X4;->d(Z)LX/1X4;

    move-result-object v4

    invoke-virtual {v4, p2}, LX/1X4;->g(Z)LX/1X4;

    move-result-object v4

    invoke-virtual {v4, p2}, LX/1X4;->e(Z)LX/1X4;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v4

    const v5, 0x7f021479

    invoke-virtual {v4, v5}, LX/1o5;->h(I)LX/1o5;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    invoke-interface {v4, p2}, LX/1Di;->c(I)LX/1Di;

    move-result-object v4

    const/4 v5, 0x2

    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const p0, 0x7f0b0f75

    invoke-virtual {v6, p0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    invoke-interface {v4, v5, v6}, LX/1Di;->k(II)LX/1Di;

    move-result-object v4

    const/4 v5, 0x0

    invoke-interface {v4, p2, v5}, LX/1Di;->k(II)LX/1Di;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 1828195
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1828196
    invoke-static {}, LX/1dS;->b()V

    .line 1828197
    const/4 v0, 0x0

    return-object v0
.end method
