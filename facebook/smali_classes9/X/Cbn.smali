.class public final LX/Cbn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/21M;


# instance fields
.field public final synthetic a:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;)V
    .locals 0

    .prologue
    .line 1919776
    iput-object p1, p0, LX/Cbn;->a:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;LX/1zt;LX/0Ve;)V
    .locals 4

    .prologue
    .line 1919777
    sget-object v0, LX/1zt;->d:LX/1zt;

    if-ne p2, v0, :cond_1

    .line 1919778
    :cond_0
    :goto_0
    return-void

    .line 1919779
    :cond_1
    iget-object v0, p0, LX/Cbn;->a:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;

    iget-object v1, p0, LX/Cbn;->a:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;

    iget-object v1, v1, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->at:LX/5kD;

    invoke-static {v0, v1}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->j(Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;LX/5kD;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    .line 1919780
    iget-object v0, p0, LX/Cbn;->a:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;

    invoke-virtual {v0}, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->getTrackingCodes()LX/162;

    move-result-object v0

    .line 1919781
    if-nez v0, :cond_2

    .line 1919782
    new-instance v0, LX/23u;

    invoke-direct {v0}, LX/23u;-><init>()V

    .line 1919783
    iput-object v1, v0, LX/23u;->D:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1919784
    move-object v0, v0

    .line 1919785
    invoke-virtual {v0}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 1919786
    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 1919787
    invoke-static {v0}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v0

    .line 1919788
    :cond_2
    new-instance v2, LX/21A;

    invoke-direct {v2}, LX/21A;-><init>()V

    const-string v3, "photo_gallery"

    .line 1919789
    iput-object v3, v2, LX/21A;->c:Ljava/lang/String;

    .line 1919790
    move-object v2, v2

    .line 1919791
    const-string v3, "media_gallery_ufi"

    .line 1919792
    iput-object v3, v2, LX/21A;->b:Ljava/lang/String;

    .line 1919793
    move-object v2, v2

    .line 1919794
    iput-object v0, v2, LX/21A;->a:LX/162;

    .line 1919795
    move-object v0, v2

    .line 1919796
    iget-object v2, p0, LX/Cbn;->a:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;

    iget-object v2, v2, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->au:LX/21D;

    .line 1919797
    iput-object v2, v0, LX/21A;->i:LX/21D;

    .line 1919798
    move-object v0, v0

    .line 1919799
    invoke-virtual {v0}, LX/21A;->b()Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-result-object v0

    .line 1919800
    iget-object v2, p0, LX/Cbn;->a:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;

    iget-object v2, v2, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->y:LX/20h;

    invoke-virtual {v2, v1, p2, v0, p3}, LX/20h;->a(Lcom/facebook/graphql/model/GraphQLFeedback;LX/1zt;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;LX/0Ve;)V

    .line 1919801
    iget v0, p2, LX/1zt;->e:I

    move v0, v0

    .line 1919802
    const/16 v1, 0xb

    if-ne v0, v1, :cond_3

    .line 1919803
    iget-object v0, p0, LX/Cbn;->a:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->D:LX/20r;

    invoke-virtual {v0, p1}, LX/20r;->a(Landroid/view/View;)V

    .line 1919804
    :cond_3
    iget v0, p2, LX/1zt;->e:I

    move v0, v0

    .line 1919805
    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 1919806
    iget-object v0, p0, LX/Cbn;->a:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryFooterView;->E:LX/20s;

    invoke-virtual {v0, p1}, LX/20s;->a(Landroid/view/View;)V

    goto :goto_0
.end method
