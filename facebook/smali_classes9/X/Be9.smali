.class public LX/Be9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0g1;
.implements LX/0g0;
.implements LX/1vq;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<TEdge:",
        "Ljava/lang/Object;",
        "TUserInfo:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/0g1",
        "<TTEdge;>;",
        "LX/0g0",
        "<TTEdge;>;",
        "LX/1vq",
        "<TTEdge;TTUserInfo;>;"
    }
.end annotation


# instance fields
.field public final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/0qs",
            "<TTEdge;>;>;"
        }
    .end annotation
.end field

.field private final b:LX/2kW;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/controller/connectioncontroller/common/ConnectionController",
            "<TTEdge;TTUserInfo;>;"
        }
    .end annotation
.end field

.field private c:LX/2kM;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2kM",
            "<TTEdge;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/2kW;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/controller/connectioncontroller/common/ConnectionController",
            "<TTEdge;TTUserInfo;>;)V"
        }
    .end annotation

    .prologue
    .line 1804645
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1804646
    iput-object p1, p0, LX/Be9;->b:LX/2kW;

    .line 1804647
    new-instance v0, LX/0UE;

    invoke-direct {v0}, LX/0UE;-><init>()V

    iput-object v0, p0, LX/Be9;->a:Ljava/util/Set;

    .line 1804648
    return-void
.end method


# virtual methods
.method public final a(I)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TTEdge;"
        }
    .end annotation

    .prologue
    .line 1804609
    iget-object v0, p0, LX/Be9;->c:LX/2kM;

    if-nez v0, :cond_0

    .line 1804610
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Connection Store hasn\'t been initialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1804611
    :cond_0
    iget-object v0, p0, LX/Be9;->c:LX/2kM;

    invoke-interface {v0, p1}, LX/2kM;->a(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 1804612
    iget-object v0, p0, LX/Be9;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 1804613
    const/4 v0, 0x0

    iput-object v0, p0, LX/Be9;->c:LX/2kM;

    .line 1804614
    return-void
.end method

.method public final a(LX/0Px;LX/3Cb;LX/2kM;LX/2kM;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/3CY;",
            ">;",
            "LX/3Cb;",
            "LX/2kM",
            "<TTEdge;>;",
            "LX/2kM",
            "<TTEdge;>;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1804615
    iput-object p4, p0, LX/Be9;->c:LX/2kM;

    .line 1804616
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v4

    move v3, v2

    :goto_0
    if-ge v3, v4, :cond_2

    invoke-virtual {p1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3CY;

    move v1, v2

    .line 1804617
    :goto_1
    iget v5, v0, LX/3CY;->b:I

    if-ge v1, v5, :cond_1

    .line 1804618
    const/4 p4, 0x0

    .line 1804619
    sget-object v5, LX/Be8;->a:[I

    iget-object v6, v0, LX/3CY;->a:LX/3Ca;

    invoke-virtual {v6}, LX/3Ca;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 1804620
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1804621
    :cond_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 1804622
    :cond_2
    return-void

    .line 1804623
    :pswitch_0
    invoke-virtual {v0}, LX/3CY;->b()I

    move-result v5

    add-int/2addr v5, v1

    invoke-virtual {p0, v5}, LX/Be9;->a(I)Ljava/lang/Object;

    move-result-object v6

    .line 1804624
    iget-object v5, p0, LX/Be9;->a:Ljava/util/Set;

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0qs;

    .line 1804625
    invoke-virtual {v0}, LX/3CY;->b()I

    move-result v8

    add-int/2addr v8, v1

    invoke-interface {v5, v8, v6, p4}, LX/0qs;->a(ILjava/lang/Object;Z)V

    goto :goto_2

    .line 1804626
    :pswitch_1
    invoke-virtual {v0}, LX/3CY;->b()I

    move-result v5

    add-int/2addr v5, v1

    invoke-virtual {p0, v5}, LX/Be9;->a(I)Ljava/lang/Object;

    move-result-object v6

    .line 1804627
    invoke-virtual {v0}, LX/3CY;->a()I

    move-result v5

    add-int/2addr v5, v1

    invoke-interface {p3, v5}, LX/2kM;->a(I)Ljava/lang/Object;

    move-result-object v7

    .line 1804628
    iget-object v5, p0, LX/Be9;->a:Ljava/util/Set;

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_3
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0qs;

    .line 1804629
    invoke-virtual {v0}, LX/3CY;->b()I

    move-result p2

    add-int/2addr p2, v1

    invoke-interface {v5, p2, v7, v6, p4}, LX/0qs;->a(ILjava/lang/Object;Ljava/lang/Object;Z)V

    goto :goto_3

    .line 1804630
    :pswitch_2
    invoke-virtual {v0}, LX/3CY;->a()I

    move-result v5

    add-int/2addr v5, v1

    invoke-interface {p3, v5}, LX/2kM;->a(I)Ljava/lang/Object;

    move-result-object v6

    .line 1804631
    iget-object v5, p0, LX/Be9;->a:Ljava/util/Set;

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_4
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0qs;

    .line 1804632
    invoke-virtual {v0}, LX/3CY;->a()I

    move-result v8

    add-int/2addr v8, v1

    invoke-interface {v5, v8, v6, p4}, LX/0qs;->b(ILjava/lang/Object;Z)V

    goto :goto_4

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(LX/0qs;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0qs",
            "<TTEdge;>;)V"
        }
    .end annotation

    .prologue
    .line 1804633
    iget-object v0, p0, LX/Be9;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1804634
    return-void
.end method

.method public final a(LX/2kM;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2kM",
            "<TTEdge;>;)V"
        }
    .end annotation

    .prologue
    .line 1804635
    iput-object p1, p0, LX/Be9;->c:LX/2kM;

    .line 1804636
    return-void
.end method

.method public final a(LX/2nj;LX/3DP;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2nj;",
            "LX/3DP;",
            "TTUserInfo;)V"
        }
    .end annotation

    .prologue
    .line 1804637
    return-void
.end method

.method public final a(LX/2nj;LX/3DP;Ljava/lang/Object;Ljava/lang/Throwable;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2nj;",
            "LX/3DP;",
            "TTUserInfo;",
            "Ljava/lang/Throwable;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1804638
    return-void
.end method

.method public final b(LX/0qs;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0qs",
            "<TTEdge;>;)V"
        }
    .end annotation

    .prologue
    .line 1804639
    iget-object v0, p0, LX/Be9;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1804640
    return-void
.end method

.method public final b(LX/2nj;LX/3DP;Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2nj;",
            "LX/3DP;",
            "TTUserInfo;)V"
        }
    .end annotation

    .prologue
    .line 1804641
    return-void
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 1804642
    iget-object v0, p0, LX/Be9;->c:LX/2kM;

    if-nez v0, :cond_0

    .line 1804643
    const/4 v0, 0x0

    .line 1804644
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/Be9;->c:LX/2kM;

    invoke-interface {v0}, LX/2kM;->c()I

    move-result v0

    goto :goto_0
.end method
