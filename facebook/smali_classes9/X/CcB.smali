.class public final LX/CcB;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/CcC;


# direct methods
.method public constructor <init>(LX/CcC;)V
    .locals 0

    .prologue
    .line 1920702
    iput-object p1, p0, LX/CcB;->a:LX/CcC;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1920703
    iget-object v0, p0, LX/CcB;->a:LX/CcC;

    iget-object v0, v0, LX/CcC;->b:LX/CcO;

    iget-object v0, v0, LX/CcO;->a:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->p:LX/745;

    const/4 p1, 0x0

    .line 1920704
    sget-object p0, LX/74R;->TAGS_DELETE_FAILED:LX/74R;

    invoke-static {v0, p0, p1, p1}, LX/745;->a(LX/745;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 1920705
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 1920706
    iget-object v0, p0, LX/CcB;->a:LX/CcC;

    iget-object v0, v0, LX/CcC;->b:LX/CcO;

    iget-object v0, v0, LX/CcO;->a:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->p:LX/745;

    const/4 p1, 0x0

    .line 1920707
    sget-object p0, LX/74R;->TAGS_DELETE_SUCCEEDED:LX/74R;

    invoke-static {v0, p0, p1, p1}, LX/745;->a(LX/745;LX/74R;Ljava/util/Map;Ljava/lang/String;)V

    .line 1920708
    return-void
.end method
