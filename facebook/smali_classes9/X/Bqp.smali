.class public LX/Bqp;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field public final a:LX/Bqw;

.field private final b:LX/Bqz;

.field private final c:LX/0ad;

.field private final d:LX/Bqe;


# direct methods
.method public constructor <init>(LX/Bqz;LX/Bqw;LX/0ad;LX/Bqe;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1825416
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1825417
    iput-object p2, p0, LX/Bqp;->a:LX/Bqw;

    .line 1825418
    iput-object p1, p0, LX/Bqp;->b:LX/Bqz;

    .line 1825419
    iput-object p3, p0, LX/Bqp;->c:LX/0ad;

    .line 1825420
    iput-object p4, p0, LX/Bqp;->d:LX/Bqe;

    .line 1825421
    return-void
.end method

.method public static a(LX/0QB;)LX/Bqp;
    .locals 7

    .prologue
    .line 1825422
    const-class v1, LX/Bqp;

    monitor-enter v1

    .line 1825423
    :try_start_0
    sget-object v0, LX/Bqp;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1825424
    sput-object v2, LX/Bqp;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1825425
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1825426
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1825427
    new-instance p0, LX/Bqp;

    invoke-static {v0}, LX/Bqz;->a(LX/0QB;)LX/Bqz;

    move-result-object v3

    check-cast v3, LX/Bqz;

    invoke-static {v0}, LX/Bqw;->a(LX/0QB;)LX/Bqw;

    move-result-object v4

    check-cast v4, LX/Bqw;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    invoke-static {v0}, LX/Bqe;->a(LX/0QB;)LX/Bqe;

    move-result-object v6

    check-cast v6, LX/Bqe;

    invoke-direct {p0, v3, v4, v5, v6}, LX/Bqp;-><init>(LX/Bqz;LX/Bqw;LX/0ad;LX/Bqe;)V

    .line 1825428
    move-object v0, p0

    .line 1825429
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1825430
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Bqp;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1825431
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1825432
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(LX/Bqp;Lcom/facebook/graphql/model/GraphQLStoryInsights;Landroid/content/res/Resources;)Landroid/text/SpannableStringBuilder;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1825406
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryInsights;->m()I

    move-result v0

    .line 1825407
    invoke-virtual {p2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget-object v1, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-static {v1}, Ljava/text/NumberFormat;->getInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v1

    .line 1825408
    const v2, 0x7f0f006d

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    int-to-long v4, v0

    invoke-virtual {v1, v4, v5}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v6

    invoke-virtual {p2, v2, v0, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1825409
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 1825410
    new-instance v2, LX/0wM;

    invoke-direct {v2, p2}, LX/0wM;-><init>(Landroid/content/res/Resources;)V

    const v3, 0x7f0207ed

    const v4, 0x7f0a00d1

    invoke-virtual {p2, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v2, v3, v4}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 1825411
    if-eqz v2, :cond_0

    iget-object v3, p0, LX/Bqp;->c:LX/0ad;

    sget-short v4, LX/0wi;->n:S

    invoke-interface {v3, v4, v6}, LX/0ad;->a(SZ)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1825412
    new-instance v0, Landroid/text/SpannableStringBuilder;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 1825413
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    invoke-virtual {v2, v6, v6, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1825414
    new-instance v3, LX/34T;

    const/4 v4, 0x2

    invoke-direct {v3, v2, v4}, LX/34T;-><init>(Landroid/graphics/drawable/Drawable;I)V

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    const/16 v4, 0x21

    invoke-virtual {v0, v3, v2, v1, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1825415
    :cond_0
    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Po;LX/1np;)LX/1Dg;
    .locals 11
    .param p2    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # LX/1Po;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;",
            "LX/1np",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    .line 1825295
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1825296
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1825297
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aj()Lcom/facebook/graphql/model/GraphQLStoryInsights;

    move-result-object v2

    .line 1825298
    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 1825299
    iget-object v1, p0, LX/Bqp;->a:LX/Bqw;

    invoke-virtual {v1, v0}, LX/Bqw;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v4

    .line 1825300
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aG()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v1

    if-eqz v1, :cond_a

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aG()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEntity;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    if-eqz v1, :cond_a

    const-string v1, "Event"

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aG()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLEntity;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    const/4 v1, 0x1

    :goto_0
    move v5, v1

    .line 1825301
    move-object v1, p3

    .line 1825302
    check-cast v1, LX/1Pr;

    invoke-static {p2, v1}, LX/Bqw;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pr;)LX/Br2;

    move-result-object v6

    .line 1825303
    iget-object v1, v6, LX/Br2;->a:Ljava/lang/Integer;

    move-object v1, v1

    .line 1825304
    if-nez v1, :cond_0

    .line 1825305
    invoke-static {v0}, LX/1Wq;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    move-result-object v1

    .line 1825306
    iget-object v7, p0, LX/Bqp;->a:LX/Bqw;

    invoke-virtual {v7, v0, v1, v5}, LX/Bqw;->a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;Z)I

    move-result v1

    invoke-virtual {v6, v1}, LX/Br2;->a(I)V

    .line 1825307
    :cond_0
    sget-object v1, LX/Bqw;->a:Ljava/util/Map;

    invoke-interface {p3}, LX/1Po;->c()LX/1PT;

    move-result-object v7

    invoke-interface {v7}, LX/1PT;->a()LX/1Qt;

    move-result-object v7

    invoke-interface {v1, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 1825308
    iput-object v1, p4, LX/1np;->a:Ljava/lang/Object;

    .line 1825309
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v1

    invoke-static {p0, v2, v3}, LX/Bqp;->a(LX/Bqp;Lcom/facebook/graphql/model/GraphQLStoryInsights;Landroid/content/res/Resources;)Landroid/text/SpannableStringBuilder;

    move-result-object v3

    invoke-virtual {v1, v3}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v1

    const v3, 0x7f0b004e

    invoke-virtual {v1, v3}, LX/1ne;->q(I)LX/1ne;

    move-result-object v1

    .line 1825310
    iget-object v3, p0, LX/Bqp;->c:LX/0ad;

    sget-short v7, LX/0wi;->n:S

    const/4 v8, 0x0

    invoke-interface {v3, v7, v8}, LX/0ad;->a(SZ)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1825311
    const v3, 0x7f0a00d1

    invoke-virtual {v1, v3}, LX/1ne;->n(I)LX/1ne;

    .line 1825312
    :cond_1
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    const/4 v7, 0x0

    invoke-interface {v3, v7}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    const/4 v7, 0x1

    invoke-interface {v3, v7}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v3

    const/high16 v7, 0x3f800000    # 1.0f

    invoke-interface {v3, v7}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v3

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    const/4 v7, 0x3

    const v8, 0x7f0b09c9

    invoke-interface {v1, v7, v8}, LX/1Di;->g(II)LX/1Di;

    move-result-object v1

    invoke-interface {v3, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    .line 1825313
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryInsights;->a()I

    move-result v1

    if-eqz v1, :cond_3

    .line 1825314
    const/4 v1, 0x0

    .line 1825315
    new-instance v7, LX/BqM;

    invoke-direct {v7}, LX/BqM;-><init>()V

    .line 1825316
    sget-object v8, LX/BqO;->b:LX/0Zi;

    invoke-virtual {v8}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/BqN;

    .line 1825317
    if-nez v8, :cond_2

    .line 1825318
    new-instance v8, LX/BqN;

    invoke-direct {v8}, LX/BqN;-><init>()V

    .line 1825319
    :cond_2
    invoke-static {v8, p1, v1, v1, v7}, LX/BqN;->a$redex0(LX/BqN;LX/1De;IILX/BqM;)V

    .line 1825320
    move-object v7, v8

    .line 1825321
    move-object v1, v7

    .line 1825322
    move-object v1, v1

    .line 1825323
    iget-object v7, v1, LX/BqN;->a:LX/BqM;

    iput-object v2, v7, LX/BqM;->a:Lcom/facebook/graphql/model/GraphQLStoryInsights;

    .line 1825324
    iget-object v7, v1, LX/BqN;->d:Ljava/util/BitSet;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Ljava/util/BitSet;->set(I)V

    .line 1825325
    move-object v1, v1

    .line 1825326
    invoke-interface {v3, v1}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    .line 1825327
    :cond_3
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v7

    .line 1825328
    if-eqz v4, :cond_5

    .line 1825329
    iget-object v1, p0, LX/Bqp;->d:LX/Bqe;

    const/4 v8, 0x0

    .line 1825330
    new-instance v9, LX/Bqc;

    invoke-direct {v9, v1}, LX/Bqc;-><init>(LX/Bqe;)V

    .line 1825331
    sget-object v10, LX/Bqe;->a:LX/0Zi;

    invoke-virtual {v10}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, LX/Bqd;

    .line 1825332
    if-nez v10, :cond_4

    .line 1825333
    new-instance v10, LX/Bqd;

    invoke-direct {v10}, LX/Bqd;-><init>()V

    .line 1825334
    :cond_4
    invoke-static {v10, p1, v8, v8, v9}, LX/Bqd;->a$redex0(LX/Bqd;LX/1De;IILX/Bqc;)V

    .line 1825335
    move-object v9, v10

    .line 1825336
    move-object v8, v9

    .line 1825337
    move-object v8, v8

    .line 1825338
    iget-object v9, p0, LX/Bqp;->a:LX/Bqw;

    move-object v1, p3

    check-cast v1, LX/1Pq;

    invoke-virtual {v9, v5, v6, v1, p2}, LX/Bqw;->a(ZLX/Br2;LX/1Pq;Lcom/facebook/feed/rows/core/props/FeedProps;)Landroid/text/style/ClickableSpan;

    move-result-object v1

    .line 1825339
    iget-object v9, v8, LX/Bqd;->a:LX/Bqc;

    iput-object v1, v9, LX/Bqc;->a:Landroid/text/style/ClickableSpan;

    .line 1825340
    iget-object v9, v8, LX/Bqd;->d:Ljava/util/BitSet;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Ljava/util/BitSet;->set(I)V

    .line 1825341
    move-object v1, v8

    .line 1825342
    iget-object v8, p0, LX/Bqp;->a:LX/Bqw;

    invoke-virtual {v8, v0}, LX/Bqw;->c(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 1825343
    iget-object v8, v1, LX/Bqd;->a:LX/Bqc;

    iput-object v0, v8, LX/Bqc;->b:Ljava/lang/Boolean;

    .line 1825344
    iget-object v8, v1, LX/Bqd;->d:Ljava/util/BitSet;

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Ljava/util/BitSet;->set(I)V

    .line 1825345
    move-object v0, v1

    .line 1825346
    invoke-interface {v7, v0}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    .line 1825347
    :cond_5
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    const/4 v1, 0x2

    invoke-interface {v0, v1}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v0

    const/4 v1, 0x3

    invoke-interface {v0, v1}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v0

    const/4 v1, 0x1

    const v8, 0x7f0b09c8

    invoke-interface {v0, v1, v8}, LX/1Dh;->q(II)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    const/4 v1, 0x0

    .line 1825348
    new-instance v3, LX/BqQ;

    invoke-direct {v3}, LX/BqQ;-><init>()V

    .line 1825349
    sget-object v8, LX/BqS;->b:LX/0Zi;

    invoke-virtual {v8}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/BqR;

    .line 1825350
    if-nez v8, :cond_6

    .line 1825351
    new-instance v8, LX/BqR;

    invoke-direct {v8}, LX/BqR;-><init>()V

    .line 1825352
    :cond_6
    invoke-static {v8, p1, v1, v1, v3}, LX/BqR;->a$redex0(LX/BqR;LX/1De;IILX/BqQ;)V

    .line 1825353
    move-object v3, v8

    .line 1825354
    move-object v1, v3

    .line 1825355
    move-object v1, v1

    .line 1825356
    iget-object v3, v6, LX/Br2;->a:Ljava/lang/Integer;

    move-object v3, v3

    .line 1825357
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 1825358
    iget-object v8, v1, LX/BqR;->a:LX/BqQ;

    invoke-virtual {v1, v3}, LX/1Dp;->b(I)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, LX/BqQ;->a:Ljava/lang/CharSequence;

    .line 1825359
    iget-object v8, v1, LX/BqR;->d:Ljava/util/BitSet;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Ljava/util/BitSet;->set(I)V

    .line 1825360
    move-object v1, v1

    .line 1825361
    iget-object v3, v1, LX/BqR;->a:LX/BqQ;

    iput-boolean v4, v3, LX/BqQ;->b:Z

    .line 1825362
    move-object v1, v1

    .line 1825363
    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    const/4 v3, 0x1

    invoke-static {p1, v3}, LX/Bqo;->onClick(LX/1De;Z)LX/1dQ;

    move-result-object v3

    invoke-interface {v1, v3}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    iget-object v0, p0, LX/Bqp;->b:LX/Bqz;

    const/4 v3, 0x0

    .line 1825364
    new-instance v4, LX/Bqy;

    invoke-direct {v4, v0}, LX/Bqy;-><init>(LX/Bqz;)V

    .line 1825365
    sget-object v8, LX/Bqz;->a:LX/0Zi;

    invoke-virtual {v8}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/Bqx;

    .line 1825366
    if-nez v8, :cond_7

    .line 1825367
    new-instance v8, LX/Bqx;

    invoke-direct {v8}, LX/Bqx;-><init>()V

    .line 1825368
    :cond_7
    invoke-static {v8, p1, v3, v3, v4}, LX/Bqx;->a$redex0(LX/Bqx;LX/1De;IILX/Bqy;)V

    .line 1825369
    move-object v4, v8

    .line 1825370
    move-object v3, v4

    .line 1825371
    move-object v0, v3

    .line 1825372
    check-cast p3, LX/1Pq;

    .line 1825373
    iget-object v3, v0, LX/Bqx;->a:LX/Bqy;

    iput-object p3, v3, LX/Bqy;->c:LX/1Pq;

    .line 1825374
    iget-object v3, v0, LX/Bqx;->d:Ljava/util/BitSet;

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Ljava/util/BitSet;->set(I)V

    .line 1825375
    move-object v0, v0

    .line 1825376
    iget-object v3, v0, LX/Bqx;->a:LX/Bqy;

    iput-object p2, v3, LX/Bqy;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1825377
    iget-object v3, v0, LX/Bqx;->d:Ljava/util/BitSet;

    const/4 v4, 0x3

    invoke-virtual {v3, v4}, Ljava/util/BitSet;->set(I)V

    .line 1825378
    move-object v3, v0

    .line 1825379
    iget-object v0, p4, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 1825380
    check-cast v0, Ljava/lang/String;

    .line 1825381
    iget-object v4, v3, LX/Bqx;->a:LX/Bqy;

    iput-object v0, v4, LX/Bqy;->g:Ljava/lang/String;

    .line 1825382
    iget-object v4, v3, LX/Bqx;->d:Ljava/util/BitSet;

    const/4 v8, 0x4

    invoke-virtual {v4, v8}, Ljava/util/BitSet;->set(I)V

    .line 1825383
    move-object v0, v3

    .line 1825384
    iget-object v3, v0, LX/Bqx;->a:LX/Bqy;

    iput-boolean v5, v3, LX/Bqy;->a:Z

    .line 1825385
    iget-object v3, v0, LX/Bqx;->d:Ljava/util/BitSet;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/BitSet;->set(I)V

    .line 1825386
    move-object v0, v0

    .line 1825387
    iget-object v3, v0, LX/Bqx;->a:LX/Bqy;

    iput-object v6, v3, LX/Bqy;->b:LX/Br2;

    .line 1825388
    iget-object v3, v0, LX/Bqx;->d:Ljava/util/BitSet;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Ljava/util/BitSet;->set(I)V

    .line 1825389
    move-object v0, v0

    .line 1825390
    invoke-interface {v1, v0}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v0

    .line 1825391
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStoryInsights;->l()I

    move-result v1

    if-nez v1, :cond_8

    .line 1825392
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1, v0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, v7}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p1, v1}, LX/Bqo;->onClick(LX/1De;Z)LX/1dQ;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    .line 1825393
    :goto_1
    return-object v0

    :cond_8
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1, v0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    const/4 v1, 0x0

    .line 1825394
    new-instance v3, LX/BqY;

    invoke-direct {v3}, LX/BqY;-><init>()V

    .line 1825395
    sget-object v4, LX/Bqa;->b:LX/0Zi;

    invoke-virtual {v4}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/BqZ;

    .line 1825396
    if-nez v4, :cond_9

    .line 1825397
    new-instance v4, LX/BqZ;

    invoke-direct {v4}, LX/BqZ;-><init>()V

    .line 1825398
    :cond_9
    invoke-static {v4, p1, v1, v1, v3}, LX/BqZ;->a$redex0(LX/BqZ;LX/1De;IILX/BqY;)V

    .line 1825399
    move-object v3, v4

    .line 1825400
    move-object v1, v3

    .line 1825401
    move-object v1, v1

    .line 1825402
    iget-object v3, v1, LX/BqZ;->a:LX/BqY;

    iput-object v2, v3, LX/BqY;->a:Lcom/facebook/graphql/model/GraphQLStoryInsights;

    .line 1825403
    iget-object v3, v1, LX/BqZ;->d:Ljava/util/BitSet;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/BitSet;->set(I)V

    .line 1825404
    move-object v1, v1

    .line 1825405
    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, v7}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {p1, v1}, LX/Bqo;->onClick(LX/1De;Z)LX/1dQ;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    goto :goto_1

    :cond_a
    const/4 v1, 0x0

    goto/16 :goto_0
.end method
