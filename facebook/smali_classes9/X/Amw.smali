.class public LX/Amw;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:[LX/1Wk;


# instance fields
.field private final b:LX/0wM;

.field private final c:Landroid/graphics/drawable/Drawable;

.field private final d:Landroid/graphics/drawable/Drawable;

.field private final e:Landroid/graphics/drawable/Drawable;

.field private final f:Landroid/graphics/drawable/Drawable;

.field private final g:I

.field private final h:I


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1711700
    const/4 v0, 0x2

    new-array v0, v0, [LX/1Wk;

    const/4 v1, 0x0

    sget-object v2, LX/1Wk;->NEWSFEED_SHADOW:LX/1Wk;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, LX/1Wk;->SUBSTORY_SHADOW:LX/1Wk;

    aput-object v2, v0, v1

    sput-object v0, LX/Amw;->a:[LX/1Wk;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0wM;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1711671
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1711672
    iput-object p2, p0, LX/Amw;->b:LX/0wM;

    .line 1711673
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1711674
    const v1, 0x7f0a00d2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, LX/Amw;->g:I

    .line 1711675
    const v1, 0x7f0a00e6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, LX/Amw;->h:I

    .line 1711676
    const v1, 0x7f0219c7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, LX/Amw;->c:Landroid/graphics/drawable/Drawable;

    .line 1711677
    const v0, 0x7f0219c6

    invoke-direct {p0, v0}, LX/Amw;->b(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, LX/Amw;->d:Landroid/graphics/drawable/Drawable;

    .line 1711678
    const v0, 0x7f0219c3

    invoke-direct {p0, v0}, LX/Amw;->b(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, LX/Amw;->e:Landroid/graphics/drawable/Drawable;

    .line 1711679
    const v0, 0x7f0219c9

    invoke-direct {p0, v0}, LX/Amw;->b(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, LX/Amw;->f:Landroid/graphics/drawable/Drawable;

    .line 1711680
    return-void
.end method

.method public static a(LX/Amv;)I
    .locals 2

    .prologue
    .line 1711681
    sget-object v0, LX/Amu;->a:[I

    invoke-virtual {p0}, LX/Amv;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1711682
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown FeedbackActionButton"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1711683
    :pswitch_0
    const v0, 0x7f080fc8

    .line 1711684
    :goto_0
    return v0

    .line 1711685
    :pswitch_1
    const v0, 0x7f080fcc

    goto :goto_0

    .line 1711686
    :pswitch_2
    const v0, 0x7f080fd9

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a(I)LX/1Wk;
    .locals 1

    .prologue
    .line 1711687
    sget-object v0, LX/Amw;->a:[LX/1Wk;

    aget-object v0, v0, p0

    return-object v0
.end method

.method public static a(LX/0QB;)LX/Amw;
    .locals 1

    .prologue
    .line 1711688
    invoke-static {p0}, LX/Amw;->b(LX/0QB;)LX/Amw;

    move-result-object v0

    return-object v0
.end method

.method private static b(LX/0QB;)LX/Amw;
    .locals 3

    .prologue
    .line 1711689
    new-instance v2, LX/Amw;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v1

    check-cast v1, LX/0wM;

    invoke-direct {v2, v0, v1}, LX/Amw;-><init>(Landroid/content/Context;LX/0wM;)V

    .line 1711690
    return-object v2
.end method

.method private b(I)Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 1711691
    iget-object v0, p0, LX/Amw;->b:LX/0wM;

    const v1, -0x6e685d

    invoke-virtual {v0, p1, v1}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Z)I
    .locals 1

    .prologue
    .line 1711692
    if-eqz p1, :cond_0

    iget v0, p0, LX/Amw;->g:I

    :goto_0
    return v0

    :cond_0
    iget v0, p0, LX/Amw;->h:I

    goto :goto_0
.end method

.method public final b(LX/Amv;)Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 1711693
    sget-object v0, LX/Amu;->a:[I

    invoke-virtual {p1}, LX/Amv;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1711694
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown FeedbackActionButton"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1711695
    :pswitch_0
    iget-object v0, p0, LX/Amw;->d:Landroid/graphics/drawable/Drawable;

    .line 1711696
    :goto_0
    return-object v0

    .line 1711697
    :pswitch_1
    iget-object v0, p0, LX/Amw;->e:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    .line 1711698
    :pswitch_2
    iget-object v0, p0, LX/Amw;->f:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final b(Z)Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 1711699
    if-eqz p1, :cond_0

    iget-object v0, p0, LX/Amw;->c:Landroid/graphics/drawable/Drawable;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/Amw;->d:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method
