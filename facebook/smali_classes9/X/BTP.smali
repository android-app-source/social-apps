.class public LX/BTP;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/util/concurrent/Executor;

.field public final b:LX/BTl;

.field public final c:LX/BTJ;

.field public final d:LX/BTL;

.field public final e:LX/BT7;

.field public f:Lcom/facebook/video/creativeediting/ui/fresco/ZoomableDraweeStripView;

.field private g:Ljava/util/Timer;

.field public h:Z


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;LX/BTl;LX/BTJ;LX/BTL;LX/BT7;)V
    .locals 1
    .param p1    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p2    # LX/BTl;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/BTJ;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/BTL;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/BT7;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1787331
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1787332
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, LX/BTP;->g:Ljava/util/Timer;

    .line 1787333
    iput-object p1, p0, LX/BTP;->a:Ljava/util/concurrent/Executor;

    .line 1787334
    iput-object p2, p0, LX/BTP;->b:LX/BTl;

    .line 1787335
    iput-object p3, p0, LX/BTP;->c:LX/BTJ;

    .line 1787336
    iput-object p4, p0, LX/BTP;->d:LX/BTL;

    .line 1787337
    iput-object p5, p0, LX/BTP;->e:LX/BT7;

    .line 1787338
    return-void
.end method


# virtual methods
.method public final a(LX/BTI;)V
    .locals 4

    .prologue
    .line 1787342
    invoke-virtual {p0}, LX/BTP;->b()V

    .line 1787343
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, LX/BTP;->g:Ljava/util/Timer;

    .line 1787344
    iget-object v0, p0, LX/BTP;->g:Ljava/util/Timer;

    new-instance v1, Lcom/facebook/video/creativeediting/trimmer/StripZoomingController$1;

    invoke-direct {v1, p0, p1}, Lcom/facebook/video/creativeediting/trimmer/StripZoomingController$1;-><init>(LX/BTP;LX/BTI;)V

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 1787345
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1787339
    iget-object v0, p0, LX/BTP;->g:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 1787340
    iget-object v0, p0, LX/BTP;->g:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 1787341
    :cond_0
    return-void
.end method
