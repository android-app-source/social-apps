.class public LX/C5S;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pk;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public final a:LX/C5C;

.field public final b:LX/C5K;

.field public final c:LX/AnA;


# direct methods
.method public constructor <init>(LX/C5C;LX/C5K;LX/AnA;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1848898
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1848899
    iput-object p1, p0, LX/C5S;->a:LX/C5C;

    .line 1848900
    iput-object p2, p0, LX/C5S;->b:LX/C5K;

    .line 1848901
    iput-object p3, p0, LX/C5S;->c:LX/AnA;

    .line 1848902
    return-void
.end method

.method public static a(LX/0QB;)LX/C5S;
    .locals 6

    .prologue
    .line 1848903
    const-class v1, LX/C5S;

    monitor-enter v1

    .line 1848904
    :try_start_0
    sget-object v0, LX/C5S;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1848905
    sput-object v2, LX/C5S;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1848906
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1848907
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1848908
    new-instance p0, LX/C5S;

    invoke-static {v0}, LX/C5C;->a(LX/0QB;)LX/C5C;

    move-result-object v3

    check-cast v3, LX/C5C;

    invoke-static {v0}, LX/C5K;->a(LX/0QB;)LX/C5K;

    move-result-object v4

    check-cast v4, LX/C5K;

    invoke-static {v0}, LX/AnA;->a(LX/0QB;)LX/AnA;

    move-result-object v5

    check-cast v5, LX/AnA;

    invoke-direct {p0, v3, v4, v5}, LX/C5S;-><init>(LX/C5C;LX/C5K;LX/AnA;)V

    .line 1848909
    move-object v0, p0

    .line 1848910
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1848911
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C5S;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1848912
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1848913
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
