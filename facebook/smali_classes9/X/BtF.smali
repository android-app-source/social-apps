.class public LX/BtF;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:Landroid/text/Layout$Alignment;

.field public static final b:I

.field public static final c:I

.field public static final d:I

.field private static f:LX/0Xm;


# instance fields
.field private final e:LX/1WB;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1828868
    sget-object v0, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    sput-object v0, LX/BtF;->a:Landroid/text/Layout$Alignment;

    .line 1828869
    const v0, 0x7f0b10e4

    sput v0, LX/BtF;->b:I

    .line 1828870
    const v0, 0x7f0b10e5

    sput v0, LX/BtF;->c:I

    .line 1828871
    const v0, 0x7f0b10e3

    sput v0, LX/BtF;->d:I

    return-void
.end method

.method public constructor <init>(LX/1WB;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1828865
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1828866
    iput-object p1, p0, LX/BtF;->e:LX/1WB;

    .line 1828867
    return-void
.end method

.method public static a(LX/0QB;)LX/BtF;
    .locals 4

    .prologue
    .line 1828854
    const-class v1, LX/BtF;

    monitor-enter v1

    .line 1828855
    :try_start_0
    sget-object v0, LX/BtF;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1828856
    sput-object v2, LX/BtF;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1828857
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1828858
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1828859
    new-instance p0, LX/BtF;

    invoke-static {v0}, LX/1WB;->a(LX/0QB;)LX/1WB;

    move-result-object v3

    check-cast v3, LX/1WB;

    invoke-direct {p0, v3}, LX/BtF;-><init>(LX/1WB;)V

    .line 1828860
    move-object v0, p0

    .line 1828861
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1828862
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/BtF;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1828863
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1828864
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static b(Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;)I
    .locals 2

    .prologue
    .line 1828853
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "#"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static c(Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;)Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1828852
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static f(Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;)I
    .locals 2

    .prologue
    .line 1828850
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->l()Ljava/lang/String;

    move-result-object v0

    .line 1828851
    const-string v1, "BOLD"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLStory;)LX/BtE;
    .locals 10
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1828810
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->bi()Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1828811
    :cond_0
    const/4 v0, 0x0

    .line 1828812
    :goto_0
    return-object v0

    .line 1828813
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->bi()Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;

    move-result-object v8

    .line 1828814
    iget-object v0, p0, LX/BtF;->e:LX/1WB;

    invoke-virtual {v0, p1}, LX/1WB;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/1z6;

    move-result-object v5

    .line 1828815
    new-instance v0, LX/BtE;

    .line 1828816
    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->j()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "#"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    :goto_1
    move v1, v1

    .line 1828817
    invoke-static {v8}, LX/BtF;->b(Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;)I

    move-result v2

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 1828818
    invoke-static {v8}, LX/BtF;->f(Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;)I

    move-result v6

    .line 1828819
    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->k()Ljava/lang/String;

    move-result-object v7

    .line 1828820
    const-string v9, "ITALIC"

    invoke-virtual {v9, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_8

    const/4 v7, 0x2

    :goto_2
    move v7, v7

    .line 1828821
    if-ne v6, v3, :cond_5

    if-nez v7, :cond_5

    .line 1828822
    :goto_3
    move v3, v3

    .line 1828823
    if-eqz v5, :cond_9

    iget v4, v5, LX/1z6;->b:I

    :goto_4
    invoke-static {v4}, LX/1W8;->a(I)I

    move-result v4

    move v4, v4

    .line 1828824
    invoke-static {v8}, LX/BtF;->f(Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;)I

    move-result v6

    .line 1828825
    if-eqz v5, :cond_2

    iget v7, v5, LX/1z6;->b:I

    const/16 v9, 0xe

    if-ne v7, v9, :cond_a

    .line 1828826
    :cond_2
    const-string v7, "sans-serif"

    .line 1828827
    :goto_5
    move-object v5, v7

    .line 1828828
    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->m()Ljava/lang/String;

    move-result-object v6

    if-nez v6, :cond_c

    .line 1828829
    sget-object v6, LX/BtF;->a:Landroid/text/Layout$Alignment;

    .line 1828830
    :goto_6
    move-object v6, v6

    .line 1828831
    invoke-static {v8}, LX/BtF;->c(Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;)Ljava/lang/String;

    move-result-object v7

    .line 1828832
    invoke-static {v8}, LX/BtF;->b(Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;)I

    move-result v9

    const/4 p0, -0x1

    if-ne v9, p0, :cond_3

    invoke-static {v8}, LX/BtF;->c(Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_f

    :cond_3
    const/4 v9, 0x1

    :goto_7
    move v9, v9

    .line 1828833
    if-eqz v9, :cond_e

    sget v9, LX/BtF;->b:I

    :goto_8
    move v8, v9

    .line 1828834
    sget v9, LX/BtF;->d:I

    move v9, v9

    .line 1828835
    invoke-direct/range {v0 .. v9}, LX/BtE;-><init>(IIIILjava/lang/String;Landroid/text/Layout$Alignment;Ljava/lang/String;II)V

    goto/16 :goto_0

    :cond_4
    const/high16 v1, -0x1000000

    goto :goto_1

    .line 1828836
    :cond_5
    if-nez v6, :cond_6

    if-ne v7, v4, :cond_6

    move v3, v4

    .line 1828837
    goto :goto_3

    .line 1828838
    :cond_6
    if-ne v6, v3, :cond_7

    if-ne v7, v4, :cond_7

    .line 1828839
    const/4 v3, 0x3

    goto :goto_3

    .line 1828840
    :cond_7
    const/4 v3, 0x0

    goto :goto_3

    :cond_8
    const/4 v7, 0x0

    goto :goto_2

    :cond_9
    const/16 v4, 0xe

    goto :goto_4

    .line 1828841
    :cond_a
    if-nez v6, :cond_b

    .line 1828842
    const-string v7, "sans-serif-light"

    goto :goto_5

    .line 1828843
    :cond_b
    const-string v7, "sans-serif"

    goto :goto_5

    .line 1828844
    :cond_c
    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLTextFormatMetadata;->m()Ljava/lang/String;

    move-result-object v7

    const/4 v6, -0x1

    invoke-virtual {v7}, Ljava/lang/String;->hashCode()I

    move-result v9

    sparse-switch v9, :sswitch_data_0

    :cond_d
    :goto_9
    packed-switch v6, :pswitch_data_0

    .line 1828845
    sget-object v6, LX/BtF;->a:Landroid/text/Layout$Alignment;

    goto :goto_6

    .line 1828846
    :sswitch_0
    const-string v9, "CENTER"

    invoke-virtual {v7, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_d

    const/4 v6, 0x0

    goto :goto_9

    :sswitch_1
    const-string v9, "LEFT"

    invoke-virtual {v7, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_d

    const/4 v6, 0x1

    goto :goto_9

    :sswitch_2
    const-string v9, "RIGHT"

    invoke-virtual {v7, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_d

    const/4 v6, 0x2

    goto :goto_9

    .line 1828847
    :pswitch_0
    sget-object v6, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    goto :goto_6

    .line 1828848
    :pswitch_1
    sget-object v6, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    goto :goto_6

    .line 1828849
    :pswitch_2
    sget-object v6, Landroid/text/Layout$Alignment;->ALIGN_OPPOSITE:Landroid/text/Layout$Alignment;

    goto :goto_6

    :cond_e
    sget v9, LX/BtF;->c:I

    goto :goto_8

    :cond_f
    const/4 v9, 0x0

    goto :goto_7

    :sswitch_data_0
    .sparse-switch
        0x239807 -> :sswitch_1
        0x4a5c9fc -> :sswitch_2
        0x7645c055 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
