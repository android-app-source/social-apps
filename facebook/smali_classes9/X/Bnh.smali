.class public LX/Bnh;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/6RZ;


# direct methods
.method public constructor <init>(LX/6RZ;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1821214
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1821215
    iput-object p1, p0, LX/Bnh;->a:LX/6RZ;

    .line 1821216
    return-void
.end method

.method public static a(Ljava/util/List;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;
    .locals 7
    .param p0    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryActionLink;",
            ">;)",
            "Lcom/facebook/graphql/model/GraphQLStoryActionLink;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 1821197
    if-nez p0, :cond_0

    move-object v0, v1

    .line 1821198
    :goto_0
    return-object v0

    .line 1821199
    :cond_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v4

    move v3, v2

    :goto_1
    if-ge v3, v4, :cond_2

    .line 1821200
    invoke-interface {p0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    .line 1821201
    if-eqz v0, :cond_1

    invoke-interface {p0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->E()Lcom/facebook/graphql/model/GraphQLEvent;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {p0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 1821202
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v5

    const v6, -0x1e53800c

    if-ne v5, v6, :cond_5

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->ah()Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-result-object v5

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->EVENT_RSVP:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    if-ne v5, v6, :cond_5

    const/4 v5, 0x1

    :goto_2
    move v0, v5

    .line 1821203
    if-eqz v0, :cond_1

    .line 1821204
    invoke-interface {p0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    goto :goto_0

    .line 1821205
    :cond_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 1821206
    :cond_2
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v3

    :goto_3
    if-ge v2, v3, :cond_4

    .line 1821207
    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    .line 1821208
    if-eqz v0, :cond_3

    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->E()Lcom/facebook/graphql/model/GraphQLEvent;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 1821209
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v4

    const v5, 0x5a1cd4ef

    if-ne v4, v5, :cond_6

    const/4 v4, 0x1

    :goto_4
    move v0, v4

    .line 1821210
    if-eqz v0, :cond_3

    .line 1821211
    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    goto/16 :goto_0

    .line 1821212
    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    :cond_4
    move-object v0, v1

    .line 1821213
    goto/16 :goto_0

    :cond_5
    const/4 v5, 0x0

    goto :goto_2

    :cond_6
    const/4 v4, 0x0

    goto :goto_4
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLNode;)Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1821191
    const/4 v0, 0x0

    .line 1821192
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->iG()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1821193
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->iG()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    .line 1821194
    :cond_0
    :goto_0
    return-object v0

    .line 1821195
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->cz()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1821196
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLNode;->cz()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLPlace;)Ljava/lang/String;
    .locals 2
    .param p0    # Lcom/facebook/graphql/model/GraphQLPlace;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1821160
    if-eqz p0, :cond_2

    .line 1821161
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->r()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1821162
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->r()Ljava/lang/String;

    move-result-object v0

    .line 1821163
    :goto_0
    return-object v0

    .line 1821164
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->A()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1821165
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->A()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1821166
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->k()Lcom/facebook/graphql/model/GraphQLStreetAddress;

    move-result-object v0

    .line 1821167
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStreetAddress;->j()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1821168
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStreetAddress;->j()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1821169
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Ljava/util/List;)Lcom/facebook/graphql/model/GraphQLEvent;
    .locals 1
    .param p0    # Ljava/util/List;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryActionLink;",
            ">;)",
            "Lcom/facebook/graphql/model/GraphQLEvent;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1821189
    invoke-static {p0}, LX/Bnh;->a(Ljava/util/List;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    .line 1821190
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->E()Lcom/facebook/graphql/model/GraphQLEvent;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(Lcom/facebook/graphql/model/GraphQLPlace;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/facebook/graphql/model/GraphQLPlace;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1821186
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->q()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1821187
    :cond_0
    const/4 v0, 0x0

    .line 1821188
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPlace;->q()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->t()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final b(Lcom/facebook/graphql/model/GraphQLNode;)Ljava/lang/String;
    .locals 10
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1821170
    const-wide/16 v8, 0x3e8

    .line 1821171
    if-nez p1, :cond_4

    .line 1821172
    const/4 v4, 0x0

    .line 1821173
    :goto_0
    move-object v2, v4

    .line 1821174
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->cI()Lcom/facebook/graphql/model/GraphQLPlace;

    move-result-object v0

    :goto_1
    invoke-static {v0}, LX/Bnh;->a(Lcom/facebook/graphql/model/GraphQLPlace;)Ljava/lang/String;

    move-result-object v0

    .line 1821175
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 1821176
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " \u2022 "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1821177
    :cond_0
    :goto_2
    return-object v1

    :cond_1
    move-object v0, v1

    .line 1821178
    goto :goto_1

    .line 1821179
    :cond_2
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    move-object v1, v2

    .line 1821180
    goto :goto_2

    .line 1821181
    :cond_3
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    move-object v1, v0

    .line 1821182
    goto :goto_2

    .line 1821183
    :cond_4
    new-instance v4, Ljava/util/Date;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->iZ()J

    move-result-wide v6

    mul-long/2addr v6, v8

    invoke-direct {v4, v6, v7}, Ljava/util/Date;-><init>(J)V

    .line 1821184
    new-instance v5, Ljava/util/Date;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->cp()J

    move-result-wide v6

    mul-long/2addr v6, v8

    invoke-direct {v5, v6, v7}, Ljava/util/Date;-><init>(J)V

    .line 1821185
    iget-object v6, p0, LX/Bnh;->a:LX/6RZ;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->eq()Z

    move-result v7

    invoke-virtual {v6, v7, v4, v5}, LX/6RZ;->a(ZLjava/util/Date;Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method
