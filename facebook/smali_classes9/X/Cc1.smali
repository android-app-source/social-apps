.class public final LX/Cc1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Ljava/lang/Object;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Cc3;


# direct methods
.method public constructor <init>(LX/Cc3;)V
    .locals 0

    .prologue
    .line 1920596
    iput-object p1, p0, LX/Cc1;->a:LX/Cc3;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 5

    .prologue
    .line 1920597
    iget-object v0, p0, LX/Cc1;->a:LX/Cc3;

    iget-object v0, v0, LX/Cc3;->d:LX/CcO;

    iget-object v0, v0, LX/CcO;->a:Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;

    iget-object v0, v0, Lcom/facebook/photos/mediagallery/ui/widget/MediaGalleryMenuHelper;->i:LX/9hh;

    iget-object v1, p0, LX/Cc1;->a:LX/Cc3;

    iget-object v1, v1, LX/Cc3;->a:LX/5kD;

    invoke-interface {v1}, LX/5kD;->d()Ljava/lang/String;

    move-result-object v1

    const/4 p0, 0x0

    .line 1920598
    new-instance v2, LX/9hb;

    invoke-direct {v2, v0, v1}, LX/9hb;-><init>(LX/9hh;Ljava/lang/String;)V

    .line 1920599
    iget-object v3, v0, LX/9hh;->f:LX/0Uh;

    const/16 v4, 0x405

    invoke-virtual {v3, v4, p0}, LX/0Uh;->a(IZ)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1920600
    const/4 v3, 0x3

    new-array v3, v3, [LX/4VT;

    new-instance v4, LX/9hU;

    invoke-direct {v4, v1}, LX/9hU;-><init>(Ljava/lang/String;)V

    aput-object v4, v3, p0

    const/4 v4, 0x1

    new-instance p0, LX/9hT;

    invoke-direct {p0, v1}, LX/9hT;-><init>(Ljava/lang/String;)V

    aput-object p0, v3, v4

    const/4 v4, 0x2

    new-instance p0, LX/9hV;

    invoke-direct {p0, v1}, LX/9hV;-><init>(Ljava/lang/String;)V

    aput-object p0, v3, v4

    invoke-static {v0, v2, v3}, LX/9hh;->a(LX/9hh;Ljava/util/concurrent/Callable;[LX/4VT;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 1920601
    :goto_0
    move-object v0, v2

    .line 1920602
    return-object v0

    :cond_0
    iget-object v3, v0, LX/9hh;->d:LX/9hs;

    invoke-virtual {v3, v1}, LX/9hs;->a(Ljava/lang/String;)LX/9hk;

    move-result-object v3

    invoke-static {v0, v2, v3}, LX/9hh;->a(LX/9hh;Ljava/util/concurrent/Callable;LX/9hk;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    goto :goto_0
.end method
