.class public LX/Ani;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/24J;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ">",
        "Ljava/lang/Object;",
        "LX/24J",
        "<",
        "LX/Anh;",
        "TV;>;"
    }
.end annotation


# instance fields
.field private final a:LX/1Rg;

.field private final b:LX/Ann;

.field public final c:LX/Ank;


# direct methods
.method public constructor <init>(LX/1Rg;LX/Ank;LX/Ann;)V
    .locals 0

    .prologue
    .line 1712817
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1712818
    iput-object p1, p0, LX/Ani;->a:LX/1Rg;

    .line 1712819
    iput-object p2, p0, LX/Ani;->c:LX/Ank;

    .line 1712820
    iput-object p3, p0, LX/Ani;->b:LX/Ann;

    .line 1712821
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLFeedback;)I
    .locals 1
    .param p0    # Lcom/facebook/graphql/model/GraphQLFeedback;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1712816
    if-eqz p0, :cond_0

    invoke-static {p0}, LX/16z;->e(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Ljava/util/List;LX/Anh;Landroid/view/View;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/8sj;",
            ">;",
            "LX/Anh;",
            "TV;)V"
        }
    .end annotation

    .prologue
    .line 1712803
    iget-object v0, p0, LX/Ani;->c:LX/Ank;

    invoke-virtual {v0, p3}, LX/Ank;->b(Landroid/view/View;)Landroid/view/View;

    move-result-object v1

    .line 1712804
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1712805
    iget-object v0, p0, LX/Ani;->b:LX/Ann;

    .line 1712806
    iget v2, v0, LX/Ann;->b:I

    move v0, v2

    .line 1712807
    iget-object v2, p0, LX/Ani;->b:LX/Ann;

    .line 1712808
    iget v3, v2, LX/Ann;->a:I

    move v2, v3

    .line 1712809
    iget-boolean v3, p2, LX/Anh;->b:Z

    if-eqz v3, :cond_0

    move v4, v0

    .line 1712810
    :goto_0
    iget-boolean v3, p2, LX/Anh;->b:Z

    if-eqz v3, :cond_1

    move v5, v2

    .line 1712811
    :goto_1
    new-instance v6, LX/Anf;

    invoke-direct {v6, p0, p3, p2}, LX/Anf;-><init>(LX/Ani;Landroid/view/View;LX/Anh;)V

    .line 1712812
    iget-object v0, p0, LX/Ani;->a:LX/1Rg;

    const-wide/16 v2, 0xc8

    invoke-virtual/range {v0 .. v6}, LX/1Rg;->a(Landroid/view/View;JIILandroid/animation/Animator$AnimatorListener;)LX/8sj;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1712813
    return-void

    :cond_0
    move v4, v2

    .line 1712814
    goto :goto_0

    :cond_1
    move v5, v0

    .line 1712815
    goto :goto_1
.end method

.method public static b(Lcom/facebook/graphql/model/GraphQLFeedback;)I
    .locals 1
    .param p0    # Lcom/facebook/graphql/model/GraphQLFeedback;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1712822
    if-eqz p0, :cond_0

    invoke-static {p0}, LX/16z;->o(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Ljava/util/List;LX/Anh;LX/Anh;Landroid/view/View;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/8sj;",
            ">;",
            "LX/Anh;",
            "LX/Anh;",
            "TV;)V"
        }
    .end annotation

    .prologue
    .line 1712801
    iget-object v0, p0, LX/Ani;->a:LX/1Rg;

    iget-object v1, p0, LX/Ani;->c:LX/Ank;

    invoke-virtual {v1, p4}, LX/Ank;->a(Landroid/view/View;)Ljava/lang/Object;

    move-result-object v1

    const-wide/16 v2, 0xaa

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v5, 0x0

    new-instance v6, LX/Ane;

    invoke-direct {v6, p0, p4, p2, p3}, LX/Ane;-><init>(LX/Ani;Landroid/view/View;LX/Anh;LX/Anh;)V

    invoke-virtual/range {v0 .. v6}, LX/1Rg;->a(Ljava/lang/Object;JFFLandroid/animation/Animator$AnimatorListener;)LX/8sj;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1712802
    return-void
.end method

.method private c(Ljava/util/List;LX/Anh;LX/Anh;Landroid/view/View;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/8sj;",
            ">;",
            "LX/Anh;",
            "LX/Anh;",
            "TV;)V"
        }
    .end annotation

    .prologue
    .line 1712795
    new-instance v6, LX/Ang;

    invoke-direct {v6, p0, p4, p3}, LX/Ang;-><init>(LX/Ani;Landroid/view/View;LX/Anh;)V

    .line 1712796
    iget v0, p2, LX/Anh;->d:I

    if-lez v0, :cond_0

    .line 1712797
    iget-object v0, p0, LX/Ani;->a:LX/1Rg;

    const-wide/16 v2, 0xaa

    invoke-virtual {v0, v2, v3, v6}, LX/1Rg;->a(JLandroid/animation/Animator$AnimatorListener;)LX/8sj;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1712798
    :cond_0
    iget-object v0, p0, LX/Ani;->a:LX/1Rg;

    iget-object v1, p0, LX/Ani;->c:LX/Ank;

    invoke-virtual {v1, p4}, LX/Ank;->a(Landroid/view/View;)Ljava/lang/Object;

    move-result-object v1

    const-wide/16 v2, 0x2a

    const/high16 v4, 0x3f800000    # 1.0f

    const v5, 0x3f8ccccd    # 1.1f

    invoke-virtual/range {v0 .. v6}, LX/1Rg;->a(Ljava/lang/Object;JFFLandroid/animation/Animator$AnimatorListener;)LX/8sj;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1712799
    iget-object v0, p0, LX/Ani;->a:LX/1Rg;

    iget-object v1, p0, LX/Ani;->c:LX/Ank;

    invoke-virtual {v1, p4}, LX/Ank;->a(Landroid/view/View;)Ljava/lang/Object;

    move-result-object v1

    const-wide/16 v2, 0x2a

    const v4, 0x3f8ccccd    # 1.1f

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-virtual/range {v0 .. v6}, LX/1Rg;->a(Ljava/lang/Object;JFFLandroid/animation/Animator$AnimatorListener;)LX/8sj;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1712800
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Ljava/lang/Object;Landroid/view/View;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 1712793
    check-cast p2, LX/Anh;

    .line 1712794
    new-instance v0, Lcom/facebook/feedplugins/base/blingbar/BlingBarAnimationBuilder$1;

    invoke-direct {v0, p0, p3, p2}, Lcom/facebook/feedplugins/base/blingbar/BlingBarAnimationBuilder$1;-><init>(LX/Ani;Landroid/view/View;LX/Anh;)V

    return-object v0
.end method

.method public final a(Ljava/util/List;Ljava/lang/Object;Ljava/lang/Object;Landroid/view/View;)V
    .locals 6

    .prologue
    .line 1712778
    check-cast p2, LX/Anh;

    check-cast p3, LX/Anh;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1712779
    iget v0, p2, LX/Anh;->d:I

    iget v3, p3, LX/Anh;->d:I

    if-ne v0, v3, :cond_1

    iget-boolean v0, p2, LX/Anh;->b:Z

    iget-boolean v3, p3, LX/Anh;->b:Z

    if-ne v0, v3, :cond_1

    move v0, v1

    .line 1712780
    :goto_0
    iget-boolean v3, p3, LX/Anh;->a:Z

    iget-boolean v4, p2, LX/Anh;->a:Z

    if-ne v3, v4, :cond_2

    move v3, v1

    .line 1712781
    :goto_1
    iget v4, p3, LX/Anh;->d:I

    iget v5, p2, LX/Anh;->d:I

    if-ge v4, v5, :cond_3

    iget v4, p3, LX/Anh;->d:I

    if-lez v4, :cond_3

    .line 1712782
    :goto_2
    if-nez v0, :cond_0

    if-nez v1, :cond_0

    if-eqz v3, :cond_4

    .line 1712783
    :cond_0
    :goto_3
    return-void

    :cond_1
    move v0, v2

    .line 1712784
    goto :goto_0

    :cond_2
    move v3, v2

    .line 1712785
    goto :goto_1

    :cond_3
    move v1, v2

    .line 1712786
    goto :goto_2

    .line 1712787
    :cond_4
    iget v0, p3, LX/Anh;->d:I

    if-nez v0, :cond_5

    iget v0, p2, LX/Anh;->d:I

    if-eqz v0, :cond_5

    .line 1712788
    invoke-direct {p0, p1, p2, p3, p4}, LX/Ani;->b(Ljava/util/List;LX/Anh;LX/Anh;Landroid/view/View;)V

    .line 1712789
    :cond_5
    iget-boolean v0, p3, LX/Anh;->b:Z

    iget-boolean v1, p2, LX/Anh;->b:Z

    if-eq v0, v1, :cond_6

    .line 1712790
    invoke-direct {p0, p1, p3, p4}, LX/Ani;->a(Ljava/util/List;LX/Anh;Landroid/view/View;)V

    .line 1712791
    :cond_6
    iget v0, p3, LX/Anh;->d:I

    iget v1, p2, LX/Anh;->d:I

    if-lt v0, v1, :cond_0

    .line 1712792
    invoke-direct {p0, p1, p2, p3, p4}, LX/Ani;->c(Ljava/util/List;LX/Anh;LX/Anh;Landroid/view/View;)V

    goto :goto_3
.end method
