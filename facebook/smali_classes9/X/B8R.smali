.class public LX/B8R;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements LX/B7m;


# static fields
.field public static final a:LX/B7j;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/B7j",
            "<",
            "LX/B8R;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public b:LX/7Tk;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/B7W;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Landroid/widget/AutoCompleteTextView;

.field public e:Lcom/facebook/resources/ui/FbButton;

.field public f:Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;

.field public g:LX/B7w;

.field private h:Lcom/facebook/resources/ui/FbTextView;

.field public i:Lcom/facebook/widget/text/BetterTextView;

.field public j:LX/7Tj;

.field public k:LX/B7F;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1748797
    new-instance v0, LX/B8L;

    invoke-direct {v0}, LX/B8L;-><init>()V

    sput-object v0, LX/B8R;->a:LX/B7j;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1748858
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 1748859
    const v0, 0x7f0309cb

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1748860
    const v0, 0x7f0d18ed

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/AutoCompleteTextView;

    iput-object v0, p0, LX/B8R;->d:Landroid/widget/AutoCompleteTextView;

    .line 1748861
    const v0, 0x7f0d18e7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, LX/B8R;->e:Lcom/facebook/resources/ui/FbButton;

    .line 1748862
    const v0, 0x7f0d18ec

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/B8R;->h:Lcom/facebook/resources/ui/FbTextView;

    .line 1748863
    const v0, 0x7f0d18d4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/B8R;->i:Lcom/facebook/widget/text/BetterTextView;

    .line 1748864
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, LX/B8R;

    const-class p1, LX/7Tk;

    invoke-interface {v0, p1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object p1

    check-cast p1, LX/7Tk;

    invoke-static {v0}, LX/B7W;->a(LX/0QB;)LX/B7W;

    move-result-object v0

    check-cast v0, LX/B7W;

    iput-object p1, p0, LX/B8R;->b:LX/7Tk;

    iput-object v0, p0, LX/B8R;->c:LX/B7W;

    .line 1748865
    return-void
.end method

.method public static f(LX/B8R;)V
    .locals 1

    .prologue
    .line 1748856
    const v0, 0x7f020deb

    invoke-static {p0, v0}, LX/B8R;->setIconDrawable(LX/B8R;I)V

    .line 1748857
    return-void
.end method

.method public static setIconDrawable(LX/B8R;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1748853
    invoke-virtual {p0}, LX/B8R;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1748854
    iget-object v1, p0, LX/B8R;->d:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v1, v2, v2, v0, v2}, Landroid/widget/AutoCompleteTextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 1748855
    return-void
.end method

.method public static setInputKeypadType(LX/B8R;Lcom/facebook/common/locale/Country;)V
    .locals 2

    .prologue
    .line 1748849
    invoke-static {p1}, LX/46H;->a(Lcom/facebook/common/locale/Country;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1748850
    iget-object v0, p0, LX/B8R;->d:Landroid/widget/AutoCompleteTextView;

    const/16 v1, 0x70

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setInputType(I)V

    .line 1748851
    :goto_0
    return-void

    .line 1748852
    :cond_0
    iget-object v0, p0, LX/B8R;->d:Landroid/widget/AutoCompleteTextView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setInputType(I)V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1748843
    iget-object v0, p0, LX/B8R;->d:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1748844
    iget-object v0, p0, LX/B8R;->d:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 1748845
    iget-object v0, p0, LX/B8R;->d:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 1748846
    iget-object v0, p0, LX/B8R;->d:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1748847
    iget-object v0, p0, LX/B8R;->e:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1748848
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;LX/B7F;I)V
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 1748815
    iput-object p1, p0, LX/B8R;->f:Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;

    .line 1748816
    iput-object p2, p0, LX/B8R;->k:LX/B7F;

    .line 1748817
    iget-object v0, p0, LX/B8R;->h:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, LX/B8R;->f:Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->p()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1748818
    invoke-virtual {p2}, LX/B7F;->w()Lcom/facebook/common/locale/Country;

    move-result-object v1

    .line 1748819
    iget-object v0, p0, LX/B8R;->e:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v1}, Lcom/facebook/common/locale/LocaleMember;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 1748820
    iget-object v0, p0, LX/B8R;->d:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {p0}, LX/B8R;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a074f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/AutoCompleteTextView;->setHintTextColor(I)V

    .line 1748821
    iget-object v0, p0, LX/B8R;->d:Landroid/widget/AutoCompleteTextView;

    const/4 v2, 0x1

    new-array v2, v2, [Landroid/text/InputFilter;

    new-instance v3, Landroid/text/InputFilter$AllCaps;

    invoke-direct {v3}, Landroid/text/InputFilter$AllCaps;-><init>()V

    aput-object v3, v2, v4

    invoke-virtual {v0, v2}, Landroid/widget/AutoCompleteTextView;->setFilters([Landroid/text/InputFilter;)V

    .line 1748822
    iget-object v0, p0, LX/B8R;->d:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->q()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/AutoCompleteTextView;->setHint(Ljava/lang/CharSequence;)V

    .line 1748823
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->r()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->r()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1748824
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->r()LX/0Px;

    move-result-object v0

    .line 1748825
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1748826
    invoke-virtual {p2}, LX/B7F;->w()Lcom/facebook/common/locale/Country;

    move-result-object v6

    .line 1748827
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result p1

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-ge v3, p1, :cond_1

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1748828
    invoke-static {v2, v6}, LX/46H;->a(Ljava/lang/CharSequence;Lcom/facebook/common/locale/Country;)Z

    move-result p3

    if-eqz p3, :cond_0

    .line 1748829
    invoke-interface {v5, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1748830
    :cond_0
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0

    .line 1748831
    :cond_1
    move-object v2, v5

    .line 1748832
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1748833
    iget-object v3, p0, LX/B8R;->d:Landroid/widget/AutoCompleteTextView;

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {v3, v0}, Landroid/widget/AutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1748834
    :cond_2
    new-instance v0, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, LX/B8R;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x1090011

    invoke-direct {v0, v3, v4, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 1748835
    iget-object v2, p0, LX/B8R;->d:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v2, v0}, Landroid/widget/AutoCompleteTextView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1748836
    :cond_3
    invoke-static {p0, v1}, LX/B8R;->setInputKeypadType(LX/B8R;Lcom/facebook/common/locale/Country;)V

    .line 1748837
    invoke-static {p0}, LX/B8R;->f(LX/B8R;)V

    .line 1748838
    iget-object v0, p0, LX/B8R;->d:Landroid/widget/AutoCompleteTextView;

    new-instance v1, LX/B8M;

    invoke-direct {v1, p0}, LX/B8M;-><init>(LX/B8R;)V

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 1748839
    iget-object v0, p0, LX/B8R;->d:Landroid/widget/AutoCompleteTextView;

    new-instance v1, LX/B8N;

    invoke-direct {v1, p0}, LX/B8N;-><init>(LX/B8R;)V

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 1748840
    iget-object v0, p0, LX/B8R;->d:Landroid/widget/AutoCompleteTextView;

    new-instance v1, LX/B8O;

    invoke-direct {v1, p0}, LX/B8O;-><init>(LX/B8R;)V

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1748841
    iget-object v0, p0, LX/B8R;->e:Lcom/facebook/resources/ui/FbButton;

    new-instance v1, LX/B8Q;

    invoke-direct {v1, p0}, LX/B8Q;-><init>(LX/B8R;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1748842
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1748812
    const v0, 0x7f020dec

    invoke-static {p0, v0}, LX/B8R;->setIconDrawable(LX/B8R;I)V

    .line 1748813
    iget-object v0, p0, LX/B8R;->i:Lcom/facebook/widget/text/BetterTextView;

    invoke-static {v0, p1}, LX/B8v;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 1748814
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1748866
    iget-object v0, p0, LX/B8R;->d:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->requestFocus()Z

    .line 1748867
    iget-object v0, p0, LX/B8R;->d:Landroid/widget/AutoCompleteTextView;

    iget-object v1, p0, LX/B8R;->i:Lcom/facebook/widget/text/BetterTextView;

    invoke-static {v0, v1}, LX/B8v;->a(Landroid/view/View;Landroid/widget/TextView;)V

    .line 1748868
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1748809
    invoke-static {p0}, LX/B8R;->f(LX/B8R;)V

    .line 1748810
    iget-object v0, p0, LX/B8R;->i:Lcom/facebook/widget/text/BetterTextView;

    invoke-static {v0}, LX/B8v;->a(Landroid/widget/TextView;)V

    .line 1748811
    return-void
.end method

.method public getBoundedInfoFieldData()Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;
    .locals 1

    .prologue
    .line 1748808
    iget-object v0, p0, LX/B8R;->f:Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;

    return-object v0
.end method

.method public getCountry()Lcom/facebook/common/locale/Country;
    .locals 1

    .prologue
    .line 1748807
    iget-object v0, p0, LX/B8R;->e:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbButton;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/locale/Country;->a(Ljava/lang/String;)Lcom/facebook/common/locale/Country;

    move-result-object v0

    return-object v0
.end method

.method public getInputCustomToken()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1748806
    invoke-virtual {p0}, LX/B8R;->getInputValue()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getInputValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1748805
    iget-object v0, p0, LX/B8R;->d:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setInputValue(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1748800
    iget-object v0, p0, LX/B8R;->e:Lcom/facebook/resources/ui/FbButton;

    iget-object v1, p0, LX/B8R;->k:LX/B7F;

    invoke-virtual {v1}, LX/B7F;->w()Lcom/facebook/common/locale/Country;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/common/locale/LocaleMember;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 1748801
    iget-object v0, p0, LX/B8R;->d:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0, p1}, Landroid/widget/AutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1748802
    iget-object v0, p0, LX/B8R;->d:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0}, Landroid/widget/AutoCompleteTextView;->clearFocus()V

    .line 1748803
    iget-object v0, p0, LX/B8R;->e:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbButton;->clearFocus()V

    .line 1748804
    return-void
.end method

.method public setOnDataChangeListener(LX/B7w;)V
    .locals 0

    .prologue
    .line 1748798
    iput-object p1, p0, LX/B8R;->g:LX/B7w;

    .line 1748799
    return-void
.end method
