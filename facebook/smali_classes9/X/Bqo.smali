.class public LX/Bqo;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Bqp;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Bqo",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/Bqp;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1825249
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1825250
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/Bqo;->b:LX/0Zi;

    .line 1825251
    iput-object p1, p0, LX/Bqo;->a:LX/0Ot;

    .line 1825252
    return-void
.end method

.method public static a(LX/0QB;)LX/Bqo;
    .locals 4

    .prologue
    .line 1825253
    const-class v1, LX/Bqo;

    monitor-enter v1

    .line 1825254
    :try_start_0
    sget-object v0, LX/Bqo;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1825255
    sput-object v2, LX/Bqo;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1825256
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1825257
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1825258
    new-instance v3, LX/Bqo;

    const/16 p0, 0x1d09

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/Bqo;-><init>(LX/0Ot;)V

    .line 1825259
    move-object v0, v3

    .line 1825260
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1825261
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Bqo;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1825262
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1825263
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1De;Z)LX/1dQ;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Z)",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1825264
    const v0, -0x648c8310

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method

.method public static onClick(LX/1X1;Z)LX/1dQ;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            "Z)",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1825265
    const v0, -0x648c8310

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 4

    .prologue
    .line 1825266
    check-cast p2, LX/Bqn;

    .line 1825267
    invoke-static {}, LX/1cy;->f()LX/1np;

    move-result-object v1

    .line 1825268
    iget-object v0, p0, LX/Bqo;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Bqp;

    iget-object v2, p2, LX/Bqn;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p2, LX/Bqn;->b:LX/1Po;

    invoke-virtual {v0, p1, v2, v3, v1}, LX/Bqp;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Po;LX/1np;)LX/1Dg;

    move-result-object v2

    .line 1825269
    iget-object v0, v1, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 1825270
    check-cast v0, Ljava/lang/String;

    iput-object v0, p2, LX/Bqn;->c:Ljava/lang/String;

    .line 1825271
    invoke-static {v1}, LX/1cy;->a(LX/1np;)V

    .line 1825272
    return-object v2
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 1825273
    invoke-static {}, LX/1dS;->b()V

    .line 1825274
    iget v0, p1, LX/1dQ;->b:I

    .line 1825275
    packed-switch v0, :pswitch_data_0

    .line 1825276
    :goto_0
    return-object v3

    .line 1825277
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 1825278
    iget-object v1, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v0, p1, LX/1dQ;->c:[Ljava/lang/Object;

    const/4 v2, 0x0

    aget-object v0, v0, v2

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iget-object v2, p1, LX/1dQ;->a:LX/1X1;

    .line 1825279
    check-cast v2, LX/Bqn;

    .line 1825280
    iget-object v4, p0, LX/Bqo;->a:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/Bqp;

    iget-object v7, v2, LX/Bqn;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object p1, v2, LX/Bqn;->c:Ljava/lang/String;

    iget-object p2, v2, LX/Bqn;->b:LX/1Po;

    move-object v5, v1

    move v6, v0

    .line 1825281
    check-cast p2, LX/1Pr;

    invoke-static {v7, p2}, LX/Bqw;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pr;)LX/Br2;

    move-result-object v1

    .line 1825282
    iget-object v0, v4, LX/Bqp;->a:LX/Bqw;

    .line 1825283
    iget-object p0, v7, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object p0, p0

    .line 1825284
    check-cast p0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0, p0}, LX/Bqw;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result p0

    if-eqz p0, :cond_0

    .line 1825285
    :goto_1
    goto :goto_0

    .line 1825286
    :cond_0
    if-eqz v6, :cond_1

    .line 1825287
    iget-object v0, v4, LX/Bqp;->a:LX/Bqw;

    invoke-virtual {v5}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 1825288
    iget-object p0, v7, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object p0, p0

    .line 1825289
    check-cast p0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0, v2, p0, v1, p1}, LX/Bqw;->a(Landroid/content/Context;Lcom/facebook/graphql/model/GraphQLStory;LX/Br2;Ljava/lang/String;)V

    goto :goto_1

    .line 1825290
    :cond_1
    iget-object v0, v4, LX/Bqp;->a:LX/Bqw;

    invoke-virtual {v5}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 1825291
    iget-object p0, v7, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object p0, p0

    .line 1825292
    check-cast p0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1825293
    iget-object v4, v1, LX/Br2;->a:Ljava/lang/Integer;

    move-object v1, v4

    .line 1825294
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v2, p0, p1, v1}, LX/Bqw;->a(Landroid/content/Context;Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;I)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch -0x648c8310
        :pswitch_0
    .end packed-switch
.end method
