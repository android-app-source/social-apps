.class public LX/AhO;
.super Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;
.source ""

# interfaces
.implements LX/3qu;
.implements LX/3u7;
.implements LX/3uw;
.implements LX/3ux;


# instance fields
.field private a:LX/AhS;

.field public b:LX/AhN;

.field private c:LX/3v0;

.field private final d:I

.field private e:Landroid/graphics/Paint;

.field private f:I

.field private g:Z

.field private h:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1702519
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1702520
    invoke-virtual {p0}, LX/AhO;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0d77

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/AhO;->d:I

    .line 1702521
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, LX/AhO;->e:Landroid/graphics/Paint;

    .line 1702522
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, LX/AhO;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1702523
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 1702590
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1702591
    invoke-virtual {p0}, LX/AhO;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0d77

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/AhO;->d:I

    .line 1702592
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, LX/AhO;->e:Landroid/graphics/Paint;

    .line 1702593
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/AhO;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1702594
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 1702595
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1702596
    invoke-virtual {p0}, LX/AhO;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0d77

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/AhO;->d:I

    .line 1702597
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, LX/AhO;->e:Landroid/graphics/Paint;

    .line 1702598
    invoke-direct {p0, p1, p2, p3}, LX/AhO;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1702599
    return-void
.end method

.method private a(IIILjava/lang/CharSequence;)LX/3qv;
    .locals 2

    .prologue
    .line 1702600
    iget-object v0, p0, LX/AhO;->c:LX/3v0;

    invoke-virtual {v0, p1, p2, p3, p4}, LX/3v0;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v0

    check-cast v0, LX/3v3;

    .line 1702601
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/3v3;->setShowAsAction(I)V

    .line 1702602
    return-object v0
.end method

.method public static a(LX/AhO;)LX/3v0;
    .locals 2

    .prologue
    .line 1702603
    new-instance v0, LX/3v0;

    invoke-virtual {p0}, LX/AhO;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/3v0;-><init>(Landroid/content/Context;)V

    .line 1702604
    invoke-virtual {v0, p0}, LX/3v0;->a(LX/3u7;)V

    .line 1702605
    return-object v0
.end method

.method public static a(LX/AhO;LX/3qu;LX/3uE;)V
    .locals 2

    .prologue
    .line 1702606
    iget-object v0, p0, LX/AhO;->c:LX/3v0;

    if-ne p1, v0, :cond_0

    .line 1702607
    :goto_0
    return-void

    .line 1702608
    :cond_0
    iget-object v0, p0, LX/AhO;->c:LX/3v0;

    if-eqz v0, :cond_1

    .line 1702609
    iget-object v0, p0, LX/AhO;->c:LX/3v0;

    iget-object v1, p0, LX/AhO;->a:LX/AhS;

    invoke-virtual {v0, v1}, LX/3v0;->b(LX/3us;)V

    .line 1702610
    :cond_1
    iget-object v0, p0, LX/AhO;->a:LX/AhS;

    .line 1702611
    iput-object p2, v0, LX/3ut;->g:LX/3uE;

    .line 1702612
    check-cast p1, LX/3v0;

    iput-object p1, p0, LX/AhO;->c:LX/3v0;

    .line 1702613
    iget-object v0, p0, LX/AhO;->c:LX/3v0;

    iget-object v1, p0, LX/AhO;->a:LX/AhS;

    invoke-virtual {v0, v1}, LX/3v0;->a(LX/3us;)V

    goto :goto_0
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1702614
    invoke-super {p0, v4}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setOrientation(I)V

    .line 1702615
    new-instance v0, Landroid/view/ContextThemeWrapper;

    const v1, 0x7f0e0682

    invoke-direct {v0, p1, v1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 1702616
    new-instance v1, Landroid/graphics/Paint;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, LX/AhO;->e:Landroid/graphics/Paint;

    .line 1702617
    iget-object v1, p0, LX/AhO;->e:Landroid/graphics/Paint;

    invoke-virtual {p0}, LX/AhO;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a00e8

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 1702618
    new-instance v1, LX/AhS;

    invoke-direct {v1, v0, p0}, LX/AhS;-><init>(Landroid/content/Context;LX/3ux;)V

    iput-object v1, p0, LX/AhO;->a:LX/AhS;

    .line 1702619
    sget-object v1, LX/03r;->InlineActionBar:[I

    invoke-virtual {v0, p2, v1, p3, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1702620
    const/16 v1, 0x7

    const v2, 0x7f0208f0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 1702621
    iget-object v2, p0, LX/AhO;->a:LX/AhS;

    .line 1702622
    iput v1, v2, LX/AhS;->j:I

    .line 1702623
    const/16 v1, 0x6

    const v2, 0x7fffffff

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    .line 1702624
    invoke-virtual {p0, v1}, LX/AhO;->setMaxNumOfVisibleButtons(I)V

    .line 1702625
    const/16 v1, 0x8

    const v2, 0x7f0e0680

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 1702626
    iget-object v2, p0, LX/AhO;->a:LX/AhS;

    .line 1702627
    iput v1, v2, LX/AhS;->i:I

    .line 1702628
    const/16 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1702629
    const/16 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    .line 1702630
    iget-object v2, p0, LX/AhO;->a:LX/AhS;

    .line 1702631
    iput-object v1, v2, LX/AhS;->k:Landroid/content/res/ColorStateList;

    .line 1702632
    :cond_0
    const/16 v1, 0x5

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 1702633
    if-eqz v1, :cond_1

    .line 1702634
    invoke-static {p0}, LX/AhO;->a(LX/AhO;)LX/3v0;

    move-result-object v2

    .line 1702635
    const/4 v3, 0x0

    invoke-static {p0, v2, v3}, LX/AhO;->a(LX/AhO;LX/3qu;LX/3uE;)V

    .line 1702636
    invoke-virtual {v2}, LX/3v0;->f()V

    .line 1702637
    new-instance v3, LX/3ui;

    invoke-virtual {p0}, LX/AhO;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, LX/3ui;-><init>(Landroid/content/Context;)V

    .line 1702638
    invoke-virtual {v3, v1, p0}, LX/3ui;->inflate(ILandroid/view/Menu;)V

    .line 1702639
    invoke-virtual {v2}, LX/3v0;->g()V

    .line 1702640
    :goto_0
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1702641
    return-void

    .line 1702642
    :cond_1
    invoke-static {p0}, LX/AhO;->a(LX/AhO;)LX/3v0;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {p0, v1, v2}, LX/AhO;->a(LX/AhO;LX/3qu;LX/3uE;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(IIII)LX/3qv;
    .locals 2

    .prologue
    .line 1702643
    iget-object v0, p0, LX/AhO;->c:LX/3v0;

    invoke-virtual {v0, p1, p2, p3, p4}, LX/3v0;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    check-cast v0, LX/3v3;

    .line 1702644
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/3v3;->setShowAsAction(I)V

    .line 1702645
    return-object v0
.end method

.method public final a(LX/3v0;)V
    .locals 0

    .prologue
    .line 1702646
    return-void
.end method

.method public final a(ZZI)V
    .locals 0

    .prologue
    .line 1702647
    iput-boolean p1, p0, LX/AhO;->g:Z

    .line 1702648
    iput-boolean p2, p0, LX/AhO;->h:Z

    .line 1702649
    iput p3, p0, LX/AhO;->f:I

    .line 1702650
    return-void
.end method

.method public final a(LX/3v0;Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 1702651
    iget-object v0, p0, LX/AhO;->b:LX/AhN;

    if-eqz v0, :cond_0

    .line 1702652
    iget-object v0, p0, LX/AhO;->b:LX/AhN;

    invoke-interface {v0, p2}, LX/AhN;->a(Landroid/view/MenuItem;)Z

    move-result v0

    .line 1702653
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/3v3;)Z
    .locals 2

    .prologue
    .line 1702654
    iget-object v0, p0, LX/AhO;->c:LX/3v0;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, LX/3v0;->a(Landroid/view/MenuItem;I)Z

    move-result v0

    return v0
.end method

.method public final add(I)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 1702655
    const/4 v0, 0x0

    .line 1702656
    invoke-virtual {p0, v0, v0, v0, p1}, LX/AhO;->a(IIII)LX/3qv;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic add(IIII)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 1702657
    invoke-virtual {p0, p1, p2, p3, p4}, LX/AhO;->a(IIII)LX/3qv;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 1702673
    invoke-direct {p0, p1, p2, p3, p4}, LX/AhO;->a(IIILjava/lang/CharSequence;)LX/3qv;

    move-result-object v0

    return-object v0
.end method

.method public final add(Ljava/lang/CharSequence;)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 1702658
    const/4 v0, 0x0

    .line 1702659
    invoke-direct {p0, v0, v0, v0, p1}, LX/AhO;->a(IIILjava/lang/CharSequence;)LX/3qv;

    move-result-object v0

    return-object v0
.end method

.method public final addIntentOptions(IIILandroid/content/ComponentName;[Landroid/content/Intent;Landroid/content/Intent;I[Landroid/view/MenuItem;)I
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1702672
    const/4 v0, 0x0

    return v0
.end method

.method public final addSubMenu(I)Landroid/view/SubMenu;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1702671
    const/4 v0, 0x0

    return-object v0
.end method

.method public final addSubMenu(IIII)Landroid/view/SubMenu;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1702670
    const/4 v0, 0x0

    return-object v0
.end method

.method public final addSubMenu(IIILjava/lang/CharSequence;)Landroid/view/SubMenu;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1702669
    const/4 v0, 0x0

    return-object v0
.end method

.method public final addSubMenu(Ljava/lang/CharSequence;)Landroid/view/SubMenu;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1702674
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1702667
    iget-object v0, p0, LX/AhO;->c:LX/3v0;

    invoke-virtual {v0}, LX/3v0;->f()V

    .line 1702668
    return-void
.end method

.method public b_(LX/3v0;)V
    .locals 0

    .prologue
    .line 1702666
    return-void
.end method

.method public final checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 1

    .prologue
    .line 1702665
    if-eqz p1, :cond_0

    instance-of v0, p1, Landroid/widget/LinearLayout$LayoutParams;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final clear()V
    .locals 1

    .prologue
    .line 1702663
    iget-object v0, p0, LX/AhO;->c:LX/3v0;

    invoke-virtual {v0}, LX/3v0;->clear()V

    .line 1702664
    return-void
.end method

.method public final close()V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1702662
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 1702660
    iget-object v0, p0, LX/AhO;->c:LX/3v0;

    invoke-virtual {v0}, LX/3v0;->g()V

    .line 1702661
    return-void
.end method

.method public final dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 1

    .prologue
    .line 1702582
    const/4 v0, 0x0

    return v0
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1702583
    invoke-super {p0, p1}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->draw(Landroid/graphics/Canvas;)V

    .line 1702584
    iget-boolean v0, p0, LX/AhO;->g:Z

    if-eqz v0, :cond_0

    .line 1702585
    iget v0, p0, LX/AhO;->f:I

    int-to-float v1, v0

    invoke-virtual {p0}, LX/AhO;->getWidth()I

    move-result v0

    iget v3, p0, LX/AhO;->f:I

    sub-int/2addr v0, v3

    int-to-float v3, v0

    iget-object v5, p0, LX/AhO;->e:Landroid/graphics/Paint;

    move-object v0, p1

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1702586
    :cond_0
    iget-boolean v0, p0, LX/AhO;->h:Z

    if-eqz v0, :cond_1

    .line 1702587
    invoke-virtual {p0}, LX/AhO;->getHeight()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 1702588
    iget v1, p0, LX/AhO;->f:I

    int-to-float v1, v1

    int-to-float v2, v0

    invoke-virtual {p0}, LX/AhO;->getWidth()I

    move-result v3

    iget v4, p0, LX/AhO;->f:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    int-to-float v4, v0

    iget-object v5, p0, LX/AhO;->e:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1702589
    :cond_1
    return-void
.end method

.method public final findItem(I)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 1702518
    iget-object v0, p0, LX/AhO;->c:LX/3v0;

    invoke-virtual {v0, p1}, LX/3v0;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 1702524
    invoke-virtual {p0}, LX/AhO;->generateDefaultLayoutParams()Landroid/widget/LinearLayout$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method public generateDefaultLayoutParams()Landroid/widget/LinearLayout$LayoutParams;
    .locals 3

    .prologue
    .line 1702525
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    iget v1, p0, LX/AhO;->d:I

    const/4 v2, -0x1

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1702526
    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 1702527
    return-object v0
.end method

.method public final bridge synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 1702528
    invoke-virtual {p0, p1}, LX/AhO;->generateLayoutParams(Landroid/util/AttributeSet;)Landroid/widget/LinearLayout$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 1702529
    invoke-virtual {p0, p1}, LX/AhO;->generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/widget/LinearLayout$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method public final generateLayoutParams(Landroid/util/AttributeSet;)Landroid/widget/LinearLayout$LayoutParams;
    .locals 2

    .prologue
    .line 1702530
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {p0}, LX/AhO;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method public final generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/widget/LinearLayout$LayoutParams;
    .locals 2

    .prologue
    .line 1702531
    instance-of v0, p1, Landroid/widget/LinearLayout$LayoutParams;

    if-eqz v0, :cond_1

    .line 1702532
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    check-cast p1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, p1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(Landroid/widget/LinearLayout$LayoutParams;)V

    .line 1702533
    iget v1, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    if-gtz v1, :cond_0

    .line 1702534
    const/16 v1, 0x10

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 1702535
    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, LX/AhO;->generateDefaultLayoutParams()Landroid/widget/LinearLayout$LayoutParams;

    move-result-object v0

    goto :goto_0
.end method

.method public final getItem(I)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 1702536
    iget-object v0, p0, LX/AhO;->c:LX/3v0;

    invoke-virtual {v0, p1}, LX/3v0;->getItem(I)Landroid/view/MenuItem;

    move-result-object v0

    return-object v0
.end method

.method public getSuggestedMinimumWidth()I
    .locals 3

    .prologue
    .line 1702537
    invoke-super {p0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->getSuggestedMinimumWidth()I

    move-result v0

    iget v1, p0, LX/AhO;->d:I

    invoke-virtual {p0}, LX/AhO;->getChildCount()I

    move-result v2

    mul-int/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method public getWindowAnimations()I
    .locals 1

    .prologue
    .line 1702538
    const/4 v0, 0x0

    return v0
.end method

.method public final hasVisibleItems()Z
    .locals 1

    .prologue
    .line 1702539
    iget-object v0, p0, LX/AhO;->c:LX/3v0;

    invoke-virtual {v0}, LX/3v0;->hasVisibleItems()Z

    move-result v0

    return v0
.end method

.method public final isShortcutKey(ILandroid/view/KeyEvent;)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1702540
    const/4 v0, 0x0

    return v0
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 1702541
    invoke-super {p0, p1}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1702542
    iget-object v0, p0, LX/AhO;->a:LX/AhS;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/3ut;->b(Z)V

    .line 1702543
    iget-object v0, p0, LX/AhO;->a:LX/AhS;

    invoke-virtual {v0}, LX/AhS;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1702544
    iget-object v0, p0, LX/AhO;->a:LX/AhS;

    invoke-virtual {v0}, LX/AhS;->d()Z

    .line 1702545
    iget-object v0, p0, LX/AhO;->a:LX/AhS;

    invoke-virtual {v0}, LX/AhS;->c()Z

    .line 1702546
    :cond_0
    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x7b392423

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1702547
    invoke-super {p0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->onDetachedFromWindow()V

    .line 1702548
    iget-object v1, p0, LX/AhO;->a:LX/AhS;

    invoke-virtual {v1}, LX/AhS;->e()Z

    .line 1702549
    const/16 v1, 0x2d

    const v2, 0x3be46216

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onMeasure(II)V
    .locals 4

    .prologue
    .line 1702550
    invoke-virtual {p0}, LX/AhO;->getSuggestedMinimumWidth()I

    move-result v0

    invoke-static {v0, p1}, LX/AhO;->getDefaultSize(II)I

    move-result v0

    .line 1702551
    invoke-virtual {p0}, LX/AhO;->getPaddingLeft()I

    move-result v1

    .line 1702552
    invoke-virtual {p0}, LX/AhO;->getPaddingRight()I

    move-result v2

    .line 1702553
    sub-int/2addr v0, v1

    sub-int/2addr v0, v2

    .line 1702554
    iget-object v1, p0, LX/AhO;->a:LX/AhS;

    iget v2, p0, LX/AhO;->d:I

    .line 1702555
    iput v0, v1, LX/AhS;->m:I

    .line 1702556
    iput v2, v1, LX/AhS;->q:I

    .line 1702557
    const/4 v3, 0x1

    iput-boolean v3, v1, LX/AhS;->n:Z

    .line 1702558
    iget-object v0, p0, LX/AhO;->c:LX/3v0;

    if-eqz v0, :cond_0

    .line 1702559
    iget-object v0, p0, LX/AhO;->a:LX/AhS;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/3ut;->b(Z)V

    .line 1702560
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->onMeasure(II)V

    .line 1702561
    return-void
.end method

.method public final performIdentifierAction(II)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1702562
    const/4 v0, 0x0

    return v0
.end method

.method public final performShortcut(ILandroid/view/KeyEvent;I)Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1702563
    const/4 v0, 0x0

    return v0
.end method

.method public final removeGroup(I)V
    .locals 1

    .prologue
    .line 1702564
    iget-object v0, p0, LX/AhO;->c:LX/3v0;

    invoke-virtual {v0, p1}, LX/3v0;->removeGroup(I)V

    .line 1702565
    return-void
.end method

.method public final removeItem(I)V
    .locals 1

    .prologue
    .line 1702566
    iget-object v0, p0, LX/AhO;->c:LX/3v0;

    invoke-virtual {v0, p1}, LX/3v0;->removeItem(I)V

    .line 1702567
    return-void
.end method

.method public final setGroupCheckable(IZZ)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1702568
    return-void
.end method

.method public final setGroupEnabled(IZ)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1702569
    return-void
.end method

.method public final setGroupVisible(IZ)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1702570
    return-void
.end method

.method public setInlineActionBarActionItemsSelected(LX/AhM;)V
    .locals 1

    .prologue
    .line 1702571
    iget-object v0, p0, LX/AhO;->a:LX/AhS;

    .line 1702572
    iput-object p1, v0, LX/AhS;->h:LX/AhM;

    .line 1702573
    return-void
.end method

.method public setMaxNumOfVisibleButtons(I)V
    .locals 1

    .prologue
    .line 1702574
    iget-object v0, p0, LX/AhO;->a:LX/AhS;

    .line 1702575
    iput p1, v0, LX/AhS;->p:I

    .line 1702576
    return-void
.end method

.method public setOrientation(I)V
    .locals 2

    .prologue
    .line 1702577
    if-eqz p1, :cond_0

    .line 1702578
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "InlineActionBar only supports horizontal orientation"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1702579
    :cond_0
    return-void
.end method

.method public setQwertyMode(Z)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1702580
    return-void
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 1702581
    iget-object v0, p0, LX/AhO;->c:LX/3v0;

    invoke-virtual {v0}, LX/3v0;->size()I

    move-result v0

    return v0
.end method
