.class public LX/AZN;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:I

.field public final b:I


# direct methods
.method public constructor <init>(ILcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;)V
    .locals 4

    .prologue
    .line 1686944
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1686945
    int-to-float v0, p1

    iget v1, p2, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->payoutEstimateFactorHigh:F

    mul-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-int v0, v0

    .line 1686946
    int-to-float v1, p1

    iget v2, p2, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->payoutEstimateFactorLow:F

    mul-float/2addr v1, v2

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-int v1, v2

    .line 1686947
    if-ne v0, v1, :cond_0

    .line 1686948
    add-int/lit8 v0, v0, 0x1

    .line 1686949
    :cond_0
    iput v0, p0, LX/AZN;->a:I

    .line 1686950
    iput v1, p0, LX/AZN;->b:I

    .line 1686951
    return-void
.end method

.method public static a(Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;)Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1686943
    iget v0, p0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->payoutEstimateFactorLow:F

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->payoutEstimateFactorHigh:F

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
