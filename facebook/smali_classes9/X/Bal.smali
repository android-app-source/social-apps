.class public final synthetic LX/Bal;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final synthetic a:[I

.field public static final synthetic b:[I


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1799555
    invoke-static {}, LX/Bam;->values()[LX/Bam;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/Bal;->b:[I

    :try_start_0
    sget-object v0, LX/Bal;->b:[I

    sget-object v1, LX/Bam;->STANDARD:LX/Bam;

    invoke-virtual {v1}, LX/Bam;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_5

    :goto_0
    :try_start_1
    sget-object v0, LX/Bal;->b:[I

    sget-object v1, LX/Bam;->NARROW:LX/Bam;

    invoke-virtual {v1}, LX/Bam;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_4

    :goto_1
    :try_start_2
    sget-object v0, LX/Bal;->b:[I

    sget-object v1, LX/Bam;->CUSTOM:LX/Bam;

    invoke-virtual {v1}, LX/Bam;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_3

    .line 1799556
    :goto_2
    invoke-static {}, LX/Bap;->values()[LX/Bap;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/Bal;->a:[I

    :try_start_3
    sget-object v0, LX/Bal;->a:[I

    sget-object v1, LX/Bap;->IMAGE:LX/Bap;

    invoke-virtual {v1}, LX/Bap;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_2

    :goto_3
    :try_start_4
    sget-object v0, LX/Bal;->a:[I

    sget-object v1, LX/Bap;->FACEPILE:LX/Bap;

    invoke-virtual {v1}, LX/Bap;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_1

    :goto_4
    :try_start_5
    sget-object v0, LX/Bal;->a:[I

    sget-object v1, LX/Bap;->OTHER:LX/Bap;

    invoke-virtual {v1}, LX/Bap;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_0

    :goto_5
    return-void

    :catch_0
    goto :goto_5

    :catch_1
    goto :goto_4

    :catch_2
    goto :goto_3

    :catch_3
    goto :goto_2

    :catch_4
    goto :goto_1

    :catch_5
    goto :goto_0
.end method
