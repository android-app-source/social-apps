.class public LX/CAM;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/CAK;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CAN;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1855268
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/CAM;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/CAN;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1855232
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1855233
    iput-object p1, p0, LX/CAM;->b:LX/0Ot;

    .line 1855234
    return-void
.end method

.method public static a(LX/0QB;)LX/CAM;
    .locals 4

    .prologue
    .line 1855257
    const-class v1, LX/CAM;

    monitor-enter v1

    .line 1855258
    :try_start_0
    sget-object v0, LX/CAM;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1855259
    sput-object v2, LX/CAM;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1855260
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1855261
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1855262
    new-instance v3, LX/CAM;

    const/16 p0, 0x20a3

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/CAM;-><init>(LX/0Ot;)V

    .line 1855263
    move-object v0, v3

    .line 1855264
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1855265
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/CAM;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1855266
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1855267
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 2

    .prologue
    .line 1855245
    check-cast p2, LX/CAL;

    .line 1855246
    iget-object v0, p0, LX/CAM;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CAN;

    iget-object v1, p2, LX/CAL;->a:Lcom/facebook/graphql/model/GraphQLStory;

    const/4 p2, 0x1

    .line 1855247
    iget-object p0, v0, LX/CAN;->a:LX/1WX;

    invoke-virtual {p0, p1}, LX/1WX;->c(LX/1De;)LX/1XJ;

    move-result-object p0

    invoke-virtual {p0, v1}, LX/1XJ;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/1XJ;

    move-result-object p0

    invoke-virtual {p0, p2}, LX/1XJ;->a(Z)LX/1XJ;

    move-result-object p0

    .line 1855248
    iget-object v1, p0, LX/1XJ;->a:LX/1XI;

    iput-boolean p2, v1, LX/1XI;->c:Z

    .line 1855249
    move-object p0, p0

    .line 1855250
    const p2, 0x55beb93f

    const/4 v1, 0x0

    invoke-static {p1, p2, v1}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p2

    move-object p2, p2

    .line 1855251
    iget-object p1, p0, LX/1XJ;->a:LX/1XI;

    iput-object p2, p1, LX/1XI;->e:LX/1dQ;

    .line 1855252
    move-object p0, p0

    .line 1855253
    iget-object p2, v0, LX/CAN;->c:LX/1VI;

    invoke-virtual {p2}, LX/1VI;->a()Z

    move-result p2

    if-eqz p2, :cond_0

    .line 1855254
    const p2, 0x7f0a00aa

    invoke-virtual {p0, p2}, LX/1XJ;->h(I)LX/1XJ;

    .line 1855255
    :cond_0
    invoke-virtual {p0}, LX/1X5;->b()LX/1Dg;

    move-result-object p0

    move-object v0, p0

    .line 1855256
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1855235
    invoke-static {}, LX/1dS;->b()V

    .line 1855236
    iget v0, p1, LX/1dQ;->b:I

    .line 1855237
    packed-switch v0, :pswitch_data_0

    .line 1855238
    :goto_0
    return-object v2

    .line 1855239
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 1855240
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1855241
    check-cast v1, LX/CAL;

    .line 1855242
    iget-object v3, p0, LX/CAM;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/CAN;

    iget-object p1, v1, LX/CAL;->a:Lcom/facebook/graphql/model/GraphQLStory;

    iget-object p2, v1, LX/CAL;->b:LX/1Po;

    .line 1855243
    iget-object p0, v3, LX/CAN;->b:LX/CAJ;

    invoke-virtual {p0, v0, p1, p2}, LX/CAJ;->a(Landroid/view/View;Lcom/facebook/graphql/model/GraphQLStory;LX/1Po;)V

    .line 1855244
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x55beb93f
        :pswitch_0
    .end packed-switch
.end method
