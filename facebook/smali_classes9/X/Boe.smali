.class public final LX/Boe;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic b:Landroid/view/View;

.field public final synthetic c:LX/Bof;


# direct methods
.method public constructor <init>(LX/Bof;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 1821901
    iput-object p1, p0, LX/Boe;->c:LX/Bof;

    iput-object p2, p0, LX/Boe;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iput-object p3, p0, LX/Boe;->b:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 6

    .prologue
    .line 1821890
    iget-object v0, p0, LX/Boe;->c:LX/Bof;

    iget-object v0, v0, LX/Bof;->b:LX/1dt;

    iget-object v0, v0, LX/1dt;->v:LX/1e6;

    .line 1821891
    new-instance v2, LX/4Cq;

    invoke-direct {v2}, LX/4Cq;-><init>()V

    iget-object v1, v0, LX/1e6;->b:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1821892
    const-string v3, "actor_id"

    invoke-virtual {v2, v3, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1821893
    move-object v1, v2

    .line 1821894
    new-instance v2, LX/AD1;

    invoke-direct {v2}, LX/AD1;-><init>()V

    move-object v2, v2

    .line 1821895
    const-string v3, "input"

    invoke-virtual {v2, v3, v1}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1821896
    invoke-static {v2}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v1

    .line 1821897
    iget-object v2, v0, LX/1e6;->a:LX/0tX;

    invoke-virtual {v2, v1}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1821898
    iget-object v0, p0, LX/Boe;->c:LX/Bof;

    iget-object v0, v0, LX/Bof;->b:LX/1dt;

    iget-object v1, p0, LX/Boe;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p0, LX/Boe;->b:Landroid/view/View;

    const/4 v3, 0x0

    sget-object v4, Lcom/facebook/graphql/enums/StoryVisibility;->GONE:Lcom/facebook/graphql/enums/StoryVisibility;

    const/4 v5, 0x0

    .line 1821899
    invoke-virtual/range {v0 .. v5}, LX/1SX;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;Lcom/facebook/analytics/logger/HoneyClientEvent;Lcom/facebook/graphql/enums/StoryVisibility;Z)V

    .line 1821900
    const/4 v0, 0x1

    return v0
.end method
