.class public final LX/BRO;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/5QV;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1783907
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1783908
    iput-object v0, p0, LX/BRO;->a:LX/5QV;

    .line 1783909
    iput-object v0, p0, LX/BRO;->b:Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;

    .line 1783910
    iput-object v0, p0, LX/BRO;->c:Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;

    .line 1783911
    return-void
.end method

.method public constructor <init>(Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;)V
    .locals 1

    .prologue
    .line 1783912
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1783913
    iget-object v0, p1, Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;->a:LX/5QV;

    iput-object v0, p0, LX/BRO;->a:LX/5QV;

    .line 1783914
    iget-object v0, p1, Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;->b:Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;

    iput-object v0, p0, LX/BRO;->b:Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;

    .line 1783915
    iget-object v0, p1, Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;->c:Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;

    iput-object v0, p0, LX/BRO;->c:Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;

    .line 1783916
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/BRO;
    .locals 5

    .prologue
    .line 1783917
    iget-object v0, p0, LX/BRO;->a:LX/5QV;

    iget-object v1, p0, LX/BRO;->b:Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;

    invoke-static {v0, v1}, LX/B5R;->a(LX/5QV;Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;)LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    .line 1783918
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5QV;

    .line 1783919
    invoke-interface {v0}, LX/5QV;->c()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, p1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1783920
    iput-object v0, p0, LX/BRO;->a:LX/5QV;

    .line 1783921
    return-object p0

    .line 1783922
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1783923
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "id not found: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a()Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;
    .locals 4

    .prologue
    .line 1783924
    iget-object v0, p0, LX/BRO;->a:LX/5QV;

    if-nez v0, :cond_0

    iget-object v0, p0, LX/BRO;->b:Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;

    if-nez v0, :cond_0

    iget-object v0, p0, LX/BRO;->c:Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;

    if-nez v0, :cond_0

    .line 1783925
    :goto_0
    new-instance v0, Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;

    iget-object v1, p0, LX/BRO;->a:LX/5QV;

    iget-object v2, p0, LX/BRO;->b:Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;

    iget-object v3, p0, LX/BRO;->c:Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;-><init>(LX/5QV;Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;)V

    return-object v0

    .line 1783926
    :cond_0
    iget-object v0, p0, LX/BRO;->a:LX/5QV;

    invoke-static {v0}, LX/5Qm;->b(LX/5QV;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    goto :goto_0
.end method
