.class public final LX/CKd;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/B5Z;

.field public final synthetic b:LX/CKg;


# direct methods
.method public constructor <init>(LX/CKg;LX/B5Z;)V
    .locals 0

    .prologue
    .line 1877372
    iput-object p1, p0, LX/CKd;->b:LX/CKg;

    iput-object p2, p0, LX/CKd;->a:LX/B5Z;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 9

    .prologue
    .line 1877373
    if-nez p1, :cond_0

    iget-object v0, p0, LX/CKd;->b:LX/CKg;

    .line 1877374
    iget-object v1, v0, LX/CKg;->m:LX/0ad;

    sget-short v2, LX/8Bi;->a:S

    const/4 p1, 0x0

    invoke-interface {v1, v2, p1}, LX/0ad;->a(SZ)Z

    move-result v1

    move v0, v1

    .line 1877375
    if-eqz v0, :cond_0

    .line 1877376
    iget-object v0, p0, LX/CKd;->b:LX/CKg;

    iget-object v1, p0, LX/CKd;->a:LX/B5Z;

    .line 1877377
    invoke-interface {v1}, LX/B5Z;->iT_()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBMessengerSubscriptionInfoModel$MessengerContentSubscriptionOptionModel;

    move-result-object v4

    .line 1877378
    invoke-interface {v1}, LX/B5Z;->e()LX/B5Y;

    move-result-object v2

    invoke-interface {v2}, LX/B5Y;->n()LX/8Yp;

    move-result-object v2

    invoke-static {v2}, LX/CKg;->a(LX/8Yp;)Lcom/facebook/user/model/User;

    move-result-object v2

    iput-object v2, v0, LX/CKg;->i:Lcom/facebook/user/model/User;

    .line 1877379
    invoke-interface {v1}, LX/B5Z;->d()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, LX/CKg;->j:Ljava/lang/String;

    .line 1877380
    iget-object v2, v0, LX/0Dp;->c:Landroid/view/View;

    check-cast v2, Lcom/facebook/messaging/business/subscription/instantarticle/view/BusinessIASubscribeBannerView;

    invoke-virtual {v4}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBMessengerSubscriptionInfoModel$MessengerContentSubscriptionOptionModel;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$FBMessengerSubscriptionInfoModel$MessengerContentSubscriptionOptionModel;->a()Ljava/lang/String;

    move-result-object v4

    iget-object v5, v0, LX/CKg;->i:Lcom/facebook/user/model/User;

    .line 1877381
    new-instance v6, LX/CN8;

    invoke-direct {v6}, LX/CN8;-><init>()V

    iget-object v7, v0, LX/CKg;->g:LX/3Rb;

    .line 1877382
    iput-object v7, v6, LX/CN8;->a:LX/3Rb;

    .line 1877383
    move-object v6, v6

    .line 1877384
    iget-object v7, v5, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v7, v7

    .line 1877385
    invoke-virtual {v7}, Lcom/facebook/user/model/UserKey;->c()Ljava/lang/String;

    move-result-object v7

    .line 1877386
    iput-object v7, v6, LX/CN8;->b:Ljava/lang/String;

    .line 1877387
    move-object v6, v6

    .line 1877388
    sget-object v7, LX/8ue;->MESSENGER:LX/8ue;

    .line 1877389
    iput-object v7, v6, LX/CN8;->f:LX/8ue;

    .line 1877390
    move-object v6, v6

    .line 1877391
    iget-object v7, v5, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v7, v7

    .line 1877392
    invoke-static {v7}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v7

    .line 1877393
    iput-object v7, v6, LX/CN8;->e:LX/0Px;

    .line 1877394
    move-object v6, v6

    .line 1877395
    invoke-virtual {v5}, Lcom/facebook/user/model/User;->j()Ljava/lang/String;

    move-result-object v7

    .line 1877396
    iput-object v7, v6, LX/CN8;->g:Ljava/lang/String;

    .line 1877397
    move-object v6, v6

    .line 1877398
    invoke-virtual {v6}, LX/CN8;->a()LX/CN9;

    move-result-object v6

    move-object v5, v6

    .line 1877399
    iget-object v6, v0, LX/CKg;->i:Lcom/facebook/user/model/User;

    .line 1877400
    iget-object v7, v6, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v6, v7

    .line 1877401
    const-string v7, "instant_article"

    new-instance v8, LX/CKe;

    invoke-direct {v8, v0}, LX/CKe;-><init>(LX/CKg;)V

    invoke-virtual/range {v2 .. v8}, Lcom/facebook/messaging/business/subscription/instantarticle/view/BusinessIASubscribeBannerView;->a(Ljava/lang/String;Ljava/lang/String;LX/8Vc;Ljava/lang/String;Ljava/lang/String;LX/CKe;)V

    .line 1877402
    invoke-virtual {v0}, LX/0Dp;->a()V

    .line 1877403
    :cond_0
    return-void
.end method
