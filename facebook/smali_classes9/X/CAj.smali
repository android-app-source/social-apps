.class public final LX/CAj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

.field public final synthetic b:Lcom/facebook/graphql/model/GraphQLStory;

.field public final synthetic c:LX/355;


# direct methods
.method public constructor <init>(LX/355;Lcom/facebook/graphql/model/GraphQLStoryActionLink;Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 0

    .prologue
    .line 1855640
    iput-object p1, p0, LX/CAj;->c:LX/355;

    iput-object p2, p0, LX/CAj;->a:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    iput-object p3, p0, LX/CAj;->b:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 11

    .prologue
    const/4 v0, 0x1

    const/4 v9, 0x0

    const/4 v1, 0x2

    const v2, 0x119498a6

    invoke-static {v1, v0, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v10

    .line 1855641
    iget-object v1, p0, LX/CAj;->a:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aU()Lcom/facebook/graphql/model/GraphQLImageOverlay;

    move-result-object v5

    .line 1855642
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Landroid/app/Activity;

    invoke-static {v1, v2}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    .line 1855643
    iget-object v2, p0, LX/CAj;->b:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v2}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v3

    .line 1855644
    iget-object v2, p0, LX/CAj;->c:LX/355;

    iget-object v2, v2, LX/355;->g:LX/0ad;

    sget-short v4, LX/B3X;->c:S

    invoke-interface {v2, v4, v9}, LX/0ad;->a(SZ)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1855645
    :cond_0
    :goto_0
    iget-object v0, p0, LX/CAj;->a:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->j()Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;

    move-result-object v2

    .line 1855646
    if-eqz v1, :cond_1

    if-eqz v3, :cond_1

    iget-object v0, p0, LX/CAj;->a:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->j()Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1855647
    iget-object v0, p0, LX/CAj;->c:LX/355;

    iget-object v0, v0, LX/355;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/B35;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->B()Ljava/lang/String;

    move-result-object v3

    const-string v4, "story_cta"

    invoke-static {v5}, LX/B5O;->a(Lcom/facebook/graphql/model/GraphQLImageOverlay;)LX/5QV;

    move-result-object v5

    iget-object v6, p0, LX/CAj;->a:Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->w()J

    move-result-wide v6

    const/4 v8, 0x0

    invoke-virtual/range {v0 .. v9}, LX/B35;->a(Landroid/app/Activity;Lcom/facebook/graphql/enums/GraphQLProfilePictureActionLinkType;Ljava/lang/String;Ljava/lang/String;LX/5QV;JLcom/facebook/productionprompts/logging/PromptAnalytics;I)V

    .line 1855648
    :cond_1
    const v0, -0x455dea91

    invoke-static {v0, v10}, LX/02F;->a(II)V

    return-void

    .line 1855649
    :cond_2
    iget-object v2, p0, LX/CAj;->c:LX/355;

    iget-object v2, v2, LX/355;->g:LX/0ad;

    sget-short v4, LX/B3X;->d:S

    invoke-interface {v2, v4, v9}, LX/0ad;->a(SZ)Z

    move-result v2

    if-eqz v2, :cond_3

    move v9, v0

    .line 1855650
    goto :goto_0

    .line 1855651
    :cond_3
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLMedia;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v2

    const v4, 0x4ed245b

    if-ne v2, v4, :cond_0

    move v9, v0

    .line 1855652
    goto :goto_0
.end method
