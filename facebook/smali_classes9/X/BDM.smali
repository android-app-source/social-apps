.class public final LX/BDM;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1763048
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 1763049
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1763050
    :goto_0
    return v1

    .line 1763051
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1763052
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 1763053
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1763054
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1763055
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 1763056
    const-string v4, "notif_options"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1763057
    invoke-static {p0, p1}, LX/BDL;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 1763058
    :cond_2
    const-string v4, "option_set_display"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1763059
    invoke-static {p0, p1}, LX/BDb;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 1763060
    :cond_3
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1763061
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1763062
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1763063
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    move v2, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1763064
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1763065
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1763066
    if-eqz v0, :cond_0

    .line 1763067
    const-string v1, "notif_options"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1763068
    invoke-static {p0, v0, p2, p3}, LX/BDL;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1763069
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1763070
    if-eqz v0, :cond_1

    .line 1763071
    const-string v1, "option_set_display"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1763072
    invoke-static {p0, v0, p2}, LX/BDb;->a(LX/15i;ILX/0nX;)V

    .line 1763073
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1763074
    return-void
.end method
