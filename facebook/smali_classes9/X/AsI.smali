.class public LX/AsI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0iK;
.implements LX/AsA;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParamsSpec$ProvidesInspirationDoodleParams;",
        ":",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingDataSpec$ProvidesInspirationLoggingData;",
        ":",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationStateSpec$ProvidesInspirationState;",
        "Mutation::",
        "Lcom/facebook/ipc/composer/dataaccessor/ComposerCanSave;",
        ":",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParamsSpec$SetsInspirationDoodleParams",
        "<TMutation;>;:",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingDataSpec$SetsInspirationLoggingData",
        "<TMutation;>;:",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationStateSpec$SetsInspirationState",
        "<TMutation;>;DerivedData:",
        "Ljava/lang/Object;",
        "Services::",
        "LX/0il",
        "<TModelData;>;:",
        "LX/0ik",
        "<TDerivedData;>;:",
        "LX/0im",
        "<TMutation;>;>",
        "Ljava/lang/Object;",
        "LX/0iK",
        "<TModelData;TDerivedData;>;",
        "LX/AsA;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TServices;>;"
        }
    .end annotation
.end field

.field public final b:LX/Ase;

.field public final c:Landroid/view/ViewStub;

.field public final d:Landroid/view/WindowManager;

.field public final e:LX/0w3;

.field private final f:Landroid/view/ViewTreeObserver$OnDrawListener;

.field private final g:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

.field public h:Z

.field public i:Z

.field public j:Landroid/view/View;

.field public k:Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;

.field public l:Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;

.field public final m:LX/AsF;


# direct methods
.method public constructor <init>(Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;LX/0il;Landroid/view/ViewStub;LX/Asf;Landroid/view/WindowManager;LX/0w3;)V
    .locals 2
    .param p1    # Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/0il;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Landroid/view/ViewStub;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;",
            "TServices;",
            "Landroid/view/ViewStub;",
            "LX/Asf;",
            "Landroid/view/WindowManager;",
            "LX/0w3;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1720300
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1720301
    new-instance v0, LX/AsD;

    invoke-direct {v0, p0}, LX/AsD;-><init>(LX/AsI;)V

    iput-object v0, p0, LX/AsI;->f:Landroid/view/ViewTreeObserver$OnDrawListener;

    .line 1720302
    new-instance v0, LX/AsE;

    invoke-direct {v0, p0}, LX/AsE;-><init>(LX/AsI;)V

    iput-object v0, p0, LX/AsI;->g:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 1720303
    new-instance v0, LX/AsG;

    invoke-direct {v0, p0}, LX/AsG;-><init>(LX/AsI;)V

    iput-object v0, p0, LX/AsI;->m:LX/AsF;

    .line 1720304
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/AsI;->a:Ljava/lang/ref/WeakReference;

    .line 1720305
    iget-object v0, p0, LX/AsI;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p4, p0, p1, v0}, LX/Asf;->a(LX/AsA;Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;Ljava/lang/Object;)LX/Ase;

    move-result-object v0

    iput-object v0, p0, LX/AsI;->b:LX/Ase;

    .line 1720306
    iput-object p3, p0, LX/AsI;->c:Landroid/view/ViewStub;

    .line 1720307
    iput-object p5, p0, LX/AsI;->d:Landroid/view/WindowManager;

    .line 1720308
    iput-object p6, p0, LX/AsI;->e:LX/0w3;

    .line 1720309
    invoke-static {p2}, LX/87N;->a(LX/0il;)LX/86o;

    move-result-object v0

    invoke-virtual {p0}, LX/AsI;->b()LX/86o;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 1720310
    invoke-direct {p0}, LX/AsI;->c()V

    .line 1720311
    :goto_0
    return-void

    .line 1720312
    :cond_0
    sget-object v0, LX/ArJ;->UNKNOWN:LX/ArJ;

    invoke-direct {p0, v0}, LX/AsI;->b(LX/ArJ;)V

    goto :goto_0
.end method

.method private b(LX/ArJ;)V
    .locals 2

    .prologue
    .line 1720313
    iget-object v0, p0, LX/AsI;->b:LX/Ase;

    invoke-virtual {v0}, LX/Ase;->c()V

    .line 1720314
    iget-object v0, p0, LX/AsI;->j:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1720315
    iget-object v0, p0, LX/AsI;->j:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iget-object v1, p0, LX/AsI;->f:Landroid/view/ViewTreeObserver$OnDrawListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->removeOnDrawListener(Landroid/view/ViewTreeObserver$OnDrawListener;)V

    .line 1720316
    iget-object v0, p0, LX/AsI;->j:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iget-object v1, p0, LX/AsI;->g:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 1720317
    iget-object v0, p0, LX/AsI;->j:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    .line 1720318
    :cond_0
    return-void
.end method

.method private c()V
    .locals 3

    .prologue
    .line 1720330
    iget-boolean v0, p0, LX/AsI;->h:Z

    if-nez v0, :cond_0

    .line 1720331
    iget-object v0, p0, LX/AsI;->b:LX/Ase;

    iget-object v1, p0, LX/AsI;->c:Landroid/view/ViewStub;

    const v2, 0x7f03093e

    invoke-virtual {v0, v1, v2}, LX/Ase;->a(Landroid/view/ViewStub;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/AsI;->j:Landroid/view/View;

    .line 1720332
    iget-object v0, p0, LX/AsI;->b:LX/Ase;

    .line 1720333
    iget-object v1, v0, LX/Ase;->h:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1720334
    iget-object v1, v0, LX/Ase;->g:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1720335
    iget-object v0, p0, LX/AsI;->j:Landroid/view/View;

    const v1, 0x7f0d17b8

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;

    iput-object v0, p0, LX/AsI;->k:Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;

    .line 1720336
    iget-object v0, p0, LX/AsI;->j:Landroid/view/View;

    const v1, 0x7f0d17b9

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;

    iput-object v0, p0, LX/AsI;->l:Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;

    .line 1720337
    iget-object v0, p0, LX/AsI;->k:Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;

    iget-object v1, p0, LX/AsI;->m:LX/AsF;

    .line 1720338
    iput-object v1, v0, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->d:LX/AsF;

    .line 1720339
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/AsI;->h:Z

    .line 1720340
    :cond_0
    iget-object v0, p0, LX/AsI;->j:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iget-object v1, p0, LX/AsI;->g:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 1720341
    invoke-direct {p0}, LX/AsI;->d()V

    .line 1720342
    invoke-static {p0}, LX/AsI;->f(LX/AsI;)V

    .line 1720343
    invoke-static {p0}, LX/AsI;->g$redex0(LX/AsI;)V

    .line 1720344
    invoke-static {p0}, LX/AsI;->e(LX/AsI;)LX/870;

    move-result-object v0

    sget-object v1, LX/870;->TEXT_EDITING:LX/870;

    if-eq v0, v1, :cond_1

    .line 1720345
    iget-object v0, p0, LX/AsI;->b:LX/Ase;

    invoke-virtual {v0}, LX/Ase;->b()V

    .line 1720346
    :cond_1
    return-void
.end method

.method private d()V
    .locals 4

    .prologue
    .line 1720319
    iget-object v0, p0, LX/AsI;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v0

    .line 1720320
    invoke-static {p0}, LX/AsI;->e(LX/AsI;)LX/870;

    move-result-object v1

    .line 1720321
    sget-object v2, LX/AsH;->a:[I

    invoke-virtual {v1}, LX/870;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 1720322
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unsupported format mode: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, LX/870;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1720323
    :pswitch_0
    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->getTextColorSelection()I

    move-result v0

    .line 1720324
    :goto_0
    if-ltz v0, :cond_0

    .line 1720325
    iget-object v0, p0, LX/AsI;->k:Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->getDefaultColor()I

    move-result v0

    .line 1720326
    :cond_0
    iget-object v1, p0, LX/AsI;->l:Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;

    iget-object v2, p0, LX/AsI;->k:Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;

    invoke-virtual {v2, v0}, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->a(I)F

    move-result v2

    invoke-virtual {v1, v0, v2}, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->a(IF)V

    .line 1720327
    iget-object v1, p0, LX/AsI;->k:Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;

    invoke-virtual {v1, v0}, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->setCurrentColor(I)V

    .line 1720328
    return-void

    .line 1720329
    :pswitch_1
    iget-object v0, p0, LX/AsI;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->k()Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;->getColorSelection()I

    move-result v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public static e(LX/AsI;)LX/870;
    .locals 1

    .prologue
    .line 1720290
    iget-object v0, p0, LX/AsI;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->getFormatMode()LX/870;

    move-result-object v0

    .line 1720291
    return-object v0
.end method

.method public static f(LX/AsI;)V
    .locals 4

    .prologue
    .line 1720292
    iget-object v0, p0, LX/AsI;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 1720293
    invoke-static {p0}, LX/AsI;->e(LX/AsI;)LX/870;

    move-result-object v1

    sget-object v2, LX/870;->DOODLE_EMPTY:LX/870;

    if-eq v1, v2, :cond_0

    invoke-static {p0}, LX/AsI;->e(LX/AsI;)LX/870;

    move-result-object v1

    sget-object v2, LX/870;->DOODLE_HAS_DRAWING:LX/870;

    if-ne v1, v2, :cond_1

    :cond_0
    move-object v1, v0

    .line 1720294
    check-cast v1, LX/0im;

    invoke-interface {v1}, LX/0im;->c()LX/0jJ;

    move-result-object v1

    const-class v2, LX/AsI;

    invoke-static {v2}, LX/0jK;->a(Ljava/lang/Class;)LX/0jK;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v1

    check-cast v1, LX/0jL;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->k()Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;)Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams$Builder;

    move-result-object v0

    iget-object v2, p0, LX/AsI;->l:Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;

    .line 1720295
    iget v3, v2, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->c:I

    move v2, v3

    .line 1720296
    invoke-virtual {v0, v2}, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams$Builder;->setColorSelection(I)Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams$Builder;

    move-result-object v0

    iget-object v2, p0, LX/AsI;->l:Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;

    .line 1720297
    iget v3, v2, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->p:F

    move v2, v3

    .line 1720298
    invoke-virtual {v0, v2}, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams$Builder;->setStrokeWidth(F)Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0jL;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParams;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jL;

    invoke-virtual {v0}, LX/0jL;->a()V

    .line 1720299
    :cond_1
    return-void
.end method

.method public static g$redex0(LX/AsI;)V
    .locals 3

    .prologue
    .line 1720273
    iget-object v0, p0, LX/AsI;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 1720274
    invoke-static {p0}, LX/AsI;->e(LX/AsI;)LX/870;

    move-result-object v1

    sget-object v2, LX/870;->TEXT_EDITING:LX/870;

    if-ne v1, v2, :cond_0

    move-object v1, v0

    .line 1720275
    check-cast v1, LX/0im;

    invoke-interface {v1}, LX/0im;->c()LX/0jJ;

    move-result-object v1

    const-class v2, LX/AsI;

    invoke-static {v2}, LX/0jK;->a(Ljava/lang/Class;)LX/0jK;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v1

    check-cast v1, LX/0jL;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationState;)Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;

    move-result-object v0

    iget-object v2, p0, LX/AsI;->l:Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;

    .line 1720276
    iget p0, v2, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationStrokeIndicator;->c:I

    move v2, p0

    .line 1720277
    invoke-virtual {v0, v2}, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->setTextColorSelection(I)Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0jL;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationState;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jL;

    invoke-virtual {v0}, LX/0jL;->a()V

    .line 1720278
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/5L2;)V
    .locals 0

    .prologue
    .line 1720272
    return-void
.end method

.method public final a(LX/ArJ;)V
    .locals 0

    .prologue
    .line 1720289
    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1720279
    check-cast p1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    .line 1720280
    iget-object v0, p0, LX/AsI;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->isBottomTrayTransitioning()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, LX/AsI;->e(LX/AsI;)LX/870;

    move-result-object v0

    sget-object v1, LX/870;->TEXT_EDITING:LX/870;

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, LX/AsI;->i:Z

    if-eqz v0, :cond_0

    .line 1720281
    iget-object v0, p0, LX/AsI;->k:Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/editgallery/InspirationColorPicker;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iget-object v1, p0, LX/AsI;->f:Landroid/view/ViewTreeObserver$OnDrawListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnDrawListener(Landroid/view/ViewTreeObserver$OnDrawListener;)V

    .line 1720282
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/AsI;->i:Z

    :cond_0
    move-object v0, p1

    .line 1720283
    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v1

    iget-object v0, p0, LX/AsI;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v0

    invoke-virtual {p0}, LX/AsI;->b()LX/86o;

    move-result-object v2

    invoke-static {v1, v0, v2}, LX/87N;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationState;Lcom/facebook/friendsharing/inspiration/model/InspirationState;LX/86o;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1720284
    invoke-direct {p0}, LX/AsI;->c()V

    .line 1720285
    :cond_1
    :goto_0
    return-void

    .line 1720286
    :cond_2
    check-cast p1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {p1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v1

    iget-object v0, p0, LX/AsI;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v0

    invoke-virtual {p0}, LX/AsI;->b()LX/86o;

    move-result-object v2

    invoke-static {v1, v0, v2}, LX/87N;->b(Lcom/facebook/friendsharing/inspiration/model/InspirationState;Lcom/facebook/friendsharing/inspiration/model/InspirationState;LX/86o;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1720287
    sget-object v0, LX/ArJ;->UNKNOWN:LX/ArJ;

    invoke-direct {p0, v0}, LX/AsI;->b(LX/ArJ;)V

    goto :goto_0
.end method

.method public final b()LX/86o;
    .locals 1

    .prologue
    .line 1720288
    sget-object v0, LX/86o;->COLOR_PICKER:LX/86o;

    return-object v0
.end method
