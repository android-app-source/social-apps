.class public LX/BXz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8RE;


# instance fields
.field public a:Z


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1794012
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1794013
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/BXz;->a:Z

    .line 1794014
    return-void
.end method


# virtual methods
.method public final a()LX/03R;
    .locals 1

    .prologue
    .line 1794015
    sget-object v0, LX/03R;->UNSET:LX/03R;

    return-object v0
.end method

.method public final a(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 1794019
    new-instance v0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteItemRow;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/widget/singleclickinvite/SingleClickInviteItemRow;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public final a(Landroid/view/View;LX/621;)V
    .locals 1

    .prologue
    .line 1794016
    iget-boolean v0, p0, LX/BXz;->a:Z

    if-eqz v0, :cond_0

    .line 1794017
    check-cast p1, Lcom/facebook/fig/sectionheader/FigSectionHeader;

    invoke-interface {p2}, LX/621;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/fig/sectionheader/FigSectionHeader;->setTitleText(Ljava/lang/CharSequence;)V

    .line 1794018
    :cond_0
    return-void
.end method

.method public final a(Landroid/view/View;LX/8QL;Z)V
    .locals 0

    .prologue
    .line 1794008
    check-cast p1, Lcom/facebook/widget/singleclickinvite/SingleClickInviteItemRow;

    invoke-virtual {p1, p2}, Lcom/facebook/widget/singleclickinvite/SingleClickInviteItemRow;->a(LX/8QL;)V

    .line 1794009
    return-void
.end method

.method public final a(Landroid/view/View;Landroid/view/View$OnClickListener;)V
    .locals 0

    .prologue
    .line 1794010
    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1794011
    return-void
.end method

.method public final b(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 1794007
    new-instance v0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteItemRow;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/widget/singleclickinvite/SingleClickInviteItemRow;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public final b(Landroid/view/View;LX/8QL;Z)V
    .locals 0

    .prologue
    .line 1794005
    check-cast p1, Lcom/facebook/widget/singleclickinvite/SingleClickInviteItemRow;

    invoke-virtual {p1, p2}, Lcom/facebook/widget/singleclickinvite/SingleClickInviteItemRow;->a(LX/8QL;)V

    .line 1794006
    return-void
.end method

.method public final b(Landroid/view/View;Landroid/view/View$OnClickListener;)V
    .locals 0

    .prologue
    .line 1794002
    check-cast p1, Lcom/facebook/widget/singleclickinvite/SingleClickInviteItemRow;

    .line 1794003
    iget-object p0, p1, Lcom/facebook/widget/singleclickinvite/SingleClickInviteItemRow;->e:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {p0, p2}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1794004
    return-void
.end method

.method public final c(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 1794001
    new-instance v0, Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public final d(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 1793998
    iget-boolean v0, p0, LX/BXz;->a:Z

    if-eqz v0, :cond_0

    .line 1793999
    new-instance v0, Lcom/facebook/fig/sectionheader/FigSectionHeader;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/fig/sectionheader/FigSectionHeader;-><init>(Landroid/content/Context;)V

    .line 1794000
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public final e(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 1793997
    new-instance v0, Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    return-object v0
.end method
