.class public LX/B3j;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:LX/2nf;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Z


# direct methods
.method public constructor <init>(Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;)V
    .locals 1
    .param p1    # Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1740811
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1740812
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/B3j;->c:Z

    .line 1740813
    iput-object p1, p0, LX/B3j;->a:Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;

    .line 1740814
    return-void
.end method

.method public static b(LX/B3j;)Z
    .locals 1

    .prologue
    .line 1740815
    iget-object v0, p0, LX/B3j;->a:Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/B3j;->c:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(LX/B3j;)V
    .locals 2

    .prologue
    .line 1740816
    iget-object v0, p0, LX/B3j;->b:LX/2nf;

    const-string v1, "If mCursor is null, then getItemCount() is 0. moveToPosition is invalid for any position"

    invoke-static {v0, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1740817
    return-void
.end method

.method public static c(LX/B3j;I)V
    .locals 1

    .prologue
    .line 1740818
    invoke-static {p0}, LX/B3j;->b(LX/B3j;)Z

    move-result v0

    if-eqz v0, :cond_0

    add-int/lit8 p1, p1, -0x1

    .line 1740819
    :cond_0
    invoke-static {p0}, LX/B3j;->c(LX/B3j;)V

    .line 1740820
    if-ltz p1, :cond_1

    iget-object v0, p0, LX/B3j;->b:LX/2nf;

    invoke-interface {v0}, LX/2nf;->getCount()I

    move-result v0

    if-ge p1, v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1740821
    iget-object v0, p0, LX/B3j;->b:LX/2nf;

    invoke-interface {v0, p1}, LX/2nf;->moveToPosition(I)Z

    .line 1740822
    return-void

    .line 1740823
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final b(I)I
    .locals 1

    .prologue
    .line 1740824
    invoke-static {p0}, LX/B3j;->c(LX/B3j;)V

    .line 1740825
    invoke-static {p0}, LX/B3j;->b(LX/B3j;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    .line 1740826
    const/4 v0, 0x3

    .line 1740827
    :goto_0
    return v0

    .line 1740828
    :cond_0
    invoke-static {p0, p1}, LX/B3j;->c(LX/B3j;I)V

    .line 1740829
    iget-object v0, p0, LX/B3j;->b:LX/2nf;

    invoke-interface {v0}, LX/2nf;->d()I

    move-result v0

    goto :goto_0
.end method
