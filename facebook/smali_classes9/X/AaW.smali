.class public final LX/AaW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/facecast/protocol/FetchLiveVideoTipJarDataQueryModels$FetchLiveVideoTipJarDataQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/AXW;

.field public final synthetic b:LX/AaX;


# direct methods
.method public constructor <init>(LX/AaX;LX/AXW;)V
    .locals 0

    .prologue
    .line 1688654
    iput-object p1, p0, LX/AaW;->b:LX/AaX;

    iput-object p2, p0, LX/AaW;->a:LX/AXW;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1688655
    iget-object v0, p0, LX/AaW;->b:LX/AaX;

    iget-object v0, v0, LX/AaX;->d:LX/03V;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, LX/AaX;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_graphFailure"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Failed to fetch video tip jar data, like tip amount and tip giver number"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1688656
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1688657
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1688658
    iget-object v0, p0, LX/AaW;->a:LX/AXW;

    if-nez v0, :cond_1

    .line 1688659
    :cond_0
    :goto_0
    return-void

    .line 1688660
    :cond_1
    if-eqz p1, :cond_2

    .line 1688661
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1688662
    if-nez v0, :cond_3

    .line 1688663
    :cond_2
    new-instance v0, Ljava/lang/Throwable;

    const-string v1, "Feteched null result"

    invoke-direct {v0, v1}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, LX/AaW;->onFailure(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1688664
    :cond_3
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1688665
    check-cast v0, Lcom/facebook/facecast/protocol/FetchLiveVideoTipJarDataQueryModels$FetchLiveVideoTipJarDataQueryModel;

    .line 1688666
    invoke-virtual {v0}, Lcom/facebook/facecast/protocol/FetchLiveVideoTipJarDataQueryModels$FetchLiveVideoTipJarDataQueryModel;->j()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const/4 v3, 0x2

    invoke-virtual {v2, v1, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    .line 1688667
    invoke-virtual {v0}, Lcom/facebook/facecast/protocol/FetchLiveVideoTipJarDataQueryModels$FetchLiveVideoTipJarDataQueryModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v3}, LX/15i;->j(II)I

    move-result v0

    .line 1688668
    iget-object v2, p0, LX/AaW;->a:LX/AXW;

    if-eqz v2, :cond_0

    .line 1688669
    iget-object v2, p0, LX/AaW;->a:LX/AXW;

    invoke-interface {v2, v1, v0}, LX/AXW;->a(Ljava/lang/String;I)V

    goto :goto_0
.end method
