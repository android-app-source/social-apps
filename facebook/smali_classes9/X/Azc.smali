.class public LX/Azc;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:LX/0Zb;

.field private final c:Landroid/content/Context;

.field private final d:LX/7yc;

.field private final e:LX/BV0;

.field private final f:I

.field private final g:I

.field private final h:LX/0So;

.field public i:Z

.field public j:Z

.field public k:LX/ATs;

.field public l:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1732798
    const-class v0, LX/Azc;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Azc;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Zb;Landroid/content/Context;LX/7yc;LX/BV0;LX/0So;IILX/ATs;)V
    .locals 2
    .param p6    # I
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # I
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p8    # LX/ATs;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1732785
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1732786
    iput-boolean v0, p0, LX/Azc;->i:Z

    .line 1732787
    iput-boolean v0, p0, LX/Azc;->j:Z

    .line 1732788
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/Azc;->l:J

    .line 1732789
    iput-object p1, p0, LX/Azc;->b:LX/0Zb;

    .line 1732790
    iput-object p2, p0, LX/Azc;->c:Landroid/content/Context;

    .line 1732791
    iput-object p3, p0, LX/Azc;->d:LX/7yc;

    .line 1732792
    iput-object p4, p0, LX/Azc;->e:LX/BV0;

    .line 1732793
    iput-object p5, p0, LX/Azc;->h:LX/0So;

    .line 1732794
    iput p6, p0, LX/Azc;->f:I

    .line 1732795
    iput p7, p0, LX/Azc;->g:I

    .line 1732796
    iput-object p8, p0, LX/Azc;->k:LX/ATs;

    .line 1732797
    return-void
.end method

.method private static a(LX/BUz;IF)LX/1FJ;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/BUz;",
            "IF)",
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1732778
    :try_start_0
    invoke-virtual {p0, p1, p2}, LX/BUz;->a(IF)LX/1FJ;

    move-result-object v0

    .line 1732779
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/1FJ;->a()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1732780
    :goto_0
    return-object v0

    .line 1732781
    :catch_0
    move-exception v0

    .line 1732782
    sget-object v2, LX/Azc;->a:Ljava/lang/String;

    const-string v3, "Unable to extract frame"

    invoke-static {v2, v3, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v1

    .line 1732783
    goto :goto_0

    :cond_0
    move-object v0, v1

    .line 1732784
    goto :goto_0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;II)Lcom/facebook/ipc/media/data/MediaData;
    .locals 2

    .prologue
    .line 1732799
    new-instance v0, LX/4gP;

    invoke-direct {v0}, LX/4gP;-><init>()V

    invoke-virtual {v0, p0}, LX/4gP;->a(Ljava/lang/String;)LX/4gP;

    move-result-object v0

    sget-object v1, LX/4gQ;->Photo:LX/4gQ;

    invoke-virtual {v0, v1}, LX/4gP;->a(LX/4gQ;)LX/4gP;

    move-result-object v0

    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4gP;->a(Landroid/net/Uri;)LX/4gP;

    move-result-object v0

    sget-object v1, Lcom/facebook/ipc/media/data/MimeType;->a:Lcom/facebook/ipc/media/data/MimeType;

    invoke-virtual {v0, v1}, LX/4gP;->a(Lcom/facebook/ipc/media/data/MimeType;)LX/4gP;

    move-result-object v0

    .line 1732800
    iput p2, v0, LX/4gP;->f:I

    .line 1732801
    move-object v0, v0

    .line 1732802
    iput p3, v0, LX/4gP;->g:I

    .line 1732803
    move-object v0, v0

    .line 1732804
    invoke-virtual {v0}, LX/4gP;->a()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v0

    return-object v0
.end method

.method private static a(LX/Azc;JDZJJJJJJJ)V
    .locals 10

    .prologue
    .line 1732771
    move-wide/from16 v0, p8

    long-to-double v2, v0

    div-double v2, p3, v2

    const-wide v4, 0x408f400000000000L    # 1000.0

    mul-double/2addr v2, v4

    .line 1732772
    move-wide/from16 v0, p6

    long-to-double v4, v0

    div-double v4, p3, v4

    const-wide v6, 0x408f400000000000L    # 1000.0

    mul-double/2addr v4, v6

    .line 1732773
    sget-object v6, Ljava/util/Locale;->UK:Ljava/util/Locale;

    invoke-static {v6}, Ljava/text/NumberFormat;->getInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v6

    .line 1732774
    const/4 v7, 0x2

    invoke-virtual {v6, v7}, Ljava/text/NumberFormat;->setMaximumFractionDigits(I)V

    .line 1732775
    new-instance v7, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v8, "video_tagging_result"

    invoke-direct {v7, v8}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v8, "duration_ms"

    move-wide/from16 v0, p10

    invoke-virtual {v7, v8, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string v8, "dfps"

    invoke-virtual {v6, v2, v3}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v8, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "efps"

    invoke-virtual {v6, v4, v5}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "face_found"

    invoke-virtual {v2, v3, p5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "face_frame_position_ms"

    invoke-virtual {v2, v3, p1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "ttf_ms"

    move-wide/from16 v0, p12

    invoke-virtual {v2, v3, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "frame_extraction_start_ms"

    move-wide/from16 v0, p14

    invoke-virtual {v2, v3, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "frame_extraction_end_ms"

    move-wide/from16 v0, p16

    invoke-virtual {v2, v3, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "video_length_ms"

    move-wide/from16 v0, p18

    invoke-virtual {v2, v3, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    sget-object v3, LX/Azc;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->f(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    .line 1732776
    iget-object v3, p0, LX/Azc;->b:LX/0Zb;

    invoke-interface {v3, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1732777
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    .line 1732764
    iget-object v0, p0, LX/Azc;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Azc;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1732765
    :cond_0
    return-void

    .line 1732766
    :cond_1
    iget-object v0, p0, LX/Azc;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 1732767
    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    const-string v5, "facedetection_"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1732768
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    move-result v3

    if-nez v3, :cond_2

    .line 1732769
    sget-object v3, LX/Azc;->a:Ljava/lang/String;

    const-string v4, "Couldn\'t delete video frame file"

    invoke-static {v3, v4}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1732770
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public final a(Landroid/net/Uri;Ljava/lang/String;FJJJ)V
    .locals 24

    .prologue
    .line 1732727
    move-object/from16 v0, p0

    iget-object v4, v0, LX/Azc;->e:LX/BV0;

    const/4 v5, 0x0

    const/4 v6, 0x0

    sget-object v7, LX/7Sv;->NONE:LX/7Sv;

    move-object/from16 v0, p1

    invoke-virtual {v4, v0, v5, v6, v7}, LX/BV0;->a(Landroid/net/Uri;Lcom/facebook/timeline/profilevideo/ProfileVideoEditFragment;Ljava/util/List;LX/7Sv;)LX/BUz;

    move-result-object v15

    .line 1732728
    const-wide/16 v6, -0x1

    .line 1732729
    const/4 v4, 0x0

    .line 1732730
    const-wide/16 v10, 0x0

    .line 1732731
    const-wide/16 v12, 0x0

    .line 1732732
    move-object/from16 v0, p0

    iget-object v5, v0, LX/Azc;->h:LX/0So;

    invoke-interface {v5}, LX/0So;->now()J

    move-result-wide v16

    .line 1732733
    const/4 v5, 0x0

    move-wide/from16 v8, p4

    .line 1732734
    :goto_0
    cmp-long v14, v8, p6

    if-gez v14, :cond_4

    .line 1732735
    add-int/lit8 v14, v4, 0x1

    .line 1732736
    move-object/from16 v0, p0

    iget-object v4, v0, LX/Azc;->h:LX/0So;

    invoke-interface {v4}, LX/0So;->now()J

    move-result-wide v18

    .line 1732737
    long-to-int v4, v8

    move/from16 v0, p3

    invoke-static {v15, v4, v0}, LX/Azc;->a(LX/BUz;IF)LX/1FJ;

    move-result-object v20

    .line 1732738
    move-object/from16 v0, p0

    iget-object v4, v0, LX/Azc;->h:LX/0So;

    invoke-interface {v4}, LX/0So;->now()J

    move-result-wide v22

    sub-long v18, v22, v18

    add-long v10, v10, v18

    .line 1732739
    move-object/from16 v0, p0

    iget-object v4, v0, LX/Azc;->h:LX/0So;

    invoke-interface {v4}, LX/0So;->now()J

    move-result-wide v18

    .line 1732740
    move-object/from16 v0, p0

    iget-object v0, v0, LX/Azc;->d:LX/7yc;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v20}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/Bitmap;

    const/16 v22, 0x0

    const/16 v23, 0x0

    move-object/from16 v0, v21

    move/from16 v1, v22

    move/from16 v2, v23

    invoke-virtual {v0, v4, v1, v2}, LX/7yc;->a(Landroid/graphics/Bitmap;IZ)Ljava/util/List;

    move-result-object v4

    .line 1732741
    move-object/from16 v0, p0

    iget-object v0, v0, LX/Azc;->h:LX/0So;

    move-object/from16 v21, v0

    invoke-interface/range {v21 .. v21}, LX/0So;->now()J

    move-result-wide v22

    sub-long v18, v22, v18

    add-long v12, v12, v18

    .line 1732742
    invoke-virtual/range {v20 .. v20}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v18

    if-eqz v18, :cond_2

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_2

    .line 1732743
    add-int/lit8 v5, v5, 0x1

    .line 1732744
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v18, "facedetection_"

    move-object/from16 v0, v18

    invoke-direct {v4, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p2

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v18, "_"

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v18, ".jpg"

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    .line 1732745
    const-wide/16 v22, -0x1

    cmp-long v4, v6, v22

    if-nez v4, :cond_1

    .line 1732746
    move-object/from16 v0, p0

    iget-object v4, v0, LX/Azc;->h:LX/0So;

    invoke-interface {v4}, LX/0So;->now()J

    move-result-wide v6

    sub-long v6, v6, v16

    move-object/from16 v0, p0

    iput-wide v6, v0, LX/Azc;->l:J

    .line 1732747
    move-object/from16 v0, p0

    iget-object v4, v0, LX/Azc;->k:LX/ATs;

    if-eqz v4, :cond_0

    .line 1732748
    move-object/from16 v0, p0

    iget-object v4, v0, LX/Azc;->k:LX/ATs;

    move-object/from16 v0, p0

    iget-wide v6, v0, LX/Azc;->l:J

    invoke-virtual {v4, v6, v7}, LX/ATs;->a(J)V

    :cond_0
    move-wide v6, v8

    .line 1732749
    :cond_1
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, LX/Azc;->j:Z

    .line 1732750
    :try_start_0
    move-object/from16 v0, p0

    iget-object v4, v0, LX/Azc;->c:Landroid/content/Context;

    const/16 v19, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v4, v0, v1}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v19

    .line 1732751
    invoke-virtual/range {v20 .. v20}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/Bitmap;

    sget-object v21, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    move-object/from16 v0, p0

    iget v0, v0, LX/Azc;->g:I

    move/from16 v22, v0

    move-object/from16 v0, v21

    move/from16 v1, v22

    move-object/from16 v2, v19

    invoke-virtual {v4, v0, v1, v2}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 1732752
    invoke-virtual/range {v19 .. v19}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1732753
    :goto_1
    move-object/from16 v0, p0

    iget-object v4, v0, LX/Azc;->k:LX/ATs;

    if-eqz v4, :cond_2

    .line 1732754
    move-object/from16 v0, p0

    iget-object v0, v0, LX/Azc;->k:LX/ATs;

    move-object/from16 v19, v0

    invoke-static {}, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame;->newBuilder()Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame$Builder;

    move-result-object v4

    sub-long v22, v8, p4

    move-wide/from16 v0, v22

    invoke-virtual {v4, v0, v1}, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame$Builder;->setTimestamp(J)Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame$Builder;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v4, v0, LX/Azc;->c:Landroid/content/Context;

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Landroid/content/Context;->getFileStreamPath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v20 .. v20}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v23

    invoke-virtual/range {v20 .. v20}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    move-object/from16 v0, v18

    move-object/from16 v1, v22

    move/from16 v2, v23

    invoke-static {v0, v1, v2, v4}, LX/Azc;->a(Ljava/lang/String;Ljava/lang/String;II)Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v4

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame$Builder;->setMediaData(Lcom/facebook/ipc/media/data/MediaData;)Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame$Builder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame$Builder;->a()Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, LX/ATs;->a(Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame;)V

    :cond_2
    move v4, v5

    .line 1732755
    invoke-virtual/range {v20 .. v20}, LX/1FJ;->close()V

    .line 1732756
    const/4 v5, 0x5

    if-eq v4, v5, :cond_3

    .line 1732757
    move-object/from16 v0, p0

    iget v5, v0, LX/Azc;->f:I

    int-to-long v0, v5

    move-wide/from16 v18, v0

    add-long v8, v8, v18

    move v5, v4

    move v4, v14

    goto/16 :goto_0

    .line 1732758
    :catch_0
    move-exception v4

    .line 1732759
    sget-object v19, LX/Azc;->a:Ljava/lang/String;

    const-string v21, "Couldn\'t save bitmap to file"

    move-object/from16 v0, v19

    move-object/from16 v1, v21

    invoke-static {v0, v1, v4}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_3
    move v4, v14

    .line 1732760
    :cond_4
    invoke-virtual {v15}, LX/BUz;->a()V

    .line 1732761
    sub-long v5, v6, p4

    int-to-double v7, v4

    move-object/from16 v0, p0

    iget-boolean v9, v0, LX/Azc;->j:Z

    move-object/from16 v0, p0

    iget-object v4, v0, LX/Azc;->h:LX/0So;

    invoke-interface {v4}, LX/0So;->now()J

    move-result-wide v14

    sub-long v14, v14, v16

    move-object/from16 v0, p0

    iget-wide v0, v0, LX/Azc;->l:J

    move-wide/from16 v16, v0

    move-object/from16 v4, p0

    move-wide/from16 v18, p4

    move-wide/from16 v20, p6

    move-wide/from16 v22, p8

    invoke-static/range {v4 .. v23}, LX/Azc;->a(LX/Azc;JDZJJJJJJJ)V

    .line 1732762
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, LX/Azc;->i:Z

    .line 1732763
    return-void
.end method
