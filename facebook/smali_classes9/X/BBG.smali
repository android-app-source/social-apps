.class public final LX/BBG;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/BBA;

.field private final b:LX/BB5;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1756824
    new-instance v0, LX/BBA;

    invoke-direct {v0}, LX/BBA;-><init>()V

    invoke-direct {p0, v0}, LX/BBG;-><init>(LX/BBA;)V

    .line 1756825
    return-void
.end method

.method public constructor <init>(LX/BB5;)V
    .locals 1

    .prologue
    .line 1756819
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1756820
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1756821
    const/4 v0, 0x0

    iput-object v0, p0, LX/BBG;->a:LX/BBA;

    .line 1756822
    iput-object p1, p0, LX/BBG;->b:LX/BB5;

    .line 1756823
    return-void
.end method

.method public constructor <init>(LX/BBA;)V
    .locals 1

    .prologue
    .line 1756814
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1756815
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1756816
    iput-object p1, p0, LX/BBG;->a:LX/BBA;

    .line 1756817
    const/4 v0, 0x0

    iput-object v0, p0, LX/BBG;->b:LX/BB5;

    .line 1756818
    return-void
.end method

.method private b()Z
    .locals 1

    .prologue
    .line 1756813
    iget-object v0, p0, LX/BBG;->b:LX/BB5;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/0Px;)LX/BBG;
    .locals 4
    .param p1    # LX/0Px;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;",
            ">;)",
            "LX/BBG;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x0

    .line 1756790
    invoke-direct {p0}, LX/BBG;->b()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1756791
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 1756792
    if-nez p1, :cond_0

    .line 1756793
    iget-object v0, p0, LX/BBG;->b:LX/BB5;

    .line 1756794
    iput-object v3, v0, LX/BB5;->a:LX/0Px;

    .line 1756795
    :goto_0
    return-object p0

    .line 1756796
    :cond_0
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_1

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;

    .line 1756797
    iget-object v0, v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;->b:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NotifOptionSetsModel$EdgesModel;

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1756798
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1756799
    :cond_1
    iget-object v0, p0, LX/BBG;->b:LX/BB5;

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 1756800
    iput-object v1, v0, LX/BB5;->a:LX/0Px;

    .line 1756801
    goto :goto_0

    .line 1756802
    :cond_2
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 1756803
    if-nez p1, :cond_3

    .line 1756804
    iget-object v0, p0, LX/BBG;->b:LX/BB5;

    .line 1756805
    iput-object v3, v0, LX/BB5;->a:LX/0Px;

    .line 1756806
    goto :goto_0

    .line 1756807
    :cond_3
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    move v1, v0

    :goto_2
    if-ge v1, v3, :cond_4

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;

    .line 1756808
    iget-object v0, v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;->a:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1756809
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 1756810
    :cond_4
    iget-object v0, p0, LX/BBG;->a:LX/BBA;

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 1756811
    iput-object v1, v0, LX/BBA;->a:LX/0Px;

    .line 1756812
    goto :goto_0
.end method

.method public final a()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel;
    .locals 8

    .prologue
    .line 1756777
    invoke-direct {p0}, LX/BBG;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel;

    iget-object v1, p0, LX/BBG;->b:LX/BB5;

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 1756778
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1756779
    iget-object v3, v1, LX/BB5;->a:LX/0Px;

    invoke-static {v2, v3}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v3

    .line 1756780
    invoke-virtual {v2, v6}, LX/186;->c(I)V

    .line 1756781
    invoke-virtual {v2, v5, v3}, LX/186;->b(II)V

    .line 1756782
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1756783
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1756784
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1756785
    invoke-virtual {v3, v5}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1756786
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1756787
    new-instance v3, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NotifOptionSetsModel;

    invoke-direct {v3, v2}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NotifOptionSetsModel;-><init>(LX/15i;)V

    .line 1756788
    move-object v1, v3

    .line 1756789
    invoke-direct {v0, v1}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel;-><init>(Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NotifOptionSetsModel;)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel;

    iget-object v1, p0, LX/BBG;->a:LX/BBA;

    invoke-virtual {v1}, LX/BBA;->a()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel$NotifOptionSetsModel;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel;-><init>(Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel$NotifOptionSetsModel;)V

    goto :goto_0
.end method
