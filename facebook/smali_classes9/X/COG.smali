.class public final LX/COG;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/COH;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/CNb;

.field public b:LX/CNr;

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/CNb;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1882805
    invoke-static {}, LX/COH;->q()LX/COH;

    move-result-object v0

    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1882806
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1882807
    const-string v0, "NTFBFeedStoryChevronButtonComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1882808
    if-ne p0, p1, :cond_1

    .line 1882809
    :cond_0
    :goto_0
    return v0

    .line 1882810
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1882811
    goto :goto_0

    .line 1882812
    :cond_3
    check-cast p1, LX/COG;

    .line 1882813
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1882814
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1882815
    if-eq v2, v3, :cond_0

    .line 1882816
    iget-object v2, p0, LX/COG;->a:LX/CNb;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/COG;->a:LX/CNb;

    iget-object v3, p1, LX/COG;->a:LX/CNb;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1882817
    goto :goto_0

    .line 1882818
    :cond_5
    iget-object v2, p1, LX/COG;->a:LX/CNb;

    if-nez v2, :cond_4

    .line 1882819
    :cond_6
    iget-object v2, p0, LX/COG;->b:LX/CNr;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/COG;->b:LX/CNr;

    iget-object v3, p1, LX/COG;->b:LX/CNr;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1882820
    goto :goto_0

    .line 1882821
    :cond_8
    iget-object v2, p1, LX/COG;->b:LX/CNr;

    if-nez v2, :cond_7

    .line 1882822
    :cond_9
    iget-object v2, p0, LX/COG;->c:Ljava/util/List;

    if-eqz v2, :cond_a

    iget-object v2, p0, LX/COG;->c:Ljava/util/List;

    iget-object v3, p1, LX/COG;->c:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1882823
    goto :goto_0

    .line 1882824
    :cond_a
    iget-object v2, p1, LX/COG;->c:Ljava/util/List;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
