.class public abstract LX/BPy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/BP9;


# instance fields
.field private final a:LX/5SB;

.field private b:Z

.field public c:I

.field public d:I

.field public e:LX/BPx;

.field public f:LX/0ta;

.field public g:I


# direct methods
.method public constructor <init>(LX/5SB;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1781114
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1781115
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/BPy;->b:Z

    .line 1781116
    sget-object v0, LX/BPx;->UNINITIALIZED:LX/BPx;

    iput-object v0, p0, LX/BPy;->e:LX/BPx;

    .line 1781117
    sget-object v0, LX/0ta;->NO_DATA:LX/0ta;

    iput-object v0, p0, LX/BPy;->f:LX/0ta;

    .line 1781118
    iput v1, p0, LX/BPy;->g:I

    .line 1781119
    iput-object p1, p0, LX/BPy;->a:LX/5SB;

    .line 1781120
    iput v1, p0, LX/BPy;->c:I

    .line 1781121
    iput v1, p0, LX/BPy;->d:I

    .line 1781122
    return-void
.end method


# virtual methods
.method public final c()V
    .locals 0

    .prologue
    .line 1781133
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 1781128
    sget-object v0, LX/BPx;->FINAL_DATA:LX/BPx;

    iput-object v0, p0, LX/BPy;->e:LX/BPx;

    .line 1781129
    const/4 v0, 0x0

    .line 1781130
    iput v0, p0, LX/BPy;->g:I

    .line 1781131
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/BPy;->b:Z

    .line 1781132
    return-void
.end method

.method public final j()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 1781127
    iget-object v1, p0, LX/BPy;->e:LX/BPx;

    sget-object v2, LX/BPx;->FINAL_DATA:LX/BPx;

    if-eq v1, v2, :cond_0

    iget v1, p0, LX/BPy;->g:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final l()V
    .locals 1

    .prologue
    .line 1781134
    iget v0, p0, LX/BPy;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/BPy;->c:I

    .line 1781135
    return-void
.end method

.method public final m()V
    .locals 1

    .prologue
    .line 1781125
    iget v0, p0, LX/BPy;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/BPy;->d:I

    .line 1781126
    return-void
.end method

.method public final n()Z
    .locals 1

    .prologue
    .line 1781124
    iget-boolean v0, p0, LX/BPy;->b:Z

    return v0
.end method

.method public p()V
    .locals 0

    .prologue
    .line 1781123
    return-void
.end method
