.class public LX/Bdv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/BcX;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "LX/5Je;",
        ":",
        "LX/BcX;",
        ">",
        "Ljava/lang/Object;",
        "LX/BcX;"
    }
.end annotation


# instance fields
.field private a:LX/Bdb;

.field private b:LX/5Je;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1804365
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()LX/5Je;
    .locals 1

    .prologue
    .line 1804366
    iget-object v0, p0, LX/Bdv;->b:LX/5Je;

    return-object v0
.end method

.method public final a(ILX/1X1;Z)V
    .locals 2

    .prologue
    .line 1804367
    iget-object v0, p0, LX/Bdv;->b:LX/5Je;

    if-nez v0, :cond_0

    .line 1804368
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Wrapping Target delegate not initialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1804369
    :cond_0
    iget-object v0, p0, LX/Bdv;->b:LX/5Je;

    check-cast v0, LX/BcX;

    invoke-interface {v0, p1, p2, p3}, LX/BcX;->a(ILX/1X1;Z)V

    .line 1804370
    return-void
.end method

.method public final a(LX/Bdb;LX/1De;)V
    .locals 2

    .prologue
    .line 1804371
    iget-object v0, p0, LX/Bdv;->a:LX/Bdb;

    if-eq v0, p1, :cond_1

    .line 1804372
    iget-object v0, p0, LX/Bdv;->a:LX/Bdb;

    if-eqz v0, :cond_0

    .line 1804373
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Changing the layout of a RecyclerCollectionComponent is not supported"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1804374
    :cond_0
    iput-object p1, p0, LX/Bdv;->a:LX/Bdb;

    .line 1804375
    invoke-interface {p1, p2}, LX/Bdb;->a(LX/1De;)LX/5Je;

    move-result-object v0

    iput-object v0, p0, LX/Bdv;->b:LX/5Je;

    .line 1804376
    :cond_1
    return-void
.end method

.method public final b(I)V
    .locals 2

    .prologue
    .line 1804377
    iget-object v0, p0, LX/Bdv;->b:LX/5Je;

    if-nez v0, :cond_0

    .line 1804378
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Wrapping Target delegate not initialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1804379
    :cond_0
    iget-object v0, p0, LX/Bdv;->b:LX/5Je;

    check-cast v0, LX/BcX;

    invoke-interface {v0, p1}, LX/BcX;->b(I)V

    .line 1804380
    return-void
.end method

.method public final b(ILX/1X1;Z)V
    .locals 2

    .prologue
    .line 1804381
    iget-object v0, p0, LX/Bdv;->b:LX/5Je;

    if-nez v0, :cond_0

    .line 1804382
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Wrapping Target delegate not initialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1804383
    :cond_0
    iget-object v0, p0, LX/Bdv;->b:LX/5Je;

    check-cast v0, LX/BcX;

    invoke-interface {v0, p1, p2, p3}, LX/BcX;->b(ILX/1X1;Z)V

    .line 1804384
    return-void
.end method
