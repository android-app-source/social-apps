.class public LX/CUX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6F4;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6F4",
        "<",
        "Lcom/facebook/payments/confirmation/SimpleConfirmationData;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/6ul;

.field public final c:LX/17Y;

.field public d:LX/6qh;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/6ul;LX/17Y;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1896611
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1896612
    iput-object p1, p0, LX/CUX;->a:Landroid/content/Context;

    .line 1896613
    iput-object p2, p0, LX/CUX;->b:LX/6ul;

    .line 1896614
    iput-object p3, p0, LX/CUX;->c:LX/17Y;

    .line 1896615
    return-void
.end method


# virtual methods
.method public final a(LX/6qh;)V
    .locals 1

    .prologue
    .line 1896600
    iput-object p1, p0, LX/CUX;->d:LX/6qh;

    .line 1896601
    iget-object v0, p0, LX/CUX;->b:LX/6ul;

    invoke-virtual {v0, p1}, LX/6ul;->a(LX/6qh;)V

    .line 1896602
    return-void
.end method

.method public final bridge synthetic a(Lcom/facebook/payments/confirmation/ConfirmationData;)V
    .locals 0

    .prologue
    .line 1896610
    return-void
.end method

.method public bridge synthetic onClick(Lcom/facebook/payments/confirmation/ConfirmationData;LX/6uH;)V
    .locals 3

    .prologue
    .line 1896603
    check-cast p1, Lcom/facebook/payments/confirmation/SimpleConfirmationData;

    .line 1896604
    sget-object v0, LX/CUW;->a:[I

    invoke-interface {p2}, LX/6uG;->d()LX/6uT;

    move-result-object v1

    invoke-virtual {v1}, LX/6uT;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1896605
    iget-object v0, p0, LX/CUX;->b:LX/6ul;

    invoke-virtual {v0, p1, p2}, LX/6ul;->onClick(Lcom/facebook/payments/confirmation/SimpleConfirmationData;LX/6uH;)V

    .line 1896606
    :goto_0
    return-void

    .line 1896607
    :pswitch_0
    invoke-virtual {p1}, Lcom/facebook/payments/confirmation/SimpleConfirmationData;->a()Lcom/facebook/payments/confirmation/ConfirmationParams;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/payments/InstantWorkflowsConfirmationParams;

    .line 1896608
    iget-object v1, p0, LX/CUX;->d:LX/6qh;

    iget-object v2, p0, LX/CUX;->c:LX/17Y;

    iget-object p1, p0, LX/CUX;->a:Landroid/content/Context;

    iget-object p2, v0, Lcom/facebook/pages/common/platform/payments/InstantWorkflowsConfirmationParams;->c:Ljava/lang/String;

    invoke-interface {v2, p1, p2}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/6qh;->b(Landroid/content/Intent;)V

    .line 1896609
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
