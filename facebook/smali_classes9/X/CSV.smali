.class public final enum LX/CSV;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/CSV;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/CSV;

.field public static final enum COMPONENTS:LX/CSV;

.field public static final enum DISCLAIMER:LX/CSV;

.field public static final enum FOOTER:LX/CSV;

.field public static final enum SHIMMERING_VIEW:LX/CSV;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1894652
    new-instance v0, LX/CSV;

    const-string v1, "COMPONENTS"

    invoke-direct {v0, v1, v2}, LX/CSV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CSV;->COMPONENTS:LX/CSV;

    .line 1894653
    new-instance v0, LX/CSV;

    const-string v1, "FOOTER"

    invoke-direct {v0, v1, v3}, LX/CSV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CSV;->FOOTER:LX/CSV;

    .line 1894654
    new-instance v0, LX/CSV;

    const-string v1, "DISCLAIMER"

    invoke-direct {v0, v1, v4}, LX/CSV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CSV;->DISCLAIMER:LX/CSV;

    .line 1894655
    new-instance v0, LX/CSV;

    const-string v1, "SHIMMERING_VIEW"

    invoke-direct {v0, v1, v5}, LX/CSV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CSV;->SHIMMERING_VIEW:LX/CSV;

    .line 1894656
    const/4 v0, 0x4

    new-array v0, v0, [LX/CSV;

    sget-object v1, LX/CSV;->COMPONENTS:LX/CSV;

    aput-object v1, v0, v2

    sget-object v1, LX/CSV;->FOOTER:LX/CSV;

    aput-object v1, v0, v3

    sget-object v1, LX/CSV;->DISCLAIMER:LX/CSV;

    aput-object v1, v0, v4

    sget-object v1, LX/CSV;->SHIMMERING_VIEW:LX/CSV;

    aput-object v1, v0, v5

    sput-object v0, LX/CSV;->$VALUES:[LX/CSV;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1894657
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/CSV;
    .locals 1

    .prologue
    .line 1894658
    const-class v0, LX/CSV;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/CSV;

    return-object v0
.end method

.method public static values()[LX/CSV;
    .locals 1

    .prologue
    .line 1894659
    sget-object v0, LX/CSV;->$VALUES:[LX/CSV;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/CSV;

    return-object v0
.end method
