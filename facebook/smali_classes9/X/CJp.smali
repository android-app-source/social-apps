.class public final LX/CJp;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Sq;
.implements LX/0Or;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Sq",
        "<",
        "LX/CJh;",
        ">;",
        "LX/0Or",
        "<",
        "Ljava/util/Set",
        "<",
        "LX/CJh;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final a:LX/0QB;


# direct methods
.method public constructor <init>(LX/0QB;)V
    .locals 0

    .prologue
    .line 1875844
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1875845
    iput-object p1, p0, LX/CJp;->a:LX/0QB;

    .line 1875846
    return-void
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1875847
    new-instance v0, LX/0U8;

    iget-object v1, p0, LX/CJp;->a:LX/0QB;

    invoke-interface {v1}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-direct {v0, v1, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    return-object v0
.end method

.method public final provide(LX/0QC;I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1875848
    packed-switch p2, :pswitch_data_0

    .line 1875849
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid binding index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1875850
    :pswitch_0
    new-instance p0, LX/IZ5;

    invoke-static {p1}, LX/6pC;->a(LX/0QB;)LX/6pC;

    move-result-object v0

    check-cast v0, LX/6pC;

    invoke-static {p1}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    invoke-direct {p0, v0, v1}, LX/IZ5;-><init>(LX/6pC;Lcom/facebook/content/SecureContextHelper;)V

    .line 1875851
    move-object v0, p0

    .line 1875852
    :goto_0
    return-object v0

    .line 1875853
    :pswitch_1
    new-instance p0, LX/IcP;

    invoke-static {p1}, LX/Ic6;->b(LX/0QB;)LX/Ic6;

    move-result-object v0

    check-cast v0, LX/Ic6;

    invoke-static {p1}, LX/CK5;->a(LX/0QB;)LX/CK5;

    move-result-object v1

    check-cast v1, LX/CK5;

    invoke-direct {p0, v0, v1}, LX/IcP;-><init>(LX/Ic6;LX/CK5;)V

    .line 1875854
    move-object v0, p0

    .line 1875855
    goto :goto_0

    .line 1875856
    :pswitch_2
    new-instance v1, LX/IcQ;

    invoke-static {p1}, LX/IcC;->b(LX/0QB;)LX/IcC;

    move-result-object v0

    check-cast v0, LX/IcC;

    invoke-direct {v1, v0}, LX/IcQ;-><init>(LX/IcC;)V

    .line 1875857
    move-object v0, v1

    .line 1875858
    goto :goto_0

    .line 1875859
    :pswitch_3
    new-instance p0, LX/FEz;

    invoke-static {p1}, LX/FEZ;->b(LX/0QB;)LX/FEZ;

    move-result-object v0

    check-cast v0, LX/FEZ;

    invoke-static {p1}, LX/FEb;->b(LX/0QB;)LX/FEb;

    move-result-object v1

    check-cast v1, LX/FEb;

    invoke-direct {p0, v0, v1}, LX/FEz;-><init>(LX/FEZ;LX/FEb;)V

    .line 1875860
    move-object v0, p0

    .line 1875861
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 1875862
    const/4 v0, 0x4

    return v0
.end method
