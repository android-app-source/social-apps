.class public abstract LX/BYF;
.super LX/0gG;
.source ""


# instance fields
.field public a:Landroid/database/Cursor;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1795343
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/BYF;-><init>(Landroid/database/Cursor;)V

    .line 1795344
    return-void
.end method

.method private constructor <init>(Landroid/database/Cursor;)V
    .locals 1
    .param p1    # Landroid/database/Cursor;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1795328
    invoke-direct {p0}, LX/0gG;-><init>()V

    .line 1795329
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/BYF;->a(Landroid/database/Cursor;Z)V

    .line 1795330
    return-void
.end method

.method private a(Landroid/database/Cursor;Z)V
    .locals 1
    .param p1    # Landroid/database/Cursor;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1795337
    iget-object v0, p0, LX/BYF;->a:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 1795338
    iget-object v0, p0, LX/BYF;->a:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 1795339
    :cond_0
    iput-object p1, p0, LX/BYF;->a:Landroid/database/Cursor;

    .line 1795340
    if-eqz p2, :cond_1

    .line 1795341
    invoke-virtual {p0}, LX/0gG;->kV_()V

    .line 1795342
    :cond_1
    return-void
.end method


# virtual methods
.method public a(Landroid/database/Cursor;)V
    .locals 1
    .param p1    # Landroid/database/Cursor;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1795335
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, LX/BYF;->a(Landroid/database/Cursor;Z)V

    .line 1795336
    return-void
.end method

.method public final declared-synchronized b()I
    .locals 1

    .prologue
    .line 1795331
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/BYF;->a:Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 1795332
    const/4 v0, 0x0

    .line 1795333
    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    iget-object v0, p0, LX/BYF;->a:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0

    .line 1795334
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
