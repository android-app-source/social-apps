.class public LX/Awv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final a:LX/BMP;

.field private final b:LX/B5l;

.field private c:LX/1RN;


# direct methods
.method public constructor <init>(LX/1RN;LX/BMP;LX/B5l;)V
    .locals 1
    .param p1    # LX/1RN;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1727309
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1727310
    const/4 v0, 0x0

    iput-object v0, p0, LX/Awv;->c:LX/1RN;

    .line 1727311
    iput-object p1, p0, LX/Awv;->c:LX/1RN;

    .line 1727312
    iput-object p2, p0, LX/Awv;->a:LX/BMP;

    .line 1727313
    iput-object p3, p0, LX/Awv;->b:LX/B5l;

    .line 1727314
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, -0x74ec1373

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1727315
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1727316
    iget-object v1, p0, LX/Awv;->c:LX/1RN;

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1727317
    iget-object v1, p0, LX/Awv;->a:LX/BMP;

    iget-object v2, p0, LX/Awv;->c:LX/1RN;

    iget-object v3, p0, LX/Awv;->b:LX/B5l;

    iget-object v4, p0, LX/Awv;->c:LX/1RN;

    invoke-virtual {v3, v4}, LX/B5l;->a(LX/1RN;)LX/B5n;

    move-result-object v3

    invoke-virtual {v3}, LX/B5n;->a()LX/B5p;

    move-result-object v3

    invoke-virtual {v1, p1, v2, v3}, LX/B5s;->a(Landroid/view/View;LX/1RN;LX/B5o;)V

    .line 1727318
    const v1, -0x197a61e

    invoke-static {v5, v5, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
