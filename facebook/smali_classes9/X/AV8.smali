.class public LX/AV8;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:Ljava/text/SimpleDateFormat;


# instance fields
.field public final c:Ljava/io/File;

.field public final d:Ljava/io/File;

.field public final e:LX/AXF;

.field private final f:LX/2Ib;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1678937
    const-class v0, LX/AV8;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/AV8;->a:Ljava/lang/String;

    .line 1678938
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyyMMdd_HHmmss"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, LX/AV8;->b:Ljava/text/SimpleDateFormat;

    return-void
.end method

.method public constructor <init>(Ljava/io/File;LX/AXF;Ljava/lang/String;LX/2Ib;)V
    .locals 2
    .param p1    # Ljava/io/File;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/AXF;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/mediastorage/annotations/MediaStorageDirString;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1678939
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1678940
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    iput-object v0, p0, LX/AV8;->c:Ljava/io/File;

    .line 1678941
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AXF;

    iput-object v0, p0, LX/AV8;->e:LX/AXF;

    .line 1678942
    new-instance v0, Ljava/io/File;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-direct {v0, v1, p3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v0, p0, LX/AV8;->d:Ljava/io/File;

    .line 1678943
    iput-object p4, p0, LX/AV8;->f:LX/2Ib;

    .line 1678944
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 7

    .prologue
    .line 1678945
    invoke-static {}, LX/2Ib;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1678946
    iget-object v1, p0, LX/AV8;->c:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v1

    .line 1678947
    new-instance v3, Landroid/os/StatFs;

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 1678948
    invoke-virtual {v3}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v4

    int-to-long v5, v4

    invoke-virtual {v3}, Landroid/os/StatFs;->getBlockSize()I

    move-result v3

    int-to-long v3, v3

    mul-long/2addr v3, v5

    .line 1678949
    cmp-long v1, v1, v3

    if-gez v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 1678950
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method
