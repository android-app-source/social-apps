.class public final LX/Apy;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/Apz;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Z

.field public b:Z

.field public c:LX/1dQ;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1717083
    invoke-static {}, LX/Apz;->q()LX/Apz;

    move-result-object v0

    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1717084
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Apy;->b:Z

    .line 1717085
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1717086
    const-string v0, "FigCheckBox"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1717087
    if-ne p0, p1, :cond_1

    .line 1717088
    :cond_0
    :goto_0
    return v0

    .line 1717089
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1717090
    goto :goto_0

    .line 1717091
    :cond_3
    check-cast p1, LX/Apy;

    .line 1717092
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1717093
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1717094
    if-eq v2, v3, :cond_0

    .line 1717095
    iget-boolean v2, p0, LX/Apy;->a:Z

    iget-boolean v3, p1, LX/Apy;->a:Z

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 1717096
    goto :goto_0

    .line 1717097
    :cond_4
    iget-boolean v2, p0, LX/Apy;->b:Z

    iget-boolean v3, p1, LX/Apy;->b:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 1717098
    goto :goto_0
.end method
