.class public LX/AXg;
.super LX/AWT;
.source ""


# instance fields
.field public a:LX/3RX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/3RZ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final c:Landroid/view/View;

.field public final f:Lcom/facebook/video/player/CountdownRingContainer;

.field public final g:Lcom/facebook/fbui/glyph/GlyphView;

.field public final h:Lcom/facebook/resources/ui/FbButton;

.field public final i:Landroid/view/View;

.field public final j:Landroid/view/View;

.field public final k:I

.field public final l:I

.field public m:LX/7Cb;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1684415
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/AXg;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1684416
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1684454
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/AXg;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1684455
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1684437
    invoke-direct {p0, p1, p2, p3}, LX/AWT;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1684438
    const-class v0, LX/AXg;

    invoke-static {v0, p0}, LX/AXg;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1684439
    const v0, 0x7f0305df

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1684440
    const v0, 0x7f0d102b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/AXg;->c:Landroid/view/View;

    .line 1684441
    const v0, 0x7f0d102c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/player/CountdownRingContainer;

    iput-object v0, p0, LX/AXg;->f:Lcom/facebook/video/player/CountdownRingContainer;

    .line 1684442
    iget-object v0, p0, LX/AXg;->f:Lcom/facebook/video/player/CountdownRingContainer;

    const-wide/16 v2, 0xbb8

    .line 1684443
    iput-wide v2, v0, Lcom/facebook/video/player/CountdownRingContainer;->l:J

    .line 1684444
    iget-object v0, p0, LX/AXg;->f:Lcom/facebook/video/player/CountdownRingContainer;

    new-instance v1, LX/AXb;

    invoke-direct {v1, p0}, LX/AXb;-><init>(LX/AXg;)V

    .line 1684445
    iput-object v1, v0, Lcom/facebook/video/player/CountdownRingContainer;->j:LX/7Kl;

    .line 1684446
    const v0, 0x7f0d102f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, LX/AXg;->g:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1684447
    const v0, 0x7f0d1031

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, LX/AXg;->h:Lcom/facebook/resources/ui/FbButton;

    .line 1684448
    iget-object v0, p0, LX/AXg;->h:Lcom/facebook/resources/ui/FbButton;

    new-instance v1, LX/AXc;

    invoke-direct {v1, p0}, LX/AXc;-><init>(LX/AXg;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1684449
    const v0, 0x7f0d102d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/AXg;->i:Landroid/view/View;

    .line 1684450
    const v0, 0x7f0d102e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/AXg;->j:Landroid/view/View;

    .line 1684451
    invoke-virtual {p0}, LX/AXg;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b05ba

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, LX/AXg;->k:I

    .line 1684452
    invoke-virtual {p0}, LX/AXg;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x10e0001

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, LX/AXg;->l:I

    .line 1684453
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/AXg;

    invoke-static {p0}, LX/8iw;->b(LX/0QB;)LX/8iw;

    move-result-object v1

    check-cast v1, LX/3RX;

    invoke-static {p0}, LX/3RZ;->b(LX/0QB;)LX/3RZ;

    move-result-object p0

    check-cast p0, LX/3RZ;

    iput-object v1, p1, LX/AXg;->a:LX/3RX;

    iput-object p0, p1, LX/AXg;->b:LX/3RZ;

    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1684421
    iget-object v0, p0, LX/AWT;->d:Landroid/view/ViewGroup;

    if-eqz v0, :cond_1

    .line 1684422
    iget-object v0, p0, LX/AXg;->f:Lcom/facebook/video/player/CountdownRingContainer;

    invoke-virtual {v0}, Lcom/facebook/video/player/CountdownRingContainer;->b()V

    .line 1684423
    iget-object v0, p0, LX/AXg;->f:Lcom/facebook/video/player/CountdownRingContainer;

    invoke-virtual {v0}, Lcom/facebook/video/player/CountdownRingContainer;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 1684424
    iget-object v0, p0, LX/AXg;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 1684425
    iget-object v0, p0, LX/AXg;->h:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbButton;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 1684426
    iget-object v0, p0, LX/AXg;->i:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 1684427
    iget-object v0, p0, LX/AXg;->j:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 1684428
    iget-object v0, p0, LX/AXg;->g:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0}, Lcom/facebook/fbui/glyph/GlyphView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 1684429
    iget-object v0, p0, LX/AWT;->e:LX/AVF;

    iget-object v1, p0, LX/AWT;->e:LX/AVF;

    .line 1684430
    iget-object v3, v1, LX/AVF;->c:LX/AVE;

    move-object v1, v3

    .line 1684431
    invoke-virtual {v0, v1}, LX/AVF;->b(LX/AVE;)V

    .line 1684432
    iget-object v0, p0, LX/AXg;->m:LX/7Cb;

    if-eqz v0, :cond_0

    .line 1684433
    iget-object v0, p0, LX/AXg;->m:LX/7Cb;

    invoke-virtual {v0}, LX/7Cb;->a()V

    .line 1684434
    iput-object v2, p0, LX/AXg;->m:LX/7Cb;

    .line 1684435
    :cond_0
    const/4 v0, 0x1

    .line 1684436
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final iF_()V
    .locals 2

    .prologue
    .line 1684417
    invoke-super {p0}, LX/AWT;->iF_()V

    .line 1684418
    iget-object v0, p0, LX/AXg;->c:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 1684419
    iget-object v0, p0, LX/AXg;->c:Landroid/view/View;

    new-instance v1, Lcom/facebook/facecast/plugin/FacecastEndingCountdownPlugin$3;

    invoke-direct {v1, p0}, Lcom/facebook/facecast/plugin/FacecastEndingCountdownPlugin$3;-><init>(LX/AXg;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 1684420
    return-void
.end method
