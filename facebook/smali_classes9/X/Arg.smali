.class public final LX/Arg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Arf;


# instance fields
.field public final synthetic a:LX/Arh;

.field private b:F


# direct methods
.method public constructor <init>(LX/Arh;)V
    .locals 0

    .prologue
    .line 1719373
    iput-object p1, p0, LX/Arg;->a:LX/Arh;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1719363
    iget-object v0, p0, LX/Arg;->a:LX/Arh;

    iget-object v0, v0, LX/Arh;->j:LX/Arq;

    .line 1719364
    iget-object v1, v0, LX/Arq;->D:LX/6Ia;

    invoke-interface {v1}, LX/6Ia;->i()I

    move-result v1

    move v0, v1

    .line 1719365
    int-to-float v0, v0

    iput v0, p0, LX/Arg;->b:F

    .line 1719366
    return-void
.end method

.method public final a(F)V
    .locals 3

    .prologue
    .line 1719367
    iget-object v0, p0, LX/Arg;->a:LX/Arh;

    iget-object v0, v0, LX/Arh;->j:LX/Arq;

    invoke-virtual {v0}, LX/Arq;->m()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, p1

    iget v1, p0, LX/Arg;->b:F

    add-float/2addr v0, v1

    .line 1719368
    iget-object v1, p0, LX/Arg;->a:LX/Arh;

    iget-object v1, v1, LX/Arh;->j:LX/Arq;

    invoke-virtual {v1}, LX/Arq;->m()I

    move-result v1

    int-to-float v1, v1

    const/4 v2, 0x0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 1719369
    iput v0, p0, LX/Arg;->b:F

    .line 1719370
    iget-object v0, p0, LX/Arg;->a:LX/Arh;

    iget-object v0, v0, LX/Arh;->j:LX/Arq;

    iget v1, p0, LX/Arg;->b:F

    float-to-int v1, v1

    .line 1719371
    iget-object v2, v0, LX/Arq;->D:LX/6Ia;

    invoke-interface {v2, v1}, LX/6Ia;->a(I)V

    .line 1719372
    return-void
.end method
