.class public LX/BQ7;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Z


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1781480
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1781481
    iput-object p1, p0, LX/BQ7;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1781482
    iget-object v0, p0, LX/BQ7;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/BQO;->c:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    iput-boolean v0, p0, LX/BQ7;->d:Z

    .line 1781483
    iget-boolean v0, p0, LX/BQ7;->d:Z

    if-eqz v0, :cond_0

    .line 1781484
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/BQ7;->b:Ljava/util/Map;

    .line 1781485
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/BQ7;->c:Ljava/util/Map;

    .line 1781486
    :cond_0
    return-void
.end method

.method public static a(LX/0QB;)LX/BQ7;
    .locals 4

    .prologue
    .line 1781463
    const-class v1, LX/BQ7;

    monitor-enter v1

    .line 1781464
    :try_start_0
    sget-object v0, LX/BQ7;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1781465
    sput-object v2, LX/BQ7;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1781466
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1781467
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1781468
    new-instance p0, LX/BQ7;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {p0, v3}, LX/BQ7;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 1781469
    move-object v0, p0

    .line 1781470
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1781471
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/BQ7;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1781472
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1781473
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1781477
    iget-boolean v0, p0, LX/BQ7;->d:Z

    if-nez v0, :cond_0

    .line 1781478
    :goto_0
    return-void

    .line 1781479
    :cond_0
    iget-object v0, p0, LX/BQ7;->b:Ljava/util/Map;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 1781474
    iget-boolean v0, p0, LX/BQ7;->d:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/BQ7;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1781475
    :cond_0
    :goto_0
    return-void

    .line 1781476
    :cond_1
    iget-object v1, p0, LX/BQ7;->c:Ljava/util/Map;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-object v0, p0, LX/BQ7;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    sub-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
