.class public LX/B0W;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/util/concurrent/atomic/AtomicInteger;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1733950
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, LX/B0W;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1733951
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a()I
    .locals 1

    .prologue
    .line 1733952
    sget-object v0, LX/B0W;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v0

    return v0
.end method

.method public static a(Lcom/facebook/quicklog/QuickPerformanceLogger;LX/B0V;LX/B0d;)LX/0zO;
    .locals 4

    .prologue
    const/4 v3, 0x2

    const v2, 0x900001

    .line 1733953
    invoke-static {}, LX/B0W;->a()I

    move-result v0

    .line 1733954
    invoke-interface {p0, v2, v0}, Lcom/facebook/quicklog/QuickPerformanceLogger;->e(II)V

    .line 1733955
    invoke-static {p1}, LX/B0W;->a(LX/B0V;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0, v2, v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;)V

    .line 1733956
    :try_start_0
    invoke-virtual {p1, p2}, LX/B0V;->a(LX/B0d;)LX/0zO;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 1733957
    invoke-interface {p0, v2, v0, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    return-object v1

    :catchall_0
    move-exception v1

    invoke-interface {p0, v2, v0, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    throw v1
.end method

.method public static a(Lcom/facebook/quicklog/QuickPerformanceLogger;LX/B0Z;LX/4a1;)LX/0zO;
    .locals 4

    .prologue
    const/4 v3, 0x2

    const v2, 0x900001

    .line 1733958
    invoke-static {}, LX/B0W;->a()I

    move-result v0

    .line 1733959
    invoke-interface {p0, v2, v0}, Lcom/facebook/quicklog/QuickPerformanceLogger;->e(II)V

    .line 1733960
    invoke-static {p1}, LX/B0W;->a(LX/B0V;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0, v2, v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;)V

    .line 1733961
    :try_start_0
    invoke-virtual {p1, p2}, LX/B0Z;->a(LX/4a1;)LX/0zO;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 1733962
    invoke-interface {p0, v2, v0, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    return-object v1

    :catchall_0
    move-exception v1

    invoke-interface {p0, v2, v0, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    throw v1
.end method

.method public static a(Lcom/facebook/quicklog/QuickPerformanceLogger;LX/B0V;LX/B0d;Lcom/facebook/graphql/executor/GraphQLResult;)LX/B0N;
    .locals 4

    .prologue
    const/4 v3, 0x2

    const v2, 0x900002

    .line 1733963
    invoke-static {}, LX/B0W;->a()I

    move-result v0

    .line 1733964
    invoke-interface {p0, v2, v0}, Lcom/facebook/quicklog/QuickPerformanceLogger;->e(II)V

    .line 1733965
    invoke-static {p1}, LX/B0W;->a(LX/B0V;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0, v2, v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;)V

    .line 1733966
    :try_start_0
    invoke-virtual {p1, p2, p3}, LX/B0V;->a(LX/B0d;Lcom/facebook/graphql/executor/GraphQLResult;)LX/B0N;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 1733967
    invoke-interface {p0, v2, v0, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    return-object v1

    :catchall_0
    move-exception v1

    invoke-interface {p0, v2, v0, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    throw v1
.end method

.method private static a(LX/B0V;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1733968
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "configuration:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/B0V;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
