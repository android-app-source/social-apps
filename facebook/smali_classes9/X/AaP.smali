.class public LX/AaP;
.super Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;
.source ""

# interfaces
.implements LX/AXW;


# instance fields
.field public g:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/70D;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/Ac6;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/AaQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/AaX;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final m:Lcom/facebook/widget/SwitchCompat;

.field private final n:Landroid/view/ViewGroup;

.field private final o:Landroid/view/ViewGroup;

.field private final p:Lcom/facebook/resources/ui/FbTextView;

.field private final q:Lcom/facebook/resources/ui/FbTextView;

.field private final r:Lcom/facebook/resources/ui/FbButton;

.field private final s:Lcom/facebook/resources/ui/FbTextView;

.field private final t:Ljava/lang/String;

.field private final u:Ljava/lang/String;

.field private v:I

.field private w:Ljava/lang/String;

.field private x:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1688508
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/AaP;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1688509
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1688445
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/AaP;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1688446
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 9
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ConstructorMayLeakThis"
        }
    .end annotation

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 1688447
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1688448
    const-string v0, "tip_jar"

    iput-object v0, p0, LX/AaP;->t:Ljava/lang/String;

    .line 1688449
    const-string v0, "TIPJAR"

    iput-object v0, p0, LX/AaP;->u:Ljava/lang/String;

    .line 1688450
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p3

    move-object v2, p0

    check-cast v2, LX/AaP;

    invoke-static {p3}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v3

    check-cast v3, LX/1Ck;

    invoke-static {p3}, LX/70D;->a(LX/0QB;)LX/70D;

    move-result-object v6

    check-cast v6, LX/70D;

    invoke-static {p3}, LX/Ac6;->b(LX/0QB;)LX/Ac6;

    move-result-object v7

    check-cast v7, LX/Ac6;

    invoke-static {p3}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v8

    check-cast v8, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p3}, LX/AaQ;->b(LX/0QB;)LX/AaQ;

    move-result-object p2

    check-cast p2, LX/AaQ;

    const/16 v0, 0x1be3    # 1.0004E-41f

    invoke-static {p3, v0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p3

    iput-object v3, v2, LX/AaP;->g:LX/1Ck;

    iput-object v6, v2, LX/AaP;->h:LX/70D;

    iput-object v7, v2, LX/AaP;->i:LX/Ac6;

    iput-object v8, v2, LX/AaP;->j:Lcom/facebook/content/SecureContextHelper;

    iput-object p2, v2, LX/AaP;->k:LX/AaQ;

    iput-object p3, v2, LX/AaP;->l:LX/0Ot;

    .line 1688451
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 1688452
    const v0, 0x7f0d0ff7

    invoke-virtual {p0, v0}, LX/AaP;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 1688453
    const v1, 0x7f0305d7

    invoke-virtual {v2, v1, v0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 1688454
    const v1, 0x7f0d0ffa

    invoke-virtual {p0, v1}, LX/AaP;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbTextView;

    iput-object v1, p0, LX/AaP;->s:Lcom/facebook/resources/ui/FbTextView;

    .line 1688455
    iget-object v1, p0, LX/AaP;->s:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v1

    .line 1688456
    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v3, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 1688457
    const v0, 0x7f0d1002

    invoke-virtual {p0, v0}, LX/AaP;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 1688458
    const v1, 0x7f0305d8

    invoke-virtual {v2, v1, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1688459
    const v1, 0x7f0305d6

    invoke-virtual {v2, v1, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1688460
    const v0, 0x7f0d1012

    invoke-virtual {p0, v0}, LX/AaP;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, LX/AaP;->n:Landroid/view/ViewGroup;

    .line 1688461
    const v0, 0x7f0d100d

    invoke-virtual {p0, v0}, LX/AaP;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, LX/AaP;->o:Landroid/view/ViewGroup;

    .line 1688462
    const v0, 0x7f0d100e

    invoke-virtual {p0, v0}, LX/AaP;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/AaP;->p:Lcom/facebook/resources/ui/FbTextView;

    .line 1688463
    const v0, 0x7f0d100f

    invoke-virtual {p0, v0}, LX/AaP;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/AaP;->q:Lcom/facebook/resources/ui/FbTextView;

    .line 1688464
    const v0, 0x7f0d1014

    invoke-virtual {p0, v0}, LX/AaP;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/SwitchCompat;

    iput-object v0, p0, LX/AaP;->m:Lcom/facebook/widget/SwitchCompat;

    .line 1688465
    iget-object v0, p0, LX/AaP;->m:Lcom/facebook/widget/SwitchCompat;

    invoke-virtual {v0, v4}, Lcom/facebook/widget/SwitchCompat;->setChecked(Z)V

    .line 1688466
    iget-object v0, p0, LX/AaP;->m:Lcom/facebook/widget/SwitchCompat;

    invoke-virtual {p0}, LX/AaP;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080c8f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/SwitchCompat;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1688467
    const v0, 0x7f0d1011

    invoke-virtual {p0, v0}, LX/AaP;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, LX/AaP;->r:Lcom/facebook/resources/ui/FbButton;

    .line 1688468
    iget-object v0, p0, LX/AaP;->r:Lcom/facebook/resources/ui/FbButton;

    new-instance v1, LX/AaM;

    invoke-direct {v1, p0}, LX/AaM;-><init>(LX/AaP;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1688469
    iget-object v0, p0, LX/AaP;->i:LX/Ac6;

    invoke-virtual {v0}, LX/Ac6;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1688470
    iget-object v0, p0, LX/AaP;->n:Landroid/view/ViewGroup;

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1688471
    :goto_0
    iget-object v0, p0, LX/AaP;->o:Landroid/view/ViewGroup;

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1688472
    iget-object v0, p0, LX/AaP;->r:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, v5}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 1688473
    return-void

    .line 1688474
    :cond_0
    iget-object v0, p0, LX/AaP;->n:Landroid/view/ViewGroup;

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_0
.end method

.method public static setPaymentInfo(LX/AaP;Lcom/facebook/payments/paymentmethods/model/PaymentMethod;)V
    .locals 7
    .param p0    # LX/AaP;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/16 v1, 0x8

    const/4 v3, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1688475
    if-eqz p1, :cond_0

    .line 1688476
    iget-object v0, p0, LX/AaP;->o:Landroid/view/ViewGroup;

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1688477
    iget-object v0, p0, LX/AaP;->r:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 1688478
    iget-object v0, p0, LX/AaP;->p:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, LX/AaP;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080cd8

    new-array v3, v3, [Ljava/lang/Object;

    iget v4, p0, LX/AaP;->v:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    iget-object v4, p0, LX/AaP;->w:Ljava/lang/String;

    aput-object v4, v3, v6

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1688479
    iget-object v0, p0, LX/AaP;->q:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, LX/AaP;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-interface {p1, v1}, Lcom/facebook/payments/paymentmethods/model/PaymentMethod;->a(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1688480
    :goto_0
    return-void

    .line 1688481
    :cond_0
    iget-object v0, p0, LX/AaP;->o:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1688482
    iget-object v0, p0, LX/AaP;->r:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, v5}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 1688483
    iget-object v0, p0, LX/AaP;->s:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, LX/AaP;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080cd9

    new-array v3, v3, [Ljava/lang/Object;

    iget v4, p0, LX/AaP;->v:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    iget-object v4, p0, LX/AaP;->w:Ljava/lang/String;

    aput-object v4, v3, v6

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(JJLjava/lang/String;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerTargetData;I)V
    .locals 1

    .prologue
    .line 1688484
    invoke-super/range {p0 .. p8}, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;->a(JJLjava/lang/String;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerTargetData;I)V

    .line 1688485
    iput-object p6, p0, LX/AaP;->x:Ljava/lang/String;

    .line 1688486
    return-void
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    .line 1688487
    iput p2, p0, LX/AaP;->v:I

    .line 1688488
    iput-object p1, p0, LX/AaP;->w:Ljava/lang/String;

    .line 1688489
    iget v0, p0, LX/AaP;->v:I

    if-lez v0, :cond_0

    .line 1688490
    iget-object v0, p0, LX/AaP;->g:LX/1Ck;

    sget-object v1, LX/AaO;->GET_DEFAULT_PAYMENT_METHOD:LX/AaO;

    iget-object v2, p0, LX/AaP;->h:LX/70D;

    sget-object v3, LX/6xg;->MOR_P2P_TRANSFER:LX/6xg;

    invoke-static {v3}, Lcom/facebook/payments/paymentmethods/picker/protocol/GetPaymentMethodsInfoParams;->a(LX/6xg;)LX/70C;

    move-result-object v3

    invoke-virtual {v3}, LX/70C;->a()Lcom/facebook/payments/paymentmethods/picker/protocol/GetPaymentMethodsInfoParams;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/6u3;->b(Landroid/os/Parcelable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    new-instance v3, LX/AaN;

    invoke-direct {v3, p0}, LX/AaN;-><init>(LX/AaP;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->b(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1688491
    :goto_0
    return-void

    .line 1688492
    :cond_0
    iget-object v0, p0, LX/AaP;->s:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, LX/AaP;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080cda

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1688493
    iget-object v0, p0, LX/AaP;->o:Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1688494
    iget-object v0, p0, LX/AaP;->r:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    goto :goto_0
.end method

.method public final c()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 1688495
    invoke-super {p0}, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;->c()V

    .line 1688496
    iget-object v0, p0, LX/AaP;->n:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1688497
    iget-object v0, p0, LX/AaP;->r:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 1688498
    iget-object v0, p0, LX/AaP;->o:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1688499
    return-void
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 1688500
    invoke-super {p0}, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;->g()V

    .line 1688501
    iget-object v0, p0, LX/AaP;->m:Lcom/facebook/widget/SwitchCompat;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/AaP;->m:Lcom/facebook/widget/SwitchCompat;

    invoke-virtual {v0}, Lcom/facebook/widget/SwitchCompat;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1688502
    invoke-virtual {p0}, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;->h()V

    .line 1688503
    :cond_0
    return-void
.end method

.method public final iF_()V
    .locals 2

    .prologue
    .line 1688504
    invoke-super {p0}, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;->iF_()V

    .line 1688505
    iget-object v0, p0, LX/AaP;->k:LX/AaQ;

    invoke-virtual {v0}, LX/AaQ;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1688506
    iget-object v0, p0, LX/AaP;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AaX;

    iget-object v1, p0, LX/AaP;->x:Ljava/lang/String;

    invoke-virtual {v0, p0, v1}, LX/AaX;->a(LX/AXW;Ljava/lang/String;)V

    .line 1688507
    :cond_0
    return-void
.end method
