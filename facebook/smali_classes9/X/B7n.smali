.class public LX/B7n;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements LX/B7m;


# static fields
.field public static final a:LX/B7j;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/B7j",
            "<",
            "LX/B7n;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public b:LX/B7W;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private c:Landroid/widget/CheckBox;

.field private d:Landroid/widget/TextView;

.field public e:Z

.field public f:Ljava/lang/String;

.field private g:Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1748196
    new-instance v0, LX/B7k;

    invoke-direct {v0}, LX/B7k;-><init>()V

    sput-object v0, LX/B7n;->a:LX/B7j;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1748190
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 1748191
    const v0, 0x7f0309c0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1748192
    const v0, 0x7f0d18d3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, LX/B7n;->c:Landroid/widget/CheckBox;

    .line 1748193
    const v0, 0x7f0d18d4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/B7n;->d:Landroid/widget/TextView;

    .line 1748194
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, LX/B7n;

    invoke-static {v0}, LX/B7W;->a(LX/0QB;)LX/B7W;

    move-result-object v0

    check-cast v0, LX/B7W;

    iput-object v0, p0, LX/B7n;->b:LX/B7W;

    .line 1748195
    return-void
.end method

.method private d()V
    .locals 2

    .prologue
    .line 1748188
    iget-object v0, p0, LX/B7n;->c:Landroid/widget/CheckBox;

    new-instance v1, LX/B7l;

    invoke-direct {v1, p0}, LX/B7l;-><init>(LX/B7n;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 1748189
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1748186
    iget-object v0, p0, LX/B7n;->c:Landroid/widget/CheckBox;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 1748187
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;LX/B7F;I)V
    .locals 2

    .prologue
    .line 1748179
    iput-object p1, p0, LX/B7n;->g:Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;

    .line 1748180
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->p()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1748181
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->p()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/B7n;->a(Ljava/lang/CharSequence;)V

    .line 1748182
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->r()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->r()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1748183
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->r()LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-virtual {p0, v0}, LX/B7n;->setChecked(Z)V

    .line 1748184
    :cond_1
    invoke-direct {p0}, LX/B7n;->d()V

    .line 1748185
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1748176
    iget-object v0, p0, LX/B7n;->c:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setText(Ljava/lang/CharSequence;)V

    .line 1748177
    invoke-direct {p0}, LX/B7n;->d()V

    .line 1748178
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1748174
    iget-object v0, p0, LX/B7n;->d:Landroid/widget/TextView;

    invoke-static {v0, p1}, LX/B8v;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 1748175
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1748161
    iget-object v0, p0, LX/B7n;->c:Landroid/widget/CheckBox;

    iget-object v1, p0, LX/B7n;->d:Landroid/widget/TextView;

    invoke-static {v0, v1}, LX/B8v;->a(Landroid/view/View;Landroid/widget/TextView;)V

    .line 1748162
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1748172
    iget-object v0, p0, LX/B7n;->d:Landroid/widget/TextView;

    invoke-static {v0}, LX/B8v;->a(Landroid/widget/TextView;)V

    .line 1748173
    return-void
.end method

.method public getBoundedInfoFieldData()Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;
    .locals 1

    .prologue
    .line 1748171
    iget-object v0, p0, LX/B7n;->g:Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;

    return-object v0
.end method

.method public getErrorMessage()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1748170
    invoke-virtual {p0}, LX/B7n;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0824e8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getInputCustomToken()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1748169
    const-string v0, ""

    return-object v0
.end method

.method public getInputValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1748168
    iget-object v0, p0, LX/B7n;->c:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setChecked(Z)V
    .locals 1

    .prologue
    .line 1748166
    iget-object v0, p0, LX/B7n;->c:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 1748167
    return-void
.end method

.method public setInputValue(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1748164
    invoke-static {p1}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-virtual {p0, v0}, LX/B7n;->setChecked(Z)V

    .line 1748165
    return-void
.end method

.method public setOnDataChangeListener(LX/B7w;)V
    .locals 0

    .prologue
    .line 1748163
    return-void
.end method
