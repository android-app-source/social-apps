.class public LX/CML;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static final c:Ljava/lang/Object;


# instance fields
.field private b:LX/3QX;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1880534
    const-class v0, LX/CML;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/CML;->a:Ljava/lang/String;

    .line 1880535
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/CML;->c:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/3QX;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1880531
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1880532
    iput-object p1, p0, LX/CML;->b:LX/3QX;

    .line 1880533
    return-void
.end method

.method public static a(LX/0QB;)LX/CML;
    .locals 7

    .prologue
    .line 1880496
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 1880497
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 1880498
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 1880499
    if-nez v1, :cond_0

    .line 1880500
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1880501
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 1880502
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 1880503
    sget-object v1, LX/CML;->c:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 1880504
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 1880505
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1880506
    :cond_1
    if-nez v1, :cond_4

    .line 1880507
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 1880508
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 1880509
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 1880510
    new-instance p0, LX/CML;

    invoke-static {v0}, LX/3QX;->a(LX/0QB;)LX/3QX;

    move-result-object v1

    check-cast v1, LX/3QX;

    invoke-direct {p0, v1}, LX/CML;-><init>(LX/3QX;)V

    .line 1880511
    move-object v1, p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1880512
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 1880513
    if-nez v1, :cond_2

    .line 1880514
    sget-object v0, LX/CML;->c:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CML;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1880515
    :goto_1
    if-eqz v0, :cond_3

    .line 1880516
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 1880517
    :goto_3
    check-cast v0, LX/CML;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 1880518
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 1880519
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 1880520
    :catchall_1
    move-exception v0

    .line 1880521
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 1880522
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 1880523
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 1880524
    :cond_2
    :try_start_8
    sget-object v0, LX/CML;->c:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CML;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 1880529
    iget-object v0, p0, LX/CML;->b:LX/3QX;

    sget-object v1, LX/6hP;->EMOJI_COLOR_PREF:LX/6hP;

    invoke-virtual {v0, v1}, LX/3QX;->b(LX/6hP;)LX/0am;

    move-result-object v0

    .line 1880530
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(I)V
    .locals 3

    .prologue
    .line 1880525
    :try_start_0
    iget-object v0, p0, LX/CML;->b:LX/3QX;

    sget-object v1, LX/6hP;->EMOJI_COLOR_PREF:LX/6hP;

    invoke-virtual {v0, v1, p1}, LX/3QX;->a(LX/6hP;I)V
    :try_end_0
    .catch LX/6hO; {:try_start_0 .. :try_end_0} :catch_0

    .line 1880526
    :goto_0
    return-void

    .line 1880527
    :catch_0
    move-exception v0

    .line 1880528
    sget-object v1, LX/CML;->a:Ljava/lang/String;

    const-string v2, "Failed to save emoji color pref to omnistore."

    invoke-static {v1, v2, v0}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
