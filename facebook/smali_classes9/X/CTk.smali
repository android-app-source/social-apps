.class public final LX/CTk;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/CU0;

.field public final b:Lcom/facebook/payments/currency/CurrencyAmount;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/CU0;Lcom/facebook/payments/currency/CurrencyAmount;Ljava/lang/String;)V
    .locals 1
    .param p2    # Lcom/facebook/payments/currency/CurrencyAmount;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1895949
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1895950
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1895951
    iput-object p1, p0, LX/CTk;->a:LX/CU0;

    .line 1895952
    iput-object p2, p0, LX/CTk;->b:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 1895953
    iput-object p3, p0, LX/CTk;->c:Ljava/lang/String;

    .line 1895954
    return-void

    .line 1895955
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
