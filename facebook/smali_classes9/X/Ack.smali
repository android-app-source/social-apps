.class public LX/Ack;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:LX/0tX;

.field public final c:Ljava/util/concurrent/ExecutorService;

.field public final d:LX/03V;

.field public e:LX/Acj;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/facecastdisplay/protocol/FetchMomentsOfInterestQueryModels$FetchMomentsOfInterestQueryModel;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1693080
    const-class v0, LX/Ack;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Ack;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/ExecutorService;LX/0tX;LX/03V;)V
    .locals 0
    .param p1    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1693081
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1693082
    iput-object p1, p0, LX/Ack;->c:Ljava/util/concurrent/ExecutorService;

    .line 1693083
    iput-object p2, p0, LX/Ack;->b:LX/0tX;

    .line 1693084
    iput-object p3, p0, LX/Ack;->d:LX/03V;

    .line 1693085
    return-void
.end method


# virtual methods
.method public final b()V
    .locals 2

    .prologue
    .line 1693086
    iget-object v0, p0, LX/Ack;->g:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    .line 1693087
    iget-object v0, p0, LX/Ack;->g:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 1693088
    const/4 v0, 0x0

    iput-object v0, p0, LX/Ack;->g:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1693089
    :cond_0
    return-void
.end method
