.class public LX/Bjw;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1812903
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1812904
    iput-object p1, p0, LX/Bjw;->a:Landroid/content/res/Resources;

    .line 1812905
    return-void
.end method

.method public static a(LX/0QB;)LX/Bjw;
    .locals 2

    .prologue
    .line 1812906
    new-instance v1, LX/Bjw;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    invoke-direct {v1, v0}, LX/Bjw;-><init>(Landroid/content/res/Resources;)V

    .line 1812907
    move-object v0, v1

    .line 1812908
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/base/activity/FbFragmentActivity;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V
    .locals 6

    .prologue
    .line 1812909
    invoke-static {p1}, LX/63Z;->b(Landroid/app/Activity;)Z

    .line 1812910
    const v0, 0x7f0d00bc

    invoke-virtual {p1, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    .line 1812911
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v1

    const v2, 0x7f0d01a2

    .line 1812912
    iput v2, v1, LX/108;->a:I

    .line 1812913
    move-object v1, v1

    .line 1812914
    iget-object v2, p0, LX/Bjw;->a:Landroid/content/res/Resources;

    const v3, 0x7f082159

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1812915
    iput-object v2, v1, LX/108;->g:Ljava/lang/String;

    .line 1812916
    move-object v1, v1

    .line 1812917
    const/4 v2, -0x2

    .line 1812918
    iput v2, v1, LX/108;->h:I

    .line 1812919
    move-object v1, v1

    .line 1812920
    invoke-virtual {v1}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v1

    .line 1812921
    const/4 v2, 0x1

    invoke-interface {v0, v2}, LX/0h5;->setShowDividers(Z)V

    .line 1812922
    invoke-virtual {p1}, Lcom/facebook/base/activity/FbFragmentActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f0304c4

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    invoke-interface {v0, v2}, LX/0h5;->setCustomTitleView(Landroid/view/View;)V

    .line 1812923
    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 1812924
    invoke-interface {v0, p2}, LX/0h5;->setTitlebarAsModal(Landroid/view/View$OnClickListener;)V

    .line 1812925
    new-instance v1, LX/Bju;

    invoke-direct {v1, p0, p3}, LX/Bju;-><init>(LX/Bjw;Landroid/view/View$OnClickListener;)V

    invoke-interface {v0, v1}, LX/0h5;->setOnToolbarButtonListener(LX/63W;)V

    .line 1812926
    return-void
.end method
