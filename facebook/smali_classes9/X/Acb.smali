.class public final LX/Acb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingBarFeedbackQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;


# direct methods
.method public constructor <init>(Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;)V
    .locals 0

    .prologue
    .line 1692949
    iput-object p1, p0, LX/Acb;->a:Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 3
    .param p1    # Lcom/facebook/graphql/executor/GraphQLResult;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingBarFeedbackQueryModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1692936
    iget-object v0, p0, LX/Acb;->a:Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Acb;->a:Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    .line 1692937
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1692938
    if-nez v0, :cond_1

    .line 1692939
    :cond_0
    :goto_0
    return-void

    .line 1692940
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1692941
    check-cast v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingBarFeedbackQueryModel;

    .line 1692942
    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingBarFeedbackQueryModel;->a()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1692943
    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingBarFeedbackQueryModel;->a()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;

    move-result-object v0

    invoke-static {v0}, LX/6Tu;->a(Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveBlingbarFeedbackModel;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    .line 1692944
    iget-object v1, p0, LX/Acb;->a:Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;

    iget v1, v1, Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;->j:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p0, LX/Acb;->a:Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;

    iget-object v2, v2, Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;->i:Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;

    invoke-static {v0, v1, v2}, LX/39G;->a(Lcom/facebook/graphql/model/GraphQLFeedback;Ljava/lang/Integer;Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;)V

    .line 1692945
    iget-object v0, p0, LX/Acb;->a:Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;->i:Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 1692947
    iget-object v0, p0, LX/Acb;->a:Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;

    iget-object v0, v0, Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;->e:LX/03V;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_graphFailure"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to fetch video feedback for blingbar with video id - "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/Acb;->a:Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;

    iget-object v3, v3, Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;->k:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1692948
    return-void
.end method

.method public final synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1692946
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    invoke-direct {p0, p1}, LX/Acb;->a(Lcom/facebook/graphql/executor/GraphQLResult;)V

    return-void
.end method
