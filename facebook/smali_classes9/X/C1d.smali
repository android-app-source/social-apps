.class public LX/C1d;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final b:Ljava/lang/String;


# instance fields
.field public a:LX/157;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation
.end field

.field public final c:LX/14v;

.field public final d:LX/C1m;

.field public final e:LX/3Hf;

.field public final f:LX/0Sh;

.field public final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0ti;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0bH;

.field public final i:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public final j:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field public k:LX/0TF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0TF",
            "<",
            "Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveStartTimeSubscriptionModels$FacecastScheduledLiveStartTimeSubscriptionModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1842744
    const-class v0, LX/C1d;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/C1d;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/14v;LX/C1m;LX/3Hf;LX/0Sh;LX/0Ot;LX/0bH;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/14v;",
            "LX/C1m;",
            "LX/3Hf;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/0Ot",
            "<",
            "LX/0ti;",
            ">;",
            "LX/0bH;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1842745
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1842746
    iput-object p1, p0, LX/C1d;->c:LX/14v;

    .line 1842747
    iput-object p2, p0, LX/C1d;->d:LX/C1m;

    .line 1842748
    iput-object p3, p0, LX/C1d;->e:LX/3Hf;

    .line 1842749
    iput-object p4, p0, LX/C1d;->f:LX/0Sh;

    .line 1842750
    iput-object p5, p0, LX/C1d;->g:LX/0Ot;

    .line 1842751
    iput-object p6, p0, LX/C1d;->h:LX/0bH;

    .line 1842752
    new-instance v0, LX/C1c;

    invoke-direct {v0, p0}, LX/C1c;-><init>(LX/C1d;)V

    iput-object v0, p0, LX/C1d;->a:LX/157;

    .line 1842753
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/C1d;->i:Ljava/util/HashMap;

    .line 1842754
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/C1d;->j:Ljava/util/HashMap;

    .line 1842755
    return-void
.end method

.method public static a$redex0(LX/C1d;Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveStartTimeSubscriptionModels$FacecastScheduledLiveStartTimeSubscriptionModel$VideoBroadcastScheduleModel;)V
    .locals 10

    .prologue
    .line 1842630
    iget-object v0, p0, LX/C1d;->i:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1842631
    invoke-static {v0}, LX/Abd;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    move-result-object v2

    .line 1842632
    if-eqz v2, :cond_0

    .line 1842633
    invoke-virtual {p1}, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveStartTimeSubscriptionModels$FacecastScheduledLiveStartTimeSubscriptionModel$VideoBroadcastScheduleModel;->k()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->k()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1842634
    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    .line 1842635
    invoke-static {v1}, LX/Abd;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;

    move-result-object v3

    .line 1842636
    new-instance v6, LX/4ZO;

    invoke-direct {v6}, LX/4ZO;-><init>()V

    .line 1842637
    invoke-virtual {v2}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1842638
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->o()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, LX/4ZO;->b:Ljava/lang/String;

    .line 1842639
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->E()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, LX/4ZO;->c:Ljava/lang/String;

    .line 1842640
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->p()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, LX/4ZO;->d:Ljava/lang/String;

    .line 1842641
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->D()J

    move-result-wide v8

    iput-wide v8, v6, LX/4ZO;->e:J

    .line 1842642
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->q()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, LX/4ZO;->f:Ljava/lang/String;

    .line 1842643
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->F()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, LX/4ZO;->g:Ljava/lang/String;

    .line 1842644
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->j()J

    move-result-wide v8

    iput-wide v8, v6, LX/4ZO;->h:J

    .line 1842645
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->r()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, LX/4ZO;->i:Ljava/lang/String;

    .line 1842646
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->C()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, LX/4ZO;->j:Ljava/lang/String;

    .line 1842647
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->k()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, LX/4ZO;->k:Ljava/lang/String;

    .line 1842648
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->B()Z

    move-result v7

    iput-boolean v7, v6, LX/4ZO;->l:Z

    .line 1842649
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->s()Z

    move-result v7

    iput-boolean v7, v6, LX/4ZO;->m:Z

    .line 1842650
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->l()Z

    move-result v7

    iput-boolean v7, v6, LX/4ZO;->n:Z

    .line 1842651
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->m()J

    move-result-wide v8

    iput-wide v8, v6, LX/4ZO;->o:J

    .line 1842652
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->t()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, LX/4ZO;->p:Ljava/lang/String;

    .line 1842653
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->u()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, LX/4ZO;->q:Ljava/lang/String;

    .line 1842654
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->v()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, LX/4ZO;->r:Ljava/lang/String;

    .line 1842655
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->G()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, LX/4ZO;->s:Ljava/lang/String;

    .line 1842656
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->w()J

    move-result-wide v8

    iput-wide v8, v6, LX/4ZO;->t:J

    .line 1842657
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->x()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, LX/4ZO;->u:Ljava/lang/String;

    .line 1842658
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->y()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    iput-object v7, v6, LX/4ZO;->v:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1842659
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->z()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    iput-object v7, v6, LX/4ZO;->w:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1842660
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->A()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v7

    iput-object v7, v6, LX/4ZO;->x:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1842661
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->n()J

    move-result-wide v8

    iput-wide v8, v6, LX/4ZO;->y:J

    .line 1842662
    invoke-static {v6, v2}, LX/0ur;->a(LX/0ur;Lcom/facebook/graphql/modelutil/BaseModel;)V

    .line 1842663
    move-object v2, v6

    .line 1842664
    invoke-virtual {p1}, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveStartTimeSubscriptionModels$FacecastScheduledLiveStartTimeSubscriptionModel$VideoBroadcastScheduleModel;->p()J

    move-result-wide v4

    .line 1842665
    iput-wide v4, v2, LX/4ZO;->y:J

    .line 1842666
    move-object v2, v2

    .line 1842667
    invoke-virtual {p1}, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveStartTimeSubscriptionModels$FacecastScheduledLiveStartTimeSubscriptionModel$VideoBroadcastScheduleModel;->j()Ljava/lang/String;

    move-result-object v4

    .line 1842668
    iput-object v4, v2, LX/4ZO;->j:Ljava/lang/String;

    .line 1842669
    move-object v2, v2

    .line 1842670
    invoke-virtual {p1}, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveStartTimeSubscriptionModels$FacecastScheduledLiveStartTimeSubscriptionModel$VideoBroadcastScheduleModel;->o()Ljava/lang/String;

    move-result-object v4

    .line 1842671
    iput-object v4, v2, LX/4ZO;->r:Ljava/lang/String;

    .line 1842672
    move-object v2, v2

    .line 1842673
    invoke-virtual {p1}, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveStartTimeSubscriptionModels$FacecastScheduledLiveStartTimeSubscriptionModel$VideoBroadcastScheduleModel;->n()Ljava/lang/String;

    move-result-object v4

    .line 1842674
    iput-object v4, v2, LX/4ZO;->q:Ljava/lang/String;

    .line 1842675
    move-object v2, v2

    .line 1842676
    invoke-virtual {p1}, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveStartTimeSubscriptionModels$FacecastScheduledLiveStartTimeSubscriptionModel$VideoBroadcastScheduleModel;->m()Ljava/lang/String;

    move-result-object v4

    .line 1842677
    iput-object v4, v2, LX/4ZO;->p:Ljava/lang/String;

    .line 1842678
    move-object v2, v2

    .line 1842679
    invoke-virtual {p1}, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveStartTimeSubscriptionModels$FacecastScheduledLiveStartTimeSubscriptionModel$VideoBroadcastScheduleModel;->l()Z

    move-result v4

    .line 1842680
    iput-boolean v4, v2, LX/4ZO;->l:Z

    .line 1842681
    move-object v2, v2

    .line 1842682
    new-instance v4, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    invoke-direct {v4, v2}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;-><init>(LX/4ZO;)V

    .line 1842683
    move-object v2, v4

    .line 1842684
    new-instance v4, LX/4Yt;

    invoke-direct {v4}, LX/4Yt;-><init>()V

    .line 1842685
    invoke-virtual {v3}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1842686
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->L()LX/0Px;

    move-result-object v5

    iput-object v5, v4, LX/4Yt;->b:LX/0Px;

    .line 1842687
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->j()Z

    move-result v5

    iput-boolean v5, v4, LX/4Yt;->c:Z

    .line 1842688
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->k()Z

    move-result v5

    iput-boolean v5, v4, LX/4Yt;->d:Z

    .line 1842689
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    iput-object v5, v4, LX/4Yt;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1842690
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->m()Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    move-result-object v5

    iput-object v5, v4, LX/4Yt;->f:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    .line 1842691
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->Q()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v5

    iput-object v5, v4, LX/4Yt;->g:Lcom/facebook/graphql/model/GraphQLProfile;

    .line 1842692
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->J()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    iput-object v5, v4, LX/4Yt;->h:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1842693
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->T()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, LX/4Yt;->i:Ljava/lang/String;

    .line 1842694
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->U()Lcom/facebook/graphql/enums/GraphQLMarketplaceNavigationDestinationType;

    move-result-object v5

    iput-object v5, v4, LX/4Yt;->j:Lcom/facebook/graphql/enums/GraphQLMarketplaceNavigationDestinationType;

    .line 1842695
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->n()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, LX/4Yt;->k:Ljava/lang/String;

    .line 1842696
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->o()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, LX/4Yt;->l:Ljava/lang/String;

    .line 1842697
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->p()Lcom/facebook/graphql/enums/GraphQLGamesInstantPlaySupportedOrientation;

    move-result-object v5

    iput-object v5, v4, LX/4Yt;->m:Lcom/facebook/graphql/enums/GraphQLGamesInstantPlaySupportedOrientation;

    .line 1842698
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->q()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, LX/4Yt;->n:Ljava/lang/String;

    .line 1842699
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->Y()Lcom/facebook/graphql/model/GraphQLGroup;

    move-result-object v5

    iput-object v5, v4, LX/4Yt;->o:Lcom/facebook/graphql/model/GraphQLGroup;

    .line 1842700
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->r()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, LX/4Yt;->p:Ljava/lang/String;

    .line 1842701
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->X()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, LX/4Yt;->q:Ljava/lang/String;

    .line 1842702
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->N()Lcom/facebook/graphql/model/GraphQLApplication;

    move-result-object v5

    iput-object v5, v4, LX/4Yt;->r:Lcom/facebook/graphql/model/GraphQLApplication;

    .line 1842703
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->H()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, LX/4Yt;->s:Ljava/lang/String;

    .line 1842704
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->M()LX/0Px;

    move-result-object v5

    iput-object v5, v4, LX/4Yt;->t:LX/0Px;

    .line 1842705
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->S()Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceFeatureEnabledList;

    move-result-object v5

    iput-object v5, v4, LX/4Yt;->u:Lcom/facebook/graphql/model/GraphQLPlatformInstantExperienceFeatureEnabledList;

    .line 1842706
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->I()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, LX/4Yt;->v:Ljava/lang/String;

    .line 1842707
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->W()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v5

    iput-object v5, v4, LX/4Yt;->w:Lcom/facebook/graphql/model/GraphQLPage;

    .line 1842708
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->O()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, LX/4Yt;->x:Ljava/lang/String;

    .line 1842709
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->P()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, LX/4Yt;->y:Ljava/lang/String;

    .line 1842710
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->s()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, LX/4Yt;->z:Ljava/lang/String;

    .line 1842711
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->t()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, LX/4Yt;->A:Ljava/lang/String;

    .line 1842712
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->u()LX/0Px;

    move-result-object v5

    iput-object v5, v4, LX/4Yt;->B:LX/0Px;

    .line 1842713
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->v()I

    move-result v5

    iput v5, v4, LX/4Yt;->C:I

    .line 1842714
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->w()I

    move-result v5

    iput v5, v4, LX/4Yt;->D:I

    .line 1842715
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->x()I

    move-result v5

    iput v5, v4, LX/4Yt;->E:I

    .line 1842716
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->y()I

    move-result v5

    iput v5, v4, LX/4Yt;->F:I

    .line 1842717
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->z()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v5

    iput-object v5, v4, LX/4Yt;->G:Lcom/facebook/graphql/model/GraphQLLocation;

    .line 1842718
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->A()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    iput-object v5, v4, LX/4Yt;->H:Lcom/facebook/graphql/model/GraphQLImage;

    .line 1842719
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->B()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, LX/4Yt;->I:Ljava/lang/String;

    .line 1842720
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->V()Lcom/facebook/graphql/model/GraphQLNativeTemplateView;

    move-result-object v5

    iput-object v5, v4, LX/4Yt;->J:Lcom/facebook/graphql/model/GraphQLNativeTemplateView;

    .line 1842721
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->C()LX/0Px;

    move-result-object v5

    iput-object v5, v4, LX/4Yt;->K:LX/0Px;

    .line 1842722
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->K()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v5

    iput-object v5, v4, LX/4Yt;->L:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1842723
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->G()Lcom/facebook/graphql/model/GraphQLPlaceRecommendationPostInfo;

    move-result-object v5

    iput-object v5, v4, LX/4Yt;->M:Lcom/facebook/graphql/model/GraphQLPlaceRecommendationPostInfo;

    .line 1842724
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->D()Z

    move-result v5

    iput-boolean v5, v4, LX/4Yt;->N:Z

    .line 1842725
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->E()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, LX/4Yt;->O:Ljava/lang/String;

    .line 1842726
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->R()Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    move-result-object v5

    iput-object v5, v4, LX/4Yt;->P:Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    .line 1842727
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->F()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, LX/4Yt;->Q:Ljava/lang/String;

    .line 1842728
    invoke-static {v4, v3}, LX/0ur;->a(LX/0ur;Lcom/facebook/graphql/modelutil/BaseModel;)V

    .line 1842729
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v5

    iput-object v5, v4, LX/4Yt;->R:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1842730
    move-object v3, v4

    .line 1842731
    iput-object v2, v3, LX/4Yt;->P:Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    .line 1842732
    move-object v2, v3

    .line 1842733
    invoke-virtual {v2}, LX/4Yt;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;

    move-result-object v2

    .line 1842734
    invoke-static {v1}, LX/39x;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/39x;

    move-result-object v1

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    .line 1842735
    iput-object v2, v1, LX/39x;->o:LX/0Px;

    .line 1842736
    move-object v1, v1

    .line 1842737
    invoke-virtual {v1}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    .line 1842738
    invoke-static {v0}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object v0

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 1842739
    iput-object v1, v0, LX/23u;->k:LX/0Px;

    .line 1842740
    move-object v0, v0

    .line 1842741
    invoke-virtual {v0}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 1842742
    iget-object v1, p0, LX/C1d;->f:LX/0Sh;

    new-instance v2, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentBroadcastStatusManager$3;

    invoke-direct {v2, p0, v0}, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentBroadcastStatusManager$3;-><init>(LX/C1d;Lcom/facebook/graphql/model/GraphQLStory;)V

    invoke-virtual {v1, v2}, LX/0Sh;->b(Ljava/lang/Runnable;)V

    .line 1842743
    :cond_1
    return-void
.end method

.method public static b(LX/0QB;)LX/C1d;
    .locals 7

    .prologue
    .line 1842756
    new-instance v0, LX/C1d;

    invoke-static {p0}, LX/14v;->a(LX/0QB;)LX/14v;

    move-result-object v1

    check-cast v1, LX/14v;

    .line 1842757
    new-instance v3, LX/C1m;

    invoke-static {p0}, LX/0gX;->a(LX/0QB;)LX/0gX;

    move-result-object v2

    check-cast v2, LX/0gX;

    invoke-direct {v3, v2}, LX/C1m;-><init>(LX/0gX;)V

    .line 1842758
    move-object v2, v3

    .line 1842759
    check-cast v2, LX/C1m;

    invoke-static {p0}, LX/3Hf;->a(LX/0QB;)LX/3Hf;

    move-result-object v3

    check-cast v3, LX/3Hf;

    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v4

    check-cast v4, LX/0Sh;

    const/16 v5, 0xafb

    invoke-static {p0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static {p0}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v6

    check-cast v6, LX/0bH;

    invoke-direct/range {v0 .. v6}, LX/C1d;-><init>(LX/14v;LX/C1m;LX/3Hf;LX/0Sh;LX/0Ot;LX/0bH;)V

    .line 1842760
    return-object v0
.end method

.method public static c(Lcom/facebook/feed/rows/core/props/FeedProps;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1842625
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1842626
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v0}, LX/Abd;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;

    move-result-object v0

    .line 1842627
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->R()Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    move-result-object v1

    if-nez v1, :cond_1

    .line 1842628
    :cond_0
    const/4 v0, 0x0

    .line 1842629
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->R()Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->k()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1842588
    invoke-static {p1}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 1842589
    invoke-static {v1}, LX/17E;->v(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    .line 1842590
    if-nez v0, :cond_0

    .line 1842591
    :goto_0
    return-void

    .line 1842592
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v2

    .line 1842593
    iget-object v0, p0, LX/C1d;->i:Ljava/util/HashMap;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1842594
    iget-object v0, p0, LX/C1d;->c:LX/14v;

    iget-object v3, p0, LX/C1d;->a:LX/157;

    invoke-virtual {v0, v1, v3}, LX/14v;->a(Lcom/facebook/graphql/model/GraphQLStory;LX/157;)V

    .line 1842595
    iget-object v0, p0, LX/C1d;->d:LX/C1m;

    invoke-static {p1}, LX/C1d;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Ljava/lang/String;

    move-result-object v3

    .line 1842596
    iget-object v4, p0, LX/C1d;->k:LX/0TF;

    if-nez v4, :cond_1

    .line 1842597
    new-instance v4, LX/C1b;

    invoke-direct {v4, p0}, LX/C1b;-><init>(LX/C1d;)V

    iput-object v4, p0, LX/C1d;->k:LX/0TF;

    .line 1842598
    :cond_1
    iget-object v4, p0, LX/C1d;->k:LX/0TF;

    move-object v4, v4

    .line 1842599
    if-nez v3, :cond_4

    .line 1842600
    :cond_2
    :goto_1
    iget-object v0, p0, LX/C1d;->j:Ljava/util/HashMap;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1842601
    iget-object v3, p0, LX/C1d;->f:LX/0Sh;

    iget-object v0, p0, LX/C1d;->j:Ljava/util/HashMap;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    invoke-virtual {v3, v0}, LX/0Sh;->c(Ljava/lang/Runnable;)V

    .line 1842602
    iget-object v0, p0, LX/C1d;->j:Ljava/util/HashMap;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1842603
    :cond_3
    iget-object v0, p0, LX/C1d;->i:Ljava/util/HashMap;

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1842604
    :cond_4
    new-instance v5, LX/4KE;

    invoke-direct {v5}, LX/4KE;-><init>()V

    .line 1842605
    const-string v6, "video_broadcast_schedule_id"

    invoke-virtual {v5, v6, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1842606
    move-object v5, v5

    .line 1842607
    new-instance v6, LX/C1n;

    invoke-direct {v6}, LX/C1n;-><init>()V

    move-object v6, v6

    .line 1842608
    const-string v7, "input"

    invoke-virtual {v6, v7, v5}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1842609
    move-object v6, v6

    .line 1842610
    const/4 v5, 0x0

    .line 1842611
    :try_start_0
    iget-object v7, v0, LX/C1m;->b:LX/0gX;

    invoke-virtual {v7, v6, v4}, LX/0gX;->a(LX/0gV;LX/0TF;)LX/0gM;
    :try_end_0
    .catch LX/31B; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    .line 1842612
    :goto_2
    iget-object v6, v0, LX/C1m;->c:Ljava/util/HashMap;

    invoke-virtual {v6, v3, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 1842613
    :catch_0
    move-exception v6

    .line 1842614
    sget-object v7, LX/C1m;->a:Ljava/lang/String;

    const-string p1, "Scheduled live start time subscription failed. %s"

    invoke-static {v7, p1, v6}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2
.end method

.method public final b(Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1842615
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1842616
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    .line 1842617
    if-nez v0, :cond_1

    .line 1842618
    :cond_0
    :goto_0
    return-void

    .line 1842619
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v1

    .line 1842620
    iget-object v0, p0, LX/C1d;->j:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    .line 1842621
    if-nez v0, :cond_0

    .line 1842622
    new-instance v0, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentBroadcastStatusManager$1;

    invoke-direct {v0, p0, v1, p1}, Lcom/facebook/feedplugins/attachments/scheduledlive/ScheduledLiveAttachmentBroadcastStatusManager$1;-><init>(LX/C1d;Ljava/lang/String;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1842623
    iget-object v2, p0, LX/C1d;->j:Ljava/util/HashMap;

    invoke-virtual {v2, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1842624
    iget-object v1, p0, LX/C1d;->f:LX/0Sh;

    const-wide/16 v2, 0x32

    invoke-virtual {v1, v0, v2, v3}, LX/0Sh;->a(Ljava/lang/Runnable;J)V

    goto :goto_0
.end method
