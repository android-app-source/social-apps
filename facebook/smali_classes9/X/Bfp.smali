.class public final LX/Bfp;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# instance fields
.field public final synthetic a:LX/Bfc;

.field public final synthetic b:Lcom/google/common/util/concurrent/SettableFuture;

.field public final synthetic c:LX/BeU;


# direct methods
.method public constructor <init>(LX/BeU;LX/Bfc;Lcom/google/common/util/concurrent/SettableFuture;)V
    .locals 0

    .prologue
    .line 1806636
    iput-object p1, p0, LX/Bfp;->c:LX/BeU;

    iput-object p2, p0, LX/Bfp;->a:LX/Bfc;

    iput-object p3, p0, LX/Bfp;->b:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 3

    .prologue
    .line 1806637
    iget-object v0, p0, LX/Bfp;->c:LX/BeU;

    iget-object v1, p0, LX/Bfp;->a:LX/Bfc;

    .line 1806638
    invoke-virtual {v0, v1}, LX/BeU;->removeView(Landroid/view/View;)V

    .line 1806639
    iget-object v2, v0, LX/BeU;->d:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1806640
    iget-object v0, p0, LX/Bfp;->b:Lcom/google/common/util/concurrent/SettableFuture;

    iget-object v1, p0, LX/Bfp;->a:LX/Bfc;

    const v2, -0x2300de46

    invoke-static {v0, v1, v2}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    .line 1806641
    return-void
.end method

.method public final onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 1806642
    return-void
.end method

.method public final onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 1806643
    return-void
.end method
