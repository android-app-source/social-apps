.class public final LX/Acg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/facecastdisplay/LiveWithPipOverlayView;


# direct methods
.method public constructor <init>(Lcom/facebook/facecastdisplay/LiveWithPipOverlayView;)V
    .locals 0

    .prologue
    .line 1693027
    iput-object p1, p0, LX/Acg;->a:Lcom/facebook/facecastdisplay/LiveWithPipOverlayView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x2

    const v1, -0x55c19150

    invoke-static {v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1693028
    iget-object v1, p0, LX/Acg;->a:Lcom/facebook/facecastdisplay/LiveWithPipOverlayView;

    iget-object v1, v1, Lcom/facebook/facecastdisplay/LiveWithPipOverlayView;->b:Landroid/os/Handler;

    iget-object v2, p0, LX/Acg;->a:Lcom/facebook/facecastdisplay/LiveWithPipOverlayView;

    iget-object v2, v2, Lcom/facebook/facecastdisplay/LiveWithPipOverlayView;->e:Ljava/lang/Runnable;

    invoke-static {v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1693029
    iget-object v1, p0, LX/Acg;->a:Lcom/facebook/facecastdisplay/LiveWithPipOverlayView;

    iget-boolean v1, v1, Lcom/facebook/facecastdisplay/LiveWithPipOverlayView;->f:Z

    if-eqz v1, :cond_0

    .line 1693030
    iget-object v1, p0, LX/Acg;->a:Lcom/facebook/facecastdisplay/LiveWithPipOverlayView;

    invoke-virtual {v1}, Lcom/facebook/facecastdisplay/LiveWithPipOverlayView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    .line 1693031
    iget-object v1, p0, LX/Acg;->a:Lcom/facebook/facecastdisplay/LiveWithPipOverlayView;

    const/4 v2, 0x0

    .line 1693032
    iput-boolean v2, v1, Lcom/facebook/facecastdisplay/LiveWithPipOverlayView;->f:Z

    .line 1693033
    :goto_0
    const v1, -0x5918cfbb

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 1693034
    :cond_0
    iget-object v1, p0, LX/Acg;->a:Lcom/facebook/facecastdisplay/LiveWithPipOverlayView;

    invoke-virtual {v1}, Lcom/facebook/facecastdisplay/LiveWithPipOverlayView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    .line 1693035
    iget-object v1, p0, LX/Acg;->a:Lcom/facebook/facecastdisplay/LiveWithPipOverlayView;

    .line 1693036
    iput-boolean v3, v1, Lcom/facebook/facecastdisplay/LiveWithPipOverlayView;->f:Z

    .line 1693037
    iget-object v1, p0, LX/Acg;->a:Lcom/facebook/facecastdisplay/LiveWithPipOverlayView;

    iget-object v1, v1, Lcom/facebook/facecastdisplay/LiveWithPipOverlayView;->b:Landroid/os/Handler;

    iget-object v2, p0, LX/Acg;->a:Lcom/facebook/facecastdisplay/LiveWithPipOverlayView;

    iget-object v2, v2, Lcom/facebook/facecastdisplay/LiveWithPipOverlayView;->e:Ljava/lang/Runnable;

    const-wide/16 v4, 0x2710

    const v3, -0xa81f9b3

    invoke-static {v1, v2, v4, v5, v3}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    goto :goto_0
.end method
