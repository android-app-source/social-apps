.class public final LX/BmT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/BmO;


# instance fields
.field public final synthetic a:Lcom/facebook/events/ui/date/StartAndEndTimePicker;


# direct methods
.method public constructor <init>(Lcom/facebook/events/ui/date/StartAndEndTimePicker;)V
    .locals 0

    .prologue
    .line 1818386
    iput-object p1, p0, LX/BmT;->a:Lcom/facebook/events/ui/date/StartAndEndTimePicker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/Calendar;)V
    .locals 3

    .prologue
    .line 1818387
    if-nez p1, :cond_0

    .line 1818388
    iget-object v0, p0, LX/BmT;->a:Lcom/facebook/events/ui/date/StartAndEndTimePicker;

    iget-object v0, v0, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->e:Lcom/facebook/events/ui/date/DatePickerView;

    iget-object v1, p0, LX/BmT;->a:Lcom/facebook/events/ui/date/StartAndEndTimePicker;

    iget-object v1, v1, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->f:Lcom/facebook/events/ui/date/TimePickerView;

    invoke-static {v0, v1}, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->b(Lcom/facebook/events/ui/date/DatePickerView;Lcom/facebook/events/ui/date/TimePickerView;)V

    .line 1818389
    iget-object v0, p0, LX/BmT;->a:Lcom/facebook/events/ui/date/StartAndEndTimePicker;

    iget-object v0, v0, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->d:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1818390
    :goto_0
    iget-object v0, p0, LX/BmT;->a:Lcom/facebook/events/ui/date/StartAndEndTimePicker;

    invoke-static {v0}, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->b(Lcom/facebook/events/ui/date/StartAndEndTimePicker;)V

    .line 1818391
    iget-object v0, p0, LX/BmT;->a:Lcom/facebook/events/ui/date/StartAndEndTimePicker;

    iget-object v0, v0, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->c:Lcom/facebook/events/ui/date/TimePickerView;

    iget-object v1, p0, LX/BmT;->a:Lcom/facebook/events/ui/date/StartAndEndTimePicker;

    invoke-virtual {v1}, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->getStartDate()Ljava/util/Date;

    move-result-object v1

    iget-object v2, p0, LX/BmT;->a:Lcom/facebook/events/ui/date/StartAndEndTimePicker;

    invoke-virtual {v2}, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->a(Lcom/facebook/events/ui/date/TimePickerView;Ljava/util/Date;Ljava/util/TimeZone;)V

    .line 1818392
    return-void

    .line 1818393
    :cond_0
    iget-object v0, p0, LX/BmT;->a:Lcom/facebook/events/ui/date/StartAndEndTimePicker;

    iget-object v0, v0, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->d:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method
