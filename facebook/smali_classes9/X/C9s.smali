.class public final LX/C9s;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLStory;

.field public final synthetic b:LX/C9t;


# direct methods
.method public constructor <init>(LX/C9t;Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 0

    .prologue
    .line 1854678
    iput-object p1, p0, LX/C9s;->b:LX/C9t;

    iput-object p2, p0, LX/C9s;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v0, 0x1

    const v1, -0x540262d8

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1854679
    iget-object v0, p0, LX/C9s;->b:LX/C9t;

    iget-object v0, v0, LX/C9t;->d:LX/9A1;

    iget-object v2, p0, LX/C9s;->a:Lcom/facebook/graphql/model/GraphQLStory;

    sget-object v3, LX/5Ro;->OFFLINE_POSTING_HEADER:LX/5Ro;

    invoke-virtual {v0, v2, v3}, LX/9A1;->b(Lcom/facebook/graphql/model/GraphQLStory;LX/5Ro;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1854680
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomRelativeLayout;

    .line 1854681
    instance-of v2, v0, LX/CAB;

    if-eqz v2, :cond_0

    .line 1854682
    check-cast v0, LX/CAB;

    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08003f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-wide/16 v4, 0xbb8

    .line 1854683
    iget-object v3, v0, LX/CAB;->a:Lcom/facebook/widget/NotificationTextSwitcher;

    invoke-virtual {v3, v2, v4, v5}, Lcom/facebook/widget/NotificationTextSwitcher;->a(Ljava/lang/CharSequence;J)V

    .line 1854684
    :cond_0
    const v0, 0x68f33915

    invoke-static {v6, v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
