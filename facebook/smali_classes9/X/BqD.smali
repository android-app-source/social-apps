.class public LX/BqD;
.super LX/Be9;
.source ""

# interfaces
.implements LX/BpF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Be9",
        "<",
        "LX/5kD;",
        "Ljava/lang/Void;",
        ">;",
        "LX/BpF;"
    }
.end annotation


# direct methods
.method public constructor <init>(LX/2kW;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/controller/connectioncontroller/common/ConnectionController",
            "<",
            "LX/5kD;",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1824429
    invoke-direct {p0, p1}, LX/Be9;-><init>(LX/2kW;)V

    .line 1824430
    return-void
.end method


# virtual methods
.method public final b()LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/5kD;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1824431
    invoke-virtual {p0}, LX/Be9;->size()I

    move-result v1

    .line 1824432
    if-nez v1, :cond_0

    .line 1824433
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1824434
    :goto_0
    return-object v0

    .line 1824435
    :cond_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1824436
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v1, :cond_1

    .line 1824437
    invoke-virtual {p0, v0}, LX/Be9;->a(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1824438
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1824439
    :cond_1
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method
