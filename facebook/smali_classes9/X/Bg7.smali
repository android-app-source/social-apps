.class public final LX/Bg7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:LX/Bh0;

.field public final synthetic b:Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

.field public final synthetic c:LX/97f;

.field public final synthetic d:LX/BgS;

.field public final synthetic e:LX/Bg8;


# direct methods
.method public constructor <init>(LX/Bg8;LX/Bh0;Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;LX/97f;LX/BgS;)V
    .locals 0

    .prologue
    .line 1807066
    iput-object p1, p0, LX/Bg7;->e:LX/Bg8;

    iput-object p2, p0, LX/Bg7;->a:LX/Bh0;

    iput-object p3, p0, LX/Bg7;->b:Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    iput-object p4, p0, LX/Bg7;->c:LX/97f;

    iput-object p5, p0, LX/Bg7;->d:LX/BgS;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    .line 1807067
    iget-object v0, p0, LX/Bg7;->a:LX/Bh0;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/Bg7;->a:LX/Bh0;

    iget-object v1, p0, LX/Bg7;->b:Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    .line 1807068
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;->ADD_VALUE:Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    invoke-virtual {v3, v1}, Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1807069
    iget-object v4, v0, LX/Bh0;->a:Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiTextFieldView;

    iget-object v5, v0, LX/Bh0;->b:LX/97f;

    iget-object p1, v0, LX/Bh0;->c:LX/BgS;

    .line 1807070
    invoke-virtual {v4}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiTextFieldView;->getLastTextFieldView()Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;

    move-result-object v0

    invoke-static {v0}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;

    .line 1807071
    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->hasFocus()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1807072
    invoke-static {v4, v5, p1}, LX/Bh5;->a(Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiTextFieldView;LX/97f;LX/BgS;)LX/97f;

    move-result-object v5

    .line 1807073
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1807074
    const-string v0, ""

    .line 1807075
    invoke-static {v5}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel;->a(LX/97f;)Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsFieldModel;

    move-result-object v1

    .line 1807076
    if-nez v1, :cond_4

    .line 1807077
    :goto_0
    move-object v0, v5

    .line 1807078
    invoke-interface {p1, v0}, LX/BgS;->a(Ljava/lang/Object;)V

    .line 1807079
    :cond_1
    invoke-virtual {v4}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiTextFieldView;->getLastTextFieldView()Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;

    move-result-object v0

    .line 1807080
    iget-object v1, v0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->a:Landroid/widget/EditText;

    move-object v0, v1

    .line 1807081
    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 1807082
    const/4 v3, 0x1

    .line 1807083
    :goto_1
    move v0, v3

    .line 1807084
    if-eqz v0, :cond_2

    .line 1807085
    :goto_2
    return v2

    .line 1807086
    :cond_2
    iget-object v0, p0, LX/Bg7;->c:LX/97f;

    iget-object v1, p0, LX/Bg7;->b:Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    invoke-static {v0, v1}, LX/BgV;->a(LX/97f;Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;)LX/97f;

    move-result-object v0

    .line 1807087
    iget-object v1, p0, LX/Bg7;->d:LX/BgS;

    invoke-interface {v1, v0}, LX/BgS;->a(Ljava/lang/Object;)V

    goto :goto_2

    :cond_3
    const/4 v3, 0x0

    goto :goto_1

    .line 1807088
    :cond_4
    new-instance v1, LX/97m;

    invoke-direct {v1}, LX/97m;-><init>()V

    .line 1807089
    iput-object v0, v1, LX/97m;->m:Ljava/lang/String;

    .line 1807090
    move-object v1, v1

    .line 1807091
    invoke-virtual {v1}, LX/97m;->a()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;

    move-result-object v1

    .line 1807092
    new-instance v3, LX/97l;

    invoke-direct {v3}, LX/97l;-><init>()V

    .line 1807093
    iput-object v1, v3, LX/97l;->a:Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel$NodeModel;

    .line 1807094
    move-object v1, v3

    .line 1807095
    invoke-virtual {v1}, LX/97l;->a()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel;

    move-result-object v1

    .line 1807096
    invoke-static {v5, v1}, LX/BgV;->a(LX/97f;Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$CrowdsourcedFieldModel$UserValuesModel$EdgesModel;)LX/97f;

    move-result-object v5

    goto :goto_0
.end method
