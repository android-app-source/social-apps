.class public LX/AeX;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final a:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<TK;",
            "Ljava/util/List",
            "<TE;>;>;"
        }
    .end annotation
.end field

.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<TK;>;"
        }
    .end annotation
.end field

.field private final c:LX/AeM;

.field public d:I


# direct methods
.method public constructor <init>(LX/AeM;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/facecastdisplay/liveevent/ListFactory",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    .line 1697812
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1697813
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/AeX;->a:Ljava/util/HashMap;

    .line 1697814
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/AeX;->b:Ljava/util/List;

    .line 1697815
    const/4 v0, -0x1

    iput v0, p0, LX/AeX;->d:I

    .line 1697816
    iput-object p1, p0, LX/AeX;->c:LX/AeM;

    .line 1697817
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)I
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)I"
        }
    .end annotation

    .prologue
    .line 1697818
    iget-object v0, p0, LX/AeX;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 1697819
    if-eqz v0, :cond_0

    .line 1697820
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 1697821
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TE;)V"
        }
    .end annotation

    .prologue
    .line 1697822
    iget-object v0, p0, LX/AeX;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1697823
    iget-object v0, p0, LX/AeX;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1697824
    :cond_0
    iget-object v0, p0, LX/AeX;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 1697825
    if-nez v0, :cond_1

    .line 1697826
    iget-object v0, p0, LX/AeX;->c:LX/AeM;

    .line 1697827
    sget-object v1, LX/AeN;->LIVE_COMMENT_EVENT:LX/AeN;

    if-ne p1, v1, :cond_2

    iget-object v1, v0, LX/AeM;->a:LX/Ac6;

    invoke-virtual {v1}, LX/Ac6;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1697828
    iget-object v1, v0, LX/AeM;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 1697829
    :goto_0
    move-object v0, v1

    .line 1697830
    iget-object v1, p0, LX/AeX;->a:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1697831
    :cond_1
    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1697832
    return-void

    :cond_2
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)",
            "Ljava/util/List",
            "<TE;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1697833
    iget-object v0, p0, LX/AeX;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method
