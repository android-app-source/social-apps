.class public LX/C17;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/feedplugins/attachments/poll/PollDisplayAndClickListenerBuilder",
        "<TE;>;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Lcom/facebook/intent/feed/IFeedIntentBuilder;

.field public final c:Lcom/facebook/content/SecureContextHelper;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/intent/feed/IFeedIntentBuilder;Lcom/facebook/content/SecureContextHelper;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1841927
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1841928
    iput-object p1, p0, LX/C17;->a:Landroid/content/Context;

    .line 1841929
    iput-object p3, p0, LX/C17;->c:Lcom/facebook/content/SecureContextHelper;

    .line 1841930
    iput-object p2, p0, LX/C17;->b:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    .line 1841931
    return-void
.end method

.method public static a(LX/0QB;)LX/C17;
    .locals 6

    .prologue
    .line 1841916
    const-class v1, LX/C17;

    monitor-enter v1

    .line 1841917
    :try_start_0
    sget-object v0, LX/C17;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1841918
    sput-object v2, LX/C17;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1841919
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1841920
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1841921
    new-instance p0, LX/C17;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/1nB;->a(LX/0QB;)LX/1nB;

    move-result-object v4

    check-cast v4, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v5

    check-cast v5, Lcom/facebook/content/SecureContextHelper;

    invoke-direct {p0, v3, v4, v5}, LX/C17;-><init>(Landroid/content/Context;Lcom/facebook/intent/feed/IFeedIntentBuilder;Lcom/facebook/content/SecureContextHelper;)V

    .line 1841922
    move-object v0, p0

    .line 1841923
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1841924
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C17;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1841925
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1841926
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLQuestionOption;Ljava/lang/String;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 1841914
    new-instance v0, LX/C16;

    invoke-direct {v0, p0, p1, p2}, LX/C16;-><init>(LX/C17;Lcom/facebook/graphql/model/GraphQLQuestionOption;Ljava/lang/String;)V

    return-object v0
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1PW;)Z
    .locals 1

    .prologue
    .line 1841915
    const/4 v0, 0x0

    return v0
.end method
