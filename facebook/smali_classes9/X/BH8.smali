.class public final LX/BH8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/BH7;


# instance fields
.field public final synthetic a:Lcom/facebook/photos/simplepicker/SimplePickerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/simplepicker/SimplePickerFragment;)V
    .locals 0

    .prologue
    .line 1767634
    iput-object p1, p0, LX/BH8;->a:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/photos/base/media/PhotoItem;Z)V
    .locals 3

    .prologue
    .line 1767635
    if-eqz p2, :cond_1

    .line 1767636
    iget-object v0, p0, LX/BH8;->a:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    iget-boolean v0, v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->ap:Z

    if-eqz v0, :cond_0

    .line 1767637
    iget-object v0, p0, LX/BH8;->a:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    iget-object v0, v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->z:LX/BGe;

    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->l()LX/4gF;

    move-result-object v1

    invoke-virtual {v1}, LX/4gF;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/BGe;->b(Ljava/lang/String;)V

    .line 1767638
    :cond_0
    iget-object v0, p0, LX/BH8;->a:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    iget-object v0, v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->c:LX/BHJ;

    invoke-virtual {v0, p1}, LX/BHJ;->a(Lcom/facebook/ipc/media/MediaItem;)Z

    .line 1767639
    :cond_1
    iget-object v0, p0, LX/BH8;->a:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    iget-object v0, v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->a:LX/BHj;

    invoke-virtual {v0}, LX/BHj;->b()V

    .line 1767640
    iget-object v0, p0, LX/BH8;->a:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v1

    .line 1767641
    const-string v0, "GALLERY_FRAGMENT"

    invoke-virtual {v1, v0}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;

    .line 1767642
    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/0hH;->a(Landroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v1

    const/16 v2, 0x2002

    invoke-virtual {v1, v2}, LX/0hH;->a(I)LX/0hH;

    move-result-object v1

    invoke-virtual {v1}, LX/0hH;->b()I

    .line 1767643
    if-eqz v0, :cond_2

    .line 1767644
    iget-boolean v1, v0, Lcom/facebook/photos/taggablegallery/TaggablePhotoGalleryFragment;->v:Z

    move v0, v1

    .line 1767645
    if-eqz v0, :cond_2

    .line 1767646
    iget-object v0, p0, LX/BH8;->a:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    const/4 v1, 0x1

    .line 1767647
    iput-boolean v1, v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->V:Z

    .line 1767648
    iget-object v0, p0, LX/BH8;->a:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->a(Ljava/lang/String;)V

    .line 1767649
    :cond_2
    iget-object v0, p0, LX/BH8;->a:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    .line 1767650
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->a(Lcom/facebook/photos/simplepicker/SimplePickerFragment;I)V

    .line 1767651
    return-void
.end method
