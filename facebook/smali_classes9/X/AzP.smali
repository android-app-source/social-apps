.class public final enum LX/AzP;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/AzP;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/AzP;

.field public static final enum CANCEL_EDITOR:LX/AzP;

.field public static final enum CANCEL_PICKER:LX/AzP;

.field public static final enum CLICK_MORE:LX/AzP;

.field public static final enum CLICK_PHOTO_PREVIEW:LX/AzP;

.field public static final enum CLICK_PICKER_PHOTO:LX/AzP;

.field public static final enum CLICK_PROMPT_CONTAINER:LX/AzP;

.field public static final enum CONFIRM_UPLOAD:LX/AzP;

.field public static final enum DISMISS:LX/AzP;

.field public static final enum DOWNLOAD_SUCCESS:LX/AzP;

.field public static final enum ERROR_FAILED_DOWNLOAD:LX/AzP;

.field public static final enum HIDE:LX/AzP;

.field public static final enum IMPRESSION:LX/AzP;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1732500
    new-instance v0, LX/AzP;

    const-string v1, "IMPRESSION"

    invoke-direct {v0, v1, v3}, LX/AzP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AzP;->IMPRESSION:LX/AzP;

    .line 1732501
    new-instance v0, LX/AzP;

    const-string v1, "HIDE"

    invoke-direct {v0, v1, v4}, LX/AzP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AzP;->HIDE:LX/AzP;

    .line 1732502
    new-instance v0, LX/AzP;

    const-string v1, "DISMISS"

    invoke-direct {v0, v1, v5}, LX/AzP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AzP;->DISMISS:LX/AzP;

    .line 1732503
    new-instance v0, LX/AzP;

    const-string v1, "CLICK_PHOTO_PREVIEW"

    invoke-direct {v0, v1, v6}, LX/AzP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AzP;->CLICK_PHOTO_PREVIEW:LX/AzP;

    .line 1732504
    new-instance v0, LX/AzP;

    const-string v1, "CLICK_MORE"

    invoke-direct {v0, v1, v7}, LX/AzP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AzP;->CLICK_MORE:LX/AzP;

    .line 1732505
    new-instance v0, LX/AzP;

    const-string v1, "CLICK_PROMPT_CONTAINER"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/AzP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AzP;->CLICK_PROMPT_CONTAINER:LX/AzP;

    .line 1732506
    new-instance v0, LX/AzP;

    const-string v1, "CLICK_PICKER_PHOTO"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/AzP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AzP;->CLICK_PICKER_PHOTO:LX/AzP;

    .line 1732507
    new-instance v0, LX/AzP;

    const-string v1, "CANCEL_PICKER"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/AzP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AzP;->CANCEL_PICKER:LX/AzP;

    .line 1732508
    new-instance v0, LX/AzP;

    const-string v1, "CANCEL_EDITOR"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/AzP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AzP;->CANCEL_EDITOR:LX/AzP;

    .line 1732509
    new-instance v0, LX/AzP;

    const-string v1, "CONFIRM_UPLOAD"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/AzP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AzP;->CONFIRM_UPLOAD:LX/AzP;

    .line 1732510
    new-instance v0, LX/AzP;

    const-string v1, "DOWNLOAD_SUCCESS"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/AzP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AzP;->DOWNLOAD_SUCCESS:LX/AzP;

    .line 1732511
    new-instance v0, LX/AzP;

    const-string v1, "ERROR_FAILED_DOWNLOAD"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LX/AzP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AzP;->ERROR_FAILED_DOWNLOAD:LX/AzP;

    .line 1732512
    const/16 v0, 0xc

    new-array v0, v0, [LX/AzP;

    sget-object v1, LX/AzP;->IMPRESSION:LX/AzP;

    aput-object v1, v0, v3

    sget-object v1, LX/AzP;->HIDE:LX/AzP;

    aput-object v1, v0, v4

    sget-object v1, LX/AzP;->DISMISS:LX/AzP;

    aput-object v1, v0, v5

    sget-object v1, LX/AzP;->CLICK_PHOTO_PREVIEW:LX/AzP;

    aput-object v1, v0, v6

    sget-object v1, LX/AzP;->CLICK_MORE:LX/AzP;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/AzP;->CLICK_PROMPT_CONTAINER:LX/AzP;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/AzP;->CLICK_PICKER_PHOTO:LX/AzP;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/AzP;->CANCEL_PICKER:LX/AzP;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/AzP;->CANCEL_EDITOR:LX/AzP;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/AzP;->CONFIRM_UPLOAD:LX/AzP;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/AzP;->DOWNLOAD_SUCCESS:LX/AzP;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/AzP;->ERROR_FAILED_DOWNLOAD:LX/AzP;

    aput-object v2, v0, v1

    sput-object v0, LX/AzP;->$VALUES:[LX/AzP;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1732513
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/AzP;
    .locals 1

    .prologue
    .line 1732514
    const-class v0, LX/AzP;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/AzP;

    return-object v0
.end method

.method public static values()[LX/AzP;
    .locals 1

    .prologue
    .line 1732515
    sget-object v0, LX/AzP;->$VALUES:[LX/AzP;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/AzP;

    return-object v0
.end method
