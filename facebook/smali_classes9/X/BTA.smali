.class public LX/BTA;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/BT9;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1786968
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 1786969
    return-void
.end method


# virtual methods
.method public final a(Landroid/net/Uri;Landroid/net/Uri;LX/7Sv;Ljava/lang/String;Landroid/view/ViewStub;Lcom/facebook/photos/creativeediting/model/VideoTrimParams;Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;Lcom/facebook/video/creativeediting/VideoEditGalleryTrimmerFilmstripView;Ljava/lang/String;Landroid/view/ViewStub;Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;LX/BSr;LX/BSr;)LX/BT9;
    .locals 25

    .prologue
    .line 1786970
    new-instance v1, LX/BT9;

    const-class v2, Landroid/content/Context;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    const-class v3, LX/BTT;

    move-object/from16 v0, p0

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/BTT;

    invoke-static/range {p0 .. p0}, LX/BTl;->a(LX/0QB;)LX/BTl;

    move-result-object v4

    check-cast v4, LX/BTl;

    const-class v5, LX/BTY;

    move-object/from16 v0, p0

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/BTY;

    const-class v6, LX/BTO;

    move-object/from16 v0, p0

    invoke-interface {v0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/BTO;

    const-class v7, LX/BTQ;

    move-object/from16 v0, p0

    invoke-interface {v0, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/BTQ;

    invoke-static/range {p0 .. p0}, LX/BTG;->a(LX/0QB;)LX/BTG;

    move-result-object v8

    check-cast v8, LX/BTG;

    invoke-static/range {p0 .. p0}, LX/8tH;->a(LX/0QB;)LX/8tH;

    move-result-object v9

    check-cast v9, LX/8tH;

    invoke-static/range {p0 .. p0}, LX/8tK;->a(LX/0QB;)LX/8tK;

    move-result-object v10

    check-cast v10, LX/8tK;

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v11

    check-cast v11, LX/0ad;

    move-object/from16 v12, p1

    move-object/from16 v13, p2

    move-object/from16 v14, p3

    move-object/from16 v15, p4

    move-object/from16 v16, p5

    move-object/from16 v17, p6

    move-object/from16 v18, p7

    move-object/from16 v19, p8

    move-object/from16 v20, p9

    move-object/from16 v21, p10

    move-object/from16 v22, p11

    move-object/from16 v23, p12

    move-object/from16 v24, p13

    invoke-direct/range {v1 .. v24}, LX/BT9;-><init>(Landroid/content/Context;LX/BTT;LX/BTl;LX/BTY;LX/BTO;LX/BTQ;LX/BTG;LX/8tH;LX/8tK;LX/0ad;Landroid/net/Uri;Landroid/net/Uri;LX/7Sv;Ljava/lang/String;Landroid/view/ViewStub;Lcom/facebook/photos/creativeediting/model/VideoTrimParams;Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;Lcom/facebook/video/creativeediting/VideoEditGalleryTrimmerFilmstripView;Ljava/lang/String;Landroid/view/ViewStub;Lcom/facebook/video/creativeediting/VideoEditGalleryVideoPreviewView;LX/BSr;LX/BSr;)V

    .line 1786971
    return-object v1
.end method
