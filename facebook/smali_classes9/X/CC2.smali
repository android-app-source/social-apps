.class public LX/CC2;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0tX;

.field private final b:LX/1Ck;


# direct methods
.method private constructor <init>(LX/0tX;LX/1Ck;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1856957
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1856958
    iput-object p1, p0, LX/CC2;->a:LX/0tX;

    .line 1856959
    iput-object p2, p0, LX/CC2;->b:LX/1Ck;

    .line 1856960
    return-void
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 1857085
    const-string v0, "#"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 1857086
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x6

    if-ne v1, v2, :cond_0

    .line 1857087
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "FF"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1857088
    :cond_0
    return-object v0
.end method

.method public static b(LX/0QB;)LX/CC2;
    .locals 3

    .prologue
    .line 1857083
    new-instance v2, LX/CC2;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v1

    check-cast v1, LX/1Ck;

    invoke-direct {v2, v0, v1}, LX/CC2;-><init>(LX/0tX;LX/1Ck;)V

    .line 1857084
    return-object v2
.end method

.method private static b(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;)Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel;
    .locals 12

    .prologue
    .line 1857000
    new-instance v1, LX/CCJ;

    invoke-direct {v1}, LX/CCJ;-><init>()V

    new-instance v0, LX/CCK;

    invoke-direct {v0}, LX/CCK;-><init>()V

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v2

    .line 1857001
    iput-object v2, v0, LX/CCK;->a:Ljava/lang/String;

    .line 1857002
    move-object v0, v0

    .line 1857003
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->ba()Ljava/lang/String;

    move-result-object v2

    .line 1857004
    iput-object v2, v0, LX/CCK;->c:Ljava/lang/String;

    .line 1857005
    move-object v2, v0

    .line 1857006
    sget-object v0, LX/87X;->a:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    invoke-static {p1, v0}, LX/87X;->a(Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 1857007
    :goto_0
    iput-object v0, v2, LX/CCK;->b:Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel;

    .line 1857008
    move-object v0, v2

    .line 1857009
    const/4 v7, 0x1

    const/4 v10, 0x0

    const/4 v5, 0x0

    .line 1857010
    new-instance v3, LX/186;

    const/16 v4, 0x80

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1857011
    iget-object v4, v0, LX/CCK;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1857012
    iget-object v6, v0, LX/CCK;->b:Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel;

    invoke-static {v3, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1857013
    iget-object v8, v0, LX/CCK;->c:Ljava/lang/String;

    invoke-virtual {v3, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 1857014
    const/4 v9, 0x3

    invoke-virtual {v3, v9}, LX/186;->c(I)V

    .line 1857015
    invoke-virtual {v3, v10, v4}, LX/186;->b(II)V

    .line 1857016
    invoke-virtual {v3, v7, v6}, LX/186;->b(II)V

    .line 1857017
    const/4 v4, 0x2

    invoke-virtual {v3, v4, v8}, LX/186;->b(II)V

    .line 1857018
    invoke-virtual {v3}, LX/186;->d()I

    move-result v4

    .line 1857019
    invoke-virtual {v3, v4}, LX/186;->d(I)V

    .line 1857020
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1857021
    invoke-virtual {v4, v10}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1857022
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1857023
    new-instance v4, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel;

    invoke-direct {v4, v3}, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel;-><init>(LX/15i;)V

    .line 1857024
    move-object v0, v4

    .line 1857025
    iput-object v0, v1, LX/CCJ;->a:Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel;

    .line 1857026
    move-object v0, v1

    .line 1857027
    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 1857028
    new-instance v3, LX/186;

    const/16 v4, 0x80

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1857029
    iget-object v4, v0, LX/CCJ;->a:Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel;

    invoke-static {v3, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 1857030
    invoke-virtual {v3, v7}, LX/186;->c(I)V

    .line 1857031
    invoke-virtual {v3, v6, v4}, LX/186;->b(II)V

    .line 1857032
    invoke-virtual {v3}, LX/186;->d()I

    move-result v4

    .line 1857033
    invoke-virtual {v3, v4}, LX/186;->d(I)V

    .line 1857034
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1857035
    invoke-virtual {v4, v6}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1857036
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1857037
    new-instance v4, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel;

    invoke-direct {v4, v3}, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel;-><init>(LX/15i;)V

    .line 1857038
    move-object v0, v4

    .line 1857039
    return-object v0

    .line 1857040
    :cond_0
    new-instance v3, LX/CCN;

    invoke-direct {v3}, LX/CCN;-><init>()V

    .line 1857041
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getBackgroundColor()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/CC2;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1857042
    iput-object v4, v3, LX/CCN;->a:Ljava/lang/String;

    .line 1857043
    move-object v4, v3

    .line 1857044
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getColor()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/CC2;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1857045
    iput-object v5, v4, LX/CCN;->f:Ljava/lang/String;

    .line 1857046
    move-object v4, v4

    .line 1857047
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getFontWeight()LX/5RY;

    move-result-object v5

    invoke-virtual {v5}, LX/5RY;->name()Ljava/lang/String;

    move-result-object v5

    .line 1857048
    iput-object v5, v4, LX/CCN;->h:Ljava/lang/String;

    .line 1857049
    move-object v4, v4

    .line 1857050
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getTextAlign()LX/5RS;

    move-result-object v5

    invoke-virtual {v5}, LX/5RS;->name()Ljava/lang/String;

    move-result-object v5

    .line 1857051
    iput-object v5, v4, LX/CCN;->j:Ljava/lang/String;

    .line 1857052
    move-object v4, v4

    .line 1857053
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getPresetId()Ljava/lang/String;

    move-result-object v5

    .line 1857054
    iput-object v5, v4, LX/CCN;->i:Ljava/lang/String;

    .line 1857055
    move-object v4, v4

    .line 1857056
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getGradientEndColor()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/CC2;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1857057
    iput-object v5, v4, LX/CCN;->b:Ljava/lang/String;

    .line 1857058
    move-object v4, v4

    .line 1857059
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getGradientDirection()Ljava/lang/String;

    move-result-object v5

    .line 1857060
    iput-object v5, v4, LX/CCN;->c:Ljava/lang/String;

    .line 1857061
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getBackgroundImageName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 1857062
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getBackgroundImageName()Ljava/lang/String;

    move-result-object v4

    .line 1857063
    iput-object v4, v3, LX/CCN;->e:Ljava/lang/String;

    .line 1857064
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getBackgroundImageUrl()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 1857065
    new-instance v4, LX/CCM;

    invoke-direct {v4}, LX/CCM;-><init>()V

    invoke-virtual {p1}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getBackgroundImageUrl()Ljava/lang/String;

    move-result-object v5

    .line 1857066
    iput-object v5, v4, LX/CCM;->a:Ljava/lang/String;

    .line 1857067
    move-object v4, v4

    .line 1857068
    const/4 v10, 0x1

    const/4 v9, 0x0

    const/4 v8, 0x0

    .line 1857069
    new-instance v6, LX/186;

    const/16 v7, 0x80

    invoke-direct {v6, v7}, LX/186;-><init>(I)V

    .line 1857070
    iget-object v7, v4, LX/CCM;->a:Ljava/lang/String;

    invoke-virtual {v6, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1857071
    invoke-virtual {v6, v10}, LX/186;->c(I)V

    .line 1857072
    invoke-virtual {v6, v9, v7}, LX/186;->b(II)V

    .line 1857073
    invoke-virtual {v6}, LX/186;->d()I

    move-result v7

    .line 1857074
    invoke-virtual {v6, v7}, LX/186;->d(I)V

    .line 1857075
    invoke-virtual {v6}, LX/186;->e()[B

    move-result-object v6

    invoke-static {v6}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v7

    .line 1857076
    invoke-virtual {v7, v9}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1857077
    new-instance v6, LX/15i;

    move-object v9, v8

    move-object v11, v8

    invoke-direct/range {v6 .. v11}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1857078
    new-instance v7, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel$BackgroundImageModel;

    invoke-direct {v7, v6}, Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel$BackgroundImageModel;-><init>(LX/15i;)V

    .line 1857079
    move-object v4, v7

    .line 1857080
    iput-object v4, v3, LX/CCN;->d:Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel$BackgroundImageModel;

    .line 1857081
    :cond_2
    invoke-virtual {v3}, LX/CCN;->a()Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel$StoryModel$TextFormatMetadataModel;

    move-result-object v3

    move-object v0, v3

    .line 1857082
    goto/16 :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;)V
    .locals 4

    .prologue
    .line 1856961
    invoke-static {p1}, LX/185;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    .line 1856962
    if-nez v0, :cond_0

    .line 1856963
    :goto_0
    return-void

    .line 1856964
    :cond_0
    new-instance v1, LX/4JY;

    invoke-direct {v1}, LX/4JY;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v0

    .line 1856965
    const-string v2, "actor_id"

    invoke-virtual {v1, v2, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1856966
    move-object v0, v1

    .line 1856967
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v1

    .line 1856968
    const-string v2, "story_id"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1856969
    move-object v1, v0

    .line 1856970
    sget-object v0, LX/87X;->a:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    invoke-static {p2, v0}, LX/87X;->a(Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "0"

    .line 1856971
    :goto_1
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1856972
    const-string v2, "text_format_preset_id"

    invoke-virtual {v1, v2, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1856973
    :goto_2
    new-instance v0, LX/CCH;

    invoke-direct {v0}, LX/CCH;-><init>()V

    move-object v0, v0

    .line 1856974
    const-string v2, "input"

    invoke-virtual {v0, v2, v1}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1856975
    invoke-static {v0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    invoke-static {p1, p2}, LX/CC2;->b(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;)Lcom/facebook/feedplugins/richtextpicker/graphql/StoryMutationsModels$StoryEditMutationModel;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/399;->a(LX/0jT;)LX/399;

    move-result-object v0

    .line 1856976
    iget-object v1, p0, LX/CC2;->b:LX/1Ck;

    const-string v2, "edit_rich_text"

    iget-object v3, p0, LX/CC2;->a:LX/0tX;

    invoke-virtual {v3, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v3, LX/CC1;

    invoke-direct {v3, p0}, LX/CC1;-><init>(LX/CC2;)V

    invoke-virtual {v1, v2, v0, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto :goto_0

    .line 1856977
    :cond_1
    invoke-virtual {p2}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getPresetId()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 1856978
    :cond_2
    new-instance v0, LX/4IV;

    invoke-direct {v0}, LX/4IV;-><init>()V

    invoke-virtual {p2}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getBackgroundColor()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/CC2;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1856979
    const-string v3, "background_color"

    invoke-virtual {v0, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1856980
    move-object v0, v0

    .line 1856981
    invoke-virtual {p2}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getColor()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/CC2;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1856982
    const-string v3, "color"

    invoke-virtual {v0, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1856983
    move-object v0, v0

    .line 1856984
    invoke-virtual {p2}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getFontWeight()LX/5RY;

    move-result-object v2

    invoke-virtual {v2}, LX/5RY;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/4IV;->f(Ljava/lang/String;)LX/4IV;

    move-result-object v0

    invoke-virtual {p2}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getTextAlign()LX/5RS;

    move-result-object v2

    invoke-virtual {v2}, LX/5RS;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/4IV;->e(Ljava/lang/String;)LX/4IV;

    move-result-object v0

    invoke-virtual {p2}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getGradientEndColor()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/CC2;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1856985
    const-string v3, "background_gradient_color"

    invoke-virtual {v0, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1856986
    move-object v0, v0

    .line 1856987
    invoke-virtual {p2}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getGradientDirection()Ljava/lang/String;

    move-result-object v2

    .line 1856988
    const-string v3, "background_gradient_direction"

    invoke-virtual {v0, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1856989
    move-object v0, v0

    .line 1856990
    invoke-virtual {p2}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getBackgroundImageName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 1856991
    invoke-virtual {p2}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getBackgroundImageName()Ljava/lang/String;

    move-result-object v2

    .line 1856992
    const-string v3, "background_image_name"

    invoke-virtual {v0, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1856993
    :cond_3
    invoke-virtual {p2}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getFontWeight()LX/5RY;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 1856994
    invoke-virtual {p2}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getFontWeight()LX/5RY;

    move-result-object v2

    invoke-virtual {v2}, LX/5RY;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/4IV;->f(Ljava/lang/String;)LX/4IV;

    .line 1856995
    :cond_4
    invoke-virtual {p2}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getTextAlign()LX/5RS;

    move-result-object v2

    if-eqz v2, :cond_5

    .line 1856996
    invoke-virtual {p2}, Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;->getTextAlign()LX/5RS;

    move-result-object v2

    invoke-virtual {v2}, LX/5RS;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/4IV;->e(Ljava/lang/String;)LX/4IV;

    .line 1856997
    :cond_5
    move-object v0, v0

    .line 1856998
    const-string v2, "text_format_metadata"

    invoke-virtual {v1, v2, v0}, LX/0gS;->a(Ljava/lang/String;LX/0gS;)V

    .line 1856999
    goto/16 :goto_2
.end method
