.class public final LX/Bd1;
.super LX/BcO;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/BcO",
        "<",
        "LX/Bd3;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public b:Z

.field public c:I

.field public d:I

.field public e:LX/1X1;

.field public f:LX/BcQ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/BcQ",
            "<",
            "LX/BdG;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/2kW;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1803144
    invoke-static {}, LX/Bd3;->d()LX/Bd3;

    move-result-object v0

    invoke-direct {p0, v0}, LX/BcO;-><init>(LX/BcS;)V

    .line 1803145
    const/4 v0, 0x0

    iput v0, p0, LX/Bd1;->c:I

    .line 1803146
    sget-object v0, LX/Bd5;->a:LX/1X1;

    iput-object v0, p0, LX/Bd1;->e:LX/1X1;

    .line 1803147
    return-void
.end method


# virtual methods
.method public final a(Z)LX/BcO;
    .locals 2

    .prologue
    .line 1803148
    invoke-super {p0, p1}, LX/BcO;->a(Z)LX/BcO;

    move-result-object v0

    check-cast v0, LX/Bd1;

    .line 1803149
    if-nez p1, :cond_0

    .line 1803150
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/Bd1;->b:Z

    .line 1803151
    :cond_0
    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1803152
    if-ne p0, p1, :cond_1

    .line 1803153
    :cond_0
    :goto_0
    return v0

    .line 1803154
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1803155
    goto :goto_0

    .line 1803156
    :cond_3
    check-cast p1, LX/Bd1;

    .line 1803157
    iget-boolean v2, p0, LX/Bd1;->b:Z

    iget-boolean v3, p1, LX/Bd1;->b:Z

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 1803158
    goto :goto_0

    .line 1803159
    :cond_4
    iget v2, p0, LX/Bd1;->c:I

    iget v3, p1, LX/Bd1;->c:I

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 1803160
    goto :goto_0

    .line 1803161
    :cond_5
    iget v2, p0, LX/Bd1;->d:I

    iget v3, p1, LX/Bd1;->d:I

    if-eq v2, v3, :cond_6

    move v0, v1

    .line 1803162
    goto :goto_0

    .line 1803163
    :cond_6
    iget-object v2, p0, LX/Bd1;->e:LX/1X1;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/Bd1;->e:LX/1X1;

    iget-object v3, p1, LX/Bd1;->e:LX/1X1;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1803164
    goto :goto_0

    .line 1803165
    :cond_8
    iget-object v2, p1, LX/Bd1;->e:LX/1X1;

    if-nez v2, :cond_7

    .line 1803166
    :cond_9
    iget-object v2, p0, LX/Bd1;->f:LX/BcQ;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/Bd1;->f:LX/BcQ;

    iget-object v3, p1, LX/Bd1;->f:LX/BcQ;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 1803167
    goto :goto_0

    .line 1803168
    :cond_b
    iget-object v2, p1, LX/Bd1;->f:LX/BcQ;

    if-nez v2, :cond_a

    .line 1803169
    :cond_c
    iget-object v2, p0, LX/Bd1;->g:LX/2kW;

    if-eqz v2, :cond_d

    iget-object v2, p0, LX/Bd1;->g:LX/2kW;

    iget-object v3, p1, LX/Bd1;->g:LX/2kW;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1803170
    goto :goto_0

    .line 1803171
    :cond_d
    iget-object v2, p1, LX/Bd1;->g:LX/2kW;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
