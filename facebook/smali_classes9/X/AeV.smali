.class public LX/AeV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/AeJ;


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:Ljava/lang/Object;

.field private static final c:Ljava/lang/String;


# instance fields
.field private A:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private B:LX/Ag1;

.field private C:Z

.field private D:Z

.field public E:Z

.field public F:Z

.field private G:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/AeZ;",
            ">;"
        }
    .end annotation
.end field

.field public H:LX/AeZ;

.field private I:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/AfX;",
            ">;"
        }
    .end annotation
.end field

.field private J:LX/AfX;

.field private K:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/Afu;",
            ">;"
        }
    .end annotation
.end field

.field private L:LX/Afu;

.field public M:LX/AeO;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public N:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final d:LX/Ac6;

.field private final e:LX/1b4;

.field private final f:LX/03V;

.field public final g:Ljava/util/concurrent/ScheduledExecutorService;

.field private final h:LX/0Sh;

.field public final i:Landroid/os/Handler;

.field private final j:LX/0SG;

.field private final k:LX/AVT;

.field private final l:LX/3Hd;

.field private final m:LX/AaZ;

.field private final n:LX/Af5;

.field private final o:LX/Ag5;

.field private final p:LX/Afh;

.field private final q:LX/Aet;

.field private final r:LX/Afq;

.field public final s:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/AeQ;",
            ">;"
        }
    .end annotation
.end field

.field private final t:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final u:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final v:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final w:LX/AeX;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/AeX",
            "<",
            "LX/AeN;",
            "LX/AeO;",
            ">;"
        }
    .end annotation
.end field

.field public volatile x:LX/AcX;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public y:Ljava/util/concurrent/Future;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public z:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1697760
    const-class v0, LX/AeV;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/AeV;->a:Ljava/lang/String;

    .line 1697761
    const/4 v0, 0x0

    sput-object v0, LX/AeV;->b:Ljava/lang/Object;

    .line 1697762
    const-class v0, LX/AeV;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/AeV;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Sh;LX/0SG;LX/AVT;LX/Ag1;LX/3Hd;LX/1b4;LX/AaZ;LX/Ac6;LX/03V;LX/Aet;LX/Af5;LX/AeM;LX/Afh;LX/Ag5;LX/Afq;LX/0Or;LX/0Or;LX/0Or;Ljava/util/concurrent/ScheduledExecutorService;)V
    .locals 3
    .param p19    # Ljava/util/concurrent/ScheduledExecutorService;
        .annotation runtime Lcom/facebook/common/executors/SingleThreadedExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/0SG;",
            "LX/AVT;",
            "LX/Ag1;",
            "LX/3Hd;",
            "LX/1b4;",
            "LX/AaZ;",
            "LX/Ac6;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/Aet;",
            "LX/Af5;",
            "LX/AeM;",
            "LX/Afh;",
            "LX/Ag5;",
            "LX/Afq;",
            "LX/0Or",
            "<",
            "LX/AeZ;",
            ">;",
            "LX/0Or",
            "<",
            "LX/AfX;",
            ">;",
            "LX/0Or",
            "<",
            "LX/Afu;",
            ">;",
            "Ljava/util/concurrent/ScheduledExecutorService;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1697733
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1697734
    iput-object p1, p0, LX/AeV;->h:LX/0Sh;

    .line 1697735
    iput-object p2, p0, LX/AeV;->j:LX/0SG;

    .line 1697736
    iput-object p3, p0, LX/AeV;->k:LX/AVT;

    .line 1697737
    iput-object p4, p0, LX/AeV;->B:LX/Ag1;

    .line 1697738
    iput-object p5, p0, LX/AeV;->l:LX/3Hd;

    .line 1697739
    iput-object p6, p0, LX/AeV;->e:LX/1b4;

    .line 1697740
    iput-object p7, p0, LX/AeV;->m:LX/AaZ;

    .line 1697741
    iput-object p8, p0, LX/AeV;->d:LX/Ac6;

    .line 1697742
    iput-object p9, p0, LX/AeV;->f:LX/03V;

    .line 1697743
    iput-object p10, p0, LX/AeV;->q:LX/Aet;

    .line 1697744
    iput-object p11, p0, LX/AeV;->n:LX/Af5;

    .line 1697745
    move-object/from16 v0, p13

    iput-object v0, p0, LX/AeV;->p:LX/Afh;

    .line 1697746
    move-object/from16 v0, p14

    iput-object v0, p0, LX/AeV;->o:LX/Ag5;

    .line 1697747
    move-object/from16 v0, p15

    iput-object v0, p0, LX/AeV;->r:LX/Afq;

    .line 1697748
    move-object/from16 v0, p16

    iput-object v0, p0, LX/AeV;->G:LX/0Or;

    .line 1697749
    move-object/from16 v0, p17

    iput-object v0, p0, LX/AeV;->I:LX/0Or;

    .line 1697750
    move-object/from16 v0, p18

    iput-object v0, p0, LX/AeV;->K:LX/0Or;

    .line 1697751
    move-object/from16 v0, p19

    iput-object v0, p0, LX/AeV;->g:Ljava/util/concurrent/ScheduledExecutorService;

    .line 1697752
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    invoke-static {v1}, Ljava/util/Collections;->synchronizedSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v1

    iput-object v1, p0, LX/AeV;->v:Ljava/util/Set;

    .line 1697753
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    invoke-static {v1}, Ljava/util/Collections;->synchronizedSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v1

    iput-object v1, p0, LX/AeV;->t:Ljava/util/Set;

    .line 1697754
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v1}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, LX/AeV;->s:Ljava/util/List;

    .line 1697755
    new-instance v1, LX/AeX;

    invoke-direct {v1, p12}, LX/AeX;-><init>(LX/AeM;)V

    iput-object v1, p0, LX/AeV;->w:LX/AeX;

    .line 1697756
    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, LX/AeV;->i:Landroid/os/Handler;

    .line 1697757
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    invoke-static {v1}, Ljava/util/Collections;->synchronizedSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v1

    iput-object v1, p0, LX/AeV;->u:Ljava/util/Set;

    .line 1697758
    const/4 v1, 0x0

    iput-boolean v1, p0, LX/AeV;->C:Z

    .line 1697759
    return-void
.end method

.method public static b(LX/0QB;)LX/AeV;
    .locals 22

    .prologue
    .line 1697731
    new-instance v2, LX/AeV;

    invoke-static/range {p0 .. p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v3

    check-cast v3, LX/0Sh;

    invoke-static/range {p0 .. p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-static/range {p0 .. p0}, LX/AVT;->a(LX/0QB;)LX/AVT;

    move-result-object v5

    check-cast v5, LX/AVT;

    invoke-static/range {p0 .. p0}, LX/Ag1;->a(LX/0QB;)LX/Ag1;

    move-result-object v6

    check-cast v6, LX/Ag1;

    invoke-static/range {p0 .. p0}, LX/3Hd;->a(LX/0QB;)LX/3Hd;

    move-result-object v7

    check-cast v7, LX/3Hd;

    invoke-static/range {p0 .. p0}, LX/1b4;->a(LX/0QB;)LX/1b4;

    move-result-object v8

    check-cast v8, LX/1b4;

    invoke-static/range {p0 .. p0}, LX/AaZ;->a(LX/0QB;)LX/AaZ;

    move-result-object v9

    check-cast v9, LX/AaZ;

    invoke-static/range {p0 .. p0}, LX/Ac6;->a(LX/0QB;)LX/Ac6;

    move-result-object v10

    check-cast v10, LX/Ac6;

    invoke-static/range {p0 .. p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v11

    check-cast v11, LX/03V;

    const-class v12, LX/Aet;

    move-object/from16 v0, p0

    invoke-interface {v0, v12}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v12

    check-cast v12, LX/Aet;

    const-class v13, LX/Af5;

    move-object/from16 v0, p0

    invoke-interface {v0, v13}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v13

    check-cast v13, LX/Af5;

    invoke-static/range {p0 .. p0}, LX/AeM;->a(LX/0QB;)LX/AeM;

    move-result-object v14

    check-cast v14, LX/AeM;

    const-class v15, LX/Afh;

    move-object/from16 v0, p0

    invoke-interface {v0, v15}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v15

    check-cast v15, LX/Afh;

    const-class v16, LX/Ag5;

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v16

    check-cast v16, LX/Ag5;

    const-class v17, LX/Afq;

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v17

    check-cast v17, LX/Afq;

    const/16 v18, 0x1c07

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v18

    const/16 v19, 0x1c11

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v19

    const/16 v20, 0x1c12

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v20

    invoke-static/range {p0 .. p0}, LX/0UA;->a(LX/0QB;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v21

    check-cast v21, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-direct/range {v2 .. v21}, LX/AeV;-><init>(LX/0Sh;LX/0SG;LX/AVT;LX/Ag1;LX/3Hd;LX/1b4;LX/AaZ;LX/Ac6;LX/03V;LX/Aet;LX/Af5;LX/AeM;LX/Afh;LX/Ag5;LX/Afq;LX/0Or;LX/0Or;LX/0Or;Ljava/util/concurrent/ScheduledExecutorService;)V

    .line 1697732
    return-object v2
.end method

.method public static b(LX/AeV;LX/AeO;)Z
    .locals 2

    .prologue
    .line 1697730
    instance-of v0, p1, LX/AeP;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/AeV;->v:Ljava/util/Set;

    check-cast p1, LX/AeP;

    iget-object v1, p1, LX/AeP;->a:LX/AcC;

    iget-object v1, v1, LX/AcC;->b:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c()V
    .locals 3

    .prologue
    .line 1697724
    iget-object v1, p0, LX/AeV;->w:LX/AeX;

    monitor-enter v1

    .line 1697725
    :try_start_0
    iget-object v0, p0, LX/AeV;->w:LX/AeX;

    sget-object v2, LX/AeN;->LIVE_COMMENT_EVENT:LX/AeN;

    invoke-virtual {v0, v2}, LX/AeX;->b(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 1697726
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1697727
    iget-object v1, p0, LX/AeV;->B:LX/Ag1;

    invoke-virtual {v1, v0}, LX/Ag1;->a(Ljava/util/List;)V

    .line 1697728
    return-void

    .line 1697729
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static declared-synchronized d(LX/AeV;)LX/AeZ;
    .locals 2

    .prologue
    .line 1697718
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/AeV;->H:LX/AeZ;

    if-nez v0, :cond_0

    .line 1697719
    iget-object v0, p0, LX/AeV;->G:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AeZ;

    iput-object v0, p0, LX/AeV;->H:LX/AeZ;

    .line 1697720
    :cond_0
    iget-object v0, p0, LX/AeV;->H:LX/AeZ;

    iget-boolean v1, p0, LX/AeV;->D:Z

    .line 1697721
    iput-boolean v1, v0, LX/AeZ;->e:Z

    .line 1697722
    iget-object v0, p0, LX/AeV;->H:LX/AeZ;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 1697723
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized e()LX/AfX;
    .locals 1

    .prologue
    .line 1697714
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/AeV;->J:LX/AfX;

    if-nez v0, :cond_0

    .line 1697715
    iget-object v0, p0, LX/AeV;->I:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AfX;

    iput-object v0, p0, LX/AeV;->J:LX/AfX;

    .line 1697716
    :cond_0
    iget-object v0, p0, LX/AeV;->J:LX/AfX;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 1697717
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized f()LX/Afu;
    .locals 1

    .prologue
    .line 1697580
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/AeV;->L:LX/Afu;

    if-nez v0, :cond_0

    .line 1697581
    iget-object v0, p0, LX/AeV;->K:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Afu;

    iput-object v0, p0, LX/AeV;->L:LX/Afu;

    .line 1697582
    :cond_0
    iget-object v0, p0, LX/AeV;->L:LX/Afu;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 1697583
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static g(LX/AeV;)V
    .locals 7

    .prologue
    .line 1697682
    iget-object v0, p0, LX/AeV;->x:LX/AcX;

    if-nez v0, :cond_0

    .line 1697683
    :goto_0
    return-void

    .line 1697684
    :cond_0
    iget-object v1, p0, LX/AeV;->w:LX/AeX;

    monitor-enter v1

    .line 1697685
    :try_start_0
    iget-object v0, p0, LX/AeV;->w:LX/AeX;

    const/4 v4, 0x0

    .line 1697686
    iget v2, v0, LX/AeX;->d:I

    add-int/lit8 v2, v2, 0x1

    move v3, v4

    .line 1697687
    :goto_1
    iget-object v5, v0, LX/AeX;->b:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-ge v3, v5, :cond_8

    .line 1697688
    iget-object v5, v0, LX/AeX;->b:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    rem-int v5, v2, v5

    .line 1697689
    iget-object v2, v0, LX/AeX;->a:Ljava/util/HashMap;

    iget-object v6, v0, LX/AeX;->b:Ljava/util/List;

    invoke-interface {v6, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    .line 1697690
    invoke-static {v2}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 1697691
    iput v5, v0, LX/AeX;->d:I

    .line 1697692
    invoke-interface {v2, v4}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v3

    .line 1697693
    invoke-static {v2}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1697694
    iget-object v2, v0, LX/AeX;->b:Ljava/util/List;

    iget v4, v0, LX/AeX;->d:I

    invoke-interface {v2, v4}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 1697695
    iget v2, v0, LX/AeX;->d:I

    add-int/lit8 v2, v2, -0x1

    iput v2, v0, LX/AeX;->d:I

    :cond_1
    move-object v2, v3

    .line 1697696
    :goto_2
    move-object v0, v2

    .line 1697697
    check-cast v0, LX/AeO;

    .line 1697698
    if-eqz v0, :cond_2

    .line 1697699
    iput-object v0, p0, LX/AeV;->M:LX/AeO;

    .line 1697700
    iget-object v2, p0, LX/AeV;->i:Landroid/os/Handler;

    new-instance v3, Lcom/facebook/facecastdisplay/liveevent/LiveEventsStore$9;

    invoke-direct {v3, p0, v0}, Lcom/facebook/facecastdisplay/liveevent/LiveEventsStore$9;-><init>(LX/AeV;LX/AeO;)V

    const v4, -0x319c7903

    invoke-static {v2, v3, v4}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1697701
    :cond_2
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1697702
    iget-object v0, p0, LX/AeV;->s:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AeQ;

    .line 1697703
    iget-object v2, p0, LX/AeV;->w:LX/AeX;

    invoke-virtual {v0}, LX/AeQ;->g()LX/AeN;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/AeX;->a(Ljava/lang/Object;)I

    move-result v2

    .line 1697704
    invoke-virtual {v0}, LX/AeQ;->d()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_4

    invoke-virtual {v0}, LX/AeQ;->d()I

    move-result v3

    if-gt v2, v3, :cond_3

    .line 1697705
    :cond_4
    invoke-virtual {v0}, LX/AeQ;->f()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1697706
    iget-object v2, p0, LX/AeV;->j:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    invoke-virtual {v0}, LX/AeQ;->a()J

    .line 1697707
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    goto :goto_3

    .line 1697708
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 1697709
    :cond_5
    invoke-virtual {v0}, LX/AeQ;->c()V

    goto :goto_3

    .line 1697710
    :cond_6
    invoke-direct {p0}, LX/AeV;->k()V

    goto/16 :goto_0

    .line 1697711
    :cond_7
    add-int/lit8 v5, v5, 0x1

    .line 1697712
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v5

    goto/16 :goto_1

    .line 1697713
    :cond_8
    const/4 v2, 0x0

    goto :goto_2
.end method

.method private h()V
    .locals 3

    .prologue
    .line 1697674
    iget-object v0, p0, LX/AeV;->d:LX/Ac6;

    invoke-virtual {v0}, LX/Ac6;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1697675
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/AeV;->s:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 1697676
    iget-object v0, p0, LX/AeV;->s:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, LX/Af4;

    if-eqz v0, :cond_2

    .line 1697677
    iget-object v0, p0, LX/AeV;->s:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AeQ;

    invoke-virtual {v0}, LX/AeQ;->e()V

    .line 1697678
    iget-object v0, p0, LX/AeV;->s:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 1697679
    :cond_0
    iget-object v0, p0, LX/AeV;->i:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/facecastdisplay/liveevent/LiveEventsStore$10;

    invoke-direct {v1, p0}, Lcom/facebook/facecastdisplay/liveevent/LiveEventsStore$10;-><init>(LX/AeV;)V

    const v2, -0x2f97611b

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1697680
    :cond_1
    return-void

    .line 1697681
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public static j(LX/AeV;)Z
    .locals 2

    .prologue
    .line 1697670
    iget-object v0, p0, LX/AeV;->s:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AeQ;

    .line 1697671
    invoke-virtual {v0}, LX/AeQ;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1697672
    const/4 v0, 0x0

    .line 1697673
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private k()V
    .locals 5

    .prologue
    .line 1697652
    iget-object v0, p0, LX/AeV;->l:LX/3Hd;

    invoke-virtual {v0}, LX/3Hd;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/AeV;->N:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 1697653
    :cond_0
    :goto_0
    return-void

    .line 1697654
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1697655
    const-string v0, "downloader/bufferSize/status\n"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1697656
    iget-object v0, p0, LX/AeV;->s:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AeQ;

    .line 1697657
    iget-object v3, p0, LX/AeV;->w:LX/AeX;

    invoke-virtual {v0}, LX/AeQ;->g()LX/AeN;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/AeX;->a(Ljava/lang/Object;)I

    move-result v3

    .line 1697658
    invoke-virtual {v0}, LX/AeQ;->g()LX/AeN;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1697659
    const-string v4, " "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1697660
    const-string v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, LX/AeQ;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, " fetching"

    :goto_2
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1697661
    const-string v0, "\n"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 1697662
    :cond_2
    const-string v0, " idle"

    goto :goto_2

    .line 1697663
    :cond_3
    iget-object v0, p0, LX/AeV;->M:LX/AeO;

    if-eqz v0, :cond_4

    .line 1697664
    const-string v0, "lastEvent: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1697665
    iget-object v0, p0, LX/AeV;->M:LX/AeO;

    invoke-interface {v0}, LX/AeO;->a()LX/AeN;

    move-result-object v0

    sget-object v2, LX/AeN;->LIVE_COMMENT_EVENT:LX/AeN;

    if-ne v0, v2, :cond_5

    .line 1697666
    iget-object v0, p0, LX/AeV;->M:LX/AeO;

    check-cast v0, LX/Aeu;

    .line 1697667
    iget-object v2, v0, LX/AeP;->a:LX/AcC;

    iget-object v2, v2, LX/AcC;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, v0, LX/Aeu;->b:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1697668
    :cond_4
    :goto_3
    iget-object v0, p0, LX/AeV;->l:LX/3Hd;

    sget-object v2, LX/AeV;->c:Ljava/lang/String;

    iget-object v3, p0, LX/AeV;->N:Ljava/lang/String;

    invoke-virtual {v0, v2, v1, v3}, LX/3Hd;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1697669
    :cond_5
    iget-object v0, p0, LX/AeV;->M:LX/AeO;

    invoke-interface {v0}, LX/AeO;->a()LX/AeN;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_3
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 1697763
    iget-object v0, p0, LX/AeV;->h:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 1697764
    iget-object v0, p0, LX/AeV;->H:LX/AeZ;

    if-eqz v0, :cond_0

    .line 1697765
    iget-object v0, p0, LX/AeV;->H:LX/AeZ;

    invoke-virtual {v0}, LX/AeZ;->a()V

    .line 1697766
    :cond_0
    iget-object v0, p0, LX/AeV;->J:LX/AfX;

    if-eqz v0, :cond_1

    .line 1697767
    iget-object v0, p0, LX/AeV;->J:LX/AfX;

    .line 1697768
    iget-object v1, v0, LX/AfX;->c:LX/0Sh;

    invoke-virtual {v1}, LX/0Sh;->a()V

    .line 1697769
    iget-boolean v1, v0, LX/AfX;->j:Z

    if-nez v1, :cond_4

    .line 1697770
    :cond_1
    :goto_0
    iget-object v0, p0, LX/AeV;->L:LX/Afu;

    if-eqz v0, :cond_2

    .line 1697771
    iget-object v0, p0, LX/AeV;->L:LX/Afu;

    .line 1697772
    iget-object v1, v0, LX/Afu;->c:LX/0Sh;

    invoke-virtual {v1}, LX/0Sh;->a()V

    .line 1697773
    iget-boolean v1, v0, LX/Afu;->j:Z

    if-nez v1, :cond_6

    .line 1697774
    :cond_2
    :goto_1
    new-instance v0, Lcom/facebook/facecastdisplay/liveevent/LiveEventsStore$2;

    invoke-direct {v0, p0}, Lcom/facebook/facecastdisplay/liveevent/LiveEventsStore$2;-><init>(LX/AeV;)V

    .line 1697775
    iget-object v1, p0, LX/AeV;->g:Ljava/util/concurrent/ScheduledExecutorService;

    const v2, 0x845c06b

    invoke-static {v1, v0, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1697776
    iget-object v0, p0, LX/AeV;->y:Ljava/util/concurrent/Future;

    if-eqz v0, :cond_3

    .line 1697777
    iget-object v0, p0, LX/AeV;->y:Ljava/util/concurrent/Future;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 1697778
    const/4 v0, 0x0

    iput-object v0, p0, LX/AeV;->y:Ljava/util/concurrent/Future;

    .line 1697779
    :cond_3
    iget-object v0, p0, LX/AeV;->s:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1697780
    iget-object v0, p0, LX/AeV;->i:Landroid/os/Handler;

    sget-object v1, LX/AeV;->b:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 1697781
    iget-object v0, p0, LX/AeV;->u:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 1697782
    iget-object v0, p0, LX/AeV;->t:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 1697783
    iget-object v0, p0, LX/AeV;->v:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 1697784
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/AeV;->F:Z

    .line 1697785
    return-void

    .line 1697786
    :cond_4
    iget-object v1, v0, LX/AfX;->h:LX/0gM;

    if-eqz v1, :cond_5

    .line 1697787
    iget-object v1, v0, LX/AfX;->b:LX/0gX;

    iget-object v2, v0, LX/AfX;->h:LX/0gM;

    invoke-static {v2}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0gX;->a(Ljava/util/Set;)V

    .line 1697788
    const/4 v1, 0x0

    iput-object v1, v0, LX/AfX;->h:LX/0gM;

    .line 1697789
    :cond_5
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/AfX;->j:Z

    goto :goto_0

    .line 1697790
    :cond_6
    iget-object v1, v0, LX/Afu;->h:LX/0gM;

    if-eqz v1, :cond_7

    .line 1697791
    iget-object v1, v0, LX/Afu;->b:LX/0gX;

    iget-object v2, v0, LX/Afu;->h:LX/0gM;

    invoke-static {v2}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0gX;->a(Ljava/util/Set;)V

    .line 1697792
    const/4 v1, 0x0

    iput-object v1, v0, LX/Afu;->h:LX/0gM;

    .line 1697793
    :cond_7
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/Afu;->j:Z

    goto :goto_1
.end method

.method public final a(F)V
    .locals 0

    .prologue
    .line 1697516
    return-void
.end method

.method public final a(LX/AcX;)V
    .locals 1

    .prologue
    .line 1697517
    iget-object v0, p0, LX/AeV;->h:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 1697518
    iput-object p1, p0, LX/AeV;->x:LX/AcX;

    .line 1697519
    return-void
.end method

.method public final a(LX/AeN;Ljava/util/List;Z)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/AeN;",
            "Ljava/util/List",
            "<",
            "LX/AeO;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    const/4 v8, 0x1

    .line 1697520
    if-eqz p3, :cond_1

    iget-object v1, p0, LX/AeV;->x:LX/AcX;

    if-eqz v1, :cond_1

    .line 1697521
    iget-object v0, p0, LX/AeV;->i:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/facecastdisplay/liveevent/LiveEventsStore$7;

    invoke-direct {v1, p0, p2}, Lcom/facebook/facecastdisplay/liveevent/LiveEventsStore$7;-><init>(LX/AeV;Ljava/util/List;)V

    const v2, 0x674b42c0

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1697522
    invoke-static {p0}, LX/AeV;->j(LX/AeV;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1697523
    iget-object v0, p0, LX/AeV;->i:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/facecastdisplay/liveevent/LiveEventsStore$8;

    invoke-direct {v1, p0}, Lcom/facebook/facecastdisplay/liveevent/LiveEventsStore$8;-><init>(LX/AeV;)V

    const v2, -0x5b5561c1

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1697524
    :cond_0
    :goto_0
    sget-object v1, LX/AeN;->LIVE_COMMENT_EVENT:LX/AeN;

    if-ne p1, v1, :cond_a

    .line 1697525
    if-eqz p3, :cond_b

    .line 1697526
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/AeO;

    .line 1697527
    check-cast v1, LX/Aeu;

    .line 1697528
    iget-object v3, p0, LX/AeV;->t:Ljava/util/Set;

    iget-object v1, v1, LX/Aeu;->f:Ljava/lang/String;

    invoke-interface {v3, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1697529
    :cond_1
    iget-object v4, p0, LX/AeV;->w:LX/AeX;

    monitor-enter v4

    .line 1697530
    :try_start_0
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_2
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/AeO;

    .line 1697531
    sget-object v2, LX/AeN;->LIVE_COMMENT_EVENT:LX/AeN;

    if-ne p1, v2, :cond_6

    .line 1697532
    move-object v0, v1

    check-cast v0, LX/Aeu;

    move-object v2, v0

    .line 1697533
    iget-object v3, v2, LX/AeP;->a:LX/AcC;

    iget-object v3, v3, LX/AcC;->b:Ljava/lang/String;

    iget-object v6, p0, LX/AeV;->A:Ljava/lang/String;

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, LX/AeV;->u:Ljava/util/Set;

    iget-object v6, v2, LX/Aeu;->b:Ljava/lang/String;

    invoke-interface {v3, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 1697534
    :cond_3
    iget-object v3, p0, LX/AeV;->t:Ljava/util/Set;

    iget-object v6, v2, LX/Aeu;->f:Ljava/lang/String;

    invoke-interface {v3, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 1697535
    iget-boolean v3, p0, LX/AeV;->C:Z

    if-nez v3, :cond_4

    iget-boolean v3, p0, LX/AeV;->D:Z

    if-eqz v3, :cond_7

    :cond_4
    const/4 v3, 0x0

    .line 1697536
    :goto_3
    if-eqz v3, :cond_5

    .line 1697537
    const/4 v6, 0x1

    iput-boolean v6, p0, LX/AeV;->C:Z

    .line 1697538
    iget-object v6, p0, LX/AeV;->w:LX/AeX;

    sget-object v7, LX/AeN;->LIVE_ANNOUNCEMENT_EVENT:LX/AeN;

    invoke-virtual {v6, v7, v3}, LX/AeX;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1697539
    :cond_5
    iget-object v3, p0, LX/AeV;->t:Ljava/util/Set;

    iget-object v2, v2, LX/Aeu;->f:Ljava/lang/String;

    invoke-interface {v3, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1697540
    :cond_6
    invoke-static {p0, v1}, LX/AeV;->b(LX/AeV;LX/AeO;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1697541
    iget-object v2, p0, LX/AeV;->w:LX/AeX;

    invoke-interface {v1}, LX/AeO;->a()LX/AeN;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, LX/AeX;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_2

    .line 1697542
    :catchall_0
    move-exception v1

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 1697543
    :cond_7
    :try_start_1
    iget-object v3, p0, LX/AeV;->B:LX/Ag1;

    invoke-virtual {v3, v2}, LX/Ag1;->a(LX/Aeu;)LX/Aen;

    move-result-object v3

    goto :goto_3

    .line 1697544
    :cond_8
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 1697545
    :cond_9
    invoke-direct {p0}, LX/AeV;->h()V

    .line 1697546
    :cond_a
    :goto_4
    invoke-direct {p0}, LX/AeV;->k()V

    .line 1697547
    return-void

    .line 1697548
    :cond_b
    iget-object v1, p0, LX/AeV;->d:LX/Ac6;

    invoke-virtual {v1}, LX/Ac6;->a()Z

    move-result v1

    if-eqz v1, :cond_a

    iget-boolean v1, p0, LX/AeV;->E:Z

    if-nez v1, :cond_a

    .line 1697549
    iput-boolean v8, p0, LX/AeV;->E:Z

    .line 1697550
    iget-object v1, p0, LX/AeV;->k:LX/AVT;

    iget-object v2, p0, LX/AeV;->N:Ljava/lang/String;

    .line 1697551
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1697552
    const-string v3, "facecast_event_name"

    const-string v4, "facecast_ranking_experiment_exposure"

    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1697553
    const-string v3, "facecast_ranking_experiment_exposure"

    invoke-static {v1, v3, v0}, LX/AVT;->c(LX/AVT;Ljava/lang/String;Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 1697554
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_c

    .line 1697555
    invoke-virtual {v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->i(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1697556
    :cond_c
    iget-object v3, v1, LX/AVT;->a:LX/0Zb;

    invoke-interface {v3, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1697557
    goto :goto_4
.end method

.method public final a(LX/AeN;Z)V
    .locals 3

    .prologue
    .line 1697558
    if-eqz p2, :cond_1

    .line 1697559
    invoke-static {p0}, LX/AeV;->j(LX/AeV;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1697560
    iget-object v0, p0, LX/AeV;->i:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/facecastdisplay/liveevent/LiveEventsStore$5;

    invoke-direct {v1, p0}, Lcom/facebook/facecastdisplay/liveevent/LiveEventsStore$5;-><init>(LX/AeV;)V

    const v2, 0xda87e0e

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1697561
    :cond_0
    sget-object v0, LX/AeN;->LIVE_COMMENT_EVENT:LX/AeN;

    if-ne p1, v0, :cond_1

    .line 1697562
    invoke-direct {p0}, LX/AeV;->h()V

    .line 1697563
    :cond_1
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 11

    .prologue
    .line 1697564
    iget-object v0, p0, LX/AeV;->h:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 1697565
    iget-object v0, p0, LX/AeV;->y:Ljava/util/concurrent/Future;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/AeV;->y:Ljava/util/concurrent/Future;

    invoke-interface {v0}, Ljava/util/concurrent/Future;->isDone()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1697566
    :goto_0
    return-void

    .line 1697567
    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1697568
    iget-object v0, p0, LX/AeV;->f:LX/03V;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, LX/AeV;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_startLoading"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Tried to start loading without a valid story id."

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1697569
    :cond_1
    iget-object v0, p0, LX/AeV;->e:LX/1b4;

    invoke-virtual {v0}, LX/1b4;->j()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1697570
    invoke-direct {p0}, LX/AeV;->e()LX/AfX;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, LX/AfX;->a(LX/AeV;Ljava/lang/String;)V

    .line 1697571
    :cond_2
    iget-object v0, p0, LX/AeV;->e:LX/1b4;

    invoke-virtual {v0}, LX/1b4;->n()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, LX/AeV;->e:LX/1b4;

    invoke-virtual {v0}, LX/1b4;->s()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1697572
    :cond_3
    invoke-direct {p0}, LX/AeV;->f()LX/Afu;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, LX/Afu;->a(LX/AeV;Ljava/lang/String;)V

    .line 1697573
    :cond_4
    const/4 v10, 0x0

    .line 1697574
    iput-object p1, p0, LX/AeV;->N:Ljava/lang/String;

    .line 1697575
    new-instance v4, Lcom/facebook/facecastdisplay/liveevent/LiveEventsStore$1;

    invoke-direct {v4, p0, p1}, Lcom/facebook/facecastdisplay/liveevent/LiveEventsStore$1;-><init>(LX/AeV;Ljava/lang/String;)V

    .line 1697576
    iget-object v3, p0, LX/AeV;->g:Ljava/util/concurrent/ScheduledExecutorService;

    const-wide/16 v5, 0x0

    const-wide/16 v7, 0x2

    sget-object v9, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface/range {v3 .. v9}, Ljava/util/concurrent/ScheduledExecutorService;->scheduleAtFixedRate(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v3

    iput-object v3, p0, LX/AeV;->y:Ljava/util/concurrent/Future;

    .line 1697577
    iput-boolean v10, p0, LX/AeV;->E:Z

    .line 1697578
    iput-boolean v10, p0, LX/AeV;->F:Z

    .line 1697579
    goto :goto_0
.end method

.method public final a(Z)V
    .locals 9

    .prologue
    .line 1697584
    iput-boolean p1, p0, LX/AeV;->D:Z

    .line 1697585
    iget-object v0, p0, LX/AeV;->n:LX/Af5;

    iget-object v1, p0, LX/AeV;->g:Ljava/util/concurrent/ScheduledExecutorService;

    .line 1697586
    new-instance v3, LX/Af4;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v5

    check-cast v5, LX/0tX;

    invoke-static {v0}, LX/AaZ;->b(LX/0QB;)LX/AaZ;

    move-result-object v6

    check-cast v6, LX/AaZ;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v7

    check-cast v7, LX/03V;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v8

    check-cast v8, LX/0SG;

    move-object v4, v1

    invoke-direct/range {v3 .. v8}, LX/Af4;-><init>(Ljava/util/concurrent/ExecutorService;LX/0tX;LX/AaZ;LX/03V;LX/0SG;)V

    .line 1697587
    move-object v0, v3

    .line 1697588
    iget-boolean v1, p0, LX/AeV;->D:Z

    .line 1697589
    iput-boolean v1, v0, LX/Af4;->k:Z

    .line 1697590
    iget-object v1, p0, LX/AeV;->s:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1697591
    iget-object v0, p0, LX/AeV;->s:Ljava/util/List;

    iget-object v1, p0, LX/AeV;->o:LX/Ag5;

    iget-object v2, p0, LX/AeV;->g:Ljava/util/concurrent/ScheduledExecutorService;

    .line 1697592
    new-instance v6, LX/Ag4;

    invoke-static {v1}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {v1}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-static {v1}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v5

    check-cast v5, LX/0SG;

    invoke-direct {v6, v2, v3, v4, v5}, LX/Ag4;-><init>(Ljava/util/concurrent/ExecutorService;LX/0tX;LX/03V;LX/0SG;)V

    .line 1697593
    move-object v1, v6

    .line 1697594
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1697595
    iget-boolean v0, p0, LX/AeV;->D:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/AeV;->e:LX/1b4;

    invoke-virtual {v0}, LX/1b4;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1697596
    :cond_0
    iget-object v0, p0, LX/AeV;->s:Ljava/util/List;

    iget-object v1, p0, LX/AeV;->r:LX/Afq;

    iget-object v2, p0, LX/AeV;->g:Ljava/util/concurrent/ScheduledExecutorService;

    .line 1697597
    new-instance v6, LX/Afp;

    invoke-static {v1}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {v1}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-static {v1}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v5

    check-cast v5, LX/0SG;

    invoke-direct {v6, v2, v3, v4, v5}, LX/Afp;-><init>(Ljava/util/concurrent/ExecutorService;LX/0tX;LX/03V;LX/0SG;)V

    .line 1697598
    move-object v1, v6

    .line 1697599
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1697600
    :cond_1
    iget-boolean v0, p0, LX/AeV;->D:Z

    if-nez v0, :cond_2

    .line 1697601
    iget-object v0, p0, LX/AeV;->s:Ljava/util/List;

    iget-object v1, p0, LX/AeV;->p:LX/Afh;

    iget-object v2, p0, LX/AeV;->g:Ljava/util/concurrent/ScheduledExecutorService;

    .line 1697602
    new-instance v3, LX/Afg;

    invoke-static {v1}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v5

    check-cast v5, LX/0tX;

    invoke-static {v1}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v6

    check-cast v6, LX/03V;

    invoke-static {v1}, LX/1si;->b(LX/0QB;)Ljava/lang/String;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-static {v1}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v8

    check-cast v8, LX/0SG;

    move-object v4, v2

    invoke-direct/range {v3 .. v8}, LX/Afg;-><init>(Ljava/util/concurrent/ExecutorService;LX/0tX;LX/03V;Ljava/lang/String;LX/0SG;)V

    .line 1697603
    move-object v1, v3

    .line 1697604
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1697605
    :cond_2
    iget-object v0, p0, LX/AeV;->s:Ljava/util/List;

    iget-object v1, p0, LX/AeV;->q:LX/Aet;

    iget-object v2, p0, LX/AeV;->g:Ljava/util/concurrent/ScheduledExecutorService;

    .line 1697606
    new-instance v6, LX/Aes;

    invoke-static {v1}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {v1}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-static {v1}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v5

    check-cast v5, LX/0SG;

    invoke-direct {v6, v2, v3, v4, v5}, LX/Aes;-><init>(Ljava/util/concurrent/ExecutorService;LX/0tX;LX/03V;LX/0SG;)V

    .line 1697607
    move-object v1, v6

    .line 1697608
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1697609
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 1697610
    iget-object v0, p0, LX/AeV;->m:LX/AaZ;

    invoke-virtual {v0}, LX/AaZ;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1697611
    invoke-direct {p0}, LX/AeV;->c()V

    .line 1697612
    :cond_0
    iget-object v0, p0, LX/AeV;->d:LX/Ac6;

    invoke-virtual {v0}, LX/Ac6;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1697613
    iget-object v0, p0, LX/AeV;->i:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/facecastdisplay/liveevent/LiveEventsStore$6;

    invoke-direct {v1, p0}, Lcom/facebook/facecastdisplay/liveevent/LiveEventsStore$6;-><init>(LX/AeV;)V

    const v2, 0x283b1eb

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1697614
    :cond_1
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1697615
    iget-object v0, p0, LX/AeV;->u:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1697616
    return-void
.end method

.method public final b(Z)V
    .locals 7

    .prologue
    .line 1697617
    iget-object v0, p0, LX/AeV;->h:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 1697618
    iget-boolean v0, p0, LX/AeV;->F:Z

    if-ne v0, p1, :cond_1

    .line 1697619
    :cond_0
    :goto_0
    return-void

    .line 1697620
    :cond_1
    iput-boolean p1, p0, LX/AeV;->F:Z

    .line 1697621
    if-eqz p1, :cond_3

    .line 1697622
    new-instance v0, Lcom/facebook/facecastdisplay/liveevent/LiveEventsStore$3;

    invoke-direct {v0, p0}, Lcom/facebook/facecastdisplay/liveevent/LiveEventsStore$3;-><init>(LX/AeV;)V

    .line 1697623
    iget-object v1, p0, LX/AeV;->g:Ljava/util/concurrent/ScheduledExecutorService;

    const v2, -0x73c80e80

    invoke-static {v1, v0, v2}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1697624
    iget-object v0, p0, LX/AeV;->d:LX/Ac6;

    invoke-virtual {v0}, LX/Ac6;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/AeV;->H:LX/AeZ;

    if-eqz v0, :cond_2

    .line 1697625
    iget-object v0, p0, LX/AeV;->H:LX/AeZ;

    invoke-virtual {v0}, LX/AeZ;->a()V

    .line 1697626
    :cond_2
    iget-object v0, p0, LX/AeV;->y:Ljava/util/concurrent/Future;

    if-eqz v0, :cond_0

    .line 1697627
    iget-object v0, p0, LX/AeV;->y:Ljava/util/concurrent/Future;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 1697628
    const/4 v0, 0x0

    iput-object v0, p0, LX/AeV;->y:Ljava/util/concurrent/Future;

    goto :goto_0

    .line 1697629
    :cond_3
    iget-object v0, p0, LX/AeV;->s:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AeQ;

    .line 1697630
    invoke-virtual {v0}, LX/AeQ;->c()V

    goto :goto_1

    .line 1697631
    :cond_4
    iget-object v0, p0, LX/AeV;->d:LX/Ac6;

    invoke-virtual {v0}, LX/Ac6;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1697632
    invoke-static {p0}, LX/AeV;->d(LX/AeV;)LX/AeZ;

    move-result-object v0

    iget-object v1, p0, LX/AeV;->z:Ljava/lang/String;

    invoke-virtual {v0, p0, v1}, LX/AeZ;->a(LX/AeV;Ljava/lang/String;)V

    .line 1697633
    :cond_5
    iget-object v0, p0, LX/AeV;->g:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/facebook/facecastdisplay/liveevent/LiveEventsStore$4;

    invoke-direct {v1, p0}, Lcom/facebook/facecastdisplay/liveevent/LiveEventsStore$4;-><init>(LX/AeV;)V

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x2

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface/range {v0 .. v6}, Ljava/util/concurrent/ScheduledExecutorService;->scheduleAtFixedRate(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, LX/AeV;->y:Ljava/util/concurrent/Future;

    goto :goto_0
.end method

.method public final c(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1697634
    iput-object p1, p0, LX/AeV;->A:Ljava/lang/String;

    .line 1697635
    return-void
.end method

.method public final d(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1697636
    iget-object v1, p0, LX/AeV;->w:LX/AeX;

    monitor-enter v1

    .line 1697637
    :try_start_0
    iget-object v0, p0, LX/AeV;->w:LX/AeX;

    sget-object v2, LX/AeN;->LIVE_COMMENT_EVENT:LX/AeN;

    invoke-virtual {v0, v2}, LX/AeX;->b(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 1697638
    invoke-static {v0}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1697639
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 1697640
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1697641
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AeP;

    .line 1697642
    iget-object v0, v0, LX/AeP;->a:LX/AcC;

    iget-object v0, v0, LX/AcC;->b:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1697643
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 1697644
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1697645
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/AeV;->v:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1697646
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public final e(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1697647
    iget-object v0, p0, LX/AeV;->h:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 1697648
    iput-object p1, p0, LX/AeV;->z:Ljava/lang/String;

    .line 1697649
    iget-boolean v0, p0, LX/AeV;->D:Z

    if-eqz v0, :cond_0

    .line 1697650
    invoke-direct {p0}, LX/AeV;->h()V

    .line 1697651
    :cond_0
    return-void
.end method
