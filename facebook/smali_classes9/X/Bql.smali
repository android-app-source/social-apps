.class public LX/Bql;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field public final a:LX/BqL;

.field public final b:LX/Br8;

.field public final c:LX/17W;

.field public final d:LX/0ad;


# direct methods
.method public constructor <init>(LX/BqL;LX/17W;LX/Br8;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1825121
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1825122
    iput-object p1, p0, LX/Bql;->a:LX/BqL;

    .line 1825123
    iput-object p2, p0, LX/Bql;->c:LX/17W;

    .line 1825124
    iput-object p3, p0, LX/Bql;->b:LX/Br8;

    .line 1825125
    iput-object p4, p0, LX/Bql;->d:LX/0ad;

    .line 1825126
    return-void
.end method

.method public static a(LX/0QB;)LX/Bql;
    .locals 7

    .prologue
    .line 1825127
    const-class v1, LX/Bql;

    monitor-enter v1

    .line 1825128
    :try_start_0
    sget-object v0, LX/Bql;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1825129
    sput-object v2, LX/Bql;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1825130
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1825131
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1825132
    new-instance p0, LX/Bql;

    invoke-static {v0}, LX/BqL;->a(LX/0QB;)LX/BqL;

    move-result-object v3

    check-cast v3, LX/BqL;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v4

    check-cast v4, LX/17W;

    invoke-static {v0}, LX/Br8;->a(LX/0QB;)LX/Br8;

    move-result-object v5

    check-cast v5, LX/Br8;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v6

    check-cast v6, LX/0ad;

    invoke-direct {p0, v3, v4, v5, v6}, LX/Bql;-><init>(LX/BqL;LX/17W;LX/Br8;LX/0ad;)V

    .line 1825133
    move-object v0, p0

    .line 1825134
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1825135
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Bql;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1825136
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1825137
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
