.class public final LX/BzA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1KL;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1KL",
        "<",
        "Ljava/lang/String;",
        "LX/1yT;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/BzB;

.field private final b:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

.field public final c:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

.field private final d:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/1eK;

.field private final f:Ljava/lang/String;


# direct methods
.method private constructor <init>(LX/BzB;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLTextWithEntities;LX/1eK;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLTextWithEntities;",
            "LX/1eK;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1838278
    iput-object p1, p0, LX/BzA;->a:LX/BzB;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1838279
    iput-object p2, p0, LX/BzA;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1838280
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1838281
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    iput-object v0, p0, LX/BzA;->c:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1838282
    iput-object p4, p0, LX/BzA;->b:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1838283
    iput-object p5, p0, LX/BzA;->e:LX/1eK;

    .line 1838284
    iget-object v0, p3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1838285
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0, p6}, LX/6Bx;->a(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/BzA;->f:Ljava/lang/String;

    .line 1838286
    return-void
.end method

.method public synthetic constructor <init>(LX/BzB;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLTextWithEntities;LX/1eK;Ljava/lang/String;B)V
    .locals 0

    .prologue
    .line 1838288
    invoke-direct/range {p0 .. p6}, LX/BzA;-><init>(LX/BzB;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLTextWithEntities;LX/1eK;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 6

    .prologue
    .line 1838289
    const/4 v5, 0x0

    .line 1838290
    iget-object v0, p0, LX/BzA;->b:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/BzA;->b:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1838291
    :cond_0
    new-instance v0, LX/1yT;

    new-instance v1, LX/1zR;

    const-string v2, ""

    invoke-direct {v1, v2}, LX/1zR;-><init>(Ljava/lang/CharSequence;)V

    invoke-direct {v0, v1, v5}, LX/1yT;-><init>(Landroid/text/Spannable;Z)V

    .line 1838292
    :goto_0
    return-object v0

    .line 1838293
    :cond_1
    iget-object v0, p0, LX/BzA;->a:LX/BzB;

    iget-object v0, v0, LX/BzB;->d:LX/1Uf;

    iget-object v1, p0, LX/BzA;->b:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-static {v1}, LX/1eD;->b(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLinksGraphQLModel;

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p0, LX/BzA;->e:LX/1eK;

    iget-object v4, p0, LX/BzA;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v4}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, LX/1Uf;->a(Lcom/facebook/ufiservices/util/LinkifyUtilGraphQLModels$LinkableUtilAddLinksGraphQLModel;Lcom/facebook/graphql/model/FeedUnit;LX/1eK;LX/0lF;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    .line 1838294
    iget-object v1, p0, LX/BzA;->a:LX/BzB;

    iget-object v1, v1, LX/BzB;->d:LX/1Uf;

    new-instance v2, LX/Bz9;

    invoke-direct {v2, p0}, LX/Bz9;-><init>(LX/BzA;)V

    invoke-virtual {v1, v0, v2}, LX/1Uf;->a(Ljava/lang/CharSequence;Landroid/view/View$OnClickListener;)Landroid/text/Spannable;

    move-result-object v1

    .line 1838295
    new-instance v2, LX/1yT;

    if-nez v1, :cond_2

    :goto_1
    invoke-direct {v2, v0, v5}, LX/1yT;-><init>(Landroid/text/Spannable;Z)V

    move-object v0, v2

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_1
.end method

.method public final b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1838287
    iget-object v0, p0, LX/BzA;->f:Ljava/lang/String;

    return-object v0
.end method
