.class public final LX/Bu8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Bu9;


# direct methods
.method public constructor <init>(LX/Bu9;)V
    .locals 0

    .prologue
    .line 1830412
    iput-object p1, p0, LX/Bu8;->a:LX/Bu9;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 1830413
    iget-object v0, p0, LX/Bu8;->a:LX/Bu9;

    iget-object v0, v0, LX/Bu9;->a:LX/BuA;

    iget-object v0, v0, LX/BuA;->a:Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;

    iget-object v0, v0, LX/3Gb;->n:LX/7Lf;

    if-eqz v0, :cond_0

    .line 1830414
    iget-object v0, p0, LX/Bu8;->a:LX/Bu9;

    iget-object v0, v0, LX/Bu9;->a:LX/BuA;

    iget-object v0, v0, LX/BuA;->a:Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;

    iget-object v0, v0, LX/3Gb;->n:LX/7Lf;

    check-cast v0, LX/7Lg;

    invoke-interface {v0}, LX/7Lg;->a()V

    .line 1830415
    :cond_0
    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1830416
    instance-of v0, p1, Ljava/io/IOException;

    if-nez v0, :cond_0

    .line 1830417
    iget-object v0, p0, LX/Bu8;->a:LX/Bu9;

    iget-object v0, v0, LX/Bu9;->a:LX/BuA;

    iget-object v0, v0, LX/BuA;->a:Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;

    iget-object v0, v0, Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;->v:LX/03V;

    const-string v1, "video_delete_fail"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1830418
    :cond_0
    iget-object v0, p0, LX/Bu8;->a:LX/Bu9;

    iget-object v0, v0, LX/Bu9;->a:LX/BuA;

    iget-object v0, v0, LX/BuA;->a:Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;

    invoke-virtual {v0}, Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f080039

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1830419
    return-void
.end method

.method public final synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1830420
    invoke-direct {p0}, LX/Bu8;->a()V

    return-void
.end method
