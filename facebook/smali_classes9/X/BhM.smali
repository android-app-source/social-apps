.class public LX/BhM;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile h:LX/BhM;


# instance fields
.field private final b:LX/BgU;

.field private final c:LX/BgX;

.field public final d:LX/17Y;

.field public final e:Lcom/facebook/content/SecureContextHelper;

.field public final f:LX/03V;

.field public g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/BeE;",
            "LX/Bgq;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1809100
    const-class v0, LX/BhM;

    sput-object v0, LX/BhM;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Ljava/util/Set;LX/BgU;LX/BgX;LX/17Y;Lcom/facebook/content/SecureContextHelper;LX/03V;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "LX/Bgq;",
            ">;",
            "LX/BgU;",
            "LX/BgX;",
            "LX/17Y;",
            "Lcom/facebook/content/SecureContextHelper;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1809090
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1809091
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/BhM;->g:Ljava/util/Map;

    .line 1809092
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Bgq;

    .line 1809093
    iget-object v2, p0, LX/BhM;->g:Ljava/util/Map;

    invoke-interface {v0}, LX/Bgq;->a()LX/BeE;

    move-result-object v3

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1809094
    :cond_0
    iput-object p2, p0, LX/BhM;->b:LX/BgU;

    .line 1809095
    iput-object p3, p0, LX/BhM;->c:LX/BgX;

    .line 1809096
    iput-object p4, p0, LX/BhM;->d:LX/17Y;

    .line 1809097
    iput-object p5, p0, LX/BhM;->e:Lcom/facebook/content/SecureContextHelper;

    .line 1809098
    iput-object p6, p0, LX/BhM;->f:LX/03V;

    .line 1809099
    return-void
.end method

.method public static a(LX/0QB;)LX/BhM;
    .locals 10

    .prologue
    .line 1809015
    sget-object v0, LX/BhM;->h:LX/BhM;

    if-nez v0, :cond_1

    .line 1809016
    const-class v1, LX/BhM;

    monitor-enter v1

    .line 1809017
    :try_start_0
    sget-object v0, LX/BhM;->h:LX/BhM;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1809018
    if-eqz v2, :cond_0

    .line 1809019
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1809020
    new-instance v3, LX/BhM;

    .line 1809021
    new-instance v4, LX/0U8;

    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v5

    new-instance v6, LX/Bgp;

    invoke-direct {v6, v0}, LX/Bgp;-><init>(LX/0QB;)V

    invoke-direct {v4, v5, v6}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v4, v4

    .line 1809022
    const-class v5, LX/BgU;

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/BgU;

    const-class v6, LX/BgX;

    invoke-interface {v0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/BgX;

    invoke-static {v0}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v7

    check-cast v7, LX/17Y;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v8

    check-cast v8, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v9

    check-cast v9, LX/03V;

    invoke-direct/range {v3 .. v9}, LX/BhM;-><init>(Ljava/util/Set;LX/BgU;LX/BgX;LX/17Y;Lcom/facebook/content/SecureContextHelper;LX/03V;)V

    .line 1809023
    move-object v0, v3

    .line 1809024
    sput-object v0, LX/BhM;->h:LX/BhM;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1809025
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1809026
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1809027
    :cond_1
    sget-object v0, LX/BhM;->h:LX/BhM;

    return-object v0

    .line 1809028
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1809029
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Landroid/view/ViewGroup;LX/BeE;)Landroid/view/View;
    .locals 3

    .prologue
    .line 1809086
    iget-object v0, p0, LX/BhM;->g:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Bgq;

    .line 1809087
    if-nez v0, :cond_0

    .line 1809088
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No view viewController defined for view type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1809089
    :cond_0
    invoke-interface {v0, p1}, LX/Bgq;->a(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/widget/LinearLayout;)V
    .locals 3

    .prologue
    .line 1809084
    invoke-virtual {p0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f031417

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 1809085
    return-void
.end method

.method private a(Landroid/widget/LinearLayout;Ljava/lang/String;Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionsModel;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1809078
    invoke-virtual {p1}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f031424

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    .line 1809079
    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1809080
    invoke-virtual {p3}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionsModel;->a()I

    move-result v1

    if-lez v1, :cond_0

    .line 1809081
    new-instance v1, LX/BhL;

    invoke-direct {v1, p0, p1, p2}, LX/BhL;-><init>(LX/BhM;Landroid/widget/LinearLayout;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1809082
    :goto_0
    return-void

    .line 1809083
    :cond_0
    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbButton;->setEnabled(Z)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/support/v4/app/Fragment;LX/BgK;Landroid/widget/LinearLayout;Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionsModel;Ljava/lang/String;)LX/0Px;
    .locals 31
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/app/Fragment;",
            "Lcom/facebook/crowdsourcing/suggestedits/listener/SuggestionEditedListener;",
            "Landroid/widget/LinearLayout;",
            "Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsInterfaces$SuggestEditsSections$;",
            "Ljava/lang/String;",
            ")",
            "LX/0Px",
            "<",
            "LX/BgT;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1809046
    invoke-virtual/range {p4 .. p4}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionsModel;->b()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionsModel$CrowdsourcingSuggestEditsCardsModel;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual/range {p4 .. p4}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionsModel;->b()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionsModel$CrowdsourcingSuggestEditsCardsModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionsModel$CrowdsourcingSuggestEditsCardsModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1809047
    :cond_0
    invoke-static/range {p3 .. p3}, LX/BhM;->a(Landroid/widget/LinearLayout;)V

    .line 1809048
    invoke-static {}, LX/0Px;->of()LX/0Px;

    move-result-object v4

    .line 1809049
    :goto_0
    return-object v4

    .line 1809050
    :cond_1
    const/16 v22, 0x1

    .line 1809051
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v25

    .line 1809052
    invoke-virtual/range {p4 .. p4}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionsModel;->b()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionsModel$CrowdsourcingSuggestEditsCardsModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionsModel$CrowdsourcingSuggestEditsCardsModel;->a()LX/0Px;

    move-result-object v26

    invoke-virtual/range {v26 .. v26}, LX/0Px;->size()I

    move-result v27

    const/4 v4, 0x0

    move/from16 v24, v4

    :goto_1
    move/from16 v0, v24

    move/from16 v1, v27

    if-ge v0, v1, :cond_4

    move-object/from16 v0, v26

    move/from16 v1, v24

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionsModel$CrowdsourcingSuggestEditsCardsModel$EdgesModel;

    .line 1809053
    invoke-virtual {v4}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionsModel$CrowdsourcingSuggestEditsCardsModel$EdgesModel;->a()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionModel;

    move-result-object v28

    .line 1809054
    if-eqz v28, :cond_5

    invoke-virtual/range {v28 .. v28}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionModel;->b()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionModel$FieldSectionsModel;

    move-result-object v4

    if-eqz v4, :cond_5

    .line 1809055
    const/16 v21, 0x0

    .line 1809056
    invoke-virtual/range {v28 .. v28}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionModel;->b()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionModel$FieldSectionsModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionModel$FieldSectionsModel;->a()LX/0Px;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, LX/0Px;->size()I

    move-result v30

    const/4 v4, 0x0

    move/from16 v23, v4

    :goto_2
    move/from16 v0, v23

    move/from16 v1, v30

    if-ge v0, v1, :cond_2

    move-object/from16 v0, v29

    move/from16 v1, v23

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    move-object v12, v4

    check-cast v12, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionModel$FieldSectionsModel$EdgesModel;

    .line 1809057
    invoke-virtual {v12}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionModel$FieldSectionsModel$EdgesModel;->a()LX/97f;

    move-result-object v4

    if-eqz v4, :cond_6

    invoke-virtual {v12}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionModel$FieldSectionsModel$EdgesModel;->a()LX/97f;

    move-result-object v4

    invoke-static {v4}, LX/Bgc;->b(LX/97f;)Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-virtual {v12}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionModel$FieldSectionsModel$EdgesModel;->a()LX/97f;

    move-result-object v4

    invoke-interface {v4}, LX/97e;->e()Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;

    move-result-object v4

    invoke-static {v4}, LX/Bgg;->a(Lcom/facebook/graphql/enums/GraphQLSuggestEditsFieldOptionType;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 1809058
    invoke-virtual {v12}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionModel$FieldSectionsModel$EdgesModel;->a()LX/97f;

    move-result-object v4

    invoke-static {v4}, LX/Bgc;->a(LX/97f;)LX/BeE;

    move-result-object v10

    .line 1809059
    invoke-virtual {v12}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionModel$FieldSectionsModel$EdgesModel;->a()LX/97f;

    move-result-object v4

    invoke-static {v4}, LX/Bgc;->c(LX/97f;)LX/BeD;

    move-result-object v8

    .line 1809060
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1, v10}, LX/BhM;->a(Landroid/view/ViewGroup;LX/BeE;)Landroid/view/View;

    move-result-object v9

    .line 1809061
    move-object/from16 v0, p0

    iget-object v4, v0, LX/BhM;->b:LX/BgU;

    invoke-virtual {v12}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionModel$FieldSectionsModel$EdgesModel;->a()LX/97f;

    move-result-object v7

    move-object/from16 v5, p1

    move-object/from16 v6, p2

    move-object/from16 v11, p5

    invoke-virtual/range {v4 .. v11}, LX/BgU;->a(Landroid/support/v4/app/Fragment;LX/BgK;LX/97f;LX/BeD;Landroid/view/View;LX/BeE;Ljava/lang/String;)LX/BgT;

    move-result-object v16

    .line 1809062
    invoke-virtual {v12}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionModel$FieldSectionsModel$EdgesModel;->a()LX/97f;

    move-result-object v13

    const/4 v14, 0x0

    move-object/from16 v11, p0

    move-object v12, v9

    move-object v15, v10

    move-object/from16 v17, p2

    move-object/from16 v18, v8

    move-object/from16 v19, p1

    move-object/from16 v20, p5

    invoke-virtual/range {v11 .. v20}, LX/BhM;->a(Landroid/view/View;Ljava/lang/Object;Ljava/lang/Object;LX/BeE;LX/BgS;LX/BgK;LX/BeD;Landroid/support/v4/app/Fragment;Ljava/lang/String;)V

    .line 1809063
    move-object/from16 v0, p3

    invoke-virtual {v0, v9}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1809064
    move-object/from16 v0, v25

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1809065
    add-int/lit8 v4, v21, 0x1

    .line 1809066
    :goto_3
    add-int/lit8 v5, v23, 0x1

    move/from16 v23, v5

    move/from16 v21, v4

    goto/16 :goto_2

    .line 1809067
    :cond_2
    if-lez v21, :cond_5

    .line 1809068
    sget-object v4, LX/BeE;->SECTION_TITLE:LX/BeE;

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1, v4}, LX/BhM;->a(Landroid/view/ViewGroup;LX/BeE;)Landroid/view/View;

    move-result-object v5

    .line 1809069
    invoke-virtual/range {v28 .. v28}, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionModel;->a()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    sget-object v8, LX/BeE;->SECTION_TITLE:LX/BeE;

    new-instance v9, LX/BhK;

    move-object/from16 v0, p0

    invoke-direct {v9, v0}, LX/BhK;-><init>(LX/BhM;)V

    const/4 v11, 0x0

    move-object/from16 v4, p0

    move-object/from16 v10, p2

    move-object/from16 v12, p1

    move-object/from16 v13, p5

    invoke-virtual/range {v4 .. v13}, LX/BhM;->a(Landroid/view/View;Ljava/lang/Object;Ljava/lang/Object;LX/BeE;LX/BgS;LX/BgK;LX/BeD;Landroid/support/v4/app/Fragment;Ljava/lang/String;)V

    .line 1809070
    if-nez v22, :cond_3

    .line 1809071
    invoke-virtual/range {p3 .. p3}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    const v6, 0x7f03141b

    const/4 v7, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v4, v6, v0, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 1809072
    invoke-virtual/range {p3 .. p3}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v6

    sub-int v6, v6, v21

    move-object/from16 v0, p3

    invoke-virtual {v0, v4, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 1809073
    :cond_3
    invoke-virtual/range {p3 .. p3}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v4

    sub-int v4, v4, v21

    move-object/from16 v0, p3

    invoke-virtual {v0, v5, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 1809074
    const/4 v4, 0x0

    .line 1809075
    :goto_4
    add-int/lit8 v5, v24, 0x1

    move/from16 v24, v5

    move/from16 v22, v4

    goto/16 :goto_1

    .line 1809076
    :cond_4
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, p5

    move-object/from16 v3, p4

    invoke-direct {v0, v1, v2, v3}, LX/BhM;->a(Landroid/widget/LinearLayout;Ljava/lang/String;Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionsModel;)V

    .line 1809077
    invoke-virtual/range {v25 .. v25}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    goto/16 :goto_0

    :cond_5
    move/from16 v4, v22

    goto :goto_4

    :cond_6
    move/from16 v4, v21

    goto :goto_3
.end method

.method public final a(Landroid/support/v4/app/Fragment;LX/BgK;Landroid/widget/LinearLayout;Lcom/facebook/crowdsourcing/suggestedits/data/SuggestEditsHeaderState;Ljava/lang/String;)LX/BgW;
    .locals 12

    .prologue
    .line 1809040
    sget-object v6, LX/BeE;->PAGE_HEADER:LX/BeE;

    .line 1809041
    invoke-direct {p0, p3, v6}, LX/BhM;->a(Landroid/view/ViewGroup;LX/BeE;)Landroid/view/View;

    move-result-object v3

    .line 1809042
    iget-object v0, p0, LX/BhM;->c:LX/BgX;

    move-object v1, p1

    move-object v2, p2

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    invoke-virtual/range {v0 .. v5}, LX/BgX;->a(Landroid/support/v4/app/Fragment;LX/BgK;Landroid/view/View;Lcom/facebook/crowdsourcing/suggestedits/data/SuggestEditsHeaderState;Ljava/lang/String;)LX/BgW;

    move-result-object v4

    .line 1809043
    const/4 v5, 0x0

    const/4 v9, 0x0

    move-object v2, p0

    move-object v7, v4

    move-object v8, p2

    move-object v10, p1

    move-object/from16 v11, p5

    invoke-virtual/range {v2 .. v11}, LX/BhM;->a(Landroid/view/View;Ljava/lang/Object;Ljava/lang/Object;LX/BeE;LX/BgS;LX/BgK;LX/BeD;Landroid/support/v4/app/Fragment;Ljava/lang/String;)V

    .line 1809044
    const/4 v0, 0x0

    invoke-virtual {p3, v3, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 1809045
    return-object v4
.end method

.method public final a(Landroid/view/View;Ljava/lang/Object;LX/BeE;LX/BgS;)V
    .locals 3

    .prologue
    .line 1809035
    iget-object v0, p0, LX/BhM;->g:Ljava/util/Map;

    invoke-interface {v0, p3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Bgq;

    .line 1809036
    if-nez v0, :cond_0

    .line 1809037
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No view viewController defined for view type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1809038
    :cond_0
    invoke-interface {v0, p1, p2, p4}, LX/Bgq;->a(Landroid/view/View;Ljava/lang/Object;LX/BgS;)Ljava/lang/Object;

    .line 1809039
    return-void
.end method

.method public final a(Landroid/view/View;Ljava/lang/Object;Ljava/lang/Object;LX/BeE;LX/BgS;LX/BgK;LX/BeD;Landroid/support/v4/app/Fragment;Ljava/lang/String;)V
    .locals 9
    .param p3    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # LX/BeD;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1809030
    iget-object v0, p0, LX/BhM;->g:Ljava/util/Map;

    invoke-interface {v0, p4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Bgq;

    .line 1809031
    if-nez v0, :cond_0

    .line 1809032
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No view viewController defined for view type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p5

    move-object v5, p6

    move-object/from16 v6, p7

    move-object/from16 v7, p8

    move-object/from16 v8, p9

    .line 1809033
    invoke-interface/range {v0 .. v8}, LX/Bgq;->a(Landroid/view/View;Ljava/lang/Object;Ljava/lang/Object;LX/BgS;LX/BgK;LX/BeD;Landroid/support/v4/app/Fragment;Ljava/lang/String;)V

    .line 1809034
    return-void
.end method
