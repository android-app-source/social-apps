.class public final LX/AY0;
.super Landroid/os/Handler;
.source ""


# instance fields
.field public final synthetic a:LX/AY1;


# direct methods
.method public constructor <init>(LX/AY1;Landroid/os/Looper;)V
    .locals 0

    .prologue
    .line 1684691
    iput-object p1, p0, LX/AY0;->a:LX/AY1;

    .line 1684692
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 1684693
    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 4

    .prologue
    .line 1684694
    iget v0, p1, Landroid/os/Message;->what:I

    .line 1684695
    packed-switch v0, :pswitch_data_0

    .line 1684696
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unhandled msg what="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1684697
    :pswitch_0
    iget-object v0, p0, LX/AY0;->a:LX/AY1;

    const/4 p0, 0x0

    .line 1684698
    new-instance v1, Landroid/view/ScaleGestureDetector;

    invoke-virtual {v0}, LX/AY1;->getContext()Landroid/content/Context;

    move-result-object v2

    new-instance v3, LX/AXw;

    invoke-direct {v3, v0}, LX/AXw;-><init>(LX/AY1;)V

    invoke-direct {v1, v2, v3}, Landroid/view/ScaleGestureDetector;-><init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    iput-object v1, v0, LX/AY1;->n:Landroid/view/ScaleGestureDetector;

    .line 1684699
    iget-object v1, v0, LX/AY1;->n:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v1, p0}, Landroid/view/ScaleGestureDetector;->setQuickScaleEnabled(Z)V

    .line 1684700
    :goto_0
    return-void

    .line 1684701
    :pswitch_1
    iget-object v1, p0, LX/AY0;->a:LX/AY1;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/view/MotionEvent;

    .line 1684702
    :try_start_0
    iget-object v2, v1, LX/AY1;->n:Landroid/view/ScaleGestureDetector;

    if-eqz v2, :cond_0

    .line 1684703
    iget-object v2, v1, LX/AY1;->n:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v2, v0}, Landroid/view/ScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1684704
    :cond_0
    :goto_1
    goto :goto_0

    .line 1684705
    :catch_0
    move-exception v2

    .line 1684706
    iget-object v3, v1, LX/AY1;->b:LX/03V;

    const-string p0, "broadcaster_zoom_illegal_argument_exception"

    invoke-virtual {v2}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, p0, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
