.class public final LX/Bck;
.super LX/BcS;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<TEdge:",
        "Ljava/lang/Object;",
        "TUserInfo:",
        "Ljava/lang/Object;",
        ">",
        "LX/BcS;"
    }
.end annotation


# static fields
.field private static a:LX/Bck;


# instance fields
.field private b:LX/Bcs;

.field public final c:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Bck",
            "<TTEdge;TTUserInfo;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1802696
    const/4 v0, 0x0

    sput-object v0, LX/Bck;->a:LX/Bck;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 1802692
    invoke-direct {p0}, LX/BcS;-><init>()V

    .line 1802693
    new-instance v0, LX/Bcs;

    invoke-direct {v0}, LX/Bcs;-><init>()V

    iput-object v0, p0, LX/Bck;->b:LX/Bcs;

    .line 1802694
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/Bck;->c:LX/0Zi;

    .line 1802695
    return-void
.end method

.method public static declared-synchronized d()LX/Bck;
    .locals 2

    .prologue
    .line 1802688
    const-class v1, LX/Bck;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/Bck;->a:LX/Bck;

    if-nez v0, :cond_0

    .line 1802689
    new-instance v0, LX/Bck;

    invoke-direct {v0}, LX/Bck;-><init>()V

    sput-object v0, LX/Bck;->a:LX/Bck;

    .line 1802690
    :cond_0
    sget-object v0, LX/Bck;->a:LX/Bck;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 1802691
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/BcO;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1802687
    check-cast p1, LX/Bch;

    iget-object v0, p1, LX/Bch;->k:LX/Bcp;

    return-object v0
.end method

.method public final a(LX/BcQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1802685
    invoke-static {}, LX/1dS;->b()V

    .line 1802686
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/BcO;LX/BcO;)V
    .locals 1

    .prologue
    .line 1802678
    check-cast p1, LX/Bch;

    .line 1802679
    check-cast p2, LX/Bch;

    .line 1802680
    iget-object v0, p1, LX/Bch;->b:Landroid/os/Handler;

    iput-object v0, p2, LX/Bch;->b:Landroid/os/Handler;

    .line 1802681
    iget-object v0, p1, LX/Bch;->f:LX/Bcm;

    iput-object v0, p2, LX/Bch;->f:LX/Bcm;

    .line 1802682
    iget-object v0, p1, LX/Bch;->i:LX/0YT;

    iput-object v0, p2, LX/Bch;->i:LX/0YT;

    .line 1802683
    iget-object v0, p1, LX/Bch;->j:Ljava/lang/String;

    iput-object v0, p2, LX/Bch;->j:Ljava/lang/String;

    .line 1802684
    return-void
.end method

.method public final a(LX/BcP;IIILX/BcO;)V
    .locals 9

    .prologue
    .line 1802666
    check-cast p5, LX/Bch;

    .line 1802667
    iget-object v3, p5, LX/Bch;->k:LX/Bcp;

    iget-object v4, p5, LX/Bch;->c:LX/2kW;

    iget v5, p5, LX/Bch;->h:I

    iget-object v6, p5, LX/Bch;->g:LX/Exd;

    iget-object v7, p5, LX/Bch;->i:LX/0YT;

    iget-object v8, p5, LX/Bch;->j:Ljava/lang/String;

    move v0, p2

    move v1, p3

    move v2, p4

    .line 1802668
    iget-object p0, v4, LX/2kW;->o:LX/2kM;

    move-object p0, p0

    .line 1802669
    if-eqz v6, :cond_1

    .line 1802670
    :goto_0
    if-ltz v0, :cond_1

    if-gt v0, v1, :cond_1

    .line 1802671
    invoke-virtual {v7, v0}, LX/0YT;->b(I)Z

    move-result p1

    if-nez p1, :cond_0

    invoke-interface {p0}, LX/2kM;->c()I

    move-result p1

    if-ge v0, p1, :cond_0

    .line 1802672
    invoke-interface {p0, v0}, LX/2kM;->a(I)Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {v6, v0, p1, v8}, LX/Exd;->a(ILjava/lang/Object;Ljava/lang/String;)V

    .line 1802673
    iget-object p1, v7, LX/0YT;->a:LX/0YU;

    invoke-virtual {p1, v0, v7}, LX/0YU;->a(ILjava/lang/Object;)V

    .line 1802674
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1802675
    :cond_1
    sub-int p0, v2, v5

    if-le v1, p0, :cond_2

    .line 1802676
    invoke-virtual {v3}, LX/Bcp;->b()V

    .line 1802677
    :cond_2
    return-void
.end method

.method public final a(LX/BcP;LX/BcJ;LX/BcO;LX/BcO;)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 1802629
    check-cast p3, LX/Bch;

    .line 1802630
    check-cast p4, LX/Bch;

    .line 1802631
    if-nez p3, :cond_1

    move-object v1, v0

    :goto_0
    if-nez p4, :cond_2

    :goto_1
    invoke-static {v1, v0}, LX/BcS;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/3lz;

    move-result-object v0

    .line 1802632
    const/4 v4, 0x0

    .line 1802633
    invoke-virtual {p1}, LX/BcP;->i()LX/BcO;

    move-result-object v1

    if-nez v1, :cond_9

    .line 1802634
    const/4 v1, 0x0

    .line 1802635
    :goto_2
    move-object p0, v1

    .line 1802636
    iget-object v1, v0, LX/3lz;->a:Ljava/lang/Object;

    move-object v1, v1

    .line 1802637
    check-cast v1, LX/Bcm;

    .line 1802638
    iget-object v2, v0, LX/3lz;->b:Ljava/lang/Object;

    move-object v2, v2

    .line 1802639
    check-cast v2, LX/Bcm;

    .line 1802640
    if-nez v1, :cond_3

    move v3, v4

    .line 1802641
    :goto_3
    iget v5, v2, LX/Bcm;->b:I

    if-ge v5, v3, :cond_4

    .line 1802642
    :cond_0
    return-void

    .line 1802643
    :cond_1
    iget-object v1, p3, LX/Bch;->f:LX/Bcm;

    goto :goto_0

    :cond_2
    iget-object v0, p4, LX/Bch;->f:LX/Bcm;

    goto :goto_1

    .line 1802644
    :cond_3
    iget v3, v1, LX/Bcm;->b:I

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 1802645
    :cond_4
    if-nez v1, :cond_7

    move v1, v4

    :goto_4
    move v3, v1

    .line 1802646
    :goto_5
    iget-object v1, v2, LX/Bcm;->c:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-ge v3, v1, :cond_0

    .line 1802647
    iget-object v1, v2, LX/Bcm;->c:LX/0Px;

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [LX/Bcl;

    move v5, v4

    .line 1802648
    :goto_6
    array-length v6, v1

    if-ge v5, v6, :cond_8

    .line 1802649
    aget-object v6, v1, v5

    if-eqz v6, :cond_6

    .line 1802650
    const/4 v6, 0x0

    .line 1802651
    aget-object p3, v1, v5

    iget-object p3, p3, LX/Bcl;->a:LX/3Ca;

    sget-object p4, LX/3Ca;->DELETE:LX/3Ca;

    if-eq p3, p4, :cond_5

    .line 1802652
    aget-object v6, v1, v5

    iget v6, v6, LX/Bcl;->b:I

    aget-object p3, v1, v5

    iget-object p3, p3, LX/Bcl;->c:Ljava/lang/Object;

    .line 1802653
    new-instance p4, LX/BdG;

    invoke-direct {p4}, LX/BdG;-><init>()V

    .line 1802654
    iput v6, p4, LX/BdG;->a:I

    .line 1802655
    iput-object p3, p4, LX/BdG;->b:Ljava/lang/Object;

    .line 1802656
    iget-object p1, p0, LX/BcQ;->a:LX/BcO;

    .line 1802657
    iget-object v6, p1, LX/BcO;->i:LX/BcS;

    move-object p1, v6

    .line 1802658
    invoke-virtual {p1, p0, p4}, LX/BcS;->a(LX/BcQ;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p4

    check-cast p4, LX/1X1;

    move-object v6, p4

    .line 1802659
    :cond_5
    sget-object p3, LX/Bcr;->a:[I

    aget-object p4, v1, v5

    iget-object p4, p4, LX/Bcl;->a:LX/3Ca;

    invoke-virtual {p4}, LX/3Ca;->ordinal()I

    move-result p4

    aget p3, p3, p4

    packed-switch p3, :pswitch_data_0

    .line 1802660
    :cond_6
    :goto_7
    add-int/lit8 v5, v5, 0x1

    goto :goto_6

    .line 1802661
    :cond_7
    iget-object v1, v1, LX/Bcm;->c:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    goto :goto_4

    .line 1802662
    :pswitch_0
    aget-object p3, v1, v5

    iget p3, p3, LX/Bcl;->b:I

    invoke-virtual {p2, p3, v6}, LX/BcJ;->a(ILX/1X1;)V

    goto :goto_7

    .line 1802663
    :pswitch_1
    aget-object v6, v1, v5

    iget v6, v6, LX/Bcl;->b:I

    invoke-virtual {p2, v6}, LX/BcJ;->b(I)V

    goto :goto_7

    .line 1802664
    :pswitch_2
    aget-object p3, v1, v5

    iget p3, p3, LX/Bcl;->b:I

    invoke-virtual {p2, p3, v6}, LX/BcJ;->b(ILX/1X1;)V

    goto :goto_7

    .line 1802665
    :cond_8
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_5

    :cond_9
    invoke-virtual {p1}, LX/BcP;->i()LX/BcO;

    move-result-object v1

    check-cast v1, LX/Bch;

    iget-object v1, v1, LX/Bch;->l:LX/BcQ;

    goto/16 :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(LX/BcP;LX/BcO;)V
    .locals 3

    .prologue
    .line 1802697
    check-cast p2, LX/Bch;

    .line 1802698
    iget-object v0, p0, LX/Bck;->b:LX/Bcs;

    iget-object v1, p2, LX/Bch;->k:LX/Bcp;

    iget-object v2, p2, LX/Bch;->b:Landroid/os/Handler;

    .line 1802699
    new-instance p0, LX/Bcq;

    invoke-direct {p0, v0, p1}, LX/Bcq;-><init>(LX/Bcs;LX/BcP;)V

    .line 1802700
    invoke-virtual {v1, p0}, LX/Bcp;->a(LX/Bcn;)V

    .line 1802701
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object p0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object p2

    if-ne p0, p2, :cond_0

    .line 1802702
    const/4 p0, 0x0

    invoke-virtual {v1, p0}, LX/Bcp;->a(Ljava/lang/Object;)V

    .line 1802703
    :goto_0
    return-void

    .line 1802704
    :cond_0
    new-instance p0, Lcom/facebook/components/list/fb/datasources/GraphQLConnectionChangeSetSpec$2;

    invoke-direct {p0, v0, v1}, Lcom/facebook/components/list/fb/datasources/GraphQLConnectionChangeSetSpec$2;-><init>(LX/Bcs;LX/Bcp;)V

    const p2, -0x52dc4cbe

    invoke-static {v2, p0, p2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto :goto_0
.end method

.method public final a(LX/BcP;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1802626
    check-cast p2, LX/Bcp;

    .line 1802627
    const/4 p0, 0x0

    invoke-virtual {p2, p0}, LX/Bcp;->a(Z)V

    .line 1802628
    return-void
.end method

.method public final b(LX/BcO;LX/BcO;)V
    .locals 1

    .prologue
    .line 1802622
    check-cast p1, LX/Bch;

    .line 1802623
    check-cast p2, LX/Bch;

    .line 1802624
    iget-object v0, p1, LX/Bch;->k:LX/Bcp;

    iput-object v0, p2, LX/Bch;->k:LX/Bcp;

    .line 1802625
    return-void
.end method

.method public final b(LX/BcP;LX/BcO;)V
    .locals 1

    .prologue
    .line 1802618
    check-cast p2, LX/Bch;

    .line 1802619
    iget-object v0, p2, LX/Bch;->k:LX/Bcp;

    .line 1802620
    const/4 p0, 0x0

    invoke-virtual {v0, p0}, LX/Bcp;->a(LX/Bcn;)V

    .line 1802621
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1802617
    const/4 v0, 0x1

    return v0
.end method

.method public final c(LX/BcP;)LX/Bcg;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/BcP;",
            ")",
            "LX/Bck",
            "<TTEdge;TTUserInfo;>.Builder;"
        }
    .end annotation

    .prologue
    .line 1802608
    new-instance v0, LX/Bch;

    invoke-direct {v0, p0}, LX/Bch;-><init>(LX/Bck;)V

    .line 1802609
    iget-object v1, p0, LX/Bck;->c:LX/0Zi;

    invoke-virtual {v1}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Bcg;

    .line 1802610
    if-nez v1, :cond_0

    .line 1802611
    new-instance v1, LX/Bcg;

    invoke-direct {v1, p0}, LX/Bcg;-><init>(LX/Bck;)V

    .line 1802612
    :cond_0
    iput-object v0, v1, LX/BcN;->a:LX/BcO;

    .line 1802613
    iput-object v0, v1, LX/Bcg;->a:LX/Bch;

    .line 1802614
    iget-object p0, v1, LX/Bcg;->e:Ljava/util/BitSet;

    invoke-virtual {p0}, Ljava/util/BitSet;->clear()V

    .line 1802615
    move-object v0, v1

    .line 1802616
    return-object v0
.end method

.method public final c(LX/BcP;LX/BcO;)V
    .locals 6

    .prologue
    .line 1802586
    check-cast p2, LX/Bch;

    .line 1802587
    invoke-static {}, LX/BcS;->a()LX/1np;

    move-result-object v0

    .line 1802588
    invoke-static {}, LX/BcS;->a()LX/1np;

    move-result-object v1

    .line 1802589
    invoke-static {}, LX/BcS;->a()LX/1np;

    move-result-object v2

    .line 1802590
    invoke-static {}, LX/BcS;->a()LX/1np;

    move-result-object v3

    .line 1802591
    sget-object v4, LX/Bcm;->a:LX/Bcm;

    .line 1802592
    iput-object v4, v0, LX/1np;->a:Ljava/lang/Object;

    .line 1802593
    new-instance v4, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 1802594
    iput-object v4, v1, LX/1np;->a:Ljava/lang/Object;

    .line 1802595
    new-instance v4, LX/0YT;

    invoke-direct {v4}, LX/0YT;-><init>()V

    .line 1802596
    iput-object v4, v2, LX/1np;->a:Ljava/lang/Object;

    .line 1802597
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    .line 1802598
    iput-object v4, v3, LX/1np;->a:Ljava/lang/Object;

    .line 1802599
    iget-object v4, v0, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v4

    .line 1802600
    check-cast v0, LX/Bcm;

    iput-object v0, p2, LX/Bch;->f:LX/Bcm;

    .line 1802601
    iget-object v0, v1, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 1802602
    check-cast v0, Landroid/os/Handler;

    iput-object v0, p2, LX/Bch;->b:Landroid/os/Handler;

    .line 1802603
    iget-object v0, v2, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 1802604
    check-cast v0, LX/0YT;

    iput-object v0, p2, LX/Bch;->i:LX/0YT;

    .line 1802605
    iget-object v0, v3, LX/1np;->a:Ljava/lang/Object;

    move-object v0, v0

    .line 1802606
    check-cast v0, Ljava/lang/String;

    iput-object v0, p2, LX/Bch;->j:Ljava/lang/String;

    .line 1802607
    return-void
.end method

.method public final d(LX/BcP;LX/BcO;)V
    .locals 3

    .prologue
    .line 1802579
    move-object v0, p2

    check-cast v0, LX/Bch;

    .line 1802580
    check-cast p2, LX/Bch;

    .line 1802581
    iget-object v1, p2, LX/Bch;->c:LX/2kW;

    iget v2, p2, LX/Bch;->d:I

    iget p0, p2, LX/Bch;->e:I

    .line 1802582
    new-instance p1, LX/Bcp;

    if-nez p0, :cond_0

    move p0, v2

    :cond_0
    invoke-direct {p1, v1, p0, v2}, LX/Bcp;-><init>(LX/2kW;II)V

    move-object v1, p1

    .line 1802583
    move-object v1, v1

    .line 1802584
    iput-object v1, v0, LX/Bch;->k:LX/Bcp;

    .line 1802585
    return-void
.end method

.method public final e(LX/BcP;LX/BcO;)V
    .locals 2

    .prologue
    .line 1802569
    check-cast p2, LX/Bch;

    .line 1802570
    iget-object v0, p2, LX/Bch;->k:LX/Bcp;

    iget-object v1, p2, LX/Bch;->g:LX/Exd;

    .line 1802571
    invoke-virtual {v0}, LX/Bcp;->a()V

    .line 1802572
    if-eqz v1, :cond_0

    .line 1802573
    invoke-virtual {p1}, LX/BcP;->i()LX/BcO;

    move-result-object p0

    .line 1802574
    if-nez p0, :cond_1

    .line 1802575
    :cond_0
    :goto_0
    return-void

    .line 1802576
    :cond_1
    check-cast p0, LX/Bch;

    .line 1802577
    new-instance v0, LX/Bci;

    iget-object v1, p0, LX/Bch;->m:LX/Bck;

    invoke-direct {v0, v1}, LX/Bci;-><init>(LX/Bck;)V

    move-object p0, v0

    .line 1802578
    invoke-virtual {p1, p0}, LX/BcP;->a(LX/BcR;)V

    goto :goto_0
.end method
