.class public final LX/Bvp;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/api/graphql/videocaptions/FetchVideoCaptionsGraphQLModels$FetchVideoCaptionsGraphQLModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Bu6;

.field public final synthetic b:LX/3J2;


# direct methods
.method public constructor <init>(LX/3J2;LX/Bu6;)V
    .locals 0

    .prologue
    .line 1832900
    iput-object p1, p0, LX/Bvp;->b:LX/3J2;

    iput-object p2, p0, LX/Bvp;->a:LX/Bu6;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 1832901
    return-void
.end method

.method public final synthetic onSuccess(Ljava/lang/Object;)V
    .locals 14
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1832902
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v13, 0x1

    const/4 v2, 0x0

    .line 1832903
    :try_start_0
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1832904
    check-cast v0, Lcom/facebook/api/graphql/videocaptions/FetchVideoCaptionsGraphQLModels$FetchVideoCaptionsGraphQLModel;

    invoke-virtual {v0}, Lcom/facebook/api/graphql/videocaptions/FetchVideoCaptionsGraphQLModels$FetchVideoCaptionsGraphQLModel;->a()LX/2uF;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/2uF;->a(I)LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const/4 v3, 0x1

    const v4, 0x20defee2

    invoke-static {v1, v0, v3, v4}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 1832905
    :goto_0
    invoke-virtual {v0}, LX/39O;->c()I

    move-result v3

    .line 1832906
    new-array v4, v3, [LX/7QR;

    move v1, v2

    .line 1832907
    :goto_1
    if-ge v1, v3, :cond_1

    .line 1832908
    invoke-virtual {v0, v1}, LX/2uF;->a(I)LX/1vs;

    move-result-object v5

    iget-object v6, v5, LX/1vs;->a:LX/15i;

    iget v5, v5, LX/1vs;->b:I

    .line 1832909
    invoke-virtual {v0, v1}, LX/2uF;->a(I)LX/1vs;

    move-result-object v7

    iget-object v8, v7, LX/1vs;->a:LX/15i;

    iget v7, v7, LX/1vs;->b:I

    .line 1832910
    invoke-virtual {v0, v1}, LX/2uF;->a(I)LX/1vs;

    move-result-object v9

    iget-object v10, v9, LX/1vs;->a:LX/15i;

    iget v9, v9, LX/1vs;->b:I

    .line 1832911
    new-instance v11, LX/7QR;

    const/4 v12, 0x2

    invoke-virtual {v6, v5, v12}, LX/15i;->j(II)I

    move-result v5

    invoke-virtual {v8, v7, v13}, LX/15i;->j(II)I

    move-result v6

    invoke-virtual {v10, v9, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v11, v5, v6, v7}, LX/7QR;-><init>(IILjava/lang/String;)V

    aput-object v11, v4, v1

    .line 1832912
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1832913
    :cond_0
    :try_start_1
    invoke-static {}, LX/2uF;->h()LX/2uF;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    goto :goto_0

    .line 1832914
    :catch_0
    :goto_2
    iget-object v0, p0, LX/Bvp;->b:LX/3J2;

    iget-object v0, v0, LX/3J2;->c:LX/03V;

    const-string v1, "VideoSubtitles"

    const-string v2, "Invalid results on fetching subtitles GraphQL."

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1832915
    :goto_3
    return-void

    .line 1832916
    :cond_1
    iget-object v0, p0, LX/Bvp;->a:LX/Bu6;

    new-instance v1, LX/7QP;

    invoke-direct {v1, v4}, LX/7QP;-><init>([LX/7QR;)V

    invoke-interface {v0, v1}, LX/Bu6;->a(LX/7QP;)V

    goto :goto_3

    .line 1832917
    :catch_1
    goto :goto_2
.end method
