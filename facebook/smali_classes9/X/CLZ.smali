.class public LX/CLZ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/CMh;

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Landroid/content/res/Resources;

.field private final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final e:I


# direct methods
.method public constructor <init>(LX/CMh;LX/0Or;Landroid/content/res/Resources;)V
    .locals 2
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/messaging/customthreads/annotations/CanViewThreadCustomization;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CMh;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Landroid/content/res/Resources;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1879182
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1879183
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/CLZ;->d:Ljava/util/List;

    .line 1879184
    iput-object p1, p0, LX/CLZ;->a:LX/CMh;

    .line 1879185
    iput-object p2, p0, LX/CLZ;->b:LX/0Or;

    .line 1879186
    iput-object p3, p0, LX/CLZ;->c:Landroid/content/res/Resources;

    .line 1879187
    iget-object v0, p0, LX/CLZ;->c:Landroid/content/res/Resources;

    const v1, 0x7f0a0226

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, LX/CLZ;->e:I

    .line 1879188
    return-void
.end method
