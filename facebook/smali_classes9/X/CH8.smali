.class public LX/CH8;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/CH8;


# instance fields
.field private final a:LX/0tX;

.field private final b:Ljava/util/concurrent/Executor;


# direct methods
.method public constructor <init>(LX/0tX;Ljava/util/concurrent/Executor;)V
    .locals 0
    .param p2    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThreadImmediate;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1866309
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1866310
    iput-object p1, p0, LX/CH8;->a:LX/0tX;

    .line 1866311
    iput-object p2, p0, LX/CH8;->b:Ljava/util/concurrent/Executor;

    .line 1866312
    return-void
.end method

.method public static a(LX/0QB;)LX/CH8;
    .locals 5

    .prologue
    .line 1866296
    sget-object v0, LX/CH8;->c:LX/CH8;

    if-nez v0, :cond_1

    .line 1866297
    const-class v1, LX/CH8;

    monitor-enter v1

    .line 1866298
    :try_start_0
    sget-object v0, LX/CH8;->c:LX/CH8;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1866299
    if-eqz v2, :cond_0

    .line 1866300
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1866301
    new-instance p0, LX/CH8;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {v0}, LX/0W1;->a(LX/0QB;)LX/0Tf;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/Executor;

    invoke-direct {p0, v3, v4}, LX/CH8;-><init>(LX/0tX;Ljava/util/concurrent/Executor;)V

    .line 1866302
    move-object v0, p0

    .line 1866303
    sput-object v0, LX/CH8;->c:LX/CH8;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1866304
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1866305
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1866306
    :cond_1
    sget-object v0, LX/CH8;->c:LX/CH8;

    return-object v0

    .line 1866307
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1866308
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;LX/0Ve;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0Ve",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticlesTrackerModel;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1866313
    new-instance v0, LX/B5U;

    invoke-direct {v0}, LX/B5U;-><init>()V

    move-object v0, v0

    .line 1866314
    const-string v1, "ia_tracker_node_id"

    invoke-virtual {v0, v1, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "ia_webview_share_url"

    invoke-virtual {v0, v1, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/B5U;

    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    const-wide/32 v2, 0x240c8400

    invoke-virtual {v0, v2, v3}, LX/0zO;->a(J)LX/0zO;

    move-result-object v0

    const/4 v1, 0x1

    .line 1866315
    iput-boolean v1, v0, LX/0zO;->p:Z

    .line 1866316
    move-object v0, v0

    .line 1866317
    sget-object v1, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0, v1}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v0

    sget-object v1, LX/0zS;->a:LX/0zS;

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    .line 1866318
    iget-object v1, p0, LX/CH8;->a:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 1866319
    iget-object v1, p0, LX/CH8;->b:Ljava/util/concurrent/Executor;

    invoke-static {v0, p3, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1866320
    return-void
.end method
