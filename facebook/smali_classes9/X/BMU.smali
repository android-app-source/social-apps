.class public LX/BMU;
.super Ljava/lang/Object;
.source ""


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 1777409
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1777410
    return-void
.end method

.method public static a(Lcom/facebook/productionprompts/model/ProductionPrompt;)Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1777400
    invoke-virtual {p0}, Lcom/facebook/productionprompts/model/ProductionPrompt;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/productionprompts/model/ProductionPrompt;->h()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1777401
    new-instance v0, LX/5m9;

    invoke-direct {v0}, LX/5m9;-><init>()V

    invoke-virtual {p0}, Lcom/facebook/productionprompts/model/ProductionPrompt;->g()Ljava/lang/String;

    move-result-object v1

    .line 1777402
    iput-object v1, v0, LX/5m9;->f:Ljava/lang/String;

    .line 1777403
    move-object v0, v0

    .line 1777404
    invoke-virtual {p0}, Lcom/facebook/productionprompts/model/ProductionPrompt;->h()Ljava/lang/String;

    move-result-object v1

    .line 1777405
    iput-object v1, v0, LX/5m9;->h:Ljava/lang/String;

    .line 1777406
    move-object v0, v0

    .line 1777407
    invoke-virtual {v0}, LX/5m9;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v0

    .line 1777408
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;Lcom/facebook/productionprompts/model/ProductionPrompt;)V
    .locals 6

    .prologue
    .line 1777373
    invoke-virtual {p1}, Lcom/facebook/productionprompts/model/ProductionPrompt;->d()Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 1777374
    invoke-virtual {p1}, Lcom/facebook/productionprompts/model/ProductionPrompt;->d()Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v0

    .line 1777375
    :goto_0
    move-object v0, v0

    .line 1777376
    invoke-virtual {p1}, Lcom/facebook/productionprompts/model/ProductionPrompt;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 1777377
    invoke-virtual {p1}, Lcom/facebook/productionprompts/model/ProductionPrompt;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/89G;->a(Ljava/lang/String;)LX/89G;

    move-result-object v1

    invoke-virtual {v1}, LX/89G;->b()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v1

    .line 1777378
    :goto_1
    move-object v1, v1

    .line 1777379
    invoke-static {p1}, LX/BMU;->a(Lcom/facebook/productionprompts/model/ProductionPrompt;)Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v2

    .line 1777380
    invoke-virtual {p1}, Lcom/facebook/productionprompts/model/ProductionPrompt;->C()LX/0Px;

    move-result-object v3

    if-eqz v3, :cond_7

    .line 1777381
    invoke-virtual {p1}, Lcom/facebook/productionprompts/model/ProductionPrompt;->C()LX/0Px;

    move-result-object v3

    .line 1777382
    :goto_2
    move-object v3, v3

    .line 1777383
    invoke-virtual {p1}, Lcom/facebook/productionprompts/model/ProductionPrompt;->y()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_8

    .line 1777384
    new-instance v4, LX/173;

    invoke-direct {v4}, LX/173;-><init>()V

    invoke-virtual {p1}, Lcom/facebook/productionprompts/model/ProductionPrompt;->y()Ljava/lang/String;

    move-result-object v5

    .line 1777385
    iput-object v5, v4, LX/173;->f:Ljava/lang/String;

    .line 1777386
    move-object v4, v4

    .line 1777387
    invoke-virtual {v4}, LX/173;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v4

    .line 1777388
    :goto_3
    move-object v4, v4

    .line 1777389
    if-eqz v0, :cond_0

    .line 1777390
    invoke-virtual {p0, v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setMinutiaeObjectTag(Lcom/facebook/composer/minutiae/model/MinutiaeObject;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 1777391
    :cond_0
    if-eqz v1, :cond_1

    .line 1777392
    invoke-virtual {p0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialShareParams(Lcom/facebook/ipc/composer/intent/ComposerShareParams;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 1777393
    :cond_1
    if-eqz v2, :cond_2

    .line 1777394
    invoke-static {}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->newBuilder()LX/5RO;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/5RO;->b(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)LX/5RO;

    move-result-object v0

    invoke-virtual {v0}, LX/5RO;->b()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialLocationInfo(Lcom/facebook/ipc/composer/model/ComposerLocationInfo;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 1777395
    :cond_2
    if-eqz v3, :cond_3

    .line 1777396
    invoke-virtual {p0, v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTaggedUsers(LX/0Px;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 1777397
    :cond_3
    if-eqz v4, :cond_4

    .line 1777398
    invoke-virtual {p0, v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialText(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 1777399
    :cond_4
    return-void

    :cond_5
    const/4 v0, 0x0

    goto :goto_0

    :cond_6
    const/4 v1, 0x0

    goto :goto_1

    :cond_7
    const/4 v3, 0x0

    goto :goto_2

    :cond_8
    const/4 v4, 0x0

    goto :goto_3
.end method
