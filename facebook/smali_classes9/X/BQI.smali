.class public final LX/BQI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field public final synthetic a:Landroid/content/Context;

.field public final synthetic b:LX/BQN;


# direct methods
.method public constructor <init>(LX/BQN;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1782001
    iput-object p1, p0, LX/BQI;->b:LX/BQN;

    iput-object p2, p0, LX/BQI;->a:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 6

    .prologue
    .line 1782002
    iget-object v0, p0, LX/BQI;->b:LX/BQN;

    iget-object v0, v0, LX/BQN;->b:LX/1mR;

    const-string v1, "timeline_fetch_info_review"

    const-string v2, "timeline_fetch_header"

    const-string v3, "timeline_fetch_first_units_classic"

    const-string v4, "timeline_fetch_first_units_plutonium"

    const-string v5, "timeline_fetch_section"

    invoke-static {v1, v2, v3, v4, v5}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1mR;->a(Ljava/util/Set;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1782003
    iget-object v1, p0, LX/BQI;->b:LX/BQN;

    iget-object v2, p0, LX/BQI;->a:Landroid/content/Context;

    const-string v3, "Clearing cache"

    invoke-static {v1, v2, v3}, LX/BQN;->a$redex0(LX/BQN;Landroid/content/Context;Ljava/lang/String;)V

    .line 1782004
    new-instance v1, LX/BQH;

    invoke-direct {v1, p0}, LX/BQH;-><init>(LX/BQI;)V

    iget-object v2, p0, LX/BQI;->b:LX/BQN;

    iget-object v2, v2, LX/BQN;->c:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1782005
    const/4 v0, 0x1

    return v0
.end method
