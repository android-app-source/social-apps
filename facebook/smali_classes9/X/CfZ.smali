.class public final LX/CfZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionSuggestedEventsQueryModel;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:I

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/0m9;

.field public final synthetic d:Ljava/lang/String;

.field public final synthetic e:Lcom/facebook/reaction/ReactionUtil;


# direct methods
.method public constructor <init>(Lcom/facebook/reaction/ReactionUtil;ILjava/lang/String;LX/0m9;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1926393
    iput-object p1, p0, LX/CfZ;->e:Lcom/facebook/reaction/ReactionUtil;

    iput p2, p0, LX/CfZ;->a:I

    iput-object p3, p0, LX/CfZ;->b:Ljava/lang/String;

    iput-object p4, p0, LX/CfZ;->c:LX/0m9;

    iput-object p5, p0, LX/CfZ;->d:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1926381
    new-instance v0, LX/9qQ;

    invoke-direct {v0}, LX/9qQ;-><init>()V

    move-object v0, v0

    .line 1926382
    const-string v1, "reaction_result_count"

    iget v2, p0, LX/CfZ;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1926383
    const-string v1, "reaction_after_cursor"

    iget-object v2, p0, LX/CfZ;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1926384
    const-string v1, "suggestion_context"

    iget-object v2, p0, LX/CfZ;->c:LX/0m9;

    invoke-virtual {v2}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1926385
    iget-object v1, p0, LX/CfZ;->e:Lcom/facebook/reaction/ReactionUtil;

    iget-object v2, p0, LX/CfZ;->d:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lcom/facebook/reaction/ReactionUtil;->a(LX/0gW;Ljava/lang/String;)V

    .line 1926386
    const-string v1, "reaction_context"

    iget-object v2, p0, LX/CfZ;->e:Lcom/facebook/reaction/ReactionUtil;

    iget-object v3, p0, LX/CfZ;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/facebook/reaction/ReactionUtil;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1926387
    const-string v1, "reaction_paginated_components_count"

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1926388
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    sget-object v1, LX/0zS;->c:LX/0zS;

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    .line 1926389
    iget-object v1, p0, LX/CfZ;->d:Ljava/lang/String;

    invoke-static {v1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1926390
    iget-object v1, p0, LX/CfZ;->d:Ljava/lang/String;

    .line 1926391
    iput-object v1, v0, LX/0zO;->z:Ljava/lang/String;

    .line 1926392
    :cond_0
    iget-object v1, p0, LX/CfZ;->e:Lcom/facebook/reaction/ReactionUtil;

    iget-object v1, v1, Lcom/facebook/reaction/ReactionUtil;->e:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    return-object v0
.end method
