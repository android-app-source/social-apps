.class public final enum LX/BZO;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/BZO;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/BZO;

.field public static final enum Forehead:LX/BZO;

.field public static final enum ForeheadOld:LX/BZO;

.field public static final enum Forehead_Chin:LX/BZO;

.field public static final enum OnlyFace:LX/BZO;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1797224
    new-instance v0, LX/BZO;

    const-string v1, "OnlyFace"

    invoke-direct {v0, v1, v2}, LX/BZO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BZO;->OnlyFace:LX/BZO;

    .line 1797225
    new-instance v0, LX/BZO;

    const-string v1, "ForeheadOld"

    invoke-direct {v0, v1, v3}, LX/BZO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BZO;->ForeheadOld:LX/BZO;

    .line 1797226
    new-instance v0, LX/BZO;

    const-string v1, "Forehead"

    invoke-direct {v0, v1, v4}, LX/BZO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BZO;->Forehead:LX/BZO;

    .line 1797227
    new-instance v0, LX/BZO;

    const-string v1, "Forehead_Chin"

    invoke-direct {v0, v1, v5}, LX/BZO;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BZO;->Forehead_Chin:LX/BZO;

    .line 1797228
    const/4 v0, 0x4

    new-array v0, v0, [LX/BZO;

    sget-object v1, LX/BZO;->OnlyFace:LX/BZO;

    aput-object v1, v0, v2

    sget-object v1, LX/BZO;->ForeheadOld:LX/BZO;

    aput-object v1, v0, v3

    sget-object v1, LX/BZO;->Forehead:LX/BZO;

    aput-object v1, v0, v4

    sget-object v1, LX/BZO;->Forehead_Chin:LX/BZO;

    aput-object v1, v0, v5

    sput-object v0, LX/BZO;->$VALUES:[LX/BZO;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1797229
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/BZO;
    .locals 1

    .prologue
    .line 1797230
    const-class v0, LX/BZO;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/BZO;

    return-object v0
.end method

.method public static values()[LX/BZO;
    .locals 1

    .prologue
    .line 1797231
    sget-object v0, LX/BZO;->$VALUES:[LX/BZO;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/BZO;

    return-object v0
.end method
