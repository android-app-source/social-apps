.class public final LX/Are;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Ard;


# instance fields
.field public final synthetic a:LX/Arh;


# direct methods
.method public constructor <init>(LX/Arh;)V
    .locals 0

    .prologue
    .line 1719338
    iput-object p1, p0, LX/Are;->a:LX/Arh;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 1719339
    iget-object v0, p0, LX/Are;->a:LX/Arh;

    iget-object v0, v0, LX/Arh;->f:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    sget-object v1, LX/Arh;->d:LX/0jK;

    iget-object v2, p0, LX/Are;->a:LX/Arh;

    iget-object v2, v2, LX/Arh;->n:LX/ArL;

    invoke-static {v0, v1, v2}, LX/Arx;->a(LX/0il;LX/0jK;LX/ArL;)V

    .line 1719340
    return-void
.end method

.method public final a(Landroid/view/MotionEvent;)V
    .locals 5

    .prologue
    .line 1719341
    iget-object v0, p0, LX/Are;->a:LX/Arh;

    iget-object v0, v0, LX/Arh;->u:LX/Art;

    .line 1719342
    iget-boolean v1, v0, LX/Art;->h:Z

    move v0, v1

    .line 1719343
    if-eqz v0, :cond_0

    .line 1719344
    :goto_0
    return-void

    .line 1719345
    :cond_0
    iget-object v0, p0, LX/Are;->a:LX/Arh;

    iget-object v0, v0, LX/Arh;->j:LX/Arq;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    iget-object v2, p0, LX/Are;->a:LX/Arh;

    iget-object v2, v2, LX/Arh;->e:Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;

    invoke-virtual {v2}, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->getWidth()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iget-object v3, p0, LX/Are;->a:LX/Arh;

    iget-object v3, v3, LX/Arh;->e:Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;

    invoke-virtual {v3}, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->getHeight()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    .line 1719346
    :try_start_0
    iget-object v3, v0, LX/Arq;->D:LX/6Ia;

    if-eqz v3, :cond_1

    iget-object v3, v0, LX/Arq;->D:LX/6Ia;

    invoke-interface {v3}, LX/6Ia;->c()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, v0, LX/Arq;->D:LX/6Ia;

    invoke-interface {v3}, LX/6Ia;->a()LX/6IP;

    move-result-object v3

    invoke-interface {v3}, LX/6IP;->j()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    if-nez v3, :cond_2

    .line 1719347
    :cond_1
    :goto_1
    iget-object v0, p0, LX/Are;->a:LX/Arh;

    iget-object v0, v0, LX/Arh;->u:LX/Art;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    const/4 p0, 0x1

    const/4 p1, 0x0

    const/4 v4, 0x0

    .line 1719348
    iget-boolean v3, v0, LX/Art;->h:Z

    if-eqz v3, :cond_3

    move v3, v4

    .line 1719349
    :goto_2
    move v0, v3

    .line 1719350
    const-string v1, "Supposed to start a new animation"

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    goto :goto_0

    .line 1719351
    :catch_0
    move-exception v3

    .line 1719352
    const-string v4, "pr_camera_get_characteristics"

    invoke-static {v0, v4, v3}, LX/Arq;->a$redex0(LX/Arq;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 1719353
    :cond_2
    iget-object v3, v0, LX/Arq;->D:LX/6Ia;

    invoke-interface {v3, v1, v2}, LX/6Ia;->a(FF)V

    goto :goto_1

    .line 1719354
    :cond_3
    iput-boolean p0, v0, LX/Art;->h:Z

    .line 1719355
    iget-object v3, v0, LX/Art;->e:LX/0zw;

    invoke-virtual {v3}, LX/0zw;->a()Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/facebook/friendsharing/inspiration/view/InspirationRingView;

    invoke-virtual {v3, v4}, Lcom/facebook/friendsharing/inspiration/view/InspirationRingView;->setVisibility(I)V

    .line 1719356
    iget-object v3, v0, LX/Art;->f:LX/0zw;

    invoke-virtual {v3}, LX/0zw;->a()Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1719357
    iget-object v3, v0, LX/Art;->e:LX/0zw;

    invoke-virtual {v3}, LX/0zw;->a()Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/facebook/friendsharing/inspiration/view/InspirationRingView;

    invoke-virtual {v3, p1}, Lcom/facebook/friendsharing/inspiration/view/InspirationRingView;->setAlpha(F)V

    .line 1719358
    iget-object v3, v0, LX/Art;->f:LX/0zw;

    invoke-virtual {v3}, LX/0zw;->a()Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    invoke-virtual {v3, p1}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 1719359
    iget-object v3, v0, LX/Art;->g:Lcom/facebook/friendsharing/inspiration/capture/InspirationFocusAnimation$AnimationRunnable;

    iput v1, v3, Lcom/facebook/friendsharing/inspiration/capture/InspirationFocusAnimation$AnimationRunnable;->a:F

    .line 1719360
    iget-object v3, v0, LX/Art;->g:Lcom/facebook/friendsharing/inspiration/capture/InspirationFocusAnimation$AnimationRunnable;

    iput v2, v3, Lcom/facebook/friendsharing/inspiration/capture/InspirationFocusAnimation$AnimationRunnable;->b:F

    .line 1719361
    iget-object v3, v0, LX/Art;->b:Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;

    iget-object v4, v0, LX/Art;->g:Lcom/facebook/friendsharing/inspiration/capture/InspirationFocusAnimation$AnimationRunnable;

    invoke-virtual {v3, v4}, Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;->post(Ljava/lang/Runnable;)Z

    move v3, p0

    .line 1719362
    goto :goto_2
.end method
