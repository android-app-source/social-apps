.class public LX/Azv;
.super Landroid/widget/FrameLayout;
.source ""


# instance fields
.field public final a:Lcom/facebook/fbui/glyph/GlyphView;

.field public final b:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1733068
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/Azv;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1733069
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1733081
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/Azv;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1733082
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1733076
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1733077
    const v0, 0x7f0307b6

    invoke-static {p1, v0, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1733078
    const v0, 0x7f0d1480

    invoke-virtual {p0, v0}, LX/Azv;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, LX/Azv;->a:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1733079
    const v0, 0x7f0d1481

    invoke-virtual {p0, v0}, LX/Azv;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/Azv;->b:Landroid/view/View;

    .line 1733080
    return-void
.end method


# virtual methods
.method public setSelected(Z)V
    .locals 3

    .prologue
    .line 1733070
    if-eqz p1, :cond_0

    .line 1733071
    iget-object v0, p0, LX/Azv;->a:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {p0}, LX/Azv;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00d2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 1733072
    iget-object v0, p0, LX/Azv;->b:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1733073
    :goto_0
    return-void

    .line 1733074
    :cond_0
    iget-object v0, p0, LX/Azv;->a:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {p0}, LX/Azv;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00e6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 1733075
    iget-object v0, p0, LX/Azv;->b:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method
