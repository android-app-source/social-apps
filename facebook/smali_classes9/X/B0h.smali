.class public LX/B0h;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/B0N;


# instance fields
.field private final a:Lcom/facebook/flatbuffers/MutableFlattenable;

.field private final b:LX/95b;

.field private final c:Z

.field private final d:LX/B0d;

.field private final e:Lcom/facebook/graphql/cursor/edgestore/PageInfo;


# direct methods
.method public constructor <init>(Lcom/facebook/flatbuffers/MutableFlattenable;LX/95b;Lcom/facebook/graphql/cursor/edgestore/PageInfo;ZLX/B0d;)V
    .locals 1

    .prologue
    .line 1734124
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1734125
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/flatbuffers/MutableFlattenable;

    iput-object v0, p0, LX/B0h;->a:Lcom/facebook/flatbuffers/MutableFlattenable;

    .line 1734126
    invoke-interface {p1}, Lcom/facebook/flatbuffers/MutableFlattenable;->q_()LX/15i;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1734127
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/95b;

    iput-object v0, p0, LX/B0h;->b:LX/95b;

    .line 1734128
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/cursor/edgestore/PageInfo;

    iput-object v0, p0, LX/B0h;->e:Lcom/facebook/graphql/cursor/edgestore/PageInfo;

    .line 1734129
    iput-boolean p4, p0, LX/B0h;->c:Z

    .line 1734130
    invoke-static {p5}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/B0d;

    iput-object v0, p0, LX/B0h;->d:LX/B0d;

    .line 1734131
    return-void
.end method


# virtual methods
.method public final a()Ljava/nio/ByteBuffer;
    .locals 1

    .prologue
    .line 1734132
    iget-object v0, p0, LX/B0h;->a:Lcom/facebook/flatbuffers/MutableFlattenable;

    invoke-interface {v0}, Lcom/facebook/flatbuffers/MutableFlattenable;->q_()LX/15i;

    move-result-object v0

    invoke-virtual {v0}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    return-object v0
.end method

.method public final b()LX/95b;
    .locals 1

    .prologue
    .line 1734133
    iget-object v0, p0, LX/B0h;->b:LX/95b;

    return-object v0
.end method

.method public final c()Lcom/facebook/graphql/cursor/edgestore/PageInfo;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1734134
    iget-object v0, p0, LX/B0h;->e:Lcom/facebook/graphql/cursor/edgestore/PageInfo;

    return-object v0
.end method

.method public final d()LX/B0d;
    .locals 1

    .prologue
    .line 1734135
    iget-object v0, p0, LX/B0h;->d:LX/B0d;

    return-object v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 1734136
    iget-boolean v0, p0, LX/B0h;->c:Z

    return v0
.end method
