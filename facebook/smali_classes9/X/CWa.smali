.class public final LX/CWa;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/CTM;

.field public final synthetic b:Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldContactinfoView;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldContactinfoView;LX/CTM;)V
    .locals 0

    .prologue
    .line 1908375
    iput-object p1, p0, LX/CWa;->b:Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldContactinfoView;

    iput-object p2, p0, LX/CWa;->a:LX/CTM;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x2

    const v0, -0x3f20a123    # -6.98033f

    invoke-static {v3, v4, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1908370
    iget-object v0, p0, LX/CWa;->a:LX/CTM;

    iget-object v0, v0, LX/CTK;->j:Ljava/util/HashMap;

    const-string v2, "verified_email"

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/CWa;->a:LX/CTM;

    iget-object v0, v0, LX/CTK;->j:Ljava/util/HashMap;

    const-string v2, "verified_email"

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-le v0, v4, :cond_0

    .line 1908371
    new-instance v0, LX/3Af;

    iget-object v2, p0, LX/CWa;->b:Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldContactinfoView;

    invoke-virtual {v2}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldContactinfoView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, LX/3Af;-><init>(Landroid/content/Context;)V

    .line 1908372
    iget-object v2, p0, LX/CWa;->b:Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldContactinfoView;

    iget-object v2, v2, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldContactinfoView;->i:LX/34b;

    invoke-virtual {v0, v2}, LX/3Af;->a(LX/1OM;)V

    .line 1908373
    invoke-virtual {v0}, LX/3Af;->show()V

    .line 1908374
    :cond_0
    const v0, -0x395dbdf5

    invoke-static {v3, v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
