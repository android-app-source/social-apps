.class public final LX/CAd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/CAZ;

.field public final synthetic b:LX/0Or;

.field public final synthetic c:LX/17Y;

.field public final synthetic d:Landroid/os/Bundle;

.field public final synthetic e:Lcom/facebook/content/SecureContextHelper;


# direct methods
.method public constructor <init>(LX/CAZ;LX/0Or;LX/17Y;Landroid/os/Bundle;Lcom/facebook/content/SecureContextHelper;)V
    .locals 0

    .prologue
    .line 1855562
    iput-object p1, p0, LX/CAd;->a:LX/CAZ;

    iput-object p2, p0, LX/CAd;->b:LX/0Or;

    iput-object p3, p0, LX/CAd;->c:LX/17Y;

    iput-object p4, p0, LX/CAd;->d:Landroid/os/Bundle;

    iput-object p5, p0, LX/CAd;->e:Lcom/facebook/content/SecureContextHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v5, 0x2

    const v0, -0x8790b5c

    invoke-static {v5, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1855563
    iget-object v1, p0, LX/CAd;->a:LX/CAZ;

    if-eqz v1, :cond_0

    .line 1855564
    iget-object v1, p0, LX/CAd;->a:LX/CAZ;

    .line 1855565
    iget-object v3, v1, LX/CAZ;->b:LX/CAa;

    iget-object v3, v3, LX/CAa;->d:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/D2D;

    sget-object v6, LX/D2C;->CALL_TO_ACTION_BUTTON_CLICKED:LX/D2C;

    iget-object v4, v1, LX/CAZ;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v4

    if-nez v4, :cond_2

    const-string v4, ""

    :goto_0
    invoke-virtual {v3, v6, v4}, LX/D2D;->b(LX/D2C;Ljava/lang/String;)V

    .line 1855566
    :cond_0
    sget-object v1, LX/0ax;->bE:Ljava/lang/String;

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LX/CAd;->b:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1855567
    iget-object v2, p0, LX/CAd;->c:LX/17Y;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v2, v3, v1}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 1855568
    if-eqz v1, :cond_1

    .line 1855569
    iget-object v2, p0, LX/CAd;->d:Landroid/os/Bundle;

    invoke-virtual {v1, v2}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 1855570
    iget-object v2, p0, LX/CAd;->e:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v2, v1, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1855571
    :cond_1
    const v1, -0x764ed492

    invoke-static {v5, v5, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1855572
    :cond_2
    iget-object v4, v1, LX/CAZ;->a:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method
