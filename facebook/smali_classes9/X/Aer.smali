.class public final LX/Aer;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoAnnouncementsQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Aes;


# direct methods
.method public constructor <init>(LX/Aes;)V
    .locals 0

    .prologue
    .line 1698239
    iput-object p1, p0, LX/Aer;->a:LX/Aes;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 1698226
    iget-object v2, p0, LX/Aer;->a:LX/Aes;

    monitor-enter v2

    .line 1698227
    :try_start_0
    iget-object v1, p0, LX/Aer;->a:LX/Aes;

    iget-object v1, v1, LX/Aes;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/Aer;->a:LX/Aes;

    iget-object v1, v1, LX/Aes;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v1}, Lcom/google/common/util/concurrent/ListenableFuture;->isCancelled()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1698228
    :cond_0
    monitor-exit v2

    .line 1698229
    :goto_0
    return-void

    .line 1698230
    :cond_1
    iget-object v1, p0, LX/Aer;->a:LX/Aes;

    iget-object v3, v1, LX/Aes;->i:LX/03V;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, LX/Aes;->f:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "_graphFailure"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v5, "Failed to get announcement events for "

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, LX/Aer;->a:LX/Aes;

    iget-object v5, v5, LX/AeQ;->c:Ljava/lang/String;

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, LX/Aer;->a:LX/Aes;

    iget-object v1, v1, LX/AeQ;->c:Ljava/lang/String;

    :goto_1
    invoke-virtual {v3, v4, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1698231
    iget-object v1, p0, LX/Aer;->a:LX/Aes;

    iget-boolean v1, v1, LX/AeQ;->e:Z

    if-nez v1, :cond_4

    .line 1698232
    :goto_2
    iget-object v1, p0, LX/Aer;->a:LX/Aes;

    const/4 v3, 0x1

    .line 1698233
    iput-boolean v3, v1, LX/Aes;->e:Z

    .line 1698234
    iget-object v1, p0, LX/Aer;->a:LX/Aes;

    iget-object v1, v1, LX/AeQ;->b:LX/AeV;

    if-eqz v1, :cond_2

    .line 1698235
    iget-object v1, p0, LX/Aer;->a:LX/Aes;

    iget-object v1, v1, LX/AeQ;->b:LX/AeV;

    iget-object v3, p0, LX/Aer;->a:LX/Aes;

    invoke-virtual {v3}, LX/Aes;->g()LX/AeN;

    move-result-object v3

    invoke-virtual {v1, v3, v0}, LX/AeV;->a(LX/AeN;Z)V

    .line 1698236
    :cond_2
    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1698237
    :cond_3
    :try_start_1
    const-string v1, "no story id"
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 1698238
    :cond_4
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 8
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1698240
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 1698241
    iget-object v1, p0, LX/Aer;->a:LX/Aes;

    monitor-enter v1

    .line 1698242
    :try_start_0
    iget-object v0, p0, LX/Aer;->a:LX/Aes;

    iget-object v0, v0, LX/Aes;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Aer;->a:LX/Aes;

    iget-object v0, v0, LX/Aes;->l:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1698243
    :cond_0
    monitor-exit v1

    .line 1698244
    :goto_0
    return-void

    .line 1698245
    :cond_1
    if-nez p1, :cond_2

    .line 1698246
    monitor-exit v1

    goto :goto_0

    .line 1698247
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1698248
    :cond_2
    :try_start_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1698249
    check-cast v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoAnnouncementsQueryModel;

    .line 1698250
    if-nez v0, :cond_3

    .line 1698251
    monitor-exit v1

    goto :goto_0

    .line 1698252
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FetchLiveVideoAnnouncementsQueryModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1698253
    if-nez v0, :cond_4

    .line 1698254
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 1698255
    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    throw v0

    .line 1698256
    :cond_4
    const/4 v3, 0x0

    const v4, -0x38cd6

    invoke-static {v2, v0, v3, v4}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    .line 1698257
    if-eqz v0, :cond_5

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    .line 1698258
    :goto_1
    if-nez v0, :cond_6

    .line 1698259
    monitor-exit v1

    goto :goto_0

    .line 1698260
    :cond_5
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_1

    .line 1698261
    :cond_6
    iget-object v2, p0, LX/Aer;->a:LX/Aes;

    const/4 v3, 0x1

    .line 1698262
    iput-boolean v3, v2, LX/Aes;->e:Z

    .line 1698263
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    .line 1698264
    invoke-virtual {v0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v3

    :cond_7
    :goto_2
    invoke-interface {v3}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v3}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v5, v0, LX/1vs;->b:I

    .line 1698265
    const/16 v0, 0x9

    invoke-virtual {v4, v5, v0}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v6

    .line 1698266
    const/4 v0, 0x7

    invoke-virtual {v4, v5, v0}, LX/15i;->j(II)I

    move-result v7

    .line 1698267
    iget-object v0, p0, LX/Aer;->a:LX/Aes;

    iget-object v0, v0, LX/Aes;->j:Ljava/util/Map;

    invoke-interface {v0, v6}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, LX/Aer;->a:LX/Aes;

    iget-object v0, v0, LX/Aes;->j:Ljava/util/Map;

    invoke-interface {v0, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, v7, :cond_7

    .line 1698268
    :cond_8
    iget-object v0, p0, LX/Aer;->a:LX/Aes;

    iget-object v0, v0, LX/Aes;->j:Ljava/util/Map;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v0, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1698269
    const/4 v0, 0x0

    invoke-static {v4, v5}, LX/Aen;->a(LX/15i;I)LX/Aen;

    move-result-object v4

    invoke-interface {v2, v0, v4}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_2

    .line 1698270
    :cond_9
    iget-object v0, p0, LX/Aer;->a:LX/Aes;

    iget-object v0, v0, LX/AeQ;->b:LX/AeV;

    if-eqz v0, :cond_a

    .line 1698271
    iget-object v0, p0, LX/Aer;->a:LX/Aes;

    iget-object v0, v0, LX/AeQ;->b:LX/AeV;

    iget-object v3, p0, LX/Aer;->a:LX/Aes;

    invoke-virtual {v3}, LX/Aes;->g()LX/AeN;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v2, v4}, LX/AeV;->a(LX/AeN;Ljava/util/List;Z)V

    .line 1698272
    :cond_a
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_0
.end method
