.class public final LX/Bbw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Zz;


# instance fields
.field public final synthetic a:LX/0Px;

.field public final synthetic b:LX/Bbx;


# direct methods
.method public constructor <init>(LX/Bbx;LX/0Px;)V
    .locals 0

    .prologue
    .line 1801286
    iput-object p1, p0, LX/Bbw;->b:LX/Bbx;

    iput-object p2, p0, LX/Bbw;->a:LX/0Px;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/6al;)V
    .locals 12

    .prologue
    .line 1801287
    iget-object v1, p0, LX/Bbw;->a:LX/0Px;

    .line 1801288
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v4

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-ge v3, v4, :cond_1

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$PlaceListDetailsModel$ListItemsModel$NodesModel;

    .line 1801289
    invoke-virtual {v2}, Lcom/facebook/checkin/socialsearch/graphql/SocialSearchGraphQLModels$PlaceListDetailsModel$ListItemsModel$NodesModel;->j()Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceRecommendationPageFieldsModel;

    move-result-object v2

    .line 1801290
    if-eqz v2, :cond_0

    .line 1801291
    invoke-virtual {v2}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceRecommendationPageFieldsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    move-result-object v5

    .line 1801292
    if-eqz v5, :cond_0

    .line 1801293
    new-instance v6, LX/699;

    invoke-direct {v6}, LX/699;-><init>()V

    new-instance v7, Lcom/facebook/android/maps/model/LatLng;

    invoke-virtual {v5}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;->a()D

    move-result-wide v8

    invoke-virtual {v5}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;->b()D

    move-result-wide v10

    invoke-direct {v7, v8, v9, v10, v11}, Lcom/facebook/android/maps/model/LatLng;-><init>(DD)V

    .line 1801294
    iput-object v7, v6, LX/699;->b:Lcom/facebook/android/maps/model/LatLng;

    .line 1801295
    move-object v5, v6

    .line 1801296
    invoke-virtual {v2}, Lcom/facebook/api/graphql/storyattachment/StoryAttachmentGraphQLModels$PlaceRecommendationPageFieldsModel;->d()Ljava/lang/String;

    move-result-object v2

    .line 1801297
    iput-object v2, v5, LX/699;->i:Ljava/lang/String;

    .line 1801298
    move-object v2, v5

    .line 1801299
    const v5, 0x7f020e8f

    invoke-static {v5}, LX/690;->a(I)LX/68w;

    move-result-object v5

    .line 1801300
    iput-object v5, v2, LX/699;->c:LX/68w;

    .line 1801301
    move-object v2, v2

    .line 1801302
    invoke-virtual {p1, v2}, LX/6al;->a(LX/699;)LX/6ax;

    .line 1801303
    :cond_0
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0

    .line 1801304
    :cond_1
    return-void
.end method
