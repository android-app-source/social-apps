.class public LX/BxC;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pm;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/BxF;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/BxC",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/BxF;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1835156
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1835157
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/BxC;->b:LX/0Zi;

    .line 1835158
    iput-object p1, p0, LX/BxC;->a:LX/0Ot;

    .line 1835159
    return-void
.end method

.method public static a(LX/0QB;)LX/BxC;
    .locals 4

    .prologue
    .line 1835110
    const-class v1, LX/BxC;

    monitor-enter v1

    .line 1835111
    :try_start_0
    sget-object v0, LX/BxC;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1835112
    sput-object v2, LX/BxC;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1835113
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1835114
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1835115
    new-instance v3, LX/BxC;

    const/16 p0, 0x1e05

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/BxC;-><init>(LX/0Ot;)V

    .line 1835116
    move-object v0, v3

    .line 1835117
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1835118
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/BxC;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1835119
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1835120
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 8

    .prologue
    .line 1835121
    check-cast p2, LX/BxA;

    .line 1835122
    iget-object v0, p0, LX/BxC;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BxF;

    iget-object v1, p2, LX/BxA;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/BxA;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    iget-object v3, p2, LX/BxA;->c:LX/1Pm;

    const/4 p0, 0x2

    const p2, -0xb1a99b

    .line 1835123
    new-instance v5, LX/BxW;

    invoke-direct {v5, v2}, LX/BxW;-><init>(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    .line 1835124
    iget-object v4, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, v4

    .line 1835125
    check-cast v4, LX/0jW;

    invoke-interface {v3, v5, v4}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/BxV;

    .line 1835126
    invoke-static {v2, v4}, LX/BxV;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;LX/BxV;)V

    .line 1835127
    iget-object v5, v0, LX/BxF;->a:LX/1vg;

    .line 1835128
    invoke-virtual {v5, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v6

    const v7, 0x7f020781

    invoke-virtual {v6, v7}, LX/2xv;->h(I)LX/2xv;

    move-result-object v6

    invoke-static {v4, p2}, LX/BxF;->a(LX/BxV;I)I

    move-result v7

    invoke-virtual {v6, v7}, LX/2xv;->i(I)LX/2xv;

    move-result-object v6

    invoke-virtual {v6}, LX/1n6;->b()LX/1dc;

    move-result-object v6

    move-object v5, v6

    .line 1835129
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v6

    invoke-interface {v6, p0}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v6

    const/4 v7, 0x1

    invoke-interface {v6, v7}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v6

    invoke-interface {v6, p0}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v6

    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v7

    invoke-virtual {v7, v5}, LX/1o5;->a(LX/1dc;)LX/1o5;

    move-result-object v5

    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    const/4 v7, 0x6

    const/4 p0, 0x4

    invoke-interface {v5, v7, p0}, LX/1Di;->h(II)LX/1Di;

    move-result-object v5

    invoke-interface {v6, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v5

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v6

    .line 1835130
    iget-object v7, v4, LX/BxV;->a:Ljava/lang/Boolean;

    move-object v7, v7

    .line 1835131
    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    if-eqz v7, :cond_0

    const v7, 0x7f081859

    :goto_0
    move v7, v7

    .line 1835132
    invoke-virtual {v6, v7}, LX/1ne;->h(I)LX/1ne;

    move-result-object v6

    invoke-static {v4, p2}, LX/BxF;->a(LX/BxV;I)I

    move-result v7

    invoke-virtual {v6, v7}, LX/1ne;->m(I)LX/1ne;

    move-result-object v6

    const/high16 v7, 0x41600000    # 14.0f

    invoke-virtual {v6, v7}, LX/1ne;->g(F)LX/1ne;

    move-result-object v6

    invoke-interface {v5, v6}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v5

    .line 1835133
    iget-object v6, v4, LX/BxV;->a:Ljava/lang/Boolean;

    move-object v6, v6

    .line 1835134
    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    if-eqz v6, :cond_1

    const v6, 0x7f081859

    :goto_1
    move v6, v6

    .line 1835135
    invoke-interface {v5, v6}, LX/1Dh;->Y(I)LX/1Dh;

    move-result-object v5

    .line 1835136
    const v6, 0x81a3174

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 p0, 0x0

    aput-object v4, v7, p0

    invoke-static {p1, v6, v7}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v6

    move-object v4, v6

    .line 1835137
    invoke-interface {v5, v4}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    move-object v0, v4

    .line 1835138
    return-object v0

    :cond_0
    const v7, 0x7f081858

    goto :goto_0

    :cond_1
    const v6, 0x7f081858

    goto :goto_1
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 1835139
    invoke-static {}, LX/1dS;->b()V

    .line 1835140
    iget v0, p1, LX/1dQ;->b:I

    .line 1835141
    packed-switch v0, :pswitch_data_0

    .line 1835142
    :goto_0
    return-object v2

    .line 1835143
    :pswitch_0
    iget-object v0, p1, LX/1dQ;->c:[Ljava/lang/Object;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    check-cast v0, LX/BxV;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1835144
    check-cast v1, LX/BxA;

    .line 1835145
    iget-object v3, p0, LX/BxC;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/BxF;

    iget-object v4, v1, LX/BxA;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v5, v1, LX/BxA;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    iget-object v6, v1, LX/BxA;->c:LX/1Pm;

    const/4 v1, 0x1

    const/4 p0, 0x0

    .line 1835146
    iget-object v7, v0, LX/BxV;->a:Ljava/lang/Boolean;

    move-object v7, v7

    .line 1835147
    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 1835148
    invoke-virtual {v0, p0}, LX/BxV;->a(Z)V

    .line 1835149
    iget-object v7, v3, LX/BxF;->c:LX/BxX;

    invoke-virtual {v7}, LX/BxX;->b()V

    .line 1835150
    iget-object v7, v3, LX/BxF;->b:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/5up;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->C()Ljava/lang/String;

    move-result-object v8

    const-string v9, "native_feed_chaining_box"

    const-string p1, "toggle_button"

    new-instance p2, LX/BxD;

    invoke-direct {p2, v3}, LX/BxD;-><init>(LX/BxF;)V

    invoke-virtual {v7, v8, v9, p1, p2}, LX/5up;->f(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/2h0;)V

    .line 1835151
    :goto_1
    new-array v7, v1, [Lcom/facebook/feed/rows/core/props/FeedProps;

    aput-object v4, v7, p0

    invoke-interface {v6, v7}, LX/1Pq;->a([Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1835152
    goto :goto_0

    .line 1835153
    :cond_0
    invoke-virtual {v0, v1}, LX/BxV;->a(Z)V

    .line 1835154
    iget-object v7, v3, LX/BxF;->c:LX/BxX;

    invoke-virtual {v7}, LX/BxX;->a()V

    .line 1835155
    iget-object v7, v3, LX/BxF;->b:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/5up;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->C()Ljava/lang/String;

    move-result-object v8

    const-string v9, "native_feed_chaining_box"

    const-string p1, "toggle_button"

    new-instance p2, LX/BxE;

    invoke-direct {p2, v3}, LX/BxE;-><init>(LX/BxF;)V

    invoke-virtual {v7, v8, v9, p1, p2}, LX/5up;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/2h0;)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x81a3174
        :pswitch_0
    .end packed-switch
.end method
