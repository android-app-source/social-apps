.class public LX/Agc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/AgG;


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:Ljava/util/concurrent/Executor;

.field public final c:LX/0tX;

.field public final d:LX/03V;

.field public final e:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel$FeedbackModel$ReactionTimeSlicesModel;",
            ">;"
        }
    .end annotation
.end field

.field private f:Z

.field public g:I

.field public h:I

.field public i:I

.field private j:D

.field public k:LX/AgW;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1701423
    const-class v0, LX/Agc;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Agc;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/Executor;LX/0tX;LX/03V;)V
    .locals 1
    .param p1    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1701417
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1701418
    iput-object p1, p0, LX/Agc;->b:Ljava/util/concurrent/Executor;

    .line 1701419
    iput-object p2, p0, LX/Agc;->c:LX/0tX;

    .line 1701420
    iput-object p3, p0, LX/Agc;->d:LX/03V;

    .line 1701421
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, LX/Agc;->e:Ljava/util/Queue;

    .line 1701422
    return-void
.end method

.method public static a(LX/Agc;Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel$FeedbackModel$ReactionTimeSlicesModel;)V
    .locals 14

    .prologue
    const/4 v4, 0x0

    .line 1701401
    new-instance v6, Landroid/util/SparseArray;

    invoke-direct {v6}, Landroid/util/SparseArray;-><init>()V

    .line 1701402
    invoke-virtual {p1}, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel$FeedbackModel$ReactionTimeSlicesModel;->j()LX/0Px;

    move-result-object v7

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    move v5, v4

    move v2, v4

    :goto_0
    if-ge v5, v8, :cond_2

    invoke-virtual {v7, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel$FeedbackModel$ReactionTimeSlicesModel$ReactionsModel;

    .line 1701403
    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel$FeedbackModel$ReactionTimeSlicesModel$ReactionsModel;->k()Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel$FeedbackModel$ReactionTimeSlicesModel$ReactionsModel$ReactionInfoModel;

    move-result-object v9

    .line 1701404
    if-eqz v9, :cond_4

    .line 1701405
    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel$FeedbackModel$ReactionTimeSlicesModel$ReactionsModel;->j()LX/0Px;

    move-result-object v10

    invoke-virtual {v10}, LX/0Px;->size()I

    move-result v11

    move v3, v4

    :goto_1
    if-ge v3, v11, :cond_1

    invoke-virtual {v10, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel$FeedbackModel$ReactionTimeSlicesModel$ReactionsModel$ImportantReactorsModel;

    .line 1701406
    invoke-virtual {v1}, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel$FeedbackModel$ReactionTimeSlicesModel$ReactionsModel$ImportantReactorsModel;->j()Ljava/lang/String;

    move-result-object v1

    .line 1701407
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v12

    if-nez v12, :cond_0

    iget-object v12, p0, LX/Agc;->k:LX/AgW;

    if-eqz v12, :cond_0

    .line 1701408
    iget-object v12, p0, LX/Agc;->k:LX/AgW;

    invoke-virtual {v9}, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel$FeedbackModel$ReactionTimeSlicesModel$ReactionsModel$ReactionInfoModel;->j()I

    move-result v13

    invoke-interface {v12, v13, v1}, LX/AgW;->a(ILjava/lang/String;)V

    .line 1701409
    :cond_0
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_1

    .line 1701410
    :cond_1
    invoke-virtual {v9}, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel$FeedbackModel$ReactionTimeSlicesModel$ReactionsModel$ReactionInfoModel;->j()I

    move-result v1

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel$FeedbackModel$ReactionTimeSlicesModel$ReactionsModel;->a()I

    move-result v3

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel$FeedbackModel$ReactionTimeSlicesModel$ReactionsModel;->j()LX/0Px;

    move-result-object v9

    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v9

    sub-int/2addr v3, v9

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v6, v1, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1701411
    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel$FeedbackModel$ReactionTimeSlicesModel$ReactionsModel;->a()I

    move-result v0

    add-int/2addr v0, v2

    .line 1701412
    :goto_2
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    move v2, v0

    goto :goto_0

    .line 1701413
    :cond_2
    int-to-double v0, v2

    invoke-virtual {p1}, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel$FeedbackModel$ReactionTimeSlicesModel;->a()J

    move-result-wide v2

    long-to-double v2, v2

    div-double/2addr v0, v2

    iput-wide v0, p0, LX/Agc;->j:D

    .line 1701414
    iget-object v0, p0, LX/Agc;->k:LX/AgW;

    if-eqz v0, :cond_3

    .line 1701415
    iget-object v0, p0, LX/Agc;->k:LX/AgW;

    invoke-interface {v0, v6}, LX/AgW;->a(Landroid/util/SparseArray;)V

    .line 1701416
    :cond_3
    return-void

    :cond_4
    move v0, v2

    goto :goto_2
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1701356
    iget-boolean v0, p0, LX/Agc;->f:Z

    if-nez v0, :cond_1

    .line 1701357
    :cond_0
    :goto_0
    return-void

    .line 1701358
    :cond_1
    iput-boolean v1, p0, LX/Agc;->f:Z

    .line 1701359
    iget-object v0, p0, LX/Agc;->e:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 1701360
    iget-object v0, p0, LX/Agc;->m:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    .line 1701361
    iget-object v0, p0, LX/Agc;->m:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    goto :goto_0
.end method

.method public final a(I)V
    .locals 0

    .prologue
    .line 1701400
    return-void
.end method

.method public final a(LX/AgW;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 1701392
    iget-boolean v0, p0, LX/Agc;->f:Z

    if-eqz v0, :cond_0

    .line 1701393
    :goto_0
    return-void

    .line 1701394
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Agc;->f:Z

    .line 1701395
    invoke-static {p1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AgW;

    iput-object v0, p0, LX/Agc;->k:LX/AgW;

    .line 1701396
    invoke-static {p2}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/Agc;->l:Ljava/lang/String;

    .line 1701397
    iput v1, p0, LX/Agc;->g:I

    .line 1701398
    iput v1, p0, LX/Agc;->i:I

    .line 1701399
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/Agc;->j:D

    goto :goto_0
.end method

.method public final b(I)V
    .locals 8

    .prologue
    .line 1701365
    iget-boolean v0, p0, LX/Agc;->f:Z

    if-nez v0, :cond_1

    .line 1701366
    :cond_0
    :goto_0
    return-void

    .line 1701367
    :cond_1
    iget v0, p0, LX/Agc;->g:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_2

    iget v0, p0, LX/Agc;->g:I

    sub-int v0, p1, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    const/4 v1, 0x2

    if-lt v0, v1, :cond_4

    .line 1701368
    :cond_2
    iput p1, p0, LX/Agc;->h:I

    .line 1701369
    iput p1, p0, LX/Agc;->g:I

    .line 1701370
    iget-object v0, p0, LX/Agc;->e:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 1701371
    iget-object v0, p0, LX/Agc;->k:LX/AgW;

    if-eqz v0, :cond_3

    .line 1701372
    iget-object v0, p0, LX/Agc;->k:LX/AgW;

    invoke-interface {v0}, LX/AgW;->a()V

    .line 1701373
    :cond_3
    iget-object v0, p0, LX/Agc;->m:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_4

    .line 1701374
    iget-object v0, p0, LX/Agc;->m:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 1701375
    :cond_4
    iput p1, p0, LX/Agc;->g:I

    .line 1701376
    :cond_5
    iget-object v2, p0, LX/Agc;->e:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_6

    iget-object v2, p0, LX/Agc;->e:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel$FeedbackModel$ReactionTimeSlicesModel;

    invoke-virtual {v2}, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel$FeedbackModel$ReactionTimeSlicesModel;->k()J

    move-result-wide v2

    int-to-long v4, p1

    cmp-long v2, v2, v4

    if-gtz v2, :cond_6

    .line 1701377
    iget-object v2, p0, LX/Agc;->e:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel$FeedbackModel$ReactionTimeSlicesModel;

    .line 1701378
    invoke-virtual {v2}, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel$FeedbackModel$ReactionTimeSlicesModel;->k()J

    move-result-wide v4

    invoke-virtual {v2}, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel$FeedbackModel$ReactionTimeSlicesModel;->a()J

    move-result-wide v6

    add-long/2addr v4, v6

    int-to-long v6, p1

    cmp-long v3, v4, v6

    if-lez v3, :cond_5

    .line 1701379
    invoke-virtual {v2}, Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel$FeedbackModel$ReactionTimeSlicesModel;->a()J

    move-result-wide v4

    long-to-int v3, v4

    iput v3, p0, LX/Agc;->i:I

    .line 1701380
    invoke-static {p0, v2}, LX/Agc;->a(LX/Agc;Lcom/facebook/facecastdisplay/protocol/FetchLiveReactionsQueryModels$FetchLiveVODReactionsQueryModel$FeedbackModel$ReactionTimeSlicesModel;)V

    .line 1701381
    :cond_6
    iget v0, p0, LX/Agc;->h:I

    add-int/lit8 v0, v0, -0xf

    if-lt p1, v0, :cond_0

    .line 1701382
    iget-object v0, p0, LX/Agc;->m:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_7

    iget-object v0, p0, LX/Agc;->m:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isDone()Z

    move-result v0

    if-nez v0, :cond_7

    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 1701383
    if-nez v0, :cond_0

    .line 1701384
    iget v0, p0, LX/Agc;->h:I

    .line 1701385
    new-instance v1, LX/6Rk;

    invoke-direct {v1}, LX/6Rk;-><init>()V

    move-object v1, v1

    .line 1701386
    const-string v2, "after_timestamp"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1701387
    const-string v2, "duration"

    const/16 v3, 0x3c

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1701388
    const-string v2, "targetID"

    iget-object v3, p0, LX/Agc;->l:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1701389
    iget-object v2, p0, LX/Agc;->c:LX/0tX;

    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v1

    iput-object v1, p0, LX/Agc;->m:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1701390
    iget-object v1, p0, LX/Agc;->m:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v2, LX/Agb;

    invoke-direct {v2, p0}, LX/Agb;-><init>(LX/Agc;)V

    iget-object v3, p0, LX/Agc;->b:Ljava/util/concurrent/Executor;

    invoke-static {v1, v2, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1701391
    goto/16 :goto_0

    :cond_7
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1701364
    iget-boolean v0, p0, LX/Agc;->f:Z

    return v0
.end method

.method public final c()D
    .locals 2

    .prologue
    .line 1701363
    iget-wide v0, p0, LX/Agc;->j:D

    return-wide v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 1701362
    iget v0, p0, LX/Agc;->i:I

    mul-int/lit16 v0, v0, 0x3e8

    return v0
.end method
