.class public LX/B7r;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements LX/B7m;


# static fields
.field public static final a:LX/B7j;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/B7j",
            "<",
            "LX/B7r;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public b:LX/7Tk;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/B7W;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Lcom/facebook/resources/ui/FbButton;

.field public e:Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;

.field private f:Lcom/facebook/widget/text/BetterTextView;

.field public g:LX/7Tj;

.field private h:LX/B7F;

.field public i:LX/B7w;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1748245
    new-instance v0, LX/B7o;

    invoke-direct {v0}, LX/B7o;-><init>()V

    sput-object v0, LX/B7r;->a:LX/B7j;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1748239
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 1748240
    const v0, 0x7f0309c3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1748241
    const v0, 0x7f0d18de

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, LX/B7r;->d:Lcom/facebook/resources/ui/FbButton;

    .line 1748242
    const v0, 0x7f0d18dd

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/B7r;->f:Lcom/facebook/widget/text/BetterTextView;

    .line 1748243
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, LX/B7r;

    const-class p1, LX/7Tk;

    invoke-interface {v0, p1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object p1

    check-cast p1, LX/7Tk;

    invoke-static {v0}, LX/B7W;->a(LX/0QB;)LX/B7W;

    move-result-object v0

    check-cast v0, LX/B7W;

    iput-object p1, p0, LX/B7r;->b:LX/7Tk;

    iput-object v0, p0, LX/B7r;->c:LX/B7W;

    .line 1748244
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1748236
    iget-object v0, p0, LX/B7r;->d:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1748237
    iput-object v1, p0, LX/B7r;->i:LX/B7w;

    .line 1748238
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;LX/B7F;I)V
    .locals 2

    .prologue
    .line 1748229
    iput-object p1, p0, LX/B7r;->e:Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;

    .line 1748230
    iput-object p2, p0, LX/B7r;->h:LX/B7F;

    .line 1748231
    iget-object v0, p0, LX/B7r;->f:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, p0, LX/B7r;->e:Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;->p()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1748232
    invoke-virtual {p2}, LX/B7F;->w()Lcom/facebook/common/locale/Country;

    move-result-object v0

    .line 1748233
    iget-object v1, p0, LX/B7r;->d:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0}, Lcom/facebook/common/locale/Country;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 1748234
    iget-object v0, p0, LX/B7r;->d:Lcom/facebook/resources/ui/FbButton;

    new-instance v1, LX/B7q;

    invoke-direct {v1, p0}, LX/B7q;-><init>(LX/B7r;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1748235
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1748228
    return-void
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 1748216
    return-void
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 1748227
    return-void
.end method

.method public getBoundedInfoFieldData()Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;
    .locals 1

    .prologue
    .line 1748226
    iget-object v0, p0, LX/B7r;->e:Lcom/facebook/graphql/model/GraphQLLeadGenInfoFieldData;

    return-object v0
.end method

.method public getInputCustomToken()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1748225
    invoke-virtual {p0}, LX/B7r;->getInputValue()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getInputValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1748224
    iget-object v0, p0, LX/B7r;->d:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbButton;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setInputValue(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1748219
    invoke-static {p1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1748220
    iget-object v0, p0, LX/B7r;->d:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    .line 1748221
    :goto_0
    iget-object v0, p0, LX/B7r;->d:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbButton;->clearFocus()V

    .line 1748222
    return-void

    .line 1748223
    :cond_0
    iget-object v0, p0, LX/B7r;->d:Lcom/facebook/resources/ui/FbButton;

    iget-object v1, p0, LX/B7r;->h:LX/B7F;

    invoke-virtual {v1}, LX/B7F;->w()Lcom/facebook/common/locale/Country;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/common/locale/Country;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public setOnDataChangeListener(LX/B7w;)V
    .locals 0

    .prologue
    .line 1748217
    iput-object p1, p0, LX/B7r;->i:LX/B7w;

    .line 1748218
    return-void
.end method
