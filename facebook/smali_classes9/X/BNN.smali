.class public final LX/BNN;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/composer/protocol/PostReviewParams;

.field public final synthetic c:LX/0am;

.field public final synthetic d:LX/BNP;


# direct methods
.method public constructor <init>(LX/BNP;Ljava/lang/String;Lcom/facebook/composer/protocol/PostReviewParams;LX/0am;)V
    .locals 0

    .prologue
    .line 1778622
    iput-object p1, p0, LX/BNN;->d:LX/BNP;

    iput-object p2, p0, LX/BNN;->a:Ljava/lang/String;

    iput-object p3, p0, LX/BNN;->b:Lcom/facebook/composer/protocol/PostReviewParams;

    iput-object p4, p0, LX/BNN;->c:LX/0am;

    invoke-direct {p0}, LX/2h0;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 4

    .prologue
    .line 1778623
    iget-object v0, p0, LX/BNN;->d:LX/BNP;

    iget-object v0, v0, LX/BNP;->h:LX/0kL;

    new-instance v1, LX/27k;

    iget-object v2, p0, LX/BNN;->d:LX/BNP;

    iget-object v2, v2, LX/BNP;->f:LX/BNL;

    invoke-virtual {v2, p1}, LX/BNL;->a(Lcom/facebook/fbservice/service/ServiceException;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 1778624
    iget-object v0, p0, LX/BNN;->d:LX/BNP;

    iget-object v0, v0, LX/BNP;->e:LX/79D;

    iget-object v1, p0, LX/BNN;->a:Ljava/lang/String;

    iget-object v2, p0, LX/BNN;->b:Lcom/facebook/composer/protocol/PostReviewParams;

    iget-wide v2, v2, Lcom/facebook/composer/protocol/PostReviewParams;->b:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    .line 1778625
    const-string v3, "reviews_post_review_failure"

    invoke-static {v0, v3, v1, v2}, LX/79D;->b(LX/79D;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1778626
    iget-object v0, p0, LX/BNN;->d:LX/BNP;

    iget-object v0, v0, LX/BNP;->i:LX/4BY;

    if-eqz v0, :cond_0

    .line 1778627
    iget-object v0, p0, LX/BNN;->d:LX/BNP;

    iget-object v0, v0, LX/BNP;->i:LX/4BY;

    invoke-virtual {v0}, LX/4BY;->dismiss()V

    .line 1778628
    :cond_0
    iget-object v0, p0, LX/BNN;->d:LX/BNP;

    iget-object v0, v0, LX/BNP;->h:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f0814e6

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 1778629
    iget-object v0, p0, LX/BNN;->c:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1778630
    iget-object v0, p0, LX/BNN;->c:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BNO;

    invoke-virtual {v0}, LX/BNO;->b()V

    .line 1778631
    :cond_1
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 1778613
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    .line 1778614
    iget-object v0, p0, LX/BNN;->d:LX/BNP;

    iget-object v0, v0, LX/BNP;->e:LX/79D;

    iget-object v1, p0, LX/BNN;->a:Ljava/lang/String;

    iget-object v2, p0, LX/BNN;->b:Lcom/facebook/composer/protocol/PostReviewParams;

    iget-wide v2, v2, Lcom/facebook/composer/protocol/PostReviewParams;->b:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    .line 1778615
    const-string v3, "reviews_post_review_success"

    invoke-static {v0, v3, v1, v2}, LX/79D;->b(LX/79D;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1778616
    iget-object v0, p0, LX/BNN;->d:LX/BNP;

    iget-object v0, v0, LX/BNP;->i:LX/4BY;

    if-eqz v0, :cond_0

    .line 1778617
    iget-object v0, p0, LX/BNN;->d:LX/BNP;

    iget-object v0, v0, LX/BNP;->i:LX/4BY;

    invoke-virtual {v0}, LX/4BY;->dismiss()V

    .line 1778618
    :cond_0
    iget-object v0, p0, LX/BNN;->d:LX/BNP;

    iget-object v0, v0, LX/BNP;->h:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f0814e5

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 1778619
    iget-object v0, p0, LX/BNN;->c:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1778620
    iget-object v0, p0, LX/BNN;->c:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BNO;

    invoke-virtual {v0, p1}, LX/BNO;->a(Lcom/facebook/fbservice/service/OperationResult;)V

    .line 1778621
    :cond_1
    return-void
.end method
