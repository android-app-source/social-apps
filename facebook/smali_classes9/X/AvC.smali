.class public final LX/AvC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/ArB;

.field public final synthetic b:LX/AvE;


# direct methods
.method public constructor <init>(LX/AvE;LX/ArB;)V
    .locals 0

    .prologue
    .line 1723867
    iput-object p1, p0, LX/AvC;->b:LX/AvE;

    iput-object p2, p0, LX/AvC;->a:LX/ArB;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1723868
    iget-object v0, p0, LX/AvC;->a:LX/ArB;

    .line 1723869
    iget-object v1, v0, LX/ArB;->b:Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;

    iget-object v1, v1, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->M:LX/03V;

    sget-object v2, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->F:Ljava/lang/String;

    const-string p0, "Error fetching inspirations"

    invoke-virtual {v1, v2, p0, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1723870
    iget-object v1, v0, LX/ArB;->b:Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;

    invoke-static {v1}, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ab(Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1723871
    iget-object v1, v0, LX/ArB;->b:Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;

    iget-object v1, v1, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->as:LX/ArL;

    .line 1723872
    sget-object v2, LX/ArH;->NO_EFFECTS:LX/ArH;

    invoke-static {v1, v2}, LX/ArL;->a(LX/ArL;LX/5oU;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    invoke-static {v1, v2}, LX/ArL;->a(LX/ArL;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1723873
    :cond_0
    iget-object v1, v0, LX/ArB;->b:Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;

    .line 1723874
    iget-object p0, v1, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ah:LX/HvN;

    invoke-interface {p0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {p0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->isLocationFetchAndRequeryingEffectsInProgress()Z

    move-result p0

    if-eqz p0, :cond_1

    .line 1723875
    iget-object p0, v1, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ah:LX/HvN;

    invoke-interface {p0}, LX/0im;->c()LX/0jJ;

    move-result-object p0

    sget-object v0, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->G:LX/0jK;

    invoke-virtual {p0, v0}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object p0

    check-cast p0, LX/0jL;

    iget-object v0, v1, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraFragment;->ah:LX/HvN;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationState;)Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->setIsLocationFetchAndRequeryingEffectsInProgress(Z)Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->setLocation(Lcom/facebook/ipc/composer/model/ComposerLocation;)Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/0jL;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationState;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/0jL;

    invoke-virtual {p0}, LX/0jL;->a()V

    .line 1723876
    :cond_1
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1723877
    check-cast p1, Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;

    .line 1723878
    iget-object v0, p0, LX/AvC;->a:LX/ArB;

    invoke-virtual {v0, p1}, LX/ArB;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationQueryModel;)V

    .line 1723879
    return-void
.end method
