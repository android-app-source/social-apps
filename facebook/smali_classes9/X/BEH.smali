.class public LX/BEH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7mI;


# instance fields
.field private a:LX/2U1;


# direct methods
.method public constructor <init>(LX/2U1;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1764281
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1764282
    iput-object p1, p0, LX/BEH;->a:LX/2U1;

    .line 1764283
    return-void
.end method

.method public static a(LX/0QB;)LX/BEH;
    .locals 1

    .prologue
    .line 1764284
    invoke-static {p0}, LX/BEH;->b(LX/0QB;)LX/BEH;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/BEH;
    .locals 2

    .prologue
    .line 1764285
    new-instance v1, LX/BEH;

    invoke-static {p0}, LX/2U1;->a(LX/0QB;)LX/2U1;

    move-result-object v0

    check-cast v0, LX/2U1;

    invoke-direct {v1, v0}, LX/BEH;-><init>(LX/2U1;)V

    .line 1764286
    return-object v1
.end method


# virtual methods
.method public final a()LX/03R;
    .locals 4

    .prologue
    .line 1764287
    iget-object v0, p0, LX/BEH;->a:LX/2U1;

    invoke-virtual {v0}, LX/2U1;->d()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_2

    .line 1764288
    iget-object v0, p0, LX/BEH;->a:LX/2U1;

    invoke-virtual {v0}, LX/2U1;->c()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/BEH;->a:LX/2U1;

    invoke-virtual {v0}, LX/2U2;->b()I

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v0

    .line 1764289
    :goto_1
    return-object v0

    .line 1764290
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1764291
    :cond_2
    sget-object v0, LX/03R;->UNSET:LX/03R;

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1764292
    iget-object v0, p0, LX/BEH;->a:LX/2U1;

    invoke-virtual {v0, p1}, LX/2U2;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/BEH;->a:LX/2U1;

    invoke-virtual {v0, p1}, LX/2U2;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8Dk;

    .line 1764293
    iget-object p0, v0, LX/8Dk;->a:Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;

    move-object v0, p0

    .line 1764294
    invoke-virtual {v0}, Lcom/facebook/pages/adminedpages/protocol/AdminedPagesPrefetchGraphQLModels$AdminedPagesPrefetchQueryModel$AdminedPagesModel$NodesModel;->eD_()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/8A4;->c(LX/0Px;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
