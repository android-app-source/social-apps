.class public final LX/BFY;
.super Landroid/view/animation/Animation;
.source ""


# instance fields
.field public final synthetic a:LX/BFZ;


# direct methods
.method public constructor <init>(LX/BFZ;)V
    .locals 2

    .prologue
    .line 1765964
    iput-object p1, p0, LX/BFY;->a:LX/BFZ;

    invoke-direct {p0}, Landroid/view/animation/Animation;-><init>()V

    .line 1765965
    new-instance v0, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {p0, v0}, LX/BFY;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 1765966
    iget-object v0, p1, LX/BFZ;->b:Landroid/content/res/Resources;

    const/high16 v1, 0x10e0000

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    int-to-long v0, v0

    invoke-virtual {p0, v0, v1}, LX/BFY;->setDuration(J)V

    .line 1765967
    return-void
.end method


# virtual methods
.method public final applyTransformation(FLandroid/view/animation/Transformation;)V
    .locals 5

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    .line 1765968
    sget v0, LX/BFZ;->e:I

    int-to-float v0, v0

    mul-float/2addr v0, p1

    sget v1, LX/BFZ;->d:I

    int-to-float v1, v1

    sub-float v2, v4, p1

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    float-to-int v0, v0

    .line 1765969
    sget v1, LX/BFZ;->f:I

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v1, v0

    .line 1765970
    sget v2, LX/BFZ;->g:I

    int-to-float v2, v2

    mul-float/2addr v2, p1

    sget v3, LX/BFZ;->f:I

    int-to-float v3, v3

    sub-float/2addr v4, p1

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    float-to-int v2, v2

    .line 1765971
    sget v3, LX/BFZ;->f:I

    mul-int/lit8 v3, v3, 0x2

    sub-int/2addr v3, v2

    .line 1765972
    iget-object v4, p0, LX/BFY;->a:LX/BFZ;

    iget-object v4, v4, LX/BFZ;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4, v0, v0, v1, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1765973
    iget-object v0, p0, LX/BFY;->a:LX/BFZ;

    iget-object v0, v0, LX/BFZ;->i:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v2, v2, v3, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1765974
    return-void
.end method
