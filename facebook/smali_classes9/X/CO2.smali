.class public LX/CO2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/CNe;


# instance fields
.field private final a:LX/CNb;

.field public final b:LX/CNq;

.field public final c:LX/CNe;

.field public final d:LX/CNe;


# direct methods
.method public constructor <init>(LX/CNb;LX/CNq;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1882475
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1882476
    iput-object p1, p0, LX/CO2;->a:LX/CNb;

    .line 1882477
    iput-object p2, p0, LX/CO2;->b:LX/CNq;

    .line 1882478
    const-string v0, "send-action"

    invoke-virtual {p1, v0}, LX/CNb;->b(Ljava/lang/String;)LX/CNb;

    move-result-object v0

    .line 1882479
    const-string v2, "failure-action"

    invoke-virtual {p1, v2}, LX/CNb;->b(Ljava/lang/String;)LX/CNb;

    move-result-object v2

    .line 1882480
    if-eqz v0, :cond_1

    invoke-static {v0, p2}, LX/CNd;->a(LX/CNb;LX/CNc;)LX/CNe;

    move-result-object v0

    :goto_0
    iput-object v0, p0, LX/CO2;->c:LX/CNe;

    .line 1882481
    if-eqz v2, :cond_0

    invoke-static {v2, p2}, LX/CNd;->a(LX/CNb;LX/CNc;)LX/CNe;

    move-result-object v1

    :cond_0
    iput-object v1, p0, LX/CO2;->d:LX/CNe;

    .line 1882482
    return-void

    :cond_1
    move-object v0, v1

    .line 1882483
    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    .line 1882484
    iget-object v0, p0, LX/CO2;->a:LX/CNb;

    const-string v1, "group"

    invoke-virtual {v0, v1}, LX/CNb;->d(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNode;

    .line 1882485
    iget-object v1, p0, LX/CO2;->a:LX/CNb;

    const-string v2, "source"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/CNb;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1882486
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v0

    .line 1882487
    new-instance v2, LX/4Fq;

    invoke-direct {v2}, LX/4Fq;-><init>()V

    invoke-virtual {v2, v0}, LX/4Fq;->a(Ljava/lang/String;)LX/4Fq;

    move-result-object v2

    const-string v3, "ALLOW_READD"

    invoke-virtual {v2, v3}, LX/4Fq;->c(Ljava/lang/String;)LX/4Fq;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/4Fq;->b(Ljava/lang/String;)LX/4Fq;

    move-result-object v2

    .line 1882488
    invoke-static {}, LX/B2A;->a()LX/B27;

    move-result-object v3

    .line 1882489
    const-string v4, "input"

    invoke-virtual {v3, v4, v2}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1882490
    invoke-static {v3}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v2

    .line 1882491
    new-instance v3, LX/B2J;

    invoke-direct {v3}, LX/B2J;-><init>()V

    .line 1882492
    iput-object v0, v3, LX/B2J;->a:Ljava/lang/String;

    .line 1882493
    move-object v3, v3

    .line 1882494
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 1882495
    iput-object v4, v3, LX/B2J;->b:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 1882496
    move-object v3, v3

    .line 1882497
    invoke-virtual {v3}, LX/B2J;->a()Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupLeaveCoreMutationModel$GroupModel;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/399;->a(LX/0jT;)LX/399;

    .line 1882498
    iget-object v3, p0, LX/CO2;->b:LX/CNq;

    invoke-virtual {v3}, LX/CNq;->g()LX/0tX;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    move-object v0, v2

    .line 1882499
    new-instance v1, LX/CO1;

    invoke-direct {v1, p0}, LX/CO1;-><init>(LX/CO2;)V

    .line 1882500
    iget-object v2, p0, LX/CO2;->b:LX/CNq;

    invoke-virtual {v2}, LX/CNq;->n()Ljava/util/concurrent/ExecutorService;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1882501
    return-void
.end method
