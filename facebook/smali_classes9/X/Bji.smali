.class public final LX/Bji;
.super LX/2EJ;
.source ""


# instance fields
.field public final synthetic b:Lcom/facebook/events/create/EventScheduleTimeSelectorDialogFragment;

.field public c:Lcom/facebook/resources/ui/FbEditText;

.field public d:Lcom/facebook/resources/ui/FbEditText;

.field public e:Ljava/util/Calendar;

.field public f:Ljava/util/Calendar;

.field public g:LX/BjY;


# direct methods
.method public constructor <init>(Lcom/facebook/events/create/EventScheduleTimeSelectorDialogFragment;Landroid/content/Context;Ljava/util/Calendar;LX/BjY;)V
    .locals 4

    .prologue
    .line 1812716
    iput-object p1, p0, LX/Bji;->b:Lcom/facebook/events/create/EventScheduleTimeSelectorDialogFragment;

    .line 1812717
    invoke-direct {p0, p2}, LX/2EJ;-><init>(Landroid/content/Context;)V

    .line 1812718
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1812719
    const/4 v2, 0x0

    .line 1812720
    invoke-virtual {p0}, LX/Bji;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1812721
    const v1, 0x7f030513

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 1812722
    invoke-virtual {p0, v1}, LX/2EJ;->a(Landroid/view/View;)V

    .line 1812723
    invoke-virtual {p0, v2}, LX/2EJ;->a(Ljava/lang/CharSequence;)V

    .line 1812724
    invoke-virtual {p0}, LX/Bji;->getContext()Landroid/content/Context;

    move-result-object v0

    const v2, 0x7f082166

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/Bji;->setTitle(Ljava/lang/CharSequence;)V

    .line 1812725
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, LX/Bji;->f:Ljava/util/Calendar;

    .line 1812726
    iget-object v0, p0, LX/Bji;->f:Ljava/util/Calendar;

    invoke-virtual {p3}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 1812727
    iput-object p4, p0, LX/Bji;->g:LX/BjY;

    .line 1812728
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, LX/Bji;->e:Ljava/util/Calendar;

    .line 1812729
    iget-object v0, p0, LX/Bji;->e:Ljava/util/Calendar;

    iget-object v2, p0, LX/Bji;->f:Ljava/util/Calendar;

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 1812730
    const/4 v0, -0x2

    invoke-virtual {p0}, LX/Bji;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f080017

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, LX/Bjc;

    invoke-direct {v3, p0}, LX/Bjc;-><init>(LX/Bji;)V

    invoke-virtual {p0, v0, v2, v3}, LX/2EJ;->a(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 1812731
    const/4 v0, -0x1

    invoke-virtual {p0}, LX/Bji;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f080016

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, LX/Bjd;

    invoke-direct {v3, p0}, LX/Bjd;-><init>(LX/Bji;)V

    invoke-virtual {p0, v0, v2, v3}, LX/2EJ;->a(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 1812732
    const v0, 0x7f0d0e70

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbEditText;

    iput-object v0, p0, LX/Bji;->c:Lcom/facebook/resources/ui/FbEditText;

    .line 1812733
    iget-object v0, p0, LX/Bji;->c:Lcom/facebook/resources/ui/FbEditText;

    new-instance v2, LX/Bje;

    invoke-direct {v2, p0}, LX/Bje;-><init>(LX/Bji;)V

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbEditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1812734
    invoke-static {p0}, LX/Bji;->d$redex0(LX/Bji;)V

    .line 1812735
    const v0, 0x7f0d0e71

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbEditText;

    iput-object v0, p0, LX/Bji;->d:Lcom/facebook/resources/ui/FbEditText;

    .line 1812736
    iget-object v0, p0, LX/Bji;->d:Lcom/facebook/resources/ui/FbEditText;

    new-instance v1, LX/Bjf;

    invoke-direct {v1, p0}, LX/Bjf;-><init>(LX/Bji;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1812737
    invoke-static {p0}, LX/Bji;->e(LX/Bji;)V

    .line 1812738
    return-void
.end method

.method public static a$redex0(LX/Bji;Ljava/util/Calendar;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1812741
    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    .line 1812742
    iget-object v1, p0, LX/Bji;->b:Lcom/facebook/events/create/EventScheduleTimeSelectorDialogFragment;

    iget-object v1, v1, Lcom/facebook/events/create/EventScheduleTimeSelectorDialogFragment;->n:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-gez v1, :cond_0

    .line 1812743
    iget-object v1, p0, LX/Bji;->b:Lcom/facebook/events/create/EventScheduleTimeSelectorDialogFragment;

    iget-object v1, v1, Lcom/facebook/events/create/EventScheduleTimeSelectorDialogFragment;->o:LX/0kL;

    new-instance v2, LX/27k;

    const v3, 0x7f082168

    invoke-direct {v2, v3}, LX/27k;-><init>(I)V

    invoke-virtual {v1, v2}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 1812744
    :goto_0
    return v0

    .line 1812745
    :cond_0
    iget-object v1, p0, LX/Bji;->b:Lcom/facebook/events/create/EventScheduleTimeSelectorDialogFragment;

    iget-wide v4, v1, Lcom/facebook/events/create/EventScheduleTimeSelectorDialogFragment;->q:J

    cmp-long v1, v2, v4

    if-lez v1, :cond_1

    .line 1812746
    iget-object v1, p0, LX/Bji;->b:Lcom/facebook/events/create/EventScheduleTimeSelectorDialogFragment;

    iget-object v1, v1, Lcom/facebook/events/create/EventScheduleTimeSelectorDialogFragment;->o:LX/0kL;

    new-instance v2, LX/27k;

    const v3, 0x7f082169

    invoke-direct {v2, v3}, LX/27k;-><init>(I)V

    invoke-virtual {v1, v2}, LX/0kL;->a(LX/27k;)LX/27l;

    goto :goto_0

    .line 1812747
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static d$redex0(LX/Bji;)V
    .locals 6

    .prologue
    .line 1812748
    iget-object v1, p0, LX/Bji;->c:Lcom/facebook/resources/ui/FbEditText;

    iget-object v0, p0, LX/Bji;->b:Lcom/facebook/events/create/EventScheduleTimeSelectorDialogFragment;

    iget-object v0, v0, Lcom/facebook/events/create/EventScheduleTimeSelectorDialogFragment;->m:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11S;

    sget-object v2, LX/1lB;->EVENTS_RELATIVE_DATE_STYLE:LX/1lB;

    iget-object v3, p0, LX/Bji;->f:Ljava/util/Calendar;

    invoke-virtual {v3}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    invoke-interface {v0, v2, v4, v5}, LX/11S;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbEditText;->setText(Ljava/lang/CharSequence;)V

    .line 1812749
    return-void
.end method

.method public static e(LX/Bji;)V
    .locals 6

    .prologue
    .line 1812739
    iget-object v1, p0, LX/Bji;->d:Lcom/facebook/resources/ui/FbEditText;

    iget-object v0, p0, LX/Bji;->b:Lcom/facebook/events/create/EventScheduleTimeSelectorDialogFragment;

    iget-object v0, v0, Lcom/facebook/events/create/EventScheduleTimeSelectorDialogFragment;->m:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11S;

    sget-object v2, LX/1lB;->HOUR_MINUTE_STYLE:LX/1lB;

    iget-object v3, p0, LX/Bji;->f:Ljava/util/Calendar;

    invoke-virtual {v3}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    invoke-interface {v0, v2, v4, v5}, LX/11S;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbEditText;->setText(Ljava/lang/CharSequence;)V

    .line 1812740
    return-void
.end method
