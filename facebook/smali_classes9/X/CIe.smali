.class public LX/CIe;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/CIe;


# instance fields
.field public final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1874471
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1874472
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/CIe;->a:Ljava/util/Map;

    .line 1874473
    return-void
.end method

.method public static a(LX/0QB;)LX/CIe;
    .locals 3

    .prologue
    .line 1874474
    sget-object v0, LX/CIe;->b:LX/CIe;

    if-nez v0, :cond_1

    .line 1874475
    const-class v1, LX/CIe;

    monitor-enter v1

    .line 1874476
    :try_start_0
    sget-object v0, LX/CIe;->b:LX/CIe;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1874477
    if-eqz v2, :cond_0

    .line 1874478
    :try_start_1
    new-instance v0, LX/CIe;

    invoke-direct {v0}, LX/CIe;-><init>()V

    .line 1874479
    move-object v0, v0

    .line 1874480
    sput-object v0, LX/CIe;->b:LX/CIe;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1874481
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1874482
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1874483
    :cond_1
    sget-object v0, LX/CIe;->b:LX/CIe;

    return-object v0

    .line 1874484
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1874485
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1874486
    iget-object v0, p0, LX/CIe;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/CIe;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1874487
    iget-object v0, p0, LX/CIe;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
