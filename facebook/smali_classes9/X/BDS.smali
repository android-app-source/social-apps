.class public final LX/BDS;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 12

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1763237
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_a

    .line 1763238
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1763239
    :goto_0
    return v1

    .line 1763240
    :cond_0
    const-string v10, "hide_expand_toggle"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 1763241
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v3

    move v7, v3

    move v3, v2

    .line 1763242
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_7

    .line 1763243
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 1763244
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1763245
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_1

    if-eqz v9, :cond_1

    .line 1763246
    const-string v10, "criteria"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 1763247
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 1763248
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->START_ARRAY:LX/15z;

    if-ne v9, v10, :cond_2

    .line 1763249
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_ARRAY:LX/15z;

    if-eq v9, v10, :cond_2

    .line 1763250
    invoke-static {p0, p1}, LX/BDP;->b(LX/15w;LX/186;)I

    move-result v9

    .line 1763251
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1763252
    :cond_2
    invoke-static {v8, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v8

    move v8, v8

    .line 1763253
    goto :goto_1

    .line 1763254
    :cond_3
    const-string v10, "hide_text"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 1763255
    invoke-static {p0, p1}, LX/BDQ;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 1763256
    :cond_4
    const-string v10, "initially_expanded"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 1763257
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v5, v0

    move v0, v2

    goto :goto_1

    .line 1763258
    :cond_5
    const-string v10, "show_text"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 1763259
    invoke-static {p0, p1}, LX/BDR;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 1763260
    :cond_6
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1763261
    :cond_7
    const/4 v9, 0x5

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 1763262
    invoke-virtual {p1, v1, v8}, LX/186;->b(II)V

    .line 1763263
    if-eqz v3, :cond_8

    .line 1763264
    invoke-virtual {p1, v2, v7}, LX/186;->a(IZ)V

    .line 1763265
    :cond_8
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 1763266
    if-eqz v0, :cond_9

    .line 1763267
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v5}, LX/186;->a(IZ)V

    .line 1763268
    :cond_9
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1763269
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_a
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    goto/16 :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1763270
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1763271
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1763272
    if-eqz v0, :cond_1

    .line 1763273
    const-string v1, "criteria"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1763274
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1763275
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 1763276
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    invoke-static {p0, v2, p2}, LX/BDP;->a(LX/15i;ILX/0nX;)V

    .line 1763277
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1763278
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1763279
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1763280
    if-eqz v0, :cond_2

    .line 1763281
    const-string v1, "hide_expand_toggle"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1763282
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1763283
    :cond_2
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1763284
    if-eqz v0, :cond_3

    .line 1763285
    const-string v1, "hide_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1763286
    invoke-static {p0, v0, p2}, LX/BDQ;->a(LX/15i;ILX/0nX;)V

    .line 1763287
    :cond_3
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1763288
    if-eqz v0, :cond_4

    .line 1763289
    const-string v1, "initially_expanded"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1763290
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1763291
    :cond_4
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1763292
    if-eqz v0, :cond_5

    .line 1763293
    const-string v1, "show_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1763294
    invoke-static {p0, v0, p2}, LX/BDR;->a(LX/15i;ILX/0nX;)V

    .line 1763295
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1763296
    return-void
.end method
