.class public LX/BkU;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/BiY;

.field private final c:LX/0ad;

.field public final d:LX/BkO;

.field private final e:LX/01T;

.field public f:LX/16I;

.field public g:LX/2hU;

.field public h:LX/9el;

.field public i:LX/Biv;

.field public j:Lcom/facebook/events/create/EventCompositionModel;

.field public k:Lcom/facebook/events/model/PrivacyType;

.field public l:Z

.field public m:Z

.field public n:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/BiY;LX/0ad;LX/BkO;LX/16I;LX/2hU;LX/01T;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1814299
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1814300
    iput-object p1, p0, LX/BkU;->a:Landroid/content/Context;

    .line 1814301
    iput-object p2, p0, LX/BkU;->b:LX/BiY;

    .line 1814302
    iput-object p3, p0, LX/BkU;->c:LX/0ad;

    .line 1814303
    iput-object p4, p0, LX/BkU;->d:LX/BkO;

    .line 1814304
    iput-object p5, p0, LX/BkU;->f:LX/16I;

    .line 1814305
    iput-object p6, p0, LX/BkU;->g:LX/2hU;

    .line 1814306
    iput-object p7, p0, LX/BkU;->e:LX/01T;

    .line 1814307
    return-void
.end method

.method public static a(LX/0QB;)LX/BkU;
    .locals 15

    .prologue
    .line 1814293
    new-instance v1, LX/BkU;

    const-class v2, Landroid/content/Context;

    invoke-interface {p0, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    .line 1814294
    new-instance v9, LX/BiY;

    const-class v10, Landroid/content/Context;

    invoke-interface {p0, v10}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/content/Context;

    invoke-static {p0}, LX/8GN;->b(LX/0QB;)LX/8GN;

    move-result-object v11

    check-cast v11, LX/8GN;

    const-class v12, LX/8GP;

    invoke-interface {p0, v12}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v12

    check-cast v12, LX/8GP;

    const/16 v13, 0x154b

    invoke-static {p0, v13}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v13

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v14

    check-cast v14, LX/0ad;

    invoke-direct/range {v9 .. v14}, LX/BiY;-><init>(Landroid/content/Context;LX/8GN;LX/8GP;LX/0Or;LX/0ad;)V

    .line 1814295
    move-object v3, v9

    .line 1814296
    check-cast v3, LX/BiY;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-static {p0}, LX/BkO;->b(LX/0QB;)LX/BkO;

    move-result-object v5

    check-cast v5, LX/BkO;

    invoke-static {p0}, LX/16I;->a(LX/0QB;)LX/16I;

    move-result-object v6

    check-cast v6, LX/16I;

    invoke-static {p0}, LX/2hU;->b(LX/0QB;)LX/2hU;

    move-result-object v7

    check-cast v7, LX/2hU;

    invoke-static {p0}, LX/15N;->b(LX/0QB;)LX/01T;

    move-result-object v8

    check-cast v8, LX/01T;

    invoke-direct/range {v1 .. v8}, LX/BkU;-><init>(Landroid/content/Context;LX/BiY;LX/0ad;LX/BkO;LX/16I;LX/2hU;LX/01T;)V

    .line 1814297
    move-object v0, v1

    .line 1814298
    return-object v0
.end method

.method public static c(Lcom/facebook/events/model/PrivacyType;)Z
    .locals 1

    .prologue
    .line 1814252
    if-eqz p0, :cond_1

    sget-object v0, Lcom/facebook/events/model/PrivacyType;->INVITE_ONLY:Lcom/facebook/events/model/PrivacyType;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/facebook/events/model/PrivacyType;->GROUP:Lcom/facebook/events/model/PrivacyType;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/facebook/events/model/PrivacyType;->COMMUNITY:Lcom/facebook/events/model/PrivacyType;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1814289
    const-string v0, "hasClickedOnCoverPhotoUpload"

    iget-boolean v1, p0, LX/BkU;->l:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1814290
    const-string v0, "hasClickedOnCoverPhotoFbAlbum"

    iget-boolean v1, p0, LX/BkU;->m:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1814291
    const-string v0, "hasClickedOnCoverPhotoThemes"

    iget-boolean v1, p0, LX/BkU;->n:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1814292
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 5

    .prologue
    .line 1814266
    new-instance v0, LX/6WS;

    iget-object v1, p0, LX/BkU;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/6WS;-><init>(Landroid/content/Context;)V

    .line 1814267
    invoke-virtual {v0}, LX/5OM;->c()LX/5OG;

    move-result-object v1

    .line 1814268
    iget-object v2, p0, LX/BkU;->k:Lcom/facebook/events/model/PrivacyType;

    invoke-static {v2}, LX/BkU;->c(Lcom/facebook/events/model/PrivacyType;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1814269
    iget-object v2, p0, LX/BkU;->a:Landroid/content/Context;

    const v3, 0x7f08213d

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/5OG;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v2

    .line 1814270
    new-instance v3, LX/BkT;

    invoke-direct {v3, p0}, LX/BkT;-><init>(LX/BkU;)V

    move-object v3, v3

    .line 1814271
    invoke-virtual {v2, v3}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1814272
    :cond_0
    iget-object v2, p0, LX/BkU;->a:Landroid/content/Context;

    const v3, 0x7f082138

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/5OG;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v2

    .line 1814273
    new-instance v3, LX/BkR;

    invoke-direct {v3, p0}, LX/BkR;-><init>(LX/BkU;)V

    move-object v3, v3

    .line 1814274
    invoke-virtual {v2, v3}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1814275
    iget-object v2, p0, LX/BkU;->e:LX/01T;

    sget-object v3, LX/01T;->PAA:LX/01T;

    if-eq v2, v3, :cond_1

    .line 1814276
    iget-object v2, p0, LX/BkU;->a:Landroid/content/Context;

    const v3, 0x7f08213c

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/5OG;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v2

    .line 1814277
    new-instance v3, LX/BkS;

    invoke-direct {v3, p0}, LX/BkS;-><init>(LX/BkU;)V

    move-object v3, v3

    .line 1814278
    invoke-virtual {v2, v3}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1814279
    :cond_1
    iget-object v2, p0, LX/BkU;->j:Lcom/facebook/events/create/EventCompositionModel;

    invoke-virtual {v2}, Lcom/facebook/events/create/EventCompositionModel;->g()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1814280
    iget-object v2, p0, LX/BkU;->c:LX/0ad;

    sget-short v3, LX/Bjt;->a:S

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, LX/0ad;->a(SZ)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1814281
    iget-object v2, p0, LX/BkU;->a:Landroid/content/Context;

    const v3, 0x7f08213a

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/5OG;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v2

    .line 1814282
    new-instance v3, LX/BkP;

    invoke-direct {v3, p0}, LX/BkP;-><init>(LX/BkU;)V

    move-object v3, v3

    .line 1814283
    invoke-virtual {v2, v3}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1814284
    :cond_2
    iget-object v2, p0, LX/BkU;->a:Landroid/content/Context;

    const v3, 0x7f08213b

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/5OG;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v1

    .line 1814285
    new-instance v2, LX/BkQ;

    invoke-direct {v2, p0}, LX/BkQ;-><init>(LX/BkU;)V

    move-object v2, v2

    .line 1814286
    invoke-virtual {v1, v2}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1814287
    :cond_3
    invoke-virtual {v0, p1}, LX/0ht;->a(Landroid/view/View;)V

    .line 1814288
    return-void
.end method

.method public final a(Lcom/facebook/events/create/EventCompositionModel;LX/9el;LX/Biv;)V
    .locals 2

    .prologue
    .line 1814260
    iput-object p1, p0, LX/BkU;->j:Lcom/facebook/events/create/EventCompositionModel;

    .line 1814261
    iput-object p2, p0, LX/BkU;->h:LX/9el;

    .line 1814262
    iput-object p3, p0, LX/BkU;->i:LX/Biv;

    .line 1814263
    iget-object v0, p0, LX/BkU;->d:LX/BkO;

    iget-object v1, p0, LX/BkU;->j:Lcom/facebook/events/create/EventCompositionModel;

    .line 1814264
    iput-object v1, v0, LX/BkO;->d:Lcom/facebook/events/create/EventCompositionModel;

    .line 1814265
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1814259
    iget-boolean v0, p0, LX/BkU;->l:Z

    return v0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1814255
    const-string v0, "hasClickedOnCoverPhotoThemes"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, LX/BkU;->n:Z

    .line 1814256
    const-string v0, "hasClickedOnCoverPhotoUpload"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, LX/BkU;->l:Z

    .line 1814257
    const-string v0, "hasClickedOnCoverPhotoFbAlbum"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, LX/BkU;->m:Z

    .line 1814258
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1814254
    iget-boolean v0, p0, LX/BkU;->m:Z

    return v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1814253
    iget-boolean v0, p0, LX/BkU;->n:Z

    return v0
.end method
