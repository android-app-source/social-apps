.class public LX/AVV;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/AVV;


# instance fields
.field private final a:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1679871
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1679872
    iput-object p1, p0, LX/AVV;->a:LX/0Zb;

    .line 1679873
    return-void
.end method

.method public static a(LX/0QB;)LX/AVV;
    .locals 4

    .prologue
    .line 1679874
    sget-object v0, LX/AVV;->b:LX/AVV;

    if-nez v0, :cond_1

    .line 1679875
    const-class v1, LX/AVV;

    monitor-enter v1

    .line 1679876
    :try_start_0
    sget-object v0, LX/AVV;->b:LX/AVV;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1679877
    if-eqz v2, :cond_0

    .line 1679878
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1679879
    new-instance p0, LX/AVV;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-direct {p0, v3}, LX/AVV;-><init>(LX/0Zb;)V

    .line 1679880
    move-object v0, p0

    .line 1679881
    sput-object v0, LX/AVV;->b:LX/AVV;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1679882
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1679883
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1679884
    :cond_1
    sget-object v0, LX/AVV;->b:LX/AVV;

    return-object v0

    .line 1679885
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1679886
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 6

    .prologue
    .line 1679887
    invoke-static {p3}, LX/Abd;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    move-result-object v0

    .line 1679888
    if-nez v0, :cond_1

    .line 1679889
    :cond_0
    :goto_0
    return-void

    .line 1679890
    :cond_1
    invoke-static {p3}, LX/17E;->v(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    .line 1679891
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1679892
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v2, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v3, "facecast"

    .line 1679893
    iput-object v3, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1679894
    move-object v2, v2

    .line 1679895
    const-string v3, "event"

    invoke-virtual {v2, v3, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "lobby_join_leave_reason"

    invoke-virtual {v2, v3, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "video_id"

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v2, v3, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "schedule_id"

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->k()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v1, v2, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 1679896
    iget-object v1, p0, LX/AVV;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 1

    .prologue
    .line 1679897
    const-string v0, "join_lobby"

    invoke-direct {p0, v0, p1, p2}, LX/AVV;->a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 1679898
    return-void
.end method

.method public final b(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 1

    .prologue
    .line 1679899
    const-string v0, "leave_lobby"

    invoke-direct {p0, v0, p1, p2}, LX/AVV;->a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 1679900
    return-void
.end method
