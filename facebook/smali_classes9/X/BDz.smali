.class public final LX/BDz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3Cf;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/3Cf",
        "<",
        "Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:LX/0Pz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Pz",
            "<",
            "Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1764009
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1764010
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    iput-object v0, p0, LX/BDz;->a:LX/0Pz;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1764011
    check-cast p1, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;

    .line 1764012
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 1764013
    check-cast p1, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;

    .line 1764014
    iget-object v0, p0, LX/BDz;->a:LX/0Pz;

    invoke-virtual {v0, p1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1764015
    return-void
.end method
