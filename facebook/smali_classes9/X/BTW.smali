.class public final enum LX/BTW;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/BTW;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/BTW;

.field public static final enum COLLAPSE:LX/BTW;

.field public static final enum EXPAND:LX/BTW;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1787681
    new-instance v0, LX/BTW;

    const-string v1, "EXPAND"

    invoke-direct {v0, v1, v2}, LX/BTW;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BTW;->EXPAND:LX/BTW;

    .line 1787682
    new-instance v0, LX/BTW;

    const-string v1, "COLLAPSE"

    invoke-direct {v0, v1, v3}, LX/BTW;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/BTW;->COLLAPSE:LX/BTW;

    .line 1787683
    const/4 v0, 0x2

    new-array v0, v0, [LX/BTW;

    sget-object v1, LX/BTW;->EXPAND:LX/BTW;

    aput-object v1, v0, v2

    sget-object v1, LX/BTW;->COLLAPSE:LX/BTW;

    aput-object v1, v0, v3

    sput-object v0, LX/BTW;->$VALUES:[LX/BTW;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1787680
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/BTW;
    .locals 1

    .prologue
    .line 1787679
    const-class v0, LX/BTW;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/BTW;

    return-object v0
.end method

.method public static values()[LX/BTW;
    .locals 1

    .prologue
    .line 1787678
    sget-object v0, LX/BTW;->$VALUES:[LX/BTW;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/BTW;

    return-object v0
.end method
