.class public LX/C9M;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/C9K;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/C9N;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1853986
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/C9M;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/C9N;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1853987
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1853988
    iput-object p1, p0, LX/C9M;->b:LX/0Ot;

    .line 1853989
    return-void
.end method

.method public static a(LX/0QB;)LX/C9M;
    .locals 4

    .prologue
    .line 1853990
    const-class v1, LX/C9M;

    monitor-enter v1

    .line 1853991
    :try_start_0
    sget-object v0, LX/C9M;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1853992
    sput-object v2, LX/C9M;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1853993
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1853994
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1853995
    new-instance v3, LX/C9M;

    const/16 p0, 0x2066

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/C9M;-><init>(LX/0Ot;)V

    .line 1853996
    move-object v0, v3

    .line 1853997
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1853998
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C9M;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1853999
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1854000
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 3

    .prologue
    .line 1854001
    iget-object v0, p0, LX/C9M;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    const/4 v1, 0x2

    .line 1854002
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, v1}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, v1}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v0

    const/4 v1, 0x3

    invoke-interface {v0, v1}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v0

    const/4 v1, 0x7

    const v2, 0x7f0b1cc1

    invoke-interface {v0, v1, v2}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v0

    const v1, 0x7f020ac9

    invoke-interface {v0, v1}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v0

    const/16 p2, 0x18

    .line 1854003
    invoke-static {p1}, LX/5Jt;->c(LX/1De;)LX/5Jr;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    const/4 v2, 0x6

    sget p0, LX/C9N;->a:I

    invoke-interface {v1, v2, p0}, LX/1Di;->c(II)LX/1Di;

    move-result-object v1

    invoke-interface {v1, p2}, LX/1Di;->r(I)LX/1Di;

    move-result-object v1

    invoke-interface {v1, p2}, LX/1Di;->j(I)LX/1Di;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    move-object v1, v1

    .line 1854004
    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v0

    invoke-static {p1}, LX/C9N;->d(LX/1De;)LX/1X1;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1X1;)LX/1Dh;

    move-result-object v0

    .line 1854005
    const/4 v1, 0x0

    const v2, 0x7f0e0122

    invoke-static {p1, v1, v2}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v1

    const v2, 0x7f0810a4

    invoke-virtual {v1, v2}, LX/1ne;->h(I)LX/1ne;

    move-result-object v1

    const v2, 0x7f0a05d2

    invoke-virtual {v1, v2}, LX/1ne;->n(I)LX/1ne;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-interface {v1, v2}, LX/1Di;->a(F)LX/1Di;

    move-result-object v1

    const/4 v2, 0x6

    sget p0, LX/C9N;->a:I

    invoke-interface {v1, v2, p0}, LX/1Di;->g(II)LX/1Di;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    move-object v1, v1

    .line 1854006
    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v0

    invoke-static {p1}, LX/C9N;->d(LX/1De;)LX/1X1;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1X1;)LX/1Dh;

    move-result-object v0

    .line 1854007
    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v1

    const v2, 0x7f021167    # 1.7289E38f

    invoke-virtual {v1, v2}, LX/1o5;->h(I)LX/1o5;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    const v2, 0x7f08109e

    invoke-interface {v1, v2}, LX/1Di;->A(I)LX/1Di;

    move-result-object v1

    const/4 v2, 0x6

    sget p0, LX/C9N;->a:I

    invoke-interface {v1, v2, p0}, LX/1Di;->g(II)LX/1Di;

    move-result-object v1

    .line 1854008
    const v2, -0x4757780e

    const/4 p0, 0x0

    invoke-static {p1, v2, p0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v2

    move-object v2, v2

    .line 1854009
    invoke-interface {v1, v2}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    move-object v1, v1

    .line 1854010
    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    move-object v0, v0

    .line 1854011
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1854012
    invoke-static {}, LX/1dS;->b()V

    .line 1854013
    iget v0, p1, LX/1dQ;->b:I

    .line 1854014
    packed-switch v0, :pswitch_data_0

    .line 1854015
    :goto_0
    return-object v2

    .line 1854016
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 1854017
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 1854018
    check-cast v1, LX/C9L;

    .line 1854019
    iget-object p1, p0, LX/C9M;->b:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/C9N;

    iget-object p2, v1, LX/C9L;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1854020
    iget-object v1, p1, LX/C9N;->b:LX/C99;

    .line 1854021
    iget-object p0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object p0, p0

    .line 1854022
    check-cast p0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1, p0, v0}, LX/C99;->a(Lcom/facebook/graphql/model/GraphQLStory;Landroid/view/View;)V

    .line 1854023
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x4757780e
        :pswitch_0
    .end packed-switch
.end method
