.class public LX/CMG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8CX;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/8CX",
        "<",
        "LX/7HE;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/3Rl;

.field public final b:Landroid/content/Context;

.field public final c:LX/CM4;

.field public final d:LX/7kc;

.field public final e:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/CM3;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/CM6;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/messaging/emoji/RecentEmojiView;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/3Rl;Landroid/content/Context;LX/CM4;LX/7kc;)V
    .locals 1
    .param p4    # LX/7kc;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1880349
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1880350
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/CMG;->e:Ljava/util/Set;

    .line 1880351
    iput-object p1, p0, LX/CMG;->a:LX/3Rl;

    .line 1880352
    iput-object p2, p0, LX/CMG;->b:Landroid/content/Context;

    .line 1880353
    iput-object p3, p0, LX/CMG;->c:LX/CM4;

    .line 1880354
    iput-object p4, p0, LX/CMG;->d:LX/7kc;

    .line 1880355
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1880386
    const/4 v0, -0x1

    return v0
.end method

.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 2

    .prologue
    .line 1880420
    new-instance v0, Landroid/widget/ImageView;

    iget-object v1, p0, LX/CMG;->b:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 1880421
    iget-object v1, p0, LX/CMG;->b:Landroid/content/Context;

    .line 1880422
    const p0, 0x7f010372

    const p1, 0x7f0212b0

    invoke-static {v1, p0, p1}, LX/0WH;->e(Landroid/content/Context;II)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    move-object v1, p0

    .line 1880423
    invoke-static {v0, v1}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 1880424
    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER_INSIDE:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 1880425
    new-instance v1, LX/CMF;

    invoke-direct {v1, v0}, LX/CMF;-><init>(Landroid/widget/ImageView;)V

    return-object v1
.end method

.method public final a(Ljava/lang/Object;Landroid/view/View;Landroid/view/ViewGroup;Z)Landroid/view/View;
    .locals 2
    .param p2    # Landroid/view/View;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1880387
    check-cast p1, LX/7HE;

    .line 1880388
    iget-object v0, p1, LX/7HE;->d:LX/7HD;

    move-object v0, v0

    .line 1880389
    sget-object v1, LX/7HD;->RECENTLY_USED:LX/7HD;

    if-ne v0, v1, :cond_1

    .line 1880390
    check-cast p2, Lcom/facebook/messaging/emoji/RecentEmojiView;

    iput-object p2, p0, LX/CMG;->g:Lcom/facebook/messaging/emoji/RecentEmojiView;

    .line 1880391
    iget-object v0, p0, LX/CMG;->g:Lcom/facebook/messaging/emoji/RecentEmojiView;

    if-nez v0, :cond_0

    .line 1880392
    new-instance v0, Lcom/facebook/messaging/emoji/RecentEmojiView;

    iget-object v1, p0, LX/CMG;->b:Landroid/content/Context;

    iget-object p1, p0, LX/CMG;->d:LX/7kc;

    invoke-direct {v0, v1, p1}, Lcom/facebook/messaging/emoji/RecentEmojiView;-><init>(Landroid/content/Context;LX/7kc;)V

    iput-object v0, p0, LX/CMG;->g:Lcom/facebook/messaging/emoji/RecentEmojiView;

    .line 1880393
    iget-object v0, p0, LX/CMG;->g:Lcom/facebook/messaging/emoji/RecentEmojiView;

    iget-object v1, p0, LX/CMG;->f:LX/CM6;

    .line 1880394
    iput-object v1, v0, Lcom/facebook/messaging/emoji/RecentEmojiView;->f:LX/CM6;

    .line 1880395
    iget-object p1, v0, Lcom/facebook/messaging/emoji/RecentEmojiView;->g:LX/CM3;

    if-eqz p1, :cond_0

    .line 1880396
    iget-object p1, v0, Lcom/facebook/messaging/emoji/RecentEmojiView;->g:LX/CM3;

    .line 1880397
    iput-object v1, p1, LX/CM3;->j:LX/CM6;

    .line 1880398
    :cond_0
    iget-object v0, p0, LX/CMG;->g:Lcom/facebook/messaging/emoji/RecentEmojiView;

    move-object v0, v0

    .line 1880399
    :goto_0
    return-object v0

    .line 1880400
    :cond_1
    iget-object v0, p1, LX/7HE;->c:Ljava/util/List;

    move-object v0, v0

    .line 1880401
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    .line 1880402
    check-cast p2, Landroid/support/v7/widget/RecyclerView;

    .line 1880403
    if-nez p2, :cond_2

    .line 1880404
    new-instance p2, Landroid/support/v7/widget/RecyclerView;

    iget-object v0, p0, LX/CMG;->b:Landroid/content/Context;

    invoke-direct {p2, v0}, Landroid/support/v7/widget/RecyclerView;-><init>(Landroid/content/Context;)V

    .line 1880405
    new-instance v0, LX/3wu;

    iget-object p3, p0, LX/CMG;->b:Landroid/content/Context;

    iget-object p4, p0, LX/CMG;->d:LX/7kc;

    .line 1880406
    iget p1, p4, LX/7kc;->a:I

    move p4, p1

    .line 1880407
    invoke-direct {v0, p3, p4}, LX/3wu;-><init>(Landroid/content/Context;I)V

    .line 1880408
    invoke-virtual {p2, v0}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 1880409
    iget-object v0, p0, LX/CMG;->c:LX/CM4;

    iget-object p3, p0, LX/CMG;->d:LX/7kc;

    invoke-virtual {v0, p3}, LX/CM4;->a(LX/7kc;)LX/CM3;

    move-result-object v0

    .line 1880410
    iget-object p3, p0, LX/CMG;->e:Ljava/util/Set;

    invoke-interface {p3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1880411
    iget-object p3, p0, LX/CMG;->f:LX/CM6;

    .line 1880412
    iput-object p3, v0, LX/CM3;->j:LX/CM6;

    .line 1880413
    invoke-virtual {p2, v0}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 1880414
    invoke-virtual {v0, v1}, LX/CM3;->a(LX/0Px;)V

    .line 1880415
    :goto_1
    move-object v0, p2

    .line 1880416
    goto :goto_0

    .line 1880417
    :cond_2
    iget-object v0, p2, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object v0, v0

    .line 1880418
    check-cast v0, LX/CM3;

    .line 1880419
    invoke-virtual {v0, v1}, LX/CM3;->a(LX/0Px;)V

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1880373
    check-cast p1, LX/7HE;

    .line 1880374
    iget v0, p1, LX/7HE;->a:I

    move v0, v0

    .line 1880375
    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/1a1;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 1880376
    check-cast p2, LX/7HE;

    .line 1880377
    check-cast p1, LX/CMF;

    .line 1880378
    iget-object v0, p1, LX/CMF;->l:Landroid/widget/ImageView;

    .line 1880379
    iget v1, p2, LX/7HE;->a:I

    move v1, v1

    .line 1880380
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1880381
    iget-object v0, p0, LX/CMG;->b:Landroid/content/Context;

    .line 1880382
    iget v1, p2, LX/7HE;->b:I

    move v1, v1

    .line 1880383
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1880384
    iget-object v1, p1, LX/CMF;->l:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1880385
    return-void
.end method

.method public final b(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1880370
    check-cast p1, LX/7HE;

    .line 1880371
    iget-object v0, p1, LX/7HE;->d:LX/7HD;

    move-object v0, v0

    .line 1880372
    invoke-virtual {v0}, LX/7HD;->ordinal()I

    move-result v0

    return v0
.end method

.method public final c(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 1880369
    const/4 v0, 0x0

    return v0
.end method

.method public final d(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1880357
    check-cast p1, LX/7HE;

    .line 1880358
    iget-object v0, p0, LX/CMG;->a:LX/3Rl;

    const-string v1, "Tab switched"

    sget-object v2, LX/6FV;->COMPOSE_MESSAGE_FLOW:LX/6FV;

    invoke-virtual {v0, v1, v2}, LX/3Rl;->a(Ljava/lang/String;LX/6FV;)V

    .line 1880359
    iget v0, p1, LX/7HE;->a:I

    move v0, v0

    .line 1880360
    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/CMG;->h:Ljava/lang/String;

    .line 1880361
    iget-object v0, p1, LX/7HE;->d:LX/7HD;

    move-object v0, v0

    .line 1880362
    sget-object v1, LX/7HD;->RECENTLY_USED:LX/7HD;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, LX/CMG;->g:Lcom/facebook/messaging/emoji/RecentEmojiView;

    if-eqz v0, :cond_0

    .line 1880363
    iget-object v0, p0, LX/CMG;->g:Lcom/facebook/messaging/emoji/RecentEmojiView;

    .line 1880364
    iget-object v1, v0, Lcom/facebook/messaging/emoji/RecentEmojiView;->g:LX/CM3;

    if-eqz v1, :cond_0

    .line 1880365
    iget-object v1, v0, Lcom/facebook/messaging/emoji/RecentEmojiView;->e:LX/6e7;

    .line 1880366
    iget-object v2, v1, LX/6e7;->a:LX/0Px;

    move-object v1, v2

    .line 1880367
    invoke-static {v0, v1}, Lcom/facebook/messaging/emoji/RecentEmojiView;->a$redex0(Lcom/facebook/messaging/emoji/RecentEmojiView;LX/0Px;)V

    .line 1880368
    :cond_0
    return-void
.end method

.method public final e(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1880356
    const/4 v0, 0x1

    return v0
.end method
