.class public LX/AVM;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "LX/7RY;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile a:LX/7RY;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1679231
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/7RY;
    .locals 4

    .prologue
    .line 1679232
    sget-object v0, LX/AVM;->a:LX/7RY;

    if-nez v0, :cond_1

    .line 1679233
    const-class v1, LX/AVM;

    monitor-enter v1

    .line 1679234
    :try_start_0
    sget-object v0, LX/AVM;->a:LX/7RY;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1679235
    if-eqz v2, :cond_0

    .line 1679236
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1679237
    const/16 v3, 0x384a

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 p0, 0x384c

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v3, p0}, LX/2uI;->a(LX/0Ot;LX/0Ot;)LX/7RY;

    move-result-object v3

    move-object v0, v3

    .line 1679238
    sput-object v0, LX/AVM;->a:LX/7RY;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1679239
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1679240
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1679241
    :cond_1
    sget-object v0, LX/AVM;->a:LX/7RY;

    return-object v0

    .line 1679242
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1679243
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1679244
    const/16 v0, 0x384a

    invoke-static {p0, v0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v0

    const/16 v1, 0x384c

    invoke-static {p0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-static {v0, v1}, LX/2uI;->a(LX/0Ot;LX/0Ot;)LX/7RY;

    move-result-object v0

    return-object v0
.end method
