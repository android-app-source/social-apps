.class public LX/AZO;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/AZO;


# instance fields
.field public final b:Ljava/lang/String;

.field public final c:Lcom/facebook/videocodec/effects/model/ColorFilter;

.field public final d:LX/7S6;


# direct methods
.method public static constructor <clinit>()V
    .locals 11

    .prologue
    const-wide/16 v2, 0x0

    .line 1686952
    new-instance v0, LX/AZO;

    const-string v1, "No Filter"

    const/4 v10, 0x0

    move-wide v4, v2

    move-wide v6, v2

    move-wide v8, v2

    invoke-direct/range {v0 .. v10}, LX/AZO;-><init>(Ljava/lang/String;DDDDZ)V

    sput-object v0, LX/AZO;->a:LX/AZO;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;DDDDZ)V
    .locals 10

    .prologue
    .line 1686953
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1686954
    iput-object p1, p0, LX/AZO;->b:Ljava/lang/String;

    .line 1686955
    new-instance v2, Lcom/facebook/videocodec/effects/model/ColorFilter;

    double-to-float v4, p4

    double-to-float v5, p2

    move-wide/from16 v0, p8

    double-to-float v6, v0

    move-wide/from16 v0, p6

    double-to-float v7, v0

    move-object v3, p1

    move/from16 v8, p10

    invoke-direct/range {v2 .. v8}, Lcom/facebook/videocodec/effects/model/ColorFilter;-><init>(Ljava/lang/String;FFFFZ)V

    iput-object v2, p0, LX/AZO;->c:Lcom/facebook/videocodec/effects/model/ColorFilter;

    .line 1686956
    new-instance v2, LX/7S6;

    iget-object v3, p0, LX/AZO;->c:Lcom/facebook/videocodec/effects/model/ColorFilter;

    invoke-direct {v2, v3}, LX/7S6;-><init>(Lcom/facebook/videocodec/effects/model/ColorFilter;)V

    iput-object v2, p0, LX/AZO;->d:LX/7S6;

    .line 1686957
    return-void
.end method

.method public static a(Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoBasicAdjustmentFilterModel;)LX/AZO;
    .locals 11
    .param p0    # Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoBasicAdjustmentFilterModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x0

    .line 1686958
    if-nez p0, :cond_1

    .line 1686959
    :cond_0
    :goto_0
    return-object v0

    .line 1686960
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoBasicAdjustmentFilterModel;->o()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    if-nez v1, :cond_2

    const/4 v1, 0x1

    :goto_1
    if-nez v1, :cond_0

    .line 1686961
    invoke-virtual {p0}, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoBasicAdjustmentFilterModel;->o()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v2, v0, LX/1vs;->b:I

    .line 1686962
    new-instance v0, LX/AZO;

    invoke-virtual {v1, v2, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoBasicAdjustmentFilterModel;->j()D

    move-result-wide v2

    invoke-virtual {p0}, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoBasicAdjustmentFilterModel;->n()D

    move-result-wide v4

    invoke-virtual {p0}, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoBasicAdjustmentFilterModel;->l()D

    move-result-wide v6

    invoke-virtual {p0}, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoBasicAdjustmentFilterModel;->k()D

    move-result-wide v8

    invoke-virtual {p0}, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoBasicAdjustmentFilterModel;->m()Z

    move-result v10

    invoke-direct/range {v0 .. v10}, LX/AZO;-><init>(Ljava/lang/String;DDDDZ)V

    goto :goto_0

    .line 1686963
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoBasicAdjustmentFilterModel;->o()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    invoke-virtual {v2, v1, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    goto :goto_1
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1686964
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, LX/AZO;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " BasicAdjustmentFilter:\n brightness: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/AZO;->c:Lcom/facebook/videocodec/effects/model/ColorFilter;

    .line 1686965
    iget v2, v1, Lcom/facebook/videocodec/effects/model/ColorFilter;->c:F

    move v1, v2

    .line 1686966
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\nsaturation: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/AZO;->c:Lcom/facebook/videocodec/effects/model/ColorFilter;

    .line 1686967
    iget v2, v1, Lcom/facebook/videocodec/effects/model/ColorFilter;->b:F

    move v1, v2

    .line 1686968
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\nhue: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/AZO;->c:Lcom/facebook/videocodec/effects/model/ColorFilter;

    .line 1686969
    iget v2, v1, Lcom/facebook/videocodec/effects/model/ColorFilter;->e:F

    move v1, v2

    .line 1686970
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\ncontrast: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/AZO;->c:Lcom/facebook/videocodec/effects/model/ColorFilter;

    .line 1686971
    iget v2, v1, Lcom/facebook/videocodec/effects/model/ColorFilter;->d:F

    move v1, v2

    .line 1686972
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\nhueColorize: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/AZO;->c:Lcom/facebook/videocodec/effects/model/ColorFilter;

    .line 1686973
    iget-boolean v2, v1, Lcom/facebook/videocodec/effects/model/ColorFilter;->f:Z

    move v1, v2

    .line 1686974
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
