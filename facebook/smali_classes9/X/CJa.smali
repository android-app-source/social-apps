.class public final enum LX/CJa;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/CJa;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/CJa;

.field public static final enum MESSAGE:LX/CJa;

.field public static final enum UNKNOWN:LX/CJa;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1875522
    new-instance v0, LX/CJa;

    const-string v1, "MESSAGE"

    invoke-direct {v0, v1, v2}, LX/CJa;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CJa;->MESSAGE:LX/CJa;

    .line 1875523
    new-instance v0, LX/CJa;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v3}, LX/CJa;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CJa;->UNKNOWN:LX/CJa;

    .line 1875524
    const/4 v0, 0x2

    new-array v0, v0, [LX/CJa;

    sget-object v1, LX/CJa;->MESSAGE:LX/CJa;

    aput-object v1, v0, v2

    sget-object v1, LX/CJa;->UNKNOWN:LX/CJa;

    aput-object v1, v0, v3

    sput-object v0, LX/CJa;->$VALUES:[LX/CJa;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1875525
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/CJa;
    .locals 1

    .prologue
    .line 1875526
    const-class v0, LX/CJa;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/CJa;

    return-object v0
.end method

.method public static values()[LX/CJa;
    .locals 1

    .prologue
    .line 1875527
    sget-object v0, LX/CJa;->$VALUES:[LX/CJa;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/CJa;

    return-object v0
.end method
