.class public final LX/AcP;
.super Landroid/os/Handler;
.source ""


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/facecastdisplay/LiveEventsPlugin;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/facecastdisplay/LiveEventsPlugin;)V
    .locals 1

    .prologue
    .line 1692502
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 1692503
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/AcP;->a:Ljava/lang/ref/WeakReference;

    .line 1692504
    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 2

    .prologue
    .line 1692505
    iget-object v0, p0, LX/AcP;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;

    .line 1692506
    if-nez v0, :cond_0

    .line 1692507
    :goto_0
    return-void

    .line 1692508
    :cond_0
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 1692509
    :pswitch_0
    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/LiveEventsPlugin;->g()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method
