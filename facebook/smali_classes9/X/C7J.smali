.class public final LX/C7J;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/C7M;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/1Pr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public c:Z

.field public d:LX/8pR;

.field public final synthetic e:LX/C7M;


# direct methods
.method public constructor <init>(LX/C7M;)V
    .locals 1

    .prologue
    .line 1851155
    iput-object p1, p0, LX/C7J;->e:LX/C7M;

    .line 1851156
    move-object v0, p1

    .line 1851157
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1851158
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1851159
    const-string v0, "AutoTranslationWithStateComponent"

    return-object v0
.end method

.method public final a(LX/1X1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1",
            "<",
            "LX/C7M;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1851160
    check-cast p1, LX/C7J;

    .line 1851161
    iget-object v0, p1, LX/C7J;->d:LX/8pR;

    iput-object v0, p0, LX/C7J;->d:LX/8pR;

    .line 1851162
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1851163
    if-ne p0, p1, :cond_1

    .line 1851164
    :cond_0
    :goto_0
    return v0

    .line 1851165
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1851166
    goto :goto_0

    .line 1851167
    :cond_3
    check-cast p1, LX/C7J;

    .line 1851168
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1851169
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1851170
    if-eq v2, v3, :cond_0

    .line 1851171
    iget-object v2, p0, LX/C7J;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/C7J;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/C7J;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1851172
    goto :goto_0

    .line 1851173
    :cond_5
    iget-object v2, p1, LX/C7J;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 1851174
    :cond_6
    iget-object v2, p0, LX/C7J;->b:LX/1Pr;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/C7J;->b:LX/1Pr;

    iget-object v3, p1, LX/C7J;->b:LX/1Pr;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1851175
    goto :goto_0

    .line 1851176
    :cond_8
    iget-object v2, p1, LX/C7J;->b:LX/1Pr;

    if-nez v2, :cond_7

    .line 1851177
    :cond_9
    iget-boolean v2, p0, LX/C7J;->c:Z

    iget-boolean v3, p1, LX/C7J;->c:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 1851178
    goto :goto_0
.end method

.method public final g()LX/1X1;
    .locals 2

    .prologue
    .line 1851179
    invoke-super {p0}, LX/1X1;->g()LX/1X1;

    move-result-object v0

    check-cast v0, LX/C7J;

    .line 1851180
    const/4 v1, 0x0

    iput-object v1, v0, LX/C7J;->d:LX/8pR;

    .line 1851181
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/C7J;->c:Z

    .line 1851182
    return-object v0
.end method
