.class public final LX/CWc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/BmO;


# instance fields
.field public final synthetic a:Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDateTimePickerView;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDateTimePickerView;)V
    .locals 0

    .prologue
    .line 1908443
    iput-object p1, p0, LX/CWc;->a:Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDateTimePickerView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/Calendar;)V
    .locals 4

    .prologue
    .line 1908444
    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    .line 1908445
    const/16 v1, 0xc

    invoke-virtual {p1, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    .line 1908446
    iget-object v2, p0, LX/CWc;->a:Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDateTimePickerView;

    iget-object v2, v2, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDateTimePickerView;->f:LX/CSY;

    const-string v3, "hour"

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, LX/CSY;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1908447
    iget-object v0, p0, LX/CWc;->a:Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDateTimePickerView;

    iget-object v0, v0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDateTimePickerView;->f:LX/CSY;

    const-string v2, "minute"

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, LX/CSY;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1908448
    iget-object v0, p0, LX/CWc;->a:Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDateTimePickerView;

    iget-object v0, v0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDateTimePickerView;->h:LX/CT7;

    iget-object v1, p0, LX/CWc;->a:Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDateTimePickerView;

    iget-object v1, v1, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDateTimePickerView;->g:LX/CTO;

    iget-object v1, v1, LX/CTJ;->o:Ljava/lang/String;

    iget-object v2, p0, LX/CWc;->a:Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDateTimePickerView;

    iget-object v2, v2, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDateTimePickerView;->g:LX/CTO;

    iget-object v2, v2, LX/CTK;->e:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    iget-object v3, p0, LX/CWc;->a:Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDateTimePickerView;

    iget-object v3, v3, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDateTimePickerView;->f:LX/CSY;

    invoke-virtual {v0, v1, v2, v3}, LX/CT7;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;LX/CSY;)V

    .line 1908449
    return-void
.end method
