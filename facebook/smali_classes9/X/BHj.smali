.class public LX/BHj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/BGy;


# instance fields
.field public final a:Z

.field public final b:LX/BHl;

.field private final c:Landroid/view/View;

.field public final d:LX/BHJ;

.field private final e:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Lcom/facebook/photos/simplepicker/view/PickerLongPressProgressBar;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/4gI;

.field private g:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/BGx;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/view/View;LX/BHJ;LX/0am;LX/BHl;Z)V
    .locals 3
    .param p1    # Landroid/view/View;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/BHJ;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/0am;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/BHl;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "LX/BHJ;",
            "LX/0am",
            "<",
            "Lcom/facebook/photos/simplepicker/view/PickerLongPressProgressBar;",
            ">;",
            "LX/BHl;",
            "Z)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1769563
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1769564
    iput-object p1, p0, LX/BHj;->c:Landroid/view/View;

    .line 1769565
    iput-object p2, p0, LX/BHj;->d:LX/BHJ;

    .line 1769566
    iput-object p3, p0, LX/BHj;->e:LX/0am;

    .line 1769567
    iput-object p4, p0, LX/BHj;->b:LX/BHl;

    .line 1769568
    iget-object v0, p0, LX/BHj;->d:LX/BHJ;

    .line 1769569
    new-instance v1, LX/BHi;

    invoke-direct {v1, p0}, LX/BHi;-><init>(LX/BHj;)V

    move-object v1, v1

    .line 1769570
    iput-object v1, v0, LX/BHJ;->i:LX/BHi;

    .line 1769571
    new-instance v1, LX/0zw;

    iget-object v0, p0, LX/BHj;->c:Landroid/view/View;

    const v2, 0x7f0d2c7d

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-direct {v1, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    iput-object v1, p0, LX/BHj;->g:LX/0zw;

    .line 1769572
    iput-boolean p5, p0, LX/BHj;->a:Z

    .line 1769573
    return-void
.end method


# virtual methods
.method public final a(LX/4gI;)V
    .locals 2

    .prologue
    .line 1769574
    iget-object v0, p0, LX/BHj;->f:LX/4gI;

    if-eq v0, p1, :cond_0

    .line 1769575
    iput-object p1, p0, LX/BHj;->f:LX/4gI;

    .line 1769576
    iget-object v0, p0, LX/BHj;->b:LX/BHl;

    iget-object v1, p0, LX/BHj;->f:LX/4gI;

    invoke-virtual {v0, v1}, LX/BHl;->a(LX/4gI;)V

    .line 1769577
    :cond_0
    return-void
.end method

.method public final a(Landroid/database/Cursor;)V
    .locals 12

    .prologue
    .line 1769578
    iget-object v0, p0, LX/BHj;->b:LX/BHl;

    iget-object v1, p0, LX/BHj;->d:LX/BHJ;

    iget-object v2, p0, LX/BHj;->e:LX/0am;

    iget-boolean v3, p0, LX/BHj;->a:Z

    .line 1769579
    iget-object v4, v0, LX/BHl;->b:LX/BHe;

    iget-object v6, v0, LX/BHl;->c:LX/BH5;

    iget-boolean v10, v0, LX/BHl;->d:Z

    iget-boolean v11, v0, LX/BHl;->e:Z

    move-object v5, p1

    move-object v7, v1

    move-object v8, v2

    move v9, v3

    invoke-virtual/range {v4 .. v11}, LX/BHe;->a(Landroid/database/Cursor;LX/BH5;LX/BHJ;LX/0am;ZZZ)LX/BHd;

    move-result-object v4

    iput-object v4, v0, LX/BHl;->f:LX/BHd;

    .line 1769580
    iget-object v4, v0, LX/BHl;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v4}, Landroid/support/v7/widget/RecyclerView;->getLayoutManager()LX/1OR;

    move-result-object v4

    check-cast v4, LX/3wu;

    .line 1769581
    new-instance v5, LX/BHk;

    invoke-direct {v5, v0, v4}, LX/BHk;-><init>(LX/BHl;LX/3wu;)V

    .line 1769582
    iput-object v5, v4, LX/3wu;->h:LX/3wr;

    .line 1769583
    iget-object v4, v0, LX/BHl;->f:LX/BHd;

    iget v5, v0, LX/BHl;->h:I

    .line 1769584
    iput v5, v4, LX/BHd;->f:I

    .line 1769585
    iget-object v4, v0, LX/BHl;->a:Landroid/support/v7/widget/RecyclerView;

    iget-object v5, v0, LX/BHl;->f:LX/BHd;

    invoke-virtual {v4, v5}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 1769586
    iget-object v4, v0, LX/BHl;->g:LX/BGf;

    if-eqz v4, :cond_0

    .line 1769587
    iget-object v4, v0, LX/BHl;->g:LX/BGf;

    invoke-virtual {v0, v4}, LX/BHl;->a(LX/BGf;)V

    .line 1769588
    :cond_0
    iget-object v0, p0, LX/BHj;->b:LX/BHl;

    iget-object v1, p0, LX/BHj;->f:LX/4gI;

    invoke-virtual {v0, v1}, LX/BHl;->a(LX/4gI;)V

    .line 1769589
    iget-object v0, p0, LX/BHj;->b:LX/BHl;

    invoke-virtual {v0}, LX/BHl;->b()I

    move-result v0

    if-nez v0, :cond_2

    .line 1769590
    iget-object v0, p0, LX/BHj;->g:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1769591
    :cond_1
    :goto_0
    return-void

    .line 1769592
    :cond_2
    iget-object v0, p0, LX/BHj;->g:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1769593
    iget-object v0, p0, LX/BHj;->g:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public final a()Z
    .locals 2

    .prologue
    .line 1769594
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_0

    .line 1769595
    const/4 v0, 0x0

    .line 1769596
    :goto_0
    return v0

    :cond_0
    iget-boolean v0, p0, LX/BHj;->a:Z

    goto :goto_0
.end method

.method public final b()V
    .locals 12

    .prologue
    .line 1769597
    const/4 v3, 0x0

    .line 1769598
    iget-object v0, p0, LX/BHj;->d:LX/BHJ;

    invoke-virtual {v0}, LX/BHJ;->b()LX/0Px;

    move-result-object v5

    .line 1769599
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v4, v3

    :goto_0
    if-ge v4, v6, :cond_0

    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/media/MediaItem;

    move v2, v3

    .line 1769600
    :goto_1
    iget-object v1, p0, LX/BHj;->b:LX/BHl;

    invoke-virtual {v1}, LX/BHl;->a()I

    move-result v1

    if-ge v2, v1, :cond_2

    .line 1769601
    iget-object v1, p0, LX/BHj;->b:LX/BHl;

    invoke-virtual {v1, v2}, LX/BHl;->a(I)Landroid/view/View;

    move-result-object v1

    instance-of v1, v1, LX/BIF;

    if-eqz v1, :cond_1

    .line 1769602
    iget-object v1, p0, LX/BHj;->b:LX/BHl;

    invoke-virtual {v1, v2}, LX/BHl;->a(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, LX/BIF;

    .line 1769603
    invoke-interface {v1}, LX/BIF;->getMediaItem()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v7

    .line 1769604
    if-eqz v7, :cond_1

    .line 1769605
    invoke-virtual {v7}, Lcom/facebook/ipc/media/MediaItem;->c()J

    move-result-wide v8

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->c()J

    move-result-wide v10

    cmp-long v8, v8, v10

    if-nez v8, :cond_1

    invoke-virtual {v7}, Lcom/facebook/ipc/media/MediaItem;->l()LX/4gF;

    move-result-object v7

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->l()LX/4gF;

    move-result-object v8

    if-ne v7, v8, :cond_1

    invoke-interface {v1}, LX/BIF;->isSelected()Z

    move-result v7

    if-nez v7, :cond_1

    .line 1769606
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v0

    const/4 v2, 0x1

    invoke-interface {v1, v0, v2}, LX/BIF;->a(IZ)V

    .line 1769607
    :cond_0
    return-void

    .line 1769608
    :cond_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 1769609
    :cond_2
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0
.end method
