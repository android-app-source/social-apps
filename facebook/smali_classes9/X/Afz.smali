.class public LX/Afz;
.super LX/Aec;
.source ""


# static fields
.field public static final c:Ljava/lang/String;


# instance fields
.field private final d:Ljava/util/concurrent/Executor;

.field public final e:LX/03V;

.field private final f:LX/0tX;

.field public g:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/facecastdisplay/protocol/LiveVideoTipJarFragmentModels$LiveVideoVODTipEventsQueryModel;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1700049
    const-class v0, LX/Afz;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Afz;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/Executor;LX/0tX;LX/03V;)V
    .locals 0
    .param p1    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1700044
    invoke-direct {p0}, LX/Aec;-><init>()V

    .line 1700045
    iput-object p1, p0, LX/Afz;->d:Ljava/util/concurrent/Executor;

    .line 1700046
    iput-object p2, p0, LX/Afz;->f:LX/0tX;

    .line 1700047
    iput-object p3, p0, LX/Afz;->e:LX/03V;

    .line 1700048
    return-void
.end method

.method private a(IIZ)V
    .locals 3

    .prologue
    .line 1700035
    new-instance v0, LX/6U6;

    invoke-direct {v0}, LX/6U6;-><init>()V

    move-object v0, v0

    .line 1700036
    const-string v1, "targetID"

    iget-object v2, p0, LX/Aec;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1700037
    const-string v1, "after_time"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1700038
    const-string v1, "within_time"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1700039
    const-string v1, "scale"

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    .line 1700040
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 1700041
    iget-object v1, p0, LX/Afz;->f:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    iput-object v0, p0, LX/Afz;->g:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1700042
    iget-object v0, p0, LX/Afz;->g:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v1, LX/Afy;

    invoke-direct {v1, p0, p3}, LX/Afy;-><init>(LX/Afz;Z)V

    iget-object v2, p0, LX/Afz;->d:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1700043
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 2

    .prologue
    .line 1700033
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, p1, v1}, LX/Afz;->a(IIZ)V

    .line 1700034
    return-void
.end method

.method public final a(II)V
    .locals 1

    .prologue
    .line 1700031
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/Afz;->a(IIZ)V

    .line 1700032
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1700030
    iget-object v0, p0, LX/Afz;->g:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Afz;->g:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isDone()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1700027
    iget-object v0, p0, LX/Afz;->g:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    .line 1700028
    iget-object v0, p0, LX/Afz;->g:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 1700029
    :cond_0
    return-void
.end method
