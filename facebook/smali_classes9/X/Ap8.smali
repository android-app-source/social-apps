.class public final LX/Ap8;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/Ap8;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Ap6;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/Ap9;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1715465
    const/4 v0, 0x0

    sput-object v0, LX/Ap8;->a:LX/Ap8;

    .line 1715466
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/Ap8;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1715422
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1715423
    new-instance v0, LX/Ap9;

    invoke-direct {v0}, LX/Ap9;-><init>()V

    iput-object v0, p0, LX/Ap8;->c:LX/Ap9;

    .line 1715424
    return-void
.end method

.method public static c(LX/1De;)LX/Ap6;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1715457
    new-instance v1, LX/Ap7;

    invoke-direct {v1}, LX/Ap7;-><init>()V

    .line 1715458
    sget-object v2, LX/Ap8;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Ap6;

    .line 1715459
    if-nez v2, :cond_0

    .line 1715460
    new-instance v2, LX/Ap6;

    invoke-direct {v2}, LX/Ap6;-><init>()V

    .line 1715461
    :cond_0
    invoke-static {v2, p0, v0, v0, v1}, LX/Ap6;->a$redex0(LX/Ap6;LX/1De;IILX/Ap7;)V

    .line 1715462
    move-object v1, v2

    .line 1715463
    move-object v0, v1

    .line 1715464
    return-object v0
.end method

.method public static declared-synchronized q()LX/Ap8;
    .locals 2

    .prologue
    .line 1715453
    const-class v1, LX/Ap8;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/Ap8;->a:LX/Ap8;

    if-nez v0, :cond_0

    .line 1715454
    new-instance v0, LX/Ap8;

    invoke-direct {v0}, LX/Ap8;-><init>()V

    sput-object v0, LX/Ap8;->a:LX/Ap8;

    .line 1715455
    :cond_0
    sget-object v0, LX/Ap8;->a:LX/Ap8;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 1715456
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 12

    .prologue
    .line 1715427
    check-cast p2, LX/Ap7;

    .line 1715428
    iget-object v1, p2, LX/Ap7;->a:LX/1X1;

    iget-object v2, p2, LX/Ap7;->b:LX/1X1;

    iget-object v3, p2, LX/Ap7;->c:Ljava/lang/CharSequence;

    iget-object v4, p2, LX/Ap7;->d:Ljava/lang/CharSequence;

    iget-object v5, p2, LX/Ap7;->e:Ljava/lang/CharSequence;

    iget-object v6, p2, LX/Ap7;->f:LX/1dQ;

    move-object v0, p1

    const/4 v7, 0x0

    const/4 p2, 0x2

    const/4 p1, 0x4

    const/4 v8, 0x0

    .line 1715429
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    .line 1715430
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    .line 1715431
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p0

    .line 1715432
    if-eqz v1, :cond_0

    const/4 v9, 0x1

    .line 1715433
    :goto_0
    if-eqz v10, :cond_1

    if-eqz v11, :cond_1

    if-eqz p0, :cond_1

    .line 1715434
    :goto_1
    move-object v0, v7

    .line 1715435
    return-object v0

    :cond_0
    move v9, v8

    .line 1715436
    goto :goto_0

    .line 1715437
    :cond_1
    invoke-static {v0}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v10

    invoke-interface {v10, p2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v10

    invoke-interface {v10, p1}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v10

    const v11, 0x7f020b09

    invoke-interface {v10, v11}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v10

    invoke-interface {v10, v6}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v10

    if-nez v2, :cond_3

    :goto_2
    invoke-interface {v10, v7}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v10

    const/4 v7, 0x0

    .line 1715438
    new-instance v11, LX/ApB;

    invoke-direct {v11}, LX/ApB;-><init>()V

    .line 1715439
    sget-object p0, LX/ApC;->b:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/ApA;

    .line 1715440
    if-nez p0, :cond_2

    .line 1715441
    new-instance p0, LX/ApA;

    invoke-direct {p0}, LX/ApA;-><init>()V

    .line 1715442
    :cond_2
    invoke-static {p0, v0, v7, v7, v11}, LX/ApA;->a$redex0(LX/ApA;LX/1De;IILX/ApB;)V

    .line 1715443
    move-object v11, p0

    .line 1715444
    move-object v7, v11

    .line 1715445
    move-object v7, v7

    .line 1715446
    iget-object v11, v7, LX/ApA;->a:LX/ApB;

    iput-object v3, v11, LX/ApB;->a:Ljava/lang/CharSequence;

    .line 1715447
    move-object v7, v7

    .line 1715448
    iget-object v11, v7, LX/ApA;->a:LX/ApB;

    iput-object v4, v11, LX/ApB;->b:Ljava/lang/CharSequence;

    .line 1715449
    move-object v7, v7

    .line 1715450
    iget-object v11, v7, LX/ApA;->a:LX/ApB;

    iput-object v5, v11, LX/ApB;->c:Ljava/lang/CharSequence;

    .line 1715451
    move-object v7, v7

    .line 1715452
    invoke-virtual {v7}, LX/1X5;->c()LX/1Di;

    move-result-object v7

    const/high16 v11, 0x3f800000    # 1.0f

    invoke-interface {v7, v11}, LX/1Di;->b(F)LX/1Di;

    move-result-object v7

    invoke-interface {v7, v8}, LX/1Di;->e(I)LX/1Di;

    move-result-object v7

    const v11, 0x7f0b1169

    invoke-interface {v7, p1, v11}, LX/1Di;->c(II)LX/1Di;

    move-result-object v11

    const/4 p0, 0x5

    if-eqz v9, :cond_4

    move v7, v8

    :goto_3
    invoke-interface {v11, p0, v7}, LX/1Di;->c(II)LX/1Di;

    move-result-object v7

    invoke-interface {v10, v7}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v7

    invoke-interface {v7, v1}, LX/1Dh;->a(LX/1X1;)LX/1Dh;

    move-result-object v7

    invoke-interface {v7}, LX/1Di;->k()LX/1Dg;

    move-result-object v7

    goto :goto_1

    :cond_3
    invoke-static {v0, v2}, LX/1n9;->a(LX/1De;LX/1X1;)LX/1Di;

    move-result-object v7

    invoke-interface {v7, p2}, LX/1Di;->b(I)LX/1Di;

    move-result-object v7

    const/4 v11, 0x7

    const p0, 0x7f0b1169

    invoke-interface {v7, v11, p0}, LX/1Di;->c(II)LX/1Di;

    move-result-object v7

    const v11, 0x7f0b1169

    invoke-interface {v7, p1, v11}, LX/1Di;->c(II)LX/1Di;

    move-result-object v7

    goto :goto_2

    :cond_4
    const v7, 0x7f0b1169

    goto :goto_3
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1715425
    invoke-static {}, LX/1dS;->b()V

    .line 1715426
    const/4 v0, 0x0

    return-object v0
.end method
