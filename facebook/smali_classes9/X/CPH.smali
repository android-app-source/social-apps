.class public LX/CPH;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation


# static fields
.field private static final a:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "LX/CNb;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1884264
    new-instance v0, LX/CPG;

    invoke-direct {v0}, LX/CPG;-><init>()V

    sput-object v0, LX/CPH;->a:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1884265
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/1De;LX/CNb;LX/CNc;Ljava/util/List;)LX/1Dg;
    .locals 12
    .param p1    # LX/CNb;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p2    # LX/CNc;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # Ljava/util/List;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/CNb;",
            "LX/CNc;",
            "Ljava/util/List",
            "<",
            "LX/CNb;",
            ">;)",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    const/4 v9, 0x1

    const/4 v6, 0x0

    .line 1884226
    const-string v0, "direction"

    const-string v1, ""

    invoke-virtual {p1, v0, v1}, LX/CNb;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    .line 1884227
    const/4 v2, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v2, :pswitch_data_0

    .line 1884228
    :goto_1
    :pswitch_0
    move v4, v1

    .line 1884229
    invoke-static {p0}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, v4}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v0

    const-string v1, "justify-content"

    const-string v2, ""

    invoke-virtual {p1, v1, v2}, LX/CNb;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x2

    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 1884230
    const/4 v7, -0x1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v8

    sparse-switch v8, :sswitch_data_1

    :cond_1
    :goto_2
    packed-switch v7, :pswitch_data_1

    .line 1884231
    :goto_3
    :pswitch_1
    move v1, v2

    .line 1884232
    invoke-interface {v0, v1}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v0

    const-string v1, "align-items"

    const-string v2, ""

    invoke-virtual {p1, v1, v2}, LX/CNb;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x3

    const/4 v5, 0x2

    const/4 v2, 0x1

    .line 1884233
    const/4 v7, -0x1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v8

    sparse-switch v8, :sswitch_data_2

    :cond_2
    :goto_4
    packed-switch v7, :pswitch_data_2

    .line 1884234
    :goto_5
    :pswitch_2
    move v1, v2

    .line 1884235
    invoke-interface {v0, v1}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v0

    const-string v1, "padding-top"

    invoke-virtual {p1, v1, p0}, LX/CNb;->a(Ljava/lang/String;Landroid/content/Context;)I

    move-result v1

    invoke-interface {v0, v9, v1}, LX/1Dh;->s(II)LX/1Dh;

    move-result-object v0

    const/4 v1, 0x4

    const-string v2, "padding-left"

    invoke-virtual {p1, v2, p0}, LX/CNb;->a(Ljava/lang/String;Landroid/content/Context;)I

    move-result v2

    invoke-interface {v0, v1, v2}, LX/1Dh;->s(II)LX/1Dh;

    move-result-object v0

    const/4 v1, 0x3

    const-string v2, "padding-bottom"

    invoke-virtual {p1, v2, p0}, LX/CNb;->a(Ljava/lang/String;Landroid/content/Context;)I

    move-result v2

    invoke-interface {v0, v1, v2}, LX/1Dh;->s(II)LX/1Dh;

    move-result-object v0

    const/4 v1, 0x5

    const-string v2, "padding-right"

    invoke-virtual {p1, v2, p0}, LX/CNb;->a(Ljava/lang/String;Landroid/content/Context;)I

    move-result v2

    invoke-interface {v0, v1, v2}, LX/1Dh;->s(II)LX/1Dh;

    move-result-object v10

    .line 1884236
    new-instance v11, Ljava/util/ArrayList;

    const-string v0, "children"

    invoke-virtual {p1, v0}, LX/CNb;->a(Ljava/lang/String;)LX/0Px;

    move-result-object v0

    invoke-direct {v11, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1884237
    sget-object v0, LX/CPH;->a:Ljava/util/Comparator;

    invoke-static {v11, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1884238
    const/4 v2, -0x1

    move v1, v6

    .line 1884239
    :goto_6
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_8

    .line 1884240
    invoke-virtual {v11, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CNb;

    invoke-static {v0}, LX/CPH;->b(LX/CNb;)Z

    move-result v0

    if-nez v0, :cond_3

    move v7, v1

    :goto_7
    move v8, v6

    .line 1884241
    :goto_8
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v8, v0, :cond_5

    .line 1884242
    invoke-virtual {v11, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/CNb;

    const-string v0, "spacing"

    invoke-virtual {p1, v0, p0}, LX/CNb;->a(Ljava/lang/String;Landroid/content/Context;)I

    move-result v3

    if-ne v8, v7, :cond_4

    move v5, v9

    :goto_9
    move-object v0, p0

    move-object v1, p2

    invoke-static/range {v0 .. v5}, LX/CPH;->a(LX/1De;LX/CNc;LX/CNb;IIZ)LX/1Di;

    move-result-object v0

    invoke-interface {v10, v0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    .line 1884243
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    goto :goto_8

    .line 1884244
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_6

    :cond_4
    move v5, v6

    .line 1884245
    goto :goto_9

    .line 1884246
    :cond_5
    const-string v0, "touch-up-inside-actions"

    invoke-static {p2, p1, p3, v0}, LX/CPx;->a(LX/CNc;LX/CNb;Ljava/util/List;Ljava/lang/String;)LX/CNe;

    move-result-object v0

    .line 1884247
    if-eqz v0, :cond_6

    .line 1884248
    const v1, 0x59c53a21

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-static {p0, v1, v2}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v1

    move-object v0, v1

    .line 1884249
    invoke-interface {v10, v0}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    .line 1884250
    :cond_6
    const-string v0, "long-press-actions"

    invoke-static {p2, p1, p3, v0}, LX/CPx;->a(LX/CNc;LX/CNb;Ljava/util/List;Ljava/lang/String;)LX/CNe;

    move-result-object v0

    .line 1884251
    if-eqz v0, :cond_7

    .line 1884252
    const v1, 0x3eccc4be    # 0.39993852f

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-static {p0, v1, v2}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v1

    move-object v0, v1

    .line 1884253
    invoke-interface {v10, v0}, LX/1Di;->b(LX/1dQ;)LX/1Di;

    .line 1884254
    :cond_7
    invoke-static {v10, p0, p1, p3}, LX/CPx;->a(LX/1Di;LX/1De;LX/CNb;Ljava/util/List;)LX/1Dg;

    move-result-object v0

    return-object v0

    :cond_8
    move v7, v2

    goto :goto_7

    .line 1884255
    :sswitch_0
    const-string v3, "HORIZONTAL"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v2, v1

    goto/16 :goto_0

    :sswitch_1
    const-string v3, "VERTICAL"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v2, 0x1

    goto/16 :goto_0

    .line 1884256
    :pswitch_3
    const/4 v1, 0x2

    goto/16 :goto_1

    .line 1884257
    :sswitch_2
    const-string v8, "START"

    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    move v7, v2

    goto/16 :goto_2

    :sswitch_3
    const-string v8, "END"

    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    move v7, v5

    goto/16 :goto_2

    :sswitch_4
    const-string v8, "CENTER"

    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    move v7, v3

    goto/16 :goto_2

    :pswitch_4
    move v2, v3

    .line 1884258
    goto/16 :goto_3

    :pswitch_5
    move v2, v5

    .line 1884259
    goto/16 :goto_3

    .line 1884260
    :sswitch_5
    const-string v8, "START"

    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    const/4 v7, 0x0

    goto/16 :goto_4

    :sswitch_6
    const-string v8, "END"

    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    move v7, v2

    goto/16 :goto_4

    :sswitch_7
    const-string v8, "CENTER"

    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    move v7, v5

    goto/16 :goto_4

    :sswitch_8
    const-string v8, "STRETCH"

    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    move v7, v3

    goto/16 :goto_4

    :pswitch_6
    move v2, v3

    .line 1884261
    goto/16 :goto_5

    :pswitch_7
    move v2, v5

    .line 1884262
    goto/16 :goto_5

    .line 1884263
    :pswitch_8
    const/4 v2, 0x4

    goto/16 :goto_5

    :sswitch_data_0
    .sparse-switch
        -0x479da88a -> :sswitch_1
        0x6f9f7824 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_0
    .end packed-switch

    :sswitch_data_1
    .sparse-switch
        0x10cbb -> :sswitch_3
        0x4b8cc42 -> :sswitch_2
        0x7645c055 -> :sswitch_4
    .end sparse-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_4
        :pswitch_5
    .end packed-switch

    :sswitch_data_2
    .sparse-switch
        -0x455f8d1b -> :sswitch_8
        0x10cbb -> :sswitch_6
        0x4b8cc42 -> :sswitch_5
        0x7645c055 -> :sswitch_7
    .end sparse-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method private static a(LX/1De;LX/CNc;LX/CNb;IIZ)LX/1Di;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1884212
    invoke-static {p2, p1, p0}, LX/CNd;->a(LX/CNb;LX/CNc;LX/1De;)LX/1X1;

    move-result-object v3

    .line 1884213
    invoke-static {p2}, LX/CPH;->b(LX/CNb;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1884214
    invoke-static {p0, v3}, LX/1n9;->a(LX/1De;LX/1X1;)LX/1Di;

    move-result-object v0

    .line 1884215
    :goto_0
    return-object v0

    .line 1884216
    :cond_0
    const/4 v1, 0x2

    if-ne p4, v1, :cond_3

    .line 1884217
    const/4 v2, 0x4

    .line 1884218
    const/4 v1, 0x5

    .line 1884219
    :goto_1
    if-eqz p5, :cond_1

    move p3, v0

    .line 1884220
    :cond_1
    invoke-virtual {p2}, LX/CNb;->a()Ljava/lang/String;

    move-result-object v4

    const-string v5, "NT:BOX_CHILD"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1884221
    const-string v0, "spacing-before"

    invoke-virtual {p2, v0, p0}, LX/CNb;->a(Ljava/lang/String;Landroid/content/Context;)I

    move-result v0

    add-int/2addr p3, v0

    .line 1884222
    const-string v0, "spacing-after"

    invoke-virtual {p2, v0, p0}, LX/CNb;->a(Ljava/lang/String;Landroid/content/Context;)I

    move-result v0

    .line 1884223
    :cond_2
    invoke-static {p0, v3}, LX/1n9;->a(LX/1De;LX/1X1;)LX/1Di;

    move-result-object v3

    invoke-interface {v3, v2, p3}, LX/1Di;->a(II)LX/1Di;

    move-result-object v2

    invoke-interface {v2, v1, v0}, LX/1Di;->a(II)LX/1Di;

    move-result-object v0

    goto :goto_0

    .line 1884224
    :cond_3
    const/4 v2, 0x1

    .line 1884225
    const/4 v1, 0x3

    goto :goto_1
.end method

.method public static b(LX/CNb;)Z
    .locals 2

    .prologue
    .line 1884211
    invoke-virtual {p0}, LX/CNb;->a()Ljava/lang/String;

    move-result-object v0

    const-string v1, "NT:STACK_LAYOUT_ABSOLUTE_CHILD"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
