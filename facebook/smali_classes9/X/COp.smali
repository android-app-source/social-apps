.class public final LX/COp;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/COp;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/COn;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/COw;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1883620
    const/4 v0, 0x0

    sput-object v0, LX/COp;->a:LX/COp;

    .line 1883621
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/COp;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1883622
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1883623
    new-instance v0, LX/COw;

    invoke-direct {v0}, LX/COw;-><init>()V

    iput-object v0, p0, LX/COp;->c:LX/COw;

    .line 1883624
    return-void
.end method

.method public static declared-synchronized q()LX/COp;
    .locals 2

    .prologue
    .line 1883625
    const-class v1, LX/COp;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/COp;->a:LX/COp;

    if-nez v0, :cond_0

    .line 1883626
    new-instance v0, LX/COp;

    invoke-direct {v0}, LX/COp;-><init>()V

    sput-object v0, LX/COp;->a:LX/COp;

    .line 1883627
    :cond_0
    sget-object v0, LX/COp;->a:LX/COp;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 1883628
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 1883629
    check-cast p2, LX/COo;

    .line 1883630
    iget-object v0, p2, LX/COo;->a:LX/CNb;

    iget-object v1, p2, LX/COo;->b:LX/CNc;

    iget-object v2, p2, LX/COo;->c:Ljava/util/List;

    const/4 v5, 0x0

    .line 1883631
    const-string v3, "children"

    invoke-virtual {v0, v3}, LX/CNb;->a(Ljava/lang/String;)LX/0Px;

    move-result-object p0

    .line 1883632
    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    move v4, v5

    .line 1883633
    :goto_0
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v3

    if-ge v4, v3, :cond_0

    .line 1883634
    invoke-virtual {p0, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/CNb;

    invoke-static {v3, v1, p1}, LX/CNd;->a(LX/CNb;LX/CNc;LX/1De;)LX/1X1;

    move-result-object v3

    invoke-interface {p2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1883635
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_0

    .line 1883636
    :cond_0
    new-instance v3, LX/COu;

    invoke-virtual {p1}, LX/1De;->getBaseContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4, p2}, LX/COu;-><init>(Landroid/content/Context;Ljava/util/List;)V

    .line 1883637
    const/4 v4, 0x0

    .line 1883638
    new-instance p0, LX/COy;

    invoke-direct {p0}, LX/COy;-><init>()V

    .line 1883639
    sget-object p2, LX/COz;->b:LX/0Zi;

    invoke-virtual {p2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/COx;

    .line 1883640
    if-nez p2, :cond_1

    .line 1883641
    new-instance p2, LX/COx;

    invoke-direct {p2}, LX/COx;-><init>()V

    .line 1883642
    :cond_1
    invoke-static {p2, p1, v4, v4, p0}, LX/COx;->a$redex0(LX/COx;LX/1De;IILX/COy;)V

    .line 1883643
    move-object p0, p2

    .line 1883644
    move-object v4, p0

    .line 1883645
    move-object v4, v4

    .line 1883646
    iget-object p0, v4, LX/COx;->a:LX/COy;

    iput-object v3, p0, LX/COy;->a:LX/COu;

    .line 1883647
    iget-object p0, v4, LX/COx;->d:Ljava/util/BitSet;

    const/4 p2, 0x0

    invoke-virtual {p0, p2}, Ljava/util/BitSet;->set(I)V

    .line 1883648
    move-object v3, v4

    .line 1883649
    const-string v4, "disable-bounce"

    invoke-virtual {v0, v4, v5}, LX/CNb;->a(Ljava/lang/String;Z)Z

    move-result v4

    .line 1883650
    if-eqz v4, :cond_3

    const/4 p0, 0x2

    :goto_1
    move v4, p0

    .line 1883651
    iget-object p0, v3, LX/COx;->a:LX/COy;

    iput v4, p0, LX/COy;->b:I

    .line 1883652
    iget-object p0, v3, LX/COx;->d:Ljava/util/BitSet;

    const/4 p2, 0x1

    invoke-virtual {p0, p2}, Ljava/util/BitSet;->set(I)V

    .line 1883653
    move-object v3, v3

    .line 1883654
    const-string v4, "clipping-style"

    const-string p0, "CHILDREN"

    invoke-virtual {v0, v4, p0}, LX/CNb;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string p0, "CHILDREN"

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    .line 1883655
    iget-object p0, v3, LX/COx;->a:LX/COy;

    iput-boolean v4, p0, LX/COy;->c:Z

    .line 1883656
    iget-object p0, v3, LX/COx;->d:Ljava/util/BitSet;

    const/4 p2, 0x2

    invoke-virtual {p0, p2}, Ljava/util/BitSet;->set(I)V

    .line 1883657
    move-object v4, v3

    .line 1883658
    const-string v3, "align-items"

    const-string p0, "FREE"

    invoke-virtual {v0, v3, p0}, LX/CNb;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string p0, "FREE"

    invoke-virtual {v3, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    const/4 v3, 0x1

    .line 1883659
    :goto_2
    iget-object v5, v4, LX/COx;->a:LX/COy;

    iput-boolean v3, v5, LX/COy;->e:Z

    .line 1883660
    iget-object v5, v4, LX/COx;->d:Ljava/util/BitSet;

    const/4 p0, 0x4

    invoke-virtual {v5, p0}, Ljava/util/BitSet;->set(I)V

    .line 1883661
    move-object v3, v4

    .line 1883662
    new-instance v4, LX/COv;

    invoke-direct {v4, v0, p1}, LX/COv;-><init>(LX/CNb;LX/1De;)V

    .line 1883663
    iget-object v5, v3, LX/COx;->a:LX/COy;

    iput-object v4, v5, LX/COy;->d:LX/3x6;

    .line 1883664
    iget-object v5, v3, LX/COx;->d:Ljava/util/BitSet;

    const/4 p0, 0x3

    invoke-virtual {v5, p0}, Ljava/util/BitSet;->set(I)V

    .line 1883665
    move-object v3, v3

    .line 1883666
    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    .line 1883667
    invoke-static {v3, p1, v0, v2}, LX/CPx;->a(LX/1Di;LX/1De;LX/CNb;Ljava/util/List;)LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 1883668
    return-object v0

    :cond_2
    move v3, v5

    .line 1883669
    goto :goto_2

    :cond_3
    const/4 p0, 0x0

    goto :goto_1
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1883670
    invoke-static {}, LX/1dS;->b()V

    .line 1883671
    const/4 v0, 0x0

    return-object v0
.end method
