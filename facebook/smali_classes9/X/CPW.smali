.class public final LX/CPW;
.super LX/1n4;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1n4",
        "<",
        "Landroid/graphics/drawable/Drawable;",
        ">;"
    }
.end annotation


# static fields
.field private static a:LX/CPW;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/CPU;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/CPa;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1884614
    const/4 v0, 0x0

    sput-object v0, LX/CPW;->a:LX/CPW;

    .line 1884615
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/CPW;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1884616
    invoke-direct {p0}, LX/1n4;-><init>()V

    .line 1884617
    new-instance v0, LX/CPa;

    invoke-direct {v0}, LX/CPa;-><init>()V

    iput-object v0, p0, LX/CPW;->c:LX/CPa;

    .line 1884618
    return-void
.end method

.method public static declared-synchronized a()LX/CPW;
    .locals 2

    .prologue
    .line 1884619
    const-class v1, LX/CPW;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/CPW;->a:LX/CPW;

    if-nez v0, :cond_0

    .line 1884620
    new-instance v0, LX/CPW;

    invoke-direct {v0}, LX/CPW;-><init>()V

    sput-object v0, LX/CPW;->a:LX/CPW;

    .line 1884621
    :cond_0
    sget-object v0, LX/CPW;->a:LX/CPW;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 1884622
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1dc;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1884623
    check-cast p2, LX/CPV;

    .line 1884624
    iget-object v1, p2, LX/CPV;->a:Landroid/graphics/drawable/GradientDrawable$Orientation;

    iget-object v0, p2, LX/CPV;->b:[I

    check-cast v0, [I

    .line 1884625
    sget-object p0, LX/CPa;->a:LX/CPX;

    invoke-interface {p0, v1, v0}, LX/CPX;->a(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    move-object v0, p0

    .line 1884626
    return-object v0
.end method

.method public final a(LX/1De;Ljava/lang/Object;LX/1dc;)V
    .locals 0

    .prologue
    .line 1884627
    check-cast p2, Landroid/graphics/drawable/Drawable;

    .line 1884628
    sget-object p0, LX/CPa;->a:LX/CPX;

    invoke-interface {p0, p2}, LX/CPX;->a(Landroid/graphics/drawable/Drawable;)V

    .line 1884629
    return-void
.end method
