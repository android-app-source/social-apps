.class public LX/BiT;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1810342
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1810343
    return-void
.end method

.method public static a(LX/0QB;)LX/BiT;
    .locals 3

    .prologue
    .line 1810344
    const-class v1, LX/BiT;

    monitor-enter v1

    .line 1810345
    :try_start_0
    sget-object v0, LX/BiT;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1810346
    sput-object v2, LX/BiT;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1810347
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1810348
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 1810349
    new-instance v0, LX/BiT;

    invoke-direct {v0}, LX/BiT;-><init>()V

    .line 1810350
    move-object v0, v0

    .line 1810351
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1810352
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/BiT;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1810353
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1810354
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static b()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/BiU;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1810355
    sget-object v0, LX/BiU;->WATCH_INTERESTED:LX/BiU;

    sget-object v1, LX/BiU;->GOING:LX/BiU;

    sget-object v2, LX/BiU;->NOT_INTERESTED_OR_NOT_GOING_OR_IGNORE:LX/BiU;

    invoke-static {v0, v1, v2}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
