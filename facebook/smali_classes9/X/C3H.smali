.class public final LX/C3H;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/C3I;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/C33;

.field public b:LX/1Pb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public final synthetic c:LX/C3I;


# direct methods
.method public constructor <init>(LX/C3I;)V
    .locals 1

    .prologue
    .line 1845330
    iput-object p1, p0, LX/C3H;->c:LX/C3I;

    .line 1845331
    move-object v0, p1

    .line 1845332
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1845333
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1845334
    const-string v0, "CondensedStoryHeaderComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1845335
    if-ne p0, p1, :cond_1

    .line 1845336
    :cond_0
    :goto_0
    return v0

    .line 1845337
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1845338
    goto :goto_0

    .line 1845339
    :cond_3
    check-cast p1, LX/C3H;

    .line 1845340
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1845341
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1845342
    if-eq v2, v3, :cond_0

    .line 1845343
    iget-object v2, p0, LX/C3H;->a:LX/C33;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/C3H;->a:LX/C33;

    iget-object v3, p1, LX/C3H;->a:LX/C33;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1845344
    goto :goto_0

    .line 1845345
    :cond_5
    iget-object v2, p1, LX/C3H;->a:LX/C33;

    if-nez v2, :cond_4

    .line 1845346
    :cond_6
    iget-object v2, p0, LX/C3H;->b:LX/1Pb;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/C3H;->b:LX/1Pb;

    iget-object v3, p1, LX/C3H;->b:LX/1Pb;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1845347
    goto :goto_0

    .line 1845348
    :cond_7
    iget-object v2, p1, LX/C3H;->b:LX/1Pb;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
