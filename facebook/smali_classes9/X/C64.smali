.class public LX/C64;
.super LX/3VF;
.source ""


# instance fields
.field private final b:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1849621
    const v0, 0x7f0315b2

    invoke-direct {p0, p1, v0}, LX/3VF;-><init>(Landroid/content/Context;I)V

    .line 1849622
    const v0, 0x7f0d117e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/C64;->b:Landroid/widget/TextView;

    .line 1849623
    return-void
.end method


# virtual methods
.method public setPlayCountText(I)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1849624
    if-nez p1, :cond_0

    .line 1849625
    iget-object v0, p0, LX/C64;->b:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1849626
    :goto_0
    return-void

    .line 1849627
    :cond_0
    if-gtz p1, :cond_1

    invoke-virtual {p0}, LX/3VF;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    move v0, v2

    :goto_1
    invoke-virtual {p0, v0}, LX/3VF;->setIsExpanded(Z)V

    .line 1849628
    iget-object v0, p0, LX/C64;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1849629
    invoke-virtual {p0}, LX/C64;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0f006f

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v1

    invoke-virtual {v0, v3, p1, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1849630
    iget-object v1, p0, LX/C64;->b:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_2
    move v0, v1

    .line 1849631
    goto :goto_1
.end method
