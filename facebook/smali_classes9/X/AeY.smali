.class public final LX/AeY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveVideoTopLevelCommentsSubscriptionModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/AeZ;


# direct methods
.method public constructor <init>(LX/AeZ;)V
    .locals 0

    .prologue
    .line 1697834
    iput-object p1, p0, LX/AeY;->a:LX/AeZ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 1697835
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1697836
    check-cast p1, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveVideoTopLevelCommentsSubscriptionModel;

    .line 1697837
    if-nez p1, :cond_0

    .line 1697838
    :goto_0
    return-void

    .line 1697839
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveVideoTopLevelCommentsSubscriptionModel;->a()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoCommentCoreFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1697840
    iget-object v0, p0, LX/AeY;->a:LX/AeZ;

    invoke-virtual {p1}, Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$LiveVideoTopLevelCommentsSubscriptionModel;->a()Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoCommentCoreFragmentModel;

    move-result-object v1

    invoke-static {v0, v1}, LX/AeZ;->a$redex0(LX/AeZ;Lcom/facebook/facecastdisplay/protocol/FetchLiveVideoEventsQueryModels$FeedbackLiveVideoCommentCoreFragmentModel;)V

    goto :goto_0

    .line 1697841
    :cond_1
    goto :goto_0
.end method
