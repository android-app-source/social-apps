.class public final LX/C5E;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/C5G;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/C5F;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/C5G",
            "<TE;>.FollowUpHeaderMenuComponentImpl;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/C5G;

.field private c:[Ljava/lang/String;

.field private d:I

.field public e:Ljava/util/BitSet;


# direct methods
.method public constructor <init>(LX/C5G;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 1848548
    iput-object p1, p0, LX/C5E;->b:LX/C5G;

    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 1848549
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "topStoryProps"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "environment"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/C5E;->c:[Ljava/lang/String;

    .line 1848550
    iput v3, p0, LX/C5E;->d:I

    .line 1848551
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/C5E;->d:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/C5E;->e:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/C5E;LX/1De;IILX/C5F;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "II",
            "LX/C5G",
            "<TE;>.FollowUpHeaderMenuComponentImpl;)V"
        }
    .end annotation

    .prologue
    .line 1848552
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 1848553
    iput-object p4, p0, LX/C5E;->a:LX/C5F;

    .line 1848554
    iget-object v0, p0, LX/C5E;->e:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 1848555
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1848556
    invoke-super {p0}, LX/1X5;->a()V

    .line 1848557
    const/4 v0, 0x0

    iput-object v0, p0, LX/C5E;->a:LX/C5F;

    .line 1848558
    iget-object v0, p0, LX/C5E;->b:LX/C5G;

    iget-object v0, v0, LX/C5G;->b:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 1848559
    return-void
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/C5G;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1848560
    iget-object v1, p0, LX/C5E;->e:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/C5E;->e:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/C5E;->d:I

    if-ge v1, v2, :cond_2

    .line 1848561
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1848562
    :goto_0
    iget v2, p0, LX/C5E;->d:I

    if-ge v0, v2, :cond_1

    .line 1848563
    iget-object v2, p0, LX/C5E;->e:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1848564
    iget-object v2, p0, LX/C5E;->c:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1848565
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1848566
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1848567
    :cond_2
    iget-object v0, p0, LX/C5E;->a:LX/C5F;

    .line 1848568
    invoke-virtual {p0}, LX/C5E;->a()V

    .line 1848569
    return-object v0
.end method
