.class public LX/CCV;
.super LX/1Kn;
.source ""

# interfaces
.implements LX/34p;


# static fields
.field private static final a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

.field private static final b:Lcom/facebook/interstitial/manager/InterstitialTrigger;

.field public static f:LX/0hs;


# instance fields
.field private final c:LX/0iA;

.field private final d:Landroid/content/res/Resources;

.field private final e:LX/0tQ;

.field private g:Z

.field private h:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1857919
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->FEED_STORY_CARET_AUTOPLAY_VIDEO:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    sput-object v0, LX/CCV;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    .line 1857920
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->FEED_STORY_CARET_VIDEO:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    sput-object v0, LX/CCV;->b:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LX/0tQ;Landroid/content/res/Resources;LX/0iA;LX/0bH;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1857921
    invoke-direct {p0}, LX/1Kn;-><init>()V

    .line 1857922
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/CCV;->g:Z

    .line 1857923
    iput-object p1, p0, LX/CCV;->h:Ljava/lang/String;

    .line 1857924
    iput-object p4, p0, LX/CCV;->c:LX/0iA;

    .line 1857925
    iput-object p3, p0, LX/CCV;->d:Landroid/content/res/Resources;

    .line 1857926
    iput-object p2, p0, LX/CCV;->e:LX/0tQ;

    .line 1857927
    invoke-virtual {p5, p0}, LX/0b4;->a(LX/0b2;)Z

    .line 1857928
    return-void
.end method


# virtual methods
.method public final a(LX/0hs;)V
    .locals 1

    .prologue
    .line 1857929
    sput-object p1, LX/CCV;->f:LX/0hs;

    .line 1857930
    new-instance v0, LX/CCT;

    invoke-direct {v0, p0}, LX/CCT;-><init>(LX/CCV;)V

    .line 1857931
    iput-object v0, p1, LX/0ht;->H:LX/2dD;

    .line 1857932
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/FeedUnit;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1857933
    iget-object v1, p0, LX/CCV;->e:LX/0tQ;

    invoke-virtual {v1}, LX/0tQ;->H()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/CCV;->e:LX/0tQ;

    invoke-virtual {v1}, LX/0tQ;->J()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/CCV;->e:LX/0tQ;

    invoke-virtual {v1}, LX/0tQ;->f()Z

    move-result v1

    if-nez v1, :cond_0

    iget-boolean v1, p0, LX/CCV;->g:Z

    if-nez v1, :cond_1

    .line 1857934
    :cond_0
    :goto_0
    return v0

    .line 1857935
    :cond_1
    instance-of v1, p1, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v1, :cond_0

    .line 1857936
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1857937
    invoke-static {p1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    invoke-static {v1}, LX/14w;->f(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 1857938
    if-eqz v1, :cond_0

    .line 1857939
    invoke-static {v1}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    .line 1857940
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    invoke-static {v1}, LX/17E;->a(Lcom/facebook/graphql/model/GraphQLMedia;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1857941
    iget-object v1, p0, LX/CCV;->e:LX/0tQ;

    invoke-virtual {v1}, LX/0tQ;->e()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1857942
    iget-object v1, p0, LX/CCV;->c:LX/0iA;

    sget-object v2, LX/CCV;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    const-class v3, LX/CCW;

    invoke-virtual {v1, v2, v3}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v1

    .line 1857943
    :goto_1
    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    .line 1857944
    :cond_2
    iget-object v1, p0, LX/CCV;->c:LX/0iA;

    sget-object v2, LX/CCV;->b:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    const-class v3, LX/CCX;

    invoke-virtual {v1, v2, v3}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v1

    goto :goto_1
.end method

.method public final b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1857945
    iget-object v0, p0, LX/CCV;->d:Landroid/content/res/Resources;

    const v1, 0x7f080d48

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/0b7;)V
    .locals 1

    .prologue
    .line 1857946
    check-cast p1, LX/1Zh;

    .line 1857947
    iget-boolean v0, p1, LX/1Zh;->a:Z

    if-nez v0, :cond_0

    .line 1857948
    sget-object v0, LX/CCV;->f:LX/0hs;

    if-eqz v0, :cond_0

    .line 1857949
    sget-object v0, LX/CCV;->f:LX/0hs;

    invoke-virtual {v0}, LX/0ht;->l()V

    .line 1857950
    sget v0, LX/CCX;->a:I

    if-lez v0, :cond_0

    .line 1857951
    sget v0, LX/CCX;->a:I

    add-int/lit8 v0, v0, -0x1

    sput v0, LX/CCX;->a:I

    .line 1857952
    :cond_0
    iget-boolean v0, p1, LX/1Zh;->a:Z

    iput-boolean v0, p0, LX/CCV;->g:Z

    .line 1857953
    return-void
.end method

.method public final b(Lcom/facebook/graphql/model/FeedUnit;)V
    .locals 2

    .prologue
    .line 1857954
    iget-object v0, p0, LX/CCV;->c:LX/0iA;

    invoke-virtual {v0}, LX/0iA;->a()Lcom/facebook/interstitial/manager/InterstitialLogger;

    move-result-object v1

    iget-object v0, p0, LX/CCV;->e:LX/0tQ;

    invoke-virtual {v0}, LX/0tQ;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "4279"

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/interstitial/manager/InterstitialLogger;->a(Ljava/lang/String;)V

    .line 1857955
    iget-object v0, p0, LX/CCV;->e:LX/0tQ;

    invoke-virtual {v0}, LX/0tQ;->e()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1857956
    sget v0, LX/CCX;->a:I

    add-int/lit8 v0, v0, 0x1

    sput v0, LX/CCX;->a:I

    .line 1857957
    :goto_1
    return-void

    .line 1857958
    :cond_0
    const-string v0, "4324"

    goto :goto_0

    .line 1857959
    :cond_1
    const/4 v0, 0x1

    sput-boolean v0, LX/CCW;->a:Z

    .line 1857960
    goto :goto_1
.end method

.method public final c(Lcom/facebook/graphql/model/FeedUnit;)V
    .locals 0

    .prologue
    .line 1857961
    return-void
.end method

.method public final jy_()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1857962
    sget-object v0, LX/CCU;->a:[I

    iget-object v1, p0, LX/CCV;->e:LX/0tQ;

    invoke-virtual {v1}, LX/0tQ;->m()LX/2qY;

    move-result-object v1

    invoke-virtual {v1}, LX/2qY;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1857963
    iget-object v0, p0, LX/CCV;->d:Landroid/content/res/Resources;

    const v1, 0x7f080d45

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 1857964
    :pswitch_0
    iget-object v0, p0, LX/CCV;->d:Landroid/content/res/Resources;

    const v1, 0x7f080d47

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1857965
    :pswitch_1
    iget-object v0, p0, LX/CCV;->d:Landroid/content/res/Resources;

    const v1, 0x7f080d46

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
