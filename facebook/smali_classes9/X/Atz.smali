.class public LX/Atz;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1722205
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 1722206
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/Arh;Landroid/app/Activity;Landroid/view/View;Landroid/view/ViewStub;Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;LX/Aqz;LX/ArT;LX/ArS;)Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;
    .locals 21
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ModelData:",
            "Ljava/lang/Object;",
            ":",
            "Lcom/facebook/friendsharing/inspiration/model/CameraStateSpec$ProvidesCameraState;",
            ":",
            "LX/0io;",
            ":",
            "LX/0j0;",
            ":",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingDataSpec$ProvidesInspirationLoggingData;",
            ":",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationPreviewBoundsSpec$ProvidesInspirationPreviewBounds;",
            ":",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationStateSpec$ProvidesInspirationState;",
            ":",
            "LX/0is;",
            "DerivedData:",
            "Ljava/lang/Object;",
            "Mutation:",
            "Ljava/lang/Object;",
            ":",
            "Lcom/facebook/ipc/composer/dataaccessor/ComposerCanSave;",
            ":",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingDataSpec$SetsInspirationLoggingData",
            "<TMutation;>;:",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationStateSpec$SetsInspirationState",
            "<TMutation;>;:",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationSwipeableModelSpec$SetsInspirationSwipeableModel",
            "<TMutation;>;Services:",
            "Ljava/lang/Object;",
            ":",
            "LX/0il",
            "<TModelData;>;:",
            "LX/0ik",
            "<TDerivedData;>;:",
            "LX/0im",
            "<TMutation;>;>(TServices;",
            "Lcom/facebook/friendsharing/inspiration/capture/InspirationCameraEffectsApplier;",
            "Landroid/app/Activity;",
            "Landroid/view/View;",
            "Landroid/view/ViewStub;",
            "Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;",
            "Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;",
            "Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController$Delegate;",
            "LX/ArT;",
            "LX/ArS;",
            ")",
            "Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController",
            "<TModelData;TDerivedData;TMutation;TServices;>;"
        }
    .end annotation

    .prologue
    .line 1722207
    new-instance v1, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;

    move-object/from16 v2, p1

    check-cast v2, LX/0il;

    const-class v3, LX/AuO;

    move-object/from16 v0, p0

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v12

    check-cast v12, LX/AuO;

    const-class v3, LX/9d6;

    move-object/from16 v0, p0

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v13

    check-cast v13, LX/9d6;

    const/16 v3, 0x2e31

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v14

    const-class v3, LX/8GP;

    move-object/from16 v0, p0

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v15

    check-cast v15, LX/8GP;

    const-class v3, LX/AuX;

    move-object/from16 v0, p0

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v16

    check-cast v16, LX/AuX;

    const-class v3, LX/AsN;

    move-object/from16 v0, p0

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v17

    check-cast v17, LX/AsN;

    invoke-static/range {p0 .. p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v18

    check-cast v18, LX/0Sh;

    invoke-static/range {p0 .. p0}, LX/87V;->a(LX/0QB;)LX/87V;

    move-result-object v19

    check-cast v19, LX/87V;

    const-class v3, Landroid/content/Context;

    move-object/from16 v0, p0

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Landroid/content/Context;

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v1 .. v20}, Lcom/facebook/friendsharing/inspiration/controller/InspirationSwipeableLayoutController;-><init>(LX/0il;LX/Arh;Landroid/app/Activity;Landroid/view/View;Landroid/view/ViewStub;Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;LX/Aqz;LX/ArT;LX/ArS;LX/AuO;LX/9d6;LX/0Or;LX/8GP;LX/AuX;LX/AsN;LX/0Sh;LX/87V;Landroid/content/Context;)V

    .line 1722208
    return-object v1
.end method
