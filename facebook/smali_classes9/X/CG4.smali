.class public LX/CG4;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/CG3;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:LX/CFp;

.field private final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0YU;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0YU",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/CFp;)V
    .locals 3

    .prologue
    .line 1864847
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 1864848
    new-instance v0, LX/0YU;

    invoke-direct {v0}, LX/0YU;-><init>()V

    iput-object v0, p0, LX/CG4;->c:LX/0YU;

    .line 1864849
    iput-object p1, p0, LX/CG4;->a:LX/CFp;

    .line 1864850
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 1864851
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/CG4;->a:LX/CFp;

    .line 1864852
    iget-object p1, v0, LX/CFp;->a:Lcom/facebook/greetingcards/verve/model/VMDeck;

    move-object v0, p1

    .line 1864853
    iget-object v0, v0, Lcom/facebook/greetingcards/verve/model/VMDeck;->slides:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1864854
    iget-object v0, p0, LX/CG4;->a:LX/CFp;

    .line 1864855
    iget-object p1, v0, LX/CFp;->a:Lcom/facebook/greetingcards/verve/model/VMDeck;

    move-object v0, p1

    .line 1864856
    iget-object v0, v0, Lcom/facebook/greetingcards/verve/model/VMDeck;->slides:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/greetingcards/verve/model/VMSlide;

    .line 1864857
    iget-boolean v0, v0, Lcom/facebook/greetingcards/verve/model/VMSlide;->master:Z

    if-nez v0, :cond_0

    .line 1864858
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1864859
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1864860
    :cond_1
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    move-object v0, v0

    .line 1864861
    iput-object v0, p0, LX/CG4;->b:LX/0Px;

    .line 1864862
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 1864863
    new-instance v0, LX/CG3;

    new-instance v1, Landroid/widget/FrameLayout;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    invoke-direct {v0, p0, v1}, LX/CG3;-><init>(LX/CG4;Landroid/view/View;)V

    return-object v0
.end method

.method public final a(LX/1a1;I)V
    .locals 7

    .prologue
    .line 1864864
    check-cast p1, LX/CG3;

    .line 1864865
    iget-object v0, p0, LX/CG4;->b:LX/0Px;

    invoke-virtual {v0, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v1, p0, LX/CG4;->a:LX/CFp;

    .line 1864866
    iget-object v2, p1, LX/1a1;->a:Landroid/view/View;

    check-cast v2, Landroid/view/ViewGroup;

    .line 1864867
    invoke-static {v1, v2}, LX/CGN;->a(LX/CFp;Landroid/view/ViewGroup;)V

    .line 1864868
    iget-object v3, p1, LX/CG3;->l:LX/CG4;

    iget-object v3, v3, LX/CG4;->c:LX/0YU;

    invoke-virtual {v3}, LX/0YU;->b()V

    .line 1864869
    invoke-virtual {v1, v0}, LX/CFp;->b(I)LX/0Px;

    move-result-object v6

    .line 1864870
    invoke-virtual {v2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p1, LX/CG3;->l:LX/CG4;

    iget-object v4, v4, LX/CG4;->c:LX/0YU;

    invoke-static {v6, v0, v1, v3, v4}, LX/CGN;->a(LX/0Px;ILX/CFp;Landroid/content/Context;LX/0YU;)V

    .line 1864871
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result p0

    const/4 v3, 0x0

    move v5, v3

    :goto_0
    if-ge v5, p0, :cond_1

    invoke-virtual {v6, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/greetingcards/verve/model/VMView;

    .line 1864872
    iget-object v4, p1, LX/CG3;->l:LX/CG4;

    iget-object v4, v4, LX/CG4;->c:LX/0YU;

    iget p2, v3, Lcom/facebook/greetingcards/verve/model/VMView;->a:I

    invoke-virtual {v4, p2}, LX/0YU;->a(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/View;

    .line 1864873
    invoke-virtual {v2, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1864874
    invoke-virtual {v3}, Lcom/facebook/greetingcards/verve/model/VMView;->o()LX/CFo;

    move-result-object v3

    sget-object p2, LX/CFo;->GROUP:LX/CFo;

    if-ne v3, p2, :cond_0

    .line 1864875
    check-cast v4, LX/CGH;

    .line 1864876
    iget-object v3, v4, LX/CGH;->a:LX/CGE;

    move-object v3, v3

    .line 1864877
    invoke-virtual {v3}, LX/CGE;->a()V

    .line 1864878
    :cond_0
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_0

    .line 1864879
    :cond_1
    iget-object v2, v1, LX/CFp;->a:Lcom/facebook/greetingcards/verve/model/VMDeck;

    move-object v2, v2

    .line 1864880
    iget-object v2, v2, Lcom/facebook/greetingcards/verve/model/VMDeck;->slides:LX/0Px;

    invoke-virtual {v2, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/greetingcards/verve/model/VMSlide;

    .line 1864881
    invoke-virtual {v2}, Lcom/facebook/greetingcards/verve/model/VMSlide;->c()F

    move-result v2

    .line 1864882
    iget v3, v1, LX/CFp;->f:F

    move v3, v3

    .line 1864883
    mul-float/2addr v2, v3

    float-to-int v2, v2

    .line 1864884
    iget-object v3, p1, LX/1a1;->a:Landroid/view/View;

    new-instance v4, Landroid/view/ViewGroup$LayoutParams;

    const/4 v5, -0x1

    invoke-direct {v4, v5, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v3, v4}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1864885
    return-void
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 1864886
    iget-object v0, p0, LX/CG4;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method
