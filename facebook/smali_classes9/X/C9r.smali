.class public LX/C9r;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pq;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field public static final a:I
    .annotation build Landroid/support/annotation/DimenRes;
    .end annotation
.end field

.field private static g:LX/0Xm;


# instance fields
.field private final b:Landroid/os/Handler;

.field private final c:LX/9A1;

.field public final d:LX/C9p;

.field private final e:LX/16I;

.field public final f:LX/C99;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1854650
    const v0, 0x7f0b1cc0

    sput v0, LX/C9r;->a:I

    return-void
.end method

.method public constructor <init>(Landroid/os/Handler;LX/9A1;LX/C9p;LX/16I;LX/C99;)V
    .locals 0
    .param p1    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1854651
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1854652
    iput-object p1, p0, LX/C9r;->b:Landroid/os/Handler;

    .line 1854653
    iput-object p2, p0, LX/C9r;->c:LX/9A1;

    .line 1854654
    iput-object p3, p0, LX/C9r;->d:LX/C9p;

    .line 1854655
    iput-object p4, p0, LX/C9r;->e:LX/16I;

    .line 1854656
    iput-object p5, p0, LX/C9r;->f:LX/C99;

    .line 1854657
    return-void
.end method

.method public static a(LX/0QB;)LX/C9r;
    .locals 9

    .prologue
    .line 1854658
    const-class v1, LX/C9r;

    monitor-enter v1

    .line 1854659
    :try_start_0
    sget-object v0, LX/C9r;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1854660
    sput-object v2, LX/C9r;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1854661
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1854662
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1854663
    new-instance v3, LX/C9r;

    invoke-static {v0}, LX/0Ss;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v4

    check-cast v4, Landroid/os/Handler;

    invoke-static {v0}, LX/9A1;->a(LX/0QB;)LX/9A1;

    move-result-object v5

    check-cast v5, LX/9A1;

    invoke-static {v0}, LX/C9p;->a(LX/0QB;)LX/C9p;

    move-result-object v6

    check-cast v6, LX/C9p;

    invoke-static {v0}, LX/16I;->a(LX/0QB;)LX/16I;

    move-result-object v7

    check-cast v7, LX/16I;

    invoke-static {v0}, LX/C99;->a(LX/0QB;)LX/C99;

    move-result-object v8

    check-cast v8, LX/C99;

    invoke-direct/range {v3 .. v8}, LX/C9r;-><init>(Landroid/os/Handler;LX/9A1;LX/C9p;LX/16I;LX/C99;)V

    .line 1854664
    move-object v0, v3

    .line 1854665
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1854666
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C9r;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1854667
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1854668
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a$redex0(LX/C9r;)Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 1854669
    iget-object v0, p0, LX/C9r;->e:LX/16I;

    invoke-virtual {v0}, LX/16I;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public static b(LX/1De;)LX/1X1;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1X1",
            "<",
            "LX/1o2;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1854670
    invoke-static {p0}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v0

    const v1, 0x7f02116b

    invoke-virtual {v0, v1}, LX/1o5;->h(I)LX/1o5;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)V
    .locals 5
    .param p2    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 1854671
    iget-object v1, p0, LX/C9r;->c:LX/9A1;

    .line 1854672
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1854673
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    sget-object v2, LX/5Ro;->OFFLINE_POSTING_HEADER:LX/5Ro;

    invoke-virtual {v1, v0, v2}, LX/9A1;->b(Lcom/facebook/graphql/model/GraphQLStory;LX/5Ro;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1854674
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {p1, v0}, LX/C9m;->a(LX/1De;Ljava/lang/Integer;)V

    .line 1854675
    iget-object v0, p0, LX/C9r;->b:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/feedplugins/offline/rows/OfflineRetryComponentSpec$1;

    invoke-direct {v1, p0, p1}, Lcom/facebook/feedplugins/offline/rows/OfflineRetryComponentSpec$1;-><init>(LX/C9r;LX/1De;)V

    const-wide/16 v2, 0xbb8

    const v4, 0x33789c73

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 1854676
    :goto_0
    return-void

    .line 1854677
    :cond_0
    check-cast p3, LX/1Pq;

    new-array v0, v3, [Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x0

    aput-object p2, v0, v1

    invoke-interface {p3, v0}, LX/1Pq;->a([Lcom/facebook/feed/rows/core/props/FeedProps;)V

    goto :goto_0
.end method
