.class public final LX/AoR;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 14

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1714213
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_c

    .line 1714214
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1714215
    :goto_0
    return v1

    .line 1714216
    :cond_0
    const-string v12, "is_viewer_coworker"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 1714217
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v3

    move v8, v3

    move v3, v2

    .line 1714218
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_9

    .line 1714219
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 1714220
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1714221
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_1

    if-eqz v11, :cond_1

    .line 1714222
    const-string v12, "__type__"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_2

    const-string v12, "__typename"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 1714223
    :cond_2
    invoke-static {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v10

    invoke-virtual {p1, v10}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v10

    goto :goto_1

    .line 1714224
    :cond_3
    const-string v12, "id"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 1714225
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_1

    .line 1714226
    :cond_4
    const-string v12, "is_work_user"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 1714227
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v7, v0

    move v0, v2

    goto :goto_1

    .line 1714228
    :cond_5
    const-string v12, "name"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 1714229
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_1

    .line 1714230
    :cond_6
    const-string v12, "profile_picture"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 1714231
    invoke-static {p0, p1}, LX/5SY;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 1714232
    :cond_7
    const-string v12, "url"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_8

    .line 1714233
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 1714234
    :cond_8
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 1714235
    :cond_9
    const/4 v11, 0x7

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 1714236
    invoke-virtual {p1, v1, v10}, LX/186;->b(II)V

    .line 1714237
    invoke-virtual {p1, v2, v9}, LX/186;->b(II)V

    .line 1714238
    if-eqz v3, :cond_a

    .line 1714239
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v8}, LX/186;->a(IZ)V

    .line 1714240
    :cond_a
    if-eqz v0, :cond_b

    .line 1714241
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v7}, LX/186;->a(IZ)V

    .line 1714242
    :cond_b
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1714243
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1714244
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1714245
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_c
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    move v9, v1

    move v10, v1

    goto/16 :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1714182
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1714183
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 1714184
    if-eqz v0, :cond_0

    .line 1714185
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1714186
    invoke-static {p0, p1, v1, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1714187
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1714188
    if-eqz v0, :cond_1

    .line 1714189
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1714190
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1714191
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1714192
    if-eqz v0, :cond_2

    .line 1714193
    const-string v1, "is_viewer_coworker"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1714194
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1714195
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1714196
    if-eqz v0, :cond_3

    .line 1714197
    const-string v1, "is_work_user"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1714198
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1714199
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1714200
    if-eqz v0, :cond_4

    .line 1714201
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1714202
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1714203
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1714204
    if-eqz v0, :cond_5

    .line 1714205
    const-string v1, "profile_picture"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1714206
    invoke-static {p0, v0, p2}, LX/5SY;->a(LX/15i;ILX/0nX;)V

    .line 1714207
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1714208
    if-eqz v0, :cond_6

    .line 1714209
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1714210
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1714211
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1714212
    return-void
.end method
