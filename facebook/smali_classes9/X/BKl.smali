.class public final LX/BKl;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public b:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

.field public c:Lcom/facebook/platform/composer/model/PlatformComposition;

.field public d:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

.field public e:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

.field public f:Lcom/facebook/ipc/composer/model/ComposerLocation;

.field public g:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

.field public h:Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;

.field public i:Lcom/facebook/ipc/composer/intent/ComposerPageData;

.field public j:I

.field public k:I

.field public l:Z

.field public m:Z

.field public n:Z

.field public o:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1774178
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1774179
    return-void
.end method

.method public constructor <init>(LX/BKm;)V
    .locals 1

    .prologue
    .line 1774180
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1774181
    iget-object v0, p1, LX/BKm;->a:Ljava/lang/String;

    iput-object v0, p0, LX/BKl;->a:Ljava/lang/String;

    .line 1774182
    iget-object v0, p1, LX/BKm;->b:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    iput-object v0, p0, LX/BKl;->b:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    .line 1774183
    iget-object v0, p1, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    iput-object v0, p0, LX/BKl;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    .line 1774184
    iget-object v0, p1, LX/BKm;->d:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    iput-object v0, p0, LX/BKl;->d:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    .line 1774185
    iget-object v0, p1, LX/BKm;->e:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    iput-object v0, p0, LX/BKl;->e:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    .line 1774186
    iget-object v0, p1, LX/BKm;->f:Lcom/facebook/ipc/composer/model/ComposerLocation;

    iput-object v0, p0, LX/BKl;->f:Lcom/facebook/ipc/composer/model/ComposerLocation;

    .line 1774187
    iget-object v0, p1, LX/BKm;->g:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    iput-object v0, p0, LX/BKl;->g:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 1774188
    iget-object v0, p1, LX/BKm;->h:Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;

    iput-object v0, p0, LX/BKl;->h:Lcom/facebook/composer/audienceeducator/ComposerAudienceEducatorData;

    .line 1774189
    iget-object v0, p1, LX/BKm;->i:Lcom/facebook/ipc/composer/intent/ComposerPageData;

    iput-object v0, p0, LX/BKl;->i:Lcom/facebook/ipc/composer/intent/ComposerPageData;

    .line 1774190
    iget v0, p1, LX/BKm;->j:I

    iput v0, p0, LX/BKl;->j:I

    .line 1774191
    iget v0, p1, LX/BKm;->l:I

    iput v0, p0, LX/BKl;->k:I

    .line 1774192
    iget-boolean v0, p1, LX/BKm;->k:Z

    iput-boolean v0, p0, LX/BKl;->l:Z

    .line 1774193
    iget-boolean v0, p1, LX/BKm;->m:Z

    iput-boolean v0, p0, LX/BKl;->m:Z

    .line 1774194
    iget-boolean v0, p1, LX/BKm;->n:Z

    iput-boolean v0, p0, LX/BKl;->n:Z

    .line 1774195
    iget-object v0, p1, LX/BKm;->o:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    iput-object v0, p0, LX/BKl;->o:Lcom/facebook/ipc/composer/model/ComposerRichTextStyle;

    .line 1774196
    return-void
.end method

.method private static a(Lcom/facebook/ipc/composer/intent/ComposerTargetData;Lcom/facebook/platform/composer/model/PlatformComposition;)Lcom/facebook/ipc/composer/intent/ComposerTargetData;
    .locals 4
    .param p0    # Lcom/facebook/ipc/composer/intent/ComposerTargetData;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1774197
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1774198
    if-eqz p0, :cond_0

    iget-wide v0, p0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    iget-wide v0, p0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    invoke-virtual {p1}, Lcom/facebook/platform/composer/model/PlatformComposition;->b()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    sget-object v1, LX/2rw;->UNDIRECTED:LX/2rw;

    if-eq v0, v1, :cond_1

    .line 1774199
    :cond_0
    new-instance v0, LX/89I;

    invoke-virtual {p1}, Lcom/facebook/platform/composer/model/PlatformComposition;->b()J

    move-result-wide v2

    sget-object v1, LX/2rw;->UNDIRECTED:LX/2rw;

    invoke-direct {v0, v2, v3, v1}, LX/89I;-><init>(JLX/2rw;)V

    invoke-virtual {v0}, LX/89I;->a()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object p0

    .line 1774200
    :cond_1
    return-object p0
.end method


# virtual methods
.method public final a(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)LX/BKl;
    .locals 1

    .prologue
    .line 1774201
    iget-object v0, p0, LX/BKl;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-static {p1, v0}, LX/BKl;->a(Lcom/facebook/ipc/composer/intent/ComposerTargetData;Lcom/facebook/platform/composer/model/PlatformComposition;)Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v0

    iput-object v0, p0, LX/BKl;->e:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    .line 1774202
    return-object p0
.end method

.method public final a()LX/BKm;
    .locals 2

    .prologue
    .line 1774203
    new-instance v0, LX/BKm;

    invoke-direct {v0, p0}, LX/BKm;-><init>(LX/BKl;)V

    return-object v0
.end method
