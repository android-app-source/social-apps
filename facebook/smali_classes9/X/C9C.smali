.class public LX/C9C;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/C9A;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/C9D;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1853837
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/C9C;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/C9D;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1853838
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1853839
    iput-object p1, p0, LX/C9C;->b:LX/0Ot;

    .line 1853840
    return-void
.end method

.method public static a(LX/0QB;)LX/C9C;
    .locals 4

    .prologue
    .line 1853826
    const-class v1, LX/C9C;

    monitor-enter v1

    .line 1853827
    :try_start_0
    sget-object v0, LX/C9C;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1853828
    sput-object v2, LX/C9C;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1853829
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1853830
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1853831
    new-instance v3, LX/C9C;

    const/16 p0, 0x2060

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/C9C;-><init>(LX/0Ot;)V

    .line 1853832
    move-object v0, v3

    .line 1853833
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1853834
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/C9C;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1853835
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1853836
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 3

    .prologue
    .line 1853817
    iget-object v0, p0, LX/C9C;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    const/4 v1, 0x2

    .line 1853818
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, v1}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, v1}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v0

    const/4 v1, 0x3

    invoke-interface {v0, v1}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v0

    const/4 v1, 0x7

    const v2, 0x7f0b1cc1

    invoke-interface {v0, v1, v2}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v0

    const v1, 0x7f020ac9

    invoke-interface {v0, v1}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v0

    const/16 p2, 0x18

    .line 1853819
    invoke-static {p1}, LX/5Jt;->c(LX/1De;)LX/5Jr;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    const/4 v2, 0x6

    sget p0, LX/C9D;->a:I

    invoke-interface {v1, v2, p0}, LX/1Di;->c(II)LX/1Di;

    move-result-object v1

    invoke-interface {v1, p2}, LX/1Di;->r(I)LX/1Di;

    move-result-object v1

    invoke-interface {v1, p2}, LX/1Di;->j(I)LX/1Di;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    move-object v1, v1

    .line 1853820
    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v0

    .line 1853821
    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v1

    const v2, 0x7f02116b

    invoke-virtual {v1, v2}, LX/1o5;->h(I)LX/1o5;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->d()LX/1X1;

    move-result-object v1

    move-object v1, v1

    .line 1853822
    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1X1;)LX/1Dh;

    move-result-object v0

    .line 1853823
    const/4 v1, 0x0

    const v2, 0x7f0e0122

    invoke-static {p1, v1, v2}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v1

    const v2, 0x7f081a3a

    invoke-virtual {v1, v2}, LX/1ne;->h(I)LX/1ne;

    move-result-object v1

    const v2, 0x7f0a05d2

    invoke-virtual {v1, v2}, LX/1ne;->n(I)LX/1ne;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-interface {v1, v2}, LX/1Di;->a(F)LX/1Di;

    move-result-object v1

    const/4 v2, 0x6

    sget p0, LX/C9D;->a:I

    invoke-interface {v1, v2, p0}, LX/1Di;->g(II)LX/1Di;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    move-object v1, v1

    .line 1853824
    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    move-object v0, v0

    .line 1853825
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1853815
    invoke-static {}, LX/1dS;->b()V

    .line 1853816
    const/4 v0, 0x0

    return-object v0
.end method
