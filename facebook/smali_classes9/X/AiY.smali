.class public LX/AiY;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1706455
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FBFeedAwesomizerProfileListCardProfileFragmentModel;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FBFeedAwesomizerProfileListCardProfileFragmentModel;
    .locals 1

    .prologue
    .line 1706456
    new-instance v0, LX/Ahh;

    invoke-direct {v0}, LX/Ahh;-><init>()V

    invoke-static {p0}, LX/Ahh;->a(Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FBFeedAwesomizerProfileListCardProfileFragmentModel;)LX/Ahh;

    move-result-object v0

    .line 1706457
    iput-object p1, v0, LX/Ahh;->h:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 1706458
    move-object v0, v0

    .line 1706459
    invoke-virtual {v0}, LX/Ahh;->a()Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FBFeedAwesomizerProfileListCardProfileFragmentModel;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerFollowingQueryModel$FollowedProfilesModel;Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerFollowingQueryModel$FollowedProfilesModel$EdgesModel;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerFollowingQueryModel$FollowedProfilesModel;
    .locals 13

    .prologue
    .line 1706460
    new-instance v0, LX/Ahw;

    invoke-direct {v0}, LX/Ahw;-><init>()V

    .line 1706461
    invoke-virtual {p1}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerFollowingQueryModel$FollowedProfilesModel$EdgesModel;->a()Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FBFeedAwesomizerProfileListCardProfileFragmentModel;

    move-result-object v1

    iput-object v1, v0, LX/Ahw;->a:Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FBFeedAwesomizerProfileListCardProfileFragmentModel;

    .line 1706462
    invoke-virtual {p1}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerFollowingQueryModel$FollowedProfilesModel$EdgesModel;->j()I

    move-result v1

    iput v1, v0, LX/Ahw;->b:I

    .line 1706463
    move-object v0, v0

    .line 1706464
    invoke-virtual {p1}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerFollowingQueryModel$FollowedProfilesModel$EdgesModel;->a()Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FBFeedAwesomizerProfileListCardProfileFragmentModel;

    move-result-object v1

    invoke-static {v1, p2}, LX/AiY;->a(Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FBFeedAwesomizerProfileListCardProfileFragmentModel;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FBFeedAwesomizerProfileListCardProfileFragmentModel;

    move-result-object v1

    .line 1706465
    iput-object v1, v0, LX/Ahw;->a:Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FBFeedAwesomizerProfileListCardProfileFragmentModel;

    .line 1706466
    move-object v0, v0

    .line 1706467
    const/4 v11, 0x1

    const/4 v9, 0x0

    const/4 v12, 0x0

    .line 1706468
    new-instance v7, LX/186;

    const/16 v8, 0x80

    invoke-direct {v7, v8}, LX/186;-><init>(I)V

    .line 1706469
    iget-object v8, v0, LX/Ahw;->a:Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FBFeedAwesomizerProfileListCardProfileFragmentModel;

    invoke-static {v7, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 1706470
    const/4 v10, 0x2

    invoke-virtual {v7, v10}, LX/186;->c(I)V

    .line 1706471
    invoke-virtual {v7, v12, v8}, LX/186;->b(II)V

    .line 1706472
    iget v8, v0, LX/Ahw;->b:I

    invoke-virtual {v7, v11, v8, v12}, LX/186;->a(III)V

    .line 1706473
    invoke-virtual {v7}, LX/186;->d()I

    move-result v8

    .line 1706474
    invoke-virtual {v7, v8}, LX/186;->d(I)V

    .line 1706475
    invoke-virtual {v7}, LX/186;->e()[B

    move-result-object v7

    invoke-static {v7}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v8

    .line 1706476
    invoke-virtual {v8, v12}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1706477
    new-instance v7, LX/15i;

    move-object v10, v9

    move-object v12, v9

    invoke-direct/range {v7 .. v12}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1706478
    new-instance v8, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerFollowingQueryModel$FollowedProfilesModel$EdgesModel;

    invoke-direct {v8, v7}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerFollowingQueryModel$FollowedProfilesModel$EdgesModel;-><init>(LX/15i;)V

    .line 1706479
    move-object v2, v8

    .line 1706480
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1706481
    invoke-virtual {p0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerFollowingQueryModel$FollowedProfilesModel;->j()LX/0Px;

    move-result-object v4

    .line 1706482
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v5, :cond_1

    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerFollowingQueryModel$FollowedProfilesModel$EdgesModel;

    .line 1706483
    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1706484
    invoke-virtual {v3, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1706485
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1706486
    :cond_0
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 1706487
    :cond_1
    invoke-static {p0}, LX/Ahv;->a(Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerFollowingQueryModel$FollowedProfilesModel;)LX/Ahv;

    move-result-object v0

    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 1706488
    iput-object v1, v0, LX/Ahv;->b:LX/0Px;

    .line 1706489
    move-object v0, v0

    .line 1706490
    invoke-virtual {v0}, LX/Ahv;->a()Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerFollowingQueryModel$FollowedProfilesModel;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerUnfollowingQueryModel$UnfollowedProfilesModel;Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerUnfollowingQueryModel$UnfollowedProfilesModel$EdgesModel;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerUnfollowingQueryModel$UnfollowedProfilesModel;
    .locals 11

    .prologue
    .line 1706491
    new-instance v7, LX/Ai3;

    invoke-direct {v7}, LX/Ai3;-><init>()V

    .line 1706492
    invoke-virtual {p1}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerUnfollowingQueryModel$UnfollowedProfilesModel$EdgesModel;->a()Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FBFeedAwesomizerProfileListCardProfileFragmentModel;

    move-result-object v8

    iput-object v8, v7, LX/Ai3;->a:Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FBFeedAwesomizerProfileListCardProfileFragmentModel;

    .line 1706493
    invoke-virtual {p1}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerUnfollowingQueryModel$UnfollowedProfilesModel$EdgesModel;->j()J

    move-result-wide v9

    iput-wide v9, v7, LX/Ai3;->b:J

    .line 1706494
    move-object v0, v7

    .line 1706495
    invoke-virtual {p1}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerUnfollowingQueryModel$UnfollowedProfilesModel$EdgesModel;->a()Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FBFeedAwesomizerProfileListCardProfileFragmentModel;

    move-result-object v1

    invoke-static {v1, p2}, LX/AiY;->a(Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FBFeedAwesomizerProfileListCardProfileFragmentModel;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FBFeedAwesomizerProfileListCardProfileFragmentModel;

    move-result-object v1

    .line 1706496
    iput-object v1, v0, LX/Ai3;->a:Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FBFeedAwesomizerProfileListCardProfileFragmentModel;

    .line 1706497
    move-object v0, v0

    .line 1706498
    invoke-virtual {v0}, LX/Ai3;->a()Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerUnfollowingQueryModel$UnfollowedProfilesModel$EdgesModel;

    move-result-object v2

    .line 1706499
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1706500
    invoke-virtual {p0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerUnfollowingQueryModel$UnfollowedProfilesModel;->j()LX/0Px;

    move-result-object v4

    .line 1706501
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v5, :cond_1

    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerUnfollowingQueryModel$UnfollowedProfilesModel$EdgesModel;

    .line 1706502
    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1706503
    invoke-virtual {v3, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1706504
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1706505
    :cond_0
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 1706506
    :cond_1
    invoke-static {p0}, LX/Ai2;->a(Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerUnfollowingQueryModel$UnfollowedProfilesModel;)LX/Ai2;

    move-result-object v0

    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 1706507
    iput-object v1, v0, LX/Ai2;->b:LX/0Px;

    .line 1706508
    move-object v0, v0

    .line 1706509
    invoke-virtual {v0}, LX/Ai2;->a()Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerUnfollowingQueryModel$UnfollowedProfilesModel;

    move-result-object v0

    return-object v0
.end method
