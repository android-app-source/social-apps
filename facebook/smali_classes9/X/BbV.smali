.class public final LX/BbV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLStory;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Lcom/facebook/feed/rows/core/props/FeedProps;

.field public final synthetic d:Lcom/facebook/checkin/socialsearch/feed/SocialSearchImplicitPlaceListAttachmentPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/checkin/socialsearch/feed/SocialSearchImplicitPlaceListAttachmentPartDefinition;Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 0

    .prologue
    .line 1800838
    iput-object p1, p0, LX/BbV;->d:Lcom/facebook/checkin/socialsearch/feed/SocialSearchImplicitPlaceListAttachmentPartDefinition;

    iput-object p2, p0, LX/BbV;->a:Lcom/facebook/graphql/model/GraphQLStory;

    iput-object p3, p0, LX/BbV;->b:Ljava/lang/String;

    iput-object p4, p0, LX/BbV;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x121d4a93

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1800839
    iget-object v0, p0, LX/BbV;->a:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/BbV;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 1800840
    :cond_0
    iget-object v0, p0, LX/BbV;->d:Lcom/facebook/checkin/socialsearch/feed/SocialSearchImplicitPlaceListAttachmentPartDefinition;

    iget-object v0, v0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchImplicitPlaceListAttachmentPartDefinition;->k:LX/0kL;

    new-instance v2, LX/27k;

    const v3, 0x7f0829d4

    invoke-direct {v2, v3}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v2}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 1800841
    const v0, -0x3cbaa10e

    invoke-static {v4, v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1800842
    :goto_0
    return-void

    .line 1800843
    :cond_1
    iget-object v0, p0, LX/BbV;->d:Lcom/facebook/checkin/socialsearch/feed/SocialSearchImplicitPlaceListAttachmentPartDefinition;

    iget-object v0, v0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchImplicitPlaceListAttachmentPartDefinition;->f:LX/8xw;

    iget-object v2, p0, LX/BbV;->b:Ljava/lang/String;

    new-instance v3, LX/BbU;

    invoke-direct {v3, p0}, LX/BbU;-><init>(LX/BbV;)V

    .line 1800844
    const-string v4, "implicit_map"

    const/4 p1, 0x0

    invoke-static {v0, v2, v4, p1, v3}, LX/8xw;->a(LX/8xw;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0TF;)V

    .line 1800845
    iget-object v0, p0, LX/BbV;->d:Lcom/facebook/checkin/socialsearch/feed/SocialSearchImplicitPlaceListAttachmentPartDefinition;

    iget-object v2, v0, Lcom/facebook/checkin/socialsearch/feed/SocialSearchImplicitPlaceListAttachmentPartDefinition;->h:LX/189;

    iget-object v3, p0, LX/BbV;->a:Lcom/facebook/graphql/model/GraphQLStory;

    iget-object v0, p0, LX/BbV;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1800846
    iget-object v4, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v4

    .line 1800847
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1800848
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v4

    if-nez v4, :cond_2

    .line 1800849
    :goto_1
    move-object v4, v0

    .line 1800850
    invoke-static {v4}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/189;->a(Lcom/facebook/graphql/model/GraphQLStory;LX/0Px;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v4

    move-object v0, v4

    .line 1800851
    iget-object v2, p0, LX/BbV;->d:Lcom/facebook/checkin/socialsearch/feed/SocialSearchImplicitPlaceListAttachmentPartDefinition;

    iget-object v2, v2, Lcom/facebook/checkin/socialsearch/feed/SocialSearchImplicitPlaceListAttachmentPartDefinition;->i:LX/0bH;

    new-instance v3, LX/1Ne;

    invoke-direct {v3, v0}, LX/1Ne;-><init>(Lcom/facebook/graphql/model/FeedUnit;)V

    invoke-virtual {v2, v3}, LX/0b4;->a(LX/0b7;)V

    .line 1800852
    const v0, 0x114966da

    invoke-static {v0, v1}, LX/02F;->a(II)V

    goto :goto_0

    :cond_2
    invoke-static {v0}, LX/39x;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/39x;

    move-result-object v4

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->PLACE_LIST:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    sget-object p1, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->FALLBACK:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-static {v5, p1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v5

    .line 1800853
    iput-object v5, v4, LX/39x;->p:LX/0Px;

    .line 1800854
    move-object v4, v4

    .line 1800855
    invoke-virtual {v4}, LX/39x;->a()Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    goto :goto_1
.end method
