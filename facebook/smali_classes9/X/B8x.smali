.class public final LX/B8x;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/leadgen/view/LeadGenActionButtonsView;


# direct methods
.method public constructor <init>(Lcom/facebook/leadgen/view/LeadGenActionButtonsView;)V
    .locals 0

    .prologue
    .line 1750784
    iput-object p1, p0, LX/B8x;->a:Lcom/facebook/leadgen/view/LeadGenActionButtonsView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v0, 0x2

    const v1, 0x65a90bb1

    invoke-static {v0, v4, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1750785
    iget-object v1, p0, LX/B8x;->a:Lcom/facebook/leadgen/view/LeadGenActionButtonsView;

    iget-object v1, v1, Lcom/facebook/leadgen/view/LeadGenActionButtonsView;->e:LX/B6u;

    invoke-virtual {v1}, LX/B6u;->k()LX/B77;

    move-result-object v1

    .line 1750786
    iget-object v2, p0, LX/B8x;->a:Lcom/facebook/leadgen/view/LeadGenActionButtonsView;

    iget-object v2, v2, Lcom/facebook/leadgen/view/LeadGenActionButtonsView;->e:LX/B6u;

    .line 1750787
    iget v3, v2, LX/B6u;->h:I

    move v2, v3

    .line 1750788
    invoke-virtual {v1}, LX/B77;->isValid()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1750789
    iget-object v1, p0, LX/B8x;->a:Lcom/facebook/leadgen/view/LeadGenActionButtonsView;

    iget-object v1, v1, Lcom/facebook/leadgen/view/LeadGenActionButtonsView;->e:LX/B6u;

    invoke-virtual {v1}, LX/B6u;->d()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1750790
    iget-object v1, p0, LX/B8x;->a:Lcom/facebook/leadgen/view/LeadGenActionButtonsView;

    invoke-virtual {v1}, Lcom/facebook/leadgen/view/LeadGenActionButtonsView;->a()V

    .line 1750791
    iget-object v1, p0, LX/B8x;->a:Lcom/facebook/leadgen/view/LeadGenActionButtonsView;

    iget-object v1, v1, Lcom/facebook/leadgen/view/LeadGenActionButtonsView;->b:LX/B7W;

    new-instance v3, LX/B7e;

    invoke-direct {v3, v4}, LX/B7e;-><init>(Z)V

    invoke-virtual {v1, v3}, LX/0b4;->a(LX/0b7;)V

    .line 1750792
    iget-object v1, p0, LX/B8x;->a:Lcom/facebook/leadgen/view/LeadGenActionButtonsView;

    iget-object v1, v1, Lcom/facebook/leadgen/view/LeadGenActionButtonsView;->c:LX/0if;

    sget-object v3, LX/0ig;->v:LX/0ih;

    const-string v4, "click_submit_button"

    invoke-virtual {v1, v3, v4}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1750793
    iget-object v1, p0, LX/B8x;->a:Lcom/facebook/leadgen/view/LeadGenActionButtonsView;

    iget-object v1, v1, Lcom/facebook/leadgen/view/LeadGenActionButtonsView;->g:LX/B6S;

    iget-object v3, p0, LX/B8x;->a:Lcom/facebook/leadgen/view/LeadGenActionButtonsView;

    iget-object v3, v3, Lcom/facebook/leadgen/view/LeadGenActionButtonsView;->f:LX/B7F;

    iget-object v4, p0, LX/B8x;->a:Lcom/facebook/leadgen/view/LeadGenActionButtonsView;

    iget-object v4, v4, Lcom/facebook/leadgen/view/LeadGenActionButtonsView;->e:LX/B6u;

    invoke-virtual {v4}, LX/B6u;->e()LX/0P1;

    move-result-object v4

    iget-object v5, p0, LX/B8x;->a:Lcom/facebook/leadgen/view/LeadGenActionButtonsView;

    iget-object v5, v5, Lcom/facebook/leadgen/view/LeadGenActionButtonsView;->e:LX/B6u;

    invoke-virtual {v5}, LX/B6u;->g()LX/0P1;

    move-result-object v5

    invoke-virtual {v1, v3, v4, v5, v2}, LX/B6S;->a(LX/B7F;LX/0P1;LX/0P1;I)V

    .line 1750794
    :goto_0
    iget-object v1, p0, LX/B8x;->a:Lcom/facebook/leadgen/view/LeadGenActionButtonsView;

    iget-object v1, v1, Lcom/facebook/leadgen/view/LeadGenActionButtonsView;->h:LX/D82;

    if-eqz v1, :cond_0

    .line 1750795
    iget-object v1, p0, LX/B8x;->a:Lcom/facebook/leadgen/view/LeadGenActionButtonsView;

    iget-object v1, v1, Lcom/facebook/leadgen/view/LeadGenActionButtonsView;->h:LX/D82;

    invoke-virtual {v1}, LX/D82;->a()V

    .line 1750796
    :cond_0
    :goto_1
    const v1, -0x39f2ba78

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 1750797
    :cond_1
    iget-object v1, p0, LX/B8x;->a:Lcom/facebook/leadgen/view/LeadGenActionButtonsView;

    iget-object v1, v1, Lcom/facebook/leadgen/view/LeadGenActionButtonsView;->e:LX/B6u;

    invoke-virtual {v1}, LX/B6u;->a()Z

    .line 1750798
    iget-object v1, p0, LX/B8x;->a:Lcom/facebook/leadgen/view/LeadGenActionButtonsView;

    iget-object v1, v1, Lcom/facebook/leadgen/view/LeadGenActionButtonsView;->c:LX/0if;

    sget-object v3, LX/0ig;->v:LX/0ih;

    const-string v4, "cta_lead_gen_next_button_click"

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v3, v4, v2}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1750799
    :cond_2
    iget-object v1, p0, LX/B8x;->a:Lcom/facebook/leadgen/view/LeadGenActionButtonsView;

    iget-object v1, v1, Lcom/facebook/leadgen/view/LeadGenActionButtonsView;->h:LX/D82;

    if-eqz v1, :cond_0

    .line 1750800
    iget-object v1, p0, LX/B8x;->a:Lcom/facebook/leadgen/view/LeadGenActionButtonsView;

    iget-object v1, v1, Lcom/facebook/leadgen/view/LeadGenActionButtonsView;->h:LX/D82;

    invoke-virtual {v1}, LX/D82;->b()V

    goto :goto_1
.end method
