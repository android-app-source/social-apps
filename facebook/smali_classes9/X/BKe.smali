.class public LX/BKe;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/BKe;


# instance fields
.field public final a:LX/11i;

.field public final b:LX/4i1;

.field public final c:LX/BKd;


# direct methods
.method public constructor <init>(LX/11i;LX/4i1;LX/BKd;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1774071
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1774072
    iput-object p1, p0, LX/BKe;->a:LX/11i;

    .line 1774073
    iput-object p2, p0, LX/BKe;->b:LX/4i1;

    .line 1774074
    iput-object p3, p0, LX/BKe;->c:LX/BKd;

    .line 1774075
    return-void
.end method

.method public static a(LX/0QB;)LX/BKe;
    .locals 6

    .prologue
    .line 1774076
    sget-object v0, LX/BKe;->d:LX/BKe;

    if-nez v0, :cond_1

    .line 1774077
    const-class v1, LX/BKe;

    monitor-enter v1

    .line 1774078
    :try_start_0
    sget-object v0, LX/BKe;->d:LX/BKe;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1774079
    if-eqz v2, :cond_0

    .line 1774080
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1774081
    new-instance p0, LX/BKe;

    invoke-static {v0}, LX/11h;->a(LX/0QB;)LX/11h;

    move-result-object v3

    check-cast v3, LX/11i;

    invoke-static {v0}, LX/4i1;->a(LX/0QB;)LX/4i1;

    move-result-object v4

    check-cast v4, LX/4i1;

    invoke-static {v0}, LX/BKd;->a(LX/0QB;)LX/BKd;

    move-result-object v5

    check-cast v5, LX/BKd;

    invoke-direct {p0, v3, v4, v5}, LX/BKe;-><init>(LX/11i;LX/4i1;LX/BKd;)V

    .line 1774082
    move-object v0, p0

    .line 1774083
    sput-object v0, LX/BKe;->d:LX/BKe;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1774084
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1774085
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1774086
    :cond_1
    sget-object v0, LX/BKe;->d:LX/BKe;

    return-object v0

    .line 1774087
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1774088
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static e(LX/BKe;)V
    .locals 3

    .prologue
    .line 1774089
    iget-object v0, p0, LX/BKe;->b:LX/4i1;

    iget-object v1, p0, LX/BKe;->c:LX/BKd;

    invoke-virtual {v0, v1}, LX/4i1;->a(LX/4i0;)V

    .line 1774090
    iget-object v0, p0, LX/BKe;->a:LX/11i;

    iget-object v1, p0, LX/BKe;->c:LX/BKd;

    invoke-interface {v0, v1}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v0

    .line 1774091
    if-eqz v0, :cond_0

    .line 1774092
    const-string v1, "PlatformShareDialogSetup"

    const v2, -0x24e8eba4

    invoke-static {v0, v1, v2}, LX/096;->a(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 1774093
    :cond_0
    return-void
.end method
