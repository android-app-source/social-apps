.class public LX/AsB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0iK;
.implements LX/AsA;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "LX/0iq;",
        ":",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingDataSpec$ProvidesInspirationLoggingData;",
        ":",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationStateSpec$ProvidesInspirationState;",
        "DerivedData:",
        "Ljava/lang/Object;",
        "Mutation::",
        "Lcom/facebook/ipc/composer/dataaccessor/ComposerCanSave;",
        ":",
        "Lcom/facebook/composer/attachments/ComposerAttachment$SetsAttachments",
        "<TMutation;>;:",
        "Lcom/facebook/composer/privacy/model/ComposerPrivacyData$SetsPrivacyData",
        "<TMutation;>;:",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingDataSpec$SetsInspirationLoggingData",
        "<TMutation;>;:",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationStateSpec$SetsInspirationState",
        "<TMutation;>;Services::",
        "LX/0il",
        "<TModelData;>;:",
        "LX/0im",
        "<TMutation;>;>",
        "Ljava/lang/Object;",
        "LX/0iK",
        "<TModelData;TDerivedData;>;",
        "LX/AsA;"
    }
.end annotation


# static fields
.field public static final a:LX/0jK;


# instance fields
.field public final b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TServices;>;"
        }
    .end annotation
.end field

.field public final c:Landroid/content/Context;

.field public final d:LX/Ase;

.field public final e:Lcom/facebook/friendsharing/inspiration/controller/CameraRollBottomTrayAdapter;

.field public final f:LX/1kH;

.field private final g:LX/0Sh;

.field public final h:LX/0tS;

.field public final i:LX/AuP;

.field public final j:Landroid/view/ViewStub;

.field public final k:LX/74n;

.field public final l:LX/ArL;

.field public final m:LX/ArT;

.field public n:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field public o:LX/1P1;

.field public p:Z

.field public q:LX/As8;

.field public r:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1720227
    const-class v0, LX/AsB;

    invoke-static {v0}, LX/0jK;->a(Ljava/lang/Class;)LX/0jK;

    move-result-object v0

    sput-object v0, LX/AsB;->a:LX/0jK;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;LX/0il;LX/ArT;LX/ArL;Landroid/view/ViewStub;Landroid/content/Context;LX/Asf;Lcom/facebook/friendsharing/inspiration/controller/CameraRollBottomTrayAdapter;LX/1kH;LX/0Sh;LX/0tS;LX/AuQ;LX/74n;)V
    .locals 2
    .param p1    # Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/0il;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/ArT;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/ArL;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Landroid/view/ViewStub;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;",
            "TServices;",
            "LX/ArT;",
            "LX/ArL;",
            "Landroid/view/ViewStub;",
            "Landroid/content/Context;",
            "LX/Asf;",
            "Lcom/facebook/friendsharing/inspiration/controller/CameraRollBottomTrayAdapter;",
            "LX/1kH;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/0tS;",
            "LX/AuQ;",
            "LX/74n;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1720209
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1720210
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/AsB;->b:Ljava/lang/ref/WeakReference;

    .line 1720211
    iput-object p6, p0, LX/AsB;->c:Landroid/content/Context;

    .line 1720212
    iget-object v0, p0, LX/AsB;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p7, p0, p1, v0}, LX/Asf;->a(LX/AsA;Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;Ljava/lang/Object;)LX/Ase;

    move-result-object v0

    iput-object v0, p0, LX/AsB;->d:LX/Ase;

    .line 1720213
    iput-object p8, p0, LX/AsB;->e:Lcom/facebook/friendsharing/inspiration/controller/CameraRollBottomTrayAdapter;

    .line 1720214
    iput-object p9, p0, LX/AsB;->f:LX/1kH;

    .line 1720215
    iput-object p10, p0, LX/AsB;->g:LX/0Sh;

    .line 1720216
    iput-object p11, p0, LX/AsB;->h:LX/0tS;

    .line 1720217
    const v0, 0x7f0b1a81

    invoke-virtual {p12, v0}, LX/AuQ;->a(I)LX/AuP;

    move-result-object v0

    iput-object v0, p0, LX/AsB;->i:LX/AuP;

    .line 1720218
    iput-object p5, p0, LX/AsB;->j:Landroid/view/ViewStub;

    .line 1720219
    iput-object p13, p0, LX/AsB;->k:LX/74n;

    .line 1720220
    iput-object p4, p0, LX/AsB;->l:LX/ArL;

    .line 1720221
    iput-object p3, p0, LX/AsB;->m:LX/ArT;

    .line 1720222
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/AsB;->r:Ljava/util/Map;

    .line 1720223
    invoke-static {p2}, LX/87N;->a(LX/0il;)LX/86o;

    move-result-object v0

    invoke-virtual {p0}, LX/AsB;->b()LX/86o;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 1720224
    invoke-direct {p0}, LX/AsB;->h()V

    .line 1720225
    :goto_0
    return-void

    .line 1720226
    :cond_0
    sget-object v0, LX/ArJ;->UNKNOWN:LX/ArJ;

    invoke-direct {p0, v0}, LX/AsB;->b(LX/ArJ;)V

    goto :goto_0
.end method

.method private b(LX/ArJ;)V
    .locals 1

    .prologue
    .line 1720171
    iget-object v0, p0, LX/AsB;->d:LX/Ase;

    invoke-virtual {v0}, LX/Ase;->c()V

    .line 1720172
    return-void
.end method

.method private h()V
    .locals 6

    .prologue
    .line 1720176
    iget-boolean v0, p0, LX/AsB;->p:Z

    if-nez v0, :cond_1

    .line 1720177
    iget-boolean v0, p0, LX/AsB;->p:Z

    if-nez v0, :cond_1

    .line 1720178
    iget-object v0, p0, LX/AsB;->d:LX/Ase;

    iget-object v1, p0, LX/AsB;->j:Landroid/view/ViewStub;

    const v2, 0x7f030685

    invoke-virtual {v0, v1, v2}, LX/Ase;->a(Landroid/view/ViewStub;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iput-object v0, p0, LX/AsB;->n:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 1720179
    const/4 v2, 0x0

    .line 1720180
    new-instance v0, LX/1P0;

    iget-object v1, p0, LX/AsB;->c:Landroid/content/Context;

    invoke-direct {v0, v1, v2, v2}, LX/1P0;-><init>(Landroid/content/Context;IZ)V

    iput-object v0, p0, LX/AsB;->o:LX/1P1;

    .line 1720181
    iget-object v0, p0, LX/AsB;->n:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v1, p0, LX/AsB;->o:LX/1P1;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 1720182
    iget-object v0, p0, LX/AsB;->n:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v1, p0, LX/AsB;->i:LX/AuP;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(LX/3x6;)V

    .line 1720183
    iget-object v0, p0, LX/AsB;->e:Lcom/facebook/friendsharing/inspiration/controller/CameraRollBottomTrayAdapter;

    new-instance v1, LX/As7;

    invoke-direct {v1, p0}, LX/As7;-><init>(LX/AsB;)V

    .line 1720184
    iput-object v1, v0, Lcom/facebook/friendsharing/inspiration/controller/CameraRollBottomTrayAdapter;->g:LX/As7;

    .line 1720185
    iget-object v0, p0, LX/AsB;->n:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v1, p0, LX/AsB;->e:Lcom/facebook/friendsharing/inspiration/controller/CameraRollBottomTrayAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 1720186
    iget-object v0, p0, LX/AsB;->h:LX/0tS;

    .line 1720187
    invoke-static {v0}, LX/0tS;->e(LX/0tS;)V

    .line 1720188
    iget-object v1, v0, LX/0tS;->b:Landroid/util/DisplayMetrics;

    iget v1, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v1, v1

    const v2, 0x3e19999a    # 0.15f

    mul-float/2addr v1, v2

    float-to-int v1, v1

    move v0, v1

    .line 1720189
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x1

    invoke-direct {v1, v2, v0}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1720190
    iget-object v2, p0, LX/AsB;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b1a81

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    .line 1720191
    iget-object v2, p0, LX/AsB;->n:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v2, v1}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1720192
    iget-object v1, p0, LX/AsB;->e:Lcom/facebook/friendsharing/inspiration/controller/CameraRollBottomTrayAdapter;

    .line 1720193
    if-lez v0, :cond_2

    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, LX/0PB;->checkArgument(Z)V

    .line 1720194
    iput v0, v1, Lcom/facebook/friendsharing/inspiration/controller/CameraRollBottomTrayAdapter;->f:I

    .line 1720195
    iget-object v2, v1, Lcom/facebook/friendsharing/inspiration/controller/CameraRollBottomTrayAdapter;->e:LX/As2;

    iget v3, v1, Lcom/facebook/friendsharing/inspiration/controller/CameraRollBottomTrayAdapter;->f:I

    iget v4, v1, Lcom/facebook/friendsharing/inspiration/controller/CameraRollBottomTrayAdapter;->f:I

    invoke-virtual {v2, v3, v4}, LX/As2;->a(II)V

    .line 1720196
    iget-object v1, p0, LX/AsB;->n:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v1}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1720197
    iget-object v0, p0, LX/AsB;->n:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v0}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->requestLayout()V

    .line 1720198
    const/4 v5, 0x5

    .line 1720199
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 1720200
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v5, :cond_0

    .line 1720201
    new-instance v2, Lcom/facebook/media/util/model/MediaModel;

    const/4 v3, 0x0

    sget-object v4, LX/1po;->PHOTO:LX/1po;

    invoke-direct {v2, v3, v4}, Lcom/facebook/media/util/model/MediaModel;-><init>(Ljava/lang/String;LX/1po;)V

    .line 1720202
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1720203
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1720204
    :cond_0
    iget-object v0, p0, LX/AsB;->e:Lcom/facebook/friendsharing/inspiration/controller/CameraRollBottomTrayAdapter;

    invoke-virtual {v0, v1}, Lcom/facebook/friendsharing/inspiration/controller/CameraRollBottomTrayAdapter;->a(Ljava/util/List;)V

    .line 1720205
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/AsB;->p:Z

    .line 1720206
    :cond_1
    iget-object v0, p0, LX/AsB;->d:LX/Ase;

    invoke-virtual {v0}, LX/Ase;->b()V

    .line 1720207
    return-void

    .line 1720208
    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1720228
    iget-object v0, p0, LX/AsB;->g:LX/0Sh;

    new-instance v1, LX/As9;

    invoke-direct {v1, p0}, LX/As9;-><init>(LX/AsB;)V

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, LX/0Sh;->a(LX/3nE;[Ljava/lang/Object;)LX/3nE;

    .line 1720229
    return-void
.end method

.method public final a(LX/5L2;)V
    .locals 0

    .prologue
    .line 1720175
    return-void
.end method

.method public final a(LX/ArJ;)V
    .locals 1

    .prologue
    .line 1720173
    iget-object v0, p0, LX/AsB;->m:LX/ArT;

    invoke-virtual {v0, p1}, LX/ArT;->k(LX/ArJ;)V

    .line 1720174
    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 7

    .prologue
    .line 1720152
    check-cast p1, LX/0iq;

    const/4 v6, 0x0

    .line 1720153
    iget-object v0, p0, LX/AsB;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 1720154
    check-cast p1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {p1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v2

    .line 1720155
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0iq;

    check-cast v1, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v1}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v3

    .line 1720156
    invoke-virtual {v3}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->getOpenBottomTray()LX/86o;

    move-result-object v1

    .line 1720157
    invoke-virtual {p0}, LX/AsB;->b()LX/86o;

    move-result-object v4

    if-ne v1, v4, :cond_0

    invoke-virtual {v1}, LX/86o;->shouldCloseOnBackPressed()Z

    move-result v4

    if-eqz v4, :cond_0

    sget-object v4, LX/86t;->TRAY:LX/86t;

    invoke-static {v2, v3, v4}, LX/87R;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationState;Lcom/facebook/friendsharing/inspiration/model/InspirationState;LX/86t;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1720158
    invoke-static {v3}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationState;)Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;

    move-result-object v4

    .line 1720159
    invoke-virtual {v4, v6}, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->setIsBackPressed(Z)Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;

    .line 1720160
    invoke-static {v4, v1}, LX/87N;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;LX/86o;)Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;

    move-result-object v4

    move-object v1, v0

    .line 1720161
    check-cast v1, LX/0im;

    invoke-interface {v1}, LX/0im;->c()LX/0jJ;

    move-result-object v1

    sget-object v5, LX/AsB;->a:LX/0jK;

    invoke-virtual {v1, v5}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v1

    check-cast v1, LX/0jL;

    invoke-virtual {v4}, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v4

    invoke-virtual {v1, v4}, LX/0jL;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationState;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0jL;

    invoke-virtual {v1}, LX/0jL;->a()V

    .line 1720162
    sget-object v1, LX/ArJ;->TAP_BACK_BUTTON:LX/ArJ;

    invoke-virtual {p0, v1}, LX/AsB;->a(LX/ArJ;)V

    .line 1720163
    :cond_0
    invoke-virtual {p0}, LX/AsB;->b()LX/86o;

    move-result-object v1

    invoke-static {v2, v3, v1}, LX/87N;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationState;Lcom/facebook/friendsharing/inspiration/model/InspirationState;LX/86o;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1720164
    invoke-direct {p0}, LX/AsB;->h()V

    .line 1720165
    :cond_1
    :goto_0
    invoke-virtual {v2}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->shouldRefreshCameraRoll()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {v3}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->shouldRefreshCameraRoll()Z

    move-result v1

    if-eqz v1, :cond_2

    move-object v1, v0

    .line 1720166
    check-cast v1, LX/0im;

    invoke-interface {v1}, LX/0im;->c()LX/0jJ;

    move-result-object v1

    const-class v2, LX/AsB;

    invoke-static {v2}, LX/0jK;->a(Ljava/lang/Class;)LX/0jK;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0jJ;->a(LX/0jK;)LX/0jL;

    move-result-object v1

    check-cast v1, LX/0jL;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0iq;

    check-cast v0, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v0}, Lcom/facebook/composer/system/model/ComposerModelImpl;->n()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationState;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationState;)Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->setShouldRefreshCameraRoll(Z)Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/friendsharing/inspiration/model/InspirationState$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/InspirationState;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0jL;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationState;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0jL;

    invoke-virtual {v0}, LX/0jL;->a()V

    .line 1720167
    invoke-virtual {p0}, LX/AsB;->a()V

    .line 1720168
    :cond_2
    return-void

    .line 1720169
    :cond_3
    invoke-virtual {p0}, LX/AsB;->b()LX/86o;

    move-result-object v1

    invoke-static {v2, v3, v1}, LX/87N;->b(Lcom/facebook/friendsharing/inspiration/model/InspirationState;Lcom/facebook/friendsharing/inspiration/model/InspirationState;LX/86o;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1720170
    sget-object v1, LX/ArJ;->UNKNOWN:LX/ArJ;

    invoke-direct {p0, v1}, LX/AsB;->b(LX/ArJ;)V

    goto :goto_0
.end method

.method public final b()LX/86o;
    .locals 1

    .prologue
    .line 1720151
    sget-object v0, LX/86o;->CAMERA_ROLL:LX/86o;

    return-object v0
.end method
