.class public LX/BGV;
.super LX/Aqa;
.source ""


# instance fields
.field public a:LX/BGO;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private final b:LX/BGN;

.field private c:LX/BGM;


# direct methods
.method public constructor <init>(LX/BGN;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1767117
    invoke-direct {p0}, LX/Aqa;-><init>()V

    .line 1767118
    iput-object p1, p0, LX/BGV;->b:LX/BGN;

    .line 1767119
    return-void
.end method


# virtual methods
.method public final a(LX/1RN;Landroid/content/Context;)LX/AlW;
    .locals 4

    .prologue
    .line 1767120
    invoke-static {p1}, LX/1RN;->a(LX/1RN;)LX/1kK;

    move-result-object v0

    .line 1767121
    instance-of v1, v0, LX/1kW;

    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 1767122
    check-cast v0, LX/1kW;

    .line 1767123
    iget-object v1, p0, LX/BGV;->c:LX/BGM;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/BGV;->c:LX/BGM;

    .line 1767124
    iget-object v2, v1, LX/BGM;->d:Lcom/facebook/productionprompts/model/ProductionPrompt;

    move-object v1, v2

    .line 1767125
    iget-object v2, v0, LX/1kW;->a:Lcom/facebook/productionprompts/model/ProductionPrompt;

    move-object v0, v2

    .line 1767126
    invoke-virtual {v1, v0}, Lcom/facebook/productionprompts/model/ProductionPrompt;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1767127
    :cond_0
    iget-object v0, p0, LX/BGV;->a:LX/BGO;

    .line 1767128
    new-instance v3, LX/BGM;

    invoke-static {v0}, LX/1bQ;->b(LX/0QB;)LX/1bQ;

    move-result-object v1

    check-cast v1, LX/1bQ;

    invoke-static {v0}, LX/1E1;->b(LX/0QB;)LX/1E1;

    move-result-object v2

    check-cast v2, LX/1E1;

    invoke-direct {v3, p2, p1, v1, v2}, LX/BGM;-><init>(Landroid/content/Context;LX/1RN;LX/1bQ;LX/1E1;)V

    .line 1767129
    move-object v0, v3

    .line 1767130
    iput-object v0, p0, LX/BGV;->c:LX/BGM;

    .line 1767131
    :cond_1
    iget-object v0, p0, LX/BGV;->c:LX/BGM;

    return-object v0
.end method

.method public final a(LX/2xq;LX/1RN;)V
    .locals 2
    .param p1    # LX/2xq;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1767132
    instance-of v0, p1, LX/2xq;

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1767133
    check-cast p1, LX/2xq;

    .line 1767134
    iget-object v0, p0, LX/BGV;->b:LX/BGN;

    .line 1767135
    iput-object p2, v0, LX/BGN;->c:LX/1RN;

    .line 1767136
    iget-object v0, p1, LX/2xq;->a:Landroid/view/View;

    iget-object v1, p0, LX/BGV;->b:LX/BGN;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1767137
    return-void
.end method

.method public final b(LX/2xq;LX/1RN;)V
    .locals 2
    .param p1    # LX/2xq;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 1767138
    instance-of v0, p1, LX/2xq;

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1767139
    check-cast p1, LX/2xq;

    .line 1767140
    iget-object v0, p0, LX/BGV;->b:LX/BGN;

    .line 1767141
    iput-object v1, v0, LX/BGN;->c:LX/1RN;

    .line 1767142
    iget-object v0, p1, LX/2xq;->a:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1767143
    return-void
.end method

.method public final e(LX/1RN;)Z
    .locals 2

    .prologue
    .line 1767144
    invoke-static {p1}, LX/1RN;->a(LX/1RN;)LX/1kK;

    move-result-object v0

    .line 1767145
    instance-of v1, v0, LX/1kW;

    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 1767146
    check-cast v0, LX/1kW;

    .line 1767147
    iget-object v1, v0, LX/1kW;->a:Lcom/facebook/productionprompts/model/ProductionPrompt;

    move-object v0, v1

    .line 1767148
    invoke-virtual {v0}, Lcom/facebook/productionprompts/model/ProductionPrompt;->o()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
