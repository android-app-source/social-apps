.class public LX/Ber;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;",
            ">;"
        }
    .end annotation
.end field

.field public c:I

.field public d:Z

.field public e:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1805466
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1805467
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/Ber;->b:Ljava/util/ArrayList;

    .line 1805468
    const/4 v0, -0x1

    iput v0, p0, LX/Ber;->c:I

    .line 1805469
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Ber;->d:Z

    .line 1805470
    const/4 v0, 0x0

    iput-object v0, p0, LX/Ber;->e:Ljava/lang/String;

    .line 1805471
    return-void
.end method


# virtual methods
.method public final b()Z
    .locals 2

    .prologue
    .line 1805472
    invoke-virtual {p0}, LX/Ber;->c()I

    move-result v0

    iget v1, p0, LX/Ber;->c:I

    add-int/lit8 v1, v1, 0x1

    sub-int/2addr v0, v1

    move v0, v0

    .line 1805473
    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 1805474
    iget-object v0, p0, LX/Ber;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public final e()Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;
    .locals 2

    .prologue
    .line 1805475
    iget v0, p0, LX/Ber;->c:I

    if-ltz v0, :cond_0

    iget v0, p0, LX/Ber;->c:I

    invoke-virtual {p0}, LX/Ber;->c()I

    move-result v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1805476
    iget-object v0, p0, LX/Ber;->b:Ljava/util/ArrayList;

    iget v1, p0, LX/Ber;->c:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;

    return-object v0

    .line 1805477
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;
    .locals 1

    .prologue
    .line 1805478
    invoke-virtual {p0}, LX/Ber;->b()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1805479
    iget v0, p0, LX/Ber;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/Ber;->c:I

    .line 1805480
    invoke-virtual {p0}, LX/Ber;->e()Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;

    move-result-object v0

    return-object v0
.end method
