.class public final LX/CVU;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 7

    .prologue
    .line 1903870
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1903871
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 1903872
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 1903873
    const/4 v2, 0x0

    .line 1903874
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_5

    .line 1903875
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1903876
    :goto_1
    move v1, v2

    .line 1903877
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1903878
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0

    .line 1903879
    :cond_1
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1903880
    :cond_2
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_4

    .line 1903881
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1903882
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1903883
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_2

    if-eqz v4, :cond_2

    .line 1903884
    const-string v5, "event_handler"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1903885
    invoke-static {p0, p1}, LX/CVT;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_2

    .line 1903886
    :cond_3
    const-string v5, "event_name"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1903887
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    goto :goto_2

    .line 1903888
    :cond_4
    const/4 v4, 0x2

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1903889
    invoke-virtual {p1, v2, v3}, LX/186;->b(II)V

    .line 1903890
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v1}, LX/186;->b(II)V

    .line 1903891
    invoke-virtual {p1}, LX/186;->d()I

    move-result v2

    goto :goto_1

    :cond_5
    move v1, v2

    move v3, v2

    goto :goto_2
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 5

    .prologue
    .line 1903854
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1903855
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 1903856
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    const/4 v4, 0x1

    .line 1903857
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1903858
    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1903859
    if-eqz v2, :cond_0

    .line 1903860
    const-string v3, "event_handler"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1903861
    invoke-static {p0, v2, p2}, LX/CVT;->a(LX/15i;ILX/0nX;)V

    .line 1903862
    :cond_0
    invoke-virtual {p0, v1, v4}, LX/15i;->g(II)I

    move-result v2

    .line 1903863
    if-eqz v2, :cond_1

    .line 1903864
    const-string v2, "event_name"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1903865
    invoke-virtual {p0, v1, v4}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1903866
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1903867
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1903868
    :cond_2
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1903869
    return-void
.end method
