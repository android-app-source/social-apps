.class public final LX/Bao;
.super LX/1cC;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;


# direct methods
.method public constructor <init>(Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;)V
    .locals 0

    .prologue
    .line 1799696
    iput-object p1, p0, LX/Bao;->a:Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;

    invoke-direct {p0}, LX/1cC;-><init>()V

    return-void
.end method

.method private a(Ljava/lang/String;LX/1ln;)V
    .locals 1
    .param p2    # LX/1ln;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1799693
    iget-object v0, p0, LX/Bao;->a:Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;

    iget-object v0, v0, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->q:LX/1cC;

    if-eqz v0, :cond_0

    .line 1799694
    iget-object v0, p0, LX/Bao;->a:Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;

    iget-object v0, v0, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->q:LX/1cC;

    invoke-virtual {v0, p1, p2}, LX/1Ah;->b(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1799695
    :cond_0
    return-void
.end method

.method private a(Ljava/lang/String;LX/1ln;Landroid/graphics/drawable/Animatable;)V
    .locals 1
    .param p2    # LX/1ln;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/graphics/drawable/Animatable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1799677
    iget-object v0, p0, LX/Bao;->a:Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;

    iget-object v0, v0, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->q:LX/1cC;

    if-eqz v0, :cond_0

    .line 1799678
    iget-object v0, p0, LX/Bao;->a:Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;

    iget-object v0, v0, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->q:LX/1cC;

    invoke-virtual {v0, p1, p2, p3}, LX/1Ah;->a(Ljava/lang/String;Ljava/lang/Object;Landroid/graphics/drawable/Animatable;)V

    .line 1799679
    :cond_0
    iget-object v0, p0, LX/Bao;->a:Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;

    iget-object v0, v0, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->d:LX/1bf;

    if-nez v0, :cond_1

    .line 1799680
    invoke-direct {p0}, LX/Bao;->b()V

    .line 1799681
    :cond_1
    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 1799689
    iget-object v0, p0, LX/Bao;->a:Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;

    iget-boolean v0, v0, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->p:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/Bao;->a:Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;

    iget-object v0, v0, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->d:LX/1bf;

    if-eqz v0, :cond_0

    .line 1799690
    iget-object v0, p0, LX/Bao;->a:Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;

    const/4 v1, 0x1

    .line 1799691
    invoke-static {v0, v1}, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->a$redex0(Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;Z)V

    .line 1799692
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 1799686
    iget-object v0, p0, LX/Bao;->a:Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;

    iget-object v0, v0, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->q:LX/1cC;

    if-eqz v0, :cond_0

    .line 1799687
    iget-object v0, p0, LX/Bao;->a:Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;

    iget-object v0, v0, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->q:LX/1cC;

    invoke-virtual {v0, p1, p2}, LX/1Ah;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1799688
    :cond_0
    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/String;Ljava/lang/Object;Landroid/graphics/drawable/Animatable;)V
    .locals 0
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/graphics/drawable/Animatable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1799685
    check-cast p2, LX/1ln;

    invoke-direct {p0, p1, p2, p3}, LX/Bao;->a(Ljava/lang/String;LX/1ln;Landroid/graphics/drawable/Animatable;)V

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1799682
    iget-object v0, p0, LX/Bao;->a:Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;

    iget-object v0, v0, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->q:LX/1cC;

    if-eqz v0, :cond_0

    .line 1799683
    iget-object v0, p0, LX/Bao;->a:Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;

    iget-object v0, v0, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->q:LX/1cC;

    invoke-virtual {v0, p1, p2}, LX/1Ah;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1799684
    :cond_0
    return-void
.end method

.method public final synthetic b(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 0
    .param p2    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1799676
    check-cast p2, LX/1ln;

    invoke-direct {p0, p1, p2}, LX/Bao;->a(Ljava/lang/String;LX/1ln;)V

    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1799671
    iget-object v0, p0, LX/Bao;->a:Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;

    iget-object v0, v0, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->q:LX/1cC;

    if-eqz v0, :cond_0

    .line 1799672
    iget-object v0, p0, LX/Bao;->a:Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;

    iget-object v0, v0, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->q:LX/1cC;

    invoke-virtual {v0, p1, p2}, LX/1Ah;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1799673
    :cond_0
    iget-object v0, p0, LX/Bao;->a:Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;

    iget-object v0, v0, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->d:LX/1bf;

    if-nez v0, :cond_1

    .line 1799674
    invoke-direct {p0}, LX/Bao;->b()V

    .line 1799675
    :cond_1
    return-void
.end method
