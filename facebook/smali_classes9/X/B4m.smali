.class public final LX/B4m;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 1742785
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 1742786
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1742787
    :goto_0
    return v1

    .line 1742788
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1742789
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 1742790
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1742791
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1742792
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 1742793
    const-string v4, "name"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1742794
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_1

    .line 1742795
    :cond_2
    const-string v4, "profile_overlay_category"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1742796
    const/4 v3, 0x0

    .line 1742797
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v4, :cond_8

    .line 1742798
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1742799
    :goto_2
    move v0, v3

    .line 1742800
    goto :goto_1

    .line 1742801
    :cond_3
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1742802
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 1742803
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1742804
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    move v2, v1

    goto :goto_1

    .line 1742805
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1742806
    :cond_6
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_7

    .line 1742807
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1742808
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1742809
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_6

    if-eqz v4, :cond_6

    .line 1742810
    const-string v5, "pages"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1742811
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1742812
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v6, :cond_d

    .line 1742813
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1742814
    :goto_4
    move v0, v4

    .line 1742815
    goto :goto_3

    .line 1742816
    :cond_7
    const/4 v4, 0x1

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1742817
    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1742818
    invoke-virtual {p1}, LX/186;->d()I

    move-result v3

    goto :goto_2

    :cond_8
    move v0, v3

    goto :goto_3

    .line 1742819
    :cond_9
    :goto_5
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_b

    .line 1742820
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 1742821
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1742822
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_9

    if-eqz v7, :cond_9

    .line 1742823
    const-string v8, "count"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_a

    .line 1742824
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v6, v0

    move v0, v5

    goto :goto_5

    .line 1742825
    :cond_a
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_5

    .line 1742826
    :cond_b
    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 1742827
    if-eqz v0, :cond_c

    .line 1742828
    invoke-virtual {p1, v4, v6, v4}, LX/186;->a(III)V

    .line 1742829
    :cond_c
    invoke-virtual {p1}, LX/186;->d()I

    move-result v4

    goto :goto_4

    :cond_d
    move v0, v4

    move v6, v4

    goto :goto_5
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1742830
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1742831
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1742832
    if-eqz v0, :cond_0

    .line 1742833
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1742834
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1742835
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1742836
    if-eqz v0, :cond_3

    .line 1742837
    const-string v1, "profile_overlay_category"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1742838
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1742839
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v1

    .line 1742840
    if-eqz v1, :cond_2

    .line 1742841
    const-string p1, "pages"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1742842
    const/4 p1, 0x0

    .line 1742843
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1742844
    invoke-virtual {p0, v1, p1, p1}, LX/15i;->a(III)I

    move-result p1

    .line 1742845
    if-eqz p1, :cond_1

    .line 1742846
    const-string v0, "count"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1742847
    invoke-virtual {p2, p1}, LX/0nX;->b(I)V

    .line 1742848
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1742849
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1742850
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1742851
    return-void
.end method
