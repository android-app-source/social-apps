.class public final enum LX/CY5;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/CY5;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/CY5;

.field public static final enum EMPTY:LX/CY5;

.field public static final enum INVALID:LX/CY5;

.field public static final enum NONE:LX/CY5;

.field public static final enum SERVER:LX/CY5;

.field public static final enum UNKNOWN:LX/CY5;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1910771
    new-instance v0, LX/CY5;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, LX/CY5;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CY5;->NONE:LX/CY5;

    .line 1910772
    new-instance v0, LX/CY5;

    const-string v1, "EMPTY"

    invoke-direct {v0, v1, v3}, LX/CY5;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CY5;->EMPTY:LX/CY5;

    .line 1910773
    new-instance v0, LX/CY5;

    const-string v1, "INVALID"

    invoke-direct {v0, v1, v4}, LX/CY5;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CY5;->INVALID:LX/CY5;

    .line 1910774
    new-instance v0, LX/CY5;

    const-string v1, "SERVER"

    invoke-direct {v0, v1, v5}, LX/CY5;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CY5;->SERVER:LX/CY5;

    .line 1910775
    new-instance v0, LX/CY5;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v6}, LX/CY5;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/CY5;->UNKNOWN:LX/CY5;

    .line 1910776
    const/4 v0, 0x5

    new-array v0, v0, [LX/CY5;

    sget-object v1, LX/CY5;->NONE:LX/CY5;

    aput-object v1, v0, v2

    sget-object v1, LX/CY5;->EMPTY:LX/CY5;

    aput-object v1, v0, v3

    sget-object v1, LX/CY5;->INVALID:LX/CY5;

    aput-object v1, v0, v4

    sget-object v1, LX/CY5;->SERVER:LX/CY5;

    aput-object v1, v0, v5

    sget-object v1, LX/CY5;->UNKNOWN:LX/CY5;

    aput-object v1, v0, v6

    sput-object v0, LX/CY5;->$VALUES:[LX/CY5;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1910777
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/CY5;
    .locals 1

    .prologue
    .line 1910778
    const-class v0, LX/CY5;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/CY5;

    return-object v0
.end method

.method public static values()[LX/CY5;
    .locals 1

    .prologue
    .line 1910779
    sget-object v0, LX/CY5;->$VALUES:[LX/CY5;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/CY5;

    return-object v0
.end method
