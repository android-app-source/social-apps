.class public final LX/BsW;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/1ym;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public b:Ljava/lang/String;

.field public c:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

.field public d:LX/1Pb;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public e:I

.field public f:I

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:LX/DHA;

.field public final synthetic j:LX/1ym;


# direct methods
.method public constructor <init>(LX/1ym;)V
    .locals 1

    .prologue
    .line 1827658
    iput-object p1, p0, LX/BsW;->j:LX/1ym;

    .line 1827659
    move-object v0, p1

    .line 1827660
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1827661
    sget v0, LX/1ys;->a:I

    iput v0, p0, LX/BsW;->e:I

    .line 1827662
    sget v0, LX/1ys;->b:I

    iput v0, p0, LX/BsW;->f:I

    .line 1827663
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1827664
    const-string v0, "FollowButtonComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1827665
    if-ne p0, p1, :cond_1

    .line 1827666
    :cond_0
    :goto_0
    return v0

    .line 1827667
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1827668
    goto :goto_0

    .line 1827669
    :cond_3
    check-cast p1, LX/BsW;

    .line 1827670
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1827671
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1827672
    if-eq v2, v3, :cond_0

    .line 1827673
    iget-object v2, p0, LX/BsW;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/BsW;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/BsW;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1827674
    goto :goto_0

    .line 1827675
    :cond_5
    iget-object v2, p1, LX/BsW;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 1827676
    :cond_6
    iget-object v2, p0, LX/BsW;->b:Ljava/lang/String;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/BsW;->b:Ljava/lang/String;

    iget-object v3, p1, LX/BsW;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1827677
    goto :goto_0

    .line 1827678
    :cond_8
    iget-object v2, p1, LX/BsW;->b:Ljava/lang/String;

    if-nez v2, :cond_7

    .line 1827679
    :cond_9
    iget-object v2, p0, LX/BsW;->c:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/BsW;->c:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    iget-object v3, p1, LX/BsW;->c:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    invoke-virtual {v2, v3}, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 1827680
    goto :goto_0

    .line 1827681
    :cond_b
    iget-object v2, p1, LX/BsW;->c:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    if-nez v2, :cond_a

    .line 1827682
    :cond_c
    iget-object v2, p0, LX/BsW;->d:LX/1Pb;

    if-eqz v2, :cond_e

    iget-object v2, p0, LX/BsW;->d:LX/1Pb;

    iget-object v3, p1, LX/BsW;->d:LX/1Pb;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    :cond_d
    move v0, v1

    .line 1827683
    goto :goto_0

    .line 1827684
    :cond_e
    iget-object v2, p1, LX/BsW;->d:LX/1Pb;

    if-nez v2, :cond_d

    .line 1827685
    :cond_f
    iget v2, p0, LX/BsW;->e:I

    iget v3, p1, LX/BsW;->e:I

    if-eq v2, v3, :cond_10

    move v0, v1

    .line 1827686
    goto :goto_0

    .line 1827687
    :cond_10
    iget v2, p0, LX/BsW;->f:I

    iget v3, p1, LX/BsW;->f:I

    if-eq v2, v3, :cond_11

    move v0, v1

    .line 1827688
    goto :goto_0

    .line 1827689
    :cond_11
    iget-object v2, p0, LX/BsW;->g:Ljava/lang/String;

    if-eqz v2, :cond_13

    iget-object v2, p0, LX/BsW;->g:Ljava/lang/String;

    iget-object v3, p1, LX/BsW;->g:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_14

    :cond_12
    move v0, v1

    .line 1827690
    goto/16 :goto_0

    .line 1827691
    :cond_13
    iget-object v2, p1, LX/BsW;->g:Ljava/lang/String;

    if-nez v2, :cond_12

    .line 1827692
    :cond_14
    iget-object v2, p0, LX/BsW;->h:Ljava/lang/String;

    if-eqz v2, :cond_16

    iget-object v2, p0, LX/BsW;->h:Ljava/lang/String;

    iget-object v3, p1, LX/BsW;->h:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_17

    :cond_15
    move v0, v1

    .line 1827693
    goto/16 :goto_0

    .line 1827694
    :cond_16
    iget-object v2, p1, LX/BsW;->h:Ljava/lang/String;

    if-nez v2, :cond_15

    .line 1827695
    :cond_17
    iget-object v2, p0, LX/BsW;->i:LX/DHA;

    if-eqz v2, :cond_18

    iget-object v2, p0, LX/BsW;->i:LX/DHA;

    iget-object v3, p1, LX/BsW;->i:LX/DHA;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1827696
    goto/16 :goto_0

    .line 1827697
    :cond_18
    iget-object v2, p1, LX/BsW;->i:LX/DHA;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
