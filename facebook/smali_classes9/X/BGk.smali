.class public final LX/BGk;
.super LX/79T;
.source ""


# instance fields
.field public final synthetic a:LX/9iX;

.field public final synthetic b:Z

.field public final synthetic c:Lcom/facebook/photos/simplepicker/SimplePickerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/simplepicker/SimplePickerFragment;LX/9iX;Z)V
    .locals 0

    .prologue
    .line 1767392
    iput-object p1, p0, LX/BGk;->c:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    iput-object p2, p0, LX/BGk;->a:LX/9iX;

    iput-boolean p3, p0, LX/BGk;->b:Z

    invoke-direct {p0}, LX/79T;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 9

    .prologue
    .line 1767393
    iget-object v0, p0, LX/BGk;->c:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    iget-object v1, p0, LX/BGk;->a:LX/9iX;

    iget-boolean v2, p0, LX/BGk;->b:Z

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1767394
    iget-object v3, v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->z:LX/BGe;

    invoke-virtual {v1}, LX/9iX;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1767395
    sget-object v5, LX/BGd;->CAMERA_OPENED:LX/BGd;

    invoke-static {v5}, LX/BGe;->a(LX/BGd;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string v8, "media_type"

    invoke-virtual {v5, v8, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    invoke-static {v3, v5}, LX/BGe;->a(LX/BGe;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1767396
    iget-boolean v3, v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->ac:Z

    if-eqz v3, :cond_0

    .line 1767397
    new-instance v4, Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    const-class v5, Lcom/facebook/friendsharing/inspiration/activity/InspirationCameraActivity;

    invoke-direct {v4, v3, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1767398
    const-string v3, "extra_creativecam_composer_session_id"

    iget-object v5, v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->W:Ljava/lang/String;

    invoke-virtual {v4, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1767399
    iget-object v3, v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->H:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/content/SecureContextHelper;

    const/16 v5, 0x4dd

    invoke-interface {v3, v4, v5, v0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 1767400
    :goto_0
    return-void

    .line 1767401
    :cond_0
    iget-object v3, v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->av:LX/0ad;

    sget-short v4, LX/BHD;->b:S

    invoke-interface {v3, v4, v6}, LX/0ad;->a(SZ)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1767402
    invoke-static {}, Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;->newBuilder()LX/ANc;

    move-result-object v4

    sget-object v3, LX/9iX;->IMAGE:LX/9iX;

    if-ne v1, v3, :cond_1

    sget-object v3, Lcom/facebook/cameracore/ui/CaptureType;->PHOTO:Lcom/facebook/cameracore/ui/CaptureType;

    :goto_1
    invoke-static {v3}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    .line 1767403
    iput-object v3, v4, LX/ANc;->d:LX/0Px;

    .line 1767404
    move-object v3, v4

    .line 1767405
    iget-object v4, v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->av:LX/0ad;

    sget-short v5, LX/BHD;->a:S

    invoke-interface {v4, v5, v6}, LX/0ad;->a(SZ)Z

    move-result v4

    .line 1767406
    iput-boolean v4, v3, LX/ANc;->e:Z

    .line 1767407
    move-object v3, v3

    .line 1767408
    iput-boolean v6, v3, LX/ANc;->h:Z

    .line 1767409
    move-object v3, v3

    .line 1767410
    const-string v4, "SimplePicker"

    .line 1767411
    iput-object v4, v3, LX/ANc;->f:Ljava/lang/String;

    .line 1767412
    move-object v3, v3

    .line 1767413
    iget-object v4, v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->W:Ljava/lang/String;

    .line 1767414
    iput-object v4, v3, LX/ANc;->g:Ljava/lang/String;

    .line 1767415
    move-object v3, v3

    .line 1767416
    invoke-virtual {v3}, LX/ANc;->a()Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;

    move-result-object v4

    .line 1767417
    iget-object v3, v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->H:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5, v4}, Lcom/facebook/cameracore/activity/CameraCoreSingleCaptureActivity;->a(Landroid/content/Context;Lcom/facebook/cameracore/ui/CameraCoreFragmentConfig;)Landroid/content/Intent;

    move-result-object v4

    const/16 v5, 0x4e8

    invoke-interface {v3, v4, v5, v0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    goto :goto_0

    .line 1767418
    :cond_1
    sget-object v3, Lcom/facebook/cameracore/ui/CaptureType;->VIDEO:Lcom/facebook/cameracore/ui/CaptureType;

    goto :goto_1

    .line 1767419
    :cond_2
    sget-object v3, LX/9iX;->IMAGE:LX/9iX;

    invoke-virtual {v3, v1}, LX/9iX;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    iget-object v3, v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->a:LX/BHj;

    invoke-virtual {v3}, LX/BHj;->a()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 1767420
    new-instance v3, LX/89V;

    invoke-direct {v3}, LX/89V;-><init>()V

    .line 1767421
    iput-boolean v6, v3, LX/89V;->b:Z

    .line 1767422
    move-object v3, v3

    .line 1767423
    sget-object v4, LX/4gI;->PHOTO_ONLY:LX/4gI;

    .line 1767424
    iput-object v4, v3, LX/89V;->f:LX/4gI;

    .line 1767425
    move-object v4, v3

    .line 1767426
    if-eqz v2, :cond_4

    sget-object v3, LX/89Z;->SIMPLEPICKER_LIVECAM_CELL:LX/89Z;

    .line 1767427
    :goto_2
    iput-object v3, v4, LX/89V;->l:LX/89Z;

    .line 1767428
    move-object v4, v4

    .line 1767429
    iget-object v3, v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->b:Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    .line 1767430
    iget-object v5, v3, Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;->d:Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;

    move-object v3, v5

    .line 1767431
    iget-boolean v5, v3, Lcom/facebook/ipc/simplepicker/SimplePickerConfiguration;->o:Z

    move v3, v5

    .line 1767432
    if-eqz v3, :cond_3

    .line 1767433
    iput-boolean v7, v4, LX/89V;->b:Z

    .line 1767434
    move-object v3, v4

    .line 1767435
    iput-boolean v7, v3, LX/89V;->e:Z

    .line 1767436
    move-object v3, v3

    .line 1767437
    iput-boolean v7, v3, LX/89V;->d:Z

    .line 1767438
    iget-object v3, v0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v3, v3

    .line 1767439
    const-string v5, "extra_staging_ground_selected_frame_id"

    invoke-virtual {v3, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1767440
    if-eqz v5, :cond_3

    .line 1767441
    iget-object v3, v0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v3, v3

    .line 1767442
    const-string v6, "extra_staging_ground_frame_pack"

    invoke-static {v3, v6}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;

    check-cast v3, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;

    invoke-static {v3}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    .line 1767443
    iput-object v5, v4, LX/89V;->i:Ljava/lang/String;

    .line 1767444
    move-object v5, v4

    .line 1767445
    iput-boolean v7, v5, LX/89V;->c:Z

    .line 1767446
    move-object v5, v5

    .line 1767447
    iput-object v3, v5, LX/89V;->h:LX/0Px;

    .line 1767448
    :cond_3
    iget-object v3, v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->C:LX/1b0;

    const/16 v5, 0x4db

    invoke-virtual {v4}, LX/89V;->a()Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;

    move-result-object v6

    iget-object v7, v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->W:Ljava/lang/String;

    const/4 v8, 0x0

    move-object v4, v0

    invoke-interface/range {v3 .. v8}, LX/1b0;->a(Landroid/support/v4/app/Fragment;ILcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;Ljava/lang/String;Lcom/facebook/productionprompts/logging/PromptAnalytics;)V

    goto/16 :goto_0

    .line 1767449
    :cond_4
    sget-object v3, LX/89Z;->SIMPLEPICKER_CAMERA_BUTTON:LX/89Z;

    goto :goto_2

    .line 1767450
    :cond_5
    :try_start_0
    iget-object v3, v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->I:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/9iY;

    invoke-virtual {v3, v1}, LX/9iY;->b(LX/9iX;)Landroid/content/Intent;

    move-result-object v4

    .line 1767451
    iget-object v3, v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->H:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/content/SecureContextHelper;

    iget-object v5, v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->I:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    .line 1767452
    sget-object v5, LX/9iW;->a:[I

    invoke-virtual {v1}, LX/9iX;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 1767453
    const/16 v5, 0x7d2

    :goto_3
    move v5, v5

    .line 1767454
    invoke-interface {v3, v4, v5, v0}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 1767455
    :catch_0
    iget-object v3, v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->aq:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/03V;

    sget-object v4, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->s:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "unable to open camera type: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1767456
    :pswitch_0
    const/16 v5, 0x7d3

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final a([Ljava/lang/String;[Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1767457
    iget-object v0, p0, LX/BGk;->c:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    iget-object v0, v0, Lcom/facebook/photos/simplepicker/SimplePickerFragment;->ad:LX/0kL;

    new-instance v1, LX/27k;

    iget-object v2, p0, LX/BGk;->c:Lcom/facebook/photos/simplepicker/SimplePickerFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f081373

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 1767458
    return-void
.end method
