.class public final LX/BCH;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 12

    .prologue
    const-wide/16 v4, 0x0

    const/4 v1, 0x1

    const/4 v6, 0x0

    .line 1759314
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_6

    .line 1759315
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1759316
    :goto_0
    return v6

    .line 1759317
    :cond_0
    const-string v10, "muted_until_time"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 1759318
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v2

    move v0, v1

    .line 1759319
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_4

    .line 1759320
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 1759321
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1759322
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_1

    if-eqz v9, :cond_1

    .line 1759323
    const-string v10, "application"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 1759324
    invoke-static {p0, p1}, LX/BCF;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 1759325
    :cond_2
    const-string v10, "owner"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 1759326
    invoke-static {p0, p1}, LX/BCG;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 1759327
    :cond_3
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1759328
    :cond_4
    const/4 v9, 0x3

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 1759329
    invoke-virtual {p1, v6, v8}, LX/186;->b(II)V

    .line 1759330
    if-eqz v0, :cond_5

    move-object v0, p1

    .line 1759331
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1759332
    :cond_5
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 1759333
    invoke-virtual {p1}, LX/186;->d()I

    move-result v6

    goto :goto_0

    :cond_6
    move v0, v6

    move v7, v6

    move-wide v2, v4

    move v8, v6

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 1759334
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1759335
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1759336
    if-eqz v0, :cond_0

    .line 1759337
    const-string v1, "application"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1759338
    invoke-static {p0, v0, p2}, LX/BCF;->a(LX/15i;ILX/0nX;)V

    .line 1759339
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 1759340
    cmp-long v2, v0, v2

    if-eqz v2, :cond_1

    .line 1759341
    const-string v2, "muted_until_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1759342
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 1759343
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1759344
    if-eqz v0, :cond_2

    .line 1759345
    const-string v1, "owner"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1759346
    invoke-static {p0, v0, p2}, LX/BCG;->a(LX/15i;ILX/0nX;)V

    .line 1759347
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1759348
    return-void
.end method
