.class public LX/Apj;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Apk;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1716577
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/Apj;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/Apk;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1716578
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1716579
    iput-object p1, p0, LX/Apj;->b:LX/0Ot;

    .line 1716580
    return-void
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 1716581
    check-cast p2, Lcom/facebook/fig/components/hscroll/footercomponents/FigHscrollFooterMediaComponent$FigHscrollFooterMediaComponentImpl;

    .line 1716582
    iget-object v0, p0, LX/Apj;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Apk;

    iget v2, p2, Lcom/facebook/fig/components/hscroll/footercomponents/FigHscrollFooterMediaComponent$FigHscrollFooterMediaComponentImpl;->a:I

    iget-object v3, p2, Lcom/facebook/fig/components/hscroll/footercomponents/FigHscrollFooterMediaComponent$FigHscrollFooterMediaComponentImpl;->b:Landroid/net/Uri;

    iget-object v4, p2, Lcom/facebook/fig/components/hscroll/footercomponents/FigHscrollFooterMediaComponent$FigHscrollFooterMediaComponentImpl;->c:Lcom/facebook/common/callercontext/CallerContext;

    iget-object v5, p2, Lcom/facebook/fig/components/hscroll/footercomponents/FigHscrollFooterMediaComponent$FigHscrollFooterMediaComponentImpl;->d:LX/1X1;

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, LX/Apk;->a(LX/1De;ILandroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;LX/1X1;)LX/1Dg;

    move-result-object v0

    .line 1716583
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1716584
    invoke-static {}, LX/1dS;->b()V

    .line 1716585
    const/4 v0, 0x0

    return-object v0
.end method
