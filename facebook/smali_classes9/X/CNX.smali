.class public final LX/CNX;
.super LX/1dc;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1dc",
        "<",
        "Landroid/graphics/drawable/Drawable;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/CO8;

.field public b:LX/CNb;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1881885
    invoke-static {}, LX/CNY;->a()LX/CNY;

    move-result-object v0

    invoke-direct {p0, v0}, LX/1dc;-><init>(LX/1n4;)V

    .line 1881886
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1881884
    const-string v0, "NTImageReference"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1881873
    if-ne p0, p1, :cond_1

    .line 1881874
    :cond_0
    :goto_0
    return v0

    .line 1881875
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1881876
    goto :goto_0

    .line 1881877
    :cond_3
    check-cast p1, LX/CNX;

    .line 1881878
    iget-object v2, p0, LX/CNX;->a:LX/CO8;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/CNX;->a:LX/CO8;

    iget-object v3, p1, LX/CNX;->a:LX/CO8;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1881879
    goto :goto_0

    .line 1881880
    :cond_5
    iget-object v2, p1, LX/CNX;->a:LX/CO8;

    if-nez v2, :cond_4

    .line 1881881
    :cond_6
    iget-object v2, p0, LX/CNX;->b:LX/CNb;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/CNX;->b:LX/CNb;

    iget-object v3, p1, LX/CNX;->b:LX/CNb;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1881882
    goto :goto_0

    .line 1881883
    :cond_7
    iget-object v2, p1, LX/CNX;->b:LX/CNb;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
