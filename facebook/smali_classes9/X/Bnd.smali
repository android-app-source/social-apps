.class public final LX/Bnd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/Bne;

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/BiU;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;


# direct methods
.method public constructor <init>(LX/Bne;Ljava/util/List;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/BiU;",
            ">;",
            "Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1820946
    iput-object p1, p0, LX/Bnd;->a:LX/Bne;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1820947
    iput-object p2, p0, LX/Bnd;->b:Ljava/util/List;

    .line 1820948
    iput-object p3, p0, LX/Bnd;->c:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 1820949
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x2

    const v1, -0x4a65c920

    invoke-static {v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v3

    .line 1820950
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1820951
    new-instance v4, LX/6WS;

    invoke-direct {v4, v0}, LX/6WS;-><init>(Landroid/content/Context;)V

    .line 1820952
    invoke-virtual {v4}, LX/5OM;->c()LX/5OG;

    move-result-object v5

    .line 1820953
    iget-object v0, p0, LX/Bnd;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BiU;

    .line 1820954
    sget-object v1, LX/BnX;->a:[I

    invoke-virtual {v0}, LX/BiU;->ordinal()I

    move-result v7

    aget v1, v1, v7

    packed-switch v1, :pswitch_data_0

    .line 1820955
    const/4 v1, 0x0

    :goto_1
    move-object v7, v1

    .line 1820956
    iget-object v1, p0, LX/Bnd;->c:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    if-ne v7, v1, :cond_0

    move v1, v2

    .line 1820957
    :goto_2
    iget-object v8, p0, LX/Bnd;->c:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    invoke-static {v8, v0, v1}, LX/Bne;->a(Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;LX/BiU;Z)I

    move-result v8

    .line 1820958
    invoke-virtual {v5, v8}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v8

    .line 1820959
    iget-object v9, p0, LX/Bnd;->a:LX/Bne;

    invoke-static {v9, v0, v1}, LX/Bne;->a$redex0(LX/Bne;LX/BiU;Z)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v8, v0}, LX/3Ai;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 1820960
    invoke-virtual {v8, v2}, LX/3Ai;->setCheckable(Z)Landroid/view/MenuItem;

    .line 1820961
    invoke-virtual {v8, v1}, LX/3Ai;->setChecked(Z)Landroid/view/MenuItem;

    .line 1820962
    new-instance v0, LX/Bnc;

    invoke-direct {v0, p0, v7}, LX/Bnc;-><init>(LX/Bnd;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)V

    invoke-virtual {v8, v0}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto :goto_0

    .line 1820963
    :cond_0
    const/4 v1, 0x0

    goto :goto_2

    .line 1820964
    :cond_1
    invoke-virtual {v4, p1}, LX/0ht;->f(Landroid/view/View;)V

    .line 1820965
    const v0, 0x7752d96f

    invoke-static {v0, v3}, LX/02F;->a(II)V

    return-void

    .line 1820966
    :pswitch_0
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->WATCHED:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    goto :goto_1

    .line 1820967
    :pswitch_1
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->GOING:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    goto :goto_1

    .line 1820968
    :pswitch_2
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->UNWATCHED:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
