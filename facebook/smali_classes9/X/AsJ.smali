.class public LX/AsJ;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/AsI;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1720349
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 1720350
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;Ljava/lang/Object;Landroid/view/ViewStub;)LX/AsI;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<ModelData:",
            "Ljava/lang/Object;",
            ":",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParamsSpec$ProvidesInspirationDoodleParams;",
            ":",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingDataSpec$ProvidesInspirationLoggingData;",
            ":",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationStateSpec$ProvidesInspirationState;",
            "Mutation:",
            "Ljava/lang/Object;",
            ":",
            "Lcom/facebook/ipc/composer/dataaccessor/ComposerCanSave;",
            ":",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParamsSpec$SetsInspirationDoodleParams",
            "<TMutation;>;:",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingDataSpec$SetsInspirationLoggingData",
            "<TMutation;>;:",
            "Lcom/facebook/friendsharing/inspiration/model/InspirationStateSpec$SetsInspirationState",
            "<TMutation;>;DerivedData:",
            "Ljava/lang/Object;",
            "Services:",
            "Ljava/lang/Object;",
            ":",
            "LX/0il",
            "<TModelData;>;:",
            "LX/0ik",
            "<TDerivedData;>;:",
            "LX/0im",
            "<TMutation;>;>(",
            "Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;",
            "TServices;",
            "Landroid/view/ViewStub;",
            ")",
            "LX/AsI",
            "<TModelData;TMutation;TDerivedData;TServices;>;"
        }
    .end annotation

    .prologue
    .line 1720347
    new-instance v0, LX/AsI;

    move-object v2, p2

    check-cast v2, LX/0il;

    const-class v1, LX/Asf;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/Asf;

    invoke-static {p0}, LX/0q4;->b(LX/0QB;)Landroid/view/WindowManager;

    move-result-object v5

    check-cast v5, Landroid/view/WindowManager;

    invoke-static {p0}, LX/0w3;->a(LX/0QB;)LX/0w3;

    move-result-object v6

    check-cast v6, LX/0w3;

    move-object v1, p1

    move-object v3, p3

    invoke-direct/range {v0 .. v6}, LX/AsI;-><init>(Lcom/facebook/friendsharing/inspiration/view/InspirationGestureHandlingLayout;LX/0il;Landroid/view/ViewStub;LX/Asf;Landroid/view/WindowManager;LX/0w3;)V

    .line 1720348
    return-object v0
.end method
