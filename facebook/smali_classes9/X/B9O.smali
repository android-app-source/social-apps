.class public LX/B9O;
.super Lcom/facebook/fbui/widget/megaphone/Megaphone;
.source ""

# interfaces
.implements LX/B9N;


# instance fields
.field public a:LX/B9T;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/graphql/model/GraphQLMegaphone;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1751506
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/megaphone/Megaphone;-><init>(Landroid/content/Context;)V

    .line 1751507
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, LX/B9O;

    invoke-static {v0}, LX/B9T;->b(LX/0QB;)LX/B9T;

    move-result-object v0

    check-cast v0, LX/B9T;

    iput-object v0, p0, LX/B9O;->a:LX/B9T;

    .line 1751508
    return-void
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLEntity;)Landroid/net/Uri;
    .locals 1
    .param p0    # Lcom/facebook/graphql/model/GraphQLEntity;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1751575
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEntity;->s()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEntity;->s()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1751576
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLEntity;->s()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1751577
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x29d89b05

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1751569
    iget-object v1, p0, LX/B9O;->b:Lcom/facebook/graphql/model/GraphQLMegaphone;

    if-nez v1, :cond_0

    .line 1751570
    const/16 v1, 0x8

    invoke-virtual {p0, v1}, LX/B9O;->setVisibility(I)V

    .line 1751571
    :cond_0
    invoke-super {p0}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->onAttachedToWindow()V

    .line 1751572
    iget-object v1, p0, LX/B9O;->b:Lcom/facebook/graphql/model/GraphQLMegaphone;

    if-eqz v1, :cond_1

    .line 1751573
    iget-object v1, p0, LX/B9O;->a:LX/B9T;

    iget-object v2, p0, LX/B9O;->b:Lcom/facebook/graphql/model/GraphQLMegaphone;

    invoke-virtual {v1, v2}, LX/B9T;->d(Lcom/facebook/graphql/model/GraphQLMegaphone;)V

    .line 1751574
    :cond_1
    const/16 v1, 0x2d

    const v2, 0x3c58e0ae

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onMeasure(II)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1751578
    invoke-virtual {p0}, LX/B9O;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    .line 1751579
    invoke-virtual {p0, v2, v2}, LX/B9O;->setMeasuredDimension(II)V

    .line 1751580
    :goto_0
    return-void

    .line 1751581
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->onMeasure(II)V

    goto :goto_0
.end method

.method public setMegaphoneStory(LX/3TD;)V
    .locals 1

    .prologue
    .line 1751565
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/B9O;->setMegaphoneStory(Lcom/facebook/graphql/model/GraphQLMegaphone;)V

    .line 1751566
    return-void

    .line 1751567
    :cond_0
    iget-object v0, p1, LX/3TD;->b:Lcom/facebook/graphql/model/GraphQLMegaphone;

    move-object v0, v0

    .line 1751568
    goto :goto_0
.end method

.method public setMegaphoneStory(Lcom/facebook/graphql/model/GraphQLMegaphone;)V
    .locals 10

    .prologue
    .line 1751509
    iput-object p1, p0, LX/B9O;->b:Lcom/facebook/graphql/model/GraphQLMegaphone;

    .line 1751510
    iget-object v0, p0, LX/B9O;->b:Lcom/facebook/graphql/model/GraphQLMegaphone;

    if-eqz v0, :cond_3

    .line 1751511
    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1751512
    iget-object v0, p0, LX/B9O;->b:Lcom/facebook/graphql/model/GraphQLMegaphone;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMegaphone;->m()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_4

    .line 1751513
    invoke-virtual {p0, v3}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setShowCloseButton(Z)V

    .line 1751514
    invoke-virtual {p0, v2}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setShowSecondaryButton(Z)V

    .line 1751515
    new-instance v0, LX/B9K;

    invoke-direct {v0, p0}, LX/B9K;-><init>(LX/B9O;)V

    .line 1751516
    iput-object v0, p0, Lcom/facebook/fbui/widget/megaphone/Megaphone;->m:LX/AhV;

    .line 1751517
    :goto_0
    iget-object v0, p0, LX/B9O;->b:Lcom/facebook/graphql/model/GraphQLMegaphone;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMegaphone;->o()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_5

    iget-object v0, p0, LX/B9O;->b:Lcom/facebook/graphql/model/GraphQLMegaphone;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMegaphone;->o()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 1751518
    iget-object v0, p0, LX/B9O;->b:Lcom/facebook/graphql/model/GraphQLMegaphone;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMegaphone;->o()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setImageUri(Landroid/net/Uri;)V

    .line 1751519
    :goto_1
    iget-object v0, p0, LX/B9O;->b:Lcom/facebook/graphql/model/GraphQLMegaphone;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMegaphone;->k()Lcom/facebook/graphql/model/GraphQLMegaphoneAction;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 1751520
    invoke-virtual {p0, v3}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setShowPrimaryButton(Z)V

    .line 1751521
    iget-object v0, p0, LX/B9O;->b:Lcom/facebook/graphql/model/GraphQLMegaphone;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMegaphone;->k()Lcom/facebook/graphql/model/GraphQLMegaphoneAction;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMegaphoneAction;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setPrimaryButtonText(Ljava/lang/CharSequence;)V

    .line 1751522
    new-instance v0, LX/B9M;

    invoke-direct {v0, p0}, LX/B9M;-><init>(LX/B9O;)V

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setOnPrimaryButtonClickListener(Landroid/view/View$OnClickListener;)V

    .line 1751523
    :goto_2
    iget-object v0, p0, LX/B9O;->b:Lcom/facebook/graphql/model/GraphQLMegaphone;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMegaphone;->t()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setTitle(Ljava/lang/CharSequence;)V

    .line 1751524
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setTitleMaxLines(I)V

    .line 1751525
    iget-object v0, p0, LX/B9O;->b:Lcom/facebook/graphql/model/GraphQLMegaphone;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMegaphone;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_7

    iget-object v0, p0, LX/B9O;->b:Lcom/facebook/graphql/model/GraphQLMegaphone;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMegaphone;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    :goto_3
    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setSubtitle(Ljava/lang/CharSequence;)V

    .line 1751526
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setSubtitleMaxLines(I)V

    .line 1751527
    iget-object v0, p0, LX/B9O;->b:Lcom/facebook/graphql/model/GraphQLMegaphone;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMegaphone;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 1751528
    iget-object v0, p0, LX/B9O;->b:Lcom/facebook/graphql/model/GraphQLMegaphone;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMegaphone;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setSocialContext(Ljava/lang/CharSequence;)V

    .line 1751529
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setSocialContextMaxLines(I)V

    .line 1751530
    const/4 p1, 0x3

    const/4 v3, 0x0

    .line 1751531
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 1751532
    iget-object v0, p0, LX/B9O;->b:Lcom/facebook/graphql/model/GraphQLMegaphone;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMegaphone;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    .line 1751533
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->b()LX/0Px;

    move-result-object v6

    .line 1751534
    if-eqz v6, :cond_1

    move v1, v3

    .line 1751535
    :goto_4
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-ge v0, p1, :cond_1

    .line 1751536
    invoke-interface {v6, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEntityAtRange;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEntityAtRange;->j()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v0

    invoke-static {v0}, LX/B9O;->a(Lcom/facebook/graphql/model/GraphQLEntity;)Landroid/net/Uri;

    move-result-object v0

    .line 1751537
    if-eqz v0, :cond_0

    .line 1751538
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1751539
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 1751540
    :cond_1
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->c()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-lt v0, p1, :cond_9

    :cond_2
    move-object v0, v4

    .line 1751541
    :goto_5
    move-object v0, v0

    .line 1751542
    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setFacepileUrls(Ljava/util/List;)V

    .line 1751543
    :goto_6
    invoke-virtual {p0, v2}, LX/B9O;->setVisibility(I)V

    .line 1751544
    :goto_7
    return-void

    .line 1751545
    :cond_3
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, LX/B9O;->setVisibility(I)V

    goto :goto_7

    .line 1751546
    :cond_4
    invoke-virtual {p0, v2}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setShowCloseButton(Z)V

    .line 1751547
    invoke-virtual {p0, v3}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setShowSecondaryButton(Z)V

    .line 1751548
    iget-object v0, p0, LX/B9O;->b:Lcom/facebook/graphql/model/GraphQLMegaphone;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMegaphone;->m()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setSecondaryButtonText(Ljava/lang/CharSequence;)V

    .line 1751549
    new-instance v0, LX/B9L;

    invoke-direct {v0, p0}, LX/B9L;-><init>(LX/B9O;)V

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setOnSecondaryButtonClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    .line 1751550
    :cond_5
    invoke-virtual {p0, v1}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setImageView(Landroid/view/View;)V

    goto/16 :goto_1

    .line 1751551
    :cond_6
    invoke-virtual {p0, v2}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setShowPrimaryButton(Z)V

    goto/16 :goto_2

    :cond_7
    move-object v0, v1

    .line 1751552
    goto/16 :goto_3

    .line 1751553
    :cond_8
    invoke-virtual {p0, v1}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setSocialContext(Ljava/lang/CharSequence;)V

    goto :goto_6

    .line 1751554
    :cond_9
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->c()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    move v5, v3

    :goto_8
    if-ge v5, v7, :cond_d

    invoke-virtual {v6, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAggregatedEntitiesAtRange;

    .line 1751555
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAggregatedEntitiesAtRange;->j()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_c

    .line 1751556
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLAggregatedEntitiesAtRange;->j()LX/0Px;

    move-result-object v8

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v9

    move v1, v3

    :goto_9
    if-ge v1, v9, :cond_c

    invoke-virtual {v8, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEntity;

    .line 1751557
    invoke-static {v0}, LX/B9O;->a(Lcom/facebook/graphql/model/GraphQLEntity;)Landroid/net/Uri;

    move-result-object v0

    .line 1751558
    if-eqz v0, :cond_a

    .line 1751559
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1751560
    :cond_a
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-lt v0, p1, :cond_b

    move-object v0, v4

    .line 1751561
    goto :goto_5

    .line 1751562
    :cond_b
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_9

    .line 1751563
    :cond_c
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_8

    :cond_d
    move-object v0, v4

    .line 1751564
    goto :goto_5
.end method
