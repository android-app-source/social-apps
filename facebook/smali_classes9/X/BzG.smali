.class public LX/BzG;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Po;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/BzH;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/BzG",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/BzH;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1838477
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1838478
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/BzG;->b:LX/0Zi;

    .line 1838479
    iput-object p1, p0, LX/BzG;->a:LX/0Ot;

    .line 1838480
    return-void
.end method

.method public static a(LX/0QB;)LX/BzG;
    .locals 4

    .prologue
    .line 1838466
    const-class v1, LX/BzG;

    monitor-enter v1

    .line 1838467
    :try_start_0
    sget-object v0, LX/BzG;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1838468
    sput-object v2, LX/BzG;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1838469
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1838470
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1838471
    new-instance v3, LX/BzG;

    const/16 p0, 0x1e42

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/BzG;-><init>(LX/0Ot;)V

    .line 1838472
    move-object v0, v3

    .line 1838473
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1838474
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/BzG;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1838475
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1838476
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 9

    .prologue
    .line 1838441
    check-cast p2, LX/BzF;

    .line 1838442
    iget-object v0, p0, LX/BzG;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BzH;

    iget-object v1, p2, LX/BzF;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v2, p2, LX/BzF;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p2, LX/BzF;->c:LX/1Pq;

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 1838443
    iget-object v4, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, v4

    .line 1838444
    check-cast v4, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1838445
    iget-object v5, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v5, v5

    .line 1838446
    check-cast v5, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1838447
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v8

    move-object v5, v3

    .line 1838448
    check-cast v5, LX/1Po;

    invoke-interface {v5}, LX/1Po;->c()LX/1PT;

    move-result-object v5

    invoke-interface {v5}, LX/1PT;->a()LX/1Qt;

    move-result-object v5

    sget-object p0, LX/1Qt;->PERMALINK:LX/1Qt;

    if-eq v5, p0, :cond_0

    move v5, v6

    .line 1838449
    :goto_0
    invoke-static {v1}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object p0

    invoke-static {p0}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object p0

    .line 1838450
    iput-object v8, p0, LX/23u;->ad:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1838451
    move-object v8, p0

    .line 1838452
    invoke-virtual {v8}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v8

    .line 1838453
    invoke-static {v1}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object p0

    invoke-virtual {p0, v8}, Lcom/facebook/feed/rows/core/props/FeedProps;->b(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v8

    .line 1838454
    iget-object p0, v0, LX/BzH;->a:LX/Bz8;

    .line 1838455
    new-instance v2, LX/Bz7;

    invoke-static {p0}, LX/1nA;->a(LX/0QB;)LX/1nA;

    move-result-object p2

    check-cast p2, LX/1nA;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v1

    check-cast v1, LX/03V;

    invoke-direct {v2, p2, v1, v8, v5}, LX/Bz7;-><init>(LX/1nA;LX/03V;Lcom/facebook/feed/rows/core/props/FeedProps;Z)V

    .line 1838456
    move-object v8, v2

    .line 1838457
    iget-object p0, v0, LX/BzH;->b:LX/1xu;

    move-object v5, v3

    check-cast v5, LX/1Pr;

    invoke-virtual {p0, v8, v5}, LX/1xu;->a(LX/1xz;LX/1Pr;)V

    .line 1838458
    check-cast v3, LX/1Pr;

    invoke-interface {v8}, LX/1xz;->a()LX/1KL;

    move-result-object v5

    invoke-interface {v3, v5, v4}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/1yT;

    .line 1838459
    iget-object v5, v4, LX/1yT;->a:Landroid/text/Spannable;

    invoke-interface {v5}, Landroid/text/Spannable;->length()I

    move-result v5

    const/16 v8, 0x64

    if-lt v5, v8, :cond_1

    sget-object v5, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    .line 1838460
    :goto_1
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v8

    invoke-virtual {v8, v7}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v7

    iget-object v4, v4, LX/1yT;->a:Landroid/text/Spannable;

    invoke-virtual {v7, v4}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v4

    const v7, 0x7f0b0050

    invoke-virtual {v4, v7}, LX/1ne;->q(I)LX/1ne;

    move-result-object v4

    const/high16 v7, -0x1000000

    invoke-virtual {v4, v7}, LX/1ne;->m(I)LX/1ne;

    move-result-object v4

    sget-object v7, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {v4, v7}, LX/1ne;->a(Landroid/graphics/Typeface;)LX/1ne;

    move-result-object v4

    invoke-virtual {v4, v5}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v4

    invoke-virtual {v4, v6}, LX/1ne;->d(Z)LX/1ne;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    move-object v0, v4

    .line 1838461
    return-object v0

    :cond_0
    move v5, v7

    .line 1838462
    goto :goto_0

    .line 1838463
    :cond_1
    sget-object v5, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    goto :goto_1
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1838464
    invoke-static {}, LX/1dS;->b()V

    .line 1838465
    const/4 v0, 0x0

    return-object v0
.end method
