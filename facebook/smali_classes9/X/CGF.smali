.class public final LX/CGF;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/greetingcards/verve/render/VerveContentView;


# direct methods
.method public constructor <init>(Lcom/facebook/greetingcards/verve/render/VerveContentView;)V
    .locals 0

    .prologue
    .line 1865108
    iput-object p1, p0, LX/CGF;->a:Lcom/facebook/greetingcards/verve/render/VerveContentView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/31M;)I
    .locals 3

    .prologue
    .line 1865109
    iget-object v0, p0, LX/CGF;->a:Lcom/facebook/greetingcards/verve/render/VerveContentView;

    iget-object v1, p0, LX/CGF;->a:Lcom/facebook/greetingcards/verve/render/VerveContentView;

    iget-object v1, v1, Lcom/facebook/greetingcards/verve/render/VerveContentView;->c:LX/CFp;

    .line 1865110
    iget-object v2, v1, LX/CFp;->a:Lcom/facebook/greetingcards/verve/model/VMDeck;

    move-object v1, v2

    .line 1865111
    iget-object v2, p0, LX/CGF;->a:Lcom/facebook/greetingcards/verve/render/VerveContentView;

    iget v2, v2, Lcom/facebook/greetingcards/verve/render/VerveContentView;->d:I

    invoke-static {v1, v2, p1}, LX/CGA;->a(Lcom/facebook/greetingcards/verve/model/VMDeck;ILX/31M;)I

    move-result v1

    .line 1865112
    iput v1, v0, Lcom/facebook/greetingcards/verve/render/VerveContentView;->e:I

    .line 1865113
    iget-object v0, p0, LX/CGF;->a:Lcom/facebook/greetingcards/verve/render/VerveContentView;

    iget-object v1, p0, LX/CGF;->a:Lcom/facebook/greetingcards/verve/render/VerveContentView;

    iget v1, v1, Lcom/facebook/greetingcards/verve/render/VerveContentView;->e:I

    invoke-static {v0, v1}, Lcom/facebook/greetingcards/verve/render/VerveContentView;->a(Lcom/facebook/greetingcards/verve/render/VerveContentView;I)V

    .line 1865114
    iget-object v0, p0, LX/CGF;->a:Lcom/facebook/greetingcards/verve/render/VerveContentView;

    iget-object v0, v0, Lcom/facebook/greetingcards/verve/render/VerveContentView;->c:LX/CFp;

    .line 1865115
    iget-object v1, v0, LX/CFp;->a:Lcom/facebook/greetingcards/verve/model/VMDeck;

    move-object v0, v1

    .line 1865116
    iget-object v1, p0, LX/CGF;->a:Lcom/facebook/greetingcards/verve/render/VerveContentView;

    iget v1, v1, Lcom/facebook/greetingcards/verve/render/VerveContentView;->e:I

    invoke-static {v0, v1}, LX/CGA;->a(Lcom/facebook/greetingcards/verve/model/VMDeck;I)I

    move-result v0

    return v0
.end method

.method public final a(D)V
    .locals 1

    .prologue
    .line 1865117
    iget-object v0, p0, LX/CGF;->a:Lcom/facebook/greetingcards/verve/render/VerveContentView;

    iget-object v0, v0, Lcom/facebook/greetingcards/verve/render/VerveContentView;->h:LX/CGC;

    invoke-virtual {v0, p1, p2}, LX/CGC;->a(D)V

    .line 1865118
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 1865119
    if-eqz p1, :cond_0

    .line 1865120
    iget-object v0, p0, LX/CGF;->a:Lcom/facebook/greetingcards/verve/render/VerveContentView;

    iget-object v1, p0, LX/CGF;->a:Lcom/facebook/greetingcards/verve/render/VerveContentView;

    iget v1, v1, Lcom/facebook/greetingcards/verve/render/VerveContentView;->e:I

    invoke-static {v0, v1}, Lcom/facebook/greetingcards/verve/render/VerveContentView;->e(Lcom/facebook/greetingcards/verve/render/VerveContentView;I)V

    .line 1865121
    :goto_0
    return-void

    .line 1865122
    :cond_0
    iget-object v0, p0, LX/CGF;->a:Lcom/facebook/greetingcards/verve/render/VerveContentView;

    iget-object v1, p0, LX/CGF;->a:Lcom/facebook/greetingcards/verve/render/VerveContentView;

    iget v1, v1, Lcom/facebook/greetingcards/verve/render/VerveContentView;->d:I

    invoke-static {v0, v1}, Lcom/facebook/greetingcards/verve/render/VerveContentView;->e(Lcom/facebook/greetingcards/verve/render/VerveContentView;I)V

    goto :goto_0
.end method
