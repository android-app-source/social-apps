.class public final LX/Ap4;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/Ap5;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/Ap5",
            "<TE;>.FigAttachmentComponentImpl;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/Ap5;

.field private c:[Ljava/lang/String;

.field private d:I

.field private e:Ljava/util/BitSet;


# direct methods
.method public constructor <init>(LX/Ap5;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1715251
    iput-object p1, p0, LX/Ap4;->b:LX/Ap5;

    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 1715252
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "type"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/Ap4;->c:[Ljava/lang/String;

    .line 1715253
    iput v3, p0, LX/Ap4;->d:I

    .line 1715254
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/Ap4;->d:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/Ap4;->e:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/Ap4;LX/1De;IILcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "II",
            "LX/Ap5",
            "<TE;>.FigAttachmentComponentImpl;)V"
        }
    .end annotation

    .prologue
    .line 1715241
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 1715242
    iput-object p4, p0, LX/Ap4;->a:Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;

    .line 1715243
    iget-object v0, p0, LX/Ap4;->e:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 1715244
    return-void
.end method


# virtual methods
.method public final a(LX/1Po;)LX/Ap4;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)",
            "LX/Ap5",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 1715245
    iget-object v0, p0, LX/Ap4;->a:Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;

    iput-object p1, v0, Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;->e:LX/1Po;

    .line 1715246
    return-object p0
.end method

.method public final a(LX/1X1;)LX/Ap4;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1",
            "<",
            "LX/ApL;",
            ">;)",
            "LX/Ap5",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 1715249
    iget-object v0, p0, LX/Ap4;->a:Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;

    iput-object p1, v0, Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;->k:LX/1X1;

    .line 1715250
    return-object p0
.end method

.method public final a(LX/1dQ;)LX/Ap4;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;)",
            "LX/Ap5",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 1715247
    iget-object v0, p0, LX/Ap4;->a:Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;

    iput-object p1, v0, Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;->j:LX/1dQ;

    .line 1715248
    return-object p0
.end method

.method public final a(Landroid/net/Uri;)LX/Ap4;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            ")",
            "LX/Ap5",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 1715237
    iget-object v0, p0, LX/Ap4;->a:Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;

    iput-object p1, v0, Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;->b:Landroid/net/Uri;

    .line 1715238
    return-object p0
.end method

.method public final a(Ljava/lang/CharSequence;)LX/Ap4;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            ")",
            "LX/Ap5",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 1715239
    iget-object v0, p0, LX/Ap4;->a:Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;

    iput-object p1, v0, Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;->g:Ljava/lang/CharSequence;

    .line 1715240
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 1715212
    invoke-super {p0}, LX/1X5;->a()V

    .line 1715213
    const/4 v0, 0x0

    iput-object v0, p0, LX/Ap4;->a:Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;

    .line 1715214
    iget-object v0, p0, LX/Ap4;->b:LX/Ap5;

    iget-object v0, v0, LX/Ap5;->b:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 1715215
    return-void
.end method

.method public final b(LX/1X1;)LX/Ap4;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1",
            "<",
            "LX/ApI;",
            ">;)",
            "LX/Ap5",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 1715216
    iget-object v0, p0, LX/Ap4;->a:Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;

    iput-object p1, v0, Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;->l:LX/1X1;

    .line 1715217
    return-object p0
.end method

.method public final b(Ljava/lang/CharSequence;)LX/Ap4;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            ")",
            "LX/Ap5",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 1715235
    iget-object v0, p0, LX/Ap4;->a:Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;

    iput-object p1, v0, Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;->h:Ljava/lang/CharSequence;

    .line 1715236
    return-object p0
.end method

.method public final c(Ljava/lang/CharSequence;)LX/Ap4;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            ")",
            "LX/Ap5",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 1715218
    iget-object v0, p0, LX/Ap4;->a:Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;

    iput-object p1, v0, Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;->i:Ljava/lang/CharSequence;

    .line 1715219
    return-object p0
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/Ap5;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1715220
    iget-object v1, p0, LX/Ap4;->e:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/Ap4;->e:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/Ap4;->d:I

    if-ge v1, v2, :cond_2

    .line 1715221
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1715222
    :goto_0
    iget v2, p0, LX/Ap4;->d:I

    if-ge v0, v2, :cond_1

    .line 1715223
    iget-object v2, p0, LX/Ap4;->e:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1715224
    iget-object v2, p0, LX/Ap4;->c:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1715225
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1715226
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1715227
    :cond_2
    iget-object v0, p0, LX/Ap4;->a:Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;

    .line 1715228
    invoke-virtual {p0}, LX/Ap4;->a()V

    .line 1715229
    return-object v0
.end method

.method public final h(I)LX/Ap4;
    .locals 2
    .param p1    # I
        .annotation build Lcom/facebook/fig/components/attachment/annotations/FigAttachmentType;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/Ap5",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 1715230
    iget-object v0, p0, LX/Ap4;->a:Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;

    iput p1, v0, Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;->a:I

    .line 1715231
    iget-object v0, p0, LX/Ap4;->e:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1715232
    return-object p0
.end method

.method public final i(I)LX/Ap4;
    .locals 1
    .param p1    # I
        .annotation build Lcom/facebook/fig/components/attachment/annotations/FigCoverPhotoIconType;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "LX/Ap5",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 1715233
    iget-object v0, p0, LX/Ap4;->a:Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;

    iput p1, v0, Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;->d:I

    .line 1715234
    return-object p0
.end method
