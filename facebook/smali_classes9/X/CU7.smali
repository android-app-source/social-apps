.class public final synthetic LX/CU7;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final synthetic a:[I

.field public static final synthetic b:[I


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1896248
    invoke-static {}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;->values()[Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/CU7;->b:[I

    :try_start_0
    sget-object v0, LX/CU7;->b:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;->CONFIRMATION:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_4

    :goto_0
    :try_start_1
    sget-object v0, LX/CU7;->b:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;->ERROR:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_3

    .line 1896249
    :goto_1
    invoke-static {}, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->values()[Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/CU7;->a:[I

    :try_start_2
    sget-object v0, LX/CU7;->a:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->CONFIRMATION:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :goto_2
    :try_start_3
    sget-object v0, LX/CU7;->a:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->ERROR:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_1

    :goto_3
    :try_start_4
    sget-object v0, LX/CU7;->a:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->CONTAINER:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_0

    :goto_4
    return-void

    :catch_0
    goto :goto_4

    :catch_1
    goto :goto_3

    :catch_2
    goto :goto_2

    :catch_3
    goto :goto_1

    :catch_4
    goto :goto_0
.end method
