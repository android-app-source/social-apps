.class public final LX/CWg;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/CSY;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/CT7;

.field public final synthetic d:LX/CTK;

.field public final synthetic e:Lcom/facebook/payments/ui/FloatingLabelTextView;

.field public final synthetic f:Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldPaymentView;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldPaymentView;LX/CSY;Ljava/lang/String;LX/CT7;LX/CTK;Lcom/facebook/payments/ui/FloatingLabelTextView;)V
    .locals 0

    .prologue
    .line 1908582
    iput-object p1, p0, LX/CWg;->f:Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldPaymentView;

    iput-object p2, p0, LX/CWg;->a:LX/CSY;

    iput-object p3, p0, LX/CWg;->b:Ljava/lang/String;

    iput-object p4, p0, LX/CWg;->c:LX/CT7;

    iput-object p5, p0, LX/CWg;->d:LX/CTK;

    iput-object p6, p0, LX/CWg;->e:Lcom/facebook/payments/ui/FloatingLabelTextView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(ILandroid/content/Intent;)V
    .locals 5

    .prologue
    .line 1908583
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    if-eqz p2, :cond_0

    .line 1908584
    invoke-static {p2}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldPaymentView;->b(Landroid/content/Intent;)Lcom/facebook/payments/paymentmethods/model/CreditCard;

    move-result-object v0

    .line 1908585
    if-eqz v0, :cond_0

    .line 1908586
    iget-object v1, p0, LX/CWg;->a:LX/CSY;

    iget-object v2, p0, LX/CWg;->b:Ljava/lang/String;

    .line 1908587
    iget-object v3, v1, LX/CSY;->c:Ljava/util/HashMap;

    new-instance v4, LX/CSZ;

    invoke-direct {v4, v0}, LX/CSZ;-><init>(Lcom/facebook/payments/paymentmethods/model/CreditCard;)V

    invoke-virtual {v3, v2, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1908588
    iget-object v1, p0, LX/CWg;->c:LX/CT7;

    iget-object v2, p0, LX/CWg;->d:LX/CTK;

    iget-object v2, v2, LX/CTJ;->o:Ljava/lang/String;

    iget-object v3, p0, LX/CWg;->d:LX/CTK;

    iget-object v3, v3, LX/CTK;->e:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    iget-object v4, p0, LX/CWg;->a:LX/CSY;

    invoke-virtual {v1, v2, v3, v4}, LX/CT7;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;LX/CSY;)V

    .line 1908589
    iget-object v1, p0, LX/CWg;->e:Lcom/facebook/payments/ui/FloatingLabelTextView;

    iget-object v2, p0, LX/CWg;->e:Lcom/facebook/payments/ui/FloatingLabelTextView;

    invoke-virtual {v2}, Lcom/facebook/payments/ui/FloatingLabelTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/payments/paymentmethods/model/CreditCard;->a(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/payments/ui/FloatingLabelTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1908590
    :cond_0
    return-void
.end method
