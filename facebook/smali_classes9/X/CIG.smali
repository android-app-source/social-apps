.class public final LX/CIG;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 1873207
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1873208
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 1873209
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 1873210
    invoke-static {p0, p1}, LX/CIG;->b(LX/15w;LX/186;)I

    move-result v1

    .line 1873211
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1873212
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1873213
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1873214
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1873215
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    invoke-static {p0, v1, p2, p3}, LX/CIG;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1873216
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1873217
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1873218
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 16

    .prologue
    .line 1873219
    const/4 v12, 0x0

    .line 1873220
    const/4 v11, 0x0

    .line 1873221
    const/4 v10, 0x0

    .line 1873222
    const/4 v9, 0x0

    .line 1873223
    const/4 v8, 0x0

    .line 1873224
    const/4 v7, 0x0

    .line 1873225
    const/4 v6, 0x0

    .line 1873226
    const/4 v5, 0x0

    .line 1873227
    const/4 v4, 0x0

    .line 1873228
    const/4 v3, 0x0

    .line 1873229
    const/4 v2, 0x0

    .line 1873230
    const/4 v1, 0x0

    .line 1873231
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->START_OBJECT:LX/15z;

    if-eq v13, v14, :cond_1

    .line 1873232
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1873233
    const/4 v1, 0x0

    .line 1873234
    :goto_0
    return v1

    .line 1873235
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1873236
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->END_OBJECT:LX/15z;

    if-eq v13, v14, :cond_c

    .line 1873237
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v13

    .line 1873238
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1873239
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v14, v15, :cond_1

    if-eqz v13, :cond_1

    .line 1873240
    const-string v14, "action"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_2

    .line 1873241
    invoke-static/range {p0 .. p1}, LX/CHy;->a(LX/15w;LX/186;)I

    move-result v12

    goto :goto_1

    .line 1873242
    :cond_2
    const-string v14, "annotations"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_3

    .line 1873243
    invoke-static/range {p0 .. p1}, LX/CI0;->a(LX/15w;LX/186;)I

    move-result v11

    goto :goto_1

    .line 1873244
    :cond_3
    const-string v14, "document_element_type"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_4

    .line 1873245
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v10

    goto :goto_1

    .line 1873246
    :cond_4
    const-string v14, "element_descriptor"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_5

    .line 1873247
    invoke-static/range {p0 .. p1}, LX/CI9;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 1873248
    :cond_5
    const-string v14, "element_photo"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_6

    .line 1873249
    invoke-static/range {p0 .. p1}, LX/CIF;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 1873250
    :cond_6
    const-string v14, "grid_width_percent"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_7

    .line 1873251
    const/4 v1, 0x1

    .line 1873252
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v7

    goto :goto_1

    .line 1873253
    :cond_7
    const-string v14, "image"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_8

    .line 1873254
    invoke-static/range {p0 .. p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 1873255
    :cond_8
    const-string v14, "logging_token"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_9

    .line 1873256
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto/16 :goto_1

    .line 1873257
    :cond_9
    const-string v14, "style_list"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_a

    .line 1873258
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v4

    goto/16 :goto_1

    .line 1873259
    :cond_a
    const-string v14, "target_uri"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_b

    .line 1873260
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto/16 :goto_1

    .line 1873261
    :cond_b
    const-string v14, "touch_targets"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 1873262
    invoke-static/range {p0 .. p1}, LX/CIM;->a(LX/15w;LX/186;)I

    move-result v2

    goto/16 :goto_1

    .line 1873263
    :cond_c
    const/16 v13, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->c(I)V

    .line 1873264
    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v12}, LX/186;->b(II)V

    .line 1873265
    const/4 v12, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v11}, LX/186;->b(II)V

    .line 1873266
    const/4 v11, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v10}, LX/186;->b(II)V

    .line 1873267
    const/4 v10, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v9}, LX/186;->b(II)V

    .line 1873268
    const/4 v9, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v9, v8}, LX/186;->b(II)V

    .line 1873269
    if-eqz v1, :cond_d

    .line 1873270
    const/4 v1, 0x5

    const/4 v8, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v7, v8}, LX/186;->a(III)V

    .line 1873271
    :cond_d
    const/4 v1, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 1873272
    const/4 v1, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 1873273
    const/16 v1, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 1873274
    const/16 v1, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 1873275
    const/16 v1, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1873276
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x2

    const/4 v2, 0x0

    .line 1873277
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1873278
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1873279
    if-eqz v0, :cond_0

    .line 1873280
    const-string v1, "action"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1873281
    invoke-static {p0, v0, p2, p3}, LX/CHy;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1873282
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1873283
    if-eqz v0, :cond_1

    .line 1873284
    const-string v1, "annotations"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1873285
    invoke-static {p0, v0, p2, p3}, LX/CI0;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1873286
    :cond_1
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 1873287
    if-eqz v0, :cond_2

    .line 1873288
    const-string v0, "document_element_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1873289
    invoke-virtual {p0, p1, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1873290
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1873291
    if-eqz v0, :cond_3

    .line 1873292
    const-string v1, "element_descriptor"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1873293
    invoke-static {p0, v0, p2, p3}, LX/CI9;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1873294
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1873295
    if-eqz v0, :cond_4

    .line 1873296
    const-string v1, "element_photo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1873297
    invoke-static {p0, v0, p2}, LX/CIF;->a(LX/15i;ILX/0nX;)V

    .line 1873298
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1873299
    if-eqz v0, :cond_5

    .line 1873300
    const-string v1, "grid_width_percent"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1873301
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1873302
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1873303
    if-eqz v0, :cond_6

    .line 1873304
    const-string v1, "image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1873305
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 1873306
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1873307
    if-eqz v0, :cond_7

    .line 1873308
    const-string v1, "logging_token"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1873309
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1873310
    :cond_7
    invoke-virtual {p0, p1, v4}, LX/15i;->g(II)I

    move-result v0

    .line 1873311
    if-eqz v0, :cond_8

    .line 1873312
    const-string v0, "style_list"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1873313
    invoke-virtual {p0, p1, v4}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 1873314
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1873315
    if-eqz v0, :cond_9

    .line 1873316
    const-string v1, "target_uri"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1873317
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1873318
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1873319
    if-eqz v0, :cond_a

    .line 1873320
    const-string v1, "touch_targets"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1873321
    invoke-static {p0, v0, p2, p3}, LX/CIM;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1873322
    :cond_a
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1873323
    return-void
.end method
