.class public final LX/CY6;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:J

.field public b:Ljava/lang/String;

.field public c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

.field public e:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

.field private f:LX/CYE;

.field public g:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private h:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1910793
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(J)LX/CY6;
    .locals 1

    .prologue
    .line 1910791
    iput-wide p1, p0, LX/CY6;->a:J

    .line 1910792
    return-object p0
.end method

.method public final a(LX/0Px;)LX/CY6;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/CY6;"
        }
    .end annotation

    .prologue
    .line 1910789
    iput-object p1, p0, LX/CY6;->c:LX/0Px;

    .line 1910790
    return-object p0
.end method

.method public final a(LX/CYE;)LX/CY6;
    .locals 0

    .prologue
    .line 1910794
    iput-object p1, p0, LX/CY6;->f:LX/CYE;

    .line 1910795
    return-object p0
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;)LX/CY6;
    .locals 0

    .prologue
    .line 1910787
    iput-object p1, p0, LX/CY6;->e:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    .line 1910788
    return-object p0
.end method

.method public final a(Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;)LX/CY6;
    .locals 0

    .prologue
    .line 1910785
    iput-object p1, p0, LX/CY6;->h:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    .line 1910786
    return-object p0
.end method

.method public final a(Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;)LX/CY6;
    .locals 0

    .prologue
    .line 1910783
    iput-object p1, p0, LX/CY6;->d:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    .line 1910784
    return-object p0
.end method

.method public final a(Ljava/lang/String;)LX/CY6;
    .locals 0

    .prologue
    .line 1910781
    iput-object p1, p0, LX/CY6;->b:Ljava/lang/String;

    .line 1910782
    return-object p0
.end method

.method public final a()LX/CY7;
    .locals 12

    .prologue
    .line 1910780
    new-instance v1, LX/CY7;

    iget-wide v2, p0, LX/CY6;->a:J

    iget-object v4, p0, LX/CY6;->b:Ljava/lang/String;

    iget-object v5, p0, LX/CY6;->c:LX/0Px;

    iget-object v6, p0, LX/CY6;->d:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    iget-object v7, p0, LX/CY6;->e:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    iget-object v8, p0, LX/CY6;->f:LX/CYE;

    iget-object v9, p0, LX/CY6;->g:LX/0P1;

    iget-object v10, p0, LX/CY6;->h:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    const/4 v11, 0x0

    invoke-direct/range {v1 .. v11}, LX/CY7;-><init>(JLjava/lang/String;LX/0Px;Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;LX/CYE;LX/0P1;Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;B)V

    return-object v1
.end method
