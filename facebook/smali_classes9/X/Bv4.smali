.class public LX/Bv4;
.super LX/7N3;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/7Lf;",
        ">",
        "LX/7N3",
        "<TE;>;"
    }
.end annotation


# instance fields
.field public o:LX/1b4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final p:Lcom/facebook/feed/video/fullscreen/LiveContextClickToPlayNoPausePlugin;

.field private final q:Landroid/view/ViewStub;

.field public final r:Landroid/view/View;

.field private s:Z

.field public t:Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1831959
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/Bv4;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1831960
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1831957
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/Bv4;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1831958
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 1831946
    invoke-direct {p0, p1, p2, p3}, LX/7N3;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1831947
    const-class v0, LX/Bv4;

    invoke-static {v0, p0}, LX/Bv4;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1831948
    const v0, 0x7f0d19b3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/video/fullscreen/LiveContextClickToPlayNoPausePlugin;

    iput-object v0, p0, LX/Bv4;->p:Lcom/facebook/feed/video/fullscreen/LiveContextClickToPlayNoPausePlugin;

    .line 1831949
    const v0, 0x7f0d19b7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, LX/Bv4;->q:Landroid/view/ViewStub;

    .line 1831950
    const v0, 0x7f0d19b6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/Bv4;->r:Landroid/view/View;

    .line 1831951
    iget-object v0, p0, LX/Bv4;->r:Landroid/view/View;

    new-instance v1, LX/Buy;

    invoke-direct {v1, p0}, LX/Buy;-><init>(LX/Bv4;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1831952
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/Bv1;

    invoke-direct {v1, p0}, LX/Bv1;-><init>(LX/Bv4;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1831953
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/Bv2;

    invoke-direct {v1, p0}, LX/Bv2;-><init>(LX/Bv4;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1831954
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/Bv3;

    invoke-direct {v1, p0}, LX/Bv3;-><init>(LX/Bv4;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1831955
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/Bv0;

    invoke-direct {v1, p0}, LX/Bv0;-><init>(LX/Bv4;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1831956
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/Bv4;

    invoke-static {p0}, LX/1b4;->b(LX/0QB;)LX/1b4;

    move-result-object p0

    check-cast p0, LX/1b4;

    iput-object p0, p1, LX/Bv4;->o:LX/1b4;

    return-void
.end method

.method public static e(LX/Bv4;I)V
    .locals 2

    .prologue
    .line 1831940
    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    .line 1831941
    iget-object v0, p0, LX/7MM;->b:LX/7Me;

    sget-object v1, LX/7OE;->CENTER:LX/7OE;

    invoke-virtual {v0, v1}, LX/7Me;->setVideoPluginAlignment(LX/7OE;)V

    .line 1831942
    :goto_0
    iget-object v0, p0, LX/Bv4;->t:Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;

    if-eqz v0, :cond_0

    .line 1831943
    iget-object v0, p0, LX/Bv4;->t:Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;

    invoke-virtual {p0}, LX/Bv4;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    invoke-virtual {v0, v1}, Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;->c(I)V

    .line 1831944
    :cond_0
    return-void

    .line 1831945
    :cond_1
    iget-object v0, p0, LX/7MM;->b:LX/7Me;

    sget-object v1, LX/7OE;->TOP:LX/7OE;

    invoke-virtual {v0, v1}, LX/7Me;->setVideoPluginAlignment(LX/7OE;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/2pa;Z)V
    .locals 5

    .prologue
    .line 1831925
    invoke-super {p0, p1, p2}, LX/7N3;->a(LX/2pa;Z)V

    .line 1831926
    iget-object v0, p1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->i:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    invoke-static {v0}, LX/Abd;->a(Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;)Z

    move-result v0

    iput-boolean v0, p0, LX/Bv4;->s:Z

    .line 1831927
    if-eqz p2, :cond_3

    .line 1831928
    const/16 v0, 0x1770

    .line 1831929
    iput v0, p0, LX/7N3;->s:I

    .line 1831930
    iget-object v0, p0, LX/Bv4;->o:LX/1b4;

    const/4 v1, 0x0

    .line 1831931
    iget-object v2, v0, LX/1b4;->a:LX/0ad;

    sget-short v3, LX/1v6;->o:S

    invoke-interface {v2, v3, v1}, LX/0ad;->a(SZ)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "tap"

    iget-object v3, v0, LX/1b4;->a:LX/0ad;

    sget-char v4, LX/1v6;->p:C

    const/4 p2, 0x0

    invoke-interface {v3, v4, p2}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    move v0, v1

    .line 1831932
    if-eqz v0, :cond_2

    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    if-eqz v0, :cond_2

    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v1, "GraphQLStoryProps"

    invoke-virtual {v0, v1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1831933
    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v1, "GraphQLStoryProps"

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 1831934
    instance-of v0, v1, Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v0, :cond_2

    .line 1831935
    iget-object v0, p0, LX/Bv4;->t:Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;

    if-nez v0, :cond_1

    .line 1831936
    iget-object v0, p0, LX/Bv4;->q:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;

    iput-object v0, p0, LX/Bv4;->t:Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;

    .line 1831937
    :cond_1
    iget-object v2, p0, LX/Bv4;->t:Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;

    move-object v0, v1

    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v0}, Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;->setMetadata(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1831938
    :cond_2
    invoke-virtual {p0}, LX/Bv4;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    invoke-static {p0, v0}, LX/Bv4;->e(LX/Bv4;I)V

    .line 1831939
    :cond_3
    return-void
.end method

.method public final c(I)V
    .locals 3

    .prologue
    .line 1831961
    invoke-super {p0, p1}, LX/7N3;->c(I)V

    .line 1831962
    iget-object v0, p0, LX/Bv4;->t:Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;

    if-eqz v0, :cond_0

    .line 1831963
    iget-object v0, p0, LX/Bv4;->t:Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_2

    .line 1831964
    :cond_0
    :goto_0
    iget-object v0, p0, LX/2oy;->i:LX/2oj;

    if-eqz v0, :cond_1

    .line 1831965
    iget-object v0, p0, LX/2oy;->i:LX/2oj;

    new-instance v1, LX/7M9;

    sget-object v2, LX/7M8;->FADE_OUT:LX/7M8;

    invoke-direct {v1, v2}, LX/7M9;-><init>(LX/7M8;)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    .line 1831966
    :cond_1
    return-void

    .line 1831967
    :cond_2
    iget-object v0, p0, LX/Bv4;->t:Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f040006

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 1831968
    iget-object v1, p0, LX/Bv4;->t:Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;

    invoke-virtual {v1, v0}, Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method public final d(I)V
    .locals 3

    .prologue
    const/16 v1, 0x8

    .line 1831907
    invoke-super {p0, p1}, LX/7N3;->d(I)V

    .line 1831908
    iget-object v0, p0, LX/Bv4;->t:Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;

    if-eqz v0, :cond_1

    .line 1831909
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    .line 1831910
    iget-object v2, v0, LX/2pb;->y:LX/2qV;

    move-object v0, v2

    .line 1831911
    invoke-virtual {v0}, LX/2qV;->isPlayingState()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    .line 1831912
    :goto_0
    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/Bv4;->s:Z

    if-eqz v0, :cond_4

    .line 1831913
    :cond_0
    iget-object v0, p0, LX/Bv4;->t:Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;->a()V

    .line 1831914
    const/4 v1, 0x0

    .line 1831915
    iget-object v0, p0, LX/Bv4;->r:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1831916
    iget-object v0, p0, LX/Bv4;->t:Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;

    invoke-virtual {v0, v1}, Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;->setVisibility(I)V

    .line 1831917
    iget-object v0, p0, LX/Bv4;->t:Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f040004

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 1831918
    iget-object v1, p0, LX/Bv4;->t:Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;

    invoke-virtual {v1, v0}, Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1831919
    :cond_1
    :goto_1
    iget-object v0, p0, LX/2oy;->i:LX/2oj;

    if-eqz v0, :cond_2

    .line 1831920
    iget-object v0, p0, LX/2oy;->i:LX/2oj;

    new-instance v1, LX/7M9;

    sget-object v2, LX/7M8;->FADE_IN:LX/7M8;

    invoke-direct {v1, v2}, LX/7M9;-><init>(LX/7M8;)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    .line 1831921
    :cond_2
    return-void

    .line 1831922
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 1831923
    :cond_4
    iget-object v0, p0, LX/Bv4;->t:Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;

    invoke-virtual {v0, v1}, Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;->setVisibility(I)V

    .line 1831924
    iget-object v0, p0, LX/Bv4;->r:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method

.method public final dH_()V
    .locals 1

    .prologue
    .line 1831904
    invoke-super {p0}, LX/7N3;->dH_()V

    .line 1831905
    iget-object v0, p0, LX/Bv4;->p:Lcom/facebook/feed/video/fullscreen/LiveContextClickToPlayNoPausePlugin;

    invoke-virtual {v0}, Lcom/facebook/feed/video/fullscreen/ClickToPlayNoPausePlugin;->f()V

    .line 1831906
    return-void
.end method

.method public getContentView()I
    .locals 1

    .prologue
    .line 1831903
    const v0, 0x7f030a28

    return v0
.end method

.method public final h()V
    .locals 1

    .prologue
    .line 1831899
    iget-object v0, p0, LX/Bv4;->t:Lcom/facebook/facecastdisplay/LiveVideoContextViewV2;

    if-eqz v0, :cond_0

    .line 1831900
    invoke-virtual {p0}, LX/7N3;->i()V

    .line 1831901
    :goto_0
    return-void

    .line 1831902
    :cond_0
    invoke-super {p0}, LX/7N3;->h()V

    goto :goto_0
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, 0x5ce3efe

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1831898
    const/4 v1, 0x0

    const v2, 0x44d62785

    invoke-static {v3, v3, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return v1
.end method

.method public final u()Z
    .locals 2

    .prologue
    .line 1831895
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    .line 1831896
    iget-object v1, v0, LX/2pb;->y:LX/2qV;

    move-object v0, v1

    .line 1831897
    sget-object v1, LX/2qV;->PAUSED:LX/2qV;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
