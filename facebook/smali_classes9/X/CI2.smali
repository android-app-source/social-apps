.class public final LX/CI2;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 3

    .prologue
    .line 1871675
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1871676
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->START_ARRAY:LX/15z;

    if-ne v1, v2, :cond_0

    .line 1871677
    :goto_0
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v1

    sget-object v2, LX/15z;->END_ARRAY:LX/15z;

    if-eq v1, v2, :cond_0

    .line 1871678
    invoke-static {p0, p1}, LX/CI2;->b(LX/15w;LX/186;)I

    move-result v1

    .line 1871679
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1871680
    :cond_0
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    return v0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1871681
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 1871682
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1}, LX/15i;->c(I)I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1871683
    invoke-virtual {p0, p1, v0}, LX/15i;->q(II)I

    move-result v1

    invoke-static {p0, v1, p2, p3}, LX/CI2;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1871684
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1871685
    :cond_0
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 1871686
    return-void
.end method

.method public static b(LX/15w;LX/186;)I
    .locals 31

    .prologue
    .line 1871687
    const/16 v27, 0x0

    .line 1871688
    const/16 v26, 0x0

    .line 1871689
    const/16 v25, 0x0

    .line 1871690
    const/16 v24, 0x0

    .line 1871691
    const/16 v23, 0x0

    .line 1871692
    const/16 v22, 0x0

    .line 1871693
    const/16 v21, 0x0

    .line 1871694
    const/16 v20, 0x0

    .line 1871695
    const/16 v19, 0x0

    .line 1871696
    const/16 v18, 0x0

    .line 1871697
    const/16 v17, 0x0

    .line 1871698
    const/16 v16, 0x0

    .line 1871699
    const/4 v15, 0x0

    .line 1871700
    const/4 v14, 0x0

    .line 1871701
    const/4 v13, 0x0

    .line 1871702
    const/4 v12, 0x0

    .line 1871703
    const/4 v11, 0x0

    .line 1871704
    const/4 v10, 0x0

    .line 1871705
    const/4 v9, 0x0

    .line 1871706
    const/4 v8, 0x0

    .line 1871707
    const/4 v7, 0x0

    .line 1871708
    const/4 v6, 0x0

    .line 1871709
    const/4 v5, 0x0

    .line 1871710
    const/4 v4, 0x0

    .line 1871711
    const/4 v3, 0x0

    .line 1871712
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v28

    sget-object v29, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v28

    move-object/from16 v1, v29

    if-eq v0, v1, :cond_1

    .line 1871713
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1871714
    const/4 v3, 0x0

    .line 1871715
    :goto_0
    return v3

    .line 1871716
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1871717
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v28

    sget-object v29, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v28

    move-object/from16 v1, v29

    if-eq v0, v1, :cond_19

    .line 1871718
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v28

    .line 1871719
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1871720
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v29

    sget-object v30, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v29

    move-object/from16 v1, v30

    if-eq v0, v1, :cond_1

    if-eqz v28, :cond_1

    .line 1871721
    const-string v29, "__type__"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-nez v29, :cond_2

    const-string v29, "__typename"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_3

    .line 1871722
    :cond_2
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v27

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v27

    goto :goto_1

    .line 1871723
    :cond_3
    const-string v29, "action"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_4

    .line 1871724
    invoke-static/range {p0 .. p1}, LX/CHy;->a(LX/15w;LX/186;)I

    move-result v26

    goto :goto_1

    .line 1871725
    :cond_4
    const-string v29, "annotations"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_5

    .line 1871726
    invoke-static/range {p0 .. p1}, LX/CI0;->a(LX/15w;LX/186;)I

    move-result v25

    goto :goto_1

    .line 1871727
    :cond_5
    const-string v29, "autoplay_style"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_6

    .line 1871728
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v24 .. v24}, Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;

    move-result-object v24

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v24

    goto :goto_1

    .line 1871729
    :cond_6
    const-string v29, "child_elements"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_7

    .line 1871730
    invoke-static/range {p0 .. p1}, LX/CIG;->a(LX/15w;LX/186;)I

    move-result v23

    goto :goto_1

    .line 1871731
    :cond_7
    const-string v29, "color"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_8

    .line 1871732
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v22

    goto/16 :goto_1

    .line 1871733
    :cond_8
    const-string v29, "color_types"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_9

    .line 1871734
    invoke-static/range {p0 .. p1}, LX/CI1;->a(LX/15w;LX/186;)I

    move-result v21

    goto/16 :goto_1

    .line 1871735
    :cond_9
    const-string v29, "content_element"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_a

    .line 1871736
    invoke-static/range {p0 .. p1}, LX/CIK;->a(LX/15w;LX/186;)I

    move-result v20

    goto/16 :goto_1

    .line 1871737
    :cond_a
    const-string v29, "control_style"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_b

    .line 1871738
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v19

    goto/16 :goto_1

    .line 1871739
    :cond_b
    const-string v29, "document_element_type"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_c

    .line 1871740
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v18

    goto/16 :goto_1

    .line 1871741
    :cond_c
    const-string v29, "element_descriptor"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_d

    .line 1871742
    invoke-static/range {p0 .. p1}, LX/CI9;->a(LX/15w;LX/186;)I

    move-result v17

    goto/16 :goto_1

    .line 1871743
    :cond_d
    const-string v29, "element_photo"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_e

    .line 1871744
    invoke-static/range {p0 .. p1}, LX/CIF;->a(LX/15w;LX/186;)I

    move-result v16

    goto/16 :goto_1

    .line 1871745
    :cond_e
    const-string v29, "element_text"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_f

    .line 1871746
    invoke-static/range {p0 .. p1}, LX/8al;->a(LX/15w;LX/186;)I

    move-result v15

    goto/16 :goto_1

    .line 1871747
    :cond_f
    const-string v29, "element_video"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_10

    .line 1871748
    invoke-static/range {p0 .. p1}, LX/8ZV;->a(LX/15w;LX/186;)I

    move-result v14

    goto/16 :goto_1

    .line 1871749
    :cond_10
    const-string v29, "grid_width_percent"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_11

    .line 1871750
    const/4 v4, 0x1

    .line 1871751
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v13

    goto/16 :goto_1

    .line 1871752
    :cond_11
    const-string v29, "image"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_12

    .line 1871753
    invoke-static/range {p0 .. p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v12

    goto/16 :goto_1

    .line 1871754
    :cond_12
    const-string v29, "logging_token"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_13

    .line 1871755
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    goto/16 :goto_1

    .line 1871756
    :cond_13
    const-string v29, "looping_style"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_14

    .line 1871757
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v10

    goto/16 :goto_1

    .line 1871758
    :cond_14
    const-string v29, "section_header"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_15

    .line 1871759
    invoke-static/range {p0 .. p1}, LX/CIK;->a(LX/15w;LX/186;)I

    move-result v9

    goto/16 :goto_1

    .line 1871760
    :cond_15
    const-string v29, "selected_index"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_16

    .line 1871761
    const/4 v3, 0x1

    .line 1871762
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v8

    goto/16 :goto_1

    .line 1871763
    :cond_16
    const-string v29, "style_list"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_17

    .line 1871764
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v7

    goto/16 :goto_1

    .line 1871765
    :cond_17
    const-string v29, "target_uri"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v29

    if-eqz v29, :cond_18

    .line 1871766
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_1

    .line 1871767
    :cond_18
    const-string v29, "touch_targets"

    invoke-virtual/range {v28 .. v29}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_0

    .line 1871768
    invoke-static/range {p0 .. p1}, LX/CIM;->a(LX/15w;LX/186;)I

    move-result v5

    goto/16 :goto_1

    .line 1871769
    :cond_19
    const/16 v28, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 1871770
    const/16 v28, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v28

    move/from16 v2, v27

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1871771
    const/16 v27, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v27

    move/from16 v2, v26

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1871772
    const/16 v26, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v26

    move/from16 v2, v25

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1871773
    const/16 v25, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v25

    move/from16 v2, v24

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1871774
    const/16 v24, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v24

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1871775
    const/16 v23, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v23

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1871776
    const/16 v22, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v22

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1871777
    const/16 v21, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v21

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1871778
    const/16 v20, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1871779
    const/16 v19, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v19

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1871780
    const/16 v18, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1871781
    const/16 v17, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v16

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1871782
    const/16 v16, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v1, v15}, LX/186;->b(II)V

    .line 1871783
    const/16 v15, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v15, v14}, LX/186;->b(II)V

    .line 1871784
    if-eqz v4, :cond_1a

    .line 1871785
    const/16 v4, 0xe

    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v13, v14}, LX/186;->a(III)V

    .line 1871786
    :cond_1a
    const/16 v4, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v12}, LX/186;->b(II)V

    .line 1871787
    const/16 v4, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v11}, LX/186;->b(II)V

    .line 1871788
    const/16 v4, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v10}, LX/186;->b(II)V

    .line 1871789
    const/16 v4, 0x12

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v9}, LX/186;->b(II)V

    .line 1871790
    if-eqz v3, :cond_1b

    .line 1871791
    const/16 v3, 0x13

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v8, v4}, LX/186;->a(III)V

    .line 1871792
    :cond_1b
    const/16 v3, 0x14

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 1871793
    const/16 v3, 0x15

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 1871794
    const/16 v3, 0x16

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v5}, LX/186;->b(II)V

    .line 1871795
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 7

    .prologue
    const/16 v6, 0x11

    const/16 v5, 0x9

    const/16 v4, 0x8

    const/4 v3, 0x3

    const/4 v2, 0x0

    .line 1871796
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1871797
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1871798
    if-eqz v0, :cond_0

    .line 1871799
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1871800
    invoke-static {p0, p1, v2, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1871801
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1871802
    if-eqz v0, :cond_1

    .line 1871803
    const-string v1, "action"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1871804
    invoke-static {p0, v0, p2, p3}, LX/CHy;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1871805
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1871806
    if-eqz v0, :cond_2

    .line 1871807
    const-string v1, "annotations"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1871808
    invoke-static {p0, v0, p2, p3}, LX/CI0;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1871809
    :cond_2
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 1871810
    if-eqz v0, :cond_3

    .line 1871811
    const-string v0, "autoplay_style"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1871812
    invoke-virtual {p0, p1, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1871813
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1871814
    if-eqz v0, :cond_4

    .line 1871815
    const-string v1, "child_elements"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1871816
    invoke-static {p0, v0, p2, p3}, LX/CIG;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1871817
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1871818
    if-eqz v0, :cond_5

    .line 1871819
    const-string v1, "color"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1871820
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1871821
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1871822
    if-eqz v0, :cond_6

    .line 1871823
    const-string v1, "color_types"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1871824
    invoke-static {p0, v0, p2, p3}, LX/CI1;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1871825
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1871826
    if-eqz v0, :cond_7

    .line 1871827
    const-string v1, "content_element"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1871828
    invoke-static {p0, v0, p2, p3}, LX/CIK;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1871829
    :cond_7
    invoke-virtual {p0, p1, v4}, LX/15i;->g(II)I

    move-result v0

    .line 1871830
    if-eqz v0, :cond_8

    .line 1871831
    const-string v0, "control_style"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1871832
    invoke-virtual {p0, p1, v4}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1871833
    :cond_8
    invoke-virtual {p0, p1, v5}, LX/15i;->g(II)I

    move-result v0

    .line 1871834
    if-eqz v0, :cond_9

    .line 1871835
    const-string v0, "document_element_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1871836
    invoke-virtual {p0, p1, v5}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1871837
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1871838
    if-eqz v0, :cond_a

    .line 1871839
    const-string v1, "element_descriptor"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1871840
    invoke-static {p0, v0, p2, p3}, LX/CI9;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1871841
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1871842
    if-eqz v0, :cond_b

    .line 1871843
    const-string v1, "element_photo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1871844
    invoke-static {p0, v0, p2}, LX/CIF;->a(LX/15i;ILX/0nX;)V

    .line 1871845
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1871846
    if-eqz v0, :cond_c

    .line 1871847
    const-string v1, "element_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1871848
    invoke-static {p0, v0, p2, p3}, LX/8al;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1871849
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1871850
    if-eqz v0, :cond_d

    .line 1871851
    const-string v1, "element_video"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1871852
    invoke-static {p0, v0, p2, p3}, LX/8ZV;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1871853
    :cond_d
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1871854
    if-eqz v0, :cond_e

    .line 1871855
    const-string v1, "grid_width_percent"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1871856
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1871857
    :cond_e
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1871858
    if-eqz v0, :cond_f

    .line 1871859
    const-string v1, "image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1871860
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 1871861
    :cond_f
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1871862
    if-eqz v0, :cond_10

    .line 1871863
    const-string v1, "logging_token"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1871864
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1871865
    :cond_10
    invoke-virtual {p0, p1, v6}, LX/15i;->g(II)I

    move-result v0

    .line 1871866
    if-eqz v0, :cond_11

    .line 1871867
    const-string v0, "looping_style"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1871868
    invoke-virtual {p0, p1, v6}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1871869
    :cond_11
    const/16 v0, 0x12

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1871870
    if-eqz v0, :cond_12

    .line 1871871
    const-string v1, "section_header"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1871872
    invoke-static {p0, v0, p2, p3}, LX/CIK;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1871873
    :cond_12
    const/16 v0, 0x13

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1871874
    if-eqz v0, :cond_13

    .line 1871875
    const-string v1, "selected_index"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1871876
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1871877
    :cond_13
    const/16 v0, 0x14

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1871878
    if-eqz v0, :cond_14

    .line 1871879
    const-string v0, "style_list"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1871880
    const/16 v0, 0x14

    invoke-virtual {p0, p1, v0}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 1871881
    :cond_14
    const/16 v0, 0x15

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1871882
    if-eqz v0, :cond_15

    .line 1871883
    const-string v1, "target_uri"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1871884
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1871885
    :cond_15
    const/16 v0, 0x16

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1871886
    if-eqz v0, :cond_16

    .line 1871887
    const-string v1, "touch_targets"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1871888
    invoke-static {p0, v0, p2, p3}, LX/CIM;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1871889
    :cond_16
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1871890
    return-void
.end method
