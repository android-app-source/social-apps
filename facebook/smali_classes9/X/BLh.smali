.class public final LX/BLh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;)V
    .locals 0

    .prologue
    .line 1776225
    iput-object p1, p0, LX/BLh;->a:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x5bb01b2

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1776226
    iget-object v1, p0, LX/BLh;->a:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;

    iget-object v1, v1, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->f:Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;

    iget-object v2, p0, LX/BLh;->a:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;

    invoke-static {v2}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->p(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;)Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;

    move-result-object v2

    if-ne v1, v2, :cond_1

    .line 1776227
    :cond_0
    iget-object v1, p0, LX/BLh;->a:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;

    invoke-static {v1}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->l(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;)Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1776228
    :cond_1
    check-cast p1, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;

    .line 1776229
    iget-object v1, p0, LX/BLh;->a:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;

    iget-object v2, p0, LX/BLh;->a:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;

    iget-object v2, v2, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->l:LX/0P1;

    invoke-virtual {v2}, LX/0P1;->keySet()LX/0Rf;

    move-result-object v2

    invoke-virtual {v2}, LX/0Py;->asList()LX/0Px;

    move-result-object v2

    invoke-virtual {v2, p1}, LX/0Px;->indexOf(Ljava/lang/Object;)I

    move-result v2

    .line 1776230
    iput v2, v1, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->r:I

    .line 1776231
    iget-object v1, p0, LX/BLh;->a:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;

    iget-object v1, v1, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->b:LX/0if;

    sget-object v2, LX/0ig;->k:LX/0ih;

    const-string v3, " %s clicked"

    iget-object v4, p0, LX/BLh;->a:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;

    invoke-static {v4}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->p(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;)Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/composer/targetandprivacy/TargetAndPrivacyItemView;->getTitleText()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1776232
    iget-object v1, p0, LX/BLh;->a:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;

    invoke-static {v1}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;->r(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerTargetPrivacySelectorFragment;)V

    .line 1776233
    const v1, -0x5917e519

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void
.end method
