.class public final enum LX/AYp;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/AYp;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/AYp;

.field public static final enum COMMERCIAL_BREAK_ELIGIBLE:LX/AYp;

.field public static final enum COMMERCIAL_BREAK_FINISHED:LX/AYp;

.field public static final enum COMMERCIAL_BREAK_INELIGIBLE_DUE_TO_VIOLATION:LX/AYp;

.field public static final enum COMMERCIAL_BREAK_INTERRUPTED:LX/AYp;

.field public static final enum COMMERCIAL_BREAK_PROMPT:LX/AYp;

.field public static final enum COMMERCIAL_BREAK_STARTED:LX/AYp;

.field public static final enum COMMERCIAL_BREAK_UNINITIALIZED:LX/AYp;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1686125
    new-instance v0, LX/AYp;

    const-string v1, "COMMERCIAL_BREAK_UNINITIALIZED"

    invoke-direct {v0, v1, v3}, LX/AYp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AYp;->COMMERCIAL_BREAK_UNINITIALIZED:LX/AYp;

    .line 1686126
    new-instance v0, LX/AYp;

    const-string v1, "COMMERCIAL_BREAK_ELIGIBLE"

    invoke-direct {v0, v1, v4}, LX/AYp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AYp;->COMMERCIAL_BREAK_ELIGIBLE:LX/AYp;

    .line 1686127
    new-instance v0, LX/AYp;

    const-string v1, "COMMERCIAL_BREAK_PROMPT"

    invoke-direct {v0, v1, v5}, LX/AYp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AYp;->COMMERCIAL_BREAK_PROMPT:LX/AYp;

    .line 1686128
    new-instance v0, LX/AYp;

    const-string v1, "COMMERCIAL_BREAK_STARTED"

    invoke-direct {v0, v1, v6}, LX/AYp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AYp;->COMMERCIAL_BREAK_STARTED:LX/AYp;

    .line 1686129
    new-instance v0, LX/AYp;

    const-string v1, "COMMERCIAL_BREAK_INTERRUPTED"

    invoke-direct {v0, v1, v7}, LX/AYp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AYp;->COMMERCIAL_BREAK_INTERRUPTED:LX/AYp;

    .line 1686130
    new-instance v0, LX/AYp;

    const-string v1, "COMMERCIAL_BREAK_FINISHED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/AYp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AYp;->COMMERCIAL_BREAK_FINISHED:LX/AYp;

    .line 1686131
    new-instance v0, LX/AYp;

    const-string v1, "COMMERCIAL_BREAK_INELIGIBLE_DUE_TO_VIOLATION"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/AYp;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AYp;->COMMERCIAL_BREAK_INELIGIBLE_DUE_TO_VIOLATION:LX/AYp;

    .line 1686132
    const/4 v0, 0x7

    new-array v0, v0, [LX/AYp;

    sget-object v1, LX/AYp;->COMMERCIAL_BREAK_UNINITIALIZED:LX/AYp;

    aput-object v1, v0, v3

    sget-object v1, LX/AYp;->COMMERCIAL_BREAK_ELIGIBLE:LX/AYp;

    aput-object v1, v0, v4

    sget-object v1, LX/AYp;->COMMERCIAL_BREAK_PROMPT:LX/AYp;

    aput-object v1, v0, v5

    sget-object v1, LX/AYp;->COMMERCIAL_BREAK_STARTED:LX/AYp;

    aput-object v1, v0, v6

    sget-object v1, LX/AYp;->COMMERCIAL_BREAK_INTERRUPTED:LX/AYp;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/AYp;->COMMERCIAL_BREAK_FINISHED:LX/AYp;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/AYp;->COMMERCIAL_BREAK_INELIGIBLE_DUE_TO_VIOLATION:LX/AYp;

    aput-object v2, v0, v1

    sput-object v0, LX/AYp;->$VALUES:[LX/AYp;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1686133
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/AYp;
    .locals 1

    .prologue
    .line 1686134
    const-class v0, LX/AYp;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/AYp;

    return-object v0
.end method

.method public static values()[LX/AYp;
    .locals 1

    .prologue
    .line 1686135
    sget-object v0, LX/AYp;->$VALUES:[LX/AYp;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/AYp;

    return-object v0
.end method
