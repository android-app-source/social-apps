.class public LX/Ajn;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field public final a:LX/1qa;

.field public final b:LX/22q;

.field private final c:Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

.field public final d:LX/0Sy;

.field public final e:LX/Ajs;

.field public final f:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1qa;LX/22q;LX/Ajs;LX/0Sy;Lcom/facebook/common/callercontexttagger/AnalyticsTagger;Ljava/util/concurrent/Executor;)V
    .locals 0
    .param p6    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1708286
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1708287
    iput-object p1, p0, LX/Ajn;->a:LX/1qa;

    .line 1708288
    iput-object p2, p0, LX/Ajn;->b:LX/22q;

    .line 1708289
    iput-object p3, p0, LX/Ajn;->e:LX/Ajs;

    .line 1708290
    iput-object p4, p0, LX/Ajn;->d:LX/0Sy;

    .line 1708291
    iput-object p5, p0, LX/Ajn;->c:Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    .line 1708292
    iput-object p6, p0, LX/Ajn;->f:Ljava/util/concurrent/Executor;

    .line 1708293
    return-void
.end method

.method public static a(LX/0QB;)LX/Ajn;
    .locals 10

    .prologue
    .line 1708294
    const-class v1, LX/Ajn;

    monitor-enter v1

    .line 1708295
    :try_start_0
    sget-object v0, LX/Ajn;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1708296
    sput-object v2, LX/Ajn;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1708297
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1708298
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1708299
    new-instance v3, LX/Ajn;

    invoke-static {v0}, LX/1qa;->a(LX/0QB;)LX/1qa;

    move-result-object v4

    check-cast v4, LX/1qa;

    const-class v5, LX/22q;

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/22q;

    invoke-static {v0}, LX/Ajs;->a(LX/0QB;)LX/Ajs;

    move-result-object v6

    check-cast v6, LX/Ajs;

    invoke-static {v0}, LX/0Sy;->a(LX/0QB;)LX/0Sy;

    move-result-object v7

    check-cast v7, LX/0Sy;

    invoke-static {v0}, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;->a(LX/0QB;)Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    move-result-object v8

    check-cast v8, Lcom/facebook/common/callercontexttagger/AnalyticsTagger;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v9

    check-cast v9, Ljava/util/concurrent/Executor;

    invoke-direct/range {v3 .. v9}, LX/Ajn;-><init>(LX/1qa;LX/22q;LX/Ajs;LX/0Sy;Lcom/facebook/common/callercontexttagger/AnalyticsTagger;Ljava/util/concurrent/Executor;)V

    .line 1708300
    move-object v0, v3

    .line 1708301
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1708302
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Ajn;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1708303
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1708304
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
