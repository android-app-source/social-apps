.class public final LX/BPu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field public final synthetic a:F

.field public final synthetic b:F

.field public final synthetic c:F

.field public final synthetic d:F

.field public final synthetic e:Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;

.field private f:F

.field private g:F


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;FFFF)V
    .locals 0

    .prologue
    .line 1780981
    iput-object p1, p0, LX/BPu;->e:Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;

    iput p2, p0, LX/BPu;->a:F

    iput p3, p0, LX/BPu;->b:F

    iput p4, p0, LX/BPu;->c:F

    iput p5, p0, LX/BPu;->d:F

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    .line 1780982
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1780983
    :goto_0
    :pswitch_0
    const/4 v0, 0x1

    return v0

    .line 1780984
    :pswitch_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, LX/BPu;->f:F

    .line 1780985
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, LX/BPu;->g:F

    goto :goto_0

    .line 1780986
    :pswitch_2
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iget v1, p0, LX/BPu;->f:F

    sub-float/2addr v0, v1

    .line 1780987
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    iget v2, p0, LX/BPu;->g:F

    sub-float/2addr v1, v2

    .line 1780988
    iget-object v2, p0, LX/BPu;->e:Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;

    invoke-static {v2, v0}, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->a(Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;F)F

    .line 1780989
    iget-object v0, p0, LX/BPu;->e:Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;

    iget v0, v0, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->d:F

    iget v2, p0, LX/BPu;->a:F

    cmpg-float v0, v0, v2

    if-gez v0, :cond_0

    .line 1780990
    iget-object v0, p0, LX/BPu;->e:Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;

    iget v2, p0, LX/BPu;->a:F

    .line 1780991
    iput v2, v0, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->d:F

    .line 1780992
    :cond_0
    iget-object v0, p0, LX/BPu;->e:Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;

    iget v0, v0, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->d:F

    iget v2, p0, LX/BPu;->b:F

    cmpl-float v0, v0, v2

    if-lez v0, :cond_1

    .line 1780993
    iget-object v0, p0, LX/BPu;->e:Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;

    iget v2, p0, LX/BPu;->b:F

    .line 1780994
    iput v2, v0, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->d:F

    .line 1780995
    :cond_1
    iget-object v0, p0, LX/BPu;->e:Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;

    invoke-static {v0, v1}, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->c(Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;F)F

    .line 1780996
    iget-object v0, p0, LX/BPu;->e:Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;

    iget v0, v0, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->e:F

    iget v1, p0, LX/BPu;->c:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_2

    .line 1780997
    iget-object v0, p0, LX/BPu;->e:Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;

    iget v1, p0, LX/BPu;->c:F

    .line 1780998
    iput v1, v0, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->e:F

    .line 1780999
    :cond_2
    iget-object v0, p0, LX/BPu;->e:Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;

    iget v0, v0, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->e:F

    iget v1, p0, LX/BPu;->d:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_3

    .line 1781000
    iget-object v0, p0, LX/BPu;->e:Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;

    iget v1, p0, LX/BPu;->d:F

    .line 1781001
    iput v1, v0, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->e:F

    .line 1781002
    :cond_3
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, LX/BPu;->f:F

    .line 1781003
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, LX/BPu;->g:F

    .line 1781004
    iget-object v0, p0, LX/BPu;->e:Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;

    iget-object v1, p0, LX/BPu;->e:Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;

    iget v1, v1, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->d:F

    iget-object v2, p0, LX/BPu;->e:Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;

    iget v2, v2, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->e:F

    .line 1781005
    invoke-static {v0, v1, v2}, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->a$redex0(Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;FF)V

    .line 1781006
    iget-object v0, p0, LX/BPu;->e:Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;

    invoke-virtual {v0}, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->invalidate()V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
