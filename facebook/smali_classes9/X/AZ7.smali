.class public final LX/AZ7;
.super Landroid/os/CountDownTimer;
.source ""


# instance fields
.field public final synthetic a:LX/AZ9;


# direct methods
.method public constructor <init>(LX/AZ9;J)V
    .locals 2

    .prologue
    .line 1686509
    iput-object p1, p0, LX/AZ7;->a:LX/AZ9;

    .line 1686510
    const-wide/16 v0, 0x3e8

    invoke-direct {p0, p2, p3, v0, v1}, Landroid/os/CountDownTimer;-><init>(JJ)V

    .line 1686511
    return-void
.end method


# virtual methods
.method public final onFinish()V
    .locals 9

    .prologue
    .line 1686512
    iget-object v0, p0, LX/AZ7;->a:LX/AZ9;

    iget-object v0, v0, LX/AZ9;->c:LX/AZ8;

    sget-object v1, LX/AZ8;->BEGIN_TRANSITION:LX/AZ8;

    if-ne v0, v1, :cond_0

    .line 1686513
    iget-object v0, p0, LX/AZ7;->a:LX/AZ9;

    sget-object v1, LX/AZ8;->COUNTDOWN:LX/AZ8;

    .line 1686514
    iput-object v1, v0, LX/AZ9;->c:LX/AZ8;

    .line 1686515
    iget-object v0, p0, LX/AZ7;->a:LX/AZ9;

    invoke-static {v0}, LX/AZ9;->c$redex0(LX/AZ9;)V

    .line 1686516
    :cond_0
    iget-object v0, p0, LX/AZ7;->a:LX/AZ9;

    iget-object v0, v0, LX/AZ9;->c:LX/AZ8;

    sget-object v1, LX/AZ8;->END_TRANSITION:LX/AZ8;

    if-ne v0, v1, :cond_3

    iget-object v0, p0, LX/AZ7;->a:LX/AZ9;

    iget-object v0, v0, LX/AZ9;->l:LX/AY9;

    if-eqz v0, :cond_3

    .line 1686517
    iget-object v0, p0, LX/AZ7;->a:LX/AZ9;

    iget-object v0, v0, LX/AZ9;->l:LX/AY9;

    .line 1686518
    iget-object v2, v0, LX/AY9;->a:LX/AYE;

    iget-object v2, v2, LX/AYE;->s:LX/2EJ;

    if-eqz v2, :cond_1

    .line 1686519
    iget-object v2, v0, LX/AY9;->a:LX/AYE;

    iget-object v2, v2, LX/AYE;->s:LX/2EJ;

    invoke-virtual {v2}, LX/2EJ;->dismiss()V

    .line 1686520
    :cond_1
    iget-object v2, v0, LX/AY9;->a:LX/AYE;

    iget-object v2, v2, LX/AYE;->c:LX/AYq;

    sget-object v3, LX/AYp;->COMMERCIAL_BREAK_FINISHED:LX/AYp;

    invoke-virtual {v2, v3}, LX/AYq;->a(LX/AYp;)V

    .line 1686521
    iget-object v2, v0, LX/AY9;->a:LX/AYE;

    iget-object v2, v2, LX/AYE;->c:LX/AYq;

    .line 1686522
    iget-boolean v3, v2, LX/AYq;->d:Z

    move v2, v3

    .line 1686523
    if-eqz v2, :cond_2

    .line 1686524
    iget-object v2, v0, LX/AY9;->a:LX/AYE;

    iget-object v2, v2, LX/AYE;->c:LX/AYq;

    sget-object v3, LX/AYp;->COMMERCIAL_BREAK_INELIGIBLE_DUE_TO_VIOLATION:LX/AYp;

    invoke-virtual {v2, v3}, LX/AYq;->a(LX/AYp;)V

    .line 1686525
    iget-object v2, v0, LX/AY9;->a:LX/AYE;

    iget-object v2, v2, LX/AYE;->m:LX/AWl;

    iget-object v2, v2, LX/AWl;->f:LX/Aby;

    invoke-virtual {v2}, LX/Aby;->d()V

    .line 1686526
    :cond_2
    iget-object v2, v0, LX/AY9;->a:LX/AYE;

    iget-object v2, v2, LX/AYE;->h:LX/AYs;

    .line 1686527
    iget-object v4, v2, LX/AYs;->a:LX/0if;

    sget-object v5, LX/0ig;->C:LX/0ih;

    const-string v6, "commercial_break_end"

    const/4 v7, 0x0

    invoke-static {v2}, LX/AYs;->i(LX/AYs;)LX/1rQ;

    move-result-object v8

    invoke-virtual {v4, v5, v6, v7, v8}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 1686528
    iget-wide v4, v2, LX/AYs;->d:J

    iput-wide v4, v2, LX/AYs;->e:J

    .line 1686529
    iget v4, v2, LX/AYs;->f:I

    add-int/lit8 v4, v4, 0x1

    iput v4, v2, LX/AYs;->f:I

    .line 1686530
    iget-object v0, p0, LX/AZ7;->a:LX/AZ9;

    sget-object v1, LX/AZ8;->NONE:LX/AZ8;

    .line 1686531
    iput-object v1, v0, LX/AZ9;->c:LX/AZ8;

    .line 1686532
    :cond_3
    return-void
.end method

.method public final onTick(J)V
    .locals 0

    .prologue
    .line 1686533
    return-void
.end method
