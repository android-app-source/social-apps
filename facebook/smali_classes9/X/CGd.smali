.class public final LX/CGd;
.super LX/AQ9;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "Lcom/facebook/ipc/composer/plugin/ComposerPluginDataProvider;",
        "DerivedData::",
        "Lcom/facebook/ipc/composer/plugin/ComposerPluginDerivedDataProvider;",
        "Mutation::",
        "Lcom/facebook/ipc/composer/plugin/ComposerPluginMutation",
        "<TMutation;>;>",
        "LX/AQ9",
        "<TModelData;TDerivedData;TMutation;>;"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field public static h:Ljava/lang/String;


# instance fields
.field public b:Lcom/facebook/groups/composer/groupspollcomposer/GroupsPollDataModel;

.field public final c:LX/AR2;

.field public final d:LX/AR4;

.field public final e:LX/ARD;

.field private final f:LX/0lB;

.field private final g:LX/03V;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1865687
    const-class v0, LX/CGd;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/CGd;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/B5j;Landroid/content/Context;LX/AR2;LX/AR4;LX/ARD;LX/0lB;LX/03V;)V
    .locals 0
    .param p1    # LX/B5j;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/B5j",
            "<TModelData;TDerivedData;TMutation;>;",
            "Landroid/content/Context;",
            "LX/AR2;",
            "LX/AR4;",
            "LX/ARD;",
            "LX/0lB;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1865680
    invoke-direct {p0, p2, p1}, LX/AQ9;-><init>(Landroid/content/Context;LX/B5j;)V

    .line 1865681
    iput-object p3, p0, LX/CGd;->c:LX/AR2;

    .line 1865682
    iput-object p4, p0, LX/CGd;->d:LX/AR4;

    .line 1865683
    iput-object p5, p0, LX/CGd;->e:LX/ARD;

    .line 1865684
    iput-object p6, p0, LX/CGd;->f:LX/0lB;

    .line 1865685
    iput-object p7, p0, LX/CGd;->g:LX/03V;

    .line 1865686
    return-void
.end method


# virtual methods
.method public final C()LX/ARN;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1865679
    new-instance v0, LX/CGZ;

    invoke-direct {v0, p0}, LX/CGZ;-><init>(LX/CGd;)V

    return-object v0
.end method

.method public final V()LX/ARN;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1865678
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method

.method public final Z()LX/ARN;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1865677
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method

.method public final a()LX/B5f;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1865670
    iget-object v0, p0, LX/CGd;->b:Lcom/facebook/groups/composer/groupspollcomposer/GroupsPollDataModel;

    if-eqz v0, :cond_0

    .line 1865671
    :try_start_0
    iget-object v0, p0, LX/CGd;->f:LX/0lB;

    iget-object v1, p0, LX/CGd;->b:Lcom/facebook/groups/composer/groupspollcomposer/GroupsPollDataModel;

    invoke-virtual {v0, v1}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1865672
    invoke-static {v0}, LX/B5f;->a(Ljava/lang/String;)LX/B5f;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1865673
    :goto_0
    return-object v0

    .line 1865674
    :catch_0
    move-exception v0

    .line 1865675
    iget-object v1, p0, LX/CGd;->g:LX/03V;

    sget-object v2, LX/CGd;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1865676
    :cond_0
    sget-object v0, LX/B5f;->a:LX/B5f;

    goto :goto_0
.end method

.method public final aA()LX/ARN;
    .locals 1

    .prologue
    .line 1865669
    sget-object v0, LX/ARN;->a:LX/ARN;

    return-object v0
.end method

.method public final aB()LX/AQ4;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/AQ4",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1865665
    new-instance v0, LX/CGX;

    invoke-direct {v0, p0}, LX/CGX;-><init>(LX/CGd;)V

    return-object v0
.end method

.method public final aC()LX/ARN;
    .locals 1

    .prologue
    .line 1865668
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method

.method public final aG()LX/ARN;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1865688
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method

.method public final aI()LX/ARN;
    .locals 1

    .prologue
    .line 1865667
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method

.method public final aa()LX/ARN;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1865666
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method

.method public final ab()LX/ARN;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1865664
    new-instance v0, LX/CGY;

    invoke-direct {v0, p0}, LX/CGY;-><init>(LX/CGd;)V

    return-object v0
.end method

.method public final ad()LX/ARN;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1865663
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method

.method public final ae()LX/ARN;
    .locals 1

    .prologue
    .line 1865662
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method

.method public final af()LX/ARN;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1865661
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method

.method public final aj()LX/AQ4;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/AQ4",
            "<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1865660
    new-instance v0, LX/CGa;

    invoke-direct {v0, p0}, LX/CGa;-><init>(LX/CGd;)V

    return-object v0
.end method

.method public final aq()LX/ARN;
    .locals 1

    .prologue
    .line 1865659
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method

.method public final c(Landroid/view/ViewStub;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1865647
    const v0, 0x7f030861

    invoke-virtual {p1, v0}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 1865648
    invoke-virtual {p1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/composer/groupspollcomposer/view/GroupsPollComposerFooterView;

    .line 1865649
    sget-object v1, LX/CGd;->h:Ljava/lang/String;

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1865650
    new-instance v1, Lcom/facebook/groups/composer/groupspollcomposer/GroupsPollDataModel;

    invoke-direct {v1}, Lcom/facebook/groups/composer/groupspollcomposer/GroupsPollDataModel;-><init>()V

    iput-object v1, p0, LX/CGd;->b:Lcom/facebook/groups/composer/groupspollcomposer/GroupsPollDataModel;

    .line 1865651
    :goto_0
    iget-object v1, p0, LX/CGd;->b:Lcom/facebook/groups/composer/groupspollcomposer/GroupsPollDataModel;

    invoke-virtual {v0, v1}, Lcom/facebook/groups/composer/groupspollcomposer/view/GroupsPollComposerFooterView;->a(Lcom/facebook/groups/composer/groupspollcomposer/GroupsPollDataModel;)V

    .line 1865652
    return-void

    .line 1865653
    :cond_0
    :try_start_0
    iget-object v1, p0, LX/CGd;->f:LX/0lB;

    sget-object v2, LX/CGd;->h:Ljava/lang/String;

    const-class v3, Lcom/facebook/groups/composer/groupspollcomposer/GroupsPollDataModel;

    invoke-virtual {v1, v2, v3}, LX/0lC;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/composer/groupspollcomposer/GroupsPollDataModel;

    iput-object v1, p0, LX/CGd;->b:Lcom/facebook/groups/composer/groupspollcomposer/GroupsPollDataModel;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1865654
    sput-object v4, LX/CGd;->h:Ljava/lang/String;

    goto :goto_0

    .line 1865655
    :catch_0
    move-exception v1

    .line 1865656
    :try_start_1
    iget-object v2, p0, LX/CGd;->g:LX/03V;

    sget-object v3, LX/CGd;->a:Ljava/lang/String;

    invoke-virtual {v2, v3, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1865657
    new-instance v1, Lcom/facebook/groups/composer/groupspollcomposer/GroupsPollDataModel;

    invoke-direct {v1}, Lcom/facebook/groups/composer/groupspollcomposer/GroupsPollDataModel;-><init>()V

    iput-object v1, p0, LX/CGd;->b:Lcom/facebook/groups/composer/groupspollcomposer/GroupsPollDataModel;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1865658
    sput-object v4, LX/CGd;->h:Ljava/lang/String;

    goto :goto_0

    :catchall_0
    move-exception v0

    sput-object v4, LX/CGd;->h:Ljava/lang/String;

    throw v0
.end method
