.class public final LX/AtG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/AsT;


# instance fields
.field public final synthetic a:LX/AtM;


# direct methods
.method public constructor <init>(LX/AtM;)V
    .locals 0

    .prologue
    .line 1721235
    iput-object p1, p0, LX/AtG;->a:LX/AtM;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick()V
    .locals 7

    .prologue
    .line 1721236
    iget-object v0, p0, LX/AtG;->a:LX/AtM;

    iget-object v0, v0, LX/AtM;->e:LX/ArT;

    sget-object v1, LX/ArJ;->TAP_EFFECTS_TRAY_BUTTON:LX/ArJ;

    .line 1721237
    iget-object v3, v0, LX/ArT;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0il;

    invoke-interface {v3}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/composer/system/model/ComposerModelImpl;

    invoke-virtual {v3}, Lcom/facebook/composer/system/model/ComposerModelImpl;->l()Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;->a(Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;)Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;

    move-result-object v3

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->setEffectsTraySessionId(Ljava/lang/String;)Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;

    move-result-object v3

    iget-object v4, v0, LX/ArT;->a:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v5

    invoke-virtual {v3, v5, v6}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->setEffectsTraySessionStartTime(J)Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData$Builder;->a()Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;

    move-result-object v3

    invoke-static {v0, v3}, LX/ArT;->a(LX/ArT;Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingData;)V

    .line 1721238
    iget-object v3, v0, LX/ArT;->b:LX/ArL;

    .line 1721239
    sget-object v4, LX/ArH;->START_EFFECTS_TRAY_SESSION:LX/ArH;

    invoke-static {v3, v4, v1}, LX/ArL;->a(LX/ArL;LX/5oU;LX/ArJ;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v3, v5}, LX/ArL;->a(LX/ArL;Z)Ljava/util/Map;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    invoke-static {v3, v4}, LX/ArL;->a(LX/ArL;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1721240
    iget-object v0, p0, LX/AtG;->a:LX/AtM;

    iget-object v0, v0, LX/AtM;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    sget-object v1, LX/AtM;->a:LX/0jK;

    sget-object v2, LX/86o;->EFFECTS:LX/86o;

    invoke-static {v0, v1, v2}, LX/87N;->a(LX/0il;LX/0jK;LX/86o;)V

    .line 1721241
    return-void
.end method
