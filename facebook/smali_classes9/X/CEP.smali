.class public final LX/CEP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/CEO;


# instance fields
.field public final synthetic a:LX/CEQ;


# direct methods
.method public constructor <init>(LX/CEQ;)V
    .locals 0

    .prologue
    .line 1861100
    iput-object p1, p0, LX/CEP;->a:LX/CEQ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/greetingcards/verve/model/VMAction;Landroid/view/View;)V
    .locals 7

    .prologue
    .line 1861101
    if-nez p1, :cond_1

    .line 1861102
    :cond_0
    :goto_0
    return-void

    .line 1861103
    :cond_1
    instance-of v0, p2, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;

    if-eqz v0, :cond_3

    .line 1861104
    check-cast p2, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;

    .line 1861105
    iget-object v0, p0, LX/CEP;->a:LX/CEQ;

    iget-object v0, v0, LX/CEQ;->a:Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;

    .line 1861106
    const-string v1, "hidden"

    iget-object v2, p1, Lcom/facebook/greetingcards/verve/model/VMAction;->type:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1861107
    iget-object v1, v0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->N:Ljava/util/List;

    invoke-interface {v1, p2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1861108
    :cond_2
    :goto_1
    invoke-static {v0}, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->b(Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;)V

    .line 1861109
    goto :goto_0

    .line 1861110
    :cond_3
    instance-of v0, p2, Lcom/facebook/friendsharing/souvenirs/verve/SouvenirVerveVideoView;

    if-eqz v0, :cond_0

    .line 1861111
    check-cast p2, Lcom/facebook/friendsharing/souvenirs/verve/SouvenirVerveVideoView;

    .line 1861112
    iget-object v0, p0, LX/CEP;->a:LX/CEQ;

    iget-object v0, v0, LX/CEQ;->a:Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;

    .line 1861113
    const-string v1, "hidden"

    iget-object v2, p1, Lcom/facebook/greetingcards/verve/model/VMAction;->type:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 1861114
    iget-object v1, v0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->x:LX/CEZ;

    .line 1861115
    iget-object v2, v1, LX/CEZ;->c:Ljava/util/ArrayDeque;

    invoke-virtual {v2, p2}, Ljava/util/ArrayDeque;->remove(Ljava/lang/Object;)Z

    .line 1861116
    iget-object v2, v1, LX/CEZ;->e:LX/CEY;

    if-ne v2, p2, :cond_4

    .line 1861117
    invoke-static {p2}, LX/CEZ;->c(LX/CEY;)V

    .line 1861118
    const/4 v2, 0x0

    iput-object v2, v1, LX/CEZ;->e:LX/CEY;

    .line 1861119
    :cond_4
    invoke-static {v1}, LX/CEZ;->c(LX/CEZ;)V

    .line 1861120
    invoke-virtual {p2}, Lcom/facebook/video/player/RichVideoPlayer;->i()V

    .line 1861121
    :cond_5
    :goto_2
    goto :goto_0

    .line 1861122
    :cond_6
    const-string v1, "visible"

    iget-object v2, p1, Lcom/facebook/greetingcards/verve/model/VMAction;->type:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1861123
    iget-object v1, v0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->N:Ljava/util/List;

    invoke-interface {v1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1861124
    iget-object v1, v0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->C:Ljava/util/TimerTask;

    if-nez v1, :cond_7

    .line 1861125
    new-instance v1, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment$5;

    invoke-direct {v1, v0}, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment$5;-><init>(Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;)V

    iput-object v1, v0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->C:Ljava/util/TimerTask;

    .line 1861126
    iget-object v1, v0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->I:Ljava/util/Timer;

    iget-object v2, v0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->C:Ljava/util/TimerTask;

    const-wide/16 v3, 0x0

    const-wide/16 v5, 0x578

    invoke-virtual/range {v1 .. v6}, Ljava/util/Timer;->scheduleAtFixedRate(Ljava/util/TimerTask;JJ)V

    .line 1861127
    :cond_7
    invoke-virtual {p2}, Lcom/facebook/friendsharing/souvenirs/ui/SouvenirsBurstView;->a()V

    goto :goto_1

    .line 1861128
    :cond_8
    const-string v1, "visible"

    iget-object v2, p1, Lcom/facebook/greetingcards/verve/model/VMAction;->type:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1861129
    iget-object v1, v0, Lcom/facebook/friendsharing/souvenirs/fragment/SouvenirsFragment;->x:LX/CEZ;

    .line 1861130
    invoke-interface {p2}, LX/CEY;->a()V

    .line 1861131
    iget-object v2, v1, LX/CEZ;->c:Ljava/util/ArrayDeque;

    invoke-virtual {v2, p2}, Ljava/util/ArrayDeque;->addLast(Ljava/lang/Object;)V

    .line 1861132
    invoke-static {v1}, LX/CEZ;->c(LX/CEZ;)V

    .line 1861133
    goto :goto_2
.end method
