.class public abstract LX/BVZ;
.super LX/BVT;
.source ""


# direct methods
.method public constructor <init>(LX/BW4;LX/BW5;)V
    .locals 0

    .prologue
    .line 1791237
    invoke-direct {p0, p1, p2}, LX/BVT;-><init>(LX/BW4;LX/BW5;)V

    .line 1791238
    return-void
.end method


# virtual methods
.method public a(Lorg/w3c/dom/Node;Landroid/view/ViewGroup;LX/BVh;Landroid/content/Context;)Landroid/view/View;
    .locals 6

    .prologue
    .line 1791239
    invoke-super {p0, p1, p2, p3, p4}, LX/BVT;->a(Lorg/w3c/dom/Node;Landroid/view/ViewGroup;LX/BVh;Landroid/content/Context;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 1791240
    invoke-interface {p1}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v2

    .line 1791241
    const/4 v1, 0x0

    :goto_0
    invoke-interface {v2}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v3

    if-ge v1, v3, :cond_1

    .line 1791242
    invoke-interface {v2, v1}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v3

    .line 1791243
    invoke-interface {v3}, Lorg/w3c/dom/Node;->getNodeType()S

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_0

    .line 1791244
    invoke-interface {v3}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p3, v4}, LX/BVh;->a(Ljava/lang/String;)LX/BVT;

    move-result-object v4

    .line 1791245
    invoke-virtual {v4, v3, v0, p3, p4}, LX/BVT;->a(Lorg/w3c/dom/Node;Landroid/view/ViewGroup;LX/BVh;Landroid/content/Context;)Landroid/view/View;

    move-result-object v3

    .line 1791246
    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1791247
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1791248
    :cond_1
    return-object v0
.end method
