.class public LX/BMj;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pp;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/productionprompts/ui/components/LowConfidencePromptIconComponentSpec;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/BMj",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/productionprompts/ui/components/LowConfidencePromptIconComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1777853
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 1777854
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/BMj;->b:LX/0Zi;

    .line 1777855
    iput-object p1, p0, LX/BMj;->a:LX/0Ot;

    .line 1777856
    return-void
.end method

.method public static a(LX/0QB;)LX/BMj;
    .locals 4

    .prologue
    .line 1777842
    const-class v1, LX/BMj;

    monitor-enter v1

    .line 1777843
    :try_start_0
    sget-object v0, LX/BMj;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1777844
    sput-object v2, LX/BMj;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1777845
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1777846
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1777847
    new-instance v3, LX/BMj;

    const/16 p0, 0x3000

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/BMj;-><init>(LX/0Ot;)V

    .line 1777848
    move-object v0, v3

    .line 1777849
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1777850
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/BMj;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1777851
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1777852
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 4

    .prologue
    .line 1777838
    check-cast p2, LX/BMi;

    .line 1777839
    iget-object v0, p0, LX/BMj;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/productionprompts/ui/components/LowConfidencePromptIconComponentSpec;

    iget-object v1, p2, LX/BMi;->a:Landroid/net/Uri;

    iget-object v2, p2, LX/BMi;->b:LX/1Pp;

    .line 1777840
    iget-object v3, v0, Lcom/facebook/productionprompts/ui/components/LowConfidencePromptIconComponentSpec;->b:LX/1nu;

    invoke-virtual {v3, p1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object v3

    invoke-virtual {v3, v1}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object v3

    sget-object p0, Lcom/facebook/productionprompts/ui/components/LowConfidencePromptIconComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v3, p0}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/1nw;->a(LX/1Pp;)LX/1nw;

    move-result-object v3

    sget-object p0, LX/1Up;->c:LX/1Up;

    invoke-virtual {v3, p0}, LX/1nw;->a(LX/1Up;)LX/1nw;

    move-result-object v3

    const/high16 p0, 0x3f800000    # 1.0f

    invoke-virtual {v3, p0}, LX/1nw;->c(F)LX/1nw;

    move-result-object v3

    const p0, 0x7f021531

    invoke-virtual {v3, p0}, LX/1nw;->h(I)LX/1nw;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const p0, 0x7f0b116f

    invoke-interface {v3, p0}, LX/1Di;->i(I)LX/1Di;

    move-result-object v3

    const p0, 0x7f0b116f

    invoke-interface {v3, p0}, LX/1Di;->q(I)LX/1Di;

    move-result-object v3

    const/16 p0, 0x8

    const p2, 0x7f0b1170

    invoke-interface {v3, p0, p2}, LX/1Di;->g(II)LX/1Di;

    move-result-object v3

    const/4 p0, 0x4

    const p2, 0x7f0b0060

    invoke-interface {v3, p0, p2}, LX/1Di;->c(II)LX/1Di;

    move-result-object v3

    const/4 p0, 0x1

    const p2, 0x7f0b0062

    invoke-interface {v3, p0, p2}, LX/1Di;->c(II)LX/1Di;

    move-result-object v3

    const/4 p0, 0x5

    const p2, 0x7f0b1171

    invoke-interface {v3, p0, p2}, LX/1Di;->c(II)LX/1Di;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 1777841
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1777836
    invoke-static {}, LX/1dS;->b()V

    .line 1777837
    const/4 v0, 0x0

    return-object v0
.end method
