.class public LX/CfU;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2ct;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/2d1;

.field private final d:LX/Cfw;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Ot;LX/2d1;LX/Cfw;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Ot",
            "<",
            "LX/2ct;",
            ">;",
            "LX/2d1;",
            "LX/Cfw;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1925968
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1925969
    iput-object p1, p0, LX/CfU;->a:Landroid/content/Context;

    .line 1925970
    iput-object p2, p0, LX/CfU;->b:LX/0Ot;

    .line 1925971
    iput-object p3, p0, LX/CfU;->c:LX/2d1;

    .line 1925972
    iput-object p4, p0, LX/CfU;->d:LX/Cfw;

    .line 1925973
    return-void
.end method

.method public static b(LX/CfU;Ljava/lang/String;)Z
    .locals 1
    .param p0    # LX/CfU;
        .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
        .end annotation
    .end param

    .prologue
    .line 1925974
    iget-object v0, p0, LX/CfU;->c:LX/2d1;

    invoke-virtual {v0}, LX/2d1;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "ANDROID_FEED_CHECKIN_SUGGESTION"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "ANDROID_SEARCH_LOCAL_PLACE_TIPS_CHECKIN"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/2iz;LX/2jY;Lcom/facebook/reaction/ReactionQueryParams;)Z
    .locals 10

    .prologue
    .line 1925975
    iget-object v0, p2, LX/2jY;->b:Ljava/lang/String;

    move-object v0, v0

    .line 1925976
    invoke-static {p0, v0}, LX/CfU;->b(LX/CfU;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1925977
    const/4 v0, 0x0

    .line 1925978
    :goto_0
    return v0

    .line 1925979
    :cond_0
    iget-object v0, p0, LX/CfU;->d:LX/Cfw;

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1925980
    iget-boolean v1, p2, LX/2jY;->v:Z

    if-eqz v1, :cond_2

    move v1, v2

    .line 1925981
    :goto_1
    move v0, v1

    .line 1925982
    if-eqz v0, :cond_1

    .line 1925983
    iget-object v0, p2, LX/2jY;->a:Ljava/lang/String;

    move-object v0, v0

    .line 1925984
    iget-object v1, p3, Lcom/facebook/reaction/ReactionQueryParams;->l:Ljava/lang/Long;

    move-object v1, v1

    .line 1925985
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1925986
    iget-object v1, p3, Lcom/facebook/reaction/ReactionQueryParams;->m:Ljava/lang/String;

    move-object v3, v1

    .line 1925987
    iget-object v1, p0, LX/CfU;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2ct;

    sget-object v4, LX/2cx;->POST_COMPOSE:LX/2cx;

    invoke-static {v4}, Lcom/facebook/placetips/bootstrap/PresenceSource;->a(LX/2cx;)Lcom/facebook/placetips/bootstrap/PresenceSource;

    move-result-object v4

    invoke-virtual {v1, v4}, LX/2ct;->a(Lcom/facebook/placetips/bootstrap/PresenceSource;)LX/CeG;

    move-result-object v1

    .line 1925988
    iput-object v2, v1, LX/CeG;->c:Ljava/lang/String;

    .line 1925989
    move-object v1, v1

    .line 1925990
    iput-object v3, v1, LX/CeG;->d:Ljava/lang/String;

    .line 1925991
    move-object v1, v1

    .line 1925992
    iget-object v2, p0, LX/CfU;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f082238

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, LX/CeK;->a(Ljava/lang/String;Ljava/lang/String;)LX/175;

    move-result-object v2

    move-object v2, v2

    .line 1925993
    iput-object v2, v1, LX/CeG;->g:LX/175;

    .line 1925994
    move-object v1, v1

    .line 1925995
    const/4 v2, 0x0

    .line 1925996
    iput-object v2, v1, LX/CeG;->h:LX/175;

    .line 1925997
    move-object v1, v1

    .line 1925998
    iput-object v0, v1, LX/CeG;->o:Ljava/lang/String;

    .line 1925999
    move-object v1, v1

    .line 1926000
    sget-object v2, LX/Cdj;->HIGH:LX/Cdj;

    .line 1926001
    iput-object v2, v1, LX/CeG;->p:LX/Cdj;

    .line 1926002
    move-object v1, v1

    .line 1926003
    invoke-virtual {v1}, LX/CeG;->a()LX/0bZ;

    .line 1926004
    :goto_2
    const/4 v0, 0x1

    goto :goto_0

    .line 1926005
    :cond_1
    const-string v0, "NO_CONTENT"

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/2iz;->a(LX/2jY;Ljava/lang/String;Ljava/lang/Long;)V

    goto :goto_2

    .line 1926006
    :cond_2
    iget-object v1, p2, LX/2jY;->d:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    move v1, v3

    .line 1926007
    goto :goto_1

    .line 1926008
    :cond_3
    iget-object v1, p2, LX/2jY;->d:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_4
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/9qT;

    .line 1926009
    invoke-interface {v1}, LX/9qT;->a()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    move v4, v3

    :goto_3
    if-ge v4, v7, :cond_4

    invoke-virtual {v6, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoriesModel$EdgesModel;

    .line 1926010
    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoriesModel$EdgesModel;->b()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v8

    if-eqz v8, :cond_5

    .line 1926011
    const-string v8, "SUCCESS"

    invoke-virtual {v1}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoriesModel$EdgesModel;->b()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Cfw;->a(LX/9qX;)LX/Cfx;

    move-result-object v1

    .line 1926012
    iget-object v9, v1, LX/Cfx;->d:Ljava/lang/String;

    move-object v1, v9

    .line 1926013
    invoke-virtual {v8, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1926014
    iput-boolean v2, p2, LX/2jY;->v:Z

    move v1, v2

    .line 1926015
    goto/16 :goto_1

    .line 1926016
    :cond_5
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_3

    :cond_6
    move v1, v3

    .line 1926017
    goto/16 :goto_1
.end method
