.class public LX/B4D;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/B4D;


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            ">;"
        }
    .end annotation
.end field

.field public volatile b:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0SG;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1741393
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1741394
    return-void
.end method

.method public static a(LX/0QB;)LX/B4D;
    .locals 5

    .prologue
    .line 1741395
    sget-object v0, LX/B4D;->c:LX/B4D;

    if-nez v0, :cond_1

    .line 1741396
    const-class v1, LX/B4D;

    monitor-enter v1

    .line 1741397
    :try_start_0
    sget-object v0, LX/B4D;->c:LX/B4D;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1741398
    if-eqz v2, :cond_0

    .line 1741399
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1741400
    new-instance v3, LX/B4D;

    invoke-direct {v3}, LX/B4D;-><init>()V

    .line 1741401
    const/16 v4, 0xdf4

    invoke-static {v0, v4}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 p0, 0x2e3

    invoke-static {v0, p0}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    .line 1741402
    iput-object v4, v3, LX/B4D;->a:LX/0Or;

    iput-object p0, v3, LX/B4D;->b:LX/0Or;

    .line 1741403
    move-object v0, v3

    .line 1741404
    sput-object v0, LX/B4D;->c:LX/B4D;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1741405
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1741406
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1741407
    :cond_1
    sget-object v0, LX/B4D;->c:LX/B4D;

    return-object v0

    .line 1741408
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1741409
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(J)J
    .locals 11

    .prologue
    const-wide/16 v2, 0x0

    .line 1741410
    iget-object v0, p0, LX/B4D;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0SG;

    .line 1741411
    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    const-wide/16 v4, 0x3e8

    div-long v4, v0, v4

    .line 1741412
    cmp-long v0, p1, v2

    if-eqz v0, :cond_0

    cmp-long v0, p1, v4

    if-gez v0, :cond_1

    .line 1741413
    :cond_0
    iget-object v0, p0, LX/B4D;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0W3;

    .line 1741414
    sget-wide v6, LX/0X5;->fh:J

    const-wide/32 v8, 0x93a80

    invoke-interface {v0, v6, v7, v8, v9}, LX/0W4;->a(JJ)J

    move-result-wide v0

    .line 1741415
    cmp-long v6, v0, v2

    if-nez v6, :cond_2

    move-wide p1, v2

    .line 1741416
    :cond_1
    :goto_0
    return-wide p1

    .line 1741417
    :cond_2
    add-long p1, v4, v0

    goto :goto_0
.end method
