.class public final LX/BMV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/AQ4;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/AQ4",
        "<",
        "Landroid/content/Intent;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/BMY;


# direct methods
.method public constructor <init>(LX/BMY;)V
    .locals 0

    .prologue
    .line 1777411
    iput-object p1, p0, LX/BMV;->a:LX/BMY;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 14

    .prologue
    .line 1777412
    iget-object v0, p0, LX/BMV;->a:LX/BMY;

    iget-object v0, v0, LX/BMY;->b:LX/BMQ;

    iget-object v1, p0, LX/BMV;->a:LX/BMY;

    .line 1777413
    invoke-virtual {v1}, LX/AQ9;->R()LX/B5j;

    move-result-object v2

    move-object v1, v2

    .line 1777414
    iget-object v2, p0, LX/BMV;->a:LX/BMY;

    .line 1777415
    iget-object v3, v2, LX/AQ9;->q:LX/ARN;

    move-object v2, v3

    .line 1777416
    iget-object v3, p0, LX/BMV;->a:LX/BMY;

    iget-object v3, v3, LX/BMY;->a:Lcom/facebook/productionprompts/composer/ProductionPromptsPluginConfig;

    invoke-virtual {v3}, Lcom/facebook/productionprompts/composer/ProductionPromptsPluginConfig;->d()Lcom/facebook/productionprompts/logging/PromptAnalytics;

    move-result-object v3

    iget-object v4, p0, LX/BMV;->a:LX/BMY;

    invoke-static {v4}, LX/BMY;->aM(LX/BMY;)Ljava/lang/Class;

    move-result-object v4

    iget-object v5, p0, LX/BMV;->a:LX/BMY;

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 1777417
    invoke-virtual {v5}, LX/AQ9;->R()LX/B5j;

    move-result-object v6

    invoke-virtual {v6}, LX/B5j;->d()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v6

    .line 1777418
    iget-object v9, v5, LX/BMY;->a:Lcom/facebook/productionprompts/composer/ProductionPromptsPluginConfig;

    invoke-virtual {v9}, Lcom/facebook/productionprompts/composer/ProductionPromptsPluginConfig;->c()Lcom/facebook/productionprompts/model/ProductionPrompt;

    move-result-object v9

    .line 1777419
    invoke-virtual {v9}, Lcom/facebook/productionprompts/model/ProductionPrompt;->d()Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v10

    if-eqz v10, :cond_0

    invoke-virtual {v9}, Lcom/facebook/productionprompts/model/ProductionPrompt;->d()Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v10

    invoke-interface {v6}, LX/0ip;->getMinutiaeObject()Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/facebook/composer/minutiae/model/MinutiaeObject;->b(Lcom/facebook/composer/minutiae/model/MinutiaeObject;)Z

    move-result v10

    if-eqz v10, :cond_0

    move v6, v7

    .line 1777420
    :goto_0
    move v5, v6

    .line 1777421
    invoke-virtual/range {v0 .. v5}, LX/BMQ;->a(LX/B5j;LX/ARN;Lcom/facebook/productionprompts/logging/PromptAnalytics;Ljava/lang/Class;Z)Landroid/content/Intent;

    move-result-object v0

    return-object v0

    .line 1777422
    :cond_0
    invoke-virtual {v9}, Lcom/facebook/productionprompts/model/ProductionPrompt;->g()Ljava/lang/String;

    move-result-object v10

    if-eqz v10, :cond_1

    .line 1777423
    invoke-virtual {v9}, Lcom/facebook/productionprompts/model/ProductionPrompt;->g()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    .line 1777424
    invoke-interface {v6}, LX/0j8;->getLocationInfo()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v11

    invoke-virtual {v11}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->j()J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    move v6, v7

    .line 1777425
    goto :goto_0

    .line 1777426
    :cond_1
    invoke-virtual {v9}, Lcom/facebook/productionprompts/model/ProductionPrompt;->f()Ljava/lang/String;

    move-result-object v10

    if-eqz v10, :cond_3

    .line 1777427
    invoke-interface {v6}, LX/0j5;->getShareParams()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v6

    .line 1777428
    if-nez v6, :cond_2

    const/4 v6, 0x0

    .line 1777429
    :goto_1
    invoke-virtual {v9}, Lcom/facebook/productionprompts/model/ProductionPrompt;->f()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    move v6, v7

    .line 1777430
    goto :goto_0

    .line 1777431
    :cond_2
    iget-object v6, v6, Lcom/facebook/ipc/composer/intent/ComposerShareParams;->linkForShare:Ljava/lang/String;

    goto :goto_1

    .line 1777432
    :cond_3
    invoke-virtual {v9}, Lcom/facebook/productionprompts/model/ProductionPrompt;->o()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;

    move-result-object v6

    if-eqz v6, :cond_6

    .line 1777433
    new-instance v10, Ljava/util/HashSet;

    invoke-direct {v10}, Ljava/util/HashSet;-><init>()V

    .line 1777434
    invoke-virtual {v9}, Lcom/facebook/productionprompts/model/ProductionPrompt;->o()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;->c()LX/0Px;

    move-result-object v11

    invoke-virtual {v11}, LX/0Px;->size()I

    move-result v12

    move v9, v8

    :goto_2
    if-ge v9, v12, :cond_4

    invoke-virtual {v11, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;

    .line 1777435
    invoke-virtual {v6}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;->k()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v10, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1777436
    add-int/lit8 v6, v9, 0x1

    move v9, v6

    goto :goto_2

    .line 1777437
    :cond_4
    invoke-virtual {v5}, LX/AQ9;->R()LX/B5j;

    move-result-object v6

    invoke-virtual {v6}, LX/B5j;->d()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v6

    invoke-interface {v6}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v11

    invoke-virtual {v11}, LX/0Px;->size()I

    move-result v12

    move v9, v8

    .line 1777438
    :goto_3
    if-ge v9, v12, :cond_6

    invoke-virtual {v11, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 1777439
    invoke-virtual {v6}, Lcom/facebook/composer/attachments/ComposerAttachment;->e()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v6

    .line 1777440
    if-eqz v6, :cond_5

    invoke-virtual {v6}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getFilterName()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v10, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    move v6, v7

    .line 1777441
    goto/16 :goto_0

    .line 1777442
    :cond_5
    add-int/lit8 v6, v9, 0x1

    move v9, v6

    goto :goto_3

    .line 1777443
    :cond_6
    invoke-virtual {v5}, LX/AQ9;->R()LX/B5j;

    move-result-object v6

    invoke-virtual {v6}, LX/B5j;->d()Lcom/facebook/composer/system/model/ComposerModelImpl;

    move-result-object v6

    invoke-interface {v6}, LX/0io;->getAttachments()LX/0Px;

    move-result-object v10

    invoke-virtual {v10}, LX/0Px;->size()I

    move-result v11

    move v9, v8

    :goto_4
    if-ge v9, v11, :cond_8

    invoke-virtual {v10, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 1777444
    invoke-virtual {v6}, Lcom/facebook/composer/attachments/ComposerAttachment;->c()Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v6}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v6

    iget-object v12, v5, LX/BMY;->a:Lcom/facebook/productionprompts/composer/ProductionPromptsPluginConfig;

    invoke-virtual {v12}, Lcom/facebook/productionprompts/composer/ProductionPromptsPluginConfig;->e()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v6, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    move v6, v7

    .line 1777445
    goto/16 :goto_0

    .line 1777446
    :cond_7
    add-int/lit8 v6, v9, 0x1

    move v9, v6

    goto :goto_4

    :cond_8
    move v6, v8

    .line 1777447
    goto/16 :goto_0
.end method
