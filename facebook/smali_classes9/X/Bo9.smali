.class public final LX/Bo9;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLStory;

.field public final synthetic b:Landroid/content/Context;

.field public final synthetic c:LX/1dt;


# direct methods
.method public constructor <init>(LX/1dt;Lcom/facebook/graphql/model/GraphQLStory;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1821557
    iput-object p1, p0, LX/Bo9;->c:LX/1dt;

    iput-object p2, p0, LX/Bo9;->a:Lcom/facebook/graphql/model/GraphQLStory;

    iput-object p3, p0, LX/Bo9;->b:Landroid/content/Context;

    invoke-direct {p0}, LX/2h0;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 7

    .prologue
    .line 1821558
    iget-object v0, p0, LX/Bo9;->c:LX/1dt;

    iget-object v0, v0, LX/1dt;->B:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f081057

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 1821559
    iget-object v0, p0, LX/Bo9;->c:LX/1dt;

    iget-object v6, v0, LX/1dt;->z:LX/0bH;

    new-instance v0, LX/1Nd;

    iget-object v1, p0, LX/Bo9;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/Bo9;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->al()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    sget-object v4, Lcom/facebook/graphql/enums/StoryVisibility;->VISIBLE:Lcom/facebook/graphql/enums/StoryVisibility;

    iget-object v5, p0, LX/Bo9;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStory;->H_()I

    move-result v5

    invoke-direct/range {v0 .. v5}, LX/1Nd;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/StoryVisibility;I)V

    invoke-virtual {v6, v0}, LX/0b4;->a(LX/0b7;)V

    .line 1821560
    iget-object v0, p0, LX/Bo9;->c:LX/1dt;

    iget-object v0, v0, LX/1dt;->z:LX/0bH;

    new-instance v1, LX/1YM;

    invoke-direct {v1}, LX/1YM;-><init>()V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1821561
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1821562
    iget-object v0, p0, LX/Bo9;->c:LX/1dt;

    iget-object v0, v0, LX/1dt;->B:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f08105e    # 1.8086E38f

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 1821563
    iget-object v0, p0, LX/Bo9;->c:LX/1dt;

    iget-object v0, v0, LX/1dt;->z:LX/0bH;

    new-instance v1, LX/6Vu;

    invoke-direct {v1}, LX/6Vu;-><init>()V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1821564
    iget-object v0, p0, LX/Bo9;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/17E;->l(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1821565
    iget-object v0, p0, LX/Bo9;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-static {v0}, LX/36q;->b(Lcom/facebook/graphql/model/GraphQLMedia;)Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v0

    .line 1821566
    iget-object v1, p0, LX/Bo9;->b:Landroid/content/Context;

    iget-object v2, p0, LX/Bo9;->c:LX/1dt;

    iget-object v2, v2, LX/1dt;->y:LX/0Or;

    invoke-static {v0, v1, v2}, LX/3In;->a(Lcom/facebook/graphql/model/GraphQLVideo;Landroid/content/Context;LX/0Or;)V

    .line 1821567
    :cond_0
    return-void
.end method
