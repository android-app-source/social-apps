.class public final LX/Adm;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7ll;


# instance fields
.field public final synthetic a:LX/Ae2;


# direct methods
.method public constructor <init>(LX/Ae2;)V
    .locals 0

    .prologue
    .line 1695545
    iput-object p1, p0, LX/Adm;->a:LX/Ae2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/composer/publish/common/PendingStory;)V
    .locals 0

    .prologue
    .line 1695551
    return-void
.end method

.method public final a(Lcom/facebook/composer/publish/common/PublishPostParams;)V
    .locals 3

    .prologue
    .line 1695552
    iget-object v0, p0, LX/Adm;->a:LX/Ae2;

    iget-object v0, v0, LX/Ae2;->o:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f081412

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 1695553
    iget-object v0, p0, LX/Adm;->a:LX/Ae2;

    const-string v1, "quick_share_publish_start"

    invoke-static {v0, v1}, LX/Ae2;->a$redex0(LX/Ae2;Ljava/lang/String;)V

    .line 1695554
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/composer/publish/common/PendingStory;)V
    .locals 2

    .prologue
    .line 1695549
    iget-object v0, p0, LX/Adm;->a:LX/Ae2;

    const-string v1, "quick_share_publish_success"

    invoke-static {v0, v1}, LX/Ae2;->a$redex0(LX/Ae2;Ljava/lang/String;)V

    .line 1695550
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/composer/publish/common/PendingStory;Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 4

    .prologue
    .line 1695546
    iget-object v0, p0, LX/Adm;->a:LX/Ae2;

    iget-object v0, v0, LX/Ae2;->t:LX/03V;

    const-string v1, "live_feedback_quick_share_publish_failure"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Quick share now failed, videoId: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/Adm;->a:LX/Ae2;

    iget-object v3, v3, LX/Ae2;->W:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1695547
    iget-object v0, p0, LX/Adm;->a:LX/Ae2;

    const-string v1, "quick_share_publish_failure"

    invoke-static {v0, v1}, LX/Ae2;->a$redex0(LX/Ae2;Ljava/lang/String;)V

    .line 1695548
    return-void
.end method
