.class public final LX/BWD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/BWC;


# instance fields
.field public final synthetic a:LX/BWG;


# direct methods
.method public constructor <init>(LX/BWG;)V
    .locals 0

    .prologue
    .line 1791762
    iput-object p1, p0, LX/BWD;->a:LX/BWG;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/facebook/webview/FacebookWebView;LX/BWN;)V
    .locals 7

    .prologue
    .line 1791763
    iget-object v0, p2, Lcom/facebook/webview/FacebookWebView;->k:Ljava/lang/String;

    move-object v0, v0

    .line 1791764
    const-string v1, "callId"

    invoke-interface {p3, v0, v1}, LX/BWN;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1791765
    iget-object v0, p2, Lcom/facebook/webview/FacebookWebView;->k:Ljava/lang/String;

    move-object v0, v0

    .line 1791766
    const-string v1, "exc"

    invoke-interface {p3, v0, v1}, LX/BWN;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1791767
    iget-object v0, p2, Lcom/facebook/webview/FacebookWebView;->k:Ljava/lang/String;

    move-object v0, v0

    .line 1791768
    const-string v1, "retval"

    invoke-interface {p3, v0, v1}, LX/BWN;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1791769
    const-string v0, "null"

    invoke-static {v0, v3}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    move v1, v0

    .line 1791770
    :goto_0
    if-eqz v1, :cond_0

    .line 1791771
    iget-object v0, p0, LX/BWD;->a:LX/BWG;

    iget-object v0, v0, LX/BWG;->c:Ljava/lang/Class;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Exception was returned by js: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 1791772
    :cond_0
    iget-object v3, p0, LX/BWD;->a:LX/BWG;

    monitor-enter v3

    .line 1791773
    :try_start_0
    iget-object v0, p0, LX/BWD;->a:LX/BWG;

    iget-object v0, v0, LX/BWG;->e:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/44w;

    .line 1791774
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1791775
    if-eqz v0, :cond_3

    .line 1791776
    iget-object v0, v0, LX/44w;->b:Ljava/lang/Object;

    check-cast v0, LX/BWL;

    .line 1791777
    if-eqz v0, :cond_1

    .line 1791778
    invoke-interface {v0, p2, v2, v1, v4}, LX/BWL;->a(Lcom/facebook/webview/FacebookWebView;Ljava/lang/String;ZLjava/lang/String;)V

    .line 1791779
    :cond_1
    :goto_1
    return-void

    .line 1791780
    :cond_2
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0

    .line 1791781
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 1791782
    :cond_3
    iget-object v0, p0, LX/BWD;->a:LX/BWG;

    iget-object v0, v0, LX/BWG;->c:Ljava/lang/Class;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "js called native_return with callId "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " but no call with that callId was made."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    goto :goto_1
.end method
