.class public LX/B8o;
.super Landroid/text/style/BulletSpan;
.source ""


# static fields
.field private static d:Landroid/graphics/Path;


# instance fields
.field private final a:I

.field private final b:Z

.field private final c:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1750642
    const/4 v0, 0x0

    sput-object v0, LX/B8o;->d:Landroid/graphics/Path;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1750637
    invoke-direct {p0, p1}, Landroid/text/style/BulletSpan;-><init>(I)V

    .line 1750638
    iput p1, p0, LX/B8o;->a:I

    .line 1750639
    iput-boolean v0, p0, LX/B8o;->b:Z

    .line 1750640
    iput v0, p0, LX/B8o;->c:I

    .line 1750641
    return-void
.end method


# virtual methods
.method public final drawLeadingMargin(Landroid/graphics/Canvas;Landroid/graphics/Paint;IIIIILjava/lang/CharSequence;IIZLandroid/text/Layout;)V
    .locals 8

    .prologue
    .line 1750617
    check-cast p8, Landroid/text/Spanned;

    move-object/from16 v0, p8

    invoke-interface {v0, p0}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    move-result v1

    move/from16 v0, p9

    if-ne v1, v0, :cond_3

    .line 1750618
    invoke-virtual {p2}, Landroid/graphics/Paint;->getStyle()Landroid/graphics/Paint$Style;

    move-result-object v2

    .line 1750619
    const/4 v1, 0x0

    .line 1750620
    iget-boolean v3, p0, LX/B8o;->b:Z

    if-eqz v3, :cond_0

    .line 1750621
    invoke-virtual {p2}, Landroid/graphics/Paint;->getColor()I

    move-result v1

    .line 1750622
    iget v3, p0, LX/B8o;->c:I

    invoke-virtual {p2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 1750623
    :cond_0
    sget-object v3, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {p2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1750624
    invoke-virtual {p1}, Landroid/graphics/Canvas;->isHardwareAccelerated()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1750625
    sget-object v3, LX/B8o;->d:Landroid/graphics/Path;

    if-nez v3, :cond_1

    .line 1750626
    new-instance v3, Landroid/graphics/Path;

    invoke-direct {v3}, Landroid/graphics/Path;-><init>()V

    .line 1750627
    sput-object v3, LX/B8o;->d:Landroid/graphics/Path;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const v6, 0x40e66667    # 7.2000003f

    sget-object v7, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/graphics/Path;->addCircle(FFFLandroid/graphics/Path$Direction;)V

    .line 1750628
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 1750629
    mul-int/lit8 v3, p4, 0x6

    add-int/2addr v3, p3

    int-to-float v3, v3

    add-int v4, p5, p7

    int-to-float v4, v4

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1750630
    sget-object v3, LX/B8o;->d:Landroid/graphics/Path;

    invoke-virtual {p1, v3, p2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 1750631
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 1750632
    :goto_0
    iget-boolean v3, p0, LX/B8o;->b:Z

    if-eqz v3, :cond_2

    .line 1750633
    invoke-virtual {p2, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1750634
    :cond_2
    invoke-virtual {p2, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1750635
    :cond_3
    return-void

    .line 1750636
    :cond_4
    mul-int/lit8 v3, p4, 0x6

    add-int/2addr v3, p3

    int-to-float v3, v3

    add-int v4, p5, p7

    int-to-float v4, v4

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    const/high16 v5, 0x40c00000    # 6.0f

    invoke-virtual {p1, v3, v4, v5, p2}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    goto :goto_0
.end method

.method public final getLeadingMargin(Z)I
    .locals 1

    .prologue
    .line 1750616
    iget v0, p0, LX/B8o;->a:I

    add-int/lit8 v0, v0, 0xc

    return v0
.end method
