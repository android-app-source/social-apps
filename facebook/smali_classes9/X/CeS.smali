.class public final LX/CeS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentInterfaces$GravitySettingsGraphQlFragment;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/CeT;


# direct methods
.method public constructor <init>(LX/CeT;)V
    .locals 0

    .prologue
    .line 1924649
    iput-object p1, p0, LX/CeS;->a:LX/CeT;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 1924653
    iget-object v0, p0, LX/CeS;->a:LX/CeT;

    iget-object v0, v0, LX/CeT;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "place_tips_settings_update"

    const-string v2, "Failed to update gravity settings"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1924654
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 1924650
    check-cast p1, Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;

    .line 1924651
    iget-object v0, p0, LX/CeS;->a:LX/CeT;

    iget-object v0, v0, LX/CeT;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2cy;

    invoke-virtual {v0, p1}, LX/2cy;->a(Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;)V

    .line 1924652
    return-void
.end method
