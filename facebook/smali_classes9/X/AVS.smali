.class public final enum LX/AVS;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/AVS;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/AVS;

.field public static final enum BACK_BUTTON:LX/AVS;

.field public static final enum DELETE_BUTTON:LX/AVS;

.field public static final enum POST_BUTTON:LX/AVS;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1679732
    new-instance v0, LX/AVS;

    const-string v1, "POST_BUTTON"

    invoke-direct {v0, v1, v2}, LX/AVS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AVS;->POST_BUTTON:LX/AVS;

    .line 1679733
    new-instance v0, LX/AVS;

    const-string v1, "DELETE_BUTTON"

    invoke-direct {v0, v1, v3}, LX/AVS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AVS;->DELETE_BUTTON:LX/AVS;

    .line 1679734
    new-instance v0, LX/AVS;

    const-string v1, "BACK_BUTTON"

    invoke-direct {v0, v1, v4}, LX/AVS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/AVS;->BACK_BUTTON:LX/AVS;

    .line 1679735
    const/4 v0, 0x3

    new-array v0, v0, [LX/AVS;

    sget-object v1, LX/AVS;->POST_BUTTON:LX/AVS;

    aput-object v1, v0, v2

    sget-object v1, LX/AVS;->DELETE_BUTTON:LX/AVS;

    aput-object v1, v0, v3

    sget-object v1, LX/AVS;->BACK_BUTTON:LX/AVS;

    aput-object v1, v0, v4

    sput-object v0, LX/AVS;->$VALUES:[LX/AVS;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1679736
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/AVS;
    .locals 1

    .prologue
    .line 1679737
    const-class v0, LX/AVS;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/AVS;

    return-object v0
.end method

.method public static values()[LX/AVS;
    .locals 1

    .prologue
    .line 1679738
    sget-object v0, LX/AVS;->$VALUES:[LX/AVS;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/AVS;

    return-object v0
.end method
