.class public final LX/C2b;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2yN;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/2yN",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/C2c;


# direct methods
.method public constructor <init>(LX/C2c;)V
    .locals 0

    .prologue
    .line 1844436
    iput-object p1, p0, LX/C2b;->a:LX/C2c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Landroid/view/View;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 2

    .prologue
    .line 1844437
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1844438
    const v0, 0x7f0d006a

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 1844439
    invoke-static {p1}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 1844440
    if-nez v1, :cond_0

    .line 1844441
    const/4 v0, 0x0

    .line 1844442
    :goto_0
    return-object v0

    .line 1844443
    :cond_0
    iget-object v0, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1844444
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->v()Z

    move-result v0

    invoke-static {v1}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v1

    invoke-static {v0, v1}, LX/17Q;->a(ZLX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    goto :goto_0
.end method
