.class public final LX/BtV;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;)V
    .locals 0

    .prologue
    .line 1829285
    iput-object p1, p0, LX/BtV;->a:Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 1829286
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1829287
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1829288
    if-eqz p1, :cond_0

    invoke-static {p1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    invoke-static {v0}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v0

    .line 1829289
    :goto_0
    iget-object v1, p0, LX/BtV;->a:Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;

    invoke-virtual {v1, p1, v0}, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->a(Lcom/facebook/graphql/model/FeedUnit;LX/162;)V

    .line 1829290
    iget-object v0, p0, LX/BtV;->a:Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;

    invoke-static {v0}, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->y(Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;)V

    .line 1829291
    return-void

    .line 1829292
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
