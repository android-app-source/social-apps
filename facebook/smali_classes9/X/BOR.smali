.class public final enum LX/BOR;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/BOR;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/BOR;

.field public static final enum PICKER_NUX_DISMISS:LX/BOR;

.field public static final enum PICKER_NUX_SEEN:LX/BOR;

.field public static final enum SLIDESHOW_DESELECTED:LX/BOR;

.field public static final enum SLIDESHOW_POSTED:LX/BOR;

.field public static final enum SLIDESHOW_PREVIEW_PLAY:LX/BOR;

.field public static final enum SLIDESHOW_PREVIEW_REORDER:LX/BOR;

.field public static final enum SLIDESHOW_PREVIEW_SAVED:LX/BOR;

.field public static final enum SLIDESHOW_PREVIEW_SEEN:LX/BOR;

.field public static final enum SLIDESHOW_PREVIEW_STOP:LX/BOR;

.field public static final enum SLIDESHOW_REMOVED:LX/BOR;

.field public static final enum SLIDESHOW_SELECTED:LX/BOR;


# instance fields
.field private final name:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1779809
    new-instance v0, LX/BOR;

    const-string v1, "PICKER_NUX_SEEN"

    const-string v2, "slideshow_picker_nux_seen"

    invoke-direct {v0, v1, v4, v2}, LX/BOR;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BOR;->PICKER_NUX_SEEN:LX/BOR;

    .line 1779810
    new-instance v0, LX/BOR;

    const-string v1, "PICKER_NUX_DISMISS"

    const-string v2, "slideshow_picker_nux_dismissed"

    invoke-direct {v0, v1, v5, v2}, LX/BOR;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BOR;->PICKER_NUX_DISMISS:LX/BOR;

    .line 1779811
    new-instance v0, LX/BOR;

    const-string v1, "SLIDESHOW_SELECTED"

    const-string v2, "intent_slideshow"

    invoke-direct {v0, v1, v6, v2}, LX/BOR;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BOR;->SLIDESHOW_SELECTED:LX/BOR;

    .line 1779812
    new-instance v0, LX/BOR;

    const-string v1, "SLIDESHOW_DESELECTED"

    const-string v2, "cancel_slideshow"

    invoke-direct {v0, v1, v7, v2}, LX/BOR;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BOR;->SLIDESHOW_DESELECTED:LX/BOR;

    .line 1779813
    new-instance v0, LX/BOR;

    const-string v1, "SLIDESHOW_PREVIEW_SEEN"

    const-string v2, "slideshow_preview_seen"

    invoke-direct {v0, v1, v8, v2}, LX/BOR;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BOR;->SLIDESHOW_PREVIEW_SEEN:LX/BOR;

    .line 1779814
    new-instance v0, LX/BOR;

    const-string v1, "SLIDESHOW_PREVIEW_PLAY"

    const/4 v2, 0x5

    const-string v3, "slideshow_preview_play"

    invoke-direct {v0, v1, v2, v3}, LX/BOR;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BOR;->SLIDESHOW_PREVIEW_PLAY:LX/BOR;

    .line 1779815
    new-instance v0, LX/BOR;

    const-string v1, "SLIDESHOW_PREVIEW_STOP"

    const/4 v2, 0x6

    const-string v3, "slideshow_preview_stop"

    invoke-direct {v0, v1, v2, v3}, LX/BOR;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BOR;->SLIDESHOW_PREVIEW_STOP:LX/BOR;

    .line 1779816
    new-instance v0, LX/BOR;

    const-string v1, "SLIDESHOW_PREVIEW_REORDER"

    const/4 v2, 0x7

    const-string v3, "slideshow_preview_reorder"

    invoke-direct {v0, v1, v2, v3}, LX/BOR;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BOR;->SLIDESHOW_PREVIEW_REORDER:LX/BOR;

    .line 1779817
    new-instance v0, LX/BOR;

    const-string v1, "SLIDESHOW_PREVIEW_SAVED"

    const/16 v2, 0x8

    const-string v3, "slideshow_preview_save"

    invoke-direct {v0, v1, v2, v3}, LX/BOR;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BOR;->SLIDESHOW_PREVIEW_SAVED:LX/BOR;

    .line 1779818
    new-instance v0, LX/BOR;

    const-string v1, "SLIDESHOW_REMOVED"

    const/16 v2, 0x9

    const-string v3, "slideshow_removed"

    invoke-direct {v0, v1, v2, v3}, LX/BOR;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BOR;->SLIDESHOW_REMOVED:LX/BOR;

    .line 1779819
    new-instance v0, LX/BOR;

    const-string v1, "SLIDESHOW_POSTED"

    const/16 v2, 0xa

    const-string v3, "post_slideshow"

    invoke-direct {v0, v1, v2, v3}, LX/BOR;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/BOR;->SLIDESHOW_POSTED:LX/BOR;

    .line 1779820
    const/16 v0, 0xb

    new-array v0, v0, [LX/BOR;

    sget-object v1, LX/BOR;->PICKER_NUX_SEEN:LX/BOR;

    aput-object v1, v0, v4

    sget-object v1, LX/BOR;->PICKER_NUX_DISMISS:LX/BOR;

    aput-object v1, v0, v5

    sget-object v1, LX/BOR;->SLIDESHOW_SELECTED:LX/BOR;

    aput-object v1, v0, v6

    sget-object v1, LX/BOR;->SLIDESHOW_DESELECTED:LX/BOR;

    aput-object v1, v0, v7

    sget-object v1, LX/BOR;->SLIDESHOW_PREVIEW_SEEN:LX/BOR;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/BOR;->SLIDESHOW_PREVIEW_PLAY:LX/BOR;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/BOR;->SLIDESHOW_PREVIEW_STOP:LX/BOR;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/BOR;->SLIDESHOW_PREVIEW_REORDER:LX/BOR;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/BOR;->SLIDESHOW_PREVIEW_SAVED:LX/BOR;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/BOR;->SLIDESHOW_REMOVED:LX/BOR;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/BOR;->SLIDESHOW_POSTED:LX/BOR;

    aput-object v2, v0, v1

    sput-object v0, LX/BOR;->$VALUES:[LX/BOR;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1779806
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1779807
    iput-object p3, p0, LX/BOR;->name:Ljava/lang/String;

    .line 1779808
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/BOR;
    .locals 1

    .prologue
    .line 1779805
    const-class v0, LX/BOR;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/BOR;

    return-object v0
.end method

.method public static values()[LX/BOR;
    .locals 1

    .prologue
    .line 1779803
    sget-object v0, LX/BOR;->$VALUES:[LX/BOR;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/BOR;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1779804
    iget-object v0, p0, LX/BOR;->name:Ljava/lang/String;

    return-object v0
.end method
