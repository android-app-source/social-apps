.class public final LX/CHy;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    .line 1871391
    const/4 v10, 0x0

    .line 1871392
    const/4 v9, 0x0

    .line 1871393
    const/4 v8, 0x0

    .line 1871394
    const/4 v7, 0x0

    .line 1871395
    const/4 v6, 0x0

    .line 1871396
    const/4 v5, 0x0

    .line 1871397
    const/4 v4, 0x0

    .line 1871398
    const/4 v3, 0x0

    .line 1871399
    const/4 v2, 0x0

    .line 1871400
    const/4 v1, 0x0

    .line 1871401
    const/4 v0, 0x0

    .line 1871402
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->START_OBJECT:LX/15z;

    if-eq v11, v12, :cond_1

    .line 1871403
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1871404
    const/4 v0, 0x0

    .line 1871405
    :goto_0
    return v0

    .line 1871406
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 1871407
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_c

    .line 1871408
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 1871409
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 1871410
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_1

    if-eqz v11, :cond_1

    .line 1871411
    const-string v12, "__type__"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_2

    const-string v12, "__typename"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_3

    .line 1871412
    :cond_2
    invoke-static {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v10

    invoke-virtual {p1, v10}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v10

    goto :goto_1

    .line 1871413
    :cond_3
    const-string v12, "action_type"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_4

    .line 1871414
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v9

    goto :goto_1

    .line 1871415
    :cond_4
    const-string v12, "notice_style"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 1871416
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPostActionNoticeStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLInstantShoppingPostActionNoticeStyle;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v8

    goto :goto_1

    .line 1871417
    :cond_5
    const-string v12, "notice_text"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 1871418
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 1871419
    :cond_6
    const-string v12, "options"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_7

    .line 1871420
    invoke-static {p0, p1}, LX/CIH;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 1871421
    :cond_7
    const-string v12, "post_uri"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_8

    .line 1871422
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto/16 :goto_1

    .line 1871423
    :cond_8
    const-string v12, "share_uri"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_9

    .line 1871424
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto/16 :goto_1

    .line 1871425
    :cond_9
    const-string v12, "should_reload"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_a

    .line 1871426
    const/4 v0, 0x1

    .line 1871427
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v3

    goto/16 :goto_1

    .line 1871428
    :cond_a
    const-string v12, "target_uri"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_b

    .line 1871429
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto/16 :goto_1

    .line 1871430
    :cond_b
    const-string v12, "title"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 1871431
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    goto/16 :goto_1

    .line 1871432
    :cond_c
    const/16 v11, 0xa

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 1871433
    const/4 v11, 0x0

    invoke-virtual {p1, v11, v10}, LX/186;->b(II)V

    .line 1871434
    const/4 v10, 0x1

    invoke-virtual {p1, v10, v9}, LX/186;->b(II)V

    .line 1871435
    const/4 v9, 0x2

    invoke-virtual {p1, v9, v8}, LX/186;->b(II)V

    .line 1871436
    const/4 v8, 0x3

    invoke-virtual {p1, v8, v7}, LX/186;->b(II)V

    .line 1871437
    const/4 v7, 0x4

    invoke-virtual {p1, v7, v6}, LX/186;->b(II)V

    .line 1871438
    const/4 v6, 0x5

    invoke-virtual {p1, v6, v5}, LX/186;->b(II)V

    .line 1871439
    const/4 v5, 0x6

    invoke-virtual {p1, v5, v4}, LX/186;->b(II)V

    .line 1871440
    if-eqz v0, :cond_d

    .line 1871441
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v3}, LX/186;->a(IZ)V

    .line 1871442
    :cond_d
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1871443
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1871444
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1871348
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1871349
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 1871350
    if-eqz v0, :cond_0

    .line 1871351
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1871352
    invoke-static {p0, p1, v1, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1871353
    :cond_0
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1871354
    if-eqz v0, :cond_1

    .line 1871355
    const-string v0, "action_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1871356
    invoke-virtual {p0, p1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1871357
    :cond_1
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 1871358
    if-eqz v0, :cond_2

    .line 1871359
    const-string v0, "notice_style"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1871360
    invoke-virtual {p0, p1, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1871361
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1871362
    if-eqz v0, :cond_3

    .line 1871363
    const-string v1, "notice_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1871364
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1871365
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1871366
    if-eqz v0, :cond_4

    .line 1871367
    const-string v1, "options"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1871368
    invoke-static {p0, v0, p2, p3}, LX/CIH;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1871369
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1871370
    if-eqz v0, :cond_5

    .line 1871371
    const-string v1, "post_uri"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1871372
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1871373
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1871374
    if-eqz v0, :cond_6

    .line 1871375
    const-string v1, "share_uri"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1871376
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1871377
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1871378
    if-eqz v0, :cond_7

    .line 1871379
    const-string v1, "should_reload"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1871380
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1871381
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1871382
    if-eqz v0, :cond_8

    .line 1871383
    const-string v1, "target_uri"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1871384
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1871385
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1871386
    if-eqz v0, :cond_9

    .line 1871387
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1871388
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1871389
    :cond_9
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1871390
    return-void
.end method
