.class public final LX/CT9;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/CTC;


# direct methods
.method public constructor <init>(LX/CTC;)V
    .locals 0

    .prologue
    .line 1895354
    iput-object p1, p0, LX/CT9;->a:LX/CTC;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1895355
    iget-object v0, p0, LX/CT9;->a:LX/CTC;

    invoke-static {v0}, LX/CTC;->n(LX/CTC;)V

    .line 1895356
    return-void
.end method

.method public final a(LX/0Px;Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformPartialScreenQueryModel;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformPartialScreenQueryModel;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1895357
    invoke-static {}, LX/CSv;->a()LX/CSr;

    move-result-object v4

    .line 1895358
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1895359
    :try_start_0
    invoke-virtual {p2}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformPartialScreenQueryModel;->a()LX/2uF;

    move-result-object v0

    invoke-virtual {v0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v6}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v7, v0, LX/1vs;->a:LX/15i;

    iget v8, v0, LX/1vs;->b:I

    .line 1895360
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 1895361
    sget-object v1, LX/CTA;->d:[I

    const/4 v0, 0x0

    const-class v3, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;

    invoke-virtual {v7, v8, v0, v3}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;->j()Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 1895362
    const/4 v0, 0x0

    const-class v1, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;

    invoke-virtual {v7, v8, v0, v1}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;

    const/4 v1, 0x0

    invoke-interface {v4, v0, v1}, LX/CSr;->a(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;Ljava/lang/String;)LX/CTJ;

    move-result-object v0

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1895363
    :cond_0
    new-instance v0, LX/3rL;

    const/4 v1, 0x1

    invoke-virtual {v7, v8, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v9}, LX/3rL;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1895364
    :catch_0
    move-exception v0

    .line 1895365
    iget-object v1, p0, LX/CT9;->a:LX/CTC;

    iget-object v1, v1, LX/CTC;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-static {v0}, LX/CXd;->a(Ljava/lang/Exception;)V

    .line 1895366
    invoke-virtual {p0}, LX/CT9;->a()V

    .line 1895367
    :goto_1
    return-void

    .line 1895368
    :pswitch_0
    const/4 v0, 0x0

    :try_start_1
    const-class v1, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;

    invoke-virtual {v7, v8, v0, v1}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;->K()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const/4 v3, 0x0

    const-class v10, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ContainerElementFragmentModel$ContainerModel$ElementsModel;

    invoke-virtual {v1, v0, v3, v10}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    move-object v3, v0

    :goto_2
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v10

    move v1, v2

    .line 1895369
    :goto_3
    if-ge v1, v10, :cond_0

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ContainerElementFragmentModel$ContainerModel$ElementsModel;

    .line 1895370
    invoke-static {v0}, LX/CVz;->a(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ContainerElementFragmentModel$ContainerModel$ElementsModel;)Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;->a(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;)Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;

    move-result-object v11

    const/4 v0, 0x0

    const-class v12, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;

    invoke-virtual {v7, v8, v0, v12}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;->o()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v4, v11, v0}, LX/CSr;->a(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;Ljava/lang/String;)LX/CTJ;

    move-result-object v0

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1895371
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 1895372
    :cond_1
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1895373
    move-object v3, v0

    goto :goto_2

    .line 1895374
    :cond_2
    iget-object v0, p0, LX/CT9;->a:LX/CTC;

    iget-object v0, v0, LX/CTC;->w:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, Lcom/facebook/pages/common/platform/infra/PlatformSessionController$8$1;

    invoke-direct {v1, p0, v5, p1}, Lcom/facebook/pages/common/platform/infra/PlatformSessionController$8$1;-><init>(LX/CT9;Ljava/util/ArrayList;LX/0Px;)V

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v7/widget/RecyclerView;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method
