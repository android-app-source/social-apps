.class public LX/ATs;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/ASn;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "LX/0io;",
        ":",
        "LX/0iv;",
        ":",
        "LX/0j3;",
        "Services::",
        "LX/0il",
        "<TModelData;>;>",
        "Ljava/lang/Object;",
        "LX/ASn;",
        "Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView$Listener;",
        "Lcom/facebook/friendsharing/videotagging/VideoTaggingManager$Listener;"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public A:I

.field public B:I

.field public final b:LX/ATX;

.field public final c:LX/0Zb;

.field private final d:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TServices;>;"
        }
    .end annotation
.end field

.field private final e:LX/ASg;

.field public final f:LX/0ad;

.field private final g:Landroid/content/Context;

.field public final h:LX/ATL;

.field private final i:Landroid/view/inputmethod/InputMethodManager;

.field public final j:LX/BTG;

.field public final k:LX/BSu;

.field public final l:LX/0gc;

.field public final m:Ljava/lang/String;

.field private final n:LX/ATR;

.field private final o:LX/Azd;

.field private final p:LX/7l0;

.field public final q:Ljava/util/concurrent/ExecutorService;

.field public final r:Landroid/os/Handler;

.field private final s:Ljava/lang/Runnable;

.field public t:Ljava/util/concurrent/Future;

.field public u:Ljava/lang/Runnable;

.field public v:Lcom/facebook/composer/attachments/ComposerAttachment;

.field public w:Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;

.field public x:Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;

.field public y:LX/BSt;

.field public z:LX/Azc;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1677323
    const-class v0, LX/ATs;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/ATs;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Zb;LX/ASg;Landroid/view/inputmethod/InputMethodManager;LX/BTG;LX/ATR;LX/0ad;Landroid/content/Context;LX/Azd;LX/7l0;LX/BSu;Ljava/util/concurrent/ExecutorService;Landroid/os/Handler;LX/0il;LX/ATL;LX/0gc;Ljava/lang/String;)V
    .locals 4
    .param p11    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
        .end annotation
    .end param
    .param p12    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p13    # LX/0il;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p14    # LX/ATL;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p15    # LX/0gc;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p16    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Zb;",
            "LX/ASg;",
            "Landroid/view/inputmethod/InputMethodManager;",
            "LX/BTG;",
            "LX/ATR;",
            "LX/0ad;",
            "Landroid/content/Context;",
            "LX/Azd;",
            "LX/7l0;",
            "LX/BSu;",
            "Ljava/util/concurrent/ExecutorService;",
            "Landroid/os/Handler;",
            "TServices;",
            "Lcom/facebook/composer/ui/underwood/AttachmentsEventListener;",
            "LX/0gc;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1677299
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1677300
    new-instance v1, LX/ATr;

    invoke-direct {v1, p0}, LX/ATr;-><init>(LX/ATs;)V

    iput-object v1, p0, LX/ATs;->b:LX/ATX;

    .line 1677301
    iput-object p1, p0, LX/ATs;->c:LX/0Zb;

    .line 1677302
    iput-object p2, p0, LX/ATs;->e:LX/ASg;

    .line 1677303
    iput-object p3, p0, LX/ATs;->i:Landroid/view/inputmethod/InputMethodManager;

    .line 1677304
    iput-object p4, p0, LX/ATs;->j:LX/BTG;

    .line 1677305
    iput-object p5, p0, LX/ATs;->n:LX/ATR;

    .line 1677306
    iput-object p6, p0, LX/ATs;->f:LX/0ad;

    .line 1677307
    iput-object p7, p0, LX/ATs;->g:Landroid/content/Context;

    .line 1677308
    iput-object p8, p0, LX/ATs;->o:LX/Azd;

    .line 1677309
    iput-object p9, p0, LX/ATs;->p:LX/7l0;

    .line 1677310
    iput-object p10, p0, LX/ATs;->k:LX/BSu;

    .line 1677311
    iput-object p11, p0, LX/ATs;->q:Ljava/util/concurrent/ExecutorService;

    .line 1677312
    move-object/from16 v0, p12

    iput-object v0, p0, LX/ATs;->r:Landroid/os/Handler;

    .line 1677313
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-static/range {p13 .. p13}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v1, p0, LX/ATs;->d:Ljava/lang/ref/WeakReference;

    .line 1677314
    move-object/from16 v0, p14

    iput-object v0, p0, LX/ATs;->h:LX/ATL;

    .line 1677315
    move-object/from16 v0, p15

    iput-object v0, p0, LX/ATs;->l:LX/0gc;

    .line 1677316
    move-object/from16 v0, p16

    iput-object v0, p0, LX/ATs;->m:Ljava/lang/String;

    .line 1677317
    new-instance v1, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;

    iget-object v2, p0, LX/ATs;->g:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, LX/ATs;->w:Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;

    .line 1677318
    iget-object v1, p0, LX/ATs;->f:LX/0ad;

    sget-short v2, LX/1EB;->V:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1677319
    iget-object v1, p0, LX/ATs;->w:Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;

    invoke-virtual {v1}, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->e()V

    .line 1677320
    :cond_0
    iget-object v1, p0, LX/ATs;->w:Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;

    invoke-virtual {v1, p0}, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->setListener(LX/ATs;)V

    .line 1677321
    new-instance v1, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentViewController$2;

    invoke-direct {v1, p0}, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentViewController$2;-><init>(LX/ATs;)V

    iput-object v1, p0, LX/ATs;->s:Ljava/lang/Runnable;

    .line 1677322
    return-void
.end method

.method public static synthetic a(LX/ATs;Lcom/facebook/composer/attachments/ComposerAttachment;)J
    .locals 6

    .prologue
    .line 1677295
    invoke-virtual {p1}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v2

    check-cast v2, Lcom/facebook/photos/base/media/VideoItem;

    .line 1677296
    iget-wide v4, v2, Lcom/facebook/photos/base/media/VideoItem;->d:J

    move-wide v2, v4

    .line 1677297
    move-wide v0, v2

    .line 1677298
    return-wide v0
.end method

.method public static e(LX/ATs;Lcom/facebook/composer/attachments/ComposerAttachment;)F
    .locals 2

    .prologue
    .line 1677294
    iget-object v0, p0, LX/ATs;->e:LX/ASg;

    invoke-virtual {p1}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/ASg;->a(Lcom/facebook/ipc/media/MediaItem;)F

    move-result v0

    return v0
.end method

.method public static o(LX/ATs;)V
    .locals 3

    .prologue
    .line 1677291
    iget-object v0, p0, LX/ATs;->w:Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;

    if-eqz v0, :cond_0

    .line 1677292
    iget-object v0, p0, LX/ATs;->i:Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, LX/ATs;->w:Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;

    invoke-virtual {v1}, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1677293
    :cond_0
    return-void
.end method

.method public static p(LX/ATs;)V
    .locals 11

    .prologue
    .line 1677252
    iget-object v0, p0, LX/ATs;->v:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/base/media/VideoItem;

    .line 1677253
    iget-wide v5, v0, Lcom/facebook/photos/base/media/VideoItem;->d:J

    move-wide v0, v5

    .line 1677254
    long-to-int v0, v0

    .line 1677255
    iget-object v1, p0, LX/ATs;->w:Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;

    iget-object v2, p0, LX/ATs;->v:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v2}, Lcom/facebook/composer/attachments/ComposerAttachment;->c()Landroid/net/Uri;

    move-result-object v2

    iget-object v3, p0, LX/ATs;->v:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v3}, Lcom/facebook/composer/attachments/ComposerAttachment;->f()Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    move-result-object v3

    iget v4, p0, LX/ATs;->A:I

    const/4 v9, 0x1

    const/4 v7, 0x0

    .line 1677256
    iget-object v5, v1, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->d:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v6, LX/04D;->COMPOSER:LX/04D;

    invoke-virtual {v5, v6}, Lcom/facebook/video/player/RichVideoPlayer;->setPlayerOrigin(LX/04D;)V

    .line 1677257
    invoke-static {}, Lcom/facebook/video/engine/VideoDataSource;->newBuilder()LX/2oE;

    move-result-object v5

    .line 1677258
    iput-object v2, v5, LX/2oE;->a:Landroid/net/Uri;

    .line 1677259
    move-object v5, v5

    .line 1677260
    sget-object v6, LX/097;->FROM_LOCAL_STORAGE:LX/097;

    .line 1677261
    iput-object v6, v5, LX/2oE;->e:LX/097;

    .line 1677262
    move-object v5, v5

    .line 1677263
    invoke-virtual {v5}, LX/2oE;->h()Lcom/facebook/video/engine/VideoDataSource;

    move-result-object v5

    .line 1677264
    invoke-static {}, Lcom/facebook/video/engine/VideoPlayerParams;->newBuilder()LX/2oH;

    move-result-object v6

    invoke-virtual {v6, v5}, LX/2oH;->a(Lcom/facebook/video/engine/VideoDataSource;)LX/2oH;

    move-result-object v5

    .line 1677265
    iput v4, v5, LX/2oH;->p:I

    .line 1677266
    move-object v5, v5

    .line 1677267
    iput v0, v5, LX/2oH;->c:I

    .line 1677268
    move-object v5, v5

    .line 1677269
    iput-boolean v9, v5, LX/2oH;->g:Z

    .line 1677270
    move-object v5, v5

    .line 1677271
    iput-boolean v7, v5, LX/2oH;->o:Z

    .line 1677272
    move-object v5, v5

    .line 1677273
    iget-object v6, v1, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->k:Lcom/facebook/widget/FbImageView;

    if-eqz v6, :cond_0

    .line 1677274
    iget-object v6, v1, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->k:Lcom/facebook/widget/FbImageView;

    invoke-virtual {v6, v7}, Lcom/facebook/widget/FbImageView;->setVisibility(I)V

    .line 1677275
    :cond_0
    new-instance v6, LX/2pZ;

    invoke-direct {v6}, LX/2pZ;-><init>()V

    invoke-virtual {v5}, LX/2oH;->n()Lcom/facebook/video/engine/VideoPlayerParams;

    move-result-object v5

    .line 1677276
    iput-object v5, v6, LX/2pZ;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 1677277
    move-object v5, v6

    .line 1677278
    iget v6, v1, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->o:F

    float-to-double v7, v6

    .line 1677279
    iput-wide v7, v5, LX/2pZ;->e:D

    .line 1677280
    move-object v5, v5

    .line 1677281
    const-string v6, "CoverImageParamsKey"

    invoke-static {v2}, LX/1bf;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, LX/2pZ;->a(Ljava/lang/String;Ljava/lang/Object;)LX/2pZ;

    move-result-object v5

    .line 1677282
    if-eqz v3, :cond_1

    invoke-virtual {v3}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->getVideoTrimParams()Lcom/facebook/photos/creativeediting/model/VideoTrimParams;

    move-result-object v6

    if-eqz v6, :cond_1

    .line 1677283
    const-string v6, "TrimStartPosition"

    invoke-virtual {v3}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->getVideoTrimParams()Lcom/facebook/photos/creativeediting/model/VideoTrimParams;

    move-result-object v7

    iget v7, v7, Lcom/facebook/photos/creativeediting/model/VideoTrimParams;->videoTrimStartTimeMs:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, LX/2pZ;->a(Ljava/lang/String;Ljava/lang/Object;)LX/2pZ;

    .line 1677284
    const-string v6, "TrimEndPosition"

    invoke-virtual {v3}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->getVideoTrimParams()Lcom/facebook/photos/creativeediting/model/VideoTrimParams;

    move-result-object v7

    iget v7, v7, Lcom/facebook/photos/creativeediting/model/VideoTrimParams;->videoTrimEndTimeMs:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, LX/2pZ;->a(Ljava/lang/String;Ljava/lang/Object;)LX/2pZ;

    .line 1677285
    :cond_1
    iget-object v6, v1, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->d:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v5}, LX/2pZ;->b()LX/2pa;

    move-result-object v5

    invoke-virtual {v6, v5}, Lcom/facebook/video/player/RichVideoPlayer;->c(LX/2pa;)V

    .line 1677286
    iget-object v5, v1, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->d:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v6, LX/04g;->BY_PLAYER:LX/04g;

    invoke-virtual {v5, v9, v6}, Lcom/facebook/video/player/RichVideoPlayer;->a(ZLX/04g;)V

    .line 1677287
    if-eqz v3, :cond_2

    .line 1677288
    invoke-virtual {v3}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->getRotationAngle()I

    move-result v5

    iput v5, v1, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->q:I

    .line 1677289
    :cond_2
    const/4 v0, 0x0

    iput v0, p0, LX/ATs;->A:I

    .line 1677290
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1677237
    iput-object v1, p0, LX/ATs;->v:Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 1677238
    iget-object v0, p0, LX/ATs;->w:Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;

    .line 1677239
    iput-object v1, v0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->n:LX/ATs;

    .line 1677240
    iget-object v0, p0, LX/ATs;->w:Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;

    invoke-virtual {v0}, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->a()V

    .line 1677241
    iget-object v0, p0, LX/ATs;->w:Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;

    .line 1677242
    iget-object v1, v0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->i:Landroid/view/View;

    if-eqz v1, :cond_0

    .line 1677243
    iget-object v1, v0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->i:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1677244
    :cond_0
    iget-object v0, p0, LX/ATs;->w:Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;

    invoke-virtual {v0}, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->h()V

    .line 1677245
    const/4 v0, 0x0

    iput v0, p0, LX/ATs;->B:I

    .line 1677246
    iget-object v0, p0, LX/ATs;->t:Ljava/util/concurrent/Future;

    if-eqz v0, :cond_1

    .line 1677247
    iget-object v0, p0, LX/ATs;->t:Ljava/util/concurrent/Future;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 1677248
    :cond_1
    iget-object v0, p0, LX/ATs;->z:LX/Azc;

    if-eqz v0, :cond_2

    .line 1677249
    iget-object v0, p0, LX/ATs;->z:LX/Azc;

    .line 1677250
    const/4 v1, 0x0

    iput-object v1, v0, LX/Azc;->k:LX/ATs;

    .line 1677251
    :cond_2
    return-void
.end method

.method public final a(F)V
    .locals 1

    .prologue
    .line 1677231
    iget-object v0, p0, LX/ATs;->w:Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;

    .line 1677232
    iput p1, v0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->p:F

    .line 1677233
    invoke-virtual {v0, p1}, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->setScaleX(F)V

    .line 1677234
    invoke-virtual {v0, p1}, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->setScaleY(F)V

    .line 1677235
    iget-object v0, p0, LX/ATs;->w:Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;

    invoke-virtual {v0, p1}, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->setAlpha(F)V

    .line 1677236
    return-void
.end method

.method public final a(J)V
    .locals 3

    .prologue
    .line 1677229
    iget-object v0, p0, LX/ATs;->r:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentViewController$5;

    invoke-direct {v1, p0, p1, p2}, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentViewController$5;-><init>(LX/ATs;J)V

    const v2, 0x39a6dd58

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1677230
    return-void
.end method

.method public final a(LX/5L2;)V
    .locals 8

    .prologue
    .line 1677209
    sget-object v0, LX/5L2;->ON_FIRST_DRAW:LX/5L2;

    if-ne p1, v0, :cond_1

    .line 1677210
    iget-object v0, p0, LX/ATs;->v:Lcom/facebook/composer/attachments/ComposerAttachment;

    if-eqz v0, :cond_0

    .line 1677211
    invoke-static {p0}, LX/ATs;->p(LX/ATs;)V

    .line 1677212
    :cond_0
    :goto_0
    return-void

    .line 1677213
    :cond_1
    sget-object v0, LX/5L2;->ON_RESUME:LX/5L2;

    if-ne p1, v0, :cond_2

    .line 1677214
    iget-object v0, p0, LX/ATs;->w:Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->b(Z)V

    goto :goto_0

    .line 1677215
    :cond_2
    sget-object v0, LX/5L2;->ON_USER_POST:LX/5L2;

    if-eq p1, v0, :cond_3

    sget-object v0, LX/5L2;->ON_USER_CANCEL:LX/5L2;

    if-ne p1, v0, :cond_5

    :cond_3
    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 1677216
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/ATs;->z:LX/Azc;

    if-eqz v0, :cond_0

    .line 1677217
    sget-object v2, LX/5L2;->ON_USER_POST:LX/5L2;

    if-ne p1, v2, :cond_4

    .line 1677218
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v3, "video_tagging_post_event"

    invoke-direct {v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v3, "composer_session_id"

    iget-object v4, p0, LX/ATs;->m:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "face_found"

    iget-object v4, p0, LX/ATs;->z:LX/Azc;

    .line 1677219
    iget-boolean v5, v4, LX/Azc;->j:Z

    move v4, v5

    .line 1677220
    invoke-virtual {v2, v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "face_detect_finished"

    iget-object v4, p0, LX/ATs;->z:LX/Azc;

    .line 1677221
    iget-boolean v5, v4, LX/Azc;->i:Z

    move v4, v5

    .line 1677222
    invoke-virtual {v2, v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "ttf_ms"

    iget-object v4, p0, LX/ATs;->z:LX/Azc;

    .line 1677223
    iget-wide v6, v4, LX/Azc;->l:J

    move-wide v4, v6

    .line 1677224
    invoke-virtual {v2, v3, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    sget-object v3, LX/ATs;->a:Ljava/lang/String;

    .line 1677225
    iput-object v3, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1677226
    move-object v2, v2

    .line 1677227
    iget-object v3, p0, LX/ATs;->c:LX/0Zb;

    invoke-interface {v3, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1677228
    :cond_4
    iget-object v0, p0, LX/ATs;->z:LX/Azc;

    invoke-virtual {v0}, LX/Azc;->a()V

    goto :goto_0

    :cond_5
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(Lcom/facebook/composer/attachments/ComposerAttachment;)V
    .locals 14

    .prologue
    const/4 v3, 0x0

    .line 1677192
    iget-object v0, p0, LX/ATs;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 1677193
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/composer/attachments/ComposerAttachment;

    iput-object v1, p0, LX/ATs;->v:Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 1677194
    iget-object v1, p0, LX/ATs;->w:Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;

    iget-object v2, p0, LX/ATs;->v:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-static {p0, v2}, LX/ATs;->e(LX/ATs;Lcom/facebook/composer/attachments/ComposerAttachment;)F

    move-result v2

    .line 1677195
    iput v2, v1, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->o:F

    .line 1677196
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    check-cast v0, LX/0iv;

    invoke-interface {v0}, LX/0iv;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1677197
    invoke-static {p0}, LX/ATs;->p(LX/ATs;)V

    .line 1677198
    :cond_0
    iget-object v0, p0, LX/ATs;->n:LX/ATR;

    iget-object v1, p0, LX/ATs;->v:Lcom/facebook/composer/attachments/ComposerAttachment;

    iget-object v2, p0, LX/ATs;->s:Ljava/lang/Runnable;

    invoke-virtual {v0, v1, v2}, LX/ATR;->a(Lcom/facebook/composer/attachments/ComposerAttachment;Ljava/lang/Runnable;)V

    .line 1677199
    iput v3, p0, LX/ATs;->B:I

    .line 1677200
    iget-object v0, p0, LX/ATs;->f:LX/0ad;

    sget-short v1, LX/1EB;->U:S

    invoke-interface {v0, v1, v3}, LX/0ad;->a(SZ)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, LX/ATs;->f:LX/0ad;

    sget-short v1, LX/1EB;->V:S

    invoke-interface {v0, v1, v3}, LX/0ad;->a(SZ)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, LX/ATs;->f:LX/0ad;

    sget-short v1, LX/1EB;->W:S

    invoke-interface {v0, v1, v3}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1677201
    :cond_1
    iget-object v0, p0, LX/ATs;->r:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentViewController$3;

    invoke-direct {v1, p0}, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentViewController$3;-><init>(LX/ATs;)V

    const v2, -0x448ffa94

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1677202
    iget-object v0, p0, LX/ATs;->o:LX/Azd;

    iget-object v1, p0, LX/ATs;->f:LX/0ad;

    sget v2, LX/1EB;->S:I

    const/16 v3, 0x3e8

    invoke-interface {v1, v2, v3}, LX/0ad;->a(II)I

    move-result v1

    iget-object v2, p0, LX/ATs;->f:LX/0ad;

    sget v3, LX/1EB;->R:I

    const/16 v4, 0x64

    invoke-interface {v2, v3, v4}, LX/0ad;->a(II)I

    move-result v2

    .line 1677203
    new-instance v5, LX/Azc;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v6

    check-cast v6, LX/0Zb;

    const-class v7, Landroid/content/Context;

    invoke-interface {v0, v7}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/content/Context;

    invoke-static {v0}, LX/7yc;->b(LX/0QB;)LX/7yc;

    move-result-object v8

    check-cast v8, LX/7yc;

    const-class v9, LX/BV0;

    invoke-interface {v0, v9}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v9

    check-cast v9, LX/BV0;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v10

    check-cast v10, LX/0So;

    move v11, v1

    move v12, v2

    move-object v13, p0

    invoke-direct/range {v5 .. v13}, LX/Azc;-><init>(LX/0Zb;Landroid/content/Context;LX/7yc;LX/BV0;LX/0So;IILX/ATs;)V

    .line 1677204
    move-object v0, v5

    .line 1677205
    iput-object v0, p0, LX/ATs;->z:LX/Azc;

    .line 1677206
    new-instance v0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentViewController$4;

    invoke-direct {v0, p0, p1}, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentViewController$4;-><init>(LX/ATs;Lcom/facebook/composer/attachments/ComposerAttachment;)V

    iput-object v0, p0, LX/ATs;->u:Ljava/lang/Runnable;

    .line 1677207
    iget-object v0, p0, LX/ATs;->q:Ljava/util/concurrent/ExecutorService;

    iget-object v1, p0, LX/ATs;->u:Ljava/lang/Runnable;

    const v2, -0x1ef4f0a3

    invoke-static {v0, v1, v2}, LX/03X;->a(Ljava/util/concurrent/ExecutorService;Ljava/lang/Runnable;I)Ljava/util/concurrent/Future;

    move-result-object v0

    iput-object v0, p0, LX/ATs;->t:Ljava/util/concurrent/Future;

    .line 1677208
    :cond_2
    return-void
.end method

.method public final a(Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame;)V
    .locals 3

    .prologue
    .line 1677324
    iget v0, p0, LX/ATs;->B:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/ATs;->B:I

    .line 1677325
    iget-object v0, p0, LX/ATs;->r:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentViewController$6;

    invoke-direct {v1, p0, p1}, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentViewController$6;-><init>(LX/ATs;Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame;)V

    const v2, -0x4b08d114

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1677326
    return-void
.end method

.method public final a(Lcom/facebook/ipc/media/data/MediaData;Z)V
    .locals 6

    .prologue
    .line 1677155
    iget-object v0, p0, LX/ATs;->v:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->g()Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/ATs;->v:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->g()Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;->getFrames()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/ATs;->v:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->g()Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;->getFrames()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1677156
    :cond_0
    :goto_0
    return-void

    .line 1677157
    :cond_1
    iget-object v0, p0, LX/ATs;->v:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->g()Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;->getFrames()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    .line 1677158
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame;

    .line 1677159
    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame;->getMediaData()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame;->getMediaData()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v4

    .line 1677160
    iget-object v5, v4, Lcom/facebook/ipc/media/data/MediaData;->mUri:Landroid/net/Uri;

    move-object v4, v5

    .line 1677161
    if-eqz v4, :cond_2

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame;->getMediaData()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v4

    .line 1677162
    iget-object v5, v4, Lcom/facebook/ipc/media/data/MediaData;->mUri:Landroid/net/Uri;

    move-object v4, v5

    .line 1677163
    iget-object v5, p1, Lcom/facebook/ipc/media/data/MediaData;->mUri:Landroid/net/Uri;

    move-object v5, v5

    .line 1677164
    invoke-virtual {v4, v5}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1677165
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 1677166
    iget-object v2, p0, LX/ATs;->p:LX/7l0;

    new-instance v3, LX/74k;

    invoke-direct {v3}, LX/74k;-><init>()V

    new-instance v4, LX/4gN;

    invoke-direct {v4}, LX/4gN;-><init>()V

    invoke-virtual {v4, p1}, LX/4gN;->a(Lcom/facebook/ipc/media/data/MediaData;)LX/4gN;

    move-result-object v4

    invoke-virtual {v4}, LX/4gN;->a()Lcom/facebook/ipc/media/data/LocalMediaData;

    move-result-object v4

    .line 1677167
    iput-object v4, v3, LX/74k;->f:Lcom/facebook/ipc/media/data/LocalMediaData;

    .line 1677168
    move-object v3, v3

    .line 1677169
    invoke-virtual {v3}, LX/74k;->a()Lcom/facebook/photos/base/media/PhotoItem;

    move-result-object v3

    invoke-virtual {v1, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/7l0;->a(Ljava/util/List;)LX/0Px;

    move-result-object v1

    .line 1677170
    iget-object v2, p0, LX/ATs;->w:Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame;->getTimestamp()J

    move-result-wide v4

    .line 1677171
    iget-object v3, v2, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->e:LX/7OL;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x0

    :goto_2
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result p0

    .line 1677172
    iget-object p1, v3, LX/7OL;->r:Ljava/util/LinkedHashMap;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_4

    .line 1677173
    :goto_3
    goto/16 :goto_0

    .line 1677174
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1677175
    :cond_3
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/base/tagging/Tag;

    .line 1677176
    iget-object p0, v0, Lcom/facebook/photos/base/tagging/Tag;->i:Ljava/lang/String;

    move-object v0, p0

    .line 1677177
    goto :goto_2

    .line 1677178
    :cond_4
    iget-object p1, v3, LX/7OL;->r:Ljava/util/LinkedHashMap;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/facebook/video/player/plugins/VideoTaggingSeekbarIndicator;

    .line 1677179
    if-nez p0, :cond_5

    .line 1677180
    invoke-virtual {p1}, Lcom/facebook/video/player/plugins/VideoTaggingSeekbarIndicator;->a()V

    goto :goto_3

    .line 1677181
    :cond_5
    iget-object v2, p1, Lcom/facebook/video/player/plugins/VideoTaggingSeekbarIndicator;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v1, 0x0

    invoke-virtual {v2, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1677182
    iget-object v2, p1, Lcom/facebook/video/player/plugins/VideoTaggingSeekbarIndicator;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v3, Lcom/facebook/video/player/plugins/VideoTaggingSeekbarIndicator;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v1, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1677183
    const v2, 0x7f0218f8

    invoke-virtual {p1, v2}, Lcom/facebook/video/player/plugins/VideoTaggingSeekbarIndicator;->setBackgroundResource(I)V

    .line 1677184
    const/4 v1, 0x4

    const/4 v0, 0x0

    .line 1677185
    const/4 v2, 0x2

    if-ge p0, v2, :cond_6

    .line 1677186
    iget-object v2, p1, Lcom/facebook/video/player/plugins/VideoTaggingSeekbarIndicator;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v2, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1677187
    iget-object v2, p1, Lcom/facebook/video/player/plugins/VideoTaggingSeekbarIndicator;->c:Lcom/facebook/widget/FbImageView;

    invoke-virtual {v2, v1}, Lcom/facebook/widget/FbImageView;->setVisibility(I)V

    .line 1677188
    :goto_4
    goto :goto_3

    .line 1677189
    :cond_6
    iget-object v2, p1, Lcom/facebook/video/player/plugins/VideoTaggingSeekbarIndicator;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v2, v0}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1677190
    iget-object v2, p1, Lcom/facebook/video/player/plugins/VideoTaggingSeekbarIndicator;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p1}, Lcom/facebook/video/player/plugins/VideoTaggingSeekbarIndicator;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f080deb

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    add-int/lit8 v5, p0, -0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v0

    invoke-virtual {v1, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1677191
    iget-object v2, p1, Lcom/facebook/video/player/plugins/VideoTaggingSeekbarIndicator;->c:Lcom/facebook/widget/FbImageView;

    invoke-virtual {v2, v0}, Lcom/facebook/widget/FbImageView;->setVisibility(I)V

    goto :goto_4
.end method

.method public final b()Landroid/view/View;
    .locals 1

    .prologue
    .line 1677154
    iget-object v0, p0, LX/ATs;->w:Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;

    return-object v0
.end method

.method public final b(Lcom/facebook/composer/attachments/ComposerAttachment;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1677150
    iget-object v0, p0, LX/ATs;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 1677151
    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    check-cast v0, LX/0j3;

    invoke-interface {v0}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isEdit()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->m()LX/4gF;

    move-result-object v0

    sget-object v2, LX/4gF;->VIDEO:LX/4gF;

    if-ne v0, v2, :cond_0

    invoke-virtual {p1}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    invoke-static {v0}, LX/74c;->a(Lcom/facebook/ipc/media/MediaItem;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    .line 1677152
    iget-object v2, v0, Lcom/facebook/ipc/media/MediaItem;->c:Lcom/facebook/ipc/media/data/LocalMediaData;

    move-object v0, v2

    .line 1677153
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/ATs;->f:LX/0ad;

    sget-short v2, LX/1EB;->B:S

    invoke-interface {v0, v2, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final c()Lcom/facebook/composer/attachments/ComposerAttachment;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1677149
    iget-object v0, p0, LX/ATs;->v:Lcom/facebook/composer/attachments/ComposerAttachment;

    return-object v0
.end method

.method public final c(Lcom/facebook/composer/attachments/ComposerAttachment;)V
    .locals 1

    .prologue
    .line 1677147
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/attachments/ComposerAttachment;

    iput-object v0, p0, LX/ATs;->v:Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 1677148
    return-void
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 1677143
    iget-object v0, p0, LX/ATs;->w:Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;

    .line 1677144
    iget-object v1, v0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->e:LX/7OL;

    if-nez v1, :cond_0

    .line 1677145
    iget-object v1, v0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->i:Landroid/view/View;

    iget-object v2, v0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->d:Lcom/facebook/video/player/RichVideoPlayer;

    iget-object p0, v0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->l:Lcom/facebook/resources/ui/FbFrameLayout;

    invoke-static {v1, v2, p0}, LX/ATC;->a(Landroid/view/View;Landroid/view/View;Landroid/view/View;)V

    .line 1677146
    :cond_0
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 1677135
    iget-object v0, p0, LX/ATs;->w:Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;

    invoke-virtual {v0}, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1677136
    iget-object v0, p0, LX/ATs;->w:Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->b(Z)V

    .line 1677137
    :cond_0
    iget-object v0, p0, LX/ATs;->w:Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;

    .line 1677138
    iget-object v1, v0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->i:Landroid/view/View;

    if-eqz v1, :cond_1

    .line 1677139
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 1677140
    iget-object p0, v0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->i:Landroid/view/View;

    invoke-virtual {p0, v1}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1677141
    iget-object v1, v0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->b:LX/BTf;

    invoke-virtual {v1}, LX/BTf;->a()V

    .line 1677142
    :cond_1
    return-void
.end method

.method public final f()F
    .locals 1

    .prologue
    .line 1677134
    iget-object v0, p0, LX/ATs;->v:Lcom/facebook/composer/attachments/ComposerAttachment;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/ATs;->v:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-static {p0, v0}, LX/ATs;->e(LX/ATs;Lcom/facebook/composer/attachments/ComposerAttachment;)F

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()F
    .locals 1

    .prologue
    .line 1677131
    iget-object v0, p0, LX/ATs;->w:Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;

    .line 1677132
    iget p0, v0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->p:F

    move v0, p0

    .line 1677133
    return v0
.end method

.method public final h()V
    .locals 0

    .prologue
    .line 1677130
    return-void
.end method

.method public final i()V
    .locals 0

    .prologue
    .line 1677129
    return-void
.end method
