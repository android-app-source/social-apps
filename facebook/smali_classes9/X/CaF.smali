.class public final LX/CaF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;)V
    .locals 0

    .prologue
    .line 1917201
    iput-object p1, p0, LX/CaF;->a:Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    const v0, 0x1e6a53aa

    invoke-static {v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v4

    .line 1917174
    iget-object v0, p0, LX/CaF;->a:Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;

    iget-object v0, v0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->m:Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultBlingBarView;

    .line 1917175
    iget-boolean v5, v0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultBlingBarView;->l:Z

    move v0, v5

    .line 1917176
    if-nez v0, :cond_0

    .line 1917177
    const v0, 0x22e53833

    invoke-static {v3, v3, v0, v4}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1917178
    :goto_0
    return-void

    .line 1917179
    :cond_0
    iget-object v0, p0, LX/CaF;->a:Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;

    iget-object v0, v0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->n:LX/Ca6;

    .line 1917180
    iget v3, v0, LX/Ca6;->f:I

    move v0, v3

    .line 1917181
    if-lez v0, :cond_2

    move v0, v1

    .line 1917182
    :goto_1
    iget-object v3, p0, LX/CaF;->a:Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;

    iget-object v3, v3, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->n:LX/Ca6;

    .line 1917183
    iget v5, v3, LX/Ca6;->g:I

    move v3, v5

    .line 1917184
    if-lez v3, :cond_3

    move v3, v1

    .line 1917185
    :goto_2
    iget-object v5, p0, LX/CaF;->a:Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;

    iget-object v5, v5, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->n:LX/Ca6;

    .line 1917186
    iget-boolean p1, v5, LX/Ca6;->j:Z

    move v5, p1

    .line 1917187
    if-nez v5, :cond_4

    .line 1917188
    :goto_3
    iget-object v2, p0, LX/CaF;->a:Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;

    iget-object v2, v2, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->n:LX/Ca6;

    .line 1917189
    iget v5, v2, LX/Ca6;->f:I

    move v2, v5

    .line 1917190
    if-eqz v1, :cond_5

    add-int/lit8 v2, v2, 0x1

    .line 1917191
    :goto_4
    iget-object v5, p0, LX/CaF;->a:Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;

    .line 1917192
    invoke-static {v5, v0, v3, v2}, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->a$redex0(Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;ZZI)V

    .line 1917193
    iget-object v0, p0, LX/CaF;->a:Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;

    invoke-virtual {v0, v1}, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->setIsLiked(Z)V

    .line 1917194
    iget-object v0, p0, LX/CaF;->a:Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;

    iget-object v0, v0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->l:LX/CES;

    if-eqz v0, :cond_1

    .line 1917195
    iget-object v0, p0, LX/CaF;->a:Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;

    iget-object v0, v0, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->l:LX/CES;

    iget-object v2, p0, LX/CaF;->a:Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;

    iget-object v2, v2, Lcom/facebook/photos/consumptiongallery/snowflake/SnowflakeDefaultFooterView;->n:LX/Ca6;

    invoke-virtual {v0, v2, v1}, LX/CES;->a(LX/Ca6;Z)V

    .line 1917196
    :cond_1
    const v0, 0x52b51379

    invoke-static {v0, v4}, LX/02F;->a(II)V

    goto :goto_0

    :cond_2
    move v0, v2

    .line 1917197
    goto :goto_1

    :cond_3
    move v3, v2

    .line 1917198
    goto :goto_2

    :cond_4
    move v1, v2

    .line 1917199
    goto :goto_3

    .line 1917200
    :cond_5
    add-int/lit8 v2, v2, -0x1

    goto :goto_4
.end method
