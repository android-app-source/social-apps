.class public LX/Atp;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/AsV;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "LX/0io;",
        ":",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationDoodleParamsSpec$ProvidesInspirationDoodleParams;",
        ":",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationStateSpec$ProvidesInspirationState;",
        ":",
        "LX/0is;",
        ":",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationTextParamsSpec$ProvidesInspirationTextParams;",
        "DerivedData:",
        "Ljava/lang/Object;",
        "Mutation::",
        "Lcom/facebook/ipc/composer/dataaccessor/ComposerCanSave;",
        ":",
        "Lcom/facebook/composer/attachments/ComposerAttachment$SetsAttachments",
        "<TMutation;>;:",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationLoggingDataSpec$SetsInspirationLoggingData",
        "<TMutation;>;:",
        "Lcom/facebook/friendsharing/inspiration/model/InspirationStateSpec$SetsInspirationState",
        "<TMutation;>;Services::",
        "LX/0il",
        "<TModelData;>;:",
        "LX/0ik",
        "<TDerivedData;>;:",
        "LX/0im",
        "<TMutation;>;>",
        "Ljava/lang/Object;",
        "LX/AsV;"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:LX/0jK;


# instance fields
.field private final c:LX/AsT;

.field public final d:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TServices;>;"
        }
    .end annotation
.end field

.field public final e:LX/ArL;

.field public final f:LX/Ata;

.field private final g:Lcom/facebook/widget/CustomFrameLayout;

.field private final h:LX/0TF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0TF",
            "<",
            "Lcom/facebook/ipc/media/MediaItem;",
            ">;"
        }
    .end annotation
.end field

.field public i:Landroid/content/Context;

.field public j:Lcom/facebook/composer/attachments/ComposerAttachment;

.field public k:Lcom/facebook/friendsharing/inspiration/model/InspirationTextParams;

.field public l:Ljava/lang/String;

.field private m:LX/0wW;

.field public n:LX/0wd;

.field public o:LX/0hs;

.field private p:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private q:Landroid/widget/ProgressBar;

.field public r:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public s:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/87T;",
            ">;"
        }
    .end annotation
.end field

.field public t:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/87P;",
            ">;"
        }
    .end annotation
.end field

.field public u:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/74n;",
            ">;"
        }
    .end annotation
.end field

.field public v:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/AtS;",
            ">;"
        }
    .end annotation
.end field

.field public w:LX/0Ot;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;"
        }
    .end annotation
.end field

.field public x:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8Of;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1721830
    const-class v0, LX/Atp;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Atp;->a:Ljava/lang/String;

    .line 1721831
    const-class v0, LX/Atp;

    invoke-static {v0}, LX/0jK;->a(Ljava/lang/Class;)LX/0jK;

    move-result-object v0

    sput-object v0, LX/Atp;->b:LX/0jK;

    return-void
.end method

.method public constructor <init>(LX/0il;LX/ArL;LX/Ata;Lcom/facebook/widget/CustomFrameLayout;Landroid/content/Context;LX/0wW;)V
    .locals 4
    .param p1    # LX/0il;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/ArL;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/Ata;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/widget/CustomFrameLayout;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TServices;",
            "LX/ArL;",
            "LX/Ata;",
            "Lcom/facebook/widget/CustomFrameLayout;",
            "Landroid/content/Context;",
            "LX/0wW;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1721851
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1721852
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1721853
    iput-object v0, p0, LX/Atp;->r:LX/0Ot;

    .line 1721854
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1721855
    iput-object v0, p0, LX/Atp;->s:LX/0Ot;

    .line 1721856
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1721857
    iput-object v0, p0, LX/Atp;->t:LX/0Ot;

    .line 1721858
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1721859
    iput-object v0, p0, LX/Atp;->u:LX/0Ot;

    .line 1721860
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1721861
    iput-object v0, p0, LX/Atp;->v:LX/0Ot;

    .line 1721862
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1721863
    iput-object v0, p0, LX/Atp;->w:LX/0Ot;

    .line 1721864
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1721865
    iput-object v0, p0, LX/Atp;->x:LX/0Ot;

    .line 1721866
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/Atp;->d:Ljava/lang/ref/WeakReference;

    .line 1721867
    iput-object p2, p0, LX/Atp;->e:LX/ArL;

    .line 1721868
    iput-object p3, p0, LX/Atp;->f:LX/Ata;

    .line 1721869
    iput-object p4, p0, LX/Atp;->g:Lcom/facebook/widget/CustomFrameLayout;

    .line 1721870
    iget-object v0, p0, LX/Atp;->g:Lcom/facebook/widget/CustomFrameLayout;

    const v1, 0x7f0d1793

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, LX/Atp;->p:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1721871
    iget-object v0, p0, LX/Atp;->g:Lcom/facebook/widget/CustomFrameLayout;

    const v1, 0x7f0d1794

    invoke-static {v0, v1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, LX/Atp;->q:Landroid/widget/ProgressBar;

    .line 1721872
    iput-object p5, p0, LX/Atp;->i:Landroid/content/Context;

    .line 1721873
    iput-object p6, p0, LX/Atp;->m:LX/0wW;

    .line 1721874
    new-instance v0, LX/Atm;

    invoke-direct {v0, p0}, LX/Atm;-><init>(LX/Atp;)V

    move-object v0, v0

    .line 1721875
    iput-object v0, p0, LX/Atp;->c:LX/AsT;

    .line 1721876
    new-instance v0, LX/Atn;

    invoke-direct {v0, p0}, LX/Atn;-><init>(LX/Atp;)V

    move-object v0, v0

    .line 1721877
    iput-object v0, p0, LX/Atp;->h:LX/0TF;

    .line 1721878
    new-instance v0, LX/0hs;

    iget-object v1, p0, LX/Atp;->i:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/0hs;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/Atp;->o:LX/0hs;

    .line 1721879
    iget-object v0, p0, LX/Atp;->o:LX/0hs;

    sget-object v1, LX/3AV;->ABOVE:LX/3AV;

    invoke-virtual {v0, v1}, LX/0ht;->a(LX/3AV;)V

    .line 1721880
    iget-object v0, p0, LX/Atp;->o:LX/0hs;

    iget-object v1, p0, LX/Atp;->g:Lcom/facebook/widget/CustomFrameLayout;

    invoke-virtual {v0, v1}, LX/0ht;->c(Landroid/view/View;)V

    .line 1721881
    iget-object v0, p0, LX/Atp;->m:LX/0wW;

    invoke-virtual {v0}, LX/0wW;->a()LX/0wd;

    move-result-object v0

    invoke-static {}, LX/87U;->a()LX/0wT;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v0

    new-instance v1, LX/Ato;

    iget-object v2, p0, LX/Atp;->p:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v3, p0, LX/Atp;->q:Landroid/widget/ProgressBar;

    invoke-direct {v1, p0, v2, v3}, LX/Ato;-><init>(LX/Atp;Landroid/view/View;Landroid/view/View;)V

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0xi;)LX/0wd;

    move-result-object v0

    iput-object v0, p0, LX/Atp;->n:LX/0wd;

    .line 1721882
    return-void
.end method

.method public static a$redex0(LX/Atp;I)V
    .locals 1

    .prologue
    .line 1721848
    iget-object v0, p0, LX/Atp;->o:LX/0hs;

    invoke-virtual {v0, p1}, LX/0hs;->b(I)V

    .line 1721849
    iget-object v0, p0, LX/Atp;->o:LX/0hs;

    invoke-virtual {v0}, LX/0ht;->d()V

    .line 1721850
    return-void
.end method

.method public static a$redex0(LX/Atp;Z)V
    .locals 4

    .prologue
    .line 1721883
    iget-object v0, p0, LX/Atp;->n:LX/0wd;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, LX/0wd;->a(D)LX/0wd;

    move-result-object v0

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    .line 1721884
    if-eqz p1, :cond_0

    const v0, 0x7f08278f

    :goto_0
    invoke-static {p0, v0}, LX/Atp;->a$redex0(LX/Atp;I)V

    .line 1721885
    return-void

    .line 1721886
    :cond_0
    const v0, 0x7f082788

    goto :goto_0
.end method

.method public static e$redex0(LX/Atp;)V
    .locals 7

    .prologue
    .line 1721836
    iget-object v0, p0, LX/Atp;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0il;

    .line 1721837
    iget-object v1, p0, LX/Atp;->s:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/87T;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    invoke-virtual {v1, v0}, LX/87T;->a(LX/0io;)Lcom/facebook/composer/attachments/ComposerAttachment;

    move-result-object v1

    .line 1721838
    iget-object v0, p0, LX/Atp;->v:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AtS;

    .line 1721839
    invoke-static {v1}, LX/7kq;->a(Lcom/facebook/composer/attachments/ComposerAttachment;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1721840
    invoke-virtual {v1}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v2

    check-cast v2, Lcom/facebook/photos/base/media/VideoItem;

    invoke-virtual {v1}, Lcom/facebook/composer/attachments/ComposerAttachment;->f()Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    move-result-object v3

    .line 1721841
    iget-object v4, v0, LX/AtS;->d:LX/1Er;

    const-string v5, "FB_VIDEO_FOR_UPLOAD"

    const-string v6, ".mp4"

    sget-object v1, LX/46h;->REQUIRE_PRIVATE:LX/46h;

    invoke-virtual {v4, v5, v6, v1}, LX/1Er;->a(Ljava/lang/String;Ljava/lang/String;LX/46h;)Ljava/io/File;

    move-result-object v4

    .line 1721842
    iget-object v5, v0, LX/AtS;->f:LX/8Of;

    .line 1721843
    iget-object v6, v5, LX/8Of;->b:LX/0TD;

    new-instance v1, LX/8Oe;

    invoke-direct {v1, v5, v2, v3, v4}, LX/8Oe;-><init>(LX/8Of;Lcom/facebook/photos/base/media/VideoItem;Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;Ljava/io/File;)V

    invoke-interface {v6, v1}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    move-object v4, v6

    .line 1721844
    move-object v2, v4

    .line 1721845
    :goto_0
    move-object v1, v2

    .line 1721846
    iget-object v2, p0, LX/Atp;->h:LX/0TF;

    iget-object v0, p0, LX/Atp;->w:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    invoke-static {v1, v2, v0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1721847
    return-void

    :cond_0
    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, LX/AtS;->a(LX/AtS;Lcom/facebook/composer/attachments/ComposerAttachment;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/AsT;
    .locals 1

    .prologue
    .line 1721835
    iget-object v0, p0, LX/Atp;->c:LX/AsT;

    return-object v0
.end method

.method public final a(Lcom/facebook/drawee/fbpipeline/FbDraweeView;)V
    .locals 2

    .prologue
    .line 1721832
    iput-object p1, p0, LX/Atp;->p:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1721833
    iget-object v0, p0, LX/Atp;->p:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const v1, 0x7f021552

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setImageResource(I)V

    .line 1721834
    return-void
.end method
