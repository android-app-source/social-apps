.class public LX/AuP;
.super LX/3x6;
.source ""


# instance fields
.field public final a:I


# direct methods
.method public constructor <init>(ILandroid/content/Context;)V
    .locals 1
    .param p1    # I
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1722568
    invoke-direct {p0}, LX/3x6;-><init>()V

    .line 1722569
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/AuP;->a:I

    .line 1722570
    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Rect;Landroid/view/View;Landroid/support/v7/widget/RecyclerView;LX/1Ok;)V
    .locals 2

    .prologue
    .line 1722571
    iget v0, p0, LX/AuP;->a:I

    iput v0, p1, Landroid/graphics/Rect;->left:I

    .line 1722572
    invoke-static {p2}, Landroid/support/v7/widget/RecyclerView;->d(Landroid/view/View;)I

    move-result v0

    .line 1722573
    iget-object v1, p3, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object v1, v1

    .line 1722574
    invoke-virtual {v1}, LX/1OM;->ij_()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_0

    .line 1722575
    iget v0, p0, LX/AuP;->a:I

    iput v0, p1, Landroid/graphics/Rect;->right:I

    .line 1722576
    :cond_0
    return-void
.end method
