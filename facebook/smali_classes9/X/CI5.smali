.class public final LX/CI5;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 23

    .prologue
    .line 1871950
    const/16 v19, 0x0

    .line 1871951
    const/16 v18, 0x0

    .line 1871952
    const/16 v17, 0x0

    .line 1871953
    const/16 v16, 0x0

    .line 1871954
    const/4 v15, 0x0

    .line 1871955
    const/4 v14, 0x0

    .line 1871956
    const/4 v13, 0x0

    .line 1871957
    const/4 v12, 0x0

    .line 1871958
    const/4 v11, 0x0

    .line 1871959
    const/4 v10, 0x0

    .line 1871960
    const/4 v9, 0x0

    .line 1871961
    const/4 v8, 0x0

    .line 1871962
    const/4 v7, 0x0

    .line 1871963
    const/4 v6, 0x0

    .line 1871964
    const/4 v5, 0x0

    .line 1871965
    const/4 v4, 0x0

    .line 1871966
    const/4 v3, 0x0

    .line 1871967
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v20

    sget-object v21, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    if-eq v0, v1, :cond_1

    .line 1871968
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1871969
    const/4 v3, 0x0

    .line 1871970
    :goto_0
    return v3

    .line 1871971
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 1871972
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v20

    sget-object v21, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    if-eq v0, v1, :cond_11

    .line 1871973
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v20

    .line 1871974
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 1871975
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v21

    sget-object v22, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    if-eq v0, v1, :cond_1

    if-eqz v20, :cond_1

    .line 1871976
    const-string v21, "__type__"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-nez v21, :cond_2

    const-string v21, "__typename"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_3

    .line 1871977
    :cond_2
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v19

    goto :goto_1

    .line 1871978
    :cond_3
    const-string v21, "action"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_4

    .line 1871979
    invoke-static/range {p0 .. p1}, LX/CHy;->a(LX/15w;LX/186;)I

    move-result v18

    goto :goto_1

    .line 1871980
    :cond_4
    const-string v21, "color"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_5

    .line 1871981
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v17

    goto :goto_1

    .line 1871982
    :cond_5
    const-string v21, "do_action"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_6

    .line 1871983
    invoke-static/range {p0 .. p1}, LX/CHy;->a(LX/15w;LX/186;)I

    move-result v16

    goto :goto_1

    .line 1871984
    :cond_6
    const-string v21, "document_element_type"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_7

    .line 1871985
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v15

    goto/16 :goto_1

    .line 1871986
    :cond_7
    const-string v21, "element_descriptor"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_8

    .line 1871987
    invoke-static/range {p0 .. p1}, LX/CI9;->a(LX/15w;LX/186;)I

    move-result v14

    goto/16 :goto_1

    .line 1871988
    :cond_8
    const-string v21, "element_text"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_9

    .line 1871989
    invoke-static/range {p0 .. p1}, LX/8al;->a(LX/15w;LX/186;)I

    move-result v13

    goto/16 :goto_1

    .line 1871990
    :cond_9
    const-string v21, "grid_width_percent"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_a

    .line 1871991
    const/4 v4, 0x1

    .line 1871992
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v12

    goto/16 :goto_1

    .line 1871993
    :cond_a
    const-string v21, "is_on"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_b

    .line 1871994
    const/4 v3, 0x1

    .line 1871995
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v11

    goto/16 :goto_1

    .line 1871996
    :cond_b
    const-string v21, "logging_token"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_c

    .line 1871997
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto/16 :goto_1

    .line 1871998
    :cond_c
    const-string v21, "off_text"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_d

    .line 1871999
    invoke-static/range {p0 .. p1}, LX/8al;->a(LX/15w;LX/186;)I

    move-result v9

    goto/16 :goto_1

    .line 1872000
    :cond_d
    const-string v21, "on_text"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_e

    .line 1872001
    invoke-static/range {p0 .. p1}, LX/8al;->a(LX/15w;LX/186;)I

    move-result v8

    goto/16 :goto_1

    .line 1872002
    :cond_e
    const-string v21, "style_list"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_f

    .line 1872003
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v7

    goto/16 :goto_1

    .line 1872004
    :cond_f
    const-string v21, "target_uri"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_10

    .line 1872005
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_1

    .line 1872006
    :cond_10
    const-string v21, "undo_action"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_0

    .line 1872007
    invoke-static/range {p0 .. p1}, LX/CHy;->a(LX/15w;LX/186;)I

    move-result v5

    goto/16 :goto_1

    .line 1872008
    :cond_11
    const/16 v20, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 1872009
    const/16 v20, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1872010
    const/16 v19, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v19

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1872011
    const/16 v18, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1872012
    const/16 v17, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v16

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 1872013
    const/16 v16, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v1, v15}, LX/186;->b(II)V

    .line 1872014
    const/4 v15, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v15, v14}, LX/186;->b(II)V

    .line 1872015
    const/4 v14, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v13}, LX/186;->b(II)V

    .line 1872016
    if-eqz v4, :cond_12

    .line 1872017
    const/4 v4, 0x7

    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v12, v13}, LX/186;->a(III)V

    .line 1872018
    :cond_12
    if-eqz v3, :cond_13

    .line 1872019
    const/16 v3, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v11}, LX/186;->a(IZ)V

    .line 1872020
    :cond_13
    const/16 v3, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v10}, LX/186;->b(II)V

    .line 1872021
    const/16 v3, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9}, LX/186;->b(II)V

    .line 1872022
    const/16 v3, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v8}, LX/186;->b(II)V

    .line 1872023
    const/16 v3, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 1872024
    const/16 v3, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 1872025
    const/16 v3, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v5}, LX/186;->b(II)V

    .line 1872026
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 5

    .prologue
    const/16 v4, 0xc

    const/4 v3, 0x4

    const/4 v2, 0x0

    .line 1872027
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 1872028
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 1872029
    if-eqz v0, :cond_0

    .line 1872030
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1872031
    invoke-static {p0, p1, v2, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1872032
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1872033
    if-eqz v0, :cond_1

    .line 1872034
    const-string v1, "action"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1872035
    invoke-static {p0, v0, p2, p3}, LX/CHy;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1872036
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1872037
    if-eqz v0, :cond_2

    .line 1872038
    const-string v1, "color"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1872039
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1872040
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1872041
    if-eqz v0, :cond_3

    .line 1872042
    const-string v1, "do_action"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1872043
    invoke-static {p0, v0, p2, p3}, LX/CHy;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1872044
    :cond_3
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 1872045
    if-eqz v0, :cond_4

    .line 1872046
    const-string v0, "document_element_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1872047
    invoke-virtual {p0, p1, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1872048
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1872049
    if-eqz v0, :cond_5

    .line 1872050
    const-string v1, "element_descriptor"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1872051
    invoke-static {p0, v0, p2, p3}, LX/CI9;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1872052
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1872053
    if-eqz v0, :cond_6

    .line 1872054
    const-string v1, "element_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1872055
    invoke-static {p0, v0, p2, p3}, LX/8al;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1872056
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 1872057
    if-eqz v0, :cond_7

    .line 1872058
    const-string v1, "grid_width_percent"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1872059
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 1872060
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1872061
    if-eqz v0, :cond_8

    .line 1872062
    const-string v1, "is_on"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1872063
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 1872064
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1872065
    if-eqz v0, :cond_9

    .line 1872066
    const-string v1, "logging_token"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1872067
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1872068
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1872069
    if-eqz v0, :cond_a

    .line 1872070
    const-string v1, "off_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1872071
    invoke-static {p0, v0, p2, p3}, LX/8al;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1872072
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1872073
    if-eqz v0, :cond_b

    .line 1872074
    const-string v1, "on_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1872075
    invoke-static {p0, v0, p2, p3}, LX/8al;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1872076
    :cond_b
    invoke-virtual {p0, p1, v4}, LX/15i;->g(II)I

    move-result v0

    .line 1872077
    if-eqz v0, :cond_c

    .line 1872078
    const-string v0, "style_list"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1872079
    invoke-virtual {p0, p1, v4}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 1872080
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1872081
    if-eqz v0, :cond_d

    .line 1872082
    const-string v1, "target_uri"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1872083
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1872084
    :cond_d
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 1872085
    if-eqz v0, :cond_e

    .line 1872086
    const-string v1, "undo_action"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1872087
    invoke-static {p0, v0, p2, p3}, LX/CHy;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1872088
    :cond_e
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 1872089
    return-void
.end method
