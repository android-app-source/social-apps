.class public LX/ApJ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:LX/Apn;


# direct methods
.method public constructor <init>(LX/Apn;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1715791
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1715792
    iput-object p1, p0, LX/ApJ;->a:LX/Apn;

    .line 1715793
    return-void
.end method

.method public static a(LX/0QB;)LX/ApJ;
    .locals 4

    .prologue
    .line 1715797
    const-class v1, LX/ApJ;

    monitor-enter v1

    .line 1715798
    :try_start_0
    sget-object v0, LX/ApJ;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1715799
    sput-object v2, LX/ApJ;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1715800
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1715801
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1715802
    new-instance p0, LX/ApJ;

    invoke-static {v0}, LX/Apn;->a(LX/0QB;)LX/Apn;

    move-result-object v3

    check-cast v3, LX/Apn;

    invoke-direct {p0, v3}, LX/ApJ;-><init>(LX/Apn;)V

    .line 1715803
    move-object v0, p0

    .line 1715804
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1715805
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/ApJ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1715806
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1715807
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;ILjava/lang/CharSequence;ILjava/lang/CharSequence;Landroid/util/SparseArray;LX/1dQ;Landroid/view/View$OnClickListener;LX/1dQ;Landroid/widget/CompoundButton$OnCheckedChangeListener;LX/1X1;)LX/1Dg;
    .locals 3
    .param p2    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation

        .annotation build Lcom/facebook/fig/components/attachment/annotations/FigAttachmentFooterActionType;
        .end annotation
    .end param
    .param p3    # Ljava/lang/CharSequence;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->STRING:LX/32B;
        .end annotation
    .end param
    .param p4    # I
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation

        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p5    # Ljava/lang/CharSequence;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->STRING:LX/32B;
        .end annotation
    .end param
    .param p6    # Landroid/util/SparseArray;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p7    # LX/1dQ;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p8    # Landroid/view/View$OnClickListener;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p9    # LX/1dQ;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p10    # Landroid/widget/CompoundButton$OnCheckedChangeListener;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p11    # LX/1X1;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation

        .annotation runtime Ljava/lang/Deprecated;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "I",
            "Ljava/lang/CharSequence;",
            "I",
            "Ljava/lang/CharSequence;",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/Object;",
            ">;",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;",
            "Landroid/view/View$OnClickListener;",
            "LX/1dQ",
            "<",
            "LX/Apr;",
            ">;",
            "Landroid/widget/CompoundButton$OnCheckedChangeListener;",
            "LX/1X1",
            "<*>;)",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    .line 1715794
    packed-switch p2, :pswitch_data_0

    .line 1715795
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported attachment type = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1715796
    :pswitch_0
    iget-object v0, p0, LX/ApJ;->a:LX/Apn;

    invoke-virtual {v0, p1}, LX/Apn;->c(LX/1De;)LX/Apl;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/Apl;->h(I)LX/Apl;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/Apl;->a(Ljava/lang/CharSequence;)LX/Apl;

    move-result-object v0

    invoke-virtual {v0, p4}, LX/Apl;->i(I)LX/Apl;

    move-result-object v0

    invoke-virtual {v0, p5}, LX/Apl;->b(Ljava/lang/CharSequence;)LX/Apl;

    move-result-object v0

    invoke-virtual {v0, p6}, LX/Apl;->a(Landroid/util/SparseArray;)LX/Apl;

    move-result-object v0

    invoke-virtual {v0, p7}, LX/Apl;->a(LX/1dQ;)LX/Apl;

    move-result-object v0

    invoke-virtual {v0, p8}, LX/Apl;->a(Landroid/view/View$OnClickListener;)LX/Apl;

    move-result-object v0

    invoke-virtual {v0, p9}, LX/Apl;->b(LX/1dQ;)LX/Apl;

    move-result-object v0

    invoke-virtual {v0, p10}, LX/Apl;->a(Landroid/widget/CompoundButton$OnCheckedChangeListener;)LX/Apl;

    move-result-object v0

    invoke-virtual {v0, p11}, LX/Apl;->a(LX/1X1;)LX/Apl;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->b()LX/1Dg;

    move-result-object v0

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
