.class public final LX/CFq;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/CFr;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1864564
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1864565
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/CFq;->a:Ljava/util/ArrayList;

    .line 1864566
    return-void
.end method


# virtual methods
.method public final a(LX/CFr;)I
    .locals 1

    .prologue
    .line 1864558
    iget-object v0, p0, LX/CFq;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final a()LX/CFt;
    .locals 3

    .prologue
    .line 1864563
    new-instance v0, LX/CFt;

    iget-object v1, p0, LX/CFq;->a:Ljava/util/ArrayList;

    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1}, LX/CFt;-><init>(LX/0Px;)V

    return-object v0
.end method

.method public final a(Lcom/facebook/greetingcards/verve/model/VMView;Lcom/facebook/greetingcards/verve/model/VMView;)V
    .locals 4

    .prologue
    .line 1864561
    iget-object v0, p0, LX/CFq;->a:Ljava/util/ArrayList;

    new-instance v1, LX/CFr;

    sget-object v2, LX/CFs;->IN:LX/CFs;

    invoke-direct {v1, v2, p1, p2}, LX/CFr;-><init>(LX/CFs;Lcom/facebook/greetingcards/verve/model/VMView;Lcom/facebook/greetingcards/verve/model/VMView;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1864562
    return-void
.end method

.method public final b(Lcom/facebook/greetingcards/verve/model/VMView;Lcom/facebook/greetingcards/verve/model/VMView;)V
    .locals 4

    .prologue
    .line 1864559
    iget-object v0, p0, LX/CFq;->a:Ljava/util/ArrayList;

    new-instance v1, LX/CFr;

    sget-object v2, LX/CFs;->OUT:LX/CFs;

    invoke-direct {v1, v2, p1, p2}, LX/CFr;-><init>(LX/CFs;Lcom/facebook/greetingcards/verve/model/VMView;Lcom/facebook/greetingcards/verve/model/VMView;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1864560
    return-void
.end method
