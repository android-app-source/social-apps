.class public LX/BHQ;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final b:Ljava/lang/String;


# instance fields
.field public final a:LX/0Sh;

.field public c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Ck;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/8I2;

.field public e:LX/0Sg;

.field public f:LX/0TD;

.field public g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field private h:I

.field public final i:LX/BHs;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1769024
    const-class v0, LX/BHQ;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/BHQ;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0TD;LX/0Sh;LX/BHY;LX/8I2;LX/0Sg;LX/BHs;)V
    .locals 1
    .param p3    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/ForegroundExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/1Ck;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0TD;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/BHY;",
            "Lcom/facebook/photos/local/LocalMediaCursor;",
            "Lcom/facebook/common/appchoreographer/AppChoreographer;",
            "LX/BHs;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1769025
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1769026
    iput-object p1, p0, LX/BHQ;->c:LX/0Ot;

    .line 1769027
    iput-object p2, p0, LX/BHQ;->g:LX/0Ot;

    .line 1769028
    iput-object p3, p0, LX/BHQ;->f:LX/0TD;

    .line 1769029
    iput-object p4, p0, LX/BHQ;->a:LX/0Sh;

    .line 1769030
    iput-object p6, p0, LX/BHQ;->d:LX/8I2;

    .line 1769031
    iput-object p7, p0, LX/BHQ;->e:LX/0Sg;

    .line 1769032
    iput-object p8, p0, LX/BHQ;->i:LX/BHs;

    .line 1769033
    invoke-virtual {p5}, LX/BHY;->a()I

    move-result v0

    iput v0, p0, LX/BHQ;->h:I

    .line 1769034
    return-void
.end method

.method public static a(LX/0QB;)LX/BHQ;
    .locals 1

    .prologue
    .line 1769035
    invoke-static {p0}, LX/BHQ;->b(LX/0QB;)LX/BHQ;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/BHQ;
    .locals 9

    .prologue
    .line 1769036
    new-instance v0, LX/BHQ;

    const/16 v1, 0x12b1

    invoke-static {p0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0x259

    invoke-static {p0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    invoke-static {p0}, LX/0tb;->a(LX/0QB;)LX/0TD;

    move-result-object v3

    check-cast v3, LX/0TD;

    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v4

    check-cast v4, LX/0Sh;

    invoke-static {p0}, LX/BHY;->a(LX/0QB;)LX/BHY;

    move-result-object v5

    check-cast v5, LX/BHY;

    invoke-static {p0}, LX/8I3;->b(LX/0QB;)LX/8I2;

    move-result-object v6

    check-cast v6, LX/8I2;

    invoke-static {p0}, LX/0Sg;->a(LX/0QB;)LX/0Sg;

    move-result-object v7

    check-cast v7, LX/0Sg;

    invoke-static {p0}, LX/BHs;->a(LX/0QB;)LX/BHs;

    move-result-object v8

    check-cast v8, LX/BHs;

    invoke-direct/range {v0 .. v8}, LX/BHQ;-><init>(LX/0Ot;LX/0Ot;LX/0TD;LX/0Sh;LX/BHY;LX/8I2;LX/0Sg;LX/BHs;)V

    .line 1769037
    return-object v0
.end method


# virtual methods
.method public final a(LX/4gI;ZLX/BGy;LX/BHj;Ljava/lang/String;)V
    .locals 9
    .param p3    # LX/BGy;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # LX/BHj;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1769038
    iget-object v0, p0, LX/BHQ;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, LX/1Ck;

    sget-object v8, LX/BHC;->MEDIA_STORE_QUERY:LX/BHC;

    new-instance v0, LX/BHO;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p5

    move-object v4, p4

    move v5, p2

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, LX/BHO;-><init>(LX/BHQ;LX/4gI;Ljava/lang/String;LX/BHj;ZLX/BGy;)V

    new-instance v1, LX/BHP;

    invoke-direct {v1, p0}, LX/BHP;-><init>(LX/BHQ;)V

    invoke-virtual {v7, v8, v0, v1}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 1769039
    return-void
.end method
