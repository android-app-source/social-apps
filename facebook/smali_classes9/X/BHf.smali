.class public final LX/BHf;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# instance fields
.field public final synthetic a:LX/BHh;


# direct methods
.method public constructor <init>(LX/BHh;)V
    .locals 0

    .prologue
    .line 1769484
    iput-object p1, p0, LX/BHf;->a:LX/BHh;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 1769485
    return-void
.end method

.method public final onAnimationEnd(Landroid/animation/Animator;)V
    .locals 6

    .prologue
    .line 1769486
    iget-object v0, p0, LX/BHf;->a:LX/BHh;

    iget-object v0, v0, LX/BHh;->d:Landroid/view/View;

    iget-object v1, p0, LX/BHf;->a:LX/BHh;

    iget-object v1, v1, LX/BHh;->d:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getPaddingLeft()I

    move-result v1

    iget-object v2, p0, LX/BHf;->a:LX/BHh;

    iget-object v2, v2, LX/BHh;->d:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getPaddingTop()I

    move-result v2

    iget-object v3, p0, LX/BHf;->a:LX/BHh;

    iget-object v3, v3, LX/BHh;->d:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getPaddingRight()I

    move-result v3

    iget-object v4, p0, LX/BHf;->a:LX/BHh;

    iget-object v4, v4, LX/BHh;->d:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getPaddingBottom()I

    move-result v4

    iget-object v5, p0, LX/BHf;->a:LX/BHh;

    iget-object v5, v5, LX/BHh;->c:Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;

    invoke-virtual {v5}, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v4, v5

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/view/View;->setPadding(IIII)V

    .line 1769487
    return-void
.end method

.method public final onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 1769488
    return-void
.end method

.method public final onAnimationStart(Landroid/animation/Animator;)V
    .locals 2

    .prologue
    .line 1769489
    iget-object v0, p0, LX/BHf;->a:LX/BHh;

    iget-object v0, v0, LX/BHh;->c:Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/photos/simplepicker/view/SlideshowEntrypointView;->setVisibility(I)V

    .line 1769490
    return-void
.end method
