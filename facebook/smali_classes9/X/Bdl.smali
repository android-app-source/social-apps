.class public final LX/Bdl;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/Bdq;",
        ">;"
    }
.end annotation


# static fields
.field private static b:[Ljava/lang/String;

.field private static c:I


# instance fields
.field public a:LX/Bdm;

.field private d:Ljava/util/BitSet;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 1804056
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "section"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "recyclerConfiguration"

    aput-object v2, v0, v1

    sput-object v0, LX/Bdl;->b:[Ljava/lang/String;

    .line 1804057
    sput v3, LX/Bdl;->c:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1804033
    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 1804034
    new-instance v0, Ljava/util/BitSet;

    sget v1, LX/Bdl;->c:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/Bdl;->d:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/Bdl;LX/1De;IILX/Bdm;)V
    .locals 1

    .prologue
    .line 1804052
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 1804053
    iput-object p4, p0, LX/Bdl;->a:LX/Bdm;

    .line 1804054
    iget-object v0, p0, LX/Bdl;->d:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 1804055
    return-void
.end method


# virtual methods
.method public final a(LX/1X1;)LX/Bdl;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1",
            "<*>;)",
            "LX/Bdl;"
        }
    .end annotation

    .prologue
    .line 1804050
    iget-object v0, p0, LX/Bdl;->a:LX/Bdm;

    iput-object p1, v0, LX/Bdm;->c:LX/1X1;

    .line 1804051
    return-object p0
.end method

.method public final a(LX/3x6;)LX/Bdl;
    .locals 1

    .prologue
    .line 1804048
    iget-object v0, p0, LX/Bdl;->a:LX/Bdm;

    iput-object p1, v0, LX/Bdm;->j:LX/3x6;

    .line 1804049
    return-object p0
.end method

.method public final a(LX/5K7;)LX/Bdl;
    .locals 1

    .prologue
    .line 1804046
    iget-object v0, p0, LX/Bdl;->a:LX/Bdm;

    iput-object p1, v0, LX/Bdm;->k:LX/5K7;

    .line 1804047
    return-object p0
.end method

.method public final a(LX/BcO;)LX/Bdl;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/BcO",
            "<*>;)",
            "LX/Bdl;"
        }
    .end annotation

    .prologue
    .line 1804058
    iget-object v0, p0, LX/Bdl;->a:LX/Bdm;

    iput-object p1, v0, LX/Bdm;->a:LX/BcO;

    .line 1804059
    iget-object v0, p0, LX/Bdl;->d:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1804060
    return-object p0
.end method

.method public final a(LX/Bdb;)LX/Bdl;
    .locals 2

    .prologue
    .line 1804043
    iget-object v0, p0, LX/Bdl;->a:LX/Bdm;

    iput-object p1, v0, LX/Bdm;->b:LX/Bdb;

    .line 1804044
    iget-object v0, p0, LX/Bdl;->d:Ljava/util/BitSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 1804045
    return-object p0
.end method

.method public final a(Z)LX/Bdl;
    .locals 1

    .prologue
    .line 1804041
    iget-object v0, p0, LX/Bdl;->a:LX/Bdm;

    iput-boolean p1, v0, LX/Bdm;->m:Z

    .line 1804042
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 1804037
    invoke-super {p0}, LX/1X5;->a()V

    .line 1804038
    const/4 v0, 0x0

    iput-object v0, p0, LX/Bdl;->a:LX/Bdm;

    .line 1804039
    sget-object v0, LX/Bdq;->b:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 1804040
    return-void
.end method

.method public final b(LX/1X1;)LX/Bdl;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1",
            "<*>;)",
            "LX/Bdl;"
        }
    .end annotation

    .prologue
    .line 1804035
    iget-object v0, p0, LX/Bdl;->a:LX/Bdm;

    iput-object p1, v0, LX/Bdm;->d:LX/1X1;

    .line 1804036
    return-object p0
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/Bdq;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1804023
    iget-object v1, p0, LX/Bdl;->d:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/Bdl;->d:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    sget v2, LX/Bdl;->c:I

    if-ge v1, v2, :cond_2

    .line 1804024
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1804025
    :goto_0
    sget v2, LX/Bdl;->c:I

    if-ge v0, v2, :cond_1

    .line 1804026
    iget-object v2, p0, LX/Bdl;->d:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1804027
    sget-object v2, LX/Bdl;->b:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1804028
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1804029
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1804030
    :cond_2
    iget-object v0, p0, LX/Bdl;->a:LX/Bdm;

    .line 1804031
    invoke-virtual {p0}, LX/Bdl;->a()V

    .line 1804032
    return-object v0
.end method
