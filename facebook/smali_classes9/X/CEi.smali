.class public final LX/CEi;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/friendsharing/souvenirs/verve/SouvenirVerveVideoView;

.field public final synthetic b:Lcom/facebook/graphql/model/GraphQLVideo;

.field public final synthetic c:Lcom/facebook/friendsharing/souvenirs/verve/SouvenirsVerveViewSupplier;


# direct methods
.method public constructor <init>(Lcom/facebook/friendsharing/souvenirs/verve/SouvenirsVerveViewSupplier;Lcom/facebook/friendsharing/souvenirs/verve/SouvenirVerveVideoView;Lcom/facebook/graphql/model/GraphQLVideo;)V
    .locals 0

    .prologue
    .line 1861745
    iput-object p1, p0, LX/CEi;->c:Lcom/facebook/friendsharing/souvenirs/verve/SouvenirsVerveViewSupplier;

    iput-object p2, p0, LX/CEi;->a:Lcom/facebook/friendsharing/souvenirs/verve/SouvenirVerveVideoView;

    iput-object p3, p0, LX/CEi;->b:Lcom/facebook/graphql/model/GraphQLVideo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0x1c44ead5

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1861746
    iget-object v1, p0, LX/CEi;->a:Lcom/facebook/friendsharing/souvenirs/verve/SouvenirVerveVideoView;

    .line 1861747
    sget-object v2, LX/04g;->BY_AUTOPLAY:LX/04g;

    invoke-virtual {v1, v2}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/04g;)V

    .line 1861748
    iget-object v1, p0, LX/CEi;->a:Lcom/facebook/friendsharing/souvenirs/verve/SouvenirVerveVideoView;

    invoke-virtual {v1}, Lcom/facebook/friendsharing/souvenirs/verve/SouvenirVerveVideoView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, LX/04D;->VERVE:LX/04D;

    invoke-static {v1, v2}, Lcom/facebook/video/activity/FullScreenVideoPlayerActivity;->a(Landroid/content/Context;LX/04D;)Landroid/content/Intent;

    move-result-object v1

    .line 1861749
    const-string v2, "video_graphql_object"

    iget-object v3, p0, LX/CEi;->b:Lcom/facebook/graphql/model/GraphQLVideo;

    invoke-static {v1, v2, v3}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1861750
    iget-object v2, p0, LX/CEi;->c:Lcom/facebook/friendsharing/souvenirs/verve/SouvenirsVerveViewSupplier;

    iget-object v2, v2, Lcom/facebook/friendsharing/souvenirs/verve/SouvenirsVerveViewSupplier;->d:Lcom/facebook/content/SecureContextHelper;

    iget-object v3, p0, LX/CEi;->a:Lcom/facebook/friendsharing/souvenirs/verve/SouvenirVerveVideoView;

    invoke-virtual {v3}, Lcom/facebook/friendsharing/souvenirs/verve/SouvenirVerveVideoView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v2, v1, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1861751
    const v1, -0x34d344c2

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
